<?php

class LabelAnnotation
{
    /**
     * @param $text
     * @param Imagick $im
     * @param int $baseX
     * @param int $baseY
     * @param bool $vertical
     */
    public static function createAnnotation($text, Imagick &$im, int $baseX = 0, int $baseY = 0, bool $vertical = false, $border = true, $fontSize = 8, $bold = false)
    {
        $text = strtoupper($text);

        $negative = 1;
        $yTextFix = 33;
        $xTextFix = 5;
        $yBoxFix = 1;
        $xBoxFix = 2;

        if($vertical) {
            $negative = -1;
            $yTextFix = -10;
            $xTextFix = 5;
            $yBoxFix = -1;
            $xBoxFix = 1;

            $temp = $baseX;
            $baseX = $baseY;
            $baseY = -$temp;
        }

        $draw = new ImagickDraw();

        if($vertical)
            $draw->rotate(90);

        $draw->setFillColor('black');
        $draw->setFontSize( $fontSize);
        if($bold)
            $draw->setFontWeight(800);
        $draw->annotation($baseX + $xTextFix, $baseY + $yTextFix, $text);
        $metrics = $im->queryFontMetrics($draw, $text);
        $draw->setStrokeColor('black');
        $draw->setFillColor('white');
        $draw->setFillOpacity(0);
        $draw->setStrokeOpacity(1);
        $draw->setStrokeWidth(3);
        if($border)
            $draw->rectangle($baseX + $xBoxFix,$yBoxFix + $baseY, $baseX + $metrics['textWidth'] * 4.3 + $xBoxFix, $yBoxFix + $baseY + $negative * 40);

        $im->drawImage($draw);
    }
}