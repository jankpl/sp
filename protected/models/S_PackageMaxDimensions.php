<?php

class S_PackageMaxDimensions
{
    protected static $_packageMaxDimensions = Array(
        'l' => 140,
        'd' => 140,
        'w' => 140,
        'combination' => 370,
        'weight' => 50,
    );

    public static function getFinalWeight(Courier $courier)
    {
        $dw = self::calculateDimensionsWeight($courier);

        if($dw > $courier->package_weight)
            $weight =  $dw;
        else
            $weight =  $courier->package_weight;

        $weight = round($weight,2);
        return $weight;
    }

    public static function calculateDimensionsWeightBySizes($l, $d, $w)
    {
        $courier = new Courier();
        $courier->package_size_d = $d;
        $courier->package_size_l = $l;
        $courier->package_size_w = $w;

        $size = self::calculateDimensionsWeight($courier);

        return $size;
    }

    public static function calculateDimensionsWeight(Courier $courier)
    {
        $dw = Courier::calculateDimensionsWeight($courier);

        return $dw;
    }

    public static function isPackageTooBig(Courier $courier)
    {
        if($courier->_disableAdditionalWeightAndSizeCheck)
            return false;

        $dimensions = [
            $courier->package_size_l,
            $courier->package_size_d,
            $courier->package_size_w,
        ];

        // biggest dimension is length and is first
        rsort($dimensions);

        if(
            $courier->package_size_l <= self::$_packageMaxDimensions['l']
            &&
            $courier->package_size_d <= self::$_packageMaxDimensions['d']
            &&
            $courier->package_size_w <= self::$_packageMaxDimensions['w']
            &&
            $dimensions[0] + 2 * $dimensions[1] + 2 * $dimensions[2] <= self::$_packageMaxDimensions['combination']
        )
            return false;
        else
            return true;
    }

    // used in Courier Simple and OOE
    public static function isPackageTooHeavy(Courier $courier, $internal = false)
    {
        if($courier->_disableAdditionalWeightAndSizeCheck)
            return false;

        if($internal)
            return $courier->package_weight > CourierTypeInternal::MAX_PACKAGE_WEIGHT;


        if($courier->package_weight <= self::$_packageMaxDimensions['weight'])
            return false;
        else return true;
    }

    public static function getPackageMaxDimensions()
    {
        return self::$_packageMaxDimensions;
    }
}