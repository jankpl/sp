<?php
/* @var $model CountryGroup */
?>

<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'country-group-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->

    <div class="row">

        <?php
        echo CHtml::listBox('countryList',$model->listCountriesForGroup(true),CHtml::listData($model->listUngroupedCountriesAndMine(),'id','name'), array('multiple' => true, 'size' => 15));?>

    </div>


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->