<div class="alert alert-success">
    <h4><?php echo Yii::t('postal','List przewozowy');?></h4>
    <br/>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'mass-carriage-ticket',
        'action' => Yii::app()->createUrl('/postal/postal/CarriageTicket', array('/postal/postal/CarriageTicket', 'urlData' => $model->hash)),
        'htmlOptions' =>
            [
                'class' => 'do-not-block',
                'target' => '_blank',
            ],
    ));
    ?>
    <?php
    echo CHtml::dropDownList('type','', PostalLabelPrinter::getTypeNames(), array('style' => 'width: 300px; display: inline-block; height: 23px; vertical-align: top;'));
    ?>

    <?php echo TbHtml::submitButton(Yii::t('postal','Generuj list przewozowy'));?>
    <?php $this->endWidget();?>

</div>
