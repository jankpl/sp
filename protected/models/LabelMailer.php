<?php



use Zebra\Zpl\Image;
use Zebra\Zpl\Builder;

class LabelMailer extends BasePrintServer
{

    protected static function getLabelPdf(CourierLabelNew $courierLabelNew)
    {
        $userAllowExtendedLabelGeneration = $courierLabelNew->courier->user->getAllowCourierInternalExtendedLabel();
        return LabelPrinter::generateLabels($courierLabelNew->courier, LabelPrinter::PDF_4_ON_1, true, $userAllowExtendedLabelGeneration, true);
    }


    public static function newCourierLabelNotify(CourierLabelNew $courierLabelNew)
    {
        if($courierLabelNew->stat == CourierLabelNew::STAT_SUCCESS)
        {
            $pdf = self::getLabelPdf($courierLabelNew);
            $text = Yii::t('courier', 'W załączniku znajdziesz etykietę dla swojej paczki.{br}{br}Wydrukują ją i przyklej do paczki.{br}{br}Inne formaty etykiety możesz znaleźć w szczegółach paczki, po zalogowaniu się na swoje konto!', ['{br}' => '<br/>']);

            if(!in_array($courierLabelNew->courier->receiverAddressData->country_id, CountryList::getUeList()))
                $text = Yii::t('courier', '{br}{br}Pamiętaj o dołączeniu dokumentów celnych - możesz je pobrać w szczegółach paczki!', ['{br}' => '<br/>']);

            if($courierLabelNew->courier->senderAddressData->email)
                $receiverAddress = $courierLabelNew->courier->senderAddressData->email;
            else
                $receiverAddress = $courierLabelNew->courier->user->email;

            if($pdf)
                S_Mailer::sendToCustomer($receiverAddress, $receiverAddress, 'Etykieta #'.$courierLabelNew->courier->local_id, $text, NULL, NULL, true, false, 'etykieta_'.$courierLabelNew->courier->local_id.'.pdf', false, false, null, $pdf);


        }
        return false;
    }

}