<?php

$order = $_POST['order'];
parse_str($order, $order);
$order = $order['attr'];

if(is_array($order))
    foreach($order AS $key => $item)
        $order[$key] = intval($item);

if(is_array($order)) {
    foreach ($order AS $item) {
        $item = intval($item);
        echo F_OoeImport::getAttributesList()[$item] . '<br/>';
    }

    Yii::app()->cache->set('user_ooe_import_order_' . Yii::app()->user->id, $order);
}
?>

    <ul id="sortable">
        <?php
        $data = F_OoeImport::getAttributesList();

        $order = Yii::app()->cache->get('user_ooe_import_order_'.Yii::app()->user->id);

        if(is_array($order))
            $data = array_replace(array_flip($order), $data);

        if(is_array($data))
            foreach($data AS $key => $item):
                $item = str_replace('_', ' ', $item);
                $item = ucwords($item);
                ?>
                <li class="ui-state-default" id="attr-<?= $key;?>" title="<?= Yii::t('site', 'Przytrzymaj i przeciągnij');?>"><?= $item;?></li>
                <?php
            endforeach;
        ?>
    </ul>

    <form method="POST">
        <input name="order" type="text" id="order" value="" />

        <input type="submit" id="action" />
    </form>

    <script>
        $(function() {
            $( "#sortable" ).sortable({
                placeholder: "ui-state-highlight"
            });
            $( "#sortable" ).disableSelection();


        });

        $('#action').on('click', function(e){
            e.preventDefault();
            $('#order').val($('#sortable').sortable('serialize'));
            $(this).closest('form').submit();

        });
    </script>
<?php
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/custom-theme/jquery-ui-1.10.3.custom.min.css');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
?>