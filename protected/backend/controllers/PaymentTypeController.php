<?php

class PaymentTypeController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }


    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'PaymentType'),
		));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'PaymentType');


		if (isset($_POST['PaymentType'])) {
			$model->setAttributes($_POST['PaymentType']);

            $model->commissionPrice = Price::generateByPost($_POST['PriceValue']);

			if ($model->saveWithPrices()) {

				$this->redirect(array('view', 'id' => $model->id));
			}

		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionIndex() {

        $this->redirect(array('admin'));
	}

	public function actionAdmin() {
		$model = new PaymentType('search');
		$model->unsetAttributes();

		if (isset($_GET['PaymentType']))
			$model->setAttributes($_GET['PaymentType']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}