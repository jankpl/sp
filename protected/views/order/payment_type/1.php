<?php
/* @var $model Order */
?>

<h2><?php echo Yii::t('order','Płatność online');?></h2>
<p class="text-center"><?php echo Yii::t('order','Wybrałeś płatność online.');?></p>
<br/><br/><br/>
<div style="text-align: center;">

    <form action="<?php echo Yii::app()->createUrl('payment/pay', array('hash' => $model->hash, 'cs' => $model->calculateCS()));?>" method="post" class="form">
      <button name="submit_send" type="submit" class="btn btn-lg btn-success"><?php echo Yii::t('pay','Zapłać przez Przelewy24');?></button>
    </form>
<br/>
<br/>
<br/>
    <img src="<?php echo Yii::app()->getBaseUrl(true);?>/images/layout/przelewy24.png" />
</div>

