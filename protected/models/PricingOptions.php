<?php

Yii::import('application.models._base.BasePricingOptions');

class PricingOptions extends BasePricingOptions
{
    const OPTION_GROUP_MAIL_SPEED = 1;
    const OPTION_1_PRIORITY = 1;

    const OPTION_GROUP_MAIL_REGISTERED = 2;
    const OPTION_2_REGISTERED = 1;
    const OPTION_2_REGISTERED_SIZE_B = 2;

    const OPTION_GROUP_MAIL_SIZE = 3;
    const OPTION_3_SIZE_A = 1;
    const OPTION_3_SIZE_B = 2;

    const OPTION_GROUP_COURIER_INSURANCE = 4;
    const OPTION_4_INSURANCE = 1;

    const OPTION_GROUP_COURIER_COD = 5;
    const OPTION_5_COD = 1;

    const OPTION_GROUP_COURIER_EXPRESS = 6;
    const OPTION_6_EXPRESS = 1;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function getOptions()
    {
        $options = [];

        $options[self::OPTION_GROUP_MAIL_SPEED] = array(
            'name' => Yii::t('m_pricingOptions', 'Priorytet'),
            'options' => array(
                self::OPTION_1_PRIORITY => array(
                    'name' => Yii::t('m_pricingOptions', 'Priorytet'),
                    'tooltip' => Yii::t('m_pricingOptions', 'priorytet tooltip')),
            ));

        $options[self::OPTION_GROUP_MAIL_REGISTERED] = array(
            'forceCheckbox' => true,
            'name' => Yii::t('m_pricingOptions', 'Polecony'),
            'options' => array(
                self::OPTION_2_REGISTERED => array(
                    'name' => Yii::t('m_pricingOptions', 'Polecony'),
                    'tooltip' => Yii::t('m_pricingOptions', 'polecony_tooltip')),
                self::OPTION_2_REGISTERED_SIZE_B => array(
                    'name' => Yii::t('m_pricingOptions', 'Polecony Gabaryt B'),
                    'tooltip' => Yii::t('m_pricingOptions', 'polecony_gabaryt_b_tooltip'),
                    'hidden' => true,),

            ));

        $options[self::OPTION_GROUP_MAIL_SIZE] = array(
            'name' => Yii::t('m_pricingOptions', 'Gabaryt'),
            'options' => array(
                self::OPTION_3_SIZE_A => array(
                    'name' => Yii::t('m_pricingOptions', 'Gabaryt A'),
                    'tooltip' => Yii::t('m_pricingOptions', 'gabaryt A tooltip')),
                self::OPTION_3_SIZE_B => array(
                    'name' => Yii::t('m_pricingOptions', 'Gabaryt B'),
                    'tooltip' => Yii::t('m_pricingOptions', 'gabaryt b tooltip')),
            ));


        $options[self::OPTION_GROUP_COURIER_INSURANCE] = array(
            'name' => Yii::t('m_pricingOptions', 'Dodatkowe ubezpiczenienie'),
            'options' => array(
                self::OPTION_4_INSURANCE => array(
                    'name' => Yii::t('m_pricingOptions', 'Dodatkowe ubezpieczenie'),
                    'tooltip' => Yii::t('m_pricingOptions', 'dodatkowe ubezpieczenie tooltip')),
            ));

        $options[self::OPTION_GROUP_COURIER_COD] = array(
            'name' => Yii::t('m_pricingOptions', 'Za pobraniem'),
            'options' => array(
                self::OPTION_GROUP_COURIER_COD => array(
                    'name' => Yii::t('m_pricingOptions', 'Za pobraniem'),
                    'tooltip' => Yii::t('m_pricingOptions', 'za pobraniem tooltip')),
            ));

        $options[self::OPTION_GROUP_COURIER_EXPRESS] = array(
            'name' => Yii::t('m_pricingOptions', 'Ekspresowa'),
            'options' => array(
                self::OPTION_GROUP_COURIER_EXPRESS => array(
                    'name' => Yii::t('m_pricingOptions', 'Ekspresowa'),
                    'tooltip' => Yii::t('m_pricingOptions', 'ekspresowa_tooltip')),
            ));


        $optionsProducts = [];
        $optionsProducts[Pricing::PRODUCT_PP_LOCAL] = array(
            self::OPTION_GROUP_MAIL_SPEED => $options[self::OPTION_GROUP_MAIL_SPEED],
            self::OPTION_GROUP_MAIL_REGISTERED => $options[self::OPTION_GROUP_MAIL_REGISTERED],
            self::OPTION_GROUP_MAIL_SIZE => $options[self::OPTION_GROUP_MAIL_SIZE],
        );

        $optionsProducts[Pricing::PRODUCT_PP_ALTERNATIVE] = array(
            self::OPTION_GROUP_MAIL_SPEED => $options[self::OPTION_GROUP_MAIL_SPEED],
            self::OPTION_GROUP_MAIL_REGISTERED => $options[self::OPTION_GROUP_MAIL_REGISTERED],
            self::OPTION_GROUP_MAIL_SIZE => $options[self::OPTION_GROUP_MAIL_SIZE],
        );

        $optionsProducts[Pricing::PRODUCT_COURIER] = array(
            self::OPTION_GROUP_COURIER_INSURANCE => $options[self::OPTION_GROUP_COURIER_INSURANCE],
            self::OPTION_GROUP_COURIER_COD => $options[self::OPTION_GROUP_COURIER_COD],
            self::OPTION_GROUP_COURIER_EXPRESS => $options[self::OPTION_GROUP_COURIER_EXPRESS],
        );

        return $optionsProducts;
    }


    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on'=>'insert, update'),

            array('pricing_id, option_group, option_item, price, product', 'required'),
            array('pricing_id, option_group, option_item, product', 'numerical', 'integerOnly'=>true),
            array('price', 'numerical', 'min' => 0),
            array('id, pricing_id, option_group, option_item, price, date_entered, product', 'safe', 'on'=>'search'),
        );
    }

    public static function caulculatePrice($product, array $array, $pricing_id)
    {

        $price = 0;

        // search if is set size b and registered mail
        // if yes, take registered size b price
        // else take regular registered price

        $size_b = false;
        foreach($array AS $item)
        {
            $data = self::decodeValue($item);
            if($data['option_group'] == self::OPTION_GROUP_MAIL_SIZE AND $data['option_item'] == self::OPTION_3_SIZE_B)
            {
                $size_b = true;
            }
        }

        foreach($array AS $item)
        {
            $data = self::decodeValue($item);

            $option_item = $data['option_item'];
            if($data['option_group'] == self::OPTION_GROUP_MAIL_REGISTERED)
            {
                if($option_item == self::OPTION_2_REGISTERED)
                {
                    if($size_b)
                    {
                        $option_item = self::OPTION_2_REGISTERED_SIZE_B;
                    }
                }
            }

            $price += self::calculatePriceForOption($product,$data['option_group'],$option_item, $pricing_id);
        }

        return $price;
    }

    protected static function calculatePriceForOption($product, $option_group, $option_item, $pricing_id)
    {
        $cmd = Yii::app()->db->createCommand();

        $cmd->select('price');
        $cmd->from('pricing_options');
        $cmd->andWhere('product=:product AND option_group=:option_group AND option_item=:option_item AND pricing_id=:pricing_id', array(':product' => $product, ':option_group' => $option_group, ':option_item' => $option_item, ':pricing_id' => $pricing_id));
        $cmd->limit('1');

        $result = $cmd->queryScalar();

        return $result;
    }

    public static function encodeValue($product, $option_group, $option_item)
    {
        return $product.'_'.$option_group.'_'.$option_item;
    }

    public static function encodeName($product, $option_group)
    {
        return $product.'_'.$option_group;
    }

    public static function decodeValue($text)
    {
        $text = explode('_', $text);

        $array = [];
        $array['product'] = $text[0];
        $array['option_group'] = $text[1];
        $array['option_item'] = $text[2];

        return $array;
    }

    public static function decodeName($text)
    {
        $text = explode('_', $text);

        $array = [];
        $array['product'] = $text[0];
        $array['option_group'] = $text[1];

        return $array;
    }
}