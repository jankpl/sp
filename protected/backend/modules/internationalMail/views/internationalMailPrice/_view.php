<div class="view">

    <h3><?php echo $model->countryGroup->name;?></h3>

    <div style="width: 500px;">
        <?php
        $modelGV = new InternationalMailPriceValue();

        if($model->id !== NULL)
        {
            $this->widget('bootstrap.widgets.TbGridView', array(
                'id' => 'international-mail-grid-'.$model->id,
                'dataProvider' => $modelGV->gridViewData($model->id),

                'columns' => array(
                    array(
                        'name' => 'weight_top_limit',
                        'value' => '$data->weight_top_limit." ".InternationalMail::getWeightUnit()',
                    ),
                    array(
                        'name' => 'price_per_item',
                        'value' => 'S_Price::formatPrice($data->price_per_item)." zł"',
                    ),
                    array(
                        'name' => 'price_per_weight',
                        'value' => 'S_Price::formatPrice($data->price_per_weight)." zł/".InternationalMail::getWeightUnit()',
                    ),
                ),
            ));
        }
        ?>

    </div>


    <table class="list hLeft" style="width: 500px;">
        <tr>
            <td>Action</td>
            <td><?php
                if($model->id === NULL)
                    echo CHtml::link('create',array('/internationalMail/internationalMailPrice/create', 'id' => $model->country_group_id, 'id2' => $group->id), array('class' => 'likeButton'));
                else
                {
                    echo CHtml::link('edit',array('/internationalMail/internationalMailPrice/update', 'id' => $model->id), array('class' => 'likeButton'));
                    echo CHtml::link($model->stat?'activate':'deactivate',array('/internationalMail/internationalMailPrice/stat', 'id' => $model->id), array('class' => 'likeButton'));
                }
                ?></td>
        </tr>
        <tr>
            <td>Last update</td>
            <td><?php echo $model->date_updated; ?> by <?php echo $model->updateAdmin->login; ?> </td>
        </tr>
    </table>

</div>