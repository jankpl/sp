<?php

Yii::import('application.models._base.BaseUserHasUserGroup');

class UserHasUserGroup extends BaseUserHasUserGroup
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}