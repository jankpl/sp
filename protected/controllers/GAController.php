<?php


class GAController extends CController {

    public function actionGetByAddress()
    {
        if(Yii::app()->user->isGuest)
            throw new CHttpException(403);

        $address = $_REQUEST['address'];
        $result = GoogleApi::getDataByAddress($address);

        echo json_encode($result);
    }

    public function actionGetByCoordinates()
    {
        if(Yii::app()->user->isGuest)
            throw new CHttpException(403);

        $lat = $_REQUEST['lat'];
        $lng = $_REQUEST['lng'];

        $result = GoogleApi::getDataByCoordinates($lat, $lng);

        echo json_encode($result);
    }


}
