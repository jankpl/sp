<h3>Change statuses of selected items</h3>

<div style="text-align: center;">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'mass-assign-stat',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('order/assingStatusByGridView'),
    ));
    ?>

    <?php echo CHtml::dropDownList('order_stat_id','', CHtml::listData(OrderStat::model()->findAll(),'id','name')); ?>

    <?php
    echo Chtml::submitButton('Change', array(
        'onClick' => 'js:
    $("#order-ids").val($.fn.yiiGridView.getChecked("order-grid","selected_rows").toString());
    ;',
        'name' => 'OrderIds'));
    ?>

    <?php echo CHtml::hiddenField('Order[ids]','', array('id' => 'order-ids')); ?>
    <?php
    $this->endWidget();
    ?>
</div>