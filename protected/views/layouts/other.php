<?php Yii::app()->clientScript->registerCoreScript('jquery');?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.preventFormMultisubmit.js'); ?>

<?php $this->beginContent('//layouts/main2'); ?>
<div id="sub-page" >
    <div class="section-main" id="main-content-bg-container" >
        <div id="main-content-bg-one" class="main-content-bg"></div>
        <div id="main-content-bg-two" class="main-content-bg"></div>
        <div class="section-main-bg">
            <div class="container" style="min-height: 400px;">
                <div class="section-main-content"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="main">
                            <?php if(Yii::app()->user->hasFlash('top-info')):?>
                                <div class="alert alert-warning">
                                    <?= Yii::app()->user->getFlash('top-info'); ?>
                                </div>
                            <?php endif; ?>
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if(0 && in_array(Yii::app()->PV->get(), [
            PageVersion::PAGEVERSION_DEFAULT,
            PageVersion::PAGEVERSION_RS
        ])):
            ?>
            <div class="section-main-sub">
                <div class="container">
                    <h2><?= Yii::t('site', 'Na skróty:');?></h2>
                    <div class="row text-center" id="bottom-menu">

                        <div class="col-md-2 col-sm-4 col-md-offset-1">
                            <a href="<?php echo Yii::app()->createUrl('/specialTransport');?>">
                                <i class="fa fa-truck fa-inverse fa-5x" aria-hidden="true" style="margin-bottom: 10px; margin-top: 10px;"></i>
                                <span><?php echo Yii::t('site', 'przesyłki specjalne');?></span>
                            </a>
                        </div>

                        <div class="col-md-2 col-sm-4">
                            <a href="<?php echo Yii::app()->createUrl('/price/');?>">
                                <img src="<?php echo Yii::app()->baseUrl; ?>/images/layout/new/main-submenu-international-prices.png" alt="<?php echo Yii::t('site', 'cennik');?>"/>
                                <span><?php echo Yii::t('site', 'cennik');?></span>
                            </a>
                        </div>
                        <?php
                        if(0):
                            ?>
                            <div class="col-md-2 col-sm-4">
                                <a href="<?php echo Yii::app()->createUrl('/price/');?>">
                                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/layout/new/main-submenu-domestic-prices.png" alt="<?php echo Yii::t('site', 'cennik usług krajowych');?>"/>
                                    <span><?php echo Yii::t('site', 'cennik usług krajowych');?></span>
                                </a>
                            </div>
                            <div class="col-md-2 col-sm-4">
                                <a href="<?php echo Yii::app()->createUrl('price/table', array('urlData' => 'int'));?>">
                                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/layout/new/main-submenu-international-prices.png" alt="<?php echo Yii::t('site', 'cennik usług międzynarodowych');?>"/>
                                    <span><?php echo Yii::t('site', 'cennik usług międzynarodowych');?></span>
                                </a>
                            </div>
                        <?php
                        endif;
                        ?>
                        <div class="col-md-2 col-sm-4">
                            <a href="<?php echo Yii::app()->createUrl('page/view', array('id' => 3));?>">
                                <img src="<?php echo Yii::app()->baseUrl; ?>/images/layout/new/main-submenu-payments.png" alt="<?php echo Yii::t('site', 'płatności');?>"/>
                                <span><?php echo Yii::t('site', 'płatności');?></span>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <a href="<?php echo Yii::app()->createUrl('faq');?>">
                                <img src="<?php echo Yii::app()->baseUrl; ?>/images/layout/new/main-submenu-payments.png" alt="<?php echo Yii::t('site', 'FAQ');?>"/>
                                <span><?php echo Yii::t('site', 'FAQ');?></span>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <a href="<?php echo Yii::app()->createUrl('/site/contact');?>">
                                <i class="fa fa-phone-square fa-inverse fa-5x" aria-hidden="true" style="margin-bottom: 10px; margin-top: 10px;"></i>
                                <span><?php echo Yii::t('site','kontakt');?></span>
                            </a>
                        </div>
                    </div>


                </div>
            </div>
        <?php
        endif;
        ?>


    </div>
</div>

<?php $this->endContent(); ?>

<?php

if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH):
    $images = array(
        Yii::app()->baseUrl."/theme/ruch/images/bg_1.jpg",
        Yii::app()->baseUrl."/theme/ruch/images/bg_2.jpg",
        Yii::app()->baseUrl."/theme/ruch/images/bg_3.jpg",
    );
elseif(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI):
    $images = array(
        Yii::app()->baseUrl."/theme/pli/images/bg_1.jpg",
    );
else:
    $images = array(
        Yii::app()->baseUrl."/images/layout/new/bg.jpg",
        Yii::app()->baseUrl."/images/layout/new/bg2.jpg",
        Yii::app()->baseUrl."/images/layout/new/bg3.jpg",
        Yii::app()->baseUrl."/images/layout/new/bg5.jpg",
        Yii::app()->baseUrl."/images/layout/new/bg7.jpg",
        Yii::app()->baseUrl."/images/layout/new/bg8.jpg"
    );
endif;

if(in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS]))
    $images[] = Yii::app()->baseUrl."/images/layout/new/bg6.jpg";

array_rand($images);

$script = '
    var images = ["'.implode('","', $images).'"];

    var $bg1 = $("#main-content-bg-one");
    var $bg2 = $("#main-content-bg-two");

    animateBg(images, $bg1, $bg2, 2000);
';
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/animateBg.js');
Yii::app()->clientScript->registerScript('animateBg', $script, CClientScript::POS_READY);
?>
