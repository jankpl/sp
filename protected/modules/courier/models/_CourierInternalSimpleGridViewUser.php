<?php

class _CourierInternalSimpleGridViewUser extends Courier
{
    public $__receiver_country_id;
    public $__receiver_usefulName;
    public $__receiver_name;
    public $__receiver_company;
    public $__receiver_address;
    public $__receiver_city;
    public $__receiver_zip_code;
    public $__receiver_tel;
    public $__sender_country_id;
    public $__sender_usefulName;
    public $__sender_name;
    public $__sender_company;
    public $__sender_address;
    public $__sender_city;
    public $__sender_zip_code;
    public $__sender_tel;
    public $__stat;
    public $__premium;

    public function rules() {

        $array = array(
            array('
                 __sender_country_id,
                 __sender_usefulName,
                 __sender_name,
                 __sender_company,
                 __sender_address,
                 __sender_city,
                 __sender_zip_code,
                 __sender_tel,
                 __receiver_country_id,
                 __receiver_usefulName,
                 __receiver_name,
                 __receiver_company,
                 __receiver_address,
                 __receiver_city,
                 __receiver_zip_code,
                 __receiver_tel,
                 __stat,
                 __premium
                 ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('local_id', $this->local_id, true);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('sender_address_data_id', $this->sender_address_data_id);
        $criteria->compare('receiver_address_data_id', $this->receiver_address_data_id);
        $criteria->compare('courier_stat_id', $this->courier_stat_id);
        $criteria->compare('package_weight', $this->package_weight);
        $criteria->compare('package_size_l', $this->package_size_l);
        $criteria->compare('package_size_w', $this->package_size_w);
        $criteria->compare('package_size_d', $this->package_size_d);
        $criteria->compare('packages_number', $this->packages_number);
        $criteria->compare('package_value', $this->package_value);
        $criteria->compare('package_content', $this->package_content, true);

        $criteria->compare('user_id', Yii::app()->user->id);

        // $criteria->compare('__type', $this->__type);
        $criteria->addCondition('courier_type_external_id IS NULL');
        $criteria->addCondition('courier_type_internal_id IS NULL');

        $criteria->addCondition('t.id IN (SELECT courier_id FROM courier_type_internal_simple)');


        if($this->__stat)
        {
            $criteria->compare('courier_stat_id',$this->__stat);
        }


        if($this->__sender_country_id)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->compare('senderAddressData.country_id',$this->__sender_country_id);
        }

        if($this->__receiver_country_id)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->compare('receiverAddressData.country_id',$this->__receiver_country_id);
        }

        if($this->__receiver_usefulName)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.name LIKE '%".$this->__receiver_usefulName."%' OR receiverAddressData.company LIKE '%".$this->__receiver_usefulName."%')");
        }

        if($this->__sender_usefulName)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.name LIKE '%".$this->__sender_usefulName."%' OR senderAddressData.company LIKE '%".$this->__sender_usefulName."%')");
        }

        if($this->__receiver_name)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.name LIKE '%".$this->__receiver_name."%')");
        }

        if($this->__sender_name)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.name LIKE '%".$this->__sender_name."%')");
        }

        if($this->__receiver_company)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.company LIKE '%".$this->__receiver_company."%')");
        }

        if($this->__sender_company)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.company LIKE '%".$this->__sender_company."%')");
        }

        if($this->__receiver_address)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.address_line_1 LIKE '%".$this->__receiver_address."%' OR receiverAddressData.address_line_2 LIKE '%".$this->__receiver_address."%')");
        }

        if($this->__sender_address)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.address_line_1 LIKE '%".$this->__receiver_address."%' OR senderAddressData.address_line_2 LIKE '%".$this->__receiver_address."%')");
        }

        if($this->__receiver_city)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.city LIKE '%".$this->__receiver_city."%')");
        }

        if($this->__sender_city)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.city LIKE '%".$this->__sender_city."%')");
        }

        if($this->__receiver_zip_code)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.zip_code LIKE '%".$this->__receiver_zip_code."%')");
        }

        if($this->__sender_zip_code)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.zip_code LIKE '%".$this->__sender_zip_code."%')");
        }

        if($this->__receiver_tel)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.tel LIKE '%".$this->__receiver_tel."%')");
        }

        if($this->__sender_tel)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.tel LIKE '%".$this->__sender_tel."%')");
        }

        $criteria->with = array('courierTypeOoe');

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes = array(
            '__sender_country_id' => array(
                'asc' => 'senderAddressData.country ASC',
                'desc' => 'senderAddressData.country DESC',
            ),
            '__receiver_country_id' => array(
                'asc' => 'receiverAddressData.country ASC',
                'desc' => 'receiverAddressData.country DESC',
            ),
            '__stat' => array(
                'asc' => 'courier_stat_id ASC',
                'desc' => 'courier_stat_id DESC',
            ),
            '*', // add all of the other columns as sortable
        );


        return new CActiveDataProvider($this->with('user')->with('senderAddressData')->with('receiverAddressData')->with('courierTypeOoe'), array(
            'criteria' => $this->searchCriteria(),
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

    }


}