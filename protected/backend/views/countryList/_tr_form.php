<div class="form">

    <?php echo $form->hiddenField($model, '['.$i.']language_id'); ?>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']name'); ?>
        <?php echo $form->textField($model, '['.$i.']name', array('maxlength' => 256)); ?>
        <?php echo $form->error($model,'['.$i.']name'); ?>
    </div><!-- row -->

</div><!-- form -->