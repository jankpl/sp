<?php
$script = "
    (function($) {

        var lat = 52.887719;
        var lon = 21.390474;
        var line;

        map = L.map('tt-map').setView([lat, lon], 5);
        
        map.scrollWheelZoom.disable();
        map.on('focus', () => { map.scrollWheelZoom.enable(); });
        map.on('blur', () => { map.scrollWheelZoom.disable(); });
        
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors',
            maxZoom: 18,
        }).addTo(map);

        var G_from = yii.map.from;
        var G_to = yii.map.to;
        var G_location = yii.map.location;
        var delivered = yii.map.isDelivered;

        if(G_from && G_to && G_location)
        {
            if(yii.map)
                G_location = G_to;

            if(G_location instanceof Object)
            {
                marker = L.marker([G_location[0], G_location[1]]).addTo(map);
                map.flyTo([G_location[0], G_location[1]],7);

            }

            if(G_from instanceof Object)
            {
                if(G_location instanceof Object)
                {
                    line = new L.Polyline([new L.LatLng(G_from[0], G_from[1]), new L.LatLng(G_location[0], G_location[1])], {
                        color: '#1dff18',
                        weight: 5,
                        opacity: 0.5,
                        smoothFactor: 1
                    });
                    line.addTo(map);
                  
                    if(!delivered && G_to instanceof Object)
                    {
                        line = new L.Polyline([new L.LatLng(G_location[0], G_location[1]), new L.LatLng(G_to[0], G_to[1])], {
                            color: 'red',
                            weight: 5,
                            opacity: 0.5,
                            smoothFactor: 1
                        });
                        line.addTo(map);
                    }
                }
                else if(G_to instanceof Object)
                {
                    line = new L.Polyline([new L.LatLng(G_from[0].geometry.location.lat(), G_from[0].geometry.location.lng()), new L.LatLng(G_to[0].geometry.location.lat(), G_to[0].geometry.location.lng())], {
                        color: 'red',
                        weight: 5,
                        opacity: 0.5,
                        smoothFactor: 1
                    });
                    line.addTo(map);
                }

            }
        }
    })(jQuery);"
?>

<?php
$geoLocation = GoogleApi::getCoordinatesByAddress($location);
$geoFrom = GoogleApi::getCoordinatesByAddress($send_location);
$geoTo = GoogleApi::getCoordinatesByAddress($receiver_location);
?>

<?php
if($geoLocation && $geoFrom && $geoTo):
    ?>
    <div id="map-wrapper" style="display: ; margin-top: 25px;">
        <div id="tt-map" style="width: 100%; height: 400px; border: 10px solid #dddddd; background: #dddddd; margin: 10px auto;"></div>
        <div class="alert alert-warning text-center"><?php echo Yii::t('courier', 'Mapa ma jedynie charakter ilustracyjny i może nie odzwierciedlać rzeczywistej lokalizacji paczki !');?></div>
    </div>
    <?php
    Yii::app()->clientScript->registerScript('tt-map', $script, CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/leaflet/leaflet.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/leaflet/leaflet.css');
    Yii::app()->clientScript->registerScript('helpers', '
          yii = {
                map: {
                  location: '.CJSON::encode($geoLocation).',
                  from: '.CJSON::encode($geoFrom).',
                  to: '.CJSON::encode($geoTo).',
                  isDelivered: '.CJSON::encode($stat_map == StatMap::MAP_DELIVERED).',                  
             },          
          };
      ',CClientScript::POS_HEAD);
    ?>
<?php
endif;


