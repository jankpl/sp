<h2>Zakresy numerów</h2>
<h3>Przesyłka firmowa Polecona</h3>
<table class="table table-striped table-bordered">
    <tr>
        <th>Konto</th>
        <th>Od</th>
        <th>Bieżący</th>
        <th>Do</th>
        <th>Wykorzystano</th>
        <th>Pozostało</th>
    </tr>
    <?php
    foreach(PocztaPolskaClient::getAccountList(false, false) AS $item):

        $cmd = Yii::app()->db->createCommand();
        $res = $cmd->select('*')->from('poczta_polska_numery_nadania')->where('id_acc = :acc', [':acc' => $item])->queryRow();
        ?>
        <tr>
            <td>#<?= $item;?> : <?= PocztaPolskaClient::getLoginByAccount($item);?></td>
            <td><?= $res['prefix'];?><strong><?= $res['start'];?></strong></td>
            <td><?= $res['prefix'];?><strong><?= $res['current'];?></strong></td>
            <td><?= $res['prefix'];?><strong><?= $res['end'];?></strong></td>
            <td><?= ($res['current'] - $res['start']);?><strong></td>
            <td><?= ($res['end'] - $res['current'] + 1);?><strong></td>
        </tr>

    <?php
    endforeach;
    ?>
    <tr>

    </tr>
</table>
<h2>Hasła</h2>
<table class="table table-striped table-bordered">
    <tr>
        <th>Konto</th>
        <th>Hasło</th>
        <th>Data ustawienia</th>
    </tr>
    <?php
    foreach(PocztaPolskaClient::getAccountList(false, true) AS $item):

        $cmd = Yii::app()->db->createCommand();
        $res = $cmd->select('*')->from('poczta_polska_ustawienia')->where('what = :what', [':what' => 'pass_'.$item])->queryRow();
        ?>
        <tr>
            <td><?= PocztaPolskaClient::getLoginByAccount($item);?></td>
            <td><input type="text" value="<?= $res['value'];?>" readonly/></td>
            <td><?= $res['date'];?></td>
        </tr>

        <?php
    endforeach;
    ?>
    <tr>

    </tr>
</table>