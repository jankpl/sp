<div class="form">
    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']title'); ?>
        <?php echo $form->textField($model, '['.$i.']title', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'['.$i.']title'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']text'); ?>
        <?php
        $this->widget('ext.editMe.widgets.ExtEditMe', array(
            'model'=>$model,
            'attribute'=>'['.$i.']text',
            'resizeMode' => 'vertical',
            'width' => '580',
            'baseHref' => Yii::app()->baseUrl.'/',
            'toolbar' => array(
                array(
                    'NewPage', 'Preview', 'Source',
                ),
                array(
                    'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',
                ),
                array(
                    'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat',
                ),
                array(
                    'Image','Link','NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Table', 'HorizontalRule','SpecialChar', 'Font',  'FontSize', 'TextColor',
                ),
            ),
            'advancedTabs' => false,
            'ckeConfig' => array(
                'forcePasteAsPlainText' => true,
                'allowedContent' => true,
            )
        ));
        ?>
        <?php echo $form->error($model,'['.$i.']text'); ?>
    </div><!-- row -->
</div><!-- form -->