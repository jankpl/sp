<h3><?php echo Yii::t('specialTransport','Szczegóły przesyłki');?></h3>
<table class="list hLeft" style="width: 500px;">
    <tr>
        <td><?php echo $model->getAttributeLabel('date_entered');?></td>
        <td><?php echo substr($model->date_entered,0,16);?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('typ');?></td>
        <td><?php echo SpecialTransport::types()[$model->type];?></td>
    </tr>
    <tr>
        <td><?php echo ($model->getAttributeLabel('customer_notes'));?></td>
        <td><?php echo nl2br($model->customer_notes);?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('notes');?></td>
        <td><?php echo nl2br($model->notes);?></td>
    </tr>
</table>

<h3><?php echo Yii::t('specialTransport','Nadawca');?></h3>

<?php

$this->renderPartial('//_addressData/_listInTable',
    array('model' => $model->senderAddressData,
    ));

?>

<h3><?php echo Yii::t('specialTransport','Odbiorca');?></h3>
<?php

$this->renderPartial('//_addressData/_listInTable',
    array('model' => $model->receiverAddressData,
    ));

?>