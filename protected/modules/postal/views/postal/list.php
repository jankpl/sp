<?php
/* @var $this PostalController */
?>

<?php
if(!Yii::app()->NPS->isNewPanelSet())
    $this->renderPartial('index');
?>

    <h2 id="list"><?php echo Yii::t('site','Your packages');?></h2>

    <div class="panel panel-info actions-for-selected">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('postal', 'Akcje dla zaznaczonych paczek');?> (<span class="selected-no">0</span>)</h3>
        </div>
        <div class="panel-body" style="position: relative;">
            <div class="too-few-curain" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; background: rgba(255,255,255,0.9); z-index: 50; padding: 10px 25px;">
                <div class="alert alert-danger too-much-alert hidden"><?= Yii::t('postal', 'Wybrano zbyt dużo pozycji na raz!');?></div>
            </div>
            <div class="selected-actions">
                <?php echo TbHtml::tabbableTabs(array(
                    array('id' => 'tab_bis_1', 'label' => Yii::t('postal','Generate labels'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_labels', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_2','label' => Yii::t('postal','Generate ACK cards'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_ack_cards', array('id_prefix' => 'bis'), true)),
//                array('label' => Yii::t('postal','Generate status cards'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_stat_conf', array(), true)),
                    array('id' => 'tab_bis_3','label' => Yii::t('postal','Cancel packages'), 'content' => $this->renderPartial('gridview_actions_tabs/_user_cancel', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_4','label' => Yii::t('postal','Export to .XLS'), 'content' => $this->renderPartial('gridview_actions_tabs/_export', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_5','label' => Yii::t('postal','Generate KsiążkaNadawcza'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_kn', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_6','label' => Yii::t('postal','Generate CN22'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_cn22', array('id_prefix' => 'bis'), true)),
                ), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>
            </div>
        </div>
    </div>

    <hr/>

    <div class="gridview-container">
        <div class="gridview-overlay"></div>
        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;"><?= Yii::t('site', 'Zaznaczonych pozycji:');?> <span class="selected-no" style="font-weight: bold;">0</span></div>
        <?php
        $this->_getGridViewPostalgrid();

        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css');
        if(Yii::app()->getLanguage() == 'pl')
            Yii::app()->clientScript->registerScriptFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/i18n/ui.datepicker-pl.js');

        $script = "$( function() {
       
         $('[name=\"PostalGridViewUser[date_entered]\"], [name=\"PostalGridViewUser[date_updated]\"]').datepicker({
                dateFormat : 'yy-mm-dd',
                minDate: \"-5Y\", 
                maxDate: \"+0M\",
                changeMonth: true,
                changeYear: true,
            });
          

        } );";
        Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);
        ?>
        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;"><?= Yii::t('site', 'Zaznaczonych pozycji:');?> <span class="selected-no" style="font-weight: bold;">0</span></div>
    </div>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    if($key == 'top-info')
    {
        Yii::app()->user->setFlash('top-info', $message);
        continue;
    }
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

    <hr/>

    <div class="panel panel-info actions-for-selected">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('postal', 'Akcje dla zaznaczonych paczek');?> (<span class="selected-no">0</span>)</h3>
        </div>
        <div class="panel-body" style="position: relative;">
            <div class="too-few-curain" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; background: rgba(255,255,255,0.9); z-index: 50; padding: 10px 25px;">
                <div class="alert alert-danger too-much-alert hidden"><?= Yii::t('postal', 'Wybrano zbyt dużo pozycji na raz!');?></div>
            </div>
            <div class="selected-actions">
                <?php echo TbHtml::tabbableTabs(array(
                    array('label' => Yii::t('postal','Generate labels'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_labels', array(), true)),
                    array('label' => Yii::t('postal','Generate ACK cards'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_ack_cards', array(), true)),
//                array('label' => Yii::t('postal','Generate status cards'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_stat_conf', array(), true)),
                    array('label' => Yii::t('postal','Cancel packages'), 'content' => $this->renderPartial('gridview_actions_tabs/_user_cancel', array(), true)),
                    array('label' => Yii::t('postal','Export to .XLS'), 'content' => $this->renderPartial('gridview_actions_tabs/_export', array(), true)),
                    array('label' => Yii::t('postal','Generate KsiążkaNadawcza'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_kn', array(), true)),
                    array('label' => Yii::t('postal','Generate CN22'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_cn22', true)),
                ), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>
            </div>
        </div>
    </div>



    <script>
        // $('.grid-view tbody tr').unbind('click');
    </script>

<?php $this->widget('BackLink'); ?>

<?php
$script = 'function checkNumberOfChecks(id)
    {
        var number = $("#postalgrid").selGridViewNoGet("getAllSelection").length;
       $(".selected-no").html(number);
        if(number == 0)
        {
             $(".too-few-curain").show();
             $(".too-much-alert").addClass("hidden");
        }
        else if(number > 500)
        {
            //bootbox.alert("'.Yii::t('courier', 'Wybrano zbyt dużo pozycji na raz!').'");
            $(".too-few-curain").show();
            $(".too-much-alert").removeClass("hidden");
        } else {
            $(".too-few-curain").hide();
            $(".too-much-alert").addClass("hidden");
        }
    }';
Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-bootbox.min.js');
Yii::app()->clientScript->registerScript('cnoc', $script);

$script = "
$('.gridview-container').doubleScroll({contentElement: '.items', resetOnWindowResize: true});
";
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.doubleScroll.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('ds',$script, CClientScript::POS_READY);
?>