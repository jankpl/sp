<?php

class CourierAcknowlegmentCard
{


    protected static function printTableHeader(&$pdf)
    {
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(50,3,Yii::t('courier_pdf','Paczka'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('courier_pdf','Nadawca'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('courier_pdf','Odbiorca'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(40,3,Yii::t('courier_pdf','Inne'),1,'L',false,0,'','',true,0,false,true,0,'T',true);

        $pdf->ln(4);

        $pdf->SetFont('freesans', '', 8);
    }

    public static function generateCardMultiPerOperator(array $couriers, $bulkNo = false, $ignoreExtCards = false)
    {
        $done = [];
        $printExtAckCardsOnEnd = [];

        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();
        $pdf->setMargins(10,10,10, true);
        $pdf->SetAutoPageBreak(false);


        $couriersGrouped = [];
        /* @var $couriers Courier[] */
        foreach($couriers AS $key => $item) {

            $operator = '';
            if ($item->courierTypeInternal) {
                if ($item->courierTypeInternal->common_operator_ordered)
                    $operator = $item->courierTypeInternal->delivery_operator_name;
                else if ($item->courierTypeInternal->pickup_operator_ordered)
                    $operator = $item->courierTypeInternal->pickup_operator_name;

            } else if ($item->courierTypeDomestic) {
                $operator = $item->courierTypeDomestic->courierDomesticOperator->name;
            }
            else if($item->courierTypeU)
            {
                $operator = $item->courierTypeU->courierUOperator->name_customer;
            }


            if(!isset($couriersGrouped[$operator]))
                $couriersGrouped[$operator] = [];

            $couriersGrouped[$operator][$key] = $item;
        }

        foreach($couriersGrouped AS $group => $items) {
            $pdf->AddPage('P', 'A4');
            $pdf = self::generateCardMulti($items, $bulkNo, $pdf, $done, $group, $printExtAckCardsOnEnd, false, $ignoreExtCards, false);
        }


        if(S_Useful::sizeof($printExtAckCardsOnEnd))
        {
            // clear empty pages before inserting new
            foreach($printExtAckCardsOnEnd AS $item2)
            {
                $done[$item2['key']] = $item2['item']->id;
                $pdf->AddPage('L','A4');
                $pdf->Image('@'.$item2['ack'],5,5,round($pdf->getPageWidth())-10, round($pdf->getPageHeight())-10,'','','', true, 300, '', false, false, 0, 'TL', false, true);
            }
        }

        $pdf->Output('manifest.pdf', 'D');


        Courier::onAfterAckGenerationGroup($done);

        return $done;


    }

    public static function generateCardMulti(array $couriers, $bulkNo = false, $perOperatorOwnPdf = false, &$done = [], $operatorHeader = false, &$printExtAckCardsOnEnd = [], $forReturns = false, $ignoreExtCards = false, $archiveAckCard = false, $setManifestStatus = false)
    {

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $totalNumber = S_Useful::sizeof($couriers);

        if($totalNumber > 2500)
            throw new Exception(Yii::t('courier','Za dużo paczek do wygenerowania potwierdzenia nadania.'));

        $pageSize = 14;
        $firstPageSizeOffset = 1;
        $lastPageSizeOffset = 0;

        $itemsOnCurrentPage = 0;

        if(!$perOperatorOwnPdf) {
            /* @var $pdf TCPDF */
            $pdf = PDFGenerator::initialize();

            $pdf->setMargins(10, 10, 10, true);

            $pdf->AddPage('P', 'A4');

        } else {
            $pdf = $perOperatorOwnPdf;
        }
//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);

        $firstModel = reset($couriers);

        $suffix = '';
        if($operatorHeader)
            $suffix .= ' : '.$operatorHeader;

        $prefix = '';
        if($bulkNo)
            $prefix = $bulkNo.' : ';


        $title = Yii::t('courier_pdf','Potwierdzenie nadania');


        if($forReturns)
            $title = Yii::t('courier_pdf','Lista zwróconych przesyłek - potwierdzenie zwrotu');

        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest && in_array(Yii::app()->user->id, [3008])) // @ 29.04.2019 - special header for GRUPA_TOPEX user
        {
            /* @var $userModel User */
            $userModel = Yii::app()->user->getModel();

            $pdf->MultiCell(70,8,$prefix.$title.$suffix,1,'C',false,0,10, 5,true,0,false,true,0,'M',true);

            $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            $pdf->SetFont('freesans', '', 6);
            $pdf->MultiCell(70,8,'Adres nadania:',1,'C',false,0,80, 5,true,0,false,true,0,'T',true);
            $pdf->MultiCell(70,4,trim($userModel->address_line_1.' '.$userModel->address_line_2).', '.$userModel->zip_code.' '.$userModel->city.', '.$userModel->country0->code,0,'C',false,0,80, 9,true,0,false,true,0,'T',true);
        }
        else
        {
            $pdf->MultiCell(140,8,$prefix.$title.$suffix,1,'C',false,0,10, 5,true,0,false,true,0,'M',true);
        }

        $pdf->SetY($pdf->GetY() + 8);
        $pdf->SetTextColor(0,0,0);

        $pw = $pdf->getPageWidth();
        $ph = $pdf->getPageHeight();

        $stat_packages = 0;
        $stat_weight = 0;
        // for stats
        foreach($couriers AS $item) {

            if(in_array($item->courier_stat_id, [CourierStat::CANCELLED_PROBLEM, CourierStat::CANCELLED, CourierStat::CANCELLED_USER]))
                continue;

            if(in_array($item->user_cancel, [Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED]))
                continue;

//            if($forReturns && $item->getType() != Courier::TYPE_EXTERNAL_RETURN)
//                continue;

            if(!$forReturns && $item->getType() == Courier::TYPE_EXTERNAL_RETURN)
                continue;

            $stat_packages++;
            $stat_weight += $item->package_weight;
        }

        $pdf->SetFont('freesans', '', 8);
        $y = $pdf->GetY();
        $pdf->MultiCell(50,8,Yii::t('courier_pdf','Łącznie paczek:').' '.$stat_packages."\r\n".Yii::t('courier_pdf','Łączna waga:').' '.$stat_weight.' [kg]',1,'R',false,0,$pw-60, 5,true,0,false,true,0,'M',true);
        $pdf->SetAbsY($y - 8);

        $pdf->ln();

//        self::printTableHeader($pdf);

        if($totalNumber <= ($pageSize - $firstPageSizeOffset - $lastPageSizeOffset))
            $totalPages = 1;
        else {
            $totalPages = ceil((($totalNumber + $firstPageSizeOffset + $lastPageSizeOffset)) / $pageSize);
        }

        $pdf->SetFont('freesans', '', 8);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->MultiCell(20,3,'1/'.$totalPages,0,'R', false, 0, $pw-30, $ph - 10);
        $pdf->SetAbsX($x);
        $pdf->SetAbsY($y);

        $i = 0;
        $pageI = 0;
        $page = 1;

        /* @var $item Courier */
        foreach($couriers AS $key => $item) {

            if(in_array($item->courier_stat_id, [CourierStat::CANCELLED_PROBLEM, CourierStat::CANCELLED, CourierStat::CANCELLED_USER]))
                continue;

            if(in_array($item->user_cancel, [Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED]))
                continue;

//            if($forReturns && $item->getType() != Courier::TYPE_EXTERNAL_RETURN)
//                continue;

            if(!$forReturns && $item->getType() == Courier::TYPE_EXTERNAL_RETURN)
                continue;

            if($setManifestStatus && $item->stat_map_id == StatMap::MAP_NEW && $item->courier_stat_id != CourierStat::STAT_MANIFEST_GENERATED_CUSTOMER)
                $item->changeStat(CourierStat::STAT_MANIFEST_GENERATED_CUSTOMER);

            $currentY = $pdf->GetY();

            $cellContent = '';
            $track_id = false;

            $extAckCard = false;
            if ($item->courierTypeInternal) {

                if ($item->courierTypeInternal->common_operator_ordered) {

                    $cellContent .= Yii::t('courier_pdf','Operator odbierający:').' '.$item->courierTypeInternal->delivery_operator_name;
                    if ($item->courierTypeInternal->commonLabel->track_id != '') {
                        $track_id = $item->courierTypeInternal->commonLabel->track_id;
                        $cellContent .= "\r\n" . Yii::t('courier_pdf', 'Nr nadania:') . ' ' . $item->courierTypeInternal->commonLabel->track_id;

                        if(!$ignoreExtCards)
                            $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeInternal->commonLabel->courierLabelNew);
                    }

                } else if ($item->courierTypeInternal->pickup_operator_ordered) {

                    $cellContent .= Yii::t('courier_pdf','Operator odbierający:').' '.$item->courierTypeInternal->pickup_operator_name;
                    if ($item->courierTypeInternal->pickupLabel->track_id != '') {
                        $cellContent .= "\r\n" . Yii::t('courier_pdf', 'Nr nadania:') . ' ' . $item->courierTypeInternal->pickupLabel->track_id;
                        $track_id = $item->courierTypeInternal->pickupLabel->track_id;

                        if(!$ignoreExtCards)
                            $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeInternal->pickupLabel->courierLabelNew);
                    }
                }
                else if($item->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_CONTRACT && $item->courierTypeInternal->delivery_operator_ordered)
                {
                    $cellContent .= Yii::t('courier_pdf','Operator odbierający:').' '.$item->courierTypeInternal->delivery_operator_name;
                    if ($item->courierTypeInternal->pickupLabel->track_id != '') {
                        $cellContent .= "\r\n" . Yii::t('courier_pdf', 'Nr nadania:') . ' ' . $item->courierTypeInternal->commonLabel->track_id;
                        $track_id = $item->courierTypeInternal->pickupLabel->track_id;

                        if(!$ignoreExtCards)
                            $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeInternal->commonLabel->courierLabelNew);
                    }
                }
            } else if ($item->courierTypeDomestic) {

                $cellContent .= Yii::t('courier_pdf','Operator odbierający:').' '.$item->courierTypeDomestic->courierDomesticOperator->name;
                if ($item->courierTypeDomestic->courierLabelNew->track_id != '') {
                    $cellContent .= "\r\n" . Yii::t('courier_pdf', 'Nr nadania:') . ' ' . $item->courierTypeDomestic->courierLabelNew->track_id;
                    $track_id = $item->courierTypeDomestic->courierLabelNew->track_id;

                    if(!$ignoreExtCards)
                        $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeDomestic->courierLabelNew);
                }
            } else if ($item->courierTypeU) {

                $cellContent .= Yii::t('courier_pdf','Operator odbierający:').' '.$item->courierTypeU->courierUOperator->name_customer;
                if ($item->courierTypeU->courierLabelNew->track_id != '') {
                    $cellContent .= "\r\n" . Yii::t('courier_pdf', 'Nr nadania:') . ' ' . $item->courierTypeU->courierLabelNew->track_id;
                    $track_id = $item->courierTypeU->courierLabelNew->track_id;

                    if(!$ignoreExtCards)
                        $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeU->courierLabelNew);
                }
            }

            if($extAckCard && !$ignoreExtCards)
            {
                $printExtAckCardsOnEnd[] = [
                    'item' => $item,
                    'ack' => $extAckCard,
                    'key' => $key,
                ];
                continue;
            }

            $itemsOnCurrentPage++;

            if($track_id)
                $pdf->write1DBarcode($track_id, 'C128', 70, $currentY - 1, 60, 10, 1, $barcodeStyle);


            $x = $pdf->GetX();
            $y = $pdf->GetY();
            $pdf->MultiCell(190,8,'',1,'L',false,0,$x,$y,true,0,false,true,0,'T',true);
            $pdf->MultiCell(70,8,$cellContent,0,'L',false,0,$x + 120,$y,true,0,false,true,0,'M',true);

            $pdf->MultiCell(10,8,($i+1),1,'C',false,0,$x,$y,true,0,false,true,0,'M',true);
            $cellContent = Yii::t('courier_pdf','Nr paczki:').' '.$item->local_id."\r\n";
            $cellContent .= Yii::t('courier_pdf','Data nadania:').' '.substr($item->date_entered,0,10);
            $pdf->MultiCell(50,8,$cellContent,1,'L',false,0,$x+10,$y,true,0,false,true,0,'M',true);
            $pdf->ln(8);


            $x = $pdf->GetX();
            $y = $pdf->GetY();
            $pdf->StartTransform();
            $pdf->Rotate(90);
            $pdf->SetFont('freesans', '', 4);
            $pdf->MultiCell(10, 2, Yii::t('courier_pdf','Nadawca'), 1, 'C', false,'',0,$y+58,true,0,false,false,0,'M',true);
            $pdf->MultiCell(10, 2, Yii::t('courier_pdf','Odbiorca'), 1, 'C', false,'',0,$y+123,true,0,false,false,0,'M',true);
            $pdf->SetFont('freesans', '', 8);
            $pdf->StopTransform();

            $pdf->SetX($x);
            $pdf->SetY($y);

            $cellContent = Yii::t('courier_pdf','Waga').' ['.Courier::getWeightUnit().']: '.$item->package_weight;
            $cellContent .= " | ".Yii::t('courier_pdf','Wymiary').' ['.Courier::getDimensionUnit().']: '.$item->package_size_d . ' x ' . $item->package_size_l . ' x ' . $item->package_size_w;
            $cellContent .= "\r\n".Yii::t('courier_pdf','Wartość').': '.$item->package_value.' '.$item->getPackageValueCurrency();

            if($item->ref != '')
                $cellContent .= " | ".Yii::t('courier_pdf','Ref:')." ".$item->ref;

            $cellContent .= "\r\n".Yii::t('courier_pdf','Zawartość:').' '.$item->package_content;

            $cellContent .= "\r\n".Yii::t('courier_pdf','Kwota pobrania:').' '.($item->cod_value ? $item->cod_value.' '.$item->cod_currency : '-');

            // @09.23.2017 - do not show prices for users:
            // DOBRAPACZKA, DOBRAPACZKA-DPD
            // @ 18.10.2017 - KEDZIERZYNSP
            // @ 11.05.2018 - ANWISDHL
            if(!in_array($item->user_id, [698,743, 332, 2671]))
                if ($item->courierTypeInternal) {
                    $cellContent .= "\r\n".Yii::t('courier_pdf','Cena usługi:').' '.$item->courierTypeInternal->order->value_brutto.' '.$item->courierTypeInternal->order->currency;
                }

            $pdf->MultiCell(60,10,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $cellContent = $item->senderAddressData->company != '' ? $item->senderAddressData->company."\r\n" : '';
            $cellContent .= $item->senderAddressData->name != '' ? $item->senderAddressData->name."\r\n" : '';
            $cellContent .= $item->senderAddressData->address_line_1."\r\n";
            $cellContent .= $item->senderAddressData->address_line_2 != '' ? $item->senderAddressData->address_line_2."\r\n" : '';
            $cellContent .= $item->senderAddressData->zip_code.', ';
            $cellContent .= $item->senderAddressData->city."\r\n";
            $cellContent .= $item->senderAddressData->country;

            $pdf->MultiCell(65,10,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $cellContent = $item->receiverAddressData->company != '' ? $item->receiverAddressData->company."\r\n" : '';
            $cellContent .= $item->receiverAddressData->name != '' ? $item->receiverAddressData->name."\r\n" : '';
            $cellContent .= $item->receiverAddressData->address_line_1."\r\n";
            $cellContent .= $item->receiverAddressData->address_line_2 != '' ? $item->receiverAddressData->address_line_2."\r\n" : '';
            $cellContent .= $item->receiverAddressData->zip_code.', ';
            $cellContent .= $item->receiverAddressData->city."\r\n";
            $cellContent .= $item->receiverAddressData->country;

            $pdf->MultiCell(65,10,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $pdf->ln(10.7);

            // break first page earlier to make space for header
            if($page == 1 && $pageI > 0  && ($pageI%($pageSize-$firstPageSizeOffset) == 0))
            {

                $pdf->SetFont('freesans', '', 8);
//                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);


                // clear empty pages before inserting new
                if(!$itemsOnCurrentPage)
                {
                    $noOfPages = $pdf->getNumPages();
                    if($noOfPages)
                        $pdf->deletePage($noOfPages);
                }

                $pdf->AddPage('P','A4');


                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }
            // break last page earlier to make space for footer
            else if($page > 1 && $page == $totalPages && ($pageI%($pageSize-$lastPageSizeOffset) == 0))
            {

                $pdf->SetFont('freesans', '', 8);
//                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

                // clear empty pages before inserting new
                if(!$itemsOnCurrentPage)
                {
                    $noOfPages = $pdf->getNumPages();
                    if($noOfPages)
                        $pdf->deletePage($noOfPages);
                }

                $pdf->AddPage('P','A4');

                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }
            else if(($pageI%$pageSize) == 0 && $pageI > 0)
            {

                $pdf->SetFont('freesans', '', 8);
//                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

                // clear empty pages before inserting new
                if(!$itemsOnCurrentPage)
                {
                    $noOfPages = $pdf->getNumPages();
                    if($noOfPages)
                        $pdf->deletePage($noOfPages);
                }

                $pdf->AddPage('P','A4');


                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }

            $i++;
            $pageI++;

            $done[$key] = $item->id;
        }

        $pdf->SetAbsY($pdf->GetY() + 20);

        $pdf->SetFont('freesans', '', 8);

//        $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//        $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

        /* @var $pdf TCPDF */

        if($perOperatorOwnPdf)
        {
            return $pdf;
        } else {

            if(S_Useful::sizeof($printExtAckCardsOnEnd))
            {
                // clear empty pages before inserting new
                if(!$itemsOnCurrentPage)
                {
                    $noOfPages = $pdf->getNumPages();
                    if($noOfPages)
                        $pdf->deletePage($noOfPages);
                }

                foreach($printExtAckCardsOnEnd AS $item2)
                {
                    $done[$item2['key']] = $item2['item']->id;
                    $pdf->AddPage('L','A4');
                    $pdf->Image('@'.$item2['ack'],5,5,round($pdf->getPageWidth())-10, round($pdf->getPageHeight())-10,'','','', true, 300, '', false, false, 0, 'TL', false, true);
                }
            }

            $pdfString = $pdf->Output('manifest.pdf', 'S');


            Courier::onAfterAckGenerationGroup($done);

            if($archiveAckCard)
                AckCardArchive::archiveForCourier($pdfString, $couriers);

            header('Content-Type: application/pdf');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=manifest.pdf");
            echo $pdfString;

            return $done;
        }
    }

    public static function generateCard(Courier $item, $ignoreExtCards = false)
    {
        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();
        $pdf->setMargins(10,10,10, true);


        if(in_array($item->courier_stat_id, [CourierStat::CANCELLED_PROBLEM, CourierStat::CANCELLED, CourierStat::CANCELLED_USER]) OR in_array($item->user_cancel, [Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED]))
        {
            // just throw empty page if package is Cancelled or Problem
            $pdf->Output('manifest.pdf', 'D');
            Yii::app()->end();
        }

        $extAckCard = false;
        if($item->courierTypeInternal) {

            if($item->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_CONTRACT && $item->courierTypeInternal->delivery_operator_ordered)
            {
                if(!$ignoreExtCards)
                    $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeInternal->deliveryLabel->courierLabelNew);
            }
            else if ($item->courierTypeInternal->common_operator_ordered) {
                if(!$ignoreExtCards)
                    $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeInternal->commonLabel->courierLabelNew);
            }
            else if ($item->courierTypeInternal->pickup_operator_ordered) {
                if(!$ignoreExtCards)
                    $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeInternal->pickupLabel->courierLabelNew);
            }
        }
        else if ($item->courierTypeDomestic)
        {
            if(!$ignoreExtCards)
                $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeDomestic->courierLabelNew);

        }
        else if ($item->courierTypeU)
        {
            if(!$ignoreExtCards)
                $extAckCard = CourierExtExtAckCards::getAckCardAsString($item->courierTypeU->courierLabelNew);
        }

        if($extAckCard && !$ignoreExtCards)
        {

            $pdf->AddPage('L','A4');
            $pdf->Image('@'.$extAckCard,5,5,round($pdf->getPageWidth())-10, round($pdf->getPageHeight())-10,'','','', true, 300, '', false, false, 0, 'TL', false, true);

            $pdf->Output('manifest.pdf', 'D');
            Yii::app()->end();
        }

        $pdf->AddPage('P','A4');


//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);

//        $pdf->Image('@'.PDFGenerator::getLogoPlBig($item->user), $pdf->getPageWidth() /2 - 15 - $pdf->getMargins()['right'], 5 , 50,'','','N',false);

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->SetY($pdf->GetY() + 15);
        $pdf->SetTextColor(0,0,0);

        $pdf->SetFont('freesans', '', 20);
        $y = $pdf->GetY();
        $pdf->MultiCell(0, 0, Yii::t('courier_pdf','Potwierdzenie nadania'), 1, 'L', false);

        $pdf->MultiCell(0, 0, "#".$item->local_id, 0, 'R', false, 1, 15, $y, true, 0, false, true, 0);


        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'B', 15);
        $pdf->MultiCell(0,0, Yii::t('courier_pdf','Nadawca:'), 0, 'L');
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() -5);
        $data = array(
            array(Yii::t('courier_pdf','Firma:'), $item->senderAddressData->company),
            array(Yii::t('courier_pdf','Imię i nazwisko:'), $item->senderAddressData->name),
            array(Yii::t('courier_pdf','Adres:'), $item->senderAddressData->address_line_1),
            array(Yii::t('courier_pdf','Adres c.d.:'), $item->senderAddressData->address_line_2),
            array(Yii::t('courier_pdf','Kod pocztowy:'), $item->senderAddressData->zip_code),
            array(Yii::t('courier_pdf','Miasto:'), $item->senderAddressData->city),
            array(Yii::t('courier_pdf','Kraj:'), $item->senderAddressData->country),
        );
        self::ColoredTable($pdf, $data);

        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'B', 15);
        $pdf->MultiCell(0,0, Yii::t('courier_pdf', 'Odbiorca:'), 0, 'L', false, 1);
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() -5);
        $data = array(
            array(Yii::t('courier_pdf','Firma:'), $item->receiverAddressData->company),
            array(Yii::t('courier_pdf','Imię i nazwisko:'), $item->receiverAddressData->name),
            array(Yii::t('courier_pdf','Adres:'), $item->receiverAddressData->address_line_1),
            array(Yii::t('courier_pdf','Adres c.d.:'), $item->receiverAddressData->address_line_2),
            array(Yii::t('courier_pdf','Kod pocztowy:'), $item->receiverAddressData->zip_code),
            array(Yii::t('courier_pdf','Miasto:'), $item->receiverAddressData->city),
            array(Yii::t('courier_pdf','Kraj:'), $item->receiverAddressData->country),
        );
        self::ColoredTable($pdf, $data);

        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'B', 15);
        $pdf->MultiCell(0,0, Yii::t('courier_pdf', 'Szczegóły:'), 0, 'L', false, 1);
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() -5);
        $data = array(
            array(Yii::t('courier_pdf','Data nadania:'), substr($item->date_entered, 0, 10)),
            array(Yii::t('courier_pdf','Waga przesyłki').' ['.Courier::getWeightUnit().']:', $item->package_weight),
            array(Yii::t('courier_pdf','Wymiary przesyłki').' ['.Courier::getDimensionUnit().']:', $item->package_size_d.' x '.$item->package_size_l.' x '.$item->package_size_w),
            array(Yii::t('courier_pdf','Zawartość:'), $item->package_content),
            array(Yii::t('courier_pdf','Wartość').':', $item->package_value.' '.$item->getPackageValueCurrency()),
        );

        if($item->ref != '')
            $data[] =  array(Yii::t('courier_pdf','Ref:'), $item->ref);

        array_push($data, array(Yii::t('courier_pdf','Kwota pobrania:'),$item->cod_value ? $item->cod_value.' '.$item->cod_currency : '-'));
        array_push($data, array(Yii::t('courier_pdf','Nr konta:'), $item->senderAddressData->getBankAccount()));

// @09.23.2017 - do not show prices for users:
// DOBRAPACZKA, DOBRAPACZKA-DPD
// @ 18.10.2017 - KEDZIERZYNSP
        // @ 11.05.2018 - ANWISDHL
        if(!in_array($item->user_id, [698,743,332, 2671]))
            if($item->courierTypeInternal) {
                array_push($data, array(Yii::t('courier_pdf','Cena usługi:'), $item->courierTypeInternal->order->value_brutto.' '.$item->courierTypeInternal->order->currency));
            }
//        else if($item->courierTypeDomestic)
//        {
//            array_push($data, array(Yii::t('courier_pdf','Kwota pobrania:'), $item->courierTypeDomestic->cod_value.' '.$item->courierTypeDomestic->cod_currency));
//            array_push($data, array(Yii::t('courier_pdf','Nr konta:'), $item->senderAddressData->getBankAccount()));
//        }

        self::ColoredTable($pdf, $data);



        $data = '';
        $track_id = false;

        if($item->courierTypeInternal) {

            if ($item->courierTypeInternal->common_operator_ordered) {
                $data = [];
                $data[0] = array(Yii::t('courier_pdf', 'Operator odbierający:'), $item->courierTypeInternal->delivery_operator_name);
                if ($item->courierTypeInternal->commonLabel->track_id != '') {
                    $data[1] = array(Yii::t('courier_pdf', 'Nr nadania:'), $item->courierTypeInternal->commonLabel->track_id);
                    $track_id = $item->courierTypeInternal->commonLabel->track_id;
                }

            } else if ($item->courierTypeInternal->pickup_operator_ordered) {
                $data = [];
                $data[0] = array(Yii::t('courier_pdf','Operator odbierający:'), $item->courierTypeInternal->pickup_operator_name);
                if ($item->courierTypeInternal->pickupLabel->track_id != '') {
                    $data[1] = array(Yii::t('courier_pdf', 'Nr nadania:'), $item->courierTypeInternal->pickupLabel->track_id);
                    $track_id = $item->courierTypeInternal->pickupLabel->track_id;
                }

            }
        }
        else if ($item->courierTypeDomestic)
        {
            $data = [];
            $data[0] = array(Yii::t('courier_pdf','Operator odbierający:'), $item->courierTypeDomestic->courierDomesticOperator->name);
            if ($item->courierTypeDomestic->courierLabelNew->track_id != '') {
                $data[1] = array(Yii::t('courier_pdf', 'Nr nadania:'), $item->courierTypeDomestic->courierLabelNew->track_id);
                $track_id = $item->courierTypeDomestic->courierLabelNew->track_id;
            }
        }
        else if ($item->courierTypeU)
        {
            $data = [];
            $data[0] = array(Yii::t('courier_pdf','Operator odbierający:'), $item->courierTypeU->courierUOperator->name_customer);
            if ($item->courierTypeU->courierLabelNew->track_id != '') {
                $data[1] = array(Yii::t('courier_pdf', 'Nr nadania:'), $item->courierTypeU->courierLabelNew->track_id);
                $track_id = $item->courierTypeU->courierLabelNew->track_id;
            }
        }




        if(is_array($data)) {
            $pdf->SetAbsY($pdf->GetY() + 5);

            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0,0, 'Operator:', 0, 'L', false, 1);
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() -5);

            self::ColoredTable($pdf, $data, [], true);


            if($track_id)
            {
                $barcodeStyle = array(
                    'position' => '',
                    'align' => '',
                    'stretch' => false,
                    'fitwidth' => true,
                    'cellfitalign' => '',
                    'border' => false,
                    'hpadding' => 'auto',
                    'vpadding' => 'auto',
                    'fgcolor' => array(0,0,0),
                    'bgcolor' => false, //array(255,255,255),
                    'text' => false,
                    'font' => 'helvetica',
                    'fontsize' => 8,
                    'stretchtext' => 4
                );
                $pdf->write1DBarcode($track_id, 'C128', 110, $pdf->GetY() - 12, 90, 12, 1, $barcodeStyle);
            }
        }


        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', '', 9);
//        $pdf->MultiCell(0, 0, Yii::t('courier_pdf',"Dziękujemy za skorzystanie z usług Świata Przesyłek.{nl}Twoja przesyłka zostanie obsłużona przez najlepszych operatorów w zależności od wybranego kierunku i rodzaju przesyłki.{nl}Korzystając z usług Świata Przesyłek akceptujesz regulamin świadczenia usług.", ["{nl}" => "\r\n"]), 0, 'L', false);


        $pdf->SetAbsY($pdf->GetY() + 20);

        $y = $pdf->GetY();
        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(0, 0, Yii::t('courier_pdf','Podpis Nadawcy:').' ....................................................', 0, 'L', false);

        $pdf->SetAbsY($y);
        $pdf->MultiCell(0, 0, Yii::t('courier_pdf','Podpis Kuriera:').' ....................................................', 0, 'R', false);

        $pdf->SetFont('freesans', '', 8);
//        $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//        $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

        /* @var $pdf TCPDF */
        $pdf->Output('manifest.pdf', 'D');

        $item->onAfterAckGeneration();
    }

    protected static function ColoredTable(TCPDF $pdf,$data, $header = array(), $thirdColumn = false) {
        // Colors, line width and B font
        $pdf->SetFillColor(255, 0, 0);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(128, 0, 0);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFont('', 'B', 10);
        // Header
        $w = array(40, 100);


        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $pdf->Ln();
        // Color and font restoration
        $pdf->SetFillColor(224, 235, 255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('');
        // Data
        $fill = 0;
        $i = 0;
        $size = S_Useful::sizeof($data);
        foreach($data as $row) {
            $border = 'LR';
            if(!$i) {
                if($size - 1)
                    $border = 'LRT';
                else
                    $border = 'LRTB';
            }
            else if($i == $size - 1)
                $border = 'LRB';


            $pdf->Cell($w[0], 6, $row[0].'  ', $border, 0, 'R', $fill);

            if(!$thirdColumn)
                $pdf->Cell($w[1], 6, '  '.$row[1], $border, 0, 'L', $fill);
            else {
                $pdf->Cell(60, 6, '  '.$row[1], $border, 0, 'L', $fill);
                $pdf->Cell(90, 6, '', $border, 0, 'L', $fill);
            }



            $pdf->Ln();
            // $fill=!$fill;
            $i++;
        }

    }
}
