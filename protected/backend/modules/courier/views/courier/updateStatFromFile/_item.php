<?php
/* @var $model F_MassStatusChange */
/* @var $i Integer */

?>


<tr class="condensed">
    <td>
        <?php echo $form->hiddenField($model,'['.$i.']courier_local_id'); ?>
        <?php
        if($model->courierFound())
            echo CHtml::link($model->courier_local_id, Yii::app()->createUrl('/courier/courier/view', array('urlData' => $model->courier_local_id)), array('_target' => 'blank'));
        else
            echo $model->courier_local_id;
        ?>
    </td><td>
        <?php echo $model->duplicate?TbHtml::badge('yes', array('color' => TbHtml::BADGE_COLOR_WARNING)):TbHtml::badge('no', array('color' => TbHtml::BADGE_COLOR_SUCCESS));?>
    </td><td>
        <?php echo $model->courierFound()?TbHtml::badge('yes', array('color' => TbHtml::BADGE_COLOR_SUCCESS)):TbHtml::badge('no', array('color' => TbHtml::BADGE_COLOR_WARNING));?>
    </td>
    <?php if(!$model->courierFound()):?>
        <td colspan="3"></td>
    <?php else:?>
        <td>
            <?php echo $model->courierFound()?$model->Courier()->stat->name:'-'; ?>
        </td><td>
            <?php echo $form->dropDownList($model, '['.$i.']status_id', CHtml::listData(CourierStat::model()->findAll(),'id','name'), array('title' => $model->getError('status_id'))); ?>
        </td><td>
            <?php echo $form->textField($model,'['.$i.']status_date', array('style' => 'width: 150px;', 'title' => $model->getError('package_size_w'))); ?>
        </td>
    <?php endif;?>
</tr>

