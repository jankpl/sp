<h2><?php echo Yii::t('user', 'Gratulacje');?></h2>

<div style="text-align: center;">
    <p><?php echo Yii::t('user', 'Twoje konto zostało założone!');?></p>

    <p><?php echo Yii::t('user', 'Na podany adres email został wysłany list z linkiem aktywacyjnym. Po poprawnej aktywacji będziesz mógł się zalogować!');?></p>
</div>
<div class="navigate">
    <?php echo CHtml::link(Yii::t('app','Dalej'), Yii::app()->createUrl('site/login'), array('class' => 'btn')); ?>
</div>