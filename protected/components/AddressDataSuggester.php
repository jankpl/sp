<?php

class AddressDataSuggester extends CWidget
{
    /**
     * @var CFormModel
     */
    public $type;
    public $fieldName;
    public $countries;
    public $i;

    public function run()
    {
        $this->render('addressDataSuggester',
            array(
                'type' => $this->type,
                'fieldName' => $this->fieldName,
                'countries' => $this->countries,
                'i' => $this->i,
            ));
    }


    public static function registerAssets()
    {
        Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/addressDataSuggester.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/addressDataSuggester.css');
    }

}