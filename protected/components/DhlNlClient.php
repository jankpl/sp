<?php

class DhlNlClient extends GlobalOperator
{

    const URL = 'https://test.api.infinidelta.com/action/bd6dd59543d92e692c355715c9cf6f5d/101';

    public $package_value;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_mail;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $packages_number = 1;

    public $package_content;

    public $pod = false;
    public $no_neighbour_delivery = false;

    public $sms_notify = false;
    public $email_notify = false;

    public $ref1 = '';
    public $ref2 = '';
    public $local_id = '';

    public $label_annotation_text = false;

    public static function getAdditions(Courier $courier)
    {
        $CACHE_NAME = 'DHLNL_ADDITIONS_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_SMS_NOTIFY;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_EMAIL_NOTIFY;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_POD;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_NO_NEIGHBOUR_DELIVERY;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected function _curlCall($data = false)
    {

        MyDump::dump('dhl_nl.txt', print_r($data, 1));

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $url = self::URL.'?pl=N&zpl=N';


        $curl = curl_init($url);
        curl_setopt_array($curl, array(

            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING => "UTF-8",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Cache-Control: max-age=0',
            ),
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $resp = curl_exec($curl);
        $resp = json_decode($resp);

        MyDump::dump('dhl_nl.txt', print_r($resp, 1));

        curl_close($curl);

        if(!$resp OR $resp->state != 'true') {
            $return->error = true;
            $return->errorCode = $resp->message;
            $return->errorDesc = $resp->message ? $resp->message : 'Unknown error';
        } else {

            $return->success = true;
            $return->data = $resp;
        }

        return $return;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;
        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id, GLOBAL_BROKERS::BROKER_DHLNL);

        $senderName = $from->name;
        $senderCompany = $from->company;

        $model->sender_name = $senderName;
        $model->sender_company = $senderCompany;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->packages_number = $courierInternal->courier->packages_number;

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->ref1 = '#'.$courierInternal->courier->local_id;
        $model->ref2 = $courierInternal->courier->package_content;

        $model->package_value = $courierInternal->courier->getPackageValueConverted('EUR');

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_SMS_NOTIFY))
            $model->sms_notify = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_EMAIL_NOTIFY))
            $model->email_notify = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_POD))
            $model->pod = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_NO_NEIGHBOUR_DELIVERY))
            $model->no_neighbour_delivery = true;

        return $model->createShipment($returnErrors);
    }
//
    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $courierLabelNew, $returnError = false)
    {

        $model = new self;
        $model->label_annotation_text = $courierU->courierUOperator->label_symbol;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;


        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierU->courier->package_weight;
        $model->packages_number = $courierU->courier->packages_number;

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->ref1 = '#'.$courierU->courier->local_id;
        $model->ref2 = $courierU->courier->package_content;

        $model->package_value = $courierU->courier->getPackageValueConverted('EUR');

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_SMS_NOTIFY))
            $model->sms_notify = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_EMAIL_NOTIFY))
            $model->email_notify = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_POD))
            $model->pod = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DHLNL_NO_NEIGHBOUR_DELIVERY))
            $model->no_neighbour_delivery = true;

        return $model->createShipment($returnError);
    }

    public static function test()
    {
        $model = new self;

        $model->package_content = 'abc';
        $model->package_size_l = 10;
        $model->package_size_d = 10;
        $model->package_size_w = 10;
        $model->package_weight = 10;

        $model->receiver_name = 'Jan Nowak';
        $model->receiver_company = 'Nowak Inc';
        $model->receiver_address1 = 'Test Str 1';
        $model->receiver_city = 'DEN HAAG';
        $model->receiver_zip_code = '2511 AB';
        $model->receiver_country = 'NL';
        $model->receiver_tel = '123123123';
        $model->receiver_mail = 'dasdsaad@dsassaddsasaddsada.pl';

        $model->package_weight = 1;
        $model->package_content = 'test';
        $model->ref1 = '12321123';
        $model->ref2 = 'abc 123';


        $model->packages_number = 1;

//        $model->saturday_delivery = true;

        return $model->createShipment(true);
    }


    public function createShipment($returnError = false)
    {
        if(preg_match('/(po\-box)|(pobox)|(postbus)|(postbox)/i', ($this->receiver_address1.' '.$this->receiver_address2), $match)) {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('Cannot delivery to: ' . $match[0]);
            } else {
                return false;
            }
        }


        $name1 = mb_substr($this->receiver_name,0, 25);
        $name2 = mb_substr($this->receiver_name,25, 25);

        $data = [
            'parcel_CreatedBy' => 'KAAB',
            'checkbox_stdemailtosender' => 'N',
            'checkbox_stdemailtoreceiver' => $this->email_notify ? 'Y' : 'N',
            'parcel_checkbox_print_telefoonnummer' => 'Y',
            'parcel_select_vervoerder' => 2,
            'parcel_select_vervoerderoptie' => 32,
            'parcel_select_handtekeningvo' => $this->pod ? 'Y' : 'N',
            'parcel_input_aantalpakketten' => $this->packages_number,
            'parcel_input_gewicht' => $this->package_weight,
            'parcel_input_waarde' => $this->package_value,
            'parcel_input_afmetingl' => $this->package_size_l * 10,
            'parcel_input_afmetingb' => $this->package_size_w * 10,
            'parcel_input_afmetingh' => $this->package_size_d * 10,
            'parcel_select_nabb' => $this->no_neighbour_delivery ? 'Y' : 'N',
            'parcel_input_bedrijfsnaam' => $this->receiver_company,
            'parcel_input_afdeling' => '',
            'parcel_input_voornaam' => $name1,
            'parcel_input_achternaam' => $name2,
            'parcel_input_email' => $this->receiver_mail,
            'parcel_input_inhoud' => $this->package_content,
            'parcel_input_inhoudopetiket' => $this->local_id,
            'parcel_input_straat' => $this->receiver_address1,
            'parcel_input_straat2' => $this->receiver_address2,
            'parcel_input_huisnummer' => 0,
            'parcel_input_postcode' => $this->receiver_zip_code,
            'parcel_input_plaats' => $this->receiver_city,
            'parcel_select_land' => $this->receiver_country,
            'parcel_input_telefoonnummer' => $this->receiver_tel,
            'parcel_input_referentie' => $this->ref1,
            'parcel_input_referentie2' => $this->ref2,
        ];

        $resp = $this->_curlCall($data);


        if(!$resp->success)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->errorDesc);
            } else {
                return false;
            }
        }

        $return = [];
        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);


        $ttNumber = (string) $resp->data->Barcode;

        $context = [
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];


        $labelUrl = (string) $resp->data->url;
        $label = file_get_contents($labelUrl, false, stream_context_create($context));

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        if($this->label_annotation_text)
            LabelAnnotation::createAnnotation($this->label_annotation_text, $im, 230,60);

        $im->trimImage(0);
        $im->stripImage();

        file_put_contents('dhl_nl.png', $im->getImageBlob());

        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);


        return $return;
    }



    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return false;
    }


}