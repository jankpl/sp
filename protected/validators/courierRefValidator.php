<?php


class courierRefValidator extends CValidator
{

    const MIN = 5;
    const MAX = 50; // last characters are reserved for multipackages suffix

    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object,$attribute)
    {
        if($object->$attribute == '')
            return;

        if($object->user_id == '')
        {
            $this->addError($object,$attribute, 'Package without user cannot have ref number!');
            return;
        }

        $value = $object->$attribute;
        $value = strtoupper($value);

        if(!preg_match('/^[\p{L}\d\-\/ ]{'.self::MIN.','.self::MAX.'}$/iu', $value))
        {
            $this->addError($object,$attribute, Yii::t('courier', 'Pole REF może zawierać tylko litery i cyfry w ilości od {n} do {m} znaków!', ['{n}' => self::MIN, '{m}' => self::MAX]));
            return;
        }

        $courierRefPrefix = $object->user->getCourierRefPrefix();

        if(!$courierRefPrefix OR $courierRefPrefix == '')
        {
            $this->addError($object,$attribute, Yii::t('courier', 'Nie masz uprawnień do pola REF!'));
            return;
        }

        $rule = '^'.$courierRefPrefix.'.*';

        // @ 21.02.2019
        // Special rule for SF Express
        if(in_array($object->user->id, [1289,812,1534,2813,2404]))
            $rule = '(^'.$courierRefPrefix.'|^SF|^46|^99).*';

        if(!preg_match('/'.$rule.'/i', $value))
        {
            $this->addError($object,$attribute, Yii::t('courier', 'Pole REF nie jest zgodne z Twoim prefiksem!'));
            return;
        }

        $cmd = Yii::app()->db->createCommand();
        if($cmd->select('id')->from('courier')->where('ref = :ref', [':ref' => $value])->limit(1)->queryScalar())
        {
            $this->addError($object,$attribute, Yii::t('courier', 'Ten numer REF został juz wykorzystany!'));
            return;
        }
    }
}