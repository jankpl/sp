<?php

/* @var $model User */
/* @var $returnAddressModel AddressData */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label' =>'Create ' . $model->label(), 'url'=>array('create')),
    array('label' =>'View ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
    array('label' =>'Manage ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1>Update <?php echo GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>


<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<script>
    // go to particular tab on reload
    $(function(){
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');

        $('.nav-tabs a').click(function (e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });
    });
</script>
<?php
if(0):
    ?>
    <a href="<?= Yii::app()->createUrl('/user/balance', ['id' => $model->id]);?>" class="btn btn-primary">Balance</a>
    <br/>
    <br/>
<?php
endif;
?>

<?php echo TbHtml::tabbableTabs(array(
    array('label' => 'Data', 'active' => true, 'content' => $this->renderPartial('update_tabs/_data', array('model' => $model), true)),
    array('label' => 'Settings', 'content' => $this->renderPartial('update_tabs/_settings', array('model' => $model), true)),
    array('label' => 'Return address', 'content' => $this->renderPartial('update_tabs/_returnAddress', array('model' => $returnAddressModel), true)),
)); ?>

<script>
    $(document).ready(function(){
        var hash = window.location.hash;

        if(hash)
        {
            var hashSplit = hash.split('_');
            var base = hashSplit[0];

            if(base == '#set')
            {
                $('ul.nav a[href="#tab_2"]').tab('show');
                setTimeout(function(){
                    $(document).scrollTop( $(hash).offset().top);
                },1);
            }
        }
    });
</script>
