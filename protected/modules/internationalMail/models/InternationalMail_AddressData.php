<?php

class InternationalMail_AddressData extends AddressData
{


    public function rules() {

        $arrayValidate = array(

            array('email', 'email'),

            array('tel, email', 'required', 'on' => 'sender'),

            array('name, company, city, address_line_1, address_line_2, zip_code, tel', 'application.validators.lettersNumbersBasicValidator'),

            array('country_id, city, address_line_1, zip_code', 'required'),

            array('name', 'application.validators.eitherOneValidator', 'other' => 'company'),


            array('name,company,country,zip_code,city,address_line_1,address_line_2,tel, email,', 'length', 'max'=>45),

            array('country_id', 'safe'),

            array('id,name,company,country,zip_code,city,address_line_1,address_line_2,tel, email,', 'safe', 'on'=>'search'),
        );


        $arrayFilter =
            array(
                array('country_id, date_entered, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'filter', 'filter' => array( $this, 'filterStripTags')),

                array('date_entered', 'default',
                    'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

                array('country_id, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'default', 'setOnEmpty' => true, 'value' => null),
            );

        return array_merge($arrayFilter, $arrayValidate);
    }
}