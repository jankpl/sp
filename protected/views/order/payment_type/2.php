<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Janek
 * Date: 17.09.13
 * Time: 09:53
 * To change this template use File | Settings | File Templates.
 */

?>

<h2><?php echo Yii::t('order','Na moje konto');?></h2>
<p><?php echo Yii::t('order','Płatność została doliczona do Twojego konta.');?></p>
<div class="navigate">
<?php echo CHtml::link(Yii::t('app', 'Dalej'), Yii::app()->createUrl('order/view', array('urlData' => $model->hash, 'jumpToProduct' => true, 'new' => true)), array('class' => 'btn'));?>
</div>