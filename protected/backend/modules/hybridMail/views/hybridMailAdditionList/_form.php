<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'hybrid-mail-addition-list-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div><!-- row -->

    <div class="row">
        <table style="width: 500px;">
            <tr>
                <td>
                    <strong>First page price:</strong>
                    <?php $this->widget('PriceForm', array(
                        'form' => $form,
                        'model' => $model->priceFirstPage,
                        'formFieldPrefix' => '[first_page]',
                    ));?>
                </td>
                <td>
                    <strong>Next page price:</strong>
                    <?php $this->widget('PriceForm', array(
                        'form' => $form,
                        'model' => $model->priceNextPage,
                        'formFieldPrefix' => '[next_page]',
                    ));?>
                </td>
            </tr>
        </table>
    </div>

    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):



            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';


        endforeach;
        ?>
    </div>


    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

            $model_tr = $modelTrs[$languageItem->id];

            if($model_tr === null)
            {
                $model_tr = new HybridMailAdditionListTr;
                $model_tr->language_id = $languageItem->id;
            }


            $this->renderPartial('_tr_form', array(
                'model' => $model_tr,
                'form' => $form,
                'i' => $languageItem->id,
            ));

            echo'</div>';

        endforeach;
        ?>
    </div>

    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->