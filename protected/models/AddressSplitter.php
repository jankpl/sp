<?php


// NOT USED ANY LONGER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
class AddressSplitter
{
    public static function extractBuildingNo($text, $defaultValue = '0')
    {


        $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[2][0];


        // EXCEPTION - IF NUMBER IS CONNECTED WITH "FLAT" - take different number
        if(preg_match('/(FLAT '.str_replace('/','\/', $building).')/i', $text))
        {
            $tempItem = str_ireplace('FLAT '.$building, '', $text);
            if(preg_match_all($REG_PATTERN, $tempItem, $result2))
            {
                $building = $result2[2][0];
                $building = str_replace(['/', '.'], '', $building);
            }
        }

        // EXCEPTION - IF NUMBER WITH SEPARATOR IS IN THE END - it's probably with flat no
        if(preg_match('/(\/|\.|\-)/i', $building))
        {
            if(preg_match('/'.str_replace('/','\/', $building).'$/i', $text))
            {
                $building = preg_split('/(\/|\.|\-)/', $building);
                $building = $building[0];
            }

        }

        // IF NUMBER IS TOO LONG - split it:
        if(mb_strlen($building) > 5)
        {
            $building = explode('-', $building);
            $building = $building[0];
        }
        if(mb_strlen($building) > 5)
        {
            $building = explode('/', $building);
            $building = $building[0];
        }

        $building = trim($building);

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }
}