<?php

Yii::import('application.modules.postal.models._base.BasePostalAdditionalExtId');

class PostalAdditionalExtId extends BasePostalAdditionalExtId
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function add(Postal $postal, $ext_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)')->from((new self)->tableName())->where('postal_id = :postal_id AND external_id = :external_id', [':postal_id' => $postal->id, ':external_id' => $ext_id]);
        $result = $cmd->queryScalar();

        if($result)
            return false;

        $model = new self;
        $model->postal_id = $postal->id;
        $model->external_id = $ext_id;

        if($model->save())
        {
            $postal->addToLog('Added new external ID: '.$ext_id);
            return true;
        }

        IdStore::createNumber(IdStore::TYPE_POSTAL, $postal->id, $ext_id);

        return false;
    }

    public static function remove($id)
    {
        /* @var $model self */
        $model = self::findByPk($id);

        IdStore::removeNumber(IdStore::TYPE_POSTAL, $model->postal->id, $model->external_id);

        if($model) {
            $model->postal->addToLog('Removed external ID: ' . $model->external_id);
            $model->delete();
            return true;

        }

        return false;
    }

}