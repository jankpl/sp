<?php
/* @var $items_ids [] */
/* @var $form CActiveForm */

?>

<h2>Poczta Polska Waiting - Scanner</h2>

<div class="form" style="position: relative;">
    <div class="ajax-preloader"><div></div></div>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'=>'scanner-form',
        'enableClientValidation' => false,
    )); ?>

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <table class="table table-striped" style="margin: 0 auto; width: 500px;">
        <tr>
            <td style="text-align: center;"><?= CHtml::textArea('items_ids', $items_ids, ['id' => 'item-id', 'style' => 'padding: 15px 5px; text-align: center; width: 400px; font-size: 1.1em; font-weight: bold; height: 300px;', 'placeholder' => 'Items Id(s) (local or remote). Every ID must be in new line.']);?></td>
        </tr>
        <tr>
            <td style="text-align: center;"><?= CHtml::submitButton('Send data to PP system', ['class' => 'btn btn-large btn-primary']);?></td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>
</div>

<div class="alert alert-info">If posting date is earlier than tomorrow, tomorrow will be set automatically for those packages.</div>

<br/><br/>
<div style="text-align: center;">
    <a href="<?= Yii::app()->createUrl('/pocztaPolskaWaiting/scanner');?>" class="btn btn-mini">Restart</a>
</div>