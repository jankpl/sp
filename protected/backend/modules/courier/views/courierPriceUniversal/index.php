<?php
/* @var $model CourierPrice */
/* @var $models CourierPrice[] */
/* @var $this CourierPriceController */
?>


<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);


?>

    <h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?><?php echo ($group!== NULL)?' for group '.CHtml::link($group->name,Yii::app()->createUrl('/userGroup/view', array('id' => $group->id))):'';?></h1>

<?php
foreach($models AS $item)
{

    $this->renderPartial('_view',array(
        'model' => $item,
        'group' => $group,
    ));
}



