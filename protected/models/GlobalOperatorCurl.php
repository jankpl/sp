<?php

abstract class GlobalOperatorCurl extends GlobalOperatorBody
{
    protected function _curlCall($data)
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $headers = [];
        $headers[] = 'Content-Type: text/xml';

        $curl = curl_init(self::URL);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt_array($curl, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        );
        $resp = curl_exec($curl);

        MyDump::dump('yodel.txt', print_r($data,1));
        MyDump::dump('yodel.txt', print_r($resp,1));

        return $resp;
    }
}