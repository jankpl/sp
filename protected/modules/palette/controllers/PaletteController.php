<?php

class PaletteController extends Controller {

    public function beforeAction($action)
    {

        if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI)
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'HybridMail',
            ));
            exit;
        }

    }

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {

        return array(
            array('allow',
                'actions'=>array('index',),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('create', 'view', 'list'),
                'users'=>array('@'),
                'expression' => "0 < Yii::app()->user->getModel()->isPremium",
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }


    public function actionView($urlData) {

        $this->panelLeftMenuActive = 500;

        /* @var $model Palette */

        $model = Palette::model()->find('hash=:hash', array(':hash'=>$urlData));

        if($model === null)
            throw new CHttpException(404);

        $this->render('view', array(
            'model' => $model,
        ));
    }


    public function actionCreate() {

        $this->panelLeftMenuActive = 501;

        $saveToContactBookSender = NULL;
        $saveToContactBookReceiver = NULL;

        $model = new Palette;
        $model->senderAddressData = new AddressData();
        $model->receiverAddressData = new AddressData();
        //$this->performAjaxValidation($model, 'palette-form');
        //$this->performAjaxValidation($model->senderAddressData, 'palette-form');
        //$this->performAjaxValidation($model, 'palette-form->receiverAddressData');

        if(!isset($_POST['Palette']))
        {
            $model->senderAddressData->attributes = User::getLoggedUserAddressData();
        }

        if (isset($_POST['Palette'])) {

            $saveToContactBookSender = $_POST['Other']['saveToContactBook']['sender-data'];
            $saveToContactBookReceiver = $_POST['Other']['saveToContactBook']['receiver-data'];

            $model->setAttributes($_POST['Palette']);

            $model->senderAddressData->setAttributes($_POST['AddressData']['sender-data']);
            $model->senderAddressData->validate();

            $model->validate();


            $model->receiverAddressData->setAttributes($_POST['AddressData']['receiver-data']);
            $model->receiverAddressData->validate();


            if((!$model->hasErrors() AND !$model->senderAddressData->hasErrors() AND !$model->receiverAddressData->hasErrors()))            {
                $error = false;
                $transaction = Yii::app()->db->beginTransaction();

                try
                {

                    if(!$model->senderAddressData->save())
                        $error = true;
                    $model->sender_address_data_id = $model->senderAddressData->id;

                    if($saveToContactBookSender)
                    {
                        $modelContactBookSenderAddressData = new AddressData();
                        $modelContactBookSenderAddressData->attributes = $model->senderAddressData->attributes;

                        $modelContactBookSender = new UserContactBook();
                        $modelContactBookSender->user_id = Yii::app()->user->id;
                        $modelContactBookSender->addressData = $modelContactBookSenderAddressData;

                        if(!$modelContactBookSender->saveWithAddressData(false))
                            $error = true;
                    }

                    if(!$model->receiverAddressData->save())
                        $error = true;
                    $model->receiver_address_data_id = $model->receiverAddressData->id;

                    if($saveToContactBookReceiver)
                    {
                        $modelContactBookReceiverAddressData = new AddressData();
                        $modelContactBookReceiverAddressData->attributes = $model->receiverAddressData->attributes;

                        $modelContactBookReceiver = new UserContactBook();
                        $modelContactBookReceiver->user_id = Yii::app()->user->id;
                        $modelContactBookReceiver->addressData = $modelContactBookReceiverAddressData;

                        if(!$modelContactBookReceiver->saveWithAddressData(false))
                            $error = true;
                    }


                    if(!$model->save())
                        $error = true;


                    if(!$error)
                    {
                        $transaction->commit();

                        Yii::app()->user->setFlash('success', 'Twoje zlecenie zostało zapisane w systemie!');
                        $this->redirect(array('palette/list',
                        ));

                        return;
                    }
                    else
                    {
                        $transaction->rollback();
                    }

                }
                catch (Exception $ex)
                {
                    $transaction->rollback();
                }
            }
            //Yii::app()->user->setFlash('palette-form-errors', 'Nie wszystko jest poprawnie wypełnione!');

        }

        $this->render('create', array(
            'model' => $model,
            'saveToContactBooksender' => $saveToContactBookSender,
            'saveToContactBookreceiver' => $saveToContactBookReceiver
        ));
    }



    public function actionList()
    {
        $this->panelLeftMenuActive = 502;

        $model = new Palette('search');
        $model->unsetAttributes();

        if (isset($_GET['Palette']))
            $model->setAttributes($_GET['Palette']);

        $this->render('list', array(
            'model' => $model,
        ));


    }

    public function actionIndex() {

        $this->panelLeftMenuActive = 500;

        $this->render('index');
    }

}