<?php
/* @var $this PrintServerController */
/* @var $model PrintServer */

$this->breadcrumbs=array(
    'Print Servers'=>array('index'),
    $model->id,
);

$this->menu=array(
    array('label'=>'List PrintServer', 'url'=>array('index')),
);
?>

<h1>Manage PrintServer #<?php echo $model->id; ?></h1>
<br/>
<br/>
<br/>

<?php
if($model->stat == PrintServer::STAT_NEW)
    echo CHtml::link('Reset verification counter', ['/printServer/reset', 'id' => $model->id], ['class' => 'btn', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Are you sure?').'")']); ?>

<?php
if($model->stat != PrintServer::STAT_LOCKED)
    echo CHtml::link('Lock premanently this IP address', ['/printServer/lock', 'id' => $model->id], ['class' => 'btn btn-warning', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Are you sure?').'")']); ?>

<?php
if($model->stat != PrintServer::STAT_DELETED)
    echo CHtml::link('Mark as "Deleted"', ['/printServer/delete', 'id' => $model->id], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Are you sure?').'")']); ?>

<?php
if($model->stat == PrintServer::STAT_NEW)
    CHtml::link('Activate', ['/printServer/activate', 'id' => $model->id], ['class' => 'btn btn-success', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Are you sure?').'")']); ?>

<?php
echo CHtml::link('Delete permanently', ['/printServer/deletePermanently', 'id' => $model->id], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Are you sure?').'")']); ?>
