<?php

class S_InternationalMailMaxDimensions
{
    protected static $_maxDimensions = Array(
        'l' => 50,
        'd' => 50,
        'w' => 50,
        'combination' => 150,
        'weight' => 500,
    );


    public static function isTooBig(InternationalMail $model)
    {
        if(
            $model->mail_size_l < self::$_maxDimensions['l']
            &&
            $model->mail_size_d < self::$_maxDimensions['d']
            &&
            $model->mail_size_w < self::$_maxDimensions['w']
            &&
            $model->mail_size_l + $model->mail_size_w + $model->mail_size_d < self::$_maxDimensions['combination']
        )
            return false;
        else
            return true;
    }

    public static function isTooHeavy(InternationalMail $model)
    {
        if($model->mail_weight < self::$_maxDimensions['weight'])
            return false;
        else return true;
    }

    public static function getMaxDimensions()
    {
        return self::$_maxDimensions;
    }
}