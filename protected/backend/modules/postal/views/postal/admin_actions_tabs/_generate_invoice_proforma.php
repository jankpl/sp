<h4>Generate Invoice Proforma</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-inv',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/generateInvoiceProFormaByGridView'),
));
?>
<?php
echo TbHtml::submitButton('Generate Invoice Proforma', array(
    'onClick' => 'js:
    $("#postal-ids-inv").val($("#postal").selGridView("getAllSelection").toString());
    ;',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => 'postal-ids-inv')); ?>
<?php
$this->endWidget();
?>
