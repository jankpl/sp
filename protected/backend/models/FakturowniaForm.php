<?php


class FakturowniaForm extends CFormModel
{
    public $addressData_receiver;
    public $items = [];

    public $sellDate;
    public $issueDate;
    public $paymentToDate;
    public $paidDate = '';

    public $currency; // ID
    public $lang;

    public $saveToUserInvoice = false;
    public $userInvoiceDescription;
    public $userInvoicePublish = false;

    public $file_sheet;

    public $fakturowniaAccountId = Fakturownia::ACCOUNT_SP;

    public $correction = false;
    public $itemsBefore = [];
    public $itemsAfter = [];

    public $paymentType;

    public $description;

    public $number = '';

    public static function getPaymentTypeList()
    {
        return [
            1 => 'Gotówka',
            2 => 'Karta płatnicza',
            3 => 'Przelew',
        ];
    }

    public function getPaymentTypeDesc()
    {
        return isset(self::getPaymentTypeList()[$this->paymentType]) ? self::getPaymentTypeList()[$this->paymentType] : NULL;
    }

    public function getUserInvoiceSource()
    {
        switch($this->fakturowniaAccountId)
        {
            case Fakturownia::ACCOUNT_SP:
            default:
                return UserInvoice::SOURCE_FAKTUROWNIA_MANUAL;
                break;
            case Fakturownia::ACCOUNT_KEDZIERZYN:
                return UserInvoice::SOURCE_FAKTUROWNIA_MANUAL_KEDZIERZYN;
                break;
        }
    }

    public function getIsPaid()
    {
        return ($this->paidDate != '');
    }

    public function init()
    {
        $this->items[] = new FakturowniaItem();

        $this->addressData_receiver = new AddressData_Order();
        $this->addressData_receiver->scenario = AddressData_Order::SCENARIO_ONE_OR_ANOTHER;

        $this->issueDate = date('Y-m-d');
        $this->sellDate = date('Y-m-d');
        $this->paymentToDate = date('Y-m-d');
//        $this->paidDate = date('Y-m-d');

        parent::init();
    }

    public function afterValidate()
    {
        if($this->isPaid && $this->paidDate == '')
            $this->addError('paidDate', 'Set payment date');

        if(!S_Useful::sizeof($this->items))
            $this->addError('items', 'Add at least one position!');


        parent::afterValidate();
    }

    public function rules()
    {
        return array(
            ['paymentType', 'required'],
            ['paymentType', 'in', 'range' => array_keys(self::getPaymentTypeList())],
            ['fakturowniaAccountId', 'in', 'range' => array_keys(Fakturownia::listAccounts())],
            ['sellDate, issueDate, paymentToDate', 'required'],
            ['currency', 'in', 'range' => Currency::listIds() ],
            ['lang', 'in', 'range' => Language::languagesArray()],
            ['saveToUserInvoice', 'numerical'],
            ['userInvoiceDescription', 'length', 'max' => 128],
            ['number', 'length', 'max' => 32],
            ['description', 'length', 'max' => 512],
            ['isPaid, correction, userInvoicePublish', 'boolean'],
            ['sellDate, issueDate, paymentToDate, paidDate', 'date', 'format' => 'yyyy-MM-dd'],
            ['file_sheet', 'file', 'allowEmpty'=>true, 'types'=>'xls, xlsx, pdf, doc, docx', 'maxSize' => 10448896, 'maxFiles' => 1,],
        );
    }


    public function attributeLabels()
    {
        return array(
            'description' => 'Description on Invoice',
        );
    }

    public function getUserLogin()
    {
        if($this->saveToUserInvoice)
        {
            $model = User::model()->findByPk($this->saveToUserInvoice);
            return $model->login;
        }

        return false;
    }

    public function calculateTotalPaymentValue()
    {
        $value = 0;

        if($this->correction)
        {
            /* @var $item FakturowniaItem */
            foreach($this->itemsAfter AS $key => $item)
                $value += ($item->no * $item->value_brutto - $this->itemsBefore[$key]->no * $this->itemsBefore[$key]->value_brutto);

        } else {
            /* @var $item FakturowniaItem */
            foreach($this->items AS $item)
                $value += $item->value_brutto;
        }




        return $value;
    }

}