<?php
/* @var $model CountryList */
/* @var $modelTrs CountryListTr[] */
?>


<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'country-list-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div><!-- row -->

    <div class="alert alert-info">Please provide 2 letters uppercase code (ISO 3166-1 alfa-2)</div>

    <div class="row">
        <?php echo $form->labelEx($model,'code'); ?>
        <?php echo $form->textField($model, 'code', array('maxlength' => 2)); ?>
        <?php echo $form->error($model,'code'); ?>
    </div><!-- row -->

    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';


        endforeach;
        ?>
    </div>


    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

            $model_tr = $modelTrs[$languageItem->id];

            if($model_tr === null)
            {
                $model_tr = new CountryListTr();
                $model_tr->country_list_id = $model->id;
                $model_tr->language_id = $languageItem->id;
            }

            $this->renderPartial('_tr_form', array(
                'model' => $model_tr,
                'form' => $form,
                'i' => $languageItem->id,
            ));

            echo'</div>';

        endforeach;
        ?>
    </div>

    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->