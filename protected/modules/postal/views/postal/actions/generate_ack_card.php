<div class="alert alert-success">
    <h4><?php echo Yii::t('postal','Karta potwierdzenia');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t('postal','Generuj kartę potwierdzenia'), array('/postal/postal/ackCard', 'hash' => $model->hash), array('target' => '_blank', 'class' => 'btn'));?>
</div>
