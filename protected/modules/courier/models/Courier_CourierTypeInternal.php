<?php

class Courier_CourierTypeInternal extends Courier
{

    public $_package_weight_total;
    public $_client_cod;

//    public $_client_cod_value;

    const SCENARIO_DEFAULT_PACKAGE_DATA = 'default_package_data';
    const SCENARIO_STEP1 = 'step1';

//    public function checkClientCodValue($attribute,$params)
//    {
//        if($this->_client_cod)
//        {
//            if(!($this->_client_cod_value > 0))
//                $this->addError('_client_cod_value', Yii::t('courier', 'Podaj wartość COD dla paczki!'));
//        }
//    }

    public function rules() {


        $arrayValidate = array(
            array('value_currency', 'default', 'value' => 'PLN'),
            array('value_currency', 'in', 'range' => self::getCurrencyList(), 'allowEmpty' => true),

            array('ref', 'application.validators.courierRefValidator'),

//            array('_client_cod_value', 'numerical', 'max' =>  PaymentType::MAX_COD_AMOUNT, 'min' => 0),
//            array('_client_cod_value', 'checkClientCodValue', 'on' => 'step1'),

            array('cod_value', 'numerical', 'min' => 0),
            array('cod_value', 'match', 'pattern'=> '/^[0-9]+(\.[0-9]{1,2})?$/'),


            array('cod_currency', 'in', 'range' => array_keys(CourierCodAvailability::getCurrencyList()) , 'message' => Yii::t('courier', 'Ta waluta nie jest obsługiwana')),


            array('package_value', 'numerical', 'min'=>1, 'max' => 999999),
            array('package_value', 'match', 'pattern'=> '/^[0-9]+(\.[0-9]{1,2})?$/'),

            array('packages_number', 'numerical', 'integerOnly'=>true, 'min' => 1),
            array('packages_number', 'default', 'setOnEmpty' => true, 'value' => 1),

            array('package_weight, package_size_l, package_size_w, package_size_d', 'required'),
            array('package_content', 'required', 'except' => self::SCENARIO_DEFAULT_PACKAGE_DATA),
            array('package_size_l', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 330),
            array('package_size_w', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 330),
            array('package_size_d', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 330),
            array('package_weight', 'numerical', 'min' => 0.01, 'max' => 70),


            array('user_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?NULL:Yii::app()->user->id),
            array('admin_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?Yii::app()->user->id:NULL),
            array('package_weight, package_weight, package_size_l, package_size_w, package_size_d, package_value', 'required'),

            array('courier_type_external_id, courier_type_internal_id, sender_address_data_id, receiver_address_data_id, courier_stat_id', 'numerical', 'integerOnly'=>true),

//            array('package_size_l, package_size_w, package_size_d', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('package_weight', 'numerical', 'min' => 0.01),

            array('_package_weight_total', 'numerical', 'min' => 0.01),
            array('_package_weight_total', 'required', 'on' => self::SCENARIO_STEP1),

            array('ref', 'length', 'max'=>64),
            array('note1, note2', 'length', 'max'=>50),

            array('_sendLabelOnMail', 'boolean', 'allowEmpty'=> true),

            array('source', 'numerical', 'integerOnly'=>true),

            array('date_updated, _generate_return', 'safe'),
//            array('date_updated', 'safe'),
            array('courier_type_external_id, courier_type_internal_id, date_updated, sender_address_data_id, package_value, package_content, package_weight, package_size_l, package_size_w, package_size_d, _client_cod', 'default', 'setOnEmpty' => true, 'value' => null),
        );

        $arrayBasic = array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert', 'setOnEmpty' => false),

            array('courier_type_external_id, courier_type_internal_id, date_updated, local_id, hash, sender_address_data_id, receiver_address_data_id, courier_stat_id, package_weight, package_size_l, package_size_w, package_size_d, packages_number, package_value, package_content, cod, ref, user_id, admin_id, _first_status_date, note1, note2', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('courier_type_external_id, courier_type_internal_id, date_updated, sender_address_data_id, receiver_address_data_id, package_weight, package_size_l, package_size_w, package_size_d, package_value, package_content, cod, ref, user_id, admin_id', 'default', 'setOnEmpty' => true, 'value' => null),

            array('courier_stat_id', 'default', 'setOnEmpty' => true, 'value' => CourierStat::NEW_ORDER),
        );


        return array_merge($arrayBasic, $arrayValidate);
    }

    public function attributeLabels() {
        $labels = array(
            '_package_weight_total' => Yii::t('courier', 'Packages total weight'),
//            '_client_cod_value' => Yii::t('courier', 'COD Value'),
        );

        return array_merge($labels, parent::attributeLabels());
    }

    public function beforeValidate()
    {
        if(is_string($this->cod_value))
            $this->cod_value = (double) ($this->cod_value);

        // for Internal Form
        if($this->_client_cod)
        {
            if(!($this->cod_value > 0))
                $this->addError('cod_value', Yii::t('courier', 'Podaj wartość COD dla paczki!'));
        }

        if($this->_generate_return)
        {
            if(!CourierTypeReturn::isAutomaticReturnAvailable($this->receiverAddressData->country_id))
            {
                $this->_generate_return = false;
            }
//                $this->addError('_generate_return', Yii::t('courier', 'Etykieta zwrotna nie jest dostępna dla tych parametrów!'));
        }

        if($this->cod_value)
        {
            if(!$this->findCodCurrency())
                $this->addError('cod_value', Yii::t('courier', 'COD nie jest dostępne dla tych parametrów!'));
            else {

                if ($this->cod_currency == '') {
                    $this->cod_currency = $this->findCodCurrency();
                } else if (!CourierCodAvailability::validateCurrencyNameForOperatorAndCountry($this->cod_currency, CourierCodAvailability::OPERATOR_INTERNAL, $this->courierTypeInternal->getDeliveryOperator(), $this->receiverAddressData->country_id)) {
                    $properCurrency = CourierCodAvailability::findCurrencyForOperatorAndCountry(CourierCodAvailability::OPERATOR_INTERNAL, $this->courierTypeInternal->getDeliveryOperator(), $this->receiverAddressData->country_id);
                    $properCurrency = CourierCodAvailability::getCurrencyNameById($properCurrency);
                    $this->addError('cod_currency', Yii::t('courier', 'Prawidłowa waluta dla tego kraju to:') . ' ' . $properCurrency);
                }

                if(!$this->hasErrors('cod_currency'))
                {
                    $max_cod_value = CourierCodAvailability::getCurrencyMaxValueById($this->cod_currency);
                    if ($this->cod_value > $max_cod_value)
                        $this->addError('cod_value', Yii::t('courier', 'Maksymalna wartość COD dla {currency} to: {max_value}', ['{currency}' => $this->cod_currency, '{max_value}' => $max_cod_value]));
                }
            }
        }


        return parent::beforeValidate();
    }


    public function afterValidate()
    {
        $_isWeightValidated = $_isDimensionsValidated = false;

        if($this->user && $this->user->getDisableDimensionLimits())
            $_isDimensionsValidated = true;
//        if(in_array($this->courierTypeDomestic->domestic_operator_id, [CourierDomesticOperator::OPERATOR_DHL_PALETA, CourierDomesticOperator::OPERATOR_DHL_F_PALETA])) {
//            $this->_disableCalculationDimensionalWeight = true;
//            $this->_disableAdditionalWeightAndSizeCheck = true;
//        }

//        if(parent::beforeValidate())
        {
            $dimensions = [
               intval($this->package_size_l),
                intval($this->package_size_d),
                intval($this->package_size_w),
            ];

            // biggest dimension is length and is first
            rsort($dimensions);

            if(!$_isDimensionsValidated) {
                // NEW RULES @ 25.08.2017
                {
                    // RUCH
                    if ($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH) {

//                    $_isWeightValidated = true;


                        if (!$_isDimensionsValidated) {

                            if ($dimensions[0] > PaczkaWRuchu::MAX_DIMENSIONS_1 OR
                                $dimensions[1] > PaczkaWRuchu::MAX_DIMENSIONS_2 OR
                                $dimensions[2] > PaczkaWRuchu::MAX_DIMENSIONS_3) {
                                $this->clearErrors('package_size_l');
                                $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => PaczkaWRuchu::MAX_DIMENSIONS_1, '{b}' => PaczkaWRuchu::MAX_DIMENSIONS_2, '{c}' => PaczkaWRuchu::MAX_DIMENSIONS_3]));
                            }
                        }


                    } else if ($this->senderAddressData->country_id == $this->receiverAddressData->country_id && $this->senderAddressData->country_id == CountryList::COUNTRY_PL) {
                        // DOMESTIC PACKAGE
                        $this->_disableCalculationDimensionalWeight = true;

                        // above a > 140 or a + b + c > 260 operator DHL i changed to DPD

                        if (
                            ($this->courierTypeInternal->_package_wrapping && $this->courierTypeInternal->_package_wrapping != User::WRAPPING_DEFAULT) OR
                            $dimensions[0] > 175 OR
                            array_sum($dimensions) > 300
                        ) {
                            if (
                                $dimensions[0] > 250 OR
                                array_sum($dimensions) > 330
                            ) {
                                $this->clearErrors('package_size_l');
                                $this->addError('package_size_l', Yii::t('courier', 'Najdłuższy bok paczki nie może przekroczyć {a} cm oraz suma najdłuższego boku i obwodu nie może być większa niż {b} cm!', ['{a}' => 250, '{b}' => 330]));
                            } else {
                                // @12.05.2018
                                // Do not force NST for user MOKEE (#1885)
                                if (!in_array($this->user_id, [1885])) {
                                    $this->courierTypeInternal->_courier_additions[] = CourierAdditionList::PACKAGE_NON_STANDARD_DOMESTIC;
                                    Yii::app()->user->setFlash('notice', Yii::t('courier', 'Ze względu na podane parametry paczki został automatycznie wybrany dodatek "Przesyłka niestandardowa"!'));
                                }
                            }
                        }

                    } else if ($this->senderAddressData->country_id != $this->receiverAddressData->country_id && ($this->senderAddressData->country_id == CountryList::COUNTRY_PL OR $this->receiverAddressData->country_id == CountryList::COUNTRY_PL)) {
                        // IMPORT OR EXPORT

                        if (
                            $dimensions[0] > 120 OR
                            $dimensions[1] > 60 OR
                            ($this->courierTypeInternal->_package_wrapping && $this->courierTypeInternal->_package_wrapping != User::WRAPPING_DEFAULT)
                        ) {
                            if (
                                $dimensions[0] > 200 OR
                                ($dimensions[0] + 2 * $dimensions[1] + 2 * $dimensions[2] > 300)
                            ) {
                                $this->clearErrors('package_size_l');
                                $this->addError('package_size_l', Yii::t('courier', 'Najdłuższy bok paczki nie może przekroczyć {a} cm oraz suma najdłuższego boku i obwodu nie może być większa niż {b} cm!', ['{a}' => 200, '{b}' => 300]));
                            } else {
                                // @12.05.2018
                                // Do not force NST for user MOKEE (#1885)
                                if (!in_array($this->user_id, [1885])) {
                                    $this->courierTypeInternal->_courier_additions[] = CourierAdditionList::PACKAGE_NON_STANDARD_INT;
                                    Yii::app()->user->setFlash('notice', Yii::t('courier', 'Ze względu na podane parametry paczki został automatycznie wybrany dodatek "Przesyłka niestandardowa"!'));
                                }
                                if (
                                    $dimensions[0] > 175 OR
                                    array_sum($dimensions) > 300
                                ) {
                                    // @12.05.2018
                                    // Do not force NST for user MOKEE (#1885)
                                    if (!in_array($this->user_id, [1885])) {
                                        $this->courierTypeInternal->_courier_additions[] = CourierAdditionList::PACKAGE_NON_STANDARD_DOMESTIC;
                                        Yii::app()->user->setFlash('notice', Yii::t('courier', 'Ze względu na podane parametry paczki został automatycznie wybrany dodatek "Przesyłka niestandardowa"!'));
                                    }
                                }
                            }

                        }


                    } else {
                        // INTERNATIONAL PACKAGE

                        if (
                            $dimensions[0] > 120 OR
                            $dimensions[1] > 60 OR
                            ($this->courierTypeInternal->_package_wrapping && $this->courierTypeInternal->_package_wrapping != User::WRAPPING_DEFAULT)
                        ) {
                            if (
                                $dimensions[0] > 200 OR
                                ($dimensions[0] + 2 * $dimensions[1] + 2 * $dimensions[2] > 300)
                            ) {
                                $this->clearErrors('package_size_l');
                                $this->addError('package_size_l', Yii::t('courier', 'Najdłuższy bok paczki nie może przekroczyć {a} cm oraz suma najdłuższego boku i obwodu nie może być większa niż {b} cm!', ['{a}' => 200, '{b}' => 300]));
                            } else {
                                // @12.05.2018
                                // Do not force NST for user MOKEE (#1885)
                                if (!in_array($this->user_id, [1885])) {
                                    $this->courierTypeInternal->_courier_additions[] = CourierAdditionList::PACKAGE_NON_STANDARD_INT;
                                    Yii::app()->user->setFlash('notice', Yii::t('courier', 'Ze względu na podane parametry paczki został automatycznie wybrany dodatek "Przesyłka niestandardowa"!'));
                                }
                            }

                        }
                    }
                }


                // RUCH
                if ($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH) {

                    if ($this->package_weight > PaczkaWRuchu::MAX_WEIGHT) {

                        $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => PaczkaWRuchu::MAX_WEIGHT]));
                    }
                }
                // RM
                else if ($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_RM) {

                    if ($this->package_weight > RoyalMailClient::RM_MAX_WEIGHT) {
                        $this->clearErrors('package_weight');
                        $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => RoyalMailClient::RM_MAX_WEIGHT]));
//                        $_isWeightValidated = true;
                    }
                }

                // VALIDATE WEIGHT
                if(!in_array($this->courierTypeInternal->with_pickup, [CourierTypeInternal::WITH_PICKUP_RM, CourierTypeInternal::WITH_PICKUP_RUCH]))
                {
//
//
                    $dw = $this->courierTypeInternal->getDimensionWeight();
                    $weight = $this->package_weight;

                    if($dw > $weight)
                        $weight = $dw;

                    $suffix = '';
                    if($dw > $this->package_weight)
                        $suffix = ' '.Yii::t('courier', 'Obliczona waga gabarytowa to {a} kg.', ['{a}' => $dw]);

                    if($this->senderAddressData->country_id == $this->receiverAddressData->country_id &&
                        $this->senderAddressData->country_id == CountryList::COUNTRY_PL
                    )
                    {
                        if($weight > 50) {
                            $this->clearErrors('package_weight');
                            $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki dla podanych parametrów to {a} kg!', ['{a}' => 50]) . $suffix);
                        }
                    }
                    else if($this->senderAddressData->country_id == CountryList::COUNTRY_PL && $this->receiverAddressData->country_id != CountryList::COUNTRY_PL)
                    {
                        if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR) {
                            if ($weight > 50) {
                                $this->clearErrors('package_weight');
                                $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki dla podanych parametrów to {a} kg!', ['{a}' => 50]) . $suffix);
                            }
                        }
                        else {
                            if ($weight > 70) {
                                $this->clearErrors('package_weight');
                                $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki dla podanych parametrów to {a} kg!', ['{a}' => 70]) . $suffix);
                            }
                        }
                    }
                    else if($this->senderAddressData->country_id != CountryList::COUNTRY_PL && $this->receiverAddressData->country_id == CountryList::COUNTRY_PL)
                    {
                        if ($weight > 50) {
                            $this->clearErrors('package_weight');
                            $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki dla podanych parametrów to {a} kg!', ['{a}' => 50]) . $suffix);

                        }
                    } else {
                        if ($weight > 70) {
                            $this->clearErrors('package_weight');
                            $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki dla podanych parametrów to {a} kg!', ['{a}' => 70]) . $suffix);
                        }
                    }

                }

            }


            if($this->courierTypeInternal->_inpost_locker_active && $this->courierTypeInternal->_inpost_locker_delivery_id)
            {
                if($dimensions[2] > 8 && $dimensions[2] <= 19)
                {
                    $this->courierTypeInternal->_courier_additions[] = CourierAdditionList::getIdByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_SIZE_B);
                    Yii::app()->user->setFlash('notice', Yii::t('courier', 'Ze względu na podane parametry paczki został automatycznie wybrany dodatek gabarytowy Inpost!'));
                }
                else if($dimensions[2] > 19)
                {
                    $this->courierTypeInternal->_courier_additions[] = CourierAdditionList::getIdByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_SIZE_C);
                    Yii::app()->user->setFlash('notice', Yii::t('courier', 'Ze względu na podane parametry paczki został automatycznie wybrany dodatek gabarytowy Inpost!'));
                }

            }

//            return true;


        }
//        return false;

        parent::afterValidate();
    }

    // there is only one mode left, so method is not needed
    public function setDimensionalWeightCalculationMode()
    {

//        $dimensions = [
//            $this->package_size_l,
//            $this->package_size_d,
//            $this->package_size_w,
//        ];
//
//        // biggest dimension is length and is first
//        rsort($dimensions);
//
//        {
//
//            // RUCH
//            if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH)
//            {
//
//
//
//            }
//
//            // RM
//            else if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_RM)
//            {
//
//
//            }
//
//            else if($this->senderAddressData->country_id == $this->receiverAddressData->country_id && $this->senderAddressData->country_id == CountryList::COUNTRY_PL)
//            {
//                // DOMESTIC PACKAGE
//
//                if(
//                    $this->package_weight > 31.5 OR
//                    ( $this->courierTypeInternal->_package_wrapping && $this->courierTypeInternal->_package_wrapping != User::WRAPPING_DEFAULT) OR
//                    $dimensions[0] > 140 OR
//                    $dimensions[1] > 60 OR
//                    array_sum($dimensions) > 260
//                )
//                {
//
//                    if(
//                        $dimensions[0] > 200 OR
//                        ($dimensions[0] + 2*$dimensions[1] + 2*$dimensions[2] > 300)
//                    )
//                    {
//
//                    }
//                    else {
//
//                        if($this->package_weight > 31.5)
//                            $this->_calculationDimensionalWeightDHLMode = false;
//                        else
//                            $this->_calculationDimensionalWeightDHLMode = true;
//                    }
//
//                }
//                else
//                {
//                    $this->_disableCalculationDimensionalWeight = true;
//                }
//            }
//
//            else if($this->senderAddressData->country_id != $this->receiverAddressData->country_id && ($this->senderAddressData->country_id == CountryList::COUNTRY_PL OR $this->receiverAddressData->country_id == CountryList::COUNTRY_PL))
//            {
//                // IMPORT OR EXPORT
//                $this->_calculationDimensionalWeightDHLMode = false;
//
//                if(
//                    $dimensions[0] > 120 OR
//                    $dimensions[1] > 60 OR
//                    ( $this->courierTypeInternal->_package_wrapping && $this->courierTypeInternal->_package_wrapping != User::WRAPPING_DEFAULT)
//                )
//                {
//
//
//                    if(
//                        $dimensions[0] > 200 OR
//                        ($dimensions[0] + 2*$dimensions[1] + 2*$dimensions[2] > 300)
//                    )
//                    {}
//                    else {
//
//                        $this->_calculationDimensionalWeightDHLMode = false;
//                    }
//
//                } else {
//                    //
//                }
//
//
//            }
//            else {
//                // INTERNATIONAL PACKAGE
//
//                // IMPORT OR EXPORT
//                $this->_calculationDimensionalWeightDHLMode = false;
//
//                if(
//                    $dimensions[0] > 120 OR
//                    $dimensions[1] > 60 OR
//                    ( $this->courierTypeInternal->_package_wrapping && $this->courierTypeInternal->_package_wrapping != User::WRAPPING_DEFAULT)
//                )
//                {
//
//
//                    if(
//                        $dimensions[0] > 200 OR
//                        ($dimensions[0] + 2*$dimensions[1] + 2*$dimensions[2] > 300)
//                    )
//                    {}
//                    else {
//
//                        $this->_calculationDimensionalWeightDHLMode = false;
//                    }
//
//                } else {
//                    //
//                }
//            }
//
//
//        }
    }

    public function findCodCurrency()
    {

        $deliveryOperator = $this->courierTypeInternal->getDeliveryOperator();
        $codCurrency = CourierCodAvailability::findCurrencyForOperatorAndCountry(CourierCodAvailability::OPERATOR_INTERNAL, $deliveryOperator, $this->receiverAddressData->country_id);
        if($codCurrency)
        {
            $codCurrency = CourierCodAvailability::getCurrencyNameById($codCurrency);
        }

        return $codCurrency;
    }

    public function validateCodCurrency()
    {
        return CourierCodAvailability::validateCurrencyNameForOperatorAndCountry($this->cod_currency, CourierCodAvailability::OPERATOR_INTERNAL, $this->courierTypeInternal->getDeliveryOperator(), $this->receiverAddressData->country_id);
    }

    public function beforeSave()
    {

        $this->setDimensionalWeightCalculationMode();

        if($this->cod_value && $this->cod_currency === NULL)
            $this->cod_currency = $this->findCodCurrency();

        $dim_weight = S_PackageMaxDimensions::calculateDimensionsWeight($this);

        if ($this->package_weight < $dim_weight) {
            $this->package_weight_client = $this->package_weight;
            $this->package_weight = $dim_weight;
        }


        $this->courier_type = self::TYPE_INTERNAL;


        if($this->courierTypeInternal->getPickupOperator()) {
            $this->operator_partner_id = $this->courierTypeInternal->getPickupOperator(true)->partner_id;
            $this->operator_partner_id_2 = $this->courierTypeInternal->getDeliveryOperator(true)->partner_id;
        } else {
            $this->operator_partner_id = $this->courierTypeInternal->getDeliveryOperator(true)->partner_id;
        }

        return parent::beforeSave();
    }
}