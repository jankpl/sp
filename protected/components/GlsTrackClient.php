<?php


class GlsTrackClient extends CApplicationComponent
{
    const URL = GLOBAL_CONFIG::GLS_TRACK_URL;
    const WSDL = GLOBAL_CONFIG::GLS_TRACK_WSDL;
    const LOGIN = GLOBAL_CONFIG::GLS_TRACK_LOGIN;
    const PASSWORD = GLOBAL_CONFIG::GLS_TRACK_PASSWORD;

    protected $track_id;

    public static function track($id)
    {
        $model = new self;
        $model->track_id = $id;
        return $model->_go();
    }


    protected function _go()
    {
        try {

            $mode = array
            (
                'soap_version' => SOAP_1_1,  // use soap 1.1 client
                'trace' => 1
            );

            $client = new SoapClient(self::WSDL, $mode);
            $client->__setLocation(self::URL);

            $data = [
                'Credentials' => [
                    'UserName' => self::LOGIN,
                    'Password' => self::PASSWORD,
                ],
                'RefValue' => $this->track_id,
                'Parameters' => [
                    'ParamCode' => 'LangCode',
                    'ParamValue' => 'EN',
                ],
            ];

            $functionName = 'GetTuDetail';

            $resp = $client->__soapCall($functionName, [$functionName => $data]);

            // IF PASSWORD HAS EXPIRED
            if($resp->ExitCode->ErrorCode == 502) {
                Yii::log('GLS TT Password has expired!', CLogger::LEVEL_ERROR);
               return false;
            }

            $history = $resp->History;

            $statuses = array();

            if (!is_array($history))
                $history = array($history);


            foreach ($history AS $item) {

               if($item->Desc == 'The parcel data was entered into the GLS system\3B the parcel was not yet handed over to GLS.')
                   continue;

                $name = $item->Desc;
                $date = $item->Date->Year.'-'.$item->Date->Month.'-'.$item->Date->Day.' '.$item->Date->Hour.':'.$item->Date->Minut.':00';

                $where = $item->LocationName.', '.$item->CountryName;

                $temp = array(
                    'name' => $name,
                    'date' => $date,
                    'location' => $where,
                );

                array_push($statuses, $temp);
            }


            return $statuses;

        } catch (SoapFault $E) {
//            Yii::log(print_r($E, 1), CLogger::LEVEL_ERROR);
            return false;
//
//                    throw new CException(print_r([
//                        'code' => $E->faultcode,
//                        'string' => $E->faultstring,
//                        'lastRequest' => print_r($client->__getLastRequest(), 1),
//                    ], 1)
//                );
        }
        catch(Exception $ex)
        {
            return false;
        }
    }
}