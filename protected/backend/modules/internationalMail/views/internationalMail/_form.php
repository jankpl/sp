<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'international-mail-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'sender_country_id'); ?>
		<?php echo $form->dropDownList($model, 'sender_country_id', GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'sender_country_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'receiver_country_id'); ?>
		<?php echo $form->dropDownList($model, 'receiver_country_id', GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'receiver_country_id'); ?>
		</div><!-- row -->

		<div class="row">
		<?php echo $form->labelEx($model,'sender_name'); ?>
		<?php echo $form->textField($model, 'sender_name', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_country'); ?>
		<?php echo $form->textField($model, 'sender_country', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_country'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_zip_code'); ?>
		<?php echo $form->textField($model, 'sender_zip_code', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_zip_code'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_city'); ?>
		<?php echo $form->textField($model, 'sender_city', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_city'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_address_line_1'); ?>
		<?php echo $form->textField($model, 'sender_address_line_1', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_address_line_1'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_address_line_2'); ?>
		<?php echo $form->textField($model, 'sender_address_line_2', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_address_line_2'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_tel'); ?>
		<?php echo $form->textField($model, 'sender_tel', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_tel'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'receiver_name'); ?>
		<?php echo $form->textField($model, 'receiver_name', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'receiver_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'receiver_country'); ?>
		<?php echo $form->textField($model, 'receiver_country', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'receiver_country'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'receiver_zip_code'); ?>
		<?php echo $form->textField($model, 'receiver_zip_code', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'receiver_zip_code'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'receiver_city'); ?>
		<?php echo $form->textField($model, 'receiver_city', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'receiver_city'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'receiver_address_line_1'); ?>
		<?php echo $form->textField($model, 'receiver_address_line_1', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'receiver_address_line_1'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'receiver_address_line_2'); ?>
		<?php echo $form->textField($model, 'receiver_address_line_2', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'receiver_address_line_2'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'receiver_tel'); ?>
		<?php echo $form->textField($model, 'receiver_tel', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'receiver_tel'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'package_weight'); ?>
		<?php echo $form->textField($model, 'package_weight'); ?>
		<?php echo $form->error($model,'package_weight'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'package_size_l'); ?>
		<?php echo $form->textField($model, 'package_size_l'); ?>
		<?php echo $form->error($model,'package_size_l'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'package_size_w'); ?>
		<?php echo $form->textField($model, 'package_size_w'); ?>
		<?php echo $form->error($model,'package_size_w'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'package_size_d'); ?>
		<?php echo $form->textField($model, 'package_size_d'); ?>
		<?php echo $form->error($model,'package_size_d'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->