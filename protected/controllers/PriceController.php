<?php

class PriceController extends Controller {

    public $layout = 'other';

    public function init()
    {
        parent::init();

        $this->layout = 'other';
    }

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('index', 'table', 'calc'),
                'users'=>array('*'),
            ),    
        );
    }

//    public function actionCalc($id)
//    {
//        $id = (int) $id;
//
//        if($id < 1 OR $id > 4)
//            throw new CHttpException(404);
//
//        $page_id = NULL;
//        switch($id)
//        {
//            case 1: $page_id = 11; break;
//            case 2: $page_id = 12; break;
//            case 3: $page_id = 13; break;
//        }
//
//        $this->render('calc',
//            array(
//                'id' => $id,
//                'page_id' => $page_id,
//            ));
//
//    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionAjaxCalculatePrice()
    {

        $data = $_POST;

        $weight_id = $data['weight_id'];
        unset($data['weight_id']);

        $product = $data['product'];
        unset($data['product']);

//        Yii::log('AACP:'.print_r($data,1),CLogger::LEVEL_ERROR);

        echo Pricing::calculatePrice($product,$weight_id,$data);
    }


//    public function actionTable($urlData)
//    {
//        $id = null;
//        switch($urlData)
//        {
//            case 'hm': $id = 10; break;
//            case 'tp' : $id = 9; break;
//            case 'int' : $id = 5; break;
//        }
//
//
//        if($id == NULL)
//            throw new CHttpException(404);
//
//        $criteria = new CDbCriteria();
//        $criteria->condition = 'page_id=:page_id AND language_id=:language_id AND stat = 1';
//        $criteria->params = array(
//            ':page_id'=>$id,
//            ':language_id'=>Yii::app()->params['language'],
//        );
//
//        $model = PageTr::model()->find($criteria);
//
//
//        if($model === NULL)
//            throw new CHttpException('404');
//
//        $this->render('table', array(
//            'model' => $model,
//            'showBanners' => $id==5?true:false,
//        ));
//
//
//
//    }



}