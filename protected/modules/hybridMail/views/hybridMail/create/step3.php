<?php

/* @var $this HybridMailController */
/* @var $modelsAddressData AddressData[] */
/* @var $form CActiveForm */
/* @var $saveToContactBooks bool[] */
/* @var $countries CountryList[] */

?>

<div class="form">

    <h2><?php echo Yii::t('hybridMail','HybridMail');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '3/6')); ?> | <?php echo Yii::t('app','Dane odbiorców'); ?></h2>

    <?php

 $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
      'id' => 'hybrid-mail-form',
     'enableAjaxValidation' => false,
     'enableClientValidation'=>true,
     'clientOptions'=>array(
         'validateOnType'=>true,
         'validationDelay'=> 0.1,
     ),
     'stateful'=>true,
  ));

    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <?php foreach($modelsAddressData as $key => $item): ?>

        <?php $this->renderPartial('create/_receiver', array(
            'model' => $item,
            'form' => $form,
            'i' => $key,
            'saveToContactBook' => $saveToContactBooks[$key],
            'type' => $type,
            'countries' => $countries,
        )); ?>


    <?php endforeach; ?>

    <?php
    /*
    echo CHtml::ajaxButton('Dodaj kolejnego odbiorcę',
        Yii::app()->createUrl('hybridMail/ajaxGenerateReceiverField'), array(
            'data' => 'js: {i : $(".receiver_name").length, form : \''.base64_encode(serialize($form)).'\'}',
            'dataType' => 'json',
            'type' => 'post',
            'success' => 'js:function(result) {
               $("#generateReceiverFieldButton").before(result);
            }'
        ) // ajax
    , array('id' => 'generateReceiverFieldButton', )
    ); // script
    */

    echo TbHtml::submitButton(Yii::t('app','Więcej'),array(
        'name'=>'step4_more',
        'onClick'=>'$(this).closest(\'form\').attr(\'action\', $(this).closest(\'form\').attr(\'action\') + \'#_receiver_\' + ($(".receiver_name").length));',
        'style' => 'margin-left: 340px;',
    ));

    ?>

    <div class="navigate">
    <?php
    echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step2', 'class' => 'btn-small'));
    echo ' ';
    echo TbHtml::submitButton(Yii::t('app','Dalej'),array('name'=>'step4', 'class' => 'btn-primary'));
    echo ' ';
    echo $jumpToEnd? TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'step5', 'class' => 'btn-small')):'';
    $this->endWidget();
    ?>
    </div>

</div><!-- form -->