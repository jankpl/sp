<?php

class F_PostalRoutingExport extends CFormModel
{

    public $country_list_id;
    public $user_group_id;


    public function rules() {
        return array(
            array('country_list_id, user_group_id', 'required'),
            array('user_group_id', 'in', 'range' => CHtml::listData(PostalUserGroup::model()->findAll(),'id', 'id')),
        );
    }

    public function listCountryGroups()
    {
        return CountryGroup::model()->findAll();
    }

    public function afterValidate()
    {
        if($this->user_group_id == -1)
            $this->clearErrors('user_group_id');

        parent::afterValidate();
    }


}