<?php
/* @var $this UserMessageController */
/* @var $model UserMessage */

$this->breadcrumbs=array(
	'User Messages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserMessage', 'url'=>array('index')),
	array('label'=>'Create UserMessage', 'url'=>array('create')),
	array('label'=>'Update UserMessage', 'url'=>array('update', 'id'=>$model->id)),
);
?>

<h1>View UserMessage #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date_entered',
		'date_updated',
		'file_name',
		'text',
		'date',
		'user.login',
		array(
			'name' => 'seen',
			'value' => UserMessage::getStatArray()[$model->stat],
		),
		'no',
	),
)); ?>

<?php if($model->file_name !== NULL): ?>
	<br/>
	<br/>
	<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file', array('/userMessage/getFile', 'id' => $model->id), array('class' => 'btn btn-primary')), array('closeText' => false));?>

<?php endif; ?>



<?php if($model->stat == UserMessage::UNPUBLISHED): ?>
<br/>
<br/>
	<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, CHtml::link('Publish', array('/userMessage/publish', 'id' => $model->id), array('class' => 'btn btn-larget btn-success')), array('closeText' => false));?>

<?php endif; ?>
