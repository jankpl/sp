<?php
/* @var $model ContactForm */
?>
<?php
$this->homePage = true;
$branches = [];
?>

<div class="section-main" id="home-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="home-send">
                    <a href="<?php echo Yii::app()->createUrl('courier/create');?>" id="home-send-top">
                        <p class="top"><?php echo Yii::t('site','Wyślij');?> <?php echo Yii::t('site','paczkę');?></p>
                        <p class="bottom">&gt; <img src="<?= Yii::app()->baseUrl;?>/theme/ruch/images/home-ico-courier.png"/> &gt;</p>
                    </a>
                    <div id="home-send-bottom">
                        <a href="<?php echo Yii::app()->createUrl('/postal/');?>"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-postal.png"/><br/><?php echo Yii::t('site','przesyłkę pocztową');?></a>
                        <?php if(!in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_RUCH, PageVersion::PAGEVERSION_QURIERS, PageVersion::PAGEVERSION_PLI])): ?><a href="<?php echo Yii::app()->createUrl('/palette/');?>"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-palette.png"/><br/><?php echo Yii::t('site','paletę');?></a><?php endif; ?>
                        <?php if(0 && !in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_RUCH, PageVersion::PAGEVERSION_QURIERS, PageVersion::PAGEVERSION_PLI])): ?><a href="<?php echo Yii::app()->createUrl('/hybridMail/');?>"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-hm.png"/><br/><?php echo Yii::t('site','e-list');?></a><?php endif; ?>
                    </div>
                </div>

                <div id="home-right">
                    <div id="main-tt-block" style="position: relative;">
                        <?php
                        if(in_array(Yii::app()->PV->get(), [
                            PageVersion::PAGEVERSION_DEFAULT,
                            PageVersion::PAGEVERSION_RS,
                            PageVersion::PAGEVERSION_DE,
                        ])):
                            ?>
                            <a href="https://carrierlist.17track.net/en/international/100033" target="_blank"><img src="<?= Yii::app()->baseUrl;?>/images/misc/17track.png" style="position: absolute; right: 5px; top: 5px; width: 45%;"/></a>
                        <?php
                        endif;
                        ?>
                        <h2><?= Yii::t('site', 'Śledzenie przesyłki');?></h2>
                        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'tt-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation'=> false,
                            'action' => Yii::app()->urlManager->createUrl('/site/tt'),
                        ));
                        ?>

                        <?php echo CHtml::textField('id', '', ['placeholder' => Yii::t('site', 'Wprowadź nr przesyłki')]); ?>
                        <?php
                        echo TbHtml::submitButton('&gt;');
                        $this->endWidget();
                        ?>

                    </div>

                    <?php
                    if(in_array(Yii::app()->PV->get(), [
                        PageVersion::PAGEVERSION_DEFAULT,
                        PageVersion::PAGEVERSION_RS,
                        PageVersion::PAGEVERSION_PLI,
                        PageVersion::PAGEVERSION_CQL,
                        PageVersion::PAGEVERSION_DE,
                    ])):
                        if(Yii::app()->user->isGuest OR !Yii::app()->user->model->getHidePrices()):
                            Yii::app()->getModule('postal');
                            $countryList = CountryList::getActiveCountries(true);
                            ?>
                            <div id="main-pricing-block">
                                <div id="main-pricing-block-list">
                                    <a href="#" data-pricing-show="courier"><img src="<?= Yii::app()->baseUrl;?>/theme/ruch/images/home-ico-courier.png"/></a>
                                    <a href="#" data-pricing-show="postal"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-postal.png"/></a>
                                    <?php
                                    if(0 && in_array(Yii::app()->PV->get(), [
                                            PageVersion::PAGEVERSION_DEFAULT,
                                            PageVersion::PAGEVERSION_RS,
                                        ])):
                                        ?>
                                        <a href="#" data-pricing-show="hm"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-hm.png"/></a>
                                    <?php
                                    endif;
                                    ?>
                                </div>

                                <?php
                                $force_uid = 0;
                                if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI)
                                    $force_uid = User::PLI_GENERIC_USER_ID;
                                else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                                    $force_uid = User::CQL_GENERIC_USER_ID;
                                ?>


                                <div class="main-pricing-courier main-pricing-type" data-pricing="courier" data-force-uid="<?= $force_uid;?>" data-force-currency="<?= in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_PLI, PageVersion::PAGEVERSION_CQL]) && Yii::app()->user->isGuest ? Currency::EUR_ID : '';?>">
                                    <h2><?= Yii::t('home','Kalkulator ceny');?></h2>
                                    <p class="main-pricing-subtype"><?= Yii::t('home','kurierzy');?></p>
                                    <div class="pricing-labels">
                                        <span><?= Yii::t('home','waga');?> [kg]</span>
                                        <span><?= Yii::t('home','wymiary');?> [cm]</span>

                                    </div>
                                    <div>
                                        <input type="text" id="pricing-courier-weight" class="pricing-courier-weight" data-pricing-input="true" value="2"/>
                                        <input type="text" id="pricing-courier-dim-l" class="pricing-courier-dim" data-pricing-input="true" value="20"/>x
                                        <input type="text" id="pricing-courier-dim-w" class="pricing-courier-dim" data-pricing-input="true" value="20"/>x
                                        <input type="text" id="pricing-courier-dim-d" class="pricing-courier-dim" data-pricing-input="true" value="10"/>
                                    </div>
                                    <div class="pricing-labels">
                                        <span><?= Yii::t('home','kraj nadawcy');?></span>
                                        <span><?= Yii::t('home','kraj odbiorcy');?></span>
                                    </div>
                                    <div>
                                        <?= CHtml::dropDownList('pricing_sender', in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_PLI, PageVersion::PAGEVERSION_CQL]) ? CountryList::COUNTRY_UK : '', $countryList, ['id' => 'pricing-courier-country-sender', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                                        <?= CHtml::dropDownList('pricing_receiver', in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_PLI, PageVersion::PAGEVERSION_CQL]) ? CountryList::COUNTRY_PL : CountryList::COUNTRY_DE, $countryList, ['id' => 'pricing-courier-country-receiver', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                                    </div>

                                    <div class="main-pricing-block-price">
                                        <div class="main-pricing-note">
                                            <?php echo TbHtml::tooltip('[?]', '#', Yii::t('home', 'Cena dotyczy nadania w punkcie/oddziale Świata Przesyłek')); ?>
                                        </div>

                                        <div class="pricing-currency-label"><?= Yii::t('home','cena');?> [<span>-</span>]</div>
                                        <div class="main-pricing-block-price-amount-loading"></div>
                                        <div class="main-pricing-block-price-amount">-</div>
                                        <div class="main-pricing-block-price-amount-brutto">(<span>-</span> <?= Yii::t('home','brutto');?>)</div>
                                        <div class="main-pricing-block-price-go" data-url="<?= Yii::app()->createUrl('/courier/courier/createFP');?>"><?= Yii::t('home','zamów');?></div>
                                    </div>

                                    <a class="main-pricing-block-contact" href="#home-contact-block"><p><?= Yii::t('home','Skontaktuj się z nami, aby uzyskać wycenę!');?><br/><span class="glyphicon glyphicon-envelope"></span></p></a>
                                </div>

                                <div class="main-pricing-postal main-pricing-type" data-pricing="postal" data-force-uid="<?= Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI ? User::PLI_GENERIC_USER_ID : 0;?>" data-force-currency="<?= Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI && Yii::app()->user->isGuest ? Currency::EUR_ID : '';?>">
                                    <h2><?= Yii::t('home','Kalkulator ceny');?></h2>
                                    <p class="main-pricing-subtype"><?= Yii::t('home','przesyłki pocztowe');?></p>
                                    <div class="pricing-labels">
                                        <span><?= Yii::t('home','typ');?></span>
                                        <span><?= Yii::t('home','gabaryt');?></span>
                                        <span><?= Yii::t('home','waga');?></span>
                                    </div>
                                    <div>
                                        <?= CHtml::dropDownList('pricing_postal_type', '', Postal::getPostalTypeList(), ['id' => 'pricing-postal-type', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                                        <?= CHtml::dropDownList('pricing_postal_size', '', Postal::getSizeClassList(), ['id' => 'pricing-postal-size', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                                        <?= CHtml::dropDownList('pricing_postal_weight', '', Postal::getWeightClassList(), ['id' => 'pricing-postal-weight', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                                    </div>
                                    <div class="pricing-labels">
                                        <span><?= Yii::t('home','kraj odbiorcy');?></span>
                                    </div>
                                    <div>
                                        <?= CHtml::dropDownList('pricing_receiver', '', $countryList, ['id' => 'pricing-postal-country-receiver', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                                    </div>

                                    <div class="main-pricing-block-price">
                                        <div class="pricing-currency-label"><?= Yii::t('home','cena');?> [<span>-</span>]</div>
                                        <div class="main-pricing-block-price-amount-loading"></div>
                                        <div class="main-pricing-block-price-amount">-</div>
                                        <div class="main-pricing-block-price-amount-brutto">(<span>-</span> <?= Yii::t('home','brutto');?>)</div>
                                        <div class="main-pricing-block-price-go" data-url="<?= Yii::app()->createUrl('/postal/postal/createFP');?>"><?= Yii::t('home','zamów');?></div>
                                    </div>

                                    <a class="main-pricing-block-contact" href="#home-contact-block"><p><?= Yii::t('home','Skontaktuj się z nami, aby uzyskać wycenę!');?><br/><span class="glyphicon glyphicon-envelope"></span></p></a>
                                </div>
                                <?php
                                Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/dimensionalWeight.js', CClientScript::POS_HEAD);
                                Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/home-pricing.min.js', CClientScript::POS_END);
                                Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
                                Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
                                ?>
                            </div>
                        <?php
                        endif;
                    endif;
                    ?>
                </div>




            </div>
        </div>
    </div>
</div>

<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
    PageVersion::PAGEVERSION_CQL,
    PageVersion::PAGEVERSION_DE,
])):
    ?>

    <div class="section-2">

        <div class="section-2-1">
            <div class="container">
                <h2><?= Yii::t('home','Na skróty:');?></h2>
                <div class="row text-center" id="bottom-menu">

                    <div class="col-md-2 col-sm-4 col-md-offset-1">
                        <a href="<?php echo Yii::app()->createUrl('/specialTransport');?>">
                            <i class="fa fa-truck fa-inverse fa-5x" aria-hidden="true" style="margin-bottom: 10px; margin-top: 10px;"></i>
                            <span><?php echo Yii::t('site', 'przesyłki specjalne');?></span>
                        </a>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <a href="<?php echo Yii::app()->createUrl('/price/');?>">
                            <img src="<?php echo Yii::app()->baseUrl; ?>/images/layout/new/main-submenu-international-prices.png" alt="<?php echo Yii::t('site', 'cennik');?>"/>
                            <span><?php echo Yii::t('site', 'cennik');?></span>
                        </a>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <a href="<?php echo Yii::app()->createUrl('page/view', array('id' => 3));?>">
                            <img src="<?php echo Yii::app()->baseUrl; ?>/images/layout/new/main-submenu-payments.png" alt="<?php echo Yii::t('site', 'płatności');?>"/>
                            <span><?php echo Yii::t('site', 'płatności');?></span>
                        </a>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <a href="<?php echo Yii::app()->createUrl('faq');?>">
                            <img src="<?php echo Yii::app()->baseUrl; ?>/images/layout/new/main-submenu-payments.png" alt="<?php echo Yii::t('site', 'FAQ');?>"/>
                            <span><?php echo Yii::t('site', 'FAQ');?></span>
                        </a>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <a href="<?php echo Yii::app()->createUrl('/site/contact');?>">
                            <i class="fa fa-phone-square fa-inverse fa-5x" aria-hidden="true" style="margin-bottom: 10px; margin-top: 10px;"></i>
                            <span><?php echo Yii::t('site','kontakt');?></span>
                        </a>
                    </div>
                </div>


            </div>
        </div>
        <div class="section-2-2">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div id="home-about-block">
                            <h2><?= Yii::t('home','O nas:');?></h2>
                            <p class="text-justify"><?php
                                $this->widget('ShowPageContent', array('id' => 34));
                                ?>
                            </p>
                            <p class="text-right"><a href="<?= Yii::app()->createUrl('/page/view', array('id' => 1));?>"><?= Yii::t('home','czytaj więcej');?> &gt;</a></p>
                            <?php
                            if(in_array(Yii::app()->PV->get(), [
                                PageVersion::PAGEVERSION_DEFAULT,
                                PageVersion::PAGEVERSION_RS,
                                PageVersion::PAGEVERSION_DE,
                            ])):
                                ?>
                                <div class="home-yt-container">
                                    <iframe src="https://www.youtube.com/embed/vPC5ehXDe1U?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div id="home-contact-block">
                            <h2><?= Yii::t('home','Kontakt:');?></h2>
                            <p class="home-tel"><?= Yii::t('home','tel.:');?> <span class="orange"><?= Yii::t('home','22 122 12 18');?></span></p>
                            <p class="home-email">e-mail: <?= Yii::t('home','info@swiatprzesylek.pl');?></p>

                            <?php if(Yii::app()->user->hasFlash('contact')): ?>
                                <div class="alert alert-success">
                                    <?php echo Yii::app()->user->getFlash('contact'); ?>
                                </div>
                            <?php else: ?>

                                <h2 style="margin: 5px 0 0 0;"><?= Yii::t('home','Zostaw wiadomość:');?></h2>
                                <p style="margin: 0 0 5px 0; font-weight: 300; "><?= Yii::t('home','odpowiemy tak szybko, jak to możliwe');?></p>


                                <div class="form" style="margin-right: 50px; position: relative;">
                                    <?php
                                    /* @var $form TbActiveForm */
                                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                                        'id'=>'home-contact-form',
                                        'enableClientValidation'=>true,
                                        'clientOptions'=>array(
                                            'validateOnSubmit'=>false,
                                        ),
                                    )); ?>
                                    <?php echo $form->errorSummary($model); ?>
                                    <div>
                                        <?php echo $form->textField($model,'name', ['placeholder' => $model->getAttributeLabel('name')]); ?>
                                    </div>
                                    <div>
                                        <?php echo $form->textField($model,'email', ['placeholder' => $model->getAttributeLabel('email')]); ?>
                                    </div>
                                    <div>
                                        <?php echo $form->textArea($model,'body',['placeholder' => $model->getAttributeLabel('body')]); ?>
                                    </div>
                                    <?php if(CCaptcha::checkRequirements()): ?>
                                        <div style="clear: both;">
                                            <?php $this->widget('CCaptcha', array('buttonLabel' => Yii::t('site', 'Załaduj nowy kod'), 'imageOptions' => array('style' => 'margin-right: 5px;'))); ?>
                                        </div>
                                        <div>
                                            <?php echo $form->textField($model,'verifyCode', ['placeholder' => Yii::t('form','Proszę przepisać kod z obrazka.')]); ?>
                                        </div>
                                    <?php endif; ?>
                                    <div>
                                        <?php echo $form->checkbox($model,'accept'); ?> <?= $form->label($model, 'accept');?>
                                    </div>
                                    <?php echo TbHtml::submitButton('&gt', ['class' => 'text-arrow']); ?>
                                    <?php $this->endWidget(); ?>
                                </div><!-- form -->
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-push-6">
                        <div id="home-departments-block">
                            <h2><?= Yii::t('home','Nasze oddziały:');?></h2>
                            <?php
                            $spPoints = SpPoints::getPoints();
                            $no = S_Useful::sizeof($spPoints);

                            $CACHE_NAME = 'SP_POINTS_HOME_'.$no;
                            $branches = Yii::app()->cache->get($CACHE_NAME);

                            if($branches === false)
                            {
                                $branches = [];
                                foreach($spPoints AS $item)
                                {
                                    $address = $item->addressData->address_line_1.' '.$item->addressData->address_line_2.' '.$item->addressData->city.' '.$item->addressData->country0->code;

                                    $addressHuman = $item->addressData->address_line_1;
                                    $addressHuman .= $item->addressData->address_line_2 != '' ? "<br/>".$item->addressData->address_line_2 : '';
                                    $addressHuman .= "<br/>".$item->addressData->zip_code.', '.$item->addressData->city;
                                    $addressHuman .= "<br/>".strtoupper($item->addressData->country0->countryListTr->name);

                                    $address = urlencode($address);
                                    $json = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.GLOBAL_CONFIG::GOOGLE_MAPS_API_KEY);
                                    $obj = json_decode($json);

                                    $lat = $obj->results[0]->geometry->location->lat;
                                    $lng = $obj->results[0]->geometry->location->lng;

                                    $branches[] = [
                                        'id' => $item->id,
                                        'name' => $item->spPointsTr->title,
                                        'lat' => $lat,
                                        'lng' => $lng,
                                        'address' => $addressHuman,
                                        'desc' => $item->spPointsTr->text,
                                    ];
                                }
                                Yii::app()->cache->set($CACHE_NAME, $branches, 60 * 60 * 24);
                            }
                            ?>
                            <?= CHtml::dropDownList('branches', '', CHtml::listData(SpPoints::getPoints(), 'id', 'spPointsTr.title'), ['prompt' => '-wybierz oddział-', 'id' => 'map-sp-point-list', 'style' => 'width: 100%; text-align: center;']); ?>
                            <?php
                            Yii::app()->clientScript->registerScriptFile('//maps.googleapis.com/maps/api/js?key='.GLOBAL_CONFIG::GOOGLE_MAPS_API_KEY, CClientScript::POS_END);
                            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/home-sp-points-map.js', CClientScript::POS_END);
                            ?>
                            <div id="sp-points-map" style="width: 100%; height: 300px; margin: 10px auto;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>



    <div class="section-3">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/v2/stat-1.png" class="img-responsive" style="display: inline-block;"/>
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/v2/stat-2.png" class="img-responsive" style="display: inline-block;"/>
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/v2/stat-3.png" class="img-responsive" style="display: inline-block;"/>
                </div>
            </div>
        </div>

    </div>



    <div class="section-4">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 text-center we-do">
                    <a href="<?= Yii::app()->createUrl('/page/view', ['id' => 20]);?>" class="we-do-item">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/v2/wedo-1.jpg" class="img-responsive"/>
                        <div class="we-do-content">
                            <h2><?= Yii::t('home','Logistyka{br}dla e-commerce', ['{br}' => '<br/>']);?></h2>
                            <p><?= Yii::t('home','Logistyka dla e-commerce - opis');?></p>
                            <p class="we-do-more"><?= Yii::t('home','czytaj więcej');?> &gt;</p>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-3 text-center we-do">
                    <a href="<?= Yii::app()->createUrl('/page/view', ['id' => 15]);?>" class="we-do-item">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/v2/wedo-2.jpg" class="img-responsive"/>
                        <div class="we-do-content">
                            <h2><?= Yii::t('home','Obsługa sklepów internetowych');?></h2>
                            <p><?= Yii::t('home','Obsługa sklepów internetowych - opis');?></p>
                            <p class="we-do-more"><?= Yii::t('home','czytaj więcej');?> &gt;</p>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-3 text-center we-do">
                    <a href="<?= Yii::app()->createUrl('/page/view', ['id' => 6]);?>" class="we-do-item">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/v2/wedo-3.jpg" class="img-responsive"/>
                        <div class="we-do-content">
                            <h2><?= Yii::t('home','Korespondencja masowa');?></h2>
                            <p><?= Yii::t('home','Korespondencja masowa - opis');?></p>
                            <p class="we-do-more"><?= Yii::t('home','czytaj więcej');?> &gt;</p>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-3 text-center we-do">
                    <a href="<?= Yii::app()->createUrl('/page/view', ['id' => 19]);?>" class="we-do-item">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/v2/wedo-4.jpg" class="img-responsive"/>
                        <div class="we-do-content">
                            <h2><?= Yii::t('home','Co-packing');?></h2>
                            <p><?= Yii::t('home','Co-packing - opis');?></p>
                            <p class="we-do-more"><?= Yii::t('home','czytaj więcej');?> &gt;</p>
                        </div>
                    </a>
                </div>

            </div>
        </div>

    </div>



    <div class="section-5">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-push-6">
                    <a href="<?= Yii::app()->createUrl('/user/create');?>" id="home-join-block">
                        <h2><?= Yii::t('home','Dołącz do nas');?></h2>
                        <p style="text-align: justify;"><?php
                            $this->widget('ShowPageContent', array('id' => 35));
                            ?></p>
                        <div class="join-block-arrow text-arrow">&gt;</p>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
<?php
endif;
?>
<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
                map: {
                  img: '.CJSON::encode(Yii::app()->baseUrl.'/images/v2/sp_map_ico.png').',
                  branches: '.CJSON::encode($branches).',
             },
              urls: {
                  getPriceCourier: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxGetPrice')).',
                  getPricePostal: '.CJSON::encode(Yii::app()->urlManager->createUrl('/postal/postal/ajaxGetPrice')).',
                  getPriceHm: '.CJSON::encode(Yii::app()->urlManager->createUrl('/hybridMail/hybridMail/ajaxGetPrice')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).'
              },
              pv: '.CJSON::encode(Yii::app()->PV->get()).'
          };
      ',CClientScript::POS_HEAD);
?>



