<?php

class F_UploadFile extends CFormModel {

    public $file;
    public $omitFirst;
    public $split;


    const SPLIT_FULL_EDIT = 1;
    const SPLIT_DEFAULT = 0;

    const SPLIT_AFTER_FULL_EDIT = 30;
    const SPLIT_AFTER_DEFAULT = 150;

    const SPLIT_AFTER_FULL_EDIT_STAT = 250;
    const SPLIT_AFTER_DEFAULT_STAT = 900;

    public static $_splitArray = array(
        self::SPLIT_DEFAULT =>  '150 lines',
        self::SPLIT_FULL_EDIT => '30 lines - full edit',
    );

    public static $_splitArrayStat = array(
        self::SPLIT_DEFAULT =>  '900 lines',
        self::SPLIT_FULL_EDIT => '250 lines - full edit',
    );



    public function rules() {
        return array(
            array('file', 'file',  'allowEmpty' => false,
                'maxSize' => 512400, 'types' => 'csv, xls, xlsx'),
            array('omitFirst', 'safe'),
            array('split', 'numerical'),
        );
    }

    public function attributeLabels() {
        return array(
            'file' => Yii::t('app', 'File'),
            'omitFirst' => Yii::t('app', 'Omit first line (header)'),
            'split' => Yii::t('app', 'Split after'),
        );
    }

}