<?php

Yii::import('application.modules.courier.models._base.BaseCourierUOperator2CountryGroup');

class CourierUOperator2CountryGroup extends BaseCourierUOperator2CountryGroup
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function setCourierUOperatorId($courier_u_operator_id)
    {
        $this->courier_u_operator_id = $courier_u_operator_id;
    }

    public static function getStatList()
    {
        return [
            0 => 'not active',
            1 => 'active'
        ];
    }

    public function getStatName()
    {
        return isset(self::getStatList()[$this->stat]) ? self::getStatList()[$this->stat] : '';
    }

    public static function getCodList()
    {
        return [
            0 => 'not active',
            1 => 'active'
        ];
    }

    public function getCodName()
    {
        return isset(self::getCodList()[$this->cod]) ? self::getCodList()[$this->cod] : '';
    }

    /**
     * @return UserGroup[]
     */
    public function getUnunsedUserGroups()
    {
        $models = UserGroup::model()->forThisAdmin()->findAll([
            'condition' => 't.id NOT IN (SELECT DISTINCT user_group_id FROM ('.(new CourierUPriceGroup())->tableName().') WHERE courier_u_2_operator_country_group_id = :courier_u_2_operator_country_group_id AND user_group_id IS NOT NULL)',
            'params' => [
                ':courier_u_2_operator_country_group_id' => $this->id
            ]]);

        return $models;
    }

    public function saveWithPrice($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $price_id = $this->price->saveWithPrices();

        if(!$price_id)
            $errors = true;

        $this->cod_price_id = $price_id;

        if(!$this->save())
            $errors = true;

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

    public function listCodPricesInline()
    {
        if($this->price)
        {
            return $this->price->getValue(Currency::PLN_ID)->value.'/'.$this->price->getValue(Currency::EUR_ID)->value.'/'.$this->price->getValue(Currency::GBP_ID)->value;
        } else
            return '-';
    }

    /**
     * @param $operator_id
     * @param $country_id
     * @return self|null
     */
    public static function findForOperatorAndCountryId($operator_id, $country_id)
    {
        $country_group_id = CourierCountryGroup::getGroupForCountryId($country_id);

        $model = self::model()->findByAttributes([
            'courier_country_group_id' => $country_group_id,
            'courier_u_operator_id' => $operator_id
        ]);

        return $model;
    }
}