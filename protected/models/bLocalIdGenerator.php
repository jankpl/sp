<?php

class bLocalIdGenerator extends bBaseBehavior
{
    protected $_allowedClass = array('Courier', 'HybridMail', 'InternationalMail', 'Postal');

    protected function getTable()
    {
        $table = NULL;

        switch($this->getClass())
        {
            case 'Courier':
                $table = 'courier';
                break;
            case 'HybridMail':
                $table = 'hybrid_mail';
                break;
            case 'InternationalMail':
                $table = 'international_mail';
                break;
            case 'Postal':
                $table = 'postal';
                break;
        }

        return $table;
    }

    protected function getIdPrefix()
    {
        $prefix = NULL;

        switch($this->getClass())
        {
            case 'Courier':
                $prefix = 1;
                break;
            case 'HybridMail':
                $prefix = 2;
                break;
            case 'InternationalMail':
                $prefix = 3;
                break;
            case 'Postal':
                $prefix = 4;
                break;
        }

        return $prefix;
    }

    public function beforeSave($event)
    {
        if ($this->owner()->isNewRecord)
        {
            if($this->owner()->local_id == '')
                $this->owner()->local_id = $this->generateLocalId();

            $this->owner()->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => $this->owner()->user_id.$this->owner()->local_id.$this->owner()->date_entered.microtime()));
        }
    }

    protected function _random_key($length,$base) {
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $key = '';
        for ( $i=0; $i<$length; $i++ ) {
            $key .= $chars[ (mt_rand( 0, ($base-1) ))];
        }
        return $key;
    }

    protected function generateLocalId()
    {
        $is_it_unique = false;

        $today = date('ymd');

        $local_id = null;
        while(!$is_it_unique)
        {
            $local_id = $this->getIdPrefix().$today.$this->_random_key(5,10);

            $cmd = Yii::app()->db->createCommand();
            $cmd->select = 'COUNT(*)';
            $cmd->from = $this->getTable();
            $cmd->where = "local_id = :local_id";
            $cmd->bindParam(':local_id', $local_id);
            $num = $cmd->queryScalar();

            if($num == 0)
                $is_it_unique = true;
        }

        return $local_id;
    }

}
