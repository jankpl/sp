<?php

$this->breadcrumbs = array(
	UserContactBook::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . UserContactBook::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . UserContactBook::label(2), 'url' => array('admin')),
);
?>

<h2><?php echo GxHtml::encode(UserContactBook::label(2)); ?></h2>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
?>

<?php $this->widget('BackLink');?>