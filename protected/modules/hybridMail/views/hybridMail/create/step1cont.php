<?php

/* @var $this HybridMailController */
/* @var $model HybridMail */
/* @var $form CActiveForm */
/* @var $saveToContactBook bool */
/* @var $countries CountryList[] */

?>


<div class="form">

    <h2><?php echo Yii::t('hybridMail','HybridMail');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '2/6')); ?> | <?php echo Yii::t('app','Dane nadawcy'); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'address-data',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));
    ?>

    <div class="text-center">

        <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('hm', 'Liczba stron w Twoim dokumencie: ').$model->pages, array('closeText' => false)); ?>

        <a href="<?php echo Yii::app()->createUrl('/hybridMail/hybridMail/getTmpFile', array('urlData' => $model->file_hash, 'e' => $model->_file_ext));?>" target="_blank" class="btn btn-lg btn-success"><?php echo Yii::t('hm', 'Zweryfikuj plik');?></a>

    </div>
    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step1', 'class' => 'btn-small'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Dalej'),array('name'=>'step2', 'class' => 'btn-primary'));
        echo ' ';
        echo $jumpToEnd? TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'step5', 'class' => 'btn-small')):'';
        $this->endWidget();
        ?>
    </div>

</div><!-- form -->