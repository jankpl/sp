<div id="upload-form">
    <div class="form">
        <?php

        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
            array(
                'enableAjaxValidation' => false,
                'htmlOptions' =>
                    array('enctype' => 'multipart/form-data'),
            )
        );
        ?>

        <h2><?php echo Yii::t('courier','Import packages from file');?></h2>

        <?php
        $this->widget('FlashPrinter');
        ?>

        <?php
        echo $form->errorSummary($model);
        ?>

        <table class="table table-striped" style="margin: 10px auto;">
            <tr>
                <td style="width: 300px;"><?php echo $form->labelEx($model,'file', ['style' => 'width: 100%;']); ?></td>
                <td><?php echo $form->fileField($model, 'file'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'omitFirst', ['style' => 'width: 100%;']); ?></td>
                <td><?php echo $form->checkBox($model, 'omitFirst'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'ooe_operator_id', ['style' => 'width: 100%;']); ?></td>
                <td><?php echo $form->dropDownList($model, 'ooe_operator_id', (new CourierTypeOoe())->getServiceList(true,false), array('prompt' => Yii::t('courier', 'Wybierz operatora'))); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'custom_settings', ['style' => 'width: 100%;']); ?></td>
                <td>
                    <?php echo $form->dropdownList($model,'custom_settings', CHtml::listData(CourierImportSettings::listModelsForUserId(Yii::app()->user->id),'id','name'), ['id' => 'custom-settings', 'prompt' => '-']); ?> | <input type="button" class="btn btn-xs btn-default" id="open-settings-modal" data-settings-modal="edit" value="<?= Yii::t('courier', 'Customize settings');?>" style="display: none;"/> <input type="button" data-settings-modal="new" class="btn btn-xs btn-info" id="new-settings-modal" value="<?= Yii::t('courier', 'Add new settings');?>"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><?php echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('class' => 'btn btn-lg btn-primary'));?></td>
            </tr>
        </table>
        <?php
        $this->endWidget();
        ?>

        <div class="info">
            <div class="alert alert-info">
                <?php echo Yii::t('courier', 'Maksymalna liczba wierszy w pliku to: {number1}. Maksymalna liczba paczek w pliku to: {number2}.', array('{number1}' => '<strong>'.F_OoeImport::MAX_LINES.'</strong>', '{number2}' => '<strong>'.F_OoeImport::MAX_PACKAGES.'</strong>')); ?>
            </div>
            <p><?php echo Yii::t('courier','Allowed file type:');?> .csv,.xls,.xlsx.</p>
            <br/>
            <p><?php echo Yii::t('courier','The file must contain following columns:');?><br/>
                [package_weight],
                [package_size_l],
                [package_size_w],
                [package_size_d],
                [package_value],
                [package_content],
                [sender_name],
                [sender_company],
                [sender_zip_code],
                [sender_city],
                [sender_address_line_1],
                [sender_address_line_2],
                [sender_tel],
                [sender_email],
                [receiver_name],
                [receiver_company],
                [receiver_zip_code],
                [receiver_city],
                [receiver_address_line_1],
                [receiver_address_line_2],
                [receiver_tel],
                [receiver_email],
                [ref],
                [options]
            </p>
            <br/>
            <?php echo CHtml::link(Yii::t('courier','Download file template'), Yii::app()->baseUrl.'/misc/ipff_template_ooe.xlsx', array('target' => '_blank', 'class' => 'btn btn-success', 'download' => 'ipff_template_ooe.xlsx'));?>
            <br/>
            <br/>
            <div class="alert alert-info">
                <?php echo Yii::t('courier', 'W {a}ustawieniach profilu{/a} istnieje możliwość wyłączenia powiadomień SMS oraz email.', array('{a}' => '<a href="'.Yii::app()->createUrl('/user/update').'#block-notify-form" target="_blank">', '{/a}' => '</a>')); ?>
            </div>
        </div>
    </div>
</div>
<?php
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/custom-theme/jquery-ui-1.10.3.custom.min.css');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courier-import-settings.js');

$script = "prepareSettingsModal({
        textConfirmDelete: '".Yii::t('courier', 'Na pewno?')."',
        textError: '".Yii::t('courier', 'Wystąpił bład zapisywania. Spróbuj ponownie.')."',
        ajaxUrl: '".Yii::app()->urlManager->createUrl('/courier/courier/ajaxImportModalSettings')."',
        type: '".CourierImportSettings::TYPE_OOE."',
    });";
Yii::app()->clientScript->registerScript('courier-import-settings', $script);
?>