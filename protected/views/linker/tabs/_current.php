<h3><?= Yii::t('linkerManager', 'Twoje integracje');?></h3>

<?php
$this->widget('ShowPageContent', array('id' => 75));
?>
<br/>
<table class="table table-striped table-bordered table-condensed"  style="max-width: 500px;">
    <tr>
        <td>#</td>
        <th><?= Yii::t('linkerManager', 'Adapter');?></th>
        <th><?= Yii::t('linkerManager', 'Nazwa');?></th>
        <th><?= Yii::t('linkerManager', 'Data utworzenia');?></th>
    </tr>
    <?php
    $myIntegrations = $lm->listMyIntegrations();
    if(!S_Useful::sizeof($myIntegrations)):
        ?>
        <tr>
            <td colspan="4" class="text-center"><?= Yii::t('linkerManager', '-- brak --');?></td>
        </tr>
    <?php
    else:
        $i = 1;
        foreach($myIntegrations AS $item):
            ?>
            <tr>
                <td><?= $i;?></td>
                <td><?= $item->adapter;?></td>
                <td><?= $item->name;?></td>
                <td><?= $item->createdAt;?></td>
            </tr>
            <?php
            $i++;
        endforeach;
    endif;
    ?>
</table>