<?php
/* @var $id_prefix string */
?>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-labels',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/generateLabelsByGridView'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php echo CHtml::dropDownList('Courier[type]','',LabelPrinter::getLabelGenerateTypes(), array('style' => 'width: 120px; display: inline-block;'));?>

<?php
echo TbHtml::submitButton(Yii::t('courier','Generate labels'), array(
    'onClick' => 'js:
    $("#'.$id_prefix.'courier-ids-labels").val($("#couriergrid").selGridViewNoGet("getAllSelection").toString());
    ;',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => $id_prefix.'courier-ids-labels')); ?>
<?php
$this->endWidget();
?>
<br/>
<small><?= Yii::t('courier', 'Packages without labels or damaged labels will be omitted.', ['{type}' => Courier::getTypesNames()[Courier::TYPE_INTERNAL]]);?></small>
