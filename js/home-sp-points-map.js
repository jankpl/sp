(function($) {

    var map;
    var dymek = new google.maps.InfoWindow();
    var markersArray = [];
    var spPointSelector;

    function addMarker(lat,lng,txt,id)
    {

        var img = {
            url: yii.map.img,
            size: new google.maps.Size(32,32),
            orgin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(16,32)
        };

        var markerOptions =
            {
                position: new google.maps.LatLng(lat,lng),
                map: map,
                icon: img,
                animation: google.maps.Animation.DROP
            };

        var marker = new google.maps.Marker(markerOptions);
        marker.txt=txt;

        google.maps.event.addListener(marker,'click',function()
        {
            dymek.setContent(marker.txt);
            dymek.open(map,marker);
        });
        markersArray[id] = marker;

        return marker;
    }

    function mapStart()
    {

        var mapOptions =
            {
                center: new google.maps.LatLng(52.887719, 21.390474),
                zoom: 4,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                scaleControl: true,
                scrollwheel: false
            };
        map = new google.maps.Map(document.getElementById('sp-points-map'), mapOptions);

        spPointSelector = $('#map-sp-point-list');

        spPointSelector.on('change', onSelectPoint);

    }

    function mapAddPoints()
    {
        $.each(yii.map.branches, function(i,v){
            addMarker(v.lat, v.lng,'<strong>' + v.name + '</strong><br />' + v.desc + '<br/><br/>' + v.address, v.id);
        })
    }

    function onSelectPoint()
    {

        var markerId = $(this).val();

        if(markerId == '')
        {
            map.panTo(new google.maps.LatLng(52.887719, 21.390474));
            map.setZoom(4);


        } else {
            var marker = markersArray[markerId];

            map.panTo(marker.getPosition());
            map.setZoom(14);
            google.maps.event.trigger(marker,'click');

            spPointSelector.unbind('change');
            spPointSelector.val(markerId);
            spPointSelector.on('change', onSelectPoint);
        }

    }


    mapStart();
    mapAddPoints();


})(jQuery);