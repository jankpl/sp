<?php

class ManifestQuequeController extends Controller
{

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_ADMIN.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }


    function actionIndex()
    {
        $model = new ManifestQueque('search');
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['ManifestQueque']))
            $model->attributes = $_GET['ManifestQueque'];

        $this->render('index',[
            'model' => $model,
        ]);
    }




}