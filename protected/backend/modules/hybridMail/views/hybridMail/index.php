<?php

$this->breadcrumbs = array(
    HybridMail::label(2),
    Yii::t('app', 'Index'),
);

$this->menu = array(
    array('label'=>'Manage' . HybridMail::label(2), 'url' => array('admin')),
);
?>

    <h1><?php echo GxHtml::encode(HybridMail::label(2)); ?></h1>

<?php echo CHtml::link('manage',array('/hybridMail/hybridMail/admin'), array('class' => 'likeButton')); ?>
    <br/><br/>
