<h1>Matrix test</h1>
<div class="row">
    <div class="col-xs-12 col-md-6">
        <h2>New request</h2>
        <form method="POST">
            <input type="text" name="test" placeholder="Variable value"/>
            <input type="submit" name="SEND"/>
        </form>


    </div>

    <div class="col-xs-12 col-md-6">
        <h2>Response</h2>

        <?php
        if($data):
            ?>
            <h2>Data sent:</h2>


            <textarea style="width: 100%; height: 100px;"><?= $data;?></textarea>
        <?php
        endif;
        ?>

        <h2>Response value:</h2>
        <textarea style="width: 100%; height: 500px;"><?= $response;?></textarea>
    </div>
</div>

