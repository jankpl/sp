<?php

/**
 * This is the model base class for the table "courier_domestic_addition_list".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CourierDomesticAdditionList".
 *
 * Columns in table "courier_domestic_addition_list" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property string $date_entered
 * @property string $date_updated
 * @property string $name
 * @property integer $stat
 * @property integer $broker_id
 * @property integer $group
 *
 * @property CourierDomesticAdditionListTr[] $courierDomesticAdditionListTrs
 * @property CourierDomesticAdditionListTr $courierDomesticAdditionListTr
 */
abstract class BaseCourierDomesticAdditionList extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'courier_domestic_addition_list';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CourierDomesticAdditionList|CourierDomesticAdditionLists', $n);
	}

	public static function representingColumn() {
		return 'date_entered';
	}

	public function rules() {
		return array(
			array('id, date_entered, name, stat', 'required'),
			array('id, stat', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('date_updated', 'safe'),
			array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, date_entered, date_updated, name, stat', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'courierDomesticAdditionListTrs' => array(self::HAS_MANY, 'CourierDomesticAdditionListTr', 'courier_domestic_addition_list_id'),
			'courierDomesticAdditionListTr' => array(self::HAS_ONE, 'CourierDomesticAdditionListTr', 'courier_domestic_addition_list_id', 'on'=>'courierDomesticAdditionListTr.language_id='.Yii::app()->params['language']),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'date_entered' => Yii::t('app', 'Date Entered'),
			'date_updated' => Yii::t('app', 'Date Updated'),
			'name' => Yii::t('app', 'Name'),
			'stat' => Yii::t('app', 'Stat'),
			'broker_id' => Yii::t('app', 'Courier Domestic Broker'),
			'group' => Yii::t('app', 'Group'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('date_entered', $this->date_entered, true);
		$criteria->compare('date_updated', $this->date_updated, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('stat', $this->stat);
		$criteria->compare('broker_id', $this->broker_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}