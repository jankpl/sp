<?php
/* @var $id_prefix string */
?>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-ack-card',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/ackCardMulti'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
    <?php
    echo TbHtml::submitButton(Yii::t('postal', 'Generate ACK card'), array(
        'onClick' => 'js:
    $("#'.$id_prefix.'postal-ids-ack").val($("#postalgrid").selGridViewNoGet("getAllSelection").toString());
    ',
        'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
    ?>

    <?php echo CHtml::hiddenField('Postal[ids]','', array('id' => $id_prefix.'postal-ids-ack')); ?>
<?php
$this->endWidget();
?>
