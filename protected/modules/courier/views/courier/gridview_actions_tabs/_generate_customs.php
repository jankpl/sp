<?php
/* @var $id_prefix string */
?>
    <br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-customs',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/customsPacketMulti'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('courier', 'Generuj dokumenty celne'), array(
    'onClick' => 'js:
    $("#'.$id_prefix.'courier-ids-customs").val($("#couriergrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => $id_prefix.'courier-ids-customs')); ?>
<?php
$this->endWidget();
?>