<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label' =>'List ' . $model->label(2), 'url'=>array('index')),
    array('label' =>'Create ' . $model->label(), 'url'=>array('create')),
    array('label' =>'View ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
);
?>

    <h1>Update <?php echo GxHtml::encode($model->label()) . ' "' . GxHtml::encode(GxHtml::valueEx($model)); ?>"</h1>

<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'modelTrs' => $modelTrs,
));
?>