<?php
/* @var $this PrintServerController */
/* @var $model PrintServer */

$this->breadcrumbs=array(
    'Print Servers'=>array('index'),
    'Manage',
);

?>

<h1>Manage Print Servers</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'print-server-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'date_entered',
        'date_updated',
        array(
            'name' => 'user_id',
            'value' => '$data->user->login',
            'filter' => CHtml::listData(User::model()->findAll(),'id', 'login'),
        ),
        [
            'name' => 'stat',
            'value' => '$data->statName',
            'filter' => PrintServer::getStatList(),
        ],
        'address_ip',
        'address_port',
        'no',
        'is_default',
//		'verify_key',
		'verify_counter',
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}',
            'htmlOptions'=> array('style'=>'width:80px'),
        ),
	),
)); ?>
