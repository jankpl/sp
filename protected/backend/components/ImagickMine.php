<?php

class ImagickMine
{

    private static $_instance;

    private function __construct() {}
    private function __clone() {}

    /**
     * @return Imagick
     */
    public static function newInstance()
    {
        // file_put_contents('abc.txt', "W\r\n", FILE_APPEND);

        if(self::$_instance === null) {
            self::$_instance = new Imagick();

            // 1 MB:
            // self::$_instance->setResourceLimit(imagick::RESOURCETYPE_MEMORY, 1048576); // Set maximum amount of memory in bytes to allocate for the pixel cache from the heap. When this limit is exceeded, the image pixels are cached to memory-mapped disk (see MAGICK_MAP_LIMIT).

            // 1 MB:
            // self::$_instance->setResourceLimit(imagick::RESOURCETYPE_MAP, 1048576); // Set maximum amount of memory map in bytes to allocate for the pixel cache. When this limit is exceeded, the image pixels are cached to disk (see MAGICK_DISK_LIMIT).

            // 800 x 1400:
            // self::$_instance->setResourceLimit(imagick::RESOURCETYPE_AREA, 2120000); // Set the maximum width * height of an image that can reside in the pixel cache memory. Images that exceed the area limit are cached to disk (see MAGICK_DISK_LIMIT) and optionally memory-mapped.
            // self::$_instance->setResourceLimit(imagick::RESOURCETYPE_FILE, 768); // Set maximum number of open pixel cache files. When this limit is exceeded, any subsequent pixels cached to disk are closed and reopened on demand. This behavior permits a large number of images to be accessed simultaneously on disk, but with a speed penalty due to repeated open/close calls.
            // self::$_instance->setResourceLimit(imagick::RESOURCETYPE_DISK, -1); // Set maximum amount of memory map in bytes to allocate for the pixel cache. When this limit is exceeded, the image pixels are cached to disk (see MAGICK_DISK_LIMIT).
//            self::$_instance->setResourceLimit(6,1);

            // file_put_contents('abc.txt', "I\r\n", FILE_APPEND);
        }
        else
        {
            self::$_instance->clear();
        }

        return self::$_instance;

    }
}
