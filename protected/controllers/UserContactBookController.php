<?php

class UserContactBookController extends Controller {



    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }


    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','view', 'create','update', 'admin','delete', 'ajaxGetData', 'createFromFile', 'ajaxSuggestItems', 'import', 'ajaxRemoveRowOnImport', 'ajaxUpdateRowItemOnImport'),
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionAjaxSuggestItems()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $countries = CJSON::decode($_POST['countries']);
            $searchFor = $_POST['term'];
            $searchFor = trim($searchFor);
            $type = $_POST['type'];

            if (strlen($searchFor) < 3)
                echo CJSON::encode('');

            echo S_Useful::listDataForAutocomplete(UserContactBook::listContactBookItems($type, $countries, $searchFor), 'id', 'RepresentingName', 'RepresentingNameForSearch');
        }
    }

    public function actionAjaxGetData($id)
    {

        $model = UserContactBook::model()->find(array('select' => 'address_data_id',
            'condition' => 'id=:id AND user_id=:user_id',
            'params' => array(':id' => $id,':user_id' => Yii::app()->user->id)));

        /* @var $model AddressData_ContactBook */
        $model = AddressData_ContactBook::model()->find(array('select' => 'id, name, company, country_id, zip_code, city, address_line_1, address_line_2, tel, email, params',
            'condition' => 'id=:id',
            'params' => array(':id' => $model->address_data_id)));

        $attributes = $model->attributes;
        $attributes[AddressData::PARAMS_BANK_ACCOUNT] = $model['params'][AddressData::PARAMS_BANK_ACCOUNT];
        $attributes[AddressData::PARAMS_SMS_NOTIFICATION] = $model['params'][AddressData::PARAMS_SMS_NOTIFICATION];
        unset($attributes['params']);

        echo CJSON::encode($attributes);
    }

    public function actionView($id) {

        $this->panelLeftMenuActive = 800;

        $this->render('view', array(
            'model' => $this->loadModel($id, 'UserContactBook'),
        ));
    }

    public function actionCreate() {

        $this->panelLeftMenuActive = 802;

        Yii::app()->getModule('courier');

        $model = new UserContactBook;
        $model2 = new AddressData_ContactBook();

        $this->performAjaxValidation($model, 'user-contact-book-form');
        $this->performAjaxValidation($model2, 'user-contact-book-form');

        if (isset($_POST['UserContactBook'])) {
            $model->setAttributes($_POST['UserContactBook']);
            $model2->setAttributes($_POST['AddressData_ContactBook']);

            $model2->setBankAccount($_POST['AddressData_ContactBook']['bankAccount']);

            $model->addressData = $model2;
            $model->validateWithAddressData();


            if($model->saveWithAddressData())
            {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }
        $this->render('create', array('model' => $model, 'model2' => $model2));
    }

    public function actionUpdate($id) {
        $this->panelLeftMenuActive = 800;

        $model = $this->loadModel($id, 'UserContactBook');

        if($model->user_id != Yii::app()->user->id)
            throw new CHttpException(403);


        $model2 = $this->loadModel($model->address_data_id, 'AddressData_ContactBook');

        $this->performAjaxValidation($model, 'user-contact-book-form');
        $this->performAjaxValidation($model2, 'user-contact-book-form');

        if (isset($_POST['UserContactBook'])) {
            $model->setAttributes($_POST['UserContactBook']);
            $model2->setAttributes($_POST['AddressData_ContactBook']);

            $model->addressData = $model2;
            $model->validateWithAddressData();

            if($model->saveWithAddressData())
            {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'model2' => $model2,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {


            $model = $this->loadModel($id, 'UserContactBook');

            if($model->user_id != Yii::app()->user->id)
                throw new CHttpException(403);

            $model->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {
        $this->redirect(array('userContactBook/admin'));
    }


    public function actionAdmin() {

        $this->panelLeftMenuActive = 801;

        $model = new UserContactBook('search');
        $model->unsetAttributes();

        if($model->addressData!== NULL)
            $model->addressData->unsetAttributes();

        if (isset($_GET['UserContactBook']))
            $model->setAttributes($_GET['UserContactBook']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }




    public function actionImport($hash = NULL)
    {
        $this->panelLeftMenuActive = 803;


        $fileModel = new F_ContactBookImport();


        if($hash !== NULL)
        {
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
            Yii::import('zii.behaviors.CTimestampBehavior');


            $models = Yii::app()->cache->get('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID);

            if(isset($_POST['save']))
            {

                if(F_ContactBookImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
                    Yii::app()->cache->set('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                    $this->refresh();
                    exit;
                }

                try
                {
                    $result = UserContactBook::saveWholeGroup($models);
                }
                catch (Exception $ex) {
                    Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);
                    return;
                }

                if($result['success'])
                {

                    Yii::app()->cache->delete('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID);
                    Yii::app()->user->setFlash('success', Yii::t('site', 'Adresy zostały zaimportowane!'));
                    $this->redirect(Yii::app()->createUrl('/userContactBook/admin'));
                    Yii::app()->end();
                } else {
                    Yii::app()->cache->delete('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID);
                    Yii::app()->user->setFlash('error', Yii::t('site', 'Żadne adres nie został zaimporotowany!'));
                    $this->redirect(Yii::app()->createUrl('/userContactBook/import'));
                    Yii::app()->end();
                }

            } else {

                if (!$models OR !S_Useful::sizeof($models)) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Import nie powiódł się!'));
                    $this->redirect(Yii::app()->createUrl('/userContactBook/import/'));
                }

                $this->render('import/showModels', array(
                    'models' => $models,
                    'hash' => $hash,
                ));
                return;
            }
        }


        // file upload
        if(isset($_POST['F_ContactBookImport']) ) {
            $fileModel->setAttributes($_POST['F_ContactBookImport']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if (!$fileModel->validate()) {
                $this->render('import/index', array(
                    'model' => $fileModel,
                ));
                return;
            } else {
                // file upload success:


                // generate new hash
                $hash = hash('sha256', time() . Yii::app()->user->id);

                $inputFile = $fileModel->file->tempName;

//                Yii::import('application.extensions.phpexcel.XPHPExcel');
//                XPHPExcel::init();

                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFile);


                // WITH FIX:
                if(strtoupper($inputFileType) == 'CSV')
                {
                    // SET UTF8 ENCODING IN CSV
                    $file_content = file_get_contents( $inputFile );

                    $file_content = S_Useful::forceToUTF8($file_content);
                    file_put_contents( $inputFile, $file_content );

                    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                    $objReader->setDelimiter(',');

                    // FIX ENCODING

//                    if($fileModel->_customSeparator)
//                        $objReader->setDelimiter($fileModel->_customSeparator);


                } else {
                    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                }
//                $objReader->setReadDataOnly(true);

//                $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
//                $cacheSettings = array( ' memoryCacheSize ' => '128MB');
//                PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

                // WITHOUT FIX:
//
//                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//                if(strtoupper($inputFileType) == 'CSV' && $fileModel->_customSeparator)
//                {
//                    $objReader->setDelimiter($fileModel->_customSeparator);
//                }


                 $objPHPExcel = $objReader->load($inputFile);

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $data = [];
                $start = 1;

                if ($fileModel->omitFirst) {
                    $headerSeen = false;
                } else
                    $headerSeen = true;

                for ($row = $start; $row <= $highestRow; $row++) {
                    if (!$headerSeen) {
                        $headerSeen = true;
                        continue;
                    }

                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                    array_push($data, $rowData[0]);
                }

                // remove empty lines:
                $data = (array_filter(array_map('array_filter', $data)));

                $numberOfLines = S_Useful::sizeof($data);

                // check just by lines number
                if($numberOfLines > F_ContactBookImport::MAX_LINES)
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość wierszy w pliku to: {no}! Naliczono: {number}', array('{no}' => F_ContactBookImport::MAX_LINES, '{number}' => $numberOfLines)));
                    $this->refresh(true);
                    exit;
                }


                $models = F_ContactBookImport::mapAttributesToModels($data);
                F_ContactBookImport::validateModels($models);

                Yii::app()->cache->set('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                $this->redirect(Yii::app()->createUrl('/userContactBook/import', ['hash' => $hash]));
                exit;
            }
        }

        $this->render('import/index', array(
            'model' => $fileModel,
        ));

    }

    public function actionAjaxRemoveRowOnImport()
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        // from file import
        $models = Yii::app()->cache->get('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID);
        unset($models[$id]);

        Yii::app()->cache->set('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);


        echo CJSON::encode(array('success' => true));
    }


    public function actionAjaxUpdateRowItemOnImport()
    {
        if(Yii::app()->request->isAjaxRequest) {


            $hash = $_POST['hash'];
            $i = $_POST['i'];
            $attribute = $_POST['attribute'];
            $val = $_POST['val'];

            $possibleToEdit = [
                'addressData.name',
                'addressData.company',
                'addressData.zip_code',
                'addressData.address_line_1',
                'addressData.address_line_2',
                'addressData.city',
                'addressData.tel',
                'addressData.email',
                'type',
            ];

            if(!in_array($attribute, $possibleToEdit))
                return false;

            try {
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
                Yii::import('zii.behaviors.CTimestampBehavior');


                // from file import
                $models = Yii::app()->cache->get('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID);

                /* @var $model UserContactBook */
                if (is_array($models)) {
                    $model = $models[$i];

                    $submodel = false;
                    if ($model) {

                        $attribute = explode('.', $attribute);
                        if(S_Useful::sizeof($attribute) > 1)
                        {
                            $submodel = $attribute[0];

                            // $model = $model->$attribute[0];
                            // PHP7 fix
                            $temp = $attribute[0];
                            $model = $model->$temp;

                            $attribute = $attribute[1];
                        } else
                            $attribute = $attribute[0];


                        if ($model->hasAttribute($attribute)) {
                            $backupValue = $model->$attribute;
                            $model->$attribute = $val;

                            $model->validate([$attribute]);
                            if (!$model->hasErrors($attribute)) {

                                if($submodel)
                                    $models[$i]->$submodel = $model;
                                else
                                    $models[$i] = $model;


                                Yii::app()->cache->set('contactBookImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                                echo CJSON::encode(['success' => true]);
                                return;

                            } else {

                                echo CJSON::encode([
                                    'success' => false,
                                    'msg' => strip_tags($model->getError($attribute)),
                                    'backup' => $backupValue,
                                ]);
                                return;
                            }
                        }
                    }
                }

                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie. {msg}', ['{msg}' => print_r($attribute,1)])
                ]);
                return;

            } catch (Exception $ex) {
                Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);
                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie')
                ]);
                return;

            }
        }
    }

}