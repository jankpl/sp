<?php

class OneWorldExpress extends CComponent
{
    protected $location;
    protected $url;
    protected $password;
    protected $username;
    protected $user_id;

    public function init() {

        Yii::import('application.models.OneWorldExpressPackage');
        //require_once Yii::getPathOfAlias( 'application.components.oneWorldExpress.OneWorldExpressPackage' ) . '.php';

        $this->location = "https://oneworldexpress.co.uk/remote/main/smartsystemintegration.php?wsdl";
        $this->url = "https://oneworldexpress.co.uk/remote/main/index.php";
    }

    protected static function generateString(array $data)
    {
        $string = implode($data, '||');
        return $string;
    }

    protected function initSoap()
    {
        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);

        $client = new SoapClient(
            null,
            array('location' => $this->location,
                'uri' => $this->url,
                'soap_version' => SOAP_1_1,
                'stream_context' => $context
            )
        );

        return $client;
    }

    public function callSoapFunction($client, $functionName, array $params)
    {
        $result =  $client->__soapCall($functionName, $params);

        MyDump::dump('ooe.txt', print_r($client->__getLastRequest(),1));
        MyDump::dump('ooe.txt', print_r($client->__getLastResponse(),1));

        return $result;

    }


    // TRACK PACKAGE BY ID
    public function track($id)
    {
        // @ 13.06.2017 - for HUNGARIAN POST and Czech Post use external tracking service
        if(preg_match('/^RR[\d]{9}HU$/i', $id))
        {

            $response = Client17Track::getTt($id, false, false, false, 'OWE');
//            $response = TrackingmoreClient::getTt($id, 'sweden-posten');
            return $response;

//            $track = new Trackingmore;
//
//            $result = $track->createTracking('magyar-posta',$id);
//
//            if($result['meta']['type'] == 'Success')
//                sleep(15); // wait after adding new package
//
//            $result = $track->getSingleTrackingResult('magyar-posta',$id);
//
//            $response = [];
//            if(is_array($result))
//            {
//                if($result['meta']['type'] == 'Success')
//                {
//                    $data = $result['data']['origin_info']['trackinfo'];
//
//                    if(is_array($data))
//                        foreach($data AS $item)
//                        {
//                            array_push($response, [
//                                'date' => $item['Date'],
//                                'location' => $item['Details'],
//                                'name' => preg_replace('/\s+/', ' ',$item['StatusDescription'])
//                            ]);
//                        }
//                }
//            }
//            return $response;
        }
        else if(preg_match('/^RR[\d]{9}CZ$/i', $id))
        {

            $response = Client17Track::getTt($id, false, false, false, 'OWE2');
//            $response = TrackingmoreClient::getTt($id, 'sweden-posten');
            return $response;

//            $track = new Trackingmore;
//            $result = $track->createTracking('czech-post',$id);
//
//            if($result['meta']['type'] == 'Success')
//                sleep(15); // wait after adding new package
//
//            $result = $track->getSingleTrackingResult('czech-post',$id);
//
//            $response = [];
//            if(is_array($result))
//            {
//                if($result['meta']['type'] == 'Success')
//                {
//                    $data = $result['data']['origin_info']['trackinfo'];
//
//                    if(is_array($data))
//                        foreach($data AS $item)
//                        {
//                            array_push($response, [
//                                'date' => $item['Date'],
//                                'location' => $item['Details'],
//                                'name' => $item['StatusDescription']
//                            ]);
//                        }
//                }
//            }
//            return $response;
        }
        else if(preg_match('/^RE[\d]{9}SE/i', $id))
        {
            $response = Client17Track::getTt($id, false, false, false, 'OWE3');
//            $response = TrackingmoreClient::getTt($id, 'sweden-posten');
            return $response;
        }

        try {
            $client  = $this->initSoap();
            $result = $this->callSoapFunction( $client, 'getTracking', array( 'consignmentinformation' => $id ) );
        }
        catch(Exception $ex)
        {
            return false;
        }

        if($result == '')
            return false;

        if(substr($result,0,5) == 'ERROR')
            return false;
        else {

            if($result == '')
                return false;

            // extract data from HTML table to array
            $doc = new DOMDocument();
            libxml_use_internal_errors(true);
            $doc->loadHTML('<?xml encoding="UTF-8">' . $result);

            $doc = $doc->getElementsByTagName('table');


            $data = [];

            // CHECK IF IT'S OPERATOR REG....
            $table = $doc->item(0);
            if($table !== NULL)
                if($table->getElementsByTagName('tr') !== NULL)
                    if($table->getElementsByTagName('tr')->item(1) !== NULL)
                        if($table->getElementsByTagName('tr')->item(1)->getElementsByTagName('td') !== NULL)
                            if($table->getElementsByTagName('tr')->item(1)->getElementsByTagName('td')->item(0) !== NULL && $table->getElementsByTagName('tr')->item(1)->getElementsByTagName('td')->item(1) != NULL)
                                if($table->getElementsByTagName('tr')->item(1)->getElementsByTagName('td')->item(0)->nodeValue == 'Service:' &&
                                    substr($table->getElementsByTagName('tr')->item(1)->getElementsByTagName('td')->item(1)->nodeValue, 0, 3) == 'REG' &&
                                    $table->getElementsByTagName('tr')->item(1)->getElementsByTagName('td')->item(1)->nodeValue != 'REGPOSTIREEUR'
                                )
                                {
                                    // OPERATOR REG********** EXCEPT REGPOSTIREEUR - special rules

                                    $table = $doc->item(1);
                                    if($table === NULL)
                                        $table = $doc->item(0);

                                    $doc = $table;

                                    $first = true;


                                    $subTable = 0;
                                    if(($doc->getElementsByTagName('tr')))
                                        foreach($doc->getElementsByTagName('tr') AS $td)
                                        {
                                            // omit header
                                            if($first)
                                            {
                                                $first = false;
                                                continue;
                                            }

                                            // find beggining of second part of table
                                            if($td->getElementsByTagName('th')->item(0)->nodeValue == 'Destination Post Tracking')
                                            {
                                                $first = true;
                                                $subTable = true;
                                                continue;
                                            }
                                            $date = $td->getElementsByTagName('td')->item(0)->nodeValue;
                                            $location = '';

                                            // if first part of table
                                            if(!$subTable) {

                                                $information = $td->getElementsByTagName('td')->item(1)->nodeValue;
                                                if (trim($information) == '') {
                                                    $temp = S_Useful::trimBetter($td->getElementsByTagName('td')->item(2)->nodeValue);
                                                    if (trim($temp) != '')
                                                        $information = $temp;
                                                }
                                                $name = $information;
                                            } else {
                                                $name = $td->getElementsByTagName('td')->item(2)->nodeValue;

                                                if(trim($name) == '')
                                                    $name = $td->getElementsByTagName('td')->item(1)->nodeValue;
                                            }

                                            array_push($data, array('date' => $date, 'location' => $location, 'name' => $name));
                                        }

                                } else {
                                    // OTHER OPERATOR

                                    // extract data from html table to php array
                                    $table = $doc->item(1);
                                    if($table === NULL)
                                        $table = $doc->item(0);

                                    $doc = $table;

                                    if($doc === NULL)
                                        return false;

                                    $first = true;

                                    if(($doc->getElementsByTagName('tr')))
                                        foreach($doc->getElementsByTagName('tr') AS $td)
                                        {

                                            // omit header
                                            if($first)
                                            {
                                                $first = false;
                                                continue;
                                            }

                                            $date = $td->getElementsByTagName('td')->item(0)->nodeValue;
                                            $location = $td->getElementsByTagName('td')->item(1)->nodeValue;
                                            $information = $td->getElementsByTagName('td')->item(2)->nodeValue;
                                            if(trim($information) == '')
                                            {
                                                $temp = S_Useful::trimBetter($td->getElementsByTagName('td')->item(3)->nodeValue);
                                                if(trim($temp) != '')
                                                    $information = $temp;
                                            }
                                            $name = $information;

                                            if($name == '' && $location != '')
                                            {
                                                $name = $location;
                                                $location = '';
                                            }


                                            if($name != '' OR $location != '')
                                                array_push($data, array('date' => $date, 'location' => $location, 'name' => $name));
                                        }
                                    // extraction end

                                }

            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////

            // apply filters
            foreach($data AS $key => $item)
            {
                $data[$key]['date'] = S_Useful::trimBetter($item['date']);
                $data[$key]['location'] = S_Useful::trimBetter($item['location']);
                $data[$key]['name'] = S_Useful::trimBetter(str_replace('&nbsp;', '', $item['name']));
            }

            // special rules filters
            foreach($data AS $key => $item)
            {


                // CORRECT STAT:
                if(preg_match('/The shipment is being brought to %FIL_BEZ (.*) for pick-up. The earliest time when it can be picked up can be found on the notification card./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment is being brought for pick-up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment is ready for pick-up at the PACKSTATION (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment is ready for pick-up at the at the PACKSTATION';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/Processed at (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Processed';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/Departed Facility in (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Departed Facility';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/Arrived at Sort Facility (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Arrived at Sort Facility';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/Arrived at Delivery Facility in (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Arrived at Delivery Facility';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/Clearance processing complete at (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Clearance processing complete';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/Delivered - Signed for by (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Delivered - Signed for by';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/posted on (.*) with reference (.*) was delivered in (.*) on (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Delivered';
                    $data[$key]['location'] = trim($matches[3]);
                } else if (preg_match('/The shipment is being brought to outlet (.*) for pick-up. The earliest time when it can be picked up can be found on the notification card./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment is being brought to outlet for pick-up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/En cours de traitement(.*)\./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'En cours de traitement';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment has been delivered for pick-up at the outlet (.*)./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment has been delivered for pick-up at the outlet';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/Processed for clearance at (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Processed for clearance';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment is being brought to FIL_BEZ (.*) for pick-up. The earliest time when it can be picked up can be found on the notification card./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment is being brought for pick-up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment has been delivered for pick-up at the FIL_BEZ (.*)./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment has been delivered for pick-up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment is currently at the DHL parcel shop (.*) and is ready to be picked up./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment is currently at the DHL parcel shop and is ready to be picked up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment is being brought to (.*) for pick-up. The earliest time when it can be picked up can be found on the notification card./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment is being brought for pick-up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment is being brought to (.*) for pick-up. The earliest time when it can be picked up can be found on the notification card/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment is being brought for pick-up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment will be delivered for pick-up (.*). The earliest possible pick-up time can be found in the text message\/e-mail./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment is being brought for pick-up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/The shipment has been delivered for pick-up at the (.*)./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The shipment has been delivered for pick-up';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/Further Tracking : (.*)/', $item['name'], $matches)) {
                    $data[$key]['name'] = 'Further Tracking via WWW';
                    $data[$key]['location'] = trim($matches[1]);
                    $data[$key]['date'] = '';
                } else if (preg_match('/(.*), The parcel is in the GLS delivery vehicle to be delivered in the course of the day./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The parcel is in the GLS delivery vehicle to be delivered in the course of the day';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/(.*), The changed delivery option has been saved in the GLS system../', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The changed delivery option has been saved in the GLS system.';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/(.*), The parcel is stored in the GLS warehouse./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The parcel is stored in the GLS warehouse';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/(.*), The parcel remains in the GLS depot. It will delivered at a new delivery date./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The parcel remains in the GLS depot. It will delivered at a new delivery date';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/(.*), The parcel was handed over to the consignee./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The parcel was handed over to the consignee';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/(.*), The parcel was handed over to the consignee./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The parcel was handed over to the consignee';
                    $data[$key]['location'] = trim($matches[1]);
                } else if (preg_match('/(.*),The parcel has been returned to the shipper./', $item['name'], $matches)) {
                    $data[$key]['name'] = 'The parcel has been returned to the shipper';
                    $data[$key]['location'] = trim($matches[1]);
                }
                // special rules

                // IGNORE STATS:
                if(in_array($data[$key]['name'], [
                    'The instruction data for this shipment have been provided by the sender to DHL electronically',
                    'Order information has been transmitted to DPD.',
                    'Further Tracking via WWW',
                ]))
                    unset($data[$key]);

                // IGNORE STATS:
                if(in_array($data[$key]['location'], [
                    'Order information has been transmitted to DPD.',
                ]))
                    unset($data[$key]);


            if (preg_match('/Alternate Tracking Number (.*)/', $item['name'], $matches))
            {
                $couriers = CourierExternalManager::findCourierIdsByRemoteId($id);
                foreach($couriers AS $courier)
                    CourierAdditionalExtId::add($courier, trim($matches[1]));
            }

            }


            return $data;
        }

    }


    // GET AVAILABLE SERVICES FOR USER
    public function getServicesForUser($username, $password, $returnStatus = false, $idAsKeys = false)
    {
        try {
            //Yii::trace('OOE GetServicesForUser');
            $client  = $this->initSoap();

            $result = $this->callSoapFunction($client, 'enabledServices', array('consignmentinformation' => self::generateString(array(
                    $username,
                    $password
                )))
            );
        }
        catch(Exception $ex)
        {

            return false;
        }

        if(substr($result,0,7) != 'SUCCESS')
        {
            if($returnStatus)
            {
                $error = explode('~~', $result);
                return array(false, $error[1]);
            } else
                return false;
        }



        // SPLIT STATUS AND DATA
        $result = explode('||', $result);

        array_shift($result);

        if(!is_array($result))
            return false;

        if($idAsKeys)
        {
            $data = [];
            foreach($result AS $service) {
                // SPLIT SERVICE NAME AND TYPE
                $service = explode('**', $service);
                $data[$service[0]] = $service[1];
            }

            return $data;
        }

        $data = [];
        foreach($result AS $service)
        {
            // SPLIT SERVICE NAME AND TYPE
            $service = explode('**', $service);

            if(!is_array($service))
                return false;

            $array = [
                'id' => $service[0],
                'name' => $service[1]
            ];

            array_push($data, $array);

        }
        if($returnStatus) {
            $list = array();

            foreach($data AS $item)
            {
                array_push($list, array('name' => $item['name'], 'id' => $item['id']));
            }


            return array(true, $list);
        }
        else
            return $data;
    }

    public function getLabel(OneWorldExpressPackage $package, $returnError = false, $loop = 0)
    {

        try {
            //Yii::trace('OOE GetLabel');

            $client = $this->initSoap();
            $result = $this->callSoapFunction($client, 'getLabels', array('consignmentinformation' => $package->getString())
            );
        }
        catch(Exception $ex)
        {

            if(!$returnError) {
                throw new CHttpException('', $ex->getMessage());
            }
            else
            {
                $data = array('error' => $ex->getMessage());
                return $data;
            }
        }


        if(substr($result,0,7) != 'SUCCESS')
        {
            if(strpos($result,'~~')  === false)
                $errorMsg = explode('||', $result);
            else
                $errorMsg = explode('~~', $result);

            $errorMsg = $errorMsg[1];

            if($errorMsg == '' && OneWorldExpressPackage::isPalletways($package->serviceCode))
                $errorMsg = 'Please check if postal code is valid.';

            if($errorMsg == '')
                $errorMsg = 'No reponse from OWE...';

            // HACK FOR "Consignment XXX rejected - repeated HAWB." ERROR - TRY AGAIN IF THIS IS THE ERROR
            if($loop < 2)
            {
//                file_put_contents('ooe_dump.txt', $errorMsg."\r\n", FILE_APPEND);
                $temp = explode(' - ', $errorMsg);
                $temp = $temp[1];
                if(!strcasecmp($temp,'repeated HAWB.'))
                {
//                    file_put_contents('ooe_dump.txt', "błąd!\r\n", FILE_APPEND);
                    return $this->getLabel($package, $returnError, ++$loop);
                }
            }

            if(!$returnError)
                throw new CHttpException('',$errorMsg);
            else
            {
                $data = array('error' => $errorMsg);
                return $data;
            }
        }


        $result = explode('||', $result);

        $label = $result[1];
        $trackId = $result[2];

        $data = array(
            'label' => $label,
            'trackId' => $trackId,
        );

        return $data;

    }
}