<?php

class EobuwieDeliveredRaport
{


    protected static function printTableHeader(&$pdf)
    {
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(10,3, '#',1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(60,3, 'Paczka',1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(60,3, 'Nadawca',1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(60,3, 'Odbiorca',1,'L',false,0,'','',true,0,false,true,0,'T',true);

        $pdf->ln(4);

        $pdf->SetFont('freesans', '', 8);
    }


    public static function generateCardMulti(array $couriers, $filename = 'eob_rap')
    {


        $totalNumber = S_Useful::sizeof($couriers);

//        if($totalNumber > 2500)
//            throw new Exception(Yii::t('courier','Za dużo paczek do wygenerowania potwierdzenia nadania.'));

        $pageSize = 25;
        $firstPageSizeOffset = 1;
        $lastPageSizeOffset = 0;

        $itemsOnCurrentPage = 0;


        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();

        $pdf->setMargins(10, 10, 10, true);

        $pdf->AddPage('P', 'A4');


//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);

        $firstModel = reset($couriers);

        $suffix = '';


        $prefix = '';


        $title = 'Potwierdzenie doreczenia';



//        $pdf->Image('@'.PDFGenerator::getLogoPlBig($firstModel->user), $pdf->getPageWidth() /2 - 15 - $pdf->getMargins()['right'], 5 , 40,'','','N',false);
        $pdf->MultiCell(140,8,$prefix.$title.$suffix,1,'C',false,0,10, 5,true,0,false,true,0,'M',true);
//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->SetY($pdf->GetY() + 8);
        $pdf->SetTextColor(0,0,0);

        $pw = $pdf->getPageWidth();
        $ph = $pdf->getPageHeight();

        $stat_packages = 0;
        // for stats
        foreach($couriers AS $item) {

            $stat_packages++;

        }

        $pdf->SetFont('freesans', '', 8);
        $y = $pdf->GetY();
        $pdf->MultiCell(50,8,Yii::t('courier_pdf','Łącznie paczek:').' '.$stat_packages,1,'R',false,0,$pw-60, 5,true,0,false,true,0,'M',true);
        $pdf->SetAbsY($y - 8);

        $pdf->ln();

        self::printTableHeader($pdf);

        if($totalNumber <= ($pageSize - $firstPageSizeOffset - $lastPageSizeOffset))
            $totalPages = 1;
        else {
            $totalPages = ceil((($totalNumber + $firstPageSizeOffset + $lastPageSizeOffset)) / $pageSize);
        }

        $pdf->SetFont('freesans', '', 8);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->MultiCell(20,3,'1/'.$totalPages,0,'R', false, 0, $pw-30, $ph - 10);
        $pdf->SetAbsX($x);
        $pdf->SetAbsY($y);

        $i = 0;
        $pageI = 0;
        $page = 1;


        $xlsData = [];
        $xlsData[] = ['id', 'weight', 'cod_value','cod_currency', 'delivery_date', 'status', 'sender_name','sender_company', 'sender_address_line_1', 'sender_address_line_2', 'sender_zip_code', 'sender_city', 'sender_country', 'sender_tel', 'sender_email', 'receiver_name','receiver_company', 'receiver_address_line_1', 'receiver_address_line_2', 'receiver_zip_code', 'receiver_city', 'receiver_country', 'receiver_tel', 'receiver_email'];

        /* @var $item Courier */
        $j = 1;
        foreach($couriers AS $key => $item) {

            $currentY = $pdf->GetY();

            $cellContent = '';

            $itemsOnCurrentPage++;


            $xlsData[] = [
                $item->local_id,
                $item->getWeight(true),
                $item->cod_value,
                $item->cod_currency,
                $item->date_delivered,
                'Doręczona',
                $item->senderAddressData->name,
                $item->senderAddressData->company,
                $item->senderAddressData->address_line_1,
                $item->senderAddressData->address_line_2,
                $item->senderAddressData->zip_code,
                $item->senderAddressData->city,
                $item->senderAddressData->country,
                $item->senderAddressData->tel,
                $item->senderAddressData->email,
                $item->receiverAddressData->name,
                $item->receiverAddressData->company,
                $item->receiverAddressData->address_line_1,
                $item->receiverAddressData->address_line_2,
                $item->receiverAddressData->zip_code,
                $item->receiverAddressData->city,
                $item->receiverAddressData->country,
                $item->receiverAddressData->tel,
                $item->receiverAddressData->email,
            ];

            $pdf->MultiCell(10,10, $j,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $cellContent = 'Nr przesyłki: '. $item->local_id."\r\n";
            $cellContent .= 'Data doręczenia: '. substr($item->date_delivered,0,10)."\r\n";
            $cellContent .= 'Status: Doręczona';

            if($item->cod_value)
                $cellContent .= "\r\nKwota pobrania: ".($item->cod_value ? $item->cod_value.' '.$item->cod_currency : '-');


            $pdf->MultiCell(60,10,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $cellContent = $item->senderAddressData->company != '' ? $item->senderAddressData->company."\r\n" : '';
            $cellContent .= $item->senderAddressData->name != '' ? $item->senderAddressData->name."\r\n" : '';
            $cellContent .= $item->senderAddressData->address_line_1."\r\n";
            $cellContent .= $item->senderAddressData->address_line_2 != '' ? $item->senderAddressData->address_line_2."\r\n" : '';
            $cellContent .= $item->senderAddressData->zip_code.', ';
            $cellContent .= $item->senderAddressData->city."\r\n";
            $cellContent .= $item->senderAddressData->country;

            $pdf->MultiCell(60,10,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $cellContent = $item->receiverAddressData->company != '' ? $item->receiverAddressData->company."\r\n" : '';
            $cellContent .= $item->receiverAddressData->name != '' ? $item->receiverAddressData->name."\r\n" : '';
            $cellContent .= $item->receiverAddressData->address_line_1."\r\n";
            $cellContent .= $item->receiverAddressData->address_line_2 != '' ? $item->receiverAddressData->address_line_2."\r\n" : '';
            $cellContent .= $item->receiverAddressData->zip_code.', ';
            $cellContent .= $item->receiverAddressData->city."\r\n";
            $cellContent .= $item->receiverAddressData->country;

            $pdf->MultiCell(60,10,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $pdf->ln(10.7);

            // break first page earlier to make space for header
            if($page == 1 && $pageI > 0  && ($pageI%($pageSize-$firstPageSizeOffset) == 0))
            {

                $pdf->SetFont('freesans', '', 8);
//                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);


                // clear empty pages before inserting new
                if(!$itemsOnCurrentPage)
                {
                    $noOfPages = $pdf->getNumPages();
                    if($noOfPages)
                        $pdf->deletePage($noOfPages);
                }

                $pdf->AddPage('P','A4');


                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }
            // break last page earlier to make space for footer
            else if($page > 1 && $page == $totalPages && ($pageI%($pageSize-$lastPageSizeOffset) == 0))
            {

                $pdf->SetFont('freesans', '', 8);
//                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

                // clear empty pages before inserting new
                if(!$itemsOnCurrentPage)
                {
                    $noOfPages = $pdf->getNumPages();
                    if($noOfPages)
                        $pdf->deletePage($noOfPages);
                }

                $pdf->AddPage('P','A4');

                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }
            else if(($pageI%$pageSize) == 0 && $pageI > 0)
            {

                $pdf->SetFont('freesans', '', 8);
//                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

                // clear empty pages before inserting new
                if(!$itemsOnCurrentPage)
                {
                    $noOfPages = $pdf->getNumPages();
                    if($noOfPages)
                        $pdf->deletePage($noOfPages);
                }

                $pdf->AddPage('P','A4');


                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }

            $i++;
            $pageI++;

            $done[$key] = $item->id;

            $j++;
        }

        $pdf->SetAbsY($pdf->GetY() + 20);

        $pdf->SetFont('freesans', '', 8);

//        $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//        $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

        /* @var $pdf TCPDF */

        $filename = Yii::app()->basePath.'/../er/'.$filename;

        $pdf = $pdf->Output($filename.'.pdf', 'S');

        $xls = S_XmlGenerator::generateXml($xlsData, $filename.'.xlsx', true);
        return [$xls, $pdf];

//        header('Content-Type: application/pdf');
//        header("Content-Transfer-Encoding: Binary");
//        header("Content-disposition: attachment; filename=eob_rap.pdf");
//        echo $pdfString;
//
//        return $done;

    }

}
