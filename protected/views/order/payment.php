<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $payment PaymentType */
?>

<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'order-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(

            'date_entered',
            array(
                'name' => 'orderStat',
                'type' => 'raw',
                'value' => $model->orderStat !== null ? GxHtml::encode(GxHtml::valueEx($model->orderStat)) : null,
            ),
            array(
                'name' => 'produkt',
                'type' => 'raw',
                'value' => $product->name !== null ? GxHtml::link(GxHtml::encode($product->name),$product->url) : null,

            ),
        ),
    )); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'payment_type_id'); ?><br/>
        <?php echo $form->radioButtonList($model, 'payment_type_id', GxHtml::listDataEx($payment), array('id'=>'payment')); ?>
        <?php echo $form->error($model,'payment_type_id'); ?>
    </div><!-- row -->

    <div class="row">
        <?php echo Yii::t('order','Prowizja płatności:');?> <span id="commission_amount"></span>
    </div><!-- row -->
    <div class="row">
        <?php echo Yii::t('order','Wartość zamówienia');?> <span id="order_value"><?php echo $model->getAmountToPay();?></span>
    </div><!-- row -->
    <div class="row">
        <?php echo Yii::t('order','Do zapłaty');?> <span id="payment_value"></span>
    </div><!-- row -->

    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    echo Chtml::script('

            $("input[name=\'Order[payment_type_id]\']").click(function(){

                $.post("'.Yii::app()->createUrl('paymentType/ajaxGetPaymentCommissionValue').'",
                {\'payment_type_id\' : $(this).val(),\'order_hash\' : \''.$model->hash.'\'},
                function(data){
                    $("span#commission_amount").html(data);
                });


            });

        ');




    ?>


    <?php
    echo TbHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->