<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Zend\\Stdlib\\' => array($vendorDir . '/zendframework/zend-stdlib/src'),
    'Zend\\Escaper\\' => array($vendorDir . '/zendframework/zend-escaper/src'),
    'Symfony\\Polyfill\\Php73\\' => array($vendorDir . '/symfony/polyfill-php73'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Php70\\' => array($vendorDir . '/symfony/polyfill-php70'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Contracts\\' => array($vendorDir . '/symfony/contracts'),
    'Symfony\\Component\\Stopwatch\\' => array($vendorDir . '/symfony/stopwatch'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\OptionsResolver\\' => array($vendorDir . '/symfony/options-resolver'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'SSilence\\ImapClient\\' => array($vendorDir . '/ssilence/php-imap-client/ImapClient'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'PhpOffice\\PhpWord\\' => array($vendorDir . '/phpoffice/phpword/src/PhpWord'),
    'PhpOffice\\PhpSpreadsheet\\' => array($vendorDir . '/phpoffice/phpspreadsheet/src/PhpSpreadsheet'),
    'PhpOffice\\Common\\' => array($vendorDir . '/phpoffice/common/src/Common'),
    'PhpCsFixer\\' => array($vendorDir . '/friendsofphp/php-cs-fixer/src'),
    'PhpAmqpLib\\' => array($vendorDir . '/php-amqplib/php-amqplib/PhpAmqpLib'),
    'PHPMailer\\PHPMailer\\' => array($vendorDir . '/phpmailer/phpmailer/src'),
    'Omines\\DirectAdmin\\' => array($vendorDir . '/omines/directadmin/src/DirectAdmin'),
    'MyCLabs\\Enum\\' => array($vendorDir . '/myclabs/php-enum/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Metadata\\' => array($vendorDir . '/jms/metadata/src'),
    'Matrix\\' => array($vendorDir . '/markbaker/matrix/classes/src'),
    'League\\OAuth2\\Client\\' => array($vendorDir . '/league/oauth2-client/src'),
    'KeepItSimple\\Http\\Soap\\' => array($vendorDir . '/debuss-a/mtomsoapclient'),
    'JMS\\Serializer\\' => array($vendorDir . '/jms/serializer/src'),
    'IRebega\\DocxReplacer\\' => array($vendorDir . '/irebega/docx-replacer/src'),
    'Hoa\\Zformat\\' => array($vendorDir . '/hoa/zformat'),
    'Hoa\\Visitor\\' => array($vendorDir . '/hoa/visitor'),
    'Hoa\\Ustring\\' => array($vendorDir . '/hoa/ustring'),
    'Hoa\\Stream\\' => array($vendorDir . '/hoa/stream'),
    'Hoa\\Regex\\' => array($vendorDir . '/hoa/regex'),
    'Hoa\\Protocol\\' => array($vendorDir . '/hoa/protocol'),
    'Hoa\\Math\\' => array($vendorDir . '/hoa/math/Source'),
    'Hoa\\Iterator\\' => array($vendorDir . '/hoa/iterator'),
    'Hoa\\File\\' => array($vendorDir . '/hoa/file'),
    'Hoa\\Exception\\' => array($vendorDir . '/hoa/exception'),
    'Hoa\\Event\\' => array($vendorDir . '/hoa/event'),
    'Hoa\\Consistency\\' => array($vendorDir . '/hoa/consistency'),
    'Hoa\\Compiler\\' => array($vendorDir . '/hoa/compiler'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'GusApi\\' => array($vendorDir . '/gusapi/gusapi/src/GusApi'),
    'Google\\Cloud\\Translate\\' => array($vendorDir . '/google/cloud-translate/src'),
    'Google\\Cloud\\Core\\' => array($vendorDir . '/google/cloud-core/src'),
    'Google\\Auth\\' => array($vendorDir . '/google/auth/src'),
    'FontLib\\' => array($vendorDir . '/phenx/php-font-lib/src/FontLib'),
    'FluidXml\\' => array($vendorDir . '/servo/fluidxml/source/FluidXml'),
    'Firebase\\JWT\\' => array($vendorDir . '/firebase/php-jwt/src'),
    'Dompdf\\' => array($vendorDir . '/dompdf/dompdf/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Common/Inflector'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib/Doctrine/Common/Collections'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib/Doctrine/Common/Cache'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib/Doctrine/Common', $vendorDir . '/doctrine/event-manager/lib/Doctrine/Common', $vendorDir . '/doctrine/persistence/lib/Doctrine/Common', $vendorDir . '/doctrine/reflection/lib/Doctrine/Common'),
    'Composer\\XdebugHandler\\' => array($vendorDir . '/composer/xdebug-handler/src'),
    'Composer\\Semver\\' => array($vendorDir . '/composer/semver/src'),
    'Complex\\' => array($vendorDir . '/markbaker/complex/classes/src'),
    'Box\\Spout\\' => array($vendorDir . '/box/spout/src/Spout'),
    '' => array($vendorDir . '/linker/api-client/src'),
);
