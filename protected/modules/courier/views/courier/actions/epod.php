<div class="alert alert-success text-center">
    <h4><?php echo Yii::t('courier','Pobierz Elektroniczne Potwierdzenie Odbioru');?></h4>
    <br/>
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'get-epod',
        'action' => Yii::app()->createUrl('/courier/courier/getEpod'),
        'htmlOptions' => [
            'class' => 'do-not-block',
            'target' => '_blank',
        ],
    ));
    ?>
    <?php
    echo TbHtml::submitButton(Yii::t('courier', 'Pobierze ePOD'), array(
        'name' => 'getEpod', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
    ?>
    <?php
    echo TbHtml::hiddenField('hash', $hash);
    $this->endWidget();
    ?>
</div>
