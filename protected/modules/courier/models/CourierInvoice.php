<?php

class CourierInvoice
{


    public static function generateInvoice(array $items, $pipelinePdf = false)
    {
        /* @var $items Courier[] */

        /* @var $pdf TCPDF */
        if($pipelinePdf)
            $pdf = $pipelinePdf;
        else
            $pdf = PDFGenerator::initialize();


        $pdf->setMargins(10,10,10, true);

        foreach($items AS $item) {

            if (in_array($item->courier_stat_id, [CourierStat::CANCELLED, CourierStat::CANCELLED_USER])) {
                continue;
            }

            $itemNo = '';
            if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
            {
                $itemNo = ($item->getExternalOperatorsAndIds(true))[1][0];
            }

            if($itemNo == '')
                $itemNo = $item->local_id;

            $pdf->AddPage('P', 'A4');

//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
            $pdf->SetAutoPageBreak(false);

//            $firstModel = reset($items);
//            $pdf->Image('@' . PDFGenerator::getLogoPlBig($firstModel->user), $pdf->getPageWidth() / 2 - 15 - $pdf->getMargins()['right'], 5, 50, '', '', 'N', false);

//        $pdf->AddPage();
            $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            $pdf->SetY($pdf->GetY() + 0);
            $pdf->SetTextColor(0, 0, 0);

            $pdf->SetFont('freesans', '', 20);
            $pdf->MultiCell(0, 0, Yii::t('courier_invoice_pdf', 'Faktura pro-forma'), 0, 'C', false);
            $pdf->MultiCell(0, 0, 'No. '.$itemNo, 0, 'C', false);
            $pdf->MultiCell(0, 0, '', 0, 'C', false);

            $pdf->SetAbsY($pdf->GetY() - 5);
            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0, 0, Yii::t('courier_invoice_pdf', 'Nadawca:'), 0, 'L');
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() - 5);
            $data = array(
                array(Yii::t('courier_invoice_pdf', 'Firma:'), $item->senderAddressData->company),
                array(Yii::t('courier_invoice_pdf', 'Imię i nazwisko:'), $item->senderAddressData->name),
                array(Yii::t('courier_invoice_pdf', 'Adres:'), $item->senderAddressData->address_line_1),
                array(Yii::t('courier_invoice_pdf', 'Adres c.d.:'), $item->senderAddressData->address_line_2),
                array(Yii::t('courier_invoice_pdf', 'Kod pocztowy:'), $item->senderAddressData->zip_code),
                array(Yii::t('courier_invoice_pdf', 'Miasto:'), $item->senderAddressData->city),
                array(Yii::t('courier_invoice_pdf', 'Kraj:'), $item->senderAddressData->country),
            );
            self::ColoredTable($pdf, $data);

            $pdf->SetAbsY($pdf->GetY() + 5);

            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0, 0, Yii::t('courier_invoice_pdf', 'Odbiorca:'), 0, 'L', false, 1);
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() - 5);
            $data = array(
                array(Yii::t('courier_invoice_pdf', 'Firma:'), $item->receiverAddressData->company),
                array(Yii::t('courier_invoice_pdf', 'Imię i nazwisko:'), $item->receiverAddressData->name),
                array(Yii::t('courier_invoice_pdf', 'Adres:'), $item->receiverAddressData->address_line_1),
                array(Yii::t('courier_invoice_pdf', 'Adres c.d.:'), $item->receiverAddressData->address_line_2),
                array(Yii::t('courier_invoice_pdf', 'Kod pocztowy:'), $item->receiverAddressData->zip_code),
                array(Yii::t('courier_invoice_pdf', 'Miasto:'), $item->receiverAddressData->city),
                array(Yii::t('courier_invoice_pdf', 'Kraj:'), $item->receiverAddressData->country),
            );
            self::ColoredTable($pdf, $data);

            $pdf->SetAbsY($pdf->GetY() + 8);

            $pdf->SetFont('freesans', 'B', 12);
            $pdf->MultiCell(75, 5, Yii::t('courier_invoice_pdf', 'Zawartość').' '.Yii::t('courier_invoice_pdf', '(po polsku i angielsku)'), 1, 'L', false, 1, 10, 142,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

            $pdf->MultiCell(10, 5, Yii::t('courier_invoice_pdf', 'Ilość'), 1, 'L', false, 1, 85, 142,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

            $pdf->MultiCell(40, 5, Yii::t('courier_invoice_pdf', 'Kraj pochodzenia'), 1, 'L', false, 1, 95, 142,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );
            $pdf->MultiCell(15, 5, Yii::t('courier_invoice_pdf', 'Waga'), 1, 'L', false, 1, 135, 142,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );
            $pdf->MultiCell(25, 5, Yii::t('courier_invoice_pdf', 'Wartość jednostkowa'), 1, 'L', false, 1, 150, 142,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

            $pdf->MultiCell(25, 5, Yii::t('courier_invoice_pdf', 'Wartość'), 1, 'L', false, 1, 175, 142,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

            $pdf->SetFont('freesans', '', 12);

            $pdf->MultiCell(75, 40, $item->package_content, 1, 'L', false, 1, 10, 147,
                true,
                0,
                false,
                true,
                40,
                'T',
                true
            );

            $pdf->MultiCell(10, 40, '', 1, 'L', false, 1, 85, 147,
                true,
                0,
                false,
                true,
                40,
                'T',
                true
            );

            $pdf->MultiCell(40, 40, $item->senderAddressData->country0->name, 1, 'L', false, 1, 95, 147,
                true,
                0,
                false,
                true,
                40,
                'T',
                true
            );
            $pdf->MultiCell(15, 40, $item->package_weight.' KG', 1, 'L', false, 1, 135, 147,
                true,
                0,
                false,
                true,
                40,
                'T',
                true
            );
            $pdf->MultiCell(25, 40, $item->package_value.' '.$item->getPackageValueCurrency(), 1, 'L', false, 1, 150, 147,
                true,
                0,
                false,
                true,
                40,
                'T',
                true
            );

            $pdf->MultiCell(25, 40, '', 1, 'L', false, 1, 175, 147,
                true,
                0,
                false,
                true,
                40,
                'T',
                true
            );

            $pdf->SetAbsY($pdf->GetY() + 3);
            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0, 0, Yii::t('courier_invoice_pdf', 'Powód wysyłki:'), 0, 'L');
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() - 5);

            $pdf->MultiCell(165, 10, '', 1, 'L', false, 1, 10, 198,
                true,
                0,
                false,
                true,
                10,
                'T',
                true
            );


            $pdf->SetAbsY($pdf->GetY() + 3);

            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0, 0, Yii::t('courier_invoice_pdf', 'Deklaracja:'), 0, 'L');
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() - 8);

            $pdf->SetFont('freesans', '', 12);
            $pdf->MultiCell(165, 5, Yii::t('courier_invoice_pdf', 'Deklaruję, że zawartość tej faktury jest prawidłowa i prawdziwa.'), 1, 'L', false, 1, 10, 220,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

            $pdf->SetFont('freesans', 'B', 12);

            $pdf->MultiCell(55, 5, Yii::t('courier_invoice_pdf', 'Imię i nazwisko'), 1, 'C', false, 1, 10, 225,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );
            $pdf->MultiCell(55, 5, Yii::t('courier_invoice_pdf', 'Firma i stanowisko'), 1, 'C', false, 1, 65, 225,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );
            $pdf->MultiCell(55, 5, Yii::t('courier_invoice_pdf', 'Data'), 1, 'C', false, 1, 120, 225,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

            $pdf->MultiCell(55, 10, '', 1, 'L', false, 1, 10, 230,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );
            $pdf->MultiCell(55, 10, '', 1, 'L', false, 1, 65, 230,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );
            $pdf->MultiCell(55, 10, '', 1, 'L', false, 1, 120, 230,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

            $pdf->SetAbsY($pdf->GetY() + 5);

            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0, 0, Yii::t('courier_invoice_pdf', 'Dodatkowe informacje:'), 0, 'L');
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() - 5);

            $pdf->MultiCell(165, 30, '', 1, 'L', false, 1, 10, 255,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

//            $pdf->SetFont('freesans', '', 12);



//            $pdf->SetFont('freesans', '', 8);
//            $pdf->MultiCell(0, 10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//            $pdf->MultiCell(0, 6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);


        }

        /* @var $pdf TCPDF */
        if($pipelinePdf)
            return $pdf;
        else
            $pdf->Output('Invoice.pdf', 'D');
    }

    protected static function ColoredTable(TCPDF $pdf,$data, $header = array()) {
        // Colors, line width and B font
        $pdf->SetFillColor(255, 0, 0);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(128, 0, 0);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFont('', 'B', 10);
        // Header
        $w = array(40, 100);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $pdf->Ln();
        // Color and font restoration
        $pdf->SetFillColor(224, 235, 255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('');
        // Data
        $fill = 0;
        $i = 0;
        $size = S_Useful::sizeof($data);
        foreach($data as $row) {
            $border = 'LR';
            if(!$i) {
                if($size - 1)
                    $border = 'LRT';
                else
                    $border = 'LRTB';
            }
            else if($i == $size - 1)
                $border = 'LRB';


            $pdf->Cell($w[0], 6, $row[0].'  ', $border, 0, 'R', $fill);
            $pdf->Cell($w[1], 6, '  '.$row[1], $border, 0, 'L', $fill);

            $pdf->Ln();
            // $fill=!$fill;
            $i++;
        }

    }
}
