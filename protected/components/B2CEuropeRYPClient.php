<?php

class B2CEuropeRYPClient extends GlobalOperatorWithReturn
{
    const URL = 'https://www.returnyourparcel.eu/api/returnyourparcel/';
    const USER = 'Kaab';
    const KEY = 'buSsyzW6tM';

    public $sender_prefix = false;
    public $local_id = false;
    public $user_id = false;
    //
    public static function test()
    {
        $model = new self;
        $model->local_id = '123213123';
        return $model->_createShipment();
    }

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    protected function _createShipment($returnError = false)
    {
//        if(!in_array($this->user_id, [947, 948]))
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse('Operator not active for this customer. Please contact us.');
//            } else {
//                return false;
//            }

        $data = '<ReturnYourParcelAPIRequest>
<Userid>'.self::USER.'</Userid>
<Password>'.self::KEY.'</Password>
<LayoutType>P06</LayoutType>
<LayoutVersion>3.0</LayoutVersion>
<CreateReturnLabel>
<ServiceCode>01</ServiceCode>
<Instructions>01</Instructions>
<Order>
      <CountryCode>ESP</CountryCode>
      <OrderNumber>'.$this->local_id.'</OrderNumber>
    </Order>
</CreateReturnLabel>
</ReturnYourParcelAPIRequest>';

        $resp = $this->_curlCall($data);

        if ($resp->success == true) {
            $return = [];

            $ttNumber = (string) $resp->data->CreateReturnLabel->Suppliercode;
            $label = (string) $resp->data->CreateReturnLabel->ReturnLabel;
            $label = base64_decode($label);

            $im = ImagickMine::newInstance();
            $im->setResolution(300,300);

            $im->readImageBlob($label);
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            if($this->sender_prefix)
                LabelAnnotation::createAnnotation($this->sender_prefix, $im, 1800, 2100, false, false, 8);

            $im->rotateImage(new ImagickPixel(), 90);
//            $im->trimImage(0);

            $imgWidth = $im->getImageWidth();
            $imgHeight = $im->getImageHeight();

            $im2 = clone $im;

            $im->cropImage($imgWidth / 2, $imgHeight, 0, 0);
            $im->stripImage();


            $im2->cropImage($imgWidth / 2, $imgHeight, $imgWidth / 2, 0);
            $im2->stripImage();


            $labels = [];
            $labels[] = $im->getImageBlob();

            if(!in_array($this->user_id, [947, 948, 2036, 2037])) // @08.11.2018 - don't add second page for EOBUWIE
                $labels[] = $im2->getImageBlob();

            $return[] = GlobalOperatorResponse::createSuccessResponse($labels, $ttNumber);

            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }

    }

    protected function _curlCall($data)
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

//        $dataOryginal = $data;

        $headers = [];
        $headers[] = 'Content-Type: text/xml';

        $curl = curl_init(self::URL);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt_array($curl, array(
                CURLOPT_POST =>TRUE,
                CURLOPT_RETURNTRANSFER =>TRUE,
                CURLOPT_ENCODING =>"UTF-8",
                CURLOPT_HTTPHEADER =>$headers,
                CURLOPT_SSL_VERIFYHOST =>false,
                CURLOPT_SSL_VERIFYPEER =>false)

        );
        $resp = curl_exec($curl);


        MyDump::dump('b2ceurope_ryp.txt', print_r($data,1));
        MyDump::dump('b2ceurope_ryp.txt', print_r(preg_replace('%<ReturnLabel>(.*)</ReturnLabel>%i', '<ReturnLabel>(...TRUNCATED LABEL BASE64...)</ReturnLabel>',  $resp),1));


        $xml = @simplexml_load_string($resp);

        if($xml->Result == 'SUCCESS')
        {
            $return->success = true;
            $return->data = $xml;
        }
        else
        {
            if ($xml == '')
            {
                $error = curl_error($curl);
                if ($error == '')
                    $error = 'Unknown error';

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
            else
            {
                $error = (string) $xml->ErrorMessage;

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
        }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;
        $model->user_id = $courierInternal->courier->user_id;
        $model->local_id = $courierInternal->courier->local_id;

        if(in_array($courierInternal->courier->user_id, [947, 948, 2036, 2037]))
            $model->sender_prefix = 'ZAPATOS.ES';
        else
            $model->sender_prefix = $courierInternal->courier->user->login;

        return $model->_createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;
        $model->user_id = $courierU->courier->user_id;
        $model->local_id = $courierU->courier->local_id;

        if(in_array($courierU->courier->user_id, [947, 948, 2036, 2037]))
            $model->sender_prefix = 'ZAPATOS.ES';
        else
            $model->sender_prefix = $courierU->courier->user->login;

        return $model->_createShipment($returnErrors);
    }

    public static function orderForCourierReturn(CourierTypeReturn $courierReturn, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;
        $model->user_id = $courierReturn->courier->user_id;
        $model->local_id = $courierReturn->courier->local_id;

        if(in_array($courierReturn->courier->user_id,[947, 948, 2036, 2037]))
            $model->sender_prefix = 'ZAPATOS.ES';
        else
            $model->sender_prefix = $courierReturn->courier->user->login;


        return $model->_createShipment($returnErrors);
    }


    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return Client17Track::getTtGlobalBrokers($no, false, false, false, 'B2C_RYP');
//        return TrackingmoreClient::getTtGlobalBrokers($no, 'correos-spain');
    }

}

