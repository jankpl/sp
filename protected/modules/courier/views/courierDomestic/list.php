<h2><?php echo Yii::t('site','Your packages');?></h2>


<?php
$this->widget('FlashPrinter');
?>

<?php if($parent_group !== NULL):?>
    <?php
    $parent = Courier::model()->findByPk($parent_group);
    if($parent->courier_stat_id < CourierStat::PACKAGE_RETREIVE):
        ?>

        <div class="alert alert-warining"><?php echo Yii::t('courier', 'Twoja paczka jest w trakcie przetwarzania. Możesz odświeżać stronę, aby sprawdzić, czy etykieta jest już gotowa.');?></div>

    <?php else: ?>
        <div style="width: 80%; padding: 5px; margin: 5px; text-align: left; border: 1px dashed gray;">
            <p style="font-size: 12px; font-weight: bold;"><?php echo Yii::t('courier','Etykiety dla wszyskich paczek');?></p>
            <?php $form = $this->beginWidget('GxActiveForm', array(
                'id' => 'universal-carriage-ticket',
                'action' => Yii::app()->createUrl('/courier/courierDomestic/getLabelsGroup',['parent_group' => $parent_group]),
                'htmlOptions' => array('class' => 'do-not-block',
                    'target' => Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL ? '_blank' : '_self'
                ),
            ));
            ?>
            <?php echo CHtml::dropDownList('type','',LabelPrinter::getLabelGenerateTypes(), array('style' => 'width: 250px; display: inline-block; height: 23px; vertical-align: top;'));?>
            <br/>
            <?php echo CHtml::submitButton(Yii::t('courier','Generuj etykiety dla poniższych paczek'));?>
            <?php $this->endWidget();?>
        </div>
    <?php endif;?>
    <?php
endif;
?>

<?php
$this->widget('ext.selgridview.BootSelGridView', array(
    'id' => 'courier-grid',
    'dataProvider' => $model->search($parent_group),
    'filter' => $model,
    'selectableRows'=>2,
    'columns' => array(
        array(
            'name'=>'local_id',
            'header'=>Yii::t('courier','#ID'),
            'type'=>'raw',
            'value'=>'CHtml::link($data->local_id,array("/courier/courier/view/", "urlData" => $data->hash))',
        ),
        array(
            'name'=>'date_entered',
            'value' => 'substr($data->date_entered, 0, 10)',
            'header'=>Yii::t('courier','Data utworzenia'),
        ),
        array(
            'name'=>'__stat',
            'type'=>'raw',
            'filter'=>GxHtml::listDataEx(CourierStat::model()->findAllAttributes(null, true)),
            'header' => Yii::t('courier','Status'),
            'value'=>'CHtml::link($data->stat->courierStatTr->short_text,array("/courier/courier/tt", "urlData" => $data->local_id))',
        ),
        array(
            'name'=>'__sender_country_id',
            'header'=>Yii::t('courier','Sender country'),
            'value'=>'$data->senderAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name' => '__sender_usefulName',
            'header' => Yii::t('courier','Sender'),
            'value' => '$data->senderAddressData->usefulName',
        ),
        array(
            'name'=>'__receiver_country_id',
            'header'=>Yii::t('courier','Receiver country'),
            'value'=>'$data->receiverAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name' => '__receiver_name',
            'header' => Yii::t('courier','Receiver'),
            'value' => '$data->receiverAddressData->usefulName',
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}',
            'buttons'=>array(
                'view' => array(
                    'url' => 'Yii::app()->createUrl("/courier/courier/view/", array("urlData" => $data->hash))',
                ),
            ),
        ),
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'selected_rows'
        ),
    ),
));
?>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    if($key == 'top-info')
    {
        Yii::app()->user->setFlash('top-info', $message);
        continue;
    }
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php
$this->renderPartial('/_listActions/_generateAckCardMulti');
?>

<?php $this->widget('BackLink'); ?>





