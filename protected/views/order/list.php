<?php
/* @var $model Order */

?>
<h2><?php echo Yii::t('internationalMail','Lista Twoich zamówień');?></h2>

<?php
if(0):
    ?>
    <div class="alert alert-info text-center"><?= Yii::t('order', 'Znajdziesz tutaj listę Twoich zamówień. Szczegóły Twoich produktów znajdują się w odpowiednich zakładkach:');?><br/><br/>
        <a href="<?= Yii::app()->createUrl('/courier/courier/list');?>" class="btn"><?= Yii::t('site','Przesyłki');?></a>
        <a href="<?= Yii::app()->createUrl('/hybridMail/hybridMail/list');?>" class="btn"><?= Yii::t('site','HybridMail');?></a>
        <a href="<?= Yii::app()->createUrl('/internationalMail/internationalMail/list');?>" class="btn"><?= Yii::t('site','InternationalMail');?></a>
    </div>
<?php
endif;
?>

<?php

$columns = [];

$columns[] = 'local_id';
$columns[] =  array(
    'name' => 'date_entered',
    'value' => 'substr($data->date_entered, 0, 10)',
);
$columns[] =  array(
    'name'=>'order_stat_id',
    'value'=>'$data->orderStat->nameTr',
    'filter'=>CHtml::listData(OrderStat::model()->findAllAttributes(null, true),'id','nameTr'),
);
$columns[] = array(
    'header'=> Yii::t('internationalMail','Typ produkty'),
    'name' => 'product_type',
    'type' => 'raw',
    'value'=> 'Order::getProductName($data->product_type)',
    'filter' => CHtml::listData(Order::listProducts(),"id","name"),
    'headerHtmlOptions' => array('style' => 'width: 130px;'),
);
$columns[] = array(
    'header'=> Yii::t('internationalMail','Produktów'),
    'type' => 'raw',
    'name' => 'orderProductSTAT',
    'headerHtmlOptions' => array('style' => 'width: 50px;'),
);

$columns[] = array(
    'name' => '__payment_type',
    'value' => '$data->paymentType->localizedName',
    'filter' => GxHtml::listData(PaymentType::model()->findAllAttributes(null, true),'id','localizedName'),
);


if(!Yii::app()->user->model->getHidePrices())
    $columns[] = array(
        'header' => Yii::t('internationalMail','Wartość brutto'),
        'name' => 'value_brutto',
        'value' => '$data->value_brutto ." " . $data->currency',
    );

$columns[] = array(
    'class' => 'CButtonColumn',
    'template' => '{view}',
    'buttons'=>array(
        'view' => array(
            'url' => 'Yii::app()->createUrl("order/view/", array("urlData" => $data->hash))',
        ),
    )
);


//$this->widget('zii.widgets.grid.CGridView', array(
//    'id' => 'order-grid',
//    'dataProvider' => $model->searchForUser(),
//    'filter' => $model,
//    'columns' => $columns,
//));
?>
