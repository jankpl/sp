<?php
/* @var $model UserContactBook */
/* @var $i Integer */


$rowWithErrors = true;
if(!$model->hasErrors() && !$model->addressData->hasErrors())
    $rowWithErrors = false;
?>
<tr class="condensed <?= $rowWithErrors ? 'row-errored' : '';?>" data-row-id="<?= $i;?>">
    <td>
        <?= ($i+1);?>
    </td>
    <td>
        <?php if($rowWithErrors)
            echo '<i class="glyphicon glyphicon-remove"></i>';
        else
            echo '<i class="glyphicon glyphicon-ok"></i>';
        ?>
    </td>
    <td>
        <input type="button" value="<?php echo Yii::t('courier', 'Remove');?>" data-remove-this-package="true"/>
    </td>
    <td <?php echo ($model->hasErrors('type')?'class="td-with-error"':'');?> title="<?= $model->getError('type');?>">
        <?php echo CHtml::activeDropDownList($model,'['.$i.']type', UserContactBook::getTypes_enum(), array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'type')); ?>
    </td>
    <td style="<?php echo ($model->addressData->hasErrors('name')?'background: red;':'');?>" title="<?= $model->addressData->getError('name');?>">
        <?php echo CHtml::activeTextField($model->addressData,'['.$i.'][addressData]name', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'addressData.name')); ?>
    </td>
    <td <?php echo ($model->addressData->hasErrors('company')?'class="td-with-error"':'');?> title="<?= $model->addressData->getError('company');?>">
        <?php echo CHtml::activeTextField($model->addressData,'['.$i.'][addressData]company', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'addressData.company')); ?>
    </td>
    <td <?php echo ($model->addressData->hasErrors('country_id')?'class="td-with-error"':'');?> title="<?= $model->addressData->getError('country_id');?>">
        <?php echo CHtml::activeHiddenField($model->addressData,'['.$i.'][addressData]country_id', array('disabled' => 'disabled')); ?>
        <?php echo CHtml::textField('['.$i.'][addressData]country_temp', $model->addressData->country0 !== NULL ? $model->addressData->country0->getTrName() : '', array('disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->addressData->hasErrors('zip_code')?'class="td-with-error"':'');?>>
        <?php echo CHtml::activeTextField($model->addressData,'['.$i.'][addressData]zip_code', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'addressData.zip_code')); ?>
        <?= $model->addressData->getError('zip_code');?>
    </td>
    <td <?php echo ($model->addressData->hasErrors('city')?'class="td-with-error"':'');?> title="<?= $model->addressData->getError('city');?>">
        <?php echo CHtml::activeTextField($model->addressData,'['.$i.'][addressData]city', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'addressData.city')); ?>
    </td>
    <td <?php echo ($model->addressData->hasErrors('address_line_1')?'class="td-with-error"':'');?> title="<?= $model->addressData->getError('address_line_1');?>">
        <?php echo CHtml::activeTextField($model->addressData,'['.$i.'][addressData]address_line_1', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'addressData.address_line_1')); ?>
    </td>
    <td <?php echo ($model->addressData->hasErrors('address_line_2')?'class="td-with-error"':'');?> title="<?= $model->addressData->getError('address_line_2');?>">
        <?php echo CHtml::activeTextField($model->addressData,'['.$i.'][addressData]address_line_2', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'addressData.address_line_2')); ?>
    </td>
    <td <?php echo ($model->addressData->hasErrors('tel')?'class="td-with-error"':'');?>  title="<?= $model->addressData->getError('tel');?>">
        <?php echo CHtml::activeTextField($model->addressData,'['.$i.'][addressData]tel', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'addressData.tel')); ?>
    </td>
    <td <?php echo ($model->addressData->hasErrors('email')?'class="td-with-error"':'');?> title="<?= $model->addressData->getError('email');?>">
        <?php echo CHtml::activeTextField($model->addressData,'['.$i.'][addressData]email', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'addressData.email')); ?>
    </td>
</tr>