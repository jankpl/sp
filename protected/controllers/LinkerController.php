<?php

class LinkerController extends Controller {


    public function filters()
    {
        return array_merge(array(
            'accessControl', // perform access control for CRUD operations
        ));
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionAdd($adapter)
    {
        try {
            $lm = LinkerManager::init(Yii::app()->user->getModel());

            $model = new LinkerFormBuilder();

            $model->initialize($lm, $lm->getDepotId(), $adapter);

            $formData = $lm->getForm($adapter);
            $model->generate($formData);

            if (isset($_POST['LinkerFormBuilder'])) {
                $model->setAttributes($_POST['LinkerFormBuilder']);

                if ($model->validate()) {
                    if($_POST['step2Id']) {
                        // step2:

                        $result = $model->submitForm($lm, $_POST['step2Id']);
                        if($result->id) {
                            $result2 = $lm->createScheudle($_POST['internalName']);
                            if($result2->id)
                            {
                                Yii::app()->user->setFlash('success', Yii::t('linkerManager', 'Integracja została utworzona!'));
                                $this->redirect(['/linker']);
                                Yii::app()->end();
                            }
                        }

                        Yii::app()->user->setFlash('error', Yii::t('linkerManager', 'Integracja nie została w pełni ukończona! Spróbuj ponownie lub skontaktuj się z nami.'));
                        $this->redirect(['/linker']);
                        Yii::app()->end();
                    } else {
                        $result = $model->submitForm($lm);
                        if ($result) {

                            $model = new LinkerFormBuilder();
                            $formData = $lm->getForm($adapter, 2, $result['internalName']);

                            if($formData->code == 500)
                            {
                                Yii::app()->user->setFlash('error', Yii::t('linkerManager', 'Błąd - prawdopowodnie podano błedne dane dostępowe do integracji!').' <i>[Error 500: "'.$formData->message.'"]</i>');
                                $this->refresh();
                            }
                            else {

                                $model->initialize($lm, $lm->getDepotId(), $adapter);

                                $model->generate($formData);

                                $model->step = 2;
                                $model->step2Id = $result['id'];

                                $model->internalName = $result['internalName'];
                                $model->setAttributes($_POST['LinkerFormBuilder']);
                            }
                        }
                    }
                }
            }
        }
        catch(Exception $ex)
        {
            Yii::app()->user->setFlash('error', $ex->getMessage());
        }

        $this->render('add', array(
            'model' => $model,
        ));
    }

    public function actionIndex()
    {
        try {
            $lm = LinkerManager::init(Yii::app()->user->getModel());
        }
        catch(Exception $ex)
        {
            Yii::app()->user->setFlash('error', $ex->getMessage());
        }

        $this->render('index', array(
            'lm' => $lm,
        ));
    }


}