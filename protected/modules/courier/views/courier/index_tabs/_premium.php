<?php if(Yii::app()->user->getModel()->isPremium):?>

    <div class="alert alert-info"><?php $this->widget('ShowPageContent', ['id' => 28]);?></div>

    <?php echo CHtml::link(Yii::t('courier','Utwórz nową przesyłkę Premium'),array('/courier/courier/createPremium'), array('class' => 'btn btn-primary')); ?>

    <?php echo CHtml::link(Yii::t('courier','Importuj przesyłki Premium z pliku'),array('/courier/importCourierExternalFromFile'), array('class' => 'btn')); ?>
    <br/>
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'export-to-file-premium',
        'action' => Yii::app()->createUrl('/courier/courier/exportToFileAll', ['mode' => Controller::EXPORT_MODE_EXTERNAL]),
        'htmlOptions' => [
            'class' => 'do-not-block',
        ]
    ));
    ?>
    <hr/>
    <?php
    echo TbHtml::submitButton(Yii::t('courier','Eksportuj wszyskie przesyłki do .xls').' :', ['style' => 'margin-top: -6px; margin-right: 5px;']);
    echo Chtml::dropDownList('period', '', S_Useful::listPeriods(), ['style' => 'height: 30px;']);
    $this->endWidget();
    ?>
<?php endif; ?>