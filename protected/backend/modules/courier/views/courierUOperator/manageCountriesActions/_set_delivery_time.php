<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'set-delivery-time',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courierUOperator/manageCouriersSetHoursByGV', ['operator_id' => $operator_id]),
    'htmlOptions' => [
        'class' => 'confirm-me',
    ]
));
?>


<?php echo CHtml::textField('SetHours[hours]',NULL); ?>
<br/>
<?php
echo TbHtml::submitButton('Set', array(
    'onClick' => 'js:
    $(".gv-manage-countries-ids").val($("#gv-manage-countries").selGridView("getAllSelection").toString());
    ;',
    'name' => 'Ids', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>
<?php echo CHtml::hiddenField('SetHours[ids]','', array('class' => 'gv-manage-countries-ids')); ?>
<?php
$this->endWidget();
?>