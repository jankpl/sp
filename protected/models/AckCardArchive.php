<?php

Yii::import('application.models._base.BaseAckCardArchive');

class AckCardArchive extends BaseAckCardArchive
{
    const KEEP_DAYS = 7;

    const TYPE_PDF = 1;
    const TYPE_XLS = 2;

    const FINAL_DIR = 'upadmin/ack_cards/';

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),
        );
    }


    public function rules() {
        return array(
            array('user_id, items, path_pdf, path_xls, hash', 'required'),
            array('user_id, items', 'numerical', 'integerOnly'=>true),
            array('path_pdf, path_xls', 'length', 'max'=>256),
            array('id, date_entered, user_id, items, path_pdf, path_xls, hash, no', 'safe', 'on'=>'search'),
        );
    }

    public static function deleteOldItems()
    {
        $models = self::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY)', [':days' => self::KEEP_DAYS]);

        foreach($models AS $model)
            $model->deleteItem();
    }

    public function deleteItem()
    {
        // always keep at least one item per user
        $cmd = Yii::app()->db->createCommand();
        $count = $cmd->select('COUNT(*)')->from('ack_card_archive')->where('user_id = :user_id', [':user_id' => $this->user_id])->queryScalar();

        if($count <= 1)
            return false;

        @unlink($this->path_xls);
        @unlink($this->path_pdf);

        $this->delete();
    }

    public function beforeSave()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('no');
        $cmd->from('ack_card_archive');
        $cmd->where('user_id = :user_id', array(':user_id' => $this->user_id));
        $cmd->order('no desc');
        $cmd->limit(1);
        $count = $cmd->queryScalar();

        $count = intval($count);

        $count++;
        $this->no = $count;

        return parent::beforeSave();
    }

    public static function archiveForCourier($pdf, array $models)
    {
        if(!Yii::app()->params['frontend'])
            return false;

        $model = new self;
        $hash = hash('sha512',  microtime().uniqid());
        $model->hash = $hash;
        $model->items = S_Useful::sizeof($models);
        $model->user_id = Yii::app()->user->id;

        $xls = S_CourierIO::exportToXlsUser($models, false, true);

        $model->path_xls = self::getFinalDir($hash.'.xls');
        $model->path_pdf = self::getFinalDir($hash.'.pdf');

        file_put_contents($model->path_xls, $xls);
        file_put_contents($model->path_pdf, $pdf);

        return $model->save();
    }

    public static function getFinalDir($fileName)
    {
        $path = realpath(self::FINAL_DIR);
        $path = $path.'/'.$fileName;
        return $path;
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('items', $this->items);
        $criteria->compare('path_pdf', $this->path_pdf, true);
        $criteria->compare('path_xls', $this->path_xls, true);
        $criteria->compare('no', $this->no);

        $sort = new CSort();
        $sort->defaultOrder = 't.date_entered DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
        ));
    }
}