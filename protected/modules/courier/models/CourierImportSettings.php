<?php

Yii::import('application.modules.courier.models._base.BaseCourierImportSettings');

/**
 * Class CourierImportSettings
 *
 * Class for keeping user's OOE import settings (like order of attributes or data separator)
 */
class CourierImportSettings extends BaseCourierImportSettings
{
	const TYPE_OOE = 0;
	const TYPE_INTERNAL = 1;
	const TYPE_U = 2;
	const POSTAL = 10; // used for CustomImportSetting on Postal import as well

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('date_entered', 'default',
				'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
			array('date_updated', 'default',
				'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),

//			array('name', 'required'),
			array('user_id, type', 'required', 'except' => 'universal'),
			array('user_id, type', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('date_updated', 'safe'),
			array('date_updated, user_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name, date_entered, date_updated, data, user_id', 'safe', 'on'=>'search'),
		);
	}

	protected static function serialize($data)
	{
		$data = serialize($data);
		$data = base64_encode($data);
		return $data;
	}

	protected static function unserialize($data)
	{
		$data = base64_decode($data);
		$data = @unserialize($data);
		return $data;
	}

	/**
	 * Returns list of items for user
	 *
	 * @param null $user_id
	 * @param int $type
	 * @return static[]
	 */
	public static function listModelsForUserId($user_id = NULL, $type = self::TYPE_OOE)
	{
		if($user_id === NULL)
			$user_id = Yii::app()->user->id;

		$models = self::model()->findAllByAttributes(['user_id' => $user_id, 'type' => $type], ['order' => 'name DESC']);
		return $models;
	}

	/**
	 * Retrieves model by Id and User ID
	 *
	 * @param $id integer Id of model
	 * @param null $user_id Id of user
	 * @return CourierImportSettings|null
	 */
	public static function getModelForUserId($id, $user_id = NULL)
	{
		if($user_id === NULL)
			$user_id = Yii::app()->user->id;

		$model = self::model()->findByAttributes(['id' => $id, 'user_id' => $user_id]);

		return $model;
	}

	public function afterFind()
	{
		$this->data = self::unserialize($this->data);
		return parent::afterFind();
	}

	public function beforeSave()
	{
		if($this->name == '')
			$this->name = '???';

		$this->data = self::serialize($this->data);
		return parent::beforeSave();
	}

	/**
	 * Safe way of setting item name
	 * @param $name string
	 */
	public function setNameSafe($name)
	{
		$name = strip_tags($name);
		$name = trim($name);
		$name = mb_substr($name,0,45);
		$this->name = $name;
	}
}