<?php

/* @var $models Faq[] */

$this->breadcrumbs = array(
	Faq::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Faq::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Faq::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Faq::label(2)); ?></h1>

<ul>
<?php
foreach($models AS $item):
?>

    <li><?php echo CHtml::link($item->faqTrs->title, '#faq-'.$item->id);?></li>

<?php
endforeach;
?>
</ul>

<ul>
    <?php
    foreach($models AS $item):
        ?>

        <li id="faq-<?php echo$item->id;?>"><h3><?php echo $item->faqTrs->title;?></h3>
        <p>
            <?php echo $item->faqTrs->text;?>
        </p></li>

    <?php
    endforeach;
    ?>

</ul>