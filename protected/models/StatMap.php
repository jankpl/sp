<?php


class StatMap
{
    const MAP_NEW = 1;
    const MAP_ACCEPTED = 2;
    const MAP_DELIVERED_TO_HUB = 10;
    const MAP_IN_TRANSPORTATION = 20;
    const MAP_IN_DELIVERY = 30;
    const MAP_DELIVERED = 40;
    const MAP_PROBLEM = 50;
    const MAP_RETURN = 60;
    const MAP_CANCELLED = 90;

    public static function mapNameList()
    {
        return [
            self::MAP_NEW => Yii::t('statMap', 'Nowe zlecenie'),
            self::MAP_ACCEPTED => Yii::t('statMap', 'Przesyłka została nadana'),
            self::MAP_IN_TRANSPORTATION => Yii::t('statMap', 'W transporcie'),
            self::MAP_DELIVERED_TO_HUB => Yii::t('statMap', 'Dostarczono do HUB-u'),
            self::MAP_IN_DELIVERY => Yii::t('statMap', 'W doręczeniu'),
            self::MAP_DELIVERED => Yii::t('statMap', 'Doręczona'),
            self::MAP_PROBLEM => Yii::t('statMap', 'Problem'),
            self::MAP_RETURN => Yii::t('statMap', 'Zwrot'),
            self::MAP_CANCELLED => Yii::t('statMap', 'Anulowana'),
        ];
    }

    public static function mapCssClassList()
    {
        return [
            self::MAP_NEW => 'new',
            self::MAP_ACCEPTED => 'accepted',
            self::MAP_IN_TRANSPORTATION => 'transportation',
            self::MAP_DELIVERED_TO_HUB => 'hub',
            self::MAP_IN_DELIVERY => 'delivery',
            self::MAP_DELIVERED => 'delivered',
            self::MAP_PROBLEM => 'problem',
            self::MAP_RETURN => 'return',
            self::MAP_CANCELLED => 'cancelled',
        ];
    }
    public static function getMapCssClassById($id)
    {
        return isset(self::mapCssClassList()[$id]) ? self::mapCssClassList()[$id] : '-';
    }


    public static function getMapNameById($id)
    {
        return isset(self::mapNameList()[$id]) ? self::mapNameList()[$id] : '-';
    }

    public static function notifyList()
    {
        return [
            self::MAP_NEW => true,
            self::MAP_ACCEPTED => false,
            self::MAP_IN_TRANSPORTATION => true,
            self::MAP_DELIVERED_TO_HUB => false,
            self::MAP_IN_DELIVERY => true,
            self::MAP_DELIVERED => true,
            self::MAP_PROBLEM => false,
            self::MAP_RETURN => true,
            self::MAP_CANCELLED => false,
        ];
    }

    public static function getNotifyListJustActiveKeys()
    {
        return array_keys(array_filter(self::notifyList(), function($v){
            return $v;
        }));
    }

    protected static function _fetchStatCountData($what, $user_id = false, $forDays = 0, $from = false, $to = false)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*) AS count, stat_map_id');
        if($what == 'courier')
            $cmd->from((new Courier())->tableName());
        else if($what == 'postal')
            $cmd->from((new Postal())->tableName());
        else
            return false;
        $cmd->group('stat_map_id');

        if($user_id)
            $cmd->where('user_id = :user_id', [':user_id' => $user_id]);

        if($forDays)
            $cmd->andWhere('date_entered > (NOW() - INTERVAL :days DAY)', [':days' => $forDays]);
        else if($from && $to)
        {
            $cmd->andWhere('date_entered >= :from', [':from' => $from]);
            $cmd->andWhere('date_entered <= :to', [':to' => $to]);
        }

        $result = $cmd->queryAll();

        $index = $what.'_'.$user_id.'_'.$forDays.'_'.$from.'_'.$to;

        foreach($result AS $item)
        {
            if($item['stat_map_id'] === NULL)
                self::$_statMapCounter[$index]['NULL'] = $item['count'];
            else
                self::$_statMapCounter[$index][$item['stat_map_id']] = $item['count'];
        }

        // fill rest of stat maps with 0 if not found in DB
        foreach(StatMap::mapNameList() AS $key => $name)
        {
            if(!isset(self::$_statMapCounter[$index][$key]))
                self::$_statMapCounter[$index][$key] = 0;
        }

    }


    protected static $_statMapCounter = [];
    protected static function _countItemsForStatMap($stat_map_id, $what, $user_id = false, $forDays = 0, $from = false, $to = false)
    {
        $index = $what.'_'.$user_id.'_'.$forDays.'_'.$from.'_'.$to;

        if($stat_map_id === NULL)
            $stat_map_id = 'NULL';

        if(!isset(self::$_statMapCounter[$index][$stat_map_id]))
        {
            self::_fetchStatCountData($what, $user_id, $forDays, $from, $to);
        }

        return self::$_statMapCounter[$index][$stat_map_id];

    }

    public static function countCouriersForStatMap($stat_map_id, $user_id = false, $forDays = 0, $from = false, $to = false)
    {
        return self::_countItemsForStatMap($stat_map_id, 'courier', $user_id, $forDays, $from, $to);
    }

    public static function countPostalsForStatMap($stat_map_id, $user_id = false, $forDays = 0, $from = false, $to = false)
    {
        return self::_countItemsForStatMap($stat_map_id, 'postal', $user_id, $forDays, $from, $to);
    }


}
