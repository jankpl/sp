<?php
/* @var $this UserDocumentsController */
/* @var $model UserDocuments */

$this->breadcrumbs=array(
    'User Messages'=>array('index'),
    $model->id=>array('view','id'=>$model->id),
    'Update',
);

$this->menu=array(
    array('label'=>'List UserDocuments', 'url'=>array('index')),
    array('label'=>'Create UserDocuments', 'url'=>array('create')),
    array('label'=>'View UserDocuments', 'url'=>array('view', 'id'=>$model->id)),
);
?>

    <h1>Update UserDocuments <?php echo $model->id; ?></h1>

<?php
if($model->user->getDebtBlockActive()):
    ?>
    <div class="alert alert-error">User account is blocked!</div>
    <?php
endif;
?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>