<?php

class S_SystemLoad
{
    const HIGH_LOAD_VALUE = 5.6;
    const EXTREME_LOAD_VALUE = 15;

    public static function get()
    {
        $output = shell_exec('cat /proc/loadavg');
        $loadavg = substr($output,0,strpos($output," "));

        return $loadavg ? floatval($loadavg) : NULL;
    }

    public static function isHigh()
    {
        $current = self::get();

        $is = self::get() > self::HIGH_LOAD_VALUE;

        if($is)
            MyDump::dump('load_high.txt', $current);

        if($current)
            return $is;
        else
            return NULL;
    }

    public static function isExtreme()
    {
        $current = self::get();

        $is = self::get() > self::EXTREME_LOAD_VALUE;

        if($is)
            MyDump::dump('load_extreme.txt', $current);

        if($current)
            return $is;
        else
            return NULL;
    }
}