<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label' =>'Manage ' . $model->label(2), 'url'=>array('admin')),
);
?>

    <h1>Create domestic operator</h1>

<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'modelTrs' => $modelTrs,

));
?>