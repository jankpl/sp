<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-custom-pickup-price-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php $this->widget('PriceForm', array(
            'form' => $form,
            'model' => $model->price,
            'formFieldPrefix' => '',
        ));?>
    </div>


    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'), ['name' => 'CourierCustomPickupPrice']);
    ?>

    <?php
    if($userGroup !== NULL && !$model->isNewRecord):
        ?>
        <br/>
        <br/>
        <div class="alert alert-warning">
            Delete whole pricing for this group.<br/>
            <?php  echo GxHtml::submitButton('Delete', ['name' => 'delete']); ?>
        </div>

        <?php
    endif;
    ?>

    <?php
    $this->endWidget();
    ?>
</div><!-- form -->