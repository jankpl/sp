<?php

class PanelStats
{

    const CACHE_TIME = 300;

    public static function getAverageDeliveryTimeForCourier($user_id, $from = false, $to = false)
    {
        Yii::app()->getModule('courier');


        // OLD WAY:
//        $delivered_stats = CourierStat::getKnownDeliveredStatuses();
//        $delivered_stats = implode(',', $delivered_stats);
//
//        $sql = 'SELECT AVG(time_to_sec(timediff(courier_stat_history.status_date, courier.date_entered))) FROM `courier_stat_history` LEFT JOIN courier ON courier.id = courier_id LEFT JOIN stat_map_mapping ON (type = :type AND courier_stat_history.current_stat_id = stat_map_mapping.stat_id) WHERE (current_stat_id IN ('.$delivered_stats.') OR stat_map_mapping.map_id = :delivered_map) AND courier.user_id = :user_id';
//
//        $params = [];
//        $params[':user_id'] = $user_id;
//        $params[':type'] = StatMapMapping::TYPE_COURIER;
//        $params[':delivered_map'] = StatMap::MAP_DELIVERED;
//
//        if($from && $to) {
//            $sql .= ' AND courier.date_entered BETWEEN :from AND :to';
//            $params[':from'] = $from;
//            $params[':to'] = $to.' 23:59:59';
//        }

//        $result = Yii::app()->db->createCommand($sql)->queryScalar($params);




        $sql = 'SELECT AVG(time_to_sec(timediff(smh_END.stat_date, smh_START.stat_date))), COUNT(courier.id) AS total
FROM stat_map_history smh_START
LEFT JOIN stat_map_history smh_END ON smh_END.type_item_id = smh_START.type_item_id
LEFT JOIN courier ON courier.id = smh_START.type_item_id
WHERE
smh_START.stat_map_id = :stat_map_start AND
smh_END.stat_map_id = :stat_map_end AND
smh_END.stat_date IS NOT NULL AND
smh_START.stat_date IS NOT NULL AND
smh_END.type = :type AND
smh_START.type = :type AND
courier.user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;
        $params[':stat_map_start'] = StatMap::MAP_IN_TRANSPORTATION;
        $params[':stat_map_end'] = StatMap::MAP_DELIVERED;
        $params[':type'] = StatMapMapping::TYPE_COURIER;

        if($from && $to) {
            $sql .= ' AND date(courier.date_entered) BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to;
        }

        $sql .= ' HAVING total > 3';

//        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar($params);
        $result = Yii::app()->db->cache(3600*24)->createCommand($sql)->queryScalar($params);

        $h = floor($result/3600);
        $min = ($result/60)%60;

        if($result < 0)
            $result = false;

        if($result)
            $result = Yii::t('panel', '{h}g. {m}min.', ['{h}' => $h, '{m}' => $min]);
        else
            $result = 'b.d.';

        return $result;
    }

    public static function getTotalDeliveredWeightForCourier($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT SUM(package_weight) FROM courier WHERE stat_map_id = 40 AND courier.user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND courier.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }
        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar($params);

        $result = round($result);

        return $result;
    }

    public static function getHeaviestDeliveredWeightForCourier($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT MAX(package_weight) FROM courier WHERE stat_map_id = 40 AND courier.user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND courier.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar($params);

        $result = round($result);

        return $result;
    }

    public static function getAverageDeliveredWeightForCourier($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT AVG(package_weight) FROM courier WHERE stat_map_id = 40 AND courier.user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar($params);

        $result = round($result);

        return $result;
    }

    public static function getTotalDeliveredForCourier($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT COUNT(id) FROM courier WHERE stat_map_id = 40 AND courier.user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar($params);

        $result = round($result);

        return $result;
    }


    public static function getTotalDeliveredForPostal($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT COUNT(id) FROM postal WHERE stat_map_id = 40 AND postal.user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND postal.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar($params);


        $result = round($result);

        return $result;
    }

    public static function getCountriesStatForCourier($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT country_list.code, COUNT(*) AS no FROM courier JOIN address_data ON courier.receiver_address_data_id = address_data.id JOIN country_list ON country_id = country_list.id WHERE courier.user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND courier.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

        $sql .= ' GROUP BY country_list.code';

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryAll(true, $params);

        return $result;
    }

    public static function getCountriesStatForPostal($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT country_list.code, COUNT(*) AS no FROM postal JOIN address_data ON postal.receiver_address_data_id = address_data.id JOIN country_list ON country_id = country_list.id WHERE postal.user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND postal.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

        $sql .= ' GROUP BY country_list.code';

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryAll(true, $params);

        return $result;
    }


    public static function getLastStatsForCourier($user_id, $lastId = 0, $from = false, $to = false)
    {
        $current_language = intval(Yii::app()->params['language']);

        $sql = 'SELECT courier_stat_history.id, hash, local_id, status_date, courier_stat_history.location, courier_stat_tr.full_text, current_stat_id, time_to_sec(timediff(now(), status_date)) AS sec_ago, name, company, city
FROM `courier_stat_history`
JOIN courier ON courier_id = courier.id
JOIN courier_stat_tr ON (current_stat_id = courier_stat_tr.courier_stat_id AND (courier_stat_tr.language_id = '.$current_language.' OR courier_stat_tr.language_id = 1))
JOIN address_data ON (address_data.id = receiver_address_data_id) 
WHERE courier.user_id = :user_id AND status_date > (NOW() - INTERVAL 24 HOUR)';


        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND courier.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

//        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar($params);


        if($lastId)
            $sql .= 'AND courier_stat_history.id > :last_id ';

        $sql .= 'ORDER BY courier_stat_history.id DESC, FIELD(courier_stat_tr.language_id,'.$current_language.',1)';

        if(!$lastId)
            $sql .= 'LIMIT 10 ';

        if($lastId)
            $params[':last_id'] = $lastId;

        $result = Yii::app()->db->createCommand($sql)->queryAll(true, $params);

        return $result;
    }


    public static function getTotalPackagesForDate($date, $user_id)
    {
        $sql = 'SELECT COUNT(id) FROM courier WHERE date_entered LIKE :date AND user_id = :user_id';
        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar([':user_id' => $user_id, ':date' => $date.'%']);

        $result = round($result);

        return $result;
    }

    public static function getTotalPostalForDate($date, $user_id)
    {
        $sql = 'SELECT COUNT(id) FROM postal WHERE date_entered LIKE :date AND user_id = :user_id';
        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryScalar([':user_id' => $user_id, ':date' => $date.'%']);

        $result = round($result);

        return $result;
    }

    public static function getLastStatsForPostal($user_id, $lastId = 0, $from = false, $to = false)
    {

        $current_language = intval(Yii::app()->params['language']);

        $sql = 'SELECT postal_stat_history.id, hash, local_id, status_date, postal_stat_history.location, postal_stat_tr.full_text, current_stat_id, time_to_sec(timediff(now(), status_date)) AS sec_ago, name, company, city
FROM `postal_stat_history`
JOIN postal ON postal_id = postal.id
JOIN postal_stat_tr ON (current_stat_id = postal_stat_tr.postal_stat_id AND (postal_stat_tr.language_id = '.$current_language.' OR postal_stat_tr.language_id = 1))
JOIN address_data ON (address_data.id = receiver_address_data_id)
WHERE postal.user_id = :user_id AND status_date > (NOW() - INTERVAL 24 HOUR) ';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND postal.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }


        if($lastId)
            $sql .= 'AND postal_stat_history.id > :last_id ';

        $sql .= 'ORDER BY postal_stat_history.id DESC, FIELD(postal_stat_tr.language_id,'.$current_language.',1)';

        if(!$lastId)
            $sql .= 'LIMIT 10 ';

        if($lastId)
            $params[':last_id'] = $lastId;

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryAll(true, $params);

        return $result;
    }

    public static function countPostalWeightTypeStat($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT COUNT(*) AS count, weight_class FROM `postal` WHERE user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND postal.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

        $sql .= ' GROUP BY weight_class';

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryAll(true, $params);

        return $result;
    }

    public static function countCourierWeightStat($user_id, $from = false, $to = false)
    {
        $sql = 'SELECT COUNT(*) AS count, FLOOR(package_weight) AS package_weight FROM `courier` WHERE user_id = :user_id';

        $params = [];
        $params[':user_id'] = $user_id;

        if($from && $to) {
            $sql .= ' AND courier.date_entered BETWEEN :from AND :to';
            $params[':from'] = $from;
            $params[':to'] = $to.' 23:59:59';
        }

        $sql .= ' GROUP BY FLOOR(package_weight)';

        $result = Yii::app()->db->cache(self::CACHE_TIME)->createCommand($sql)->queryAll(true, $params);

        return $result;
    }


}




