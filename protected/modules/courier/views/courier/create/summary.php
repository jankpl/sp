<?php
/* @var $model Courier_CourierTypeInternal */
/* @var $currency integer */
/* @var $codIngnored integer */
?>

<div class="form">

    <h2><?php echo Yii::t('courier','Podsumowanie');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '2/2')); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Parametry przesyłki');?></h4>
            <table class="detail-view" style="width: 100%; margin: 0 auto;">
                <tr class="odd">
                    <th><?php echo $model->courierTypeInternal->getAttributeLabel('with_pickup');?></th>
                    <td><?php echo $model->courierTypeInternal->getWithPickupName(); ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo $model->getAttributeLabel('packages_number');?></th>
                    <td><?php echo $model->packages_number; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('package_weight');?>  [<?php echo Courier::getWeightUnit();?>] </th>
                    <td><?php echo $model->courierTypeInternal->getFinalWeight(); ?> <?= ($model->package_weight != $model->courierTypeInternal->getFinalWeight() ? ' ('.Yii::t('courier', 'deklarowana').': '.$model->package_weight.')' : '');?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('courier','Package dimensions');?> [<?php echo Courier::getDimensionUnit();?>]</th>
                    <td><?php echo $model->package_size_l.' x '.$model->package_size_d.' x '.$model->package_size_w; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('package_value');?></th>
                    <td><?php echo $model->package_value; ?> <?php echo $model->value_currency; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo $model->getAttributeLabel('package_content');?></th>
                    <td><?php echo $model->package_content; ?></td>
                </tr>
                <?php
                $odd = true;
                ?>
                <?php if($model->cod_value):
                    $odd = false;
                    ?>
                    <tr class="odd">
                        <th><?php echo $model->getAttributeLabel('cod_value');?></th>
                        <td><?php echo  S_Price::formatPrice($model->cod_value); ?> <?= $model->findCodCurrency();?></td>
                    </tr>
                <?php endif; ?>
                <?php
                if(!Yii::app()->user->isGuest && Yii::app()->user->getModel()->getCourierRefPrefix()):
                    ?>
                    <tr class="<?= $odd ? 'odd' : 'even';?>">
                        <th><?php echo $model->getAttributeLabel('ref');?></th>
                        <td><?php echo $model->ref; ?></td>
                    </tr>
                    <?php
                    $odd = !$odd;
                endif;
                ?>
                <?php
                if(!Yii::app()->user->isGuest && Yii::app()->user->getModel()->getCourierReturnAvailable()):
                    ?>
                    <tr class="<?= $odd ? 'odd' : 'even';?>">
                        <th><?php echo $model->getAttributeLabel('_generate_return');?></th>
                        <td><?php echo $model->_generate_return ? '&#10004;' : '&#10006'; ?></td>
                    </tr>
                    <?php
                    $odd = !$odd;
                endif;
                ?>
                <?php
                if($model->courierTypeInternal->pickup_point_text):
                    ?>
                    <tr class="<?= $odd ? 'odd' : 'even';?>">
                        <th><?= Yii::t('courier', 'Punkt nadania');?></th>
                        <td><?php echo $model->courierTypeInternal->pickup_point_text; ?></td>
                    </tr>
                    <?php
                    $odd = !$odd;
                endif;
                ?>
                <?php
                if($model->courierTypeInternal->_inpost_locker_delivery_id):
                    ?>
                    <tr class="<?= $odd ? 'odd' : 'even';?>">
                        <th><?= Yii::t('courier', 'Paczkomat docelowy Inpost:');?></th>
                        <td><?php echo $model->courierTypeInternal->_inpost_locker_delivery_id; ?></td>
                    </tr>
                    <?php
                    $odd = !$odd;
                endif;
                ?>
            </table>
        </div>

        <?php
        if(!Yii::app()->user->model->getHidePrices()):
            ?>
            <div class="col-xs-12 col-sm-6">

                <h4><?php echo Yii::t('courier','Podsumowanie');?></h4>
                <table class="detail-view" style="width: 100%;  margin: 0 auto;">
                    <!--        <tr>-->
                    <!--            <td>--><?php ////echo Yii::t('courier','Przesyłek');?><!--</td>-->
                    <!--            <td>--><?php ////echo $model->packages_number; ?><!--</td>-->

                    <!--        </tr>-->
                    <tr class="odd">
                        <th><?php echo Yii::t('courier','Cena za jedną przesyłkę');?></th>
                        <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price_each, true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price_each));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','netto/brutto');?></td>
                    </tr>
                    <tr class="even">
                        <th><?php echo Yii::t('courier','Łączna cena');?></th>
                        <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price,true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice(($price)));?>  <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','netto/brutto');?></td>
                    </tr>

                </table>

                <?php

                /* @var $remoteZipSender CourierDhlRemoteAreas*/
                /* @var $remoteZipReceiver CourierDhlRemoteAreas*/

                $remoteZipSender = false;
                if($model->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR)
                    $remoteZipSender = CourierDhlRemoteAreas::findZip($model->senderAddressData->zip_code, $model->senderAddressData->country_id, $model->senderAddressData->city);

                $remoteZipReceiver = CourierDhlRemoteAreas::findZip($model->receiverAddressData->zip_code, $model->receiverAddressData->country_id, $model->receiverAddressData->city);

                $currencySymbol = Yii::app()->PriceManager->getCurrencySymbol();

                if($remoteZipSender OR $remoteZipReceiver):
                    ?>
                    <h4><?php echo Yii::t('courier','Strefa utrudnionego dostępu - koszt za jedną przesyłkę');?></h4>

                    <table class="detail-view" style="width: 100%;  margin: 0 auto;">

                        <?php
                        if($remoteZipSender):
                            $value = $remoteZipSender->getPrice($currency);

                            ?>
                            <tr class="even">
                                <th><?php echo Yii::t('courier','Adres nadawcy');?></th>
                                <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($value,true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice(($value)));?>  <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','netto/brutto');?></td>
                            </tr>

                        <?php
                        endif;
                        ?>
                        <?php
                        if($remoteZipReceiver):
                            $value = $remoteZipReceiver->getPrice($currency);
                            ?>
                            <tr class="odd">
                                <th><?php echo Yii::t('courier','Adres odbiorcy');?></th>
                                <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($value,true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice(($value)));?>  <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','netto/brutto');?></td>
                            </tr>

                        <?php
                        endif;
                        ?>
                    </table>

                <?php
                endif;
                ?>

                <h4><?php echo Yii::t('courier','Additions');?></h4>
                <table class="detail-view" style="width: 100%; margin: 0 auto;">
                    <tr class="odd">
                        <th style="text-align: left;"><?php echo Yii::t('courier','Nazwa');?></th>
                        <th style="text-align: left;"><?php echo Yii::t('courier','Cena za 1 paczkę');?></th>
                    </tr>
                    <?php

                    /* @var $item CourierAdditionList */
                    if(!S_Useful::sizeof($model->courierTypeInternal->listAdditions(true)))
                        echo '<tr><td colspan="2">'.Yii::t('app','Brak').'</td></tr>';
                    else
                        foreach($model->courierTypeInternal->listAdditions(true) AS $item): ?>
                            <tr class="even">
                                <td><?php echo $item->getClientTitle(); ?></td>
                                <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice(),true));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice()));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','brutto/netto');?></td>
                            </tr>

                        <?php endforeach; ?>
                </table>
            </div>

        <?php
        endif;
        ?>

    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Nadawca');?></h4>

            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));

            ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Odbiorca');?> </h4>
            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));

            ?>
        </div>

    </div>

    <h3 class="text-center"><?php echo Yii::t('site', 'Regulamin');?></h3>

    <div class="row">
        <table class="detail-view">
            <tr class="odd">
                <td><?php echo $form->labelEx($model->courierTypeInternal,'regulations'); ?>
                    <?php echo $form->checkbox($model->courierTypeInternal, 'regulations'); ?>
                    <?php echo $form->error($model->courierTypeInternal,'regulations'); ?></td>
            </tr>
            <tr class="even">
                <td><?php echo $form->labelEx($model->courierTypeInternal,'regulations_rodo'); ?>
                    <?php echo $form->checkbox($model->courierTypeInternal, 'regulations_rodo'); ?>
                    <?php echo $form->error($model->courierTypeInternal,'regulations_rodo'); ?></td>
            </tr>
        </table>


    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step4', 'class' => 'btn btn-sm'));
        echo ' ';
        if(Yii::app()->user->model->getFastFinalizeInternal() && Yii::app()->user->model->payment_on_invoice)
            echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę'),array('name'=>'finish', 'class' => 'btn btn-success'));
        else
            echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę i idź do kasy'),array('name'=>'finish', 'class' => 'btn btn-success'));
        if(Yii::app()->user->model->getFastFinalizeInternal() && Yii::app()->user->model->payment_on_invoice):
            echo ' ';
            echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę i wróć do formularza'),array('name'=>'finish_goback', 'class' => 'btn-primary'));
        endif;
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p><?php echo Yii::t('courier','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>

        <?php if(!Yii::app()->user->isGuest && Yii::app()->user->model->getFastFinalizeInternal() && Yii::app()->user->model->payment_on_invoice):?>
            <br/>
            <div class="alert alert-warning"><?= Yii::t('courier', 'Masz aktywowaną opcję domyślnego wyboru formy płatności {on_invoice} oraz automatycznej finalizacji transakcji.', ['{on_invoice}' => PaymentType::getPaymentTypeName(PaymentType::PAYMENT_TYPE_INVOICE)]);?></div>
        <?php
        else:
            ?>
            <p><?php echo Yii::t('courier','Będzie można je opłacić od razu lub też zrobić to w późniejszym terminie');?>.</p>
        <?php
        endif;
        ?>
    </div>
</div><!-- form -->