<tbody class="_price_item" data-price-id="<?php echo $i; ?>">

<tr>
    <td>
        <?php $this->widget('PriceForm', array(
            'form' => $form,
            'model' => $model->firstPagePrice,
            'formFieldPrefix' => '['.$i.'][firstPagePrice]',
        ));?>
    </td>
    <td style="white-space: nowrap;">
        <?php $this->widget('PriceForm', array(
            'form' => $form,
            'model' => $model->nextPagePrice,
            'formFieldPrefix' => '['.$i.'][nextPagePrice]',
        ));?>
    </td>
</tr>
<tr>
    <td>
        <?php echo $form->error($model,'['.$i.']first_page_price'); ?>
    </td>
    <td>
        <?php echo $form->error($model,'['.$i.']next_page_price'); ?>
    </td>
    <td></td>
</tr>
</tbody>




