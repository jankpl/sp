<?php

class PrintServerQueque extends CApplicationComponent{

    const TABLE_NAME = 'print_server_queque';
    const TYPE_VERIFICATION = 1;

    const TYPE_COURIER = 10;

    const KEEP_AGE_DAYS = 21;

    const STAT_WAITING = 0;
    const STAT_SUCCESS = 1;
    const STAT_FAILED = 9;

    public function init()
    {
        return parent::init();
    }

    public function scheudleItem($type, $item_id, $user_id, $address)
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->insert(self::TABLE_NAME, [
                'item_type' => $type,
                'item_id' => $item_id,
                'user_id' => $user_id,
                'address' => $address,
            ]
        );
    }

    /**
     * Async call for print call. Allow ordering print in background
     * @param int $id Id of CourierLabelNew to be called
     * @param int $type Type of ID
     * @param int $counter Limit number of operations
     * @param int $mdelay Time to wait before label ordering [seconds]
     */
    public static function asyncCallForId($id, $type, $ignoreLimitation = false)
    {

        MyDump::dump('ps-async.txt', print_r($id,1).','.print_r($type,1));

        if(!$ignoreLimitation) {

//        save into cache with date and minute in name _user _id//
            $CACHE_NAME = 'ASYNC_PRINT_SERVER_QUEQUE_' . Yii::app()->params['console'] ? '_' : Yii::app()->user->id . '_' . date('Ymdhi');
            $check = Yii::app()->cache->get($CACHE_NAME);

            if ($check === false) {
                Yii::app()->cache->set($CACHE_NAME, 0, 60);
            } else {

                // limit number of async calls per one minute for X times:
                if ($check === 30) {
                    return false;
                }
                Yii::app()->cache->set($CACHE_NAME, (intval($check) + 1), 60);
            }
        }

        $url = Yii::app()->createAbsoluteUrl('printServerQueque/index', array('id' => $id, 'type' => $type, 'mode' => 0, 'async' => 'true'), 'http');
        $url = str_replace('nysa.php/', '', $url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec($ch);

        curl_close($ch);
    }

    public function run($limit = 0, $itemId = 0, $itemType = false)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd = $cmd->select('id, item_type, item_id, address')
            ->from(self::TABLE_NAME)
            ->where('stat = '.self::STAT_WAITING);

        if($itemId && $itemType)
            $cmd = $cmd->andWhere('item_id = :item_id AND item_type = :type', [':item_id' => $itemId, ':type' => $itemType]);
        else
            $cmd = $cmd->order('id ASC');

        if($limit)
            $cmd = $cmd->limit($limit);

        $result = $cmd->queryAll();

        foreach($result AS $item)
            $this->_runItem($item);

    }

    protected function _runItem($item)
    {
        $CACHE_NAME = 'printServerQueque_' . $item['id'];

        // PRINT IS ALREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if (Yii::app()->cache->get($CACHE_NAME) !== false) {
            MyDump::dump('ps-queque-pd.txt','-- PREVENT DUPLICATE:'.$item['id']);
            return false;
        }
        Yii::app()->cache->set($CACHE_NAME, 10, 60 * 5);

        if($item['item_type'] == self::TYPE_VERIFICATION)
            $response = $this->runTypeVerification($item['item_id'], $item['address']);
        else if($item['item_type'] == self::TYPE_COURIER)
            $response = $this->runTypeCourier($item['item_id'], $item['address']);
        else
            $response = false;

        $cmd = Yii::app()->db->createCommand();
        $cmd->update(self::TABLE_NAME, array('stat' => $response ? self::STAT_SUCCESS : self::STAT_FAILED, 'date_updated' => new CDbExpression('NOW()')), 'id = :id', array(':id' => $item['id']));
    }

    public function howLong()
    {
        $cmd = Yii::app()->db->createCommand();
        $result = $cmd->select('COUNT(*)')
            ->from(self::TABLE_NAME)
            ->where('stat = '.self::STAT_WAITING)->queryScalar();

        return $result;
    }

    protected function runTypeVerification($item_id, $address)
    {

        /* @var $model PrintServer */
        $model = PrintServer::model()->findByPk($item_id);

        if($model) {
            $data = $model->generateVerificationMessage();
            $success = PrintServer::sendToPrinter($data, $address);

            return $success;
        }

        return false;
    }


    protected function runTypeCourier($item_id, $address)
    {
        Yii::app()->getModule('courier');

        $model = CourierLabelNew::model()->findByPk($item_id);
        $labels = $model->getFileAsString(true, false, true);

        if(is_array($labels) && S_Useful::sizeof($labels))
        {
            $success = false;
            foreach($labels AS $label)
            {
                $zpl = PrintServer::stringImgToZpl($label);
                $success = PrintServer::sendToPrinter($zpl, $address);
            }
            return $success;

        } else
            return false;
    }

    public static function instantRunImgString($img, $address)
    {
        $zpl = PrintServer::stringImgToZpl($img);
        return PrintServer::sendToPrinter($zpl, $address);
    }

    public function clearOldAndFinished()
    {
        if(self::KEEP_AGE_DAYS > 0)
        {
            $cmd = Yii::app()->db->createCommand();
            return $cmd->delete(self::TABLE_NAME, 'stat > '.self::STAT_WAITING.' AND date_updated < (NOW() - INTERVAL :days DAY)', [':days' => self::KEEP_AGE_DAYS]);
        }
        return false;
    }

}