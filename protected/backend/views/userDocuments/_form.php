<?php
/* @var $this UserDocumentsController */
/* @var $model UserDocuments */
/* @var $form CActiveForm */

$readyOnly = false;
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(

        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div style="overflow: auto;">
        <div style="float: left; width: 48%;">
            <div class="row">
                <?php echo $form->labelEx($model,'user_id'); ?>
                <?php echo $form->dropDownList($model,'user_id', CHtml::listData(User::model()->findAll(),'id','login'), ['prompt' => '-', 'disabled' => $readyOnly, 'data-user-selector' => 'true']); ?>
                <?php echo $form->error($model,'user_id'); ?>
            </div>

            <?php
            if($model->isNewRecord):
                ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'_file'); ?>
                    <?php echo $form->fileField($model, '_file'); ?>
                    <?php echo $form->error($model,'_file'); ?>
                </div>
                <?php
            else:
                ?>
                <?php if($model->file_name != ''): ?>
                <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file ', array('/userDocuments/getFile', 'id' => $model->id), array('class' => 'btn btn-primary')), array('closeText' => false));?>
            <?php endif; ?>
                <?php
            endif;
            ?>

            <div class="row">
                <?php echo $form->labelEx($model,'type'); ?>
                <?php echo $form->dropDownList($model,'type', UserDocuments::getTypeList(), ['prompt' => '-']); ?>
                <?php echo $form->error($model,'type'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'stat'); ?>
                <?php echo $form->dropDownList($model,'stat', [0 => 'Inactive', 1 => 'Active']); ?>
                <?php echo $form->error($model,'stat'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'desc'); ?>
                <?php echo $form->textField($model,'desc', ['disabled' => $readyOnly]); ?>
                <?php echo $form->error($model,'desc'); ?>
            </div>





        </div>
        <div style="float: right; width: 48%;">
            <div class="row">
                <?php echo $form->labelEx($model,'customer_visible'); ?>
                <?php echo $form->dropDownList($model,'customer_visible', [0 => 'Only admins', 1 => 'Admins & Customers']); ?>
                <?php echo $form->error($model,'customer_visible'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'desc_customer'); ?>
                <?php echo $form->textField($model,'desc_customer'); ?>
                <?php echo $form->error($model,'desc_customer'); ?>
            </div>
        </div>
    </div>
    <hr/>

    <div class="row buttons text-center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn-primary btn-large']); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->