<?php
/* @var $model Courier */
/* @var $form TbActiveForm */
/* @var $additionsHtml string */
/* @var $noPickup boolean */
/* @var $noCarriage boolean */
/* @var $codCurrency false|string */
/* @var $codIgnored integer */
/* @var $deliveryOperatorBrokerId integer */
?>
<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);

Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
?>

<h2><?php echo Yii::t('courier', 'Create Courier External Return'); ?> | <?= Yii::t('app','Krok {step}', array('{step}' => '1/2')); ?></h2>
<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-internal-form',
        'enableClientValidation' => true,
        'stateful'=>true,
//        'htmlOptions' => [
//            'class' => 'form-horizontal',
//        ],
    ));
    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-info" id="show-desc">
                <?php
                $this->widget('ShowPageContent', array('id' => 54));
                ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Parametry paczki');?></legend>

                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'package_weight'); ?>
                        <?php echo $form->textField($model, 'package_weight'); ?> <?= Courier::getWeightUnit();?>
                        <?php echo $form->error($model,'package_weight'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model->courierTypeExternalReturn->courierLabelNew,'_ext_operator_name'); ?>
                        <?php echo $form->textField($model->courierTypeExternalReturn->courierLabelNew, '_ext_operator_name'); ?>
                        <?php echo $form->error($model->courierTypeExternalReturn->courierLabelNew,'_ext_operator_name'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model->courierTypeExternalReturn->courierLabelNew,'track_id'); ?>
                        <?php echo $form->textField($model->courierTypeExternalReturn->courierLabelNew, 'track_id'); ?>
                        <?php echo $form->error($model->courierTypeExternalReturn->courierLabelNew,'track_id'); ?>
                    </div>
                </div>

            </fieldset>
        </div>
    </div>
    <div class="row">
        <?php
        $this->Widget('AddressDataSwitch', ['attrBaseName' => 'CourierTypeExternalReturn_AddressData']);
        ?>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('courier', 'Zwrot od');?></legend>
                <?php
                $this->renderPartial('_addressData/_form',
                    array(
                        'model' => $model->senderAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_SENDER),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => UserContactBook::TYPE_SENDER,
                        'type' => UserContactBook::TYPE_SENDER,
//                    'focusFirst' => true,
                    ));
                ?>
            </fieldset>
        </div>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('courier', 'Zwrot do');?></legend>
                <?php
                $this->renderPartial('_addressData/_form',
                    array(
                        'model' => $model->receiverAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_RECEIVER),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => UserContactBook::TYPE_RECEIVER,
                        'type' => UserContactBook::TYPE_RECEIVER,
//                    'focusFirst' => true,
                    ));
                ?>
            </fieldset>


        </div>
    </div>
    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>
    <div class="row" style="padding: 10px 0;">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Kontynuuj');?></legend>
                <div class="text-center">
                    <?php echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('name' => 'summary', 'class' => 'btn-lg btn-primary', 'disabled' => ($noCarriage OR $noPickup) ? 'disabled' : '', 'data-continue-button' => 'true')); ?>
                </div>
            </fieldset>
        </div>
    </div>
    <?= $form->hiddenField($model, 'source'); // for cloning ?>
    <?php $this->endWidget(); ?>
</div>
