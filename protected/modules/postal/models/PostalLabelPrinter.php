<?php

class PostalLabelPrinter
{
    const PDF_4_ON_1 = 1;
    const PDF_2_ON_1 = 2;
    const PDF_ROLL = 3;
    const PDF_A4_WINDOW_LEFT = 4;
    const PDF_A4_WINDOW_RIGHT = 5;
    const PDF_C4 = 6;
    const PDF_C5 = 7;
    const PDF_C6 = 8;
    const PDF_DL = 9;

    const PDF_2_ON_1_ONE_RIGHT = 10;
    const PDF_ROLL_82X65 = 11;

    const URL = 12;

    const PDF_ROLL_WITH_PACKAGE_SLIP = 13;


    /**
     * @param $type int Type of label format
     * @return array Return arrays containing [width, height] in mm
     */
    protected static function _pageDimension($type)
    {
        $data = [
            self::PDF_4_ON_1 => [210, 297],
            self::PDF_2_ON_1 => [210, 297],
            self::PDF_ROLL => [100,150],
            self::PDF_A4_WINDOW_LEFT => [210, 297],
            self::PDF_A4_WINDOW_RIGHT => [210, 297],
            self::PDF_C4 => [229, 324],
            self::PDF_C5 => [162, 229],
            self::PDF_C6 => [114, 162],
            self::PDF_DL => [110, 220],
            self::PDF_2_ON_1_ONE_RIGHT => [210, 297],
            self::PDF_ROLL_82X65 => [65, 82],
            self::PDF_ROLL_WITH_PACKAGE_SLIP => [100, 150],
        ];

        return $data[$type];
    }

    public static function getTypeNames()
    {
        $data = [];
        if(!Yii::app()->user->isGuest && in_array(Yii::app()->user->id, [8, 2537]))
            $data[self::PDF_ROLL_WITH_PACKAGE_SLIP] = Yii::t('postal', 'Roll with packaging slip');

        $data += [
            self::PDF_ROLL => Yii::t('postal', 'Roll'),
            self::PDF_4_ON_1 => Yii::t('postal', '4 on 1'),
            self::PDF_2_ON_1 => Yii::t('postal', '2 on 1'),
            self::PDF_A4_WINDOW_LEFT => Yii::t('postal', 'A4 left window'),
            self::PDF_A4_WINDOW_RIGHT => Yii::t('postal', 'A4 right window'),
            self::PDF_C4 => Yii::t('postal', 'C4'),
            self::PDF_C5 => Yii::t('postal', 'C5'),
            self::PDF_C6 => Yii::t('postal', 'C6'),
            self::PDF_DL => Yii::t('postal', 'DL'),
            self::PDF_2_ON_1_ONE_RIGHT => Yii::t('postal', '2 on 1 - just right'),
            self::PDF_ROLL_82X65 => Yii::t('postal', 'Roll 82x65'),
            self::URL => Yii::t('postal', 'URL2Roll'),
        ];


        return $data;
    }

    static protected function _oversizedInfo($pdf, Postal $item, $blockWidth, $blockHeight, $xBase, $yBase)
    {
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(60, 40, Yii::t('postal',"{no}Ta etykieta nie mogła zostać wyskalowana do wybranego przez Ciebie formatu i została przeniesiona w formacie A4 na kolejną stronę. Przepraszamy za utrudnienia.", ['{no}' => "#$item->local_id\r\n\r\n"]),1,'C', false, 0, $xBase, $yBase, true, 0, false, true,40, 'M');

        return $pdf;
    }

    static public function generateLabels($models, $type, $noError = true, $asPngString = false, $asPdfString = false, $resizeForZebra = false, TcPdf $ownPdf = NULL, $forceSubsetting = false)
    {
        $done = [];


        if(!is_array($models))
            $models = [$models];
//        try
//        {
        /* @var $pdf TCPDF */

        if($ownPdf === NULL) {
            $pdf = PDFGenerator::initialize();
        } else {
            $pdf = $ownPdf;
        }

        if($forceSubsetting)
            $pdf->setFontSubsetting(true);

        // queque for oversized labels;
        $labelQueque = [];

        // used when $asPngString == true
        $returnedImages = [];

        $error = false;
        switch ($type) {
            case self::PDF_4_ON_1:
            default:
                $pdf = self::_pdf_4_on_1($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_2_ON_1:
                $pdf = self::_pdf_2_on_1($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_ROLL:
                $pdf = self::_pdf_roll($pdf, $models, $noError, $labelQueque, $done, $asPngString, $returnedImages);
//                $pdf = self::_pdf_roll($pdf, $models, $noError, $labelQueque, $done, false, $returnedImages);
                break;
            case self::PDF_A4_WINDOW_LEFT:
                $pdf = self::_pdf_a4_window_left($pdf, $models, $noError, $labelQueque, $done);;
                break;
            case self::PDF_A4_WINDOW_RIGHT:
                $pdf = self::_pdf_a4_window_right($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_C4:
                $pdf = self::_pdf_c4($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_C5:
                $pdf = self::_pdf_c5($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_C6:
                $pdf = self::_pdf_c6($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_DL:
                $pdf = self::_pdf_dl($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_2_ON_1_ONE_RIGHT:
                $pdf = self::_pdf_2_on_1_one_right($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_ROLL_82X65:
                $pdf = self::_pdf_roll_82x65($pdf, $models, $noError, $labelQueque, $done);
                break;
            case self::PDF_ROLL_WITH_PACKAGE_SLIP:
                $pdf = self::_pdf_roll($pdf, $models, $noError, $labelQueque, $done, $asPngString, $returnedImages, true);
                break;
            case self::URL:
                $lu = LabelsUrl::genrateNewLink(LabelsUrl::TYPE_POSTAL, CHtml::listData($models,'id', 'id'), self::PDF_ROLL);
                Yii::app()->controller->redirect($lu->getShowUrl());
                Yii::app()->end();
                break;
        }

        // print oversized labels or additional pages at the end
        if(S_Useful::sizeof($labelQueque)) {
            $dummyQueque = [];

            // print queque in 2 on 1 format
            $pdf = self::_pdf_2_on_1($pdf, $labelQueque, $noError, $dummyQueque);
        }

        if($error)
            throw new CHttpException(403, Yii::t('courier', 'Wystąpił problem z Twoją etykietą. Skontaktuj się z nami w tej sprawie!'));
        else {


            if($asPngString) {

                $return = [];

                foreach($returnedImages AS $key => $image)
                {
                    $im = ImagickMine::newInstance();

                    $im->readImageBlob($image);
                    $im->setImageFormat('png8');
//                    $im->scaleImage(0,1500);

                    if ($im->getImageAlphaChannel()) {
                        $im->setImageBackgroundColor('white');
                        $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                    }

                    if($resizeForZebra)
                        $im->scaleImage(777,1166, true);

                    $im->stripImage();

                    $return[] = $im->getImageBlob();
                }


                return $return;
            }

//            if($asPngString) {
//
//                $return = [];
//
//                $pages = $pdf->getNumPages();
//                $pdf = $pdf->Outupt('labels.pdf', 'S');
//
//                $im = ImagickMine::newInstance();
//                $im->setResolution(300, 300);
//
//                if ($pages > 1) {
//
//                    $basePath = Yii::app()->basePath . '/../uplabelsPostal/temp/';
//                    $tempFileName = $basePath . mktime() . '.pdf';
//                    @file_put_contents($tempFileName, $pdf);
//                    for ($i = 0; $i < $pages; $i++) {
//                        try {
//                            $im->readImage($tempFileName . '[' . $i . ']');
//                        } catch (Exception $ex) {
//                            continue;
//                        }
//
//                        $im->setImageFormat('png8');
//
//                        if ($im->getImageAlphaChannel()) {
//                            $im->setImageBackgroundColor('white');
//                            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//                        }
//                        $im->stripImage();
//                        $return[] = $im->getImageBlob();
//                    }
//
//                    @unlink($tempFileName);
//                } else {
//                    $im->readImageBlob($pdf);
//                    $im->setImageFormat('png8');
//
//                    if ($im->getImageAlphaChannel()) {
//                        $im->setImageBackgroundColor('white');
//                        $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//                    }
//
//                    $im->stripImage();
//                    $return[] = $im->getImageBlob();
//                }
//            }
            else {

                if($ownPdf ) {
                    return $pdf;
                }
                else if($asPdfString)
                {
                    return $pdf->Output('labels.pdf', 'S');
                }
                else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                    $pdf->Output('labels.pdf', 'I');
                else
                    $pdf->Output('labels.pdf', 'D');
                return $done;
            }

        }

//        }
//        catch(CHttpException $ex) {
//            throw new CHttpException('403', $ex->getMessage());
//        }
    }


    static protected function _pdf_4_on_1($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {

        $pageW = self::_pageDimension(self::PDF_4_ON_1)[0];
        $pageH = self::_pageDimension(self::PDF_4_ON_1)[1];

        $xBase = 5;
        $yBase = 5;

        $blockWidth = ($pageW / 2) - (2 * $xBase);
        $blockHeight = ($pageH / 2) - (2 * $yBase);

        $rightBlockXPosition = $blockWidth + 3 * $xBase;
        $bottomBlockYPosition = $blockHeight + 3 * $yBase;

        $totalCounter = 0;

        $j = 0;

        /* @var $item Postal */
        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $x = ($j%2 == 0) ? $xBase : $rightBlockXPosition;
                $y = ($j%4 == 2 OR $j%4 == 3)? $y = $bottomBlockYPosition : $yBase;

                if($j%4 == 0)
                {
                    $pdf->AddPage('H','A4');
                }

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $x, $y, ($blockWidth), ($blockHeight ), $item, self::PDF_4_ON_1, $noError, $labelQueque, $localLabelQueque, $forceImage, PostalKaabLabel::TYPE_100x150);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));

        }

        return $pdf;
    }

    static protected function _pdf_2_on_1($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_2_ON_1)[1];
        $pageH = self::_pageDimension(self::PDF_2_ON_1)[0];

        $xBase = 5;
        $yBase = 5;

        $blockWidth = ($pageW / 2) - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $rightBlockXPosition = $blockWidth + 3 * $xBase;

        $totalCounter = 0;

        $j = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $x = ($j%2 == 0) ? $xBase : $rightBlockXPosition;

                if($j%2 == 0)
                {
                    $pdf->AddPage('L','A4');
                }

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $x, $yBase, $blockWidth, $blockHeight, $item, self::PDF_2_ON_1, $noError, $labelQueque, $localLabelQueque, $forceImage, PostalKaabLabel::TYPE_100x150);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_2_on_1_one_right($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_2_ON_1)[1];
        $pageH = self::_pageDimension(self::PDF_2_ON_1)[0];

        $xBase = 5;
        $yBase = 5;

        $blockWidth = ($pageW / 2) - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $rightBlockXPosition = $blockWidth + 3 * $xBase;

        $totalCounter = 0;

        $j = 0;

        /* @var $item Postal */
        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            $forceRotate = false;

            // special rule for this operators because label is very wide
            if(isset($item->postalOperator->_ooe_service) && in_array($item->postalOperator->_ooe_service, ['REGPOSTCZINT', 'REGPOSTCZEUR']))
                $forceRotate = true;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $x = $rightBlockXPosition;
                $pdf->AddPage('L','A4');

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $x, $yBase, $blockWidth, $blockHeight, $item, self::PDF_2_ON_1_ONE_RIGHT, $noError, $labelQueque, $localLabelQueque, $forceImage, PostalKaabLabel::TYPE_100x150, 'CT', $forceRotate);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_roll($pdf, $models, $noError, &$labelQueque = [], &$done = [], $returnImagesArray = false, &$returnedImages = [], $withPackageSlip = false)
    {
        $pageW = self::_pageDimension(self::PDF_ROLL)[0];
        $pageH = self::_pageDimension(self::PDF_ROLL)[1];

        $xBase = 2;
        $yBase = 2;

        $blockWidth = $pageW - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $j = 0;

        $totalCounter = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $pdf->AddPage('H',array($pageW,$pageH), true);

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $blockWidth, $blockHeight, $item, self::PDF_ROLL, $noError, $labelQueque, $localLabelQueque, $forceImage, PostalKaabLabel::TYPE_100x150, 'CM', false, $returnImagesArray, $returnedImages, true);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                if($withPackageSlip)
                    $pdf = PackageSlipLabel::generateReminderPdfPage($pdf, $item);

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_roll_82x65($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_ROLL_82X65)[0];
        $pageH = self::_pageDimension(self::PDF_ROLL_82X65)[1];

        $xBase = 0;
        $yBase = 0;

        $blockWidth = $pageW - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $j = 0;

        $totalCounter = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $pdf->AddPage('H',array($pageW,$pageH), true);

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $blockWidth, $blockHeight, $item, self::PDF_ROLL_82X65, $noError, $labelQueque, $localLabelQueque, $forceImage);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_a4_window_left($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_A4_WINDOW_LEFT)[0];
        $pageH = self::_pageDimension(self::PDF_A4_WINDOW_LEFT)[1];

        $blockWidth = 90;
        $blockHeight = 45;

        $xBase = 23;
        $yBase = 43;

        $j = 0;

        $totalCounter = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {
                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $pdf->AddPage('H','A4');

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $blockWidth, $blockHeight, $item, self::PDF_A4_WINDOW_LEFT, $noError, $labelQueque, $localLabelQueque, $forceImage, PostalKaabLabel::TYPE_90x45);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_a4_window_right($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_A4_WINDOW_RIGHT)[0];
        $pageH = self::_pageDimension(self::PDF_A4_WINDOW_RIGHT)[1];

        $blockWidth = 90;
        $blockHeight = 45;

        $xBase = 210 - $blockWidth - 23;
        $yBase = 43;

        $j = 0;

        $totalCounter = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $pdf->AddPage('H','A4');

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $blockWidth, $blockHeight, $item, self::PDF_A4_WINDOW_RIGHT, $noError, $labelQueque, $localLabelQueque, $forceImage, PostalKaabLabel::TYPE_90x45);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_c4($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_C4)[0];
        $pageH = self::_pageDimension(self::PDF_C4)[1];

        $xBase = 20;
        $yBase = $pageH - 82 - 20;

        $j = 0;

        $totalCounter = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $pdf->AddPage('H',array($pageW,$pageH), true);

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $pageW, $pageH, $item, self::PDF_C4, $noError, $labelQueque, $localLabelQueque, $forceImage);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_c5($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_C5)[0];
        $pageH = self::_pageDimension(self::PDF_C5)[1];

        $xBase = 20;
        $yBase = $pageH - 82 - 20;

        $j = 0;

        $totalCounter = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $pdf->AddPage('H',array($pageW,$pageH), true);

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $pageW, $pageH, $item, self::PDF_C5, $noError, $labelQueque, $localLabelQueque, $forceImage);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_c6($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_C6)[0];
        $pageH = self::_pageDimension(self::PDF_C6)[1];

        $xBase = 20;
        $yBase = $pageH - 82 - 20;

        $j = 0;

        $totalCounter = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $pdf->AddPage('H',array($pageW,$pageH), true);

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $pageW, $pageH, $item, self::PDF_C6, $noError, $labelQueque, $localLabelQueque, $forceImage);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _pdf_dl($pdf, $models, $noError, &$labelQueque = [], &$done = [])
    {
        $pageW = self::_pageDimension(self::PDF_DL)[0];
        $pageH = self::_pageDimension(self::PDF_DL)[1];

        $xBase = 20;
        $yBase = $pageH - 82 - 20;

        $j = 0;

        $totalCounter = 0;

        foreach($models AS $key => $item)
        {
            $localLabelQueque = [];
            $forceImage = false;

            do {

                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('postal','Too many labels!'));

                $pdf->AddPage('H',array($pageW,$pageH), true);

                if(S_Useful::sizeof($localLabelQueque))
                    $forceImage = array_shift($localLabelQueque);

                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $pageW, $pageH, $item, self::PDF_DL, $noError, $labelQueque, $localLabelQueque, $forceImage);

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;
            } while (S_Useful::sizeof($localLabelQueque));


        }

        return $pdf;
    }

    static protected function _labelBody($key, &$done, TCPDF $pdf, $xBase, $yBase, $blockWidth, $blockHeight, Postal $item, $type, $noError = false, &$labelQueque = [], &$localLabelQueque = [], $forceImage = false, $postalKaabLabelType = PostalKaabLabel::TYPE_82x65, $positionOnPage = 'CM', $forceRotate = false, $returnImagesArray = false, &$returnedImages = [], $returnPdf = false)
    {

        if($forceImage)
        {
            // used with local label queque, when label has more than one page (for example CN23)

            $image = $forceImage;
            $imgW = $blockWidth;
            $imgH = $blockHeight;

        } else {
            if ($item->bulk)
                return $pdf;

            // add to queque oversized labels
            if (
                ($item->postalOperator->label_mode == PostalOperator::LABEL_MODE_WHOLE_EXTERNAL && $item->postalLabel !== NULL && !in_array($type, [self::PDF_4_ON_1, self::PDF_2_ON_1, self::PDF_ROLL, self::PDF_2_ON_1_ONE_RIGHT]))
                OR
                (in_array($item->postalOperator->label_mode, [PostalOperator::LABEL_MODE_CN23, PostalOperator::LABEL_MODE_CN22_CPOST]) && in_array($type, [self::PDF_ROLL_82X65, self::PDF_A4_WINDOW_LEFT, self::PDF_A4_WINDOW_RIGHT]))
            )
            {
                // PRINT INFO ABOUT NOT SCALING
                $pdf = self::_oversizedInfo($pdf, $item, $blockWidth, $blockHeight, $xBase, $yBase);
                array_push($labelQueque, $item);
                return $pdf;
            }



            if ($item->postalOperator->label_mode == PostalOperator::LABEL_MODE_WHOLE_EXTERNAL && $item->postalLabel !== NULL) {

                if($item->postal_stat_id == PostalStat::CANCELLED_PROBLEM)
                    $image = ProblemLabel::generatePostal($item, $item->postalLabel->log);
                else
                    $image = $item->postalLabel->getFileAsString($noError, $forceRotate);

                $imgW = $blockWidth;
                $imgH = $blockHeight;

            } else if ($item->postalOperator->label_mode == PostalOperator::LABEL_MODE_CN23) {

                $imgW = $blockWidth;
                $imgH = $blockHeight;

                if($item->postal_stat_id == PostalStat::CANCELLED_PROBLEM)
                    $image = ProblemLabel::generatePostal($item, $item->postalLabel->log);
                else {
                    $image = PostalCN23::generate($item, false);

                    $localLabelQueque[] = PostalCN23::generate($item, true);
                }
            } else if ($item->postalOperator->label_mode == PostalOperator::LABEL_MODE_CN22_CPOST) {

                $imgW = $blockWidth;
                $imgH = $blockHeight;

                if($item->postal_stat_id == PostalStat::CANCELLED_PROBLEM)
                    $image = ProblemLabel::generatePostal($item, $item->postalLabel->log);
                else {
                    $image = CzechPostClient::generateNonRegLabel($item, $item->external_id, false, NULL, true,  false);
                }

            } else {
                if ($postalKaabLabelType == PostalKaabLabel::TYPE_82x65) {
                    $imgW = 82;
                    $imgH = 65;
                } else if ($postalKaabLabelType == PostalKaabLabel::TYPE_100x150) {
                    $imgW = 140;
                    $imgH = 90;
                } else {
                    $imgW = 90;
                    $imgH = 45;
                }

                $rotate = false;
                if ($blockWidth < $blockHeight) {
                    $rotate = true;
                    $temp = $imgW;
                    $imgW = $imgH;
                    $imgH = $temp;
                }


                if($item->postal_stat_id == PostalStat::CANCELLED_PROBLEM)
                    $return = ProblemLabel::generatePostal($item, $item->postalLabel->log, $returnPdf && !$returnImagesArray ? $pdf : false);
                else
                    $return = PostalKaabLabel::generate($item, false, $rotate, $postalKaabLabelType, $returnPdf && !$returnImagesArray ? $pdf : false);

                if($returnPdf && !$returnImagesArray) // for ROLL format return PDF handler, not image
                {
                    $done[$key] = $item->id;
                    return $return;
                }
                else
                    $image = $return;

            }
        }

        $done[$key] = $item->id;

        if($returnImagesArray)
        {
            $returnedImages[] = $image;
            return $pdf;
        }


// PRINT IMAGE TO PDF
        $pdf->Image(
            '@' . $image, // file
            $xBase, // x
            $yBase, // y
            $imgW, // w
            $imgH, // h
            '', // type
            '', // link
            '', // align
            false, // resize
            300, // dpi
            '', // palign
            false, // ismask
            false, // imgmask
            0, // border
            $positionOnPage, // fitbox
            false, // hidden
            false, // fitonpage
            false, // alt
            array() // altimgs
        );


// RETURN PDF
        return $pdf;
    }


}