<?php

class PostalController extends Controller
{

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function beforeAction($action)
    {
        if(Postal::isAvailableForUser() OR $action->id == 'ajaxGetPrice')
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('postal', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Postal',

            ));
            exit;
        }
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('ajaxGetPrice'),
                'users'=>array('*'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        $this->redirect(['postal/list']);

//        $this->panelLeftMenuActive = 200;

//        $this->render('index',
//            []
//        );
    }

    public function actionAck()
    {
        $this->panelLeftMenuActive = 240;

        $this->render('ack',
            []
        );
    }

    public function actionExport()
    {
        $this->panelLeftMenuActive = 242;


        $this->render('export',
            []
        );
    }

    public function actionAjaxGetPrice()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $postal_type = $_POST['postal_type'];
            $weight = $_POST['weight'];
            $size_class = $_POST['size_class'];

            $force_user_id = $_POST['force_user_id'];

            $currency = $_POST['currency'];

            $receiver_country_id =  $_POST['receiver_country_id'];

            $noTax = $_POST['noTax'] ? true : false;

            if(intval($force_user_id) > 0)
                $user_id = $force_user_id;
            else
                $user_id = false;

            if($currency != '')
                Yii::app()->PriceManager->setCurrency($currency);

            $postal = new Postal;

            if($user_id)
                $postal->user_id = $user_id;
            else
                $postal->user_id = Yii::app()->user->id;

            $weight_class = Postal::getWeightClassForWeight($weight);

            $postal->postal_type = $postal_type;
            $postal->weight_class = $weight_class;
            $postal->size_class = $size_class;
            $postal->receiver_country_id = $receiver_country_id;

            $currencySymbol = Yii::app()->PriceManager->getCurrencyCode();

            $price = $postal->getPrice(false, false, $user_id);

            if ($price === NULL)
                $available = false;
            else {
                $available = true;
                $price = Yii::app()->PriceManager->getFinalPrice($price, $noTax) . ' ' . $currencySymbol;
            }

            echo CJSON::encode([
                    'available' => $available,
                    'price' => $price,
                ]
            );
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }
    }





///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

    /**
     * Fills form for creating new packages based on old ones.
     *
     * @param $urlData string Hash of package that will be base on new one
     * @throws CHttpException
     */
    public function actionClone($urlData)
    {
        $model = Postal::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->findByAttributes(['hash' => $urlData]);

        if($model === NULL)
            throw new CHttpException(404);

        if($model->bulk)
            throw new CHttpException(403);


        $model->source = Postal::SOURCE_CLONE;

        Yii::app()->user->setFlash('success', Yii::t('postal', 'Dane paczki zostały sklonowane. {br}{strong}Pamiętaj, że nowa paczka nie będzie powiąznana z usługami paczki źrodłowej, takimi jak np. eBay i jej dane nie zostaną automatycznie przekazane do tych usług.{/strong}', ['{br}' => '<br/>', '{strong}' => '<strong>', '{/strong}' => '</strong>']));

        $this->_clone($model);


    }

    /**
     * Prepare data array for cloning package
     *
     * @param Postal $model
     */
    protected function _clone(Postal $model)
    {
        $data = [];

        $data['senderAddressData'] = $model->senderAddressData->attributes;
        $data['receiverAddressData'] = $model->receiverAddressData->attributes;
        $data['model'] = $model->attributes;

        Yii::app()->session['clonePostal'] = $data;

        $this->redirect(['/postal/postal/create']);
    }


    /**
     * Fill form with cloned package data
     *
     * @param Postal $model
     */
    protected function _clone_fill()
    {

        if(isset(Yii::app()->session['clonePostal'])) {

            $data = Yii::app()->session['clonePostal'];
            unset(Yii::app()->session['clonePostal']);

            $this->setPageState('step1_model', $data['model']);
            $this->setPageState('step1_senderAddressData', $data['senderAddressData']);
            $this->setPageState('step1_receiverAddressData', $data['receiverAddressData']);
        }


    }

    /**
     * Create new postal based on data from pricing calculator on homepage. Use cloning methods
     */
    public function actionCreateFP()
    {

        $postal = new Postal();
        $postal->postal_type = (int) $_GET['t'];
        $postal->weight_class = (int) $_GET['wc'];
        $postal->size_class = (int) $_GET['sc'];

        $addressData = new Postal_AddressData();
        $addressData->country_id = (int) $_GET['rci'];

        $data = [];

        $data['senderAddressData'] = [];
        $data['receiverAddressData'] = $addressData->attributes;
        $data['model'] = $postal->attributes;

        Yii::app()->session['clonePostal'] = $data;


        $this->redirect(['/postal/postal/create']);
    }


///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

    public function actionCreate()
    {

        $this->panelLeftMenuActive = 212;

        if((Yii::app()->user->model->getPostalInternationalDisable()))
        {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Postal',
            ));
            exit;
        }




        // flashes do not like redirects to clear page states, so keep them in session before that
        $tempFlashes = Yii::app()->session['tempFlashContainer'];
        if (is_array($tempFlashes) && S_Useful::sizeof($tempFlashes)) {
            unset(Yii::app()->session['tempFlashContainer']);
            foreach ($tempFlashes AS $key => $value)
                Yii::app()->user->setFlash($key, $value);
        }

        $this->_clone_fill();

        if (Yii::app()->session['postalFormFinished']) // prevent going back and sending the same form again
        {
            Yii::app()->session['tempFlashContainer'] = Yii::app()->user->getFlashes();

            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['postalFormFinished']);

            $this->refresh(true);
        }

        if (isset($_POST['summary']))
            $this->create_step_summary(); // Validate addition list, summary
        elseif (isset($_POST['finish']))
            $this->create_finish();
        elseif (isset($_POST['forceReload']))
            $this->create_step_1( true);
        else
            $this->create_step_1(); // this is the default, first time (step1)
    }

    protected function create_step_1($validate = false)
    {
        if ($validate) {
            if ($this->create_validate_step_1() == 'break')
                return;
        }

        $price = false;
        $available = false;

        $this->setPageState('form_started', true);
        $this->setPageState('start_user_id', Yii::app()->user->id);

        $this->setPageState('last_step', 1); // save information about last step

        $model = new Postal('step1');

        $model->user_id = Yii::app()->user->id;

        $model->senderAddressData = new Postal_AddressData('insert', UserContactBook::TYPE_SENDER);
        $model->receiverAddressData = new Postal_AddressData('insert', UserContactBook::TYPE_RECEIVER);

        $model->receiverAddressData->country_id = Yii::app()->user->model->getDefaultPostalReceiverCountryId() ? Yii::app()->user->model->getDefaultPostalReceiverCountryId() : CountryList::COUNTRY_PL;

        $model->senderAddressData->_postal = $model;
        $model->receiverAddressData->_postal = $model;


        // load user data as sender
        if (!S_Useful::sizeof($this->getPageState('step1_senderAddressData', array())))
            $model->senderAddressData->loadUserData();
        else
            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', array());


        $model->attributes = $this->getPageState('step1_model', []);

        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);


        $senderCountryList = CountryList::model()->with('countryListTr')->with('countryListTrs')->findAll();
        $receiverCountryList = $senderCountryList;


        if (Postal::checkIfAvailable($model->getReceiverCountryId(), $model->postal_type, $model->weight_class, $model->size_class, Yii::app()->user->id)) {
            $currencySymbol = Yii::app()->PriceManager->getCurrencyCode();

            $price = $model->getPrice();
            if ($price === NULL)
                $available = false;
            else {
                $available = true;
                $price = Yii::app()->PriceManager->getFinalPrice($price) . ' ' . $currencySymbol;
            }
        }

        $this->setPageState('last_step', 1); // save information about last step

        $this->render('create/step1',
            [
                'price' => $price,
                'available' => $available,
                'model' => $model,
                'senderCountryList' => $senderCountryList,
                'receiverCountryList' => $receiverCountryList,
            ]);

    }

    protected function create_validate_step_1()
    {

        $model = new Postal('step1');
        $model->user_id = Yii::app()->user->id;

        $model->senderAddressData = new Postal_AddressData();
        $model->receiverAddressData = new Postal_AddressData();

        $model->senderAddressData->_postal = $model;
        $model->receiverAddressData->_postal = $model;


        $price = false;
        $available = false;

        $errors = false;

        if (isset($_POST['Postal'])) {

            $model->attributes = $_POST['Postal'];
            $model->senderAddressData->attributes = $_POST['Postal_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['Postal_AddressData']['receiver'];

            $this->setPageState('step1_model', $model->getAttributes(array_keys(($_POST['Postal'])))); // save previous form into form state
            $this->setPageState('step1_senderAddressData', $model->senderAddressData->getAttributes(array_merge(['country_id'], array_keys($_POST['Postal_AddressData']['sender'])))); // save previous form into form state
            $this->setPageState('step1_receiverAddressData', $model->receiverAddressData->getAttributes(array_merge(['country_id'], array_keys($_POST['Postal_AddressData']['receiver'])))); // save previous form into form state

            if (!$model->validate())
                $errors = true;

            if (!$model->senderAddressData->validate())
                $errors = true;

            if (!$model->receiverAddressData->validate())
                $errors = true;

        }

        if ($errors) {

            $senderCountryList = CountryList::model()->with('countryListTr')->with('countryListTrs')->findAll();
            $receiverCountryList = $senderCountryList;


            $this->render('create/step1',
                [
                    'price' => $price,
                    'available' => $available,
                    'model' => $model,
                    'senderCountryList' => $senderCountryList,
                    'receiverCountryList' => $receiverCountryList,
                ]);
            return 'break';
        }
    }

    protected function create_step_summary()
    {
        if ($this->getPageState('start_user_id') != Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');

        switch ($lastStep) {
            case 1 :
                if ($this->create_validate_step_1() == 'break')
                    return;
                break;
        }

        $model = new Postal();
        $model->scenario = 'summary';

        $model->senderAddressData = new Postal_AddressData();
        $model->receiverAddressData = new Postal_AddressData();

        $model->senderAddressData->_postal = $model;
        $model->receiverAddressData->_postal = $model;

        $model->attributes = $this->getPageState('step1_model', []);
        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);

        $model->user_id = Yii::app()->user->id;

        $this->setPageState('last_step', 'summary'); // save information about last step
        $price = $model->getPrice();

        $this->render('create/summary', array(
            'model' => $model,
            'price' => $price,
        ));
    }

    protected function create_validate_summary()
    {
        $errors = false;


        $model = new Postal();

        $model->senderAddressData = new Postal_AddressData();
        $model->receiverAddressData = new Postal_AddressData();

        $model->senderAddressData->_postal = $model;
        $model->receiverAddressData->_postal = $model;

        $model->attributes = $this->getPageState('step1_model', []);
        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);


        $model->user_id = Yii::app()->user->id;

        $model->scenario = 'summary';
        $model->regulations = $_POST['Postal']['regulations'];
        $model->regulations_rodo = $_POST['Postal']['regulations_rodo'];

        if(!$model->validate())
            $errors = true;

        if ($errors) {
            $this->render('create/summary', array(
                'model' => $model,
            ));
            return 'break';
        }

    }

    protected function create_finish()
    {
        if ($this->getPageState('start_user_id') != Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');
        if ($lastStep === 'summary') {

            if ($this->create_validate_summary() == 'break')
                return;

            $this->setPageState('last_step', 6); // save information about last step

            $model = new Postal('insert');

            $model->attributes = $this->getPageState('step1_model', array()); //get the info from step 1

            $model->senderAddressData = new Postal_AddressData();
            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
            $model->receiverAddressData = new Postal_AddressData();
            $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);

            $model->senderAddressData->_postal = $model;
            $model->receiverAddressData->_postal = $model;

            $model->user_id = Yii::app()->user->id;

            try
            {
                $transaction = Yii::app()->db->beginTransaction();
                $return = $model->saveNewPostal();

                if ($return instanceof Postal)
                    $return = $return->createAndFinalizeOrder();

            } catch (Exception $ex) {

                $transaction->rollback();
                Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
                throw new CHttpException(403, 'Wystapił nieznany błąd!');
            }

            $message = '';
            // label quequed
            if ($return) {
                $transaction->commit();

                $status = true;


                if ($model->postalLabel !== NULL)
                    PostalLabel::asyncCallForId($model->postalLabel->id);

//
            } else {
                list($status, $message) = $return;
            }

            if ($status) {
                Yii::app()->session['postalFormFinished'] = true;


                $model = Postal::model()->findByPk($model->id); // to get proper hash
                Yii::app()->user->setFlash('success', Yii::t('postal', 'Przesyłka została zapisana! {link}', ['{link}' => CHtml::link('#' . $model->local_id, Yii::app()->createUrl('/postal/postal/view', ['urlData' => $model->hash]), ['target' => '_blank', 'class' => 'btn btn-sm'])]));
                $this->redirect(array('/postal/postal/create'));

            } else {

                $this->render('create/errorPage', array('text' => $message));
                exit;
            }
            throw new CHttpException(403, 'Wystapił nieznany błąd!');


        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public function actionCreateBulk()
    {
        $this->panelLeftMenuActive = 222;

        // flashes do not like redirects to clear page states, so keep them in session before that
        $tempFlashes = Yii::app()->session['tempFlashContainer'];
        if (is_array($tempFlashes) && S_Useful::sizeof($tempFlashes)) {
            unset(Yii::app()->session['tempFlashContainer']);
            foreach ($tempFlashes AS $key => $value)
                Yii::app()->user->setFlash($key, $value);
        }

        if (Yii::app()->session['postalFormFinished']) // prevent going back and sending the same form again
        {
            Yii::app()->session['tempFlashContainer'] = Yii::app()->user->getFlashes();

            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['postalFormFinished']);

            $this->refresh(true);
        }

        if (isset($_POST['summary']))
            $this->createBulk_step_summary(); // Validate addition list, summary
        elseif (isset($_POST['finish']))
            $this->createBulk_finish();
        elseif (isset($_POST['forceReload']))
            $this->createBulk_step_1( true);
        else
            $this->createBulk_step_1(); // this is the default, first time (step1)
    }

    protected function createBulk_step_1($validate = false)
    {
        if ($validate) {
            if ($this->createBulk_validate_step_1() == 'break')
                return;
        }

        $this->setPageState('form_started', true);
        $this->setPageState('start_user_id', Yii::app()->user->id);

        $this->setPageState('last_step', 1); // save information about last step

        $models = [];
        $stateAttributes = $this->getPageState('step1_models', []);

        for ($i = 0; $i < Postal::BULK_ITEMS_ON_FORM_NUMBER; $i++) {
            $models[$i] = Postal::newBulk();
            $models[$i]->user_id = Yii::app()->user->id;

            if (isset($stateAttributes[$i]))
                $models[$i]->attributes = $stateAttributes[$i];
        }


        $this->setPageState('last_step', 1); // save information about last step

        $this->render('createBulk/step1',
            [
                'models' => $models,
            ]);

    }

    protected function createBulk_validate_step_1()
    {

        $models = [];
        $stateAttributes = $this->getPageState('step1_models', []);


        for ($i = 0; $i < Postal::BULK_ITEMS_ON_FORM_NUMBER; $i++) {
            $models[$i] = Postal::newBulk();
            $models[$i]->user_id = Yii::app()->user->id;

            if (isset($stateAttributes[$i]))
                $models[$i]->attributes = $stateAttributes[$i];
        }


        if (isset($_POST['Postal'])) {
            $errors = false;


            foreach ($models AS $id => $model) {
                $models[$id]->attributes = $_POST['Postal'][$id];

                $stateAttributes[$id] = $models[$id]->getAttributes(array_merge(['receiver_country_id'], array_keys(($_POST['Postal'][0]))));
            }

            $this->setPageState('step1_models', $stateAttributes); // save previous form into form state

            $activeModels = 0;
            foreach ($models AS $id => $model) {
                if ($model->bulk_number > 0) {
                    $activeModels++;

                    if (!$models[$id]->validate())
                        $errors = true;
                }
            }

            if (!$activeModels) {
                $errors = true;
                Yii::app()->user->setFlash('error', Yii::t('postal', 'Uzupełnij przynajmniej jeden wiersz danych'));
            }

        }

        if ($errors) {


            $this->render('createBulk/step1',
                [
                    'models' => $models,
                ]);
            return 'break';
        }
    }

    protected function createBulk_step_summary()
    {
        if ($this->getPageState('start_user_id') != Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');

        switch ($lastStep) {
            case 1 :
                if ($this->createBulk_validate_step_1() == 'break')
                    return;
                break;
        }

        $models = [];
        $stateAttributes = $this->getPageState('step1_models', []);

        for ($i = 0; $i < Postal::BULK_ITEMS_ON_FORM_NUMBER; $i++) {
            $models[$i] = Postal::newBulk();
            $models[$i]->user_id = Yii::app()->user->id;

            if (isset($stateAttributes[$i]))
                $models[$i]->attributes = $stateAttributes[$i];

            if ($models[$i]->bulk_number < 1)
                unset($models[$i]);
        }


        $model = new PostalBulkForm;

        $this->setPageState('last_step', 'summary'); // save information about last step

        $this->render('createBulk/summary', array(
            'models' => $models,
            'modelBulkForm' => $model
        ));

    }

    protected function createBulk_validate_summary()
    {
        $errors = false;

        $models = [];
        $stateAttributes = $this->getPageState('step1_models', []);

        for ($i = 0; $i < Postal::BULK_ITEMS_ON_FORM_NUMBER; $i++) {
            $models[$i] = Postal::newBulk();
            $models[$i]->user_id = Yii::app()->user->id;

            if (isset($stateAttributes[$i]))
                $models[$i]->attributes = $stateAttributes[$i];

            if ($models[$i]->bulk_number < 1)
                unset($models[$i]);
        }

        $model = new PostalBulkForm;
        $model->regulations = $_POST['PostalBulkForm']['regulations'];
        $model->regulations_rodo = $_POST['PostalBulkForm']['regulations_rodo'];
        if (!$model->validate())
            $errors = true;

        if ($errors) {

            $this->render('createBulk/summary', array(
                'modelBulkForm' => $model,
                'models' => $models,
            ));

            return 'break';
        }

    }

    protected function createBulk_finish()
    {
        if ($this->getPageState('start_user_id') != Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');
        if ($lastStep === 'summary') {

            if ($this->createBulk_validate_summary() == 'break')
                return;

            $this->setPageState('last_step', 6); // save information about last step

            $models = [];
            $stateAttributes = $this->getPageState('step1_models', []);

            for ($i = 0; $i < Postal::BULK_ITEMS_ON_FORM_NUMBER; $i++) {
                $models[$i] = Postal::newBulk();
                $models[$i]->user_id = Yii::app()->user->id;

                if (isset($stateAttributes[$i]))
                    $models[$i]->attributes = $stateAttributes[$i];

                if ($models[$i]->bulk_number < 1)
                    unset($models[$i]);
            }

            try
            {
                $transaction = Yii::app()->db->beginTransaction();
                $return_bulk_id = Postal::saveNewMassPostal($models);

                if ($return_bulk_id)
                    $return = Postal::createAndFinalizeOrderForBulkId($return_bulk_id);


            }
            catch(Exception $ex)
            {
                $transaction->rollback();
                Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
                throw new CHttpException(403, 'Wystapił nieznany błąd!');
            }

            if ($return) {
                $transaction->commit();
            } else {
                $transaction->rollback();
            }

            $message = '';

            if ($return) {

                Yii::app()->session['postalFormFinished'] = true;

                Yii::app()->user->setFlash('success', Yii::t('postal', 'Przesyłki zostały zapisane! {link}', ['{link}' => CHtml::link('#' . $return_bulk_id, Yii::app()->createUrl('/postal/postal/list', ['bulk_id' => $return_bulk_id]), ['target' => '_blank', 'class' => 'btn btn-sm'])]));
                $this->redirect(array('/postal/postal/createBulk'));

            } else {

                $this->render('createBulk/errorPage', array('text' => $message));
                exit;
            }
            throw new CHttpException(403, 'Wystapił nieznany błąd!');


        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }



    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////


    public function actionView($urlData, $new = false)
    {
        $this->panelLeftMenuActive = 200;


        /* @var $model Postal */

        $model = Postal::model()->find('hash=:hash', array(':hash' => $urlData));

        if ($model === null)
            throw new CHttpException(404);


        if ($model->user_id !== Yii::app()->user->id)
            throw new CHttpException(403);

        $actions = [];
        if (!$model->bulk && !$model->isLabelArchived() && $model->postal_stat_id != PostalStat::CANCELLED && $model->postal_stat_id != PostalStat::CANCELLED_USER && $model->postal_stat_id != PostalStat::PACKAGE_PROCESSING) {
            $actions[] = $this->renderPartial('actions/generate_carriage_ticket', array('model' => $model), true);
        }

        if (!in_array($model->postal_stat_id, [PostalStat::CANCELLED, PostalStat::CANCELLED_PROBLEM, PostalStat::CANCELLED_USER]))
            $actions[] = $this->renderPartial('actions/generate_ack_card', array('model' => $model), true);

        if(!$model->bulk)
            $actions[] = $this->renderPartial('actions/clone', array('model' => $model), true);

//        if(Yii::app()->user->getModel()->premium && $model->courierTypeInternalSimple !== NULL)
//            if($model->courier_stat_id == CourierStat::NEW_ORDER)
//                $actions .= $this->renderPartial('actions/cancel', array('model' => $model), true);

        if ($model->isUserCancelByUserPossible())
            $actions[] = $this->renderPartial('actions/cancelByUser', array('model' => $model), true);

        if($model->isUserComplaintPossible())
            $actions[] = $this->renderPartial('actions/complaint', array('model' => $model), true);

        $this->render('view', array(
            'model' => $model,
            'actions' => $actions,
            'new' => $new,
        ));

    }
//


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// GRIDVIEW START

    public function actionList()
    {
        $this->panelLeftMenuActive = 201;

        $this->render('list', array());
    }


    private static function getGvItemCssClassByStat($stat)
    {
        if ($stat == PostalStat::NEW_ORDER)
            return 'gv-courier-new';
        else if (in_array($stat, PostalStat::getKnownDeliveredStatuses()))
            return 'gv-courier-finished';
        else if ($stat === PostalStat::CANCELLED)
            return 'gv-courier-cancelled';
        else
            return 'gv-courier-in-progress';
    }

    /**
     * Function to memorize recently added pages in order to highlight them in GridView
     *
     * @param array $packagesIds Ids of recently created packages (ids of Postal models)
     */
    protected static function addToListOfJustCreated(array $packagesIds)
    {

        $justCreated = Yii::app()->session['postalJustCreated'];
        $justCreated = CMap::mergeArray($justCreated, $packagesIds);
        Yii::app()->session['postalJustCreated'] = $justCreated;
    }

    /**
     * Function to check, if this package is memorized as recently added to highlight it in GridView
     *
     * @param int $packageId Id of package (id of Postal model)
     */
    protected static function checkIfJustCreated($packageId)
    {
        if (in_array($packageId, Yii::app()->session->get('postalJustCreated', [])))
            return true;
        else
            return false;
    }

    /**
     * Clear list of memorized recently added packages.
     */
    protected static function clearJustCreatedPackagesList()
    {
        Yii::app()->session['postalJustCreated'] = [];
    }

    public static function _cancelColumnForGridView($data)
    {
        if($data->postal_stat_id == PostalStat::CANCELLED_PROBLEM)
        {
            return '<span title="'.Yii::t('postal', 'Paczka została automatycznie anulowana z powodue problemu z zamówieniem').'" style="font-weight: bold; text-decoration: italic; color: darkviolet; cursor: help;">P</span>';
        }
        else if($data->isUserCancelByUserPossible())
        {
            return CHtml::link("<span class=\"glyphicon glyphicon-remove\"></span>",array("/postal/postal/cancelByUser", "urlData" => $data->hash), ["target" => "_blank", "onclick" => "return confirm(\"".Yii::t("site", "Czy jesteś pewien?")."\")", "title" => Yii::t("postal", "Anuluj paczkę")]);
        }
        else if($data->isUserComplaintPossible())
        {
            return CHtml::link("<strong>[Z]</strong>",array("/user/complaint", "hash" => $data->hash, "type" => UserComplaintForm::WHAT_POSTAL), ["target" => "_blank", "title" => Yii::t("postal", "Reklamuj paczkę")]) ;
        } else {
            if($data->user_cancel == Postal::USER_CANCELL_COMPLAINED)
                return "<strong style=\"text-decoration: underline; color: red;\" title=\"".Yii::t("postal", "Zgłoszono reklamację tej paczki")."\">R</strong>";
        }



    }

    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewPostalgrid()
    {
        // for transation bot to prepare this text
        $temp = Yii::t('postal', 'Nowa paczka');

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSizePostal',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new _PostalGridViewUser('search');
        $model->unsetAttributes();

        if (isset($_GET['PostalGridViewUser']))
            $model->setAttributes($_GET['PostalGridViewUser']);

        if(isset($_GET['bulk_id']))
            $model->bulk_id = filter_var($_GET['bulk_id'], FILTER_SANITIZE_NUMBER_INT);

        $countries = [];
        $countries[-1] = '';
        $countries += GxHtml::listDataEx(CountryList::model()->findAll(['order' => '(CASE WHEN id = 1 THEN 0 ELSE 1 END) ASC, name ASC']));
        $countries[-1] = '<'.Yii::t('site','oprócz').' '.$countries[1].'>';

        $pageSize = Yii::app()->user->getState('pageSizePostal', 50);


        $columns = [];
        $colSpan = 4;

        if(Yii::app()->user->getModel()->getPostalRefPrefix())
            $colSpan++;

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_EXTERNAL))
            $colSpan++;

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_CONTENT))
            $colSpan++;

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_BULK)) {
            $colSpan++;
            $colSpan++;
            $colSpan++;
        }

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_VALUE))
            $colSpan++;

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_NOTE)) {
            $colSpan++;
            $colSpan++;
        }

        $columns[] = array(
            'class'=>'GridViewIndexColumn',
            'header' => '#',
            'htmlOptions' => [
                'width' => '37',
                'class' => 'text-right',
            ],
        );

        $columns[] =  array(
            'name' => 'local_id',
            'header' => Yii::t('postal', '#ID'),
            'type' => 'raw',
            'value' => 'CHtml::link($data->local_id,array("/postal/postal/view/", "urlData" => $data->hash), ["target" => "_blank"])',
            'class' => 'DataColumn',
            'evaluateHtmlOptions' => true,
            'htmlOptions' => [
                'class' => '(PostalController::checkIfJustCreated($data->id) ? "gv-postal-just-created" : "")." ".($data->stat == NULL ? "gv-postal button-column button-column-fix" : $data->stat->getGvItemCssClassByStat()." button-column button-column-fix")',
                'title' => 'PostalController::checkIfJustCreated($data->id) ? Yii::t(\'postal\', \'Nowa paczka\') : ""',
                'width' => '110',
            ],
        );
//                array(
//                    'name' => '__stat',
//                    'type' => 'raw',
//                    'filter' => CHtml::listData(PostalStat::getStatsForUser(), 'id', 'statText'),
//                    'header' => Yii::t('postal', 'Status'),
//                    'value' => '$data->stat->statText',
//                    'htmlOptions' => [
//                        'width' => '160',
//                        'class' => 'text-center'
//                    ],
//                ),

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_EXTERNAL))
            $columns[] =  array(
                'name'=>'__external_id',
                'header' => Yii::t('postal', 'External ID'),
                'value'=> '$data->postalLabel ? $data->postalLabel->track_id : ""',
                'htmlOptions' => [
                    'width' => '100',
                    'class' => 'text-center'
                ],
            );

        if(Yii::app()->user->getModel()->getPostalRefPrefix())
            $columns[] = array(
                'name' => 'ref',
                'htmlOptions' => [
                    'width' => '170',
                    'class' => 'text-center'
                ],
            );

        $columns[] =  array(
            'name'=>'stat_map_id',
            'type'=>'raw',
            'filter'=>StatMap::mapNameList(),
            'header' => Yii::t('courier','Status'),
            'value'=>'StatMap::getMapNameById($data->stat_map_id)',
            'htmlOptions' => [
                'width' => '150',
                'class' => 'text-center'
            ],
        );

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_VALUE))
        {
            $columns[] = [
                'name' => 'value',
                'value' => '$data->value ? $data->value." ".$data->value_currency : ""',
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            ];
        }

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_BULK)) {
            $columns[] = [
                'name' => 'bulk',
                'value' => '$data->bulkName',
                'filter' => Postal::getBulkList(),
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            ];

            $columns[] = [
                'name' => 'bulk_id',
                'htmlOptions' => [
                    'width' => '100',
                    'class' => 'text-center'
                ],
            ];

            $columns[] = [
                'name' => 'bulk_number',
                'htmlOptions' => [
                    'width' => '55',
                    'class' => 'text-center'
                ],
            ];
        }

        $columns[] = [
            'name' => 'postal_type',
            'value' => '$data->postalTypeName',
            'filter' => Postal::getPostalTypeList(),
            'htmlOptions' => [
                'width' => '50',
                'class' => 'text-center'
            ],
        ];

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_CONTENT))
            $columns[] = [
                'name' => 'content',
//            'value' => '$data->postalTypeName',
//            'filter' => Postal::getPostalTypeList(),
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            ];

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_NOTE))
        {
            $columns[] = [
                'name' => 'note1',
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            ];

            $columns[] = [
                'name' => 'note2',
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            ];
        }

        $columns[] = array(
            'name' => 'date_entered',
            'value' => 'substr($data->date_entered, 0, 10)',
            'header' => Yii::t('postal', 'Utworzenia'),
            'htmlOptions' => [
                'width' => '80',
                'class' => 'text-center'
            ],
        );
        $columns[] = array(
            'name' => 'date_updated',
            'value' => 'substr($data->date_updated, 0, 10)',
            'header' => Yii::t('postal', 'Aktualizacji'),
            'htmlOptions' => [
                'width' => '80',
                'class' => 'text-center'
            ],
        );



        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_SENDER)) {
            $columns[] = array(
                'name' => '__sender_country_id',
                'header' => Yii::t('postal', 'Country'),
                'value' => '$data->senderAddressData->country0->code',
                'filter' => $countries,
                'htmlOptions' => [
                    'width' => '60',
                    'class' => 'text-center'
                ],
            );

            $columns[] = array(
                'name' => '__sender_usefulName',
                'header' => Yii::t('postal', 'Name'),
                'value' => '$data->senderAddressData->usefulName',
                'htmlOptions' => [
                    'width' => '110',
                    'class' => 'text-center'
                ],
            );
        }

        $columns[] = array(
            'name'=>'__receiver_country_id',
            'header'=>Yii::t('postal','Country'),
            'value'=>'$data->receiverCountryCode',
            'filter'=> $countries,
            'htmlOptions' => [
                'width' => '60',
                'class' => 'text-center'
            ],
        );
        $columns[] = array(
            'name' => '__receiver_usefulName',
            'header' => Yii::t('postal','Name'),
            'value' => '$data->receiverAddressData->usefulName',
            'htmlOptions' => [
                'width' => '110',
                'class' => 'text-center'
            ],
        );
        $columns[] = [
            'class' => 'yiiwheels.widgets.grid.WhRelationalColumn',
            'name' => 'subGrid',
            'url' => $this->createUrl('/postal/postal/listMore', ['id' => $data->id]),
            'value' => '"<span class=\"glyphicon glyphicon-save\"></span>"',
            'type' => 'raw',
            'filter' => false,
            'header' => '',
            'cssClass' => 'gridview-show-more',
            'afterAjaxUpdate' => 'js:function(tr,rowid,data){
                         //$(tr).hide();
                         //$(\'.gridview-dont-hover\').not(tr).hide();
                         $(tr).addClass(\'gridview-dont-hover\');
                         $(tr).fadeIn();
                         $(\'[data-popup-me]\').on(\'click\', function(e){
                            e.preventDefault();
                            window.open($(this).attr(\'href\'),$(this).attr(\'data-popup-me\'), \'height=200,width=850,resizable=yes,scrollbars=yes\');
                         });
                    }',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix',
                "title" => Yii::t("postal", "Rozwiń szczegóły poniżej")
            ],
        ];
        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value' => 'CHtml::link("<span class=\"glyphicon glyphicon-new-window\"></span>",array("/postal/postal/view/", "urlData" => $data->hash), ["target" => "_blank", "title" => Yii::t("postal", "Pokaż szczegóły w nowym oknie")])',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix'
            ],
        );
        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value' => '$data->bulk ? "" : CHtml::link("<span class=\"glyphicon glyphicon-duplicate\"></span>",array("/postal/postal/clone/", "urlData" => $data->hash), ["target" => "_blank", "title" => Yii::t("postal", "Klonuj list")])',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix'
            ],
        );
        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value'=>'PostalController::_cancelColumnForGridView($data)',
            'htmlOptions' => [
                'width' => '28',
                'class' => 'text-center button-column button-column-fix'
            ],
        );
        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value'=>'$data->ack_generated ? "<span style=\"cursor: help;\" class=\"glyphicon glyphicon-print\" title=\"".substr($data->ack_generated,0, 16)." - ".Yii::t("postal", "data wygenerowania potwierdzenia:")."\"></span>" : ""',

            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix'
            ],
        );
        $columns[] = array(
            'class' => 'MyCheckBoxColumn',
            'id' => 'selected_rows',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center'
            ],
            'disabled' => 'in_array($data->postal_stat_id, [PostalStat::CANCELLED, PostalStat::NEW_ORDER, PostalStat::PACKAGE_PROCESSING])',
        );



        $headers = [];
        $headers[] = [
            'text' => '',
            'colspan' => $colSpan,
            'options' => []
        ];

        $headers[] = [
            'text' => Yii::t('postal', 'Data'),
            'colspan' => 2,
            'options' => []
        ];

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_POSTAL_SENDER))
            $headers[] = [
                'text' => Yii::t('postal', 'Nadawca'),
                'colspan' => 2,
                'options' => []
            ];

        $headers[] = [
            'text' => Yii::t('postal', 'Odbiorca'),
            'colspan' => 2,
            'options' => []
        ];


        $headers[] = [
            'text'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10 ,20=>20 ,50=>50 , 100=>100, 250 => 250, 500 => 500),array(
                'onchange'=>"$.fn.yiiGridView.update('postalgrid',{ data:{pageSize: $(this).val() }})",
            )),

            'colspan'=>6,
            'options'=>[]
        ];


//$this->widget('zii.widgets.grid.CGridView', array(
//$this->widget('yiiwheels.widgets.grid.WhGridView', array(
//        $this->widget('ext.selgridview.BootSelGridView', array(



        echo '<input type="hidden" class="grid-data"/>';
        $this->widget('SPGridViewNoGet', array(
            'id' => 'postalgrid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows' => 2,
//            'enableHistory' => true,
            'selectionChanged' => 'function(id){ checkNumberOfChecks(id); }',
            'beforeAjaxUpdate' => 'function(id,options){$(\'.gridview-overlay\').show();}',
            'afterAjaxUpdate' => 'function(id,data){
            $(\'.gridview-overlay\').hide();
              $(\'[name="PostalGridViewUser[date_entered]"], [name="PostalGridViewUser[date_updated]"]\').datepicker({
                dateFormat : \'yy-mm-dd\',
                minDate: "-5Y", 
                maxDate: "+0M",
                changeMonth: true,
                changeYear: true,
            });
            
            }',
//    'afterAjaxUpdate' => "function(id,data){  $('[name=\"selected_rows[]\"]').on('click', function(){
//        checkNumberOfChecks($(this));
//    });
//      }",
            //'rowCssClassExpression'=>'$data->stat?"row-open":"row-closed"',
            'template' => "{pager}\n{summary}\n{items}\n{summary}\n{pager}",
            'htmlOptions' => ['style' => 'table-layout: fixed'],
            'addingHeaders' => array(
                $headers,
            ),
            'rowCssClassExpression' => '
                ( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) .
                ( in_array($data->postal_stat_id, [PostalStat::CANCELLED, PostalStat::CANCELLED_USER, PostalStat::NEW_ORDER, PostalStat::PACKAGE_PROCESSING]) ? " disabled" : null )
            ',
            'columns' => $columns,
        ));

        self::clearJustCreatedPackagesList();

    }

    public function actionListMore()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $model = Postal::model()->with('senderAddressData')->with('receiverAddressData')->findByPk($_GET['id']);

            if(Yii::app()->user->isGuest OR $model->user_id != Yii::app()->user->id)
                throw new Exception(403);

            $html = $this->renderPartial('_listMore', [
                'model' => $model,
            ], true);

            echo $html;

        }
    }





// GRIDVIEW END
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////



    public function actionCancelByUser($urlData)
    {

        if(Yii::app()->user->isGuest)
            throw new CHttpException(403);

        /* @var $model Postal */
        $model = Postal::model()->find('hash=:hash AND user_id = :user_id', array(':hash' => $urlData, ':user_id' => Yii::app()->user->id));

        if($model === NULL)
            throw new CHttpException(404);

        if($model->isUserCancelByUserPossible())
        {
            if($model->userCancelRequestByUser())
            {
//                if($model->user_cancel == Postal::USER_CANCEL_REQUEST)
//                    Yii::app()->user->setFlash('notice', Yii::t('postal', 'Ze względu na złożone zamówienie, paczka nie może zostać anulowana automatycznie, ale zgłoszenie zostało wysłane do administratora.'));
//
//                else if($model->user_cancel == Postal::USER_CANCEL_CONFIRMED)
                Yii::app()->user->setFlash('success', Yii::t('postal', 'Paczka została anulowana.'));
//                else
//                    Yii::app()->user->setFlash('error', Yii::t('postal', 'Anulowanie tej paczki nie było możliwe.'));

            } else
                Yii::app()->user->setFlash('error', Yii::t('postal', 'Anulowanie tej paczki nie było możliwe.'));

        } else {

            Yii::app()->user->setFlash('error', Yii::t('postal', 'Anulowanie tej paczki nie było możliwe.'));
        }



        $this->redirect(['/postal/postal/view', 'urlData' => $model->hash]);
    }



    public function actionCancelByUserMulti()
    {
        $ids = $_POST['Postal']['ids'];

        $models = Postal::model()
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'postal_stat_id != :stat AND postal_stat_id != :stat2 AND user_cancel = 0', [':stat' => PostalStat::CANCELLED, ':stat2' => PostalStat::CANCELLED_USER]);

        /* @var $model Postal */
        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('postal', 'Brak paczek do anulowania!'));
        } else {

        }

        $return = Postal::userCancelMulti($models);
        if($return)
            Yii::app()->user->setFlash('top-info', Yii::t('postal', 'Anulowano lub zgłoszono cheć anulowania dla paczek: {no}', ['{no}' => $return]));
        else
            Yii::app()->user->setFlash('top-info', Yii::t('postal', 'Brak paczek do anulowania!'));

        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionCarriageTicket($urlData, $type = false)
    {
        /* @var $model Postal */
        $model = Postal::model()->find('hash=:hash', array(':hash' => $urlData));

        if($model === null OR $model->postal_stat_id == PostalStat::CANCELLED OR $model->postal_stat_id == PostalStat::CANCELLED_USER)
            throw new CHttpException('404');

        if(!$type)
            $type = $_REQUEST['type'];

        if($model->bulk)
            throw new CHttpException('404');

        PostalLabelPrinter::generateLabels($model, $type);
    }

    public function actionGenerateLabelsByGridView()
    {
        /* @var $model Postal */
        $i = 0;
        if (isset($_POST['Postal']))
        {
            $type = $_POST['Postal']['type'];
            $postal_ids =  $_POST['Postal']['ids'];

            $postal_ids = explode(',', $postal_ids);

            if(is_array($postal_ids) AND S_Useful::sizeof($postal_ids))
            {

                $models = [];
                foreach($postal_ids AS $id)
                {
                    $model = Postal::model()->findByPk($id);
                    if($model === null)
                        continue;

                    if(($model->bulk))
                        continue;

                    if(in_array($model->postal_stat_id, [PostalStat::CANCELLED, PostalStat::NEW_ORDER, PostalStat::PACKAGE_PROCESSING, PostalStat::CANCELLED_USER]) OR $model->isLabelArchived())
                        continue;

                    array_push($models, $model);

                    $i++;
                }
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/postal/postal/list'));
        } else {
            PostalLabelPrinter::generateLabels($models, $type, true);
        }
    }

    public function actionAckCard($hash = NULL)
    {
        $model = Postal::model()->findByAttributes(array('hash' => $hash));

        /* @var $model Postal */

        if($model == NULL OR $model->postal_stat_id == PostalStat::CANCELLED OR $model->postal_stat_id == PostalStat::CANCELLED_USER)
            throw new CHttpException(403, Yii::t('postal', 'Nie można wygenerować potwierdzenia dla tej paczki'));

        PostalAcknowlegmentCard::generateCard($model);
    }

    public function actionKnMulti()
    {
        $ids = $_POST['Postal']['ids'];

        $models = Postal::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'postal_stat_id != :stat AND postal_stat_id != :stat2 AND postal_stat_id != :stat3', [':stat' => PostalStat::CANCELLED, ':stat2' => PostalStat::CANCELLED_PROBLEM, ':stat3' => PostalStat::CANCELLED_USER]);

        /* @var $model Postal */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('postal', 'Brak paczek do wygenerowania KN!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $postingDate = false;
        if(Yii::app()->user->getModel()->getPPDaysDelay())
            $postingDate = S_Useful::workDaysNextDate(date('Y-m-d'), Yii::app()->user->getModel()->getPPDaysDelay());
        KsiazkaNadawcza::generate($models, KsiazkaNadawcza::MODE_POSTAL, $postingDate);

    }


    public function actionAckCardMulti()
    {
        $ids = $_POST['Postal']['ids'];

        $models = Postal::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'postal_stat_id != :stat AND postal_stat_id != :stat2 AND postal_stat_id != :stat3', [':stat' => PostalStat::CANCELLED, ':stat2' => PostalStat::CANCELLED_PROBLEM, ':stat3' => PostalStat::CANCELLED_USER]);

        /* @var $model Postal */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('postal', 'Brak paczek do wygenerowania potwierdzenia!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        PostalAcknowlegmentCard::generateCardMulti($models);
    }

    public function actionAckCardToday()
    {
        $models = Postal::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->findAll(['condition' => 't.user_id = :user_id AND t.date_entered LIKE :today AND t.postal_stat_id != :stat AND t.postal_stat_id != :stat2 AND t.postal_stat_id != :stat3', 'params' => [':user_id' => Yii::app()->user->id, ':today' => date('Y-m-d').'%', ':stat' => PostalStat::CANCELLED, ':stat2' => PostalStat::CANCELLED_PROBLEM, ':stat3' => PostalStat::CANCELLED_USER]]);

        /* @var $model Postal */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('postal', 'Brak paczek do wygenerowania potwierdzenia!'));
            $this->redirect(['/postal/postal']);
        }

        PostalAcknowlegmentCard::generateCardMulti($models);

//        $this->render('index');
    }



############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################


    public function actionQuequeLabelByLocalId()
    {

        $local_id = $_POST['localId'];

        /* @var $postal Postal */
        $postal = Postal::model()->findByAttributes(array('local_id' => $local_id));

        $return = false;
        $ebayImport = NULL;
        if($postal !== NULL)
        {
            if($postal->_ebay_order_id != '')
                $postal->__temp_ebay_update_stat = EbayImport::STAT_UNKNOWN;

            if($postal->postalLabel !== NULL) {

                if ($postal->getUsefulLabelStat() == PostalLabel::STAT_NEW) {
                    $return = $postal->postalLabel->runLabelOrdering(true);
                }

                $stat = $postal->getUsefulLabelStat();
                if($stat == PostalLabel::STAT_NEW)
                {
                    if(!$return)
                        $rerun = true;
                    else
                    {
                        //get current status
                        $postalRefreshed = Postal::model()->findByPk($postal->id);
                        $stat = $postalRefreshed->getUsefulLabelStat();
                    }
                }

//                if($postal->_ebay_order_id != '')
//                    $ebayImport = $postal->__temp_ebay_update_stat;


                $log = '';
                if ($stat == PostalLabel::STAT_FAILED) {
                    $log = 'Error';
                    if($postal->postalLabel->log != '')
                        $log = 'Error: '.$postal->postalLabel->log;
                }
            } else {
                $log = '';
                $rerun = false;

//                if($postal->_ebay_order_id != '')
//                {
//                    if($postal->__temp_ebay_update_stat !== NULL)
//                        $ebayImport = $postal->__temp_ebay_update_stat;
//                }

            }

            $ebayImport = EbayImport::STAT_UNKNOWN;
            if(!$rerun && $postal->_ebay_order_id != '')
            {
                // get fresh data from DB
                $ebayImport = Postal::model()->findByPk($postal->id)->_ebay_export_status;
            }

            $return = array(
                'request' => true,
                'stat' => $stat,
                'desc' => $log,
                'rerun' => $rerun,
            );

            if($ebayImport !== NULL)
                $return['ebayImport'] = $ebayImport;

            echo CJSON::encode($return);
            exit;
        } else {
            echo CJSON::encode(array('request' => false));
            exit;
        }
    }

    public function actionImport($hash = NULL)
    {

        $this->panelLeftMenuActive = 213;

        // for CourierImportManager class
        Yii::app()->getModule('courier');

        $fileModel = new F_PostalImport();

        if($hash !== NULL)
        {
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/postalRefValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
            Yii::import('zii.behaviors.CTimestampBehavior');

            $models = Yii::app()->cacheUserData->get('postalImport_' . $hash.'_'.Yii::app()->session->sessionID);

            if(isset($_POST['refresh'])) {
                if(F_PostalImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('postal', 'Popraw najpierw wszyskie błędy!'));
                }
                // save modified by validators models:
                Yii::app()->cacheUserData->set('postalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                $this->refresh();
                exit;
            }
            if(isset($_POST['save']))
            {

                if(F_PostalImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('postal', 'Popraw najpierw wszyskie błędy!'));
                    // save modified by validators models:
                    Yii::app()->cacheUserData->set('postalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                    $this->refresh();
                    exit;
                }


                try
                {
                    $result = Postal::saveWholeGroupNotMass($models);
                }
                catch (Exception $ex)
                {
                    Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
                    return;
                }
                // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING


                if($result['success'])
                {
                    $import = PostalImportManager::getByHash($hash);
                    /* @var $item Postal */
                    $ids = array();
                    if(is_array($result['data']))
                        foreach($result['data'] AS $key => $item)
                        {
                            $ids[$key] = $item->id;
//                            array_push($ids, $item->id);
                        }

                    $import->addPostalIds($ids);
                    $import->save();

//                    Yii::app()->cacheUserData->delete('postalImport_rawdata_' . $hash.'_'.Yii::app()->session->sessionID);
                    Yii::app()->cacheUserData->delete('postalImport' . $hash.'_'.Yii::app()->session->sessionID);
                    $this->redirect(Yii::app()->createUrl('/postal/postal/listImported', ['hash' => $hash]));
                    exit;
                }

            } else {

                // validate only after editing one item in row
                if(Yii::app()->session['postalImport_'.$hash] == 10)
                {
                    unset(Yii::app()->session['postalImport_'.$hash]);
                    if (S_Useful::sizeof($models))
                        F_PostalImport::validateModels($models);
                }

                if (!$models OR !S_Useful::sizeof($models)) {
                    Yii::app()->user->setFlash('error', Yii::t('postal', 'Import nie powiódł się!'));
                    $this->redirect(Yii::app()->createUrl('postal/postal/import'));
                }

                $this->render('import/showModels', array(
                    'models' => $models,
                    'hash' => $hash,
                ));
                return;
            }
        }


        // file upload
        if(isset($_POST['F_PostalImport']) ) {
            $fileModel->setAttributes($_POST['F_PostalImport']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if (!$fileModel->validate()) {
                $this->render('import/index', array(
                    'model' => $fileModel,
                ));
                return;
            } else {
                // file upload success:

                if($fileModel->custom_settings)
                    $fileModel->applyCustomData($fileModel->custom_settings, Yii::app()->user->id);

                // generate new hash
                $hash = hash('sha256', time() . Yii::app()->user->id);

                $inputFile = $fileModel->file->tempName;

//                Yii::import('application.extensions.phpexcel.XPHPExcel');
//                XPHPExcel::init();

                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFile);


                // WITH FIX:
                if(strtoupper($inputFileType) == 'CSV')
                {
                    // SET UTF8 ENCODING IN CSV
                    $file_content = file_get_contents( $inputFile );

                    $file_content = S_Useful::forceToUTF8($file_content);
                    file_put_contents( $inputFile, $file_content );

                    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                    // FIX ENCODING

                    if($fileModel->_customSeparator)
                        $objReader->setDelimiter($fileModel->_customSeparator);
                    else
                        $objReader->setDelimiter(',');


                } else {
                    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                }


                // WITHOUT FIX:
//
//                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//                if(strtoupper($inputFileType) == 'CSV' && $fileModel->_customSeparator)
//                {
//                    $objReader->setDelimiter($fileModel->_customSeparator);
//                }

                $objPHPExcel = $objReader->load($inputFile);

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $data = [];
                $start = 1;

                if ($fileModel->omitFirst) {
                    $headerSeen = false;
                } else
                    $headerSeen = true;

                for ($row = $start; $row <= $highestRow; $row++) {
                    if (!$headerSeen) {
                        $headerSeen = true;
                        continue;
                    }

                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                    array_push($data, $rowData[0]);
                }

                // remove empty lines:
                $data = (array_filter(array_map('array_filter', $data)));

                $numberOfLines = S_Useful::sizeof($data);

                // check just by lines number
                if($numberOfLines > F_PostalImport::MAX_LINES)
                {
                    Yii::app()->user->setFlash('error', Yii::t('postal', 'Maksymalna ilość wierszy w pliku to: {no}! Naliczono: {number}', array('{no}' => F_PostalImport::MAX_LINES, '{number}' => $numberOfLines)));
                    $this->refresh(true);
                    exit;
                }

                $numberOfPackages = F_PostalImport::calculateTotalNumberOfPackages($data, $fileModel->_customOrder);
                // check by number of packages
                if($numberOfPackages > F_PostalImport::MAX_PACKAGES) {
                    Yii::app()->user->setFlash('error', Yii::t('postal', 'Maksymalna ilość paczek w pliku to: {no}! Naliczono: {number}', array('{no}' => F_PostalImport::MAX_PACKAGES, '{number}' => $numberOfPackages)));
                    $this->refresh(true);
                    exit;
                }

                $models = F_PostalImport::mapAttributesToModels($data, $fileModel->_customOrder, $fileModel->cutTooLong);
                F_PostalImport::validateModels($models);

                Yii::app()->cacheUserData->set('postalImport_rawdata_' . $hash.'_'.Yii::app()->session->sessionID, $data, 86400);
                Yii::app()->cacheUserData->set('postalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                $this->redirect(Yii::app()->createUrl('postal/postal/import', ['hash' => $hash]));
                exit;
            }
        }

        $this->render('import/index', array(
            'model' => $fileModel,
        ));

    }

    public function actionListImported($hash)
    {

        $this->panelLeftMenuActive = 200;

        $import = PostalImportManager::getByHash($hash);

        if($import->user_id != Yii::app()->user->id)
            throw new CHttpException(404);

        if(!$import)
            throw new CHttpException(404);

        // FASTER WAY:
        $postals = [];
        if(is_array($import->postal_ids))
            $postals = Postal::model()->findAllByPk($import->postal_ids);


        $assingProperKeysFromImport = array_combine(array_keys($import->postal_ids), $postals);


        $CACHE_NAME = 'POSTAL_IMPORT_INSTANCE_'.$hash;
        $cacheValue = Yii::app()->cache->get($CACHE_NAME);

        $mainInstance = false;
        if($cacheValue === false)
            $mainInstance = true;

        Yii::app()->cache->set($CACHE_NAME, 10, 60*60);

        $this->render('listImported',array(
            'import' => $import,
            'postals' => $assingProperKeysFromImport,
            'hash' => $hash,
            'mainInstance' => $mainInstance
        ));

    }

    /**
     * @param bool|false $e Whether it's ebay's originating packages
     */
    public function actionAjaxRemoveRowOnImport($e = false, $l = false)
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/postalRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        if($e) {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);

            unset($models[$id]);
            Yii::app()->cacheUserData->set('ebayImport_postal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
        }
        else if($l)
        {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('linkerImport_postal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);

            unset($models[$id]);
            Yii::app()->cacheUserData->set('linkerImport_postal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);

        } else {

            // from file import
            $models = Yii::app()->cacheUserData->get('postalImport_' . $hash.'_'.Yii::app()->session->sessionID);
            unset($models[$id]);

            Yii::app()->cacheUserData->set('postalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        }

        Yii::app()->session['courierpostalImport_'.($e ? 'e_' : ($l ? 'l_' : '')) .$hash] = 10;

        echo CJSON::encode(array('success' => true));
    }

    /**
     * @param bool|false $e Whether it's ebay's originating packages
     * @return bool|void
     */
    public function actionAjaxUpdateRowItemOnImport($e = false, $l = false)
    {

        if(Yii::app()->request->isAjaxRequest) {

            $hash = $_POST['hash'];
            $i = $_POST['i'];
            $attribute = $_POST['attribute'];
            $val = $_POST['val'];



            $possibleToEdit = [
                'receiverAddressData.name',
                'receiverAddressData.company',
                'receiverAddressData.zip_code',
                'receiverAddressData.address_line_1',
                'receiverAddressData.address_line_2',
                'receiverAddressData.city',
                'receiverAddressData.tel',
                'receiverAddressData.email',
                'senderAddressData.name',
                'senderAddressData.company',
                'senderAddressData.zip_code',
                'senderAddressData.address_line_1',
                'senderAddressData.address_line_2',
                'senderAddressData.city',
                'senderAddressData.tel',
                'senderAddressData.email',
                'weight',
                'postal_type',
                'size_class',
            ];


            if(!in_array($attribute, $possibleToEdit))
                return false;

            try {
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/postalRefValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
                Yii::import('zii.behaviors.CTimestampBehavior');

                if($e)
                    $models = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
                elseif($l)
                    $models = Yii::app()->cacheUserData->get('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
                else
                    $models = Yii::app()->cacheUserData->get('postalImport_' . $hash.'_'.Yii::app()->session->sessionID);


                /* @var $model Postal */
                if (is_array($models)) {
                    $model = $models[$i];

                    $submodel = false;
                    if ($model) {

                        $attribute = explode('.', $attribute);
                        if(S_Useful::sizeof($attribute) > 1)
                        {
                            $submodel = $attribute[0];

                            // $model = $model->$attribute[0];
                            // PHP7 fix
                            $temp = $attribute[0];
                            $model = $model->$temp;

                            $attribute = $attribute[1];
                        } else
                            $attribute = $attribute[0];


                        if ($model->hasAttribute($attribute)) {
                            $backupValue = $model->$attribute;
                            $model->$attribute = $val;

                            $model->validate([$attribute]);
                            if (!$model->hasErrors($attribute)) {

                                if($model instanceof Postal)
                                    $model->clearRouting(); // clear routing after changes

                                if($submodel)
                                    $models[$i]->$submodel = $model;
                                else
                                    $models[$i] = $model;



                                if($e)
                                    Yii::app()->cacheUserData->set('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                                elseif($l)
                                    Yii::app()->cacheUserData->set('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                                else
                                    Yii::app()->cacheUserData->set('postalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);


                                echo CJSON::encode(['success' => true]);
                                return;

                            } else {

                                echo CJSON::encode([
                                    'success' => false,
                                    'msg' => strip_tags($model->getError($attribute)),
                                    'backup' => $backupValue,
                                ]);
                                return;
                            }
                        }
                    }
                }

                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie. {msg}', ['{msg}' => print_r($attribute,1)])
                ]);
                return;

            } catch (Exception $ex) {
                Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);
                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie')
                ]);
                return;

            }
        }
    }

    /**
     * @param bool|false $e Whether it's ebay's originating packages
     * @throws CException
     */
    public function actionAjaxEditRowOnImport($e = false, $l = false)
    {

        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/postalRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        if($e) {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        }
        else if($l)
        {
            // from linker import
            $models = Yii::app()->cacheUserData->get('linkerImport_postal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);

        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('postalImport_' . $hash.'_'.Yii::app()->session->sessionID);
        }

        $model = $models[$id];

        if(isset($model)) {


            $countries = CountryList::model()->cache(60*60)->with('countryListTr')->with('countryListTrs')->findAll();
            $points = SpPoints::getPoints();
            if($e)
                $html = $this->renderPartial('import/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'points' => $points, 'e' => $e, 'l' => $l], true, true);
            else if($l)
                $html = $this->renderPartial('import/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'points' => $points, 'e' => $e, 'l' => $l], true, true);
            else
                $html = $this->renderPartial('import/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'points' => $points, 'e' => $e, 'l' => $l], true, true);

//            file_put_contents('htmldump.txt', print_r($html,1));

            echo CJSON::encode(array('success' => true, 'html' => $html));

        } else {
            echo CJSON::encode(array('success' => false));
        }

    }

    /**
     * @param bool|false $e Whether it's ebay's originating packages
     */
    public function actionAjaxSaveRowOnImport($e = false, $l = false)
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/postalRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        if($e)
        {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);
        } elseif($l) {
            // from linker import
            $models = Yii::app()->cacheUserData->get('linkerImport_postal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);
        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('postalImport_' . $hash.'_'.Yii::app()->session->sessionID);
        }

        $model = $models[$id];

        if(isset($model)) {

            /* @var $model Postal */
            $model->attributes = $_POST['Postal'];


            $model->senderAddressData->attributes = $_POST['Postal_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['Postal_AddressData']['receiver'];

            $model->senderAddressData->country0 = CountryList::model()->findByPk($model->senderAddressData->country_id);
            $model->receiverAddressData->country0 = CountryList::model()->findByPk($model->receiverAddressData->country_id);

            $model->validate();
            $model->senderAddressData->validate();
            $model->receiverAddressData->validate();

            $model->clearRouting(); // clear routing after changes


            $models[$id] = $model;

            if($e)
            {
                // from ebay import
                Yii::app()->cacheUserData->set('ebayImport_postal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            } elseif($l) {
                // from linker import
                Yii::app()->cacheUserData->set('linkerImport_postal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            } else {
                // from file import
                Yii::app()->cacheUserData->set('postalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            }

            // used to force reloading list of imported packages with validation to, for example: recalculate prices
            Yii::app()->session['postalImport_'.($e ? 'e_' : '') .$hash] = 10;

            echo CJSON::encode(array('success' => true));
        } else {
            echo CJSON::encode(array('success' => false));
        }

    }

    protected function _exportOrDeleteErroredImportData($hash, $e, $export = false, $delete = false)
    {
        if(!$export && !$delete)
            return false;

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/postalRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');



        if(!$e) {
            // from file import
            $models = Yii::app()->cacheUserData->get('postalImport_' . $hash . '_' . Yii::app()->session->sessionID);
            $rawData = Yii::app()->cacheUserData->get('postalImport_rawdata_' . $hash . '_' . Yii::app()->session->sessionID);
        } else {
            $models = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        }


        if(!S_Useful::sizeof($models))
            return false;

        $toExport = [];

        foreach($models AS $key => $model)

            if(!$model->validate() OR !$model->senderAddressData->validate() OR !$model->receiverAddressData->validate())
            {
                if($export)
                {
                    if(!$e)
                        $toExport[] = $rawData[$key];
                    else
                        $toExport[] = $model;
                }


                if($delete)
                    unset($models[$key]);
            }

        if($delete) {
            if (!$e) {
                // from file import
                Yii::app()->cacheUserData->set('postalImport_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
            } else {
                Yii::app()->cacheUserData->set('ebayImport_postal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
            }

// used to force reloading list of imported packages with validation to, for example: recalculate prices
            Yii::app()->session['postalImport_' . ($e ? 'e_' : '') . $hash] = 10;
        }

        if($export)
        {
            if(!$e) {
                $toExport = S_XmlGenerator::fillEmptyKeys($toExport);
                S_XmlGenerator::generateXml($toExport);
            }
            else
                S_PostalIO::exportToXlsUser($toExport);
            Yii::app()->end();
        }

        if($delete)
        {
            $this->redirect(['/postal/postal/import', 'hash' => $hash]);
        }

    }

    public function actionExportImportedWithError($hash, $e = false)
    {
        $keys = $_POST['errored-keys'];

        $keys = explode(',', $keys);
        $keys = array_unique($keys);

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/postalRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        if(!$e) {
            // from file import
            $items = Yii::app()->cacheUserData->get('postalImport_rawdata_' . $hash . '_' . Yii::app()->session->sessionID);
        } else {
            $items = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        }

        $toExport = [];

        foreach($keys AS $key) {

            if(isset($items[$key]))
                $toExport[] = $items[$key];
        }

        if(!S_Useful::sizeof($toExport))
            throw new CHttpException(403);


        if(!$e) {
            $toExport = S_XmlGenerator::fillEmptyKeys($toExport);
            S_XmlGenerator::generateXml($toExport);
        }
        else
            S_PostalIO::exportToXlsUser($toExport);

        Yii::app()->end();


    }

    public function actionExportErroredImportData($hash, $e = false)
    {
        self::_exportOrDeleteErroredImportData($hash, $e, true, false);
    }

    public function actionDeleteErroredImportData($hash, $e = false)
    {
        self::_exportOrDeleteErroredImportData($hash, $e, false, true);
    }


    public function actionGetLabels($hash)
    {
        if(isset($_POST['label']))
        {
            $postals = [];
            if(is_array($_POST['label']))
                foreach($_POST['label'] AS $key => $item)
                {
                    $temp = Postal::model()->findByAttributes(['id' => $key, 'user_id' => Yii::app()->user->id]);
                    array_push($postals, $temp);
                }

            if(S_Useful::sizeof($postals)) {



                PostalLabelPrinter::generateLabels($postals, $_POST['Postal']['type'],true);
                exit;
            }
        }

        Yii::app()->user->setFlash('error', Yii::t('postal','Nie znaleziono żadnych etykiet do wygenerowania!'));
        $this->redirect(Yii::app()->createUrl('/postal/postal/listImported', ['hash' => $hash]));
        exit;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

    public function actionPrepare()
    {
        $this->panelLeftMenuActive = 241;

        $this->render('prepare');
    }

    public function actionOrderCourier()
    {
        Yii::app()->getModule('courier');

        if (Yii::app()->PV->get() !== PageVersion::PAGEVERSION_CQL) {
            $type = Courier::TYPE_INTERNAL;
            $point = SpPoints::CENTRAL_SP_POINT_ID;
        } else {
            $type = Courier::TYPE_DOMESTIC;
            $point = SpPoints::CENTRAL_SP_POINT_ID;
        }

        $this->redirect(['/courier/courier/createPackageBySpPoint',
            'spPointId' => $point,
            'type' => $type]);
        Yii::app()->end();
    }


    public function actionExportToFileAll()
    {
        $period = $_POST['period'];

        if($period == '' or strlen($period) < 6)
            throw new CHttpException(404);

        $period = substr($period, 2,4).'-'.substr($period, 0,2).'-';

        $models = Postal::model()
            ->with('receiverAddressData')
            ->with('senderAddressData')
            ->findAll(array('condition' => 'user_id = :user_id  AND t.date_entered LIKE :date', 'params' => array(':user_id' => Yii::app()->user->id, ':date' => $period.'%')));

        S_PostalIO::exportToXlsUser($models);
    }

    public function actionMassExportByGridView()
    {
//        if(!Yii::app()->user->getModel()->isPremium)
//            throw new CHttpException(403);

        /* @var $model Postal */
        $i = 0;
        if (isset($_POST['Postal']))
        {
            $postal_ids =  $_POST['Postal']['ids'];

            $postal_ids = explode(',', $postal_ids);

            if(is_array($postal_ids) AND S_Useful::sizeof($postal_ids))
            {

                $models = [];
                foreach($postal_ids AS $id)
                {
                    $model = Postal::model()->findByPk($id);
                    if($model === null OR $model->user_id != Yii::app()->user->id)
                        continue;


                    array_push($models, $model);

                    $i++;
                }
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error',Yii::t('postal','Żadna przesyłka nie została eksportowana.'));
            $this->redirect(Yii::app()->createUrl('postal/postal/list'));
        } else {

            S_PostalIO::exportToXlsUser($models);
        }
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

    public function actionEbay($al = false, $hash = false)
    {

        $this->panelLeftMenuActive = 214;

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/postalRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        // autoload list once on enter
        if($al)
        {
            Yii::app()->session->add('ebay_autoload', true);
            $this->redirect(['/postal/postal/ebay']);
            exit;
        }

        // initialize ebayClient
        $ebayClient = ebayClient::instance();

        if($hash)
        {
            if(isset($_POST['refresh'])) {
                $this->_ebay_step3_refresh($ebayClient, $hash);
                exit;
            }
            else if(isset($_POST['save']))
            {
                $this->_ebay_step3_process($ebayClient, $hash);
                exit;
            } else
                $this->_ebay_step3($ebayClient, $hash);
        }
        else if(isset($_POST['import']))
        {
            if($this->_ebay_step1_process($ebayClient) === 'true')
                $this->_ebay_step2($ebayClient);
            else
                $this->_ebay_step1($ebayClient);

        }
        else if(isset($_POST['save-default-data']))
        {
            if($this->_ebay_step2_process($ebayClient) === 'true')
                $this->_ebay_step3($ebayClient, $hash);

        }
        else
        {
            $this->_ebay_step1($ebayClient);
        }
    }


    protected function _ebay_step1(ebayClient $ebayClient)
    {
        $orders = [];

        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $stat = (int) $_POST['stat'];
        $statEbay = (int) $_POST['statEbay'];

        if($date_from == '')
            $date_from = Yii::app()->session->get('ebay_date_from', NULL);

        if($date_to == '')
            $date_to = Yii::app()->session->get('ebay_date_to', NULL);

        if(!$stat)
            $stat = Yii::app()->session->get('stat', NULL);

        if(!$statEbay)
            $statEbay = Yii::app()->session->get('statEbay', NULL);

        $date_begining = date('Y-m-d', strtotime('-'.ebayClient::ORDERS_GOBACK_MONTHS.' months'));
        $date_end = date('Y-m-d');

        if (strtotime($date_from) > strtotime($date_to))
            $date_from = $date_to;

        if (!strtotime($date_from) OR strtotime($date_from) < strtotime($date_begining))
            $date_from = $date_end;

        if (!strtotime($date_to) OR strtotime($date_to) > strtotime($date_end))
            $date_to = $date_end;

        if (!in_array($stat, array_keys(EbayOrderItem::getStatShowProcessedList())))
            $stat = EbayOrderItem::STAT_SHOW_PROCESSED_ALL;

        if (!in_array($statEbay, array_keys(EbayOrderItem::getStatShowEbayList())))
            $statEbay = EbayOrderItem::STAT_SHOW_EBAY_READY;

        $more = false;
        // on load order
        if((isset($_POST['get-data']) OR Yii::app()->session->get('ebay_autoload', false)) && $ebayClient->isLogged()) {

            Yii::app()->session->add('ebay_date_from', $date_from);
            Yii::app()->session->add('ebay_date_to', $date_to);
            Yii::app()->session->add('stat', $stat);
            Yii::app()->session->add('statEbay', $statEbay);

            Yii::app()->session->add('ebay_autoload', false);
            list($orders, $more) = $ebayClient->getOrders($date_from, $date_to, $stat, $statEbay, EbayImport::TARGET_POSTAL);
            Yii::app()->cacheUserData->set('ebayImport_postal_'.$ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $orders, 86400);
        }

        $number = S_Useful::sizeof($orders);

        $this->render('importEbay/step_1', [
            'ebayClient' => $ebayClient,
            'orders' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'stat' => $stat,
            'statEbay' => $statEbay,
            'date_begining' => $date_begining,
            'date_end' => $date_end,
            'number' => $number,
            'more' => $more,
        ]);
    }

    protected function _ebay_step1_process(ebayClient $ebayClient)
    {
        $ordersToImport = [];
        $ordersCached = Yii::app()->cacheUserData->get('ebayImport_postal_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);

        if (is_array($_POST['import'])) {
            foreach ($_POST['import'] AS $importKey => $temp) {
                $temp = $ordersCached[$importKey];

                /* @var $temp EbayOrderItem */
                // prevent already processed in system
//                if(CourierEbayImport::findActivePackageByEbayOrderId($temp->orderID) === false) {
                if($temp instanceof EbayOrderItem) {
                    $temp = $temp->convertToPostalModel();
                    array_push($ordersToImport, $temp);
//                }
                } else {
                    throw new CHttpException(403);
                }

            }

            if(S_Useful::sizeof($ordersToImport) > EbayImportPostal::MAX_IMPORT_NUMBER)
            {
                Yii::app()->user->setFlash('error', Yii::t('courier_ebay', 'Maksymalna ilość paczek do importu to: {no}!', array('{no}' => EbayImportPostal::MAX_IMPORT_NUMBER)));
                Yii::app()->session->add('ebay_autoload', true);
                $this->refresh(true);
                exit;
            }

            Yii::app()->cacheUserData->set('ebayImport_postal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            return "true";
        }
    }

    protected function _ebay_step2(ebayClient $ebayClient)
    {
        $senderAddressData = new Postal_AddressData();
        $package = new Postal('default_package_data');
        $ebayImport = new EbayImportPostal();

        $senderAddressData->_postal = $package;

        $senderAddressData->attributes= User::getLoggedUserAddressData();

        $this->render('importEbay/step_2', [
            'ebayClient' => $ebayClient,
            'senderAddressData' => $senderAddressData,
            'package' => $package,
            'ebayImport' => $ebayImport,
        ]);
    }

    protected function _ebay_step2_process(ebayClient $ebayClient)
    {

        $senderAddressData = new Postal_AddressData();
        $package = new Postal('default_package_data');
        $ebayImport = new EbayImportPostal();

        $senderAddressData->_postal = $package;

        $senderAddressData->attributes = $_POST['Postal_AddressData'];
        $package->attributes = $_POST['Postal'];
        $ebayImport->attributes = $_POST['EbayImportOoe'];

        $senderAddressData->validate();
        $package->validate();
        $ebayImport->validate();

        if(!$senderAddressData->hasErrors() && !$package->hasErrors() && !$ebayImport->hasErrors()) {

            $ordersToImport = Yii::app()->cacheUserData->get('ebayImport_postal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, []);

            if(is_array($ordersToImport) && S_Useful::sizeof($ordersToImport))
            {
                /* @var $model Postal */
                foreach($ordersToImport AS &$order)
                {
                    $order->senderAddressData = new Postal_AddressData();
                    $order->senderAddressData->attributes = $senderAddressData->attributes;
                    $order->postal_type = $package->postal_type;
                    $order->weight_class = $package->weight_class;
                    $order->size_class = $package->size_class;
                    $order->target_sp_point = $package->target_sp_point;
                }

                F_PostalImport::validateModels($ordersToImport);

                Yii::app()->cacheUserData->delete('ebayImport_postal_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);
                Yii::app()->cacheUserData->set('ebayImport_postal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            }

            return "true";
        }

        $this->render('importEbay/step_2', [
            'ebayClient' => $ebayClient,
            'senderAddressData' => $senderAddressData,
            'package' => $package,
            'ebayImport' => $ebayImport,
        ]);
    }

    protected function _ebay_step3($ebayClient, $hash)
    {
        if(!$hash)
        {
            // generate new hash
            $hash = hash('sha256', time() . $ebayClient->getSessionId());
            $ordersToImport = Yii::app()->cacheUserData->get('ebayImport_postal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));

            // rename data by hash and clear previous
            Yii::app()->cacheUserData->set('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            Yii::app()->cacheUserData->delete('ebayImport_postal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);

            $this->redirect(['/postal/postal/ebay', 'hash' => $hash]);
            exit;

        } else {
            $ordersToImport = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));

            // added refresh button instead
            // validate only after editing one item in row
//            if(Yii::app()->session['postalImport_e_'.$hash] == 10)
//            {
//                unset(Yii::app()->session['postalImport_e_'.$hash]);
//                if (S_Useful::sizeof($ordersToImport))
//                    F_PostalImport::validateModels($ordersToImport);
//            }


            $this->render('importEbay/step_3', [
                'models' => $ordersToImport,
                'hash' => $hash,
            ]);
            return;
        }

    }

    protected function _ebay_step3_refresh(ebayClient $ebayClient, $hash)
    {
        $models = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));


        if(F_PostalImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
        }

        Yii::app()->cacheUserData->set('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        $this->refresh();
        exit;
    }

    protected function _ebay_step3_process(ebayClient $ebayClient, $hash)
    {
        $models = Yii::app()->cacheUserData->get('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));

        if(F_PostalImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
            Yii::app()->cacheUserData->set('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            $this->refresh();
            exit;
        }
        try
        {
            $result = Postal::saveWholeGroupNotMass($models, false, $ebayClient->getAuthToken(), Postal::SOURCE_EBAY);
        }
        catch (Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            return;
        }


        // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING
        if($result['success'])
        {
            $import = PostalImportManager::getByHash($hash);
            /* @var $item Postal */
            $ids = array();
            if(is_array($result['data']))
                foreach($result['data'] AS $item)
                {
                    array_push($ids, $item->id);
                }

            $import->source = PostalImportManager::SOURCE_EBAY;
            $import->addPostalIds($ids);
            $import->save();


            Yii::app()->cacheUserData->delete('ebayImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
            $this->redirect(Yii::app()->createUrl('/postal/postal/listImported', ['hash' => $hash]));
            exit;
        }
    }



    public function actionCn22Multi()
    {
        $ids = $_POST['Postal']['ids'];

        $models = Postal::model()
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'postal_stat_id != :stat AND postal_stat_id != :stat2 AND postal_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', [':stat' => PostalStat::CANCELLED, ':stat2' => PostalStat::CANCELLED_PROBLEM, ':stat3' => PostalStat::CANCELLED_USER, ':uc1' => Postal::USER_CANCEL_REQUEST, ':uc2' => Postal::USER_CANCEL_CONFIRMED, ':uc3' => Postal::USER_CANCEL_WARNED]);

        /* @var $model Postal */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('postal', 'Brak paczek do wygenerowania CD23!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        PostalCN22::generateMulti($models);
    }

    public function actionAjaxIsLabelReady()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $hash = $_POST['hash'];

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('postal_stat_id');
            $cmd->from('postal');
            $cmd->where('hash = :hash', [':hash' => $hash]);
            $result = $cmd->queryScalar();

            if($result && !in_array($result, [PostalStat::PACKAGE_PROCESSING, PostalStat::NEW_ORDER]))
                $result = true;
            else
                $result = false;

            echo CJSON::encode($result);

        } else
            throw new CHttpException(404);

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

    public function actionLinker($al = false, $hash = false)
    {

        $this->panelLeftMenuActive = 215;

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/postalRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        // autoload list once on enter
        if($al)
        {
            Yii::app()->session->add('linker_autoload', true);
            $this->redirect(['/postal/postal/linker']);
            exit;
        }


        if($hash)
        {
            if(isset($_POST['refresh'])) {
                $this->_linker_step3_refresh($hash);
                exit;
            }
            else if(isset($_POST['save']))
            {
                $this->_linker_step3_process($hash);
                exit;
            } else
                $this->_linker_step3($hash);
        }
        else if(isset($_POST['import']))
        {
            if($this->_linker_step1_process() === 'true')
                $this->_linker_step2();
            else
                $this->_linker_step1();

        }
        else if(isset($_POST['save-default-data']))
        {
            if($this->_linker_step2_process() === 'true')
                $this->_linker_step3($hash);

        }
        else
        {
            $this->_linker_step1();
        }
    }


    protected function _linker_step1()
    {
        $orders = [];

        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $stat = (int) $_POST['stat'];
        $statLinker = (int) $_POST['statLinker'];

        if($date_from == '')
            $date_from = Yii::app()->session->get('linker_date_from', NULL);

        if($date_to == '')
            $date_to = Yii::app()->session->get('linker_date_to', NULL);

        if(!$stat)
            $stat = Yii::app()->session->get('stat', NULL);

        if(!$statLinker)
            $statLinker = Yii::app()->session->get('statLinker', NULL);

        $date_begining = date('Y-m-d', strtotime('-'.LinkerClient::ORDERS_GOBACK_MONTHS.' months'));
        $date_end = date('Y-m-d');

        if (strtotime($date_from) > strtotime($date_to))
            $date_from = $date_to;

        if (!strtotime($date_from) OR strtotime($date_from) < strtotime($date_begining))
            $date_from = $date_end;

        if (!strtotime($date_to) OR strtotime($date_to) > strtotime($date_end))
            $date_to = $date_end;

        if (!in_array($stat, array_keys(LinkerOrderItem::getStatShowProcessedList())))
            $stat = LinkerOrderItem::STAT_SHOW_PROCESSED_ALL;

        if (!in_array($statLinker, array_keys(LinkerOrderItem::getStatShowLinkerList())))
            $statLinker = LinkerOrderItem::STAT_SHOW_LINKER_ALL;

        $sd = intval($_POST['sd']);

        $sa = $_POST['sa'];
        if (!in_array($sa, LinkerClient::sortAttributesList()))
            $sa = false;

        $more = false;
        // on load order
        if((isset($_POST['get-data']) OR Yii::app()->session->get('linker_autoload', false))) {

            Yii::app()->session->add('linker_date_from', $date_from);
            Yii::app()->session->add('linker_date_to', $date_to);
            Yii::app()->session->add('stat', $stat);
            Yii::app()->session->add('statLinker', $statLinker);

            Yii::app()->session->add('linker_autoload', false);
            list($orders, $more) = LinkerClient::getOrders($date_from, $date_to, $statLinker, LinkerImport::TARGET_POSTAL, $sa, $sd, $stat);
            Yii::app()->cacheUserData->set('linkerImport_postal_'.Yii::app()->session->sessionID, $orders, 86400);
        }

        $number = S_Useful::sizeof($orders);

        $this->render('importLinker/step_1', [
            'orders' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'stat' => $stat,
            'statLinker' => $statLinker,
            'date_begining' => $date_begining,
            'date_end' => $date_end,
            'number' => $number,
            'more' => $more,
            'sa' => $sa,
            'sd' => $sd,
        ]);
    }

    protected function _linker_step1_process()
    {
        $ordersToImport = [];
        $ordersCached = Yii::app()->cacheUserData->get('linkerImport_postal_'.Yii::app()->session->sessionID);

        if (is_array($_POST['import'])) {
            foreach ($_POST['import'] AS $importKey => $temp) {
                $temp = $ordersCached[$importKey];

                /* @var $temp LinkerOrderItem */
                // prevent already processed in system
//                if(CourierLinkerImport::findActivePackageByLinkerOrderId($temp->orderID) === false) {
                if($temp instanceof LinkerOrderItem) {
                    $temp = $temp->convertToPostalModel();
                    array_push($ordersToImport, $temp);
//                }
                } else {
                    throw new CHttpException(403);
                }

            }

            if(S_Useful::sizeof($ordersToImport) > LinkerImportPostal::MAX_IMPORT_NUMBER)
            {
                Yii::app()->user->setFlash('error', Yii::t('courier_linker', 'Maksymalna ilość paczek do importu to: {no}!', array('{no}' => LinkerImportPostal::MAX_IMPORT_NUMBER)));
                Yii::app()->session->add('linker_autoload', true);
                $this->refresh(true);
                exit;
            }

            Yii::app()->cacheUserData->set('linkerImport_postal_models_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            return "true";
        }
    }

    protected function _linker_step2()
    {
        $senderAddressData = new Postal_AddressData();
        $package = new Postal('default_package_data');
        $linkerImport = new LinkerImportPostal();

        $senderAddressData->_postal = $package;

        $senderAddressData->attributes = User::getLoggedUserAddressData();

        $this->render('importLinker/step_2', [

            'senderAddressData' => $senderAddressData,
            'package' => $package,
            'linkerImport' => $linkerImport,
        ]);
    }

    protected function _linker_step2_process()
    {

        $senderAddressData = new Postal_AddressData();
        $package = new Postal('default_package_data');
        $linkerImport = new LinkerImportPostal();

        $senderAddressData->_postal = $package;

        $senderAddressData->attributes = $_POST['Postal_AddressData'];
        $package->attributes = $_POST['Postal'];
        $linkerImport->attributes = $_POST['LinkerImportOoe'];

        $senderAddressData->validate();
        $package->validate();
        $linkerImport->validate();

        if(!$senderAddressData->hasErrors() && !$package->hasErrors() && !$linkerImport->hasErrors()) {

            $ordersToImport = Yii::app()->cacheUserData->get('linkerImport_postal_models_'.Yii::app()->session->sessionID, []);

            if(is_array($ordersToImport) && S_Useful::sizeof($ordersToImport))
            {
                /* @var $model Postal */
                foreach($ordersToImport AS &$order)
                {
                    $order->senderAddressData = new Postal_AddressData();
                    $order->senderAddressData->attributes = $senderAddressData->attributes;
                    $order->postal_type = $package->postal_type;
                    if($order->weight == '')
                        $order->weight = $package->weight;
                    $order->size_class = $package->size_class;

                    if($order->weight > 100 && $order->size_class == Postal::SIZE_CLASS_1)
                        $order->size_class = Postal::SIZE_CLASS_2;
                    else if($order->weight > 500 && $order->size_class < Postal::SIZE_CLASS_3)
                        $order->size_class = Postal::SIZE_CLASS_3;

                    if($order->value == '') {
                        $order->value = $package->value;
                        $order->value_currency = $package->value_currency;
                    }

                    if($order->value_currency == '')
                        $order->value_currency = $package->value_currency;

                    $order->target_sp_point = $package->target_sp_point;
                }

                F_PostalImport::validateModels($ordersToImport);

                Yii::app()->cacheUserData->delete('linkerImport_postal_'.Yii::app()->session->sessionID);
                Yii::app()->cacheUserData->set('linkerImport_postal_models_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            }

            return "true";
        }

        $this->render('importLinker/step_2', [

            'senderAddressData' => $senderAddressData,
            'package' => $package,
            'linkerImport' => $linkerImport,
        ]);
    }

    protected function _linker_step3($hash)
    {
        if(!$hash)
        {
            // generate new hash
            $hash = hash('sha256', time() . Yii::app()->user->model->apikey);
            $ordersToImport = Yii::app()->cacheUserData->get('linkerImport_postal_models_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu!'));

            // rename data by hash and clear previous
            Yii::app()->cacheUserData->set('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            Yii::app()->cacheUserData->delete('linkerImport_postal_models_'.Yii::app()->session->sessionID);

            $this->redirect(['/postal/postal/linker', 'hash' => $hash]);
            exit;

        } else {
            $ordersToImport = Yii::app()->cacheUserData->get('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu!'));

            // added refresh button instead
            // validate only after editing one item in row
//            if(Yii::app()->session['postalImport_e_'.$hash] == 10)
//            {
//                unset(Yii::app()->session['postalImport_e_'.$hash]);
//                if (S_Useful::sizeof($ordersToImport))
//                    F_PostalImport::validateModels($ordersToImport);
//            }


            $this->render('importLinker/step_3', [
                'models' => $ordersToImport,
                'hash' => $hash,
            ]);
            return;
        }

    }

    protected function _linker_step3_refresh($hash)
    {
        $models = Yii::app()->cacheUserData->get('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));


        if(F_PostalImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
        }

        Yii::app()->cacheUserData->set('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        $this->refresh();
        exit;
    }

    protected function _linker_step3_process($hash)
    {
        $models = Yii::app()->cacheUserData->get('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));

        if(F_PostalImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
            Yii::app()->cacheUserData->set('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            $this->refresh();
            exit;
        }

        try
        {
            $result = Postal::saveWholeGroupNotMass($models, false, false, Postal::SOURCE_LINKER);
        }
        catch (Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            return;
        }


        // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING
        if($result['success'])
        {
            $import = PostalImportManager::getByHash($hash);
            /* @var $item Postal */
            $ids = array();
            if(is_array($result['data']))
                foreach($result['data'] AS $item)
                {
                    array_push($ids, $item->id);
                }

            $import->source = PostalImportManager::SOURCE_LINKER;
            $import->addPostalIds($ids);
            $import->save();


            Yii::app()->cacheUserData->delete('linkerImport_postal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
            $this->redirect(Yii::app()->createUrl('/postal/postal/listImported', ['hash' => $hash]));
            exit;
        }
    }

}