<h1>Temporary operators changes - add new</h1>

<?php

/* @var $form GxActiveForm */

?>

<div class="form">

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-country-group-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));
    ?>


    <?php
    echo $form->errorSummary($model); ?>

    <div class="row text-center">
        <?php echo $form->labelEx($model,'from_operator_id'); ?>
        <?php echo $form->dropDownList($model,'from_operator_id', CHtml::listData(CourierInternalOperatorChange::listAvailableFromOperators(),'id','nameWithIdAndBroker'), ['prompt' => '-']); ?>
        <?php echo $form->error($model,'from_operator_id'); ?>
    </div><!-- row -->
    <div class="row text-center">
        <?php echo $form->labelEx($model,'to_operator_id'); ?>
        <?php echo $form->dropDownList($model, 'to_operator_id',  CHtml::listData(CourierOperator::listOperators(),'id','nameWithIdAndBroker'), ['prompt' => '-']); ?>
        <?php echo $form->error($model,'to_operator_id'); ?>
    </div><!-- row -->



<div class="text-center">
    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Add'), ['class' => 'btn btn-large btn-primary']);
    ?>
<br/>
<br/>
    Change will be turned OFF after addition.
</div>
<?php
$this->endWidget();
?>


</div><!-- form -->

