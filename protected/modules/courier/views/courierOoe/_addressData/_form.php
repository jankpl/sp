<?php
/* @var $form CActiveForm */
/* @var $model AddressData */
/* @var $i Integer */
/* @var $type String */
/* @var $countries CountryList[] */

if(!isset($type))
    $type = key(UserContactBook::getTypes_enum());
?>



<?php
if($noErrorSummery !== true)
    echo $form->errorSummary($model); ?>

<?php
if(!$blockAddressSuggester)
    $this->widget('AddressDataSuggester',
        array(
            'type' => $type,
            'fieldName' => 'CourierTypeOoe_AddressData',
            'countries' => $countries,
            'i' => $i,
        ))
?>


<?php $this->renderPartial('_addressData/_form_base',
    array('i' => $i, 'model' => $model, 'form' => $form, 'countries' => $countries));
?>
<?php
if(!Yii::app()->user->isGuest):
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'addToContactBook'); ?>
        <?php echo $form->checkBox($model,(isset($i)?'['.$i.']':'').'addToContactBook'); ?>
        <?php echo $form->hiddenField($model,(isset($i)?'['.$i.']':'').'contactBookType'); ?>
    </div><!-- row -->
    <?php
endif;
?>
<?php if($noRequiredInfo != true):?>
    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>
<?php endif; ?>