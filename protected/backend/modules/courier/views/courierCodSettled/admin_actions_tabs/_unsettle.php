<h4>Unsettle</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'unsettle',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courierCodSettled/settleByGridView', ['unsettle' => true]),
));
?>

<?php
echo TbHtml::submitButton('Settle', array(
    'onClick' => 'js:
    $("#courier-ids-unsettle").val($("#courier-cod-settle-grid").selGridView("getAllSelection").toString());
    ;',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => 'courier-ids-unsettle')); ?>
<?php
$this->endWidget();
?>