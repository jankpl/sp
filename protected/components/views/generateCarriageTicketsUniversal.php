<div style="width: 100%; padding: 5px; margin: 5px; text-align: left; border: 1px dashed gray; text-align: center;">
    <p style="font-size: 12px; font-weight: bold;"><?php echo Yii::t('courier','Etykiety dla wszyskich paczek typu {typ}', ['{typ}' => Courier::getTypesNames()[Courier::TYPE_EXTERNAL]]);?></p>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'universal-carriage-ticket',
    'action' => Yii::app()->createUrl('/courier/courier/carriageTicketsUniversal',
        array('mode' => $mode)
    ),
    'htmlOptions' => array('class' => 'do-not-block',
        'onsubmit' => 'return confirm("'.Yii::t('site','Are you sure?').'");',
    ),
));
?>
<?php echo CHtml::dropDownList('type','',LabelPrinter::getLabelGenerateTypes(), array('style' => 'width: 100%; display: inline-block; height: 23px; vertical-align: top;'));?>
<br/>
<br/>
<?php echo CHtml::submitButton(Yii::t('courier','Generuj listy z 24h'), array('name' => '24', 'class' => 'btn btn-default btn-sm'));?><br/>
<?php echo CHtml::submitButton(Yii::t('courier','Generuj wszystkie niewygenerowane listy'), array('name' => 'all', 'class' => 'btn btn-default btn-sm'));?>
<?php $this->endWidget();?>
</div>