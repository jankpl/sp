<div class="alert alert-danger text-center">
    <h4><?php echo Yii::t('courier','Reklamuj paczkę');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t('courier','Reklamuj'), array('/user/complaint', "hash" => $model->hash, "type" => UserComplaintForm::WHAT_COURIER), array('class' => 'btn',));?>
</div>
