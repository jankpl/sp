<?php
/* @var $form GxActiveForm */
?>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-ooe-service-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <?php echo $form->labelEx($model,'service_name'); ?>
        <?php echo $form->textField($model, 'service_name', array('maxlength' => 32)); ?> (*)
        <?php echo $form->error($model,'service_name'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model, 'title', array('maxlength' => 128)); ?> (**)
        <?php echo $form->error($model,'title'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'stat'); ?>
        <?php echo $form->dropDownList($model, 'stat', CHtml::listData(S_Status::listStats(),'id','name')); ?>
        <?php echo $form->error($model,'stat'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'countryLists'); ?>
        <?php echo $form->listBox($model, 'countryLists', CHtml::listData(CountryList::model()->findAll(),'id','name'), array('multiple' => true, 'size' => 15));?>
        <?php echo $form->error($model,'countryLists'); ?>
    </div>
    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';

        endforeach;
        ?>
    </div>

    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

            $model_tr = $modelTrs[$languageItem->id];

            if($model_tr === null)
            {
                $model_tr = new CourierOoeServiceTr();
                $model_tr->language_id = $languageItem->id;
            }


            $this->renderPartial('_tr_form', array(
                'model' => $model_tr,
                'form' => $form,
                'i' => $languageItem->id,
            ));

            echo'</div>';

        endforeach;
        ?>
    </div>

    <br/><br/>
    <div class="info">
        (*) Important: Unique serice code form OOE, for example: 51, WPX or RMEURTS<br/>
        (**) Only local name.
    </div>
    <br/>
    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>
    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->