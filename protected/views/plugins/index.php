<?php
/* @var $models Plugins[] */
?>


<h1><?= Yii::t('plugins', 'Integracje/wtyczki');?></h1>

<div class="row">
    <?php
    foreach($models AS $model):
        ?>
        <a href="<?= Yii::app()->createUrl('/plugins/view', ['id' => $model->id]);?>" class="col-xs-12 col-sm-4 plugin-item">
            <div class="plugin-item-content">
                <p><?= $model->name;?></p>
                <?php if($model->img):?>
                    <img src="<?= $model->getImgUrl();?>" class="img-responsive"/>
                    <?php
                endif;
                ?>
            </div>
        </a>

        <?php
    endforeach;
    ?>
</div>