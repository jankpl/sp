<?php

/**
 * Class TempLangChange
 * For temporary changing application language for ex. sending emails
 */
class TempLangChange
{
    protected $_base_lang = false;
    protected $_temp_lang;

    public function temporarySetLanguage($language_code)
    {
        if(Language::isLanguageCodeValid($language_code)) {

            if(!$this->_base_lang)
                $this->_base_lang = Yii::app()->language;

            Yii::app()->language = $language_code;
            Yii::app()->params['language'] = Language::autoLanguageIdReturn(Yii::app()->language);
        }
    }

    public function revertToBaseLanguage()
    {
        if($this->_base_lang)
        {
            Yii::app()->language = $this->_base_lang;
            Yii::app()->params['language'] = Language::autoLanguageIdReturn(Yii::app()->language);
        }
    }


}