<?php

class RabenClient extends GlobalOperator
{
    const URL = GLOBAL_CONFIG::RABEN_URL;
    const URL_WSDL = GLOBAL_CONFIG::RABEN_URL_WSDL;
    const USERNAME = GLOBAL_CONFIG::RABEN_LOGIN;
    const PASSWORD = GLOBAL_CONFIG::RABEN_PASSWORD;

    const FTP_HOST = GLOBAL_CONFIG::RABEN_FTP_HOST;
    const FTP_LOGIN = GLOBAL_CONFIG::RABEN_FTP_LOGIN;
    const FTP_PASSWORD = GLOBAL_CONFIG::RABEN_FTP_PASSWORD;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_tel;
    public $sender_email;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;
    public $package_content;
    public $package_value;
    public $value_currency;

    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $local_id;
    public $user_id;

    public $rod = false;
    public $rop = false;
    public $cod = false;
    public $cod_value = 0;
    public $cod_currency;

    public $opt_advise = false;
    public $opt_isms = false;
    public $opt_delivery_10 = false;
    public $opt_delivery_12 = false;
    public $opt_delivery_8 = false;
    public $opt_delivery_next_day = false;
    public $opt_delivery_with_lift = false;
    public $opt_delivery_night = false;
    public $opt_manual_loading = false;
    public $opt_special_delivery = false;
    public $opt_delivery_sat = false;


    public static function getAdditions(Courier $courier)
    {
//
        $CACHE_NAME = 'RABEN_ADDITIONS_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ROP;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ROD;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ADVISE;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ISMS;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_10;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_12;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_SAT;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_NIGHT;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_NEXT_DAY;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_WITH_LIFT;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_SPECIAL;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_MANUAL_LOADING;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }


    public static function test($returnError = true)
    {

        $model = new self;


        $model->login = 'test3';
        $model->local_id = rand(10000,99999);
        $model->package_weight = 2;
        $model->package_size_l = 100;
        $model->package_size_d = 30;
        $model->package_size_w = 30;

        $model->package_content = 'test';

        $model->sender_name = 'test 1';
        $model->sender_address1 = 'test 1';
        $model->sender_city = 'Warszawa';
        $model->sender_zip_code = '00-001';
        $model->sender_country = 'PL';
        $model->sender_tel = '123231213';
        $model->sender_email = 'dsadsa@dssad.pl';

        $model->receiver_name = 'test 2';
        $model->receiver_address1 = 'test 2';
        $model->receiver_city = 'Wrocław';
        $model->receiver_zip_code = '53-400';
        $model->receiver_country = 'PL';
        $model->receiver_tel = '222333444';
        $model->receiver_email = 'asadasdasd@wqewqqew.pl';

        $data = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:CDM/tmsIntegrationService/" xmlns:ns2="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><SOAP-ENV:Header><ns2:Security SOAP-ENV:mustUnderstand="1"><ns2:UsernameToken><ns2:Username>'.self::USERNAME.'</ns2:Username><ns2:Password>'.self::PASSWORD.'</ns2:Password></ns2:UsernameToken></ns2:Security></SOAP-ENV:Header><SOAP-ENV:Body>
<ns2:transportInstructionMessage xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns2="urn:CDM/tmsIntegrationService/" xmlns:sh="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader" xmlns:ns3="Auth">
<sh:StandardBusinessDocumentHeader>
		<sh:HeaderVersion>1</sh:HeaderVersion>
		<sh:Sender>
			<sh:Identifier>'.self::USERNAME.'</sh:Identifier>
		</sh:Sender>
		<sh:Receiver>
			<sh:Identifier>Raben Poland Test</sh:Identifier>
		</sh:Receiver>
		<sh:DocumentIdentification>
			<sh:Standard>GS1</sh:Standard>
			<sh:TypeVersion>3.2</sh:TypeVersion>
			<sh:InstanceIdentifier>100002</sh:InstanceIdentifier>
			<sh:Type>Transport Instruction</sh:Type>
			<sh:CreationDateAndTime>'.date('c').'</sh:CreationDateAndTime>
		</sh:DocumentIdentification>
		<sh:BusinessScope>
			<sh:Scope>
				<sh:Type>EDIcustomerNumber</sh:Type>
				<sh:InstanceIdentifier>90000050</sh:InstanceIdentifier>
			</sh:Scope>
			<sh:Scope>
				<sh:Type>fileType</sh:Type>
				<sh:InstanceIdentifier>NF</sh:InstanceIdentifier>
			</sh:Scope>
			<sh:Scope>
				<sh:Type>department</sh:Type>
				<sh:InstanceIdentifier>46</sh:InstanceIdentifier>
			</sh:Scope>
			<sh:Scope>
				<sh:Type>application</sh:Type>
				<sh:InstanceIdentifier>INT</sh:InstanceIdentifier>
			</sh:Scope>
		</sh:BusinessScope>
	</sh:StandardBusinessDocumentHeader>';

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(id)')->from('courier_label_new')->where(['broker' => GLOBAL_BROKERS::BROKER_RABEN]);
        $number = $cmd->queryScalar();
        $number++;
        $number++;
        $number++;
        $number++;
        $number++;
        $number++;
        $number++;
        $number++;
        $number++;
        $number++;

        $id = substr(self::USERNAME,0,6).str_pad($number, 7,'0', STR_PAD_LEFT);


        $data .= '
	<transportInstruction>
		<creationDateTime>'.date('c').'</creationDateTime>
		<documentStatusCode>ORIGINAL</documentStatusCode>
		<documentActionCode>ADD</documentActionCode>
		<transportInstructionIdentification>
			<entityIdentification>'.$id.'</entityIdentification>
		</transportInstructionIdentification>
		<transportInstructionFunction>SHIPMENT</transportInstructionFunction>
		<logisticServicesSeller/>
		<logisticServicesBuyer>
			<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">'.self::USERNAME.'</additionalPartyIdentification>
		</logisticServicesBuyer>
		<transportInstructionShipment>
			<additionalShipmentIdentification additionalShipmentIdentificationTypeCode="refopd">'.$id.'</additionalShipmentIdentification>
			<note languageCode="PL" noteTypeCode="SMS">'.$model->sender_tel.'</note>
			<note languageCode="PL" noteTypeCode="EML">'.$model->sender_email.'</note>
			<receiver>			
				<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">UNIQUE RECEIVER_ID</additionalPartyIdentification>
				<address>
					<city>'.self::_cdata($model->receiver_city).'</city>
					<countryCode>'.self::_cdata($model->receiver_country).'</countryCode>
					<name>'.self::_cdata($model->receiver_name).'</name>
					<postalCode>'.self::_cdata($model->receiver_zip_code).'</postalCode>
					<streetAddressOne>'.self::_cdata(trim($model->receiver_address1.' '.$model->receiver_address2)).'</streetAddressOne>
				</address>
				<contact>
					<contactTypeCode>BJ</contactTypeCode>
					<personName>'.self::_cdata($model->receiver_name).'</personName>
					<communicationChannel>
						<communicationChannelCode>EMAIL</communicationChannelCode>
						<communicationValue>'.self::_cdata($model->receiver_email).'</communicationValue>
					</communicationChannel>
					<communicationChannel>
						<communicationChannelCode>TELEPHONE</communicationChannelCode>
						<communicationValue>'.self::_cdata($model->receiver_tel).'</communicationValue>
					</communicationChannel>
				</contact>
			</receiver>
			<shipper>
				<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">SWIATNYSGR</additionalPartyIdentification>
                <address>
					<city>'.self::_cdata($model->sender_city).'</city>
					<countryCode>'.self::_cdata($model->sender_country).'</countryCode>
					<name>'.self::_cdata($model->sender_name).'</name>
					<postalCode>'.self::_cdata($model->sender_zip_code).'</postalCode>
					<streetAddressOne>'.self::_cdata(trim($model->sender_address1.' '.$model->sender_address2)).'</streetAddressOne>
				</address>
				<contact>
					<contactTypeCode>BJ</contactTypeCode>
					<personName>'.self::_cdata($model->sender_name).'</personName>
					<communicationChannel>
						<communicationChannelCode>EMAIL</communicationChannelCode>
						<communicationValue>'.self::_cdata($model->sender_email).'</communicationValue>
					</communicationChannel>
					<communicationChannel>
						<communicationChannelCode>TELEPHONE</communicationChannelCode>
						<communicationValue>'.self::_cdata($model->sender_tel).'</communicationValue>
					</communicationChannel>
				</contact>
			</shipper>
			<shipTo>
				<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">UNIQUE UNLOADING_PLACE_ID</additionalPartyIdentification>
				<address>
					<city>'.self::_cdata($model->sender_city).'</city>
					<countryCode>'.self::_cdata($model->sender_country).'</countryCode>
					<name>'.self::_cdata($model->sender_name).'</name>
					<postalCode>'.self::_cdata($model->sender_zip_code).'</postalCode>
					<streetAddressOne>'.self::_cdata(trim($model->sender_address1.' '.$model->sender_address2)).'</streetAddressOne>
				</address>
				<contact>
					<contactTypeCode>BJ</contactTypeCode>
					<personName>'.self::_cdata($model->sender_name).'</personName>
					<communicationChannel>
						<communicationChannelCode>EMAIL</communicationChannelCode>
						<communicationValue>'.self::_cdata($model->sender_email).'</communicationValue>
					</communicationChannel>
					<communicationChannel>
						<communicationChannelCode>TELEPHONE</communicationChannelCode>
						<communicationValue>'.self::_cdata($model->sender_tel).'</communicationValue>
					</communicationChannel>
				</contact>
			</shipTo>
			<shipFrom>
				<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">UNIQUE LOADING_PLACE_ID</additionalPartyIdentification>
				 <address>
					<city>'.self::_cdata($model->sender_city).'</city>
					<countryCode>'.self::_cdata($model->sender_country).'</countryCode>
					<name>'.self::_cdata($model->sender_name).'</name>
					<postalCode>'.self::_cdata($model->sender_zip_code).'</postalCode>
					<streetAddressOne>'.self::_cdata(trim($model->sender_address1.' '.$model->sender_address2)).'</streetAddressOne>
				</address>
				<contact>
					<contactTypeCode>BJ</contactTypeCode>
					<personName>'.self::_cdata($model->sender_name).'</personName>
					<communicationChannel>
						<communicationChannelCode>EMAIL</communicationChannelCode>
						<communicationValue>'.self::_cdata($model->sender_email).'</communicationValue>
					</communicationChannel>
					<communicationChannel>
						<communicationChannelCode>TELEPHONE</communicationChannelCode>
						<communicationValue>'.self::_cdata($model->sender_tel).'</communicationValue>
					</communicationChannel>
				</contact>
			</shipFrom>
			<transportInstructionTerms>
				<transportServiceCategoryType>30</transportServiceCategoryType>';

        if($model->rop)
            $data .= '<logisticService>
					<logisticServiceRequirementCode logisticServiceTypeCode="parameters">ROP</logisticServiceRequirementCode>
				</logisticService>';
        if($model->rod)
            $data .= '<logisticService>
					<logisticServiceRequirementCode logisticServiceTypeCode="parameters">ROD</logisticServiceRequirementCode>
				</logisticService>';
        if($model->cod)
            $data .= '<logisticService>
                        <logisticServiceRequirementCode>COD</logisticServiceRequirementCode> 
                        <cashOnDeliveryAmount currencyCode="'.$model->cod_currency.'">'.$model->cod_value.'</cashOnDeliveryAmount>
                     </logisticService>';


        $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">DELIVERY_BY_10AMa</logisticServiceRequirementCode>
                </logisticService>';


        $data .= '</transportInstructionTerms>';

        if($model->sender_country != $model->receiver_country)
            $data .= '<deliveryTerms>
				<incotermsCode>DDP</incotermsCode>
			</deliveryTerms>';

        $data .= '<transportReference>
				<entityIdentification>'.$model->local_id.'</entityIdentification>
				<transportReferenceTypeCode>'.self::_cdata($model->login).'</transportReferenceTypeCode>
			</transportReference>			
			<transportInstructionShipmentItem>
				<lineItemNumber>1</lineItemNumber>
				<logisticUnit>	
					<grossWeight measurementUnitCode="KGM">'.$model->package_weight.'</grossWeight>
					<dimension>
						<depth measurementUnitCode="MTR">'.round($model->package_size_d/100,2).'</depth>
						<height measurementUnitCode="MTR">'.round($model->package_size_l/100,2).'</height>
						<width measurementUnitCode="MTR">'.round($model->package_size_w/100,2).'</width>
					</dimension>
				</logisticUnit>			
				<transportCargoCharacteristics>			
					<cargoTypeCode>12</cargoTypeCode>
					<cargoTypeDescription languageCode="PL">'.self::_cdata($model->package_content).'</cargoTypeDescription>
					<totalGrossVolume measurementUnitCode="MTQ">'.round(round($model->package_size_d/100,2)*round($model->package_size_l/100,2)*round($model->package_size_w/100,2),2).'</totalGrossVolume>
					<totalGrossWeight measurementUnitCode="KGM">'.$model->package_weight.'</totalGrossWeight>
					<totalTransportNetWeight measurementUnitCode="KGM">'.$model->package_weight.'</totalTransportNetWeight>
					<totalLoadingLength measurementUnitCode="MTR">0.4</totalLoadingLength>
					<totalLoadingLength measurementUnitCode="PP">1</totalLoadingLength>
					<totalPackageQuantity measurementUnitCode="ct">1</totalPackageQuantity>
					<totalItemQuantity measurementUnitCode="ep">1</totalItemQuantity>					
				</transportCargoCharacteristics>				
			</transportInstructionShipmentItem>						
		</transportInstructionShipment>
	</transportInstruction>';

        $data .= '</ns2:transportInstructionMessage>
	</SOAP-ENV:Body></SOAP-ENV:Envelope>
	';

        $response = self::_curlCall('importTransportInstruction', $data);


        $response = str_replace(['soapenv:', 'tms:', 'stan:', '<Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">','<Body>', '</Body>', '</Envelope>', 'xmlns:', ' tms="urn:CDM/tmsIntegrationService/"',' stan="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader"'], '', $response);
        $response = str_replace(['&gt;', '&lt;'], ['<', '>'], $response);

        $xml = simplexml_load_string($response);


        if($xml->transportInstructionResponse->responseType != 'FULLY_ACCEPTED')
        {
            var_dump((string) $xml->transportInstructionResponse->transportInstructionShipment->note);
            echo 'DUPA!';
            exit;

        }

        // user $parser to get your data out of XML response and to display it.

        var_dump((string) $xml->transportInstructionResponseMessage->asXML());
        var_dump($xml->transportInstructionResponseMessage->asXML());
        var_dump($xml);
        $id1 = (string) $xml->transportInstructionResponse->transportInstructionShipment->additionalShipmentIdentification[0];
        $id2 = (string) $xml->transportInstructionResponse->transportInstructionShipment->additionalShipmentIdentification[1];

        var_dump($id1);
        var_dump($id2);

// LABEL:

        $data = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:CDM/tmsIntegrationService/" xmlns:ns2="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><SOAP-ENV:Header><ns2:Security SOAP-ENV:mustUnderstand="1"><ns2:UsernameToken><ns2:Username>'.self::USERNAME.'</ns2:Username><ns2:Password>'.self::PASSWORD.'</ns2:Password></ns2:UsernameToken></ns2:Security></SOAP-ENV:Header><SOAP-ENV:Body>
<tms:transportDocumentRequestMessage xmlns:tms="urn:CDM/tmsIntegrationService/" xmlns:stan="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader">
<stan:StandardBusinessDocumentHeader>
	<stan:HeaderVersion>3.2</stan:HeaderVersion>
	<stan:Sender>
		<stan:Identifier>'.self::USERNAME.'</stan:Identifier>
	</stan:Sender>
	<stan:Receiver>
		<stan:Identifier>Raben Poland Test</stan:Identifier>
	</stan:Receiver>
	<stan:DocumentIdentification>
		<stan:Standard>GS1</stan:Standard>
		<stan:TypeVersion>3.2</stan:TypeVersion>
		<stan:InstanceIdentifier>100002</stan:InstanceIdentifier>
		<stan:Type>Transport_Document_Request_Message</stan:Type>
		<stan:CreationDateAndTime>'.date('c').'</stan:CreationDateAndTime>
	</stan:DocumentIdentification>
</stan:StandardBusinessDocumentHeader>';



        $data .= '
	<transportDocumentRequest>
		<creationDateTime>'.date('c').'</creationDateTime>
		<documentStatusCode>ADDITIONAL_TRANSMISSION</documentStatusCode>
		<documentActionCode>GET_DOC_PDF</documentActionCode>
		<transportDocumentRequestIdentification>
			<entityIdentification>'.$id.'</entityIdentification>
		</transportDocumentRequestIdentification>
		<transportDocumentInformationCode>ALL_DOCUMENTS</transportDocumentInformationCode>
		<transportDocumentObjectCode>MTOM</transportDocumentObjectCode>
<transportDocumentRequestShipment>
			<additionalShipmentIdentification additionalShipmentIdentificationTypeCode="refopd">'.$id1.'</additionalShipmentIdentification>
			<additionalShipmentIdentification additionalShipmentIdentificationTypeCode="dosvlg">'.$id2.'</additionalShipmentIdentification>
		</transportDocumentRequestShipment>
</transportDocumentRequest>
';



        $data .= '</tms:transportDocumentRequestMessage>
	</SOAP-ENV:Body></SOAP-ENV:Envelope>
	';   // data from the form, e.g. some ID number

        $response = self::_curlCall('getTransportDocument', $data);



        $response = str_replace(['soapenv:', 'tms:', 'stan:', '<Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">','<Body>', '</Body>', '</Envelope>', 'xmlns:', ' tms="urn:CDM/tmsIntegrationService/"',' stan="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader"', '<Header/>'], '', $response);
        $response = str_replace(['&gt;', '&lt;'], ['<', '>'], $response);

        $label = explode('%PDF-1.4', $response);
        $label = $label[1];

        if($label == '')
        {
            var_dump('NIE MA ETYKIETY!');
            exit;
        }


        $label = explode('%%EOF', $label);
        $label = $label[0];
        $label = trim($label);
        $label = '%PDF-1.4'.$label.'%%EOF';

//        $tt = $resp->data->ShippingParameter[0]->Value;

        file_put_contents('raben.pdf', $label);

        $tt = $id2;



        $uniqId = uniqid();
        $basePath = Yii::app()->basePath . '/../';
        $dir = 'uplabels/';
        $temp = $dir . 'temp/';
        @file_put_contents($basePath . $temp . $uniqId . '.pdf', $label);

        $labels = [];
        for ($i = 0; $i < 5; $i++) {

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImage($basePath . $temp . $uniqId . '.pdf['.$i.']');
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            $im->trimImage(0);
            $im->stripImage();

            $labels[] = $im->getImageBlob();
        }
        @unlink($basePath . $temp . $uniqId . '.pdf');


        foreach($labels AS $key => $item)
        {
            file_put_contents('raben_'.$key.'.png', $item);
        }

        exit;

        $return[] = GlobalOperatorResponse::createSuccessResponse($labels, $tt);
        $im->stripImage();


        file_put_contents('raben.png', $im->getImageBlob());
        exit;

        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $tt);

        if ($resp->success == true) {
            $tt = $resp->data->ShippingParameter[0]->Value;


            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImageBlob($label);

            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $tt);
            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }


    }

    protected static function _curlCall($method, $data)
    {
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: ".$method,
            "Content-length: ".strlen($data),
        );

        $pemfile = Yii::getPathOfAlias('application.components.rabenCert').'/cert.pem';
        $keyfile = Yii::getPathOfAlias('application.components.rabenCert').'/key.pem';




        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, self::URL_WSDL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_SSLKEY, $keyfile);
        curl_setopt($ch, CURLOPT_SSLCERT, $pemfile);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, '23rewaC#r');

        $response = curl_exec($ch);

        MyDump::dump('raben.txt', $method);
        MyDump::dump('raben.txt', $data);
        MyDump::dump('raben.txt', print_r(curl_error($ch),1));
        MyDump::dump('raben.txt', $response);

        curl_close($ch);

        return $response;
    }

    protected function _createShipment($returnError = false)
    {
        if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('Not active!');
            } else {
                return false;
            }

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(id)')->from('courier_label_new')->where(['broker' => GLOBAL_BROKERS::BROKER_RABEN]);
        $number = $cmd->queryScalar();

        $number += 10; // first numbers were used for tests;

        $id = substr(self::USERNAME,0,6).str_pad($number, 7,'0', STR_PAD_LEFT);

        $data = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:CDM/tmsIntegrationService/" xmlns:ns2="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><SOAP-ENV:Header><ns2:Security SOAP-ENV:mustUnderstand="1"><ns2:UsernameToken><ns2:Username>'.self::USERNAME.'</ns2:Username><ns2:Password>'.self::PASSWORD.'</ns2:Password></ns2:UsernameToken></ns2:Security></SOAP-ENV:Header><SOAP-ENV:Body>
<ns2:transportInstructionMessage xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns2="urn:CDM/tmsIntegrationService/" xmlns:sh="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader" xmlns:ns3="Auth">
<sh:StandardBusinessDocumentHeader>
		<sh:HeaderVersion>1</sh:HeaderVersion>
		<sh:Sender>
			<sh:Identifier>'.self::USERNAME.'</sh:Identifier>
		</sh:Sender>
		<sh:Receiver>
			<sh:Identifier>Raben Poland Test</sh:Identifier>
		</sh:Receiver>
		<sh:DocumentIdentification>
			<sh:Standard>GS1</sh:Standard>
			<sh:TypeVersion>3.2</sh:TypeVersion>
			<sh:InstanceIdentifier>100002</sh:InstanceIdentifier>
			<sh:Type>Transport Instruction</sh:Type>
			<sh:CreationDateAndTime>'.date('c').'</sh:CreationDateAndTime>
		</sh:DocumentIdentification>
		<sh:BusinessScope>
			<sh:Scope>
				<sh:Type>EDIcustomerNumber</sh:Type>
				<sh:InstanceIdentifier>90000050</sh:InstanceIdentifier>
			</sh:Scope>
			<sh:Scope>
				<sh:Type>fileType</sh:Type>
				<sh:InstanceIdentifier>NF</sh:InstanceIdentifier>
			</sh:Scope>
			<sh:Scope>
				<sh:Type>department</sh:Type>
				<sh:InstanceIdentifier>46</sh:InstanceIdentifier>
			</sh:Scope>
			<sh:Scope>
				<sh:Type>application</sh:Type>
				<sh:InstanceIdentifier>INT</sh:InstanceIdentifier>
			</sh:Scope>
		</sh:BusinessScope>
	</sh:StandardBusinessDocumentHeader>';

        $data .= '
	<transportInstruction>
		<creationDateTime>'.date('c').'</creationDateTime>
		<documentStatusCode>ORIGINAL</documentStatusCode>
		<documentActionCode>ADD</documentActionCode>
		<transportInstructionIdentification>
			<entityIdentification>'.$id.'</entityIdentification>
		</transportInstructionIdentification>
		<transportInstructionFunction>SHIPMENT</transportInstructionFunction>
		<logisticServicesSeller/>
		<logisticServicesBuyer>
			<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">'.self::USERNAME.'</additionalPartyIdentification>
		</logisticServicesBuyer>
		<transportInstructionShipment>
			<additionalShipmentIdentification additionalShipmentIdentificationTypeCode="refopd">'.$id.'</additionalShipmentIdentification>
			<note languageCode="PL" noteTypeCode="SMS">'.$this->sender_tel.'</note>
			<note languageCode="PL" noteTypeCode="EML">'.$this->sender_email.'</note>
			<note languageCode="PL" noteTypeCode="INF">'.$this->receiver_tel.'</note>
			<receiver>			
				<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">UNIQUE RECEIVER_ID</additionalPartyIdentification>
				<address>
					<city>'.self::_cdata($this->receiver_city).'</city>
					<countryCode>'.self::_cdata($this->receiver_country).'</countryCode>
					<name>'.self::_cdata($this->receiver_name).'</name>
					<postalCode>'.self::_cdata($this->receiver_zip_code).'</postalCode>
					<streetAddressOne>'.self::_cdata(trim($this->receiver_address1.' '.$this->receiver_address2)).'</streetAddressOne>
				</address>
				<contact>
					<contactTypeCode>BJ</contactTypeCode>
					<personName>'.self::_cdata($this->receiver_name != '' ? $this->receiver_name : $this->receiver_company).'</personName>
					<communicationChannel>
						<communicationChannelCode>EMAIL</communicationChannelCode>
						<communicationValue>'.self::_cdata($this->receiver_email).'</communicationValue>
					</communicationChannel>
					<communicationChannel>
						<communicationChannelCode>TELEPHONE</communicationChannelCode>
						<communicationValue>'.self::_cdata($this->receiver_tel).'</communicationValue>
					</communicationChannel>
				</contact>
			</receiver>
			<shipper>
				<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">SWIATNYSGR</additionalPartyIdentification>
                <address>
					<city>'.self::_cdata($this->sender_city).'</city>
					<countryCode>'.self::_cdata($this->sender_country).'</countryCode>
					<name>'.self::_cdata($this->sender_name).'</name>
					<postalCode>'.self::_cdata($this->sender_zip_code).'</postalCode>
					<streetAddressOne>'.self::_cdata(trim($this->sender_address1.' '.$this->sender_address2)).'</streetAddressOne>
				</address>
				<contact>
					<contactTypeCode>BJ</contactTypeCode>
					<personName>'.self::_cdata($this->sender_name).'</personName>
					<communicationChannel>
						<communicationChannelCode>EMAIL</communicationChannelCode>
						<communicationValue>'.self::_cdata($this->sender_email).'</communicationValue>
					</communicationChannel>
					<communicationChannel>
						<communicationChannelCode>TELEPHONE</communicationChannelCode>
						<communicationValue>'.self::_cdata($this->sender_tel).'</communicationValue>
					</communicationChannel>
				</contact>
			</shipper>
			<shipTo>
				<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">UNIQUE UNLOADING_PLACE_ID</additionalPartyIdentification>
				<address>
					<city>'.self::_cdata($this->receiver_city).'</city>
					<countryCode>'.self::_cdata($this->receiver_country).'</countryCode>
					<name>'.self::_cdata($this->receiver_name).'</name>
					<postalCode>'.self::_cdata($this->receiver_zip_code).'</postalCode>
					<streetAddressOne>'.self::_cdata(trim($this->receiver_address1.' '.$this->receiver_address2)).'</streetAddressOne>
				</address>
				<contact>
					<contactTypeCode>BJ</contactTypeCode>
					<personName>'.self::_cdata($this->receiver_name != '' ? $this->receiver_name : $this->receiver_company).'</personName>
					<communicationChannel>
						<communicationChannelCode>EMAIL</communicationChannelCode>
						<communicationValue>'.self::_cdata($this->receiver_email).'</communicationValue>
					</communicationChannel>
					<communicationChannel>
						<communicationChannelCode>TELEPHONE</communicationChannelCode>
						<communicationValue>'.self::_cdata($this->receiver_tel).'</communicationValue>
					</communicationChannel>
				</contact>
			</shipTo>
			<shipFrom>
				<additionalPartyIdentification additionalPartyIdentificationTypeCode="searchname">UNIQUE LOADING_PLACE_ID</additionalPartyIdentification>
				 <address>
					<city>'.self::_cdata($this->sender_city).'</city>
					<countryCode>'.self::_cdata($this->sender_country).'</countryCode>
					<name>'.self::_cdata($this->sender_name).'</name>
					<postalCode>'.self::_cdata($this->sender_zip_code).'</postalCode>
					<streetAddressOne>'.self::_cdata(trim($this->sender_address1.' '.$this->sender_address2)).'</streetAddressOne>
				</address>
				<contact>
					<contactTypeCode>BJ</contactTypeCode>
					<personName>'.self::_cdata($this->sender_name).'</personName>
					<communicationChannel>
						<communicationChannelCode>EMAIL</communicationChannelCode>
						<communicationValue>'.self::_cdata($this->sender_email).'</communicationValue>
					</communicationChannel>
					<communicationChannel>
						<communicationChannelCode>TELEPHONE</communicationChannelCode>
						<communicationValue>'.self::_cdata($this->sender_tel).'</communicationValue>
					</communicationChannel>
				</contact>
			</shipFrom>
			<transportInstructionTerms>
				<transportServiceCategoryType>30</transportServiceCategoryType>';

        if($this->rop)
            $data .= '<logisticService>
					<logisticServiceRequirementCode logisticServiceTypeCode="parameters">ROP</logisticServiceRequirementCode>
				</logisticService>';
        if($this->rod)
            $data .= '<logisticService>
					<logisticServiceRequirementCode logisticServiceTypeCode="parameters">ROD</logisticServiceRequirementCode>
				</logisticService>';
        if($this->cod)
            $data .= '<logisticService>
                        <logisticServiceRequirementCode>COD</logisticServiceRequirementCode> 
                        <cashOnDeliveryAmount currencyCode="'.$this->cod_currency.'">'.$this->cod_value.'</cashOnDeliveryAmount>
                     </logisticService>';

        if($this->opt_advise)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">ADVISE</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_delivery_8)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">DELIVERY_BY_10AM</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_delivery_12)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">DELIVERY_BY_NOON</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_delivery_next_day)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">NEXT_DAY_GUARANTEED</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_delivery_night)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">NIGHT_DELIVERY</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_delivery_10)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">DELIVERY_BY_10AM</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_delivery_sat)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">SATURDAY_DELIVERY</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_delivery_with_lift)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">DELIVERY_WITH_LIFT</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_isms)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">ISMS</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_manual_loading)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">MANUAL_LOADING</logisticServiceRequirementCode>
                </logisticService>';

        if($this->opt_special_delivery)
            $data .= '<logisticService>
                    <logisticServiceRequirementCode logisticServiceTypeCode="serviceLevel2">SPECIAL_DELIVERY</logisticServiceRequirementCode>
                </logisticService>';

        $data .= '</transportInstructionTerms>';

        if($this->sender_country != $this->receiver_country)
            $data .= '<deliveryTerms>
				<incotermsCode>DDP</incotermsCode>
			</deliveryTerms>';

        $data .= '<transportReference>
				<entityIdentification>'.$this->local_id.'</entityIdentification>
				<transportReferenceTypeCode>'.self::_cdata($this->login).'</transportReferenceTypeCode>
			</transportReference>			
			<transportInstructionShipmentItem>
				<lineItemNumber>1</lineItemNumber>
				<logisticUnit>	
					<grossWeight measurementUnitCode="KGM">'.$this->package_weight.'</grossWeight>
					<dimension>
						<depth measurementUnitCode="MTR">'.round($this->package_size_l/100,2).'</depth>
						<height measurementUnitCode="MTR">'.round($this->package_size_d/100,2).'</height>
						<width measurementUnitCode="MTR">'.round($this->package_size_w/100,2).'</width>
					</dimension>
				</logisticUnit>			
				<transportCargoCharacteristics>			
					<cargoTypeCode>12</cargoTypeCode>
					<cargoTypeDescription languageCode="PL">'.self::_cdata($this->package_content).'</cargoTypeDescription>
					<totalGrossVolume measurementUnitCode="MTQ">'.round(round($this->package_size_d/100,2)*round($this->package_size_l/100,2)*round($this->package_size_w/100,2),2).'</totalGrossVolume>
					<totalGrossWeight measurementUnitCode="KGM">'.$this->package_weight.'</totalGrossWeight>
					<totalTransportNetWeight measurementUnitCode="KGM">'.$this->package_weight.'</totalTransportNetWeight>
					<totalLoadingLength measurementUnitCode="MTR">1</totalLoadingLength>
					<totalLoadingLength measurementUnitCode="PP">1</totalLoadingLength>
					<totalPackageQuantity measurementUnitCode="ep">1</totalPackageQuantity>
					<totalItemQuantity measurementUnitCode="ep">1</totalItemQuantity>					
				</transportCargoCharacteristics>				
			</transportInstructionShipmentItem>						
		</transportInstructionShipment>
	</transportInstruction>';

        $data .= '</ns2:transportInstructionMessage>
	</SOAP-ENV:Body></SOAP-ENV:Envelope>
	';


        $response = self::_curlCall('importTransportInstruction', $data);


        $response = str_replace(['soapenv:', 'tms:', 'stan:', '<Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">','<Body>', '</Body>', '</Envelope>', 'xmlns:', ' tms="urn:CDM/tmsIntegrationService/"',' stan="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader"'], '', $response);
        $response = str_replace(['&gt;', '&lt;'], ['<', '>'], $response);

        $xml = @simplexml_load_string($response);
        if(!$xml)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('Invalid response from RABEN.');
            } else {
                return false;
            }
        }

        if($xml->transportInstructionResponse->responseType != 'FULLY_ACCEPTED')
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse((string) $xml->transportInstructionResponse->transportInstructionShipment->note);
            } else {
                return false;
            }
        }

        $id1 = (string) $xml->transportInstructionResponse->transportInstructionShipment->additionalShipmentIdentification[0];
        $id2 = (string) $xml->transportInstructionResponse->transportInstructionShipment->additionalShipmentIdentification[1];

        // LABEL:
        $data = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:CDM/tmsIntegrationService/" xmlns:ns2="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><SOAP-ENV:Header><ns2:Security SOAP-ENV:mustUnderstand="1"><ns2:UsernameToken><ns2:Username>'.self::USERNAME.'</ns2:Username><ns2:Password>'.self::PASSWORD.'</ns2:Password></ns2:UsernameToken></ns2:Security></SOAP-ENV:Header><SOAP-ENV:Body>
<tms:transportDocumentRequestMessage xmlns:tms="urn:CDM/tmsIntegrationService/" xmlns:stan="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader">
<stan:StandardBusinessDocumentHeader>
	<stan:HeaderVersion>3.2</stan:HeaderVersion>
	<stan:Sender>
		<stan:Identifier>'.self::USERNAME.'</stan:Identifier>
	</stan:Sender>
	<stan:Receiver>
		<stan:Identifier>Raben Poland Test</stan:Identifier>
	</stan:Receiver>
	<stan:DocumentIdentification>
		<stan:Standard>GS1</stan:Standard>
		<stan:TypeVersion>3.2</stan:TypeVersion>
		<stan:InstanceIdentifier>100002</stan:InstanceIdentifier>
		<stan:Type>Transport_Document_Request_Message</stan:Type>
		<stan:CreationDateAndTime>'.date('c').'</stan:CreationDateAndTime>
	</stan:DocumentIdentification>
</stan:StandardBusinessDocumentHeader>';

        // FOR PL-PL we get only label and generate ACK by ourself
        if($this->sender_country == $this->receiver_country && strtoupper($this->sender_country) == 'PL')
        {
            $what = 'LABELS';
            $labelsNo = 1;
        } else {
            $what = 'ALL_DOCUMENTS';
            $labelsNo = 2;
        }


        $data .= '
	<transportDocumentRequest>
		<creationDateTime>'.date('c').'</creationDateTime>
		<documentStatusCode>ADDITIONAL_TRANSMISSION</documentStatusCode>
		<documentActionCode>GET_DOC_PDF</documentActionCode>
		<transportDocumentRequestIdentification>
			<entityIdentification>'.$id.'</entityIdentification>
		</transportDocumentRequestIdentification>
		<transportDocumentInformationCode>'.$what.'</transportDocumentInformationCode>
		<transportDocumentObjectCode>MTOM</transportDocumentObjectCode>
<transportDocumentRequestShipment>
			<additionalShipmentIdentification additionalShipmentIdentificationTypeCode="refopd">'.$id1.'</additionalShipmentIdentification>
			<additionalShipmentIdentification additionalShipmentIdentificationTypeCode="dosvlg">'.$id2.'</additionalShipmentIdentification>
		</transportDocumentRequestShipment>
</transportDocumentRequest>
';

        $data .= '</tms:transportDocumentRequestMessage>
	</SOAP-ENV:Body></SOAP-ENV:Envelope>
	';   // data from the form, e.g. some ID number

        $response = self::_curlCall('getTransportDocument', $data);

        $response = str_replace(['soapenv:', 'tms:', 'stan:', '<Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">','<Body>', '</Body>', '</Envelope>', 'xmlns:', ' tms="urn:CDM/tmsIntegrationService/"',' stan="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader"', '<Header/>'], '', $response);
        $response = str_replace(['&gt;', '&lt;'], ['<', '>'], $response);

        $label = explode('%PDF-1.4', $response);
        $label = $label[1];

        if($label == '')
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('Error fetching label from RABEN!');
            } else {
                return false;
            }
        }

        $label = explode('%%EOF', $label);
        $label = $label[0];
        $label = trim($label);
        $label = '%PDF-1.4'.$label.'%%EOF';

        $tt = $id2;

        $labels = [];
        $ack = false;
        if($labelsNo > 1) {
            $uniqId = uniqid();
            $basePath = Yii::app()->basePath . '/../';
            $dir = 'uplabels/';
            $temp = $dir . 'temp/';
            @file_put_contents($basePath . $temp . $uniqId . '.pdf', $label);
        }

        for ($i = 0; $i < $labelsNo; $i++) {

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            if($labelsNo > 1)
                $im->readImage($basePath . $temp . $uniqId . '.pdf[' . $i . ']');
            else
                $im->readImageBlob($label);

            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            if (!$i) {
                $im->trimImage(0);
                $im->rotateImage(new ImagickPixel(), 90);
            }

            $im->stripImage();

            if(!$i)
                $labels[] = $im->getImageBlob();
            else {
                $im->rotateImage(new ImagickPixel(), 90);
                $ack = $im->getImageBlob();
            }
        }

        if($labelsNo > 1)
            @unlink($basePath . $temp . $uniqId . '.pdf');

        // FOR PL-PL we get only label and generate ACK by ourself
        if($labelsNo == 1)
           list($ackPdf, $ack) = $this->generateWZ($id2);

        if($ack)
        {
            $conn_id = @ftp_connect(self::FTP_HOST);
            if(@ftp_login($conn_id, self::FTP_LOGIN, self::FTP_PASSWORD))
            {
                @ftp_put($conn_id, $id2.'.pdf', 'data://text/plain;base64,'.base64_encode($ackPdf), FTP_BINARY);
            }
            @ftp_close($conn_id);
        }

        $return[] = GlobalOperatorResponse::createSuccessResponse($labels, $tt, CourierExtExtAckCards::TYPE_ACK, $ack);
        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnError)
    {
        if ($returnError == true) {
            return GlobalOperatorResponse::createErrorResponse('Not active!');
        } else {
            return false;
        }
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, CourierLabelNew $cln, $returnError = false)
    {
        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model = new self;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ROP))
            $model->rop = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ROD))
            $model->rod = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ADVISE))
            $model->opt_advise = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ISMS))
            $model->opt_isms = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_10))
            $model->opt_delivery_10 = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_12))
            $model->opt_delivery_12 = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8))
            $model->opt_delivery_8 = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_NEXT_DAY))
            $model->opt_delivery_next_day = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_NIGHT))
            $model->opt_delivery_night = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_WITH_LIFT))
            $model->opt_delivery_with_lift = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_SPECIAL))
            $model->opt_special_delivery = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_MANUAL_LOADING))
            $model->opt_manual_loading = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_SAT))
            $model->opt_delivery_sat = true;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_tel = $from->tel;
        $model->sender_mail = $from->fetchEmailAddress(true);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->package_weight = $courierTypeU->courier->getWeight(true);
        $model->package_content = $courierTypeU->courier->package_content;
        $model->package_size_l = $courierTypeU->courier->package_size_l;
        $model->package_size_d = $courierTypeU->courier->package_size_d;
        $model->package_size_w = $courierTypeU->courier->package_size_w;

        $model->package_value = $courierTypeU->courier->getPackageValueConverted('PLN');
        $model->value_currency = 'PLN';

        if($model->cod) {
            $model->cod = true;
            $model->cod_value = $courierTypeU->courier->cod_value;
            $model->cod_currency = $courierTypeU->courier->cod_currency;
        }

        $model->local_id = $courierTypeU->courier->local_id;
        $model->user_id = $courierTypeU->courier->user_id;

        return $model->_createShipment($returnError);
    }


    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        $data = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:CDM/tmsIntegrationService/" xmlns:ns2="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><SOAP-ENV:Header><ns2:Security SOAP-ENV:mustUnderstand="1"><ns2:UsernameToken><ns2:Username>'.self::USERNAME.'</ns2:Username><ns2:Password>'.self::PASSWORD.'</ns2:Password></ns2:UsernameToken></ns2:Security></SOAP-ENV:Header><SOAP-ENV:Body>
<tms:transportStatusRequestMessage xmlns:tms="urn:CDM/tmsIntegrationService/" xmlns:stan="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader">
<stan:StandardBusinessDocumentHeader>
	<stan:HeaderVersion>3.2</stan:HeaderVersion>
	<stan:Sender>
		<stan:Identifier>'.self::USERNAME.'</stan:Identifier>
	</stan:Sender>
	<stan:Receiver>
		<stan:Identifier>Raben Poland Test</stan:Identifier>
	</stan:Receiver>
	<stan:DocumentIdentification>
		<stan:Standard>GS1</stan:Standard>
		<stan:TypeVersion>3.2</stan:TypeVersion>
		<stan:InstanceIdentifier>100002</stan:InstanceIdentifier>
		<stan:Type>Transport_Status_Request_Message</stan:Type>
		<stan:CreationDateAndTime>'.date('c').'</stan:CreationDateAndTime>
	</stan:DocumentIdentification>
</stan:StandardBusinessDocumentHeader>';

        $data .= '<transportStatusRequest>
		<creationDateTime>2016-02-02T13:38:07+01:00</creationDateTime>
		<documentStatusCode>ADDITIONAL_TRANSMISSION</documentStatusCode>
		<transportStatusRequestIdentification>
			<entityIdentification>'.$no.'</entityIdentification>
		</transportStatusRequestIdentification>
		<transportStatusInformationCode>STATUS_ONLY</transportStatusInformationCode>
		<transportStatusObjectCode>SHIPMENT</transportStatusObjectCode>
		<transportStatusRequestShipment>
			<additionalShipmentIdentification additionalShipmentIdentificationTypeCode="refopd">'.$no.'</additionalShipmentIdentification>
		</transportStatusRequestShipment>
	</transportStatusRequest>';

        $data .= '</tms:transportStatusRequestMessage>
	</SOAP-ENV:Body></SOAP-ENV:Envelope>
	';

        $response = self::_curlCall('transportStatusRequest', $data);
        $response = str_replace(['soapenv:', 'tms:', 'stan:', '<Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">','<Body>', '</Body>', '</Envelope>', 'xmlns:', ' tms="urn:CDM/tmsIntegrationService/"',' stan="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader"'], '', $response);
        $response = str_replace(['&gt;', '&lt;'], ['<', '>'], $response);

        $xml = @simplexml_load_string($response);

        var_dump($xml);
        exit;

        return false;
    }


    public function generateWZ($ttNumber)
    {

        $bg = 'iVBORw0KGgoAAAANSUhEUgAABMAAAAKGCAMAAACbRRfvAAAC/VBMVEX////W3OQAAADtEiH//dL///ny//8XAAB2veoZdr0AABgAAFFGndeLzPL///IAACv92Z3xvXkAGXm98f29djOdRhliFwP/8b0ARp3ZnVU9AACd2fcji8zZ/f+tXyZ2GQD7zIsAFmzMi0D//+vmrWhfreHM+/8AI4ut5vsAADNGAQCf0OI9icP8+/sAPo3V0aufZS0VZqmLIwLKo27/5q3//+Jjo9F2PhiDu9wAAEXW3NEAXq3W2cHL3OTm/f8BPnsEGGGDPRfUu4y2h0wAAD+rrKoAAGf/89cjAAC22OPW3N7m//9EABntGCT//eUYXZHzzI7xfn4WABdeBUONWyv//eD95a3zk5SLIzjMi0tCATcAACO9eUftJCzmvYBQVVXtMjfM8/0jI4ud2PEZADMZYq0rKyrL5vnW1tc9ebjY8/0WWp5/gIBfjsHuQEM9cKd5fHr/88k7PDr95rrTw6BZGAFbXFpeAAD+8/LB2uSszuH95sisd0W+vb81AADmy5rzmJgQEBBBCVb629q5i1yedkWLQRl2GRmNyub74+NendNpbGry/+52rtXw2J+TlZO8oXP50M8jAEBjOBjm/vYYAEYkJCTE9ebz2bt2ocT4w8IAOm31razE89M8cZirpHobAFbc+tXx3K12oaT96+rPz8/ZrXm95vnVxrNQlMQdGXJ/YzccHBzi4uLC277wYWLBl1zM+/OGiIXxb3C72fHa/OsAGFpKTEdqGC53Rieqi2R0GU+tXzPs7OzW3NdfjKiQxtyfm5xEGXeXaD4yMzHZuo4WACvL3t3y/ddReagVVn+j1Mnyh4fvVlfvSk3Cvo+t2fHKsYNeACdeAGf3+vimutWoxqg7bn29sKtuKnTXnWPE3NL1o6Lx8fLx8b32t7edRjeXsZPd3d0aRp2LQCrJjl6Rp7mOi2YJPEteWzORfFIaWF10dlYbdpXS5bJEQCOKss2EvblbiYScVmYZI4tORZTmzLFgpr6LNlqtYkMji7iOcaEARYcjACKt5uf3IdGQAABz+0lEQVR42uzdMY+iQBjG8Tdzn4ePQUlhRUWkIhaEGAtCQyKhMlMQmi2IpXYaEy2sLEwslWqLLTQmfpJjEB0UOO+aW7l7fsV5N77rbfUP4oAEAAAAAAAAAAAAAAAAAABNIqvK9elilQ1UIm5V9RQCAPg2fVaV9arHymZboi6rmiJgAPD7EDAAgHLA3Ad5wOauOwhYZtfNlrgvAhas3bIuAgYA30QGLKAyJQ9YRt2xjL2hqy4b+FTWQ8AA4M8gYAAADwHrsquZLQOWUfOA2Wy2vQXMYlfRCQEDgD+EgAEAvAiYMp+fFFJnM7s35wgYALylhoDNOd/O6RJFNuc7BAwA3lJ9wESd3Gn+fMAYAgYAb6k5YGu1l+EIGAC8q8aA3SFgAPCmEDAAaK1rwPq7q3UkAxZYxeKsFLBusaYiYADw7a4Bm/qFiwzYelusufeAycG5goABwHeSAZOePoUUxEZWXEoEAO9HBEx9UArYXFV7n/eAWbZaFiFgAPC9+qxCBqy4lAj3AwOAt4SAAUBrqd2q/lQEbLBUl92uespGuE+07FZdEDAA+EanedXmUwQsmM1ccUm3GDkRbeZVnwQA8HZ6TOgSAMD78m17SYXIjnwiW+CMWdxWi5FnJGxse463kAAtdT7vO3WOoafQH0v0zs3Yo7+hug/MKi4lEh72gT3DPjCAttP11KmTLlad/TH5wwqND85NZ0J/zzIIZpvNJ5Gy2ewCa0lKEASMsSBw/U3m8xawoGQjTBEwgPbqdIwf9TTDTBd6MlHo9w3NHzeLEf09X5zzKLrQ5yWK+tz2iTjvfrCA8/4yykw/i4CtuRQJfQQMoL1kwGppi2OovH/AxKEXYyqdvhjr0ZXfZRbRdscyy00RMJUk7AMDaDsZsCadsM0BK75WDQED+Be9DpipKy0J2MB1uary7A+i7K9LdZt/M/eguBZSGLiSqq4DNlPVEwIG0FKvA6al45YEjLFgTWQ939Cw2/gpJA+YRQDQWjJgjYzO2wds05v6jM2sndvrfeQBsyzXp4sl8J7AxfOZWZCX7sOyEDCAtvuNgGmLybsHTOXcZYxH0ZLzQASMoki90CYSllzYscCNMv0PlhnYUYSAAbTdQ8CMdHF1MMsBO8RUpnjeZFSYeMovAqZcB+t3Ysin7y8iKTe3Sfmf1bFZpjiJz0TAylR5T/yMP2AZfiFCwADa7iFgztHLTeK9qcnlNCHJG8XJeDzUr8bnJJw0BcyLz8ehPhyO45FCFV6YiKd1/Tg+xyOPSpRRfDO6To6H2Qsdz7FHtfoIGMD/6DFgY7o5mvUB85KV+AHJOBy9+oApiXGb6cRKpV9nRzbSXI2pJOzcX31FRMmimNScpDFgwXR6IuU0nX6Ik113/V4RMD71uVjoqtNMtLYy0XTaIwBoraaAeanMy/0tpBIfTK1my37HqwbscE5Lo2YnphIl7uT5kjRzOKkPWCyLmSfMe3ktpFTc0FCwac7F48CnzPaj+Fo1AGixpoApB02eAwspNzk6hlitKVhYCZjhGA8ji7jUr/Gi8kKauYqVmoAlh4dRzTl7VLFdr10q8PXdoBQw92u7Fo/W8uvrRH4+9YVbgQG0WlPARo72/CmkJ46p6pln7zlg2tOsIT/LVI6HctzkRKJUArZYPI1qi4QqLr7vU8GXtqWAfawHM/EY7NbrOc19ATfSAWi3+oAp4d6Uddp7JMQr7UcDbR/KgDUwhrdcnA9a/cQqUZ4CpjlGZUoP6Tf1ZMAe4MQXwL/hMWB6KMTJuWPKA7BVXBw3mTJYZpqm5bakycuAaU5Y5EkeVBmmkzqmod0LFsqANVoMETAAqNkHtsosUsP4ITlHyo1kVbRUjyfheGHIlaMiA9b0NlKf5CHsyH6l+/Mo0Q9G6WDvdcCcBT2zm76VSHAVpR8wW7kS2yh6OIkP8E+QAWtw/3QwWcjQxXmuJis5th/VBMx0jId/5j/m3dc0fUSCJ3+HNHkdMNP5/a9VE7pEImB0hYAB/DteBMxY3bctjM7Dm+OEXgfMWMWjSag7ckUbe0QTXavsLwtXtzVzXwmYc4zDMBETBU2rDVj/QR6web+/DkTAfLvv08lfCtmTmzxg/b69FDY4lQ/QUr8MmGbu5d53ZSIpitiQr5t1AZPns36yd7YxbVVhHD/etKUwBmUVGFBwMFC22cWgZi5LgKg4E1Og0Uk2161mTrupONEoOhdx9WWocVYnKqLzhTgzZ4hWgzLJdJnRDwXf5ssM2snUzLepMWrUL/6fc2/vuW0pFTahnef/Ae4997nn9Nze/u5znvvcc9vNyKk3Mq0DRv4hfTUY0KrrDHn0QFlvDMBcI51ONIekWKEx88B8UeIA2+/zvXQ5AWxw1DfIBjd0QasbfL79HGB4wHsflfgkwKSk0lTjA8wzFPLH59ADXSF3sG8IzlUigIk7moGhKCNnwCNM+oKaRH6Gp9sZDbBh9QO0e11JAMaMcnKAQQ37ADCuuERWepRITmgoJZXOShYD87i7e6PYFe7pcPeJG4cJARbJfu0MCaOgH8lkSebuaYkBWEhrvs+TFGAv+FSNDjKnwQPr9/kwShz0bbqnq+s2Zd+bZLK1rasLsf99XbskwKSk0lfJAAYShcWUEGH3sFezTwIwV1ADj5FYQwFk8yebu6c9GmAUN1OrTgqw1QpXzISG+pTSu94UJvQwt3ytmpRUuisaYC6R+CDU5497jjs5wDwhpsnvEgDrZr0dyeZ/jQFYtwSYlJTUv8vEd4Z7enpCQ94opgQZqTcEEv33AGs9hgD7HDPk79IBRhPi7zIA7AZSvwSYlFT6KgZgrB3q7Qy4XbEzsjpHWmPSsfrcwfGGkO3xQ8hgYGoBhtfcbr1cAIy/7FYADKvQtADszLJLqrTFnAplyQVMqPHsv7GaXKYaVBGnOeefV83G1hk3njuTSUkdX0rwLGTAEKV3DRGcAsHolP2WcCDQkjyIHxX0cvujQ2IdPfHqdh47gLG416oZAaYupRjAaFUCTErqqADGnH0ojpoPbGTYkF0R6gn0OlmvOyHAxBNI/mDCNIqgn42lYwqwTfeshho+J4Dto8U27oX1b21oQEE/lYxOF8CEJMCk/p1yfBY2ptK4pamZkTVkzFHlUHH6h8dLZFVj/709rQJ8Hb1RiazD4XZ9muqI/McWYEJXiSmluabzUaJ4D2zp/cVK1hMvY4MCORh75A5Fyd1zhW7DdcrJ3FBpulkF2CDLuX6RotT/XAujjQMZuVefDYCZK2GosQzUeuP0YqXpZQ1g196ZtQ7GVMviR9ezuSdmZTNmm48t5iIl0xLdrNibf+I/ViqzS3mtKS3bCitDx2aU4Bjlo0uGawMOi5C5EqvpoJwKHHJ9eckFZ1w4/oUI288sc6hLX35QxSYmnBp0BvCWjEdr1d7I0co7VBopSoEDmAhg4ViA9bmEWWR+ME9CgEFedyfCaT3DhrFomD9KJBinZck6A6GhiILO/yPAloFbUHl1BGAXFygQgBEDMM1wRgkAltXmMFdm0CooBDSpNrEAu4T3d5adA2zpacqV63Xbuh0sL2N2KdCFCugjxTRr2Bubs9oygLjUB5gp30F/6Jic8YGVpT/ASBMBGKQBbO7KLcsnCLBlZU1XsKW3LLyUWjLyq1g7Wrg2ZEeKUuEAJgKYv3UcgPk5drqHx0tkpXi82z3kiX2Y29kpijxuzeHyipKj8MAGR1W9MGiIgfV3qbpNA9j+F0Y1cYCN4lGi6Y+B2eYvJu8IgOCr5A6trQVdDLBoXKlknWqxzS+vZoUnYW9TjVL3nik/twpnFHbBHout7LpFcQCDFTAHF4vcqTJl4aVocjkK2dOLlEzUN8uO/VFB4UkzSmKaNe4NstavMX/OUh9gc08Ef217B/A378kFtII1O3r72POnvFhx+MGstodmcpgBYI0r63bkvFWg1J+1vwgkr0TfTAe3kFdbtyCn4rHvlNyra9nS2zPICZ1qzXmAf8yH+SVr3k+8H5ufx1XsyIUzzavgZj5yET7eetvmEoC6CqfMjzWfLFLqt0Q8sOteW3ffNUcO7GATEqriXuxZFWjp3Qe+xcWMn3yn6rg3/VrC1KKUuAIknBO/1ZUQYC4vIu2dLV7XuACDHWRcDasjz6ChJs+w2x0UFbmGA0cBMKeu2CA+SQOYsOIA86VEEN82P+uJTeujYmCNn24vIE9LEzACuNDC7w0XwWOCs6RWsbFhIANnGSiE4RLgEwswWIF4QBC5U1Qhrc6zMrKdZTflz7MWnkQ+Fvlisc2KvQlgmWkRE0G/dtrNRZn37bXnVMwuzXuyhGHti7vWbVtVDIDlVi199oEIwB4Gv8yVD1nBg6q886rnnjjPCti9u4KuBLNfqQC9mldYl5XV7WhcWT612BYAwzcJFp26Su3HvaoHBn7hBFhXe92itV/cVYVvMtNiOvhLTf2WnOWz7CrAwK/aSfmvYHXE15tz/tptJ/CTcVOtTitcDbSi9ALYSKvRt6JXR8IgMcCEBJx6ndrTkR4D47zGx5K87vbJA8yoxAATSiGAwb2C6r9ZrwEMTrxCEt5Oc7HqPD1bTOWXVGGwhyrIjYKwC9XIkRMXA5upxsuwCEM4bBTzIkLxbfC5HLb5P95y7pYiQDG2WbE3AczB0kOFu/EDzjYdtJrys+HWZlqwWDnLDtcMAMP/CBkqy7+r28HjZOS1HXkV9ooDkLDwb4Xbou8OHlMrvCubTbEEwJaVLbxU64c6hNywwgqa4eNxHzNzfxG+cNvex2syLbQXB9iPX02KX/yMooupCrDLrHEDbjpAqTQGT/JWIgEwfzA+ZT8xwMCluJktWpwaXMIg2JjyIlFssgBTXjLqKg6wQSxsIOHWYxvNnqNpUANY/1Uv3ZMSAGM5z92hQGtr+Sp+UKDZR81iuIaxYf0a7jXl7tn0FIhie4bGkXCemhpuquAAA4CSAWzxD3C6ogCG5YUDWdmVWXvysSKajQcY1Z8eArcKd5eQ2/Wq+juHzwBfjH5wLwIIAmAKrgnoXTbDJgzVHLa92w89ddDKln7acIfCbQlgc+D78KDS1EoALOezndWRfhBW6IukTeQxA1aVpzx+8Jnda4oy36lxMB1gFG9gk9NG9L5uB7WEyuIABoqmVBAx+VuJUBwWiaxGuQw2bn8UZTx94F2UvLDQ1N7T5x2bX90TTKNIPqEhppSm7bdpr1UTU0qnXh7YxgGM//iqygwRjMI1GL6TbkwxMG7CIaXGwAxDSJQCU8BYLMBy734Hse2oISQt/zSjxDb/cDFWRLNjAAx/0kPmoiXP4kbZnJ3bd9r5TUmMvMYEWP2Xr1bpAHuxaPb7h458+PEB+8UFTQ0fnTMNAMMXpyiOOIBhGMu0flg4wD7Yc9raWh1gts0f717w+h9/Wk1GgDXdD+pNVhieOhIBrPCAPT0A1ueJeauHP/K2bjEvfhgl4pHvKICF4GZFGQc7mVB3MP6ZcE+rOzDZPLCtXfFavYn5uro2+Pj2fV1baT4wTaOMpC5PUxBfUaURKy+jfg2utCAKuIMT01QDZ2zjRdpYDlF37YIK4/Lqxq8zBMByq8y3FqAGxMgiQXxeisU4gGGxuZiPpfQgPpYpAIb2sCKaTQiwNAjicw44aNTXBtiD/Yf7rWZ1CCkAhuWTCVrl1ZEhZKltxU8Yd96AARlsNNvpHkLSx0QAzMK0fkTuQtpWVEWGkCWm/LZDpUXfH7BHAcxhykfIdOJChfz8TAgwJFGkGsBcuowAC4pyD1wwqLPDQDAYIwliSDfyhno5ZSK7tPQGDP4ZjDuZQU7/SPTrHrGHG6jSAebSJQAmylisnGNJLde3G6yid2JTqniAiTSK3Lst8H9wFQaxoMPF4AdEYOE6d2ZzMb8lpTg4VtR8COQ3UILYaZEa9dIxALaMRoIEN4gH1dAe0AWMkVMmmk1rgHEPU81x0x1NBPFvLeBQ0pb5Xchr71y7TQvio5vzrBTGNxfVr0GoUQfYNAXxtY+JoS0SX/R+kN8FQOUsL6/WgvhwxfAFUhg/GmCsGX2asHDuNL2MSOvmmdRSPMAoiSK18lC6uztadPX4DRtGRHlHQBv7jdA7hHDv0Dsc7CEihTvEvmQTGNH3cNLra7m1t7VvpDOeOQE3NgOJ/NVEwRHRNNrpFm13EmBE1VzHHcBY4/vIwKr7yMLopn1WNv97zdugTQzA6J5/3QLy0jhWcCsdCalP3zijhHbUElm10luxGAcwuGDl1bBFc0hkVX/taEQdbzLRbFoDbO6Js+y6M4EOZlrokCCNQoUSuaxNAwCYOjxT0ygsfC+6g4k7GQhjDzy5RgPY1KdR4GD/VbZkm/ox3z1f/ea1ftxXcN7HyAOjmL6aRoF+ItReiHBfDMDohiSbuBDIoExp3tJ2AIwnVghaIbooAXb0AJOS+reiqHO6Kc/RfM0Fx0E/pKSkjlLNv5WyNJPp1wUrrcdBP6SkpI42p2LxceG4HC/9kJKSkpKSkpKSkpKSkpKSkvqHHbtnaRiKwjh+uNgapIY26UuKpPjSN7BIB0Wkgw7WDBbtEAXrJB0EHRwcHLoqBeeKa/0O+j109YO4CTY3ZuycFv6/6XCfOz9wDgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwDTbD0prfZRaxzLFTpABwCwyv18MSgrAXOp2GkkKDMA8Os/f3ug1MfHjq6W7axHvcVGdvsvJV16p/U2dhfNnlBWGjaRIZtwvHy4LAMTkrLRWD+9c3d5z0mtuXQUvXtOyiwd9GfiWPclG5cl8v+u4/9lr1rJlI+u4AgCxMS/2jPBQP9J9lO4Z6U5KIoniUT1aL831WjXM9DcpDFMCALHRB7CwwN5yKnBZWf2tiDZot59UWGDB7KtaNcoWco6bGa8I/tg789CZoiiOv27znnmYGdtgFvvYl6yNSY3JXjRkyxaJX0KyhbKkyJJsiZDsZUmJQigRkeUPS7aI/GPf/5HkD+fcd9+7c2csM7bZzrcwv3fvDO/r3M/ce99555FIpLxpdhQ2wGyAmYaGEpDSxw7Y0fuOmIENwtc9JcBg/6s1rSBJJFK+JDfAxBIS97VQYpnoQqA1qYUA42li8FouIbU6DV7PoRUkiUTKn/TIJrfGZc2yJseTT3eFkGrJMVVa1+q+37svyAG2EV4nn1Zq3ky0hWDvjFWhFSSJRMqXEFvMUvPNmEaxOMrYwPk8VQLTJxZAVsWO0xN5kiu+nr5+V0i04TytFawgdUqjIJFIBS0nyXX4lbhzkK5BkkikYpDLHxYZFxJZ8APsmJFIJFJhq3olzMDnlyWD3fo7d4EnVmokEomURzESiUQqHqUBTCNlijwiu8iOwhQBjEJQEdmliOwocBHAKAQVkV2KyI4C148AVq0VpuQ3qdXSnX4FsuzTVdEjaZO0Qw9UzimRBPtnZW6dBsV8gZfJs2Og2NYpZW0Sk6f2aRljvrOTZJsokJD6EwNhSasM6RHRMfPtZaUfA6xyG4MA9nPLwB9WuSoBLHuAoSrisjF5c02ZmcRS7nrZGofk8HbjfwYwvMzft720TBEB7GcAYzEPAeynluGoebscAvC3AXbwTCmNze+LqWe3ZSgPKWlZmZnEnH/wyaYi/9suGKrtibJth+AmGAbq5XEApkf6XOs4Lhh7w7jw9F1P3DAD67s+iNMz60EWzZtZbz+KDHN1DLeN9Gmhlb5+DLAXwYo4B5j3Ym2G/qLPiY/LwT803DdtSp0GdevpAWYaLj8ET7JTEO8pshshMyyslaRSLKs+0R2ZCOMRPfKd7Vm5KoyjxPNKpgEm8In/AlglJD7EraHIUDVqCj9xbM6qKtyS5qLl1sCuXqnD9dqVp0/hY7Nve9+q1XZTUX2NqADDyDINwBj40vV2QDFEfHGCWrpFD4ObJDqUhEnMmYA5wB1WH24sHtOoRZ0VkzeODfIp1MzlFfEUgNXY3rFRi6VxTBdvdBeIp1U7XzPS/Gig+359rFVNYdCopuLtAmAlOvhyANjXVy3dPFAGBRkIi6+KLwDvSL4yr9oEylW4/NBSp4FpWAdjHtFYwrcVScvQADx3CLFKDMQBBn+al7lV7cZbqyZfUwVgtp9wYOpx2y1hrjI25WfW2DzGt8poUuBjMzuAIWlmLucnOyGgGJIKMNEjhCZNsTuUgknMWezhCTvFQHEmFuFVjRFgyTEwr3cANjtasbujafDU8SUevOVFD5iGtQcmUDX7VBvDejsBzAZY3d3L222GaBB18mvUrNYKykivrc2jAoeseTvQ0g0Dtkqo+mAMoEYt8CD6bP1ZqmKaNMmEAQOGuPw+rL7Nx5Gvqf6oWquKuPfBRHfaugbci0k/vSOhv+2WMFcdm4kJ0IbfDyfeVJ4c10RTkX05fA9g1ryjpVueDBoi+oGPcLKiB5rkdCgFkxSA8U36bot3hfAG4y94hO/N61iwHcXbcYYugDT85CoDeQcrSN5xS+/eURZGp6ZO8VpvJ4A5AKsXGXyQA6zvfbCpRs3qSCWxkdF7Z5DhqDPHXpo7sSuOYd4KEo3lADA9wOTXf034Ub5iXOZqmGDF9t4xVH4JPyOVoL/tljBXHZsQkfgbzurgkkrBj80cAOY9ALV9LYBJQxR+iR5oktOhFExiTvDgCSO4MgCG68GUTXw5z8KFJfporj1fE1EVuXS292FogYVlC40AlgGwYfVPvIRogOKrdx43SAkfKJu/9xj+5PJ3v1DjMGxkg3U2wKzGspiB4QLRWtv8CGAG8GkZH1eSX7afm+/DnplwSwEYfAr/DQ/ZY/NqFEJUNBXs2Mx+D2zm8u7PjtcSAHMCLJVfokdXNMnpUAomCTtwNtVUAGxt2hJy9il0IBNgyTHd91uXL1+aBqBquz+M3wdhfSx+gJ6yhISjBDCAEuw94D4YLIrGBq0JvL4YJvC4WkyOYWBiAGcZPJUAd68nwBvqvuONZTEDw6EIgQMLab6EXDDUBhiMKCjMLbvBIckvx08cvY6Vjrl6AHg3O6qOTdwnMg3RVLBjMyuALYCrkHBWFfHZUQtg0pAUfik9nA6lYJJjB2AHUuIW7BzQ5nLKJv7i2nAVEjbAMgGG4PKIC65gIQfY1Cl9F7Lw8Nr8G1K8vVn1TW6oNUoAA4A1qcUBhruq72vUhBcoBBj88QKCBbPFqgLFMDDtTXzRWAYAw90vm2Ope8k1cU8ixQwxA7Ptay38FAATbjnmym1pZWwOqx/ziKaC3Z/OPg8MT6TyrIlufFFjs20I9HFOUfQQABMdSsEkJl9uOV0JKoTOt5+tiikmmAdxvR8DjW4KaRCpAINfDNWtP147QoBh3kRs79CvyxiqTwvr7c2AXr6zQymNAgAGO6fAeoB84lwg5kGfY3sxjQKMG/jQ3248jxHR1UqjuGM4jSWfRoFZJNbAbNTCexGu5fd0AObUqd2yDHPP41oKwEK2nzg2HbekuRh/046kj034r6iIW00FOzazAljiGcxNoTK5b9raAW3gwgdcq7UNSQWY3eM2mmR3KAWTmPbHwmuQ8Fu5pazSvZCFE4LlJLLrb9sx83MIvw9hckYigNGIVEV2KSo8O/QA7hLWaQAbqiQCGI3INJFdisiOAhcBjEJQEdmliOwocBHAKAQVkV2KyI4CFwGMQlAR2aWI7ChwEcAoBBWRXYrIjgIXAYxC8B9KzQPjZTl+eptZSd+DRtHDRQDLVRSCeZMKMEhaJYBpJAJYbqIQzJtUgOFNVWXAKIqen4oAlqsoBPMmFWAt3zdqwQEm6tbCQbWCrUo3Uc7WLgmMH7B3KBs4aU8Ua9h2qV+U99JQ9BDAchWFYP6kVqNYFzQNC2BWsSEtrYKtAjC7nK1TEhgOoF7PKeaKvxQ9BLBcRSGYP6kAaz0y5kFGWXVr8aBawVYCTJZulRVbXX64iWZQEN4QKN5yTRQ9BLBcRSGYR6n1wIbXrnsTGCXZlFYAMhVgXlHO1inICktIN6+uVsz7aBQ9GgEsR1EI5lEqwPTIpbnZAkyzy9laBVkJYCQCGIXgf5dakRWfNqQATKlgm0Em5NVmqyArAYxEAKMQ/N9SAYaIEgBDeFUJqRVsOZn4czpAdjnb1VZBVgIYiQBGIfi/pQIMWaUATK1gqwDMLmfrFGRVAUZpFCQCGIVguv6LXZSJTyKAZS0KwRyUT7tc/iJO8KLoyU4EsJxFIZiT8gqwgZO00pa0Q/8ES2TfWfWEvY1/e2HcpX74F5/UeYhHKzURwAhgisiufypphx7ZBPt7C4a2G08AI4D9XDQisxfZ9U8l7aiDT+bmT+R2ngu5J4oPdtxcn4F6efjjIMcFq0yAW0ETXY3O8xio+faO96Ks+w1OLNGx7/oggztJEWDiuof4JHxINz5S0oXvSaxEgOFTvBtu8HC8db7VVCt6EcBoRCoiu/6tmDMBc26UGpbyZO6xQT5vmrm8Is4B1qjF0k6b3IAdzppBo5q6Osbc2FlD8Y4bA93362MHVO1S/+1h/+R4ckyVkPgkCbDESu/IGjUBYMAvzQJYUV7DJYDRiPyFyK5/K+as8DBZhKsaQAVnYoi0JrUQLMkxuLBEgJmGy28acBh7zz7VxsBD2BmbZUfEVBd8LjkI+CQ+SQLMNPBw5yEz4K8igJW5yCOy6+8ADH5jjHVbvCuE68EveESPAFj0SGKCjSX4BbDCwzBTg/sVXCOawnF/GJvtjlt6946yLyNZWPMe6L0z2Gul+CQJsDDHFqxCYx4CWLmLPCK7/sISMoBLSARXBsBwpahlAsw7EhaOCsCwI7ZdOtv7cMcvB9qHh9VPPFvUY8MPAXbrbPuKOAGszEUefWPvXEKbCMI4Pg7NNlGTNj6i1hpfiQpWhWKNRWiC1gcotWAM+ECQKmJFVCoIRikIShH1IBbrwTeoVwWx9qJ4KnoQBFE86UFRT3rQg3jw+3ZndzPRFoORbLf/H1KWzD7sv5Mf+81OMoirDHHk+pNKYOeKSsiuh3tbXIE5JeQO3k2oEtI8lndUNpsyk0rI+tqtYSGivVoJSS2OwDaFa0l5tEWtENhoBRkhrjLEQQPzlzpF/uqGhsGCQfzuGD2FpHEtV2CBhDWIn4s1GOZLmZ627ebd22bekQW2r5M+bWUJ7LRxI9UaVmeaF+0Ntr2vLhRYZIt9HQhstIKMEFdZ4ugbqJZjrxwVzjSKvixPfni+ThLbkok1C0zzRMxpFGfmSGbNs8anKd6ZS0+1I82ZaL6ZnXo/Xh+5VSfT72Lj1Znmkb1qHmULBUaPLUNGd0xuHMA0itEKMkJclYuDZQQgMLwjhwBxaXguDggMAsM7chgQl4bn4oDAIDC8I4cBcWkgDo8DgaELaiAuDcThcSAwdEENxKWBODwOBIYuqIG4NBCHx4HA0AU1EJcG4vA4EBi6oAbi0kAcHgcCQxfUQFwaiMPjQGDoghqISwNxeBwIDF1QA3FpIA6PA4GhC2ogLg3E4XEgMHRBDcSlgTg8DgSGLqiBuDQQh8eBwNAFNf5fXEuP8Eo7AfqaK/3DyZKgpcDoU8qj4bPL6D0QWKmgC1YMXWA1p40iganveBfn+pMQGIDA/gi6YMXQBSabw6bAZp3/JKfOcARmfTP8dVKV+mrSWefvxeS+JbyAa1Xjj5Wx9EJrWbHaJxfjK9aLEQ16j4DASgRdsFLoAnvx6NTinabA1u2+MEboAgsNNtarFV6pfV9ne12mg77+eFCt4xqllXsC00KGGOmg9wgIrETQBSuGLrDkuViDwQLbFdRKSF7ggktIe4VXbufVd2jtr8tqHVdymVH1NihGPOg9EFipoAtWDl1gtMJOj1lChosG8TPLDRLYLGuBxHpu58EvFphaBpGXUax9MlGMeNB7ILBSQResHLrAxOb4z4EigbGhmOEFxuvv+6GCLGfv4WDowe7oBgKDwP4rusBELiaHFJhbQiqBuSUkq+/jAx9UkAW9J/D9pJQ1j/ZozcOtmB1IFLdF07fF8NBKan/dyJEXX9Hzq0dCYBDY/0UXWCBRPZTA3EF8TWCZDmsd12j1BB9UkG4cVE5fahH57OKdJQmsdCAwgIzKJTC6k2KBWdMpdIG50yg0gb1K8Uu8eH7IEE3+mUYxpZ9/cb7hdBa2vZHi5WgvxiXRGrZmnhDz7x5OSZleyDpp256+ba10azTFn2Zl81lD5E/Kmv0tCcnUB7rNxirOLdPDjopYRwuiSZ2676Q5d5gFpjZpSdyxr2LOErizzh+skxv32Fdkq1LTIv6beQwIDALT8GxcvngG6cYRSDg3lPaNJy/5v6POvOc5dMoZ3GrfmuTSuis1dUZi/kWyCd26BfmxbVO8ORh5c2365vjine0bxvPt25bZC9ppX3NacKYnsmXCRHKUfbQgrFNfn7y3pW37uOlLN12yN6O9NFtFugJbt3EPP/i1rsi3hV6dUgyBQWAano2r/dsM4QOkUys6SrGH/lhpcyexwNq2O4Vl18MGw64tE7M/p2/TyH3IoP2m3o9PnUHHhms3JJUTd5wIz51EjXTu6zR2yCYigWmVqXtqbjzC27z5OM5DjTNdge0KFlwRAvMVyKgCcVVNbvbFDZgmMPohpVzRbT18/cGvcNFGPzIdzt3Zvk4h1r5efVKSTqQk41Q1jhe8n/mpBBKUM6KY6z9NpaNk5l9WJqJ2+2hBqFNH7qy+Wtf69VRr2N7kczbFHYHRP/rPqStCYP4CGSGuf4+DJ7ZZw1K/CYyrRkGoopAf3W5c/XIV6STzrj85pMC47HQ0U+UKzD7aLkjZipkPx1ee/3J4U1htDikwviIE5i+QEeIqQxy5/qQS2LmiErLr4d4WuyikJnId+YMbEvPvTpu9wCkhlcBUCcmD7YJfn6ELrMc+WhDmqbnuFCLay3ZTm1oJSS1KYOqKvEWtEJg/QEaIqwxxkJwudYr81Q0NgwWD+N0xurNyBsBysQaDBZbpoKeErJN5dJt1wR7EVwLjQfyuVOhWb9i6xTpt5LOhQVdg9tFckFqnrqV9bqRaWWD2Jg/iv6+unzspfTufbVUCs6/YYrVCYP4AGSGussTRN1Atx145KpxpFH1ZnkbxfJ0ktiUFD5Exa54do/0GrnVY5WVQTaOwBcbTKMYeuN4omRUXqLFmf2dBCbnWOnq6oBesUy+7VSfT72LjczTHQm3SOXkahchnZebdJkdg6opkr5pHWUyj8AfICHEhDq8AgaELaiAuDcThcSAwdEENxKWBODxOscAAAGDkIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwi73zB20qiOM4eUshDtk6uHWUhNogxLQkqG2XFEEomXQXpIpLM1RKBnGTYiE4iKCDCIogxaFahapUWmwH6aAVOrmoqOCfxdXv997vri8vvut7jdZXvB9ik5f73f3+3efdXZ7ViRMnTpw4ceLEiRMnTpw4ceLEiRMnTpw4ceLESVJxv5HViRMne1ZCAMvEEbRKLvYO97yk3wVvZ1p7aVgvs5d8DOjvqfJTY6WlVwew/8YFB7CU+egA5gCWHkm/C3YLx+aulvhzcXq+nILJ7QCW3vLbEwCrHOvL72CsQz2ex/9teOtK5fHMnzR+aMAzhh3sxcskYhRMZ7lSpwvjha7izyAcQQCG9xer4dhAVl9buzFti1UxxBg59qrlzV+rauC8XejozB4UL9SKYwSuPCnJz3KEFotiX5kZ6K9Z6+Pgo5Ilt+wAHsEEZBJ/QYxXQS2b2th6w7v+zO4iW5pivPOpxxt9WotIgtepYw8qrUEoOjqgkZDcy1p0a0mrmRhG35pYe9Gyh7DFgzMyi2MLa7a/BtVsQZXviEdBti1jhUzLnqlmxkbEwE65s8Ac2stTsoWaYLIo+8rGm7UBj4Je/h7AKJjAIvTmTwJssgHjUw8wqYCqJTaxABayfmLq0NWyxHW2yc6Q164AlsAvSSYqAxWlSj1yGlwsbwMwr8jqFICF3RAtm9o6nT/6LAbAIBNTrBrK9fzvk+D9RidGUCMB5nEeRwnTGp4YXnuFG+fsYgeYuc0kELZHaqE6NpIt+ADL3c3EBxhKwzLl2WkMDyp1GdcATLxJDjD+HLzVyt6oxQYYaqyyBDsXMcNyL6cYhOKK/7p7gDE4PxqcOkOPm/NnG335kexGqy9/5543eg4DC7mV3aOXm8UqPsi9RDZUVRVQh9+qky3c4C5wPZO7sZArsTZp9n04uoAPWGCLl8bPbBJu/HAnAEP9EWArvnVtsanvK/Pq0S++SZl20w/2+on60DNxIWCI3B6PXJhbLukCnZ9BoDGl4YhajSze5KqEJr9rIdi6p7wdYH6AqMyfmD4LWKlMRaWGKsPHX8AITkMdyfXG6NM6jNTvRzCyFWC5F335oYEDpwkwGAgvCzEAZtRQxrm76uZj05LkHTy8r1yp0zcVLZOEsFpYRyJjKm3tmD+XpK4jV2C0arjRfz9Yfxm2lkxLfZ1XE6Ma1meFg56VerbAgTHxkPbr9049RMh5zxgaoPMlSZcVYCQA80+PaRZmxI/W6DlEATmSvtUs+dwwlcJ2mGKMMy1mJMDiJACD8QQYjWYkZB6uqBpV05DD2CcQHD36bGyup79G00PemFVHfIDJHSk+wDgeB1egVHma9V93DzAFYYYW/nkqGuy//35dEZsB4t1c7CaxDvu3WwOw7BnEQpWOf6MLAsyjqIXPbHMCFdoFwD6iAwUwWYGHAEabPvkmVdpNZ90ggYjY4EzQEP90Kvui50QteB8/ePa1f8dmxhkRIsH3S/dUtQJMB2i9aaJCcyKZwMQu/+iFZ4zclk7uhX7P2YJBrQAb/HK1XDm2etgKMJuauk0fuRALYGjOqS81HQtg1NGRkUozAJO63h5gwfpja8l0RtJqAdj1W7BWSpx1hGEfHC/gA8aryBsar3GZZwPYgdNHn7UDjLYceKhWFNI3rqGbrUqhcWDkh+USNZUxKLgEADv1grVMAxkrMw+lRmMDbPTp6y2aGG92CLDcXSxaYExsgIkyUzBe0OtJvu4eYIwzCUOwl7CLAsCQBoT9TLVS79+UotJ2c8l6fqqyBAswSXqu5zWPJnHP4UeTrSDAcGG9iQk6/rmBudEVwE5gQUKA0brwFhJD86qYFDb9kB8yxjFoCIR3TbP1YRszr2cwYwqVY/NlBBmQHi0BdQXdk3ULuaIDJGt7KCDbiE5QK3z/wFzGn85I6vc8ArMDjPNx+PiNY4Jb1npnOG1qdEAwawOY3s7JRCBGTBJCamEdExlTaWQmrkkt287AOGXb64+tJdNSX9FbSAW91fdYjB+5sHjpapnrEQQeJeKhLpZLrAfk1uK5JPgDijoAMFq0OA0gAedvpG9eMzUn8wuMfLWff6pMJ8ZOArDcDxSWApgocgRTozG3kLAS56KguCSrWBVvdgYwJIo6iQF258qHJljPyMjratcAY5zVMsBskBASXJVZsN7E/udb8LwZfynojc0BFpU6+T/0/ErLk3XcSBBgAixEDYDpDmDFycaRmwAYrOv8gkPSqEwKm07nkKd3DUQraAirY8n73Bicefu8Zu70gSUyatKflNqfqt9T3gqwTR0g7P6yG89rDJglNQyYWn7tu4XRwpGU93k5ArMB7Pux4uzV7wZg179FhDNKTYVv+W4MgAEFU5kQwJiECDWjY0rHVJrwS2p50wIwbsLb64+tdaYlrVEAwwDoHwh9td9HIZePaN33pnejtzCyr8zUA3LZDThmBdhaffBWAGC0iKXHEtN985rUnJ7u/vKr/yYQCY9HYWMigJXmxs/DMxpt5qGp0VocgLGfmy21cDUAE292DWBM+uzJp68F7WPyuvsVmDnz/T3AUFyfekD8MMB4CIc7wtBbTNvJ1sZ7aG92AAwKMq8PPIS73QFsZW78YTMMMMbGHGWO0aQO0zEsKrV4exZtjSH65jgxNdv8+BWOb20hL7+OBJjfUzUWwND6+YOGd2TNDjAO+/B4ITN8nH+HIynvayOMmR1g3wYGv6pJExFgalnU1qbla4Rtt5B3FrCk1E25M2RkLIMZnU6ADS2RO7qWbVtIig1gTKsFYPwi8lNP37sAwBD45csnfw6sHu6vqSGYLphjARhLvfHxaxyABSoFR4wP2Yx/c6V5ppoUYJU63OsSYKT4g4a/Dwl5kxxg3Fkl3EIOTY+r0AwtZRXA9OvuAUYkU4AbvYVkt1ydBzeZOnBmCb/enC9LHKAAxeAW8ugz1GYAYNyGqTq5i4Y7BFiVObMCTEwKm45K5YEt224ZUtLqWFxD1xziw27wQx/6Dc7A3qIAzPQUbwupa20EGy7O8w4tY6KnwuThthSOpLzfxBGYNbeyEComBphR4yH+ypyE034GNolFK4v4hj7EtwPM6IyFtpCApjoOklqOBbDOLSQ/kbRGAUyd2XBAbvPMow0MObLD7HMIPUPtAOPdkWmeQHraAab79gEWqBSqYDjEWcUBiEwKMJZIGGDcQsb+FlIO3mCyAZjxJhHAPApW7VuruHyCxyi4libjOeyJOl93DTBjO00xh/js1j8rnbivxgmswOSDE/qAXu8QmSR9iC+OtgEM25OSrI12CjAASgOMnXYATA8bNp2VKgQSQ8Rp+IKoPm7yOyR9xMu1KAeiprqAIjcAk55EbIf4qBcqM22Iiu10GGajT/m6PBjJwa8sN1kKSqJtABvej8jbAWZT45D0fVuAMU6Yq8PmMQorwII6OjISaFkQ5X5KXccCWEZ3wtaS6SlTXxGH+BxPncJN+49TKBYw5PxiTLZDqs22KzAMzx29MrwdYDXpWwAWqBS0VqWD0XDiIlMgEcAWpzXA+FaNYGo03gqM2TVbSGW7eLMzgMljFAkAlnt5QT2ztvqml/D3ivp1twATG+gCpvLoj4YAjN9Q81yjoh5XnNoqIPkuvBIA2NB0z/y1AXxr96qV3Vjwv/xf/dDTBjAFi8oSDhJ7dwow5DEWwKoh0+EcLqq2YsiaJIy+oMmDizPtzzvCEWr6j1F829pCSk+Z2I9R0Az7YxRiHTzgbdpEcr2Bn7mSvH93sbw9wJie5AAzavCZvsYAGM5xwGN5kBX+xQCY6EhkMlJpArCS1PJmLIDp8NJ2XaS6viZbvwUYUtHio71THJhOkgVSPbRODaHTZQcYwdGXx3MfuZf3QgCTvnnNZFX04ACuox3JlxxgpHYIYKZGYwGMBLvnQ8QATHsTH2BGaID7p0S76AK3Fn+nJ+/v+KVoxgcTUvCPgrxdy13ECszSOsX/lIiV8s9na5JeHcAyaYl/h/AE6s/1tBtM4J2aC/4UoGi3ABbennsxWqcYYKyUfz5bHcASSVri3/ks8/zMn+tpN5jA/QE3s2lA0S4BjNDGQ/UxO5DWqQWYqpR/P1vdb6NIJGmJf7SkxEL32yjcb6NwAEtT+H1JS/yjJSUWOoA5gDmApSn8vqQl/tGSEgsdwBzAfrF3/i5OBFEc57YRbOzsU0fUICiEBPS0UQQb/wmxsUgKmxRiZ6GQMqCFlQiWKhYqCIo2h81ZWFmdPw4UG1vfd/b73iR72WNX193ZdRYkR7wk92befnZn971P/gOAZe5O837w23GAw88tkPFfa2T6N1uyjzMoX62T7COMGoeGor8H2ORavzMAe39DCudY/5/xAlX9WUwHqaxeuA6PrCqEArR2AUxCigALFGDYygMsK4wKDkV/BTDG1xmADY8flcZtymRqAZjTE+TlEmo1wgSYFOi9+7rXLYWx6/WlHDR5LK2LaK5B9fbrX9o6FwHmixAECOlAQfq1M5N7VjQ2qclMn67wDIyfYPNnQrT8V2WFUXRjoaR18Ul2DRrB2gswlBdLZrpEhnurz8rPAS1yF5zgbVtrhesBmHVjlnyHzYtH6HaQEhemlxrxMGsoWpaU0gSk8FdCvPv1gPwq/WUlAPbmprM98QyM+YWMvj2lGVDHMSiAsePAu6VkRNhND1UBZUo4GKAnLK10x3REgC0D7CMGyjd+PFAbFAGmT1cIMP0Ezp8XouW/KiuMohvLvdNnKZNWI1jLAWYWOZT9o3/yNV1nFLzVDDAzu5V7B2/IkabbIdPLjHhoQZm6RwXY+xk9Qiy/p7+sOMAuPUPXlgFs2WdmZkDNkJAAJvJFdOpbY7DJijjmqcMvbcTFsQwHFPxXBJiN4PTcSR0opJtMvRmbqA3D065zujqAmW+K82dCtPxXZYVRdGOZMkuNYK0FmDuyWgv25kUZJByENTKq3I7VC7Cs2a1E65VLKkJIfmKnLN/Su9+QgDKJaKuDYW8hNkfvLysMMLanKsB8fjlBHUPgOIYEMPAKeW1qFpUVgVLSsnf7ITAvLpDFw6NbF4+4gZVY4jUw3Sbglw4Ur5KbTCXVhm3o0/3qALbJT+D8eSFa/quywii6sWyNo0awdgPMJDiPoB/bFQEZ3Wd0B9QOMPq6yrwDQ3EHGEjMmF6/aMTbmbEd3Segm0RA5myS6ndU/1McYJfuifTcALZpOiA3WjQDchyDBtiYsiL346vFffkPIf+7U4M7p3fxK7gO1utHgCm/5jhE6kBlAZZqw+oBmEzOvgDLCqPUjUWAqWut7WdgSxav07u97ae3vPvsfCMA876u8gCbwP5q6fUDRrxjpz78MKOZJaABDCH+EcAg3cDwZAGG1CXAOI5hnYHtWULq6AFg+Osnc2gkXShJqjb6Nh2MI8C0Dw3KvQ0OlADMab1obDJtGM7D/8EScmn+CgAsK4yiG8uWkHSttR1gtoRElJDbC6oZGQFGhVxdADNfV3mAjS7j8OfTy1lIXota2vncroxlzbidJqAtIRVgWEKWLaOAgloB5vPLA4zjGBbA/EV875bSq3dy5CDGoTJC/ZB+wV8sozAgYLOB0kuez1Njk+rYtvg0MqsigPETXnL+PMDwLyeuVWGUOd8w49+m6lprN8BkkDWRJUqstAhsU4pShVQjwNgMXv4aGGbGJc5PppcZ8SSOydQ96kV7OrveKMDUX1YcYHBIyQsIMM0vDzD9ep8kLIDhdutid41bavgKwyJ3UO+IvRwj592ivX4E2ArAbKC0XoI2qJECjE9XCDB+gs5fUYB5YZR3vkkZhTx611p7ASanB1ZGQfWUWytqZA5gVMjVBzD6usrfhWT6CJSZXjTivcOSlO43TUAroyDA1F9WBmDu+3QJsBWfGQYRArSPHMegAFZuY6wRYHXXt2YBVlVcpBnuLTWOoo62EvEGXvk6sPwpu3wytFRvD8CwAI+V+M0BjIv46uJite2VceMo6ijA6OsqXYm/75enh5bqrQHYiQNSlhsBVh5gbeqqjgCr6vX0dYXUjPefA2wj9kIW2gL5CyPANho/Awss/SLAWrj3dyCECLDAYowAiwALZws/hAiwwGKMAIsAC2cLP4SkUlWb3UrmV5adDwFFEWB5jiSWvxfaaO7i/YMjEWBd2Ps7EEKSoU9FALMvjQ0ARRFgVQCMVRv8udePAOvA3t+BEJIl+DjF0blbs8GbVOk1ogxMZW6FAQbbwVt+bX/zKIoAy5P0OQeM9EUCakPXqbjYmS1eTuYy+evMXZcuU9sn1WQRYB3Y+zsQwh6AoZGESi/rjqHMbVwMYCzwYOdd8yiKAMuT9AnAtudoVDSA4fnP35Mkx9x1h9o+kC8CrP17fwdCyCwhR2cgITClF2VgWqJfGGDYI9h51zyKIsDyDAvHDi++wNblAXb6JTwCaH1ca+5SbR8gFgHW/r2/AyHsARiQQ6WXycAocysNMOwUzaMoAixX0ncYjdUGMOedQwbgN9eau3BE0scIsPbv/R0IYR3AVOk1MpcOZW6llpDDVy+2pvEMLGyAHXwyG4zzAZY1d1HbFwHWkb2/AyGsAxiVXh5gapIqDDBZib4V1RQcKc2jKAIsfwl5FDATPl29PpmvAmyduUu1fRFgHdn7OxDCCsDkIj7Sl0ovDzCVuRUAWJKqnVhGcehBACiKAPvN3t3jtBEFYBS13Gc3NIlEE0XKApyKJaRKJDrYAvsAsQcoqNgOFSUYDOJXQgibO4/z9cZcwRyh0fjx2iF9y1tau9cg3f7YHgP24sld26tP/LsHNsbVP0DCg+/wx9kKsLsjve4PA7s7zO3tgN0+yHqxOPUXWAewZ4f0LQE72f9+cHJ94Nn58WPAXjy5a3t1bN/y//AAbPpX/wAJ6zXh8Pjg8ykC2MdtdZPg586R58AGuPoHSPBZyFhjG7DlsX2exB/m6h8gAWCxxjRgN8f2+SzkMFf/AAkAizWmAZvqaRRmZtPZE8Bmb9l89sGbzya/fsL8fa+a0tvOZ1NqfPD6Sf363bxX5asC7MskACzWCDCAddZPAFisEWCbAOz/5TeADZEAsFgjwDYA2N6fXwAbIwFgsUaArR+wrcW/HYCNkQCwWCPA1g/Y779bC4CNkQCwWCPANnEPDGCjJAAs1ggwgHXWTwBYrBFgAOusnwCwWCPAANZZPwFgsUaAAayzfgLAYo0A8yR+Z/0EgMUaAQawzvoJAIs1AgxgnfUTABZrBBjAOusnACzWCDCAddZPAFisEWAA66yfALBYI8AA1lk/AWCxRoABrLN+AsBijQADWGf9BIDFGgEGsM76CQCLNQIMYJ31EwAWawQYwDrrJwAs1ggwgHXWTwBYrBFgAOusnwCwWCPAANZZPwFgsUaAAayzfgLAYo0AA1hn/QSAxRoBBrDO+gkAizUCDGCd9RMAFmsEGMA66ycALNYIMIB11k8AWKwRYADrrJ8AsFgjwADWWT8BYLFGgAGss34CwGKNAANYZ/0EgMUaAQawzvoJAIs1AgxgnfUTABZrBBjAOusnACzWCDCAddZPAFisEWAA66yfALBYI8AA1lk/AWCxRoABrLN+AsBijQC7YqcOCQAAAAAE/X/tDQM+sACwT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQawT/8FgM0eAQaw2Ll/1ibCAI7jx0OSJvFsmz/XXCyhSHSIlHZwqF2sm0tQcBB0EARBEAQHd6EIbm4OLqKTb8BBXEVw9SW4VQdfg8+B0iyXSblvr9/vUtpCud9T+PAklHLiTxAw2EYBEzBO/AkCBtsoYALGiT9BwGAbBUzAOPEnCBhso4AJGCf+BAGDbRQwAePEnyBgsI0CJmCc+BMEDLZRwASME3+CgME2CpiAceJPEDDYRgETME78CQIG2yhgAsaJP0HAYBsFTMA48ScIGGyjgAkYJ/4EAYNtFDAB48SfIGCwjQImYJz4EwQMtlHABIwTf4KAwTYKmIBx4k8QMNhGARMwTvwJAgbbKGACxok/QcBgGwXsPwBmZnZyQtwj+NeXGkzwBgbb6A3sH/xUATs1EwQMtlHAqgOs+Wwv7L++mqxf6rSS0VbnaxbySfysm8ZvNrJZm3AkS4Ocf3mQJxQwAashYKMX260Lt9JFwPqD5jgUgMUvhk4LcCRLg5x/eZAnFDABqyFgvZWbD5PYMWBHd9vT4dE8AtYcn//ZH8QPZzaJx/8nyPmXB3lCAROwGgIWL1ndNx8WATv4nI4+PikAa2Sdl/NUwGgJGGyjgFUH2OqdEMJ+ewGwnU/57uH3ArDeg3YjyyfVH8nSIOdfHuQJBUzAaghYsvp0L4R8cgxY/u3Vj3w3AjYd9gdJ7+1m9UeyNMj5lwd5QgETsDoCFgn7stEfRLv+AvZrKxQvHeNnIdZNqz+SpUHOvzzIEwqYgNUPsOZ47WJye2O7NR0evG/eOzzbyPJ32ay9Pk+LV5DF+2A7930PjJWAwTYKWHU3sBuXz4W1x4+S5PrzlXDtSisCNolgRcCKV5CFcLO2gLESMNhGAfMv8TnxJwgYbKOACRgn/gQBg20UMAHjxJ8gYLCNAiZgnPgTBAy2UcD8f2BmdqpD3CP415caTPAGBtvoDYzwuzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzOz3+xdW2hcRRg+HLKbZHezaW7bzb1tkjZJ64WoxKWYBJWmoGgQE4iVisQitoh3BVsQxFbRGooo1Ydqq2CpD6KgIiIoglLtg61olT6IRUWq1ivqQ6n4fXM55yS7WXOSXXY2nR+yu5mZnfOf+eb/Zv6ZPfNbsWLFihUrVqxYsWLFihUrVqxYsWLFihUrVqxYsZJfTDmY2orFYUmKxcF0sVGJrFixUr5iRxwTxeJghlgcTBdLYEaKxcEMsTiYLpbAjBSLgxlicTBdLIEZKecIDsv6twzNSqiOOgbJOYLDfGTFxvVXOebJkiCwukrXdYe/YNcfPZYSH1XitQ86ZSmlxiHS7bbh7ZKOK2vylrtrV30D37sa0di1793qhJLxjpW9jpbkZF+VcVZSShx061/8wJrVecuNtVRHFQixdfx0zY1uLOEsSpZ3AtDMc1stgRVfaG2U2h4alEuJ3TakEjM1TjlKKXAIT2Ds5yQwFnQp59/shJGxh73aaX6WwBZGYHWV1VENQrxVzGNdwLcI4UVdypYhS2DFF1obR531VyUn3eGjTvLllkwNEzHEu2a5JGVhOHkILK+tTaT6qpaYn1IWBOaXj//MmVekue+JjYskMF40srllQ6+x0Cw1AhMNvLwzUyPn0m3nIZE5Rra62YYzk8BWbKyOcpDnS1tFE531tXDVH6GHvmbfALq4KoiXTT0VA9v2tFRP9yMTZrd9pxu749bkqr4qYCJeUGz7O5XuyG9DkWYX0pZcteHITnfkMSe5alOPAPGewXgr5hD8phNelhAOPoG9PqMFvcZ59nrRhGw0WX7D8f50u1PRVP8pCGx2MY2deL/yd8ydQXWbelac/eeiltrbh9D2gFy8aNasALqAib4ks7ftQbmtTuShQSYgd/29g+614UGyBJZ7Bgb/f1k/ByvJW0wcvdDOwBZLYPQSk6vc+gYM7FWi8+Mz17yyCGw6hQIDSF6vCIwOPf1K0h8y8NJU3yATYwmPwFzKyl5NYIc7anuE8bkGYGcIga2e0YK6cbBONZvA/m2Kt3InZAw8NLuYxk4S2PukwwoUX7FRrr9kERhnYOffLGFCWVkORjbWIt6hj/YyQ4olsNxrYMMH0cgBAlOG4ZSjlBiHIIFxzEefj7fSduTSOxIG1qyGvfRWeASmOnPFAAdzLsnX9kS60c25Nr+8c81qmFO6Hab1ebO7bStMA0BhxTJTk1yFQfyuXX1VisC2XVH7uOStiRTKhJYlhEOAwIItGNWNA7JB5vqrAgTW2xxLcKbGrNnFNHZi2X/LEDlxGSZsKzaOPJacBNHNIjDimalSvaGyDTnDR/Fhzd6OTI3YvBFQj7WgU4QVS2A5CIyLXx6BNQMPJsaef8MpSykxDkECAw0lJj678+rExAPV0cjfdEfIanJu5ROY2rRSCZHNMBE1udrUg8WZqe6VP+KFPIXRXBTDpBnjPnhLvigCkzsB13Az2bUEpgks2IKqcchqcioUJDBQ0i+dZKW2rGIaO8e5ZxDjOmfWoDu5tsWXbALLPBl16PFz/iaK4EsbjsiJlxq8xEtYsQSWbW1iuotmj7eKcSaWECZYtlJiHNikGOnRX4VHkj62/8hA+th1CYzorjCC5CTern1D9V9/tV8loBzQUATWVxVpjh1qTF9+9lBjvNUnsEj3dbcNZRHYJ9fT1rorXUtgl3SgqbjJmG4PtiAbx/PQuSgVJLCuxtr7gRN4KKuYxg7mwZViTI2f7AC8AQID5EEX8p7BDAu7bi4Cq2+wBFZIAhOkhRcuKl7zfeXKXktgi/yJaaaKDQkeqmjKnEq/2fjWqXgrjIktzYUx4cnkJjD6hiNTnAiDi1RtP6USyzt/ug+mqFzIdPtYy7atThaBrX8xlRFuq3UhI81cFtlORg+2oNc4dbdIDy9IYEDIFazUllVMY5ecpJNOUFhbkMDOvxl7M4E1sG5QJ53XoAuZPtCUkVBbAiusv1NHY5tO6Z+EWQJbjKCn64akHWE6BsNIt6uFYbEoJra45iAwFKaTIcd8JKB4XxXcTm7yM5EVSy90/dOzCezpbgDZ7NoZGJtKr+MGWzCiG4dgcD9wBoHBYRSs1JZVTGOHEvTrUSVr8wkM6RSfwFBXhkjrGZjcfJEzOywyWwIrJIER4r4q6bHHnn/QsQS2OOEKinqQAazVV4XOzN6e/DDlXrvnqRr86o5SfTgvgTnP7hTvtLn6BqTSwWcid/PnIrCrxvFrDMwEat/bea4TGHuzer4h2IKycejiSQ89SGBoVslKbVnFFHaawFSVPoFFHkL+qwM+gcm1ZPwIY+RkJwjs7D/fV4IHneS9g5bArBhtOFry//ibe1jO0haTceBuI930RPhiGsTS/07FEtjSFNNx4BSLW1vlbwDli0NXo3pYLnwxn9nKXiyBGSmm4wA3gs/MDzlLXIzGYXRPC5dLQhcLPsda9mIJzEixOJghFgfTxRKYkWJxMEMsDqaLJTAjxeJghlgcTBdLYEaKxcEMsTiYLpbAjBSLgxlicTBdLIEZKe7/H2baOuvZn6sT3scb5rs9XtGUbs9VzcTbj0fnffYwDpiIt6rKfL0izfUNs/TKVTGfc9G6FHpTjHVWR53sG/Qzw+GwGFB4NQvKvEApUwLjcVPzLYknXAoh8sHi/MKDY0IfKyoffS0igXU1ptuLR2Dj7+Ip6wXZCvXytVAV6rdcFdftbw3YSrZaIW4krK3QmsPhsBhQqMo5BkoEKhQelPAEpg2Z2vBBBflcaN74AuFFPtU730fECnSIGi9ZbAJjixSewPgwdXYfKIXgAePn0FGz9eJHpVd+9QpGYCGFE41wOFhQQhPYQkApBoEp6pKPis45WSKzFZ/AkpMhYkcs6weRlI7A2CJFIDCOpJHmlSdTOFeAJ/wOr2VnFE+/3T7EX8nXf4p/OZqJHsE/FLhJpu1rOv1I5fCD31cOH2T/FI/I3apr44F5o8g+Ki7y0k4cHVHRdHpH/aud4lhOPhj3GE+pGJlC7+Rg+nXnlqfx8a5d6QOojCO8rFDNR6hXcjJ+sp/xJlAxzuDBpV64oUqWUv2VRy58OSh1Gbn0MJ4fjillG4Qy4pYatu/kCVU0Q6HMNRe14HFyfNXddoDfSEDfwdM7xCmksiVkuewbREV8qG/5o4nwOIQHxVflztKBIgvNDYrDxqvevWhQ1I0TlK18mjx9AevdLIqGAKUIBKYff4MiaKLw1BqewIofzY6XLE8XEl0Mj7fd9vQkjo//eOt4R7odfZLMPvF2j4NOsO4wkuRcHRniody6/Wtl2uGmDM5I5R8+1jcgNNB4B/q7rA1daXfj8MF7zrST/Ff2Lrt63eEm4bHzInd9ty7a/UovXIzRM7+kEjTZiRS9lbUIkEbDY9eXFQ5JS+Xluz/q4RtGnuGD4x08amLLkC5Vd11CjI7n3zzWAl1AbCf2t3KwH5fK7kYOlZlIJSr+POqN6cw98WQUqTTJ6Sf5DaEvn+Lf3+q1BCvOusHDqAiFfF8mPA7hQaEqe0sHiio0NyistwCgyBt/U4DCDAUKi4YApRgEBk14MFDfr+JkgvQvOiaAOtr/sg9TsTvk2WoqmIB/6L+OJqBPOeAD9sy9AGR91Mslm4hzonhyJM9ByBlNQseAUCrJc9nrG6CSeEH4gw3HMWqoOuRZrW1aEz79JSdlcvTGJf/ZoSvTCvkqOqPIPNkJApPF5eVW6/vDYMPiFB2ownFexHX2kMB0CAS2iM7mE/071Hm/eFm44bBrcBpMmxFdrb4BnZH+AfHHuNbLdQzpIbDD3Xj/t+xTMo09kf0a7xif9+K77NaqNnSlr1GNF2VWFBIzXvRjFqB91H2EGyNzNMeP/HgfbCXz8zM1DuuEXl2qQuoobOXL1LqosBXqxyqa8VyeLiVtRWgudXGUhuoGmCNm3cjRDoZSRnwPxjRyaVT6N7DrKM8iq47qloCyPV1ZNxhVFfFLocxjMaBQldKBogvNDQobrzCgoJa9AhReTIHCoiFAKQaBoWbROV+Q5z2SC0QSjvbXos6nVcEE/EP/dTQB6f+JRKA1oCIJ6lwS2ESKw0ALE+KtOaNJeDEgfAJ7XQYUxAtHKuZv6FV1TCkC05qQDvlgvtACb8HKPIU8FXkmlqAsVVwRmL4/vqugRzpQBQYa//Qk/7xfL5upm3oKS2AgV9cVtqIVwwQsyu73Gjur8E/+PPnBPnQdmTbTVvaphvW70nR/0FZQAuXl8KoDPMBbwby/7pbfkSPWi688lfFsRSGlCYxOkrIV1sdrMEOVoq3MuhLnIHyXyvJ7cgDngMGL67GeIY94t/BWRqYkgbGsGMZ1S3Csr8i6waioqKgElg0KVSkdKLrQ3KDgcyFAUTcuQFEElpBFQ4BSHAIjZWGWMo0zVHhZHRNAHe0PP7i7UsYX0MEEVI4fCUK2/8gUzuRmWAh4vBMppKpcENh7OMgTdwAccN5UW85oEuCc4YOjF4omU7EjOEEDQLEEe4+IEeHVoVw5TxNyIlmXWkQ2P+4FlGBVnkKeivyAGJOgLFlcXs4PlnD2tqEXz/B2IzpQxev8gHmbHwJhNY/oU9n8h9O9whJY3TM1arBXywecgBGut07JrrDs4ZN/HfjmpberVNrswV6UiuQb7FmCw6u/PTV6IRvzp/sSasPryC50Sj0Dk5f1bOXQJAzJH+z3TzFDl5pzsFfKMkeN9eRlaKjHetqBzJ1Owbo0gXEC5i2kcNbTlX2DoqJiElg2KFSlhKCwUF5QSEoFAMVRNy5A+dwjMBYNAUpxCIyUNY37ULekYwLwRDN5/Bm5gn86mIB34JkfTcCRfiEnW8zAfWzqUbmkJs5Y8A19zFquaBL8qlq70rEjcPX0mx2nd+FFzuMcrw7FqJ4mJGEZiUV0hxmVeQopFdUHroHxg385fX+YV6nY69457//iA6/rnyDeSwLT2b1MLQSBEfSAraDn0la6sE6CPPYpARfQgeD/u+Fo3fRBg0qbaSsNdc9UzexKXOEY1cstY6rncnhlVdxxj7yMMDYMrYbBRe/Y133Uo5dbWKFyofAmPI4tQ+i9SOHYk25nv2Yp31a4CLO5ReiSPPFKL7u3UpY0tZ1LO+K+dk+yJaUyzCF1ncEQBJ0lgckJmKNagkXElbJsZfcJKEo7DE9g4UHRqpQSFBbKCwqSCgCKI298H0HhCp8kME7AwoBSHAIjTxwjv175+wBUVTEBsgiMRwK7MwkMflUeAtO5SPwDx6EHyCdnNAmfc3TsCHqyxx89tCp+HOFY8hMYf9TQDQQWQmD6cn7AidGvBl0khCYwHT1hcbuQGnwu1P3wCm3FwUdMROEtyE6MF2//mJEhHJE221Ywx6S/rGpT+0Fyw6v/k+uxsKkWyFk1lvhYNwusdUSd2lbGO+r3yQ0vehGs0N/winTfAhOoxDbUDi4eylmAuqy0FUx4Y3dcIXVhzYwwqW5AKCNuaerllto/MGVxlDLQ5ZPr6XqgTn5De0GsVraELJd1g7tlReF3IRcMilalZKDQ1WOhOUFhUiFAUTcuQWFY1mmOKrJLhgClKATG4A71DaBnbqbqmAA5CEwFE/AJDC7ebBeSa2cZ7iLFW3UuKYPHoSv3D8DkiCYRJDAdO4J7Cffvx3CDcCwqy6sDSikd1UuTqE34hA/dPqMyTyFPRQ522oVkcXk5L1jC2HND4x2iP/qBKrjBFnQh0+1sES9bEpiOnrDI34GF/2lUyPk57SZPNvt8br2SJ7B8zI/zmrWEuIEINtLmrMrA34GZBAoIaF10SYCyUAKjjydpnxMKHRMgm8Ca3VkzMLl6p1a7/Aioaqlc55JIEFoIu72VcmU9ZzSJIOfoo9eZSe8fJK+yIl4dSMXJ7VoTVonalBZtMyrzFPJVbFJ3ooqry+n7E/9ypucHqlBqCgILbGt42ZLAdPSExf8SP0xP1mkFsxXOpHLrRTOZ3/MArCPEDYhNrEJJ+F/ilzUo9B6XBigLIzDlwFNfWq0OM+ATGJpWemwqmIBPYDqagHc+JFbLkRE/KeaZOpdEAiZAcfzkARk5o0nkJDCqxM0NkI/O0nVwUhxLeJqoKkVcBGQHK/MU8lV0uJfy/ircmiyuLqfvT/0CkKIDVTg6MIIfAoEt4mXzH79QQZ+FzO4JmZp8adkS1rIqsJOfW6/kJL0DlZtfSeyuhLgBVFwoY6H+60I+C1nWoGD5JD61JEBZIIEVNiZAqcKZcO/BKIXsKQgmi8XBdFkggemYAGVHYGRdoxSyhmOyWBxMl4UTGOcyZUdgZF2jFLKGY7RYHEwXc47TsWJxME4sDqaLJTAjxeJghlgcTBdLYEaKxcEMsTiYLpbAjBSLw3/s1DENAAAAgKD+ra3BIRXcNNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcw3EH3wEh3MNxB98BIdzDcQffASHcwxF4dqyAIBWAUvkTUlFtbRAS11NrU0AO4NOrqC/gAvplPJ+IgXgQFQY5wvkkuiBcO/NqBzgFDsgODHegcMCQ7MNiBzgFDsgODHegcMCQ7MNiBzgFDsgODHegcMCQ7MNiBzgFDsgODHegcMCQ7MNiBzgFDsgODHejiAZOk7fCPQ2QHBjvQOWBIdmCwwyL78/sQYisNWFUfd79XGPiXafeQ3C8zbp+dQudWPEMvOkvyx8jx1Ffa1z7Xbzp9hU1q2LfSUBmjMPw1zYzBzFiu7d4ZTNfMuCLE2FL4YSu6ZJksEzEkVxJCoZRs2csIyV7WP24Z2X5IRPwgsv4Q2bNlyxZ53/O833fM1zCu2zDFyY/vnu/7znnP8zzn+d5zzjB56NSMkuRa0yblgs9+q/pl+puiDAqmfyjL+Q70ITH8nj5c4b5lv0ocns/rFHFYsUYk0J8gjYpc8tVAxi5HbHW/WdCTqMF+o/rFPrDYHBJlt009jNyCeDfuO7ZBLUreDWz6/WkVrjELfZli6V5lA8sNSacTkaoGjdf+CQNjcIY3q+n+CQ55MTD/SG8m0bErOdQHfVTZwOxtVFXlzju2VvJpYLmRzu0eAFIMLBe4VZ8VeTCwrCOIH/FU8TXXnzQwV3GTpjT+jstC/clt6x+jGdT4+Lb+ju6pM40cgyYRMJ2anR9GV8XqToDD23zA0fOS0W9RjTpTK4ydXWpdH+qLDX1Wo9sqNxG9s4uj1zIL190l3Z69jc5b7KAWSh2jI7gSTVhtUS9orocxb5ijV9LZgWLp7eOHp6TptffNuqfCUyrKO5agbQnJQGsSgmtBF0edqTs+BFzFtQMtGnjLH9Sos3z1qSKDro2CLRkG5gq3rBw5tuRxCQ9/DUInnBiTZX6Gz8+kHPoOWanQrISermv10kEl6FcEhu41dTZs0pSY20itdHUrmlu+GOozbSV+s8joN9+x+UBQAOzg6FsmwEoRTnRQM2qY+iAaX1x+3ajO1JTog8m53qjWtIpMfUh0lj6g8vjNJEWBQDny2kk1HB2z6rkugHBxD/aSxwxM6WdlqcIyNP5aFwofApT7CFuwixMILF+ATHUCJFRNBjb843I3tU51F0Y8qeVVUPcI93pUg9pTkJgEogiIfrsaSokl7pbVsAJVEuf5LmDMaU0ANZoVJd2W3arRc0+2uZJSg3t+hwyq3r21iIUGhpjFwDiUoBaVGuAy3QdIFlEBA2IU1LowyDwaGJDSs3v4xwjJmQbTlL6wg0d5yhMTU2xgvZLT73vlDiuP8BrZJD1y0KSZR9vcvr985a31vticKRWDFwZC47fcibjC/Bgyy4XJfu1bVjaY2GNmlyDN0Ra4EgnotkqD/pE998w8Sg9MSY9sonqZmKaH40ciyMAWvNtj1NtfVp5oSa9JSNLadITQ+EjENWbUyQbeTs1aeQjier2Sgxe2/uhR06pgS4aB9Wtfc2Vp23GGGv4OhE6QNpzcY/XQAD1BeC4rT/ADgmxUKiwkm4bWiy2VgcDbxwOxOfWLQvd23J+a4kQbNMPAJJnyj5xSsbpREAD6SLUmTSgmJzqopKWPHmRgoF70YZHjz9CHRKf1wQZ24YOHo5CWOHIM54UZc1r1nBYO2chsJZ8GpvQTAJahLyp8ESDuywQBdnGAMF2BvKyYhHs8QECKquOXO4+cbFqCs0MbN6AWVHYAEoGIn9Ig2tSQLA3WHeXx37rXlNQwXFVB58lS9TyzZU0Agw1zYqq4TsTZMJhtrvgwuHBNN71kMVSJmC0DW33Eo0U1/f7k1JhTX80+kiDZDVEBA2IU1Eon+TYwnYr6b7Vxt0h4aDCk6lRx9wE0gK9sYDzAIO4oNOsXGbGhF0Z4OGO4uL+IUPTFLnvYC3m2Dbrq1oktfTTrvY0qRtjAwI02MGmLeHHeifBk2gH9EGzvxiE8GNjjgainHuk1CUlaCyEEhfhQX72JO0a89JLYS9Uyp15NN0+rgi22PbAKhgzDR+ikqHYLy1iWhAPf5DFayFZKhYVkgMEgJ3zjcwmBiUjo9MJkcc3BdIPAAs0xGJisPZxEJcMJAFm1ANa+qtJBWfoo4wzMw/CLPkCOK9xkR4Y+JDrRB5hs9ZKacI5YJi1R5DK+k2bM59CzcCgc65JXA6tU+gGWLFYOXwSI+wj7ArCLA4SQAvlTca+HFTArqHrn5RlYVHEdow2oGRUiEZAwgdKHBjFtU0O69BN1y7ZFapAqihN6kGUkJgDcnvtSuGafKxhc45tFZI2IJUoMIeYB1h7YKrclKqMuaZG6NvvYApKjtBFBogIGpCtQi07ybGAQLrxhzLQeLBb6ZlP8SC97L2MDC7LMcUcoJGTuUmjE40WilkKmQPkhInrjYp038msqKb3Rp4tDGRiutIGhLeqFVyiytuEy8yYRzg+PhoH1fjKxh+Hf22exAwaGkNBaCCEYG/tsK+nti928eGrblIM1z6kJSOxsKOQVpO1DAsgwfITOEHOCruDjccN4BNkNUmEh6VOwlicihh8E+sI1Dz6f//6Oh+7zW6BZDAyi5iu+BQDZwCyatAgzgtL6oP5kOSr6EOdDtdaHjg764IBrjT1VxuRbLUVlfFbM6Bl98o0/aGCA1gCWRmjiAO4eAhQDQ9ivgJ2ktAJyv7UltFwjIE1Vz3m8LqINzGcAavW4BYlAhDAExEqbGvylnwQtUgOq+EnoQWzKnABuNRplvnquSCfSPwZHnx76PiAWNjDEHEW0egsQQECLceljJ0iOUopoiYr+gVp0km8D4+QVO2Fs84zTtntNWaDwfr9lYLiTMwOL0jtnoDsrT9hwYmufsyoDG46rbBkYNSf6gdmzt9LDkoF1v33c5wr37DM3/J2BSWvyVYkP3XWVAHXemT1x9agZHj9/mZidXYW8gsxqYBg+QiecGGIqv5aBMaycY+EJKnUXPokUPz5VFJIMjGlmndHDtgwMALKBaZrsnCAorQ9DGxj0kSsDE32oJeTXBm3cZGBWS1FzfGbM6FmA+CsZGLCUDCwpAsR9hG1iBxBMkA3aKwrGkW1hE78erQW1gQFqMTCBJGsGZlMDGxh3C72giuLMMDBzAhjawH40VzA4o97z+V6XNjDJEG0GJkCwFnUfTsnA1CGkNjBQK538iVNI/+6FPrJ5HErV8hoEXVPajnCNOVVkGRjuGLLbksiyBzYGe2DL3bRhk7EHtmUELYZLqCGvE1cCim6L9sCoufJEzZUNplSE929IRBRKEf9u9RoZWKqYtod4Gf+dgUlrsq4P8QZFb1o6OSido4yd9gb4AGVwSZMCXkFmNbDyhLpG6ATp/ak9aFOCcMi1B5bgPYwoUmlDCHQ2pOSmf/cB2J5hmvGzHYLMtgcmANIk0jQhSQcnOqjv9KENDPrA9s3Mo/Y9MERn6QOb+HXZo3xWS1FzfGbM6LkIQPzpPTDoB1iGvkTKWaYQIO4j7A3ATgzM3AObUtGvvTc+NCCq3nk54rwDK2NwfQaghoEJJEmBCGEIiHY10N/otozUgCrovKs2MGsCaAP70VzB4IzpS2oHtIFVImZ3poGJqKYrLXrMPoTknUpU2sBALTrJu4Gp44merV1hBxWe99tJXfFGLSvPNKLq7zIw3GHlHX4ixy/fnUJeni1nGDu78KqZBqPPyirHlNTaurhJ03CNoFwBFKstOYXkn0GpE7c4r71HR/jhYTXd4Rqf6GcUtCs6s4uj57aFAcvAXNIaQihPOLrtOuo16k2gzRjqovyBOkxpvLSQV5BZDSyG4SN0XmIM4yWGi+AjPG2nkKjQrKiFGFPZPaUIJPupX8QNEVF8QCY0L2hU5/VQ2ymkADi8WW8fgE1LEiac6KC0PnQGJvpgcp7OonAz9YHotD5gYMNPeHEKiZaihgxHYpaeDfT5h04hOyjwFuAUcmq6mfx5rRGFLwKEehG2YCcGJiBTHW1nMpBQdeyyebJFdbzoFKhhGgKJCdH2Mg2iXQ30N3XLmLAaVBV0ntIGZk0AbWA/mCv7MLgB/LQ2sChitmVgneRZ0iL/MEz6AMnpUiWqa5aBgVoZZJ4NzF74tOLnd9RQ7cW+Wed/kNt5pa2fP7DbU71h89GlUcDFkSN0nIGs3uSuDpK5fweWvQzeVwU8oQ98XbJFV10OnW/yuRXgqMrDYKUaJSeBANEOAnas/bt91dE2Z7sZFXxcaBR++TUDa9y856Ucd37JwJyfx1XbwDC9qpWDEuP8uS/g4vh56K7iOsvdpNQawXwZmPwSP/vXw/fLeEIf+TAw9Jn/X+JXoQgrf9DAAIKzYa8kX2JOVFnbSKYpbbQ54xD61V7hl///F7Igy38eCqP856HQy38DK8jyn4fCKP95+MZOHdMAAAAACOrf2hoeUMHNOwNb0uFBhzsDW9LhQYc7A1vS4UGHOwNb0uFBhzsDW9LhQYc7A1vS4UGHOwNb0uFBhzsDW9LhQYc7A1vS4UGHOwNb0uFBhzsDW9LhQYc7A1vS4UGHOwNb0uFBhzsDW9LhQYc7A1vS4UGHOwNb0uFBhzsDW9LhQYc7A1vS4UGHOwNb0uFBhzsDW9LhQYc7A1vS4UGHOwNb0uFBhzsDW9LhQYc7A1vS4UGHOwNb0uFBh9g1txCZwyiAf/3bHTPMYOzaYWZMa277DyGWaVJjHnZ2lBqbuYSmKTbJSBoTCqWmMG1IoV0P7iW8jWwRD1IijyuX9SCyLoUXl8KDnO/CmDE1KHV2nN/D7s7+z/edU6f9df7ft9ghgaGE+oAD6gN2SGAooT7ggPqAHRIYSqgPOKA+YIcEhhLqAw6oD9ghgaGE+oAD6gN2SGAooT7gQGOSBS5N0yZszDJF7pNeDhrvdrIqqqOaVlhYXdrX+Kui1eb1M6lsnUM+9p9BAkMJ9QEHPwR2DcywIzHLwCSdd3VWF4j6Y4H9PZCNBEZ/OEigPuCgQmBsYsERm2/XwsWmeVq3visAM1moPBfFno+Z0LfPrQFvnxgZs76yQ9RAAuK5wLzhl2PCxYk3Wpin1cx9E38zJrjfMP5mm7bsEht/1h5886Hf9aBt7KYsREd3Vmy+QD7gH+SGzbvatA3Z9iWQrKPEK+m7DTXdH/IlP/cZduzWtGXrxOI7gbGzDrQFjUwWrvL4ZYRcPkeUdN51JwG/6oxbhG1HDSQwlFAfcFApsKYVRusFPZbu8MO0Y7vsa86sspQFZg339hQc8ENqWPeaDKCp1F09N7L+cOaGzgUGdls5ZfCzkdmu8qjObRuyEN9TKMbSs1f3FHqjczv6XeHe3Ii5aUVx6tpQSoQpgckH8EFuCNn7Ho9wDyY/+3gl3m5dTGALV64PeVrXhvIBp1g8XZ84Y+3hqR1+VbjMU5IRcnlSlDRTZkiRwAgSWMNQJbAn/GuzVwiMiSlKCUx8XbSUcaLvLcw23AKagqiJBZ0v5gKb1MKsH/xWk8H6bBpfO2QEL5ndJgNsU+TfrDCBmQzN3imnQGDhF1nGygKTDwbdTrVh+0ldPn4kX2shlRDY5m5dHdqJmmBbsK56OYXCSzKPX0ao5aKkQRd3rpMERpDAGodfJrDx5yK7NSmwgcgJe5cSmBICJ5aGUE/aCJqCKHhLA5zgBNAeF4ltuL/VrOQHK7/wVTBxwRbwtN8lvh2DV8iDdvkWqDaXD0pup9qQ7wY0ZzaFmKhETmDbXh2C7NGHkYDmVIshNc8rC1cJ/DJCLRclQQYIJ4ERJLAGovoMrNe7OLJdTmCp+Jl7p6smsPJIZH2708y4wC60MKAsME/6DLwa1pvA+LYDCWeNCUxtqCawAx9Xg4CuQiVqAvNZZ69OXjseueWqFFhJFl5SeWSEXM5ESUpgEA3lkMAIElgjUH0Luc/ND49AYHFHOz9U6qo8A0tdNvKZBsjtGedgEJUb2RjKXzGWBcZ67FOmMXUGlqlxBpa/Is7ANmSjc81lgckH8EFtCGdgt12mx8M69+oNPbqzywLZQGBNTyAZnM7ZqwSmCpd5jsmIPCwHREnfBTZ51r5HR0hgBAmsEaj4P7Dg0SzLB7TFJwqOpKvreloLnrlirryF3Djo0oBFSz2toCmIsgwkYF3oJ4HZ9pqZnMC2jpHXgj/fQr7eApeP8hYSbh3LApMP+Ae5Ib+FXLbOqwHdX6GSi5eNkO3pEBznP5uWsY89vhvy//wKqQqXeUoy4qBYrouSlMBg3wnv4hYxTo4KSGAooT7g4K/7ADeDrDZwHciAGi9q5XO0+g+qSb5vYb9JLG0y/FrSaIUEhhLqAw7+tg/W5TA+1cLTGtxv+AcCy+2BAa8+MGDBZLeuRkmjFRIYSqgPOKA+YIcEhhLqAw6oD9ghgaGE+oAD6gN2SGAooT7ggPqAHRIYSqgPOKA+YIcEhhLqAw6oD9ghgaGE+oAD6gN2SGAooT7ggPqAHRIYSqgPOKA+YOcbO3VMAwAAACCof2treEAFNw1sSYcHHe4MbEmHBx3uDGxJhwcd7gxsSYcHHe4MbEmHBx3uDGxJhwcd7gxsSYcHHe4MbEmHBx3uDGxJhwcd7gxsSYcHHe4MbEmHBx3uDGxJhwcd7gxsSYcHHe4MbEmHBx3uDGxJhwcd7gxsSYcHHe4MbEmHBx3uDGxJhwcd7gxsSYcHHe4MbEmHBx3uDGxJhwcd7gxsSYcHHe4MbEmHBx3uDGxJhwcd7gxsSYcHHe5irw5KAABgGAb6d73/FKRwZyHQGrAkHRp0qDNgSTo06FD3Bwxgh8cp0qFBhzqFAAAAAAAAAAAAAAAAAAAAAAAAAAAArl1rC5kpisKnkxkz4zLMMcyMkFBuoTyMecEDeRESikRCQnLNA0rJLSFFIbkXoTwheaGUyAMPQh6Ucs8lRMSDb+39nb3PcY77nf2l37nss9a39lr723ufOQ4ODg4ODg4ODg4ODg4ODg4ODg4ODv8KMtVGC8/h70eTcrP2Xpc2fqUDDkvBt5ho3TS/vg2sfATtOsE20HLR9qz340EGaaYRlkKfU4Xm0ggcM9Uez8q+AJfiwK2c5y2sF2ZuaYOjH0aHHRDHtrdB6qgiq7RgvpoUn/g8yVY9QfALo/m2dt8fUhdUWLtOpSDxLO99sVOW4fzXuU8PhUs9fQEuMTbxaA+EiznwHH4POGyRLySKdfyzBAzmUeC/EFbAKn3hOlOFZkmRXtQC1rl7Wk8M61eYOdD76cAA+SoBI36mgP1ykOBX4nsFjGX4+aHQVwvY9IFOwP5sSEpaDV8wDckZ3Vxn+N3uWmNDtkm5Ma/2eLWahq524rTEe96wZb4/eK3nLV3ljzylBWxYv0YLlU0ODbZRAraxbZ9FKIjSUW3HL+bUqOnSprSz3OzuGH/kDGVKLGeWt21sX4bKGNG/5o88xgXLGB/HuFf3B12WcX6tfuJBKZABD/v5SfDIJpaBLXJ1sUn5fi2fpZqOqjVapNXvsH7FdVn1HOkoV6WAXFpPM7yDYSubFubO8loeqPnFOQO1KjEUcY8ncNsoOJkzEPSAL9RgzAf07cEDxNuFZapd0uFWIbWvLs7wuFob0DcSdqkt/lexURphsHh6DJ6gZQ+wx8waScYznl+vrcaisVGaxkyvPW8dNbjjYY8c3SE/WVncIJj9IUN0Ax5BP29fJi6ASKscO0vTOb+yqVgxKzATnC2RLqw+CgpqqrG/3thwXtmmsTAc6fFSQGrRfKYMhZbj/T5TPEDOlRdzoJJqDjyH3wdJgJRW/opMQGZQFbvJMCmc1mOlF1Ilc9dG3tNrm1IwtqMvEAHrNbHYLS4fbAMHT+qdu6vK2artYC2Uqapb+fNlbSGrTMHEuLYY3E975FA9gNYZNWJRTLgnSyc8K/vCZu1xPS9VCI/n2SRVwHT1P15WCmT+FXvQqRQB296v0DurnyMd5apynFxaDbe85b54b+pTjMgS18R916Z6+mYPkzkD2fmhgI2qyaa295UqF4ctkw7Xg1S7TspZKGD0DSPqf4kyXGDAn+ZFy3LNHDNrhmQs481pNRaNjZKUbQmE59Lp1qDQo7vz5UqHVj2FXpj+/L62JEwX0nm2VcDO0nSquieMgJngbIl0YfVZAfOBIQ+UbWtMgAoW0qAmnCPdm00OBTAdPNVTQEPUh5DngeLLAydgvxNIAOoKOZd/gce0ZSbVKnvLqAlA7aow88gKhfdUysZ2bNa+Vc9BB0dMFAFrPBqNURJbgek2KLsdR6BDemhpO8/KleNtUIHQniZlWB8Fi63x/LB+lQ5VnC+swxVMY+I3laV0CHY2toUFkQbUEga0kA/g8aZukiKh8AOX4gt/VWVmujbF80kB0xKoBczQUa7I5bzhDXqzZ0l0npjTS1cbitDpM6XljT3tKWDHyZyBRLaQuNpAJzRkdVUKqr5YLTRPcSikuBgIk6J9o3EDYxlrUSZJLrMXrWVcNMfMmiEpWUWfMOO0Go0mG4kyUQI8Z9Q0iA26DUqkFt6gEmRYLa6DsCA0uhDzttV6dlYT0DHbw4iAZT4sEVafFTA82HI8uhKrK/a8DUfKUKjhIhqa7k0OBUQH/xSwCTm4LQXmAG3zWXPgOfw+oKDUnKO3iXwjossApSNpROZxjvJG6fNekHk1dBUyj4KUB1BCahKOCxjbQGUwA1LAaKdaOrrm+ZrmVVlE6aLD2DVrPXCiaQ50mWoH3Z3BWVRETt3LT9pzb8/Fsho763WTdAGDb7X8Gn4I1mUGH3TQUwA3mlQCVlxXLXAgkM5U5YpcorzlksLSobtrXEQwFOgpl6p4FpCYyfw4WVoBGwf90gItQYs3YT8p6VCNzhHXd9fQRCeFvuGLIbNzVeTsRWMZF3lssmZIxjNOq+cj0WQjUX5YAjxn1NagdddqwoA2g85W+g5vzvRvpSjF3iHZVkwzWqKZLBsb+5dQwGxwjIOqI9VHe4wDKkd3oTH6kjIUaqq17d7kUIBio49ZJydzyMvhwBzg6XyWB07AfiswOiejtFoNl79JAcNMLasVrOyPYgYN722dOOjucdRRRMDuby4FMfkYodug7AqTsZBSAkY72PbNQ70+Xl3pIO89Pi1g1MIxfueLUQGDqc4PS0dPzgNt8aibdE/dQsLB/c3iS/5i9GNSThOw4jqUNtx/XMAsbwpYZtKE/cd4zFDSBYxuyNII2MI6Rl6KgCUdCqn5t3ctgTMmhb61XEjI7NzvEjBjNRpNNhLlhyXweQHD5n11ftLhh7CnGa5PEzDbygoY9fRU02K3TwoYq+/TAsZwrIDBjO3exFDA1ILVONHkBZi0etnBHMB/c3vg8LvAlxoqtz5Hu+xI9BYSl9VKXOX23OJpOXOvLyp0YT2yhdRiBiudu+NcT9Vso3ZQfDtDO2gn2oBSt6Ne9mxp+4OsXSkOkO2A/bEOOw6QBHkKFkcLGUQFDF7gS63IoHRdz3X72I9QencZbiGNK8PF8MalPlNGTOzxDPRHTCzEBYybKY/gFjLqJxSw+avVy7jIFhJWYS7FoSJ1DlujHjkmpQl9GwFj537VFpIZb2EyTqsxATtvo0yUAM/NFtKInHGHbheSkG+mPyPr3IV1CE9EwGwreVNg+hPgFPnxLSSrDyaK3UDFChh7Ppac6BYyMN2bGAqY5rBtcPgLgMGtZi+f5RK+E5aUmxNkFa14yrfJeMC+xJdj+anPvpYN2+h32LJ6gwVcpTepeDgwhTV/tf7VWl66nngQvqHVb1HlmuxD+e6VAoalXXMY0qN3H5tEGeCmgvxcKTGgHR6EMTZJCpiSlG6iFaSzRbkyXCxvHbm8QgESAoYdraYTvsQn83GWJZzBJfnp2+uuhOdJh1rAmipnzMIA+jYCxs71Ei/x+ZNFeMysGZKxjHeh1Vg0NspECZgKiUStGFnXwh/JwuNkCHZiLrYCi7RiZ4EB+xknqS/x2UtdWH3sTiNg0Z6PCBgYTNIXbfcmhgJmMr5/cJ9R/OFAvlD7SD6KLvo7eUzA2Cq8l1mO3+0PYCWDH+QH3eN3YKOG987id/6Rm7geYhupopbjGy0w55cC2uErG5ybwhJThbkz5LHBZ9qkfEYxqJf6SbyxfSAFzFSU+sMmHhnEBQwt8Af0pbL9TwkYWvaRlQHp0BW5RHh7+IxC6C7Fb/NHO8ku24bCDwoUHQoYmZtAJtViApb4jCLpUKIdcacpnOU3Min0vdwIGDsXgMHCro9+RsGskWQ842FEe6PR2CgTJRD/jIIGwci6s4OdxSb5lN6IC4Btxc7SdNDPcmIFjMHFPqNg9eHLjsG37BYy2vMmOaoMSc12b2IoQMucgP2dkOpL32Dx3neAdj4OXT3q571/Bj/2S6Gxb3Jf2bk/tTzSS+KHMqS2/Esl4RDHzxcwbK1KwXcLGO18xj2/ZvhXIB8HZH9gela0/9rO/YME7FsZYuvpFjoO31KhnP/ko+vvFjDa+Ti4z5F9wb+CTFX2hD8OXJ1+Ref+OQJGht/ktbHBfa3g4ODg4ODg4ODg4ODg4ODg4ODg8B/hPfV4VVt/3yF2AAAAAElFTkSuQmCC';


        $pdf = PDFGenerator::initialize();
        $border = 0;

        $pdf->addPage('L', 'A4');
        $image = base64_decode($bg);

        $pdf->Image(
            '@' . $image, // file
            10, // x
            5, // y
            275, // w
            '' // h
        );

        $pdf->SetFont('freesans', 'B', 10);
        $pdf->MultiCell(40, 5, $this->local_id, $border, 'L', false, 1, 142, 17,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );



        $pdf->SetFont('freesans', '', 10);

        $pdf->MultiCell(20, 5, date('Y-m-d'), $border, 'L', false, 1, 195, 17,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );


        $pdf->MultiCell(150, 5, 'KAAB Joanna Kocoń', $border, 'L', false, 1, 28, 25,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $text = '';
        $text .= $this->sender_name ? $this->sender_name."\r\n" : '';
        $text .= $this->sender_company ? $this->sender_company."\r\n" : '';
        $text .= $this->sender_address1 ? $this->sender_address1."\r\n" : '';
        $text .= $this->sender_address2 ? $this->sender_address2."\r\n" : '';
        $text .= $this->sender_zip_code.' '.$this->sender_city.', ';
        $text .= $this->sender_country;

        $pdf->MultiCell(60, 13, $text, $border, 'L', false, 1, 13, 34,
            true,
            0,
            false,
            true,
            13,
            'T',
            true
        );


        $pdf->MultiCell(60, 13, $text, $border, 'L', false, 1, 79, 34,
            true,
            0,
            false,
            true,
            13,
            'T',
            true
        );

        $pdf->MultiCell(18, 5, date('Y-m-d'), $border, 'L', false, 1, 178, 29.5,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );


        $pdf->MultiCell(18, 5, S_Useful::workDaysNextDate(date('Y-m-d'),1), $border, 'L', false, 1, 21, 64,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );


        $text = '';
        $text .= $this->receiver_name ? $this->receiver_name."\r\n" : '';
        $text .= $this->receiver_company ? $this->receiver_company."\r\n" : '';
        $text .= $this->receiver_address1 ? $this->receiver_address1."\r\n" : '';
        $text .= $this->receiver_address2 ? $this->receiver_address2."\r\n" : '';
        $text .= $this->receiver_zip_code.' '.$this->receiver_city."\r\n";
        $text .= $this->receiver_country;

        $pdf->MultiCell(39, 20, $text, $border, 'L', false, 1, 40, 64,
            true,
            0,
            false,
            true,
            20,
            'T',
            true
        );


        $text = $ttNumber."\r\n";
        $text .= 'Folia zwykł.+taśma zwykł.';
        $pdf->MultiCell(35, 20, $text, $border, 'C', false, 1, 83, 64,
            true,
            0,
            false,
            true,
            20,
            'T',
            true
        );

        $pdf->MultiCell(12, 10, 1, $border, 'C', false, 1, 120, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        $pdf->MultiCell(12, 4, 1, $border, 'C', false, 1, 120, 88.5,
            true,
            0,
            false,
            true,
            4,
            'T',
            true
        );

        $pdf->MultiCell(13, 10, $this->package_weight, $border, 'C', false, 1, 133, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        $pdf->MultiCell(13, 4, $this->package_weight, $border, 'C', false, 1, 133, 88.5,
            true,
            0,
            false,
            true,
            4,
            'T',
            true
        );

        // MP:
        $pdf->MultiCell(12, 10, 1, $border, 'C', false, 1, 146, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        // MP:
        $pdf->MultiCell(12, 4, 1, $border, 'C', false, 1, 146, 88.5,
            true,
            0,
            false,
            true,
            4,
            'T',
            true
        );

        // M3:
        $pdf->MultiCell(12, 10, round(($this->package_size_l/100) * ($this->package_size_d/100) * ($this->package_size_w/100),2), $border, 'C', false, 1, 159, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        // M3:
        $pdf->MultiCell(12, 4, round(($this->package_size_l/100) * ($this->package_size_d/100) * ($this->package_size_w/100),2), $border, 'C', false, 1, 159, 88.5,
            true,
            0,
            false,
            true,
            4,
            'T',
            true
        );

        $pdf->MultiCell(12, 10, '', $border, 'L', false, 1, 172, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        $pdf->MultiCell(12, 4, '0', $border, 'L', false, 1, 172, 88.5,
            true,
            0,
            false,
            true,
            4,
            'T',
            true
        );

        $pdf->MultiCell(13, 10, '', $border, 'L', false, 1, 185, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        $pdf->MultiCell(15, 10, '', $border, 'L', false, 1, 198, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        //PROJEKT
        $pdf->MultiCell(15, 10, '', $border, 'L', false, 1, 214, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );


        $text = [];
        if($this->rop)
            $text[] = 'ROP';

        if($this->rod)
            $text[] = 'ROD';

        if($this->cod)
            $text[] = 'COD';

        if($this->opt_advise)
            $text[] = 'ADVISE';

        if($this->opt_delivery_8)
            $text[] = 'ND8';

        if($this->opt_delivery_12)
            $text[] = 'ND12';

        if($this->opt_delivery_next_day)
            $text[] = 'NDG';

        if($this->opt_delivery_night)
            $text[] = 'nd';

        if($this->opt_delivery_10)
            $text[] = 'ND10';

        if($this->opt_delivery_sat)
            $text[] = 'sob';

        if($this->opt_delivery_with_lift)
            $text[] = 'TLD';

        if($this->opt_isms)
            $text[] = 'ISMS';

        if($this->opt_manual_loading)
            $text[] = 'ZAL';

        if($this->opt_special_delivery)
            $text[] = 'sd';

        $text = implode(', ', $text);

        //SERWISY
        $pdf->MultiCell(20, 10, $text, $border, 'L', false, 1, 230, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        // REF
        $pdf->MultiCell(18, 10, '', $border, 'L', false, 1, 252, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        // SENT
        $pdf->MultiCell(11, 10, '', $border, 'L', false, 1, 272, 64,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        $resp = $pdf->Output('temp.pdf', 'S');

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($resp);
        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $im->stripImage();

        return [$resp, $im->getImageBlob()];
    }

}