$(document).ready(function(){
    var $button = $('[address-data-switch]');

    $button.parent().css('position', 'relative');
    $button.on('click', function(){
        var prefix = $(this).attr('address-data-switch');

        var senderData = $('input[id^=' + prefix + '_sender_]');
        var receiverData = $('input[id^=' + prefix + '_receiver_]').clone();

        senderData.each(function(i,v)
        {
            var id = $(v).attr('id');
            id = id.replace('sender', 'receiver');
            $('#' + id).val($(v).val()).trigger('change');
        });

        receiverData.each(function(i,v)
        {
            var id = $(v).attr('id');
            id = id.replace('receiver', 'sender');
            $('#' + id).val($(v).val()).trigger('change');
        });
    });
    $button.show();
});