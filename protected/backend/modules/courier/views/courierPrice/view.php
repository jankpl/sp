<?php
/* @var $model CourierCountryGroup */
?>

<?php
$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
);
?>

<h1>Pricing for country group: <?php echo GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
foreach (Yii::app()->user->getFlashes() as
         $key => $message) {
    echo '<div class="flash-' . $key . '">' .
        $message . '</div>';
}
?>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'name',
        'date_entered',
        'date_updated',
        [
            'name' => 'Countries',
            'value' => implode(',', CHtml::listData($model->countryLists, 'name', 'name')),
        ]
    ),
)); ?>
<br/>


<div style="overflow: auto;">

    <div style="width: 49%; float: left;">
        <h2>Pickup Price</h2>


        <?php
        if($model->isAvailableForPickup()):
            ?>


            <table class="list hTop" style="width: 100%;">
                <tr>
                    <td>Regular price</td>
                    <td>Group prices</td>
                    <td>Action</td>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold;"><?php echo $model->numberOfPickupPrices()>0?'<span style="color: green;">yes</span>':'<span style="color: red;">no</span>';?></td>
                    <td><?php echo $model->numberOfPickupPrices(true);?></td>
                    <td><?php echo CHtml::link('edit', array('/courier/courierPrice/pickupPrice', 'id' => $model->id), array('class' => 'likeButton')); ?></td>
                </tr>
            </table>

        <?php
        else:
            ?>
            <div class="flash-notice">
                Edit group and set pickup hub and pickup operator first!
            </div>
        <?php
        endif;
        ?>
    </div> <div style="width: 49%; float: right;">

        <h2>Delivery Price</h2>
        <table class="list hTop" style="width: 100%;">
            <tr>
                <td>Pickup Hub (package's source hub)</td>
                <td>Regular price</td>
                <td>Group prices</td>
                <td>Action</td>
            </tr>

            <?php
            /* @var $item CourierHub */
            foreach(CourierHub::listHubs() AS $item):
                ?>
                <tr>
                    <td><?php echo $item->getNameWithId();?></td>
                    <td style="text-align: center; font-weight: bold;"><?php echo $model->numberOfDeliveryPricesForHub($item->id)>0?'<span style="color: green;">yes</span>':'<span style="color: red;">no</span>';?></td>
                    <td><?php echo $model->numberOfDeliveryPricesForHub($item->id, true);?></td>
                    <td><?php echo CHtml::link('edit', array('/courier/courierPrice/deliveryPrice', 'id' => $model->id, 'id2' => $item->id), array('class' => 'likeButton')); ?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </table>
    </div>

</div>

<br/>
<br/>
<div class="alert alert-info">
    Prices consists of 2 parts: <strong>PICKUP</strong> price and <strong>DELIVERY</strong> price.<br/><br/>
    <strong>PRICE</strong> = <strong>PICKUP</strong> price + <strong>DELIVERY</strong> price<br/><br/>
    <strong>PICKUP</strong> price is omited for packages <strong>without pickup</strong>.<br/><br/>
    <strong>PICKUP</strong> price is <strong>single</strong> for each country.<br/>
    <strong>DELIVERY</strong> price is <strong>individual</strong> for <strong>each HUB</strong> that package originates from for each country<br/><br/><br/>
    <strong>Example:</strong><br/>Price for packages with pickup from <strong>PL</strong> to <strong>DE</strong>:<br/><br/>
    <strong>PL</strong> pickup HUB is "<strong>HUB PL</strong>"<br/><br/>
    Pickup price = Pickup price set in <strong>PL</strong> settings.<br/>
    Delivery price = Delivery price set in <strong>DE</strong> setting for <strong>HUB PL</strong>
</div>
