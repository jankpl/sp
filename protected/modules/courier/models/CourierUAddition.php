<?php

Yii::import('application.modules.courier.models._base.BaseCourierUAddition');

class CourierUAddition extends BaseCourierUAddition
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(

            array('name, description, price, courier_id, courier_u_addition_list_id', 'required'),
            array('price', 'numerical'),
            array('courier_u_addition_list_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max'=>256),
            array('courier_id', 'length', 'max'=>10),
            array('id, name, description, price, courier_id', 'safe', 'on'=>'search'),
        );
    }


    public function getClientTitle()
    {
        return $this->name;
    }

    public function getClientDesc()
    {
        return $this->description;
    }
}