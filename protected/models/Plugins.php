<?php

Yii::import('application.models._base.BasePlugins');

class Plugins extends BasePlugins
{
    const IMG_WIDTH = 300;
    const IMG_HEIGHT = 300;

    private $_transaction;

    public $__img_temp;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            )
        );
    }

    public function rules() {
        return array(
            array('name, stat', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>128),
            array('img', 'length', 'max'=>256),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, date_updated, stat, img', 'safe', 'on'=>'search'),
        );
    }

    public function saveWithTr($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        if(!$this->save())
            $errors = true;

        foreach($this->pluginsTrs AS $item)
        {
            $item->plugins_id = $this->id;
            if(!$item->save()) {
                $errors = true;
            }
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

    public function toggleStat()
    {
        $this->stat = abs($this->stat - 1);
        return $this->save();
    }

    public function afterFind()
    {
        if($this->img)
        {
            $img = @file_get_contents(self::getImgPath().$this->img);
            if($img)
            {
                $this->__img_temp = 'data:image/x-png;base64,'.base64_encode($img);
            }
        }

        parent::afterFind();
    }

    protected static function getImgPath()
    {
        $path = Yii::app()->getBasePath().DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'upcommon'.DIRECTORY_SEPARATOR.'/plugins'.DIRECTORY_SEPARATOR;

        return $path;
    }

    public function beforeSave()
    {
        $this->_transaction = Yii::app()->db->beginTransaction();


        if($this->__img_temp)
        {
            $imagick = ImagickMine::newInstance();

            $img = explode(',', $this->__img_temp);
            $img = $img[1];
            $img = base64_decode($img);

            if($img)
            {
                $imagick->readImageBlob($img);
                if($imagick->valid())
                {
                    $fileName = md5(microtime(true));
                    $fullPath = self::getImgPath().$fileName.'.png';
                    @file_put_contents($fullPath, $img);

                    $this->img = $fileName.'.png';
                }
            }
        }

        return parent::beforeSave();
    }

    public function afterSave()
    {

        $this->_transaction->commit();

        return parent::afterSave();
    }

    public function setAttributesAll(array $data)
    {
        foreach($data AS $key => $item)
        {
            try {
                $this->$key = $item;
            } catch (Exception $ex) {
            }
        }
    }

    public function getImgUrl()
    {
        if($this->img)
            return Yii::app()->baseUrl.'/upcommon/plugins/'.$this->img;
        else
            return false;
    }

}