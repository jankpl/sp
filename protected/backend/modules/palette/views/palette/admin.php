<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);


?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'palette-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
            'headerHtmlOptions' => array('style' => 'width: 30px;'),
        ),
        array(
            'name' => 'date_entered',
            'value' => 'substr($data->date_entered,0,-3)',
            'headerHtmlOptions' => array('style' => 'width: 100px;'),
        ),
        array(
            'name' => 'date_updated',
            'value' => 'substr($data->date_updated,0,-3)',
            'headerHtmlOptions' => array('style' => 'width: 100px;'),
        ),
        array(
            'name'=>'__sender_country_id',
            'header'=>'Sender country',
            'value'=>'$data->senderAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name'=>'__receiver_country_id',
            'header'=>'Receiver country',
            'value'=>'$data->receiverAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name'=>'type',
            'value'=>'Palette::types()[$data->type]',
            'filter'=>Palette::types(),

        ),
        array(
            'name'=>'notes',
            'value'=>'$data->notes==""?"-":"&check;"',
            'filter'=> false,
            'headerHtmlOptions' => array('style' => 'width: 70px;'),
            'type' => 'raw',
        ),
        array(
            'name'=>'__stat',
            'value'=>'Palette::stat()[$data->stat]',
            'filter'=>Palette::stat(),
            'headerHtmlOptions' => array('style' => 'width: 70px;'),
        ),
        array(
            'name'=>'__user_login',
            'value'=>'CHtml::link($data->user->login, array("/user/view", "id" => $data->user->id), array("target" => "_blank"))',
            'type' => 'raw',

        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
        ),
    ),
)); ?>