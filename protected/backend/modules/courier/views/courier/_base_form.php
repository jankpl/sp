<?php echo $form->errorSummary($model); ?>

<hr/>

<h3>Dane adresowe</h3>

<div style="overflow: auto;" class="inline-form">
    <div style="float: left; width: 45%;">
        <h4>Nadawca (opcjonalnie)</h4>
        <?php

//        $listAddressData = AddressData::loadDistinctData(array('email' => 'email'));

        $this->widget('application.backend.components.AddressDataFrom', array(
            'addressData' => $model->senderAddressData,
//            'listAddressData' => $listAddressData,
            'form' => $form,
            'noEmail' => true,
            'groupName' => 'sender',
        ));

        ?>
    </div>
    <div style="float: right; width: 45%;">
        <h4>Odbiorca</h4>
        <?php


        $this->widget('application.backend.components.AddressDataFrom', array(
            'addressData' => $model->receiverAddressData,
//            'listAddressData' => $listAddressData,
            'form' => $form,
            'noEmail' => true,
            'groupName' => 'receiver',
        ));

        ?>
    </div>
</div>

<hr/>

<style>
    .inline-form label
    {
        display: inline-block !important;
        width: 250px;
    }
</style>

<h2>Package data</h2>

<div class="inline-form">

    <div class="row">
        <?php echo $form->labelEx($model,'local_id'); ?>
        <?php echo $form->textField($model, 'local_id', array('maxlength' => 13)); ?>
        <?php echo $form->error($model,'local_id'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'courier_stat_id'); ?>
        <?php echo $form->dropDownList($model, 'courier_stat_id', CHtml::listData(CourierStat::model()->findAll(),'id','name')); ?>
        <?php echo $form->error($model,'courier_stat_id'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'_first_status_date'); ?>
        <?php
        $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
            'model'=>$model,
            'attribute'=>'_first_status_date',
            'options'=>array(
                'hourGrid' => 4,
                'hourMin' => 0,
                'hourMax' => 23,
                'showSecond'=>true,
                'timeFormat' => 'hh:mm:ss',
                'changeMonth' => true,
                'changeYear' => true,
                'dateFormat' => 'yy-mm-dd',
            ),
        ));
        ?>
        <?php echo $form->error($model,'_first_status_date'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'package_weight'); ?>
        <?php echo $form->textField($model, 'package_weight'); ?>
        <?php echo $form->error($model,'package_weight'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'package_size_l'); ?>
        <?php echo $form->textField($model, 'package_size_l'); ?>
        <?php echo $form->error($model,'package_size_l'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'package_size_w'); ?>
        <?php echo $form->textField($model, 'package_size_w'); ?>
        <?php echo $form->error($model,'package_size_w'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'package_size_d'); ?>
        <?php echo $form->textField($model, 'package_size_d'); ?>
        <?php echo $form->error($model,'package_size_d'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'packages_number'); ?>
        <?php echo $form->dropDownList($model, 'packages_number', S_Useful::generateArrayOfItemsNumber(1,100)); ?>
        <?php echo $form->error($model,'packages_number'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'package_value'); ?>
        <?php echo $form->textField($model, 'package_value'); ?>
        <?php echo $form->error($model,'package_value'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'package_content'); ?>
        <?php echo $form->textField($model, 'package_content', array('maxlength' => 256)); ?>
        <?php echo $form->error($model,'package_content'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'cod'); ?>
        <?php echo $form->textField($model, 'cod', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'cod'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->dropDownList($model, 'user_id', CHtml::listData(User::model()->findAll(),'id','login'), array('empty' => '--select user--')); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div><!-- row -->
</div>