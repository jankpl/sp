<?php

class PostalTtCommand extends ConsoleCommand {

    const PACKAGE_DAYS_IGNORE = 45; // for how many days check statuses

//    const PACKAGE_DAYS_IGNORE_INTERNAL_RETURN = 62; // for how many days check statuses of Internal Return packages

    const PACKAGE_DAYS_FRESH = 4; // for how many days package is considered as fresh

    const TTLOG_BLOCK_MINUTES = 720; // how long to waint between checks for not-fresh packages
    const TTLOG_BLOCK_MINUTES_FRESH = 360; // how long to wait between checks for fresh packages
    const DELAY_NEW_MINUTES = 360; // how long to wait before checking TT for new packages

    const STRIP_SIZE = 50;

//    const STRIP_SIZE_EXPIRE_LABELS = 500;
//    const STRIP_SIZE_ARCHIVE_HISTORY = 5000;

    const TYPE_MAIN = 'main';
    const TYPE_OLDER = 'older';
    const TYPE_CANCELLED = 'cancelled';

    const INSTANCES_MAIN = 10;
    const INSTANCES_OLDER = 6;
    const INSTANCES_CANCELLED = 4;

    const TIMEOUT = 900;

    public function init()
    {
        Yii::app()->getModule('postal');
        parent::init();
    }

    public function filters()
    {
        return array();
    }

    protected function getIngnoredStats()
    {
        $_ingoredStats = [
            PostalStat::CANCELLED,
            PostalStat::CANCELLED_PROBLEM,
            PostalStat::NEW_ORDER,
            PostalStat::PACKAGE_PROCESSING
        ];

        $data = array_merge($_ingoredStats, PostalStat::getKnownDeliveredStatuses());
        return $data;
    }

    function actionIndex()
    {
        if (date('G') <= 23 && date('G') >= 5)
            for ($i = 1; $i <= self::INSTANCES_MAIN; $i++)
                exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic postalTt run --type=".self::TYPE_MAIN." --mod=".$i." > /dev/null &");


        if (date('G') <= 23 && date('G') >= 5)
            for ($i = 1; $i <= self::INSTANCES_OLDER; $i++)
                exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic postalTt run --type=".self::TYPE_OLDER." --mod=".$i." > /dev/null &");


        if (date('G') > 23 OR date('G') < 5)
            for ($i = 1; $i <= self::INSTANCES_CANCELLED; $i++)
                exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic postalTt run --type=".self::TYPE_CANCELLED." --mod=".$i." > /dev/null &");
    }

    protected function _modelsMain($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', $this->getIngnoredStats());

        $sql = "SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (postal.date_updated > (NOW() - INTERVAL :past_date_fresh DAY) OR postal.date_entered > (NOW() - INTERVAL :past_date_fresh DAY))
AND (postal.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND postal_stat_id NOT IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
AND stat_map_id != :stat_map AND stat_map_id != :stat_map2
AND (postal.id MOD ".$instances.") IN ($mods)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models =  Postal::model()->with('postalLabel')
            ->findAllBySql($sql,
                [
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES_FRESH,
                    ':type' => TtLog::TYPE_POSTAL,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':stat_map' => StatMap::MAP_DELIVERED,
                    ':stat_map2' => StatMap::MAP_CANCELLED,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);


        return $models;
    }

    protected function _modelsCancelled($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', $this->getIngnoredStats());

        $sql = "SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (postal.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR postal.date_entered > (NOW() - INTERVAL :past_date2 DAY))
AND postal_stat_id IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 24 HOUR))
AND postal.stat_map_id = :ignored_map AND postal.postal_stat_id != :canceled_problem
AND (postal.id MOD ".$instances.") IN ($mods)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models =  Postal::model()->with('postalLabel')
            ->findAllBySql($sql,
                [
                    ':past_date2' => self::PACKAGE_DAYS_IGNORE,
//                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':type' => TtLog::TYPE_POSTAL,
//                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_CANCELLED,
                    ':canceled_problem' => PostalStat::CANCELLED_PROBLEM,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);

        return $models;
    }

    protected function _modelsMainSlower($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', $this->getIngnoredStats());
        $sql = "SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (
(postal.date_updated >= (NOW() - INTERVAL :past_date DAY) AND postal.date_updated < (NOW() - INTERVAL :past_date_fresh DAY)) OR (postal.date_updated IS NULL AND postal.date_entered >= (NOW() - INTERVAL :past_date DAY) AND postal.date_entered < (NOW() - INTERVAL :past_date_fresh DAY))
	)
AND postal_stat_id NOT IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
AND stat_map_id != :stat_map AND stat_map_id != :stat_map2
AND (postal.id MOD ".$instances.") IN ($mods)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models =  Postal::model()->with('postalLabel')
            ->findAllBySql($sql,
                [
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':past_date' => self::PACKAGE_DAYS_IGNORE,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':type' => TtLog::TYPE_POSTAL,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':stat_map' => StatMap::MAP_DELIVERED,
                    ':stat_map2' => StatMap::MAP_CANCELLED,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);

        return $models;
    }

    protected function _modelsOld($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', $this->getIngnoredStats());

        $sql = "SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (
(postal.date_updated >= (NOW() - INTERVAL :past_date_old MONTH) AND postal.date_updated < (NOW() - INTERVAL :past_date DAY)) OR (postal.date_updated IS NULL AND postal.date_entered >= (NOW() - INTERVAL :past_date MONTH) AND postal.date_entered < (NOW() - INTERVAL :past_date DAY))
	)
AND postal_stat_id NOT IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 7 DAY))
AND stat_map_id != :stat_map AND stat_map_id != :stat_map2
AND (postal.id MOD ".$instances.") IN ($mods)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models =  Postal::model()->with('postalLabel')
            ->findAllBySql($sql,
                [
                    ':past_date_old' => 6,
                    ':past_date' => 62,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':type' => TtLog::TYPE_POSTAL,
//                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':stat_map' => StatMap::MAP_DELIVERED,
                    ':stat_map2' => StatMap::MAP_CANCELLED,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);

        return $models;
    }



    // update courier packages statuses with external statuses
    public function actionRun($type, $mod = 0, $nextStep = false)
    {

        echo 'START';
        echo "\r\n";
        echo 'TYPE: ' . $type;
        echo "\r\n";
        echo 'MOD: ' . $mod;
        echo "\r\n";
        echo 'NEXT STEP: ' . ($nextStep ? 'T' : 'N');
        echo "\r\n";
        echo "\r\n";

        $filename = 'cmd_postalTt_log_' . $type.'.txt';

        $start_time = time();
        $limit_90 = 0.9 * self::TIMEOUT;

        $CACHE_NAME = 'CMD_POSTAL_TT' . $type.'_'.$mod;
        $CACHE = Yii::app()->cacheFile;

        if (!$nextStep && $CACHE->get($CACHE_NAME)) {
            echo 'Already in progress!';
            Yii::app()->end();
        }
        $CACHE->set($CACHE_NAME, 10, $limit_90);

        MyDump::dump('cmd_postalTt.txt', 'stat');

        if ((date('G') > 23 OR date('G') < 5) && in_array($type, [self::TYPE_MAIN, self::TYPE_OLDER]))
            exit;

        if ((date('G') >= 5 AND date('G') <= 23) && in_array($type, [self::TYPE_CANCELLED]))
            exit;

        if (!$mod)
            exit;

        $start = date('Y-m-d H:i:s');

        $j = 0;
        while (S_SystemLoad::isHigh())
        {
            echo 'HIGH LOAD! Sleep for a while...'."\r\n";
            sleep(60);

            $j++;
            if($j > 10)
            {
                echo 'STILL HIGH LOAD! GOODBYE!'."\r\n";
                MyDump::dump($filename, "\r\n".$start." : [$mod] : HIGH LOAD");
                $CACHE->delete($CACHE_NAME);
                Yii::app()->end();
            }
        }
        echo 'Here we go...'."\r\n";


        if($type == self::TYPE_MAIN)
            $postals = self::_modelsMain(self::INSTANCES_MAIN, $mod);
        else if($type == self::TYPE_OLDER)
            $postals = self::_modelsMainSlower(self::INSTANCES_OLDER, $mod);
        else if($type == self::TYPE_CANCELLED)
            $postals = self::_modelsCancelled(self::INSTANCES_CANCELLED, $mod);
        else
            throw new Exception('Unknowny type!');

        $log = $start.' ['.print_r($mod,1).'] : --- '. sprintf("%03d",S_Useful::sizeof($postals))." --- ";
        MyDump::dump('cmd_postalTt_full_log_divided.txt', 'START : '.print_r($type,1).' : '.print_r($mod,1));
        echo 'TO PROCESS '.sizeof($postals)."\r\n";
        try {
            /* $item Postal */
            if(is_array($postals))
                foreach($postals AS $item)
                {
                    echo 'Process: '.$item->local_id."\r\n";
                    PostalExternalManager::updateStatusForPackage($item);
                    MyDump::dump('cmd_postalTt_full_log_divided.txt', $item->id.' : '.print_r($type,1).' : '.print_r($mod,1));
                    if(time() - $start_time > $limit_90)
                    {
                        MyDump::dump($filename, 'TIMEOUTED : '.print_r($type,1).' : '.print_r($mod,1));
                        $CACHE->set($CACHE_NAME, false);
                        Yii::app()->end();
                    }
                }

        }
        catch(Exception $ex)
        {
            var_dump($ex);
            MyDump::dump('cjp_cmd_exception.txt', print_r($ex,1));
            Yii::log('POSTAL TT CMD LOG: '.print_r($ex,1), CLogger::LEVEL_ERROR);
            $CACHE->set($CACHE_NAME, false);
            Yii::app()->end();
        }


        // there is propably more
        if(S_Useful::sizeof($postals) == self::STRIP_SIZE)
        {
            MyDump::dump($filename, $log);
            $this->actionRun($type, $mod , true);
        } else {
            MyDump::dump($filename, $log);

            Yii::app()->cache->delete($CACHE_NAME);
            Yii::app()->end();
        }

    }





}