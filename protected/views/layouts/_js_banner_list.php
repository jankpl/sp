<?php

switch(Yii::app()->params['language'])
{
    case 1:
        $lang = 'pl';
        break;
    case 2:
        $lang = 'en';
        break;
    default:
        $lang = 'pl';
        break;
}

$counter = 0;
$jsArray = '';
$imagesArray = Array();
$files = @scandir(Yii::app()->basePath.'/../images/priceBanners/'.$lang);


if(is_array($files))
    foreach($files AS $fileItem)
    {
        if ($fileItem != "." && $fileItem != ".." && !preg_match('/.*_g\..*$/', $fileItem)) {
            array_push($imagesArray, Yii::app()->baseUrl.'/images/priceBanners/'.$lang.'/'.$fileItem);
            $counter++;
        }
    }
shuffle($imagesArray);

foreach($imagesArray AS $imageItem) $jsArray.= '\''.$imageItem.'\',';
$jsArray = substr($jsArray,0,strlen($jsArray)-1);

if(is_array($files) && S_Useful::sizeof($files) > 0):
    ?>

    $(document).ready(function(){
    var imagesList = Array(<?php echo $jsArray;?>);
   showSlides(imagesList );
 });


<?php
endif;
?>