<?php
/* @var $form TbActiveForm */
/* @var $model AddressData */
/* @var $i Integer */
/* @var $noEmail boolean */
/* @var $type string */


?>


<?php //echo $form->errorSummary($model); ?>


<?php
AddressDataSuggester::registerAssets();

if($countries===NULL)
    $dataSuggesterContent = S_Useful::listDataForAutocomplete(CountryList::model()->cache(60*60)->with('countryListTr')->with('countryListTrs')->findAll(),'id','trName', 'namesForSuggester', '', false, false);
else
    $dataSuggesterContent = S_Useful::listDataForAutocomplete($countries,'id','trName', 'namesForSuggester', '', false, false);

if($model->country_id == '')
    $model->country_id = CountryList::COUNTRY_PL;

$country_name = $dataSuggesterContent[$model->country_id];
if(is_array($country_name))
    $country_name = $country_name['label'];

$dataSuggesterContent = CJSON::encode($dataSuggesterContent);

?>

<?php

if(1):
//if(S_Useful::sizeof($listAddressData)):

    ?>

    <?php
    if(!$blockAddressSuggester)
        $this->widget('AddressDataSuggester',
            array(
                'type' => $i == '' ? $type : $i,
                'fieldName' => 'CourierTypeReturn_AddressData',
                'i' => $i,
            ));
    ?>


<?php

endif;
$array = [];

?>
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'country_id'); ?>
        <?php echo TbHtml::textField('country_label_'.$i,$country_name, array(
            'data-autocomplete-cl' => true,
            'data-real-val' => (isset($i)?'['.$i.']':'').'country_id_val',
            'data-content' => $dataSuggesterContent,
            'data-autocomplete-type' => $i,
            'style' => 'text-align: center; font-weight: bold;'
        ));?>
        <?php echo $form->hiddenField($model,(isset($i)?'['.$i.']':'').'country_id', array(
            'data-autocomplete-id' => (isset($i)?'['.$i.']':'').'country_id_val',
            'data-country' => $i,
        )); ?>
        <?= CHtml::hiddenField('country_suggester_trigger','',[
            'data-suggester-type' => (isset($i)?'['.$i.']':'').'country_id_val',
            'data-trigger-type' => $i,
            'id' => 'country_suggester_trigger_'.$i,
        ]); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'country_id'); ?>
    </div>
</div><!-- row -->
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'company'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'company', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'company'); ?>
    </div>
</div><!-- row -->
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'name'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'name', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'autofocus' => $focusFirst?'autofocus':'')); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'name'); ?>
    </div>
</div><!-- row -->
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_1', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?> <?= $model->address_line_2 != '' ? '' : '<span class="glyphicon glyphicon-plus" data-show-address-more="true" style="cursor: pointer;" tabindex="0"></span>';?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
    </div>
</div><!-- row -->
<div class="row" style="<?= $model->address_line_2 != '' ? '' : 'display: none;';?>" data-address-more="true">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_2', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'data-address-line-2' => true)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
    </div>
</div><!-- row -->
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'zip_code', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'data-dependency' => 'true', 'data-zip-code' => $i)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
    </div>
</div><!-- row -->
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'city'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'city', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'data-city' => $i)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'city'); ?>
    </div>
</div><!-- row -->
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'tel'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'tel', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <div style="display: inline-block; background: #eeeeee; border: 1px solid #cccccc; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height: 28px; line-height: 26px; padding: 0 5px; margin-left: -5px;"><span rel="tooltip" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-html="true" data-title="<?= Yii::t('courier', 'Please provide number with country prefix (not required for polish numbers)');?>" style="cursor: help;" class="glyphicon glyphicon-info-sign"></span></div>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'tel'); ?>
    </div>
</div><!-- row -->
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'email'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'email', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'style' => 'border-top-right-radius: 0; border-bottom-right-radius: 0;')); ?>
        <div style="display: inline-block; background: #eeeeee; border: 1px solid #cccccc; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height: 28px; line-height: 26px; padding: 0 5px; margin-left: -5px;">@</div>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'email'); ?>
    </div>
</div><!-- row -->

<?php if(!Yii::app()->user->isGuest && Yii::app()->user->getModel()->hasUserAvailableSmsOptions()): ?>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-12">
            <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'smsNotification'); ?>
            <?php echo $form->checkBox($model,(isset($i)?'['.$i.']':'').'smsNotification', ['data-bootstrap-checkbox' => true, 'data-default-class' => 'btn-sm', 'data-on-class' => 'btn-sm btn-primary', 'data-off-class' => 'btn-sm btn-default', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak')]); ?>
            <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'smsNotification'); ?>
        </div>
    </div><!-- row -->
<?php endif; ?>
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'addToContactBook'); ?>
        <?php echo $form->checkBox($model,(isset($i)?'['.$i.']':'').'addToContactBook', ['data-bootstrap-checkbox' => true, 'data-default-class' => 'btn-sm', 'data-on-class' => 'btn-sm btn-primary', 'data-off-class' => 'btn-sm btn-default', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak')]); ?>
        <?php echo $form->hiddenField($model,(isset($i)?'['.$i.']':'').'contactBookType'); ?>
    </div>
</div><!-- row -->