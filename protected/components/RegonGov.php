<?php

use GusApi\Exception\InvalidUserKeyException;
use GusApi\GusApi;
use GusApi\ReportTypes;

class RegonGov
{
    const KEY = 'a8067be8981741e0916f';

    public $typ;
    public $nip;
    public $regon;
    public $krs;

    public static function getDataByNip($nip, $asAddressDataAttributes = false)
    {
        return self::getData($nip, NULL, NULL, $asAddressDataAttributes);
    }

    public static function getDataByKrs($krs, $asAddressDataAttributes)
    {
        return self::getData(NULL, NULL, $krs, $asAddressDataAttributes);
    }

    public static function getDataByRegon($regon, $asAddressDataAttributes)
    {
        return self::getData(NULL, $regon, NULL, $asAddressDataAttributes);
    }

    protected static function getData($nip, $regon, $krs, $asAddressDataAttributes = false)
    {
        $nip = preg_replace('/([^0-9])/', '', $nip);

        $model = new self;

        $model->nip = $nip;
        $model->regon = $regon;
        $model->krs = $krs;

        $CACHE = Yii::app()->cache;
        $CACHE_NAME = 'REGON_CHECK_' . $nip.$regon.$krs. '_'.print_r($asAddressDataAttributes,1);

        $data = $CACHE->get($CACHE_NAME);

        $data = false;

        if ($data === false) {
            try {
                $data = $model->_fetchData();
                $CACHE->set($CACHE_NAME, $data, 60 * 60);

                if($data && $asAddressDataAttributes)
                    $data = $model->mapToAddressDataAttributes($data);
            } catch(Exception $ex) {
                $data = NULL;
            }
        }

        return $data;
    }

    protected function mapToAddressDataAttributes(array $data)
    {
        switch($this->type)
        {
            default:
            case 'f':
                $attrPrefix = 'fiz';
                break;
            case 'p':
                $attrPrefix = 'praw';
                break;
        }

        $address = $data[$attrPrefix.'_adSiedzUlica_Nazwa'];
        $address .= ' '.$data["fiz_adSiedzNumerNieruchomosci"];
        if($data["fiz_adSiedzNumerLokalu"] != '')
            $address .= '/'.$data["fiz_adSiedzNumerLokalu"];

        $address = trim($address);

        $attributes = [
            'company' => $data[$attrPrefix.'_nazwa'],
            'regon' => $data[$attrPrefix.'_regon9'],
            'country' => $data[$attrPrefix.'_adSiedzKraj_Symbol'],
            'country_id' => CountryList::getIdByCode($data[$attrPrefix.'_adSiedzKraj_Symbol']),
            'zip_code' => $data[$attrPrefix.'_adSiedzKodPocztowy'],
            'email' => $data[$attrPrefix.'_adresEmail'],
            'city' => $data[$attrPrefix.'_adSiedzMiejscowosc_Nazwa'],
            'address_line_1' => $address,
            'tel' => $data[$attrPrefix.'_C_numerTelefonu'],
            'nip' => $this->nip ? $this->nip : $data[$attrPrefix.'_nip'],
            'regon' => $this->regon ? $this->regon : ($data[$attrPrefix.'_regon14'] != '' ? $data[$attrPrefix.'_regon14'] : $data[$attrPrefix.'_regon9']),
            'krs' => $this->krs ? $this->krs : $data[$attrPrefix.'_numerWrejestrzeEwidencji'],
        ];

        if($attributes['country'] == 'PL')
            $attributes['zip_code'] = substr_replace($attributes['zip_code'], '-', 2, 0);

        return $attributes;
    }

    protected function _fetchData()
    {
        $gus = new GusApi(self::KEY);
        //for development server use:
        //$gus = new GusApi('abcde12345abcde12345', 'dev');

        try {
            $gus->login();

            if($this->nip != '')
            $gusReports = $gus->getByNip($this->nip);
            else if($this->regon != '')
                $gusReports = $gus->getByRegon($this->regon);
            else if($this->krs != '')
                $gusReports = $gus->getByKrs($this->krs);


            MyDump::dump('regonGov.txt', $this->nip.'|'.$this->regon.'|'.$this->krs.'::'.print_r($gusReports,1));

            foreach ($gusReports as $gusReport) {

                $this->type = $gusReport->getType();
                switch($this->type)
                {
                    default:
                    case 'f':
                        $reportType = ReportTypes::REPORT_ACTIVITY_PHYSIC_CEIDG;
                        break;
                    case 'p':
                        $reportType = ReportTypes::REPORT_PUBLIC_LAW;
                        break;
                }

                $fullReport = $gus->getFullReport($gusReport, $reportType);

                MyDump::dump('regonGov.txt', print_r($fullReport,1));
                return $fullReport[0];
            }
        } catch (InvalidUserKeyException $e) {
            Yii::log('Bad user key: '.$e->getMessage(), CLogger::LEVEL_ERROR);
        } catch (\GusApi\Exception\NotFoundException $e) {
            Yii::log('No data found: '.$gus->getResultSearchMessage(), CLogger::LEVEL_ERROR);
        }

        return NULL;
    }


}



