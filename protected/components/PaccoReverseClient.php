<?php

class PaccoReverseClient extends GlobalOperatorWithReturn
{
    const URL = 'https://wsrest.sda.it/SPEDIZIONE-WS-WEB/rest/spedizioneService';
    const LOGIN = 'ST_348206';
    const PASSWORD = 'Webs_2018';

    public $sender_name;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $packages_number = 1;

    public $package_weight;
    public $package_size_l;
    public $package_size_d;
    public $package_size_w;

    public $package_content;
    public $package_value;

    public $ref;

    public $user_id;
    public $login;


    public static function getAdditions(Courier $courier)
    {
        return [];
    }

//    public static function test()
//    {
//
//        $data = [];
//
//        $data = new stdClass();
//        $data->ldv = new stdClass();
//        $data->ldv->mittente = new stdClass();
//        $data->ldv->mittente->cap = '';
//        $data->ldv->mittente->tipoAnagrafica = '';
//        $data->ldv->mittente->telefono = '';
//        $data->ldv->mittente->indirizzo = '';
//        $data->ldv->mittente->provincia = '';
//        $data->ldv->mittente->codNazione = '';
//        $data->ldv->mittente->localita = '';
//        $data->ldv->mittente->intestatario = '';
//        $data->ldv->datiSpedizione = new stdClass();
//        $data->ldv->datiSpedizione->sezioneColli = new stdClass();
//        $data->ldv->datiSpedizione->sezioneColli->colli = new stdClass();
//        $data->ldv->datiSpedizione->sezioneColli->colli->larghezza = '';
//        $data->ldv->datiSpedizione->sezioneColli->colli->peso = '';
//        $data->ldv->datiSpedizione->sezioneColli->colli->profondita = '';
//        $data->ldv->datiSpedizione->sezioneColli->colli->altezza = '';
//        $data->ldv->datiSpedizione->datiGenerali = new stdClass();
//        $data->ldv->datiSpedizione->datiGenerali->dataSpedizione = '';
//        $data->ldv->datiSpedizione->datiGenerali->note = '';
//        $data->ldv->datiSpedizione->accessori = new stdClass();
//        $data->ldv->datiSpedizione->accessori->apo = '';
//        $data->ldv->datiSpedizione->accessori->pkp = '';
//        $data->ldv->datiSpedizione->codiceServizio = '';
//        $data->ldv->destinatario = new stdClass();
//        $data->ldv->destinatario->cap = '';
//        $data->ldv->destinatario->tipoAnagrafica = '';
//        $data->ldv->destinatario->telefono = '';
//        $data->ldv->destinatario->indirizzo = '';
//        $data->ldv->destinatario->provincia = '';
//        $data->ldv->destinatario->codNazione = '';
//        $data->ldv->destinatario->localita = '';
//        $data->ldv->destinatario->intestatario = '';
//        $data->formatoStampa = 'A4';
//
//        $data = json_encode($data);
//
//        $data = '{"ldv":{"mittente":{"cap":"00185","tipoAnagrafica":"S","telefono":"987456123","indirizzo":"PIAZZA DEI CINQUECENTO 4 5","provincia":"00","codNazione":"ITA","localita":"ROMA","intestatario":"TEST WaOCHY"},"datiSpedizione":{"sezioneColli":{"colli":[{"peso":"5","larghezza":"30","profondita":"30","altezza":"20"}]},"datiGenerali":{"dataSpedizione":"26\/10\/2018","note":"483181026453560"},"accessori":{"apo":"SI","pkp":"NO"},"codiceServizio":"P51"},"destinatario":{"cap":"39100","tipoAnagrafica":"S","telefono":"","indirizzo":"Via Luigi Negrelli 15 (Rampa nr. 3)","provincia":"00","codNazione":"ITA","localita":"Bolzano","intestatario":"S\u00f9dtirol Post Srl \/ Att. Logistica"}},"formatoStampa":"A6"}';
//        $return = new stdClass();
//        $return->success = false;
//        $return->error = false;
//
//        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_URL, self::URL);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
//
//        curl_setopt($curl, CURLOPT_POST, TRUE);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
//
//        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
//
//        curl_setopt($curl, CURLOPT_USERPWD,  self::LOGIN.':'.self::PASSWORD);
//
//        $resp = curl_exec($curl);
//        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//
//
//        $resp = json_decode($resp);
//
//
//        if($resp->outcome == 'OK')
//        {
//            $return->success = true;
//            $return->data = $resp;
//        }
//        else
//        {
//            if ($resp == '')
//            {
//                $error = curl_error($curl);
//                if ($error == '')
//                    $error = 'Unknown error';
//
//                $return->error = $error;
//                $return->errorCode = '';
//                $return->errorDesc = $error;
//            }
//            else if($resp && $resp->messages)
//            {
//                $messages = $resp->messages->messages;
//
//                if(!is_array($messages))
//                    $messages = [ $messages ];
//
//
//                $error = [];
//                foreach($messages AS $message) {
//
//
//                    $error[] = $message->field . ': ' . $message->message;
//                }
//
//                $error = implode(' || ', $error);
//
//                $return->error = $error;
//                $return->errorCode = '';
//                $return->errorDesc = $error;
//            }
//        }
//
//
//        var_dump($return);
//    }

    protected function _createShipment($returnError = false)
    {
        if(!in_array($this->user_id, [User::INTEGRATIONS_TEST_USER_ID, 2036]))
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('Operator not active for this customer. Please contact us.');
            } else {
                return false;
            }

        // receiver is FIXED
        {
            $namePrefix = $this->login;

            if(in_array($this->user_id, [947, 948, 2036, 2037])) {
                $namePrefix = 'Escarpe.it';
            }

            $this->receiver_name = $namePrefix. '/ KAAB / Sùdtirol Post Srl / Att. Logistica';
            $this->receiver_address1 = 'Via Luigi Negrelli 15';
            $this->receiver_address2 = '(Rampa nr. 3)';
            $this->receiver_zip_code = '39100';
            $this->receiver_city = 'Bolzano';
            $this->receiver_country = 'IT';
            $this->receiver_country_id = CountryList::COUNTRY_IT;
            $this->receiver_mail = '';
            $this->receiver_tel = '';
        }

        $data = new stdClass();
        $data->ldv = new stdClass();
        $data->ldv->mittente = new stdClass();
        $data->ldv->mittente->cap = $this->sender_zip_code;
        $data->ldv->mittente->tipoAnagrafica = 'S';
        $data->ldv->mittente->telefono = $this->sender_tel;
        $data->ldv->mittente->indirizzo = S_Useful::removeNationalCharacters(trim($this->sender_address1.' '.$this->sender_address2));
        $data->ldv->mittente->provincia = '00';
        $data->ldv->mittente->codNazione = CountryList::codeIso2To3($this->sender_country);
        $data->ldv->mittente->localita = S_Useful::removeNationalCharacters($this->sender_city);
        $data->ldv->mittente->intestatario = S_Useful::removeNationalCharacters($this->sender_name);
        $data->ldv->datiSpedizione = new stdClass();
        $data->ldv->datiSpedizione->sezioneColli = new stdClass();
//        $data->ldv->datiSpedizione->sezioneColli->pesoLdv = $this->package_weight;
        $data->ldv->datiSpedizione->sezioneColli->colli = [];
        $dim = new stdClass();
        $dim->peso = $this->package_weight;
        $dim->larghezza = $this->package_size_l;
        $dim->profondita = $this->package_size_d;
        $dim->altezza = $this->package_size_w;

        $data->ldv->datiSpedizione->sezioneColli->colli[] = $dim;

//        $data->ldv->datiSpedizione->sezioneColli->colli->peso = $this->package_weight;
//        $data->ldv->datiSpedizione->sezioneColli->colli->larghezza = $this->package_size_l;
//        $data->ldv->datiSpedizione->sezioneColli->colli->profondita = $this->package_size_d;
//        $data->ldv->datiSpedizione->sezioneColli->colli->altezza = $this->package_size_w;
        $data->ldv->datiSpedizione->datiGenerali = new stdClass();
        $data->ldv->datiSpedizione->datiGenerali->dataSpedizione = date('d/m/Y');
//        $data->ldv->datiSpedizione->datiGenerali->mancataConsegna = 'R';
        $data->ldv->datiSpedizione->datiGenerali->note = S_Useful::removeNationalCharacters($this->ref);
        $data->ldv->datiSpedizione->accessori = new stdClass();
        $data->ldv->datiSpedizione->accessori->apo = 'SI';
        $data->ldv->datiSpedizione->accessori->pkp = 'NO';
        $data->ldv->datiSpedizione->codiceServizio = 'P51';
        $data->ldv->destinatario = new stdClass();
        $data->ldv->destinatario->cap = $this->receiver_zip_code;
        $data->ldv->destinatario->tipoAnagrafica = 'S';
        $data->ldv->destinatario->telefono = $this->receiver_tel;
        $data->ldv->destinatario->indirizzo = S_Useful::removeNationalCharacters(trim($this->receiver_address1.' '.$this->receiver_address2));
        $data->ldv->destinatario->provincia = '00';
        $data->ldv->destinatario->codNazione = CountryList::codeIso2To3($this->receiver_country);
        $data->ldv->destinatario->localita = S_Useful::removeNationalCharacters($this->receiver_city);
        $data->ldv->destinatario->intestatario = S_Useful::removeNationalCharacters($this->receiver_name);
        $data->formatoStampa = 'A6';

        $resp = $this->_curlCall($data);

        if ($resp->success == true) {
            $return = [];

            $ttNumber = (string) $resp->data->spedizioni[0]->datiSpedizione->sezioneColli->colli[0]->numero;
            $label = (string) $resp->data->documentoDiStampa;
            $label = base64_decode($label);

            $im = ImagickMine::newInstance();
            $im->setResolution(300,300);

            $im->readImageBlob($label);
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            $im->rotateImage(new ImagickPixel(), 270);
            $im->trimImage(0);
            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);

            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }

    }

    public static function orderForCourierReturn(CourierTypeReturn $courierReturn, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;

        $from = $courierReturn->courier->senderAddressData;
        $to = $courierReturn->courier->receiverAddressData;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->email;
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->email;
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierReturn->courier->getWeight(true);
        $model->package_size_l = $courierReturn->courier->package_size_l;
        $model->package_size_d = $courierReturn->courier->package_size_d;
        $model->package_size_w = $courierReturn->courier->package_size_w;

        $model->package_content = $courierReturn->courier->package_content;
        $model->package_value = $courierReturn->courier->getPackageValueConverted('GBP');
        $model->ref = $courierReturn->courier->local_id;

        $model->user_id = $courierReturn->courier->user_id;
        $model->login = $courierReturn->courier->user->login;

        return $model->_createShipment($returnErrors);
    }

    protected function _curlCall($data)
    {

        $data = json_encode($data);

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::URL);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($curl, CURLOPT_USERPWD,  self::LOGIN.':'.self::PASSWORD);

        $resp = curl_exec($curl);


        MyDump::dump('paccoReverse.txt', print_r($data,1));
        MyDump::dump('paccoReverse.txt', print_r($resp,1));

        $resp = json_decode($resp);

        if($resp->outcome == 'OK')
        {
            $return->success = true;
            $return->data = $resp;
        }
        else
        {
            if ($resp == '')
            {
                $error = curl_error($curl);
                if ($error == '')
                    $error = 'Unknown error';

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
            else if($resp && $resp->messages)
            {
                $messages = $resp->messages->messages;

                if(!is_array($messages))
                    $messages = [ $messages ];


                $error = [];
                foreach($messages AS $message) {


                    $error[] = $message->field . ': ' . $message->message;
                }

                $error = implode(' || ', $error);

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
        }

        return $return;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->email;
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->email;
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->package_value = $courierInternal->courier->getPackageValueConverted('GBP');
        $model->ref = $courierInternal->courier->local_id;

        $model->user_id = $courierInternal->courier->user_id;
        $model->login = $courierInternal->courier->user->login;

        return $model->_createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->email;
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->zip_code;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->email;
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierU->courier->getWeight(true);
        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->package_value = $courierU->courier->getPackageValueConverted('GBP');
        $model->ref = $courierU->courier->local_id;

        $model->user_id = $courierU->courier->user_id;
        $model->login = $courierU->courier->user->login;

        return $model->_createShipment($returnErrors);
    }


    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return false;

    }

}