<?php

Yii::import('application.models._base.BaseConfig');

class Config extends BaseConfig
{

    const SITE_MESSAGE_ACTIVE = 'siteMessageActive';
    const SITE_MESSAGE_CONTENT = 'siteMessageContent';
    const SITE_MESSAGE_CONTENT_OTHER = 'siteMessageContent_other';


    const DEBT_BLOCK_CONTENT = 'debtBlockContent';

    const CZECH_POST_FILE_NO = 'czechPostFileNo';

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }


    public static function getData($what)
    {
        $model = Config::model()->find(array(
            'condition' => "what =:what",
            'params' => array(':what' => $what),
            'limit' => 1
        ));

        if($model === NULL)
            return false;

        return $model->value;
    }

    public static function setData($what, $value)
    {
        $model = Config::model()->find(array(
            'condition' => "what =:what",
            'params' => array(':what' => $what),
            'limit' => 1
        ));

        if($model === NULL)
        {
            $model = new Config();
            $model->what = $what;
        }

        $model->value = $value;
        return $model->save();
    }
}