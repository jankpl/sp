<?php

class UserDocumentsController extends Controller
{

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        /* @var $model UserDocuments */
        $model = $this->loadModel($id, 'UserDocuments');

        $this->render('view',array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($user_id = NULL)
    {
        $model=new UserDocuments;


        if($user_id)
            $model->user_id = $user_id;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['UserDocuments']))
        {
            $model->attributes=$_POST['UserDocuments'];
            $model->_file = CUploadedFile::getInstance($model,'_file');

            $model->validate();
            $errors = false;

            if(!$model->hasErrors())
            {

                $transaction = Yii::app()->db->beginTransaction();
                if(!$model->save())
                    $errors = true;

                if($model->_file !== NULL) {
                    // to get proper hash
                    $modelNew = UserDocuments::model()->findByPk($model->id);

                    $path = UserDocuments::getFinalDir($modelNew->hash.'.'.$model->_file->extensionName);
                    if (!$model->_file->saveAs($path)) {
                        $errors = true;
                    } else {
                        $model->file_path = $path;
                        $model->file_name = $model->_file->name;
                        $model->file_type = mime_content_type($path);
                        $model->update(array('file_path', 'file_name', 'file_type'));
                    }
                }

                if($errors)
                {
                    $transaction->rollback();
                } else {
                    $transaction->commit();

                    $this->redirect(array('view','id'=>$model->id));
                }
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id, 'UserDocuments');
        $model->scenario = 'update';

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['UserDocuments']))
        {
            $model->attributes=$_POST['UserDocuments'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {

        $model = UserDocuments::model()->findByPk($id);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        throw new CHttpException(403, 'You can\'t delete files - change stat!');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(array('/userDocuments/admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new _UserDocumentsGridView('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['UserDocumentsGridView']))
            $model->attributes = $_GET['UserDocumentsGridView'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }




    public function actionGetFile($id)
    {
        $model = UserDocuments::model()->findByPk($id);

        if($model === NULL) {
            throw new CHttpException(404);
        }


        $file_path = $model->file_path;
        $file_name = $model->file_name;
        $file_type = $model->file_type;


        if($file_path == NULL)
            throw new CHttpException(404);

        return Yii::app()->getRequest()->sendFile($file_name, @file_get_contents($file_path), $file_type);
    }


}

