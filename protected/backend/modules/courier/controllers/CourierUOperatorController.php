<?php

class CourierUOperatorController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->redirect(['/courier//courierUOperator/update', 'id' => $id]);

//        $this->render('view', array(
//            'model' => $this->loadModel($id, 'CourierUOperator'),
//        ));
    }

    public function actionCreate() {

        $model = new CourierUOperator;
        $modelTrs = [];

        $model->_max_dim_l = $model->max_dim[0];
        $model->_max_dim_d = $model->max_dim[1];
        $model->_max_dim_w = $model->max_dim[2];

        $model->_max_dim_nst_l = $model->max_dim_nst[0];
        $model->_max_dim_nst_d = $model->max_dim_nst[1];
        $model->_max_dim_nst_w = $model->max_dim_nst[2];

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $operatorTr = new CourierUOperatorTr();
            $operatorTr->language_id = $item->id;
            $modelTrs[$item->id] = $operatorTr;
        }

        $this->performAjaxValidation($model, 'courier-operator-form');

        if (isset($_POST['CourierUOperator'])) {

            $model->setAttributesAll($_POST['CourierUOperator']);

            $model->max_dim = [
                $_POST['CourierUOperator']['_max_dim_l'],
                $_POST['CourierUOperator']['_max_dim_d'],
                $_POST['CourierUOperator']['_max_dim_w'],
            ];

            $model->max_dim_nst = [
                $_POST['CourierUOperator']['_max_dim_nst_l'],
                $_POST['CourierUOperator']['_max_dim_nst_d'],
                $_POST['CourierUOperator']['_max_dim_nst_w'],
            ];

            $model->_max_dim_l = $model->max_dim[0];
            $model->_max_dim_d = $model->max_dim[1];
            $model->_max_dim_w = $model->max_dim[2];

            $model->_max_dim_nst_l = $model->max_dim_nst[0];
            $model->_max_dim_nst_d = $model->max_dim_nst[1];
            $model->_max_dim_nst_w = $model->max_dim_nst[2];

            foreach($_POST['CourierUOperatorTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierUOperatorTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierUOperatorTrs = $modelTrs;

            if ($model->saveWithTr(false)) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id, 'CourierUOperator');
        $modelTrs = [];

        $model->_max_dim_l = $model->max_dim[0];
        $model->_max_dim_d = $model->max_dim[1];
        $model->_max_dim_w = $model->max_dim[2];

        $model->_max_dim_nst_l = $model->max_dim_nst[0];
        $model->_max_dim_nst_d = $model->max_dim_nst[1];
        $model->_max_dim_nst_w = $model->max_dim_nst[2];

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = CourierUOperatorTr::model()->find('courier_u_operator_id=:courier_u_operator_id AND language_id=:language_id', array(':courier_u_operator_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $modelTr = new CourierUOperatorTr();
                $modelTr->language_id = $item->id;
                $modelTr->courier_u_operator_id = $id;
                $modelTrs[$item->id] = $modelTr;
            }
        }

        $this->performAjaxValidation($model, 'courier-operator-form');

        if (isset($_POST['CourierUOperator'])) {
            $model->setAttributesAll($_POST['CourierUOperator']);

            $model->max_dim = [
                $_POST['CourierUOperator']['_max_dim_l'],
                $_POST['CourierUOperator']['_max_dim_d'],
                $_POST['CourierUOperator']['_max_dim_w'],
            ];

            $model->max_dim_nst = [
                $_POST['CourierUOperator']['_max_dim_nst_l'],
                $_POST['CourierUOperator']['_max_dim_nst_d'],
                $_POST['CourierUOperator']['_max_dim_nst_w'],
            ];

            $model->_max_dim_l = $model->max_dim[0];
            $model->_max_dim_d = $model->max_dim[1];
            $model->_max_dim_w = $model->max_dim[2];

            $model->_max_dim_nst_l = $model->max_dim_nst[0];
            $model->_max_dim_nst_d = $model->max_dim_nst[1];
            $model->_max_dim_nst_w = $model->max_dim_nst[2];

            foreach($_POST['CourierUOperatorTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierUOperatorTr;
                $modelTrs[$key]->setAttributes($item);
            }


            $model->courierUOperatorTrs = $modelTrs;

            if ($model->saveWithTr(false)) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model = $this->loadModel($id, 'CourierUOperator');

            if(!$model->editable)
                throw new CHttpException(400, 'You cannot delete this item.');

            try
            {
                $model->delete();
            } catch(Exception $ex)
            {
                throw new CHttpException(400, Yii::t('app', 'You cannot delete it!'));
            }
            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierUOperator('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierUOperator']))
            $model->setAttributes($_GET['CourierUOperator']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionManageCountries($operator_id) {

        // used only for attribute labels
        $model = new CourierUOperator2CountryGroup('search');


        $modelOperator = CourierUOperator::model()->findByPk($operator_id);
        if($modelOperator === NULL)
            throw new CHttpException(404);

        $this->render('manageCountries', array(
            'model' => $model,
            'operator_id' => $operator_id,
            'modelOperator' => $modelOperator,
        ));
    }


    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewManageCountries($operator_id){
        $model = new CourierUOperator2CountryGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierUOperator2CountryGroup']))
            $model->setAttributes($_GET['CourierUOperator2CountryGroup']);

        $model->setCourierUOperatorId($operator_id);

        $this->widget('backend.extensions.selgridview.BootSelGridView', array(
            'id' => 'gv-manage-countries',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>2,
            'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'afterAjaxUpdate' => true), true),
            'selectionChanged'=>'function(id){  
                var number = $("#gv-manage-countries").selGridView("getAllSelection").length;
                $(".selected-no").html(number); 
         }',
            'columns' => array(
                'courier_country_group_id',
                [
                    'name' => 'courier_country_group_id',
                    'value' => '$data->courierCountryGroup->name',
                    'filter' => CHtml::listData(CourierCountryGroup::model()->primary()->findAll(),'id', 'name'),
                ],
                'delivery_hours',
                [
                    'name' => 'cod',
                    'value' => '$data->codName',
                    'filter' => CourierUOperator2CountryGroup::getCodList(),
                ],
                [
                    'name' => 'stat',
                    'value' => '$data->statName',
                    'filter' => CourierUOperator2CountryGroup::getStatList(),
                ],
                [
                    'header' => 'COD Price (PLN/EUR/GBP)',
                    'value' => '$data->listCodPricesInline()',
                    'filter' => false,
                ],
                [
                    'class' => 'CButtonColumn',
                    'template' => '{update}',
                    'buttons' => [
                        'update'=>array(
                            'url'=> 'Yii::app()->controller->createUrl("/courier/courierUOperator/courierUOperator2CountryGroupUpdate", ["id" => $data->id])',
                        ),
                    ]
                ],
                array(
                    'class'=>'CCheckBoxColumn',
                    'id'=>'selected_rows'
                ),

            ),
        ));
    }

    public function actionManageCouriersChangeStatByGV($operator_id)
    {
        $success = false;
        if(isset($_POST['ChangeStat']))
        {

            $ids = $_POST['ChangeStat']['ids'];
            $stat = $_POST['ChangeStat']['stat'];
            $ids = explode(',', $ids);

            if(S_Useful::sizeof($ids)) {
                array_walk($ids, function (&$value, &$key) {
                    $value = intval($value);
                });
                $ids = implode(',', $ids);

                $cmd = Yii::app()->db->createCommand();
                if($cmd->update((new CourierUOperator2CountryGroup())->tableName(), ['stat' => $stat], 'id IN (' . $ids . ')'))
                    $success = true;
            }
        }

        if($success)
            Yii::app()->user->setFlash('success', 'Stat has been changed!');
        else
            Yii::app()->user->setFlash('error', 'Stat has NOT been changed!');

        $this->redirect(['/courier/courierUOperator/manageCountries', 'operator_id' => $operator_id]);
        Yii::app()->end();
    }

    public function actionManageCouriersSetCodByGV($operator_id)
    {
        $success = false;
        if(isset($_POST['SetCod']))
        {

            $ids = $_POST['SetCod']['ids'];
            $cod = $_POST['SetCod']['cod'];
            $ids = explode(',', $ids);

            if(S_Useful::sizeof($ids)) {
                array_walk($ids, function (&$value, &$key) {
                    $value = intval($value);
                });
                $ids = implode(',', $ids);

                $cmd = Yii::app()->db->createCommand();
                if($cmd->update((new CourierUOperator2CountryGroup())->tableName(), ['cod' => $cod], 'id IN (' . $ids . ')'))
                    $success = true;
            }
        }

        if($success)
            Yii::app()->user->setFlash('success', 'COD has been changed!');
        else
            Yii::app()->user->setFlash('error', 'COD has NOT been changed!');

        $this->redirect(['/courier/courierUOperator/manageCountries', 'operator_id' => $operator_id]);
        Yii::app()->end();

    }

    public function actionManageCouriersSetCodPriceByGV($operator_id)
    {
        $errors = true;
        $transaction = false;
        if(isset($_POST['SetCodPrice']))
        {
            $errors = false;

            $ids = $_POST['SetCodPrice']['ids'];
            $price = $_POST['PriceValue'];
            $ids = explode(',', $ids);


            $transaction = Yii::app()->db->beginTransaction();

            if(S_Useful::sizeof($ids)) {
                array_walk($ids, function (&$value, &$key) {
                    $value = intval($value);
                });

                foreach($ids AS $id) {

                    $priceModel = Price::generateByPost($price);
                    $price_id = $priceModel->saveWithPrices();

                    if (!$price_id) {
                        $errors = true;
                        continue;
                    }

                    /* @var $cmd CDbCommand */
                    $cmd = Yii::app()->db->createCommand();
                    if (!$cmd->update((new CourierUOperator2CountryGroup())->tableName(),
                        [
                            'cod_price_id' => $price_id,
                        ],
                        'id = :id',
                        [':id' => $id]
                    ))
                        $errors = true;
                }
            }
        }

        if(!$errors) {
            if($transaction)
                $transaction->commit();

            Yii::app()->user->setFlash('success', 'COD Price has been changed!');
        }
        else {
            if($transaction)
                $transaction->rollback();
            Yii::app()->user->setFlash('error', 'COD Price has NOT been changed!');
        }

        $this->redirect(['/courier/courierUOperator/manageCountries', 'operator_id' => $operator_id]);
        Yii::app()->end();

    }

    public function actionManageCouriersSetHoursByGV($operator_id)
    {
        $success = false;
        if(isset($_POST['SetHours']))
        {

            $ids = $_POST['SetHours']['ids'];
            $hours = $_POST['SetHours']['hours'];

            $cmd = Yii::app()->db->createCommand();
            $ids = explode(',', $ids);


            if(S_Useful::sizeof($ids)) {
                array_walk($ids, function (&$value, &$key) {
                    $value = intval($value);
                });
                $ids = implode(',', $ids);
                if($cmd->update((new CourierUOperator2CountryGroup())->tableName(), ['delivery_hours' => $hours], 'id IN (' . $ids . ')'))
                    $success = true;
            }
        }

        if($success)
            Yii::app()->user->setFlash('success', 'Hours have been changed!');
        else
            Yii::app()->user->setFlash('error', 'Hours have NOT been changed!');

        $this->redirect(['/courier/courierUOperator/manageCountries', 'operator_id' => $operator_id]);
        Yii::app()->end();
    }



    public function actionManageCountriesUserGroup($operator_id, $user_group = NULL) {

        if($user_group === NULL)
        {
            if(isset($_POST['user_group_id'])) {
                $this->redirect(['/courier/courierUOperator/manageCountriesUserGroup', 'user_group' => $_POST['user_group_id'], 'operator_id' => $operator_id]);
                Yii::app()->end();
            } else
                throw new CHttpException(404);
        }

        $modelOperator = CourierUOperator::model()->findByPk($operator_id);
        if($modelOperator === NULL)
            throw new CHttpException(404);

        $modelUserGroup = UserGroup::model()->findByPk($user_group);
        if($modelUserGroup === NULL)
            throw new CHttpException(404);

        // used only for attribute labels
        $model = new CourierUOperator2CountryGroup();

        $this->render('manageCountriesUserGroup', array(
            'model' => $model,
            'operator_id' => $operator_id,
            'user_group' => $user_group,
            'modelOperator' => $modelOperator,
            'modelUserGroup' => $modelUserGroup
        ));
    }


    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewManageCountriesUserGroup($user_group_id, $courier_u_operator_id){
        $model = new CourierUOperator2CountryGroupUserGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierUOperator2CountryGroupUserGroup']))
            $model->setAttributes($_GET['CourierUOperator2CountryGroupUserGroup']);

        $model->setUserGroupId($user_group_id);
        $model->setCourierUOperatorId($courier_u_operator_id);

        $this->widget('backend.extensions.selgridview.BootSelGridView', array(
            'id' => 'gv-manage-countries',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>2,
            'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'afterAjaxUpdate' => true), true),
            'selectionChanged'=>'function(id){  
                var number = $("#gv-manage-countries").selGridView("getAllSelection").length;
                $(".selected-no").html(number); 
         }',
            'columns' => array(
//                'courier_country_group_id',
                [
                    'name' => 'courier_country_group_id',
                    'value' => '$data->courierCountryGroup->name',
                    'filter' => CHtml::listData(CourierCountryGroup::model()->primary()->findAll(),'id', 'name'),
                ],
                [
                    'name' => '_custom_stat',
                    'value' => '$data->getCustomStatName()',
                    'filter' => CourierUOperator2CountryGroupUserGroup::getCustomStatList(),
                ],

//                'delivery_hours',
//                [
//                    'name' => 'stat',
//                    'value' => '$data->statName',
//                    'filter' => CourierUOperator2CountryGroup::getStatList(),
//                ],
                array(
                    'class'=>'CCheckBoxColumn',
                    'id'=>'selected_rows'
                ),

            ),
        ));
    }

    public function actionManageCouriersUserGroupChangeStatByGV($operator_id, $user_group)
    {

        $success = false;
        if(isset($_POST['ChangeStat']))
        {

            $ids = $_POST['ChangeStat']['ids'];
            $stat = $_POST['ChangeStat']['stat'];
            $ids = explode(',', $ids);

            $stat = $stat == '' ? NULL : $stat;



            if(S_Useful::sizeof($ids)) {
                array_walk($ids, function (&$value, &$key) {
                    $value = intval($value);
                });

                if(CourierUOperator2CountryGroupUserGroup::updateCustomSettings($ids, $user_group, $stat))
                    $success = true;
            }
        }

        if($success)
            Yii::app()->user->setFlash('success', 'Stat has been changed!');
        else
            Yii::app()->user->setFlash('error', 'Stat has NOT been changed!');

        $this->redirect(['/courier/courierUOperator/manageCountriesUserGroup', 'operator_id' => $operator_id, 'user_group' => $user_group]);
        Yii::app()->end();

    }

    public function actionCourierUOperator2CountryGroupUpdate($id)
    {
        $model = CourierUOperator2CountryGroup::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(403);

        if($model->price === NULL)
            $model->price = new Price();


        $success = false;
        $errors = false;

        if(isset($_POST['stat']))
        {
            $model->stat = $_POST['CourierUOperator2CountryGroup']['stat'];
            if($model->update(['stat']))
                $success = true;
            else
                $errors = true;
        }

        if(isset($_POST['delivery_hours']))
        {
            $model->delivery_hours = $_POST['CourierUOperator2CountryGroup']['delivery_hours'];
            if($model->update(['delivery_hours']))
                $success = true;
            else
                $errors = true;
        }

        if(isset($_POST['cod_price']))
        {
            $model->price = Price::generateByPost($_POST['PriceValue']);
            if($model->saveWithPrice())
                $success = true;
            else
                $errors = true;
        }

        if(isset($_POST['cod']))
        {
            $model->cod = $_POST['CourierUOperator2CountryGroup']['cod'];
            if($model->update(['cod']))
                $success = true;
            else
                $errors = true;
        }


        if($success OR $errors)
        {
            if($success)
                Yii::app()->user->setFlash('success', 'Changes have been saved!');
            else if($errors)
                Yii::app()->user->setFlash('error', 'Changes have NOT been saved!');

            $this->refresh();
        }


        $this->render('update_courier_2_country_group',
            [
                'model' => $model
            ]);
    }

    public function actionPrices()
    {
        $this->render('prices');
    }

}