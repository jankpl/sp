<?php
/* @var $id_prefix string */
?>
<br/>
<?= $this->renderPartial('/_listActions/_generateAckCardMulti', ['id_prefix' => $id_prefix]); ?>
<br/>
<small><?= Yii::t('courier', 'Cancelled packages will be omitted.');?></small>
