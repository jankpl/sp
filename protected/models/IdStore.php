<?php

class IdStore
{
    const TYPE_COURIER = 1;
    const TYPE_POSTAL = 2;


    public static function createNumber(int $type, int $type_id, string $ext_id) : bool
    {
        $cmd = Yii::app()->db->createCommand();

        return $cmd->insert('id_store', [
            'type' => $type,
            'type_id' => $type_id,
            'ext_id' => $ext_id
        ]);
    }

    public static function removeNumber(int $type, int $type_id, string $ext_id) : bool
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->delete('id_store', 'type = :type AND type_id = :type_id AND ext_id = :ext_id', [
                ':type' => $type,
                ':type_id' => $type_id,
                ':ext_id' => $ext_id]
        );
    }

    public static function updateNumber(int $type, int $type_id, string $ext_id_old, string $ext_id_new) : bool
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->update('id_store', [
            'ext_id' => $ext_id_new
        ], 'type = :type AND type_id = :type_id AND ext_id = :ext_id_old', [
                ':type' => $type,
                ':type_id' => $type_id,
                ':ext_id_old' => $ext_id_old
            ]
        );
    }

    public static function returnModelsByIds(int $type, array $ids)
    {
        $cmd = Yii::app()->db->createCommand();
        $result = $cmd->select('type_id')->from('id_store')->where(['in', 'ext_id', $ids])->queryColumn();

        if($result)
            $criteria = new CDbCriteria(['order'=>'FIELD(id, '.implode(',', $result).')']);
        else
            $criteria = '';

        if($type == self::TYPE_COURIER)
        {
            return Courier::model()->findAllByAttributes(['id' => $result], $criteria);
        }
        else if($type == self::TYPE_POSTAL)
        {
            return Postal::model()->findAllByAttributes(['id' => $result], $criteria);
        }
    }

}