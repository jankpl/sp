<h4>Generate Manifest/ACK</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-ack-cards',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/generateAckCardsByGridView'),
));
?>

<?php
echo TbHtml::submitButton('Generate Manifest/ACK', array(
    'onClick' => 'js:
    $("#courier-ids-ack").val($("#courier").selGridView("getAllSelection").toString());
    ;',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => 'courier-ids-ack')); ?>
<?php
$this->endWidget();
?>
