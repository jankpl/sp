<?php

class SftpClient
{
    protected $host;
    protected $port;
    protected $login;
    protected $password;

    protected $sshConnection = false;
    protected $sftp = false;

    public function __construct($host, $port, $login, $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->login = $login;
        $this->password = $password;
    }

    protected function connect()
    {
        $this->sshConnection = ssh2_connect($this->host, $this->port);

        if(ssh2_auth_password($this->sshConnection, $this->login, $this->password)) {
            $this->sftp = ssh2_sftp($this->sshConnection);
        }
    }

    public function closeConnection()
    {
//        if ($this->sshConnection) {
//            ssh2_exec($this->sshConnection, 'exit');
//        }
        $this->sshConnection = false;
    }

    public function sendFile($fileData, $filePath)
    {
        if(!$this->sshConnection OR !$this->sftp)
            $this->connect();

//        $resFile = fopen('ssh2.sftp://'.intval($this->sftp).'/./'.$filePath, 'w');
//        fwrite($resFile, $fileData);
//        fclose($resFile);

//        return true;

        $result = file_put_contents('ssh2.sftp://'.intval($this->sftp).'/./'.$filePath, $fileData);
        sleep(1);
        return $result;

//        $this->closeConnection();
    }
}