<?php


class eitherOneValidator extends CValidator
{

    public $other;

    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object,$attribute)
    {
        $other = $this->other;

        $field1 = $object->getAttributeLabel($attribute);
        $field2 = $object->getAttributeLabel($this->other);

        if (($object->$attribute == '') && ($object->$other == '')) {

            $errorMessage = Yii::t('app', 'Wprowadź treść do przynajmniej jednego pola z:');

            $this->addError($object,$attribute,$errorMessage.' '.$field1.', '.$field2.'!');
            $this->addError($object,$other,$errorMessage.' '.$field1.', '.$field2.'!');
        }
    }

    public function clientValidateAttribute($object,$attribute)
    {

        $field1 = $object->getAttributeLabel($attribute);
        $field2 = $object->getAttributeLabel($this->other);

        $errorMessage = Yii::t('app', 'Wprowadź treść do przynajmniej jednego pola z:');

        $otherVal = '$("input[name=\''.get_class($object).'['.$this->other.']\']").val()';
        $condition = $otherVal." == '' && value == ''";

        return "
    if(".$condition.") {
        messages.push(".CJSON::encode($errorMessage.' '.$field1.', '.$field2.'!').");
    }
    ";
    }

}