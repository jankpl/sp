<?php
if(Yii::app()->user->isGuest):
    ?>
    <?php $this->beginContent('//layouts/other');
    ?>
    <?= $content;?>
    <?php $this->endContent(); ?>
    <?php
else:
    ?>
    <?php
    $this->beginContent('//layouts/_panel');
    ?>
    <div id="main">
        <!--<div class="container">-->
        <!--    <div class="row">-->
        <!--        <div class="col-lg-12" id="main">-->
        <?= $content;?>
        <!--        </div>-->
        <!-- /.col-lg-12 -->
        <!--    </div>-->
        <!-- /.row -->
        <!--</div>-->
        <!-- /.container-fluid -->
    </div>
    <?php $this->endContent(); ?>
    <?php
endif;
?>