<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

    <p>
        You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
    </p>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-upc-collect',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        array(
            'name' => '__courier_id',
            'value' => 'CHtml::link("#".Courier::model()->findByPk($data->courier_id)->local_id,array("/courier/courier/view","id" => $data->courier_id),array("target" => "_blank"))',
            'type' => 'raw',
        ),
        array(
            'name' => 'stat',
            'value' => 'CourierCollect::getStats()[$data->stat]',
            'filter' => CourierCollect::getStats(),
        ),
        array(
            'header' => 'Log',
            'value' => '$data->logNice',
        ),
        array(
            'header' => 'Collect ID',
            'value' => '$data->prn',
        ),
        array(
            'header' => 'Packages',
            'value' => '$data->packagesNumber',
        ),
        array(
            'name' => 'collect_operator_id',
            'value' => 'CourierCollect::getOperators()[$data->collect_operator_id]',
            'filter' => CourierCollect::getOperators(),
        ),
        'date_entered',
        'date_updated',
        array(
            'header' => 'Details',
            'value' => 'CHtml::link(TbHtml::icon(TbHtml::ICON_SHARE_ALT, array("title" => "Details")),array("/courier/courierCollect/view","id" => $data->id),array())',
            'type' => 'raw',
        )
    ),
)); ?>