<div class="form">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'user-address-form',
        'enableAjaxValidation' => false,
    ));
    ?>
<table class="list hTop">
    <tr>
        <td>#</td>
        <td>Local ID</td>
        <td>Remote ID</td>
    </tr>

    <?php
    foreach($models AS $key => $item):
        $this->renderPartial('_assignCarrierFromFile_Item',
            array(
                'model' => $item,
                'i' => $key,
                'form' => $form,
            ));

    endforeach;
    ?>
</table>
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <div class="navigate">

        <?php
        echo GxHtml::submitButton(Yii::t('app', 'Save'));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->