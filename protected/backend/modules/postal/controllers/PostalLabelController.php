<?php

class PostalLabelController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new _PostalLabelGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['PostalLabelGridView']))
            $model->setAttributes($_GET['PostalLabelGridView']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionGetLabel($id)
    {
        $model = PostalLabel::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $content = $model->getFileAsString(true);

        if($model->file_path == '' AND (!$content OR $content == ''))
            throw new CHttpException('404', 'Label do not exists or was not downloaded into our system. Try to check directly in package.');

        $finfo = new finfo();
        $mime = $finfo->buffer($content, FILEINFO_MIME_TYPE);


        switch($mime) {
            case 'image/png':
                $ext = 'png';
                break;
            case 'image/gif':
                $ext = 'gif';
                break;
            default:
                $ext = 'txt';
                break;
        }

        return Yii::app()->getRequest()->sendFile($model->id.'.'.$ext, $content , $mime);
    }

    public function actionEditTrackingId($id)
    {
        /* @var $model PostalLabel */
        $model = PostalLabel::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        if (isset($_POST['PostalLabel'])) {
            $model->setAttributes($_POST['PostalLabel']);

            if ($model->update('track_id')) {

                $model->expireLabel();
                Yii::app()->user->setFlash('success', 'Tracking ID has benn updated!');
                $this->redirect(array('/postal/postal/view', 'id' => $model->postal_id));
            }
        }

        $this->render('updateTrackingId', array( 'model' => $model));



    }


}