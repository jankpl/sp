<?php
/* @var $form GxActiveForm */
?>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-domestic-Operator-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('maxlength' => 32, 'disabled' => (!$model->isNewRecord && !$model->editable) ? true : false)); ?> (*)
        <?php echo $form->error($model,'name'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'broker'); ?>
        <?php echo $form->dropDownList($model, 'broker', CourierDomesticOperator::getBrokerList($model->isNewRecord OR $model->editable ? true : false), array('id' => 'broker-option', 'disabled' => (!$model->isNewRecord && !$model->editable) ? true : false)); ?>
        <?php echo $form->error($model,'broker'); ?>
    </div><!-- row -->
    <div class="safepak-details">
        <div class="row">
            <?php echo CHtml::label('Safepak service ID', 'safepak_service') ?>
            <?php echo CHtml::textField('safepak_service', $model->_safepak_service, array('id' => 'safepak_service', 'disabled' => (!$model->isNewRecord && !$model->editable) ? true : false)) ?>
        </div><!-- row -->
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'stat'); ?>
        <?php echo $form->dropDownList($model, 'stat', CHtml::listData(S_Status::listStats(),'id','name')); ?>
        <?php echo $form->error($model,'stat'); ?>
    </div><!-- row -->
    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';

        endforeach;
        ?>
    </div>

    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

            $model_tr = $modelTrs[$languageItem->id];

            if($model_tr === null)
            {
                $model_tr = new CourierDomesticOperatorTr();
                $model_tr->language_id = $languageItem->id;
            }


            $this->renderPartial('_tr_form', array(
                'model' => $model_tr,
                'form' => $form,
                'i' => $languageItem->id,
            ));

            echo'</div>';

        endforeach;
        ?>
    </div>

    <br/><br/>

    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>
    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->

<script>
    var $brokerOption = $("#broker-option");
    var $safepakDetails = $(".safepak-details");

    $(document).ready(function(){
        if($brokerOption.val() == <?= CourierDomesticOperator::BROKER_SAFEPAK;?>)
            $safepakDetails.show();
        else
            $safepakDetails.hide();
    });

    $brokerOption.on('change', function(){
        if($(this).val() == <?= CourierDomesticOperator::BROKER_SAFEPAK;?>)
            $safepakDetails.show();
        else
            $safepakDetails.hide();
    })
</script>