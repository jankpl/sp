<div class="alert alert-success text-center">
    <h4><?php echo Yii::t('courier','Karta potwierdzenia statusu');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t('courier','Generuj kartę potwierdzenia statusu'), array('/courier/courier/statConf', 'hash' => $model->hash), array('target' => '_blank', 'class' => 'btn'));?>
</div>
