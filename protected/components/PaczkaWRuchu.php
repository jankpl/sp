<?php

class PaczkaWRuchu
{

    const URL = GLOBAL_CONFIG::RUCH_URL;
    const PARTNER_ID = GLOBAL_CONFIG::RUCH_ID;
    const PARTNER_KEY = GLOBAL_CONFIG::RUCH_KEY;

    const CACHE_POINTS_MINUTES = 2880; // 2 * 60 * 24 minutes

    const MAX_WEIGHT = 20; // KG
    const MAX_MINI_WEIGHT = 1; // KG

    const MAX_DIMENSIONS_1 = 50; // CM
    const MAX_DIMENSIONS_2 = 30; // CM
    const MAX_DIMENSIONS_3 = 20; // CM

    public $FirstName;
    public $LastName;
    public $CompanyName;
    public $StreetName;
    public $BuildingNumber;
    public $FlatNumber;
    public $City;
    public $PostCode;
    public $PhoneNumber;

    public $SenderEMail;
    public $SenderFirstName;
    public $SenderLastName;
    public $SenderCompanyName;
    public $SenderStreetName;
    public $SenderBuildingNumber;
    public $SenderFlatNumber;
    public $SenderCity;
    public $SenderPostCode;
    public $SenderPhoneNumber;

    public $PackValue;
    public $Insurance = false;

    public $BoxSize = false;

    public $SenderOrders;
    public $TransferDescription;

    public $DestinationCode;
    public $AmountCashOnDelivery = 0;

    public $ReturnPack = false;

    /**
     * Returns logo to merge with RUCH label
     * @return Imagick
     */
    protected static $_getLogoToMerge;
    public static function getLogoToMerge()
    {
        if(self::$_getLogoToMerge === NULL)
        {
            $imgLogo = new Imagick();
            $imgLogo->readImageBlob(base64_decode('iVBORw0KGgoAAAANSUhEUgAABnUAAACACAAAAAD4hbOJAAAaYUlEQVR4Xuzda3BU5R3H8e+C1SpihCIVVFSqWEo7VgQwXFWgIrGCop0B1NKZCsLgZER1ilYrraMAVSqjQxGBVqtUFApKzYVAIKViQQlAINySkCyEGAKJ3JKQmG0nc2ZzLs9Z3TAg3fP7vPrPsztz9t135jl7zhOCCGeBiIhIiBacLSIiIqqOiIioOiLrZ6Yk90t+YOFuYjiR/ZsByX37TFxeyv8DETk37+uIfPLyUmu6eNKTbfGxbMZ6a7o2NZVznYiEzs3qiMxLrSaq24JeGE2ZRpPB8zsRPxHRvwlEZo+rpsn2oZsxSZ2GTdbdFcRNRHRfRyTjCRwqx1bjNWs2DltSiZeIqDoix5+ow2nLFDy2P43Lu4uJk4ioOiJ/z8Ptz7twm1WD20vESURUHZF0PGqX43IoG48dG4iLiKg6IvvW4bUely1FeJzKJC4iouqIHK7AqxKXLyJ4hYmLiKg6Ike+4hsIYVBGXERE1RFpjUEElwYM2hIXEVF1RNqZ2vFdXJIwuJy4iIiqI9LhGry64tKltfFbcRERVUek1SC8Rnqqk4zH94cQHxFRdUQeS8JtcH9cQhPwGNOB+IiIqiPS4UlczpuJx4h7cen8HPESEVVH5OkUnF77KV5zurrS9GYS8RIRVUcktOge7KaPx6D9+92xabviNuInIqqOSOulr3wPCz3SnsKo2+pHiErJuYNmERFVR+Sx3Ok3tfsOwPi1Q/GRNCd3chLAxfdmLuvGOU9EzuETrEUK+pUBt68ihhM37QGGZHJaREQnWIu0/hJg9WfEkLkHoLiG0yIi2mET2VwLQAYxpAFQsY/TIiKqjsieBgAWR/BVlQ7AsSJOi4ioOiJhGuXl4is7DEBdHqdFRFQdEas2DYvw9aY1lHJaRETVETlgDR/V4aP8X9awGzMRUXVEtuV8kpPP1ympsoZdmfhYdswayqwhhrx1n+QELU4iqo5IydSfDRjYf+CAlJcqiGl7KZbVvtXBUnKAmMLPDhnQv9/AgXdOO0lwiOgpUZE/zTiIpdNLo4nhzYex3LC+DSb7by7Hkn0rMUyfXYrluqmj+ZaISOjsVkekdsJCbFJnhfA1eRYWcvpjMm8cFv76EP7GzcPmqekEgojeTSAybiF2r07BXxlRH2NSt5SoPfj79TzsZvyeIBDRfR2RBW/hNP2f+KkpJuovxzHYm2WrTgQ/s+fj9Lv1JD4RVUek/HncnjmJj3JbdcpWYrCunqj8o/gIv4jbb0l8IqqOyIowbluy8BEupckSDBbRpOgEPpZ9gdvqTBKeiKoj8g+8luDjYIQm2YfxKPw3TY7tw+zUR3i9Q8ITUXVEcvBai4/d2JRm4rH4FDbFmFVuxCuPhCei6ojUm9fMdmK3EreG97Hbhll1FV5VNSQ6EVVHpKVxzaymALu0Ulw+dXamBLMQBjUVJDoRVUekwbhmdnQrdmXpuKTVYfc5ZhEMWrUn0YmoOiJX4NURs6rjOGThVOdaqKjCKOkq00XPJ9GJqDoiffAahFkuThn7cdi8CYfj2zFqcxtet5HwRFQdkdF4hEZhFsbpyBocVp7CoSYPsxF4tLyfhCei6ogMGYzbgz/BbBMuH2B38l1cijEb3he3X/6IxCei6oi82BKnNlPxsReXzEJHlLbjko9Zi5cvwqntCwSAiKoj0vPtEHZXpF2DWeUBXKrfi/2ag0p89H6rLXbtVnYgCERUHZFR73TEJqk3PvZU4fYxTeq91Sk+iI9e52Nzw7LuBIOIqiMyau3IS4jKH4GPgmrc8vKJWrcPt/ISzGpHlhF12ficvgSHiE6wFvlseUHhkctuylsL8KsFGD0/FY8/Po6FSa/j8bcxmHx5TzbAoB9sqmzftfO9XQkSEZ0lKtLjD++uWpP12kwAFqZitB+vVREsR9LxCmP0aDZA60Vz/3fR+c8ELzoi2mETadXxQq6/HIDZszApxCsjD8uqArwKMEl9G4Aul3HJlRcRUCKqjsilw2g0+QO8qsrxakiLeTzC3lN4vTybRkMJNBFVRyTFGsZl47GnCINFDTSqWI5B4SE8Fj1hDSMJNBFVR6RXexpV/mI7bqUnMdi8jUYb9mNQ8iVu6Q9aQ9duBJqIqiNy5S3WUHHfIVyKMHqPRh9hdNBTqdFfWdPw8wk2EVVHZCCWnSlVOBVglAHA4SUY5eK0c2Qllv4EnIiqI3LXpVg2jqrHrn4vRlvXAiw9hFEYh8P3F2K5tgcBJ6LqiHS5HgvpE7E7VoJR/dIYG2zkOtM1Og8LfdojIqqOaIstat4UbI6EMVt9DA5uwKwgQpPI+EyitMEmouqIcE8LoqbNpUn4GGY7NsKHX2B2qIAmU94gqp2e1hFRdUTofT1NHvmQqCJ8NGTAKnzU7SBq1nSa3Hi1AiCi6oi0HIzNmHVYyMXPcsoy8FOMhSWTsbkDEVF1RLgLm+NjtmEJ42fXjjVH/T/EkvYQdimIiKojQvJV2JSMOECjU5/ja+o7fG11Ph95EpveXRERVUeEpP7YFT5QDUBZBb7eX4mvg7UAHLivGrvhIURE1RGBoTisGd0AsLsOX5FafB3ZB1AxZh8OfRERVUcE6NsOh2UTAPbW0ywV+QCj1uLQ5UZERNURATr3xOmNaUA+zRMpBsZl4TQ0CRFRdUQAbsFlyhzYSTPthqnzcBnAN1BL4hNRdURSLsTl8RWU00zlzH0elyuSieXopLtGjBj+8wLOBhE5j2+TyM0/3ohT9dhXa2imijmpuCV3JJatrwP0u5oAENEOm0g/3A6PLaKZPp1Yh9sgYvoPQNu3WxEAIqqOyP141NfSTDV4XHQHsdStAHjxGoJARNUR6X4dZ1Sfa4mlaB3Q92ECQUTVEblgGGfUMGLKqofQKy0IBhFVR2QwZ1LodmJaDDzVi4AQUXVEelzJGdTzh8RysqZTpzunERQiqo5Ih1s5g+6+gBgaDj367HMTyomtgW9R1aaVq9O31vr8kAjxqiPYRM/riPy3nTuPjqJM9zj+7e6ks5KNkEBCEgJBtiCbwQmEEFaRTTZ1nEFFcUYZQUFHUe+4jDMqwoy7IuMIchTGBQUREblAiCj7vgaQkAQCJmTfk+50X9+qvmk6J52FGWe8l+dzTqeqnrzvW/VPnd+p5a3+H/DTuZ4mfPnOZrMBi/XOe/sAX630xm5bEAarvjDXGe5LgP1v2Y11hod6ffSVJ9T5LfRBs2Wpl9FmnTQZeGeLP041nZ9B2b7EjGK3Xds/GWXVGr1vnblP1/j2jmHec0aiNeCxDrZF6R7OwqyBwLH3/pFrNdg9rp9zK/D+Rl9s1jG3Aby+zxMKX+j8W6MBm2FeLzRHX7abcKju9KT51QOeUDviDmDRES+wl946FYft6zONoWNGGvl3EsIAdv6ThMgYWMBPJW5nW9zJm7cSB4/XZsGyuwGO9cQ28Uvg1QdgweMAuwbOehulOBCNvjlptfrzOZeLyUT5273Uu+XpnsDsN6l3zc0PBwMsehQnw+F4a8JBnJbNgHdnV+Pwq6VevDoXIDkNqO58EeCCdwjKF+PRlxNxijjlN+Rbre8KONETzeHeaM7PW4UyetG1/NsIYcDIf5gQneP5yYx0HzqVv16JA9bfvQLXoGRCXR5AruNHTCd8UAIMaKqPo2RYAV9ctEFjxunjYfsAH5xOPZd4AMALJwJN4I8TZlh4TzUOrPxlLTcEAlyqAE6UAkzsoC3AE9el43gdI/oCf0Mz939D54ZVREy5fU7njcP2cFUR8lxHiBR+Mkm4tXgTTjx2jCB/gBKwnQXIsUMBQNtQXF08ipKZDtQ2/xHRvNm1NHByaiFQjhPWho0q2fsETqx5k+49ATKPAbsqAK6jGvfy7NQrW4niMw/dvOMJvy3c/eX6x8YWzqrmaiIkdYSYyj8nAHfCRuJO3l9QTJO6odS8SXQkwHm4WA5woQ7yAcKNuMooRCk9AnjiwowTphg0Oz/FAQO6swuB7skpKSnDbuyDYg7DIThl+I/1od15pw4lMQXNc3kkAlQdAfYBmIdgoDHdxv04QvJ4T+qtyEOZFY0mbZXvyqTqibedOfZW8L41XE2EpI4QvXtzxUInL9yYdj9uJITjztYfUJat3hqDsqK8TQeAEsi2AJwvxloEEE0DJ9AdBZ5am5r2ugllflrqV8twwm9Dah+UTThck5b2jh/KB2UwJS01NXXL+nEogzrg0D9184/1rUl5G1AmpaXeg1KwjtFGx37rTgJEX0/jHln34whpy804ULccJeR36D7krrhipr3MmZgH+ZiripDUEWIaV8I7YvJr3+z/7JFRfd/4vCuNmohb6Sjxt9P+lyil6UQAnIdMG8DFUiylAHE0sBvdEaDHhJTkUQaUxOSUMQk4YeieMs8AcKgGXfCQ5HtmouTtxcGyEuWOhiclGdkAnvM8eVgvHSQxGOCIhVPHAPr7tOzdVDt7d6JM74LuDIOpZtuLhDGSvTRJCEkdIQ92zD3G/2nT95/NGRKFMnHbTBrhMxq3MlDi7BCFZj+xAIVwxg5QXMUPJYAxFle1u9Edv4QmH00RDdhLGeAPUFiKzgLcZACw1rfekgkQOgIHbHY0hSi+EeAbjHKQgKEARy9yrBBgWAsn5Zj5CKXNbBxsGIGnn4yahz+5XFWEpI4QveNojX73vbpx5xd/GOyDYqkFwv++NoGGGBiDWyaUvVVQCoRdP7kfHQCy4Dyac5RXAl5RuDp/El3mOZrj6aH1uIRTqCeAwYTDuyhTQtwcYflRqCmAwO433gzjAHLzOQbgO97lRM7FncAcfSe3d8XBjxKMjKVnPNnEcVURkjpCBN9AC5k7z3x/1+bFDwwNALDmrXthzMDTlL1byoRNj9HQFANudUK5sBFGv75u945NnyXopXzIQnOSnDLArzuuduBQt5/m2OwABkPDEnYbuoub3b1REWoCqFsF4W/+I21X2pdzYLCHfmS7AAZ0xMFgrn0/4UHcsa0rBfCfjwMJrMWPKSM3HeEzkri6CPk2gRDXv0nzQjr2G5YU1gaNPe/iiW3fnSuxQTWG+xa/NTDgheG/P4yLwbjXDcX21Ig2Awagax9QCnUFbS+hyaHCCoQH4Gov4OtVBByiOWYjgJ8XTqUWAIMR3ReFAD0G0RCR7XMAVvxyfMDvcIi5dj+wf8Q+gBtxwGPhqe8x444j2WZG48DUJ7/cHox1+qbV9g+5lauLkGsdIYZH0LSQwfe9nXbovTu7tAEs6Sv+NHFwv18tPlxkA38TJv99SQ+VMmrXI1yuf3fcGxqIcmR0EQ5EBgO1FVVlaLIoAOhEA3sA/0FewH4LzSitAogMw+lbFN/O6D5FGe+PkwFN+xvQ3L+Vet4jAI4dzQNIBEwolvXfQ1WDEZy+3gcQ8CD1eszntuU9ku9MXDy8evpwri5CUkeIyOtwz2fok2u+2bz43niAoo3P/2pIyvSn1p3BCQ8sLyd/jPfC1AE4jfDDvfC5aHYOP45DWChQVXyhGKL9IItCgBhcVeQAXv2DgIOFNMUQwIdVAOHe6Ixw5BWU6B5oTu0G8JrW2Ek5ywsle/QK6g0C2PsJQMeegBUHbvmL23sYeShzYnF6dkr2V5UfvFD1Q8Gwl7naCLnDJsSItTTGFBaZPDw+zAeloHDvnm8v5tXSuEO3bvhjVMqW51+y4DCMpsz5KB3l4LjlyWiMoUB1cVERJH1TwQUuAcThau9FwDf541yoTA+nCbZVu/WIGeKB7sz0S3uKUO5wlL4sBugzEKfvugHz74brZv8VxTL9wiM4DIjMgUvvAwwNg48c/+g089auOM1/Htp90gEXcThhXrXgjaznIezeJ7y52ghJHSHGza+moU7d45OHBKHUZGRk7NieRdOWbXjuroAFwx89hKbrIJrSdulQC0rm+I9uRNPta6BIVa/NPE9lcRlAZ1ztqQHMA4IBtg2lCWU3o/GYjEPRCnQJc9HY1qDchBOVp4CLAC/sT0Xz6Jm30UVdlwNUACQD+8+heWckl8vNhexaXK263YST4fF7txyo6zssnKuPkNQRoku/HVwuNrFXz4RIlKLDB04fPlFAC1y8e9XzfUYnPfsiSt9AmpT4+V25KGXTPh+JEg1QUqbW4rdj358HEIur0wCBwWEAO2mJP1xLAzcs9kJzYBuA7y005A3g+en47WiW8Da6QZ+j808E7OjsNESQEVcbj7keR8i0aVylhDzXEeJmHPAMnfrG9p0rnpgUSfX5JYuO3DZm3lvfFtAy64c/a/FdsLUvwE0048YNXdFUjt+Mco0BuHAJiIwFy4kSoF0ILmqOA0QRBnCymmYZ5z7tXDegvPhlLLoP7QCj4mioHCV44wx0S36PboQ3uj69gIF3orHRELl1uLKsRyeEpI4QSSj+8ROe+yZz1f2JYZz95rWZ/brf90reiWpaofDpwWkM/fb3ngSPpTl9/3sImpqZ2QARXsDBAvAIjYS6I5dULQAX2ekAnWgPkPs9TTL6dZmw4WXqJf4dpY0JnW1tI18EChz0o2vQ+C37L3R/XYGmTzS664yq5zM0psuPI4z3bRh279nRCCF32ISI75zRr2980kCA0vTDpw7sL0Tx8QzKplX2pMx+qt2iG+b6BdOsmPX3rkTJmv8PoJNnNZzNh/DwSKg9VQnEtcHF6XyAbsQZ7FB2IB73vF+MC+4ejBP+d/4xG1g5C91XpwHChnO5Qetx4s9dZtWgPDy8A4DHoFMoppEodTTm+Vu4nM+DS38ATm7W+wgh1zpC+HyeunfpQwPJWf34hOuH/WbRpsIrvxp/Y8gyRqYuowX8V9yFZtUOILgjcHwfdGgbAXX7LgBRBlwcRenDdb4AB2iC9wNjE12jr8jUC+BEJrpP7ACTIrhcrZ3L3fWhGSV3IZpxaEKSmkidUly0e2I0yjJ0QkjqCBGfUn3yzTt69pu6YF16Jf+ck3f/OrNdT1pk6VAU63LA2BkoL4Yw2npiL60BYnFh3w9gKjyV6w2QThNspTRgoR9AwTo0RVtRxtAU+6Rn0LybjzIsBCUpiBYb6jcSZcsP6ISQ1BFiT8/us98/ccnOv8LKu2hW+YG1Lz1Txls+KF/nA93QtccYgS4cF0V7AOpGdBtRAHDiAq1h4BcmgK1o1mcBdB6FO+c2LnnwWx6fgFK2AcU3FqUPLefD0BCAH75GJ4SkjhAlWVyRShtU0JCNZvxtxvDkmx5+00rPe1AyC4BodO3xiUIXiov8s1wu8xStk9wOYNNplK9QbvSnUcdmjEi54b7XcuC/0BxCqbOhWGk5K9FJKKvRCSGpI4TJiyvgFdQpEI++nq2+it+2fE85lNTADBPKMSAGXQS+UWjCo3BxwI6Ls7ROYC+AkjSAjNUAxttpXPbyLRlAKfRLQDnHlRuDsiGdKyGEpI4Q7a4ZPOnRJVtOF53pguf24w/2pXX09pYqiDOjFDtTxxhRvxreHhd7UULie8S3RTlwZa+IpwF8VwnQ9XoaFxuAkgXmBJTzXLmxQQA1G2gNISR1hPDqlHjLYy+v3nPi5Ler/zw64PCri85R/VL6Kwe+e7o/rZCMZgOY0ASqny+KXyREA41M10lFeXzfnr1zUfZV0TrDUDYWgP0DlN/QgMGApnMvlH1ALUowbpma3ARiBqEst+CeEDJfR4iaujIUs7dP19iIjlEhQYEBRuulc7vXns4vKCqstEJKlPVh4uInz5pzcsfHxypomWt7nAD4y8TIpTUo3YCQuMMAQVEQ0ejLBIUZKL3NZvqj7L0UTavEty0A8lKncWQjgHmCFY3JgM5m0dZM5iE7AFJX/PrMepS+uFVrBTAa0Vm0TYMJp6naGAcPJuCWEJI6Qvj7DYhs3yWoXVCMp91eUlxw6mxWRkVpeR312nhiCs3//vs1/r1HDRpauv27wxdoAa8Z8wEykjrusAH0jwX8ow4DBLaDEDRRuDhYCeDbEejraQFqT0bTKiGJ6wA+nUYqinWEDSVgbVd033VCeWPyHa/WALV3LMm6gDIWt+7wAnh2pgnN/D8CxG3wod5Y30qA5Qk0JISkjhAYwtt3CItpb+vY7f58/7KsMzmXLpwvoDF2HMp37CBoyLi5lRlHP6kopTm3v34eIDMTzXw/wCMKJdAD2oXmA4ZYXOyuAYjtAAT3PASwaxStYkhaB7C5OMiKYjuPJrcWh5oLKKX0unsxgG0bmvGJuFWAUoRDcTGAyYZT+9FrAL54PgAhJHWEgLoaAEN0VGjHyA6hnmWVhgpTTuaJ6D9toeWKv/iCXyT26VlJrZmmdVg5qoZ6909BiUCJBtqF5QMecSg2lDo7h1GigwDv3ocAvgN7HYq9YRLW4WSrrwxAubRmhoHL+RpcO+ABC3YeoF7vpejq3L4a7okdJ7wN2JxtJ68ByP5sBkJI6ggBkXf5R4UH+VJcXFick1VSWFhSAQROzqKVdu6ke2iKieYMWTU7C535gUVoouqzJzgQwBSBUoNS4WPZjdLDCBh6oeys8aqzo1jQOFcr9LpzBCpV57YFACtmuOZGuQ2qwGWMgK/v/AqH5PfaobFXotTimmx6zbmp719vWw2Q7F8OsHS6B0JI6ghB6C9yM/blZFfU1NbiFObpR+ulg4lmjU9YuCWjFI+OfecMR5f4sM3DZp8KGH/TO5DqsHCUsZ7eYPU3F04v9MZeOgllXGYA1PqXhMXMt5iguj/1+s/R2ntR70ZrAJR3AyIX7GsD5Z1JfshocAZGYBjG3540O2OqL9Bu/Vsrs89BROcp95vRmWefNWO3jEQTOs9kwKFiEF1+PP76zVgzM/p7QXUyQKeX9reBioiqNvwHCWEAOz8DQnw9hkZ0/fucwzTJf1vfquh8Ghi6lZawHM6uDu8ewc9YdvoPAb278P+CEAY8+HkQwuxVw7+OgRbxHDCAn7noaH5iQsgsUSGEEJI6QgghhKSOEEIISR0hhBBCUkcIIYSkjhDWGhpRZi2jaeV12EtpqBz3hBAyS1SImAeqPWjIEhl9T4YXTbD6heM5t7hB16pruZoJIbNEhRBCCANG/i8QQgghz3WEEEIIIYQQQgghhBD/A3sg8oe/QLyzAAAAAElFTkSuQmCC'));
            self::$_getLogoToMerge = $imgLogo;
        }

        return self::$_getLogoToMerge;
    }

    public static function getTracking($track_id, $receiver_tel)
    {
//        $receiver_tel = self::formatTel($receiver_tel);

        $params = new stdClass();
        $params->PackCode = $track_id;
        $params->PhoneNumber = '';
//        $params->PhoneNumber = $receiver_tel;

        $response = self::_soapOperation('GiveMePackStatus', $params);


        if($response->success)
        {
            $statuses = [];

            $xml = $response->data->GiveMePackStatusResult->any;
            $xml = simplexml_load_string($xml);
            $xml = $xml->NewDataSet->PackStatus;



            if($xml !== NULL) {

                // add one minute, because orginal time is too soon :)
                $date = (string)$xml->Data;
                $date = new DateTime($date);
                $date->add(new DateInterval('PT' . 1 . 'M'));
                $date = $date->format('Y-m-d H:i:s');
                //

                $temp = array(
                    'name' => (string)$xml->Trans_Des,
                    'date' => $date,
                    'location' => '',
                );

                array_push($statuses, $temp);
            }

            return $statuses;
        }

        return false;
    }

    protected static function _soapOperation($functionName, stdClass $data)
    {
        $mode = array
        (
//            'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false)))
        );

        $data->PartnerID = self::PARTNER_ID;
        $data->PartnerKey = self::PARTNER_KEY;

        $return = new stdClass();
        $return->success = false;

        try {

            $client = new SoapClient(self::URL , $mode);
            $resp = $client->__soapCall($functionName, [$functionName => $data]);

        } catch (SoapFault $E) {

            MyDump::dump('ruch.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('ruch.txt', $E,1);

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            return $return;

        } catch (Exception $E) {
            MyDump::dump('ruch.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('ruch.txt', print_r($E),1);

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            return $return;
        }

        MyDump::dump('ruch.txt', print_r($client->__getLastRequest(),1));
        MyDump::dump('ruch.txt', print_r($client->__getLastResponse(),1));

        $return->success = true;
        $return->data = $resp;

        return $return;
    }

    protected function makeOrder($returnError = true)
    {
        $params = new stdClass();

        $params->FirstName = $this->FirstName;
        $params->LastName = $this->LastName;
        $params->CompanyName = $this->CompanyName;
        $params->StreetName = $this->StreetName;
        $params->BuildingNumber = $this->BuildingNumber;
        $params->FlatNumber = $this->FlatNumber;
        $params->City = $this->City;
        $params->PostCode = $this->PostCode;
        $params->PhoneNumber = $this->PhoneNumber;

        $params->SenderFirstName = $this->SenderFirstName;
        $params->SenderLastName = $this->SenderLastName;
        $params->SenderCompanyName = $this->SenderCompanyName;
        $params->SenderStreetName = $this->SenderStreetName;
        $params->SenderBuildingNumber = $this->SenderBuildingNumber;
        $params->SenderFlatNumber = $this->SenderFlatNumber;
        $params->SenderCity = $this->SenderCity;
        $params->SenderPostCode = $this->SenderPostCode;
        $params->SenderPhoneNumber = $this->SenderPhoneNumber;
        $params->SenderEMail = $this->SenderEMail;

        $params->DestinationCode = $this->DestinationCode;

        $params->CashOnDelivery = $this->AmountCashOnDelivery > 0 ? true : false;
        $params->AmountCashOnDelivery = $this->AmountCashOnDelivery;
        $params->TransferDescription = $this->TransferDescription;


        $params->PackValue = $this->PackValue;
        $params->Insurance = $this->Insurance ? true : false;

        $params->PrintAdress = 1;
        $params->PrintType = 1;
        $params->ReturnFirstName = 'Dział';
        $params->ReturnLastName = 'Wysyłek';
        $params->ReturnDestinationCode = 'WR-306852-46-62'; // Nysa, Krzywousego 21
        $params->ReturnEMail = 'info@swiatprzesylek.pl';
        $params->ReturnCompanyName = 'Świat Przesyłek';
        $params->ReturnStreetName = 'Grodkowsa';
        $params->ReturnBuildingNumber = '40';
        $params->ReturnFlatNumber = '';
        $params->ReturnCity = 'Nysa';
        $params->ReturnPostCode = '48-300';
        $params->ReturnPhoneNumber = '221221218';

        if($this->ReturnPack)
            $params->ReturnPack = 'T';

        if($this->BoxSize)
            $params->BoxSize = $this->BoxSize;

        $response = self::_soapOperation('GenerateLabelBusinessPack', $params);

        $return = [];
        if($response->success)
        {
            $xml = $response->data->GenerateLabelBusinessPackResult->any;
            $xml = simplexml_load_string($xml);

            if($xml->NewDataSet->GenerateLabelBusinessPack->Err == '000' OR $xml->NewDataSet->GenerateLabelBusinessPack->Err == '007') // accept "saved but changed ReturnDestinationCode" error
            {
                $temp = [];
                $temp['status'] = true;
                $temp['track_id'] = (string) $xml->NewDataSet->GenerateLabelBusinessPack->PackCode_RUCH;
                $temp['label'] = $response->data->LabelData;
                array_push($return, $temp);

                return $return;
            } else {
                if($returnError)
                {
                    $return = array(0 => array(
                        'error' => (string) $xml->NewDataSet->GenerateLabelBusinessPack->ErrDes));
                    return $return;
                } else
                    return false;
            }


        } else {
            if($returnError)
            {
                $return = array(0 => array(
                    'error' => $response->error));
                return $return;
            }
        }

        return false;

    }


    protected static function _requestListOfPlaces()
    {
        $CACHE_NAME = 'PACZKA_W_RUCHU_LISTA';

        $data = Yii::app()->cache->get($CACHE_NAME);

        if ($data === false OR !is_array($data)) {

            $params = new stdClass();
            $return = self::_soapOperation('GiveMeAllRUCHLocation', $params);

            if ($return->success) {
                $xml = $return->data->GiveMeAllRUCHLocationResult->any;
                $xml = simplexml_load_string($xml);
                $data = json_decode(json_encode($xml));

                Yii::app()->cache->set($CACHE_NAME, $data, (self::CACHE_POINTS_MINUTES * 60));
            }
        }

        return $data;
    }

    public static function getListOfPlacesDetailed()
    {

        $xml = self::_requestListOfPlaces();

        $data = [];


        MyDump::dump('rudas.txt', print_r($xml->NewDataSet->AllRUCHLocation,1));

        if(is_array($xml->NewDataSet->AllRUCHLocation))
            foreach($xml->NewDataSet->AllRUCHLocation AS $item)
            {
                $data[] = [
                    'id' => $item->PSD,
                    'lat' => $item->Latitude,
                    'lng' => $item->Longitude,
                    'address' => $item->StreetName.', '.$item->City,
                    'details' => is_object($item->Location) ? NULL : $item->Location,
                    'hours' => $item->OpeningHours,
                    'name' => $item->DestinationCode,
                ];
            }

        return $data;
    }


    protected static $_getListOfPlaces = NULL;
    public static function getListOfPlaces()
    {

        if(self::$_getListOfPlaces === NULL) {




            $data = [];
            $data['COD'] = [];
            $data['NOT_COD'] = [];

            $xml = self::_requestListOfPlaces();
            if (is_array($xml->NewDataSet->AllRUCHLocation)) {
                foreach ($xml->NewDataSet->AllRUCHLocation AS $item) {


                    if ($item->Available != 'T')
                        continue;

                    $address = $item->StreetName . ', ' . $item->City;

                    if ($item->CashOnDelivery == 'true')
                        $data['COD'][$item->DestinationCode] = $address;
                    else
                        $data['NOT_COD'][$item->DestinationCode] = $address;
                }
            }

            if(!is_array($data))
            {
                $data = [];
                $data['COD'] = [];
                $data['NOT_COD'] = [];
            }

            self::$_getListOfPlaces = $data;
        }

        return self::$_getListOfPlaces;
    }


    public static function validatePoint($point_code, $cod)
    {
        if($cod)
        {
            if(array_key_exists($point_code, self::getListOfPlaces()['COD']))
                return true;
        } else {
            if(array_key_exists($point_code, self::getListOfPlaces()['COD']))
                return true;

            if(array_key_exists($point_code, self::getListOfPlaces()['NOT_COD']))
                return true;
        }

        return false;


    }

    protected static function formatTel($tel)
    {
        $tel = trim(preg_replace('/\D/', '', $tel));
        $l = strlen($tel);
        if($l > 9)
            $tel = substr($tel,($l-9),9);
        else if($l < 9)
            $tel = str_pad($tel, 9, "0", STR_PAD_LEFT);

        return $tel;
    }

    public static function orderForCourierDomestic(CourierTypeDomestic $courier, $returnError = true)
    {
        $model = new self;

        $from = $courier->courier->senderAddressData;
        $to = $courier->courier->receiverAddressData;

        $model->DestinationCode = $courier->_ruch_receiver_point;

        // split name to ime i nazwisko
        $address_to = trim($to->address_line_1.' '.$to->address_line_2);

        $model->FirstName = $to->name;
        $model->LastName = $to->name;
        $model->CompanyName = $to->company;
        $model->StreetName =  $address_to;
        $model->BuildingNumber = '0';
        $model->FlatNumber = '';
        $model->City = $to->city;
        $model->PostCode = $to->zip_code;
        $model->PhoneNumber = self::formatTel($to->tel);

        $address_from = trim($from->address_line_1.' '.$from->address_line_2);

        $model->SenderFirstName = $from->name;
        $model->SenderLastName = $from->name;
        $model->SenderCompanyName = $from->company;
        $model->SenderStreetName = $address_from;
        $model->SenderBuildingNumber = '0';
        $model->SenderFlatNumber = '';
        $model->SenderCity = $from->city;
        $model->SenderPostCode = $from->zip_code;
        $model->SenderPhoneNumber = self::formatTel($from->tel);
        $model->SenderEMail = $from->fetchEmailAddress(true);

        if($courier->courier->cod_value > 0) {
            $model->AmountCashOnDelivery = ($courier->courier->cod_value * 100); // wartosc podawana w groszach
            $model->TransferDescription = 'SwiatPrzesylek no '.$courier->courier->local_id;
        }

        $model->PackValue = ($courier->courier->getPackageValueConverted('PLN') * 100); // wartosc podawana w groszach


        if($courier->courier->package_weight <= self::MAX_MINI_WEIGHT)
            $model->BoxSize = 'MINI';

        if($courier->hasAdditionById(CourierDomesticAdditionList::RUCH_INSURANCE))
            $model->Insurance = true;


        return $model->makeOrder($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierU, $returnError = true)
    {
        $model = new self;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->DestinationCode = $courierU->courier->_ruch_receiver_point;

        // split name to ime i nazwisko
        $address_to = trim($to->address_line_1.' '.$to->address_line_2);

        $model->FirstName = $to->name;
        $model->LastName = $to->name;
        $model->CompanyName = $to->company;
        $model->StreetName =  $address_to;
        $model->BuildingNumber = '0';
        $model->FlatNumber = '';
        $model->City = $to->city;
        $model->PostCode = $to->zip_code;
        $model->PhoneNumber = self::formatTel($to->tel);

        $address_from = trim($from->address_line_1.' '.$from->address_line_2);

        $model->SenderFirstName = $from->name;
        $model->SenderLastName = $from->name;
        $model->SenderCompanyName = $from->company;
        $model->SenderStreetName = $address_from;
        $model->SenderBuildingNumber = '0';
        $model->SenderFlatNumber = '';
        $model->SenderCity = $from->city;
        $model->SenderPostCode = $from->zip_code;
        $model->SenderPhoneNumber = self::formatTel($from->tel);
        $model->SenderEMail = $from->fetchEmailAddress(true);

        if($courierU->courier->cod_value > 0) {
            $model->AmountCashOnDelivery = ($courierU->courier->cod_value * 100); // wartosc podawana w groszach
            $model->TransferDescription = 'SwiatPrzesylek no '.$courierU->courier->local_id;
        }

        $model->PackValue = ($courierU->courier->getPackageValueConverted('PLN') * 100); // wartosc podawana w groszach


        if($courierU->courier->package_weight <= self::MAX_MINI_WEIGHT)
            $model->BoxSize = 'MINI';

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_RUCH_INSURANCE))
            $model->Insurance = true;


        return $model->makeOrder($returnError);
    }

    public static function orderForCourierInternal(CourierTypeInternal $courier, AddressData $from, AddressData $to, $returnError = true)
    {
        $model = new self;

        $model->DestinationCode = $courier->_ruch_receiver_point;

        // if package is to go through NYSA, then mark it as ReturnPack
        if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH && $courier->courier->receiverAddressData->country_id != CountryList::COUNTRY_PL)
            $model->ReturnPack = true;

        // split name to ime i nazwisko
        $address_to = trim($to->address_line_1.' '.$to->address_line_2);

        $model->FirstName = $to->name;
        $model->LastName = $to->name;
        $model->CompanyName = $to->company;
        $model->StreetName =  $address_to;
        $model->BuildingNumber = '0';
        $model->FlatNumber = '';
        $model->City = $to->city;
        $model->PostCode = $to->zip_code;
        $model->PhoneNumber = self::formatTel($to->tel);

        $address_from = trim($from->address_line_1.' '.$from->address_line_2);

        $model->SenderFirstName = $from->name;
        $model->SenderLastName = $from->name;
        $model->SenderCompanyName = $from->company;
        $model->SenderStreetName = $address_from;
        $model->SenderBuildingNumber = '0';
        $model->SenderFlatNumber = '';
        $model->SenderCity = $from->city;
        $model->SenderPostCode = $from->zip_code;
        $model->SenderPhoneNumber = self::formatTel($from->tel);
        $model->SenderEMail = $from->fetchEmailAddress(true);

        if($courier->courier->cod_value > 0) {
            $model->AmountCashOnDelivery = ($courier->courier->cod_value * 100); // wartosc podawana w groszach
            $model->TransferDescription = 'SwiatPrzesylek no '.$courier->courier->local_id;
        }

        $model->PackValue = ($courier->courier->getPackageValueConverted('PLN') * 100); // wartosc podawana w groszach

        if($courier->courier->getWeight(true) <= self::MAX_MINI_WEIGHT)
            $model->BoxSize = 'MINI';

//        if($courier->hasAdditionById(CourierDomesticAdditionList::RUCH_INSURANCE))
//            $model->Insurance = true;


        return $model->makeOrder($returnError);
    }

    protected static function statIdToName($stat_id)
    {
        $data = [
            100 => 'W sortowni regionalnej',
            110 => 'W Transporcie do SC z Ekspedycji',
            200 => 'W Transporcie od Nadawcy',
            201 => 'Anulowane awizo',
            210 => 'Nadana w Kiosku',
            230 => 'W Transporcie do Ekspedycji z Kiosku',
            300 => 'W sortowni centralnej',
            400 => 'W sortowni centralnej',
            450 => 'W Transporcie do Ekspedycji z SC',
            653 => 'W Ekspedycji',
            680 => 'W Transporcie do Kiosku',
            690 => 'W Kiosku',
            695 => 'W kiosku SMS wyslany',
            700 => 'Nieodebrana w Terminie',
            709 => 'Powrót - Nieodebrana w Terminie',
            729 => 'Powrót - Niepoprawny Kiosk',
            749 => 'Reklamacja',
            790 => 'Zwrot do Ekspedycji',
            800 => 'Zwrot do Sortowni',
            900 => 'Zwrot do Nadawcy',
            1000 => 'Odebrana przez Klienta',
            1100 => 'Odebrana',
        ];

        return (isset($data[$stat_id])) ? $data[$stat_id] : '?';
    }

}