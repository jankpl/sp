<?php

/**
 * CourierCodSettledSettleByGridView class.
 * CourierCodSettledSettleByGridView is the data structure for CourierCodSettledSettleByGridView form.
 */
class CourierCodSettledSettleByGridView extends CFormModel
{
    public $package_ids;
    public $type;
    public $settle_date;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            ['package_ids, type', 'required'],
            [ 'settle_date', 'date', 'format' => 'yyyy-mm-dd'],
            [ 'type', 'in', 'range' => array_keys(CourierCodSettled::listType())],
        );
    }



}