<?php

Yii::import('application.models._base.BaseDebtCollectionNotification');

class DebtCollectionNotification extends BaseDebtCollectionNotification
{
    const TYPE_MAIL = 1;
    const TYPE_SMS = 2;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),
        );
    }

    public function rules() {
        return array(
            array('user_id, type', 'required'),
            array('user_id, type', 'numerical', 'integerOnly'=>true),
            array('id, date_entered, user_id, type', 'safe', 'on'=>'search'),
        );
    }

    public static function mailSent($user_id)
    {
        return self::_itemSent($user_id, self::TYPE_MAIL);
    }

    public static function smsSent($user_id)
    {
        return self::_itemSent($user_id, self::TYPE_SMS);
    }

    protected static function _itemSent($user_id, $type)
    {
        $model = new self;
        $model->user_id = $user_id;
        $model->type = $type;
        return $model->save();
    }

    public static function hasMailBeenSent($user_id)
    {
        return self::_hasItemBeenSent($user_id, self::TYPE_MAIL);
    }

    public static function hasSmsBeenSent($user_id)
    {
        return self::_hasItemBeenSent($user_id, self::TYPE_SMS);
    }

    protected static function _hasItemBeenSent($user_id, $type)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from((new self)->tableName());
        $cmd->where('DATE(`date_entered`) = CURDATE() AND type = :type AND user_id = :user_id', [':type' => $type, ':user_id' => $user_id]);
        return $cmd->queryScalar();
    }
}