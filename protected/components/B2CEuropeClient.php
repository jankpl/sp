<?php

class B2CEuropeClient extends GlobalOperator
{
    const URL = GLOBAL_CONFIG::B2C_URL;
    const USER = GLOBAL_CONFIG::B2C_USER;
    const KEY = GLOBAL_CONFIG::B2C_KEY;

    const SHIPPING_METHOD = 'PARCELPLUS';
    const SHIPPING_METHOD_COD = 'COD';
    const TYP = 'QI0B';

    const FTP_HOST = 'ftp.b2ceurope.eu';
    const FTP_USER = 'kaab';
    const FTP_PASS = 'e1b98KaB4@60f29Lza1';
    const FTP_DIR = 'Tracking/';

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $weight;
    public $value;
    public $content;

    public $cod = 0;
    public $cod_currency;
    public $size_l;
    public $size_d;
    public $size_w;

    public $local_id;
//
//    public static function test()
//    {
//        $model = new self;
//        $model->_createShipment();
//    }

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    protected function _createShipment(Courier $courier, AddressData $from, AddressData $to, $returnError = false)
//    protected function _createShipment( $returnError = false)
    {

        $receiverTel = $this->receiver_tel;
        if(strtoupper($this->receiver_country) == 'ES') {
            $receiverTel = ltrim($receiverTel, '0');
            if (mb_strlen($receiverTel) == 9)
                $receiverTel = '+34' . $receiverTel;
        }

        if(in_array($this->receiver_country_id, CountryList::getUeList()))
            $outsideUE = false;
        else
            $outsideUE = true;

//        $outsideUE = true;

        $item = \FluidXml\fluidxml('Prealert');
        $item
            ->add('AuthenticationKey', self::KEY)
            ->add('LayoutType', $outsideUE ? 'P02' : 'P01')
            ->add('LayoutVersion', 2.2)
            ->add('LayoutPlatform', 'L01')
            ->add('PrealertReference', 1);

        $shipment = \FluidXml\fluidxml('Shipment');

        if($outsideUE)
        {
            $shipment
                ->add('OrderNumber', $this->local_id)
                ->add('OrderContent', $this->content)
                ->add('ShippingMethod', self::SHIPPING_METHOD)
                ->add('CountryCodeOrigin', $this->sender_country)
                ->add('PurchaseDate', date('Y-m-d'))
                ->add('Currency', 'EUR')
                ->add('CustomsService', 'DDP')
//                ->add('TYPNumber', 'TYP'.self::TYP.rand(10000000,99999999))
            ;
        } else {
            $shipment
                ->add('OrderNumber', $this->local_id)
                ->add('OrderContent', $this->content)
                ->add('ShippingMethod', $this->cod > 0 ? self::SHIPPING_METHOD_COD : self::SHIPPING_METHOD);

            if($this->cod > 0) {
                $shipment->add('CODValue', $this->cod)
                    ->add('CODCurrency', $this->cod_currency);
            }
            $shipment ->add('Currency', 'EUR')
//                ->add('TYPNumber', 'TYP'.self::TYP.rand(10000000,99999999))
            ;
        }


        if($outsideUE) {

            $shipmentContentCustoms = \FluidXml\fluidxml('ShipmentContentCustoms');
            $shipmentContentCustoms
                ->add('PackageNumber', 1)
                ->add('SKUCode', $this->local_id)
                ->add('SKUDescription', $this->content)
                ->add('HSCode', 123)
                ->add('Quantity', 1)
                ->add('Price', $this->value);
            $shipment->add($shipmentContentCustoms);

        } else {

//            $shipmentContent = \FluidXml\fluidxml('ShipmentContent');
//            $shipmentContent
//                ->add('PackageNumber', 123)
//                ->add('SKUCode', 123)
//                ->add('SKUDescription', 123)
//                ->add('Quantity', 123)
//                ->add('Price', 123);
//            $item->add($shipmentContent);

        }

        if($this->receiver_name == '')
        {
            $name = $this->receiver_company;
            $company = '';
        } else {
            $name = $this->receiver_name;
            $company = $this->receiver_company;
        }

        $shipmentAddress = \FluidXml\fluidxml('ShipmentAddress');
        $shipmentAddress
            ->add('AddressType', 'DL1')
            ->add('ConsigneeName', $name)
            ->add('CompanyName', $company)
            ->add('Street', $this->receiver_address1)
            ->add('AdditionalAddressInfo', $this->receiver_address2)
            ->add('CityOrTown', $this->receiver_city)
            ->add('ZIPCode', $this->receiver_zip_code)
            ->add('CountryCode', $this->receiver_country);
        $shipment->add($shipmentAddress);

        $shipmentContact = \FluidXml\fluidxml('ShipmentContact');
        $shipmentContact
            ->add('PhoneNumber', $receiverTel)
            ->add('SMSNumber', $receiverTel)
            ->add('EmailAddress', $this->receiver_mail)
            ->add('PersonalNumber', $receiverTel);
        $shipment->add($shipmentContact);

        $shipmentPackage = \FluidXml\fluidxml('ShipmentPackage');
        $shipmentPackage
            ->add('PackageBarcode', $this->local_id)
            ->add('PackageNumber', 1)
            ->add('PackageWeight', $this->weight * 1000)
            ->add('DimensionHeight', $this->size_d)
            ->add('DimensionWidth', $this->size_w)
            ->add('DimensionLength', $this->size_l);
        $shipment->add($shipmentPackage);

        $prealertValidation = \FluidXml\fluidxml('PrealertValidation');
        $prealertValidation
            ->add('TotalShipments', 1)
//            ->add('MailAddressConfirmation', 'jankopec@swiatprzesylek.pl')
//            ->add('MailAddressError', 'jankopec@swiatprzesylek.pl')
        ;

        $item->add($prealertValidation);

        $item->add($shipment);


        $xml = $item->xml();

        $resp = $this->_curlCall($xml);



        if ($resp->success == true) {
            $return = [];

            $TYPNumber = $resp->data->Shipment->TYPNumber;
            $orderNumber = $resp->data->Shipment->OrderNumber;

            $label = self::_generateLabel($TYPNumber, $orderNumber);


            $im = ImagickMine::newInstance();
            $im->setResolution(300,300);

            $im->readImageBlob($label);
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $this->local_id);

            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }

    }

    protected function _curlCall($data)
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

//        $dataOryginal = $data;

        $headers = [];
        $headers[] = 'Content-Type: text/xml';

        $curl = curl_init(self::URL);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt_array($curl, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        );
        $resp = curl_exec($curl);

        MyDump::dump('b2ceurope.txt', print_r($data,1));
        MyDump::dump('b2ceurope.txt', print_r($resp,1));

        $xml = @simplexml_load_string($resp);

        if($xml->Result == 'OK')
        {
            $return->success = true;
            $return->data = $xml;
        }
        else
        {
            if ($xml == '')
            {
                $error = curl_error($curl);
                if ($error == '')
                    $error = 'Unknown error';

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
            else
            {
                $error = $xml->Message . ' : ' . $xml->Details;;

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
        }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->getCodeIso3();

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->getCodeIso3();
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->weight = $courierInternal->courier->getWeight(true);
        $model->content = $courierInternal->courier->package_content;
        $model->value = $courierInternal->courier->getPackageValueConverted('EUR');
        $model->size_l = $courierInternal->courier->package_size_l;
        $model->size_d = $courierInternal->courier->package_size_d;
        $model->size_w = $courierInternal->courier->package_size_w;
        $model->cod = $courierInternal->courier->cod_value;
        $model->cod_currency = $courierInternal->courier->cod_currency;
        $model->local_id = $courierInternal->courier->local_id;



        return $model->_createShipment($courierInternal->courier, $from, $to, $returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->getCodeIso3();

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->getCodeIso3();
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->weight = $courierU->courier->getWeight(true);
        $model->content = $courierU->courier->package_content;
        $model->value = $courierU->courier->getPackageValueConverted('EUR');
        $model->size_l = $courierU->courier->package_size_l;
        $model->size_d = $courierU->courier->package_size_d;
        $model->size_d = $courierU->courier->package_size_w;
        $model->cod = $courierU->courier->cod_value;
        $model->cod_currency = $courierU->courier->cod_currency;
        $model->local_id = $courierU->courier->local_id;

        return $model->_createShipment($courierU->courier, $from, $to, $returnErrors);
    }


    protected function _generateLabel($TYPNumber, $orderNumber)
    {


        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pageW = 105;
        $pageH = 150;

        $border = 0;

        /* @var $pdf TCPDF */

        $pdf->AddPage('H', array($pageW, $pageH), true);

        $pdf->SetLineWidth(0.5);
        $pdf->MultiCell($pageW - 1, $pageH - 1, '', 1, 'L', false);
        $pdf->SetLineWidth(0.1);

        $pdf->Image('@' . self::_getLogo(), 5, 5, 40, '', '', 'N', false);

        $text = 'Shipping Method';
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(25, 5, $text, $border, 'L', false, 1, 73, 20,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(25, 5, $this->cod > 0 ? self::SHIPPING_METHOD_COD : self::SHIPPING_METHOD, $border, 'L', false, 1, 73, 25,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );


        $text = 'Order Number';
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(25, 5, $text, $border, 'L', false, 1, 5, 20,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(25, 5, $orderNumber, $border, 'L', false, 1, 5, 25,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $text = 'TYP Number';
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(25, 5, $text, $border, 'L', false, 1, 5, 35,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(25, 5, $TYPNumber, $border, 'L', false, 1, 5, 40,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        if($this->cod > 0) {
            $text = 'COD';
            $pdf->SetFont('freesans', 'B', 8);
            $pdf->MultiCell(25, 5, $text, $border, 'L', false, 1, 73, 35,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );

            $pdf->SetFont('freesans', '', 8);
            $pdf->MultiCell(25, 5, $this->cod . ' ' . $this->cod_currency, $border, 'L', false, 1, 73, 40,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );
        }

        $text = 'Shipper';
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(25, 5, $text, $border, 'L', false, 1, 5, 50,
            true,
            0,
            false,
            true,
            5,
            'B',
            true
        );

        $text = '';
        $text .= $this->sender_name ? $this->sender_name."\r\n" : '';
        $text .= $this->sender_company ? $this->sender_company."\r\n" : '';
        $text .= $this->sender_address1."\r\n";
        $text .= $this->sender_address2 ? $this->sender_address2."\r\n" : '';
        $text .= $this->sender_zip_code."\r\n";
        $text .= $this->sender_city."\r\n";
        $text .= $this->sender_country."\r\n";

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(70, 25, $text, $border, 'L', false, 1, 5, 55,
            true,
            0,
            false,
            true,
            25,
            'T',
            true
        );


        $text = 'Consignee';
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(25, 5, $text, $border, 'L', false, 1, 5, 80,
            true,
            0,
            false,
            true,
            5,
            'B',
            true
        );

        $text = '';
        $text .= $this->receiver_name ? $this->receiver_name."\r\n" : '';
        $text .= $this->receiver_company ? $this->receiver_company."\r\n" : '';
        $text .= $this->receiver_address1."\r\n";
        $text .= $this->receiver_address2 ? $this->receiver_address2."\r\n" : '';
        $text .= $this->receiver_zip_code."\r\n";
        $text .= $this->receiver_city."\r\n";
        $text .= $this->receiver_country."\r\n";

        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(70, 25, $text, $border, 'L', false, 1, 5, 85,
            true,
            0,
            false,
            true,
            25,
            'T',
            true
        );



        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $pdf->write1DBarcode($this->local_id, 'C128', 20, 115, '', 25, 0.4, $barcodeStyle, 'N');



        return $pdf->Output('SP.pdf', 'S');
    }

    protected static function _getLogo()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAAN4AAAA6CAIAAABQ0HB0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAFwmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDIgNzkuMTYwOTI0LCAyMDE3LzA3LzEzLTAxOjA2OjM5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAxOC0wMy0xNlQxMzozNzozOCswMTowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxOC0wMy0xNlQxMzozNzozOCswMTowMCIgeG1wOk1vZGlmeURhdGU9IjIwMTgtMDMtMTZUMTM6Mzc6MzgrMDE6MDAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6YTdkZWRkYjAtZGZmNS1mODQ2LTk2MTAtZmM5NTk3NjNhYTQ4IiB4bXBNTTpEb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MjAyMGEwNzktNTgwMi0zMjQzLTk0MjctZjdjNzFlZGU1ZTFmIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6OTJhNTY0YzMtYWY3Yy0yYTQ5LWIzNDctYjlhOTlkZWY2YTE5IiBkYzpmb3JtYXQ9ImltYWdlL3BuZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyI+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6OTJhNTY0YzMtYWY3Yy0yYTQ5LWIzNDctYjlhOTlkZWY2YTE5IiBzdEV2dDp3aGVuPSIyMDE4LTAzLTE2VDEzOjM3OjM4KzAxOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDphN2RlZGRiMC1kZmY1LWY4NDYtOTYxMC1mYzk1OTc2M2FhNDgiIHN0RXZ0OndoZW49IjIwMTgtMDMtMTZUMTM6Mzc6MzgrMDE6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+gO1ZJAAAKxtJREFUeF7t3WW3JclxheH+E2KwmNFiZmZmBoMsZouZmWUxM4PFzMzMzMyS7WfmbYXT51LPLGlWj9fkh9N1q7KSYscOyDyn9/33oeUvf/nLf/21/OEPf/jTn/70jne842lPe9qT/s7lyU9+8n8sxZ9Tpuf1puu/84ieNF085SlP+cpXvvLnP/+5hWmJ/GlxLNHd7373u971rne5y118/vtfiz/n2sWd73xnn26++MUv9q51rp3W/Kiy+wrs67HlntX/zW9+89vf/vahD33oTW5yk3/+25V/2qHc9KY33XjizhRj2Ch/uxFt31Ld3fCGN7zWta711a9+1coAkwKUClC6/sY3vnGCE5zg+Mc//vGOd7x/+L/F/bnhqTrHPvaxX/3qV4fIwH0UKA9kBTah6Z0//vGPBHDLW97y5je/+b/uUP5l57LtG6pvCwQQvPGNbwwK0KmCaje72c1c+LN2elG1bqp8OKC5MdhacHPboXr0b4eW61//+r/4xS8gKWjCaJTp+q1vfSvMBUEXxz3ucbuohMgqHOc4xznZyU72y1/+MtIN4gcimKPq7IfmmC0r4vqnP/3pda5znVvd6lY74WAnCgxhW8su9XsFSoAyXA40w6XPSDTgrpx6gNfTu/rUYN7aaajUEjSZY9ajlQmaCr1VnvjEJw4uhzhPdKITDSiDJsi6o8cAPS7TUbA7kBXYD808oZxO5VOf+tS1r31t4tmJNXeC7E5UtFM7oKYX9KzUXdR4oxvd6HrXu94NbnADhtUnMPXU5y6EvdOjdbSNsDvbjsoA0pDHPe5xoAmIw3ZgGjRvd7vbRYpxJFCe6UxnOv3pT3+GM5zhjGc84z/+tZz61Kd287Wvfe3vf//7oJlNPxDBHFVnPzTzhHKnXL/85S9nzghpJ1ra6gLOnZ1e2YU4M+vg6OIBD3gATDz72c9+yUteYhgvfOELn/GMZzz84Q+HlcNnzWPKSrCLerW27VDdVyD4Fa94xe9+9zu+zQrNYqDLXe5yKzQvdalL/frXv3af4Q67Uaw7WuiihT0Kmgeucv/LmlawhfvVr34lSsUoJASg+IxQ2bjHPvaxT3/605/61Kf6FL26EMIr/lQAaC5ce/qEJzxBGJvrphGw0Mitb33rIJKxvsUtbqGXe9zjHq95zWuEw7FUGoJpcu+65mO86lWv0g4S9QleWrjtbW/rGqbzSlFswHIT7NzUF6w///nP//CHP/zd734XdGAIXH7+859/+ctffsMb3vCYxzxGI1oztoZqbLX57ne/OxsybFcMZJCnPOUpx9fEnbe5zW2y+1puDQt3JqhfoygTVG3c1gGuxsN0zoNrs3azdRhnt9fdLFrtlWrWo8/0oT+7qB2f9VI44fVUSAVC16D73axa42yo42qPV1O/3nIx2uu6O7XTsjTIem8kPe1iJ7BuA00v3/Oe9wyayvh/P/zhD2chGuuMeGMCrQUQ6PvrX/86z+ya17wmFGqTyOMkfwLZgx70oM985jNDLRMrJIwZ9PT1ve99z9jG9dQIPGkzIoyYNe7zjne843Of+9xvf/vbxpCQRiRdJPtA/8pXvtJgNNLYNEUnvduL4yamNlRoopw8Tko7KzAOZVJRmksL8pGPfISe3P/+9zdyCuBOYBqxWeS3ve1tj370o43hzW9+86BN12p+8YtffNjDHna2s51N6qDxv/e977WGV7va1S5wgQtY5K997WuQEUyrQCfZHAty+ctfHtmboz+tuVFZ9oY3g1c/JElBPOpRjzrPec7j9Sp8//vfx0cW55KXvOSVr3xlpPOJT3wCsExhhWZdux+4v/CFL5iv9NlVr3rVi170ope+9KU5iqb/nve8Z/Rkb2iOAMwtP4zICSxICYlGiUdgG6SSJGat02mfGnzLW97Cd7RAiK1AxHA//vGPm1iLGHSmhczolNYxDkB+XL2ioqy8ccbKjRkoEV7UMlS0Kk/DTrN1angKLyKntmKQKDYKrH6vKK973esmGI87sW+sMHDMO+ozyVGqE5/4xFJLYvYiJOMP69VxTXgnPOEJOa9loOB4lP9Nb3qTwLSnikQpniOUk5zkJP503yvnOMc5ooMI7/3vf/9lLnMZHZXnqmvFKz5ZCUNq/RNB+KPPFvMUpzhFzdI6Dcoln/a0p21UPgvvZB7cb5HHUKRmPukYcZz85Cc/5jGP6RWFR55rrlntXOUqV/nSl750QKw5OPjWt74FRoXJJASaLiirLhNAk+9iNSUjxWkqlg2gEqVZTGB63vOeZ2VbkakzuFyJqpsWurVLVG9/+9tnhOWeDJKc4N44YVf97N064GSQXjVUn2H3Xe96F0xrAVw0aL4S6dFtoWHM11z4AFKVKzpxTKqlwniTK5pdo0DyINQBH1oaQvUuWAT0kKTaT37ykxjLIK90pSsd61jHcrNEFRfoile8YlgpReBTg8nF9DGTQaofIBR/DugbycUudrGxhOUiPve5z530pCcdAHmXA2Zl6qIURJ2mEu5YulnPpGPdsPJZznKWwkQKObkLFyG7dTjvec9LWHuw5rRufCgnuzZpHRe2bFahhqrGMbhJGFXraSvbzXe+852gaar4KYcvSh+WzdfprdRgLlY+9qJ1NLyi9fSHnfJpkCGyZmdStRZeV5Sr4BGXo9g/F9Y1a/74xz8+N0PXTbZxKmoGBavs8zSnOc2APhg1/V6fRUAwk/4kLTITubc+aR19S2xk5uk5z3nO9LY1YQo8ItqRd0QYCXmE53784x8HTRap+0E8rKszoVvq4c7tb3/7plYv3pVkWLUuINaU3pvCaJc7l73sZYen0l7OBlzWF32YjO+oXK83JN6FuVvkrQHiZspd0y94wQuKZIthc+Ne//rXJ+kaGmGk98N/Q4EDykHVN7/5zbGVou+ZTysCTwOdbPFAP3ysxl13OHiynhCPRPlnMuS1NuzYn/Xlpj/nuj+9cre73a2YqeHVmuRAsNBvjTR91xe5yEUSnvW19GwW28T9qiAzf/pkGeuiwciSRnvJjFRwbRUaFU9u4K6OMbQIfWLBsBgcM9NaO9WpTsX1ZFvvda971Y4YNAAFZbb73Oc+t2wDP5VkJbPc6d24kFk3L70EUE7FRnY26J/1rGe14DzFSZnlZ2uNgzszJTvTbyIeUUJLJEz86Ec/+oMf/IBbop2UJO684AUvuAc0hxVMj3tRfjFRIQmEhKKHFMemt+hj2TMKwxwuEmfW08h4S4VBGmTgctj1GM9R+je+8Y0sMhfbp9xqct3wO7PCWC13s2KrGsj0kiM/lNnrNf6hD31IpxxwRqQo0joChOUGx9JJZQ+ue93rfvCDH0w3YoI8XYUDKlvZ4o73NpuWCduf5MH9n5EYhihkeE4dJFf82+rpAoHlhMWdD3zgA1OnhnHve9+7XgJEBMa14BdZPaP60Y9+pDL9B9OGEWmd/exndzPtUmRU9JKtVw164DVMt7CwkuLVQkO6733vW0fGLAw9xjGOkYZ4BHwvetGLUl3tyMwEfV345KGK6hJ0a45Bxqxr3DrsAc1p2hC5WcExgWXpfvaznw15MMe6SVGEafTV0B/84Ac/8pGPdCikFR/zN4tigfiCIUnjYo73ve99ZR8sK30ovh6r6ropWZEV4q7d4VfldWSCGeXoJxttDBlud4xWGotKKPkAXkS6Am2ui2HkEii15k8GPXHWoHYGmryxrHnyKyDI6g2VdgdPDNNbEIAmCWJTE31e4QpXGJVunGE371CzL33pS1fdMPJQkofnk7+eBZgRuobv1YgXMEW9dffZz3428g52AHS/+91vSAf+GIGmM0rCATN9y+ipRuh2/mstuIC2wK2C3YZebIkAo0F6FP3LWOdsVAd294BmYDICTUCk8C3TFpLudKc7jUUbQoIPndGY+BUjki4zhJ+igboMoz4BscxOLZdBlJvAT6VOp0f4KOIG2eYzTQ34LKhGSMIna9X9qdZaBPqHPOQh6VjQnyKdaZql8fOqZ4sIiElikNEs0jF9xV5RYF6jJV5LMJU67RWjElkmsEQOoJIMWZiEajEJadjO65/+9Kdn6VTLzmbQAZd7MBTQ2AySNUBC0VWgQZkerXEG25VGDUMTbmPQi06HL9M6qmsKKWcmDhlNksH0VWPiauFZz3rW6G1eR1mqnmrhO9/5jgRWQWQ2hwu0dxiU8uGS2CWgKIDCcDT58TVdqMyOYDtoa+uPgKXWQDMFzUbUrD+Nu/wO7qxZsHAdptfAqwZVIN3BR25yK2iSgUlMzb5EkAPNDFPcaReAtsxW0ArNXIsVlANN1rOYbPQw8btDDAEo9rL6YmdJO0VyUbnGNa7hDtUa782AZZcGmsEakYfLPrF+XNJT8e+kPKMMPuXogKYkYkP2EJ5Z827HnQ0Z5VVW2bMGYTcl0aaYqampiWiy9eOiMPfpwCAMlQTfeFFNeescD4uw+qnGzMhQGIj0FvblFk+cl7us/b2hmY9vESlKMQFRZdaf85znDDS7yMJKPYgcYUjupi0Z2jxWOCPYCnIEUVSbLrl05SBDYR5eQJliF8dsw+JssYRUfndb/F4R2I7lTXlUTuTUbPzIaXbYsQnmYnazOgYjkii+TvAjfndUSKI+La5tocHQyC9J93pjI5UVmq4lesftUdmfxS5ZfPnzXs8OmG9QiMkUqezxMaaauYRsjeRKyoy2elM+9rGPhSef6vAa2Z+ApUfZe11EinXE489HGmiyG63ALILNZHXsXHAGuj9lXavxImrcGLCSfvdOuYcDbnIENtIyYXmfKDDeijuN1X6J1kkL+akG0wMUT2cbzSsSJZrtiEbbifmIq4mf43DBxcGz1dGsx8RAVbyoPqM8Kae0ZYyv3vF0offK6ys0s/I5LTNfClNaJ3yv0PSnaLewN/HbPZ9kwgwgup372jHODWjikpXzRHUjb5IzqgGuGUnyZ8oLL1htsy7tuiqPsSXywEdtCkxX2dvsDbsRJyDKuA34SHCYWzX2mmE0/sxgC8LL30Ce+NIjYcYwcRUyLJMDTuuyOe6bY43vAc38CZ86TmBElVDFBLabhzXVSfwuQFam4OpXvzqjyYt/5jOfWRK+9WpKSEXqWPogI56vmWUfTJQKCG3us4nqZ0fShNalU4+uhfC5qthInfy5QUaoErdh9PYDB47rRRy5cccwaA7rs4HLZgQQiGFsFsnBcby+oiQNGQVWQdADCoUv0OCY0tBqMyptGVyAIyZLFbXD+ufVFTpc+MIXTliejtfkuqdlDxTuafwdM9UaByxcBl+4cXohlCt2JvkSZZ00JdETdJK4Auu85J4OEPGlanwnLzZOfoVEQSjXxaAzJjYA820994amYZU852YFlKCZTRdMDCJDXmOFFXtN/CRZVsbduGPK4BIudc+fgO9pcIVmZnSg6c9C6fIR+Rj1VcuKGNOQsubcj22hqRpaCvpqHgg0Q6qRUDPTSfDKCjsztfSTryYbnu4oRpWHxoa/DcYW4rARQOhiAB1GnagfG6pZYl6tBFUMB2GX6g6pZ/EVSx0npTmuOTNTbQZjyyNmHQITqcwK2z6Y7USNoJuBZppG+he/+MWz+BXuY9yM+FMeN10c/ehHl5SYMbuwdBe60IVEXRgteGhcg3v7muamlD3Jxew4o8mUemgCGy5O1Gh9Q3ZWLGlpTRxjF6QEzThzASUb2ufwdIAri5YlSthpfzl5/hBiqyY+HugMr1eZ9yY9GVVvC82tNweaGHeIZIWmtGt2fGBE61qZFmcqr8RpkEEhxBAbohq/yFsWioCTaAG47e8VfKKrrHmokq2bRU57NWKfM0+0XnxKAtbIjM1bCLumwrptm7ZztUAh83fDnGvZ4oF+HQlo5vW40/ImI7FBL5ait+HJ1zcqJlfx4uRxC1sziQcETRRocPEl3kqooCnKC5otfdBsMjWdfveZ5VVgS35bCxgi9grx+XYT0c+jcfXszYjx56jVQDM9Q5Ma7BS6feRmmEIPRPRuuQmmNGp97VmGvKm1wa/IqIuxhgNNQhJnBLIxeStxxqDcg8gsbHk9jeqp1xklFnCShUyqOylYmi9ZGFWHPPqw1YuwaBnZ1EZfDGBymb4KtiYAp2ZYvzpGYsMvRIYwVrgNrVEzdexQzFN9aYGa1YJUccNrmi7anAs260jGe6zfPVgzkjN00Gw/ejw/HmSKVUPxcJ0ppbua/4izOrBuuw8+ylOuO4E5mtnxovXyR/KUEm9ha+UV/QL9Jz/5yfEOvSJnESUEzeonS96FmhRsw5tcATqhT7RdwOSTN5aFalmbbMiTq1pZk4y5WU12ln4Wp+UyKkhKVKXcOXBNJ//EAiKnzGuUiUFb0tAZHQ5iXHQiaTA31ixjGjGznpOwTEaKqKAQJASrI5HZ0iky5+ODpgay6z2qDr443/nO11wCsbwp/KVCAugJ7atga7Sla5pNp6kxp5Gdz70Nutdk6smmo0aJCqrsew5ljssSWdZlO0MZ9MQ5FiQ8aRAOMqxju1f7riPGl9rJutVUjSfaFIBFGP5rbLIYq07r1CSbvLQw7zYLsBM6I+wAeujB1EMOINOQvpgbpTWd9F5xVHGMZpts4jBQ5maJVBTa0oU7rqmZd22Sjci97vzEoXtVh+xu0N773Oc+hB2ZRXvSomMNDMCWFZEPI+LUjnFs8Jk/hUexcjpgr78zcp29l9bIXo+vSS7NsUUz/YKwbD0Et6PbU03ZvJi0VODurJOuVTDOFqcWkK6jG3P4K2fPn9bElDmdbdnsbdDTUbabbAppyZXY/Mn3TzBDHi7aPGzm9VqZJWvO7pgbcCOw/NfIsoMj4GV1QBazIuzOgA0JTYTujt0UgaH6nYUrvWUnKdykDEGzibAygoBgV79by+CyOkoj4edFw01hWNDUwKI0+xhNokre+fvRHjYiIZWzaCRU/ShNEccQpFDPecrx7WI7dYREY4gMw650EW7vnutc50phUt2Riws+ugHkb+TwoXlaKlR1OsQdQzK22kF4DnbkgEUE5z//+WO7kK1TmUHboU4rW09OlJtzxM4FBkV+4TIjmeOh3xCsDqdC0CMvy920mc4++O5US2HXIITsbdBBgT0t+A2auZuU27iT1tChykbMi9KxhNbnP/95oxz3tmpxaqV0ZjmjnM6gWXLKoIt7Zp69HtBNrIHNqMrVO7nTqIY/5mylnHl74rP/uTs0w3rkjblXpgwBOrK4UcLECjFQn+MIlr3z6ZxE40dXiTx4ZQrneiKSmvLnvJjFuMMd7jD1VbCMw+UDzQZsodqoLEABxDZ+wmvX7gtQpDxpYGFDg7T+7jeGorEG3Ly6WYMBVwv8ioKQsfjMRa+kHpkCn73VwGa5pB2T3R7QNFthaXHDmvqWtghwrcKwFD6gUioXMCEzgXO6niCHNd00gTL5WfMQ06fXrdH4r/Nu3XnR05IG7RWVzmz/hjLMxk89xp3mYo2C8mQbtoXmOBVtART/EVKZvAi4GVkBee8klHlN9WPNgeZIUU2tNS+IOd3pTjePJv2UCB0C16mmQgPhuUnrwqUVsOkcoAOWE0mNamgiATdIuwxR+4Tz4XI15TJZaKXZab91Qy6BKU6NIFOV6T2XwyOMy2RNv9kWjbCljFVbEqO0NRhMO87swoJ0KAS69k65G1wSyg4m1M7fD2sOOiXDVEbymKbjPPhGXBk4Vl8t8usMaHYTQL2CLH3qtApNb6y5OyDiWGehTNFSRJv9dTMMjTKMTdeOTf9OqFRzW4NeU2lLxR35gUzMzCL5+ZMr3CqvdBJAhxGjBLKxAcgP6+SyEfr1Dino2Eid+VoCv5PFpOfgohGoZe/k28fUduwjHah3a7Lqf/ioJCkueFvVRlKPwUISANVJPNX4vNVMjXBsQtOZrfZQXtGC6IrfH3OPoAcY5iLqz6VpNWZ9usbNbGC/2FNwtgc0LYG8DFGtmzTAV94u3YoI+7TfleNYVihTmOcx6BxrmzbnZRaJy3RK/fCIx6nS5lCg7lhVp+wQM4T1St1NOK9H0UDQ1NGAqU5pZLFXofcuvuaEZSmkr5Ia7bQWGWgQwsRz0isVDKq4+M9Dy/zZHZ6ZDCgPL1hndrjsCE+G0tceEKEZWZMCfEWaBt9IaOQ+DW48AhppONLRrMYF7OFgNmmGPjv2qsAHTw5ErLMe9eXaHYZxQtXBVvLiEMNiezlB2fajYM4WK8lyjqW3LQ6nXwutyUpDg1EDM2VZSMGfF03W9qkjAc5WczSd9GvKcXZlb9bkbheSB4Ksm5Ns4/MFSoNwRyJdnb5Tm4UVdg2CV1VOroCYKfcJcF4X9LQR3EAzB9r3KWzqXNJgawYW44Y2zl/O5YrLBkmcs/+0O2tGq0rBH/EbzArNplz+JTejBR2eGIevWY8FCJFqrmbBtXZKa+Sl5WRnMWKB8aASc9Wy11NmkKv7NN011HEJuhiWGq5ZScRq5zNEdWc+85nj+2LcmcLc3Jj+MFeLkxSaSOMv3qqke6F5b2hqRXgeegoIiJ/rWZaxzipBEwEQKmaKltp3Lk/eOFrN/kRvE8SAO+R1GjJhNPQExi1jceh6PqW3+gZmAZmOcm2DOIdphcWYYNPmWpgOHQhw3s1h7QyefQvxfkdCG3/E78L53+ZYCW3r9Gcd1ovpeuvTFK/VaLQtS58D3FHm7lQz4dXCyH4aGaIa1lxhMSOZwa9ENU9r2Sdig8jxLHHtiGZFYYjfuiZb66xdrE/Xxez+sOaqKm7u/24QWIgEiWdi1ZCHw2KmjGZFiziyH+lz2o+kZSg6xhwozWpW33VfgNQyo9C3zkePU0dvoUDfWODqAT3jooir5kJ3rplpVt7AoCpnI0EmkiY2QTqjpsHSArnOrr3oQBPHo72AHJjZyXSnA0Ez2RF/0NkWl0eum0mwMQcyF5iirFChGGveIc4jZsoNaXUJWv/90EQzhD3QTJbAEd/22kDTdRCc74vkVufHrFKsjj1DLbPpDtGFxSGJTM9qy/pK5LhN1fdn1p/DN5DqEPHsG43+RAO1yd3mn73sZS9D1Sg5T047iBwBR6XjXluBvvC/dbGOXPjbZbQbIAigVjX/stgcffKDI5cjBp2r2Rnu3A9NuYA11RLT2DAdNgrIQydjj1QYZyLLNdAMrL6nXHANH8FuXbhMQ6Zz3JG1kTFqgZVjztzH6EaIrb2eAxTbjXVLf+Yw1PgPUbu3rD4uz08oPuNLDDSnnWSzi8k+cqF29cuHNZ0FLnM0uwZioNbziITmgDJqOASaurc7NzmjiRuAaeQ90JyL1ShULRXMwuYhIap25DGT2cIEdKaLU1afaWX11akKZLJ92ilpH9t1RqmoYjVAKUnmPu+iEbpof9x9UWShXp4rypzTAqv3lmzWAR+pr7fygjVps3QS7IhTkNrSHQGTneUdX7k7+4jBIOSoY83JGrpwWH+AvHExwl6JqlDGo8JPuORcagfDQWc/P+T+VgduvbPiKZvSCEHKHg8A2XybeAiw3OQ7zln3mGBGtTHOCL4uvCLB0XwrUj8rvlspxSkHNTnBUpv/n0quvJ1xW5RBs9SptKtpcutFEUfYfA1jLXZx92Wa6U1pFFTkIiIh8p2gmYBTwRRrIAV/zKLwuci6L+9yENtwn8pJ3Z+r9xPd5kGODmVW5OQK2zPlMBoZS56xR7TcizHitLly8ErqdSGl0BebwqUR+h7MmO+WJeawOHlgkz0+8l6suwbrNyJWaLZtI0XfbtARNtl1O0NAfAhrsoklC0sbBU0B+C6bSKsHNuaPIO3xEzld7LB69leKkeVtxzajn5HdoLQgGDonY9IdRTQzO0MFLrmwRs77pHA2lsJcrmSjyohP9tQ1svSdL2TA0fT6fM8Y1m2T1G/v1q87Es6lo5PrkbrkTa5TaBMoaM6jENkO0N97vsPWQbPEqkhgH0nYqIjhyqSU6AYmzCflvm1h46Y43gE3NtDgw7t9uTb3YHZibL45CDJNebfGfe7Uizoq2EOvjt38mFKbIb4EZzlL9yUs2SBRF88B3xc25d36E+zEoXJGVK6MUqfguByhHBmXbU3rxtN1x1Hww0qZCXUnvpl9vwO82IW3hgXD0IqnDWzN/urGzqq3Oo0xTQWOlcNmLvHo7EAOra6kOy/OANbVGLjPeNKWmu3Iks8PfOAD+xADnhsx91sdyd4Fwtu2MKMKFHoaQbroe5Ul7TcKHFTBZ2/tWfq17L5+qTV/pjONbWtRzZBs9yl2aVkErhJtcS7Li06aSSP7mnzH82Z/Eqz7FnK754Fy9X3htV/9W5V7g0h2gs5OfLMLIg9TU8k+bEXqHeaYkxldM83Z7tJD6rip/no2pT335tjTgKKy6bcRX/1GqAKXtIMj6jvx1HGnSY5OazOkgWAkPXg92tGO5sjBnAhxrSlZv31YoR8+DQRT3CFpXLJtgVplHglNfAcyBgqpG8V9dfpfNdYXd2q/H/WMxWmL4k6vb9u+m3rvoEmeaISd0xyLe6oF46zEtdkKdZwI2Qihgqmj2taUQd/Trv1NfLJte9lFKyKwgLWyZqgNT7PHsxL52tGY8hUxKyOuZDkgnvnW0ZzA0h3UOltkt9MGugPOjqX69YR+gt2mPMPFfWfiHMT2VWNbJ6yZUwfsKrLkmClo4hCDbve82JyQwkHmsoTftmWi2vWifc5tKS0DOhZ5l5bnURmiBhbOitKy4FtL+U41h2uj2EFnjJ6boeYMxh3vypQNNNcMi+34+GPDPK3MtyHmrRZzT1jvXmF3x6CBbQvBYaYVpoNU7DXXGsFVDhb5UiVI+davkxnOZPCz2ze2WWiPgyESSDjAL9EmiHbkwFlbTpdf8aXDTiSJR0vn7c9uLP+0qgWXY5d6PhmVSaEcAk31bDcHiMloxjSRyp4lhy+p71J5XM+Ibc+ysiMYAZw7bQVtWxpzIM4b6fdgI2CL2yDTunWorrXMq2ldBpclrZxti412iVW3Amt3Bt0JiIePd7XmpJnTdE7W2QqPovo6gKMCtuKEDRwbFGUPBUUxkv1ingMD9o1li52UsK9bym/yEiVhClUnr7JmeVcF3nrdK3lHLgaORaW58vU1XdRdqXGhwj6jMYHhp5F6d3ait42nw50R29ayOwdv20u8OGZ9rnfyNaeRsmArfFetm/u1n/+KGMSCVmTSW6PiKkxYMBYzEt0IHSa8HYoaq7oeN27TJYpyXtPZSmnFS1ziEr4SxCFuo19Wy9aUjWKK4ZSWcNVxMrGmII/JQ1GOHAjshHelIxLzTlhZM4Cz23yYLobVVr3dtoWdso3t1Mxe1LQzSehpbRI++5AwDUMbGdy4aq7Hqu5yQbSrk7qtrQ8uh6ms7czAZmxrj10HzXlro8Ia+owPqjWgLD6T9mo7fhUweXckZxx8/MTkoai8KD8iwOStFMWLkpmyheFgP5PHH3CgRIbB1xX8NpXzjo4roKi6i6fXMswxgOsikU8qLTaaMtHb3Bmq27jYamdXk7oTsFYkDR0eVmjugtptH+3jFtj1kW3ettik3rNIyuxZ9mxka4U92zyQCprdqDbT9D1XxZ8qyHR2YCXuWUlCHVuaONUXiEFKwD5Wb8Sck7phoYLUCLW3Ji21gZjJVQ1br+9uIKPKa9lA6mEFwVDaVrRtsN34iLu/su0ADhNPH7KHbslmvVbCaGl2UrL1/k4Kut4/kHY26pR432CUAcHWTlevaBfOWBscp9u7c9xpFXNOUmXadz1bBm52XY9r5W2BuGFzhwWHilb+28mBW1G7atFWHt3awk742NMQb7x4ONC/Fc27I3j/yaPDiugjoP7sCbF9Hfq30LOv06zmhGjgCCUp2zptN9cz54Mk1bLgAynXEwz1KPQEnYZUF9qftzLN3Z/6M9rUad7t/gQZ3lrhVYiwqlkBxGq7R8Zji7sY5p5rNd2cVZqZ1sU68ZyZ6Ss1m2PqI4smOLQ9Squ1uZ5vBIxQ2vswmJZoTs6P5jSY6o9EDl5ohsIoqosVJVt5cSg2XG6c/hpKHloaourR2kV9tdYr1LpOPGExIQW1xjkQ7LRUyz0QH0M0MnAx0GxIhWK9GHDXOlUeamgMaemqnyPsGdgA153RjeCyIruTaCmei/n9i8HNjCoKSMdakybVgENYqzR/jlrOQtVC6zlN9e7BC83Bygy6offZKTilFNocB2meI7yhjSY/WK+dQcwwxCxiy92yhq2hirlOfiNaF6FqpBIsGtK0U7OdMQ15A83BcW024FWuo1e9EjhCcw5JAF1H1XV6PhB0Xc25kx5qrVOLK69rv/MP0eEAd8Qx3TVUdRJKjfTZZLu5XjSSAWUL2KIdvNA0Shldia2+2iHslfJt9e099oNHiiydOm3wOEjf2q2ZCH96y3dFVJM3drzNtyxawYRqf98Whe+w2k+SeA8fNiQ0KI2c7MOEt8TdtjESlcMD6mitE08O0mun/96KE6IvRZtyq/14kDpmJDFkU0p32un/GgSywX1SEXL1/3lq0Kc4dcYQR/bKcLDjfBK30pk2Wvomrnk5ye/EoHRERwcL4NIQTw1A4/32kEDQUKW3zauDBIr4T9fWubd8m8WfnRtsKczXhrBxtrB9E8FT0jFHN4nD10oDZRh1fEwiTFZEmqzuvOXTEhlMk/W7JtU/eKFpSr4MJGtj+eTD+80J05PbkxeUlEm9rLsf5vNdFlLxne6AtULTHSlAKUYrYgV9rVvSxxrFIi5sB/slCcdHSIuQUn0i8QqIhICK3v2ui0xndWysSXBKX2tK8O53tmztBkHteF1OijqRE7gkb0lZiXHKQGxSmzJcK+bCjWp27aQ/5drs7Cu+IDvU4mlfb49gpCD8x48SrlbAXqvUVV8yMS+/0CTD5XsBsu4GAwqRk7ck20vK2qp2R/JLd87HhIlgZOPHdpGfXTY7Cyhlpo4vJVcHpCyXm4ZH7eXza9k6qKZBE/fUz89OpzaK/ASu/7iIThpMv4bsqSmQshSeBjXVr6do7eCFphWxg2p1yNWOjlSicwaWDMLkGrML6sAl3e0osWJWbq4GXTX6Dc39TEj/TwDZRAb4DLykHufroJGxbPz8utCYUVlJ7RB2JEdgpCslDiiWG8STtKf2XTzq9HQLHXlIsPefVwAQqfv/HxpzJVwaGGo3WTo5VJf51lqgHFtJzZze6D9WjHHTDXCkzDKpGXqHp/y3aDkAhmG0/hM+A6bnEvgmRa8MzLsZBDXbW/IzRu4wWUauPj1sIiqAPvKLaN1sUhoEet/nsZ4I24bnKJX2kYts3USNDdUP75CIg+fazCq2JgcvNI3P10II2NpZFKxmAiZmHf1ARUgyH9xj+SxB5j5krGGQyZOEozEUWgW/R9LahQO5cQ36ZSmlX49vafAN0qq1GlSYM2Pod1kThu0c/MfuG6FevIuwvU5bjNyvF9npYdRSAxxM0/ySoJ0evOIVvzQxx1iz0SmGp+aFcvyimhR9xOPENFbGbTQt4fmqqgb1AhMhu0+Lg5yctArE/mQr+v5u7WuWkdFF/6sEDNFDm5l1pBGJXquEOGkIDbR6tqacCfKT0M1dm173f1zovf/uw7um0PKaIN22sDkSnlp5uLQnsiaGe4VV8Yp2/LQdP6oBeHTwQtP8LZzp0Tb/YSM4kiWLTJVLJxk9rOA88LI1bAdvzmgOmJK3s3A4wytetDrExriECX9q3GFTLXSU2DrCEAiiw5XSVAY+UOgXJXMn+h0OBQm1B13XgM5JIJj+y8M43nQ068wek0edGP3i3yBVxFazTIRqrKcjOb2rESvAeubAhQ+/q02ojOkaTHhEVeAGsNxX6LM/waUpE79FQ2/GwC9ErnxZiqQ7vRShswzGQFetPyfBj2TbKVWnH61VzVFabcKrdXPdCH3adUO3mMIP6fDFI8JcCK+rP2FremI8RMMn4csqkUu+ysELTVNlbUmRMbJS3C9yhar+p5ig6bf56GJHXRJwZU27quaAFleMviJg2GKemn++I6X3eiW7qa8cgDGdhWKIh2ZXM57APf0EtQ3uEQ+egxienzoDGtePeMQj4IB6CBHMSwCXCZ5hx8R2npzBM816X/2TjXRy0GRP+qrqRDBCMfdNsyDD3I0QIhs5KDu0RsFwp59b5/7a7rIsZh2nKs6IiKtwpBfVwe5283FkLRgzT9Ejyhwi+zRaAuJLWDronxDKU0GVKXOy+9L2WHlOC4j3S5w13moc1NAcdQc+wqbl5sAlGrfShMWkhO0Te7ELaeQGNK27ZSIGwMK7wsD2r02eF0uVnQFz+oswUFSOFL7xivMWtsjxdG0aj5HoJSqKC/mUYNQPCyYbnzQKMphyzaKE+d/u7bYbqkGSh/McqMgwBpdphSJLQIqsef8ZK9s6rvMGNI0fXGgsswCgptCvGpmFyXrUV77ALoc4PeEjgqMLjVs9h49Se45piOFSWyj+gxNJ7hsq0HNRWIZaMEfANXG9WCLTD5oiGPU5vvrieOBdS50n6i3DI0Fda8rrrZgzdSDOcFlt8xWet9Q+D2rWlP7Ai0RlrLiEzjEx2bImRnc5N9gIRbF3JNf9VYRCEzwh+kG9UJJqqgPiZCDgwAc+tcPCxlIsLz1mvjVLJSI2g5ExwAeJQYEYcvVNS11k4lMb9lGbikZ0KrsUlCWegKCaXJH+K2KPxqD3iH3sXZ+mBm1ubrv95l3GHezgkqtt2KMzlBmnIjDDK/fkkeGhf/1mgmU5cLNOWXwdlUhS06T8adiAZQUKH/3kGLC2+F5hfC2ap9at/66kwVjqAhrW3BQEgqmEtfJDYjS5E1W++Z3zYM2Np/X3WearBf8fINb702ncOeUAAAAASUVORK5CYII=';

        $data = base64_decode($data);
        return $data;

    }



    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        if($no != $courierLabelNew->courier->local_id)
        {
            return Client17Track::getTtGlobalBrokers($no, false, false, false, 'B2C');
//            return TrackingmoreClient::getTtGlobalBrokers($no, 'correos-spain');
        }
//         method only calls updating method which goes another way round - makes updates for all new stats found
        self::updateTtData();

        return false;
    }

    public static function updateTtData($justReturnData = false, $asBroker = GLOBAL_BROKERS::BROKER_B2C_EUROPE)
    {
        $CACHE_NAME = 'B2C_UPDATE_TT_FTP'.$asBroker;

        MyDump::dump('b2c_tt_log_'.$asBroker.'.txt', 'START');

        if(!$justReturnData) {
//            // prevent running mechanism too often
            $flag = Yii::app()->cache->get($CACHE_NAME);
            if ($flag) {
                MyDump::dump('b2c_tt_log_'.$asBroker.'.txt', 'LOCKED!');
                return true;
            }
//
            Yii::app()->cache->set($CACHE_NAME, true, 60 * 15);
        }

        MyDump::dump('b2c_tt_log_'.$asBroker.'.txt', ' FTP...');

        $conn_id = ftp_connect(self::FTP_HOST);

        if(!ftp_login($conn_id, self::FTP_USER, self::FTP_PASS))
        {
            Yii::log('B2C TT FTP LOGIN ERROR!', CLogger::LEVEL_ERROR);
            return false;
        }
        ftp_pasv($conn_id, true);

        $contents = ftp_nlist($conn_id, self::FTP_DIR);

        if(!S_Useful::sizeof($contents))
        {
            Yii::log('B2C TT FTP FILES NOT FOUND!', CLogger::LEVEL_ERROR);
            return false;
        }

        $ttData = [];
        if(is_array($contents))
            foreach($contents AS $file)
            {


                $array = explode('.', $file);
                $extension = end($array);

                // files with TT data starts with "cdt"
                if(strcasecmp($extension, 'xml') == 0)
                {

                    if(!TtFileProcessingLog::isProcessed($asBroker, $file))
                    {
                        // new file found - lock for 3 hours
                        Yii::app()->cache->set($CACHE_NAME, true, 60 * 60 * 3);//
                        MyDump::dump('b2c_tt_log_'.$asBroker.'.txt', ' FOUND NEW FILE: '.$file);

                        ob_start();
                        $result = ftp_get($conn_id, "php://output", self::FTP_DIR.$file, FTP_BINARY);
                        $data = ob_get_contents();
                        ob_end_clean();

                        $data = simplexml_load_string($data);

                        foreach($data->children() AS $item)
                        {


//                            $ttNo = (string) $item->TrackEventNumber;
                            $ttNo = (string) $item->OrderNumber;
                            $ttStatus = (string) $item->TrackEventStatus;
                            $ttDate = (string) $item->TrackEventDate;
                            $ttLocation = NULL;

                            if(!isset($ttData[$ttNo]))
                                $ttData[$ttNo] = [];

                            $ttData[$ttNo][] = [
                                'name' => $ttStatus,
                                'date' => $ttDate,
                                'location' => $ttLocation,
                            ];

                        }

                        if(!$justReturnData)
                            TtFileProcessingLog::markAsProcessed($asBroker, $file);
                    }
                }
            }

//        file_put_contents('glsnl_tt_log.txt', date('Y-m-d H:i:s').' DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData)."\r\n", FILE_APPEND);
        MyDump::dump('b2c_tt_log_'.$asBroker.'.txt', 'DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData));
        MyDump::dump('b2c_tt_log_'.$asBroker.'.txt', 'DATA '.print_r($ttData,1));

        if($justReturnData)
            return $ttData;

        foreach($ttData AS $ttNumber => $ttDataForItem)
        {
//                        searchForItem
            $couriers = CourierExternalManager::findCourierIdsByRemoteId($ttNumber, false, false, $asBroker);

            /* @var $courier Courier */
            foreach($couriers AS $courier)
            {
                if($courier->isStatChangeByTtAllowed(false))
                {
                    $temp = [];
                    MyDump::dump('b2c_tt_log_'.$asBroker.'.txt', 'TAK!'.$courier->local_id);
                    CourierExternalManager::processUpdateDate($courier, $ttDataForItem, false, $asBroker, $temp);
                } else
                    MyDump::dump('b2c_tt_log_'.$asBroker.'.txt', 'NIE!'.$courier->local_id);
            }
        }

    }

}

