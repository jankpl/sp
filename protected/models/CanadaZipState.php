<?php

class CanadaZipState
{
    public static function getStateByZip($zip)
    {
        $fc = substr($zip,0,1);

        switch($fc)
        {
            case 'T':
                return 'AB';
                break;
            case 'V':
                return 'BC';
                break;
            case 'R':
                return 'MB';
                break;
            case 'E':
                return 'NB';
                break;
            case 'A':
                return 'NL';
                break;
            case 'B':
                return 'NS';
                break;
            case 'X':
                if(in_array(strtoupper(str_replace(' ', '', $zip)), ['X0A0H0', 'X0A1H0']))
                    return 'NU';
                else
                    return 'NT';
                break;
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case 'P':
                return 'ON';
                break;
            case 'G':
            case 'H':
            case 'J':
                return 'QC';
                break;
            case 'S':
                return 'SK';
                break;
            case 'Y':
                return 'YT';
                break;
            case 'C':
                return 'PE';
                break;
        }
    }
}