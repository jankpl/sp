<?php

/**
 * Class bDefaultTelNumberAddressData
 *
 * Behavior validates Zip Code with data of proper codes
 */
class bValidateZipCodeWithBaseAddressData extends bBaseBehavior
{

    protected $_allowedClass = false;

    public function afterValidate($event)
    {

        /* @var $that AddressData */
        $that = $this->owner();
        if($that->country_id != '' && !$that->hasErrors('zip_code') && !$that->hasErrors('country_id'))
        {
            $countryCode = CountryList::getCodeById($that->country_id);

            if($countryCode)
            {
                if(isset(ZipValidatorBase::$RULES[$countryCode]))
                {
                    $rule = ZipValidatorBase::$RULES[$countryCode];

                    if(!preg_match('/^('.$rule.')$/i', $that->zip_code))
                        $that->addError('zip_code', Yii::t('zip_code', 'Podaj poprawny kod pocztowy dla wybranego kraju. {a}Lista reguł{/a}', ['{a}' => '<a href="'.Yii::app()->createUrl('/site/zipCodeRules', ['cc' => $countryCode]).'" target="_blank">', '{/a}' =>'</a>']));

                }
            }
        }
        return parent::afterValidate($event);
    }
}