<?php
/* @var $domain int */
/* @var $emails Omines\DirectAdmin\Objects\Email\Mailbox[] */
/* @var $vacations array */
?>

<h2>Manage mailboxes</h2>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php
if(!$domain):
    ?>


    <div class="text-center">
        <a href="<?= Yii::app()->createUrl('/daApi/index', ['domain' => DaApiController::DOMAIN_SP]);?>" class="btn btn-default">@swiatprzesylek.pl</a> | <a href="<?= Yii::app()->createUrl('/daApi/index', ['domain' => DaApiController::DOMAIN_KAAB]);?>" class="btn btn-default">@kaabpl.pl</a> | <a href="<?= Yii::app()->createUrl('/daApi/index', ['domain' => DaApiController::DOMAIN_CQL]);?>" class="btn btn-default">@cql.global</a> | <a href="<?= Yii::app()->createUrl('/daApi/index', ['domain' => DaApiController::DOMAIN_FB]);?>" class="btn btn-default">@fire-business.com.pl</a>
    </div>

<?php
else:
    ?>

    <div class="text-center">
        [ <a href="<?= Yii::app()->createUrl('/daApi/index');?>">index</a> ]
    </div>
    <br/>
    <br/>


    <h3>Autoresponders</h3>

    <table class="table table-striped table-bordered">
        <tr>
            <th>#</th>
            <th>Account</th>
            <th style="text-align: right;">From</th>
            <th>To</th>
            <th>Manage</th>
        </tr>
        <?php
        $i = 1;
        foreach($vacations AS $account => $data):

            parse_str($data, $temp);
            ?>
            <tr>
                <td><?= $i;?></td>
                <td><?= $account;?>@<?= DaApiController::domainIdToEmailDomain($domain);?></td>
                <td style="text-align: right;"><?= $temp['startday'].'-'.$temp['startmonth'].'-'.$temp['startyear']; ?></td>
                <td><?= $temp['endday'].'-'.$temp['endmonth'].'-'.$temp['endyear']; ?></td>
                <td><a href="<?= Yii::app()->createUrl('/daApi/vacation', ['domain' => $domain, 'account' => $account]);?>">manage</a></td>
            </tr>
            <?php
            $i++;
        endforeach;
        ?>
    </table>

    <?php
    if(AdminRestrictions::isUltraAdmin()):
        ?>

        <h3>Forwardes</h3>

        <table class="table table-striped table-bordered">
            <tr>
                <th>#</th>
                <th>From</th>
                <th>To</th>
                <th>Manage</th>
            </tr>
            <?php
            $i = 1;
            foreach($forwarders AS $from => $to):
                ?>
                <tr>
                    <td><?= $i;?></td>
                    <td><?= $from;?>@<?= DaApiController::domainIdToEmailDomain($domain);?></td>
                    <td><?= str_replace(',', '<br/>', $to); ?></td>
                    <td><a href="<?= Yii::app()->createUrl('/daApi/forwarder', ['domain' => $domain, 'account' => $from]);?>">manage</a></td>
                </tr>
                <?php
                $i++;
            endforeach;
            ?>
        </table>

    <?php
    endif;
    ?>

    <h3>Email accounts</h3>

    <table class="table table-striped table-bordered">
        <tr>
            <th>#</th>
            <th>Address</th>
            <th style="text-align: right;">Disk usage [MB]</th>
            <th style="text-align: right;">Sent today</th>
            <th style="text-align: right;">Autoresponder</th>
            <th style="text-align: right;">Forwarder</th>
        </tr>
        <?php
        $i = 1;
        foreach($emails AS $key => $email):
            ?>
            <tr>
                <td><?= $i;?></td>
                <td><?= $email->getEmailAddress();?></td>
                <td style="text-align: right;"><?= round($email->getDiskUsage());?></td>
                <td style="text-align: right;"><?= $email->getMailsSent();?></td>
                <td style="text-align: right;"><?= !isset($vacations[$email->getPrefix()]) ? '<a href="'.Yii::app()->createUrl('/daApi/vacation', ['domain' => $domain, 'account' => $email->getPrefix()]).'">create</a>' : '';?></td>
                <td style="text-align: right;"><?= !isset($forwarders[$email->getPrefix()]) ? '<a href="'.Yii::app()->createUrl('/daApi/forwarder', ['domain' => $domain, 'account' => $email->getPrefix()]).'">create</a>' : '';?></td>
            </tr>
            <?php
            $i++;
        endforeach;
        ?>
    </table>
<?php
endif;
?>
