<?php

class DebtBlock
{
    const STAT_NEVER_DONT_NOTIFY = 19;
    const STAT_NEVER = 9;
    const STAT_BLOCK = 1;
    const STAT_NONE = 0;
    const STAT_BLOCK_LOCK = 2;
    const DEFAULT_BLOCK_DAYS = 5;

    const SMS_DAY = 4;

    public static function getStatList()
    {
        return [
            self::STAT_NONE => '-',
            self::STAT_BLOCK => 'Blocked',
            self::STAT_NEVER => 'Never block',
            self::STAT_NEVER_DONT_NOTIFY => 'Never block & no notify',
            self::STAT_BLOCK_LOCK => 'Block & lock',
        ];
    }

    public static function tryToUnlockAccount($user_id, $fromCron = false)
    {
        /* @var $user User */
        $user = User::model()->findByPk($user_id);

        if($user->getDebtBlockActive() && $user->getDebtBlock() != DebtBlock::STAT_BLOCK_LOCK)
        {
            $outdated = UserInvoice::model()->active()->ofUser($user_id)->outdated()->count();

            if(!$outdated)
            {
                $user->setDebtBlock(DebtBlock::STAT_NONE, $fromCron);
                MyDump::dump('debtCron.txt', 'UNBLOCKBLOCK - USER: '.$user->login.' ('.$user->id.')');
            }
        }
    }

    public static function listLockedUserIds()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('user_id');
        $cmd->from('user_options');
        $cmd->where('name = :name AND (value = :value OR value = :value2)', [':name' => 'debtBlock', ':value' => self::STAT_BLOCK, ':value2' => self::STAT_BLOCK_LOCK]);
        return $cmd->queryColumn();    }

}