<?php
/* @var $modelUser User */
/* @var $model UserBalance */
/* @var $models UserBalance[] */
/* @var $currencySelected String */
/* @var $current_page Integer */
/* @var $page_size Integer */
/* @var $item_count Integer */
?>

<h1>User balance</h1>


<a href="<?= Yii::app()->createUrl('/user/view', ['id' => $modelUser->id]);?>" class="btn btn-primary">View user</a>
<br/>
<br/>

<table class="list hLeft" style="width: 500px; margin: 0 auto; font-size: 20px;">
    <tr>
        <td>Overal amount due</td>
        <td>
            <?php
            $activeCurrencies = [];
            foreach(UserBalance::calculateTotalBalanceForUserId($modelUser->id) AS $currency => $value)
            {
                if($value)
                    $activeCurrencies[$currency] = S_Price::formatPrice($value).' '.$currency;
            }
            ?>
            <?php
            if(!S_Useful::sizeof($activeCurrencies)):
                $value = '-';
            else:
                $value = implode($activeCurrencies,'<br/>');
            endif;

            echo $value;
            ?>

    </tr>
</table>
<br/>
<h3>Account - details</h3>


<h3>Add to account bill</h3>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'action' => Yii::app()->request->requestUri.'#tab_3',
        'id' => 'user-balance-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.2,
        ),

    ));
    ?>

    <?php echo $form->hiddenField($modelUser,'id'); ?>

    <?php echo $form->errorSummary($model); ?>
    <table class="list hLeft" style="margin: 0 auto; width: 600px;">

        <tr>
            <td style="width: 100px;"><?php echo $form->labelEx($model, 'description', array('style' => 'width: 100px;')); ?></td>
            <td ><?php echo $form->textArea($model, 'description'); ?></td>
            <td><?php echo $form->error($model, 'description'); ?></td>
        </tr>
        <tr>
            <td style="width: 100px;"><?php echo $form->labelEx($model, 'amount', array('style' => 'width: 100px;')); ?></td>
            <td><?php echo $form->textField($model, 'amount'); ?></td>
            <td><?php echo $form->error($model, 'amount'); ?></td>
        </tr>
        <tr>
            <td style="width: 100px;"><?php echo $form->labelEx($model, 'currency', array('style' => 'width: 100px;')); ?></td>
            <td><?php echo $form->dropDownList($model, 'currency', array_combine(Currency::listIdsWithNames(),Currency::listIdsWithNames())); ?></td>
            <td><?php echo $form->error($model, 'currency'); ?></td>
        </tr>
    </table>

    <div style="text-align: center;">
        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Save'),
            array(
                'confirm'=>'Are you sure?'
            ));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->

<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, 'Negative amount = decrease user\'s debt. Positive amount = increase user\'s debt', array('closeText' => false)); ?>

<?php
if(S_Useful::sizeof($activeCurrencies) > 1):
    ?>
    <div class="text-center">
        <?php
        foreach(array_keys($activeCurrencies) AS $currency):
            ?>
            <a href="<?= Yii::app()->urlManager->createUrl('/user/balance', ['currency' => $currency, 'id' => $modelUser->id]);?>" class="btn <?= $currency == $currencySelected ? 'btn-primary' : '';?>"><?= $currency;?></a>
            <?php
        endforeach;?>
    </div>
    <?php
endif;
?>
<br/>


<table class="table list hTop">
    <tr>
        <td>Date</td>
        <td>Amount</td>
        <td>Description</td>
        <td>Balance</td>
    </tr>

    <?php
    // find currency

    foreach($models AS $item):
        ?>
        <tr>
            <td><?php echo $item['date_entered'];?></td>
            <td><?php echo number_format($item['amount'],2,',',' ');?> <?= $currencySelected;?></td>
            <td><?php echo $item['description'];?></td>
            <td><?php echo number_format($item['suma'],2,',',' ');?> <?= $currencySelected;?></td>
        </tr>
        <?php
    endforeach;
    ?>
</table>

<div class="pager">
    <?php
    $this->widget('CLinkPager', array(
        'currentPage'=> $current_page,
        'itemCount'=> $item_count,
        'pageSize'=> $page_size,
        'maxButtonCount'=>5,
//                    'firstPageLabel'=> false,
//                    'lastPageLabel'=> false,
        'nextPageLabel'=> Yii::t('site', 'Next &gt;'),
        'prevPageLabel'=> Yii::t('site', '&lt; Previous'),
        'header'=>'',
        'htmlOptions'=>array(),
    ));
    ?>
</div>
