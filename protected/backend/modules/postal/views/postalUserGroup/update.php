<?php

/* @var $model PostalUserGroup */

?>

<?php

?>

<h1>Update <?php echo GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_form', array(
    'model' => $model,
));
?>
<br/><br/>
<h3>Add Member</h3>

<div class="form" style="text-align: center;">

    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'add-member',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('/postal/postalUserGroup/addMember', array('id' => $model->id)),
    ));
    ?>

    <div class="row">
        <?php

        echo CHtml::dropDownList('user_id', '', CHtml::listData($model->getNonMembers(),'id','login'), array('multiple' => 'multiple', 'style' => 'height: 150px; padding: 0 5px;')); ?>
    </div><!-- row -->

    <?php
    echo GxHtml::submitButton('Add to group');
    $this->endWidget();
    ?>
</div><!-- form -->

<br/><br/>
<h3>Remove member</h3>

<table class="list hTop" style="width: 150px; margin: 0 auto;">
    <tr>
        <td>Login</td>
        <td>Action</td>
    </tr>

    <?php
    foreach($model->getMembers() AS $item):
        ?>

        <tr>
            <td><?php echo $item->login;?></td>
            <td><?php echo CHtml::link('delete', Yii::app()->createUrl('/postal/postalUserGroup/removeMember',
                        array('id' => $model->id, 'id2' => $item->id)),
                    array('class' => 'likeButton'));?></td>
        </tr>

    <?php
    endforeach;
    ?>
</table>

<?php
$script = "  
(function ($) { 
     $('#user_id').chosen({allow_single_deselect:true});
        $('.chosen-drop').css('width', '500px');
        $('#user_id').hide();   
})(jQuery) 
";
Yii::app()->clientScript->registerScript('chosen', $script, CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.jquery.min.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.min.css');
