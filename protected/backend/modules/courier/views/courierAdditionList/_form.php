<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-addition-list-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div><!-- row -->
    <div class="row">
        <?php $this->widget('PriceForm', array(
            'form' => $form,
            'model' => $model->price,
            'formFieldPrefix' => '',
        ));?>
    </div>

    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):



            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';


        endforeach;
        ?>
    </div>


    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

            $model_tr = $modelTrs[$languageItem->id];

            if($model_tr === null)
            {
                $model_tr = new CourierAdditionListTr;
                $model_tr->language_id = $languageItem->id;
            }


            $this->renderPartial('_tr_form', array(
                'model' => $model_tr,
                'form' => $form,
                'i' => $languageItem->id,
            ));

            echo'</div>';

        endforeach;
        ?>
    </div>

    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>
    <input type="button" id="copy-to-tr" value="Copy texts from `pl` to all languages versions" />
    <br/>
    <br/>
    <br/>
    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->
<script>
    $('#copy-to-tr').on('click', function(){
        var data = [];
        var setData = false;

        $('.langVersion').each(function(i,v){

            if(!data.length)
                setData = true;
            else
                setData = false;

            var j = 0;
            $(v).find('input[type=text]').each(function(ii,vv) {

                if(setData)
                    data[j] = $(vv).val();
                else
                    $(vv).val( data[j]);
                j++
            });

        });
    });
</script>