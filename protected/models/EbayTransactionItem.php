<?php

class EbayTransactionItem
{

    public $transactionId;
    public $itemId;
    public $title;

    public $transactionPrice;
    public $transactionCurrency;

    public $quantity;

    public $shipped = false;

    public $shipp_track_id;
    public $shippping_operator;
    
    
    public function __construct(stdClass $transaction)
    {       
        $this->title = $transaction->Item->Title;
        $this->itemId = $transaction->Item->ItemID;
        $this->transactionId = $transaction->TransactionID;
        $this->transactionPrice = $transaction->TransactionPrice->_;
        $this->transactionCurrency = $transaction->TransactionPrice->currencyID;

        $this->quantity = $transaction->QuantityPurchased;
        $this->shipped = $transaction->ShippingDetails->ShipmentTrackingDetails->ShippingCarrierUsed != '' ? true : false;

        $this->shipp_track_id = $transaction->ShippingDetails->ShipmentTrackingDetails->ShipmentTrackingNumber;
        $this->shippping_operator = $transaction->ShippingDetails->ShipmentTrackingDetails->ShippingCarrierUsed;
    }
    
}