<?php
/* @var $this CourierController */
/* @var $models S_Courier_Id_From_File[] */
/* @var $fileModel F_FileModel */
?>


<?php

if($errors)
{
    echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, Yii::t('courier','Error - please correct data!'), array('style' => 'font-size: 16px; text-align: center;', 'closeText' => false));
}

if($remaining >0)
    echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, Yii::t('courier','Remaining to save: {n}', array('{n}' => '<strong>'.$remaining.'</strong>/'.$total)), array('style' => 'font-size: 16px; text-align: center;', 'closeText' => false));
else
    echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('courier','All done!'), array('closeText' => false));

if($models === true)
    echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, Yii::t('courier','This part has been already saved!'), array('closeText' => false));
else if(S_Useful::sizeof($models))
    $this->renderPartial('createExternalFromFile/_list',
        array(
            'models' => $models,
            'fileModel' => $fileModel,
            'split' => $split,
        ));
?>






