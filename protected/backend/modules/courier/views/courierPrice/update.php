<?php
/* @var $this CourierPriceController */
/* @var $model CourierPrice */
/* @var $courierPriceValues CourierPriceValue[] */
/* @var $groupActionListView string */
/* @var $groupActionDelete boolean */
/* @var $mode string */
?>


<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);


?>

    <h1>Manage <?php echo $mode;?> pricing for: <?php echo CourierCountryGroup::model()->findByPk($countryGroup)->name;?>
        <?php if($mode=='delivery') echo ' from: '. CourierHub::model()->findByPk(Yii::app()->request->getQuery('id2',''))->name; ?>

    </h1>
    <h2>For user group: <?php echo (UserGroup::model()->findByPk($userGroup)!=NULL)?UserGroup::model()->findByPk($userGroup)->name:'-';?></h2>

<?php
if($groupActionListView!=''):
    ?>
    <div class="flash-notice">
        <p>Edit price for user group:</p>
        <?php echo $groupActionListView; ?>
    </div>
<?php
endif;
?>

<?php
if($userGroup === NULL && !AdminRestrictions::isRoutingAdmin()):
    ?>
    <div class="alert alert-warning text-center">You have no authority to modify main routing.</div>
<?php
else:
    ?>


    <div class="form">
        <?php $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'courier-price-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'clientOptions'=>array(
                'validateOnType'=>true,
                'validationDelay'=> 0.1,
            ),
        ));
        ?>



        <?php echo $form->hiddenField($model, 'id'); ?>

        <?php echo $form->errorSummary($model); ?>

        <?php

        echo CHtml::submitButton("More",array(
            'name'=>'add_more',
            'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
            'id' => 'add_more',
        ));

        ?>

        <?php

        foreach($courierPriceValues AS $item)
        {
            $firstItem = $item;
            break;
        }


        ?>
        <table class="list hTop" style="width: 700px;">
            <tr>
                <td style="width: 40%;"><?php echo $form->labelEx($firstItem,'weight_top_limit'); ?></td>
                <td style="width: 40%;"><?php echo $form->labelEx($firstItem,'price_per_item'); ?></td>
                <td style="width: 40%;"><?php echo $form->labelEx($firstItem,'price_per_weight'); ?></td>
                <td><span style="font-weight: bold;">action</span></td>
            </tr>
            <tr><td></td><td></td><td></td></tr>
            <?php
            foreach($courierPriceValues AS $key => $item)
            {
                $this->renderPartial('_courier_price_value', array(
                    'model' => $item,
                    'form' => $form,
                    'i' => $key,
                ));
            }
            ?>

        </table>


        <?php

        echo CHtml::submitButton("Sort",array(
            'name'=>'sort',
            'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
        ));

        ?>

        <?php
        echo GxHtml::submitButton(Yii::t('app', 'Save'), array('id' => 'save_all'));

        ?>

        <?php
        if($groupActionDelete):
            ?>
            <div class="flash-notice">
                <p>Delete whole pricing for this group.</p>
                <?php


                echo CHtml::submitButton("Delete",array(
                    'name'=>'delete',
                    'onClick'=>'
                 if(confirm("Do you really want to delete all pricing?"))
                 {
                    $(this).closest(\'form\').attr(\'action\',
                    $(this).closest(\'form\').attr(\'action\'));
                 } else {
                 return false;
                 }',
                    'tabindex' => -1,

                ));

                ?>
            </div>

        <?php
        endif;
        ?>

        <?php
        $this->endWidget();
        ?>

        <div class="center">
            <?php echo CHtml::link('Back', array('/courier/courierPrice/view', 'id' => Yii::app()->request->getQuery('id','')),array('class' => 'likeButton')); ?>
        </div>

        <?php echo CHtml::script('$("[data-delete-button]").on("click", function(){


        if($("._price_item").length > 1)
        {
            $("[data-price-id=" + $(this).attr("data-delete-button") +"]").remove();
        }
        else
        {
            alert("You cannot delete all prices!");
        }

  });
  ');?>

        <script>
            $(document).ready(function(){

                $("#add_more").focus();
            })
        </script>
    </div><!-- form -->

<?php
endif;