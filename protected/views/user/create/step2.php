<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Create'),
);

?>



<div class="form">

    <h2><?php echo Yii::t('app', 'Krok {step}', array('{step}' => '2/3'));?> | <?php echo Yii::t('user','Dane Twojego konta');?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));
    ?>


    <?php echo $form->errorSummary($model); ?>

    <?php if($model->type == User::TYPE_PRIVATE):?>
        <div class="row">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
            <?php echo $form->error($model,'name'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model,'surname'); ?>
            <?php echo $form->textField($model, 'surname', array('maxlength' => 45)); ?>
            <?php echo $form->error($model,'surname'); ?>
        </div><!-- row -->
    <?php else:?>
        <div class="row">
            <?php echo $form->labelEx($model,'company'); ?>
            <?php echo $form->textField($model, 'company', array('maxlength' => 45)); ?>
            <?php echo $form->error($model,'company'); ?>
        </div><!-- row -->
    <?php endif;?>
    <div class="row">
        <?php echo $form->labelEx($model,'country_id'); ?>
        <?php echo $form->dropDownList($model, 'country_id', GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true))); ?>
        <?php echo $form->error($model,'country_id'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'city'); ?>
        <?php echo $form->textField($model, 'city', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'city'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'zip_code'); ?>
        <?php echo $form->textField($model, 'zip_code', array('maxlength' => 10)); ?>
        <?php echo $form->error($model,'zip_code'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'address_line_1'); ?>
        <?php echo $form->textField($model, 'address_line_1', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'address_line_1'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'address_line_2'); ?>
        <?php echo $form->textField($model, 'address_line_2', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'address_line_2'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'tel'); ?>
        <?php echo $form->textField($model, 'tel', array('maxlength' => 20)); ?>
        <?php echo $form->error($model,'tel'); ?>
    </div><!-- row -->
    <?php if($model->type == User::TYPE_COMPANY):?>
        <div class="row">
            <?php echo $form->labelEx($model,'nip'); ?>
            <?php echo $form->textField($model, 'nip', array('maxlength' => 32)); ?>
            <?php echo $form->error($model,'nip'); ?>
        </div><!-- row -->

    <?php endif; ?>
    <div class="row">
        <?php echo $form->labelEx($model,'price_currency_id'); ?>
        <?php echo $form->dropDownList($model, 'price_currency_id',CHtml::listData(PriceCurrency::model()->findAll(),'id','symbol')); ?>
        <?php echo $form->error($model,'price_currency_id'); ?>
    </div><!-- row -->
    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step1'));
        echo TbHtml::submitButton(Yii::t('app','Dalej'),array('name'=>'summary'));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->