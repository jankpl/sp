<?php

class CourierUOperator2CountryGroupUserGroup extends CourierUOperator2CountryGroup
{
    protected $user_group_id;

    public function setUserGroupId($user_group_id)
    {
        $this->user_group_id = $user_group_id;
    }


    public $_custom_stat;

    public function getCustomStat()
    {
        $submodel = $this->with(['courierUCustomOperators4UserGroup' => ['params' => [':user_group_id' => $this->user_group_id]]])->courierUCustomOperators4UserGroup;

        if($submodel === NULL)
            return NULL;
        else
            return $submodel->stat;

    }

    public static function getCustomStatList()
    {
        $array = self::getStatList();
        $array[NULL] = 'default';

        return $array;
    }

    public function getCustomStatName()
    {
        return isset(self::getCustomStatList()[$this->getCustomStat()]) ? self::getCustomStatList()[$this->getCustomStat()] : '';
    }

    public static function updateCustomSettings(array $courierUOperator2CountryGroupIds, $user_group_id, $stat)
    {
        $cmd = Yii::app()->db->createCommand();

        $cmd->delete((new CourierUCustomOperators4UserGroup)->tableName(), 'user_group_id = :user_group_id', [':user_group_id' => $user_group_id]);

        if ($stat !== NULL) {

            $insertArray = [];
            foreach ($courierUOperator2CountryGroupIds AS $item) {
                $insertArray[] = [
                    'courier_u_operator_2_country_group_id' => $item,
                    'user_group_id' => $user_group_id,
                    'stat' => $stat,
                ];
        }

            $builder = Yii::app()->db->schema->commandBuilder;
            $command = $builder->createMultipleInsertCommand((new CourierUCustomOperators4UserGroup())->tableName(), $insertArray);
            $command->execute();
        }

        return true;
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('courier_country_group_id', $this->courier_country_group_id);
        $criteria->compare('courier_u_operator_id', $this->courier_u_operator_id);
        $criteria->compare('delivery_hours', $this->delivery_hours);
        $criteria->compare('stat', $this->stat);
        $criteria->compare('cod', $this->cod);

        return new CActiveDataProvider($this->with(['courierUCustomOperators4UserGroup' => ['params' => [':user_group_id' => $this->user_group_id]]]), array(
            'criteria' => $criteria,
        ));
    }

}