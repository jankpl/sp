<?php

use GuzzleHttp\Client;
use JMS\Serializer\SerializerBuilder;
use Linker\Api\Client\HttpApiClient;

class LinkerClient
{
    const TEST_MODE = false;
    const TEST_KEY = '8fa53bb8288251eb9183ac7214d5168c';

    const LIMIT = 9999;

    const ORDERS_GOBACK_MONTHS = 30;

    const MAX_IMPORT_NUMBER = 500;


    public static function sortAttributesList()
    {
        $data = [
            'id',
            'orderDate',
            'cod',
            'priceGross',
            'clientOrderNumber',
            'deliveryCountry',
            'deliveryRecipient',
        ];

        return $data;
    }


    protected static function _operatorTranslation($name, $receiverCountryId = false, $pageVersion = false)
    {
        $data = [
            'Sweden Post' => 'DirectLink',
        ];

        if($receiverCountryId && $pageVersion == PageVersion::PAGEVERSION_CQL && $name == Postal::NONREG_DEFAULT_OPERATOR_NAME)
        {
//            if($receiverCountryId == CountryList::COUNTRY_UK)
            return 'RoyalMail%%%nr';
//            else
//                return 'PostNord';
        }

        if($pageVersion == PageVersion::PAGEVERSION_CQL && preg_match('/^Deutche Post/', $name))
            return 'Deutsche Post';

        if(isset($data[$name]))
            return $data[$name];
        else
            return $name;
    }


    public static function getOrders($dateFrom = NULL, $dateTo = NULL, $stat = NULL, $target = LinkerImport::TARGET_COURIER, $sort_attr = false, $sort_desc = false, $statProcessed = NULL)
    {


        if (self::TEST_MODE) {
            $apiKey = self::TEST_KEY;
            $uri = 'https://api.test.linker.shop/public-api/v1';
        }
        else {
            $apiKey = Yii::app()->user->model->apikey;
            $apiKey = '286cba4803281ba1544cb1820e6b047c8bf37a5da02f4b4752617950a7526c72';
            $uri = 'https://api.swiatprzesylek.linker.shop/public-api/v1';
        }


        $filters = '';
        if($dateFrom)
        {
            $temp = explode('-', $dateFrom);
            $dateFrom = $temp[2].'.'.$temp[1].'.'.$temp[0];
            $filters .= '&filters%5Border_date%5D='.$dateFrom;
        }

        if($dateTo)
        {
            $temp = explode('-', $dateTo);
            $dateTo = $temp[2].'.'.$temp[1].'.'.$temp[0];
            $filters .= '&filters%5Border_date_to%5D='.$dateTo;
        }

        if($sort_desc)
            $sort = 'DESC';
        else
            $sort = 'ASC';

        if($sort_attr && in_array($sort_attr, self::sortAttributesList()))
            $sortColumn = $sort_attr;
        else
            $sortColumn = 'priceGross';


        if($stat == LinkerOrderItem::STAT_SHOW_LINKER_SENT)
            $orderStatus = '&filters%5Border_status%5D=Y';
        else if($stat == LinkerOrderItem::STAT_SHOW_LINKER_NOT_SENT)
            $orderStatus = '&filters%5Border_status%5D=N';
        else
            $orderStatus = '';

        $url = $uri.'/orders?apikey='.$apiKey.$orderStatus.$filters.'&limit='.self::LIMIT.'&offset=0&sortCol='.$sortColumn.'&sortDir='.$sort;


        $data = @file_get_contents($url);
        $data = @json_decode($data, true);

        MyDump::dump('linker.txt', $apiKey.' : GET ITEM : '.print_r($url,1));
//        MyDump::dump('linker.txt', print_r($data,1));

        $orders = [];
        if(is_array($data['items']))
            foreach($data['items'] AS $item) {
                $linkerItem = new LinkerOrderItem($item, NULL, $target);

                if($statProcessed == LinkerOrderItem::STAT_SHOW_NOT_PROCESSED && $linkerItem->statProcessed)
                    continue;
                else if($statProcessed == LinkerOrderItem::STAT_SHOW_PROCESSED && !$linkerItem->statProcessed)
                    continue;
                else
                    $orders[] = $linkerItem;
            }

        return [ $orders, true ];
    }

    public static function updateItem($id, $tt, $operator, $forceKey = false, $receiverCountryId = false, $pageVersion = false)
    {
        $operator = self::_operatorTranslation($operator, $receiverCountryId, $pageVersion);

        if (self::TEST_MODE) {
            $apiKey = self::TEST_KEY;
            $uri = 'https://api.test.linker.shop/public-api/v1';
        }
        else {
            $apiKey = Yii::app()->user->model->apikey;
            $uri = 'https://api.swiatprzesylek.linker.shop/public-api/v1';

            if($forceKey)
                $apiKey = $forceKey;
        }

        $client  = new Client([
            'verify' => false
        ]);

        $serializer = SerializerBuilder::create()->build();
        $client = new HttpApiClient($client, $serializer, $uri, $apiKey);

        $ttNo = new \Linker\Api\Model\TrackingNumber();
        $ttNo->setTrackingNumber($tt);
        $ttNo->setOperator($operator);
        $ttNo->setParcelId($tt);

        \Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
            'JMS\Serializer\Annotation', Yii::app()->basePath . '/../vendor/jms/serializer/src'
        );

        // ON PHP 7.3:
        $phpV = (float) phpversion();
        if($phpV == 7.3)
            require_once(Yii::app()->basePath.'/../vendor_7.3/jms/serializer/src/Annotation/Type.php');

        $id = str_replace('#','', $id);

        MyDump::dump('linker.txt', 'UPDATE ITEM : '.print_r($tt,1).','.print_r($operator,1).','.print_r($id,1));


        return true;


        try {
            MyDump::dump('linker.txt', $apiKey.' : UPDATE ITEM : '.print_r($tt,1).','.print_r($operator,1).','.print_r($id,1));

            $resp = $client->setTrackingNumber($id, $ttNo);

            MyDump::dump('linker.txt', $apiKey.' : UPDATE ITEM RESULT : '.print_r($tt,1).','.print_r($resp,1));
        }
        catch (Exception $ex)
        {
            MyDump::dump('linker_tt_update_backlog.txt', $apiKey.'||'.$id.'||'.$tt.'||'.$operator);
            Yii::log('LINKER TT UPDAT ERROR: '.print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
            MyDump::dump('linker.txt', $apiKey.' : UPDATE ITEM ERROR : '.print_r($ex->getMessage(),1));
            return false;
        }
        return true;
    }
}