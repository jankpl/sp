<?php
/* @var $id_prefix string */
?>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'exoport-multi',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/massExportByGridView'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('courier', 'Exportuj do .xls'), array(
    'onClick' => 'js:

    if(!confirm("'.Yii::t("site", "Czy jesteś pewien?").'"))
         return false;

    $("#'.$id_prefix.'courier-ids-export-multi").val($("#couriergrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => $id_prefix.'courier-ids-export-multi')); ?>
<?php
$this->endWidget();
?>

