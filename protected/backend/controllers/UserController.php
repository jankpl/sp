<?php

class UserController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>Admin::AUTHORITY_ADMIN.' >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => ['view', 'create','update','index','admin','ajaxPrepareImage', 'exportAll', 'ajaxCheckVies', 'parentAccoutingUser'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

//    DEPRECATED
//    public function actionAddToUserBalance()
//    {
//        /* @var $model UserBalance */
//
//        $model = new UserBalance();
//
//        if (isset($_POST['User'])) {
//
//            if($model->addToBalance($_POST['User']['courier_stat_id']))
//                $this->redirect(array('view', 'id' => $model->id));
//            else
//                throw new Exception("Błąd!");
//        } else
//            throw new CHttpException('404');
//
//    }


    public function actionAjaxCheckVies()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $vies = $_POST['vies'];

            $result = ViesCheck::check($vies);

            if($result === NULL)
                $result = -1;

            echo CJSON::encode($result);

        }
    }

    public function actionView($id) {

        /* @var $model User */
        $model =  $this->loadModel($id, 'User');

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR && $model->salesman_id != Yii::app()->user->model->salesman_id)
            throw new CHttpException(403);


        /* @var $modelSms UserSendSms */
        $modelSms = new UserSendSms();
        if(isset($_POST['UserSendSms']))
        {
            $modelSms->attributes = $_POST['UserSendSms'];
            if($modelSms->validate() && $model->tel != '')
            {
                $modelSms->pv = $model->source_domain;
                $modelSms->send($model->tel);

                Yii::app()->user->setFlash('success', 'SMS has been sent!');
                $this->refresh();

            }
        }

        /* @var $modelEmail UserInvoiceSendMail */
        $modelEmail = new UserInvoiceSendMail();
        if(isset($_POST['UserInvoiceSendMail']))
        {
            $modelEmail->attributes = $_POST['UserInvoiceSendMail'];
            if($modelEmail->validate())
            {

                $modelEmail->send($model->getAccountingEmail(true), $model->name, $model);

                Yii::app()->user->setFlash('success', 'Message has been sent!');
                $this->refresh();
            }
        }

        $this->render('view', array(
            'model' => $model,
            'modelEmail' => $modelEmail,
            'modelSms' => $modelSms
        ));
    }

    public function actionCreate() {

        $model=new User('insert');
        $model->premium = User::PREMIUM_YES;
        $model->activated = true;

        $model->salesman_id = Yii::app()->user->model->salesman_id;
        if(Yii::app()->user->model->salesman)
            $model->injection_sp_point_id = Yii::app()->user->model->salesman->default_injection_sp_point_id;

        if (isset($_POST['User'])) {
            $model->setAttributes($_POST['User']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id) {


        Yii::app()->getModule('courier');

        /* @var $model User */

        $model = $this->loadModel($id, 'User');

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR && $model->salesman_id != Yii::app()->user->model->salesman_id)
            throw new CHttpException(403);

        $model->scenario = 'update';
        $model->pass = '';
        $model->passCompare = '';



        $this->performAjaxValidation($model, 'user-form');


        $returnAddressModel = $model->getReturnAddressModel();
        if(!$returnAddressModel)
            $returnAddressModel = new AddressData();

        if(isset($_POST['saveReturnAddressData']))
        {
            $returnAddressModel->attributes = $_POST['AddressData'];
            if($returnAddressModel->validate()) {

                $model->setReturnAddressModel($returnAddressModel);
                Yii::app()->user->setFlash('success', 'Return address settings has been saved');
                $this->redirect(array('update', 'id' => $id));
                exit;
            }
        }

        if(isset($_POST['toggleSalesman']))
        {

            $value = $_POST['User']['salesman'];

            $model->setSalesman($value);

            Yii::app()->user->setFlash('success', 'Ustawienia opiekuna handlowego zostały zapisane');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleCourierRefPrefix']))
        {
            $value = $_POST['User']['courierRefPrefix'];

            try {
                if($model->setCourierRefPrefix($value))
                    Yii::app()->user->setFlash('success', 'Ref prefix has been saved!');
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', $e->getMessage());
            }

            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['togglePostalRefPrefix']))
        {
            $value = $_POST['User']['postalRefPrefix'];

            try {
                if($model->setPostalRefPrefix($value))
                    Yii::app()->user->setFlash('success', 'Ref prefix has been saved!');
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', $e->getMessage());
            }

            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleClientImg']) or isset($_POST['removeClientImg']))
        {

            $value = $_POST['User']['clientImg'];

            if(isset($_POST['removeClientImg']))
                $value = false;

            $model->setClientImg($value);

            Yii::app()->user->setFlash('success', 'Ustawienia obrazka zostały zapisane');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleExternalIdsInNotifications']))
        {

            $value = $_POST['User']['externalIdsInNotifications'];

            $model->setExternalIdsInNotifications($value);

            Yii::app()->user->setFlash('success', Yii::t('user', 'Setting for external ids in notifications has been saved!'));
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleInternalPickupTypeCustomSettings']) OR isset($_POST['defaultInternalPickupTypeCustomSettings']))
        {

            $value = $_POST['internalPickupTypeCustomSettings'];

            $model->setInternalPickupTypeCustomSettings($value, isset($_POST['defaultInternalPickupTypeCustomSettings']));

            Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia Internal pickup zostały zapisane'));
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleBlockNotify']))
        {
            $notify = $_POST['User']['adminBlockNotifySms'];
            $model->setAdminBlockNotifySms($notify);

            Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia blokowania powiadomień zostały zaktualizowane'));
            $this->redirect(array('update', 'id' => $id));
            exit;

        }
        else if(isset($_POST['toggleAccountingEmail']))
        {
            $success = false;

            if($model->setAccountingEmail($_POST['User']['accountingEmail']))
                $success = true;

            if($model->setAccountingEmail2($_POST['User']['accountingEmail2']))
                $success = true;

            if($model->setAccountingEmail3($_POST['User']['accountingEmail3']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Accouting email has beem saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleAccountingTel']))
        {
            $success = false;

            if($model->setAccountingTel($_POST['User']['accountingTel']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Accouting tel has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleDebtBlockDays']))
        {
            if(0 < Yii::app()->user->authority)
                throw new Exception(403);

            $success = false;

            if($model->setDebtBlockDays($_POST['User']['debtBlockDays']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Debt block days has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleDebtBlock']))
        {
            if(0 < Yii::app()->user->authority)
                throw new Exception(403);


            $success = false;

            if($model->setDebtBlock($_POST['User']['debtBlock']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Debt block has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['togleCustomDpdPlVSubaccount'])) {
            $success = false;

            if ($model->setCustomDpdPlVSubaccount($_POST['User']['customDpdPlVSubaccount']))
                $success = true;

            if ($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleMultipackagesAsOneItem'])) {
            $success = false;

            if ($model->setMultipackagesAsOneItem($_POST['User']['multipackagesAsOneItem']))
                $success = true;

            if ($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['togleCustomDpdPlSPSubaccount']))
        {
            $success = false;

            if($model->setCustomDpdPlSPSubaccount($_POST['User']['customDpdPlSPSubaccount']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleYunExpressNameAddition']))
        {
            $success = false;

            if($model->setYunExpressNameAddition($_POST['User']['yunExpressNameAddition']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleDisableDimensionalWeight']))
        {
            $success = false;

            if($model->setDisableDimensionalWeight($_POST['User']['disableDimensionalWeight']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleUserMenuSimple']))
        {
            $success = false;

            if($model->setUserMenuSimple($_POST['User']['userMenuSimple']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleApiDhlDeNotification']))
        {
            $success = false;

            if($model->setApiDhlDeNotification($_POST['User']['apiDhlDeNotification']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleDpdplNstRedirectActive']))
        {
            $success = false;

            if($model->setDpdplNstRedirectActive($_POST['User']['dpdplNstRedirectActive']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleSpZoo']))
        {
            $success = false;

            if($model->setSpZooChangeDate($_POST['User']['spZooChangeDate']))
                $success = true;

            if($model->setSpZooStatus($_POST['User']['spZooStatus']))
                $success = true;

            if($success)
                Yii::app()->user->setFlash('success', 'Setting has been saved');

            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['setParentAccountingUser']))
        {
            $value = $_POST['User']['parentAccountingUser'];
            $model->setParentAccountingUser($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }
        else if(isset($_POST['toggleDisableDimensionLimits']))
        {
            $value = $_POST['User']['disableDimensionLimits'];
            $model->setDisableDimensionLimits($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }
        else if(isset($_POST['toggleDisableOrderingNewItems']))
        {
            $value = $_POST['User']['disableOrderingNewItems'];
            $model->setDisableOrderingNewItems($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }
        else if(isset($_POST['toggleDhlDeCustomAccountCodeSuffix']))
        {
            $value = $_POST['User']['dhlDeCustomAccountCodeSuffix'];
            $model->setDhlDeCustomAccountCodeSuffix($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }
        else if(isset($_POST['toggleForcePackageSenderDataDhlDe']))
        {
            $value = $_POST['User']['forcePackageSenderDataDhlDe'];
            $model->setForcePackageSenderDataDhlDe($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }
        else if(isset($_POST['togglePackageSlipFooter']))
        {
            $value = $_POST['User']['packageSlipFooter'];
            $model->setPackageSlipFooter($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }
        else if(isset($_POST['toggleManifestGenerationSetsStatus']))
        {
            $value = $_POST['User']['manifestGenerationSetsStatus'];
            $model->setManifestGenerationSetsStatus($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }
        else if(isset($_POST['toggleDontOverwriteSenderDataDhlDe']))
        {
            $value = $_POST['User']['dontOverwriteSenderDataDhlDe'];
            $model->setDontOverwriteSenderDataDhlDe($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }


        else if(isset($_POST['toggleLinkerImportAvailable']))
        {
            $value = $_POST['User']['linkerImportAvailable'];
            $model->setLinkerImportAvailable($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }
        else if(isset($_POST['toggleHidePrices']))
        {
            $value = $_POST['User']['hidePrices'];
            $model->setHidePrices($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleUpsCustomData']))
        {
            $value = $_POST['User']['upsCustomData'];

            $model->setUpsCustomData($value['id'], $value['login'],$value['password'], $value['active'], $value['shipper'], $value['shipper_name'], $value['shipper_address'], $value['shipper_city'], $value['shipper_postal'], $value['shipper_country'], $value['shipper_tel']);

            Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia UPS zostały zapisane'));
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleCustomVatRate']))
        {
            $value = $_POST['User']['customVatRate'];

            try {
                $model->setCustomVatRate($value);
            }
            catch (Exception $ex)
            {
                Yii::app()->user->setFlash('error', $ex->getMessage());
                $this->redirect(array('update', 'id' => $id));
                exit;
            }

            Yii::app()->user->setFlash('success', 'Custom Vat Rate setting saved!');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleCourierInternalDefaultPickupFromApi']))
        {
            $value = $_POST['User']['courierInternalDefaultPickupFromApi'];

            try {
                $model->setCourierInternalDefaultPickupFromApi($value);
            }
            catch (Exception $ex)
            {
                Yii::app()->user->setFlash('error', $ex->getMessage());
                $this->redirect(array('update', 'id' => $id));
                exit;
            }

            Yii::app()->user->setFlash('success', 'Courier Internal Default Pickup For API setting saved!');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleDhlDeCustomData']))
        {
            $value = $_POST['User']['dhlDeCustomData'];

            $model->setDhlDeCustomData($value['active'], $value['sender_name'], $value['sender_company'], $value['sender_address_line_1'],  $value['sender_address_line_2'], $value['sender_city'], $value['sender_zipcode'], $value['sender_country'], $value['sender_tel'], $value['sender_email']);

            Yii::app()->user->setFlash('success', 'DHL DE setting has been saved!');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleLpExpressCustomData']))
        {
            $value = $_POST['User']['lpExpressCustomData'];

            $model->setLpExpressCustomData($value['active'], $value['sender_name'], $value['sender_address'], $value['sender_city'], $value['sender_zipcode'], $value['sender_tel']);

            Yii::app()->user->setFlash('success', 'LP Express setting has been saved!');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleContractType']))
        {
            $value = $_POST['User']['contractType'];
            $model->setContractType($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleAnnotationModeAndText']))
        {

            $model->setAnnotationMode($_POST['User']['annotationMode']);
            $model->setAnnotationText($_POST['User']['annotationText']);
            $model->setAnnotationAttribute($_POST['User']['annotationAttribute']);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }


        if(isset($_POST['togglePrintServerActive']))
        {
            $value = $_POST['User']['printServerActive'];
            $model->setPrintServerActive($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleUpsCollectBlock']))
        {
            $value = $_POST['User']['upsCollectBlock'];
            $model->setUpsCollectBlock($value);

            Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia blokowania automatycznej usługi Collect (oprócz paczek Domestic)'));
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleDomesticCollectBlock']))
        {
            $value = $_POST['User']['domesticCollectBlock'];
            $model->setDomesticCollectBlock($value);

            Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia blokowania automatycznej usługi Collect dla paczek Domestic'));
            $this->redirect(array('update', 'id' => $id));
            exit;

        }


        if(isset($_POST['toggleAllowCourierInternalExtendedLabel']))
        {
            $value = $_POST['User']['allowCourierInternalExtendedLabel'];
            $model->setAllowCourierInternalExtendedLabel($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleShowCourierInternalOperatorsData']))
        {
            $value = $_POST['User']['showCourierInternalOperatorsData'];
            $model->setShowCourierInternalOperatorsData($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleCourierOoeAvailable']))
        {
            $value = $_POST['User']['courierOoeAvailable'];
            $model->setCourierOoeAvailable($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleCourierDomesticAvailable']))
        {
            $value = $_POST['User']['courierDomesticAvailable'];
            $model->setCourierDomesticAvailable($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleCourierUAvailable']))
        {
            $value = $_POST['User']['courierUAvailable'];
            $model->setCourierUAvailable($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleCourierUDomesticAvailable']))
        {
            $value = $_POST['User']['courierUDomesticAvailable'];
            $model->setCourierUDomesticAvailable($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleCourierReturnAvailable']))
        {
            $value = $_POST['User']['courierReturnAvailable'];
            $model->setCourierReturnAvailable($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleCourierExternalReturnAvailable']))
        {
            $value = $_POST['User']['courierExternalReturnAvailable'];
            $model->setCourierExternalReturnAvailable($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

//        if(isset($_POST['toggleCourierSimpleAvailable']))
//        {
//            $value = $_POST['User']['courierSimpleAvailable'];
//            $model->setCourierSimpleAvailable($value);
//
//            Yii::app()->user->setFlash('success', 'Settings has been saved');
//            $this->redirect(array('update', 'id' => $id));
//            exit;
//        }

//        if(isset($_POST['togglePostalAvailable']))
//        {
//            $value = $_POST['User']['postalAvailable'];
//            $model->setPostalAvailable($value);
//
//            Yii::app()->user->setFlash('success', 'Settings has been saved');
//            $this->redirect(array('update', 'id' => $id));
//            exit;
//        }

        if(isset($_POST['togglePostalAvailableSms']))
        {
            $value = $_POST['User']['postalAvailableSms'];
            $model->setPostalAvailableSms($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['togglePpDaysDelay']))
        {
            $value = $_POST['User']['ppDaysDelay'];
            $model->setPPDaysDelay($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleApiAccessActive']))
        {
            $value = $_POST['User']['apiAccessActive'];
            $model->setApiAccessActive($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        if(isset($_POST['toggleCourierExternalDisable']))
        {
            $value = $_POST['User']['courierExternalDisable'];
            $model->setCourierExternalDisable($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleDpdPlSpDisableSenderOverwrite']))
        {
            $value = $_POST['User']['dpdPlSpDisableSenderOverwrite'];
            $model->setDpdPlSpDisableSenderOverwrite($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['toggleHideSenderAddressOnNotificationAndTt']))
        {
            $value = $_POST['User']['hideSenderAddressOnNotificationAndTt'];
            $model->setHideSenderAddressOnNotificationAndTt($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

//        if(isset($_POST['togglePostalInternationalDisable']))
//        {
//            $value = $_POST['User']['postalInternationalDisable'];
//            $model->setPostalInternationalDisable($value);
//
//            Yii::app()->user->setFlash('success', 'Settings has been saved');
//            $this->redirect(array('update', 'id' => $id));
//            exit;
//        }
//
//        if(isset($_POST['togglePostalDomesticDisable']))
//        {
//            $value = $_POST['User']['postalDomesticDisable'];
//            $model->setPostalDomesticDisable($value);
//
//            Yii::app()->user->setFlash('success', 'Settings has been saved');
//            $this->redirect(array('update', 'id' => $id));
//            exit;
//        }

        if(isset($_POST['toggleCourierUCurrencyId']))
        {
            $value = $_POST['User']['courierUCurrencyId'];
            $model->setCourierUCurrencyId($value);

            Yii::app()->user->setFlash('success', 'Settings has been saved');
            $this->redirect(array('update', 'id' => $id));
            exit;
        }

        if(isset($_POST['activate']))
        {
            $model->setToggleActivate();
            Yii::app()->user->setFlash('notice','User status has been changed.');
        }
        else if(isset($_POST['premium']))
        {
            $model->setTogglePremium();
            Yii::app()->user->setFlash('notice','User status has been changed.');
        }
        else if(isset($_POST['without_vat']))
        {
            $model->setToggleWithVat();
            Yii::app()->user->setFlash('notice','User status has been changed.');
        }
        else if(isset($_POST['on_invoice']))
        {
            $model->setToggleOnInvoice();
            Yii::app()->user->setFlash('notice','User status has been changed.');
        }
        else if (isset($_POST['saveUserModel'])) {
            $model->setAttributes($_POST['User']);


            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }


        $this->render('update', array(
            'model' => $model,
            'returnAddressModel' => $returnAddressModel,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'User')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('user/admin'));
    }

//    public function actionBalance($id, $page = 1, $currency = NULL) {
//        /* @var $model User */
//        $model = new UserBalance();
//        $modelUser = $this->loadModel($id, 'User');
//
//
//        if($currency === NULL) {
//            $currency = $modelUser->price_currency_id;
//            $currency = PriceCurrency::model()->findByPk($currency);
//            $currency = $currency->short_name;
//        }
//
//        if(!in_array($currency, Currency::listIdsWithNames()))
//            throw new CHttpException(404);
//
//
//        $model->setUser($modelUser->id);
//        $model->setCurrency($currency);
//
//
//
//        if (isset($_POST['UserBalance'])) {
//            $user_id = $id;
//
//            $model->amount = $_POST['UserBalance']['amount'];
//            $model->description = $_POST['UserBalance']['description'];
//            $model->user_id = $user_id;
//            $model->currency = $_POST['UserBalance']['currency'];
//
//            if ($model->save()) {
//                $this->redirect(array('user/balance', 'id' => $user_id));
//            }
//        }
//
//
//        $page_size = 50;
//        $page = intval($page) - 1;
//
////        $currency = $model->getCurrency();
//
//        $item_count = $model->getItemsCount();
//
//        if($page < 0 OR ($page & $page_size) > $item_count) {
//            $page = 1;
//        }
//
//        $models = $model->getItemsWithBalance($currency, $page,$page_size);
//
//        $this->render('balance', array(
//            'modelUser' => $modelUser,
//            'model' => $model,
//            'models' => $models,
//            'current_page' => $page,
//            'item_count' => $item_count,
//            'page_size' => $page_size,
//            'currency' => $currency,
//            'currencySelected' => $currency,
//        ));
//
//    }

    public function actionAdmin() {



        $model = new _UserGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['UserGridView']))
            $model->setAttributes($_GET['UserGridView']);

        $this->render('admin', array(
            'model' => $model
        ));
    }


    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewUser(){
        $model = new _UserGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['UserGridView']))
            $model->setAttributes($_GET['UserGridView']);



        $this->widget('backend.extensions.selgridview.BootSelGridView', array(
            'id' => 'user',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>2,
            'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'afterAjaxUpdate' => true, 'withChosenPlugin' => true, 'chosenSelector' => 'UserGridView[id]'), true),
            'selectionChanged'=>'function(id){  
                var number = $("#user").selGridView("getAllSelection").length;
                $(".selected-no").html(number); 
         }',
            'columns' => array(
                array(
                    'name' => '_id',
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                    'value' => '$data->id',
                ),
                array(
                    'name' => 'id',
                    'value' => '$data->login',
                    'filter' => CHtml::listData(User::model()->findAll(['order' => 'login ASC']),'id', 'login'),
                    'htmlOptions' => [
                        'width' => '100',
                    ],
                    'header' => 'Login',
                ),
                'login',
                'email',

                array(
                    'name' => 'date_entered',
                    'value' => 'substr($data->date_entered,0,-3)',
                    'headerHtmlOptions' => array('style' => 'width: 100px;'),
                ),
                array(
                    'name' => 'last_logged',
                    'value' => 'substr($data->last_logged,0,-3)',
                    'headerHtmlOptions' => array('style' => 'width: 100px;'),
                ),

                array(
                    'name' => 'premium',
                    'header' => 'Premium',
                    'value' => '$data->getIsPremium()?"yes":"no"',
                    'filter'=>(array(0 => 'no', 1 => 'yes')),
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                array(
                    'name' => 'activated',
                    'header' => 'Activated',
                    'value' => '$data->activated?"yes":"no"',
                    'filter'=>(array(0 => 'no', 1 => 'yes')),
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                array(
                    'header' => 'Couriers',
                    'value' => '$data->couriersSTAT',
                    'filter' => false,
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                array(
                    'header' => 'Postals',
                    'value' => '$data->postalsSTAT',
                    'filter' => false,
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                array(
                    'header' => 'Palettes',
                    'value' => '$data->palettesSTAT',
                    'filter' => false,
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                array(
                    'header' => 'STs',
                    'value' => '$data->specialTransportsSTAT',
                    'filter' => false,
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
//                array(
//                    'header' => 'Orders',
//                    'value' => '$data->ordersSTAT',
//                    'filter' => false,
//                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
//                ),
                array(
                    'header' => 'Without VAT',
                    'value' => '$data->without_vat?"yes":"no"',
                    'name' => 'without_vat',
                    'filter'=>(array(0 => 'no', 1 => 'yes')),
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                array(
                    'header' => 'PayOnAccount',
                    'value' => '$data->payment_on_invoice?"yes":"no"',
                    'name' => 'payment_on_invoice',
                    'filter'=>(array(0 => 'no', 1 => 'yes')),
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                array(
                    'header' => 'Type',
                    'value' => '($data->type==User::TYPE_PRIVATE?"P":"C")',
                    'name' => 'type',
                    'filter' => array(User::TYPE_PRIVATE => 'Private', User::TYPE_COMPANY => 'Company'),
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
//                array(
//                    'header' => 'Balance',
//                    'value' => 'UserBalance::calculateTotalBalanceForUserId($data->id)',
//                ),
                array(
                    'header' => 'Currency',
                    'value' => 'Currency::nameById($data->price_currency_id)',
                    'name' => 'price_currency_id',
                    'filter' => CHtml::listData(PriceCurrency::model()->findAll(),'id','symbol'),
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                array(
                    'header' => 'Source',
                    'value' => '$data->sourceDomainName',
                    'name' => 'source_domain',
                    'filter' => User::sourceDomainList(),
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ),
                [
                    'name' => '_contract_type',
                    'value' => '$data->getContractTypeName()',
                    'filter' => User::getContractTypeList(),
                ],
                [
                    'name' => '_debt_block',
                    'value' => '$data->getDebtBlockName()',
                    'filter' => DebtBlock::getStatList(),
                ],
                [
                    'name' => '_debt_block_days',
                    'value' => '$data->getDebtBlockDays()',
                ],

                [
                    'name' => 'salesman_id',
                    'value' => '$data->salesman ? $data->salesman->name : ""',
                    'filter' => CHtml::listData(Salesman::model()->findAll(),'id', 'name'),
                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ],
                [
                    'name' => 'injection_sp_point_id',
                    'value' => '$data->injectionSpPoint ? $data->injectionSpPoint->nameWithId : ""',
                    'filter' => CHtml::listData(SpPoints::model()->findAll(),'id', 'nameWithId'),
                ],

                [
                    'name' => 'partner_id',
                    'value' => 'Partners::getPartnerNameById($data->partner_id)',
                    'filter' => Partners::getParntersList(),
                ],

                [
                    'name' => '_spzoo',
                    'value' => '$data->getSpZooStatus() ? "Y" : "N"',
                    'filter' => [-1 => 'No', 1 => 'Yes'],
//                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ],

                [
                    'name' => 'nip',
//                    'value' => '$data->getSpZooStatus() ? "Y" : "N"',
//                    'filter' => [-1 => 'No', 1 => 'Yes'],
//                    'headerHtmlOptions' => array('style' => 'width: 30px;'),
                ],

                array(
                    'class' => 'CButtonColumn',
//                    'htmlOptions' => array('width' => 60),
                    'template' => '{view} {update}',
                ),
                array(
                    'class'=>'CCheckBoxColumn',
                    'id'=>'selected_rows'
                ),


            ),
        ));
    }

    public function actionAjaxPrepareImage()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            try {
                $img = $_POST['img'];

                $img = explode(',', $img);

                if(!isset($img[1]))
                    throw new Exception;

                $img = $img[1];

                $img = base64_decode($img);

                if($img == '')
                    throw new Exception;

                $im = ImagickMine::newInstance();
                if(!$im->readImageBlob($img))
                    throw new Exception;

                if ($im->getImageWidth() > User::CLIENT_IMG_WIDTH)
                    $im->scaleImage(User::CLIENT_IMG_WIDTH, 0);

                if ($im->getImageHeight() > User::CLIENT_IMG_HEIGHT)
                    $im->scaleImage(0, User::CLIENT_IMG_HEIGHT);


                $im->setFormat('png');

                $imgString = $im->__tostring();
                $imgString = base64_encode($imgString);
                $imgString = 'data:' . $im->getImageMimeType() . ';base64,' . $imgString;

                echo CJSON::encode(['image' => $imgString]);
            }
            catch(Exception $ex)
            {
                echo CJSON::encode(['image' => false]);
            }
        } else {
            $this->redirect(['/']);
        }
    }


    public function actionEmailer()
    {

        /* @var $modelEmail UserSendMailGroup */
        $modelEmail = new UserSendMailGroup();

        if(isset($_POST['User']))
        {
            $modelEmail->setIds($_POST['User']['ids']);
        }

        if(isset($_POST['UserSendMailGroup']))
        {

            $modelEmail->attributes = $_POST['UserSendMailGroup'];


            $toAll = $_POST['UserSendMailGroup']['toAll'];

            if ($toAll == UserSendMailGroup::TO_ALL)
                $modelEmail->setIds(implode(',', UserSendMailGroup::getAllUsersIds()));
            else if ($toAll == UserSendMailGroup::TO_ALL_ACTIVE)
                $modelEmail->setIds(implode(',', UserSendMailGroup::getActiveUserIds()));

            if(!$modelEmail->getUserNumber())
            {
                Yii::app()->user->setFlash('error', 'No receipients selected!');
                $this->redirect(['/user/admin']);
                Yii::app()->end;
            }

            $modelEmail->attachment = CUploadedFile::getInstance($modelEmail, "attachment");
            if($modelEmail->validate())
            {
                $modelEmail->send();

                Yii::app()->user->setFlash('success', 'Messages have been sent!');
                $this->redirect(['/user/admin']);
                Yii::app()->end;
            } else {
                if($modelEmail->toAll)
                    $modelEmail->clear();
            }

        }

        $this->render('emailer',
            [
                'modelEmail' => $modelEmail,
            ]);

    }

    public function actionExportall()
    {
        $models = User::model();

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
            $models = $models->findAll('salesman_id = :salesman_id', [':salesman_id' => Yii::app()->user->model->salesman_id]);
        else
            $models = $models->findAll();

        $array = [];

        $temp = $models[0];

        $attributes = $temp->getAttributes();

        unset($attributes['apikey']);
        unset($attributes['pass']);
        unset($attributes['hash']);

        $attributes['salesman'] = NULL;
        $attributes['spzoo'] = NULL;
        $attributes['parent_accounting_user_id_#50'] = NULL;
        $attributes['disable_ordering_new_items_#53'] = NULL;

        $array[] = array_keys($attributes);

        /* @var $model User */
        foreach($models AS $model)
        {
            $item = $model->getAttributes();

            unset($item['apikey']);
            unset($item['pass']);
            unset($item['hash']);

            $item['salesman'] = $model->salesman->name;
            $item['spzoo'] = $model->getSpZooStatus() ? '1' : '0';
            $item['parent_accounting_user_id_#50'] = $model->getParentAccountingUser() ? $model->getParentAccountingUser() : '';
            $item['disable_ordering_new_items_#53'] = $model->getDisableOrderingNewItems() ? 1 : '';

            $array[] = $item;

        }

        S_XmlGenerator::generateXml($array);
    }

    public function actionAjaxGetUserAddressData()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $id = $_POST['id'];

            /* @var $user User */
            $user = User::model()->findByPk($id);

            if($user === NULL)
                $result = false;
            else
            {
                $addressData = $user->getAddressDataOrderModel();
                $result = $addressData->getAttributes();
                $result['_nip'] = $addressData->_nip;
                $result['_vatRate'] = $user->getCustomVatRate(true, true);
            }
            echo CJSON::encode($result);

        }
    }

    public function actionClone()
    {
        if(!AdminRestrictions::isSuperAdmin())
            throw new CHttpException(403);

        $hash = $_POST['hash'];
        $suffix = trim($_POST['suffix']);
        $model = User::model()->findByAttributes(['hash' => $hash]);

        if($suffix == '' OR preg_match('/[^a-zA-Z0-9_]+/', $suffix))
        {
            Yii::app()->user->setFlash('error', 'Suffix can contain only letters, numbers and _');
            $this->redirect(['/user/view', 'id' => $model->id]);
        }
        else if(User::model()->findByAttributes(['login' => $model->login.$suffix]))
        {
            Yii::app()->user->setFlash('error', 'There already is account with this login.');
            $this->redirect(['/user/view', 'id' => $model->id]);
        }



        if(!$model)
            throw new CHttpException(404);

        try {
            $new = $model->cloneUser($suffix);
        }
        catch(Exception $ex)
        {
            Yii::app()->user->setFlash('error', 'Clone failed: '.print_r($ex->getMessage(),1));
            $this->redirect(['/user/view', 'id' => $model->id]);
        }

        Yii::app()->user->setFlash('success', 'Account cloned! New account: <a href="'.Yii::app()->createUrl('/user/view', ['id' => $new->id]).'" target="_blank"><strong>'.$new->login.'</strong></a>');
        $this->redirect(['/user/view', 'id' => $model->id]);


    }

    public function actionParentAccoutingUserList()
    {
        $this->render('parentAccoutingUserList');
    }


}
