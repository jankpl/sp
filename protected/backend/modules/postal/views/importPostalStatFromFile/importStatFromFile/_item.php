<?php
/* @var $model F_MassStatusChangePostal */
/* @var $i Integer */

?>


<tr class="condensed">
    <td>
        <?php echo CHtml::activeHiddenField($model,'['.$i.']postal_local_id'); ?>
        <?php
        if($model->postalFound())
            echo CHtml::link($model->postal_local_id, Yii::app()->createUrl('/postal/postal/view', array('id' => $model->postal_id)), array('target' => '_blank'));
        else
            echo $model->postal_local_id;
        ?>
    </td><td>
        <?php echo $model->duplicate?TbHtml::badge('yes', array('color' => TbHtml::BADGE_COLOR_WARNING)):TbHtml::badge('no', array('color' => TbHtml::BADGE_COLOR_SUCCESS));?>
    </td><td>
        <?php echo $model->postalFound()?TbHtml::badge('yes', array('color' => TbHtml::BADGE_COLOR_SUCCESS)):TbHtml::badge('no', array('color' => TbHtml::BADGE_COLOR_WARNING));?>
    </td>
    <?php if(!$model->postalFound()):?>
        <td colspan="3"></td>
    <?php else:?>
        <td>
            <?php echo $model->postalFound()?$model->Postal()->stat->name:'-'; ?>
        </td><td>
            <?php
            if($split!=F_UploadFile::SPLIT_FULL_EDIT)
            {
                echo '<input type="text" value="'.PostalStat::getNameById($model->status_id).'" readonly/>';
                echo CHtml::activeHiddenField($model, '['.$i.']status_id');
            }
            else
                echo CHtml::activeDropDownList($model, '['.$i.']status_id', CHtml::listData(PostalStat::model()->findAll(),'id','name'), array('title' => $model->getError('status_id')));
            ?>
        </td><td>
            <?php echo CHtml::activeTextField($model,'['.$i.']status_date', array('style' => 'width: 150px;', 'title' => $model->getError('status_data'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
        </td>
    <?php endif;?>
</tr>


