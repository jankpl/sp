<?php

class DhlDeRetoureClient extends GlobalOperatorWithReturn
{
    const URL = GLOBAL_CONFIG::DHL_DE_RETOURE_URL;
    const LOCATION = GLOBAL_CONFIG::DHL_DE_RETOURE_LOCATION;
    const LOGIN = GLOBAL_CONFIG::DHL_DE_RETOURE_LOGIN;
    const PASSWORD = GLOBAL_CONFIG::DHL_DE_RETOURE_PASSWORD;
    const PORTAL_ID = GLOBAL_CONFIG::DHL_DE_RETOURE_PORTAL_ID;

    public $senderName1 = '';
    public $senderName2 = '';
    public $ref1 = '';
    public $ref2 = '';
    public $senderStreet = '';
    public $senderNo = '';
    public $senderPostalCode = '';
    public $senderCity = '';
    public $senderTel = '';
    public $senderCountryCode = '';
    public $weight = '';
    public $value = '';
    public $content = '';

    public $senderNamePrefix = '';

    public $user_id = '';

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

//
//    public static function test()
//    {
//        $client = new self;
//
//
//        $data = new \SoapVar('<ns1:BookLabelRequest portalId="kaab" deliveryName="Web_Fl_CB04" shipmentReference="483181101848944" customerReference="2216" labelFormat="PDF" senderName1="IELM/HELJAE PIRKKAMAA" senderName2="" senderCareOfName="" senderContactPhone="+358452595375" senderStreet="KURJENPOLKU" senderStreetNumber="15" senderBoxNumber="" senderPostalCode="02880" senderCity="VEIKKOLA" itemWeight="0.5" itemWorth="1.17" custom5="1" dutyContent1="577069535" dutyAmount1="1" dutyWeight1="0.5" dutyWorth1="1.17" dutyCurrency1="EUR"/>', XSD_ANYXML);
//
//
//        $resp = $client->_soapOperation('bookLabel', $data);
//
//        return $resp;
//    }

    protected function _soapOperation($method, $data)
    {
        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context' => stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))),
        );


        $wsse_header = new WsseAuthHeaderDhlDeReturn(self::LOGIN, self::PASSWORD);


        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        try {
            $client = new SoapClient(self::URL, $mode);
            $client->__setSoapHeaders([$wsse_header]);
            $client->__setLocation(self::LOCATION);
            $resp = $client->$method($data);


        } catch (SoapFault $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('dhlde_return.txt', print_r($client->__getLastRequest(), 1));
            MyDump::dump('dhlde_return.txt', print_r($client->__getLastResponse(), 1));

            return $return;

        } catch (Exception $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('dhlde_return.txt', print_r($client->__getLastRequest(), 1));
            MyDump::dump('dhlde_return.txt', print_r($client->__getLastResponse(), 1));
            return $return;
        }

        MyDump::dump('dhlde_return.txt', print_r(preg_replace('%<var3bl:label>(.*)</var3bl:label>%i', '<var3bl:label>(...TRUNCATED LABEL BASE64...)</var3bl:label>',  $client->__getLastRequest()), 1));
        MyDump::dump('dhlde_return.txt', print_r($client->__getLastResponse(), 1));


        if(isset($resp->Fault)) {
            $return->error = $resp->faultcode.':'.$resp->faultstring;
            $return->errorCode = '';
            $return->errorDesc = $resp->faultcode.':'.$resp->faultstring;
        }
        else
        {
            $return->success = true;
            $return->data = $resp;
        }

        return $return;
    }

    protected static function _ownExtractNumber($text, $defaultValue = '0')
    {
        $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-]+)((\s)+|$))*/i';
        $building = '';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[0][0];

        $building = trim($building);

        if(mb_strlen($building) > 8)
        {
            // max length is 8
            // if it's longer, use older, more choosy rexexp
            $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';
            if(preg_match_all($REG_PATTERN, $text, $result2)) $building = $result2[2][0];

            $building = trim($building);
        }

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }

    public static function getCountryListDeliveryNames()
    {
        $data = [
            'DE'=> 'RetourenWeb02',
            'CH'=> 'Web_CH_CB04',
            'ES'=> 'Web_ES_CB04',
            'AT'=> 'Web_AT_CB04',
            'BE'=> 'Web_BE_CB04',
            'BG'=> 'Web_BG_CB04',
            'CY'=> 'Web_CY_CB04',
            'CZ'=> 'Web_CZ_CB04',
            'DK'=> 'Web_DK_CB04',
            'EE'=> 'Web_EE_CB04',
            'FI'=> 'Web_Fl_CB04',
            'FR'=> 'Web_FR_CB04',
            'GB'=> 'Web_GB_CB04',
            'GR'=> 'Web_GR_DOM05',
            'HR'=> 'Web_HR_CB04',
            'HU'=> 'Web_HU_CB04',
            'IE'=> 'Web_IE_CB04',
            'IT'=> 'Web_IT_CB04',
            'LU'=> 'Web_LU_CB04',
            'LV'=> 'Web_LV_CB04',
            'NL'=> 'Web_NL_CB04',
            'PL'=> 'Web_PL_CB04',
            'PT'=> 'Web_PT_DOM05',
            'RO'=> 'Web_RO_CB04',
            'SE'=> 'Web_SE_DOM05',
            'SI'=> 'Web_Sl_CB04',
            'SK'=> 'Web_SK_CB04',
            'LT'=> 'Web_LT_CB04',
            'MT'=> 'Web_MT_CB04',
        ];

        return $data;
    }

    public static function clearString($text, $cc)
    {
        $text = S_Useful::removeNationalCharacters($text);
        $text = htmlspecialchars($text);
        return $text;
    }

    protected function createShipment($returnError)
    {
        $client = new self;

        $cc = strtoupper($this->senderCountryCode);

        $deliveryName = (isset(self::getCountryListDeliveryNames()[$cc])) ? self::getCountryListDeliveryNames()[$cc] : NULL;

        $streetNo = trim(self::_ownExtractNumber($this->senderStreet));
        $streetName = trim(str_replace($streetNo, '', $this->senderStreet));

        $this->value = Yii::app()->NBPCurrency->PLN2EUR($this->value);

        $senderName1 = $this->senderName1;


        $overwriteLoginsIdsList = [1732,8]; // @ 26.03.2019 - for McPlast do not send login to DHL DE, but overprint in on label anywey
        if(!in_array($this->user_id, $overwriteLoginsIdsList))
        {
            if($this->senderNamePrefix)
                $senderName1 = $this->senderNamePrefix.'/'.$senderName1;
        }


        $senderName1 = mb_substr($senderName1, 0, 40);

        $zipCode = $this->senderPostalCode;

        if($cc == 'HR')
            $zipCode = 'HR'.$zipCode;

        $data = new \SoapVar('<ns1:BookLabelRequest portalId="'.self::PORTAL_ID.'" deliveryName="'.$deliveryName.'" shipmentReference="'.self::clearString($this->ref1, $cc).'" customerReference="'.self::clearString($this->ref2, $cc).'" labelFormat="PDF" senderName1="'.self::clearString($senderName1, $cc).'" senderName2="'.self::clearString($this->senderName2, $cc).'" senderCareOfName="" senderContactPhone="'.self::clearString($this->senderTel, $cc).'" senderStreet="'.self::clearString($streetName, $cc).'" senderStreetNumber="'.self::clearString($streetNo, $cc).'" senderBoxNumber="" senderPostalCode="'.self::clearString($zipCode, $cc).'" senderCity="'.self::clearString($this->senderCity, $cc).'" itemWeight="'.self::clearString($this->weight, $cc).'" itemWorth="'.self::clearString($this->value, $cc).'" custom5="1" dutyContent1="'.self::clearString($this->content, $cc).'" dutyAmount1="1" dutyWeight1="'.self::clearString($this->weight, $cc).'" dutyWorth1="'.self::clearString($this->value, $cc).'" dutyCurrency1="EUR"/>', XSD_ANYXML);

        $resp = $client->_soapOperation('bookLabel', $data);

        if ($resp->success == true) {
            $return = [];

            $label =  base64_decode($resp->data->label);

            $uniqId = uniqid();
            $basePath = Yii::app()->basePath . '/../';
            $dir = 'uplabels/';
            $temp = $dir . 'temp/';
            @file_put_contents($basePath . $temp . $uniqId . '.pdf', $label);

            $pdfPages = 1;

            if($cc == 'PL')
                $pdfPages = 2;

            $labels = [];


            if($cc == 'DK')
            {
                $im = ImagickMine::newInstance();
                $im->setResolution(300, 300);
                $im->readImage($basePath . $temp . $uniqId . '.pdf[0]');
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }
                $im->stripImage();
                $im->trimImage(0);
                $labels[] = $im->getImageBlob();


                $im = ImagickMine::newInstance();
                $im->setResolution(300, 300);
                $im->readImage($basePath . $temp . $uniqId . '.pdf[1]');
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }

                $imgWidth = $im->getImageWidth();
                $imgHeight = $im->getImageHeight();
                $im->cropImage($imgWidth * 0.8, $imgHeight * 0.6, $imgWidth * 0.1, $imgHeight * 0.3);

                $im->trimImage(0);
                $im->rotateImage(new ImagickPixel(), 90);

                $im->stripImage();

                $labels[] = $im->getImageBlob();

            } else {

                for ($i = 0; $i < $pdfPages; $i++) {

                    $im = ImagickMine::newInstance();
                    $im->setResolution(300, 300);
                    $im->readImage($basePath . $temp . $uniqId . '.pdf[' . $i . ']');
                    $im->setImageFormat('png8');
                    if ($im->getImageAlphaChannel()) {
                        $im->setImageBackgroundColor('white');
                        $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                    }
                    $im->stripImage();

                    $imgWidth = $im->getImageWidth();
                    $imgHeight = $im->getImageHeight();

                    $im2 = clone $im;
                    $im2->cropImage($imgWidth, $imgHeight / 2, 0, 0);
                    $im->cropImage($imgWidth, $imgHeight / 2, 0, $imgHeight / 2);

                    $im->rotateImage(new ImagickPixel(), 90);
                    $im->trimImage(0);

                    $im2->rotateImage(new ImagickPixel(), 90);
                    $im2->trimImage(0);

                    if($cc == 'DE' && in_array($this->user_id, $overwriteLoginsIdsList))
                    {
                        $draw = new ImagickDraw();
                        $draw->setTextUnderColor(new ImagickPixel('white'));
                        $draw->setFontSize(40);
                        $im2->annotateImage($draw, 2155, 367, 0, str_pad($this->senderNamePrefix.'/'.$senderName1,45,' '));
                    }

                    if ($cc == 'ES') {
                        $im3 = clone $im2;

                        $imgWidth = $im3->getImageWidth();
                        $imgHeight = $im3->getImageHeight();

                        $im3->cropImage($imgWidth + 80, $imgHeight / 2 + 60, $imgWidth, 10);
                        $im2->cropImage($imgWidth + 80, $imgHeight / 2 + 0, $imgWidth - 40, $imgHeight / 2 + 80);

                        $im2->rotateImage(new ImagickPixel(), 270);
                        $im2->trimImage(0);

                        $im3->rotateImage(new ImagickPixel(), 270);
                        $im3->trimImage(0);

                        $labels[] = $im3->getImageBlob();


                    } elseif ($cc == 'IT') {

                        $im3 = clone $im2;

                        $imgWidth = $im3->getImageWidth();
                        $imgHeight = $im3->getImageHeight();

                        $im3->cropImage($imgWidth + 80, $imgHeight / 2 + 60, $imgWidth, 10);
                        $im2->cropImage($imgWidth + 80, $imgHeight / 2 + 0, $imgWidth - 40, $imgHeight / 2 + 80);

                        $im2->rotateImage(new ImagickPixel(), 270);
                        $im2->trimImage(0);

                        $im3->rotateImage(new ImagickPixel(), 270);
                        $im3->trimImage(0);

                        $labels[] = $im3->getImageBlob();
                    }


                    $labels[] = $im2->getImageBlob();

                    // for Sweden, second label is useless
                    if ($cc != 'SE') {

                        if (($cc == 'PL' && $i > 0) OR $cc != 'PL')
                            $labels[] = $im->getImageBlob();
                    }
                }
            }
            @unlink($basePath . $temp . $uniqId . '.pdf');

            $return[] = GlobalOperatorResponse::createSuccessResponse($labels, $resp->data->idc);

            return $return;
        }
        else {

            if ($returnError == true) { return GlobalOperatorResponse::createErrorResponse($resp->errorDesc != '' ? $resp->errorDesc : $resp->error);

            } else { return false;
            }
        }

    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;

        $model->user_id = $courierInternal->courier->user_id;

        // SENDER IS ALWAYS ORYGINAL SENDER
        $from = $courierInternal->courier->senderAddressData;

        $model->senderName1 = $from->getUsefulName(true);
        $model->senderPostalCode = $from->zip_code;
        $model->senderCity = $from->city;
        $model->senderStreet = trim($from->address_line_1 . ' ' . $from->address_line_2);
        $model->senderTel = $from->tel;
        $model->senderCountryCode = $from->country0->code;

        $model->weight = $courierInternal->courier->getWeight(true);
        $model->content = $courierInternal->courier->package_content;
        $model->ref1 = $courierInternal->courier->local_id;
        $model->ref2 = $courierInternal->courier->user_id;
        $model->value = $courierInternal->courier->getPackageValueConverted('EUR');

        if(in_array($courierInternal->courier->user_id,[947, 948, 2036, 2037]))
        {
            switch(strtoupper($model->senderCountryCode))
            {
                case 'ES':
                    $model->senderNamePrefix = 'ZAPATOS.ES';
                    break;
                case 'IT':
                    $model->senderNamePrefix = 'ESCARPE.IT';
                    break;
                case 'SE':
                    $model->senderNamePrefix = 'ESKOR.SE';
                    break;
                default:
                    $model->senderNamePrefix = $courierInternal->courier->user->login;
                    break;
            }
        } else {
            $model->senderNamePrefix = $courierInternal->courier->user->login;
        }

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $from = $courierU->courier->senderAddressData;

        $model = new self;

        $model->user_id = $courierU->courier->user_id;

        $model->senderName1 = $from->getUsefulName(true);
        $model->senderPostalCode = $from->zip_code;
        $model->senderCity = $from->city;
        $model->senderStreet = trim($from->address_line_1 . ' ' . $from->address_line_2);
        $model->senderTel = $from->tel;
        $model->senderCountryCode = $from->country0->code;

        $model->weight = $courierU->courier->getWeight(true);
        $model->content = $courierU->courier->package_content;
        $model->ref1 = $courierU->courier->local_id;
        $model->ref2 = $courierU->courier->user_id;
        $model->value = $courierU->courier->getPackageValueConverted('EUR');

        if(in_array($courierU->courier->user_id,[947, 948, 2036, 2037]))
        {
            switch(strtoupper($model->senderCountryCode))
            {
                case 'ES':
                    $model->senderNamePrefix = 'ZAPATOS.ES';
                    break;
                case 'IT':
                    $model->senderNamePrefix = 'ESCARPE.IT';
                    break;
                case 'SE':
                    $model->senderNamePrefix = 'ESKOR.SE';
                    break;
                default:
                    $model->senderNamePrefix = $courierU->courier->user->login;
                    break;
            }
        } else {
            $model->senderNamePrefix = $courierU->courier->user->login;
        }

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierReturn(CourierTypeReturn $courierReturn, CourierLabelNew $cln, $returnErrors = false)
    {

        $from = $courierReturn->courier->senderAddressData;

        $model = new self;

        $model->user_id = $courierReturn->courier->user_id;

        $model->senderName1 = $from->getUsefulName(true);
        $model->senderPostalCode = $from->zip_code;
        $model->senderCity = $from->city;
        $model->senderStreet = trim($from->address_line_1 . ' ' . $from->address_line_2);
        $model->senderTel = $from->tel;
        $model->senderCountryCode = $from->country0->code;

        $model->weight = $courierReturn->courier->getWeight(true);
        $model->content = $courierReturn->courier->package_content;
        $model->ref1 = $courierReturn->courier->local_id;
        $model->ref2 = $courierReturn->courier->user_id;
        $model->value = $courierReturn->courier->getPackageValueConverted('EUR');

        if(in_array($courierReturn->courier->user_id,[2036, 2037]))
        {
            switch(strtoupper($model->senderCountryCode))
            {
                case 'ES':
                    $model->senderNamePrefix = 'ZAPATOS.ES';
                    break;
                case 'IT':
                    $model->senderNamePrefix = 'ESCARPE.IT';
                    break;
                case 'SE':
                    $model->senderNamePrefix = 'ESKOR.SE';
                    break;
                case 'DE':
                    $model->senderNamePrefix = 'ESCHUHE.DE';
                    break;
                default:
                    $model->senderNamePrefix = $courierReturn->courier->user->login;
                    break;
            }
        } else {
            $model->senderNamePrefix = $courierReturn->courier->user->login;
        }

        return $model->createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        $rawData = DhlDeClient::trackSingle($no);

        $statuses = new GlobalOperatorTtResponseCollection();
        if(is_array($rawData))
            foreach($rawData AS $item)
            {
                $statuses->addItem($item['name'], $item['date'], $item['location']);
            }

        return $statuses;
    }
}

class WsseAuthHeaderDhlDeReturn extends SoapHeader {

    private $wss_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    function __construct($user, $pass, $ns = null) {
        if ($ns) {
            $this->wss_ns = $ns;
        }

        $auth = new stdClass();
        $auth->Username = new SoapVar($user, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
        $auth->Password = new SoapVar($pass, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);

        $username_token = new stdClass();
        $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns);

        $security_sv = new SoapVar(
            new SoapVar($username_token, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns),
            SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'Security', $this->wss_ns);
        parent::__construct($this->wss_ns, 'Security', $security_sv, true);
    }
}
