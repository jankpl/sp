<div class="text-right navbar-form-container">
    <?php
    /* @var $form TbActiveForm */

    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'login-form-home',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>false,
        ),
        'action'=>$this->createAbsoluteUrl('/site/login', array(),'https'),
        'htmlOptions' => array('class' => 'navbar-form', 'style' => 'margin-bottom: 0; margin-top: 6px;'),
    ));
    $model = new LoginForm();
    ?>

    <?php echo $form->textField($model,'username',array('style' => 'width: 100px;', 'placeholder' => Yii::t('site','login...'))); ?>
    <?php echo $form->passwordField($model,'password',array('style' => 'width: 100px;',  'placeholder' => Yii::t('site','hasło...'))); ?>
    <?php echo TbHtml::submitButton('',array('id'=>'header-login-button', 'title' => Yii::t('site','Zaloguj się!'))); ?>
    <?php $this->endWidget(); ?>
</div>
