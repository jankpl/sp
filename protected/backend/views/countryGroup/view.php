<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
	array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
	array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'name',
'date_entered',
'date_updated',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('countryLists')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->countryLists as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('countryList/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>