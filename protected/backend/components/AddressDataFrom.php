<?php

class AddressDataFrom extends CWidget
{
    /**
     * @var CFormModel
     */
    public $listAddressData;
    public $addressData;
    public $form;
    public $noEmail;
    public $groupName;

    public function run()
    {

        if($this->addressData == NULL)
            throw new Exception('AddressData model is NULL!');

        $this->render('addressDataForm/_form_base',
            array(
                'model' => $this->addressData,
                'listAddressData' => $this->listAddressData,
                'form' => $this->form,
                'noEmail' => $this->noEmail,
                'i' => $this->groupName,
            ));
    }

}