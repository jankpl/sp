<?php
/* @var $import CourierImportManager */
/* @var $couriers Courier[] */
/* @var $mainInstance boolean */
?>

<?php
if(!$mainInstance):
    ?>
    <div class="alert alert-warning text-center"><?= Yii::t('courier', 'Ten import jest prawdopodobnie otwarty w innym oknie. W tej instancji zostało zablokwane automatyczny podgląd postępu. Wróć do głównego okna lub ręcznie odświeżaj stronę, aby zobaczyć postęp.');?></div>
<?php
endif;
?>

<?php if($import->source == CourierImportManager::SOURCE_EBAY): ?>
    <?php
    $this->renderPartial('importEbay/_header',[
        'ebayClient' => false,
        'step' => 4,
    ]);
    ?>
<?php endif; ?>


    <div class="text-center">
        <?php if($import->source == CourierImportManager::SOURCE_EBAY): ?>
            <a href="<?php echo Yii::app()->createUrl('/courier/courierInternal/ebay');?>" class="btn btn-warning"><?php echo Yii::t('courier', 'Od nowa');?></a>
        <?php elseif($import->source == CourierImportManager::SOURCE_LINKER): ?>
            <a href="<?php echo Yii::app()->createUrl('/courier/courierInternal/linker');?>" class="btn btn-warning"><?php echo Yii::t('courier', 'Od nowa');?></a>
        <?php else: ?>
            <a href="<?php echo Yii::app()->createUrl('/courier/courierInternal/import');?>" class="btn btn-warning"><?php echo Yii::t('courier', 'Od nowa');?></a>
        <?php endif; ?>
        <br/>
    </div>


    <hr/>

<?php
$this->widget('FlashPrinter');
?>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-labels',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courierInternal/getLabels', ['hash' => $hash]),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL ? '_blank' : '_self'
    ]
,
));
?>
    <div class="alert alert-info"><?= Yii::t('courier', 'Zamknięcie tej strony nie spowoduje anulowania zamawiania paczek.');?></div>

    <table class="table table-striped table-condensed" style="width: 60%; margin: 0 auto;">
        <tr>
            <td>#</td>
            <td><?php echo Yii::t('courier', 'Local Id');?></td>
            <td style="text-align: center;"><?php echo Yii::t('label', 'Label ready');?></td>
            <?php if($import->source == CourierImportManager::SOURCE_EBAY):?>
                <td style="text-align: center;"><?php echo Yii::t('label', 'eBay ready');?></td>
            <?php elseif($import->source == CourierImportManager::SOURCE_LINKER):?>
                <td style="text-align: center;"><?php echo Yii::t('label', 'Linker ready');?></td>
            <?php endif; ?>
            <td style="text-align: center;"><?php echo Yii::t('label', 'Get label');?> <a href="#" data-toggle-labels="true"><i class="glyphicon glyphicon-check"></i></a></td>
        </tr>
        <?php
        $i = 1;
        if(is_array($couriers))
            foreach($couriers AS $item):
                $dataAction = 'false';

                if($import->source == CourierImportManager::SOURCE_EBAY OR $import->source == CourierImportManager::SOURCE_LINKER)
                {
                    if($item->courierTypeInternal->getPrimaryLabelStatus() == CourierLabelNew::STAT_NEW OR $item->courierTypeInternal->getPrimaryLabelStatus() == CourierLabelNew::STAT_WAITING_FOR_PARENT)
                        $dataAction = 'true';
                    else if($item->courierEbayImport->stat == EbayImport::STAT_NEW)
                        $dataAction = 5; // just ebay
                } else {
                    if($item->courierTypeInternal->getPrimaryLabelStatus() == CourierLabelNew::STAT_NEW OR $item->courierTypeInternal->getPrimaryLabelStatus() == CourierLabelNew::STAT_WAITING_FOR_PARENT)
                        $dataAction = 'true';
                }

                ?>
                <tr data-courier-local-id="<?= $item->local_id;?>" data-label-stat="<?= $item->courierTypeInternal->getPrimaryLabelStatus();?>" data-courier-temp-id="<?= $item->id;?>" data-action="<?= $dataAction;?>">
                    <td><?= $i;?></td>
                    <td><a href="<?php echo Yii::app()->createUrl('/courier/courier/view', ['urlData' => $item->hash]);?>" target="_blank"><?= $item->local_id;?></a></td>
                    <td class="label-stat" style="text-align: center;"><?= $item->courierTypeInternal->getPrimaryLabelStatus() == CourierLabelNew::STAT_SUCCESS ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'?></td>
                    <?php if($import->source == CourierImportManager::SOURCE_EBAY OR $import->source == CourierImportManager::SOURCE_LINKER):?>
                        <td class="ebay-stat" style="text-align: center;">
                            <?php
                            if(($item->courierTypeInternal->getPrimaryLabelStatus() == CourierLabelNew::STAT_NEW OR $item->courierTypeInternal->getPrimaryLabelStatus() == CourierLabelNew::STAT_WAITING_FOR_PARENT)):
                                ?>
                                <i class="glyphicon glyphicon-remove"></i>
                            <?php
                            else:
                                ?>
                                <?php
                                if($item->courierEbayImport->stat == EbayImport::STAT_SUCCESS) echo '<i class="glyphicon glyphicon-ok"></i>';
                                elseif ($item->courierEbayImport->stat == EbayImport::STAT_NEW) echo '<img src="'.Yii::app()->baseUrl.'/images/layout/preloader.gif" style="width: 10px; height: 10px;"/>';
                                else echo '<i class="glyphicon glyphicon-remove"></i>';?>
                            <?php
                            endif;
                            ?>
                        </td>
                    <?php endif; ?>
                    <td class="label-checkbox" style="text-align: center;"><?= in_array($item->courierTypeInternal->getPrimaryLabelStatus(), [CourierLabelNew::STAT_WAITING_FOR_PARENT, CourierLabelNew::STAT_NEW]) ? '<img src="'.Yii::app()->baseUrl.'/images/layout/preloader.gif" style="width: 10px; height: 10px;"/>' : ($item->courierTypeInternal->getPrimaryLabelStatus() == CourierLabelNew::STAT_FAILED ? '<i class="glyphicon glyphicon-remove" title="'.Yii::t('courier','Wystąpił błąd przy zamawianiu etykiety!').'"></i>' : '<input type="checkbox" data-get-label="true" data-label-checkbox="true" name="label['.$item->id.']"/>');?></td>
                </tr>

                <?php
                $i++;
            endforeach; ?>
    </table>
    <hr/>
    <div class="text-center" id="generate-labels-action">
        <?php echo CHtml::dropDownList('Courier[type]','',LabelPrinter::getLabelGenerateTypes(), array('style' => 'width: 250px; display: inline-block;'));?>

        <?php
        echo TbHtml::submitButton('Generate labels', array('data-generate-labels' => 'true'));?>
    </div>
<?php
if($mainInstance):
    ?>
    <div class="text-center" id="work-in-progress">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/layout/preloader.gif" />
        <h3>0/<?php echo S_Useful::sizeof($couriers);?></h3>
    </div>
<?php
endif;
?>
<?php
$this->endWidget();
?>

    <script>
        $(document).ready(function() {
            var propToggleTrue = true;
            $('[data-toggle-labels]').on('click', function () {

                $('[data-label-checkbox]').each(function (i, v) {
                    $(v).prop("checked", propToggleTrue);
                });

                propToggleTrue = !propToggleTrue;
            })
        });
    </script>


<?php
if($mainInstance):
    ?>
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/internalImport.js', CClientScript::POS_END);
    Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
    Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
    ?>
    <script>
        $(document).ready(function(){
            runOrdering({
                url: '<?= Yii::app()->createUrl('/courier/courierInternal/quequeLabelByLocalId');?>',
                urlEbay: '<?= Yii::app()->createUrl('/courier/courierInternal/ajaxCheckJustEbay');?>',
                statSuccess: '<?= CourierLabelNew::STAT_SUCCESS;?>',
                statEBaySuccess: '<?= EbayImport::STAT_SUCCESS;?>',
                statEBayUnknown: '<?= EbayImport::STAT_UNKNOWN;?>',
                sourceEBay: '<?= $import->source == CourierImportManager::SOURCE_EBAY OR $import->source == CourierImportManager::SOURCE_LINKER ? true : false; ?>',
                errorUnknown: '<?= Yii::t('courier', 'Wystąpił nieznany błąd. Spróbuj odświeżyć stronę.'); ?>',
                messageDone: '<?= Yii::t('courier', 'Przetworzono kolejną pozycję.'); ?>',
            });
        });
    </script>
<?php
endif;