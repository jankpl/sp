<?php

?>

<h1><?php echo GxHtml::encode(UserGroup::label(2)); ?></h1>


<table class="list hTop" style="width: 500px; margin: 0 auto;">
    <tr>
        <td>#</td>
        <td>Name</td>
        <td>Admin</td>
        <td>Members</td>
        <td>Action</td>
    </tr>

<?php /* @var $item UserGroup */ ?>
<?php foreach($models AS $item):?>

    <tr>
        <td><?php echo $item->id; ?></td>
        <td><?php echo $item->name; ?></td>
        <td><?php echo $item->admin_id ? $item->admin->login : '-'; ?></td>
        <td><?php echo $item->howManyMembers(); ?></td>
        <td><?php echo CHtml::link('details',Yii::app()->createUrl('userGroup/view', array('id' => $item->id)), array('class' => 'likeButton'));?> <?php echo CHtml::link('edit',Yii::app()->createUrl('userGroup/update', array('id' => $item->id)), array('class' => 'likeButton'));?></td>
    </tr>

<?php endforeach; ?>


</table>
<br/>
<div style="text-align: center;">
<?php echo CHtml::link('Add group',Yii::app()->createUrl('userGroup/create'), array('class' => 'likeButton'));?>
</div>