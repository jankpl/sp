<?php

if($item['M'] && (S_Useful::sizeof($item['PV']) > 1 OR $item['PV'][key(($item['PV']))]->weight_top_limit > 0)):

    echo $form->error($item['M'], '_maxWeightErrorContainer');
    ?>
    <table class="table table-striped table-bordered" style="background: white;">
        <tr>
            <th rowspan="2">Weight to</th>
            <th colspan="2" style="text-align: center;">Price</th>
        </tr>
        <tr>
            <th style="text-align: center;">per item</th>
            <th style="text-align: center;">per weight</th>
        </tr>
        <?php

        foreach($item['PV'] AS $key => $priceItem):

            if($priceItem->hasErrors() OR !$priceItem->pricePerItem OR $priceItem->pricePerItem->hasErrors() OR !$priceItem->pricePerWeight OR $priceItem->pricePerWeight->hasErrors())
                $error = true;

            ?>
            <tr>
                <td><?php echo $form->textField($priceItem, '['.$key.']weight_top_limit', ['style' => 'width: 40px;', 'disabled' => true]); ?><?php echo $form->error($priceItem,'['.$i.']weight_top_limit'); ?></td>
                <td>
                    <?php $this->widget('PriceForm', array(
                        'form' => $form,
                        'model' => $priceItem->pricePerItem,
                        'formFieldPrefix' => '['.$key.']',
                        'vertical' => false,
                        'readonly' => true
                    ));?>
                    <?= $priceItem->pricePerItem ? $form->error($priceItem->pricePerItem, 'price_id') : ''; ?>
                </td>
                <td>
                    <?php $this->widget('PriceForm', array(
                        'form' => $form,
                        'model' => $priceItem->pricePerWeight,
                        'formFieldPrefix' => '['.$key.']',
                        'vertical' => false,
                        'readonly' => true
                    ));?>
                    <?= $priceItem->pricePerWeight ? $form->error($priceItem->pricePerWeight, 'price_id') : ''; ?>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
    </table>

<?php
else:
    ?>
    - NONE -
<?php
endif;
?>