/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    var lastHeight;
    var $sideMenu = $('#side-menu');

    $sideMenu.metisMenu();
    $sideMenu.on('click', function(){

        fixHeight(true, true);

    });

    $(window).load(function(){
        fixHeight(false, false);
    });

    function fixHeight(animate, delay)
    {
        if(delay)
            delay = 300;
        else
            delay = 0;

        var height = $('#side-menu').height() + 120;

        if(height != lastHeight)
        {
            lastHeight = height;

            setTimeout(function(){
                if(animate)
                    $('#main').animate({ 'min-height': height }, 'fast');
                else
                    $('#main').css('min-height', height);
            }, delay);
        }

    }


    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }


    });
    var element = $('ul.nav a.active').parent();

    var x = 0;
    while (true) {

        if(x > 10)
            break;

        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }

        x++;
    }

    element = $('ul.nav a.active').parent();
    x = 0;
    while (true) {

        if(x > 10)
            break;

        if(element.attr('id') == 'side-menu')
            break;

        if(!element.is('li'))
            element = element.parent();
        else
            element = element.addClass('active-parent').parent();

        x++;
    }


    $('#close-panel-menu').on('click', function(){
        $('#panel-content').toggleClass('closed-sidebar');

        var cookieValue = 0;
        if ($('#panel-content').hasClass('closed-sidebar'))
            cookieValue = 1;

        var date = new Date();
        date = new Date(date.getFullYear(), date.getMonth()+1, 1);

        document.cookie="panelClosedSidebar=" + cookieValue + "; expires=" + date + " UTC; path=/";

        fixHeight(true, false);
    });

    $('#panel-sidebar-wrapper').on('mouseenter', function() {

        if ($('#panel-content').hasClass('closed-sidebar')) {
            $('#panel-sidebar-wrapper').addClass('expanded');
            fixHeight(true, true);
        }
    }).on('mouseleave', function() {

        if ($('#panel-content').hasClass('closed-sidebar')) {
            $('#panel-sidebar-wrapper').removeClass('expanded');
            fixHeight(true, true);
        }
    });

});
