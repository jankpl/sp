<?php

class SmsApi extends CApplicationComponent{

    const SENDING_ENABLED = true;

    protected $smsApi;

    protected $login = 'KAAB';
    protected $pass = '87e1992ba0e68ff736385f52dfe5e507';


    public function __construct()
    {
        $this->init();
    }

    public function init()
    {

        require_once (Yii::getPathOfAlias('application.vendor.smsapi.smsapi') .'/Autoload.php');

        $client = new \SMSApi\Client($this->login);
        $client->setPasswordHash( $this->pass );

        $smsapi = new \SMSApi\Api\SmsFactory();
        $smsapi->setClient($client);

        $this->smsApi = $smsapi;


        return parent::init();
    }

    public function sendSmsWithArchivePostal($nr, $text, $postal_id, $pv = false)
    {
        if($this->sendSms($nr, $text, $pv)) {
            SmsArchive::addToArchive($postal_id, $nr, $text, SmsArchive::TYPE_POSTAL);
            return true;
        }
        return false;

    }

    public function sendSmsWithArchiveCourier($nr, $text, $courier_id, $pv = false)
    {
        if($this->sendSms($nr, $text, $pv)) {
            SmsArchive::addToArchive($courier_id, $nr, $text);
            return true;
        }
        return false;

    }
    /**
     * Just add to queque
     * @param $nr
     * @param $text
     */
    public function sendSms($nr, $text, $pv = false, $ignoreBlock = false)
    {
        // it's rather not a real number if it's shorter than 6 digits
        if(mb_strlen($nr) < 6)
            return false;


        if(!$ignoreBlock) {
            // PREVENT SENDING SMS TO USER'S DEFAULT TEL NUMBER
            if (!Yii::app()->params['console'] && !Yii::app()->user->isGuest)
            {
                // check if number is saved as somebody's default number - if so, do not send sms
                if(User::searchForDefaultTelNo($nr)) {
                    if(!self::SENDING_ENABLED)
                        MyDump::dump('smsy.txt', 'IGNOROWANY : ' . $nr . ' || ' . $text);

                    return false;
                }
            }
            {
                $cmd = Yii::app()->db->createCommand();
                $cmd->select('COUNT(*)');
                $cmd->from('user u');
                $cmd->join('user_options o', 'u.id = o.user_id');
                $cmd->where('o.name = "blockSms"');
                $cmd->andWhere('o.value = 1');
                $cmd->andWhere('u.tel = :nr', array(':nr' => $nr));
                $result = $cmd->queryScalar();

                // user has selected blocking SMS notifications
                if ($result)
                    return false;
            }
        }

        $nr = preg_replace("/[^0-9]/","", $nr);
        $text = S_Useful::removeNationalCharacters($text);

        if($nr == '' OR $text == '')
            return false;

        if(!self::SENDING_ENABLED) {
            $this->realSendSms($nr, $text, $pv);
            return true;
        }

        Yii::app()->Queque->scheudleItem(Queque::TYPE_SMS, array('nr' => $nr, 'text' => $text, 'pv' => $pv));
        return true;
    }

    /**
     * Really send SMS from queque data
     */
    public function realSendSms($nr, $text, $pv = false)
    {
        MyDump::dump('smsy.txt', $nr . ' || ' . $text.' || '.$pv);

        if(!self::SENDING_ENABLED) {
            return true;
        } else {
            try {

                $actionSend = $this->smsApi->actionSend();

                $actionSend->setTo($nr);
                $actionSend->setText($text);

                $sender = 'KAAB';
                if($pv == PageVersion::PAGEVERSION_CQL)
                    $sender = 'CQL.GLOBAL';

                $actionSend->setSender($sender); //Pole nadawcy lub typ wiadomość 'ECO', '2Way'

                $response = $actionSend->execute();

                return true;
//            foreach( $response->getList() as $status ) {
//                echo  $status->getNumber() . ' ' . $status->getPoints() . ' ' . $status->getStatus();
//            }
            } catch (\SMSApi\Exception\SmsapiException $e) {
                //echo 'ERROR: ' . $e->getMessage();

                return false;
            }
        }

    }



}