<?php

/* @var $model HybridMail */

/*$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);*/
?>

<h1><?php echo Yii::t('hybridMail','HybridMail #{id}', array('{id}' => $model->local_id));?></h1>

<h3><?php echo Yii::t('hybridMail','Szczegóły'); ?></h3>
<table class="list hLeft" style="width: 500px;">
    <tr>
        <td><?php echo Yii::t('hybridMail','Data utworzenia'); ?></td>
        <td><?php echo substr($model->date_entered,0,16);?></td>
    </tr>
    <tr>
        <td><?php echo Yii::t('hybridMail','Status'); ?></td>
        <td><?php echo $model->stat0->name;?></td>
    </tr>
    <tr>
        <td><?php echo Yii::t('hybridMail','Zamówienie'); ?></td>
        <td><?php echo CHtml::link('Zamówienie #'.$model->order->local_id, array('/order/view', 'urlData' => $model->order->hash));?></td>
    </tr>
    <tr>
        <td><?php echo Yii::t('hybridMail','Łącznie stron'); ?></td>
        <td><?php echo $model->getTotalPages(); ?></td>
    </tr>
</table>

<h4><?php echo Yii::t('hybridMail','Zawartość');?>  </h4>


<a href="<?php echo Yii::app()->createUrl('/hybridMail/hybridMail/getFile', array('urlData' => $model->hash));?>" target="_blank" class="btn btn-primary"><?php echo Yii::t('hm', 'Podejrzyj plik');?></a>

    <h3><?php echo Yii::t('hybridMail','Dodatki');?></h3>
    <table class="list hTop" style="width: 500px;">
        <tr>
            <td><?php echo Yii::t('hybridMail','Nazwa');?></td>
            <td><?php echo Yii::t('hybridMail','Opis');?></td>
        </tr>
        <?php

        /* @var $item HybridMailAdditionList */
        if(!S_Useful::sizeof($model->listAdditions()))
            echo '<tr><td colspan="2">'.Yii::t('hybridMail','brak').'</td></tr>';
        else
            foreach($model->listAdditions() AS $item): ?>
                <tr>
                    <td><?php echo $item->getClientTitle(); ?></td>
                    <td><?php echo $item->getClientDesc(); ?></td>
                </tr>

            <?php endforeach; ?>
    </table>

<h3><?php echo Yii::t('hybridMail','Nadawca'); ?></h3>

<?php

$this->renderPartial('_addressData/_listInTable',
    array('model' => $model->senderAddressData,
    ));

?>


<h3><?php echo Yii::t('hybridMail','Odbiorcy'); ?> (<?php echo S_Useful::sizeof($model->hybridMailReceivers); ?>)</h3>
<?php $i = 1; ?>
<?php foreach($model->hybridMailReceivers AS $receiver):?>


    <h4><?php echo Yii::t('hybridMail','Odbiorca nr {number}', array('{number}' => $i++)); ?></h4>
    <?php

    $this->renderPartial('_addressData/_listInTable',
        array('model' => $receiver->addressData,
        ));
    ?>

<?php endforeach; ?>

    <h3><?php echo Yii::t('hybridMail','Historia');?></h3>

<?php
$this->renderPartial('//_statHistory/_statHistory',
    array('model' => $model->hybridMailStatHistoriesVisible));
?>