<?php 
/* @var $model AddressData */
?>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'name',
        'company',
        array(
            'name' => $model->getAttributeLabel('country'),
            'value' => CountryList::model()->findByPk($model->country_id)->trName,
        ),
        'city',
        'zip_code',
        'address_line_1',
        'address_line_2',
        'tel',
        'email',
    ),
));
?>

