<?php

class GoogleApi
{

    public static function getDataByCoordinates($lat, $lng)
    {

        if($lat == '' OR $lng == '')
            return NULL;

        $CACHE = Yii::app()->cache;
        $CACHE_NAME = 'GOOGLE_API_GETDATABYCOORDINATES_'.$lat.'_'.$lng;

        $fromCache = true;
        $data = $CACHE->get($CACHE_NAME);

        MyDump::dump('googleApi_REQ.txt', print_r($lat.'|'.$lng,1).' : '.($data ? 'CACHED' : 'NOT'));
        if($data === false)
        {
            $fromCache = false;
            $result = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&key='.GLOBAL_CONFIG::GOOGLE_MAPS_API_KEY);
            MyDump::dump('googleApi_getdatabycoordinates.txt', print_r($result,1));
            if($result)
            {
                $result = @json_decode($result);
                if($result == false OR $result->status != 'OK')
                    $data = NULL;
                else
                    $data = $result;

                $CACHE->set($CACHE_NAME, $data, 604800); // a week
            }
        }

        MyDump::dump('googleApi_getdatabycoordinates.txt', print_r($lat.'|'.$lng,1).' : '.($fromCache ? 'CACHED' : '').' : '.print_r($data,1));

        return $data;
    }



    /**
     * @param $address
     * @return array|null
     */
    public static function getCoordinatesByAddress($address)
    {
        if($address == '') {
            MyDump::dump('googleApi_coordinates.txt', print_r($address,1).' : EMPTY - IGNORED');
            return NULL;
        }

        $CACHE = Yii::app()->cache;
        $CACHE_NAME = 'GOOGLE_API_COORDINATES_'.$address;

        $fromCache = true;
        $data = $CACHE->get($CACHE_NAME);

        if($data === false)
        {
            $fromCache = false;
            $result = self::getDataByAddress($address);

            if($result)
            {

                if($result->status == 'OK')
                {
                    $data = [
                        $result->results[0]->geometry->location->lat,
                        $result->results[0]->geometry->location->lng
                    ];
                }

                if($data == false)
                    $data = NULL;

                $CACHE->set($CACHE_NAME, $data, 604800); // a week
            }

        }

        MyDump::dump('googleApi_coordinates.txt', print_r($address,1).' : '.($fromCache ? 'CACHED' : '').' : '.print_r($data,1));

        return $data;
    }

    public static function getDataByAddress($address)
    {
        $address = S_Useful::removeNationalCharacters($address);
        $address = strtolower(trim($address));

        if($address == '')
            return NULL;

        $address = urlencode($address);

        $CACHE = Yii::app()->cache;
        $CACHE_NAME = 'GOOGLE_API_GETDATABYADDRESS_'.$address;

        $fromCache = true;
        $data = $CACHE->get($CACHE_NAME);

        MyDump::dump('googleApi_REQ.txt', print_r($address,1).' : '.($data ? 'CACHED' : 'NOT'));

        if($data === false)
        {
            $fromCache = false;
            $result = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.GLOBAL_CONFIG::GOOGLE_MAPS_API_KEY);

            if($result)
            {
                $result = @json_decode($result);
                if($result == false OR $result->status != 'OK')
                    $data = NULL;
                else
                    $data = $result;

                $CACHE->set($CACHE_NAME, $data, 604800); // a week
            }

        }

        MyDump::dump('googleApi_getdatabyaddress.txt', print_r($address,1).' : '.($fromCache ? 'CACHED' : '').' : '.print_r($data,1));

        return $data;
    }

    /**
     * @param $fullAddress string
     * @return NULL|array
     */
    public static function correctAddressData($fullAddress, $withHouseNumberBypass = false)
    {
        $fromCache = false;

        if($withHouseNumberBypass)
        {
            $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-]+)((\s)+|$))*/i';
            $building = '';

            if(preg_match_all($REG_PATTERN, $fullAddress, $result2))
                $building = $result2[0][0];

            $building = trim($building);

            $fullAddress = str_replace($building, '', $fullAddress);
        }

        $data = self::getDataByAddress($fullAddress);

        $return = NULL;
        if($data)
        {
            $data = $data->results[0]->address_components;

            if(is_array($data))
                foreach($data AS $item)
                {
                    $name = implode(' ', $item->types);
                    $result[$name] = $item->short_name;
                }

            if(isset($result['street_number']))
            {
                if($withHouseNumberBypass)
                    $return['address_line'] = $result['route'].' '.$building;
                else
                    $return['address_line'] = $result['route'].' '.$result['street_number'];

                $return['zip_code'] = $result['postal_code'];
                $return['city'] = isset($result['administrative_area_level_3 political']) ? $result['administrative_area_level_3 political'] : $result['administrative_area_level_2 political'];
            }
        }

        MyDump::dump('googleApi_getcorrectaddress.txt', print_r($fullAddress,1).' : '.($fromCache ? 'CACHED' : '').' : '.print_r($return,1));

        return $return;
    }
}