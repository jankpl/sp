<?php

class CourierBufferController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }


    public function actionIndex() {

        $model = new _CourierBufferGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierBufferGridView']))
            $model->setAttributes($_GET['CourierBufferGridView']);

        $this->render('index', array(
            'model' => $model,
        ));
    }
}