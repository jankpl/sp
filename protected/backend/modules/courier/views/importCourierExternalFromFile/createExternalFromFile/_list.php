    <?php $modelTemp = new Courier;?>
    <?php $modelTemp2 = new AddressData();?>
    <?php $modelTemp3 = new CourierTypeExternal();?>

    <?php //echo CHtml::hiddenField('fileTempName', $fileModel->file->tempName);?>
    <div style="width: 100%; position: relative; height: 500px; overflow-x: scroll;">
        <div style="width: 3000px; position: absolute;">

            <table class="list hTop">
                <tr>
                    <td colspan="10">Packages</td>
                    <td colspan="2">External type</td>
                    <td colspan="8">Sender</td>
                    <td colspan="8">Receiver</td>
                </tr>


                <tr>
                   <td>
                        <?php echo $modelTemp->getAttributeLabel('courier_stat_id'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('_first_status_date'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_weight'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_size_l'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_size_w'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_size_d'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('packages_number'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_value'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_content'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('user_id'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('ref'); ?>
                    </td><td style="border-left: 2px dashed gray;">
                    <?php echo $modelTemp3->getAttributeLabel('courier_external_operator_id'); ?>
                    </td><td>
                        <?php echo $modelTemp3->getAttributeLabel('external_id'); ?>
                    </td><td style="border-left: 2px dashed gray;">
                    <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country_id'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td style="border-left: 2px dashed gray;">
                    <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country_id'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td>
                </tr>

                <?php

                foreach($models AS $key => $item):
                    $this->renderPartial('createExternalFromFile/_item',
                        array(
                            'split' => $split,
                            'model' => $item,
                            'i' => $key,
                        ));


                endforeach;
                ?>
            </table>

        </div>
    </div>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <div class="navigate" style="text-align: center;">

        <?php echo TbHtml::button(Yii::t('app', 'Validate this part of data'), array('name' => 'validate', 'class' => 'save-packages', 'data-mode' => Courier::MODE_JUST_VALIDATE)); ?>

        <?php echo TbHtml::button(Yii::t('app', 'Save this part of data without validation'), array('name' => 'save_no_validation', 'class' => 'save-packages', 'data-mode' => Courier::MODE_SAVE_NO_VALIDATE)); ?>
        
        <?php echo TbHtml::button(Yii::t('app', 'Save this part of data'), array('name' => 'save', 'size' => TbHtml::BUTTON_SIZE_LARGE, 'class' => 'save-packages', 'data-mode' => Courier::MODE_SAVE)); ?>


        <?php echo CHtml::hiddenField('mode','', array('id' => 'mode'));?>
    </div>
</div><!-- form -->