<div class="alert alert-success text-center">
    <h4><?php echo Yii::t('courier','Paczka zwrotna');?></h4>
    <strong>#<?= $model->local_id;?></strong>
    <br/>
    <?php echo CHtml::link(Yii::t('courier','Przejdź do paczki zwrotnej'), array('/courier/courier/view', 'urlData' => $model->hash), array('target' => '_blank', 'class' => 'btn'));?>
</div>
