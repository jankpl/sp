<?php

class DpdIrClient extends GlobalOperator
{
    const URL_TT = 'http://api.dpd.ie:8080/RestTracking/webresources/rest/parcels';
    const URL = 'http://api.dpd.ie:8080/RestPreadvice/service/request';
    const ACCOUNT_ID = GLOBAL_CONFIG::DPDIR_ACCOUNT_ID;

    const TT_USER_ID = '3333L3';
    const TT_USER_PASSWORD = 'M0nteryL0g1stics';

//    const TT_USER_ID = '1111L1';
//    const TT_USER_PASSWORD = '1nterl1nk';

    const SERVICE_OVERNIGHT = 1; // from NI to UK as option; DEFAULT FRO ROIS and NI
    const SERVICE_SATURDAY_DELIVERY = 2; // for ROI and NI
    const SERVICE_2_DAY = 6; // default to UK

    public $ref1;
    public $ref2;
    public $ref3;

    public $package_value;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_tel;
    public $sender_mail;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_mail;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $packages_number = 1;

    public $package_content;

    public $cod_value = 0;
    public $cod_currency = false;

    public $user_id;

    public $saturday_delivery = false;
    public $overnight_int = false;

    public $sms_notify = false;
    public $email_notify = false;

    public $label_annotation_text = false;

    public static function getAdditions(Courier $courier)
    {
        $is_receiver_ie = $courier->receiverAddressData->country_id == CountryList::COUNTRY_IE ? 1 : 0;

        $CACHE_NAME = 'DPDIR_ADDITIONS_'.$is_receiver_ie.'_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_EMAIL_NOTIFY;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SMS_NOTIFY;

            if($is_receiver_ie)
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SATURDAY_DELIVERY;
            else
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_OVERNIGHT_INT_SERVICE;


            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected function _curlCall($data = false, $tt = false)
    {
        $url = self::URL;

        if($tt)
            $url = self::URL_TT;
        else
            MyDump::dump('dpdir.txt', print_r($data, 1));

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $curl = curl_init($url);
        curl_setopt_array($curl, array(

            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING => "UTF-8",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/xml',
//                'Accept: application/json',
//                'Version : ' . '100',
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        ));

        $resp = curl_exec($curl);

        if($tt) {

            $resp = json_decode($resp);
            return $resp;

        } else {
            MyDump::dump('dpdir.txt', print_r($resp, 1));
        }

        $resp = @simplexml_load_string($resp);

        if(!$resp OR $resp->Status != 'OK') {
            $return->error = true;
            $return->errorCode = $resp->PreAdviceErrorCode;
            $return->errorDesc = $resp->PreAdviceErrorDetails ? $resp->PreAdviceErrorDetails : 'Unknown error';
        } else {

            $errors = false;
            foreach($resp->Consignment AS $item)
            {
                if($item->RecordErrorDetails)
                {
                    $errors = true;
                    $return->error = true;
                    $return->errorCode = $item->RecordErrorCode;
                    $return->errorDesc = $item->RecordErrorDetails ? $item->RecordErrorDetails : 'Unknown error';
                    break;
                }
            }

            if(!$errors) {
                $return->success = true;
                $return->data = $resp;
            }
        }

        return $return;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;
        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id, GLOBAL_BROKERS::BROKER_DPDIR);

        $senderName = $from->name;
        $senderCompany = $from->company;

        $model->sender_name = $senderName;
        $model->sender_company = $senderCompany;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->packages_number = $courierInternal->courier->packages_number;

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->ref1 = '#'.$courierInternal->courier->local_id;
        $model->ref2 = $courierInternal->courier->package_content;

        $model->cod_value = 0;
        if($courierInternal->courier->cod && $courierInternal->courier->cod_value > 0) {
            $model->cod_value = $courierInternal->courier->cod_value;
            $model->cod_currency = $courierInternal->courier->cod_currency;
        }

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SATURDAY_DELIVERY))
            $model->saturday_delivery = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_OVERNIGHT_INT_SERVICE))
            $model->overnight_int = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SMS_NOTIFY))
            $model->sms_notify = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_EMAIL_NOTIFY))
            $model->email_notify = true;

        return $model->createShipment($returnErrors);
    }
//
    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $courierLabelNew, $returnError = false)
    {

        $model = new self;
        $model->label_annotation_text = $courierU->courierUOperator->label_symbol;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;


        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierU->courier->package_weight;
        $model->packages_number = $courierU->courier->packages_number;

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->ref1 = '#'.$courierU->courier->local_id;
        $model->ref2 = $courierU->courier->package_content;

        $model->cod_value = 0;
        if($courierU->courier->cod && $courierU->courier->cod_value > 0) {
            $model->cod_value = $courierU->courier->cod_value;
            $model->cod_currency = $courierU->courier->cod_currency;
        }

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SATURDAY_DELIVERY))
            $model->saturday_delivery = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_OVERNIGHT_INT_SERVICE))
            $model->overnight_int = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SMS_NOTIFY))
            $model->sms_notify = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_EMAIL_NOTIFY))
            $model->email_notify = true;

        return $model->createShipment($returnError);
    }

//    public static function test()
//    {
//        $model = new self;
//        $model->sender_name = 'Jan Kowalski';
//        $model->sender_company = '';
//        $model->sender_address1 = 'Test Str 10';
//        $model->sender_city = 'Warsaw';
//        $model->sender_zip_code = '00-011';
//        $model->sender_country = 'PL';
//        $model->sender_tel = '222333111';
//        $model->sender_mail = 'dsaawqe@wqqweeqw.pl';
//
//        $model->receiver_name = 'Jan Nowak';
//        $model->receiver_company = 'Nowak Inc';
//        $model->receiver_address1 = 'Test Str 1';
//        $model->receiver_city = 'Ballina';
//        $model->receiver_zip_code = 'F26 V6E4';
//        $model->receiver_country = 'IE';
//        $model->receiver_tel = '123123123';
//        $model->receiver_mail = 'dasdsaad@dsasda.pl';
//
//        $model->package_weight = 1;
//        $model->package_content = 'test';
//        $model->reference = '12321123';
//
//        $model->cod_value = 10;
//        $model->cod_currency = 'EUR';
//
//        $model->packages_number = 1;
//
//        $model->user_id = '123213';
//
////        $model->saturday_delivery = true;
//
//        return $model->createShipment(true);
//    }

    protected static function clearString($text)
    {
        return htmlspecialchars($text, ENT_XML1, 'UTF-8');
    }

    public function createShipment($returnError = false)
    {

        $serviceOption = 5; // normal
        if($this->cod_value)
            $serviceOption = 1; // COD


        if($this->receiver_country_id == CountryList::COUNTRY_IE OR $this->overnight_int)
            $serviceType = self::SERVICE_OVERNIGHT;
        else
            $serviceType = self::SERVICE_2_DAY;

        if($this->saturday_delivery)
            $serviceType = self::SERVICE_SATURDAY_DELIVERY;

        $item = \FluidXml\fluidxml('PreAdvice');

        $da = \FluidXml\fluidxml('DeliveryAddress');

        $da->add('Contact', self::clearString($this->receiver_name))
            ->add('ContactTelephone', self::clearString($this->receiver_tel))
            ->add('ContactEmail', self::clearString($this->receiver_mail))
            ->add('BusinessName', self::clearString($this->receiver_company))
            ->add('AddressLine1', self::clearString($this->receiver_address1))
            ->add('AddressLine2', self::clearString($this->receiver_address2))
            ->add('AddressLine3', self::clearString($this->receiver_city))
            ->add('AddressLine4', self::clearString($this->receiver_country))
            ->add('PostCode', self::clearString($this->receiver_zip_code))
            ->add('CountryCode', self::clearString($this->receiver_country))
            ->add('Method', 'Road')
        ;

        $ca = \FluidXml\fluidxml('CollectionAddress');
        $ca->add('Contact', self::clearString($this->sender_name))
            ->add('ContactTelephone', self::clearString($this->sender_tel))
            ->add('ContactEmail', self::clearString($this->sender_mail))
            ->add('BusinessName', self::clearString($this->sender_company))
            ->add('AddressLine1', self::clearString($this->sender_address1))
            ->add('AddressLine2', self::clearString($this->sender_address2))
            ->add('AddressLine3', self::clearString($this->sender_city))
            ->add('AddressLine4', self::clearString($this->sender_country))
            ->add('PostCode', self::clearString($this->sender_zip_code))
            ->add('CountryCode', self::clearString($this->sender_country))
            ->add('Method', 'Road')
        ;

        $ra = \FluidXml\fluidxml('ReturnAddress');
        $ra->add('Contact', self::clearString($this->sender_name))
            ->add('ContactTelephone', self::clearString($this->sender_tel))
            ->add('ContactEmail', self::clearString($this->sender_mail))
            ->add('BusinessName', self::clearString($this->sender_company))
            ->add('AddressLine1', self::clearString($this->sender_address1))
            ->add('AddressLine2', self::clearString($this->sender_address2))
            ->add('AddressLine3', self::clearString($this->sender_city))
            ->add('AddressLine4', self::clearString($this->sender_country))
            ->add('PostCode', self::clearString($this->sender_zip_code))
            ->add('CountryCode', self::clearString($this->sender_country))
            ->add('Method','Road')
        ;

        $references = \FluidXml\fluidxml('References');
        $r = \FluidXml\fluidxml('Reference');
        $r->add('ReferenceName', 'ref1')
            ->add('ReferenceValue', self::clearString($this->ref1))
            ->add('ParcelNumber', 1);

        $r2 = \FluidXml\fluidxml('Reference');
        $r2->add('ReferenceName','ref2')
            ->add('ReferenceValue', self::clearString($this->ref2))
            ->add('ParcelNumber', 1);

//        $r3 = \FluidXml\fluidxml('Reference'));
//        $r3->add('ReferenceName', self::clearString('ref3'))
//            ->add('ReferenceValue', self::clearString($this->reference))
//            ->add('ParcelNumber', self::clearString(1));

        $references
            ->add($r)
            ->add($r2)
        ;


        if($this->cod_value)
        {
            $item->add('Consignment',true)
                ->add('RecordID',1)
                ->add('AlertEmailAddress', self::clearString($this->email_notify && $this->receiver_mail ? $this->receiver_mail : NULL))
                ->add('AlertSmsNumber', self::clearString($this->sms_notify && $this->receiver_tel ? $this->receiver_tel : NULL))
                ->add('CustomerAccount', self::ACCOUNT_ID)
                ->add('TotalParcels', self::clearString($this->packages_number))
                ->add('Relabel', 0)
                ->add('ServiceAmount', self::clearString($this->cod_value))
                ->add('ServiceCurrency', self::clearString($this->cod_currency))
                ->add('ServiceOption', self::clearString($serviceOption))
                ->add('ServiceType', self::clearString($serviceType))
                ->add('Weight', self::clearString($this->package_weight))
                ->add($da)
                ->add($ca)
                ->add($ra)
                ->add($references)
            ;
        } else {
        $item->add('Consignment', true)
            ->add('RecordID',1)
            ->add('AlertEmailAddress', self::clearString($this->email_notify && $this->receiver_mail ? $this->receiver_mail : NULL))
            ->add('AlertSmsNumber', self::clearString($this->sms_notify && $this->receiver_tel ? $this->receiver_tel : NULL))
            ->add('CustomerAccount', self::ACCOUNT_ID)
            ->add('TotalParcels', self::clearString($this->packages_number))
            ->add('Relabel', 0)
            ->add('ServiceOption', self::clearString($serviceOption))
            ->add('ServiceType', self::clearString($serviceType))
            ->add('Weight', self::clearString($this->package_weight))
            ->add($da)
            ->add($ca)
            ->add($references)
        ;
    }



        $xml = $item->xml();

        $resp = $this->_curlCall($xml);

        if(!$resp->success)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->errorDesc);
            } else {
                return false;
            }
        }

        $return = [];
        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        foreach($resp->data->Consignment AS $item)
        {

            $ttNumber = (string) $item->TrackingNumber;

            foreach($item->Parcel AS $item2) {

                $labelUrl = (string) $item2->LabelImage;
                $label = @file_get_contents($labelUrl);

                $im->readImageBlob($label);
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }

                if($this->label_annotation_text)
                    LabelAnnotation::createAnnotation($this->label_annotation_text, $im, 15,15);

                $im->stripImage();


                $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);
            }
        }
        return $return;
    }



    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        $item = \FluidXml\fluidxml('TrackingRequest');

        $v = \FluidXml\fluidxml('Validation');
        $v->add('UserID', self::TT_USER_ID)
            ->add('Password', self::TT_USER_PASSWORD);
        ;

        $item
            ->add($v)
            ->add('TrackingNumber', $no);

        $xml = $item->xml();

        $model = new self;

        $response = $model->_curlCall($xml, true);

        $return = false;
        if($response && $response->TrackingInfo)
        {

            $statuses = new GlobalOperatorTtResponseCollection();
//            $statuses = [];
            foreach($response->TrackingInfo->TrackingHistory->EventDetail AS $item)
            {
                $stat = $item->EventTypeName;
                $location = $item->EventDepotName;

                if(mb_substr($stat,0,9) == 'Delivered')
                {
                    $signature = trim(str_replace('Delivered', '', $stat));
                    $stat = 'Delivered';
                    $location .= ' (signed by: "'.$signature.'")';
                }


                $statuses->addItem($stat, $item->EventDate.' '.$item->EventTime, $location);

//                $statuses[] = [
//                    'name' => $item->EventTypeName,
//                    'date' => $item->EventDate.' '.$item->EventTime,
//                    'location' => $item->EventDepotName,
//                ];
            }

            return $statuses;
        }


        return $return;

    }


}