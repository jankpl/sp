<?php

class F_DomesticImport extends CFormModel {

    public $file;
    public $omitFirst;
    public $split;
    public $domestic_operator_id;


    const MAX_LINES = 1000;
    const MAX_PACKAGES = 1000;

    public function rules() {
        return array(
            array('file', 'file',  'allowEmpty' => false,
                'maxSize' => 512400, 'types' => 'csv, xls, xlsx'),
            array('domestic_operator_id', 'numerical', 'integerOnly' => true),
            array('domestic_operator_id', 'required'),
            array('omitFirst', 'safe'),
            array('split', 'numerical'),
        );
    }

    public function attributeLabels() {
        return array(
            'file' => Yii::t('app', 'File'),
            'omitFirst' => Yii::t('app', 'Omit first line (header)'),
            'split' => Yii::t('app', 'Split after'),
            'domestic_operator_id' => Yii::t('app', 'Operator'),
        );
    }


    public static function getAttributesMap()
    {
        // mapping attributes to columns in file
        $attributes = Array(
            'package_weight' => 0,
            'package_size_l' => 1,
            'package_size_w' => 2,
            'package_size_d' => 3,
            'packages_number' => 4,
            'package_value' => 5,
            'package_content' => 6,
            'cod' => 7,

            'sender_name' => 8,
            'sender_company' => 9,
//            'sender_country' => 10,
            'sender_zip_code' => 10,
            'sender_city' => 11,
            'sender_address_line_1' => 12,
            'sender_address_line_2' => 13,
            'sender_tel' => 14,
            'sender_email' => 15,

            'receiver_name' => 16,
            'receiver_company' => 17,
//            'receiver_country' => 19,
            'receiver_zip_code' => 18,
            'receiver_city' => 19,
            'receiver_address_line_1' => 20,
            'receiver_address_line_2' => 21,
            'receiver_tel' => 22,
            'receiver_email' => 23,

            'bank_account' => 24,

            'ref' => 25,
//            'options' => 26,
        );

        return $attributes;
    }

    public static function calculateTotalNumberOfPackages(array $data)
    {

        $number = 0;
        foreach($data AS $item)
            $number += $item[self::getAttributesMap()['packages_number']];

        return $number;

    }

    public static function mapAttributesToModels(array $data, $domestic_operator_id)
    {
        $models = [];

        $i = 0;
        foreach($data AS $item)
        {
            $i++;

            $courier = new Courier_CourierTypeDomestic();
            $courier->source = Courier::SOURCE_FILE_IMPORT;

            $courier->courierTypeDomestic = new CourierTypeDomestic('step_1');
            $courier->senderAddressData = new CourierTypeDomestic_AddressData();
            $courier->receiverAddressData = new CourierTypeDomestic_AddressData();

            $courier->courierTypeDomestic->domestic_operator_id = $domestic_operator_id;
            $courier->cod_value = S_Useful::convertCommaToDec($item[self::getAttributesMap()['cod']]);

            $courier->package_weight = S_Useful::convertCommaToDec($item[self::getAttributesMap()['package_weight']]);
            $courier->package_size_l = S_Useful::convertCommaToDec($item[self::getAttributesMap()['package_size_l']]);
            $courier->package_size_w = S_Useful::convertCommaToDec($item[self::getAttributesMap()['package_size_w']]);
            $courier->package_size_d = S_Useful::convertCommaToDec($item[self::getAttributesMap()['package_size_d']]);
            $courier->packages_number = S_Useful::convertCommaToDec($item[self::getAttributesMap()['packages_number']]);
            $courier->package_value = S_Useful::convertCommaToDec($item[self::getAttributesMap()['package_value']]);
            $courier->package_content = $item[self::getAttributesMap()['package_content']];


            $courier->senderAddressData->name = $item[self::getAttributesMap()['sender_name']];
            $courier->senderAddressData->company = $item[self::getAttributesMap()['sender_company']];
            $courier->senderAddressData->country_id = CountryList::COUNTRY_PL;
            $courier->senderAddressData->zip_code = $item[self::getAttributesMap()['sender_zip_code']];
            $courier->senderAddressData->city = $item[self::getAttributesMap()['sender_city']];
            $courier->senderAddressData->address_line_1 = $item[self::getAttributesMap()['sender_address_line_1']];
            $courier->senderAddressData->address_line_2 = $item[self::getAttributesMap()['sender_address_line_2']];
            $courier->senderAddressData->tel = $item[self::getAttributesMap()['sender_tel']];
            $courier->senderAddressData->email = $item[self::getAttributesMap()['sender_email']];

            $courier->senderAddressData->bankAccount = $item[self::getAttributesMap()['bank_account']];

            $courier->receiverAddressData->name = $item[self::getAttributesMap()['receiver_name']];
            $courier->receiverAddressData->company = $item[self::getAttributesMap()['receiver_company']];
            $courier->receiverAddressData->country_id = CountryList::COUNTRY_PL;
            $courier->receiverAddressData->zip_code = $item[self::getAttributesMap()['receiver_zip_code']];
            $courier->receiverAddressData->city = $item[self::getAttributesMap()['receiver_city']];
            $courier->receiverAddressData->address_line_1 = $item[self::getAttributesMap()['receiver_address_line_1']];
            $courier->receiverAddressData->address_line_2 = $item[self::getAttributesMap()['receiver_address_line_2']];
            $courier->receiverAddressData->tel = $item[self::getAttributesMap()['receiver_tel']];
            $courier->receiverAddressData->email = $item[self::getAttributesMap()['receiver_email']];

            $courier->ref = $item[self::getAttributesMap()['ref']];

            $courier->courierTypeDomestic->courier = $courier;

//            $options = $item[self::getAttributesMap()['options']];
//            $options = explode('|', $options);
//
//            if(is_array($options) && S_Useful::sizeof($options))
//            {
//                $courier->courierTypeDomestic->addAdditions($options);
//            }

//            if($validate) {
//                $courier->validate();
//                $courier->receiverAddressData->validate();
//                $courier->senderAddressData->validate();
//                $courier->courierTypeDomestic->validate();
//
//            }

            array_push($models, $courier);
        }

        return $models;
    }

    public static function validateModels(array &$models)
    {
        $refNumbers = [];

        $errors = false;
        foreach($models AS $key => $model)
        {
            $models[$key]->validate();

            if($models[$key]->cod_value > 0 && !$models[$key]->hasErrors('cod_value'))
                $model->senderAddressData->_bankAccountRequired = true;

            $models[$key]->receiverAddressData->validate();
            $models[$key]->senderAddressData->validate();
            $models[$key]->courierTypeDomestic->validate();

            if($models[$key]->hasErrors() OR $models[$key]->receiverAddressData->hasErrors() OR $models[$key]->senderAddressData->hasErrors() OR $models[$key]->courierTypeDomestic->hasErrors())
                $errors = true;

            if($models[$key]->ref)
            {
                if(isset($refNumbers[$models[$key]->ref]))
                {
                    $models[$key]->addError('ref', Yii::t('courier', 'Numer REF musi być unikatowy!'));
                    $errors = true;
                }
                $refNumbers[$models[$key]->ref] = true;
            }
        }

        return $errors;
    }

//    public static function hasGroupErrors(array &$models)
//    {
//        $errors = false;
//        foreach($models AS $key => $model)
//        {
//            if(!$models[$key]->validate())
//                $errors = true;
//
//            if(!$models[$key]->receiverAddressData->validate())
//                $errors = true;
//
//            if(!$models[$key]->senderAddressData->validate())
//                $errors = true;
//
//            if(!$models[$key]->courierTypeDomestic->validate())
//                $errors = true;
//        }
//
//        return $errors;
//    }


}