<?php

class GlsClient extends CApplicationComponent
{
    const SESSION_CACHE_NAME  = 'glsClientLogin';
    const SESSION_TIMEOUT = 25; // session timeout in minutes

    const URL = GLOBAL_CONFIG::GLS_URL;
    const LOGIN =  GLOBAL_CONFIG::GLS_LOGIN;
    const PASSWORD =  GLOBAL_CONFIG::GLS_PASSWORD;

    protected $logged = false;

    protected $throwErrors = false;

    protected $session;

    protected $_sender_name;
    protected $_sender_person;
    protected $_sender_contact_tel;
    protected $_sender_address;
    protected $_sender_city;
    protected $_sender_postalCode;
    protected $_sender_countryCode;
    protected $_sender_email;
    protected $_sender_is_private;

    protected $_receiver_name;
    protected $_receiver_person;
    protected $_receiver_contact_tel;
    protected $_receiver_address;
    protected $_receiver_city;
    protected $_receiver_postalCode;
    protected $_receiver_countryCode;
    protected $_receiver_email;
    protected $_receiver_is_private;

    protected $_package_local_id;

    protected $_package_size_d;
    protected $_package_size_w;
    protected $_package_size_s;
    protected $_package_weight;

    protected $_package_desc;

    protected $_cod_amount;
    protected $_cod_currency;

    protected $_insurance;

    protected $_number_of_packages = 1;

    protected $_options = [];

//    public $_packages_no_array;
//    public $_total_weight;
//    public $_total_quantity;

    const OPTION_ROD = 'rod';
    const OPTION_DEVLIVERY_TO_10 = 's10';
    const OPTION_DEVLIVERY_TO_12 = 's12';
    const OPTION_SATURDAY = 'sat';

    const OPTION_PICK_AND_SHIP = 'ps';


    private function __construct()
    {
    }

    public static function instance($throwErrors = false)
    {
        $model = new self;
        $model->init();
        $model->throwErrors = $throwErrors;

        return $model;
    }

    /**
     * @param $functionName
     * @param array $data
     * @param boolean $withoutLogin If true, system won't add session data or try to login before making this call. Used mostly in login function
     * @return stdClass
     * @throws CException
     */
    protected function _soapOperation($functionName, array $data, $withoutLogin = false)
    {

        $mode = array
        (
            'soap_version' => SOAP_1_1,
//            'trace' => 1,
//            'exceptions' => true,
        );

        $client = new SoapClient(self::URL.'?wsdl', $mode);
        $client->__setLocation(self::URL);

        if(!$withoutLogin && !$this->logged) {
            if($this->_login()) {
                $data['session'] = $this->session;
            } else {
                $return = new stdClass();
                $return->success = false;
                $return->error = 'Logging error';

                return $return;
            }
        }

        try {
            $resp = $client->__soapCall($functionName, [$functionName => $data]);

        } catch (SoapFault $E) {

            if($this->throwErrors)
                throw new CException(print_r([
                        'code' => $E->faultcode,
                        'string' => $E->faultstring,
                        'lastRequest' => print_r($client->__getLastRequest(),1),
                    ],1)
                );
            $return = new stdClass();
            $return->success = false;
            $return->error = $E->faultcode.'('.$E->faultstring.')';

            return $return;
        }

        if($this->session != '')
            $this->_setSessionCache();

        $return = new stdClass();
        $return->success = true;
        $return->data = $resp->return;

        return $return;
    }

    /**
     * Invoke this method after soap call to refresh session cache time
     */
    protected function _setSessionCache()
    {
        Yii::app()->cache->set(self::SESSION_CACHE_NAME, $this->session, 60 * self::SESSION_TIMEOUT);
    }

    /**
     * Login to ADE
     *
     * @return bool
     */
    protected function _login()
    {
        $sessionId = Yii::app()->cache->get(self::SESSION_CACHE_NAME);

        if($sessionId === false) {

            $response = $this->_soapOperation('adeLogin',
                [
                    'user_name' => self::LOGIN,
                    'user_password' => self::PASSWORD,
                ],
                true);

            if ($response->success) {
                $data = $response->data;
                $this->session = $data->session;
                $this->logged = true;

                $this->_setSessionCache();

                return true;
            } else {
                $this->logged = false;
                $this->session = '';
                return false;
            }
        } else {
            $this->logged = true;
            $this->session = $sessionId;
            return true;
        }
    }

    public static function getServicesAllowed()
    {
        $model = self::instance();
        $return = $model->_getServicesAllowed();

        if($return->success)
        {
            return $return->data->srv_bool;
        } else {
            return false;
        }
    }


    public static function getPremiumServices($zipCode)
    {
        $model = self::instance();
        $return = $model->_getPremiumServices($zipCode);

        if($return->success)
        {
            return $return->data->srv_bool;
        } else {
            return false;
        }
    }


    protected function _getPremiumServices($zipCode)
    {
        $response = $this->_soapOperation('adeServices_GetGuaranteed',
            [
                'zipcode' => $zipCode,
            ]);

        $return = new stdClass();
        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;

            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }
    }

    public static function checkZipCode($zipCode, $countryCode)
    {
        $model = self::instance();
        $return = $model->_checkZipCode($zipCode, $countryCode);

        if($return->success)
        {
            return $return->data->city;
        } else {
            return false;
        }
    }


    protected function _checkZipCode($zipCode, $countryCode)
    {
        $response = $this->_soapOperation('adeZip_GetCity',
            [
                'country' => $countryCode,
                'zipcode' => $zipCode,
            ]);

        $return = new stdClass();
        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;

            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }
    }

    protected function _getServicesAllowed()
    {
        $response = $this->_soapOperation('adeServices_GetAllowed',
            [
            ]);

        $return = new stdClass();
        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;

            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }
    }

    protected function _getConsignIdsFromPickup($pickupId, $start = 0)
    {
        $response = $this->_soapOperation('adePickup_GetConsignIDs',
            [
                'session' => $this->session,
                'id' => $pickupId,
                'id_start' => $start,
            ]);

        $return = new stdClass();
        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;

            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }



    }

    protected function _getLabels($packageId)
    {

        $response = $this->_soapOperation('adePreparingBox_GetConsignLabelsExt',
            [
                'session' => $this->session,
                'id' => $packageId,
                'mode' => 'one_label_on_a4_pdf',
            ]);

        $return = new stdClass();
        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;

            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }
    }

    protected function _getLabelsPickup($packageId)
    {

        $response = $this->_soapOperation('adePickup_GetConsignLabelsExt',
            [
                'session' => $this->session,
                'id' => $packageId,
                'mode' => 'one_label_on_a4_pdf',
            ]);

        $return = new stdClass();
        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;

            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }
    }


    protected function _getPickupPackages($pickupId)
    {
        $response = $this->_soapOperation('adePickup_GetConsign',
            [
                'session' => $this->session,
                'id' => $pickupId,
            ]);

        $return = new stdClass();
        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;

            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }



    }

    // DEPRECATED - WE DONT QUEQUE IT BECAUSE FOR PICK&SHIP IT HAS TO BE DONE JUST AFTER ORDERING PACKAGE
//
//    public static function quequeCollectionForCourier(Courier $courier, $gls_consigns_ids)
//    {
//        return false;
//
//        // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
//        if($courier->courierTypeInternal != NULL && $courier->courierTypeInternal->parent_courier_id != NULL)
//            return true;
//
//        // For Type Internal do not call if without pickup option
//        if($courier->courierTypeInternal != NULL && !$courier->courierTypeInternal->with_pickup)
//            return true;
//
//        // For Type Internal Simple do not call if withous pickup option
//        if($courier->courierTypeInternalSimple != NULL && !$courier->courierTypeInternalSimple->with_pickup)
//            return true;
//
//        // DO NOT CALL UPS COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
//        if($courier->user != NULL && $courier->user->getUpsCollectBlock())
//            return true;
//
//        $courierCollect = new CourierCollect();
//        $courierCollect->courier_id = $courier->id;
//        $courierCollect->stat =  CourierCollect::STAT_WAITING;
//        $courierCollect->collect_operator_id = CourierCollect::OPERATOR_GLS;
//        $courierCollect->_gls_consigns_ids = $gls_consigns_ids;
//        return $courierCollect->save();
//    }
//
//    public static function runCollection($consigns_id)
//    {
//        $glsClient = self::instance();
//        $consigns_ids = [$consigns_id];
//
//        $return = $glsClient->_callPickup($consigns_ids);
//
//        if($return->success)
//        {
//            $response = array(
//                'desc' => NULL,
//                'prn' => $return->data->id,
//                'data' => NULL,
//            );
//            return $response;
//        } else {
//            $return = array(
//                'error' => $return->error,
//                'data' => NULL,
//            );
//            return $return;
//        }
//    }

    protected function _callPickup(array $packagesIds)
    {

        $response = $this->_soapOperation('adePickup_Create',
            [
                'session' => $this->session,
                'consigns_ids' => $packagesIds,
                'desc' => 'Pickup ze ŚwiataPrzesyłek',
            ]);

        $return = new stdClass();
        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;
            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }
    }

    public function _preparePackages()
    {
        $return = new stdClass();

        try
        {
            $response = $this->_soapOperation('adePreparingBox_Insert',
                [
                    'session' => $this->session,
                    'consign_prep_data' => $this->_generateConsignData(),
                ]);
        }
        catch (GlsClientException $ex)
        {
            $return->success = false;
            $return->error = $ex->getMessage();

            return $return;
        }

        if($response->success)
        {
            $return->success = true;
            $return->data = $response->data;

            return $return;
        } else {
            $return->success = false;
            $return->error = $response->error;

            return $return;
        }
    }

    protected function _generateConsignData()
    {
        // options
        $srv_bool = [
        ];

        foreach($this->_options AS $key => $value)
            $srv_bool[$key] = $value;


        if($this->_cod_amount > 0)
        {
            $srv_bool['cod'] = 1;
            $srv_bool['cod_amount'] = $this->_cod_amount;

            if(!CourierCodAvailability::compareCurrences($this->_cod_currency, CourierCodAvailability::CURR_PLN))
                throw new GlsClientException('Incorrect COD currency! Only PLN allowed!');

        }

        $data = [
            'rname1' => $this->_receiver_name, // rname1 | string | wymagany - Pierwsza część nazwy odbiorcy (tzw. Nazwa 1)
            'rname2' => $this->_receiver_person, // rname2 | string | opcja - Druga część nazwy odbiorcy (tzw. Nazwa 2)
            'rname3' => NULL, // rname3 | string | opcja - Druga część nazwy odbiorcy (tzw. Nazwa 2)
            'rcountry' => $this->_receiver_countryCode, //  rcountry | string | wymagany - Kod kraju odbiorcy (zgodny z ISO 3166-1 alfa-2)
            'rzipcode' => $this->_receiver_postalCode, //  rzipcode | string | wymagany - Kod pocztowy odbiorcy
            'rcity' => $this->_receiver_city, // rcity | string | wymagany - Nazwa miejscowości odbiorcy
            'rstreet' => $this->_receiver_address, //  rstreet | string | wymagany - Ulica odbiorcy
            'rphone' => $this->_receiver_contact_tel, // phone | string | opcja - Telefony do odbiorcy
            // do not provide email adres to block FDS (elastyczna dostawa) and receive simple 2D barcode
            'rcontact' => '', // rcontact | string | opcja - email, osoba kontaktowa odbiorcy

            'references' => $this->_package_local_id, // references | string | opcja - Referencje (pole to jest drukowane na etykietach, zazwyczaj podaje się w tym polu skrócony opis zawartości paczki, nr zamównienia etc.)
            'notes' => '', // notes  | string | opcja - Uwagi
            'quantity' => $this->_number_of_packages, // quantity | string | opcja - Ilość paczek w przesyłce, zawartość pola jest automatycznie korygowana na podstawie zawartości elementów tablicy z paczkami
            'weight' => ($this->_package_weight * $this->_number_of_packages), // weight | string | opcja - Waga wszystkich paczek w przesyłce [kg], zawartość pola jest automatycznie korygowana na podstawie zawartości elementów tablicy z paczkami
            'date' => '', // date | string | opcja - Data nadania, jeśli brak zostanie wstawiona aktualna data [YYYY-MM-DD]
            'pfc' => '', // pfc | string | opcja - Identyfikator MPK (patrz opis metody adePfc_GetStatus)

            'sendaddr' => [
                'name1' => $this->_sender_name, // rname1 | string | wymagany - Pierwsza część nazwy nadawcy (tzw. Nazwa 1)
                'name2' => $this->_sender_person, // rname2 | string | opcja - Druga część nazwy nadawcy (tzw. Nazwa 2)
                'name3' => NULL, // rname3 | string | opcja - Druga część nazwy nadawcy (tzw. Nazwa 2)
                'country' => $this->_sender_countryCode, //  rcountry | string | wymagany - Kod kraju nadawcy (zgodny z ISO 3166-1 alfa-2)
                'zipcode' => $this->_sender_postalCode, //  rzipcode | string | wymagany - Kod pocztowy nadawcy
                'city' => $this->_sender_city, // rcity | string | wymagany - Nazwa miejscowości nadawcy
                'street' => $this->_sender_address, //  rstreet | string | wymagany - Ulica nadawcy
            ],

            'srv_bool' => $srv_bool, // srv_bool | ServicesBool | opcja - Tablica z listą usług
            'srv_ade' => '', // srv_ade | string - Usługi zapisane w standardzie ADE. Zawartość elementu użyta na wejściu jest ignorowana. Przykład zawartości: COD 120.00PLN,EXW,ROD,POD,12:00.
            'srv_daw' => '', // srv_daw | ServiceDAW | opcja - Tablica z danymi usługi DAW
            'srv_ident' => '', // srv_ident | ServiceIDENT | opcja - Tablica z danymi usługi IDENT
            'srv_ppe' => '', // srv_ppe | ServicePPE | opcja - Tablica z danymi usług PR, PS, EXC i SRS

            'parcels' => [],
        ];

        if($this->_options[self::OPTION_PICK_AND_SHIP] == true)
        {
            $data['srv_ppe'] = [
                'sname1' => $this->_sender_name, // rname1 | string | wymagany - Pierwsza część nazwy nadawcy (tzw. Nazwa 1)
                'sname2' => $this->_sender_person, // rname2 | string | opcja - Druga część nazwy nadawcy (tzw. Nazwa 2)
                'sname3' => NULL, // rname3 | string | opcja - Druga część nazwy nadawcy (tzw. Nazwa 2)
                'scountry' => $this->_sender_countryCode, //  rcountry | string | wymagany - Kod kraju nadawcy (zgodny z ISO 3166-1 alfa-2)
                'szipcode' => $this->_sender_postalCode, //  rzipcode | string | wymagany - Kod pocztowy nadawcy
                'scity' => $this->_sender_city, // rcity | string | wymagany - Nazwa miejscowości nadawcy
                'sstreet' => $this->_sender_address, //  rstreet | string | wymagany - Ulica nadawcy
                'sphone' => $this->_sender_contact_tel, // phone | string | opcja - Telefony do nadawcy
                'scontact' => '', // rcontact | string | opcja - email, osoba kontaktowa nadawcy

                'rname1' => $this->_receiver_name, // rname1 | string | wymagany - Pierwsza część nazwy odbiorcy (tzw. Nazwa 1)
                'rname2' => $this->_receiver_person, // rname2 | string | opcja - Druga część nazwy odbiorcy (tzw. Nazwa 2)
                'rname3' => NULL, // rname3 | string | opcja - Druga część nazwy odbiorcy (tzw. Nazwa 2)
                'rcountry' => $this->_receiver_countryCode, //  rcountry | string | wymagany - Kod kraju odbiorcy (zgodny z ISO 3166-1 alfa-2)
                'rzipcode' => $this->_receiver_postalCode, //  rzipcode | string | wymagany - Kod pocztowy odbiorcy
                'rcity' => $this->_receiver_city, // rcity | string | wymagany - Nazwa miejscowości odbiorcy
                'rstreet' => $this->_receiver_address, //  rstreet | string | wymagany - Ulica odbiorcy
                'rphone' => $this->_receiver_contact_tel, // phone | string | opcja - Telefony do odbiorcy
                // do not provide email adres to block FDS (elastyczna dostawa) and receive simple 2D barcode
                'rcontact' => '', // rcontact | string | opcja - email, osoba kontaktowa odbiorcy

                'references' => $this->_package_local_id, // references | string | opcja - Referencje (pole to jest drukowane na etykietach, zazwyczaj podaje się w tym polu skrócony opis zawartości paczki, nr zamównienia etc.)
                'weight' => ($this->_package_weight * $this->_number_of_packages), // weight | string | opcja - Waga wszystkich paczek w przesyłce [kg], zawartość pola jest automatycznie korygowana na podstawie zawartości elementów tablicy z paczkami
            ];
        }

        for($i = 0; $i < $this->_number_of_packages; $i++)
        {
            $parcel = [
                'number' => '', // number | string |  - Numer paczki. W przypadku użycia struktury na wejściu element number jest ignorowany. Struktura na wyjściu, w której pole number jest puste, oznacza że paczka nie ma nadanego numeru.
                'reference' => '', // reference | string | opcja - Referencje
                'weight' => $this->_package_weight, //  weight | string | opcja - Waga paczki [kg]. Waga pojedynczej paczki nie może być mniejsza od 0,01kg (10g).

                'srv_bool' => $srv_bool, // srv_bool | ServicesBool  - Tablica z listą usług.
                'srv_ade' => '', // srv_ade | string - Usługi zapisane w standardzie ADE. Zawartość elementu użyta na wejściu jest ignorowana. Przykład zawartości: COD 120.00PLN,EXW,ROD,POD,12:00.
            ];

            array_push($data['parcels'], $parcel);
        }

         return $data;
    }

    protected function _orderAndGetLabel(Courier $courier, $returnError = false, AddressData $from = NULL, AddressData $to = NULL)
    {

        // PREPARE PACKAGE
        $return = $this->_preparePackages();

        if(!$return->success)
        {
            if($returnError == true)
            {
                $return = [0 => array(
                    'error' => $return->error,
                )];
                return $return;
            } else {
                return false;
            }
        } else {
            $packageID = $return->data->id;

            // IF PICK&SHIP MODE
            if($this->_options[self::OPTION_PICK_AND_SHIP] == true) {

                if(!is_array($packageID))
                    $packageID = [ $packageID ];

                // CALL PICKUP FOR PACKAGES
                $return = $this->_callPickup($packageID);

                if(!$return->success)
                {
                    if($returnError == true)
                    {
                        $return = [0 => array(
                            'error' => $return->error,
                        )];
                        return $return;
                    } else {
                        return false;
                    }
                } else {

                    $pickupId = $return->data->id;
                    // GET PACKAGES ID FROM PICKUP
                    $return = $this->_getConsignIdsFromPickup($pickupId);

                    if(!$return->success)
                    {
                        if($returnError == true)
                        {
                            $return = [0 => array(
                                'error' => $return->error,
                            )];
                            return $return;
                        } else {
                            return false;
                        }
                    } else {

                        $packageNewID = $return->data->items;
                        // GET DETAILS OF PACKAGES BY NEW PACKAGES ID
                        $return = $this->_getPickupPackages($packageNewID);

                        if(!$return->success)
                        {
                            if($returnError == true)
                            {
                                $return = [0 => array(
                                    'error' => $return->error,
                                )];
                                return $return;
                            } else {
                                return false;
                            }
                        } else {


                            $labels = $return->data->parcels->items;

                            $return2 = [];

                            // for single labels, put into arrays to be compatible with multiple arrays return
                            if(!is_array($labels))
                                $labels = [$labels];

                            $i = 1;
                            foreach($labels AS $label)
                            {

                                if($from == NULL)
                                    $from = $courier->senderAddressData;

                                if($to == NULL)
                                    $to = $courier->receiverAddressData;

                                $temp = [];
                                $temp['status'] = true;
                                $temp['track_id'] = $label->number;
                                $temp['label'] = KaabLabel::generate($courier, $i, $from, $to, false, false, false, 'VIA GLS (' . $label->number . ')');
                                $temp['gls_no_label'] = false;
                                $temp['ownLabel'] = true;
                                array_push($return2, $temp);

                                $i++;
                            }


                            return $return2;
                        }

                    }
                }


            }
            // NO PICKUP MODE (NO PICK&SHIP):
            else
                $return = $this->_getLabels($packageID);

            // NO PICKUP MODE (NO PICK&SHIP) CONT (WE ACTUALLY GET LABELS FROM GLS!).:

            if(!$return->success)
            {
                if($returnError == true)
                {
                    $return = [0 => array(
                        'error' => $return->error,
                    )];
                    return $return;
                } else {
                    return false;
                }
            } else {

                $labels = $return->data->items;



                $return2 = [];

                // for single labels, put into arrays to be compatible with multiple arrays return
                if(!is_array($labels))
                    $labels = [$labels];

                foreach($labels AS $label)
                {
                    $temp = [];
                    $temp['status'] = true;
                    $temp['track_id'] = $label->number;
                    $temp['label'] = base64_decode($label->file);
                    $temp['gls_consign_id'] = $packageID;
                    array_push($return2, $temp);
                }

                return $return2;
            }
        }

    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $returnError = false, $internalMode = false)
    {
        $model = self::instance();

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_package_local_id = $courierInternal->courier->local_id;

        $model->_package_desc = $courierInternal->courier->package_content;

        $model->_package_size_d = $courierInternal->courier->package_size_d;
        $model->_package_size_w = $courierInternal->courier->package_size_l;
        $model->_package_size_s = $courierInternal->courier->package_size_w;

        $model->_package_weight = $courierInternal->courier->getWeight(true);

        $model->_number_of_packages = $courierInternal->courier->packages_number;

        $model->_cod_amount = $courierInternal->courier->cod_value;
        $model->_cod_currency = $courierInternal->courier->cod_currency;

        if($courierInternal->hasAdditionById(CourierAdditionList::GLS_ROD))
            $model->_options[self::OPTION_ROD] = true;

        if($courierInternal->hasAdditionById(CourierAdditionList::GLS_DELIVERY_TO_12))
            $model->_options[self::OPTION_DEVLIVERY_TO_12] = true;
        else if($courierInternal->hasAdditionById(CourierAdditionList::GLS_DELIVERY_TO_10))
            $model->_options[self::OPTION_DEVLIVERY_TO_10] = true;

        if($courierInternal->hasAdditionById(CourierAdditionList::GLS_DELIVERY_SATURDAY))
            $model->_options[self::OPTION_SATURDAY] = true;


        if(
            $internalMode != CourierLabelNew::INTERNAL_MODE_SECOND
            &&
            // PICKUP_MOD / 14.09.2016
//            $courier->with_pickup
            $courierInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR
            && $courierInternal->parent_courier_id == NULL  // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
            && (
                $courierInternal->courier->user == NULL
                OR
                (
                    $courierInternal->courier->user != NULL
                    &&
                    !$courierInternal->courier->user->getUpsCollectBlock()
                )
            )
        ) {
            $model->_options[self::OPTION_PICK_AND_SHIP] = true;
        }

        $model->UkZipCodeFix();

        return $model->_orderAndGetLabel($courierInternal->courier, true, $from, $to);
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {

        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model = self::instance();

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_package_local_id = $courierTypeU->courier->local_id;

        $model->_package_desc = $courierTypeU->courier->package_content;

        $model->_package_size_d = $courierTypeU->courier->package_size_d;
        $model->_package_size_w = $courierTypeU->courier->package_size_l;
        $model->_package_size_s = $courierTypeU->courier->package_size_w;

        $model->_package_weight = $courierTypeU->courier->package_weight;

        $model->_number_of_packages = $courierTypeU->courier->packages_number;

        $model->_cod_amount = $courierTypeU->courier->cod_value;
        $model->_cod_currency = $courierTypeU->courier->cod_currency;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_GLS_ROD))
            $model->_options[self::OPTION_ROD] = true;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_GLS_DELIVERY_12))
            $model->_options[self::OPTION_DEVLIVERY_TO_12] = true;
        else if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_GLS_DELIVERY_10))
            $model->_options[self::OPTION_DEVLIVERY_TO_10] = true;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_GLS_DELIVERY_SATURDAY))
            $model->_options[self::OPTION_SATURDAY] = true;


        if(

            // PICKUP_MOD / 14.09.2016
//            $courier->with_pickup
            $courierTypeU->collection
            && $courierTypeU->parent_courier_id == NULL  // Do not call for this package if is not first in group. Collection will be ordered for parent package
            && (
                $courierTypeU->courier->user == NULL
                OR
                (
                    $courierTypeU->courier->user != NULL
                )
            )
        ) {
            $model->_options[self::OPTION_PICK_AND_SHIP] = true;
        }

        $model->UkZipCodeFix();

        return $model->_orderAndGetLabel($courierTypeU->courier, true, $from, $to);
    }


//    public static function orderForCourierInternalSimple(CourierTypeInternalSimple $courierSimple, $returnError = false)
//    {
//        $model = self::instance();
//
//        $from = $courierSimple->courier->senderAddressData;
//        $to = $courierSimple->courier->receiverAddressData;
//
//        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
//        $model->_sender_city = $from->city;
//        $model->_sender_contact_tel = $from->tel;
//        $model->_sender_countryCode = $from->country0->code;
//        $model->_sender_email = $from->fetchEmailAddress();
//        $model->_sender_postalCode = $from->zip_code;
//        $model->_sender_is_private = $from->company == '' ? true : false;
//        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
//        $model->_sender_person = $from->name == '' ? $from->company : $from->name;
//
//        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
//        $model->_receiver_city = $to->city;
//        $model->_receiver_contact_tel = $to->tel;
//        $model->_receiver_countryCode = $to->country0->code;
//        $model->_receiver_email = $to->fetchEmailAddress();
//        $model->_receiver_postalCode = $to->zip_code;
//        $model->_receiver_is_private = $to->company == '' ? true : false;
//        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
//        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;
//
//        $model->_package_local_id = $courierSimple->courier->local_id;
//
//        $model->_package_desc = $courierSimple->courier->package_content;
//
//        $model->_package_size_d = $courierSimple->courier->package_size_d;
//        $model->_package_size_w = $courierSimple->courier->package_size_l;
//        $model->_package_size_s = $courierSimple->courier->package_size_w;
//
//        $model->_package_weight = $courierSimple->courier->package_weight;
//
//        $model->_number_of_packages = $courierSimple->courier->packages_number;
//
//        if(
//            $courierSimple->with_pickup
//            && (
//                $courierSimple->courier->user == NULL
//                OR
//                (
//                    $courierSimple->courier->user != NULL
//                    &&
//                    !$courierSimple->courier->user->getUpsCollectBlock()
//                )
//            )
//        ) {
//            $model->_options[self::OPTION_PICK_AND_SHIP] = true;
//        }
//
//        $model->UkZipCodeFix();
//
//        return $model->_orderAndGetLabel($courierSimple->courier, true);
//    }

    public static function orderForCourierDomestic(CourierTypeDomestic $courierDomestic, $returnError = false)
    {
        $model = self::instance();

        $from = $courierDomestic->courier->senderAddressData;
        $to = $courierDomestic->courier->receiverAddressData;

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_package_local_id = $courierDomestic->courier->local_id;

        $model->_package_desc = $courierDomestic->courier->package_content;

        $model->_package_size_d = $courierDomestic->courier->package_size_d;
        $model->_package_size_w = $courierDomestic->courier->package_size_l;
        $model->_package_size_s = $courierDomestic->courier->package_size_w;

        $model->_package_weight = $courierDomestic->courier->package_weight;

        $model->_number_of_packages = $courierDomestic->courier->packages_number;

        $model->_cod_amount = $courierDomestic->courier->cod_value;
        $model->_cod_currency = $courierDomestic->courier->cod_currency;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::GLS_ROD))
            $model->_options[self::OPTION_ROD] = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::GLS_DELIVERY_TO_12))
            $model->_options[self::OPTION_DEVLIVERY_TO_12] = true;
        else if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::GLS_DELIVERY_TO_10))
            $model->_options[self::OPTION_DEVLIVERY_TO_10] = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::GLS_DELIVERY_SATURDAY))
            $model->_options[self::OPTION_SATURDAY] = true;

        if(
            $courierDomestic->courier->user == NULL
            OR
            (
                $courierDomestic->courier->user != NULL
                &&
                !$courierDomestic->courier->user->getDomesticCollectBlock()
            )
        ) {
            $model->_options[self::OPTION_PICK_AND_SHIP] = true;
        }

        $model->UkZipCodeFix();

        return $model->_orderAndGetLabel($courierDomestic->courier, true);
    }

    /**
     * Method check if package fulfill maximum dimensions requirements
     *
     * @param $l integer Length of package [cm]
     * @param $w integer Width of package [cm]
     * @param $d integer Depth of package [cm]
     * @return bool Whether package fullfils requirements
     */
    public static function checkPackageDimensions($l, $w, $d)
    {
        $return = true;

        $dimensions = [$l, $w, $d];
        arsort($dimensions);

        if($dimensions[0] > 200)
            $return = false;

        if($dimensions[1] > 80)
            $return = false;

        if($dimensions[2] > 60)
            $return = false;

        // obwód + najdłuższy bok
        if(($dimensions[1] * 2 + $dimensions[2] * 2 + $dimensions[0]) > 300)
            $return = false;

        return $return;
    }

    /**
     * Method check if package fulfill maximum dimensions requirements
     *
     * @param $weight float Weight of package (not dimensional) [kg]     *
     * @param bool|false $onlyPl If requirements for polish domestic package
     * @return bool Whether package fullfils requirements
     */
    public static function checkPackageWeight($weight, $onlyPl = false)
    {
        $return = true;

        if($onlyPl)
        {
            if($weight > 31.5)
                $return = false;
        } else {
            if($weight > 50)
                $return = false;
        }

        return $return;
    }

    /**
     * Function returns maximum dimensions info text
     * @param bool|false $onlyPl If requirements for polish domestic package
     * @return string
     */
    public static function packageDimensionsInfo()
    {
        $params = [
            '{obw}' => '3 m',
            '{najdl}' => '2 m',
            '{szer}' => '80 cm',
            '{wys}' => '60 cm',
        ];

        $text = Yii::t('courier-gls', 'Paczka musi spełniać wymagania: obwód + najdłuższy bok < {obw}; najdłuższy bok < {najdl}; szerokość < {szer};wysokość < {wys}.', $params);

        return $text;
    }

    /**
     * Function returns maximum dimensions info text
     * @param bool|false $onlyPl If requirements for polish domestic package
     * @return string
     */
    public static function packageWeightInfo($onlyPl = false)
    {

        if($onlyPl)
            $params['{waga}'] = '31,5 kg';
        else
            $params['{waga}'] = '50 kg';

        $text = Yii::t('courier-gls', 'Maksymalna waga jednostkowa paczki to {waga}.', $params);

        return $text;
    }

    /**
     * Method converts XXXXXX zip code format into proper XXX XXX format for packages to GB
     */
    protected function UkZipCodeFix()
    {
        if(strtoupper($this->_receiver_countryCode) == 'GB')
            if(strlen($this->_receiver_postalCode) == 6)
                $this->_receiver_postalCode = mb_substr($this->_receiver_postalCode, 0, 3)." ".mb_substr($this->_receiver_postalCode, 0, -3);

    }

}

class GlsClientException extends Exception
{}