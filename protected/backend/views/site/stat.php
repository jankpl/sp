<?php


Yii::app()->getModule('courier');
//Yii::app()->getModule('internationalMail');
//Yii::app()->getModule('hybridMail');
Yii::app()->getModule('palette');
Yii::app()->getModule('specialTransport');
Yii::app()->getModule('postal');

?>
<div class="text-center">
    <h1>Stats</h1>
    <br/>
    <a href="<?= Yii::app()->createUrl('/site/');?>" class="btn btn-sm">Index</a>
</div>
<br/>
<br/>
<?php

$waiting_couriers['7days'] = 0;
$waiting_couriers['14days'] = 0;
$waiting_couriers['30days'] = 0;

$cmd = Yii::app()->db->cache(60*15)->createCommand();
$cmd->select("COUNT(id)");
$cmd->from("courier");
$cmd->where("courier_stat_id NOT IN (".implode(',',CourierStat::getKnownDeliveredStatuses()).") AND courier_stat_id != ".CourierStat::NEW_ORDER." AND courier_stat_id != ".CourierStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) BETWEEN 7 AND 14");
$waiting_couriers['7days'] = $cmd->queryScalar();

$cmd = Yii::app()->db->cache(60*15)->createCommand();
$cmd->select("COUNT(id)");
$cmd->from("courier");
$cmd->where("courier_stat_id NOT IN (".implode(',',CourierStat::getKnownDeliveredStatuses()).") AND courier_stat_id != ".CourierStat::NEW_ORDER." AND courier_stat_id != ".CourierStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) BETWEEN 14 AND 30");
$waiting_couriers['14days'] = $cmd->queryScalar();

$cmd = Yii::app()->db->cache(60*15)->createCommand();
$cmd->select("COUNT(id)");
$cmd->from("courier");
$cmd->where("courier_stat_id NOT IN (".implode(',',CourierStat::getKnownDeliveredStatuses()).") AND courier_stat_id != ".CourierStat::NEW_ORDER." AND courier_stat_id != ".CourierStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) > 30");
$waiting_couriers['30days'] = $cmd->queryScalar();

///

//$waiting_ims['7days'] = 0;
//$waiting_ims['14days'] = 0;
//$waiting_ims['30days'] = 0;
//
//$cmd = Yii::app()->db->cache(60*15)->createCommand();
//$cmd->select("COUNT(id)");
//$cmd->from("international_mail");
//$cmd->where("international_mail_stat_id != ".InternationalMailStat::FINISHED." AND international_mail_stat_id != ".InternationalMailStat::NEW_ORDER." AND international_mail_stat_id != ".InternationalMailStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) BETWEEN 7 AND 14");
//$waiting_international_mails['7days'] = $cmd->queryScalar();
//
//$cmd = Yii::app()->db->cache(60*15)->createCommand();
//$cmd->select("COUNT(id)");
//$cmd->from("international_mail");
//$cmd->where("international_mail_stat_id != ".InternationalMailStat::FINISHED." AND international_mail_stat_id != ".InternationalMailStat::NEW_ORDER." AND international_mail_stat_id != ".InternationalMailStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) BETWEEN 14 AND 30");
//$waiting_international_mails['14days'] = $cmd->queryScalar();
//
//$cmd = Yii::app()->db->cache(60*15)->createCommand();
//$cmd->select("COUNT(id)");
//$cmd->from("international_mail");
//$cmd->where("international_mail_stat_id != ".InternationalMailStat::FINISHED." AND international_mail_stat_id != ".InternationalMailStat::NEW_ORDER." AND international_mail_stat_id != ".InternationalMailStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) > 30");
//$waiting_international_mails['30days'] = $cmd->queryScalar();
///

//$waiting_hms['7days'] = 0;
//$waiting_hms['14days'] = 0;
//$waiting_hms['30days'] = 0;
//
//$cmd = Yii::app()->db->cache(60*15)->createCommand();
//$cmd->select("COUNT(id)");
//$cmd->from("hybrid_mail");
//$cmd->where("stat != ".HybridMailStat::FINISHED." AND stat != ".HybridMailStat::NEW_ORDER." AND stat != ".HybridMailStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) BETWEEN 7 AND 14");
//$waiting_hybrid_mails['7days'] = $cmd->queryScalar();
//
//$cmd = Yii::app()->db->cache(60*15)->createCommand();
//$cmd->select("COUNT(id)");
//$cmd->from("hybrid_mail");
//$cmd->where("stat != ".HybridMailStat::FINISHED." AND stat != ".HybridMailStat::NEW_ORDER." AND stat != ".HybridMailStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) BETWEEN 14 AND 30");
//$waiting_hybrid_mails['14days'] = $cmd->queryScalar();
//
//$cmd = Yii::app()->db->cache(60*15)->createCommand();
//$cmd->select("COUNT(id)");
//$cmd->from("hybrid_mail");
//$cmd->where("stat != ".HybridMailStat::FINISHED." AND stat != ".HybridMailStat::NEW_ORDER." AND stat != ".HybridMailStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) > 30");
//$waiting_hybrid_mails['30days'] = $cmd->queryScalar();

///

$waiting_postal['7days'] = 0;
$waiting_postal['14days'] = 0;
$waiting_postal['30days'] = 0;

$cmd = Yii::app()->db->cache(60*15)->createCommand();
$cmd->select("COUNT(id)");
$cmd->from("postal");
$cmd->where("postal_stat_id != ".PostalStat::FINISHED." AND postal_stat_id != ".PostalStat::NEW_ORDER." AND postal_stat_id != ".PostalStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) BETWEEN 7 AND 14");
$waiting_postal['7days'] = $cmd->queryScalar();

$cmd = Yii::app()->db->cache(60*15)->createCommand();
$cmd->select("COUNT(id)");
$cmd->from("postal");
$cmd->where("postal_stat_id != ".PostalStat::FINISHED." AND postal_stat_id != ".PostalStat::NEW_ORDER." AND postal_stat_id != ".PostalStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) BETWEEN 14 AND 30");
$waiting_postal['14days'] = $cmd->queryScalar();

$cmd = Yii::app()->db->cache(60*15)->createCommand();
$cmd->select("COUNT(id)");
$cmd->from("postal");
$cmd->where("postal_stat_id != ".PostalStat::FINISHED." AND postal_stat_id != ".PostalStat::NEW_ORDER." AND postal_stat_id != ".PostalStat::CANCELLED." AND TIMESTAMPDIFF(DAY, (CASE date_updated WHEN NULL THEN date_entered ELSE date_updated END), CURRENT_TIMESTAMP) > 30");
$waiting_postal['30days'] = $cmd->queryScalar();

?>

<?php

//$otherStat = 0;
//$dataJsArrayCourier = '[\'Status\',\'Items\'],';
//foreach(CourierStat::model()->with('couriersSTAT')->findAll() AS $item)
//{
//    $no = intval($item->couriersSTAT);
//    if($item->id == CourierStat::NEW_ORDER OR $item->id == CourierStat::CANCELLED OR in_array($item->id, CourierStat::getKnownDeliveredStatuses()) OR !$no)
//    {}
//    else if($no == 1)
//    {
//        $otherStat++;
//    }
//    else
//        $dataJsArrayCourier .= '[\'('.$item->id.') '.CHtml::encode($item->name).'\', '.$no.'],';
//}
//$dataJsArrayCourier .= '[\'Statuses with one item\','.$otherStat.'],';


$dataJsArrayCourier = '[\'Status Map\',\'Items\'],';
foreach(StatMap::mapNameList() AS $key => $item)
{
    $no = intval(StatMap::countCouriersForStatMap($key));
    if($key != StatMap::MAP_DELIVERED)
        $dataJsArrayCourier .= '[\'('.$key.') '.CHtml::encode($item).'\', '.$no.'],';
}
$no = intval(StatMap::countCouriersForStatMap(NULL));
$dataJsArrayCourier .= '[\'(-) Without MAP\', '.$no.'],';


//
//$otherStat = 0;
//$dataJsArrayHm = '[\'Status\',\'Items\'],';
//foreach(HybridMailStat::model()->with('hybridMailsSTAT')->findAll() AS $item)
//{
//    $no = intval($item->hybridMailsSTAT);
//    if($item->id == HybridMailStat::NEW_ORDER OR $item->id == HybridMailStat::CANCELLED OR $item->id == HybridMailStat::FINISHED OR !$no)
//    {}
//    else if($no == 1)
//    {
//        $otherStat++;
//    }
//    else
//        $dataJsArrayHm .= '[\'('.$item->id.') '.CHtml::encode($item->name).'\', '.$no.'],';
//}
//$dataJsArrayHm .= '[\'Statuses with one item\','.$otherStat.'],';

//
//$otherStat = 0;
//$dataJsArrayIm = '[\'Status\',\'Items\'],';
//foreach(InternationalMailStat::model()->with('internationalMailsSTAT')->findAll() AS $item)
//{
//    $no = intval($item->internationalMailsSTAT);
//    if($item->id == InternationalMailStat::NEW_ORDER OR $item->id == InternationalMailStat::CANCELLED OR $item->id == InternationalMailStat::FINISHED OR !$no)
//    {}
//    else if($no == 1)
//    {
//        $otherStat++;
//    }
//    else
//        $dataJsArrayIm .= '[\'('.$item->id.') '.CHtml::encode($item->name).'\', '.$no.'],';
//}
//$dataJsArrayIm .= '[\'Statuses with one item\','.$otherStat.'],';

//
//$otherStat = 0;
//$dataJsArrayPostal = '[\'Status\',\'Items\'],';
//foreach(PostalStat::model()->with('postalsSTAT')->findAll() AS $item)
//{
//    $no = intval($item->postalsSTAT);
//    if($item->id == PostalStat::NEW_ORDER OR $item->id == PostalStat::CANCELLED OR $item->id == PostalStat::FINISHED OR !$no)
//    {}
//    else if($no == 1)
//    {
//        $otherStat++;
//    }
//    else
//        $dataJsArrayPostal .= '[\'('.$item->id.') '.CHtml::encode($item->name).'\', '.$no.'],';
//}
//$dataJsArrayPostal .='[\'Statuses with one item\','.$otherStat.'],';

$dataJsArrayPostal = '[\'Status Map\',\'Items\'],';
foreach(StatMap::mapNameList() AS $key => $item)
{
    $no = intval(StatMap::countPostalsForStatMap($key));
    if($key != StatMap::MAP_DELIVERED)
        $dataJsArrayPostal .= '[\'('.$key.') '.CHtml::encode($item).'\', '.$no.'],';
}
$no = intval(StatMap::countCouriersForStatMap(NULL));
$dataJsArrayPostal .= '[\'(-) Without MAP\', '.$no.'],';
?>


<script type="text/javascript">


</script>

<?php
$googleCharts = "
function drawChart() {

        var chartCourier = new google.visualization.PieChart(document.getElementById('chart-courier'));
        chartCourier.draw(google.visualization.arrayToDataTable([
            ".$dataJsArrayCourier."
        ]), {
            legend: {position: 'bottom'},
            sliceVisibilityThreshold: 0.01,
            chartArea: {left:0,top:0,width:'100%',height:'70%'},
        });
          

        var chartPostal = new google.visualization.PieChart(document.getElementById('chart-postal'));
        chartPostal.draw(google.visualization.arrayToDataTable([
            ".$dataJsArrayPostal."
        ]), {legend: {position: 'bottom'},
            sliceVisibilityThreshold: 0.01,
            chartArea: {left:0,top:0,width:'100%',height:'70%'},
        });
    }

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
";
Yii::app()->clientScript->registerScriptFile('https://www.gstatic.com/charts/loader.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('google-charts', $googleCharts, CClientScript::POS_END);
?>

<table class="hTop table table-condensed" style="margin: 0 auto;">
    <tr style="font-weight: bold;">
        <td>Courier</td>
        <td>Postal</td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;">
            For items with status other than [DELIVERED]:
        </td>
    </tr>
    <tr>
        <td>
            <div id="chart-courier" style="width: 275px; height: 275px;"></div>
            <table class="hLeft table table-condensed">
                <tr class="header">
                    <td style="width: 195px;">Last stat. age</td>
                    <td style="width: 55px;">Items</td>
                </tr>
                <tr>
                    <td>&gt; 7 days</td>
                    <td><?php echo $waiting_couriers['7days'];?></td>
                </tr>
                <tr>
                    <td>&gt; 14 days</td>
                    <td><?php echo $waiting_couriers['14days'];?></td>
                </tr>
                <tr>
                    <td>&gt; 30 days</td>
                    <td><?php echo $waiting_couriers['30days'];?></td>
                </tr>
            </table>
        </td>

        <td>
            <div id="chart-postal" style="width: 275px; height: 275px;"></div>
            <table class="hLeft table table-condensed">
                <tr class="header">
                    <td style="width: 195px;">Last stat. age</td>
                    <td style="width: 55px;">Items</td>
                </tr>
                <tr>
                    <td>&gt; 7 days</td>
                    <td><?php echo $waiting_postal['7days'];?></td>
                </tr>
                <tr>
                    <td>&gt; 14 days</td>
                    <td><?php echo $waiting_postal['14days'];?></td>
                </tr>
                <tr>
                    <td>&gt; 30 days</td>
                    <td><?php echo $waiting_postal['30days'];?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<h2 id="summarySwitch">Summary</h2>
<?php //if($this->beginCache('summary', array('duration'=> 60 * 15))) { ?>
<div id="summary">
    <table class="hTop table table-condensed" style="margin: 0 auto;">
        <tr>
            <td>Courier</td>
            <td>Postal</td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left;">
                <table class="hLeft table table-condensed" style="width: 300px;">
                    <tr>
                        <td style="width: 195px;">Status Map</td>
                        <td style="width: 55px;">Items</td>
                    </tr>
                    <?php
                    /* @var $item CourierStat */
                    foreach(StatMap::mapNameList() AS $key => $item)
                    {
                        $no = StatMap::countCouriersForStatMap($key);
                        echo'<tr>
                <td style="width: 195px;">'.$item.' ['.$key.']</td>
                <td style="width: 55px;">'.$no.'</td>
            </tr>';
                    }
                    ?>
                </table>
            </td>

            <td style="vertical-align: top; text-align: left;">
                <table class="hLeft table table-condensed" style="width: 300px;">
                    <tr>
                        <td style="width: 195px;">Status Map</td>
                        <td style="width: 55px;">Items</td>
                    </tr>
                    <?php

                    foreach(StatMap::mapNameList() AS $key => $item)
                    {
                        $no = StatMap::countPostalsForStatMap($key);
                        echo'<tr>
                <td style="width: 195px;">'.$item.' ['.$key.']</td>
                <td style="width: 55px;">'.$no.'</td>
            </tr>';
                    }

                    ?>
                </table>

            </td>
        </tr>
    </table>
</div>
<?php //$this->endCache(); } ?>
<hr/>
