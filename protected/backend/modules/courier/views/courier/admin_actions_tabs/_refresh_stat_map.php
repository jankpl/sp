
<h4>Refresh stat map</h4>


<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'mass-refresh-stat-map',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/refreshStatMapByGridView'),
));
?>

Ignores "RETURNED" stat map lock
<br/>
<br/>
<?php
echo TbHtml::submitButton('Refresh stat map', array(
    'onClick' => 'js:
    $("#courier-ids-refresh").val($("#courier").selGridView("getAllSelection").toString());
    ;',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => 'courier-ids-refresh')); ?>
<?php
$this->endWidget();
?>