<?php

class GoogleTranslateWidget extends CWidget{

    public $formAttrNames;
    public $button = false;
    public $attrMap = [];

    public static function actions(){
        return array(
            'GoogleTranslateActions'=>'application.backend.components.googleTranslate.GoogleTranslateActions',
        );
    }

    public function run()
    {
        if($this->button)
            $this->render('button',[
            ]);
        else
            $this->render('widget',[
                'formAttrNames' => $this->formAttrNames,
                'attrMap' => $this->attrMap,
            ]);
    }

}