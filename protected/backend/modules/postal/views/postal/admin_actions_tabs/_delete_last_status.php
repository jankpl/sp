
<h4>Delete last status</h4>
<?php
if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER])):
    ?>
    Option available only for administrators
<?php
else:
    ?>

    <?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'mass-delete-stat',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/deleteLastStatusByGridView'),
));
    ?>


    <?php
    echo TbHtml::submitButton('Delete', array(
        'onClick' => 'js:
    $("#postal-ids-statuses").val($("#postal").selGridView("getAllSelection").toString());
    ;',
        'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
    ?>

    <?php echo CHtml::hiddenField('Postal[ids]','', array('id' => 'postal-ids-statuses')); ?>
    <?php
    $this->endWidget();
    ?>
<?php
endif;
