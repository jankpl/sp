<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'update-trakcing-id',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'track_id'); ?>
        <?php echo $form->textField($model, 'track_id', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'track_id'); ?>
    </div><!-- row -->

    <div class="row">
        <?php echo $form->labelEx($model,'source_postal_operator_id'); ?>
        <?php echo $form->dropDownList($model, 'source_postal_operator_id', CHtml::listData(PostalOperator::getOperators(true),'id', 'name')); ?>
        <?php echo $form->error($model,'source_postal_operator_id'); ?>
    </div><!-- row -->


    <?php
    echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING,'Label will be removed from this package');
    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->