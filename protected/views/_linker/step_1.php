<div class="alert alert-info">
    <form method="POST" style="margin: 0 0 -7px 0;" id="linker-import">
        <input type="hidden" id="sa" name="sa" value="<?= $sa;?>"/>
        <input type="hidden" id="sd" name="sd" value="<?= $sd;?>"/>
        <table>
            <tr>
                <td style="vertical-align: top; padding-top: 6px;"><?= Yii::t('courier_linker','Od:');?></td>
                <td><?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'name' => 'date_from',
                        'value' => $date_from,
                        'options' => array(
                            'firstDay' => 1,
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                            'minDate' => $date_begining,      // minimum date
                            'maxDate' => $date_end,      // maximum date
                        ),
                        'htmlOptions' => array(
                            'size' => '5',         // textField size
                            'maxlength' => '10',    // textField maxlength
                            'id' => 'date-from',
                            'style' => 'width: 100px;',
                        ),
                    ));?></td>
                <td style="vertical-align: top; padding-top: 6px; padding-left: 10px;"><?= Yii::t('courier_linker','Do:');?></td>
                <td><?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'name' => 'date_to',
                        'value' => $date_to,
                        'options' => array(
                            'firstDay' => 1,
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                            'minDate' => $date_begining,      // minimum date
                            'maxDate' => $date_end,      // maximum date
                        ),
                        'htmlOptions' => array(
                            'size' => '5',         // textField size
                            'maxlength' => '10',    // textField maxlength
                            'id' => 'date-to',
                            'style' => 'width: 100px;',
                        ),
                    ));?></td>
                <td style="vertical-align: top; padding-top: 6px; padding-left: 50px;"><?= Yii::t('courier_linker','Pobierz:');?></td>
                <td><?= CHtml::dropDownList('statLinker', $statLinker, LinkerOrderItem::getStatShowlinkerList(), ['style' => 'width: 170px;']);?></td>
                <td style="padding-left: 10px;"><?= CHtml::dropDownList('stat', $stat, LinkerOrderItem::getStatShowProcessedList(), ['style' => 'width: 130px;']);?></td>
                <td style="vertical-align: top; width: 300px; text-align: right; padding-top: 3px;"><input type="submit" name="get-data" id="formSubmit" class="btn btn-success" value="<?= Yii::t('courier_linker','Pobierz/odśwież dane');?>"/></td>
            </tr>
        </table>
    </form>
</div>

<?php if($number):?>
    <div class="alert alert-danger data-import-warning" style="display: none;"><?= Yii::t('linker_courier', 'Znaznacz przynajmniej 1 zamówienie');?>. <?= Yii::t('linker_courier', 'Maksymalna liczba importowanych zamówień za jednym razem to: {n}', ['{n}' => LinkerClient::MAX_IMPORT_NUMBER]);?></div>

    <div class="alert alert-success" style="text-align: center;"><?= Yii::t('courier_linker', 'Znaleziono zamówień: {n}', ['{n}' => $number]);?></div>
<?php endif; ?>


<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'import-data',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'stateful'=>false,
    ));
    ?>

    <?php if($number): ?>
        <div class="navigate" style="clear: both;">
            <input type="button" class="btn btn-primary importLinkerData" value="<?= Yii::t('linker_courier', 'Importuj wybrane zamówienia');?>" name="import"/>
        </div>

    <?php
    endif;
    ?>


    <?php
    $countries = [];
    $countries[-1] = '';

    $mainCountry = CountryList::COUNTRY_PL;

    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
        $mainCountry = CountryList::COUNTRY_UK;

    $countries += CHtml::listData(CountryList::model()->with('countryListTr')->findAll(['order' => '(CASE WHEN t.id = '.intval($mainCountry).' THEN 0 ELSE 1 END) ASC, countryListTr.name ASC']),'id', 'trName');

    $countries[-1] = '<'.Yii::t('courier_linker','oprócz').' '.$countries[$mainCountry].'>';
    ?>

    <br/>
    <br/>
    <?= Yii::t('courier_linker', 'Wyświetlanych:');?> <span id="no-filtered"><?= $number;?></span>/<span id="no-total"><?= $number;?></span><br/>
    <?= Yii::t('courier_linker', 'Zaznaczonych:');?> <span id="no-selected">0</span><br/>
    <div style="width: 100%; height: auto; overflow-x: scroll;">

        <table>

            <table class="table table-striped table-hover" id="linker-orders-table" style="table-layout: fixed; word-break: break-all;">
                <colgroup>
                    <col width="20"></col>
                    <col width="75"></col>
                    <col width="100"></col>
                    <col width="100"></col>
                    <col width="80"></col>
                    <col width="200"></col>
                    <col width="90"></col>
                    <col width="80"></col>
                    <col width="80"></col>
                    <col width="110"></col>
                    <col width="70"></col>
                    <col width="120"></col>
                    <col width="80"></col>
                    <col width="50"></col>
                    <col width="80"></col>
                    <col width="80"></col>
                </colgroup>
                <thead>
                <tr style="word-break: normal;">
                    <th>#</th>
                    <th style="text-align: center;"><a href="#" data-sort="id"><?= Yii::t('courier_linker','Number');?> <?= ($sa == 'id' ? '<span class="glyphicon glyphicon-sort-by-alphabet'.($sd ? '-alt' : '').'"></span>' : '');?></a></th>
                    <th style="text-align: center;"><a href="#" data-sort="clientOrderNumber"><?= Yii::t('courier_linker','Client order id');?></a> <?= ($sa == 'clientOrderNumber' ? '<span class="glyphicon glyphicon-sort-by-alphabet'.($sd ? '-alt' : '').'"></span>' : '');?></th>
                    <th style="text-align: center;"><a href="#" data-sort="orderDate"><?= Yii::t('courier_linker','Data');?></a>  <?= ($sa == 'orderDate' ? '<span class="glyphicon glyphicon-sort-by-alphabet'.($sd ? '-alt' : '').'"></span>' : '');?></th>
                    <th  style="text-align: center;"><?= Yii::t('courier_linker','Źrodło');?></th>
                    <th  style="text-align: center;"><?= Yii::t('courier_linker','Produkt');?></th>
                    <th><a href="#" data-sort="deliveryRecipient"><?= Yii::t('courier_linker','Kupujący');?>  <?= ($sa == 'deliveryRecipient' ? '<span class="glyphicon glyphicon-sort-by-alphabet'.($sd ? '-alt' : '').'"></span>' : '');?></th>
                    <th style="text-align: center;"><a href="#" data-sort="deliveryCountry"><?= Yii::t('courier_linker','Kraj');?>  <?= ($sa == 'deliveryCountry' ? '<span class="glyphicon glyphicon-sort-by-alphabet'.($sd ? '-alt' : '').'"></span>' : '');?></th>
                    <th style="text-align: center;"><?= Yii::t('courier_linker','Liczba pozycji');?></th>
                    <th><?= Yii::t('courier_linker','Zawartość');?></th>
                    <th><a href="#" data-sort="cod"><?= Yii::t('courier_linker','COD');?></a>  <?= ($sa == 'cod' ? '<span class="glyphicon glyphicon-sort-by-alphabet'.($sd ? '-alt' : '').'"></span>' : '');?></th>
                    <th><a href="#" data-sort="priceGross"><?= Yii::t('courier_linker','Wartość');?></a>  <?= ($sa == 'priceGross' ? '<span class="glyphicon glyphicon-sort-by-alphabet'.($sd ? '-alt' : '').'"></span>' : '');?></th>
                    <th><?= Yii::t('courier_linker','Operator');?></th>
                    <th><?= Yii::t('courier_linker','Sent');?></th>
                    <th style="text-align: center;"><?= Yii::t('courier_linker','Package');?></th>
                    <th style="text-align: right;"><?= Yii::t('courier_linker','Akcja');?></th>
                </tr>
                <tr style="word-break: normal;">
                    <th></th>
                    <th></th>
                    <th class="text-center"><?= Chtml::textField('_order_id_filter', '', ['id' => 'order-id-filter', 'style' => 'width: 100px;']);?></th>
                    <th class="text-center"><?= Chtml::textField('_date', '', ['id' => 'date-selector', 'style' => 'width: 70px;']);?></th>
                    <th class="text-center"><?= Chtml::textField('_source', '', ['id' => 'source-selector', 'style' => 'width: 70px;']);?></th>
                    <th><?= Chtml::dropDownList('_product_filter_mod', '', [
                            '1' => Yii::t('courier_linker','Inc.'),
                            '2' => Yii::t('courier_linker','Ex.'),

                        ], ['id' => 'product-filter-mod', 'style' => 'width: 50px;']);?>

                        <?= Chtml::dropDownList('_product_filter', '', [
                            'Award Bundle' => 'Award Bundle',
                            'Gift Card' => 'Gift Card',
                            'Issue' => 'Issue',
                            'Merch' => 'Merch',
                            'PREORDER' => 'PREORDER',
                            'Record' => 'Record',
                        ], ['id' => 'product-filter', 'prompt' => '-', 'style' => 'width: 110px;']);?>
                       </th>
                    <th></th>
                    <th class="text-center"><?= Chtml::dropDownList('_country_selector', '', $countries, ['id' => 'country-selector', 'prompt' => '-', 'style' => 'width: 100%;', 'data-main-country' => $mainCountry]);?></th>
                    <th class="text-center"><?= Chtml::textField('_order_lines', '', ['id' => 'order-lines-selector', 'style' => 'width: 50px;']);?></th>
                    <th><?= Chtml::textField('_content_filter', '', ['id' => 'content-filter', 'style' => 'width: 100px;']);?></th>
                    <th></th>
                    <th><?= Chtml::dropDownList('_value', '', [1 => Yii::t('courier_linker','>'), 2 => Yii::t('courier_linker','<=')], ['id' => 'value-selector', 'prompt' => '-', 'style' => 'width: 60px;']);?><?= Chtml::numberField('_value_value', 15, ['id' => 'value-value', 'style' => 'width: 60px;', 'min' => 1, 'step' => 0.01]);?></th>
                    <th></th>
                    <th></th>
                    <th style="text-align: center; font-weight: normal; font-size: 10px;"><?= Yii::t('courier_linker','jeżeli już przetworzono');?></th>
                    <th style="text-align: right;  font-weight: normal; font-size: 10px;">[<a href="#" data-inverse-selection="true"><?= Yii::t('courier_linker','zaznacz/odznacz wszystkie');?></a>]</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if(!$number):
                    ?>
                    <tr>
                        <td colspan="15" style="text-align: center;"><?= Yii::t('courier_linker', '-- brak zamówień --');?></td>
                    </tr>
                <?php
                else:
                    $i = 1;
                    /* @var $order LinkerOrderItem */
                    foreach($orders AS $orderKey => $order):
                        ?>
                        <tr data-country-id="<?= $order->addr_country_id; ?>" data-order-id="<?= $order->clientOrderId; ?>" data-order-value="<?= $order->priceGross;?>" data-order-lines="<?= $order->orderLines;?>" data-content="<?= strtolower($order->content);?>" data-product="<?= strtolower($order->product_type);?>" data-date="<?= $order->createdTime;?>" data-source="<?= strtolower($order->origin);?>">
                            <td><?= $i++;?></td>
                            <td style="vertical-align: middle; font-weight: bold;"><?= $order->number;?></td>
                            <td style="vertical-align: middle; font-weight: bold; text-align: center;"><?= $order->clientOrderId;?></td>
                            <td style="vertical-align: middle; text-align: center;">
                                <?= (str_replace(' ', '<br/>', $order->createdTime));?>
                            </td>
                            <td style="vertical-align: middle; font-weight: bold;"><?= $order->origin;?></td>
                            <td style="vertical-align: middle; font-weight: bold;"><?= $order->product_type;?></td>
                            <td style="font-size: 10px;">
                                <strong><?= $order->addr_name;?></strong><br/>
                                <?= $order->addr_street;?><br/>
                                <?= $order->addr_zip_code;?>, <?= $order->addr_city;?>, <?= $order->addr_country;?><br/>
                                <?= Yii::t('courier_linker', 'tel.');?> <?= $order->addr_tel;?>, <?= $order->addr_email;?>
                            </td>
                            <td style="vertical-align: middle; text-align: center;">
                                <?= $order->addr_country; ?>
                            </td>
                            <td style="vertical-align: middle; text-align: center;"><?= $order->orderLines;?></td>
                            <td style="vertical-align: middle;"><?= $order->content;?></td>
                            <td style="vertical-align: middle;"><?= $order->cod ? $order->cod : '-';?> <?= $order->currencySymbol;?></td>
                            <td style="vertical-align: middle"><?= $order->priceGross;?> <?= $order->currencySymbol;?></td>
                            <td style="vertical-align: middle"><?= $order->carrier;?></td>
                            <td style="vertical-align: middle"><?= $order->stat == 'Y' ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove"></span>';?></td>

                            <td style="vertical-align: middle; text-align: center;">
                                <?php if($order->local_package_id):?>
                                    <a class="btn" href="<?= Yii::app()->createUrl($order->local_package_target == LinkerImport::TARGET_COURIER ?  '/courier/courier/view' : '/postal/postal/view', ['urlData' => $order->local_package_hash]);?>" target="_blank"><?= Yii::t('courier_linker', 'Pokaż');?></a>
                                <?php else:?>
                                    -
                                <?php endif;?>
                            </td>
                            <td style="vertical-align: middle; text-align: right;">
                                <?php //if($order->statProcessed == LinkerOrderItem::STAT_NOT_PROCESSED):?>
                                <?php if(1 OR ($order->statShipping == LinkerOrderItem::STAT_NOT_SHIPPED && $order->statPayment == LinkerOrderItem::STAT_PAID)): ?>
                                    <input data-import-linker="true" type="checkbox" name="import[<?= $orderKey;?>]">
                                <?php else: ?>
                                    <input type="button" class="btn btn-xs btn-warning" value="<?= Yii::t('courier_linker', 'Wymuś');?>" data-unlock-id="<?= $orderKey;?>"/>
                                    <input data-import-linker="true" data-import-id="<?= $orderKey;?>" type="checkbox" name="import[<?= $orderKey;?>]" disabled="disabled" title="<?= Yii::t('courier_linker', 'To zamówienie spełnia wszyskich wymogów do wysyłki (nie zostało opłacone lub zostało już wysłane), ale możesz wymusić jego przetworzenie.');?>">
                                <?php endif;?>
                                <?php //endif; ?>
                            </td>
                        </tr>
                    <?php endforeach;
                endif;
                ?>
                </tbody>
            </table>

    </div>

    <?php if($more AND 0): ?>
        <div class="alert alert-warning"><?= Yii::t('linker_courier', 'Nie pobrano wszyskich zamówień - doprecyzuj filtrację lub przetwórz powyższe zamówienia, aby zobaczyć kolejne.');?></div>
    <?php endif; ?>

    <?php if($number): ?>
        <div class="alert alert-danger data-import-warning" style="display: none;"><?= Yii::t('linker_courier', 'Znaznacz przynajmniej 1 zamówienie');?>. <?= Yii::t('linker_courier', 'Maksymalna liczba importowanych zamówień za jednym razem to: {n}', ['{n}' => LinkerClient::MAX_IMPORT_NUMBER]);?></div>

        <div class="alert alert-warning"><?= Yii::t('linker_courier', 'Poprawne wygenerowanie etykiety dla zamówienie spowoduje automatyczne przekazanie do linker numeru paczki oraz zmianę statusu zamówienia na "wysłane".');?></div>

        <div class="navigate" style="clear: both;">
            <input type="button" class="btn btn-primary importLinkerData" value="<?= Yii::t('linker_courier', 'Importuj wybrane zamówienia');?>" name="import"/>
        </div>



        <script>
            function applyFilters()
            {

                var $rows = $('#linker-orders-table').find('tbody').find('tr');
                $rows.hide();

                var selected_country = $('#country-selector').val();

                var main_country = $('[data-main-country]').attr('data-main-country');

                var provided_content = $('#content-filter').val();

                var provided_product = $('#product-filter').val();

                var provided_product_mod = $('#product-filter-mod').val();

                var provided_order_id = $('#order-id-filter').val();

                var selected_value = $('#value-selector').val();

                var selected_value_value = $('#value-value').val();

                var provided_order_lines = $('#order-lines-selector').val();

                var provided_date = $('#date-selector').val();

                var provided_source = $('#source-selector').val();


                var $visibleRows = $rows.filter(function (i, v) {
                    if(provided_content == '')
                        return true;


                    var $t = $(this);

                    var content = String($t.attr('data-content'));
                    provided_content = String(provided_content);
                    provided_content = provided_content.toLowerCase();

                    if(content.indexOf(provided_content) >= 0)
                        return true;
                    else
                        return false;
                })
                    .filter(function (i, v) {
                        if(provided_product == '')
                            return true;


                        var $t = $(this);

                        var product = String($t.attr('data-product'));
                        provided_product = String(provided_product);
                        provided_product = provided_product.toLowerCase();

                        if(provided_product_mod == 2)
                        {
                            if(product.indexOf(provided_product) >= 0)
                                return false;
                            else
                                return true;
                        } else {
                            if(product.indexOf(provided_product) >= 0)
                                return true;
                            else
                                return false;
                        }
                    })
                    .filter(function (i, v) {

                        if(provided_order_id == '')
                            return true;

                        var $t = $(this);

                        if($t.attr('data-order-id') == provided_order_id)
                            return true;
                        else
                            return false;
                    })
                    .filter(function (i, v) {

                        if(provided_date == '')
                            return true;

                        var $t = $(this);

                        var date = String($t.attr('data-date'));
                        provided_date = String(provided_date);
                        provided_date = provided_date.toLowerCase();

                            if(date.indexOf(provided_date) >= 0)
                                return true;
                            else
                                return false;

                    })
                    .filter(function (i, v) {

                        if(provided_source == '')
                            return true;

                        var $t = $(this);

                        var source = String($t.attr('data-source'));
                        provided_source = String(provided_source);
                        provided_source = provided_source.toLowerCase();


                        if(source.indexOf(provided_source) >= 0)
                            return true;
                        else
                            return false;
                    })
                    .filter(function (i, v) {
                        var $t = $(this);

                        if(selected_value == '' || selected_value_value == '')
                            return true;

                        if(selected_value == 1)
                        {

                            if ( parseFloat($t.attr('data-order-value')) > selected_value_value)
                                return true;
                            else
                                return false;

                        } else {

                            if (parseFloat($t.attr('data-order-value')) <= selected_value_value)
                                return true;
                            else
                                return false;

                        }
                    })
                    .filter(function (i, v) {
                        if(provided_order_lines == '')
                            return true;

                        var $t = $(this);

                        provided_order_lines = parseInt(provided_order_lines);

                        if ( parseInt($t.attr('data-order-lines')) == provided_order_lines)
                            return true;
                        else
                            return false;

                    })
                    .filter(function (i, v) {
                        if(selected_country == '')
                            return true;


                        var $t = $(this);


                        if(selected_country == -1)
                        {
                            if ($t.attr('data-country-id') != main_country)
                                return true;
                            else
                                return false;

                        } else {

                            if ($t.attr('data-country-id') == selected_country)
                                return true;
                            else
                                return false;

                        }
                    });

                $visibleRows.show();
                $('#no-filtered').html($visibleRows.length);
            }


            var $warning = $(".data-import-warning");

            $('[data-import-linker]').on('change', function(){
                $warning.hide();
            });

            $(".importLinkerData").on('click', function(){

                var number = $('[data-import-linker]:checked').length;

                if(!number || number > <?= LinkerClient::MAX_IMPORT_NUMBER;?>)
                    $warning.show();
                else
                {
                    $(this).closest('form').submit();
                }
            });


            $('[data-inverse-selection]').on('click', function(){

                var startNumber = countChecked();
                var setToCheck = false;

                $('[data-import-linker]').not(':disabled').not(':hidden').each(function() {

                    if(!setToCheck)
                    {
                        if($(this).is(':checked'))
                            setToCheck = 1;
                        else
                            setToCheck = 2;
                    }

                    if(setToCheck == 1)
                    {
                        $(this).prop('checked', false);

                    } else {

                        if($(this).not(':checked')) {
                            if (startNumber < 500) {
                                $(this).prop('checked', true);
                                startNumber++;
                            }

                        }
                    }

                });

                displayNoOfCheked();
            });

            function displayNoOfCheked()
            {
                $('#no-selected').html(countChecked());
            }

            $('[data-import-linker]').on('change', function(){
                displayNoOfCheked();
            });

            function countChecked()
            {
                return parseInt($('[data-import-linker]:checked').length);
            }

            $('[data-unlock-id]').on('click', function(){
                var id = $(this).attr('data-unlock-id');
                $('[data-import-id=' + id + ']').attr('disabled', false);
                $('[data-import-id=' + id + ']').prop('checked', true);
                $(this).remove();
            });


            $('#value-selector').on('change', function(){

                applyFilters();
            });

            $('#value-value').on('change', function(){

                applyFilters();
            });

            $('#date-selector').on('change', function(){

                applyFilters();

            });

            $('#source-selector').on('change', function(){

                applyFilters();

            });



            $('#order-lines-selector').on('change', function(){

                applyFilters();

            });


            $('#country-selector').on('change', function(){

                applyFilters();

            });


            $('#order-id-filter').on('change', function(){

                applyFilters();

            });

            $('#order-id-filter').keypress(function(event) {
                if (event.keyCode == 13) {
                    $(this).trigger('change');
                    event.preventDefault();
                    return false;
                }
            });

            $('#content-filter').on('change', function(){

                applyFilters();

            });

            $('#product-filter').on('change', function(){

                applyFilters();

            });


            $('#product-filter-mod').on('change', function(){

                applyFilters();

            });

            $('#content-filter').keypress(function(event) {
                if (event.keyCode == 13) {
                    $(this).trigger('change');
                    event.preventDefault();
                    return false;
                }
            });


            $('[data-sort]').on('click', function(){

                var current_attr = $('#sa').val();
                var new_attr = $(this).attr('data-sort');


                if(current_attr == new_attr) {

                    var current_sort = parseInt($('#sd').val());

                    if(current_sort)
                        $('#sd').val('0');
                    else
                        $('#sd').val('1');

                }
                else
                    $('#sa').val($(this).attr('data-sort'));

                $('#formSubmit').trigger('click');


            });
        </script>
    <?php endif; ?>
    <?php $this->endWidget(); ?>

</div>