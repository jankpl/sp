<?php

Yii::import('application.models._base.BaseOnlinePayments');

class OnlinePayments extends BaseOnlinePayments
{

    const STAT_UNPAID = 0;
    const STAT_PAID = 1;
    const STAT_ERROR = 9;


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function makePaid()
    {
        $this->order->changeStat(OrderStat::FINISHED);
        $this->stat = self::STAT_PAID;
        return $this->update(array('stat'));
    }

    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => $this->amount.$this->currency.microtime().'sdacTEWwer'));

        }

        return parent::beforeSave();
    }

    public function rules() {
        return array(


            array('err_p24_desc','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

            array('amount, currency, order_id', 'required'),
            array('amount', 'numerical'),
            array('p24_order_id, order_id', 'numerical', 'integerOnly' => true),
            array('token', 'length', 'max'=>64),
            array('err_p24', 'length', 'max'=>16),
            array('err_p24_desc', 'length', 'max'=>256),
            array('err_p24, err_p24_desc', 'default', 'setOnEmpty' => true, 'value' => null),
            array('stat', 'default', 'setOnEmpty' => true, 'value' => 0),
            array('id, date_entered, token, p24_order_id, err_p24, err_p24_desc, amount, currency, stat

            ', 'safe', 'on'=>'search'),
        );
    }
}