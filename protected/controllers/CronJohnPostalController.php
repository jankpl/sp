<?php

class CronJohnPostalController extends CController {

    const PACKAGE_DAYS_IGNORE = 45;
    const STRIP_SIZE = 80;

    const DELAY_NEW_MINUTES = 180;

    // TODO: MOVE TO tt_log CLASS
    const TTLOG_BLOCK_MINUTES = 540;

    const STRIP_SIZE_EXPIRE_LABELS = 500;
    const STRIP_SIZE_ARCHIVE_HISTORY = 5000;

    const INSTANCES = 10;

    const MAX_STEPS = 8;

    public function init()
    {
        Yii::app()->getModule('postal');
        parent::init();
    }


    protected function getIngnoredStats()
    {
        $_ingoredStats = [
            PostalStat::CANCELLED,
            PostalStat::CANCELLED_PROBLEM,
            PostalStat::NEW_ORDER,
            PostalStat::PACKAGE_PROCESSING
        ];

        $data = array_merge($_ingoredStats, PostalStat::getKnownDeliveredStatuses());
        return $data;
    }

    public function filters()
    {
        return array();
    }


    function actionRun()
    {

        $uniqId = uniqid();

        $multiCurl = [];
        $result = [];

        $mh = curl_multi_init();
        for($i = 1; $i <= self::INSTANCES; $i++) {

//            $cookieFile = Yii::app()->basePath.'/_temp/cronJohnPostalRun_cookie_'.$i.'_'.$uniqId.'.txt';
            $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohnPostal/index', ['mod' => $i]);

            $multiCurl[$i] = curl_init();
            curl_setopt($multiCurl[$i], CURLOPT_URL,$fetchURL);
            curl_setopt($multiCurl[$i], CURLOPT_HEADER,0);
            curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER,1);
            curl_setopt($multiCurl[$i], CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($multiCurl[$i], CURLOPT_MAXREDIRS, 10);
//            curl_setopt($multiCurl[$i], CURLOPT_COOKIEJAR, $cookieFile);
//            curl_setopt($multiCurl[$i], CURLOPT_COOKIEFILE, $cookieFile);
            curl_multi_add_handle($mh, $multiCurl[$i]);
        }
        $index = null;
        do {
            curl_multi_exec($mh,$index);
        } while($index > 0);

        foreach($multiCurl as $k => $ch) {
            $result[$k] = curl_multi_getcontent($ch);
            curl_multi_remove_handle($mh, $ch);
        }

        curl_multi_close($mh);

//
//
//        for($i = 1; $i <= self::INSTANCES; $i++) {
//            $cookieFile = Yii::app()->basePath.'/_temp/cronJohnPostalRun_cookie_'.$i.'.txt';
//
//            sleep(5);
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, Yii::app()->createAbsoluteUrl('/cronJohnPostal/index', ['mod' => $i,]));
//            curl_setopt($ch, CURLOPT_HEADER, 0);
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//            curl_setopt($ch, CURLOPT_MAXREDIRS, 100);
//            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
//            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
//            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
//            curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
////            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_exec($ch);
//            curl_close($ch);
//        }
    }



    // update courier packages statuses with external statuses
    public function actionIndex($mod = 0, $set = 0, $ssid = NULL)
    {
        sleep(2 * $mod);

        if(date('G') > 23 OR date('G') < 5)
            exit;

        if(!$mod)
            exit;

        $start = date('Y-m-d H:i:s');

//        switch($mod)
//        {
//            case 0:
//            default:
//                $modParams = '0,1,2,3,4,5,6,7,8,9,10,11';
//                break;
//            case 1:
//                $modParams = '0,1';
//                break;
//            case 2:
//                $modParams = '2,3';
//                break;
//            case 3:
//                $modParams = '4,5';
//                break;
//            case 4:
//                $modParams = '6,7';
//                break;
//            case 5:
//                $modParams = '8,9';
//                break;
//            case 6:
//                $modParams = '10,11';
//                break;
//        }

        if($mod == 0)
            $modParams = implode(',', range(0,self::INSTANCES));
        else
            $modParams = $mod - 1;

        $CACHE_NAME = 'cronJohnPostalRun_'.$mod;
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== $ssid) {
            MyDump::dump('cjplog.txt', "\r\n".$start." : AIP[$mod]");
            Yii::app()->end();
        }

        if(S_SystemLoad::isHigh())
        {

            MyDump::dump('cjplog.txt', "\r\n".$start." : [$mod] : HIGH LOAD");
            Yii::app()->end();
        }

        if($ssid == '')
            $ssid = Yii::app()->session->sessionID;

        Yii::app()->cache->set($CACHE_NAME, $ssid, 60 * 15);

        if($mod == 1) {
            // MARK AS DELIVERED OLD POSTALS - run only once every 3 hours
            $CACHE_NAME = 'cronJohnPostalRun_MarkOldAsDelivered';
            if (Yii::app()->cache->get($CACHE_NAME) === false) {
                Postal::automaticDevlieryStat();
                Yii::app()->cache->set($CACHE_NAME, 10, 10800);
                Yii::app()->end();
            }
            //////////////////
            ///
            ///
            ///
            ///
            ///
            ///
            ///
            if($mod == 1)
                self::tt4CancelledPackages();
        }



        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);

        $ignoredStats = implode(',', $this->getIngnoredStats());

        $postals = Postal::model()->with('postalLabel')
            ->findAllBySql("SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (postal.date_updated > (NOW() - INTERVAL :past_date DAY) OR postal.date_entered > (NOW() - INTERVAL :past_date DAY))
                AND (postal.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND postal_stat_id NOT IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
AND stat_map_id != :stat_map AND stat_map_id != :stat_map2
AND (postal.id MOD ".self::INSTANCES.") IN ($modParams)
ORDER BY tt_log.id ASC
LIMIT ".self::STRIP_SIZE
                ,
                [
                    ':past_date' => self::PACKAGE_DAYS_IGNORE,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':type' => TtLog::TYPE_POSTAL,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':stat_map' => StatMap::MAP_DELIVERED,
                    ':stat_map2' => StatMap::MAP_CANCELLED,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);

        $log = $start.' ['.print_r($mod,1).'] : '.sprintf("%02d",$set)." --- ". sprintf("%03d",sizeof($postals))." --- ";

        /* $item Postal */
        if(is_array($postals))
            foreach($postals AS $item)
            {
                PostalExternalManager::updateStatusForPackage($item);
            }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(sizeof($postals) == self::STRIP_SIZE)
        {
            if($set < self::MAX_STEPS)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohnPostal/index', array('set' => $set, 'mod' => $mod, 'ssid' => $ssid));

                MyDump::dump('cjplog.txt', $log);

                header("Location: ".$path);
                exit;
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";
                Yii::app()->cache->delete($CACHE_NAME);

                MyDump::dump('cjplog.txt', $log);
                exit;
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";
            Yii::app()->cache->delete($CACHE_NAME);

            MyDump::dump('cjplog.txt', $log);
            exit;
        }


    }


    protected function tt4CancelledPackages()
    {
        $mod = 'cancelled';

        $start = date('Y-m-d H:i:s');

        $cancelledStats = implode(',', [
            PostalStat::CANCELLED,
            PostalStat::CANCELLED_PROBLEM,
            PostalStat::CANCELLED_USER
        ]);

        $postals = Postal::model()->with('postalLabel')
            ->findAllBySql("SELECT DISTINCT postal.* FROM postal
                    LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
                    WHERE (postal.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
                    AND postal_stat_id IN ($cancelledStats)
                    AND
                    (
                        (postal.date_updated > (NOW() - INTERVAL :past_date DAY) OR postal.date_entered > (NOW() - INTERVAL :past_date DAY))
                        AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
                    )
                    AND (user_cancel != :user_tt_found_flag)
                    ORDER BY tt_log.id ASC
                    LIMIT ".self::STRIP_SIZE
                ,
                [
                    ':delay_new' => 60,
                    ':type' => TtLog::TYPE_POSTAL,
                    ':past_date' => 31,
                    ':ttLog_block' => 720,
                    ':user_tt_found_flag' => Postal::USER_CANCELL_REQUESTED_FOUND_TT,
//                  ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                ]);

        $set = 0;
        $log = $start.' ['.print_r($mod,1).'] : '.sprintf("%02d",$set)." --- ". sprintf("%03d",sizeof($postals))." --- ";

        /* $item Postal */
        if(is_array($postals))
            foreach($postals AS $item)
            {
                PostalExternalManager::updateStatusForPackage($item);
            }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        $log .= " --- ".sprintf("%04d",$differenceInSeconds)." ---";
        MyDump::dump('cjlog_postal_cancell.txt', $log);

    }


    /**
     * Action expires old labels
     */
    public function actionArchiveHistoryStatus($set = 0)
    {

        $start = date('Y-m-d H:i:s');

        $CACHE_NAME = 'cronJohnPostalArchiveHistoryStatusRun';
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== Yii::app()->session->sessionID) {
            MyDump::dump('cjpahslog.txt', $start." : AIP");
            Yii::app()->end();
        }
        Yii::app()->cache->set($CACHE_NAME, Yii::app()->session->sessionID, 60 * 15);

        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);


        // OLD HISTORY:
        $toArchive = Postal::model()->findAll('(date_updated IS NOT NULL AND date_updated < (NOW() - INTERVAL :days DAY) OR (date_updated IS NULL AND date_entered < (NOW() - INTERVAL :days DAY))) AND ((archive != :archive AND archive != :archive2) OR archive IS NULL) LIMIT :limit', [
            ':days' => PostalStatHistory::EXPIRATION_DAYS,
            ':archive' => Postal::ARCHIVED_HISTORY,
            ':archive2' => (Postal::ARCHIVED_HISTORY + Postal::ARCHIVED_LABEL_EXPIRED),
            ':limit' => self::STRIP_SIZE_ARCHIVE_HISTORY,
        ]);

        /* @var $item Postal */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {
                PostalStatHistory::expireOldHistory($item);
            }


        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        $log = '';

        // there is propably more
        if(sizeof($toArchive) == self::STRIP_SIZE_ARCHIVE_HISTORY)
        {
            if($set < 10)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohnPostal/archiveHistoryStatus', array('set' => $set ));

                MyDump::dump('cjpahslog.txt', $log);
                header("Location: ".$path);
                exit;
            } else {
                $log .=" --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";

                Yii::app()->cache->delete($CACHE_NAME);

                MyDump::dump('cjpahslog.txt', $log);
                exit;
            }
        } else {
           $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";
            // run only on first round deleting old stats
            if(!$set)
            {
                // UNUSED STATS:
                PostalStat::expireUnusedOldStats();
                $log .=  "\r\nEOS";
            }


            MyDump::dump('cjpahslog.txt', $log);
            Yii::app()->cache->delete($CACHE_NAME);
            exit;
        }



    }


    /**
     * Action expires old labels
     */
    public function actionArchiveLabels($set = 0)
    {
        $start = date('Y-m-d H:i:s');

        $CACHE_NAME = 'cronJohnPostalArchiveLabelsRun';
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== Yii::app()->session->sessionID) {
            MyDump::dump('cjpallog.txt', $start." : AIP");
            Yii::app()->end();
        }
        Yii::app()->cache->set($CACHE_NAME, Yii::app()->session->sessionID, 60 * 15);

        $set = intval($set);

        Yii::app()->getModule('postal');
        $toArchive = PostalLabel::model()->findAll('date_updated < (NOW() - INTERVAL :days DAY) AND stat = :stat LIMIT :limit', [
            ':days' => PostalLabel::EXPIRATION_DAYS,
            ':stat' => PostalLabel::STAT_SUCCESS,
            ':limit' => self::STRIP_SIZE_EXPIRE_LABELS
        ]);

        $log = $start.' : '.sprintf("%02d",$set)." --- ". sprintf("%03d",sizeof($toArchive))." --- ";

        /* @var $item PostalLabel */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {
                $item->expireLabel();
            }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(sizeof($toArchive) == self::STRIP_SIZE_EXPIRE_LABELS)
        {
            if($set < 10)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohnPostal/archiveLabels', array('set' => $set ));

                MyDump::dump('cjpallog.txt', $log);

                header("Location: ".$path);
                exit;
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";
                Yii::app()->cache->delete($CACHE_NAME);

                MyDump::dump('cjpallog.txt', $log);
                exit;
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";
            Yii::app()->cache->delete($CACHE_NAME);

            MyDump::dump('cjpallog.txt', $log);
            exit;
        }

    }






}