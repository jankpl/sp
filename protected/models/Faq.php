<?php

Yii::import('application.models._base.BaseFaq');

class Faq extends BaseFaq
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function beforeSave()
    {

        if($this->isNewRecord)
        {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('MAX(pos)');
            $cmd->from('faq');
            $maxPos = $cmd->queryScalar();
            $maxPos += 1;

            $this->pos = $maxPos;

            $this->isNewRecord = false;
        }

        return parent::beforeSave();
    }

    public function isFirst()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id');
        $cmd->from('faq');
        $cmd->order('pos ASC');
        $cmd->limit('1');
        $first = $cmd->queryScalar();

        if($first == $this->id)
            return true;
        else
            return false;
    }

    public function isLast()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id');
        $cmd->from('faq');
        $cmd->order('pos DESC');
        $cmd->limit('1');
        $last = $cmd->queryScalar();

        if($last == $this->id)
            return true;
        else
            return false;
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),

            array('stat', 'default',
                'value'=>S_Status::ACTIVE),


            array('name', 'required'),
            array('pos, stat', 'numerical', 'integerOnly'=>true),
            array('name, author', 'length', 'max'=>45),
            array('date_updated', 'safe'),
            array('date_updated, author', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, pos, stat, author', 'safe', 'on'=>'search'),
        );
    }

    public function moveT($ownTransaction = true)
    {

        if($this->isFirst())
            return false;

        $curPos = $this->pos;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("pos");
        $cmd->from('faq');
        $cmd->where('pos<:curPos');
        $cmd->params = array(':curPos' => $curPos);
        $cmd->order('pos DESC');
        $cmd->limit(1);
        $newPos = $cmd->queryScalar();

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $cmd = Yii::app()->db->createCommand();
        if(!$cmd->update('faq',
            array('pos' => $curPos),
            'pos=:newPos',
            array(':newPos' => $newPos)
        ))
            $errors = true;

        $this->pos = $newPos;

        if(!$this->save(false))
            $errors = false;

        if(!$errors)
        {
            if($ownTransaction)
                $transaction->commit();
            return true;
        }
        else
        {
            if($ownTransaction)
                $transaction->rollBack();
            return false;
        }
    }

    public function moveB($ownTransaction = true)
    {

        if($this->isLast())
            return false;

        $curPos = $this->pos;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("pos");
        $cmd->from('faq');
        $cmd->where('pos>:curPos');
        $cmd->params = array(':curPos' => $curPos);
        $cmd->order('pos ASC');
        $cmd->limit(1);
        $newPos = $cmd->queryScalar();

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $cmd = Yii::app()->db->createCommand();
        if(!$cmd->update('faq',
            array('pos' => $curPos),
            'pos=:newPos',
            array(':newPos' => $newPos)
        ))
            $errors = true;

        $this->pos = $newPos;

        if(!$this->save(false))
            $errors = false;

        if(!$errors)
        {
            if($ownTransaction)
                $transaction->commit();
            return true;
        }
        else
        {
            if($ownTransaction)
                $transaction->rollBack();
            return false;
        }
    }

    public function toggleStat()
    {
        $this->stat = abs($this->stat - 1);
        return $this->save();
    }
}