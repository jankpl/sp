<ul class="nav pull-right">
    <li class="divider-vertical"></li>
    <li class="dropdown" role="menuitem">
        <a class="dropdown-toggle" data-toggle="dropdown" href=""><?php echo Yii::t('site','Witaj {login}.', array('{login}' => '<span class="bold">'.Yii::app()->user->name.'</span>'));?> <b class="caret"></b></a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
            <li role="menuitem"><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/user/panel');?>"><?php echo Yii::t('site','Panel zarządzania');?></a></li>
            <li role="menuitem"><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/user/update');?>"><?php echo Yii::t('site','Ustawienia konta');?></a></li>
            <li role="menuitem"><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/userMessage/');?>"><?php echo Yii::t('site','Twoje wiadomości ({no})', array('{no}' => UserMessage::getNoOfNetMessagesForUser(Yii::app()->user->id)));?></a></li>
            <li role="menuitem"><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/userContactBook/');?>"><?php echo Yii::t('user','Książka adresowa');?></a></li>
            <li role="menuitem"><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/userBalance/');?>"><?php echo Yii::t('user','Bilans');?></a></li>
            <li role="menuitem"><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/order/');?>"><?php echo Yii::t('user','Zamówienia');?></a></li>
            <li class="divider"></li>
            <li><?php echo CHtml::link(Yii::t('site','wyloguj się!'), array('/site/logout')); ?></li>
        </ul>
    </li>
</ul>