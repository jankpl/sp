<?php
/* @var $id_prefix string */
?>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'export-multi',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/massExportByGridView'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('postal', 'Exportuj do .xls'), array(
    'onClick' => 'js:

    if(!confirm("'.Yii::t("site", "Czy jesteś pewien?").'"))
         return false;

    $("#'.$id_prefix.'postal-ids-export-multi").val($("#postalgrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => $id_prefix.'postal-ids-export-multi')); ?>
<?php
$this->endWidget();
?>

