<?php

class AutentiController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
//                'actions'=> array('index'),
            ),
//            array('allow',
//                'users'=>array('@'),
//                'expression'=>'0 >= Yii::app()->user->authority',
//            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionCheckWaitingAgreements()
    {
        $models = Autenti::model()->findAllByAttributes(['status' => 'NEW']);

        /* @var $models Autenti[] */
        foreach($models AS $model)
        {
            $result = AutentiClient::checkAgreementStatus($model->autenti_document_id);
            if($result->status == 'ACCEPTED')
            {
                foreach($result->files AS $file)
                {
                    if($file->file_type == 'DOCUMENT_SIGNED') {

                        $fileString = AutentiClient::downloadFileByUrl($file->href);

                        if ($fileString)
                            UserDocuments::createByFileString($fileString, $file->filename, $model->user_id, $file->filename, UserDocuments::TYPE_CONTRACT);
                        else
                            throw new Exception('Error downloading file from Autenti');
                    }
                }

                $model->updateStatus($result->status);

            } else if($result->status != 'NEW') {
                $model->updateStatus($result->status);
            }
        }

        $this->redirect(['/autenti']);
    }

    public function actionIndex()
    {
        $model = new Autenti('search');
        $model->unsetAttributes();

        if (isset($_GET['UserLog']))
            $model->setAttributes($_GET['Autenti']);

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionCreate($user_id) {

        $model = new CreateForm();

        $user = User::model()->findByPk($user_id);
        if($user === NULL)
            throw new CHttpException(404);

        $model->initByUser($user);

        if(isset($_POST['CreateForm']) && sizeof($_POST['CreateForm']))
        {
            try {

                $model->attributes = $_POST['CreateForm'];


                if(isset($_POST['preview']))
                    $model->setScenario('preview');

                if ($model->validate()) {
//                    $file = WordGenerator::generate($model);
//
//                    if(!is_file($file))
//                        throw new Exception('Error generating file from template!');

                    if(isset($_POST['preview']))
                    {
                        Yii::app()->request->sendFile('agreement.pdf', file_get_contents($file));
                        Yii::app()->end();
                    }


                    $documents = [];
                    $documents[] = curl_file_create($file, 'application/pdf', 'umowa.pdf');

                    if (isset($_FILES['files']) && sizeof($_FILES['files'])) {
                        foreach ($_FILES['files']['name'] AS $key => $item) {

                            if($item == '')
                                continue;

                            if (!is_file($_FILES['files']['tmp_name'][$key]))
                                throw new Exception('Error uploading file!');

                            $documents[] = curl_file_create($_FILES['files']['tmp_name'][$key], 'application/pdf', basename($item));
                        }
                    }

                    $autenti_id = AutentiClient::createNewAgreement($documents, 'Umowa nr ' . $model->no, 'Testowa', $model->receiver_email, $model->receiver_name, $model->receiver_tel, $model->receiver_surname, $model->company, $model->nip, $model->receiver_country);

                    @unlink($file);

                    if(!$autenti_id)
                    {
                        Yii::app()->user->setFlash('error', 'Something went wrong...');
                    } else {

                        Autenti::createNew($user_id, $autenti_id, Yii::app()->user->id);

                        Yii::app()->user->setFlash('success', 'Success. Autenti ID: '.$autenti_id);
                        $this->redirect(['/autenti']);
                    }
//
                }

            }
            catch(Exception $ex)
            {
                Yii::app()->user->setFlash('error', 'Error: '.print_r($ex->getMessage(),1));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionGetTemplate()
    {
        $file = file_get_contents(WordGenerator::getTemplatePath());
        Yii::app()->request->sendFile('template.docx', $file);
    }


}