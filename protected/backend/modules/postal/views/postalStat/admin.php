<?php

$this->menu = array(
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>
    <div style="word-break: break-all">

<?php
$this->widget('MultipleSelect', array('selector' => '#stat-selector', 'withChosenPlugin' => true, 'chosenSelector' => 'UserGridView[id]'));
?>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
<?php $this->widget('backend.extensions.selgridview.BootSelGridView', array(
    'id' => 'postal-stat-grid',
    'selectableRows'=>2,
    'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'afterAjaxUpdate' => true, 'withChosenPlugin' => true, 'chosenSelector' => 'PostalStat[id]'), true),
    'selectionChanged'=>'function(id){  
                var number = $("#postal-stat-grid").selGridView("getAllSelection").length;
                $(".selected-no").html(number); 
         }',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'date_entered',
        array(
            'name' => 'editable',
            'value' => '$data->editable?"yes":"no"',
            'type' => 'raw',
            'filter' => array(0 => 'no', 1 => 'yes'),
        ),
//        array(
//            'name' => 'notify',
//            'value' => '$data->notify?"yes":"no"',
//            'type' => 'raw',
//            'filter' => array(0 => 'no', 1 => 'yes'),
//        ),
        array(
            'header' => 'Packages no',
            'name' => '_postalsStat',
            'value' => '$data->postalsSTAT',
            'filter' => false,
        ),
        array(
            'name' => 'operator_source_id',
            'header' => 'From external',
            'value' => 'PostalStat::getExternalServiceNameById($data->operator_source_id)',
            'filter' => PostalStat::getExternalServiceNameArray(),
            ),
        [
            'filter' => StatMap::mapNameList(),
            'name' => '_map_id',
            'value' => 'StatMap::getMapNameById($data->statMapMapping->map_id)'
        ],
        [
//            'filter' => StatMap::mapNameList(),
            'name' => '_map_date',
            'value' => '$data->statMapMapping->date_entered',
        ],
        [
//            'filter' => StatMap::mapNameList(),
            'name' => '_map_admin_id',
            'value' => '$data->statMapMapping->admin ? $data->statMapMapping->admin->login : ""',
        ],
        [
            'header' => 'Translated',
            'value' => '$data->isPlTranslationDifferent() ? "Y" : "-"',
        ],
        array(
            'class' => 'CButtonColumn',
            'buttons' => array(
                'update' => array(
                    'options' => array('class' => 'newWindow'),
                ),
            ),
        ),
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'selected_rows'
        ),
    ),
)); ?>
        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
    </div>



<h4>Set status Mapping</h4>

<?php
if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER])):
    ?>
    Option available only for administrators
<?php
else:
    ?>

    <?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'mass-assign-stat',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/statMapMapping/assingStatusByGridView', ['type' => StatMapMapping::TYPE_POSTAL]),
));
    ?>
    <table>
        <thead>
        <tr>
            <td>Stat Map</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>  <?php echo CHtml::dropDownList('stat_map_id','',StatMap::mapNameList()); ?></td>
        </tr>
        </tbody>
    </table>
    <?php
    echo TbHtml::submitButton('Change', array(
        'onClick' => 'js:
    $("#stat-ids").val($("#postal-stat-grid").selGridView("getAllSelection").toString());
    ;',
        'name' => 'StatIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
    ?>

    <?php echo CHtml::hiddenField('StatMapMapping[ids]','', array('id' => 'stat-ids')); ?>
    <?php
    $this->endWidget();
    ?>

<?php
endif;
?>


