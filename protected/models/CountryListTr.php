<?php

Yii::import('application.models._base.BaseCountryListTr');

class CountryListTr extends BaseCountryListTr
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                 'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),


            array('country_list_id, language_id, date_entered, name', 'required'),
            array('country_list_id, language_id', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>256),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, country_list_id, language_id, date_entered, date_updated, name', 'safe', 'on'=>'search'),
        );
    }
}