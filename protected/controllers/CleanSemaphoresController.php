<?php

class CleanSemaphoresController extends CController
{
    public function actionIndex()
    {
        Yii::app()->getModule('courier');

        MyDump::dump('semClean.txt', 'START');

        $models = CourierLabelNew::model()->findAll('DATE(date_entered) BETWEEN DATE((NOW() - INTERVAL 2 DAY)) AND DATE((NOW() - INTERVAL 1 DAY))');

        foreach ($models AS $model) {
            $id4Sem = intval(Semaphore::COURIER_LABEL_NEW_PREFIX . $model->id);
            Semaphore::removeSemaphore($id4Sem);
        }

        Yii::app()->getModule('postal');

        MyDump::dump('semCleanPostal.txt', 'START');

        $models = Postal::model()->findAll('DATE(date_entered) BETWEEN DATE((NOW() - INTERVAL 2 DAY)) AND DATE((NOW() - INTERVAL 1 DAY))');

        foreach ($models AS $model) {
            $id4Sem = intval(Semaphore::POSTAL_LABEL_PREFIX . $model->postal_label_id);
            Semaphore::removeSemaphore($id4Sem);
        }


        Semaphore::removeSemaphore(intval(Semaphore::MANIFEST_PREFIX . date("Ymd", strtotime("yesterday"))));
    }
}