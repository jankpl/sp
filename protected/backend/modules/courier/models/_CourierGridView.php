<?php

class _CourierGridView extends Courier
{
    public $__user_login;
    public $__receiver_country_id;
    public $__receiver_usefulName;
    public $__receiver_name;
    public $__receiver_company;
    public $__receiver_address;
    public $__receiver_city;
    public $__receiver_zip_code;
    public $__receiver_tel;
    public $__sender_country_id;
    public $__sender_usefulName;
    public $__sender_name;
    public $__sender_company;
    public $__sender_address;
    public $__sender_city;
    public $__sender_zip_code;
    public $__sender_tel;
    public $__stat;
    public $__type;
    public $__external_operator;
    public $__internal_operator;
    public $__internal_pickup_type;

    public $_multiIds;

    public static function getOperatorsList()
    {
        $CACHE_NAME = '_CGB_OPERATORS_LIST';


        $CACHE = Yii::app()->cache;

        $data = $CACHE->get($CACHE_NAME);

        if($data === false) {

            $data = [];


            $brokers = CourierLabelNew::getOperators();
            $cti = CourierOperator::model()->findAll(['order' => 'id ASC']);
            $cu = CourierUOperator::model()->findAll(['order' => 'id ASC']);


            foreach ($brokers AS $key => $item)
                $data[] = [
                    'id' => 'B_' . $key,
                    'name' => $item,
                    'type' => 'Broker'
                ];

            foreach ($cti AS $item)
                $data[] = [
                    'id' => 'I_' . $item->id,
                    'name' => $item->getNameWithIdAndBroker(),
                    'type' => 'Internal'
                ];

            foreach ($cu AS $item)
                $data[] = [
                    'id' => 'U_' . $item->id,
                    'name' => $item->getNameWithIdAndBroker(),
                    'type' => 'U'
                ];

            $CACHE->set($CACHE_NAME, $data, 60*60);
        }

        return $data;

    }

    public function rules() {

        $array = array(
            array('location,
             __user_login,
                 __sender_country_id,
                 __sender_usefulName,
                 __sender_name,
                 __sender_company,
                 __sender_address,
                 __sender_city,
                 __sender_zip_code,
                 __sender_tel,
                 __receiver_country_id,
                 __receiver_usefulName,
                 __receiver_name,
                 __receiver_company,
                 __receiver_address,
                 __receiver_city,
                 __receiver_zip_code,
                 __receiver_tel,
                 __stat,
                 __type,
				 __internal_operator,
				 __internal_pickup_type,
                 __external_operator,
                 stat_map_id,
                 _multiIds,
                    , user_cancel','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

//        $criteria->select = 'id, local_id, date_entered, date_updated, user_id, stat_map_id, courier_stat_id, packages_number, source, user_cancel, location, courier_type';

        if(strpos($this->date_entered,'|'))
        {
            $dateEntered = explode('|', $this->date_entered);
            $start = $dateEntered[0];
            $end = $dateEntered[1];

            if(S_Useful::checkDate($start) && S_Useful::checkDate($end))
                $criteria->addBetweenCondition('(t.date_entered)', $start.' 00:00:00', $end .' 23:59:59');
            else
                $this->date_entered = '';

        } else {
            $criteria->compare('(t.date_entered)', $this->date_entered, true);
        }

        if(strpos($this->date_updated,'|'))
        {
            $dateUpdated = explode('|', $this->date_updated);
            $start = $dateUpdated[0];
            $end = $dateUpdated[1];

            if(S_Useful::checkDate($start) && S_Useful::checkDate($end))
                $criteria->addBetweenCondition('(t.date_updated)', $start.' 00:00:00', $end .' 23:59:59');
            else
                $this->date_updated = '';

        } else {
            $criteria->compare('(t.date_updated)', $this->date_updated, true);
        }

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('courier_type_external_id', $this->courier_type_external_id);
        $criteria->compare('courier_type_internal_id', $this->courier_type_internal_id);
//        $criteria->compare('t.date_entered', $this->date_entered, true);
//        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('t.date_activated', $this->date_activated, true);
        $criteria->compare('t.date_delivered', $this->date_delivered, true);
        $criteria->compare('local_id', $this->local_id, false);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('sender_address_data_id', $this->sender_address_data_id);
        $criteria->compare('receiver_address_data_id', $this->receiver_address_data_id);

        $criteria->compare('package_weight', $this->package_weight);
        $criteria->compare('package_size_l', $this->package_size_l);
        $criteria->compare('package_size_w', $this->package_size_w);
        $criteria->compare('package_size_d', $this->package_size_d);
        $criteria->compare('packages_number', $this->packages_number);
        $criteria->compare('package_value', $this->package_value);
        $criteria->compare('package_content', $this->package_content, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('admin_id', $this->admin_id);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('user_cancel', $this->user_cancel);
        $criteria->compare('t.source', $this->source);
        $criteria->compare('t.stat_map_id', $this->stat_map_id);

//        $criteria->compare('courier_stat_id', $this->courier_stat_id);
        if($this->courier_stat_id)
        {
            $criteria->addInCondition('courier_stat_id', $this->courier_stat_id);
        }

        if($this->cod_value == 1)
            $criteria->addCondition('t.cod_value = 0');
        else if($this->cod_value == 2)
            $criteria->addCondition('t.cod_value > 0');

        // $criteria->compare('__type', $this->__type);

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
            $criteria->compare('user.salesman_id', Yii::app()->user->model->salesman_id);

        $with = [];

        if($this->__type)
        {
//            $criteria->addCondition('courier_type IS NOT NULL');

            if($this->__type == Courier::TYPE_INTERNAL)
                $criteria->addCondition('courier_type_internal_id IS NOT NULL');
            else if($this->__type == Courier::TYPE_EXTERNAL)
                $criteria->addCondition('courier_type_external_id IS NOT NULL');
            else if($this->__type == Courier::TYPE_INTERNAL_OOE) {
                $criteria->addCondition('courier_type_internal_id IS NULL');
                $criteria->addCondition('courier_type_external_id IS NULL');
                $criteria->together = true;
                $with[] = ('courierTypeOoe');
                $criteria->addCondition('courierTypeOoe.id IS NOT NULL');
            }
            else if($this->__type == Courier::TYPE_INTERNAL_SIMPLE)
            {
                $criteria->together = true;
                $with[] = ('courierTypeInternalSimple');
                $criteria->addCondition('courierTypeInternalSimple.id IS NOT NULL');
            }
            else if($this->__type == Courier::TYPE_DOMESTIC)
            {
                $criteria->together = true;
                $with[] = 'courierTypeDomestic';
                $criteria->addCondition('courierTypeDomestic.id IS NOT NULL');
            }
            else if($this->__type == Courier::TYPE_U)
            {
                $criteria->together = true;
                $with[] = ('courierTypeU');
                $criteria->addCondition('courierTypeU.id IS NOT NULL');
            }
            else if($this->__type == Courier::TYPE_EXTERNAL_RETURN)
            {
                $criteria->together = true;
                $with[] = ('courierTypeExternalReturn');
                $criteria->addCondition('courierTypeExternalReturn.id IS NOT NULL');
            }
            else if($this->__type == Courier::TYPE_RETURN)
            {
//                $criteria->together = true;
                $criteria->addCondition('courier_type = '.Courier::TYPE_RETURN);
            }
        }

        if($this->_multiIds)
            $criteria->addInCondition('t.id', explode(',', $this->_multiIds));

        if($this->__external_operator)
        {
            $criteria->together = true;
            $with[] = ('courierTypeExternal');
            $criteria->compare('courierTypeExternal.courier_external_operator_id',$this->__external_operator);
        }

        if($this->__internal_operator)
        {
            $criteria->together = true;

            $operatorsB = [];
            $operatorsU = [];
            $operatorsI = [];

            foreach($this->__internal_operator AS $io) {

                $operatorType = explode('_', $io);

                $operatorId = intval($operatorType[1]);
                $operatorType = $operatorType[0];

                if ($operatorType == 'B') {
                    $operatorsB[] = $operatorId;
                } elseif ($operatorType == 'I') {
                    $operatorsI[] = $operatorId;

                } elseif ($operatorType == 'U') {
                    $operatorsU[] = $operatorId;
                }
            }

            if(sizeof($operatorsB))
                $criteria->compare('courierLabelNew.operator', $operatorsB);

            if(sizeof($operatorsU))
            {
                $with[] = ('courierTypeU');
                $criteria->together = true;
                $criteria->addCondition('courierTypeU.id IS NOT NULL');
                $criteria->compare('courierTypeU.courier_u_operator_id', $operatorsU);
            }

            if(sizeof($operatorsI))
            {
                $with[] = ('courierTypeInternal');
                $criteria->together = true;
                $criteria->addCondition('courier_type_internal_id IS NOT NULL');
                $criteria->addCondition('courierTypeInternal.pickup_operator_id IN (' . implode(',', $operatorsI) . ') OR courierTypeInternal.delivery_operator_id IN (' . implode(',', $operatorsI) . ')');
            }

        }


        if($this->__internal_pickup_type)
        {
            $criteria->together = true;
            $with[] = ['courierTypeInternal'];
            $criteria->compare('courierTypeInternal.with_pickup',$this->__internal_pickup_type);
        }

//        if($this->__stat)
//        {
//            $criteria->compare('courier_stat_id',$this->__stat);
//        }

        if($this->__user_login)
        {
            $criteria->together = true;
            $with[] = ('user');
            $criteria->compare('user.login',$this->__user_login,true);
        }

        if(is_array($this->__sender_country_id))
        {
//            $criteria->together = true;
//            $with[] = ('senderAddressData');
            $criteria->addInCondition('senderAddressData.country_id',$this->__sender_country_id);
        }

        if(is_array($this->__receiver_country_id))
        {
//            $criteria->together = true;
//            $with[] = ('receiverAddressData');
            $criteria->addInCondition('receiverAddressData.country_id',$this->__receiver_country_id);
        }

        if($this->__receiver_usefulName)
        {
//            $criteria->together = true;
//            $with[] = ('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.name LIKE '%".addslashes($this->__receiver_usefulName)."%' OR receiverAddressData.company LIKE '%".addslashes($this->__receiver_usefulName)."%')");
        }

        if($this->__sender_usefulName)
        {
//            $criteria->together = true;
//            $with[] = ('senderAddressData');
            $criteria->addcondition("(senderAddressData.name LIKE '%".addslashes($this->__sender_usefulName)."%' OR senderAddressData.company LIKE '%".addslashes($this->__sender_usefulName)."%')");
        }


//        $temp = [];
//        foreach($with AS $key => $item)
//        {
//            if(is_array($item))
//                $temp[key($item)] = $item[key($item)];
//            else
//                $temp[$item] = true;
//        }

        if(is_array($with))
            $criteria->with = $with;

        return $criteria;
    }

    public function search()
    {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';


        if($this->_multiIds != '')
            $sort->defaultOrder = 'FIELD(t.id, '.$this->_multiIds.')';

        $sort->attributes = array(
            '__user_login' => array(
                'asc' => 'user.login ASC',
                'desc' => 'user.login DESC',
            ),
//            'daysFromStatusChange' => array(
//                'asc' => '(
//                        SELECT TIMESTAMPDIFF(
//                        DAY , date_entered,
//                        CURRENT_TIMESTAMP )
//                        FROM courier_stat_history
//                        WHERE courier_id = t.id
//                        ORDER BY date_entered DESC
//                        LIMIT 0 , 1
//                        ) ASC',
//                'desc' => '(
//                        SELECT TIMESTAMPDIFF(
//                        DAY , date_entered,
//                        CURRENT_TIMESTAMP )
//                        FROM courier_stat_history
//                        WHERE courier_id = t.id
//                        ORDER BY date_entered DESC
//                        LIMIT 0, 1
//                        ) DESC',
//            ),
            '__sender_country_id' => array(
                'asc' => 'senderAddressData.country ASC',
                'desc' => 'senderAddressData.country DESC',
            ),
            '__receiver_country_id' => array(
                'asc' => 'receiverAddressData.country ASC',
                'desc' => 'receiverAddressData.country DESC',
            ),
            'courier_stat_id' => array(
                'asc' => 'courier_stat_id ASC',
                'desc' => 'courier_stat_id DESC',
            ),
            '*', // add all of the other columns as sortable
        );

        $criteria = $this->searchCriteria();

        // if there are no conditions, use simple count of packages to improve performance
        $totalCount = NULL;
        if ($criteria->condition == '') {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(id)')->from((new self)->tableName());
            $totalCount = $cmd->queryScalar();
        }


        return new CActiveDataProvider($this
//            ->with('courierTypeInternal')
//            ->with('courierTypeU')
            ->with(['senderAddressData' => ['select' => 'name, company, country_id']])
//            ->with('senderAddressData.country0')
            ->with(['receiverAddressData' => ['select' => 'name, company, country_id']])
//            ->with(
//                ["receiverAddressData.country0" => [
//                    'alias' => 'receiverCountry',
//                ]
//                ])
//            ->with('courierTypeInternalSimple')
//            ->with('courierTypeU')
//            ->with('courierTypeExternal')
//            ->with('courierTypeExternalReturn')
//            ->with('courierTypeReturn')
            ->with('courierLabelNew')
//            ->with('courierTypeInternalSimple.courierExtOperatorDetails.courierLabelNew')
//            ->with(['courierTypeDomestic.courierLabelNew' => ['alias' => 'ctd']])
//            ->with('courierTypeDomestic.courierDomesticOperator')
//            ->with(['courierTypeInternal.commonLabel.courierLabelNew' => ['alias' => 'cticl']])
//            ->with(['courierTypeInternal.pickupLabel.courierLabelNew' => ['alias' => 'ctipl']])
//            ->with(['courierTypeInternal.deliveryLabel.courierLabelNew' => ['alias' => 'ctidl']])
//            ->with(['courierTypeOoe.courierLabelNew' => ['alias' => 'cto']])
            ->with(['user' => ['select' => 'login, salesman_id, id']])
//            ->with('stat')
            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', 50),
                ),
            ));



    }

}