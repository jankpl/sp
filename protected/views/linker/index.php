<?php
/* @var $lm LinkerManager */


?>
<div class="form">
    <h2><?= Yii::t('linkerManager', 'Linker');?></h2>
</div>
<?php
$this->widget('ShowPageContent', array('id' => 74));
?>
<br/>
<?php
$this->widget('FlashPrinter');
?>
<br/>
<?php

if($lm):

    $tabs = [];
    $tabs[] = array('label' => Yii::t('linkerManager', 'Twoje integracje'), 'id' => 'current', 'active' => true, 'content' => $this->renderPartial('tabs/_current', ['lm' => $lm,], true));
    $tabs[] = array('label' => Yii::t('linkerManager', 'Dodaj nową integrację'), 'id' => 'new', 'content' => $this->renderPartial('tabs/_new', ['lm' => $lm,], true));

    $this->widget('bootstrap.widgets.TbTabs', array(
        'tabs' => $tabs,
    ));

endif;

?>

<script>
    // go to particular tab on reload
    $(function(){
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');

        $('.nav-tabs a').click(function (e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });
    });
</script>
