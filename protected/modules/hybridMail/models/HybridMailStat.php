<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailStat');

class HybridMailStat extends BaseHybridMailStat
{

    const NEW_ORDER = 1;
    const PACKAGE_RETREIVE = 2;
    const FINISHED = 3;
    const CANCELLED = 99;

    public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=> S_Status::ACTIVE, 'on'=>'insert'),

            array('editable', 'default',
                'value'=> S_Status::ACTIVE, 'on'=>'insert'),

            array('notify', 'default',
                'value'=> 0),

            array('name', 'required'),
            array('stat, editable, notify', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>32),
            array('id, name, date_entered, date_updated, stat, editable, notify', 'safe', 'on'=>'search'),
        );
    }


    public static function getNameById($id)
    {
        $model = self::model()->findByPk($id);

        return $model->name;
    }

    public static function findIdByText($text)
    {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('hybrid_mail_stat.id');
        $cmd->from('hybrid_mail_stat');
        $cmd->join('hybrid_mail_stat_tr', 'hybrid_mail_stat_tr.hybrid_mail_stat_id = hybrid_mail_stat.id');
        $cmd->where('hybrid_mail_stat.id = :text', array(':text' => $text));
        $cmd->orWhere('hybrid_mail_stat.name = :text', array(':text' => $text));
        $cmd->orWhere('short_text = :text', array(':text' => $text));
        $cmd->orWhere('full_text = :text', array(':text' => $text));
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if(!$result)
            return NULL;

        return $result;
    }

    public static function convertDate($text)
    {
        if($text != '')

            try
            {
                $date = new DateTime($text);
                $text = $date->format('Y-m-d H:i:s');
            }
            catch(Exception $ex)
            {
                $text = NULL;
            }

        return $text;
    }

    /**
     * Fixed replacement for relation getStatText due to some problems with dynamic language changing for mailing
     * @return array|mixed|null
     */
    public function getStatTextOne()
    {
        return HybridMailStatTr::model()->findByAttributes(['language_id' => Yii::app()->params['language'], 'hybrid_mail_stat_id' => $this->id]);
    }

    /**
     * Function tries to return best matched stat description
     * @param bool|false $full
     * @return string
     */
    public function getStatText($full = false)
    {
        $statTr = $this->getStatTextOne();

        if($statTr === NULL)
            return $this->name;
        else
        {
            if($full)
            {
                if($statTr->full_text != '')
                    return $statTr->full_text;
                else
                    return $statTr->short_text;
            } else
                return $statTr->short_text;
        }
    }

}