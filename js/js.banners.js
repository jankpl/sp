function showSlides(imagesList)
{


    var firstVisible = true;
    var firstImg = $("div#imgFaderOne");
    var secondImg = $("div#imgFaderTwo");
    var preloader = $("div#preloader");
    var imagesNumber = imagesList.length;
    var currentImagePointer = 0;

    var imgPath = imagesList[currentImagePointer];

    var img = $('<img>').on('load', function() {
        firstImg.css('background', 'url(\'' + imgPath + '\')');
        firstImg.css('background-size', '100% 100%');
        firstImg.delay(500).fadeIn();

        runSlideshow();
    }).attr('src', imgPath);




    function runSlideshow()
    {
        $("div#banner").everyTime(3000,function() {

            currentImagePointer = (currentImagePointer + 1) % imagesNumber;

            imgPath = imagesList[currentImagePointer];

            img = $('<img>').on('load', function() {
                if(firstVisible == true)
                {
                    secondImg.css('background', 'url(\'' + imgPath + '\') no-repeat');
                    secondImg.css('background-size', '100% 100%');
                    secondImg.fadeIn();
                    firstVisible = false;
                }
                else
                {
                    firstImg.css('background', 'url(\'' + imgPath + '\') no-repeat');
                    firstImg.css('background-size', '100% 100%');
                    secondImg.fadeOut();
                    firstVisible = true;
                }
            }).attr('src', imgPath);
        });
    }


}

