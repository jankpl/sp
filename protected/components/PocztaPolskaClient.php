<?php

class PocztaPolskaClient
{

    const TEST_MODE = GLOBAL_CONFIG::POCZTA_POLSKA_TEST_MODE;

    const URL = 'https://e-nadawca.poczta-polska.pl/websrv/en.wsdl';
    const URL_TEST = 'https://en-testwebapi.poczta-polska.pl/websrv/en.wsdl';


    const ACC_WROCLAW = 1;
    const ACC_WARSZAWA = 2;
    const ACC_BOCHNIA = 3;
    const ACC_NYSA = 4;
    const ACC_FIREBUSINESS = 5;
    const ACC_SP_NYSA = 6;
    const ACC_SP_ZABRZE = 7;

    const ACC_TEST = 99;

    const ENVELOPE_DESC = '_MS';

    const DEFAULT_DAYS_DELAY = 3;

    protected $_acc = false; // e-nadawca account
    protected $_login = false;
    protected $_up = false; // urząd pocztowy
    protected $_envelope_id = false; // id zbioru


    protected $_type;
    protected $_type_id;
    protected $_local_id;


    protected $_log_id; // used for logging data

    protected static function _modifyWeight($weight)
    {
        $weight = intval($weight);

        if ($weight <= 50)
        {

        }
        else if ($weight <= 100) // top 50
        {
            $weight = 50;
            $weight -= (rand(0, 2) * 10);
        }
        else if ($weight <= 350) // top 100
        {
            $weight = 100;
            $weight -= (rand(0, 4) * 10);
        }
        else if ($weight <= 500) // top 100
        {
            $weight = 100;
            $weight -= (rand(0, 2) * 10);
        }
        else if ($weight <= 1000) // top 350
        {
            $weight = 350;
            $weight -= (rand(0, 20) * 10);
        }
        else if ($weight <= 2000) // top 1000
        {
            $weight = 1000;
            $weight -= (rand(0, 40) * 10);
        }

        return $weight;
    }

//    public static function manualReinsertOfItems(array $items)
//    {
//
//
//        $model = new self(self::ACC_FIREBUSINESS);
//
//        /* @var $items Postal[] */
//        $model->_createEnvelope();
//
//        // find ID of new envelope
//        $envelopes = $model->_listEnvelopes();
//        foreach ($envelopes AS $item) {
//            if ($item->opis == self::getEvelopeName()) {
//                $envelope_id = $item->idBufor;
//            }
//        }
//
//        $guidPrefix = '9AA';
//
//        foreach($items AS $postal) {
//            $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, true, $model->getAcc(), $postal->postalLabel->track_id, $guidPrefix);
//            $return = $model->createNewPackage($data,0, $postal->postalLabel->track_id, $guidPrefix, false, false, $envelope_id);
//
//            var_dump($return);
//        }
//
//    }


    protected static function getEvelopeName($forOnwLabelsItems = false, $customDays = false)
    {
        $name = self::ENVELOPE_DESC.'_'.date('Ymdh');

        if($customDays)
            $name .= '_CD'.$customDays;

        if($forOnwLabelsItems)
            $name = '_OL'.$name;

        return $name;
    }

    public function getAcc()
    {
        return $this->_acc;
    }

    public static function moveAndSendItems(array $items, $acc_id, $date_posting)
    {
        $buforName = 'ZB_'.date('Y-m-d _h:i');

        $model = new self;
        $model->_setAccount($acc_id);
//
        $data = new stdClass();
        $data->dataNadania = $date_posting;
        $data->urzadNadania = $model->_up;
        $data->opis = $buforName;

        $resp = $model->_go('createEnvelopeBufor', [
            'bufor' => $data,
        ]);


        if($resp['success']) {

            $buforId = $resp['response']->createdBufor->idBufor;

            $moved = $model->move($items, $buforId);
            if($moved)
            {
                $data = [
                    'urzadNadania' => $model->_up,
                    'idBufor' => $buforId
                ];

                $response = $model->_go('sendEnvelope', $data);

                if($response['success'])
                {
                    if($response['response']->envelopeStatus == 'WYSLANY') {
                        $envelopeName = $response['response']->envelopeFilename;
                        PocztaPolskaWaiting::setEnvelopeSent($buforId, $envelopeName);
                        return $buforName;
                    }
                } else
                    throw new Exception($resp['error'] == '' ? 'Errors occured - check logs of items' : $resp['error']);
            }
        } else
            throw new Exception($resp['error'] == '' ? 'Errors occured - check logs of items' : $resp['error']);


        return false;
    }



    public function move(array $items, $newBuffer)
    {

        if(S_Useful::sizeof2($items) > 500)
            throw new Exception('Max 500 items at once!');

        /* @var $items PocztaPolskaWaiting[] */


        $data = [];

        foreach($items AS $item)
        {
            if(!isset($data[$item->envelope_id]))
                $data[$item->envelope_id] = [];

            $data[$item->envelope_id][] = $item->guid;
        }



        foreach($data AS $oldBuffer => $guids)
        {
            $resp = $this->_go('moveShipments', [
                'idBuforFrom' => $oldBuffer,
                'idBuforTo' => $newBuffer,
                'guid' => $guids,
            ]);


            if($resp['success']) {

                $notMoved = $resp['response']->notMovedGuid;

                if(!is_array($notMoved))
                    $notMoved = [ $notMoved ];

                foreach($guids AS $guid) {

                    if(in_array($guid, $notMoved))
                        PocztaPolskaWaiting::setLog($guid, 'Could not move to new buffer');
                    else
                        PocztaPolskaWaiting::updateBuffor($guid, $newBuffer,'Buffer updated');
                }

            } else {
                foreach($guids AS $guid) {
                    PocztaPolskaWaiting::setLog($guid, 'Could not move to new buffer');
                }
//                throw new Exception('Could not move to new buffer');
            }
        }

        return true;

    }

    protected function _getCacheEnvelopeName($forOnwLabelsItems = false, $customDays = false)
    {
        $name = 'pocztaPolska_envelope_'.$this->_acc.'_date_'.date('Ymd');

        if($customDays)
            $name .= '_CD'.$customDays;

        if($forOnwLabelsItems)
            $name .= '_OL';

        return $name;
    }

    public function clearEnvelopeCache($forOnwLabelsItems = false, $customDays = false)
    {
        $CACHE_NAME = $this->_getCacheEnvelopeName($forOnwLabelsItems, $customDays);
        Yii::app()->cache->set($CACHE_NAME, false, 60 * 60 * 24);
        MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'Clear ENVELOPE CACHE');
    }

    protected function verifyAndFixEnvelope($forOnwLabelsItems = false, $customDays = false)
    {
        $CACHE_NAME = $this->_getCacheEnvelopeName($forOnwLabelsItems, $customDays);

        $current_envelope_id = Yii::app()->cache->get($CACHE_NAME);

        MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'VERIFY AND FIX ENVELOPE');

        if($current_envelope_id == false) {
            MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'ENVELOPE Not in CACHE');

            $envelope_id = false;

            $envelopes = $this->_listEnvelopes();

            // search for known envelope
            $envelope_date = false;

            foreach ($envelopes AS $item) {
                if ($item->opis == self::getEvelopeName($forOnwLabelsItems, $customDays)) {
                    $envelope_id = $item->idBufor;
                    $envelope_date = $item->dataNadania;

                    MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'ENVELOPE FOUND BY DESC ('.$envelope_id.')');
                    break;
                }
            }

            MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'ENVELOPE NOT FOUND IN: '.print_r($envelopes,1));

            // envelope not found - create new
            if (!$envelope_id) {
                MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'ENVELOPE - CREATE NEW');
                $this->_createEnvelope($forOnwLabelsItems, $customDays);

                // find ID of new envelope
                $envelopes = $this->_listEnvelopes();
                foreach ($envelopes AS $item) {
                    if ($item->opis == self::getEvelopeName($forOnwLabelsItems, $customDays)) {
                        $envelope_id = $item->idBufor;

                        MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'ENVELOPE FOUND NEW BY DESC ('.$envelope_id.')');
//                        $envelope_date = $item->dataNadania;
                        break;
                    }
                }
            } else {
                // check date of existing envelope

                $validDate = S_Useful::workDaysNextDate(date('Y-m-d'), $customDays ? $customDays : self::DEFAULT_DAYS_DELAY);

                if ($envelope_date != $validDate) {
                    MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'ENVELOPE UPDATE DATE');
                    // set new date for current envelope
                    $this->_updateEnvelopeDate($validDate, $envelope_id, $forOnwLabelsItems, $customDays);
                }
            }

            $current_envelope_id = $envelope_id;
            Yii::app()->cache->set($CACHE_NAME, $current_envelope_id, 60*60);
        }

        $this->_envelope_id = $current_envelope_id;
    }

    public function forgetPassword()
    {
        $CACHE_NAME = 'pocztaPolska_password_' . $this->_acc;
        Yii::app()->cache->set($CACHE_NAME, false, 60 * 60 * 5);

        MyDump::dump('pp_pass_log.txt', date('Y-m-d H:i:s').' : '.$this->_acc.' : FORGET!');

    }

    public function getPassword($doNotCheckPassword = false)
    {
        $CACHE_NAME = 'pocztaPolska_password_'.$this->_acc;

        $password = Yii::app()->cache->get($CACHE_NAME);

//        $password = false;

        if($password === false) {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('value, date, what');
            $cmd->from('poczta_polska_ustawienia');
            $cmd->where('what = :what OR what = :what2', [':what' => 'pass_' . $this->_acc, ':what2' => 'envelopeId']);
            $result = $cmd->queryRow();


            $pass = $result['value'];

            // periodical password chagne is no loger required

//            $date = $result['date'];
//
//            $passwordDate = new DateTime($date);
//            $today = new DateTime();
//            $passwordAge = $today->diff($passwordDate)->format("%a");


//            if (!$doNotCheckPassword && $passwordAge > 14 && !self::TEST_MODE) {
//
//                $newPass = $this->_makePassword();
//
//                MyDump::dump('pp_pass_log.txt', date('Y-m-d H:i:s').' : '.$this->_acc.' : '.$newPass);
//
//                $resp = $this->setNewPassword($newPass);
////
//                if ($resp['success']) {
//                    /* @var $cmd CDbCommand */
//                    $cmd = Yii::app()->db->createCommand();
//                    $cmd->update('poczta_polska_ustawienia', ['value' => $newPass, 'date' => date('Y-m-d H:i:s')], 'what = :what', [':what' => 'pass_' . $this->_acc]);
////
//                    $pass = $newPass;
//                }
//            }

            $password = $pass;
            Yii::app()->cache->set($CACHE_NAME, $password, 60 * 5);
        }

        return $password;
    }

    public function _makePassword()
    {
        return substr('Ms1'.md5(microtime()),0,8);
    }

    protected static function _accounts()
    {



        if(self::TEST_MODE) {

            return [
                self::ACC_TEST =>
                    [
                        'l' => 'jankopec@swiatprzesylek.pl',
                        'p' => '1sacWe123dq1',
                        'up' => '433423',
                        'id' => self::ACC_TEST,
//                    'bufor' => '24988182',
                    ],
            ];
        }

        return [
            self::ACC_WROCLAW =>
                [
                    'l' => 'mailsolution_wroclaw',
                    'up' => '201924', // Wrocław ul. Ibn Siny Awicenny 21  (PP PP-Wrocław D101)
                    'id' => self::ACC_WROCLAW,
//                    'bufor' => '24988182',
                ],
            self::ACC_WARSZAWA =>
                [
                    'l' => 'mailsolution_warszawa',
                    'up' => '743385', // Warszawa ul. Łączyny 8  (PP Warszawa W101)
                    'id' => self::ACC_WARSZAWA,
//                    'bufor' => '25083114',
                ],
            self::ACC_BOCHNIA =>
                [
                    'l' => 'mailsolution_bochnia',
                    'up' => '232985', // Bochnia ul. Kazimierza Wielkiego 34  (UP Bochnia)
                    'id' => self::ACC_BOCHNIA,
//                    'bufor' => '25009082',
                ],
            self::ACC_NYSA =>
                [
                    'l' => 'mailsolution_nysa',
                    'up' => '256543', // Nysa ul. Bolesława Krzywoustego 21  (UP Nysa 1)
                    'id' => self::ACC_NYSA,
//                    'bufor' => '25019129',
                ],
            self::ACC_FIREBUSINESS =>
                [
                    'l' => 'piotrgemel@fire-business.com.pl',
                    'up' => '743385', // Warszawa ul. Łączyny 8  (PP Warszawa W101)
                    'id' => self::ACC_FIREBUSINESS,
//                    'bufor' => '25019129',
                ],

            self::ACC_SP_NYSA =>
                [
                    'l' => 'piotrkocon@swiatprzesylek.pl',
                    'up' => '256543', //Nysa ul. Bolesława Krzywoustego 21 (UP Nysa 1)
                    'id' => self::ACC_SP_NYSA,
//                    'bufor' => '25019129',
                ],

            self::ACC_SP_ZABRZE =>
                [
                    'l' => 'piotrkocon@swiatprzesylek.pl',
                    'up' => '743378', //Zabrze (PP Zabrze S101)
                    'id' => self::ACC_SP_ZABRZE,
//                    'bufor' => '25019129',
                ],
        ];


    }

    public static function getAccountList($full = false, $ignoreSub = false)
    {
        $data = self::_accounts();

        if($ignoreSub)
            unset($data[self::ACC_SP_ZABRZE]);


        if($full)
            return $data;
        else
            return array_keys($data);
    }

    public static function getLoginByAccount($id)
    {
        return self::_accounts()[$id]['l'];
    }

    public function __construct($account = false, $forOnwLabelsItems = false, $customDays = false)
    {
        $this->_log_id = 'PP'.uniqid();

        if($account)
            $this->_setAccount($account, $forOnwLabelsItems, $customDays);
    }

    protected function _setAccount($account, $forOnwLabelsItems = false, $customDays = false)
    {
        // TEMP!
//        $account = self::ACC_WROCLAW;

        if(self::TEST_MODE)
        {
            $account = self::ACC_TEST;
        }

        if(!isset(self::_accounts()[$account]))
            throw new Exception('PocztaPolska account not found!');

        $this->_login = self::_accounts()[$account]['l'];
        $this->_up = self::_accounts()[$account]['up'];
        $this->_acc = $account;
//        $this->_envelope_id = self::_accounts()[$account]['bufor'];


        $this->verifyAndFixEnvelope($forOnwLabelsItems, $customDays);

    }

    protected function _go($action, $data, $doNotCheckPassword = false, $testMode = false, $retry = 0, $throwError = false)
    {
//        $throttle = new Stiphle\Throttle\LeakyBucket;
//        $storage = New Stiphle\Storage\Apcu();
//        $throttle->setStorage($storage);
//
//        $throttled = $throttle->throttle('POCZTA_POLSKA_REQUEST', 1, 2000);
//        if($throttled)
//            MyDump::dump('pp_throttled.txt', print_r($throttled,1));


        if(self::TEST_MODE)
        {
            $url = self::URL_TEST;
        } else {
            $url = self::URL;
        }

        $classMap = [
            'przesylkaFirmowaPoleconaType' => 'przesylkaFirmowaPoleconaType',
            'przesylkaPoleconaKrajowaType' => 'przesylkaPoleconaKrajowaType',
            'przesylkaRejestrowanaType' => 'przesylkaRejestrowanaType',
            'przesylkaType' => 'przesylkaType',
            'adresType' => 'adresType',
            'paczkaPocztowaType' => 'paczkaPocztowaType',
            'pobranieType' => 'pobranieType',
//            'uslugaKurierskaType9' => 'uslugaKurierskaType',
//            'przesylkaBiznesowaType' => 'przesylkaBiznesowaType',
//            'buforType' => 'buforType',
//            'urzadNadaniaType' => 'urzadNadaniaType',
        ];

        $password = $this->getPassword($doNotCheckPassword);
        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'exceptions' => 1,
//            'stream_context' => stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'ignore_errors' => true))),
            'login' => $this->_login,
            'password' => $password,
            'classmap' => $classMap,
            'connection_timeout' => 300,
        );

//        MyDump::dump('pocztaPolska.txt', 'LOGIN - URL: '.print_r($url,1));
//        MyDump::dump('pocztaPolska.txt', 'LOGIN - USER: '.print_r($this->_login,1));
//        MyDump::dump('pocztaPolska.txt', 'LOGIN - PASS: '.print_r($password,1));

        $return = [
            'success' => false,
            'error' => NULL,
            'response' => NULL,
        ];

        $client = false;

        MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action);


        try {

            $client = new SoapClient($url, $mode);
//            $client = new SoapClient(YiiBase::getPathOfAlias('application.components').'/pp.wsdl', $mode);

            if(self::TEST_MODE)
                $client->__setLocation('https://en-testwebapi.poczta-polska.pl/websrv/en.php');

            $resp = $client->__soapCall($action, [$action => $data]);

            MyDump::dump('pocztaPolska_2.txt', $this->_log_id .' ]:'. $this->_login.' :: GLREQ '. print_r($client->__getLastRequest(),1));
            MyDump::dump('pocztaPolska_2.txt', $this->_log_id .' ]:'. $this->_login.' :: GLRES '. print_r($client->__getLastResponse(),1));

        }
        catch (SoapFault $ex)
        {


            MyDump::dump('pocztaPolska_2.txt', $this->_log_id .' ]:'. $this->_login.' :: SF '. print_r($ex->getMessage(),1));
            MyDump::dump('pocztaPolska_2.txt', $this->_log_id .' ]:'. $this->_login.' :: SF_GLREQ '. print_r($client->__getLastRequest(),1));
            MyDump::dump('pocztaPolska_2.txt', $this->_log_id .' ]:'. $this->_login.' :: SF_GLRES '. print_r($client->__getLastResponse(),1));

            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : '.print_r($ex->getMessage(),1));

            if($ex->getMessage() == 'Unauthorized')
                $this->forgetPassword();

            $return['error'] = '('.$ex->getCode().') : '.$ex->getMessage();
            if($client) {
//                MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : '.print_r($client->__getLastRequestHeaders(), 1));
                MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : '.print_r($client->__getLastRequest(), 1));
                MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. print_r($client->__getLastResponse(),1));
            }


//            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : GLReq : '.print_r($client->__getLastRequest(),1));
//            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : GLReqH : '.print_r($client->__getLastRequestHeaders(),1));
//            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : GLRes : '.print_r($client->__getLastResponse(),1));
//            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : GLResH : '.print_r($client->__getLastResponseHeaders(),1));


            if($throwError)
                throw new Exception($ex);

            return $return;
        }
        catch (Exception $ex)
        {
            MyDump::dump('pocztaPolska_2.txt', $this->_log_id .' ]:'. $this->_login.' :: EX '. print_r($ex->getMessage(),1));
            MyDump::dump('pocztaPolska_2.txt', $this->_log_id .' ]:'. $this->_login.' :: EX_GLREQ '. print_r($client->__getLastRequest(),1));
            MyDump::dump('pocztaPolska_2.txt', $this->_log_id .' ]:'. $this->_login.' :: EX_GLRES '. print_r($client->__getLastResponse(),1));

            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. print_r($ex->getMessage(),1));
            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. print_r($client->__getLastResponse(),1));


            if($ex->getMessage() == 'Unauthorized')
                $this->forgetPassword();


//            if($ex->getMessage() == 'Could not connect to host' && $retry < 3)
//            {
//                MyDump::dump('pp_soap_er.txt', 'RETRY: '.$retry,1);
//                sleep(1);
//                $retry++;
//
//                return $this->_go($action, $data, $doNotCheckPassword, $testMode, $retry);
//            }

            $return['error'] = '('.$ex->getCode().') : '.$ex->getMessage();
            if($client) {
//                MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. print_r($client->__getLastRequestHeaders(), 1));
                MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. print_r($client->__getLastRequest(), 1));
            }
            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. print_r($ex,1));

//            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : GLReq : '.print_r($client->__getLastRequest(),1));
//            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : GLReqH : '.print_r($client->__getLastRequestHeaders(),1));
//            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : GLRes : '.print_r($client->__getLastResponse(),1));
//            MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. $action.' : GLResH : '.print_r($client->__getLastResponseHeaders(),1));


            if($throwError)
                throw new Exception($ex);

            return $return;
        }

//        MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. 'REQUEST HEADER: '.print_r($client->__getLastRequestHeaders(),1));
        MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. 'REQUEST: '.print_r($client->__getLastRequest(),1));
//        MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. 'RESPONSE HEADER: '.print_r($client->__getLastResponseHeaders(),1));
//        MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. 'RESPONSE: '.print_r($client->__getLastResponse(),1));
        MyDump::dump('pocztaPolska.txt', $this->_log_id .' ]:'. $this->_login.' :: '. 'RESPONSE: '.preg_replace('%<pdfContent>(.*)</pdfContent>%i', '<pdfContent>(...TRUNCATED LABEL BASE64...)</pdfContent>', print_r($resp,1)));



        // to prevent adding label content to log.
//        if($action == 'getAddresLabelByGuid' && $resp->content && $resp->content->pdfContent) {
//            // Watch out for object copying with reference! Better to temporary change the same obj...
//            $labelTemp = $resp->content->pdfContent;
//            $resp->content->pdfContent = '!LABEL CLEARED!';
////            MyDump::dump('pp_soap_dump.txt', print_r($resp,1));
//            $resp->content->pdfContent = $labelTemp;
//        } else {
////            MyDump::dump('pp_soap_dump.txt', print_r($resp,1));
//        }

        if($action == 'getAddresLabelByGuid' && (!isset($resp->content) OR !isset($resp->content->pdfContent)))
        {
            $return = [
                'success' => false,
                'error' => 'No label in PP response!',
                'response' => NULL,
            ];
            return $return;
        }

        if(isset($resp->retval))
            $resp = $resp->retval;

        if($resp->error OR (is_array($resp) && $resp[1]->error))
        {
            if(is_array($resp) && $resp[1]->error)
                $resp = $resp[1];

            $errorString = '';

            if(!is_array($resp->error))
                $resp->error = [$resp->error];

            if(is_array($resp->error))
                foreach($resp->error AS $item)
                    $errorString .= $item->errorNumber.' : '.$item->errorDesc.'; ';


            if($throwError)
                throw new Exception($errorString);

            $return['error'] = $errorString;
        } else {
            $return['success'] = true;
            $return['response'] = $resp;
        }

//        var_dump($client->__getLastRequest());

        return $return;
    }

    protected function _clearEnvelope()
    {
        $data = [];

        if($this->_envelope_id)
            $data['idBufor'] = $this->_envelope_id;

        return $this->_go('clearEnvelope', $data);
    }

//    protected function _clearEnvelope($envelope_id = -1)
//    {
//        $envelopes = $this->_listEnvelopes();
//
//        // clear envelopes only if there is more than one
//        if(is_array($envelopes) && S_Useful::sizeof2($envelopes) > 1) {
//
//            $data = [];
//            $data['idBufor'] = $envelope_id;
//
//            return $this->_go('clearEnvelope', $data);
//        }
//        else
//        {
//            MyDump::dump('pp_se.txt', date('Y-m-d h:i:s').' : '.$this->_acc.' : ENVELOPE CLEAR STOPPED - NO MORE THAN ONE!');
//            return false;
//        }
//
//    }

    protected function _getAddresLabelByGuid($guid)
    {

        $data = [
            'guid' => $guid,
        ];

        if($this->_envelope_id)
            $data['idBufor'] = $this->_envelope_id;

        return $this->_go('getAddresLabelByGuid', $data);
    }

    public function sendEnvelope()
    {
        $data = [
            'urzadNadania' => $this->_up
        ];

        if($this->_envelope_id)
            $data['idBufor'] = $this->_envelope_id;

        $response = $this->_go('sendEnvelope', $data);

//        if(!$response['success'])
//        {
//            $errorCode = explode(' : ', $response['error']);
//
//            // zbiór nie istnieje
//            if($errorCode[0] == '13207')
//            {
//                $this->_clearEnvelopeCounter();
//                MyDump::dump('pp_se.txt', date('Y-m-d h:i:s').' : '.$this->_acc.' : ENVELOPE DO NOT EXISTS');
//            }
//            // zbiór nie zawiera żadnych przesyłek
//            // If envelope is empty - delete it
//            else if($errorCode[0] == '79403')
//            {
//                MyDump::dump('pp_se.txt', date('Y-m-d h:i:s').' : '.$this->_acc.' : ENVELOPE CLEAR');
//                $this->_clearEnvelope($this->_envelope_id);
//                $this->_clearEnvelopeCounter();
//
//            }
//        }

        if($response['success']) {
            $this->_clearEnvelopeCounter();
            MyDump::dump('pp_se.txt', date('Y-m-d h:i:s').' : '.$this->_acc.' : SUCCESS');
        } else {
            MyDump::dump('pp_se.txt', date('Y-m-d h:i:s').' : '.$this->_acc.' : FAIL: '.print_r($response,1));
        }

        return $response;

    }

    public function _createEnvelope($forOnwLabelItems = false, $customDays = false)
    {
        $buforId = date('Ymdhis');

        $this->_envelope_id  = $buforId;

        $data = new stdClass();
//        $data->idBufor = '1234';
        $data->dataNadania = S_Useful::workDaysNextDate(date('Y-m-d'), $customDays ? $customDays : self::DEFAULT_DAYS_DELAY);
        $data->urzadNadania = $this->_up;
        $data->opis = self::getEvelopeName($forOnwLabelItems, $customDays);

        return $this->_go('createEnvelopeBufor', [
            'bufor' => $data,
        ]);
    }

    public function _listEnvelopes()
    {
        $list = $this->_go('getEnvelopeBuforList', [
        ]);

        if(!$list['success'])
            return $list;

        $list = $list['response']->bufor;

        if(!is_array($list))
            $list = [ $list ];

        return $list;
    }

    public function _getCurrentEnvelopeDate($forOnwLabelItems = false, $customDays = false)
    {
        $list = $this->_listEnvelopes();

        if(!$list['success'])
            return $list;

        $list = $list['response']->bufor;

        if(!is_array($list))
            $list = [ $list ];

        foreach($list AS $item)
        {
            if($item->idBufor == self::getEvelopeName($forOnwLabelItems, $customDays))
            {
                $this->_envelope_id = $item->idBufor;

                return $item->dataNadania;
                break;
            }
        }

        return $list;
    }

    public static function sendAllEnvelopes()
    {
        foreach(self::_accounts() AS $key => $item)
        {
            $model = new self($key);
            $model->sendEnvelope();
        }
    }

    public function verifyAndFixEnvelopeSend()
    {
        $value = $this->_getEnvelopeCounter();
        if($value > 550)
            $this->sendEnvelope();
    }

    public function _increaseEnvelopeCounter()
    {
        $CACHE_NAME = 'pocztaPolska_envelope_counter_'.$this->_acc.'_'.date('Ymd');

        $value = $this->_getEnvelopeCounter();
        $value++;

        Yii::app()->cache->set($CACHE_NAME, $value, 60 * 60 * 24);

        return true;
    }

    public function _clearEnvelopeCounter()
    {
        $CACHE_NAME = 'pocztaPolska_envelope_counter_'.$this->_acc.'_'.date('Ymd');

        $value = 0;
        Yii::app()->cache->set($CACHE_NAME, $value, 60 * 60 * 24);

        $this->clearEnvelopeCache();

        return true;
    }

    public function _getEnvelopeCounter()
    {
        $CACHE_NAME = 'pocztaPolska_envelope_counter_'.$this->_acc.'_'.date('Ymd');
        return Yii::app()->cache->get($CACHE_NAME);
    }


    public function _updateEnvelopeDate($date, $envelope_id = false, $forOnwLabelItems = false, $customDays = false)
    {
        $data = new stdClass();

        if($envelope_id)
            $data->idBufor = $envelope_id;
        else if($this->_envelope_id)
            $data->idBufor = $this->_envelope_id;

        $data->dataNadania = $date;
        $data->urzadNadania = $this->_up;
        $data->opis = self::getEvelopeName($forOnwLabelItems, $customDays);

        return $this->_go('updateEnvelopeBufor', [
            'bufor' => $data,
        ]);

    }

    protected function _addShipment($data, $envelopeId = false)
    {
        $params = new stdClass();
        $params->przesylki = [];
        $params->przesylki[] = $data;

        if(!$envelopeId)
            $envelopeId = $this->_envelope_id;

        if($envelopeId)
            $params->idBufor = $envelopeId;

        return $this->_go('addShipment', $params);
    }

    public function createNewPackage($data, $retry = 0, $forceTtNumber = false, $guidPrefix = false, $addToWaitingLine = true, $automaticalySendFullEnvelopes = false, $envelopeId = false, $forOnwLabelsItems = false, $customDays = false)
    {
        $return = $this->_addShipment($data, $envelopeId);

        MyDump::dump('PP_LOG.txt', print_r($return,1));

        if(!$return['success'])
        {
            $errorCode = explode(' : ', $return['error']);
            // duplicated number for registered mail
            // try to gen new number
            if(($errorCode[0] == '81039' OR $errorCode[0] == '81042') && $retry < 1 && isset($data->numerNadania) && $data->numerNadania != '')
            {
                MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'RETRY 1');
                $retry++;

                $data->numerNadania = self::_getNumberNadaniaPrzesylkaFirmowa($this->getAcc(), true, $forceTtNumber);


                $data->guid = self::generateGuid($this->_local_id, $guidPrefix);
                return $this->createNewPackage($data, $retry, $forceTtNumber, $guidPrefix, $addToWaitingLine, $automaticalySendFullEnvelopes, $envelopeId, $forOnwLabelsItems, $customDays);
            }
            // incorrect envelope ID - clear cache and try to fix it
            else if(($errorCode[0] == '13216' OR $errorCode[0] == '13201') && $retry < 1)
            {
                MyDump::dump('pocztaPolska.txt', $this->_login.' :: '. 'RETRY 2');


                $data->numerNadania = self::_getNumberNadaniaPrzesylkaFirmowa($this->getAcc(), true, $forceTtNumber);


                $data->guid = self::generateGuid($this->_local_id, $guidPrefix);

                $retry++;

                $this->clearEnvelopeCache($forOnwLabelsItems, $customDays);
                $this->verifyAndFixEnvelope($forOnwLabelsItems, $customDays);

                return $this->createNewPackage($data, $retry, $forceTtNumber, $guidPrefix, $addToWaitingLine, $automaticalySendFullEnvelopes, $envelopeId, $forOnwLabelsItems, $customDays);
            }

            return $return;
        } else {
            if($addToWaitingLine)
                PocztaPolskaWaiting::addToWaitingLine($this->_type, $this->_type_id, $this->_local_id, $return['response']->guid, $return['response']->numerNadania, $this->_envelope_id, $this->_acc, S_Useful::workDaysNextDate(date('Y-m-d'), $customDays ? $customDays : self::DEFAULT_DAYS_DELAY));

            $this->_increaseEnvelopeCounter();

            if($automaticalySendFullEnvelopes)
                $this->verifyAndFixEnvelopeSend();

//
//            // don't download label when we import with forcing TT number
//            if($forceTtNumber)
//            {
//                $response = new stdClass();
//                $response->content = new stdClass();
//                $response->content->pdfContent;
//                $response->content->nrNadania = $return['response']->numerNadania;
//
//                return [
//                    'success' => true,
//                    'response' => $response,
//                ];
//            }



            if(!$forOnwLabelsItems) {
                $return = $this->_getAddresLabelByGuid($return['response']->guid);

                if (!$return['success']) {
                    $errorCode = explode(' : ', $return['error']);
                    // ERROR : Planowana data nadania jest w przeszłości
                    // clear envelope cache to prevent errors in future
                    if ($errorCode[0] == '79401') {
                        MyDump::dump('pocztaPolska.txt', $this->_login . ' :: ' . 'RETRY 3');
                        $this->clearEnvelopeCache(false, $customDays);
                    }

                }
            }

            return $return;
        }

    }

    protected static function adresTypeByAddressData(AddressData $addressData)
    {
        $model = new adresType();
        $model->nazwa = $addressData->getUsefulName(true);
        $model->ulica = trim($addressData->address_line_1.' '.$$addressData->address_line_2);
        $model->kodPocztowy = preg_replace("/[^a-zA-Z0-9]+/", "", $addressData->zip_code);
        $model->miejscowosc = $addressData->city;
        $model->kraj = $addressData->country0->name;
        $model->tel = $addressData->tel;
        $model->email = $addressData->email;

        return $model;

    }

    protected static function _preparePostalDataForType_przesylkaFirmowaPolecona(Postal $postal, $prio = false, $acc, $forceTtNumber = false, $guidPrefix = false)
    {
        $data = new przesylkaFirmowaPoleconaType();
        $data->adres = self::adresTypeByAddressData($postal->receiverAddressData);

        $data->guid = self::generateGuid(substr($postal->local_id, 0, -3), $guidPrefix);

        $data->gabaryt = 'GABARYT_A';

        $data->masa = $postal->weight;

        if(in_array($acc,[self::ACC_FIREBUSINESS, self::ACC_SP_NYSA, self::ACC_SP_ZABRZE]))
            $data->masa = self::_modifyWeight($data->masa);

        $data->numerNadania = self::_getNumberNadaniaPrzesylkaFirmowa($acc, true, $forceTtNumber);

        if($prio)
            $data->kategoria = 'PRIORYTETOWA';
        else
            $data->kategoria = 'EKONOMICZNA';

        return $data;
    }

    public static function createByPostal(Postal $postal, $operator_id, $operator_uniq_id = false, $forceTtNumber = false)
    {
        $customDays = $postal->user->getPPDaysDelay();


        $client = new self();
        $client->_type = PocztaPolskaWaiting::TYPE_POSTAL;
        $client->_type_id = $postal->id;
        $client->_local_id = $postal->local_id;

        try
        {
            if ($operator_id == PostalOperator::OPERATOR_PP_WROCLAW_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_WROCLAW, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, false, $client->getAcc(), $forceTtNumber);
            }
            else if($operator_id == PostalOperator::OPERATOR_PP_WROCLAW_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_WROCLAW, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, true, $client->getAcc(), $forceTtNumber);
            } else if ($operator_id == PostalOperator::OPERATOR_PP_NYSA_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_NYSA, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, false, $client->getAcc(), $forceTtNumber);
            }
            else if($operator_id == PostalOperator::OPERATOR_PP_NYSA_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_NYSA, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, true, $client->getAcc(), $forceTtNumber);
            } else if ($operator_id == PostalOperator::OPERATOR_PP_WARSZAWA_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_WARSZAWA, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, false, $client->getAcc(), $forceTtNumber);
            }
            else if($operator_id == PostalOperator::OPERATOR_PP_WARSZAWA_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_WARSZAWA, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, true, $client->getAcc(), $forceTtNumber);
            }
            else if ($operator_id == PostalOperator::OPERATOR_PP_BOCHNIA_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_BOCHNIA, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, false, $client->getAcc(), $forceTtNumber);
            }
            else if($operator_id == PostalOperator::OPERATOR_PP_BOCHNIA_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_BOCHNIA, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, true, $client->getAcc(), $forceTtNumber);
            }
            else if ($operator_uniq_id == PostalOperator::UNIQID_OPERATOR_PP_FIREBUSINESS_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_FIREBUSINESS, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, false, $client->getAcc(), $forceTtNumber);
            }
            else if($operator_uniq_id == PostalOperator::UNIQID_OPERATOR_PP_FIREBUSINESS_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_FIREBUSINESS, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, true, $client->getAcc(), $forceTtNumber);
            }
            else if ($operator_uniq_id == PostalOperator::UNIQID_OPERATOR_PP_SP_Nysa_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_SP_NYSA, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, false, $client->getAcc(), $forceTtNumber);
            }
            else if ($operator_uniq_id == PostalOperator::UNIQID_OPERATOR_PP_SP_Zabrze_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_SP_ZABRZE, false, $customDays);
                $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, false, $client->getAcc(), $forceTtNumber);
            }
            else if ($operator_uniq_id == PostalOperator::UNIQID_OPERATOR_PP_SP_Zabrze_Polecona_Firmowa_OWN_LABEL) {

                try {
                    $number = self::_getNumberNadaniaPrzesylkaFirmowa(self::ACC_SP_ZABRZE, true);
                }
                catch (Exception $ex)
                {
                    $data = [
                        'error' => $ex->getMessage(),
                    ];
                    return $data;
                }

                $label = self::generateLabelByOwn($number, 'Swiat Przesylek', 'Grodkowska 40', '48-300 Nysa', '', '416058/S', 'PP Zabrze S101', $postal->receiverAddressData, $customDays);

                $data = array(
                    'label' => $label,
                    'trackId' =>$number,
                );

                return $data;
            }

            if(!isset($data))
                throw new Exception('Unknow PocztaPolska operator!');
        }
        catch (Exception $ex)
        {
            $return = [
                'success' => false,
                'error' => $ex->getMessage(),
                'response' => NULL,
            ];
            return $return;
        }


        $return = $client->createNewPackage($data,0, $forceTtNumber, false, true, false, false, false, $customDays);

        MyDump::dump('pplog2.txt', print_r($return,1));
        MyDump::dump('pplog2.txt', print_r($return['error'],1));
        MyDump::dump('pplog2.txt', print_r(!$return['success'],1));

        if(S_Useful::sizeof2($return['error']) && !$return['success'])
        {
            $data = [
                'error' => $return['error']
            ];
            return $data;
        } else {

            $data = array(
                'label' => $return['response']->content->pdfContent,
                'trackId' => $return['response']->content->nrNadania,
            );

            return $data;
        }
    }

    protected static function _prepareCourierDataForType_przesylkaFirmowaPolecona(Courier $courier, AddressData $to, $prio = false, $acc)
    {
        $data = new przesylkaFirmowaPoleconaType();
        $data->adres = self::adresTypeByAddressData($to);


        $data->guid = self::generateGuid($courier->local_id);

        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_POTW_ODBIORU))
            $data->iloscPotwierdzenOdbioru = 1;

        $data->gabaryt = 'GABARYT_A';

        $data->masa = round($courier->getWeight(true) * 1000);

        if(in_array($acc,[self::ACC_FIREBUSINESS, self::ACC_SP_NYSA, self::ACC_SP_ZABRZE]))
            $data->masa = self::_modifyWeight($data->masa);

//        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_PRIO))
        if($prio)
            $data->kategoria = 'PRIORYTETOWA';
        else
            $data->kategoria = 'EKONOMICZNA';

        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_OKRESLONA_WARTOSC))
            $data->wartosc = $courier->getPackageValueConverted('PLN') * 100;

        if($courier->courierTypeInternal->cod_value > 0) {

            $data->pobranie = new pobranieType;
            $data->pobranie->kwotaPobrania = ($courier->courierTypeInternal->cod_value * 100); // wartosc podawana w groszach
            $data->pobranie->tytulem = 'MailSolution no '.$courier->local_id;
            $data->pobranie->sposobPobrania = 'RACHUNEK_BANKOWY';
            $data->pobranie->nrb = '26105014901000009137775475'; // SPRAWDZIĆ RACHUNEK!!!
        }

        $data->numerNadania = self::_getNumberNadaniaPrzesylkaFirmowa($acc, true);

        return $data;
    }

    protected static function _prepareCourierDataForType_paczkaPocztowa(Courier $courier, AddressData $to)
    {
        $data = new paczkaPocztowaType();
        $data->adres = self::adresTypeByAddressData($to);


        $data->guid = self::generateGuid($courier->local_id);

        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_POTW_ODBIORU))
            $data->iloscPotwierdzenOdbioru = 1;

        $dimensions = [
            $courier->package_size_l,
            $courier->package_size_d,
            $courier->package_size_w,
        ];

        // biggest dimension is length and is first
        rsort($dimensions);

        if(
            $dimensions[0] > 60 OR
            $dimensions[1] > 50 OR
            $dimensions[2] > 30
        )
            $data->gabaryt = 'GABARYT_B';
        else
            $data->gabaryt = 'GABARYT_A';

        $data->masa = round($courier->getWeight(true) * 1000);

        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_PRIO))
            $data->kategoria = 'PRIORYTETOWA';
        else
            $data->kategoria = 'EKONOMICZNA';

        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_OKRESLONA_WARTOSC))
            $data->wartosc = $courier->getPackageValueConverted('PLN') * 100;

//        if($courier->courierTypeInternal->cod_value > 0) {
//
//            $data->pobranie = new pobranieType;
//            $data->pobranie->kwotaPobrania = ($courier->courierTypeInternal->cod_value * 100); // wartosc podawana w groszach
//            $data->pobranie->tytulem = 'MailSolution no '.$courier->local_id;
//            $data->pobranie->sposobPobrania = 'RACHUNEK_BANKOWY';
//            $data->pobranie->nrb = '26105014901000009137775475'; // SPRAWDZIĆ RACHUNEK!!!
//        }

        return $data;

    }

//    protected static function _prepareCourierDataForType_pocztex24(Courier $courier, AddressData $to, $prio = false, $acc)
//    {
//        $data = new uslugaKurierskaType();
//        $data->adres = self::adresTypeByAddressData($to);
//
//
//        $data->guid = self::generateGuid();
//
//        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_POTW_ODBIORU))
//            $data->iloscPotwierdzenOdbioru = 1;
//
//        $data->gabaryt = 'GABARYT_A';
//
//        $data->masa = round($courier->package_weight * 1000);
//
////        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_PRIO))
//        if($prio)
//            $data->kategoria = 'PRIORYTETOWA';
//        else
//            $data->kategoria = 'EKONOMICZNA';
//
//        if($courier->courierTypeInternal->hasAdditionById(CourierAdditionList::PP_OKRESLONA_WARTOSC))
//            $data->wartosc = $courier->package_value * 100;
//
//        if($courier->courierTypeInternal->cod_value > 0) {
//
//            $data->pobranie = new pobranieType;
//            $data->pobranie->kwotaPobrania = ($courier->courierTypeInternal->cod_value * 100); // wartosc podawana w groszach
//            $data->pobranie->tytulem = 'MailSolution no '.$courier->local_id;
//            $data->pobranie->sposobPobrania = 'RACHUNEK_BANKOWY';
//            $data->pobranie->nrb = '26105014901000009137775475'; // SPRAWDZIĆ RACHUNEK!!!
//        }
//
//        $data->numerNadania = self::_getNumberNadaniaPrzesylkaFirmowa($acc, true);
//
//        return $data;
//    }
//
//    protected static function _prepareCourierDataForType_pocztex48(Courier $courier, AddressData $to, $prio = false, $acc)
//    {
//
//    }

    public static function createByCourier(Courier $courier, AddressData $from, AddressData $to, $operator_id, $operator_uniq_id = false)
    {

        $customDays = $courier->user->getPPDaysDelay();

        if($courier->getType() != Courier::TYPE_INTERNAL)
            throw new Exception('Only CourierTypeInternal is allowed for PocztaPolska!');

        $client = new self();
        $client->_type = PocztaPolskaWaiting::TYPE_COURIER;
        $client->_type_id = $courier->id;
        $client->_local_id = $courier->local_id;

        $oversized = false;

        try
        {
            if ($operator_id == CourierOperator::OPERATOR_PP_WROCLAW_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_WROCLAW, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, false, $client->getAcc());
            }
            else if ($operator_id == CourierOperator::OPERATOR_PP_WROCLAW_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_WROCLAW, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, true, $client->getAcc());
            }
            else if ($operator_id == CourierOperator::OPERATOR_PP_NYSA_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_NYSA, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, false, $client->getAcc());
            }
            else if ($operator_id == CourierOperator::OPERATOR_PP_NYSA_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_NYSA, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, true, $client->getAcc());
            }
            else if ($operator_id == CourierOperator::OPERATOR_PP_BOCHNIA_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_BOCHNIA, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, false, $client->getAcc());
            }
            else if ($operator_id == CourierOperator::OPERATOR_PP_BOCHNIA_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_BOCHNIA, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, true, $client->getAcc());
            }
            else if ($operator_id == CourierOperator::OPERATOR_PP_WARSZAWA_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_WARSZAWA, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, false, $client->getAcc());
            }
            else if ($operator_id == CourierOperator::OPERATOR_PP_WARSZAWA_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_WARSZAWA, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, true, $client->getAcc());
            }
            else if ($operator_id == CourierOperator::OPERATOR_PP_WROCLAW_PaczkaPocztowa) {
                $client->_setAccount(self::ACC_WROCLAW, false, $customDays);
                $data = self::_prepareCourierDataForType_paczkaPocztowa($courier, $to);
                $oversized = true;
            }
            else if ($operator_uniq_id == CourierOperator::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa) {
                $client->_setAccount(self::ACC_FIREBUSINESS, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, false, $client->getAcc());
            }
            else if ($operator_uniq_id == CourierOperator::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa_PRIO) {
                $client->_setAccount(self::ACC_FIREBUSINESS, false, $customDays);
                $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courier, $to, true, $client->getAcc());
            }
//        else if ($operator_uniq_id == CourierOperator::UNIQID_PP_FIREBUSINESS_Pocztex24) {
//            $client->_setAccount(self::ACC_FIREBUSINESS);
//            $data = self::_prepareCourierDataForType_pocztex24($courier, $to, true, $client->getAcc());
//        }
//        else if ($operator_uniq_id == CourierOperator::UNIQID_PP_FIREBUSINESS_Pocztex48) {
//            $client->_setAccount(self::ACC_FIREBUSINESS);
//            $data = self::_prepareCourierDataForType_pocztex48($courier, $to, true, $client->getAcc());
//        }

            if(!isset($data))
                throw new Exception('Unknow PocztaPolska operator!');
        }
        catch (Exception $ex)
        {
            $return = [
                'success' => false,
                'error' => $ex->getMessage(),
                'response' => NULL,
            ];
            return $return;
        }


        $response = [];

        for($i = 1; $i <= $courier->packages_number; $i++) {
            $return = $client->createNewPackage($data,0, false, false, true, false, false, $customDays);

            if(S_Useful::sizeof2($return['error']) && !$return['success'])
            {
                $answ = [
                    'error' => $return['error']
                ];

            } else {

                $answ = array(
                    'label' => $return['response']->content->pdfContent,
                    'track_id' => $return['response']->content->nrNadania,
                    'oversized' => $oversized,
                );
            }

            $response[] = $answ;
        }

        return $response;
    }

    public static function createByCourierU(CourierTypeU $courierU)
    {

        $customDays = $courierU->user->getPPDaysDelay();

        $oversized = false;

        $client = new self();

        $client->_type = PocztaPolskaWaiting::TYPE_COURIER;
        $client->_type_id = $courierU->courier->id;
        $client->_local_id = $courierU->courier->local_id;


        try
        {
            switch($courierU->courierUOperator->uniq_id)
            {
                case CourierUOperator::UNIQID_PP_WROCLAW_POLECONA_STD:
                    $client->_setAccount(self::ACC_WROCLAW, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, false, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_WROCLAW_POLECONA_PRIO:
                    $client->_setAccount(self::ACC_WROCLAW, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, true, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_NYSA_POLECONA_STD:
                    $client->_setAccount(self::ACC_NYSA, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, false, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_NYSA_POLECONA_PRIO:
                    $client->_setAccount(self::ACC_NYSA, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, true, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_BOCHNIA_POLECONA_STD:
                    $client->_setAccount(self::ACC_BOCHNIA, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, false, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_BOCHNIA_POLECONA_PRIO:
                    $client->_setAccount(self::ACC_BOCHNIA, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, true, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_WARSZAWA_POLECONA_STD:
                    $client->_setAccount(self::ACC_WARSZAWA, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, false, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_WARSZAWA_POLECONA_PRIO:
                    $client->_setAccount(self::ACC_WARSZAWA, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, true, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_FIREBUSINESS_POLECONA_STD:
                    $client->_setAccount(self::ACC_FIREBUSINESS, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, false, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_FIREBUSINESS_POLECONA_PRIO:
                    $client->_setAccount(self::ACC_FIREBUSINESS, false, $customDays);
                    $data = self::_prepareCourierDataForType_przesylkaFirmowaPolecona($courierU->courier, $courierU->courier->receiverAddressData, true, $client->getAcc());
                    break;
                case CourierUOperator::UNIQID_PP_WROCLAW_PACZKA:
                    $client->_setAccount(self::ACC_WROCLAW, false, $customDays);
                    $data = self::_prepareCourierDataForType_paczkaPocztowa($courierU->courier, $courierU->courier->receiverAddressData);
                    break;
            }

            if(!isset($data))
                throw new Exception('Unknow PocztaPolska operator!');


            if(!isset($data))
                throw new Exception('Unknow PocztaPolska operator!');

        }
        catch (Exception $ex)
        {
            $return = [
                'success' => false,
                'error' => $ex->getMessage(),
                'response' => NULL,
            ];
            return $return;
        }


        $response = [];

        for($i = 1; $i <= $courierU->courier->packages_number; $i++) {
            $return = $client->createNewPackage($data, 0, false, false, true, false, false, false, $customDays);

            if(S_Useful::sizeof2($return['error']) && !$return['success'])
            {
                $answ = [
                    'error' => $return['error']
                ];

            } else {

                $answ = array(
                    'label' => $return['response']->content->pdfContent,
                    'track_id' => $return['response']->content->nrNadania,
                    'oversized' => $oversized,
                );
            }

            $response[] = $answ;
        }

        return $response;
    }

    public static function generateGuid($base, $prefix = false)
    {
        if(!$prefix)
            $prefix = 'AAA';

        return str_pad($base.$prefix.uniqid(), 32, "0", STR_PAD_LEFT);
    }

    public static function sendDataToPpByReadyItems(array $postals)
    {
        /// JUST FOR ONE ACCOUNT NOW!


        /* @var $postals Postal[] */
        $client = new self(false, true);
        $client->_setAccount(self::ACC_SP_ZABRZE, true);
        $client->clearEnvelopeCache(true);
        $client->verifyAndFixEnvelope(true);

        if(sizeof($postals) > 500)
            throw new Exception('Too many items!');


        $resp = [];
        foreach($postals AS $postal)
        {
            if($postal->postalOperator->uniq_id != PostalOperator::UNIQID_OPERATOR_PP_SP_Zabrze_Polecona_Firmowa_OWN_LABEL)
                continue;

            $client->_type = PocztaPolskaWaiting::TYPE_POSTAL;
            $client->_type_id = $postal->id;
            $client->_local_id = $postal->local_id;

            $data = self::_preparePostalDataForType_przesylkaFirmowaPolecona($postal, false, $client->getAcc(), $postal->postalLabel->track_id);

//            $resp[] = $client->createNewPackage($data,0, $postal->postalLabel->track_id, false, false, true, false, true);
            $resp[$postal->local_id] = $client->createNewPackage($data,0, $postal->postalLabel->track_id, false, false, false, false, true);
        }

        $resp['ENVELOPE'] = $client->sendEnvelope();
        return $resp;
    }


//    public static function test()
//    {
//
//
//        $client = new self();
//        $client->_setAccount(self::ACC_WROCLAW);
//
//
//        $client->_envelope_id = false;
//
//        $data = new przesylkaFirmowaPoleconaType();
//        $data->guid = self::generateGuid();
//
//        $nn = '0045900773109922814';
//
//        $data->numerNadania = $nn.self::_calculateControlDigit($nn);
//        $data->adres = new adresType();
//        $data->adres->nazwa = 'Jan Nowak';
//        $data->adres->ulica = 'Testowa 2/5a';
//        $data->adres->kodPocztowy = '00001';
//        $data->adres->miejscowosc = 'Warszawa';
//
//        return $client->createNewPackage($data);
//    }



    public static function track($id)
    {
        $CACHE_NAME = 'PP_TRACK_CHECK_'.$id;
        $CACHE = Yii::app()->cacheFile;

        // @20.02.2019 - check PP items once a day max - to prevent their server crash
        if($CACHE->get($CACHE_NAME))
        {
            MyDump::dump('pp_tt_ignore.txt', $id);
            return false;
        }
        $CACHE->set($CACHE_NAME, 10, 60*60*24);


        $throttle = new Stiphle\Throttle\LeakyBucket;

        if(Yii::app()->params['console'])
            $storage = New Stiphle\Storage\Process();
        else
            $storage = New Stiphle\Storage\Apcu();

        $throttle->setStorage($storage);

        $throttled = $throttle->throttle('POCZTA_POLSKA_TT_REQUEST', 1, 1000);
        if($throttled)
            MyDump::dump('pp_tt_throttled.txt', print_r($throttled,1));

        $username = 'sledzeniepp';
        $password = 'PPSA';

        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 0,
            'stream_context' => stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))),
            'connection_timeout' => 120,
        );

        $wsse_header = new WsseAuthHeader($username, $password);
        $client = new SoapClient('https://tt.poczta-polska.pl/Sledzenie/services/Sledzenie?wsdl', $mode);
        $client->__setSoapHeaders(array($wsse_header));

        try {

            $resp = $client->__soapCall('sprawdzPrzesylke', ['sprawdzPrzesylke' => ['numer' => $id]]);
        }
        catch (Exception $ex)
        {
//            var_dump($ex);
        }

        if($resp->return->status == -2)
            return false;


        $data = [];
        $history = $resp->return->danePrzesylki->zdarzenia->zdarzenie;
        if(!is_array($history))
            $history = [ $history ];

        foreach($history AS $item)
        {
            $temp = array(
                'name' => $item->nazwa,
                'date' => $item->czas,
                'location' => $item->jednostka->nazwa,
            );

            $data[] = $temp;
        }

        return $data;
    }


    public static function _getNumberNadaniaPrzesylkaFirmowa($acc, $useNumber = false, $forceTtNumber = false)
    {

        if($acc == self::ACC_SP_ZABRZE)
            $acc = self::ACC_SP_NYSA; // common numbers


        if($forceTtNumber)
            return $forceTtNumber;

        if(self::TEST_MODE)
            return '';

        /* @var $cmd CDbCommand */

        if($useNumber)
            $transaction = Yii::app()->db->beginTransaction();

        $sql = 'SELECT * FROM poczta_polska_numery_nadania WHERE id_acc = :acc';

        if($useNumber)
            $sql .= ' FOR UPDATE';

        $cmd = Yii::app()->db->createCommand($sql);
        $cmd->bindParam(':acc', $acc);
        $result = $cmd->queryRow();

        $current = $result['current'];
        $prefix = $result['prefix'];
        $end = $result['end'];

        if($current > $end) {
            if($useNumber)
                $transaction->rollback();

            throw new Exception('No more PocztaPolska numbers!');
        }

        if($useNumber)
        {
            $next = $current + 1;
            $sql = 'UPDATE poczta_polska_numery_nadania SET current = :current WHERE id_acc = :acc';

            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindParam(':acc', $acc);
            $cmd->bindParam(':current', $next);
            $cmd->execute();
        }

        if($useNumber)
            $transaction->commit();

        $number = $prefix.$current;
        $number .= self::_calculateControlDigit($number);

        return $number;
    }

    protected static function _calculateControlDigit($base)
    {
        $strlen = strlen($base);

        $sum = 0;
        for ($i = 0; $i < $strlen; $i++) {
            $num = $base[$i];
            $sum += (($i % 2 == 1) ? 1 : 3) * $num;
        }

        $number = (10 - $sum % 10) % 10;

        return $number;
    }

    public function setNewPassword($newPassword)
    {

//        return false;


        // don't change password since this ACC is common with ACC_SP_NYSA - change password there
        if($this->_acc == self::ACC_SP_ZABRZE)
            return true;


        return $this->_go('changePassword', ['newPassword' => $newPassword], true);
    }

    protected static function _getSpLogo()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAAJUAAABjCAMAAAClt7J5AAABblBMVEX///8AAAD9/v4DBAP7+/zr6ur9/P0hHh8JCQkGBgb9+/1kY2RQUFD6+vo8PDzp6ek2NjYNDA309PSfnJ4eHR329vZrbGw6Ojo4ODgYGBjm5ea7urxhYWEbHBv//v7n5+eUlJVwb3BAQEAeHx4KCgpZWFk+Pj8oKCj49/gQEBDy8/N5eHkTFBPv7+/Y2djBwMCfn6BycnNUU1QuLy4WFRbx8fCdmpxpaWlnZ2hbW1sSERLs7OzKysrHxsZubW5KSUpERERCQkIZGhrg39/W1tarqquoqKiEgoMsLCwlJSXj4+ScnZ6OjY6BgIEiIiLd3N21tbavr6+trK2Yl5d3d3ddXV5HRkczNDTU1NWSkZKFhYZ/fX9NTE0xMTHa2tu8vL2ysrKjoqOLiot1dHXu7u7h4eLPz8/NzM3DwsK+vr6JiIjIx8jEw8W4uLiZmZpSUVPv8fHS0tOhoaF8e3yPj5BkZWVXVlY6OjaoqqqnpaYgJECtAAAMfUlEQVRo3sTYe1MSURgG8OfZxWLHtoCyzLICLdFuTqYlkjcUgQhQZLyWpSRi2mTpTB+/ZS+Hs+Q42ezO/v6EvTxz9t1z3rP4X0pfuQudQoXMcwSoMpMa7L/9wh10cW44WtrrQ1B2NbbcOkBbKE5Tbh/ByGq0lAoQNmhbG0cQRsboWIXjxi06HiEIOxSGRmD7TWFOQQAmKEQXYFui0F9AAKoU1DRstymkPiEAPykMiwBFCvk6AtDQ6IjosB2rdFxDEPQZOqpw9KzRsYxANF7TMtsD4V6UlpqOYDSHaNC2+yBZ7GfL/XEE5eHpRzKRhEumRo5VEKSf5NBzuL0htxGoLVKtwGUkT+ZiCJAyQ3IaLvdUMlVAgJI5/jUweyS1E5xLgbe+3tzbWo6hw42rrQjfIInlaMj+lSZdjBfT8JTyvp+GO2kokKU1Gn5BchSmYQtu9bNBktHuH14O14ZK03wDLos0w+po22TLGVxi284y1APPnERpm9UhO2VLdB2C/sQ67gFkRTqK8EycDrXpqpa3NH2HsBBmy0BMQdt4Xm4QPRKKUFhypZqR2065b0+VIXnnasU8YL9pwqRcrrGxzgwP7JzqESQrpPetxFSJQrec6uswLaOwFV7TkoXkM9sq8IjyhMIpJA2VlsdQOnYTm5Dsq3SE1+GVzfZF9899MvNdsEy2n7Qk2UtHbxJeKdyi7ez8tOqyXYEDtF0PueJrtC3CO8d2AQ3cgKybIm1nAQ3VIavRsqrAQ0cJjVSpfXetsHPtED86Nl7DZUgW8tZRj67AU8n9lXuH5PCB/FueDnUHhpEhCk201SPk3FH2ZR3eEvN2PgOhnKLw1BxRlUIVgl4TL68P9AjJRAyOl1KIsR6zM5VjCrsktTT8EqdhWtRGlm1aQ94MylMYlsOt1DH45ZgGtercb4mSTaARZttaEpb1fhrewjd95g2iTXttvk9JIoldSuanYJqKiBXIF2LtLX0yU11JUBJdUGYpGVxHS2jS+jcD/xRpmhuHoStP2WjfIGUVczwP7TNC8M+3ME3TDwBkUpRtT9BlAoYV+4QP8JGekz56HqiU3V2jSxzAyV2a1JfwkSjw6A6wwQtdA6Z6aSlNwU9fSDoVv8oL9UKfpO0VfFUWFT07UuOF7j48pGMUvtITdHR/5IW01bA0S/jrFwWV/yoSgr+aKi8vDp91DfDyPsNnyite2rOH8FuVl/aneTN/SxsN4vhMElOwXIKUG5Uqp8ghgogg3gdetYq361GtW229Wm3/+51JgoCt+7S7W579/uAzhOTNJzPzDknesSrD79aNDn9Vx/DbFR7EX5S+CL9fr/Dv5UofTzeRh1rx6v0NPq9IsJQw89NqHuvyQgu04MIfqjdUOprkWXo6BTJ8GsGajqAFkqv4ncTldPeZDCAdzJRMHS8hNgNnFlTVcQCt0Ien5ai8k4gCtN9Uvmx2qG+T0mLJan0lIisgQStU1DeEbXN4dQHAPlrJz/XW33H1IW4W4cinPhi2RNZ5LWyd3uQpgHBxnH6tgtapGHjWf1BW3qS2Rhv8YGU4fm+HtsTSbkwjaqZiVaekmDhhhtbom2X3D7uxzXNXpTSy5PA5Kswk4U0JWiTZurJaMvWjItOr56hYfV1+P7RIb5Wp9izVBqQbbAFapCV8nkqfmVuE2QhquoKWafZHVL2uQUNq9u3l+8sBGDjfCeha8HTzLJXzle51KBDb3p7xrO2kcnuDro5hyM8PH32NhcSWUmkRzAwG06Xuw6Xju1y+Ot+vE5uyfeJ+Z/f+dcsiKMDbzXHv0uzsYixvCOZPxGfnYIdpIwstkgB/vLwyzbl6kVXtweeoWH3QMu1iXWOp56hanu2tpBL+Sb0a6/n9vhJ+heq5vPLCPf5nVXTq9vZQhp/RdueLR3lnXzxR5zqUHj/k4F8qjxjzw8/I2tVWkzluNbc1yxyFIXPNHhLgX+ndPM63wf9NCREr8P+SwEVo3P405YXnZ4Cg/RV+cp4IvzavtDPLtyUPGbLU3i4I7VK7LAtSu6R8BFK7JPlBkNtZkiDRjiSBJwdvFdpBtdiU6XtBFsgiGwDsU6Nu+s7Y3i4LgjJoO5sg1IaTZR4TgGyjCitr+e0pT0RcfZNw4bAMy2GvZSMM6xbnzaHzIXAKAO7qg2kUJsedzgen07H5CcCfszgcjvsjCaIGp8MU3Og2A0he2uh0WJZg0ekgiwzI7kd0nSdRiFmcswDvTc69gasHyzaQ1p0PDueD5dvZg/PhTzgzPThTfBnmWGDvVqg3CW4WbhDnwvEXSA8BJsStt4jiACcdIm7DlIiq7gAEp2r2yNeuxzZt46Bq3kNONQy15QyvsItoEuATovPUpTVt7aGiDzz6NHDhswEIBYe4H+jfFSBsQdS7+HSFfpyPuiO0LGMfpNUXGqSXqYgOczAQ0SNJFMkFgokM/ngefU2WjqygXRrz+Xy0MQ+vaNuEz1eajCBG+On+4yXiCzN8QUy969ReShrorHTAp3PuvrDTMINRbuBy9AX3jzOjSkfqB3OINrfNkZtuRMRLcthylGh0TPVFaR2yFy/36CSHiUmVypKg8detNFx6NM+AEH0XH8iQW5nKtRKPhyt0WR5eaPHGabeisEl8TJXSqILX19dGD1NxAyzH1T64lvsw/vFqCVYRxSL00UjuMew4WOVr6xpBB3SrVJKDb+MWALhfpz8OoFIFgFgM16/Zj3y9t1r3U2SKqXwF9ceps2AMITp4wexbVwYjC+YGqvFa092hl8/OWTW/lfPtt530KcuRR/AhHZs25lEcrXCf0KQO8zUqt6/WePOSqMx1qhTiGDthGLb06lqzn0DLoPiqTaXq2PL3bMx6OMNSCTpI6CKqk3dr12DgT1pT+mefFlbr4GopEBQMdzBFye6MAyuG6OE7lVfvRYzVqC5EnCDQRiq/QpV7pCqK5AruIaUdu0Hx1TttFdgrAOtcRMMORRyYam5Zt1DzFVOJZcIfAFZPeeO4uui6AGOZ50tUW6mZzmEE76eJX6PiZE+LOKxRddV8NS5QZL9wtr8cIq+JF2qXSu+UQpWZcg+p7Tx3wCp04vIYNxWtEDjqvFamcpi7rOBpenY0W0Z27zFGdqKXV/jMKne3Acu4t0jXzlS9B0AuEXdGMCA1UPmD5A2DSJHnydMfIqscVlfFgkaFSsz0n2grY+uSUgh4qmYW/H28jLmlzUFX5lONKgWq3D2Wqk1m6y1P7TErwJkeT5y4pNvvQdGjUUkm1HtC2Dn0xFcsyxBnO2tzira2UXSOgalY3to/ndzJtVtYhyQFRI6jQsU6Jk+Qftj7amOsHj8s+HBsWb824rrCkUktgoUJzExuon600VcKVaZvBZgq5NJeCR1ydqtU+n0L8xlT6mq4oHZM7oBQnKjXqw6LZUb1lfjDl80zHURwwf1nnZGRYqY3hMt2jaqox/kwDZFsoJKJav+Gi8UQUfVsUMHTGnocRpXK5Q4bH1eklt0AXKfwI4C5sV6Fw5Liq7yFHfhUsn9R6Q0SykQdcs+hHjeNWgRt5N1RAx/FZ808RjAIUKOiHcuSANF5ptOoCqDIaLxXOxKFK8QJOtjNtb2pXnEVXf+uFTF+O1z2hPc5tJyuSOWCJyVovuL5pReR0r0524NCjapE7hxZUOqh7kyj8nG9Ov3srZ6uRNQHw5zaueJu8FVA0KhW6VDx/Lumjls++cSp8qiQ5gEwVaOqoqrltnq9kk2NVMOFDNdhKCE6jTVf8X47StNcVW0TG6aga1Q9T6neRF2IJ023XQOUU7s8R1yn2g9xTO045QiuxJcRvZ5dMt83zMFHXylVVBjnGTcU4gBqVJ1WtYqKh3D1HRWnUL2KikQFe0+7aYwOxBcVh7L5kHkYB2dUX02eKQet6BCn63nlb/YVX8a8NUFRiCWTyXOmimwnk4nTEQKy+Ri5iaqcTE7bCTZEe0/9iYhrkNXauOr6Wm9xvOnlXZjtQvXV6RqhrUDBRx5unoOarzqZappr+zSqSjMV6wpOVEPHdSNXpyKNuNOoaCehUB1EOGkaJcREJHnpmMIIN5oVEfUHSmUWJ0vKfJICiGM8FzrUCFoQnSqVix+XB0TE2DQ23fUxXtyAJP1nrWyEmCqDrH53812fFKTMHYImrQ5XvWsCR/Or7agA8SPbVwryVtZ2FE1ks4cSwHk2uwrFiu1bWIngaqWy6lfCP5PNFsE4bcv+sVKx2WxJWyUBxSwbtgRAONlXTV0CiQ6uvJEA7F/5y8qM/c+sjZTd6qI/kwCjFdttA9VfeRrKFsQHbMAAAAAASUVORK5CYII=';

        return base64_decode($data);
    }

    public static function generateLabelByOwn($ttNo, $senderName, $senderStreet, $senderZipCity, $contractDate, $contractId, $contractPlace, AddressData $receiver, $customDays = false)
    {
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pdf->addPage('H', array(126, 71));

        $pdf->SetFont('freesans', '', 5);
        $pdf->MultiCell(20, 5, 'Nadawca:', 0, 'L', false, 1, 0, 8.7,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        // NADAWCA:
        $text = $senderName.', ';
        $text .= $senderStreet.', ';
        $text .= $senderZipCity;

        $pdf->SetFont('freesans', 'B', 5);
        $pdf->MultiCell(50, 20, $text, 0, 'L', false, 1, 0, 12,
            true,
            0,
            false,
            true,
            20,
            'T',
            true
        );

        $pdf->Image('@'.self::_getSpLogo(),12, 1, 15);


        $sendDate = S_Useful::workDaysNextDate(date('Y-m-d'), $customDays ? $customDays : self::DEFAULT_DAYS_DELAY);

        $pdf->SetFont('freesans', 'B', 6);
        $text = 'PRZESYŁKA NIESTEMPLOWANA'."\r\n";
        $text .= "OPŁATA POBRANA\r\nUmowa z Pocztą Polską S.A. ID nr ".$contractId."\r\n";
        $text .= 'Nadano w '.$contractPlace."\r\nDnia ".$sendDate."\r\n";

        $pdf->MultiCell(45, 12, $text, 0, 'C', false, 1, 73, 0,
            true,
            0,
            false,
            true,
            12,
            'T',
            true
        );


        $pdf->SetFont('freesans', 'B', 8);
        $text = 'F eCommerce';
        $pdf->MultiCell(45, 5, $text, 0, 'C', false, 1, 73, 12,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(10, 10, 'F', 0, 'C', false, 1, 27, 45,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 25);
        $pdf->MultiCell(10, 10, 'R', 0, 'C', false, 1, 65, 27,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        /* @var $pdf TCPDF */

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

//        $code = '00759007731514226715';
        $pdf->write1DBarcode($ttNo, 'C128', 75, 27, 53, 13, 1, $barcodeStyle);

        $pdf->SetFont('freesans', '', 8);

        $pdf->MultiCell(40, 10, '('.substr($ttNo,0,2).') '.substr($ttNo,2,8).' '.substr($ttNo,10,1).' '.substr($ttNo,11,8).' '.substr($ttNo,19,1), 0, 'C', false, 1, 76, 39,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );




        $pdf->SetFont('freesans', 'B', 25);
        $text = $receiver->getUsefulName(true)."\r\n";
        $text .=  trim($receiver->address_line_1.' '.$receiver->address_line_2)."\r\n";
        $text .= $receiver->zip_code.' '.$receiver->city;
        $pdf->MultiCell(75, 30, $text, 0, 'l', false, 1, 50, 50,
            true,
            0,
            false,
            true,
            30,
            'T',
            true
        );

        $pdfString = $pdf->Output('asString', 'S');
//
        return $pdfString;

        $img = new Imagick;
        $img->setResolution(300, 300);
        $img->readImageBlob($pdfString);
        $img->setImageFormat('png8');
        if ($img->getImageAlphaChannel()) {
            $img->setImageBackgroundColor('white');
            $img->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $img->rotateImage(new ImagickPixel(), 90);

//        $img->trimImage(0);
        $img->stripImage();

        return $img->getImageBlob();
    }

}



class WsseAuthHeader extends SoapHeader {

    private $wss_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    function __construct($user, $pass, $ns = null) {
        if ($ns) {
            $this->wss_ns = $ns;
        }

//        MyDump::dump('pocztaPolska.txt', 'LOGIN HEADER - USER: '.print_r($user,1));
//        MyDump::dump('pocztaPolska.txt', 'LOGIN HEADER - PASS: '.print_r($pass,1));

        $auth = new stdClass();
        $auth->Username = new SoapVar($user, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
        $auth->Password = new SoapVar($pass, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);

        $username_token = new stdClass();
        $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns);

        $security_sv = new SoapVar(
            new SoapVar($username_token, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns),
            SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'Security', $this->wss_ns);
        parent::__construct($this->wss_ns, 'Security', $security_sv, true);
    }
}





class adresType
{
    public $nazwa;
    public $nazwa2;
    public $ulica;
    public $numerDomu;
    public $numerLokalu;
    public $miejscowosc;
    public $kodPocztowy;
    public $kraj;
    public $telefon;
    public $email;
    public $mobile;
    public $osobaKontaktowa;
    public $nip;
}

abstract class przesylkaType
{
    public $guid;
}

abstract class przesylkaRejestrowanaType extends przesylkaType
{
    public $numerNadania;
    public $adres;
    public $sygnatura;
    public $terminSprawy;
    public $rodzaj;
}

abstract class przesylkaPoleconaKrajowaType extends przesylkaRejestrowanaType
{
    public $posteRestante;
    public $iloscPotwierdzenOdbioru;
    public $kategoria;
    public $gabaryt;
    public $masa;
    public $epo;
    public $miejscowa;
    public $obszarMiasto;
    public $zasadySpecjalne;
}

class przesylkaFirmowaPoleconaType extends przesylkaPoleconaKrajowaType
{
    public $miejscowa;
    public $obszarMiasto;
    public $gabaryt;
    public $zasadySpecjalne;
}

class paczkaPocztowaType extends przesylkaRejestrowanaType
{
    public $posteRestante;
    public $iloscPotwierdzenOdbioru;
    public $kategoria;
    public $gabaryt;
    public $wartosc;
    public $masa;
    public $zwrotDoslanie;
    public $egzemplarzBiblioteczny;
    public $dlaOciemnialych;
    public $epo;
    public $zasadySpecjalne;
}

class pobranieType
{
    public $sposobPobrania;
    public $kwotaPobrania;
    public $nrb;
    public $tytulem;
    public $sprawdzenieZawartosciPrzesylkiPrzezOdbiorce;
}

//class uslugaKurierskaType extends przesylkaRejestrowanaType
//{
//    public $masa;
//    public $wartosc;
//    public $ponadgabaryt;
//    public $odleglosc;
//    public $zawartosc;
//    public $sprawdzenieZawartosciPrzesylkiPrzezOdbiorce;
//    public $ostroznie;
//    public $uiszczaOplate;
//    public $termin;
//    public $opakowanie;
//    public $numerPrzesylkiKlienta;
//}
//
//class przesylkaBiznesowaType extends przesylkaRejestrowanaType
//{
//public $masa;
//public $gabaryt;
//public $wartosc;
//public $ostroznie;
//public $pobranie;
//}

//class buforType
//{
//    public $idBufor;
//    public $dataNadania;
//    public $urzadNadania;
//    public $active;
//    public $opis;
//}
//
//class urzadNadaniaType
//{
//    public $id;
//}