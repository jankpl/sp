
<?php

/* @var $model Courier */

$this->menu = array(
    array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('index')),
    array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<h2><?php echo Yii::t('courier', 'Create Courier Domestic'); ?></h2>
<div class="form" id="domestic-form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'stateful'=>true,
    ));
    ?>

    <?php if(Yii::app()->user->hasFlash('previousModel'))
    {
        echo TbHtml::alert(TbHtml::LABEL_COLOR_SUCCESS, Yii::app()->user->getFlash('previousModel'), array( 'closeText' => false));
    }
    ?>
    <fieldset><legend><?= Yii::t('courier','Operator');?></legend>
        <div class="inline-form">
            <div class="row">
                <?php echo $form->labelEx($model->courierTypeDomestic,'domestic_operator_id'); ?>
                <?php echo $form->dropDownList($model->courierTypeDomestic, 'domestic_operator_id', CHtml::listData(CourierDomesticOperator::getActiveOperators(),'id','courierDomesticOperatorTr.title'), array('prompt' => Yii::t('courierDomestic', 'Wybierz operatora'), 'data-additions-dependency' => 'true', 'id' => 'domestic_operator_id_selector')); ?>
                <?php echo $form->error($model->courierTypeDomestic,'domestic_operator_id'); ?>
            </div><!-- row -->
        </div>
    </fieldset>
    <fieldset><legend><?php echo Yii::t('courier','Package data');?></legend>
        <div class="inline-form">
            <div style="float: left; width: 48%;">
                <div class="row">
                    <?php echo $form->labelEx($model,'package_weight'); ?>
                    <?php echo $form->textField($model, 'package_weight', array('data-additions-dependency' => 'true')); ?> <?php echo Courier::getWeightUnit();?>
                    <?php echo $form->error($model,'package_weight'); ?>
                </div><!-- row -->
                <div class="row" data-pallette-ignore="true">
                    <?php echo $form->labelEx($model,'package_size_l'); ?>
                    <?php echo $form->textField($model, 'package_size_l', array('data-additions-dependency' => 'true')); ?> <?php echo Courier::getDimensionUnit();?>
                    <?php echo $form->error($model,'package_size_l'); ?>
                </div><!-- row -->
                <div class="row" data-pallette-ignore="true">
                    <?php echo $form->labelEx($model,'package_size_w'); ?>
                    <?php echo $form->textField($model, 'package_size_w', array('data-additions-dependency' => 'true')); ?> <?php echo Courier::getDimensionUnit();?>
                    <?php echo $form->error($model,'package_size_w'); ?>
                </div><!-- row -->
                <div class="row" data-pallette-ignore="true">
                    <?php echo $form->labelEx($model,'package_size_d'); ?>
                    <?php echo $form->textField($model, 'package_size_d', array('data-additions-dependency' => 'true')); ?> <?php echo Courier::getDimensionUnit();?>
                    <?php echo $form->error($model,'package_size_d'); ?>
                </div><!-- row -->
            </div>
            <div style="float: right; width: 48%;">
                <div class="row" data-packages-no="true">
                    <?php echo $form->labelEx($model,'packages_number'); ?>
                    <?php echo $form->dropDownList($model, 'packages_number', S_Useful::generateArrayOfItemsNumber(1,10)); ?>
                    <?php echo $form->error($model,'packages_number'); ?>
                </div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($model,'package_value'); ?>
                    <?php echo $form->textField($model, 'package_value', array('data-additions-dependency' => 'true')); ?> PLN
                    <?php echo $form->error($model,'package_value'); ?>
                </div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($model,'package_content'); ?>
                    <?php echo $form->textField($model, 'package_content', array('maxlength' => 256)); ?>
                    <?php echo $form->error($model,'package_content'); ?>
                </div><!-- row -->

                <div id="cod-row">
                    <div class="row">
                        <?php echo $form->labelEx($model,'cod_value'); ?>
                        <?php echo $form->textField($model, 'cod_value', array('maxlength' => 256, 'data-additions-dependency' => 'true', 'data-cod-info' => true, 'data-cod-value' => true)); ?> <span id="currency"><?php echo Currency::PLN;?></span>
                        <?php echo $form->error($model,'cod_value'); ?>
                        <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('courier','Paczka będzie automatycznie ubezpieczona do kwoty równiej kwocie COD.'), array('closeText' => false, 'id' => 'cod-info'));?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccount'); ?>
                        <?php echo $form->textField($model->senderAddressData, '['.UserContactBook::TYPE_SENDER.']bankAccount', array('maxlength' => 50, 'data-bank-account-target' => true)); ?> <input type="button" class="btn btn-xs btn-primary" value="<?= Yii::t('courier', 'Z ustawień');?>" data-copy-bank-account="<?= Yii::app()->user->model->getBankAccount();?>"/>
                        <?php echo $form->error($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccount'); ?>
                        <div style="font-style: italic; font-size: 0.8em; text-align: center;"><?= Yii::t('courier', 'Nadawca przesyłki musi być właścicielem powyższego konta bankowego.');?></div>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                        <?php echo $form->checkBox($model->senderAddressData, '['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                        <?php echo $form->error($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                    </div>
                </div>

            </div>
            <div style="clear: both;"></div>
        </div>
    </fieldset>

    <div class="row">
        <?php
        $this->Widget('AddressDataSwitch', ['attrBaseName' => 'CourierTypeDomestic_AddressData']);
        ?>
        <div class="col-xs-6">
            <fieldset><legend><?= Yii::t('courier','Nadawca');?></legend>
                <?php
                $this->renderPartial('create/_addressDataForm/_form',
                    array(
                        'model' => $model->senderAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems('sender'),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => 'sender',
                        'type' => 'sender',
                    )
                );
                ?>
            </fieldset>
        </div>
        <div class="col-xs-6">
            <fieldset><legend><?= Yii::t('courier','Odbiorca');?></legend>
                <?php
                $this->renderPartial('create/_addressDataForm/_form',
                    array(
                        'model' => $model->receiverAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems('receiver'),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => 'receiver',
                        'type' => 'receiver',
//                    'focusFirst' => true,
                    ));?>
            </fieldset>
        </div>
    </div>

    <br/>
    <br/>
    <div id="part-paczkaWRuchu">
        <fieldset><legend><?= Yii::t('courier', 'Paczka w RUCHu');?></legend>

            <div class="alert alert-info">

                <p><?= Yii::t('courier', 'Wybierz punkt, do którego ma być dostarczona paczka.');?></p>
                <?php echo $form->error($model->courierTypeDomestic,'_ruch_receiver_point_address'); ?>
                <?php echo $form->labelEx($model->courierTypeDomestic,'_ruch_receiver_point'); ?>
                <?php echo $form->textField($model->courierTypeDomestic, '_ruch_receiver_point_address', ['id' => 'ruch_point_address', 'readonly' => true, 'placeholder' => Yii::t('courier', '-- kliknij, aby wybrać punkt --'), 'style' => 'text-align: center; cursor: pointer; width: 80%; max-width: 500px']); ?>
                <?php echo $form->error($model->courierTypeDomestic,'_ruch_receiver_point'); ?>

                <?php echo $form->hiddenField($model->courierTypeDomestic, '_ruch_receiver_point', ['id' => 'ruch_point']); ?>
                <br/>
                <br/>
                <p class="text-center"><strong><?= Yii::t('courier', 'Upewnij się, że podałeś poprawny nr telefonu obiorcy, gdyż zostanie na niego wysłany kod niezbędny do odbioru paczki!');?></strong></p>
            </div>
        </fieldset>
    </div>



    <div id="additions" class="domestic-additions">
        <fieldset><legend><?php echo Yii::t('courier','Opcje');?></legend>
            <div class="da-overlay"><div class="da-loader"></div></div>
            <div class="domestic-additions-content"><?php echo $additionsHtml;?></div>
        </fieldset>
    </div>
    <fieldset><legend><?= Yii::t('courier','Kontynuuj');?></legend>
        <div style="text-align: center;">
            <?php echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('name' => 'summary', 'class' => 'btn-lg btn-primary')); ?>

            <?= $form->hiddenField($model, 'source'); // for cloning ?>
            <?php $this->endWidget();
            ?>
        </div>
    </fieldset>
</div>

<p class="note">
    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
</p>
<?php $this->widget('BackLink');?>


<?php
Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyCXAS9HuDHuilN1AWAkGaSqQooOQKRTieo');
Yii::app()->clientScript->registerCssFile('https://mapka.paczkawruchu.pl/paczkawruchu.css');
Yii::app()->clientScript->registerScriptFile('https://mapka.paczkawruchu.pl/jquery.pwrgeopicker.min.js');

Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  mapIco: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico.png').',
                  mapIcoOrange: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico_orange.png').',
                  mapIcoGreen: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico_green.png').',
                  getAdditions: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierDomestic/ajaxGetAdditionList')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).'
              },
               operators: {
                  ruch: '.CJSON::encode(CourierDomesticOperator::OPERATOR_RUCH).',
                  dhlPaleta: '.CJSON::encode(CourierDomesticOperator::OPERATOR_DHL_PALETA).',
                  gls: '.CJSON::encode(CourierDomesticOperator::OPERATOR_GLS).',
              },
          };
      ',CClientScript::POS_HEAD);

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courierDomesticForm.js', CClientScript::POS_HEAD);

?>