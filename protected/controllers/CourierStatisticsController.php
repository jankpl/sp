<?php

class CourierStatisticsController extends CController {


    public function init()
    {
        Yii::app()->getModule('courier');
        parent::init();
    }


    public function actionIndex($set = 0)
    {

        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier.id , courier_package_statistics.id');
        $cmd->from('courier');
        $cmd->leftJoin('courier_package_statistics', 'courier.id = courier_id');
        $cmd->having('courier_package_statistics.id IS NULL');

        $toCalculate = $cmd->queryColumn();

        $packages = implode(',', $toCalculate);

        $sql = 'SELECT csh_END.status_date AS date_end, csh_START.status_date AS date_start, courier.date_entered AS date_entered_c, courier.user_id, courier.id AS courier_id
FROM courier_stat_history csh_START
LEFT JOIN courier_stat_history csh_END ON csh_END.courier_id = csh_START.courier_id
LEFT JOIN courier ON courier.id = csh_START.courier_id
LEFT JOIN stat_map_mapping smp_START ON (smp_START.type = :type AND csh_START.current_stat_id = smp_START.stat_id)
LEFT JOIN stat_map_mapping smp_END ON (smp_END.type = :type AND csh_END.current_stat_id = smp_END.stat_id)
WHERE
(smp_START.map_id = :stat_map_start) AND
(smp_END.map_id = :stat_map_end) AND
courier.id IN ('.$packages.')';

        $params = [];
        $params[':type'] = StatMapMapping::TYPE_COURIER;
        $params[':stat_map_start'] = StatMap::MAP_IN_TRANSPORTATION;
        $params[':stat_map_end'] = StatMap::MAP_DELIVERED;

        $resp = Yii::app()->db->createCommand($sql)->queryAll(true, $params);

        $insertArray = [];
        foreach($resp AS $item)
        {
            $insertArray[] = [
                'courier_id' => $item['courier_id'],
                'user_id' => $item['user_id'],
                'date_created' => $item['date_entered_c'],
                'date_start' => $item['date_start'],
                'date_delivered' => $item['date_end'],
                'delivery_time' => (strtotime($item['date_end']) - strtotime($item['date_start'])),
            ];
        }

        $builder = Yii::app()->db->schema->commandBuilder;
        $command=$builder->createMultipleInsertCommand('courier_package_statistics', $insertArray);
        $command->execute();
    }



}