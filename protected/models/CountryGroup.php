<?php

Yii::import('application.models._base.BaseCountryGroup');

class CountryGroup extends BaseCountryGroup
{
    public static function getGroupForCountryId($id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('group');
        $cmd->from('country_group_has_country');
        $cmd->where('country=:country',array(':country' => $id));
        $result = $cmd->queryScalar();

        return $result;
    }

    protected function calculateChildrenNumber()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select = 'COUNT(id)';
        $cmd->from = 'country_list';
        $cmd->where = 'country_group_id=:country_group_id';
        $cmd->params = array(':country_group_id' => $this->id);
        $cmd->limit = 1;

        $result = $cmd->queryScalar();

        return $result;
    }

    public function unsetAttributes($names = NULL)
    {

        return parent::unsetAttributes($names);
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('name', 'filter', 'filter' => 'trim'),

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=>0, 'on'=>'insert'),

            array('name', 'required'),
            array('name, stat', 'length', 'max'=>45),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, date_updated, stat,
            __children_number, __ChildrenNumber, CountryListCOUNT,
            ', 'safe', 'on'=>'search'),
        );
    }

    public function delete()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select = 'COUNT(id)';
        $cmd->from = 'country_group';
        $cmd->limit = 1;

        $result = $cmd->queryScalar();

        if($result == 1)
            throw new CHttpException(403,'Nie można usunąć jedynej pozycji!');

        return parent::delete();
    }

    public function saveWithCountries(Array $countryList = array(), $ownTransaction = true)
    {
        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        if(!$this->save())
            $errors = true;

        if(!$errors)
        {
            $cmd = Yii::app()->db->createCommand();

            $cmd->delete('country_group_has_country','`group`=:group', array(':group' => $this->id));

            /* @var $item CountryList */
            foreach($countryList AS $item)
            {
                $cmd->insert('country_group_has_country',array('group' => $this->id, 'country' => $item));
            }
        }

        if(!$errors)
        {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollBack();
            return false;
        }

    }

    public static function listUngroupedCountries()
    {
        $models = CountryList::model()->findAll('t.id NOT IN (SELECT country FROM country_group_has_country)');

        return $models;
    }

    public function listUngroupedCountriesAndMine()
    {
        // DELETE ASSIGNMENT COUNTRIES TO NON-EXISTING GROUPS
        $cmd = Yii::app()->db->createCommand('DELETE FROM country_group_has_country WHERE `group` NOT IN (SELECT id FROM country_group)');
        $cmd->execute();


        if($this->id === NULL)
            $models = CountryList::model()->findAll('t.id NOT IN (SELECT country FROM country_group_has_country)');
        else
            $models = CountryList::model()->findAll('t.id NOT IN (SELECT country FROM country_group_has_country WHERE `group` != :group)', array(':group' => $this->id));




        return $models;
    }

    public function listCountriesForGroup($asArrayOfId = false)
    {
        $models =  CountryList::model()->findAll(array('condition' => 't.id IN (SELECT country FROM country_group_has_country WHERE `group` = :group)', 'params' => array(':group' => $this->id)));


        if($asArrayOfId)
        {
            $array = [];
            foreach($models AS $item)
            {
                array_push($array, $item->id);
            }

            $models = $array;
        }

        return $models;
    }

    /////

    public function numberOfInternationalMailPrices($group = false)
    {

        if(!$group)
            $models = $this->internationalMailPriceHasGroups(array('condition' => 'internationalMailPriceHasGroups.user_group_id IS NULL'));
        else
            $models = $this->internationalMailPriceHasGroups(array('condition' => 'internationalMailPriceHasGroups.user_group_id IS NOT NULL'));

        return S_Useful::sizeof($models);
    }


    public function loadInternationalMailPrices()
    {
        $models = $this->internationalMailPriceHasGroups;

        return $models;
    }

    /////

    public function numberOfHybridMailPrices($group = false)
    {

        if(!$group)
            $models = $this->hybridMailPriceHasGroups(array('condition' => 'hybridMailPriceHasGroups.user_group_id IS NULL'));
        else
            $models = $this->hybridMailPriceHasGroups(array('condition' => 'hybridMailPriceHasGroups.user_group_id IS NOT NULL'));

        return S_Useful::sizeof($models);
    }


    public function loadHybridMailPrices()
    {
        $models = $this->hybridMailPriceHasGroups;

        return $models;
    }

}