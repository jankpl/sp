<?php

class WebUser extends CWebUser {

// Store model to not repeat query.


    private $_model;

// Return first name.
// access it by Yii::app()->user->first_name
    function getFirst_Name(){
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->first_name;
    }

    public function isAdmin()
    {
        return false;
    }

// Load user model.
    protected function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model=User::model()->findByPk($id);
        }
        return $this->_model;
    }


    function getPaymentAvailableLevel()
    {


        $return = $this->loadUser(Yii::app()->user->id)->getPaymentAvailableLevel();

        return $return;
    }

    function getIsPremium()
    {
        /* @var $model User */

        $model = $this->loadUser(Yii::app()->user->id);

        $return = $model->getIsPremium();

        return $return;
    }

    public function getModel()
    {
        if($this->_model===null)
            $this->_model = User::model()->cache(60)->findByPk(Yii::app()->user->id);

        return $this->_model;
    }

    function getUserGroupId()
    {
        /* @var $model User */
        $model = $this->loadUser(Yii::app()->user->id);

        return User::getUserGroupId($model->id);
    }
}
?>