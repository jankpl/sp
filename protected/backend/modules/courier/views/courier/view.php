

<?php

/* @var $this CourierController */
/* @var $model Courier */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);



$this->menu=array(

    array('label'=>'User details', 'url'=>array('/user/view', 'id' => $model->user_id),'visible'=>$model->user_id!==null),
    array('label'=>'Edit', 'url'=>array('/courier/courier/updateExternal', 'id' => $model->id),'visible'=>$model->courierTypeExternal !== NULL),

);

?>

<h1>Package #<?php echo $model->local_id; ?></h1>
<a href="<?= Yii::app()->createUrl('/courier/courier/updateTtOne', ['id' => $model->id]);?>" class="btn btn-mini btn-primary">Force update TT now</a><br/>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div>

    <?php
    if($model->user_cancel):
        ?>


        <h3>User cancel</h3>

        <?php
        if($model->user_cancel == Courier::USER_CANCELL_REQUESTED):
            ?>
            <div class="alert alert-warning">Status: <?= Courier::getUserCancelList()[$model->user_cancel];?>
                <p>T&T is disabled, but new statuses will raise flag!</p>
            </div>
        <?php
        elseif($model->user_cancel == Courier::USER_CANCELL_REQUESTED_FOUND_TT):
            ?>
            <div class="alert alert-error">Status: <?= Courier::getUserCancelList()[$model->user_cancel];?>
                <p>Found new T&T status for this package after canceling by user!</p>
            </div>
        <?php
        elseif($model->user_cancel == Courier::USER_CANCELL_COMPLAINED):
            ?>
            <div class="alert alert-warning">Status: <?= Courier::getUserCancelList()[$model->user_cancel];?>
                <p>This package has been complained by customer. T&T is active.</p>
            </div>
        <?php
        endif;
        ?>

        <div class="alert alert-info">Manual change of status will clear User cancel flag.</div>

    <?php
    endif;
    ?>

    <h3>Logs</h3>

    <table class="list hTop" style="width: 100%; table-layout: fixed;">
        <tr>
            <td style="width: 150px;">Date</td>
            <td style="width: 150px;">Cat</td>
            <td style="width: 150px;">Source</td>
            <td>Text</td>
        </tr>
        <?php
        foreach($model->courierLogs AS $item):
            ?>
            <tr>
                <td><?= $item->date_entered;?></td>
                <td><?= $item->getCatName();?></td>
                <td><?= $item->source;?></td>
                <td><?= $item->text;?></td>
            </tr>
        <?php
        endforeach;?>
    </table>


    <div style="float: left; width: 48%;">


        <h3>Package type</h3>

        <table class="list hLeft" style="width: 500px;">
            <tr>
                <td>Type</td>
                <td><?php echo $model->getTypeName();?></td>
            </tr>
        </table>

        <h3>Package details </h3>
        <table class="list hLeft" style="width: 500px;">
            <tr>
                <td>User</td>
                <td><?php echo $model->user_id !== null ? CHtml::link($model->user->login, array('/user/view', 'id' => $model->user_id)) : '-';?></td>
            </tr>
            <tr>
                <td>Source</td>
                <td><?php echo $model->sourceName; ?></td>
            </tr>
            <tr>
                <td>Creation date</td>
                <td><?php echo substr($model->date_entered,0,16);?></td>
            </tr>
            <tr>
                <td>Update date</td>
                <td><?php echo substr($model->date_updated,0,16);?></td>
            </tr>
            <tr>
                <td>Activation date</td>
                <td><?php echo substr($model->date_activated,0,16);?></td>
            </tr>
            <tr>
                <td>Attempted delivery date</td>
                <td><?php echo substr($model->date_attempted_delivery,0,16);?></td>
            </tr>
            <tr>
                <td>Delivery date</td>
                <td><?php echo substr($model->date_delivered,0,16);?></td>
            </tr>
            <tr>
                <td>Status</td>
                <td>[<?php echo $model->courier_stat_id;?>] <?php echo TbHtml::badge($model->stat->name);?> </td>
            </tr>
            <tr>
                <td>Status Map</td>
                <td>[<?php echo $model->stat_map_id ? $model->stat_map_id : '-';?>] <?php echo  $model->stat_map_id ? TbHtml::badge(StatMap::getMapNameById($model->stat_map_id)) : '';?> </td>
            </tr>
            <tr>
                <td>Lokalizacja statusu</td>
                <td><?php echo $model->location ? $model->location : '-';?></td>
            </tr>
            <tr>
                <td>Packages</td>
                <td><?php echo $model->packages_number ;?></td>
            </tr>
            <tr>
                <td>Weight</td>
                <td><?php echo $model->package_weight;?> <?php echo Courier::getWeightUnit();?> <?= ($model->package_weight_client ? '(declared: '.$model->package_weight_client.' '.Courier::getWeightUnit().')' : '') ;?></td>
            </tr>
            <tr>
                <td>Dimensions</td>
                <td><?php echo $model->package_size_d;?><?php echo Courier::getDimensionUnit();?> x <?php echo $model->package_size_l;?><?php echo Courier::getDimensionUnit();?> x <?php echo $model->package_size_w;?><?php echo Courier::getDimensionUnit();?></td>
            </tr>
            <tr>
                <td>Content</td>
                <td><?php echo $model->package_content;?></td>
            </tr>
            <tr>
                <td>Value</td>
                <td><?php echo $model->package_value; ?> <?= $model->getPackageValueCurrency();?></td>
            </tr>
            <tr>
                <td>Ref</td>
                <td><?php echo $model->ref != '' ? $model->ref : '-'; ?> [<?= ($model->ref != '') ? CHtml::link('remove', array('/courier/courier/removeRef', 'urlData' => $model->hash), ['data-confirm' => true, 'data-text' => 'Are you sure?', ]) : CHtml::link('add', array('/courier/courier/addRef', 'urlData' => $model->hash));?>]</td>
            </tr>
            <tr>
                <td>Note1</td>
                <td><?php echo $model->note1; ?></td>
            </tr>
            <tr>
                <td>Note2</td>
                <td><?php echo $model->note2; ?></td>
            </tr>
        </table>

        <h3>COD</h3>
        <table class="list hLeft" style="width: 500px;">
            <tr>
                <td>COD</td>
                <td><?php echo $model->cod ? TbHtml::badge('yes', ['class' => 'badge-success']):TbHtml::badge('no', ['class' => 'badge']); ?></td>
            </tr>
            <tr>
                <td>Amount</td>
                <td><?php echo $model->cod_value ? $model->cod_value : '-'; ?></td>
            </tr>
            <tr>
                <td>Currency</td>
                <td><?php echo $model->cod_currency ? $model->cod_currency : '-'; ?></td>
            </tr>
        </table>

        <?php
        if($model->courierTypeReturn !== NULL):
            ?>

            <h3>Return details</h3>

            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td colspan="2" style="text-align: center;">

                    </td>
                </tr>
                <tr>
                    <td>Operator Tracking number</td>
                    <td><?php echo $model->courierTypeReturn->courierLabelNew ?  $model->courierTypeReturn->courierLabelNew->track_id.' '.CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeReturn->courier_label_new_id]) : '' ;?></td>
                </tr>
                <tr>
                    <td>Operator</td>
                    <td><?= $model->courierTypeReturn->courierLabelNew ? CourierLabelNew::getOperators()[$model->courierTypeReturn->courierLabelNew->operator] : '';?></td>
                </tr>
                <tr>
                    <td>Base package</td>
                    <td><?= $model->courierTypeReturn->baseCourier ? CHtml::link('#'.$model->courierTypeReturn->baseCourier->local_id, ['/courier/courier/view', 'id' => $model->courierTypeReturn->baseCourier->id]) : '';?></td>
                </tr>
            </table>

        <?php endif; ?>

        <?php
        if($model->courierTypeExternalReturn !== NULL):
            ?>

            <h3>External Return details</h3>

            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <?= CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeExternalReturn->courier_label_new_id]) ;?>
                    </td>
                </tr>
                <tr>
                    <td>External TT number</td>
                    <td><?php echo $model->courierTypeExternalReturn->courierLabelNew ?  $model->courierTypeExternalReturn->courierLabelNew->track_id : '' ;?></td>
                </tr>
                <tr>
                    <td>External TT operator</td>
                    <td><?php echo $model->courierTypeExternalReturn->courierLabelNew ?  $model->courierTypeExternalReturn->courierLabelNew->_ext_operator_name : '' ;?></td>
                </tr>
            </table>

        <?php endif; ?>


        <?php
        if($model->courierTypeU !== NULL):
            ?>

            <h3>U details</h3>

            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td>Number in one order</td>
                    <td><?php echo $model->courierTypeU->family_member_number.' / '.$model->packages_number;?></td>
                </tr>
                <tr>
                    <td>First package in order</td>
                    <td><?php echo $model->courierTypeU->parent_courier_id ? CHtml::link('#'.$model->courierTypeU->parentCourier->local_id, array('/courier/courier/view', 'id' => $model->courierTypeU->parent_courier_id)):'-';?></td>
                </tr>
                <tr>
                    <td>Order</td>
                    <td><?php echo $model->courierTypeU->order_id?CHtml::link('#'.$model->courierTypeU->order_id, array('/order/view', 'id' => $model->courierTypeU->order_id)):'-';?></td>
                </tr>
                <tr>
                    <td>Operator Tracking ID</td>
                    <td><?php echo $model->courierTypeU->courierLabelNew ?  $model->courierTypeU->courierLabelNew->track_id.' '.CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeU->courier_label_new_id]) : '' ;?></td>
                </tr>
                <?php
                /* @var $orderProduct OrderProduct */
                $orderProduct = OrderProduct::getModelForProductItem(OrderProduct::TYPE_COURIER, $model->id);
                ?>
                <tr>
                    <td>Cost (netto/brutto)</td>
                    <td><?php echo S_Price::formatPrice($orderProduct->value_netto)?> / <?php echo S_Price::formatPrice($orderProduct->value_brutto)?> <?php  echo $orderProduct->currency;?></td>
                </tr>
                <tr>
                    <td>Operator</td>
                    <td><?php echo $model->courierTypeU->courierUOperator->name;?> (#<?= $model->courierTypeU->courier_u_operator_id;?>)</td>
                </tr>
                <tr>
                    <td>Collection</td>
                    <td><?php echo $model->courierTypeU->getCollectionName();?></td>
                </tr>

            </table>

        <?php endif; ?>


        <?php
        if($model->courierTypeOoe !== NULL):
            ?>

            <h3>OOE details</h3>

            <table class="list hLeft" style="width: 500px;">
                <?php if(($model->courierTypeOoe->courierLabelNew !== NULL && $model->courierTypeOoe->courierLabelNew->operator == CourierLabelNew::OPERATOR_OOE) OR $model->courierTypeOoe->courierLabelNew == NULL): ?>
                    <tr>
                        <td>Service name</td>
                        <td><?php echo $model->courierTypeOoe->service_name?></td>
                    </tr>
                    <tr>
                        <td>Service code</td>
                        <td><?php echo $model->courierTypeOoe->service_code;?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td>Operator</td>
                    <td><?php echo $model->courierTypeOoe->courierLabelNew ? CourierLabelNew::getOperators()[$model->courierTypeOoe->courierLabelNew->operator] : '';?></td>
                </tr>
                <tr>
                    <td>Track ID</td>
                    <td><?php echo $model->courierTypeOoe->findRemoteId();?> <?php echo $model->courierTypeOoe->courierLabelNew ? CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeOoe->courier_label_new_id]) : '' ;?></td>
                </tr>
                <?php
                if(OneWorldExpressPackage::isPalletways($model->courierTypeOoe->service_code)):
                    ?>
                    <tr>
                        <td>Palletway size</td>
                        <td><?php echo CourierTypeOoe::getPalletwayTypes()[$model->courierTypeOoe->_palletway_size]; ?></td>
                    </tr>
                <?php endif;?>
            </table>

        <?php endif; ?>


        <?php
        if($model->courierTypeExternal !== NULL):
            ?>

            <h3>External details</h3>

            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td>External operator</td>
                    <td><?php echo $model->courierTypeExternal->courier_external_operator_name;?></td>
                </tr>
                <tr>
                    <td>External Id</td>
                    <td><?php echo $model->courierTypeExternal->external_id;?></td>
                </tr>
            </table>

        <?php endif; ?>

        <?php
        if($model->courierTypeDomestic !== NULL):
            ?>

            <h3>Domestic details</h3>

            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td>Domestic operator</td>
                    <td><?php echo $model->courierTypeDomestic->getOperator();?></td>
                </tr>
                <tr>
                    <td>Operator Track ID</td>
                    <td><?php echo $model->courierTypeDomestic->courierLabelNew->track_id;?> <?php echo $model->courierTypeDomestic->courierLabelNew ? CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeDomestic->courier_label_new_id]) : '' ;?></td>
                </tr>
                <tr>
                    <td>COD Value</td>
                    <td><?php echo $model->courierTypeDomestic->cod_value;?> <?php echo $model->courierTypeDomestic->cod_currency;?></td>
                </tr>
                <tr>
                    <td>Number in one order</td>
                    <td><?php echo $model->courierTypeDomestic->family_member_number.' / '.$model->packages_number;?></td>
                </tr>
                <tr>
                    <td>First package in order</td>
                    <td><?php echo $model->courierTypeDomestic->parent_courier_id ? CHtml::link('#'.$model->courierTypeDomestic->parentCourier->local_id, array('/courier/courier/view', 'id' => $model->courierTypeDomestic->parent_courier_id)):'-';?></td>
                </tr>
            </table>



        <?php endif;?>

        <?php
        if($model->courierTypeInternalSimple !== NULL):
            ?>

            <h3>Internal Simple details</h3>

            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td>Inernal operator</td>
                    <td><?php if($model->courierTypeInternalSimple !== NULL) echo $model->courierTypeInternalSimple->getOperator();?></td>
                </tr>
                <tr>
                    <td>Internal tracking ID</td>
                    <td><?php echo $model->courierTypeInternalSimple->courierExtOperatorDetails->track_id;?> <?php echo $model->courierTypeInternalSimple->courierExtOperatorDetails->courierLabelNew ? CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeInternalSimple->courierExtOperatorDetails->courier_label_new_id]) : '' ;?></td>
                </tr>
                <tr>
                    <td>Dimension weight</td>
                    <td><?php echo $model->courierTypeInternalSimple->dimension_weight;?> <?php echo Courier::getWeightUnit();?></td>
                </tr>
                <tr>
                    <td>Weight for pricing</td>
                    <td><?php if($model->courierTypeInternalSimple !== NULL) echo $model->courierTypeInternalSimple->getFinalWeight();?> <?php echo Courier::getWeightUnit();?></td>
                </tr>
                <tr>
                    <td>With pickup</td>
                    <td><?php echo $model->courierTypeInternalSimple->isWithPickup();?></td>
                </tr>
                <tr>
                    <td>Other params</td>
                    <td>
                        <?php foreach($model->courierTypeInternalSimple->getParamsWithLabelNames() AS $key => $item)
                        {
                            if($key == $model->courierTypeInternalSimple->getAttributeLabel('_preffered_pickup_time'))
                                $item = PrefferedPickupTime::convertTimeNameToText($item);
                            else if($key == $model->courierTypeInternalSimple->getAttributeLabel('_preffered_pickup_day'))
                                $item = PrefferedPickupTime::convertDateNameToText($item);

                            echo '<strong>'.$key.'</strong>: '.$item.' ,<br/>';
                        }?>
                    </td>
                </tr>
            </table>



        <?php endif;?>


        <?php
        if($model->courierTypeInternal !== NULL):

            ?>
            <h3>Internal details</h3>

            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td>Paid</td>
                    <td><?php echo $model->courierTypeInternal->paid ? TbHtml::badge('yes', ['class' => 'badge-success']):TbHtml::badge('no', ['class' => 'badge-warning']);?></td>
                </tr>
                <tr>
                    <td>Number in one order</td>
                    <td><?php echo $model->courierTypeInternal->family_member_number.' / '.$model->packages_number;?></td>
                </tr>
                <tr>
                    <td>First package in order</td>
                    <td><?php echo $model->courierTypeInternal->parent_courier_id ? CHtml::link('#'.$model->courierTypeInternal->parentCourier->local_id, array('/courier/courier/view', 'id' => $model->courierTypeInternal->parent_courier_id)):'-';?></td>
                </tr>
                <tr>
                    <td>Order</td>
                    <td><?php echo $model->courierTypeInternal->order_id?CHtml::link('#'.$model->courierTypeInternal->order_id, array('/order/view', 'id' => $model->courierTypeInternal->order_id)):'-';?></td>
                </tr>
                <tr>
                    <td>Pickup mode</td>
                    <td><?php echo $model->courierTypeInternal->getWithPickupName() ;?></td>
                </tr>
                <tr>
                    <td>Pickup HUB</td>
                    <td><?php echo $model->courierTypeInternal->pickup_hub_id?$model->courierTypeInternal->pickup_hub_name:'-';?></td>
                </tr>
                <tr>
                    <td>Pickup Operator</td>
                    <td>
                        <?= $model->courierTypeInternal->pickupOperator ? $model->courierTypeInternal->pickupOperator->getNameWithIdAndBroker() : ''; ?> ||
                        <?php echo $model->courierTypeInternal->pickupLabel->courierLabelNew ? CourierLabelNew::getOperators()[$model->courierTypeInternal->pickupLabel->courierLabelNew->operator] :'-';?></td>
                </tr>
                <tr>
                    <td>Pickup Operator Tracking ID</td>
                    <td><?php echo ($model->courierTypeInternal->pickupLabel->track_id? $model->courierTypeInternal->pickupLabel->track_id:'-');?> <?php echo $model->courierTypeInternal->pickupLabel->courierLabelNew ? CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeInternal->pickupLabel->courier_label_new_id]) : '' ;?></td>
                </tr>
                <tr>
                    <td>Delivery HUB</td>
                    <td><?php echo $model->courierTypeInternal->delivery_hub_id?$model->courierTypeInternal->delivery_hub_name:'-';?></td>
                </tr>
                <tr>
                    <td>Delivery Operator</td>
                    <td>
                        <?= $model->courierTypeInternal->deliveryLabel ? $model->courierTypeInternal->deliveryOperator->getNameWithIdAndBroker() : ''; ?> ||
                        <?php echo $model->courierTypeInternal->deliveryLabel->courierLabelNew ? CourierLabelNew::getOperators()[$model->courierTypeInternal->deliveryLabel->courierLabelNew->operator] :'-';?></td>
                </tr>
                <tr>
                    <td>Delivery Operator Tracking ID</td>
                    <td><?php echo ($model->courierTypeInternal->deliveryLabel->track_id ? $model->courierTypeInternal->deliveryLabel->track_id:'-');?> <?php echo $model->courierTypeInternal->deliveryLabel->courierLabelNew ? CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeInternal->deliveryLabel->courier_label_new_id]) : '' ;?></td>
                </tr>
                <tr>
                    <td>Common Operator</td>
                    <td>
                        <?= $model->courierTypeInternal->commonLabel ? $model->courierTypeInternal->deliveryOperator->getNameWithIdAndBroker() : ''; ?> ||
                        <?php echo $model->courierTypeInternal->commonLabel->courierLabelNew ? CourierLabelNew::getOperators()[$model->courierTypeInternal->commonLabel->courierLabelNew->operator] :'-';?></td>
                </tr>
                <tr>
                    <td>Common Operator Tracking ID</td>
                    <td><?php echo ($model->courierTypeInternal->commonLabel->track_id?$model->courierTypeInternal->commonLabel->track_id:'-');?> <?php echo $model->courierTypeInternal->commonLabel->courierLabelNew ? CHtml::link('[edit]', ['/courier/courierOrder/editTrackingId', 'id' => $model->courierTypeInternal->commonLabel->courier_label_new_id]) : '' ;?></td>
                </tr>
                <tr>
                    <td>COD</td>
                    <td><?php echo $model->courierTypeInternal->cod?$model->courierTypeInternal->cod_value.' '.$model->courierTypeInternal->cod_currency:'-';?></td>
                </tr>
                <tr>
                    <td>Client COD Value</td>
                    <td><?php echo $model->courierTypeInternal->client_cod_value > 0 ? $model->courierTypeInternal->client_cod_value.' '.$model->courierTypeInternal->cod_currency : '-';?></td>
                </tr>

                <?php
                /* @var $orderProduct OrderProduct */
                $orderProduct = OrderProduct::getModelForProductItem(OrderProduct::TYPE_COURIER, $model->id);
                ?>
                <tr>
                    <td>Cost (netto/brutto)</td>
                    <td><?php echo S_Price::formatPrice($orderProduct->value_netto)?> / <?php echo S_Price::formatPrice($orderProduct->value_brutto)?> <?php  echo $orderProduct->currency;?></td>
                </tr>
                <tr>
                    <td>Other params</td>
                    <td>
                        <?php foreach($model->courierTypeInternal->getParamsWithLabelNames() AS $key => $item)
                        {
                            if($key == $model->courierTypeInternal->getAttributeLabel('_preffered_pickup_time'))
                                $item = PrefferedPickupTime::convertTimeNameToText($item);
                            else if($key == $model->courierTypeInternal->getAttributeLabel('_preffered_pickup_day'))
                                $item = PrefferedPickupTime::convertDateNameToText($item);

                            echo '<strong>'.$key.'</strong>: '.$item.' ,<br/>';
                        }?>
                    </td>
                </tr>
            </table>


        <?php
        endif;
        ?>



        <?php
        $collect = CourierCollect::findForCourierId($model->id);
        ?>
        <h3>Collect Service</h3>
        <table class="list hLeft" style="width: 500px;">
            <tr>
                <td>Collect Order</td>
                <td><?php echo ($collect ? CHtml::link('#'.$collect->id, Yii::app()->createUrl('/courier/courierCollect/view', array('id' => $collect->id))) : '-');?></td>
            </tr>
        </table>


    </div>
    <div style="float: right; width: 48%;">

        <?php
        if($model->courierTypeU !== NULL):
            ?>

            <h3>Additions</h3>
            <table class="list hTop" style="width: 500px;">
                <tr>
                    <td>Name</td>
                    <td>Description</td>
                </tr>
                <?php

                /* @var $item CourierUAdditionList */
                if(!S_Useful::sizeof($model->courierTypeU->listAdditions()))
                    echo '<tr><td colspan="2">none</td></tr>';
                else
                    foreach($model->courierTypeU->listAdditions() AS $item): ?>
                        <tr>
                            <td><?php echo $item->name; ?></td>
                            <td><?php echo $item->description; ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        <?php endif; ?>

        <?php
        if($model->courierTypeInternal !== NULL):
            ?>

            <h3>Additions</h3>
            <table class="list hTop" style="width: 500px;">
                <tr>
                    <td>Name</td>
                    <td>Description</td>
                </tr>
                <?php

                /* @var $item CourierAdditionList */
                if(!S_Useful::sizeof($model->listAdditions()))
                    echo '<tr><td colspan="2">none</td></tr>';
                else
                    foreach($model->listAdditions() AS $item): ?>
                        <tr>
                            <td><?php echo $item->name; ?></td>
                            <td><?php echo $item->description; ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        <?php endif; ?>

        <?php
        if($model->courierTypeDomestic !== NULL):
            ?>

            <h3>Additions</h3>
            <table class="list hTop" style="width: 500px;">
                <tr>
                    <td>Name</td>
                    <td>Description</td>
                </tr>
                <?php

                /* @var $item CourierAdditionList */
                if(!S_Useful::sizeof($model->courierTypeDomestic->listAdditions()))
                    echo '<tr><td colspan="2">none</td></tr>';
                else
                    foreach($model->courierTypeDomestic->listAdditions() AS $item): ?>
                        <tr>
                            <td><?php echo $item->name; ?></td>
                            <td><?php echo $item->description; ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        <?php endif; ?>

        <h3>Carriage ticket</h3>

        <?php
        if(!in_array($model->courier_stat_id, [CourierStat::CANCELLED, CourierStat::NEW_ORDER, CourierStat::PACKAGE_PROCESSING, CourierStat::CANCELLED_USER]) &&
            !in_array($model->user_cancel, [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_WARNED]) &&
            !$model->isLabelArchived()):
            ?>

            <table class="list " style="width: 500px;">
                <tr>
                    <?php
                    if($model->courierTypeInternal !== NULL):
                    ?>
                    <td colspan="4" style="text-align: center;">First</td>
                    <td><?php echo TbHtml::link('Generate Both', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_BOTH), array('class' => 'btn'));?></td>
                    <td colspan="4" style="text-align: center;">Last</td>
                </tr>
                <tr>
                    <td><?php echo TbHtml::link('4 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_FIRST, 'type' => Courier::PDF_4_ON_1), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('2 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_FIRST, 'type' => Courier::PDF_2_ON_1), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('2 on 1 (1R)', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_FIRST, 'type' => Courier::PDF_2_ON_1_ONE_RIGHT), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('Roll', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_FIRST, 'type' => Courier::PDF_ROLL), array('class' => 'btn'));?></td>
                    <td></td>
                    <td><?php echo TbHtml::link('4 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_4_ON_1), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('2 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_2_ON_1), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('2 on 1 (1R)', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_2_ON_1_ONE_RIGHT), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('Roll', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_ROLL), array('class' => 'btn'));?></td>
                </tr>
                <tr>
                    <?php
                    elseif($model->courierTypeOoe !== NULL && $model->courierTypeOoe->courier_label_new_id === NULL):
                        ?>
                        <td><?php echo TbHtml::link('Generate', array('/courier/courier/CarriageTicket', 'id' => $model->id), array('class' => 'btn'));?></td>
                    <?php
                    else:
                        ?>
                        <td><?php echo TbHtml::link('4 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'type' => Courier::PDF_4_ON_1), array('class' => 'btn'));?></td>
                        <td><?php echo TbHtml::link('2 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'type' => Courier::PDF_2_ON_1), array('class' => 'btn'));?></td>
                        <td><?php echo TbHtml::link('2 on 1 (1R)', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'type' => Courier::PDF_2_ON_1_ONE_RIGHT), array('class' => 'btn'));?></td>
                        <td><?php echo TbHtml::link('Roll', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'type' => Courier::PDF_ROLL), array('class' => 'btn'));?></td>
                    <?php endif; ?>
                </tr>
            </table>
        <?php
        else:
            ?>
            <?php
            if($model->isLabelArchived()):?>
                Labels has expired.
            <?php
            else:
                ?>
                With this package status you cannot generate label.
            <?php
            endif;
            ?>


        <?php
        endif;
        ?>

        <h3>Change status</h3>

        <?php
        if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER])):
            ?>
            Option available only for administrators
        <?php
        else:
        if ($model->isStatHistoryArchived()):
            ?>
            Status history has expired
        <?php
        else:
        ?>

        <?php $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'change-status',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('/courier/courier/changeStat'),
        ));
        ?>

        <?php echo $form->hiddenField($model, 'id'); ?>

        <div class="form">

            <?php echo $form->errorSummary($model); ?>

            <table class="list hTop" style="width: 500px;">
                <tr>
                    <td>Current status</td>
                </tr>
                <tr>
                    <td>[<?php echo $model->stat->id; ?>
                        ] <?php echo $model->stat->name; ?> <?php echo $model->location ? '(' . $model->location . ')' : ''; ?></td>
                </tr>
            </table>


            <table class="list hTop" style="width: 500px;">
                <tr>
                    <td>New status</td>
                    <td>Location</td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->dropDownList($model, 'courier_stat_id', CHtml::listData(CourierStat::model()->findAll(), 'id', 'name'), ['data-chosen-widget' => true]); ?>
                    </td>
                    <td>
                        <?php echo CHtml::textField('location'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">Date (yyyy-mm-dd hh:mm:ss): *</td>
                    <td>
                        <?php
                        $this->widget('application.extensions.timepicker.EJuiDateTimePicker', array(
                            'name' => 'status_date',
                            'options' => array(
                                'hourGrid' => 4,
                                'hourMin' => 0,
                                'hourMax' => 23,
                                'showSecond' => true,
                                'timeFormat' => 'hh:mm:ss',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'dateFormat' => 'yy-mm-dd',
                            ),
                        ));
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php
                        echo TbHtml::submitButton(Yii::t('app', 'Save'),
                            array(
                                'confirm' => 'Are you sure?'
                            ));
                        $this->endWidget();
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        (*) leave empty for current date
                    </td>
                </tr>
            </table>

            <?php

            $script = "
                 $('[data-chosen-widget]').chosen({allow_single_deselect:true});
                    $('.chosen-drop').css('width', '300px');
                    $('[data-chosen-widget]').hide();
                ";
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.jquery.min.js');
            Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.min.css');
            Yii::app()->clientScript->registerScript('chosen-widget', $script, CClientScript::POS_READY);
            ?>


            <?php $form = $this->beginWidget('GxActiveForm', array(
                'id' => 'change-status',
                'enableAjaxValidation' => false,
                'action' => Yii::app()->createUrl('/courier/courier/deleteLastStatus', array('id' => $model->id)),
            ));
            ?>

            <table class="list hTop" style="width: 500px;">
                <tr>
                    <td>Delete last status</td>
                </tr>
                <tr>
                    <td>
                        <?php
                        echo TbHtml::submitButton('Delete',
                            array(
                                'confirm' => 'Are you sure?'
                            ));
                        $this->endWidget();
                        ?>
                    </td>
                </tr>
            </table>

            <table class="list hTop" style="width: 500px;">
                <tr>
                    <td>Refresh stat map</td>
                </tr>
                <tr>
                    <td>
                        <a href="<?= Yii::app()->createUrl('/courier/courier/refreshStatMap', ['urlData' => $model->hash]); ?>"
                           class="btn">Refresh stat map</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        Ignores "RETURNED" stat map lock
                    </td>
                </tr>
            </table>
            <?php
            endif;
            endif;
            ?>
            <?php
            if($model->getType() != Courier::TYPE_EXTERNAL_RETURN):
                ?>
                <h3>Return label</h3>
                <table class="list hTop" style="width: 500px;">

                    <tr>
                        <td>
                            <a href="<?= Yii::app()->createUrl('/courier/courier/generateReturnLabel', ['urlData' => $model->hash]);?>" target="_blank" class="btn">Generate return label</a>
                        </td>
                    </tr>
                </table>
            <?php
            endif;
            ?>

            <h3>Create invoice by Fakturownia</h3>
            <a href="<?= Yii::app()->createUrl('/fakturownia/createByCourier', ['id' => $model->id]);?>" class="btn" target="_blank">With sender data</a> <a href="<?= Yii::app()->createUrl('/fakturownia/createByCourier', ['id' => $model->id, 'userData' => true]);?>" class="btn" target="_blank">With user data</a>

        </div><!-- form -->



    </div>
    <div style="clear: both;"></div>
</div>

<div>

    <div style="float: left; width: 48%;">
        <h3>Package sender</h3>
        <?php

        $this->Widget('AddressDataList', array(
            'addressData' => $model->senderAddressData,
            'editUrl' => Yii::app()->createUrl('/addressData/editBankAccountForCourier', ['id' => $model->sender_address_data_id, 'courier_id' => $model->id]),
        ));

        ?>
    </div>
    <div style="float: right; width: 48%;">

        <h3>Package receiver</h3>
        <?php

        $this->Widget('AddressDataList', array(
            'addressData' => $model->receiverAddressData
        ));
        ?>
    </div>
    <div style="clear: both;"></div>
</div>

<?php
if($model->courierTypeInternal && $model->courierTypeInternal->order->user_id === NULL):
    ?>
    <h3>Order owner (NON-USER)</h3>
    <?php
    $this->Widget('AddressDataList', array(
        'addressData' => $model->courierTypeInternal->order->ownerData
    ));
    ?>
<?php
endif;
?>


<div>

    <h3>Additional external IDs</h3>
    <p>Allows package to be found by this ids</p>

    <table class="list hTop" style="width: 100%; table-layout: fixed;">
        <tr>
            <td style="width: 150px;">External ID</td>
        </tr>
        <?php
        foreach($model->courierAdditionalExtId AS $item):
            ?>
            <tr>
                <td><?= $item->external_id;?></td>
            </tr>
        <?php
        endforeach;?>
    </table>


    <div style="float: left; width: 68%;">

        <h3>History</h3>
        <?php
        if($model->isStatHistoryArchived()):?>
            History has expired.

        <?php
        else:
            ?>
            <?php
            $this->renderPartial('/_statHistory/_statHistory', array(
                'model' => $model->courierStatHistories,
                'type' => 1,
            ));

            ?>

        <?php
        endif;
        ?>
    </div>
    <div style="float: right; width: 28%;">

        <h3 style="display: inline-block; margin-right: 5px;">TT Logs</h3>
        Last TT check(s):
        <?php
        $logs = $model->ttLogs;
        if(!S_Useful::sizeof($logs)):
            ?>
            <br/>- none -
        <?php
        else:
            ?>
            <ul>
                <?php
                foreach($model->ttLogs AS $item)
                    echo '<li>'.$item->date_entered.'</li>';
                ?>
            </ul>
        <?php
        endif;
        ?>
    </div>
    <div style="clear: both;"></div>
</div>

<h3>SMSes</h3>

<table class="list hTop" style="width: 100%; table-layout: fixed;">
    <tr>
        <td style="width: 150px;">Date</td>
        <td style="width: 150px;">Nr</td>
        <td>Text</td>
    </tr>
    <?php
    foreach($model->smsArchive AS $sms):
        ?>
        <tr>
            <td><?= $sms->date_entered;?></td>
            <td><?= $sms->number;?></td>
            <td><?= $sms->text;?></td>
        </tr>
    <?php
    endforeach;?>
</table>


