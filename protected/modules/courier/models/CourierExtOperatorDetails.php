<?php

Yii::import('application.modules.courier.models._base.BaseCourierExtOperatorDetails');

class CourierExtOperatorDetails extends BaseCourierExtOperatorDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    const STAT_OK = 1;
    const STAT_FAIL = 9;

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),

        );
    }

    public function rules() {
        return array(
            array('stat, operator', 'required'),
            array('stat, courier_label_new_id', 'numerical', 'integerOnly'=>true),
            array('operator', 'length', 'max'=>32),
            array('label_old', 'safe'),
            array('label_old, track_id_old, courier_label_new_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, label_old, stat, operator, track_id_old, courier_label_new_id', 'safe', 'on'=>'search'),
        );
    }

    public function fetchLabel($noError = false)
    {

        if($this->label_old !='')
            return $this->label_old;

        return $this->courierLabelNew->getFileAsString($noError);
    }

    public function isOversized()
    {
        $this->courierLabelNew->_oversized;
    }

    public function getTrack_id()
    {
        if($this->track_id_old != '')
            return $this->track_id_old;

        return $this->courierLabelNew->track_id;
    }

}