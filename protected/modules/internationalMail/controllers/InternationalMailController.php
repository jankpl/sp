<?php

class InternationalMailController extends Controller {

    public function beforeAction($action)
    {
        // 12.09.2016
        if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI)
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'InternationalMail',
            ));
            exit;
        }
    }

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('index', 'create', 'view', 'carriageTicket'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('list', 'massCarriageTicketByGridView'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($urlData) {

        $this->panelLeftMenuActive = 400;

        $model = InternationalMail::model()->find('hash=:hash', array(':hash'=>$urlData));

        if($model === null)
            throw new CHttpException(404);

        if($model->international_mail_stat_id == InternationalMailStat::PACKAGE_RETREIVE) $actions = $this->renderPartial('actions/generate_carriage_ticket', array('model' => $model), true);

        $this->render('view', array(
            'model' => $model,
            'actions' => $actions,
        ));
    }

    protected function create_step_1()
    {
        if(Yii::app()->user->isGuest && !$this->getPageState('loginProposition'))
        {

            $this->setPageState('loginProposition',1);

            $model = new LoginForm();
            $model->returnURL = Yii::app()->createUrl('internationalMail/create');

            $this->render('create/loginProposition', array('model' => $model));

        } else {
            $this->setPageState('form_started', true);
            $this->setPageState('start_user_id', Yii::app()->user->id);

            if($this->getPageState('form_started'))
            {
                if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
                    throw new CHttpException(403, 'Sesja została przerwana!');
            }

            $this->setPageState('last_step',1); // save information about last step

            $model=new InternationalMail('step1');
            $model->attributes = $this->getPageState('step1_model',array());

            $currency = $this->getPageState('step1_currency', NULL);

            $this->render('create/step1',array(
                'model'=>$model,
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
                'currency' => $currency,
            ));

        }
    }

    protected function create_validate_step_1()
    {
        $model=new InternationalMail('step1');
        $model->attributes = $_POST['InternationalMail'];
        $model->validate();
        $this->setPageState('step1_model',$model->getAttributes(array_keys($_POST['InternationalMail']))); // save previous form into form state

        $error = false;

        $currency = PriceCurrency::model()->findByPk($_POST['currency']);
        if($currency == NULL)
            $currency = 1;
        else
            $currency = $currency->id;

        $this->setPageState('step1_currency', $currency);

        if($model->isItTooBig())
        {
            Yii::app()->user->setFlash('error',"List jest zbyt duży!");
            $error = true;
        }

        if($model->isItTooHeavy())
        {
            Yii::app()->user->setFlash('error',"List jest zbyt ciężki!");
            $error = true;
        }

        if($error OR $model->hasErrors())
        {
            $this->render('create/step1',array(
                'model'=>$model,
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }
        unset($model);
    }

    protected function create_step_2()
    {

        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');


        $lastStep =  $this->getPageState('last_step');

        if($lastStep === 1)
            if($this->create_validate_step_1() == 'break')
                return;

        $model = new InternationalMail_AddressData('sender');
        if(!S_Useful::sizeof($this->getPageState('step2_model',array())))
            $model->loadUserData();
        else
            $model->attributes = $this->getPageState('step2_model',array());

        $model2 = null;
        $saveToContactBook = false;
        if($this->getPageState('step2_saveToContactBook'))
        {
            $model2 = new UserContactBook();
            $model2->attributes = $this->getPageState('step2_saveToContactBook');
            $saveToContactBook = true;
        }

        $this->setPageState('last_step',2); // save information about last step
        $this->render('create/step2',array(
            'model'=>$model,
            'model2'=>$model2,
            'saveToContactBook' => $saveToContactBook,
            'type' => 'sender',
            'countries' => InternationalMail::listCountries(),
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));

    }

    protected function create_validate_step_2()
    {
        $model=new InternationalMail_AddressData('sender');
        $model->attributes = $_POST['InternationalMail_AddressData'];
        $model->validate();

        $this->setPageState('step2_model', $model->getAttributes(array_keys($_POST['InternationalMail_AddressData']))); // save previous form into form state

        $model2 = null;
        $saveToContactBook = false;
        if($_POST['Other']['saveToContactBook']) // for saving data to userAddress
        {
            $saveToContactBook = true;
            $model2 = new UserContactBook();
            $model2->type = 'sender';
            $model2->setAttributesWithPrefix($model->getAttributes(array_keys($_POST['InternationalMail_AddressData'])),'');
            $model2->validate();
        }

        $this->setPageState('step2_saveToContactBook',$saveToContactBook);
        if(isset($model2))
            $this->setPageState('step2_saveToContactBookAddressData',$model2->attributes);

        if($model->hasErrors() OR (isset($model2) AND $model2->hasErrors()))
        {
            $this->render('create/step2',array(
                'model'=>$model,
                'model2'=>$model2,
                'saveToContactBook' => $saveToContactBook, 'type' => 'sender',
                'countries' => InternationalMail::listCountries(),
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }
    }

    protected function create_step_3()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');


        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 2)
            if($this->create_validate_step_2() == 'break')
                return;

        $model3 = null;
        $saveToContactBook = false;
        if($this->getPageState('step3_saveToContactBook'))
        {
            $model3 = new UserContactBook('set_safe');
            $model3->attributes = $this->getPageState('step3_saveToContactBook');
            $saveToContactBook = true;
        }


        $model=new InternationalMail_AddressData('receiver');
        $model->attributes = $this->getPageState('step3_model',array());

        $userGroup = NULL;
        if(!Yii::app()->user->isGuest)
            $userGroup = Yii::app()->user->getUserGroupId();


        $this->setPageState('last_step',3); // save information about last step
        $this->render('create/step3',array(
            'model'=>$model,
            'model3'=>$model3,
            'saveToContactBook' => $saveToContactBook,
            'type' => 'receiver',
            'countries' => InternationalMail::listCountries($userGroup),
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));
    }

    protected function create_validate_step_3()
    {
        $model=new InternationalMail_AddressData('receiver');
        $model->attributes = $_POST['InternationalMail_AddressData'];
        $model->validate();
        $this->setPageState('step3_model',$model->getAttributes(array_keys($_POST['InternationalMail_AddressData']))); // save previous form into form state

        $model3 = NULL;
        $saveToContactBook = false;
        if($_POST['Other']['saveToContactBook']) // for saving data to userAddress
        {
            $saveToContactBook = true;
            $model3 = new UserContactBook();
            $model3->type = 'receiver';
            $model3->setAttributesWithPrefix($_POST['InternationalMail_AddressData'],'');
            $model3->validate();
        }

        $this->setPageState('step3_saveToContactBook',$saveToContactBook);
        if(isset($model3))
            $this->setPageState('step3_saveToContactBookAddressData',$model3->attributes);

        if($model->hasErrors() OR (isset($model3) AND $model3->hasErrors()))
        {
            $this->render('create/step3',array('model'=>$model,
                'model3'=>$model3,
                'saveToContactBook' => $saveToContactBook,
                'type' => 'receiver',
                'countries' => InternationalMail::listCountries(),
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }
    }


    protected function create_step_4()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 3)
            if($this->create_validate_step_3() == 'break')
                return;

        $model = new InternationalMail();

        $modelInternationalMailAddition = $this->getPageState('international_mail_additions', array());
        $modelInternationalMailAdditionList = InternationalMailAdditionList::model()->findAll('stat = '.S_Status::ACTIVE);

        if(Yii::app()->user->isGuest)
        {
            $currency = $this->getPageState('step1_currency', 1);
            Yii::app()->PriceManager->setCurrency($currency);
        }


        $this->setPageState('last_step',4); // save information about last step
        $this->render('create/step4',array(
            'model' => $model,
            'currency' => $this->getPageState('step1_currency', NULL),
            'modelInternationalMailAdditionList'=>$modelInternationalMailAdditionList,
            'internationalMailAddition'=>$modelInternationalMailAddition,
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));



    }

    protected function create_validate_step_4()
    {
        $internationalMailAddition = Array();

        if(isset($_POST))
        {
            if(is_array($_POST['InternationalMailAddition']))
                foreach($_POST['InternationalMailAddition'] AS $item)
                    if(InternationalMailAdditionList::model()->findByPk($item)!==null) array_push($internationalMailAddition, $item);

            if(!is_array($internationalMailAddition)) $internationalMailAddition = Array();

            $this->setPageState('international_mail_additions',$internationalMailAddition);
        }

    }


    protected function create_step_5()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep =  $this->getPageState('last_step');

        switch($lastStep)
        {
            case 1 :
                if($this->create_validate_step_1() == 'break')
                    return;
                break;
            case 2 :
                if($this->create_validate_step_2() == 'break')
                    return;
                break;
            case 3 :
                if($this->create_validate_step_3() == 'break')
                    return;
                break;
            case 4 :
                if($this->create_validate_step_4() == 'break')
                    return;
                break;
        }

        $model = new InternationalMail('step5');

        if(Yii::app()->user->isGuest)
        {
            $currency = $this->getPageState('step1_currency', 1);
            Yii::app()->PriceManager->setCurrency($currency);
        }

        $currency = Yii::app()->PriceManager->getCurrency();

        $modelReceiver = new InternationalMail_AddressData('receiver');
        $modelSender = new InternationalMail_AddressData('sender');

        $modelReceiver->attributes = $this->getPageState('step3_model',array());
        $modelSender->attributes = $this->getPageState('step2_model',array());

        $model->receiverAddressData = $modelReceiver;
        $model->senderAddressData = $modelSender;


        if(!Yii::app()->user->isGuest) $model->user_id = Yii::app()->user->id;


        $model->attributes = $this->getPageState('step1_model',array());

        $internationalMailAdditions = $this->getPageState('international_mail_additions',array());
        $model->addAdditions($internationalMailAdditions);

        $modelInternationalMailAddition = InternationalMailAdditionList::model()->findAllByAttributes(array('id' => $internationalMailAdditions));



        $priceEach = $model->priceEach($currency);
        $priceTotal = $model->priceTotal($currency);


        $this->setPageState('price',$priceTotal); // save previous form into form state
        $this->setPageState('last_step',5); // save information about last step

        $this->setPageState('jump_to_end',true);

        $this->render('create/step5',array(
            'model'=>$model,
            'price' => $priceTotal,
            'price_each' => $priceEach,
            'modelInternationalMailAddition' => $modelInternationalMailAddition,
        ));


    }

    protected function create_validate_step_5()
    {
        $error = false;
        $model=new InternationalMail('step5');
        $model->attributes = $_POST['InternationalMail'];
        $model->validate();
        $this->setPageState('step5',$model->getAttributes(array_keys($_POST['InternationalMail']))); // save previous form into form state

        if(Yii::app()->user->isGuest)
        {
            $currency = $this->getPageState('step1_currency', 1);
            Yii::app()->PriceManager->setCurrency($currency);
        }

        $currency = Yii::app()->PriceManager->getCurrency();

        if($model->hasErrors() OR $error)
        {
            $modelReceiver = new InternationalMail_AddressData('receiver');
            $modelSender = new InternationalMail_AddressData('sender');

            $modelReceiver->attributes = $this->getPageState('step3_model',array());
            $modelSender->attributes = $this->getPageState('step2_model',array());

            $model->receiverAddressData = $modelReceiver;
            $model->senderAddressData = $modelSender;

            $internationalMailAdditions = array();

            $model->addAdditions($this->getPageState('international_mail_additions',array()));

            $modelInternationalMailAddition = InternationalMailAdditionList::model()->findAllByAttributes(array('id' => $internationalMailAdditions));

            if(!Yii::app()->user->isGuest) $model->user_id = Yii::app()->user->id;


            $model->attributes = $this->getPageState('step1_model',array());

            $priceEach = $model->priceEach($currency);
            $priceTotal = $model->priceTotal($currency);


            $this->setPageState('price',$priceTotal); // save previous form into form state
            $this->setPageState('last_step',5); // save information about last step

            $this->setPageState('jump_to_end',true);

            $this->render('create/step5',array(
                'model'=>$model,
                'price' => $priceTotal,
                'price_each' => $priceEach,
                'modelInternationalMailAddition' => $modelInternationalMailAddition,
            ));
            return 'break';
        }
    }

    protected function create_finish()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 5)
        {

            if($this->create_validate_step_5() == 'break')
                return;

            $this->setPageState('last_step',6); // save information about last step

            $model = new InternationalMail('insert');

            $model->attributes = $this->getPageState('step1_model',array()); //get the info from step 1

            $model->receiverAddressData = new InternationalMail_AddressData('receiver');
            $model->receiverAddressData->attributes = $this->getPageState('step3_model',array());
            $model->senderAddressData = new InternationalMail_AddressData('sender');
            $model->senderAddressData->attributes = $this->getPageState('step2_model',array());


            $model->addAdditions($this->getPageState('international_mail_additions',array()));


            $saveSenderToContactBook = $this->getPageState('step2_saveToContactBook');
            $saveReceiverToContactBook = $this->getPageState('step3_saveToContactBook');

            $model->user_id = Yii::app()->user->id;
            $return = $model->saveNewIM($saveReceiverToContactBook, $saveSenderToContactBook);

            if($return instanceof Order)
            {
                Yii::app()->session['internationalMailFormFinished'] = true;

                /* @var $orderProduct OrderProduct */
                $order = Order::model()->findByPk($return->id); // to get proper hash
                Yii::app()->session['orderHash'] = $order->hash;

                $this->redirect(array('/order/finalizeOrder','urlData' => $order->hash));
            }
            throw new CHttpException(403, 'Wystapił nieznany błąd');


        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }


    public function actionCreate() {

        $this->panelLeftMenuActive = 401;

        if(Yii::app()->session['internationalMailFormFinished']) // prevent going back and sending the same form again
        {
            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['internationalMailFormFinished']);
            $this->refresh(true);
        }

        //for AJAX validation
        {

            if(isset($_POST['InternationalMail']))
            {
                $model=new InternationalMail('insert');
                $this->performAjaxValidation($model, 'international-mail-form');
                unset($model);
            }

        }

        if(isset($_POST['step2']))
            $this->create_step_2(); // Validate mail data, sender
        elseif(isset($_POST['step3']))
            $this->create_step_3(); // Validate sender, receiver
        elseif(isset($_POST['step4']))
            $this->create_step_4(); // Validate receivers, additions
        elseif(isset($_POST['step5']))
            $this->create_step_5(); // Validate additions, summary
        elseif (isset($_POST['finish']))
            $this->create_finish();
        else
            $this->create_step_1(); // this is the default, first time (step1)

    }

    public function actionList()
    {
        $this->panelLeftMenuActive = 402;

        $model = new InternationalMail('search');
        $model->unsetAttributes();

        if (isset($_GET['InternationalMai']))
            $model->setAttributes($_GET['InternationalMai']);

        $this->render('list', array(
            'model' => $model,
        ));

    }

    public function actionIndex() {

        $this->render('index', array(
        ));
    }


    public function actionCarriageTicket($urlData)
    {
        /* @var $model InternationalMail */
        $model = InternationalMail::model()->find('hash=:hash', array(':hash' => $urlData));

        if($model === null OR $model->international_mail_stat_id == InternationalMailStat::CANCELLED)
            throw new CHttpException('404');

        $type = $_POST['type'];


        $model->generateCarriageTicket($type);

    }

//    public function actionCarriageTicket($urlData)
//    {
//
//        $model = InternationalMail::model()->find('hash=:hash', array(':hash' => $urlData));
//
//
//        if($model === null) throw new CHttpException('404');
//
//        $model->generateCarriageTicket();
//
//    }

    public function actionMassCarriageTicketByGridView()
    {
        /* @var $model Internationalmail */
        $i = 0;
        if (isset($_POST['InternationalMail'])) {

            $international_mail_ids =  $_POST['InternationalMail']['ids'];

            $international_mail_ids = explode(',', $international_mail_ids);

            if(is_array($international_mail_ids) AND S_Useful::sizeof($international_mail_ids))
            {
                $international_mails = Array();
                foreach($international_mail_ids AS $id)
                {
                    $model = InternationalMail::model()->findByPk($id);
                    if($model !== null)
                    {
                        if($model->international_mail_stat_id == InternationalMailStat::PACKAGE_RETREIVE)
                            array_push($international_mails, $model);


                    }
                    else continue;

                    $i++;
                }
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('internationalMail/massCarriageTicke').'#mass-carriage-ticket');
        }

        InternationalMail::generateCarriageTickets($international_mails);
    }

}