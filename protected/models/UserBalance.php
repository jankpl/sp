<?php

Yii::import('application.models._base.BaseUserBalance');

class UserBalance extends BaseUserBalance
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    protected $_totalBalance = null;
    protected $_user;
    protected $_currency;


    public function afterConstruct()
    {
        return parent::afterConstruct();
    }

    public function beforeValidate()
    {
        if($this->_user === NULL)
            $this->_user = $this->user_id;

        if($this->currency == '')
            $this->currency = $this->getCurrency();

        $this->amount = str_replace(',','.', $this->amount);


        return parent::beforeValidate();
    }

    public function beforeSave()
    {
        if($this->_user === NULL)
            $this->_user = $this->user_id;

        if($this->currency == '')
            $this->currency = $this->getCurrency();
        return parent::beforeValidate();
    }

    /* @var $order Order */
    public function createWithOrder($order)
    {
        $this->user_id = $order->user_id;
        $this->amount = $order->value_brutto;
        $this->currency = $order->currency;
        $this->description = '#'.Yii::t('site','Order').' '.$order->id;
        $this->status = 1;
        $this->type = 1;
        $this->source = 1;
    }

    public function getUserModels()
    {

        if($this->_user === null) $user = Yii::app()->user->id;
        else $user = $this->_user;

        $models = self::model()->findAll('user_id = :user_id', array(':user_id' => $user));

        return $models;
    }

    public function calculateTotalBalance()
    {
        if($this->_user === null)
            $user = Yii::app()->user->id;
        else
            $user = $this->_user;

        if($this->_totalBalance === null)
        {
            $value = 0;
            $models = self::model()->findAll('user_id = :user_id', array(':user_id' => $user));

            /* @var $item UserBalance */
            foreach($models AS $item)
            {
                $value += $item->amount;
            }
            $this->_totalBalance = $value;

        } else {
            $value = $this->_totalBalance;
        }

        $value = -$value; // from user point of view

        $value = round($value,2);


        return $value;
    }

    public function calculateBalance()
    {
        if($this->_user === null)
            $user = Yii::app()->user->id;
        else
            $user = $this->_user;

        $value = [];
        foreach(Currency::listIdsWithNames() AS $currency)
            $value[$currency] = 0;

        $models = self::model()->findAll('user_id = :user_id AND id <= :id', array(':user_id' => $user, ':id' => $this->id));

        foreach($models AS $item)
        {
            $value[$item->currency] += $item->amount;
//            $value += $item->amount;
        }

//        $value = -$value; // from user point of view

//        $value = round($value,2);

        foreach($value AS $key => $item)
        {
            $value[$key] =-round($item,2); // from user point of view
        }

        return $value;

    }

    public static function calculateTotalBalanceForUserId($id)
    {

        $value = [];
        $models = self::model()->findAll('user_id = :user_id', array(':user_id' => $id));

        foreach(Currency::listIdsWithNames() AS $currency)
            $value[$currency] = 0;

        /* @var $item UserBalance */
        foreach($models AS $item)
        {
            $value[$item->currency] += $item->amount;
        }

        foreach($value AS $key => $item)
        {
            $value[$key] =-round($item,2); // from user point of view
        }

        return $value;
    }


    public function setUser($id)
    {
        $this->_user = $id;
    }

    public function setCurrency($id)
    {
        $this->_currency = $id;
    }

    /**
     * Returns default currency for user
     */
    public function getCurrency()
    {
        $user_id = $this->_user;
        $user = User::model()->findByPk($user_id);
        $currency = PriceCurrency::model()->findByPk($user->price_currency_id);
        return $currency->short_name;

//
//        $user_id = $this->_user;
//        $cmd = Yii::app()->db->createCommand();
//        $cmd->distinct = true;
//        $cmd->select('currency');
//        $cmd->from((new UserBalance())->tableName());
//        $cmd->where('user_id=:user_id', [':user_id' => $user_id]);
//        $currency = $cmd->queryAll();
//
//
//        // if this is first item in balance, just tak user's account currency
//        if(!S_Useful::sizeof($currency))
//        {
//            $user = User::model()->findByPk($user_id);
//            $currency = PriceCurrency::model()->findByPk($user->price_currency_id);
//            return $currency->short_name;
//        }
////        else if(S_Useful::sizeof($currency) > 1)
////            throw new CHttpException(403, Yii::t('site', 'Wystąpił błąd z bilansem Twojego konta. Skontaktuj się z nami w tej sprawie.'));
//        else
//            return $currency[0]['currency'];
    }

    /**
     * Function returning number of items from user balance
     *
     * @return integer
     */
    public function getItemsCount()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(id)');
        $cmd->from((new UserBalance())->tableName());
        $cmd->where('user_id=:user_id', [':user_id' => $this->_user]);

        if($this->_currency)
            $cmd->andWhere('currency = :currency', [':currency' => $this->_currency]);

        $result = $cmd->queryScalar();

        return $result;
    }

    /**
     * Function returning assoc arrays with items from user balance
     *
     * @param int $page Select page - 0 means first page
     * @param int $limit Set numer of items per page - 0 means no limit
     * @return array
     */
    public function getItemsWithBalance($currency, $page = 0, $limit = 0)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, date_entered, amount, description, (SELECT -SUM(amount) FROM '.(new UserBalance())->tableName().' AS a WHERE user_id = :user_id AND a.currency = :currency AND  a.id <= '.(new UserBalance())->tableName().'.id) AS suma');
        $cmd->from((new UserBalance())->tableName());
        $cmd->where('user_id=:user_id AND currency = :currency', [':user_id' => $this->_user, ':currency' => strtoupper($currency)]);
        $cmd->order('date_entered DESC');
        if($limit > 0) {
            $cmd->limit($limit);
            $cmd->offset($page * $limit);
        }
        $result = $cmd->queryAll();

        return $result;
    }

    // DEPRECATED
//    public function getCurrency()
//    {
//
//        $id = $this->_user;
//        $models = self::model()->findAll('user_id = :user_id', array(':user_id' => $id));
//        if(!S_Useful::sizeof($models))
//        {
//            /* @var $user User */
//            $user = User::model()->findByPk($id);
//            return $user->priceCurrency->short_name;
//        }
//
//        $currency = NULL;
//
//        foreach($models AS $model)
//        {
//            if($currency === NULL)
//            {
//                $currency = $model->currency;
//                continue;
//            }
//
//            if($model->currency != $currency)
//                throw new CHttpException(403, Yii::t('site', 'Wystąpił błąd z bilansem Twojego konta. Skontaktuj się z nami w tej sprawie.'));
//        }
//
//
//        return $currency;
//    }

// DEPRECATED
//    public function addToBalance()
//    {
//
//    }



}