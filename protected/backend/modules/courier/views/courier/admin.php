<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<style>
    p.vertical {
        writing-mode: vertical-rl;
        text-orientation: upright;
        word-break: normal;
    }
</style>

<h2>Manage <?php echo GxHtml::encode((new Courier)->label(2)); ?></h2>

<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO,'You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.', array('closeText' => false));?>

<?php
$cmd = Yii::app()->db->createCommand();
$cmd->select('COUNT(*)');
$cmd->from((new Courier)->tableName());
$cmd->where('courier_stat_id = :stat', [':stat' => CourierStat::PACKAGE_PROBLEMS]);
$packagesWithProblems = $cmd->queryScalar();

if($packagesWithProblems)
{
    echo '<div class="alert alert-danger">Number of packages with problem: <strong>'.$packagesWithProblems.'</strong></div>';
}
?>

<?php
if(0):
    ?>
    <?php
    $cmd = Yii::app()->db->createCommand();
    $cmd->select('COUNT(*)');
    $cmd->from((new Courier)->tableName());
    $cmd->where('user_cancel = :stat', [':stat' => Courier::USER_CANCEL_REQUEST]);
    $cancelRequest = $cmd->queryScalar();

    if($cancelRequest)
    {
        echo '<div class="alert alert-danger">Number of packages with cancel request: <strong>'.$cancelRequest.'</strong></div>';
    }
endif;
?>

<?php
$cmd = Yii::app()->db->createCommand();
$cmd->select('COUNT(*)');
$cmd->from((new Courier)->tableName());
$cmd->where('user_cancel = :stat', [':stat' => Courier::USER_CANCELL_REQUESTED_FOUND_TT]);
$cancelWarned = $cmd->queryScalar();

if($cancelWarned)
{
    echo '<div class="alert alert-danger">Number of packages with cancel warnings: <strong>'.$cancelWarned.'</strong></div>';
}
?>


<br/>

<fieldset>
    <legend>Prefilter items</legend>
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'export-to-file',
        'enableAjaxValidation' => false,
//    'action' => Yii::app()->createUrl('/courier/courier/searchByRemote'),
    ));
    ?>

    <div class="overflow: auto;">
        <div style="width: 48%; float: left; text-align: right; vertical-align: top;"><?= Chtml::textArea('preFilter', $preFilter, ['placeholder' => '123456789ABC1'.PHP_EOL.'123456789ABC2'.PHP_EOL.'123456789ABC3', 'style' => 'height: 100px; width: 100%;']); ?></div>
        <div style="width: 48%; float: right; text-align: left; vertical-align: top;">
            To pre-filter list, provide local ids, remote ids or refs. Each one in new line.
            <br/>
            <br/>
            <?= TbHtml::submitButton('Prefilter'); ?>
            <br/>
            <?php
            if($preFilter !=''):
                ?>
                <?= TbHtml::submitButton('Clear', ['name' => 'preFilterClear', 'style' => 'margin-top: 10px;']); ?>
            <?php
            endif;
            ?>
        </div>
    </div>




    <?php $this->endWidget(); ?>
</fieldset>
<hr/>

<div style="word-break: break-all">

    <?php
//    $this->widget('MultipleSelect', array('selector' => '#stat-selector;#operator-selector'));
    ?>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
    <?php
    $multipleSelectAttributes = '#stat-selector;#operator-selector;#receiver-country-selector;#user-selector;#sender-country-selector;#user-selector';

    $dataRangeScript = '
setDataRange($("input[name=\"CourierGridView[date_entered]\"]", false, false));
setDataRange($("input[name=\"CourierGridView[date_updated]\"]", false, false));

function setDataRange(input, start, end)
{

var begin = moment("2014-01-01");
var finish = moment();

if(start)
    start = moment(start);
else
    start = begin;

if(end)
    end = moment(end);
else
    end = finish;

input.daterangepicker({
    startDate: start,
    endDate: end,
    autoApply: true,
    autoUpdateInput: false,
    showDropdowns: true,
    ranges: {
        "Today": [
            moment(),
            moment(),
        ],
        "Yesterday": [
           moment().subtract(1, "day"),
           moment().subtract(1, "day")
        ],
        "Last 7 days": [
           moment().subtract(7, "day"),
           moment()
        ],      
        "This month": [
           moment().startOf("month"),
           moment()
        ],
        "Previous month": [
           moment().subtract(1, "month").startOf("month"),
           moment().subtract(1, "month").endOf("month")
        ],
         "This year": [
             moment().startOf("year"),
           moment()
        ],
         "Full range": [
         begin,
         finish    
        ]
    },
    locale: {
        format: "MM/DD/YYYY",
        separator: " - ",
        applyLabel: "Ok",       
        weekLabel: "T",       
        firstDay: 1
    },
    showCustomRangeLabel: true,
    alwaysShowCalendars: false,  
    maxDate: finish,
    minDate: begin,
    opens: "center",
    drops: "up"
}, function(start, end, label) {
if(start.isValid() && end.isValid)
   input.val(start.format("YYYY-MM-DD") + "|" + end.format("YYYY-MM-DD")).trigger("change");
   else
      input.val("").trigger("change");

});

}';

    $dataRangeScriptAfterUpdate = ' var de = $(\'input[name="CourierGridView[date_entered]"]\').val();
             de = de.split("|"); 
             
             var de_start = false;
             var de_end = false;
             if($.isArray(de) && de.length == 2)
             {
               de_start = de[0];
               de_end = de[1];
             }    
             
             var du = $(\'input[name="CourierGridView[date_updated]"]\').val();
             du = du.split("|"); 
             
             var du_start = false;
             var du_end = false;
             if($.isArray(du) && du.length == 2)
             {
               du_start = du[0];
               du_end = du[1];
             }    
                  
            $(\'.gridview-overlay\').hide();
          setDataRange($(\'input[name="CourierGridView[date_entered]"]\'), de_start, de_end);
          setDataRange($(\'input[name="CourierGridView[date_updated]"]\'), du_start, du_end);
          ';

    $this->widget('MultipleSelect', array('selector' => $multipleSelectAttributes));
    ?>
    <?php $this->_getGridViewCourier($preModels, $multipleSelectAttributes, $dataRangeScriptAfterUpdate);?>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
</div>


<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/daterangepicker/moment.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/daterangepicker/daterangepicker.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/daterangepicker/daterangepicker.css');

Yii::app()->clientScript->registerScript('daterangepicker', $dataRangeScript, CClientScript::POS_READY);

?>

<h3>Actions for selected</h3>

<?php echo TbHtml::tabbableTabs(array(
    array('label' => 'Change statuses', 'active' => true, 'content' => $this->renderPartial('admin_actions_tabs/_change_statuses', array(), true)),
    array('label' => 'Delete last status', 'content' => $this->renderPartial('admin_actions_tabs/_delete_last_status', array(), true)),
    array('label' => 'Refresh stat map', 'content' => $this->renderPartial('admin_actions_tabs/_refresh_stat_map', array(), true)),
    array('label' => 'Export to file', 'content' => $this->renderPartial('admin_actions_tabs/_export_to_file', array(), true)),
    array('label' => 'Generate labels', 'content' => $this->renderPartial('admin_actions_tabs/_generate_labels', array(), true)),
    array('label' => 'Generate Manifest/ACK', 'content' => $this->renderPartial('admin_actions_tabs/_generate_ack_cards', array(), true)),
), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>


