<?php
/* @var $text string */
/* @var $model Courier */
/* @var $status string */
/* @var $addEnAnnouncement boolean */
/* @var $addEnIntro boolean */
/* @var $toWho string RECEIVER/SENDER/OWNER */
/* @var $packageNo string */
?>
<?php if($addEnAnnouncement):?>
    <div style="font-style: italic; font-size: 0.8em;">
        ---------------------------------<br/>
        <?= Yii::t('mail_courier', 'English version is on the bottom of this email [{a}go to english version{/a}].', ['{a}' => '<a href="#en">', '{/a}' => '</a>']); ?>
        <br/>---------------------------------
    </div><br/>
<?php endif; ?>
<?php if($addEnIntro):?>
<br/>
<div style="font-size: 0.8em;" id="en">
    <br/>---------------------------------<br/>
    <?php endif; ?>
    <?= $text;?><br/><br/>
    <strong style="padding-left: 15px; font-size: 1.2em;"><?= $status; ?></strong><br/><br/>

    <?php
    // 08.11.2016
    // SPECIAL RULE FOR USER "CREATA" (337)
    // 01.12.2016
    // AND FOR USER "DEKOGLAS" (625)
    // 12.01.2017
    // AND FOR USER "STK" (681)

    $showDefaultTrackingLink = true;

    if($toWho == 'RECEIVER'):
        if (($model->courierTypeInternal->deliveryLabel !== NULL && '' != $extTt = $model->courierTypeInternal->deliveryLabel->getTrack_id())
            OR
            ($model->courierTypeInternal->commonLabel !== NULL && '' != $extTt = $model->courierTypeInternal->commonLabel->getTrack_id())
        ):

            if(in_array($model->user_id, [337,625,681])
                &&
                ($model->getType() == Courier::TYPE_INTERNAL &&
                    $model->receiverAddressData->country_id == CountryList::COUNTRY_DE &&
                    ($model->courierTypeInternal->delivery_operator_id == 19 OR $model->courierTypeInternal->deliveryOperator->broker == CourierOperator::BROKER_DHLDE) // DHL DE
                )):
                $showDefaultTrackingLink = false;
                ?>

                <strong><?= Yii::t('mail_courier', 'Operator:');?></strong> DHL DE<br/>
                <strong><?= Yii::t('mail_courier', 'Zew. numer TT:');?></strong> <?= $extTt;?> (<a href="https://nolp.dhl.de/nextt-online-public/de/search?piececode=<?= $extTt;?>"><?= Yii::t('mail_courier', 'sprawdź status');?></a>)<br/><br/>

            <?php
            elseif($model->user_id == 3233 &&
                ($model->getType() == Courier::TYPE_INTERNAL &&
                    $model->receiverAddressData->country_id == CountryList::COUNTRY_DE &&
                    ($model->courierTypeInternal->deliveryOperator->broker == CourierOperator::BROKER_DPD_DE) // DPD DE
                )):
                $showDefaultTrackingLink = false;
                ?>

                <strong><?= Yii::t('mail_courier', 'Operator:');?></strong> DPD DE<br/>
                <strong><?= Yii::t('mail_courier', 'Zew. numer TT:');?></strong> <?= $extTt;?> (<a href="https://tracking.dpd.de/status/de_DE/parcel/<?= $extTt;?>"><?= Yii::t('mail_courier', 'sprawdź status');?></a>)<br/><br/>

            <?php
            endif;
        endif;
    endif;
    ?>

    <?php
    if($showDefaultTrackingLink) {
        $baseUrl = PageVersion::lang2domain(Yii::app()->language);
        echo Yii::t('mail_courier', 'Paczkę możesz śledzić {a}tutaj{/a}.', array('{a}' => '<a href="' . $baseUrl . '/tt/' . $packageNo . '">', '{/a}' => '</a>')); ?>
        <br/><br/>
        <?php
    }
    ?>

    <strong><?= Yii::t('mail_courier', 'Dane paczki:');?></strong><br/>
    <?= Yii::t('mail_courier', 'Wymiary {unit}: {w}x{s}x{d}', [
        '{unit}' => '['.Courier::getDimensionUnit().']',
        '{w}' => $model->package_size_d,
        '{s}' => $model->package_size_l,
        '{d}' => $model->package_size_w,
    ]);?><br/>
    <?= Yii::t('mail_courier', 'Waga {unit}: {n}', [
        '{unit}' =>  '['.Courier::getWeightUnit().']',
        '{n}' =>$model->package_weight,
    ]);?><br/>
    <?= Yii::t('mail_courier', 'Opis:');?> <?= $model->package_content; ?>
    <br/><br/>

    <strong><?= Yii::t('mail_courier', 'Nadawca paczki:');?></strong><br/>
    <?= $model->senderAddressData->getUsefulName(); ?><br/>
    <?php
    // @ 21.04.2017
    // SPECIAL RULE FOR "KASTOR" (718)
    // SPECIAL RULE FOR (#1797) @ 23.04.2018
    if(!in_array($model->user_id, [718]) && !$model->user->getHideSenderAddressOnNotificationAndTt()):
        ?>
        <?= $model->senderAddressData->address_line_1; ?><br/>
        <?= $model->senderAddressData->address_line_2 != '' ? $model->senderAddressData->address_line_2.'<br/>' : '';?>
        <?= $model->senderAddressData->city;?>, <?= $model->senderAddressData->zip_code;?><br/>
        <?php
        // 05.12.2016
        // FOR USER "DEKOGLAS" (625)
        if($toWho != 'RECEIVER'
            OR ($model->user_id != 625)
        ):    ?>
            <?= $model->senderAddressData->getCountryNameUniversal();?><br/>
        <?php
        endif;
        ?>
    <?php
    endif;
    ?>
    <br/>
    <strong><?= Yii::t('mail_courier', 'Odbiorca paczki:');?></strong><br/>
    <?= $model->receiverAddressData->getUsefulName(); ?><br/>
    <?= $model->receiverAddressData->address_line_1; ?><br/>
    <?= $model->receiverAddressData->address_line_2 != '' ? $model->receiverAddressData->address_line_2.'<br/>' : '';?>
    <?= $model->receiverAddressData->city;?>, <?= $model->receiverAddressData->zip_code;?><br/>
    <?= $model->receiverAddressData->getCountryNameUniversal();?>
    <?php if($model->isCod()): ?>
        <br/><br/><strong><?= Yii::t('mail_courier', 'Paczka została wysłana za pobraniem: ');?></strong> <?= $model->getCodValue();?> <?= $model->getCodCurrency();?>
    <?php endif; ?>
    <?php if($addEnIntro):?>
    <br/>---------------------------------<br/>
</div>
<?php endif; ?>



