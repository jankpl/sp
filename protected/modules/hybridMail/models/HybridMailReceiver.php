<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailReceiver');

class HybridMailReceiver extends BaseHybridMailReceiver
{

    /*public function init()
    {
        if($this->addressData === null) $this->addressData = new HybridMail_AddressData();
        return parent::init();
    }*/


    public function getReceiverCountry()
    {
        return $this->addressData->country0;
    }

    public function getReceiver_country_id()
    {
        return $this->addressData->country_id;
    }

    public function getReceiver_name()
    {
        return $this->addressData->name;
    }

    public function getReceiver_company()
    {
        return $this->addressData->company;
    }

    public function getReceiver_country()
    {
        return $this->addressData->country;
    }

    public function getReceiver_zip_code()
    {
        return $this->addressData->zip_code;
    }

    public function getReceiver_city()
    {
        return $this->addressData->city;
    }

    public function getReceiver_address_line_1()
    {
        return $this->addressData->address_line_1;
    }

    public function getReceiver_address_line_2()
    {
        return $this->addressData->address_line_2;
    }

    public function getReceiver_tel()
    {
        return $this->addressData->tel;
    }

    public function setReceiverCountry($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->country0 = $val;
    }

    public function setReceiver_country_id($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->country_id = $val;
    }

    public function setReceiver_name($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->name = $val;
    }

    public function setReceiver_company($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->company = $val;
    }

    public function setReceiver_country($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->country = $val;
    }

    public function setReceiver_zip_code($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->zip_code = $val;
    }

    public function setReceiver_city($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->city = $val;
    }

    public function setReceiver_address_line_1($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->address_line_1 = $val;
    }

    public function setReceiver_address_line_2($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->address_line_2 = $val;
    }

    public function setReceiver_tel($val)
    {
        if($this->addressData === NULL) $this->addressData = new HybridMail_AddressData('receiver');
        $this->addressData->tel = $val;
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(

            array('address_data_id,
            ', 'required'),

            array('hybrid_mail_id, id', 'length', 'max'=>10),
            array('id, hybrid_mail_id, address_data_id', 'safe', 'on'=>'search, set_safe'),
        );
    }

    public static function createWithAddressDataAttributes($attributes)
    {
        $model = new HybridMailReceiver();
        $model->addressData = new HybridMail_AddressData('receiver');
        $model->addressData->attributes = $attributes;

        return $model;
    }

    public function saveWithAddressData($ownTransaction = false, $hybrid_mail_id)
    {

       if($ownTransaction)
           $transaction = Yii::app()->db->beginTransaction();

        if($this->addressData === NULL)
            $this->addressData = new HybridMail_AddressData('receiver');


        $this->addressData->validate();

        if(!$this->addressData->hasErrors())
            $this->addressData->save();

        $this->hybrid_mail_id = $hybrid_mail_id;
        $this->address_data_id = $this->addressData->id;

        $this->validate();

        if(!$this->hasErrors())
        {

                if($this->save())
                {
                    if($ownTransaction)
                        $transaction->commit();
                    return true;
                }
        }
        else
        {
            if($ownTransaction)
                $transaction->rollback();
            return false;
        }


    }
}