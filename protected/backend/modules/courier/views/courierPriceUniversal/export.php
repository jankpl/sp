<?php
/* @var $data array */
/* @var $countriesModels array */
/* @var $model F_CourierRoutingExport */
?>

    <h1>Export courier pricing</h1>

<?php

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
    array(
        'enableAjaxValidation' => false,
    )
);
?>

<?php
$this->widget('FlashPrinter');
?>

<?php

echo $form->errorSummary($model);
?>

    <table class="table table-striped" style="margin: 10px auto;">
        <tr>
            <td style="width: 300px;"><?php echo $form->labelEx($model,'country_list_id', ['style' => 'width: 100%;']); ?></td>
            <td><?php echo $form->dropDownList($model, 'country_list_id', CHtml::listData($model->listCountryGroups(),'id','name'), ['multiple' => true, 'data-drag-select' => true]); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->labelEx($model,'user_group_id', ['style' => 'width: 100%;']); ?></td>
            <td>
                <?php
                $groups[-1] = '-- DEFAULT --';
                $groups +=  CHtml::listData(UserGroup::listGroups(),'id','name');
                echo $form->dropdownList($model,'user_group_id', $groups, ['prompt' => '-']); ?>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><?php echo TbHtml::submitButton(Yii::t('app', 'Export'), array('class' => 'btn btn-lg btn-primary'));?></td>
        </tr>
    </table>
<?php
$this->endWidget();
?>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/dragSelect/js/jquery.multi-select.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/dragSelect/css/multi-select.css');
$script = "
$('[data-drag-select]').multiSelect({
selectableHeader: '<div class=\"drag-select-header drag-select-header-available\">Available:</div>',
  selectionHeader: '<div class=\"drag-select-header drag-select-header-selected\">Selected:</div>',
  })
";
Yii::app()->clientScript->registerScript('drag-select', $script, CClientScript::POS_READY);
$css = '
.ms-container{ 
  width: 100%;
}
.ms-container .ms-list{
height: 350px;
}

.drag-select-header
{
background: lightgray;
font-weight: bold;
padding: 2px;
text-indent: 2px;
}

.drag-select-header-available
{
}

.drag-select-header-selected
{
background: lightblue;
}

.ms-selection ul
{
font-weight: bold;
}
';
Yii::app()->clientScript->registerCss('drag-select', $css);

