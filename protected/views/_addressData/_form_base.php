<?php
/* @var $bankAccountEnabled boolean */

$array = [];

?>

<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'country_id'); ?>
    <?php echo $form->dropDownList($model,(isset($i)?'['.$i.']':'').'country_id', $countries===NULL?CHtml::listData(CountryList::model()->with('countryListTr')->findAll(),'id','trName'):CHtml::listData($countries,'id','trName')); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'country_id'); ?>
</div><!-- row -->
<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'company'); ?>
    <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'company', array('maxlength' => 45)); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'company'); ?>
</div><!-- row -->

<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'name'); ?>
    <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'name', array('maxlength' => 45)); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'name'); ?>
</div><!-- row -->
<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
    <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_1', array('maxlength' => 45)); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
</div><!-- row -->
<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
    <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_2', array('maxlength' => 45)); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
</div><!-- row -->
<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
    <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'zip_code', array('maxlength' => 45)); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
</div><!-- row -->
<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'city'); ?>
    <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'city', array('maxlength' => 45)); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'city'); ?>
</div><!-- row -->
<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'tel'); ?>
    <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'tel', array('maxlength' => 45)); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'tel'); ?>
</div><!-- row -->
<div class="row">
    <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'email'); ?>
    <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'email', array('maxlength' => 45)); ?>
    <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'email'); ?>
</div><!-- row -->
<?php
if($bankAccountEnabled):
    ?>
    <div data-bank-accound-enabled="true">
        <br/>
        <div style="text-align: center; font-weight: bold;">
            <?= Yii::t('contactBook', 'For COD:');?>
        </div>
        <br/>
        <div class="row">
            <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'bankAccount'); ?>
            <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'bankAccount', array('maxlength' => 50)); ?>
            <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'bankAccount'); ?>
            <div style="font-style: italic; font-size: 0.8em; text-align: center;"><?= Yii::t('courier', 'Nadawca przesyłki musi być właścicielem powyższego konta bankowego.');?></div>
        </div><!-- row -->
    </div>
    <?php
endif;
?>
