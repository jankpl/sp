<?php

class InternationalMailAdditionListController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'InternationalMailAdditionList'),
        ));
    }

    public function actionCreate() {

        $model = new InternationalMailAdditionList;
        $model->price = new Price();
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $additionTr = new InternationalMailAdditionListTr();
            $additionTr->language_id = $item->id;
            $modelTrs[$item->id] = $additionTr;
        }

        if (isset($_POST['InternationalMailAdditionList'])) {
            $model->setAttributes($_POST['InternationalMailAdditionList']);
            $model->price = Price::generateByPost($_POST['PriceValue']);

            foreach($_POST['InternationalMailAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new InternationalMailAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->internationalMailAdditionListTrs = $modelTrs;

            if ($model->saveWithTrAndPrice()) {
                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,

        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'InternationalMailAdditionList');

        $modelTrs = Array();


        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = InternationalMailAdditionListTr::model()->find('international_mail_addition_list_id=:international_mail_addition_list_id AND language_id=:language_id', array(':international_mail_addition_list_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $additionTr = new InternationalMailAdditionListTr();
                $additionTr->language_id = $item->id;
                $additionTr->international_mail_addition_list_id = $id;
                $modelTrs[$item->id] = $additionTr;
            }
        }


        if (isset($_POST['InternationalMailAdditionList'])) {
            $model->setAttributes($_POST['InternationalMailAdditionList']);

            $model->price = Price::generateByPost($_POST['PriceValue']);

            foreach($_POST['InternationalMailAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new InternationalMailAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->internationalMailAdditionListTrs = $modelTrs;


            if ($model->saveWithTrAndPrice()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $this->loadModel($id, 'InternationalMailAdditionList')->delete();
            }
            catch(Exception $ex)
            {
                throw new CHttpException(403,'Nie udało się usunąć tego wpisu!');
            }


            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new InternationalMailAdditionList('search');
        $model->unsetAttributes();

        if (isset($_GET['InternationalMailAdditionList']))
            $model->setAttributes($_GET['InternationalMailAdditionList']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionStat($id)
    {
        /* @var $model InternationalMailAdditionList */


        $model = InternationalMailAdditionList::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}