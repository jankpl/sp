<?php

/* @var $model Courier */
/* @var $actions String[] */
/* @var $new boolean */


$type = $model->getType();


$stat = $model->courier_stat_id;
?>

<?php
if($new && $model->packages_number > 1 && !Yii::app()->user->getModel()->getMultipackagesAsOneItem() && (($model->courierTypeU && $model->courierTypeU->family_member_number == 1) OR ($model->courierTypeInternal && $model->courierTypeInternal->family_member_number == 1))):
    ?>
    <script>
        <?php
        if(!isset($_SESSION['openChildren_'.$model->id]))
        {
            Yii::app()->session->add('openChildren_'.$model->id, true);
            $children = $model->findChildren();
            foreach($children AS $item)
                echo 'window.open("'.Yii::app()->createUrl('/courier/courier/view', ['urlData' => $item->hash, 'new' => true]).'","_blank");';
        }
        ?>
    </script>
<?php
endif;
?>


<h2><?php echo Yii::t('courier','Przesyłka #{id}', array('{id}' => $model->local_id));?></h2>

<?php
if($model->courier_stat_id == CourierStat::NEW_ORDER):
    ?>
    <div class="alert alert-warning text-center"><?= Yii::t('courier', 'Ta paczka oczekuje na opłacenie zamówienia.');?></div>
<?php
endif;
?>

<?php
if($new OR $stat == CourierStat::PACKAGE_PROCESSING):
    ?>
    <div style="min-height: 100px;">
        <div class="alert alert-info text-center" id="label-waiting" style="display: <?= $stat != CourierStat::PACKAGE_PROCESSING ? 'none' : '';?>">
            <br/>
            <strong><?= Yii::t('courier', 'Ta paczka oczekuje na wygenerowanie etykiety.');?></strong>
            <br/>
            <br/>
            <i class="fa fa-clock-o" aria-hidden="true" style="font-size: 4em;" id="clock-animation"></i>
            <br/><br/>
            <?= Yii::t('courier', 'Opcja pobrania etykiety będzie automatycznie dostępna po wygenerowaniu etykiety.');?>
        </div>

        <div class="alert alert-success text-center" id="label-ready" style="display: <?= in_array($stat,[CourierStat::PACKAGE_PROCESSING, CourierStat::NEW_ORDER]) ? 'none' : '';?>">
            <br/>
            <strong><?= Yii::t('courier', 'Etykieta jest gotowa do pobrania!');?></strong>
            <br/>
            <br/>
            <i class="fa fa-check-square-o" style="font-size: 4em;" aria-hidden="true"></i>
        </div>
    </div>
    <?php
    if($stat == CourierStat::PACKAGE_PROCESSING):
        ?>
        <script>
            $(document).ready(function(){
                var $labelActionBlockOverlay = $('#label-action-block .overlay');

                $labelActionBlockOverlay.show();

                var $clock = $('#clock-animation');

                function runIt() {
                    $clock.animate({opacity:'1'}, 1000);
                    $clock.animate({opacity:'0.1'}, 1000, runIt);
                }
                runIt();

                function checkData()
                {
                    $.ajax({
                        url: '<?= Yii::app()->createUrl('/courier/courier/ajaxIsLabelReady');?>',
                        data: { hash: '<?= $model->hash;?>'},
                        method: 'POST',
                        dataType: 'json',
                        success: function(result){
                            console.log(result);
                            if(result)
                            {
                                $('#label-waiting').fadeOut('fast', function(){
                                    $('#label-ready').fadeIn('fast', function(){
                                        $labelActionBlockOverlay.hide();
                                    });
                                });

                            } else
                                setTimeout(checkData, 1000);

                        },
                        error: function(e){
                            console.log(e);
                        }
                    });
                }

                checkData();
            });
        </script>
    <?php
    endif;
endif;
?>

<?php
if(in_array($model->courier_stat_id,[CourierStat::CANCELLED_PROBLEM])):
    ?>

    <div class="alert alert-danger"><?= Yii::t('courier', 'Ta paczka zotała automatycznie anulowana z powodu błędu. Pobierz etykietę, aby poznać szczegóły.');?></div>

<?php
endif;
?>


<?php
$this->widget('FlashMessages');
?>

<?php
if($model->isLabelArchived())
    echo '<div class="alert alert-warning">'.Yii::t('courier', 'Etykieta dla tej paczki już wygasła').'</div>';
?>
<?php
if($model->user_cancel == Courier::USER_CANCELL_COMPLAINED)
    echo '<div class="alert alert-warning">'.Yii::t('courier', 'Ta paczka została zgłoszona do reklamacji').'</div>';
?>

<?php
if(S_Useful::sizeof($actions)):
    ?>
    <fieldset>
        <legend><?= Yii::t('courier','Akcje');?></legend>

        <?php
        foreach($actions AS $item):
            ?>
            <div style="width: 350px; display: inline-block; margin: 10px; vertical-align: top;">
                <?php echo $item; ?>
            </div>
        <?php
        endforeach;
        ?>
    </fieldset>
<?php
endif;
?>


<div class="row">
    <div class="col-xs-12 col-sm-6">

        <h4><?php echo Yii::t('courier','Szczegóły przesyłki'); ?></h4>



        <?php
        $rows = array(
            array(
                'name' => 'local_id',
                'value' => $model->local_id,
            ),
            array(
                'name' => 'date_entered',
                'value' => substr($model->date_entered,0,16)
            ),
            array(
                'name' => 'date_updated',
                'value' => substr($model->date_entered,0,16)
            ),
            array(
                'name' => 'stat',
                'value' => StatMap::getMapNameById($model->stat_map_id),
            ),
            array(
                'name' => 'package_weight',
                'value' => $model->package_weight.' '.Courier::getWeightUnit(). ($model->package_weight != $model->package_weight_client && $model->package_weight_client ? ' ('.Yii::t('courier', 'deklarowana').': '.$model->package_weight_client.' '.Courier::getWeightUnit().')' : ''),
            ),
            array(
                'label' => Yii::t('courier','Wymiary'),
                'value' => $model->package_size_d.' '.Courier::getDimensionUnit().' x '.$model->package_size_l.' '.Courier::getDimensionUnit().' x '.$model->package_size_w.' '.Courier::getDimensionUnit(),
            ),
            array(
                'name' => 'package_content',
                'value' => $model->package_content,
            ),
            array(
                'name' => 'package_value',
                'value' => $model->package_value.' '.$model->getPackageValueCurrency(),
            ),
            array(
                'name' => 'cod_value',
                'value' => $model->cod_value ? $model->cod_value.' '.$model->cod_currency : '-',
            ),
            array(
                'name' => 'note1',
                'value' => $model->note1,
            ),
            array(
                'name' => 'note2',
                'value' => $model->note2,
            ),
        );

        if($model->ref)
        {
            $rows[] = [
                'name' => 'ref',
                'value' => $model->ref.' [<a href="'.Yii::app()->createUrl('/courier/courier/removeRef', ['hash' => $model->hash]).'" onclick="return confirm(\''.Yii::t('site', 'Czy jesteś pewien?').'\')"><u>'.Yii::t('courier', 'skasuj').'</u></a>]',
                'type' => 'raw',
            ];
        }

        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=> $rows
        ));
        ?>
        <br/>
        <table class="detail-view">
            <tr class="even">
                <th><?= Yii::t('courier','Type');?></th>
                <td><?= $model->getTypeName(true);?></td>
            </tr>
            <?php if($type == Courier::TYPE_EXTERNAL):?>
                <tr class="odd">
                    <th><?= Yii::t('courier', 'Operator');?></th>
                    <td><?= $model->courierTypeExternal->courierExternalOperator->name;?></td>
                </tr>
                <tr class="even">
                    <th><?= Yii::t('courier', 'Nr zew');?></th>
                    <td><?= $model->courierTypeExternal->external_id;?></td>
                </tr>
            <?php endif; ?>
            <?php if($type == Courier::TYPE_RETURN):?>
                <tr class="odd">
                    <th><?= $model->courierTypeReturn->getAttributeLabel('base_courier_id'); ?></th>
                    <td><a target="_blank" href="<?= Yii::app()->createUrl('/courier/courier/view', ['urlData' => $model->courierTypeReturn->baseCourier->hash]);?>">#<?= $model->courierTypeReturn->baseCourier->local_id;?></a></td>
                </tr>
                <tr class="even">
                    <th><?= Yii::t('courier', 'Operator');?></th>
                    <td><?= CourierLabelNew::getOperators()[$model->courierTypeReturn->courierLabelNew->operator];?></td>
                </tr>
                <tr class="odd">
                    <th><?= Yii::t('courier', 'Nr zew');?></th>
                    <td><?= $model->courierTypeReturn->courierLabelNew->track_id;?></td>
                </tr>
            <?php endif; ?>
            <?php if($type == Courier::TYPE_U):?>
                <tr class="odd">
                    <th><?= $model->courierTypeU->getAttributeLabel('collection'); ?></th>
                    <td><?= $model->courierTypeU->getCollectionName(); ?></td>
                </tr>
                <tr class="even">
                    <th> <?= Yii::t('courier', 'Paczka');?></th>
                    <td>  <?= $model->courierTypeU->family_member_number;?>/<?= $model->packages_number;?></td>
                </tr>
                <tr class="odd">
                    <th><?= Yii::t('courier', 'Zamówienie');?></th>
                    <td>#<a href="<?= Yii::app()->urlManager->createUrl('/order/view', ['urlData' => $model->courierTypeU->order->hash]);?>"><?= $model->courierTypeU->order->local_id; ?></a></td>
                </tr>
            <?php endif; ?>
            <?php if($type == Courier::TYPE_INTERNAL):?>
                <tr class="odd">
                    <th><?= $model->courierTypeInternal->getAttributeLabel('with_pickup'); ?></th>
                    <td><?= $model->courierTypeInternal->getWithPickupName(); ?></td>
                </tr>
                <tr class="even">
                    <th> <?= Yii::t('courier', 'Paczka');?></th>
                    <td>  <?= $model->courierTypeInternal->family_member_number;?>/<?= $model->packages_number;?></td>
                </tr>
                <tr class="odd">
                    <th><?= Yii::t('courier', 'Zamówienie');?></th>
                    <td>#<a href="<?= Yii::app()->urlManager->createUrl('/order/view', ['urlData' => $model->courierTypeInternal->order->hash]);?>"><?= $model->courierTypeInternal->order->local_id; ?></a></td>
                </tr>
                <?php
                if(!Yii::app()->user->isGuest && Yii::app()->user->model->getShowCourierInternalOperatorsData()):
                    $operatorsList = $model->courierTypeInternal->listOperatorsWithTt();
                    if(S_Useful::sizeof($operatorsList)):
                        ?>
                        <tr class="<?= $even ? 'even' : 'odd';?>">
                            <th><?= Yii::t('courier', 'Operatorzy');?></th>
                            <td><?php
                                foreach($operatorsList AS $operator)
                                    echo $operator['name'].($operator['tt'] != '' ? ' ('.$operator['tt'].')' : '').'<br/>';
                                ?></td>
                        </tr>
                        <?php
                        $even = !$even;
                    endif;
                endif;
                ?>
            <?php elseif($type == Courier::TYPE_DOMESTIC):?>
                <tr class="odd">
                    <th><?= Yii::t('courier', 'Operator');?></th>
                    <td><?= $model->courierTypeDomestic->getOperator() ;?></td>
                </tr>
                <tr class="even">
                    <th><?= Yii::t('courier', 'Nr zew.');?></th>
                    <td><?= $model->courierTypeDomestic->courierLabelNew->track_id ;?></td>
                </tr>
                <tr class="odd">
                    <th><?= Yii::t('courier', 'Paczka');?></th>
                    <td> <?= $model->courierTypeDomestic->family_member_number;?>/<?= $model->packages_number;?></td>
                </tr>
                <?php if($model->courierTypeDomestic->_ruch_receiver_point_address != ''):?>
                    <tr class="<?= $even ? 'even' : 'odd';?>">
                        <th><?= $model->courierTypeDomestic->getAttributeLabel('_ruch_receiver_point_address'); ?></th>
                        <td><?= $model->courierTypeDomestic->_ruch_receiver_point_address; ?></td>
                    </tr>
                    <?php
                    $even = !$even;
                    ?>
                <?php
                endif;
                ?>
            <?php
            elseif($type == Courier::TYPE_INTERNAL_SIMPLE):?>
                <tr class="odd">
                    <th><?= Yii::t('courier', 'Operator');?></th>
                    <td><?= $model->courierTypeInternalSimple->getOperator();?></td>
                </tr>
                <tr class="even">
                    <th><?= Yii::t('courier', 'Nr zew.');?></th>
                    <td><?= $model->courierTypeInternalSimple->courierExtOperatorDetails->courierLabelNew->track_id ;?></td>
                </tr>
                <tr class="odd">
                    <th><?= $model->courierTypeInternalSimple->getAttributeLabel('with_pickup'); ?></th>
                    <td><?= $model->courierTypeInternalSimple->with_pickup ? Yii::t('site', 'tak') : Yii::t('site', 'nie'); ?></td>
                </tr>
            <?php elseif($type == Courier::TYPE_INTERNAL_OOE):?>
                <tr class="odd">
                    <th><?= Yii::t('courier', 'Operator');?></th>
                    <td><?= $model->courierTypeOoe->findServiceName() ;?></td>
                </tr>
                <tr class="even">
                    <th><?= Yii::t('courier', 'Nr zew.');?></th>
                    <td><?= $model->courierTypeOoe->findRemoteId() ;?></td>
                </tr>
                <?php
                $even = false;
                ?>
            <?php endif;?>
            <?php
            if(!Yii::app()->user->isGuest && (($model->getType() == Courier::TYPE_INTERNAL && Yii::app()->user->model->getShowCourierInternalOperatorsData()) OR $model->getType() != Courier::TYPE_INTERNAL)):
                $collect = CourierCollect::findForCourierId($model->id);
                if($collect !== NULL && $collect->prn != '' && $collect->stat == CourierCollect::STAT_SUCCESS):
                    ?>
                    <tr class="<?= $even ? 'even' : 'odd';?>">
                        <th><?= Yii::t('courier', 'Numer obioru');?></th>
                        <td><?= $collect->prn; ?></td>
                    </tr>
                <?php
                endif;
            endif;
            ?>
        </table>
    </div>
    <?php
    if($type == Courier::TYPE_INTERNAL):
        ?>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Dodatki');?></h4>
            <table class="list hTop" style="width: 100%;">
                <tr>
                    <td><?php echo Yii::t('courier','Nazwa');?></td>
                    <td><?php echo Yii::t('courier','Opis');?></td>
                </tr>
                <?php

                /* @var $item CourierAddition */
                if(!S_Useful::sizeof($model->courierTypeInternal->listAdditions()))
                    echo '<tr><td colspan="2">'.Yii::t('courier','brak').'</td></tr>';
                else
                    foreach($model->courierTypeInternal->listAdditions() AS $item): ?>
                        <tr>
                            <td><?php echo $item->getClientTitle(); ?></td>
                            <td><?php echo $item->getClientDesc(); ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        </div>
    <?php
    elseif($type == Courier::TYPE_U):
        ?>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Dodatki');?></h4>
            <table class="list hTop" style="width: 100%;">
                <tr>
                    <td><?php echo Yii::t('courier','Nazwa');?></td>
                    <td><?php echo Yii::t('courier','Opis');?></td>
                </tr>
                <?php

                /* @var $item CourierAddition */
                if(!S_Useful::sizeof($model->courierTypeU->listAdditions()))
                    echo '<tr><td colspan="2">'.Yii::t('courier','brak').'</td></tr>';
                else
                    foreach($model->courierTypeU->listAdditions() AS $item): ?>
                        <tr>
                            <td><?php echo $item->getClientTitle(); ?></td>
                            <td><?php echo $item->getClientDesc(); ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        </div>
    <?php elseif($type == Courier::TYPE_DOMESTIC):?>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Dodatki');?></h4>
            <table class="list hTop" style="width: 100%;">
                <tr>
                    <td><?php echo Yii::t('courier','Nazwa');?></td>
                    <td><?php echo Yii::t('courier','Opis');?></td>
                </tr>
                <?php

                /* @var $item CourierDomesticAddition */
                if(!S_Useful::sizeof($model->courierTypeDomestic->listAdditions()))
                    echo '<tr><td colspan="2">'.Yii::t('courier','brak').'</td></tr>';
                else
                    foreach($model->courierTypeDomestic->listAdditions() AS $item): ?>
                        <tr>
                            <td><?php echo $item->getClientTitle(); ?></td>
                            <td><?php echo $item->getClientDesc(); ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        </div>
    <?php elseif($type == Courier::TYPE_INTERNAL_OOE):?>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Dodatki');?></h4>
            <table class="list hTop" style="width: 100%;">
                <tr>
                    <td><?php echo Yii::t('courier','Nazwa');?></td>
                    <td><?php echo Yii::t('courier','Opis');?></td>
                </tr>
                <?php

                /* @var $item CourierOoeAddition */
                if(!S_Useful::sizeof($model->courierTypeOoe->listAdditions()))
                    echo '<tr><td colspan="2">'.Yii::t('courier','brak').'</td></tr>';
                else
                    foreach($model->courierTypeOoe->listAdditions() AS $item): ?>
                        <tr>
                            <td><?php echo $item->getClientTitle(); ?></td>
                            <td><?php echo $item->getClientDesc(); ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        </div>
    <?php endif; ?>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-6">
        <h4><?php echo Yii::t('courier','Nadawca'); ?></h4>
        <?php
        if($model->senderAddressData->country_id)
            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));
        ?>
    </div>
    <div class="col-xs-12 col-sm-6">
        <h4><?php echo Yii::t('courier','Odbiorca'); ?></h4>
        <?php
        if($model->receiverAddressData->country_id)
            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));
        ?>
    </div>
</div>

<?php
if(!$model->isStatHistoryArchived()):
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Historia');?></h4>

            <?php
            $this->renderPartial('//_statHistory/_statHistory',
                array('model' => $model->courierStatHistoriesVisible));
            ?>
        </div>
    </div>

    <h4><?php echo Yii::t('site','Odnośnik');?></h4>
    <?php echo CHtml::link(Yii::t('site','Śledź zamówienie #{number}', array('{number}' => $model->local_id)), Yii::app()->createAbsoluteUrl('/site/tt', array('urlData' => $model->local_id))); ?>

<?php
endif;
?>

<?php $this->widget('BackLink');?>
