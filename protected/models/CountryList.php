<?php

Yii::import('application.models._base.BaseCountryList');

class CountryList extends BaseCountryList
{
    const COUNTRY_PL = 1;
    const COUNTRY_DE = 142;
    const COUNTRY_UK = 217;

    const COUNTRY_AT = 15;
    const COUNTRY_BE = 21;
    const COUNTRY_HR = 37;
    const COUNTRY_CY = 39;
    const COUNTRY_DK = 43;
    const COUNTRY_CZ = 42;
    const COUNTRY_EE = 51; // Estonia
    const COUNTRY_FI = 56;
    const COUNTRY_FR = 57;
    const COUNTRY_GR = 62;
    const COUNTRY_NL = 77;
    const COUNTRY_IS = 84; // Iceland
    const COUNTRY_LT = 115;
    const COUNTRY_LU = 116; // Luxembourg
    const COUNTRY_LV = 117; // Lativa
    const COUNTRY_MT = 126; // Malta
    const COUNTRY_PT = 162; // Portugal
    const COUNTRY_RO = 168;
    const COUNTRY_SK = 183;
    const COUNTRY_SI = 184; // Slovenia
    const COUNTRY_IT = 219;
    const COUNTRY_BG = 32;
    const COUNTRY_ES = 76;
    const COUNTRY_IE = 84; // Irleand
    const COUNTRY_HU = 216;
    const COUNTRY_SE = 194;

    const COUNTRY_CA = 95;
    const COUNTRY_AU = 14;

    const COUNTRY_US = 187;

    const COUNTRY_BR = 30;
    const COUNTRY_IL = 86; // Israel

    const COUNTRY_CH = 193; // Switzerland

    const COUNTRY_ND = 249; // Northern Ireland


    const COUNTRY_NO = 148; // Norway

    public function __get($name)
    {
       if($name == 'code')
       {
           $oryginalValue = parent::__get($name);
           if($oryginalValue == 'ND')
               return 'GB';
           else
               return $oryginalValue;
       }
       else
           return parent::__get($name);
    }

    public function getCodeForMatrix()
    {
        return $this->getAttribute('code');
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function defaultScope()
    {

        return array(
            'order' => 'CASE WHEN '.$this->tableAlias.'.id = 1
                            THEN 0
                        ELSE 1
                            END ASC
                     , '.$this->tableAlias.'.name ASC'
        );
    }

    public static function countryCodeToId($code)
    {
        $code = trim($code);
        $cmd = Yii::app()->db->createCommand();
        return $cmd->select('id')->from('country_list')->where('code = :code', [':code' => $code])->queryScalar();
    }

    public static function isUe($country_id, $withPl = true, $withUk = true)
    {
        return in_array($country_id, self::getUeList($withPl, $withUk));
    }

    public static function getUeList($withPl = true, $withUk = true)
    {

        $data = [];
        if($withPl)
            $data[] = self::COUNTRY_PL;

        if($withUk) {
            $data[] = self::COUNTRY_UK;
            $data[] = self::COUNTRY_ND;
        }

        $data = array_merge($data , [
            self::COUNTRY_DE,
            self::COUNTRY_AT,
            self::COUNTRY_BE,
            self::COUNTRY_HR,
            self::COUNTRY_CY,
            self::COUNTRY_DK,
            self::COUNTRY_CZ,
            self::COUNTRY_EE,
            self::COUNTRY_FI,
            self::COUNTRY_FR,
            self::COUNTRY_GR,
            self::COUNTRY_NL,
            self::COUNTRY_IS,
            self::COUNTRY_LT,
            self::COUNTRY_LU,
            self::COUNTRY_LV,
            self::COUNTRY_MT,
            self::COUNTRY_PT,
            self::COUNTRY_RO,
            self::COUNTRY_SK,
            self::COUNTRY_SI,
            self::COUNTRY_IT,
            self::COUNTRY_BG,
            self::COUNTRY_ES,
            self::COUNTRY_IE,
            self::COUNTRY_HU,
            self::COUNTRY_SE,
        ]);

        return $data;
    }


    public function getNamesForSuggester()
    {
        $name = '';

        foreach($this->countryListTrs AS $item)
        {
            if($item->name != '')
                $name .= $item->name.'|';
        }

        //$name .= $this->name .'|';
        $name .= $this->code;

        return $name;
    }

    public function getTrName()
    {
        if($this->countryListTr == '')
            return $this->name;
        else
            return $this->countryListTr->name;
    }

    public function getFirstLetter()
    {
        return substr($this->trName,0,1);
    }


    public function getContinentName()
    {
//        return self::continents()[$this->continent_id];
    }

    public function rules() {
        return array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),

            array('code', 'unique'),

            array('name, code', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>45),
            array('code', 'length', 'max'=>3),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, code, stat', 'safe', 'on'=>'search'),
        );
    }

    public static function getIdByName($name)
    {

        $country = self::model()->find('name LIKE :name', array(':name' => '%'.$name.'%'));
        $id = $country->id;

        return $id;
    }

    public function toggleStat()
    {
        $this->stat = abs($this->stat - 1);
        return $this->save();
    }

    public function getTranslatedName()
    {
        return $this->trName;
    }

    /**
     * @param bool|false $asAssocArray
     * @return array|static[]
     */
    public static function getActiveCountries($asAssocArray = false)
    {
        if($asAssocArray)
        {

            $models = self::model()->with('countryListTr')->findAll(array(
                'condition' => 'stat = :stat',
                'params' => array('stat' => S_Status::ACTIVE),
            ));

            $countryList = CHtml::listData($models,'id','trName');

            asort($countryList);

            $temp = [];
            $temp[CountryList::COUNTRY_PL] = $countryList[CountryList::COUNTRY_PL];

            unset($countryList[CountryList::COUNTRY_PL]);
            $countryList = $temp + $countryList;

            return $countryList;
        } else {
            $models = self::model()->with('countryListTr')->findAll(array(
                'condition' => 'stat = :stat',
                'params' => array('stat' => S_Status::ACTIVE),
                'order' => 'countryListTr.name',
            ));
        }

        return $models;
    }

    public static function findIdByText($text)
    {

        $cmd = Yii::app()->db->cache(60*60)->createCommand();
//        $cmd = Yii::app()->db->createCommand();
        $cmd->select('country_list.id');
        $cmd->from('country_list');
        $cmd->join('country_list_tr', 'country_list_tr.country_list_id = country_list.id');
        $cmd->where('country_list.id = :text', array(':text' => $text));
        $cmd->orWhere('country_list.code = :text', array(':text' => $text));
        $cmd->orWhere('country_list.name = :text', array(':text' => $text));
        $cmd->orWhere('country_list_tr.name = :text', array(':text' => $text));
        $cmd->order('id ASC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if(!$result)
            return NULL;

        return $result;
    }

    public static function findCodeByText($text)
    {

        $cmd = Yii::app()->db->cache(60*60)->createCommand();
//        $cmd = Yii::app()->db->createCommand();
        $cmd->select('country_list.code');
        $cmd->from('country_list');
        $cmd->join('country_list_tr', 'country_list_tr.country_list_id = country_list.id');
        $cmd->where('country_list.id = :text', array(':text' => $text));
        $cmd->orWhere('country_list.code = :text', array(':text' => $text));
        $cmd->orWhere('country_list.name = :text', array(':text' => $text));
        $cmd->orWhere('country_list_tr.name = :text', array(':text' => $text));

        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if(!$result)
            return NULL;

        return $result;
    }

    /**
     * Returns country ISO Code
     *
     * @param $id int Id of country
     * @return boolean|string
     */
    public static function getCodeById($id)
    {
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        $cmd->select('code');
        $cmd->from('country_list');
        $cmd->where('id = :id', [':id' => $id]);
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        return $result;
    }

    public static function getIdByCode($code)
    {
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        $cmd->select('id');
        $cmd->from('country_list');
        $cmd->where('code = :code', [':code' => $code]);
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        return $result;
    }


    public function getCodeIso3()
    {
        return (isset(self::codeIso2To3List()[strtoupper($this->code)])) ? self::codeIso2To3List()[strtoupper($this->code)] : NULL;
    }

    public static function codeIso2To3($code)
    {
        return (isset(self::codeIso2To3List()[strtoupper($code)])) ? self::codeIso2To3List()[strtoupper($code)] : NULL;
    }

    public static function codeIso2To3List()
    {
        $data = [
            'AF' => 'AFG',
            'AX' => 'ALA',
            'AL' => 'ALB',
            'DZ' => 'DZA',
            'AS' => 'ASM',
            'AD' => 'AND',
            'AO' => 'AGO',
            'AI' => 'AIA',
            'AQ' => 'ATA',
            'AG' => 'ATG',
            'AR' => 'ARG',
            'AM' => 'ARM',
            'AW' => 'ABW',
            'AU' => 'AUS',
            'AT' => 'AUT',
            'AZ' => 'AZE',
            'BS' => 'BHS',
            'BH' => 'BHR',
            'BD' => 'BGD',
            'BB' => 'BRB',
            'BY' => 'BLR',
            'BE' => 'BEL',
            'BZ' => 'BLZ',
            'BJ' => 'BEN',
            'BM' => 'BMU',
            'BT' => 'BTN',
            'BO' => 'BOL',
            'BA' => 'BIH',
            'BW' => 'BWA',
            'BV' => 'BVT',
            'BR' => 'BRA',
            'VG' => 'VGB',
            'IO' => 'IOT',
            'BN' => 'BRN',
            'BG' => 'BGR',
            'BF' => 'BFA',
            'BI' => 'BDI',
            'KH' => 'KHM',
            'CM' => 'CMR',
            'CA' => 'CAN',
            'CV' => 'CPV',
            'KY' => 'CYM',
            'CF' => 'CAF',
            'TD' => 'TCD',
            'CL' => 'CHL',
            'CN' => 'CHN',
            'HK' => 'HKG',
            'MO' => 'MAC',
            'CX' => 'CXR',
            'CC' => 'CCK',
            'CO' => 'COL',
            'KM' => 'COM',
            'CG' => 'COG',
            'CD' => 'COD',
            'CK' => 'COK',
            'CR' => 'CRI',
            'CI' => 'CIV',
            'HR' => 'HRV',
            'CU' => 'CUB',
            'CY' => 'CYP',
            'CZ' => 'CZE',
            'DK' => 'DNK',
            'DJ' => 'DJI',
            'DM' => 'DMA',
            'DO' => 'DOM',
            'EC' => 'ECU',
            'EG' => 'EGY',
            'SV' => 'SLV',
            'GQ' => 'GNQ',
            'ER' => 'ERI',
            'EE' => 'EST',
            'ET' => 'ETH',
            'FK' => 'FLK',
            'FO' => 'FRO',
            'FJ' => 'FJI',
            'FI' => 'FIN',
            'FR' => 'FRA',
            'GF' => 'GUF',
            'PF' => 'PYF',
            'TF' => 'ATF',
            'GA' => 'GAB',
            'GM' => 'GMB',
            'GE' => 'GEO',
            'DE' => 'DEU',
            'GH' => 'GHA',
            'GI' => 'GIB',
            'GR' => 'GRC',
            'GL' => 'GRL',
            'GD' => 'GRD',
            'GP' => 'GLP',
            'GU' => 'GUM',
            'GT' => 'GTM',
            'GG' => 'GGY',
            'GN' => 'GIN',
            'GW' => 'GNB',
            'GY' => 'GUY',
            'HT' => 'HTI',
            'HM' => 'HMD',
            'VA' => 'VAT',
            'HN' => 'HND',
            'HU' => 'HUN',
            'IS' => 'ISL',
            'IN' => 'IND',
            'ID' => 'IDN',
            'IR' => 'IRN',
            'IQ' => 'IRQ',
            'IE' => 'IRL',
            'IM' => 'IMN',
            'IL' => 'ISR',
            'IT' => 'ITA',
            'JM' => 'JAM',
            'JP' => 'JPN',
            'JE' => 'JEY',
            'JO' => 'JOR',
            'KZ' => 'KAZ',
            'KE' => 'KEN',
            'KI' => 'KIR',
            'KP' => 'PRK',
            'KR' => 'KOR',
            'KW' => 'KWT',
            'KG' => 'KGZ',
            'LA' => 'LAO',
            'LV' => 'LVA',
            'LB' => 'LBN',
            'LS' => 'LSO',
            'LR' => 'LBR',
            'LY' => 'LBY',
            'LI' => 'LIE',
            'LT' => 'LTU',
            'LU' => 'LUX',
            'MK' => 'MKD',
            'MG' => 'MDG',
            'MW' => 'MWI',
            'MY' => 'MYS',
            'MV' => 'MDV',
            'ML' => 'MLI',
            'MT' => 'MLT',
            'MH' => 'MHL',
            'MQ' => 'MTQ',
            'MR' => 'MRT',
            'MU' => 'MUS',
            'YT' => 'MYT',
            'MX' => 'MEX',
            'FM' => 'FSM',
            'MD' => 'MDA',
            'MC' => 'MCO',
            'MN' => 'MNG',
            'ME' => 'MNE',
            'MS' => 'MSR',
            'MA' => 'MAR',
            'MZ' => 'MOZ',
            'MM' => 'MMR',
            'NA' => 'NAM',
            'NR' => 'NRU',
            'NP' => 'NPL',
            'NL' => 'NLD',
            'AN' => 'ANT',
            'NC' => 'NCL',
            'NZ' => 'NZL',
            'NI' => 'NIC',
            'NE' => 'NER',
            'NG' => 'NGA',
            'NU' => 'NIU',
            'NF' => 'NFK',
            'MP' => 'MNP',
            'NO' => 'NOR',
            'OM' => 'OMN',
            'PK' => 'PAK',
            'PW' => 'PLW',
            'PS' => 'PSE',
            'PA' => 'PAN',
            'PG' => 'PNG',
            'PY' => 'PRY',
            'PE' => 'PER',
            'PH' => 'PHL',
            'PN' => 'PCN',
            'PL' => 'POL',
            'PT' => 'PRT',
            'PR' => 'PRI',
            'QA' => 'QAT',
            'RE' => 'REU',
            'RO' => 'ROU',
            'RU' => 'RUS',
            'RW' => 'RWA',
            'BL' => 'BLM',
            'SH' => 'SHN',
            'KN' => 'KNA',
            'LC' => 'LCA',
            'MF' => 'MAF',
            'PM' => 'SPM',
            'VC' => 'VCT',
            'WS' => 'WSM',
            'SM' => 'SMR',
            'ST' => 'STP',
            'SA' => 'SAU',
            'SN' => 'SEN',
            'RS' => 'SRB',
            'SC' => 'SYC',
            'SL' => 'SLE',
            'SG' => 'SGP',
            'SK' => 'SVK',
            'SI' => 'SVN',
            'SB' => 'SLB',
            'SO' => 'SOM',
            'ZA' => 'ZAF',
            'GS' => 'SGS',
            'SS' => 'SSD',
            'ES' => 'ESP',
            'LK' => 'LKA',
            'SD' => 'SDN',
            'SR' => 'SUR',
            'SJ' => 'SJM',
            'SZ' => 'SWZ',
            'SE' => 'SWE',
            'CH' => 'CHE',
            'SY' => 'SYR',
            'TW' => 'TWN',
            'TJ' => 'TJK',
            'TZ' => 'TZA',
            'TH' => 'THA',
            'TL' => 'TLS',
            'TG' => 'TGO',
            'TK' => 'TKL',
            'TO' => 'TON',
            'TT' => 'TTO',
            'TN' => 'TUN',
            'TR' => 'TUR',
            'TM' => 'TKM',
            'TC' => 'TCA',
            'TV' => 'TUV',
            'UG' => 'UGA',
            'UA' => 'UKR',
            'AE' => 'ARE',
            'GB' => 'GBR',
            'ND' => 'GBR',
            'US' => 'USA',
            'UM' => 'UMI',
            'UY' => 'URY',
            'UZ' => 'UZB',
            'VU' => 'VUT',
            'VE' => 'VEN',
            'VN' => 'VNM',
            'VI' => 'VIR',
            'WF' => 'WLF',
            'EH' => 'ESH',
            'YE' => 'YEM',
            'ZM' => 'ZMB',
            'ZW' => 'ZWE',
        ];

        return $data;
    }

}