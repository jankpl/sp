<?php

class LinexClient extends GlobalOperator
{
    const URL = 'http://interface.lentongrp.com/ShipmentInformation.asmx';
    const URL_TT = 'http://interface.lentongrp.com/ShipmentTracking.asmx';
    const USERNAME = 'ROMLIM';
    const PASSWORD = 'romLim2018@!';

    public $sender_name;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_tel;
    public $sender_email;

    public $receiver_name;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;
    public $package_content;
    public $package_value;

    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $local_id;

    public $insurance = false;

    public static function getAdditions(Courier $courier)
    {
        $CACHE_NAME = 'LINEX_ADDITIONS_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_LINEX_INSURANCE;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

//    public static function test()
//    {
//        $data = new stdClass();
//
//        $data->UserName = self::USERNAME;
//        $data->Password = self::PASSWORD;
//
//        $data->ShipmentAPI = new stdClass();
//        $data->ShipmentAPI->ShipmentNumber = rand(1,99999999);
//        $data->ShipmentAPI->CustomerReference = '1234678';
////        $data->ShipmentAPI->PickupDatetime = date('Y-m-d').' '.date('H:i:s');
//        $data->ShipmentAPI->PickupDatetime = date('Y-m-d');
////        $data->ShipmentAPI->PickupDatetime = date("c", time());
//        $data->ShipmentAPI->Origin = 'HKG';
//        $data->ShipmentAPI->Destination = 'CTU';
//        $data->ShipmentAPI->ServiceCode = 'ADC';
//        $data->ShipmentAPI->ServiceName = 'SAM';
//        $data->ShipmentAPI->ShipmentType = 'N';
//
//        $data->ShipmentAPI->ConsignorName = 'Abc';
//        $data->ShipmentAPI->ConsignorAddress1 = 'Abc';
//        $data->ShipmentAPI->ConsignorAddress2 = 'Abc';
//        $data->ShipmentAPI->ConsignorAddress3 = '';
//        $data->ShipmentAPI->ConsignorAddress4 = '';
//        $data->ShipmentAPI->ConsignorContactPerson = 'Abc';
//        $data->ShipmentAPI->ConsignorPhone = '123123123';
//        $data->ShipmentAPI->ConsignorEmail = 'dsdasads@dsadsa.pl';
//        $data->ShipmentAPI->ConsignorZipCode = '53-400';
//        $data->ShipmentAPI->ConsignorCity = '123456';
//        $data->ShipmentAPI->ConsignorProvice = '';
//        $data->ShipmentAPI->ConsignorCountry = 'CN';
//
//        $data->ShipmentAPI->ConsigneeName = 'Abc';
//        $data->ShipmentAPI->ConsigneeAddress1 = 'Abc';
//        $data->ShipmentAPI->ConsigneeAddress2 = 'Abc';
//        $data->ShipmentAPI->ConsigneeAddress3 = '';
//        $data->ShipmentAPI->ConsigneeAddress4 = '';
//        $data->ShipmentAPI->ConsigneeZipCode = '44-200';
//        $data->ShipmentAPI->ConsigneeProvice = '';
//        $data->ShipmentAPI->ConsigneeCity = '123456';
//        $data->ShipmentAPI->ConsigneeCountry = 'CN';
//        $data->ShipmentAPI->ConsigneeContactPerson = 'Abc';
//        $data->ShipmentAPI->ConsigneePhone = '123123123';
//        $data->ShipmentAPI->ConsigneeEmail = 'dsdasads@dsadsa.pl';
//        $data->ShipmentAPI->ConsigneeIdCopy = '';
//
//
//        $data->ShipmentAPI->Pieces = 1;
//        $data->ShipmentAPI->ActualWeight = 1.4;
//        $data->ShipmentAPI->VolumetricWeight = 1.4;
//        $data->ShipmentAPI->DutyPaidBy = 'R';
//        $data->ShipmentAPI->Insured = 'Y';
//        $data->ShipmentAPI->Insurance_Value = 100;
//        $data->ShipmentAPI->DeclaredCurrency = 'CNY';
//        $data->ShipmentAPI->DeclaredValue = 100;
//        $data->ShipmentAPI->Content = 'Content....';
//        $data->ShipmentAPI->Instructions = '';
//
//
//
//        $sdl = new stdClass();
//        $sdl->BrandName = 'Test';
//        $sdl->ShipmentContentName = 'Test';
//        $sdl->HSCode = '';
//        $sdl->ShipmentDescription = 'Test';
//        $sdl->ShipmentUnit = 'KG';
//        $sdl->ShipmentQuantity = 1;
//        $sdl->ShipmentCurrencyCode = 'CNY';
//        $sdl->ShipmentSinglePrice = 100;
//        $sdl->Shipment_Single_Weight = 1.4;
//
//        $data->ListDetailAPI = [];
//        $data->ListDetailAPI[] = $sdl;
//
//        $sda = new stdClass;
//        $sda->Lengths = 10;
//        $sda->Widths = 10;
//        $sda->Depths = 10;
//
//        $data->ListDimensionsAPI = [];
//        $data->ListDimensionsAPI[] = $sda;
//
////        $data->ListChildAPI = [];
//
//        $model = new self;
//
//        $resp = $model->_soapOperation('InsertShipmentInfo', $data);
//
//        var_dump($resp);
//        $shipmentNumber = (string) $resp->data->ShipmentNumber;
//
//
//        $data = new stdClass();
//        $data->UserName = self::USERNAME;
//        $data->Password = self::PASSWORD;
//        $data->ShipmentNumberList = $shipmentNumber;
//
//        $resp = $model->_soapOperation('GetRPXLabelByByte', $data);
//
//        $label = $resp->data;
//
//        $im = ImagickMine::newInstance();
//        $im->setResolution(300, 300);
//        $im->readImageBlob($label);
//
//        $imgWidth = $im->getImageWidth();
//        $imgHeight = $im->getImageHeight();
//
//        $im->cropImage($imgWidth, $imgHeight / 2, 0, $imgHeight / 2);
//
////                    $im->annotateImage($draw, 50, 50, 0, 'DPD DE');
//
//        $im->setImageFormat('png8');
//        if ($im->getImageAlphaChannel()) {
//            $im->setImageBackgroundColor('white');
//            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//        }
//
//        $im->trimImage(0);
//        $im->rotateImage(new ImagickPixel(), 90);
//        $im->stripImage();
//
//        file_put_contents('linnex.png', $im->getImageBlob());
//
//
//    }

    protected function _createShipment($returnError = false)
    {
//        if ($returnError == true) {
//            return GlobalOperatorResponse::createErrorResponse('Not active!');
//        } else {
//            return false;
//        }

        $data = new stdClass();

        $data->UserName = self::USERNAME;
        $data->Password = self::PASSWORD;

        $data->ShipmentAPI = new stdClass();
        $data->ShipmentAPI->ShipmentNumber = $this->local_id;
        $data->ShipmentAPI->CustomerReference = $this->local_id;
//        $data->ShipmentAPI->PickupDatetime = date('Y-m-d').' '.date('H:i:s');
        $data->ShipmentAPI->PickupDatetime = date('Y-m-d');
//        $data->ShipmentAPI->PickupDatetime = date("c", time());
        $data->ShipmentAPI->Origin = CountryList::codeIso2To3($this->sender_country);
        $data->ShipmentAPI->Destination = CountryList::codeIso2To3($this->receiver_country);
        $data->ShipmentAPI->ServiceCode = 'ADC';
        $data->ShipmentAPI->ServiceName = 'SAM';
        $data->ShipmentAPI->ShipmentType = 'N';

        $data->ShipmentAPI->ConsignorName = $this->sender_name;
        $data->ShipmentAPI->ConsignorAddress1 = $this->sender_address1;
        $data->ShipmentAPI->ConsignorAddress2 = $this->sender_address2;
        $data->ShipmentAPI->ConsignorAddress3 = '';
        $data->ShipmentAPI->ConsignorAddress4 = '';
        $data->ShipmentAPI->ConsignorContactPerson = '';
        $data->ShipmentAPI->ConsignorPhone = $this->sender_tel;
        $data->ShipmentAPI->ConsignorEmail = $this->sender_email;
        $data->ShipmentAPI->ConsignorZipCode = $this->sender_zip_code;
        $data->ShipmentAPI->ConsignorCity = $this->sender_city;
        $data->ShipmentAPI->ConsignorProvice = '';
        $data->ShipmentAPI->ConsignorCountry = $this->sender_country;

        $data->ShipmentAPI->ConsigneeName = $this->receiver_name;
        $data->ShipmentAPI->ConsigneeAddress1 = $this->receiver_address1;
        $data->ShipmentAPI->ConsigneeAddress2 = $this->receiver_address2;
        $data->ShipmentAPI->ConsigneeAddress3 = '';
        $data->ShipmentAPI->ConsigneeAddress4 = '';
        $data->ShipmentAPI->ConsigneeZipCode = $this->receiver_zip_code;
        $data->ShipmentAPI->ConsigneeProvice = '';
        $data->ShipmentAPI->ConsigneeCity = $this->receiver_city;
        $data->ShipmentAPI->ConsigneeCountry = $this->receiver_country;
        $data->ShipmentAPI->ConsigneeContactPerson = '';
        $data->ShipmentAPI->ConsigneePhone = $this->receiver_tel;
        $data->ShipmentAPI->ConsigneeEmail = $this->receiver_email;
        $data->ShipmentAPI->ConsigneeIdCopy = '';


        $data->ShipmentAPI->Pieces = 1;
        $data->ShipmentAPI->ActualWeight = $this->package_weight;
        $data->ShipmentAPI->VolumetricWeight = round(($this->package_size_l * $this->package_size_d * $this->package_size_w) / 6000,2);
        $data->ShipmentAPI->DutyPaidBy = 'R';
        $data->ShipmentAPI->Insured = $this->insurance ? 'Y' : 'N';
        $data->ShipmentAPI->Insurance_Value = $this->insurance ? $this->package_value : 0;
        $data->ShipmentAPI->DeclaredCurrency = 'EUR';
        $data->ShipmentAPI->DeclaredValue = $this->package_value;
        $data->ShipmentAPI->Content = $this->package_content;
        $data->ShipmentAPI->Instructions = '';



        $sdl = new stdClass();
        $sdl->BrandName = '';
        $sdl->ShipmentContentName = $this->package_content;
        $sdl->HSCode = '';
        $sdl->ShipmentDescription = $this->package_content;
        $sdl->ShipmentUnit = 'KG';
        $sdl->ShipmentQuantity = 1;
        $sdl->ShipmentCurrencyCode = 'EUR';
        $sdl->ShipmentSinglePrice = $this->package_value;
        $sdl->Shipment_Single_Weight = $this->package_weight;

        $data->ListDetailAPI = [];
        $data->ListDetailAPI[] = $sdl;

        $sda = new stdClass;
        $sda->Lengths = $this->package_size_l;
        $sda->Widths = $this->package_size_w;
        $sda->Depths = $this->package_size_d;

        $data->ListDimensionsAPI = [];
        $data->ListDimensionsAPI[] = $sda;

        $model = new self;

        $resp = $model->_soapOperation('InsertShipmentInfo', $data, self::URL);

        if($resp->success)
        {
            $shipmentNumber = (string) $resp->data->ShipmentNumber;

            $data = new stdClass();
            $data->UserName = self::USERNAME;
            $data->Password = self::PASSWORD;
            $data->ShipmentNumberList = $shipmentNumber;

            $resp = $model->_soapOperation('GetRPXLabelByByte', $data, self::URL);

            $label = $resp->data;

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImageBlob($label);

            $imgWidth = $im->getImageWidth();
            $imgHeight = $im->getImageHeight();

            $im->cropImage($imgWidth, $imgHeight / 2, 0, $imgHeight / 2);

            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            $im->trimImage(0);
            $im->rotateImage(new ImagickPixel(), 90);
            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $shipmentNumber);
            return $return;

        }
        else
        {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }
    }

    protected function _soapOperation($method, $data ,$url)
    {
        $mode = [
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context' => stream_context_create(
                [
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false
                    ],
                ]
            )
        ];

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        try {

            $client = new SoapClient($url.'?wsdl', $mode);
            $resp = $client->$method($data);

        } catch (Exception $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('linnex.txt', print_r($E->getMessage(),1));
            MyDump::dump('linnex.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('linnex.txt', print_r($client->__getLastResponse(),1));
            return $return;
        }

        MyDump::dump('linnex.txt', print_r($client->__getLastRequest(),1));
        MyDump::dump('linnex.txt', print_r($client->__getLastResponse(),1));

        $resultName = $method.'Result';
        $xml = $resp->$resultName;

        if($method == 'InsertShipmentInfo') {
            $xml = $xml->any;
            $xml = str_replace('xs:', '', $xml);
            preg_match('@<DocumentElement xmlns="">(.*)</DocumentElement>@i', $xml, $xml);
            $xml = $xml[1];
            $xml = @simplexml_load_string($xml);

            if($xml->Result == 'true')
            {
                $return->success = true;
                $return->data = $xml;
            } else {

                $error = (string) $xml->Message;

                $return->error = $error;
                $return->errorCode = $error;
                $return->errorDesc = $error;
            }
        } else {
            $return->success = true;
            $return->data = $xml;
        }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnError)
    {
        $model = new self;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_LINEX_INSURANCE))
            $model->insurance = true;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_tel = $from->tel;
        $model->sender_email = $from->email;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->email;

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->package_content = $courierInternal->courier->package_content;
        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_value = $courierInternal->courier->getPackageValueConverted('EUR');

        $model->local_id = $courierInternal->courier->local_id;

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, CourierLabelNew $cln, $returnError = false)
    {

        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model = new self;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_LINEX_INSURANCE))
            $model->insurance = true;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_tel = $from->tel;
        $model->sender_email = $from->email;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->package_weight = $courierTypeU->courier->getWeight(true);
        $model->package_content = $courierTypeU->courier->package_content;
        $model->package_size_l = $courierTypeU->courier->package_size_l;
        $model->package_size_d = $courierTypeU->courier->package_size_d;
        $model->package_size_w = $courierTypeU->courier->package_size_w;

        $model->package_value = $courierTypeU->courier->getPackageValueConverted('EUR');

        $model->local_id = $courierTypeU->courier->local_id;

        return $model->_createShipment($returnError);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return false;


        $model = new self;

        $data = new stdClass();
        $data->UserName = self::USERNAME;
        $data->Password = self::PASSWORD;
        $data->ShipmentNumber = $no;

        $resp = $model->_soapOperation('TrackingInformatoin', $data, self::URL_TT);

        var_dump($resp);
        exit;
    }


}
