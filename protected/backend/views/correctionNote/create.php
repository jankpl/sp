<?php
/* @var $this CorrectionNoteController */
/* @var $model CorrectionNote */


?>

<h1>Create Accountant Note</h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>


<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(

        'enableAjaxValidation'=>false,
//        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'htmlOptions' => [
            'enctype' => 'multipart/form-data',
        ]
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>


    <div>
        <div class="row">
            <?php echo $form->labelEx($model, 'saveToUserInvoice'); ?>
            <?php echo $form->dropDownList($model, 'saveToUserInvoice', CHtml::listData(User::model()->findAll(),'id','login'), ['prompt' => '- no -', 'data-user-save' => true, 'data-chosen-widget' => true]); ?>
            <?php echo $form->error($model,'saveToUserInvoice'); ?>
        </div>

        <div class="row"  style="display: none;" data-requires-user-save="true">
            <?php echo $form->labelEx($model, 'userInvoiceDescription'); ?>
            <?php echo $form->textField($model, 'userInvoiceDescription'); ?>
            <?php echo $form->error($model,'userInvoiceDescription'); ?>
        </div>

        <div class="row"  style="display: none;" data-requires-user-save="true">
            <?php echo $form->labelEx($model, 'file_sheet'); ?>
            <?php echo $form->fileField($model, 'file_sheet'); ?>
            <?php echo $form->error($model,'file_sheet'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'place'); ?>
            <?php echo $form->textField($model,'place'); ?>
            <?php echo $form->error($model,'place'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'date'); ?>
            <?php echo $form->textField($model,'date',  ['date-picker' => 'true',]); ?>
            <?php echo $form->error($model,'date'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->dropDownList($model,'type', CorrectionNote::getTypeList(), ['data-type' => true]); ?>
            <?php echo $form->error($model,'type'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'payment'); ?>
            <?php echo $form->dropDownList($model,'payment', CorrectionNote::getPaymentList()); ?>
            <?php echo $form->error($model,'payment'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'paymentDate'); ?>
            <?php echo $form->textField($model,'paymentDate',  ['date-picker' => 'true',]); ?>
            <?php echo $form->error($model,'paymentDate'); ?>
        </div>


    </div>

    <br/>
    <br/>
    <br/>


    <table style="width: 100%;">
        <tr>
            <td><?php echo $form->labelEx($model,'textBefore'); ?></td>
            <td><?php echo $form->labelEx($model,'amount'); ?></td>
            <td><?php echo $form->labelEx($model,'currency'); ?></td>
            <td><?php echo $form->labelEx($model,'textAfter'); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->textField($model,'textBefore', ['style' => 'width: 300px;', 'data-type-text' => true, 'data-type-text-0' => Yii::t('correction_note', 'Niniejszym obciążąmy Państwa kwotą'), 'data-type-text-1' => Yii::t('correction_note', 'Niniejszym uznajemy Państwa kwotą')]); ?></td>
            <td><?php echo $form->textField($model,'amount', ['style' => 'width: 50px;']); ?></td>
            <td><?php echo $form->dropDownList($model,'currency', Currency::listIdsWithNames(), ['style' => 'width: 50px;']); ?></td>
            <td><?php echo $form->textArea($model,'textAfter', ['style' => 'width: 500px; height: 50px;']); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->error($model,'textBefore'); ?></td>
            <td><?php echo $form->error($model,'amount'); ?></td>
            <td><?php echo $form->error($model,'currency'); ?></td>
            <td><?php echo $form->error($model,'textAfter'); ?></td>
        </tr>
    </table>


    <script>


        $(document).ready(function(){

            requiresUserSave();
            $('[data-user-save]').on('change', requiresUserSave);

            function requiresUserSave()
            {
                if(parseInt($('[data-user-save]').val()))
                    $('[data-requires-user-save]').show();
                else
                    $('[data-requires-user-save]').hide();
            }

            $('[data-type]').on('change', function(){
                setTypeTexts($(this).val());

            });

            setTypeTexts($('[data-type]').val());
        });

        function setTypeTexts(type)
        {
            $('[data-type-text]').val($('[data-type-text]').attr('data-type-text-' + type));
            $('[data-type-who]').each(function(i,v){
                $(this).html($(this).attr('data-type-who-' + type));
            });
        }
    </script>


    <br/>
    <br/>
    <br/>

    <div style="overflow: auto;">
        <div style="float: left; width: 48%;">

            <h3 data-type-who="true" data-type-who-0="Creator" data-type-who-1="Receiver"></h3>

            <div class="row">
                <?php echo $form->labelEx($model,'creator_name'); ?>
                <?php echo $form->textField($model,'creator_name', array('maxlength' => 45)); ?>
                <?php echo $form->error($model,'creator_name'); ?>
            </div><!-- row -->



            <br/>
            <br/>


            <div class="row">
                <?php echo $form->labelEx($model->receiverAddressModel,'[receiver]company'); ?>
                <?php echo $form->textField($model->receiverAddressModel,'[receiver]company', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->receiverAddressModel,'[receiver]company'); ?>
            </div><!-- row -->

            <div class="row">
                <?php echo $form->labelEx($model->receiverAddressModel,'[receiver]name'); ?>
                <?php echo $form->textField($model->receiverAddressModel,'[receiver]name', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->receiverAddressModel,'[receiver]name'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->receiverAddressModel,'[receiver]address_line_1'); ?>
                <?php echo $form->textField($model->receiverAddressModel,'[receiver]address_line_1', array('maxlength' => 90)); ?>
                <?php echo $form->error($model->receiverAddressModel,'[receiver]address_line_1'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->receiverAddressModel,'[receiver]zip_code'); ?>
                <?php echo $form->textField($model->receiverAddressModel,'[receiver]zip_code', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->receiverAddressModel,'[receiver]zip_code'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->receiverAddressModel,'[receiver]city'); ?>
                <?php echo $form->textField($model->receiverAddressModel,'[receiver]city', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->receiverAddressModel,'[receiver]city'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->receiverAddressModel,'[receiver]country_id'); ?>
                <?php echo $form->dropDownList($model->receiverAddressModel, '[receiver]country_id', CHtml::listData(CountryList::model()->with('countryListTr')->findAll(),'id','trName')); ?>
                <?php echo $form->error($model->receiverAddressModel, '[receiver]country_id'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->receiverAddressModel,'[receiver]_nip'); ?>
                <?php echo $form->textField($model->receiverAddressModel, '[receiver]_nip', array('maxlength' => 14)); ?>
                <?php echo $form->error($model->receiverAddressModel, '[receiver]_nip'); ?>
            </div><!-- row -->


        </div>
        <div style="float: right; width: 48%;">

            <h3 data-type-who="true" data-type-who-1="Creator" data-type-who-0="Receiver"></h3>

            <div class="row">
                <?php echo $form->labelEx($model,'receiver_name'); ?>
                <?php echo $form->textField($model,'receiver_name', array('maxlength' => 45)); ?>
                <?php echo $form->error($model,'receiver_name'); ?>
            </div><!-- row -->

            <br/>
            <br/>

            <div class="row">
                <?php echo $form->labelEx($model->creatorAddressModel,'[creator]company'); ?>
                <?php echo $form->textField($model->creatorAddressModel,'[creator]company', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->creatorAddressModel,'[creator]company'); ?>
            </div><!-- row -->

            <div class="row">
                <?php echo $form->labelEx($model->creatorAddressModel,'[creator]name'); ?>
                <?php echo $form->textField($model->creatorAddressModel,'[creator]name', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->creatorAddressModel,'[creator]name'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->creatorAddressModel,'[creator]address_line_1'); ?>
                <?php echo $form->textField($model->creatorAddressModel,'[creator]address_line_1', array('maxlength' => 90)); ?>
                <?php echo $form->error($model->creatorAddressModel,'[creator]address_line_1'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->creatorAddressModel,'[creator]zip_code'); ?>
                <?php echo $form->textField($model->creatorAddressModel,'[creator]zip_code', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->creatorAddressModel,'[creator]zip_code'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->creatorAddressModel,'[creator]city'); ?>
                <?php echo $form->textField($model->creatorAddressModel,'[creator]city', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->creatorAddressModel,'[creator]city'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->creatorAddressModel,'[creator]country_id'); ?>
                <?php echo $form->dropDownList($model->creatorAddressModel, '[creator]country_id', CHtml::listData(CountryList::model()->with('countryListTr')->findAll(),'id','trName')); ?>
                <?php echo $form->error($model->creatorAddressModel, '[creator]country_id'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->creatorAddressModel,'[creator]_nip'); ?>
                <?php echo $form->textField($model->creatorAddressModel, '[creator]_nip', array('maxlength' => 14)); ?>
                <?php echo $form->error($model->creatorAddressModel, '[creator]_nip'); ?>
            </div><!-- row -->


        </div>
    </div>

    <hr/>

    <?php
    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('jquery.ui');
    Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    $script = "$( function() {
            $('[date-picker]').datepicker({
            dateFormat : 'yy-mm-dd'
            });
            
            $('[data-paid-value-fill]').on('click', function(){
                 $('[data-paid-value]').val($('[data-inv-value]').val());
            });
                           
                    } );";
    Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);

    $script = "
     $('[data-chosen-widget]').chosen({allow_single_deselect:true});
        $('.chosen-drop').css('width', '300px');
        $('[data-chosen-widget]').hide();
    ";
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.jquery.min.js');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.min.css');
    Yii::app()->clientScript->registerScript('chosen-widget', $script, CClientScript::POS_READY);

    ?>

    <div class="row buttons text-center">
        <?php echo CHtml::submitButton('Generate', ['class' => 'btn-primary btn-large']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
