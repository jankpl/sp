(function() {

    $('.pricing-courier-weight').on('change', function(){
        var val = $(this).val();

        if(val < 0.1)
            val = 0.1;
        else if(val > 70)
            val = 70;

        if(val != $(this).val())
            $(this).val(val);
    });

    $('.pricing-courier-dim').on('change', function(){
        var val = $(this).val();

        if(val < 1)
            val = 1;
        else if(val > 140)
            val = 140;

        if(val != $(this).val())
            $(this).val(val);
    });

    $('#pricing-postal-country-receiver').on('change', postalPricingSizeChecker);


    function postalPricingSizeChecker() {

        var $input = $('#pricing-postal-size');

        if ($('#pricing-postal-country-receiver').val() == 1) {
            $input.find('option').each(function (i, v) {
                if (i > 1)
                    $(this).attr('disabled', true);
                else
                    $(this).attr('disabled', false);
            });
        } else {
            $input.find('option').each(function (i, v) {
                if (i > 1)
                    $(this).attr('disabled', false);
                else
                    $(this).attr('disabled', true);
            });
        }

        if ($input.find('option:selected').is(':disabled')) {
            $input.find('option:selected').attr('selected', false);
            $input.find('option').not(':disabled').first().attr('selected', true);
        };
    }

    $('.main-pricing-courier [data-pricing-input]').on('change', function(){

        if($('[data-pricing-input]').filter(function () {
                return $.trim($(this).val()).length == 0
            }).length == 0)
            getPriceCourier();
    });

    $(document).ready(function(){
        $('[data-pricing-show=courier]').addClass('active');
        getPriceCourier();
    });

    $('.main-pricing-courier .main-pricing-block-price-go').on('click', function(){
        var url = $(this).attr('data-url');
        url += '?rci=' + $('#pricing-courier-country-receiver').val() + '&sci=' + $('#pricing-courier-country-sender').val() + '&w=' + $('#pricing-courier-weight').val() + '&sl=' + $('#pricing-courier-dim-l').val() + '&sd=' + $('#pricing-courier-dim-d').val() + '&sw=' + $('#pricing-courier-dim-w').val();
        window.location = url;
    });

    $('.main-pricing-postal .main-pricing-block-price-go').on('click', function(){
        var url = $(this).attr('data-url');
        url += '?rci=' + $('#pricing-postal-country-receiver').val() + '&t=' + $('#pricing-postal-type').val() + '&wc=' + $('#pricing-postal-weight').val() + '&sc=' + $('#pricing-postal-size').val();
        window.location = url;
    });

    // $('.main-pricing-hm .main-pricing-block-price-go').on('click', function(){
    //     window.location = $(this).attr('data-url');
    // });

    $('[data-pricing-show]').on('click', function(){

        var what = $(this).attr('data-pricing-show');

        if($(this).hasClass('active'))
            return false;

        $('[data-pricing-show]').removeClass('active');
        $(this).addClass('active');

        $('.main-pricing-type').fadeOut();
        $('[data-pricing="' + what + '"]').fadeIn();

        switch(what)
        {
            case 'courier':
                getPriceCourier();
                break;
            case 'postal':
                postalPricingSizeChecker();
                getPricePostal();
                break;
            // case 'hm':
            //     getPriceHm();
            //     break;
        }

    });

    function getPriceCourier()
    {
        var $parent = $('.main-pricing-courier');

        beforePrice($parent);

        var sender_country_id = $('.main-pricing-courier #pricing-courier-country-sender').val();
        var receiver_country_id = $('.main-pricing-courier #pricing-courier-country-receiver').val();

        var weight = $('#pricing-courier-weight').val();
        var size_l = $('#pricing-courier-dim-l').val();
        var size_w = $('#pricing-courier-dim-w').val();
        var size_d = $('#pricing-courier-dim-d').val();

        var force_uid = $parent.attr('data-force-uid');

        var dim_w = DimensionalWeight.calculate(size_l, size_w, size_d);

        var force_currency = $parent.attr('data-force-currency');

        if(dim_w > 70 || (sender_country_id != 1 && yii.pv != 1))
        {
            displayNoPrice($parent);
        } else {

            $.ajax({
                url: yii.urls.getPriceCourier,
                data: { sender_country_id: sender_country_id, receiver_country_id: receiver_country_id, with_pickup: 0, weight: weight, size_l : size_l, size_w : size_w, size_d : size_d, packages_number : 1, currency: force_currency, noTax : true, force_user_id : force_uid },
                method: 'POST',
                dataType: 'json',
                cache: true,
                success: function(result){

                    result = result.total;
                    result = result.split(' ');
                    var amount = result[0];

                    amount = parseFloat(amount);

                    if(!amount)
                    {
                        displayNoPrice($parent);
                    } else {

                        var currency = result[1];

                        displayPrice(amount, currency, $parent);
                    }
                },
                error: function(e){
                }
            });
        }
    }

    function getPricePostal()
    {

        var $parent = $('.main-pricing-postal');

        beforePrice($parent);

        var receiver_country_id = $('#pricing-postal-country-receiver').val();

        var weight = $('#pricing-postal-weight').val();
        var size = $('#pricing-postal-size').val();
        var type = $('#pricing-postal-type').val();

        var force_currency = $parent.attr('data-force-currency');
        var force_uid = $parent.attr('data-force-uid');

        $.ajax({
            url: yii.urls.getPricePostal,
            data: { receiver_country_id: receiver_country_id, weight_class: weight, size_class : size, postal_type: type, noTax : true, force_user_id : force_uid, currency: force_currency },
            method: 'POST',
            dataType: 'json',
            success: function(result){

                if(!result.available)
                {
                    displayNoPrice($parent);
                } else {

                    result = result.price;
                    result = result.split(' ');
                    var amount = result[0];

                    amount = parseFloat(amount);

                    var currency = result[1];

                    displayPrice(amount, currency, $parent);
                }
            },
            error: function(e){
            }
        });

    }

    $('.main-pricing-postal [data-pricing-input]').on('change', function(){

        if($('[data-pricing-input]').filter(function () {
                return $.trim($(this).val()).length == 0
            }).length == 0)
            getPricePostal();
    });



    // function getPriceHm()
    // {
    //
    //     var $parent = $('.main-pricing-hm');
    //
    //     beforePrice($parent);
    //
    //     var receiver_country_id = $('#pricing-hm-country-receiver').val();
    //
    //     var pages = $('#pricing-hm-pages').val();
    //
    //     $.ajax({
    //         url: yii.urls.getPriceHm,
    //         data: { receiver_country_id: receiver_country_id, pages: pages, noTax : true },
    //         method: 'POST',
    //         dataType: 'json',
    //         success: function(result){
    //
    //
    //
    //             if(!result.available)
    //             {
    //                 displayNoPrice($parent);
    //             } else {
    //
    //                 result = result.price;
    //                 result = result.split(' ');
    //                 var amount = result[0];
    //
    //                 amount = parseFloat(amount);
    //
    //                 var currency = result[1];
    //
    //                 displayPrice(amount, currency, $parent);
    //             }
    //         },
    //         error: function(e){
    //         }
    //     });
    //
    // }
    //
    // $('.main-pricing-hm [data-pricing-input]').on('change', function(){
    //
    //     if($('[data-pricing-input]').filter(function () {
    //             return $.trim($(this).val()).length == 0
    //         }).length == 0)
    //         getPriceHm();
    // });

    function displayPrice(amount, currency, $parent)
    {
        amount_brutto = amount * 1.23;

        amount = amount.toFixed(2);
        amount = amount.toString();
        amount = amount.replace('.', ',');

        amount_brutto = amount_brutto.toFixed(2);
        amount_brutto = amount_brutto.toString();
        amount_brutto = amount_brutto.replace('.', ',');

        $parent.find('.main-pricing-block-price-amount-loading').hide();
        $parent.find('.main-pricing-block-price-amount').html(amount);
        $parent.find('.main-pricing-block-price-amount-brutto span').html(amount_brutto);
        $parent.find('.pricing-currency-label span').html(currency);
    }

    function displayNoPrice($parent)
    {
        $parent.find('.main-pricing-block-price').hide();
        $parent.find('.main-pricing-block-contact').css('display', 'table');
    }

    function beforePrice($parent)
    {
        $parent.find('.main-pricing-block-contact').hide();
        $parent.find('.main-pricing-block-price-amount-loading').show();
        $parent.find('.main-pricing-block-price-amount').html('');
        $parent.find('.main-pricing-block-price').show();
    }
})();
