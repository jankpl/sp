<?php

class ApaczkaClient extends GlobalOperator
{
    const S_UPS_STDANDARD = 1;
    const S_UPS_EXPRESS_SAVER = 2;
    const S_UPS_EXPRESS = 3;
    const S_UPS_EXPRESS_PLUS = 4;
    const S_UPS_EXPREDITED = 8;
    const S_UPS_TODAY_STANDARD = 9;
    const S_UPS_TODAY_EXPRESS = 10;
    const S_UPS_TODAY_EXPRESS_SAVER = 11;
    const S_UPS_TREANSBORDER_STANDARD = 12;
    const S_DPD_CLASSIC = 21;
    const S_DPD_CLASSIC_FOREIGN = 22;
    const S_INPOST_KURIER = 42;
    const S_DHL_STANDARD = 82;
    const S_DHL_STANDARD_EXPRESS_12 = 83;
    const S_DHL_STANDARD_EXPRESS_9 = 84;
    const S_DHL_STANDARD_EXPRESS_18_22 = 85;
    const S_KEX_GEIS = 150;
    const S_FEDEX = 151;
    const S_FEDEX_PRZESYLKA = 152;
    const S_POCZTA_KURIER_48 = 160;
    const S_POCZTA_POCZTEX_24 = 161;
    const S_TNT_GLOBAL_EXPRESS = 170;
    const S_TNT_ECONOMY_EXPRESS = 171;
    const S_APACZKA_NIEMCY_LINEHAUL = 190;
    const S_GLS = 200;

    public $label_annotation_text = false;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $packages_number = 1;

    public $package_weight;
    public $package_size_l;
    public $package_size_d;
    public $package_size_w;

    public $package_content;
    public $package_value;

    public $ref;

    public $user_id;

    public $service_id;

    public $cod_value = 0;

    public $rod = false;
    public $sms_notification = false;
    public $saturday_delivery = false;
    public $fragile = false;

    public $withPickup = false;

    public $courier_id;

    protected static function globalOperatorToService($gbid)
    {
        switch($gbid)
        {
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_STDANDARD:
                $service_id = self::S_UPS_STDANDARD;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_EXPRESS_SAVER:
                $service_id = self::S_UPS_EXPRESS_SAVER;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_EXPRESS:
                $service_id = self::S_UPS_EXPRESS;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_EXPRESS_PLUS:
                $service_id = self::S_UPS_EXPRESS_PLUS;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_EXPREDITED:
                $service_id = self::S_UPS_EXPREDITED;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_TODAY_STANDARD:
                $service_id = self::S_UPS_TODAY_STANDARD;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_TODAY_EXPRESS:
                $service_id = self::S_UPS_TODAY_EXPRESS;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_TODAY_EXPRESS_SAVER:
                $service_id = self::S_UPS_TODAY_EXPRESS_SAVER;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_UPS_TREANSBORDER_STANDARD:
                $service_id = self::S_UPS_TREANSBORDER_STANDARD;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_DPD_CLASSIC:
                $service_id = self::S_DPD_CLASSIC;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_DPD_CLASSIC_FOREIGN:
                $service_id = self::S_DPD_CLASSIC_FOREIGN;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_INPOST_KURIER:
                $service_id = self::S_INPOST_KURIER;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_DHL_STANDARD:
                $service_id = self::S_DHL_STANDARD;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_DHL_STANDARD_EXPRESS_12:
                $service_id = self::S_DHL_STANDARD_EXPRESS_12;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_DHL_STANDARD_EXPRESS_9:
                $service_id = self::S_DHL_STANDARD_EXPRESS_9;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_DHL_STANDARD_EXPRESS_18_22:
                $service_id = self::S_DHL_STANDARD_EXPRESS_18_22;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_KEX_GEIS:
                $service_id = self::S_KEX_GEIS;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_FEDEX:
                $service_id = self::S_FEDEX;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_FEDEX_PRZESYLKA:
                $service_id = self::S_FEDEX_PRZESYLKA;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_POCZTA_KURIER_48:
                $service_id = self::S_POCZTA_KURIER_48;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_POCZTA_POCZTEX_24:
                $service_id = self::S_POCZTA_POCZTEX_24;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_TNT_GLOBAL_EXPRESS:
                $service_id = self::S_TNT_GLOBAL_EXPRESS;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_TNT_ECONOMY_EXPRESS:
                $service_id = self::S_TNT_ECONOMY_EXPRESS;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_APACZKA_NIEMCY_LINEHAUL:
                $service_id = self::S_APACZKA_NIEMCY_LINEHAUL;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_APACZKA_GLS:
                $service_id = self::S_GLS;
                break;
            default:
                throw new Exception('Unknown service');
                break;
        }

        return $service_id;
    }

    public static function getAdditions(Courier $courier)
    {
        $CACHE_NAME = 'APACZKA_ADDITIONS_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_ROD;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_SMS_NOTIFICATION;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_SATURDAY_DELIVERY;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_FRAGILE;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _addressLinesConverter($addressLine1, $addressLine2, $getLineNo)
    {
        $l1 = mb_strlen($addressLine1);
        $l2 = mb_strlen($addressLine2);

        if($l1 + $l2 < 50) {
            if($getLineNo == 0)
                return trim($addressLine1.' '.$addressLine2);
            else if($getLineNo == 1)
                return '';
        } else {

            if($getLineNo == 0)
                return $addressLine1;
            else if($getLineNo == 1)
                return $addressLine2;
        }
    }


//    public static function test($returnError = true)
//    {
//
//        $order = '';
//
//        require_once(Yii::getPathOfAlias('application.components.apaczka').'/api.php');
////        $resp = Apaczka\Api::service_structure();
////
////        var_dump(json_decode($resp));
//
////        $resp = Apaczka\Api::points('UPS');
////
////        var_dump(json_decode($resp));
////exit;
//$a1 = 'grodkowska';
//$a2 = '40';
//
//        $order =
//            [
//                'service_id'     => 82, // endpoint: service_structure
//                'address'        => [
//                    'sender'   => [
//                        'country_code'       => 'PL',
//                        'name'               => 'Test',
//                        'line1'              => self::_addressLinesConverter($a1, $a2, 0),
//                        'line2'              => self::_addressLinesConverter($a1, $a2, 1),
//                        'postal_code'        => '48-300',
//                        'city'               => 'Nysa',
//                        'is_residential'     => 0, // 0 / 1
//                        'contact_person'     => 'Test',
//                        'email'              => 'jankopec@swiatprzesylek.pl',
//                        'phone'              => '123123123',
//                        'foreign_address_id' => '' // enpoint: points
//                    ],
//                    'receiver' => [
//                        'country_code'       => 'PL',
//                        'name'               => 'Test 2',
//                        'line1'              => 'Łokietka 22',
//                        'line2'              => '',
//                        'postal_code'        => '50-304',
//                        'city'               => 'Wrocław',
//                        'is_residential'     => 0, // 0 / 1
//                        'contact_person'     => 'Test',
//                        'email'              => 'jankopec@swiatprzesylek.pl',
//                        'phone'              => '123123123',
//                        'foreign_address_id' => '' // enpoint: points
//                    ]
//                ],
////            'option'         => [
////                '31' => 0, // powiadomienie sms,
////                '11' => 0, // rod
////                '19' => 0, // dostawa w sobotę,
////                '25' => 0, // dostawa w godzinach,
////                '58' => 0, // ostrożnie
////            ],
//            'shipment_value' => 1000, // wartość w groszach
//            'cod'            => [
//                'amount'      => 0, // wartość w groszach
//                'bankaccount' => '76109021380000000137106859'
//            ],
////            'pickup'         => [
////                'type'       => 'COURIER', // endpoint: service_structure
////                'date'       => '2019-04-18', // Y-m-d
////                'hours_from' => '08:00', // H:i - pickup_hours
////                'hours_to'   => '17:00' // H:i - pickup_hours
////            ],
//            'shipment'       => [
//                [
//                    'dimension1'         => 10, // cm
//                    'dimension2'         => 20, // cm
//                    'dimension3'         => 30, // cm
//                    'weight'             => 1, // kg
//                    'is_nstd'            => 0, // 0 / 1
//                    'shipment_type_code' => 'PACZKA' // endpoint: service_structure
//                ]
//            ],
//            'comment'        => 'Opis',
//            'content'        => 'Zawartosc'
//        ];
//
//
//        $resp = Apaczka\Api::order_send($order);
//        $resp = json_decode($resp);
//        var_dump($resp);
//
//        $id = $resp->response->order->id;
//
//        $label = Apaczka\Api::waybill($id);
//        $label = json_decode($label);
//        var_dump($label);
//        var_dump($label->response->waybill);
//
//
//        exit;
//
//        $im = ImagickMine::newInstance();
//        $im->setResolution(300, 300);
//        $im->readImageBlob(base64_decode($label->response->waybill));
//        $im->setImageFormat('png8');
//        if ($im->getImageAlphaChannel()) {
//            $im->setImageBackgroundColor('white');
//            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//        }
//        $im->stripImage();
//        $im->trimImage(0);
//
//
//        $labels = [];
//        if(1)
//        {
//            $imgWidth = $im->getImageWidth();
//            $imgHeight = $im->getImageHeight();
//
//            $im2 = clone $im;
//            $im2->cropImage($imgWidth + 150, $imgHeight / 3 + 70, 0, 0);
//            $im2->rotateImage(new ImagickPixel(), 90);
//            $im2->stripImage();
//
//
//            $im3 = clone $im;
//            $im3->cropImage($imgWidth + 150, $imgHeight / 3  - 40, 0, 1 * $imgHeight / 3 + 140);
//            $im3->rotateImage(new ImagickPixel(), 90);
//            $im3->stripImage();
//
//            $im->cropImage($imgWidth + 150, $imgHeight / 3 + 60, 0, 2 * $imgHeight / 3 + 160);
//
//            $labels[] = $im3->getImageBlob();
//            $labels[] = $im2->getImageBlob();
//        }
//
//        if ($im->getImageWidth() > $im->getImageHeight())
//            $im->rotateImage(new ImagickPixel(), 90);
//
//        $im->stripImage();
//
//        $labels[] = $im->getImageBlob();
//
//
//        file_put_contents('lab.png', $im->getImageBlob());
//        file_put_contents('lab2.png', $im2->getImageBlob());
//        file_put_contents('lab3.png', $im3->getImageBlob());
//        exit;
//
//
//        if($resp == '' OR ((string) $resp->status['code']) != 'OK' OR ((string) $resp->response->niceError) != '')
//        {
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse((string) $resp->response->niceError != '' ? (string) $resp->response->niceError : 'Operator internal error.');
//            } else {
//                return false;
//            }
//        }
//
//        foreach($resp->response->job->parcels->barcode AS $item)
//            $ttNumber = (string) $item;
//
//        $label = (string) $resp->response->job->labelData->url;
//        $label = @file_get_contents($label);
//
//        $im = ImagickMine::newInstance();
//        $im->setResolution(300, 300);
//        $im->readImageBlob($label);
//        $im->setImageFormat('png8');
//        if ($im->getImageAlphaChannel()) {
//            $im->setImageBackgroundColor('white');
//            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//        }
//
//
//        if(1)
//        {
//            $imgWidth = $im->getImageWidth();
//            $imgHeight = $im->getImageHeight();
//
//            $im2 = clone $im;
//            $im2->cropImage($imgWidth, $imgHeight / 3, 0, 0);
//
//
//            $im3 = clone $im;
//            $im3->cropImage($imgWidth, $imgHeight / 3, 0, 1 * $imgHeight / 3);
//
//            $im->cropImage($imgWidth, $imgHeight / 3, 0, 2 * $imgHeight / 3);
//        }
//
//        $im->stripImage();
//
////            $im->trimImage(0);
//
//        file_put_contents('lab.png', $im->getImageBlob());
//        file_put_contents('lab2.png', $im2->getImageBlob());
//        file_put_contents('lab3.png', $im3->getImageBlob());
//
//        $return = [];
//        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);
//
//        return $return;
//    }



    protected function createShipment($returnError, $shiftDays = 0)
    {
        require_once(Yii::getPathOfAlias('application.components.apaczka').'/api.php');

//        if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse('Not active!');
//            } else {
//                return false;
//            }

        $option = [];

        if($this->rod)
            $option[11] = 1;

        if($this->sms_notification)
            $option[31] = 1;

        if($this->saturday_delivery)
            $option[19] = 1;

        if($this->fragile)
            $option[58] = 1;

        $shipment = [];

        for($i = 1; $i <= $this->packages_number; $i++)
            $shipment[] =
                [
                    'dimension1'         => $this->package_size_l, // cm
                    'dimension2'         => $this->package_size_d, // cm
                    'dimension3'         => $this->package_size_w, // cm
                    'weight'             => $this->package_weight < 1 ? 1 : $this->package_weight, // kg
                    'is_nstd'            => 0, // 0 / 1
                    'shipment_type_code' => 'PACZKA' // endpoint: service_structure
                ];

        $order =
            [
                'service_id'     => $this->service_id, // endpoint: service_structure
                'address'        => [
                    'sender'   => [
                        'country_code'       => $this->sender_country,
                        'name'               => $this->sender_company == '' ? $this->sender_name : $this->sender_company,
                        'line1'              => self::_addressLinesConverter($this->sender_address1, $this->sender_address2, 0),
                        'line2'              => self::_addressLinesConverter($this->sender_address1, $this->sender_address2, 1),
                        'postal_code'        => $this->sender_zip_code,
                        'city'               => $this->sender_city,
                        'is_residential'     => $this->sender_company == '' ? 1 : 0,
                        'contact_person'     => $this->sender_name == '' ? $this->sender_company : $this->sender_name,
                        'email'              => $this->sender_mail,
                        'phone'              => $this->sender_tel,
                        'foreign_address_id' => '' // enpoint: points
                    ],
                    'receiver' => [
                        'country_code'       => $this->receiver_country,
                        'name'               => $this->receiver_company == '' ? $this->receiver_name : $this->receiver_company,
                        'line1'              => self::_addressLinesConverter($this->receiver_address1, $this->receiver_address2, 0),
                        'line2'              => self::_addressLinesConverter($this->receiver_address1, $this->receiver_address2, 1),
                        'postal_code'        => $this->receiver_zip_code,
                        'city'               => $this->receiver_city,
                        'is_residential'     => $this->receiver_company == '' ? 1 : 0,
                        'contact_person'     => $this->receiver_name == '' ? $this->receiver_company : $this->receiver_name,
                        'email'              => $this->receiver_mail,
                        'phone'              => $this->receiver_tel,
                        'foreign_address_id' => '' // enpoint: points
                    ]
                ],
                'option'         => $option,
                'shipment_value' => $this->cod_value > $this->package_value ? ($this->cod_value * 100) : ($this->package_value * 100) , // wartość w groszach
                'cod'            => [
                    'amount'      => $this->cod_value * 100, // wartość w groszach
                    'bankaccount' => '76109021380000000137106859'
                ],
                'shipment'       => $shipment,
                'comment'        => $this->ref,
                'content'        => $this->package_content
            ];

        if($this->withPickup)
        {
            $date = date('Y-m-d');

            if($shiftDays)
                $date = S_Useful::workDaysNextDate($date, $shiftDays);

            $order['pickup'] = [
                'type'       => 'COURIER',
                'date'       => $date, // Y-m-d
                'hours_from' => '08:00', // H:i - pickup_hours
                'hours_to'   => '17:00' // H:i - pickup_hours
            ];
        }

        MyDump::dump('apaczka.txt', 'REQ (order_send): '.print_r($order,1));

        $resp = Apaczka\Api::order_send($order);
        $resp = @json_decode($resp);

        $message = $resp->message;
        $message = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $message);


        if(preg_match('/Nie ma możliwości zamówienia kuriera na datę/iu', $message) && $shiftDays <= 4)
            return $this->createShipment($returnError, ++$shiftDays);

        MyDump::dump('apaczka.txt', 'RESP: '.print_r($resp,1));

        if(!$resp OR $resp->status != 200)
        {
            if ($returnError == true) {

                if(!$resp OR $resp->message == '')
                    $error = 'Unknown error!';
                else
                    $error = $resp->message;

                return GlobalOperatorResponse::createErrorResponse($error);
            } else {
                return false;
            }
        }

        $id = $resp->response->order->id;
        $tracking_id = $resp->response->order->waybill_number;

        MyDump::dump('apaczka.txt', 'REQ (waybill): '.print_r($id,1));
        $label = Apaczka\Api::waybill($id);

        MyDump::dump('apaczka.txt', 'RESP: '.preg_replace('%waybill":".*"%si', '(...TRUNCATED LABEL BASE64...)',  print_r($label,1)));

        $label = @json_decode($label);
        if(!$label OR $label->status != 200)
        {
            if ($returnError == true) {

                if(!$label OR $label->message == '')
                    $error = 'Unknown error!';
                else
                    $error = $label->message;

                return GlobalOperatorResponse::createErrorResponse($error);
            } else {
                return false;
            }
        }

        $label = (string) $label->response->waybill;
        $label = base64_decode($label);

        $return = [];

        $basePath = Yii::app()->basePath . '/../';
        $dir = 'uplabels/';
        $temp = $dir . 'temp/';

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        @file_put_contents($basePath . $temp . $id . '.pdf', $label);
        @file_put_contents('ap.pdf', $label);
        for ($i = 0; $i < $this->packages_number; $i++) {
            $im->readImage($basePath . $temp . $id . '.pdf['.$i.']');
            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            if ($im->getImageWidth() > $im->getImageHeight())
                $im->rotateImage(new ImagickPixel(), 90);

            $im->trimImage(0);
            $im->stripImage();

            $labels = [ $im->getImageBlob() ];

            file_put_contents('ap'.$i.'.png', $im->getImageBlob());
            $return[] = GlobalOperatorResponse::createSuccessResponse($labels, $tracking_id, false, false, false, false, $id);
        }
        @unlink($basePath . $temp . $id . '.pdf');

        if($this->withPickup)
        {
            Yii::app()->getModule('courier');
            CourierCollect::createReadyCollection('0', $this->courier_id, CourierCollect::OPERATOR_APACZKA);
        }

        return $return;

    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;
        $model->service_id = self::globalOperatorToService($uniq_id);

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_ROD))
            $model->rod = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_SMS_NOTIFICATION))
            $model->sms_notification = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_SATURDAY_DELIVERY))
            $model->saturday_delivery = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_FRAGILE))
            $model->fragile = true;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->fetchEmailAddress(true);
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->fetchEmailAddress(true);
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->package_value = $courierInternal->courier->getPackageValueConverted('PLN');
        $model->ref = $courierInternal->courier->local_id;

        $model->packages_number = $courierInternal->courier->packages_number;

        $model->cod_value = $courierInternal->courier->cod_value;

        $model->user_id = $courierInternal->courier->user_id;

        $model->courier_id = $courierInternal->courier->id;

        $withCollect = true;
        // verify collection
        if(!$blockPickup)
        {
            // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
            if(!$blockPickup && $courierInternal->parent_courier_id != NULL)
                $withCollect = false;

            // For Type Internal do not call if without pickup option
            // PICKUP_MOD / 14.09.2016
//            if($withCollect && $courier != NULL && !$courier->with_pickup)
            if(!$blockPickup && $courierInternal != NULL && $courierInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR)
                $withCollect = false;

            // DO NOT CALL UPS COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
            if(!$blockPickup && $courierInternal->courier->user != NULL && $courierInternal->courier->user->getUpsCollectBlock())
                $withCollect = false;
        } else
            $withCollect = false;

        $model->withPickup = $withCollect;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;

        $model->service_id = self::globalOperatorToService($courierU->courierUOperator->uniq_id);

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_ROD))
            $model->rod = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_SMS_NOTIFICATION))
            $model->sms_notification = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_SATURDAY_DELIVERY))
            $model->saturday_delivery = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_APACZKA_FRAGILE))
            $model->fragile = true;


        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->fetchEmailAddress(true);
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->fetchEmailAddress(true);
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierU->courier->getWeight(true);
        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->package_value = $courierU->courier->getPackageValueConverted('PLN');
        $model->ref = $courierU->courier->local_id;


        $model->packages_number = $courierU->courier->packages_number;

        $model->cod_value = $courierU->courier->cod_value;

        $model->user_id = $courierU->courier->user_id;

        $model->courier_id = $courierU->courier->id;

        if($courierU->collection)
            $model->withPickup = true;

        return $model->createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        require_once(Yii::getPathOfAlias('application.components.apaczka').'/api.php');

        $ttId = $courierLabelNew->_custom_tracking_id;

        $resp = Apaczka\Api::order($ttId);
        $resp = json_decode($resp);

        $statuses = new GlobalOperatorTtResponseCollection();
        if($resp)
        {
            $status = $resp->response->order->status;
            $statuses->addItem($status, NULL, NULL);
        }

        return $statuses;

    }
}