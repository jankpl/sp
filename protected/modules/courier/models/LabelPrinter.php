<?php

class LabelPrinter
{
    const PDF_4_ON_1 = 1;
    const PDF_2_ON_1 = 2;
    const PDF_ROLL = 3;
    const PDF_2_ON_1_ONE_RIGHT = 4;
    const PDF_ROLL_WITH_CN_22 = 5;
    const PDF_SINGLE_PNG = 6;
    const XLS_WITH_URL = 7;
    const URL = 8;

    const PDF_ROLL_WITH_PACKAGE_SLIP = 13;

    public static function getLabelGenerateTypes($omitSingleItemType = true)
    {
        $data = [];
        if(!Yii::app()->user->isGuest && in_array(Yii::app()->user->id, [8, 2537]))
            $data[self::PDF_ROLL_WITH_PACKAGE_SLIP] = Yii::t('courier', 'Roll with packaging slip');

        $data += [
            self::PDF_ROLL => Yii::t('courier','roll'),
            self::PDF_4_ON_1 => Yii::t('courier','4 on 1'),
            self::PDF_2_ON_1 => Yii::t('courier','2 on 1'),
            self::PDF_2_ON_1_ONE_RIGHT => Yii::t('courier','2 on 1 - just right'),
            self::PDF_ROLL_WITH_CN_22 => Yii::t('courier','roll with CN22'),
            self::XLS_WITH_URL => Yii::t('courier','xls with urls'),
            self::URL => Yii::t('courier','URL2Roll'),
        ];

        if(!$omitSingleItemType)
            $data[self::PDF_SINGLE_PNG] = Yii::t('courier','.PNG');


        return $data;
    }


    private function __construct() {}

    /**
     * @param $couriers
     * @param int $type
     * @param bool|false $noError
     * @param bool|false $internalGetSecond Used only for Type Internal Packages. Allows to choice wheter to generate first (default) or second label
     * @throws CHttpException
     */
    static public function generateLabels($couriers, $type = self::PDF_4_ON_1, $noError = false, $internalGetSecond = false, $returnAsPdfString = false)
    {
        /* @var $item Courier */

        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest && Yii::app()->user->getModel()->getMultipackagesAsOneItem()) {
            $listOfMultipacks = [];
            foreach ($couriers AS $key => $item) {
                if ($item->packages_number > 1) {
                    if ($item->getFamilyMemberNumber() > 1)
                        $listOfMultipacks[$item->getFamilyParentCourierId()] = true;
                    else
                        $listOfMultipacks[$item->id] = true;

                    unset($couriers[$key]); // remove all multipacks labels - will be added later
                }
            }

            foreach ($listOfMultipacks AS $parent_id => $dummy) {
                $parent = Courier::model()->findByPk($parent_id);
                $children = $parent->findChildren();

                $couriers[] = $parent;
                if (is_array($children))
                    $couriers = array_merge($couriers, $children);
            }
        }



        if($type == self::PDF_SINGLE_PNG)
        {
            if( S_Useful::sizeof($couriers) > 1)
                throw new CHttpException(405, 'Only one item for PNG label type allowed!');
        }


        $done = [];

        if(!is_array($couriers))
            $couriers = [$couriers];

        /* @var $item Courier */

        // filter nonvalid labels
        {
            foreach($couriers AS $key => &$item)
            {
                if(
                    in_array($item->courier_stat_id, Courier::_listOfStatsPreventingGeneratingLabels())
                    OR
                    in_array($item->user_cancel, [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED, Courier::USER_CANCEL_CONFIRMED])
                )
                    unset($couriers[$key]);
            }
        }

        // queque for oversized labels;
        $labelQueque = [];

        try
        {
            /* @var $pdf TCPDF */
            $pdf = PDFGenerator::initialize();

            $error = false;

            switch ($type) {
                case Courier::PDF_ROLL:
                    $pdf = self::_pdf_roll($pdf, $couriers, $noError, $labelQueque, $internalGetSecond, $done);
                    break;
                case Courier::PDF_2_ON_1:
                    $pdf = self::_pdf_2_on_1($pdf, $couriers, $noError, $labelQueque, $internalGetSecond, $done);
                    break;
                case Courier::PDF_4_ON_1:
                default:
                    $pdf = self::_pdf_4_on_1($pdf, $couriers, $noError, $labelQueque, $internalGetSecond, $done);
                    break;
                case Courier::PDF_2_ON_1_ONE_RIGHT:
                    $pdf = self::_pdf_2_on_1_one_right($pdf, $couriers, $noError, $labelQueque, $internalGetSecond, $done);
                    break;
                case Courier::PDF_ROLL_WITH_CN_22:
                    $pdf = self::_pdf_roll_with_cn_22($pdf, $couriers, $noError, $labelQueque, $internalGetSecond, $done);
                    break;
                case self::PDF_SINGLE_PNG:
                    $courier = array_pop($couriers);
                    $png = self::_png_single_file($pdf, $courier, $noError, $labelQueque, $internalGetSecond, $done);
                    header('Content-Disposition: Attachment;filename='.$courier->local_id.'.png');
                    header('Content-Type: image/png');
                    echo $png;
                    Yii::app()->end();
                    break;
                case self::XLS_WITH_URL:
                    $data = [];
                    $data[] = ['package', 'url'];
                    unset($item);
                    foreach($couriers AS $item) {

                        $url = Yii::app()->createAbsoluteUrl('/site/getLabel', ['hash' => $item->hash]);
                        if(Yii::app()->params['backend'])
                            $url = str_replace('nysa.php/site/getLabel?hash=', 'get-label/', $url);

                        $data[] = [$item->local_id, $url];
                    }

                    S_XmlGenerator::generateXml($data, 'labels_'.date('Y-m-d H:i'));
                    Yii::app()->end();
                    break;
                case self::URL:
                    $lu = LabelsUrl::genrateNewLink(LabelsUrl::TYPE_COURIER, CHtml::listData($couriers,'id', 'id'), self::PDF_ROLL);
                    Yii::app()->controller->redirect($lu->getShowUrl());
                    Yii::app()->end();
                    break;
                case self::PDF_ROLL_WITH_PACKAGE_SLIP:
                    $pdf = self::_pdf_roll($pdf, $couriers, $noError, $labelQueque, $internalGetSecond, $done, true);
                    break;
            }

            // print oversized labels or additional pages at the end
            if(S_Useful::sizeof($labelQueque))
            {
                foreach($labelQueque AS $img)
                {
                    $pageH = 297;
                    $pageW = 210;

                    // ADD WHOLE PAGE
                    $pdf->AddPage('H','A4');

                    // PRINT IMAGE TO PDF
                    $pdf->Image(
                        '@' . $img, // file
                        0, // x
                        0, // y
                        $pageW, // w
                        $pageH, // h
                        '', // type
                        '', // link
                        '', // align
                        false, // resize
                        300, // dpi
                        '', // palign
                        false, // ismask
                        false, // imgmask
                        0, // border
                        'CM', // fitbox
                        false, // hidden
                        true, // fitonpage
                        false, // alt
                        array() // altimgs
                    );

                }
            }

            $addCustomsDocumentsReminder = false;

            /* @var $item Courier */
            foreach($couriers AS $item)
            {
                if($item->user->source_domain != PageVersion::PAGEVERSION_CQL) {

                    if ($item->receiverAddressData && !in_array($item->receiverAddressData->country_id, CountryList::getUeList(true))) {
                        $addCustomsDocumentsReminder = true;
                        break;
                    }

                }
            }

            if($addCustomsDocumentsReminder)
                $pdf = GenerateCustomsPacket::generateReminderPdfPage($pdf);

            if($error)
                throw new CHttpException(ErrorCodes::LABEL_PROBLEM, Yii::t('courier', 'Wystąpił problem z Twoją etykietą. Skontaktuj się z nami w tej sprawie!'));
            else {

                if($returnAsPdfString)
                    return $pdf->Output('labels.pdf', 'S');
                else {
                    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                        $pdf->Output('labels.pdf', 'I');
                    else
                        $pdf->Output('labels.pdf', 'D');
                }

                return $done;
            }

        }
        catch(CHttpException $ex) {
            throw new CHttpException($ex->statusCode, $ex->getMessage());
        }
    }

    static public function generateLabelsForSingleInternal(CourierTypeInternal $courierTypeInternal, $noError = false, $mode = CourierTypeInternal::GET_LABEL_MODE_FIRST)
    {

        // queque for oversized labels;
        $labelQueque = [];

        try
        {

            $firstOversized = false;
            $secondOversized = false;

            /* @var $pdf TCPDF */
            $pdf = PDFGenerator::initialize();

            $error = false;

            if($mode == CourierTypeInternal::GET_LABEL_MODE_BOTH) {
                $firstImg = self::_getImageForCourierTypeInternal_First($courierTypeInternal);
                $secondImg = self::_getImageForCourierTypeInternal_Last($courierTypeInternal);

                $firstOversized = $firstImg['oversized'];
                $firstImg = $firstImg['img'];

                $secondOversized = $secondImg['oversized'];
                $secondImg = $secondImg['img'];

                // if first label == second label
                if($firstImg == $secondImg)
                    $secondImg = false;
            }
            else if($mode == CourierTypeInternal::GET_LABEL_MODE_LAST)
            {
                $firstImg = self::_getImageForCourierTypeInternal_Last($courierTypeInternal);
                $firstOversized = $firstImg['oversized'];
                $firstImg = $firstImg['img'];
                $secondImg = false;
            }
            else
            {
                $firstImg = self::_getImageForCourierTypeInternal_First($courierTypeInternal);
                $firstOversized = $firstImg['oversized'];
                $firstImg = $firstImg['img'];
                $secondImg = false;
            }



            // if labels contains of more than one page, add them to the end
            if(is_array($firstImg)) {
                $temp = $firstImg;
                $firstImg = array_shift($temp); // get first image of array
                $labelQueque = array_merge($labelQueque, $temp);
            }

            if(is_array($secondImg)) {
                $temp = $secondImg;
                $secondImg = array_shift($temp); // get first image of array
                $labelQueque = array_merge($labelQueque, $temp);
            }



            //////////

            $pageW = 297;
            $pageH = 210;

            $xBase = 5;
            $yBase = 5;

            $blockWidth = ($pageW / 2) - (2 * $xBase);
            $blockHeight = $pageH - (2 * $yBase);

            $rightBlockXPosition = $blockWidth + 3 * $xBase;

            $pdf->AddPage('L','A4');

            $x = $xBase;

            $temp = [];
            $pdf = self::_labelBody(0, $temp, $pdf, $x, $yBase, $blockWidth, $blockHeight, $courierTypeInternal->courier, 1, $noError, $firstImg, $firstOversized, $labelQueque);
            if($secondImg) {
                $x = $rightBlockXPosition;
                $pdf = self::_labelBody(0, $temp, $pdf, $x, $yBase, $blockWidth, $blockHeight, $courierTypeInternal->courier, 1, $noError, $secondImg, $secondOversized, $labelQueque);
            }


            // print oversized or additional pagages labels at the end
            if(S_Useful::sizeof($labelQueque))
            {
                foreach($labelQueque AS $img)
                {
                    $pageH = 297;
                    $pageW = 210;

                    // ADD WHOLE PAGE
                    $pdf->AddPage('H','A4');

                    // PRINT IMAGE TO PDF
                    $pdf->Image(
                        '@' . $img, // file
                        0, // x
                        0, // y
                        $pageW, // w
                        $pageH, // h
                        '', // type
                        '', // link
                        '', // align
                        false, // resize
                        300, // dpi
                        '', // palign
                        false, // ismask
                        false, // imgmask
                        0, // border
                        'CM', // fitbox
                        false, // hidden
                        true, // fitonpage
                        false, // alt
                        array() // altimgs
                    );

                }
            }


            if($error)
                throw new CHttpException(ErrorCodes::LABEL_PROBLEM, Yii::t('courier', 'Wystąpił problem z Twoją etykietą. Skontaktuj się z nami w tej sprawie!'));
            else
            {
                if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                    $pdf->Output('labels.pdf', 'I');
                else
                    $pdf->Output('labels.pdf', 'D');
            }


        }
        catch(CHttpException $ex) {
            throw new CHttpException($ex->statusCode, $ex->getMessage());
        }
    }



    static protected function _pdf_4_on_1(TCPDF $pdf, array $couriers, $noError, &$labelQueque = [], $internalGetSecond = false, &$done = [])
    {

        $pageW = 210;
        $pageH = 297;

        $xBase = 10;
        $yBase = 10;

        $blockWidth = ($pageW / 2) - (2 * $xBase);
        $blockHeight = ($pageH / 2) - (2 * $yBase);

        $rightBlockXPosition = $blockWidth + 3 * $xBase;
        $bottomBlockYPosition = $blockHeight + 3 * $yBase;

        $totalCounter = 0;

        $amount = S_Useful::sizeof($couriers);

        $j = 0;
        /* @var $item Courier */
        //$ids = [];
        foreach($couriers AS $key => $item)
        {
//            if($item->courierTypeOoe && $amount > 1)
//                continue;

            //array_push($ids, $item->id);

            // special rule for multiple external packages (all have one local id)
            if($item->courierTypeExternal && $item->packages_number > 1)
                $loop = $item->packages_number;
            else
                $loop = 1;

            $forceImage = false;
            // localQueque - for handling additional pages of particular label
            $localQueque = [];
            $localQuequeLoop = 0;
            for($i = 1; $i <= $loop; $i++)
            {
                $totalCounter++;
                if($totalCounter > 2000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));

                $x = ($j%2 == 0) ? $xBase : $rightBlockXPosition;
                $y = ($j%4 == 2 OR $j%4 == 3)? $y = $bottomBlockYPosition : $yBase;

                if($j%4 == 0)
                {
                    $pdf->AddPage('H','A4');
                }

                $return = self::_labelBody($key, $done, $pdf, $x, $y, $blockWidth, $blockHeight, $item, $i, $noError, $forceImage, false, $labelQueque, $localQueque, $internalGetSecond);

                // handling additional pages for particular label
                // it there is something in local label
                if(isset($localQueque[$localQuequeLoop]))
                {
                    // set counter back because there will be one more label
                    $i--;
                    $forceImage = $localQueque[$localQuequeLoop]; // image to be printed
                    unset($localQueque[$localQuequeLoop]);
                    $localQuequeLoop++;
                }

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;

                //file_put_contents('tcpdf.txt', print_r($j,1)."\r\n",FILE_APPEND);
            }

        }

        return $pdf;
    }


    static protected function _pdf_2_on_1(TCPDF $pdf, array $couriers, $noError, &$labelQueque = [], $internalGetSecond = false, &$done = [])
    {

        $pageW = 297;
        $pageH = 210;

        $xBase = 5;
        $yBase = 5;

        $blockWidth = ($pageW / 2) - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $rightBlockXPosition = $blockWidth + 3 * $xBase;

        $j = 0;
        /* @var $item Courier */
        $totalCounter = 0;

        $amount = S_Useful::sizeof($couriers);

        //$ids = [];
        foreach($couriers AS $key => $item)
        {
//            if($item->courierTypeOoe && $amount > 1)
//                continue;

            //array_push($ids, $item->id);

            // special rule for multiple external packages (all have one local id)
            if($item->courierTypeExternal && $item->packages_number > 1)
                $loop = $item->packages_number;
            else
                $loop = 1;

            $forceImage = false;
            // localQueque - for handling additional pages of particular label
            $localQueque = [];
            $localQuequeLoop = 0;
            for($i = 1; $i <= $loop; $i++)
            {


                $totalCounter++;
                if($totalCounter > 2000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));


                $x = ($j%2 == 0) ? $xBase : $rightBlockXPosition;

                if($j%2 == 0)
                {
                    $pdf->AddPage('L','A4');
                }

                $return = self::_labelBody($key, $done, $pdf, $x, $yBase, $blockWidth, $blockHeight, $item, $i, $noError, $forceImage, false, $labelQueque, $localQueque, $internalGetSecond);


                // handling additional pages for particular label
                // it there is something in local label
                if(isset($localQueque[$localQuequeLoop]))
                {
                    // set counter back because there will be one more label
                    $i--;
                    $forceImage = $localQueque[$localQuequeLoop]; // image to be printed
                    unset($localQueque[$localQuequeLoop]);
                    $localQuequeLoop++;
                }

                if(!$return) {
                    continue;
                }
                else
                    $pdf = $return;

                $j++;
            }

        }

        return $pdf;
    }

    static protected function _pdf_2_on_1_one_right(TCPDF $pdf, array $couriers, $noError, &$labelQueque = [], $internalGetSecond = false, &$done = [])
    {

        $pageW = 297;
        $pageH = 210;

        $xBase = 5;
        $yBase = 5;

        $blockWidth = ($pageW / 2) - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $rightBlockXPosition = $blockWidth + 3 * $xBase;

        $j = 0;
        /* @var $item Courier */
        $totalCounter = 0;

        $amount = S_Useful::sizeof($couriers);

        //$ids = [];
        foreach($couriers AS $key => $item)
        {
//            if($item->courierTypeOoe && $amount > 1)
//                continue;

            //array_push($ids, $item->id);

            // special rule for multiple external packages (all have one local id)
            if($item->courierTypeExternal && $item->packages_number > 1)
                $loop = $item->packages_number;
            else
                $loop = 1;

            $forceImage = false;
            // localQueque - for handling additional pages of particular label
            $localQueque = [];
            $localQuequeLoop = 0;
            for($i = 1; $i <= $loop; $i++)
            {


                $totalCounter++;
                if($totalCounter > 2000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));


                $x = $rightBlockXPosition;
                $pdf->AddPage('L','A4');


                $return = self::_labelBody($key, $done, $pdf, $x, $yBase, $blockWidth, $blockHeight, $item, $i, $noError, $forceImage, false, $labelQueque, $localQueque, $internalGetSecond);


                // handling additional pages for particular label
                // it there is something in local label
                if(isset($localQueque[$localQuequeLoop]))
                {
                    // set counter back because there will be one more label
                    $i--;
                    $forceImage = $localQueque[$localQuequeLoop]; // image to be printed
                    unset($localQueque[$localQuequeLoop]);
                    $localQuequeLoop++;
                }

                if(!$return) {
                    continue;
                }
                else
                    $pdf = $return;

                $j++;
            }

        }

        return $pdf;
    }

    static protected function _png_single_file(TCPDF $pdf, $courier, $noError, &$labelQueque = [], $internalGetSecond = false, &$done = [])
    {
        $localQueque = [];
        return self::_labelBody(0, $done, $pdf, 0, 0, 0, 0, $courier, 0, $noError, false, false, $labelQueque, $localQueque, $internalGetSecond, true);
    }


    static protected function _pdf_roll(TCPDF $pdf, array $couriers, $noError, &$labelQueque = [], $internalGetSecond = false, &$done = [], $withPackageSlip = false)
    {
        // queque for oversized labels;
        $labelQueque = [];

        $pageW = 100;
        $pageH = 150;

        $xBase = 1;
        $yBase = 1;

        $blockWidth = $pageW - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $totalCounter = 0;

        $j = 0;
        /* @var $item Courier */

        $amount = S_Useful::sizeof($couriers);

        //$ids = [];
        foreach($couriers AS $key => $item)
        {
//            if($item->courierTypeOoe && $amount > 1)
//                continue;

            //array_push($ids, $item->id);

            // special rule for multiple external packages (all have one local id)
            if($item->courierTypeExternal && $item->packages_number > 1)
                $loop = $item->packages_number;
            else
                $loop = 1;

            $forceImage = false;
            // localQueque - for handling additional pages of particular label
            $localQueque = [];
            $localQuequeLoop = 0;
            for($i = 1; $i <= $loop; $i++)
            {
                $totalCounter++;
                if($totalCounter > 2000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));


                $pdf->AddPage('H',array(100,150), true);
                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $blockWidth, $blockHeight, $item, $i, $noError, $forceImage, false, $labelQueque, $localQueque, $internalGetSecond);


                // handling additional pages for particular label
                // it there is something in local label
                if(isset($localQueque[$localQuequeLoop]))
                {
                    // set counter back because there will be one more label
                    $i--;
                    $forceImage = $localQueque[$localQuequeLoop]; // image to be printed
                    unset($localQueque[$localQuequeLoop]);
                    $localQuequeLoop++;
                }

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                if($withPackageSlip)
                    $pdf = PackageSlipLabel::generateReminderPdfPage($pdf, $item);

                $j++;

            }
        }

        return $pdf;
    }

    static protected function _pdf_roll_with_cn_22(TCPDF $pdf, array $couriers, $noError, &$labelQueque = [], $internalGetSecond = false, &$done = [])
    {
        // queque for oversized labels;
        $labelQueque = [];

        $pageW = 100;
        $pageH = 150;

        $xBase = 2;
        $yBase = 2;

        $blockWidth = $pageW - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $totalCounter = 0;

        $j = 0;
        /* @var $item Courier */

        $amount = S_Useful::sizeof($couriers);

        //$ids = [];
        foreach($couriers AS $key => $item)
        {
//            if($item->courierTypeOoe && $amount > 1)
//                continue;

            //array_push($ids, $item->id);

            // special rule for multiple external packages (all have one local id)
            if($item->courierTypeExternal && $item->packages_number > 1)
                $loop = $item->packages_number;
            else
                $loop = 1;

            $forceImage = false;
            // localQueque - for handling additional pages of particular label
            $localQueque = [];
            $localQuequeLoop = 0;
            for($i = 1; $i <= $loop; $i++)
            {
                $totalCounter++;
                if($totalCounter > 2000)
                    throw new CHttpException(403, Yii::t('courier','Too many labels!'));


                $pdf->AddPage('H',array(100,150), true);
                $return = self::_labelBody($key, $done, $pdf, $xBase, $yBase, $blockWidth, $blockHeight, $item, $i, $noError, $forceImage, false, $labelQueque, $localQueque, $internalGetSecond);


                // handling additional pages for particular label
                // it there is something in local label
                if(isset($localQueque[$localQuequeLoop]))
                {
                    // set counter back because there will be one more label
                    $i--;
                    $forceImage = $localQueque[$localQuequeLoop]; // image to be printed
                    unset($localQueque[$localQuequeLoop]);
                    $localQuequeLoop++;
                }

                if(!$return)
                    continue;
                else
                    $pdf = $return;

                $j++;

            }

            // ADD CN22:
            $pdf->AddPage('H',array(100,150), true);
            $pdf = CourierCN22::generateSingle($pdf, $item);
        }

        return $pdf;
    }

    static protected function _labelBody($key, &$done, TCPDF $pdf, $xBase, $yBase, $blockWidth, $blockHeight, Courier $item, $i, $noError = false, $forceImage = false, $oversized = false, &$labelQueque = [], &$localQueque = [], $internalGetSecond = false, $returnImageString = false)
    {

        // force image for generating both labels for courier type Internal
        if($forceImage !== false)
        {
            $image = $forceImage;
            $oversized = false;
        }
        else
        {
            $type = $item->getType();

            $image = '';
            $additionalImages = [];
            $error = false;

//         COURIER INTERNAL SIMPLE:
            if ($type == Courier::TYPE_INTERNAL_SIMPLE) {
                if ($item->courierTypeInternalSimple->courierExtOperatorDetails->stat == CourierExtOperatorDetails::STAT_OK) {
                    {
                        $temp = $item->courierTypeInternalSimple->courierExtOperatorDetails->fetchLabel($noError, false);
                        if ($temp == '') {
                            if ($noError)
                                return false;
                        }


                        $oversized = $item->courierTypeInternalSimple->courierExtOperatorDetails->isOversized();
                        $image = $temp;

                    }
                } else {
                    if ($noError)
                        return false;
                }
            } //         COURIER DOMESTIC:
            else if ($type == Courier::TYPE_DOMESTIC) {
//                if ($item->courierTypeDomestic->courierLabelNew->stat == CourierLabelNew::STAT_SUCCESS) {
                {
                    $temp =  $item->courierTypeDomestic->courierLabelNew->getFileAsString($noError, false);

                    if ($temp == '') {
                        if ($noError)
                            return false;
                    }

                    $oversized = $item->courierTypeDomestic->courierLabelNew->isOversized();

                    if(is_array($temp)) {
                        $image = array_shift($temp); // get first image of array
                        $additionalImages = $temp; // rest of pages
                    }
                    else $image = $temp;

                }
//                } else {
//                    if ($noError)
//                        return false;
//                }

            }
            //         COURIER TYPE U:
            else if ($type == Courier::TYPE_U) {

                $temp =  $item->courierTypeU->courierLabelNew->getFileAsString($noError, false);

                if ($temp == '') {
                    if ($noError)
                        return false;
                }

                $oversized = $item->courierTypeU->courierLabelNew->isOversized();

                if(is_array($temp)) {
                    $image = array_shift($temp); // get first image of array
                    $additionalImages = $temp; // rest of pages
                }
                else $image = $temp;

            }
            // COURIER TYPE EXTERNAL RETURN
            else if ($type == Courier::TYPE_EXTERNAL_RETURN) {

                $temp =  $item->courierTypeExternalReturn->courierLabelNew->getFileAsString($noError, false);

                if ($temp == '') {
                    if ($noError)
                        return false;
                }


                if(is_array($temp)) {
                    $image = array_shift($temp); // get first image of array
                    $additionalImages = $temp; // rest of pages
                }
                else $image = $temp;


            }
            // COURIER TYPE EXTERNAL RETURN
            else if ($type == Courier::TYPE_RETURN) {

                $temp =  $item->courierTypeReturn->courierLabelNew->getFileAsString($noError, false);

                if ($temp == '') {
                    if ($noError)
                        return false;
                }


                if(is_array($temp)) {
                    $image = array_shift($temp); // get first image of array
                    $additionalImages = $temp; // rest of pages
                }
                else $image = $temp;

            }
            // TYPE OOE - ONLY USED WHEN LABEL IN COURIER LABEL NEW (BY MASSIVE IMPORT)
            else if($type == Courier::TYPE_INTERNAL_OOE)
            {

                if($item->courierTypeOoe->courier_label_new_id != NULL)
                {
                    $temp = $item->courierTypeOoe->courierLabelNew->getFileAsString($noError, false);

                    if ($temp[0] == '') {
                        if ($noError)
                            return false;
                    }
                    $oversized = $item->courierTypeOoe->courierLabelNew->isOversized();
                    if(is_array($temp)) {
                        $image = array_shift($temp); // get first image of array
                        $additionalImages = $temp; // rest of pages
                    } else $image = $temp;

                } else {

                    try {
                        $url = $item->courierTypeOoe->label_url;

                        $f1 = @fopen($url, 'r');
                        $fcontent = $contents = @stream_get_contents($f1);

                        if (!$fcontent)
                            $error = true;

                        return Yii::app()->getRequest()->sendFile('label.pdf', ($fcontent), 'application/pdf');
                    } catch (Exception $ex) {
                        $error = true;
                    }
                }


            }
            // EXTERNAL TYPES
            else if ($type == Courier::TYPE_EXTERNAL) {
                $image = KaabLabel::generate($item, $i, $item->senderAddressData, $item->receiverAddressData);
            }
            // INTERNAL TYPE
            else if ($type == Courier::TYPE_INTERNAL) {

                if($internalGetSecond)
                    $image = self::_getImageForCourierTypeInternal_Last($item->courierTypeInternal);
                else
                    $image = self::_getImageForCourierTypeInternal_First($item->courierTypeInternal);


                $oversized = $image['oversized'];


                $temp = $image['img'];
                if(is_array($temp)) {
                    $image = array_shift($temp); // get first image of array
                    $additionalImages = $temp; // rest of pages
                } else
                    $image = $temp;
            } else {
                $error = true;
            }

            if ($image == '' OR $error) {
                if ($noError) {
                    return false;
                } else {
                    throw new CHttpException(403, Yii::t('courier', 'Błąd generowania etykiety #{no}. Skontaktuj się z nami w tej sprawie.', array('{no}' => $item->local_id)));
                }
            }

        }


        // queque additional images after first image
        if(is_array($additionalImages))
            foreach($additionalImages AS $addtional) {
                array_push($localQueque, $addtional);
            }


        if($returnImageString)
            return $image;

        if($oversized)
        {
            // PRINT INFO ABOUT NOT SCALING
            $pdf->SetFont('freesans', '', 8);
            $pdf->MultiCell($blockWidth, $blockHeight, Yii::t('courier',"{no}Ta etykieta nie mogła zostać wyskalowana do wybranego przez Ciebie formatu i została przeniesiona w formacie A4 na kolejną stronę. Przepraszamy za utrudnienia.", ['{no}' => "#$item->local_id\r\n\r\n"]),1,'C', false, 0, $xBase, $yBase, true, 0, false, true,$blockHeight, 'M');

            // add label to queque - will be printed at the end of document
            array_push($labelQueque, $image);

        } else {

            // PRINT IMAGE TO PDF
            $pdf->Image(
                '@' . $image, // file
                $xBase, // x
                $yBase, // y
                $blockWidth, // w
                $blockHeight, // h
                '', // type
                '', // link
                '', // align
                false, // resize // TEST: TRUE
                300, // dpi // TEST: 600
                '', // palign
                false, // ismask
                false, // imgmask
                0, // border
                'CM', // fitbox // TEST: false
                false, // hidden
                false, // fitonpage
                false, // alt
                array() // altimgs
            );
        }

        $done[$key] = $item->id;

// RETURN PDF
        return $pdf;
    }


    protected static function _getImageForCourierTypeInternal_First(CourierTypeInternal $item)
    {

        $oversized = false;


        if ($item->with_pickup == CourierTypeInternal::WITH_PICKUP_CONTRACT) {

            if ($item->pickupLabel instanceof CourierLabel) {
                $img = $item->pickupLabel->fetchLabel(false);
                if ($item->pickupLabel->courier_label_new_id !== NULL)
                    $oversized = $item->pickupLabel->courierLabelNew->isOversized();
            } else
                return false;


        }
        else if ($item->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH)
        {

            if($item->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
                $label = $item->commonLabel;
            else
                $label = $item->pickupLabel;


            if ($label instanceof CourierLabel) {
                $img = $label->fetchLabel(false);
                if ($label->courier_label_new_id !== NULL)
                    $oversized = $label->courierLabelNew->isOversized();
            } else
                return false;


        } else {


            if ($item->common_operator_ordered) {

                if ($item->common_operator_ordered == CourierTypeInternal::OPERATOR_OWN) {
                    // for compatibility with packages before implementing saving own labels
                    if ($item->commonLabel !== NULL)
                        $img = $item->commonLabel->fetchLabel(false);
                    else
                        $img = KaabLabel::generate($item->courier, $item->family_member_number, $item->courier->senderAddressData, $item->courier->receiverAddressData, true);
                } else if ($item->commonLabel instanceof CourierLabel) {

                    $img = $item->commonLabel->fetchLabel(false);
                    if ($item->commonLabel->courier_label_new_id !== NULL)
                        $oversized = $item->commonLabel->courierLabelNew->isOversized();
                } else
                    return false;

            } else {


                if ($item->with_pickup) {

                    if ($item->pickup_operator_ordered == CourierTypeInternal::OPERATOR_OWN) {
                        // for compatibility with packages before implementing saving own labels
                        if ($item->pickupLabel !== NULL)
                            $img = $item->pickupLabel->fetchLabel(false);
                        else
                            $img = KaabLabel::generate($item->courier, $item->family_member_number, $item->courier->senderAddressData, $item->pickupHub->address, true);
                    } else
                        if ($item->pickupLabel instanceof CourierLabel) {
                            $img = $item->pickupLabel->fetchLabel(false);
                            if ($item->pickupLabel->courier_label_new_id !== NULL)
                                $oversized = $item->pickupLabel->courierLabelNew->isOversized();
                        } else
                            return false;

                } else {

                    if ($item->delivery_operator_ordered == CourierTypeInternal::OPERATOR_OWN) {
                        // for compatibility with packages before implementing saving own labels
                        if ($item->deliveryLabel !== NULL)
                            $img = $item->deliveryLabel->fetchLabel(false);
                        else
                            $img = KaabLabel::generate($item->courier, $item->family_member_number, $item->deliveryHub->address, $item->courier->receiverAddressData, true);
                    } else
                        if ($item->deliveryLabel instanceof CourierLabel) {
                            $img = $item->deliveryLabel->fetchLabel(false);
                            if ($item->deliveryLabel->courier_label_new_id !== NULL)
                                $oversized = $item->deliveryLabel->courierLabelNew->isOversized();
                        } else
                            return false;

                }

            }

        }

        return [
            'img' => $img,
            'oversized' => $oversized
        ];
    }

    protected static function _getImageForCourierTypeInternal_Last(CourierTypeInternal $item)
    {

        $oversized = false;

        if ($item->with_pickup == CourierTypeInternal::WITH_PICKUP_CONTRACT) {
            if ($item->delivery_operator_ordered == CourierTypeInternal::OPERATOR_OWN) {
                // for compatibility with packages before implementing saving own labels
                if ($item->deliveryLabel !== NULL)
                    $img = $item->deliveryLabel->fetchLabel(false);
                else
                    $img = KaabLabel::generate($item->courier, $item->family_member_number, $item->deliveryHub->address, $item->courier->receiverAddressData, true);

            } else if ($item->deliveryLabel instanceof CourierLabel) {
                $img = $item->deliveryLabel->fetchLabel(false);
                if ($item->deliveryLabel->courier_label_new_id !== NULL)
                    $oversized = $item->deliveryLabel->courierLabelNew->isOversized();
            } else
                return false;

        }
        else if ($item->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH)
        {
            if ($item->delivery_operator_ordered == CourierTypeInternal::OPERATOR_OWN) {
                // for compatibility with packages before implementing saving own labels
                if ($item->deliveryLabel !== NULL)
                    $img = $item->deliveryLabel->fetchLabel(false);
                else
                    $img = KaabLabel::generate($item->courier, $item->family_member_number, $item->deliveryHub->address, $item->courier->receiverAddressData, true);

            } else if ($item->deliveryLabel instanceof CourierLabel) {
                $img = $item->deliveryLabel->fetchLabel(false);
                if ($item->deliveryLabel->courier_label_new_id !== NULL)
                    $oversized = $item->deliveryLabel->courierLabelNew->isOversized();
            } else
                return false;

        } else {

            if ($item->common_operator_ordered) {

                if ($item->common_operator_ordered == CourierTypeInternal::OPERATOR_OWN) {
                    // for compatibility with packages before implementing saving own labels
                    if ($item->commonLabel !== NULL)
                        $img = $item->commonLabel->fetchLabel(false);
                    else
                        $img = KaabLabel::generate($item->courier, $item->family_member_number, $item->courier->senderAddressData, $item->courier->receiverAddressData, true);
                } else if ($item->commonLabel instanceof CourierLabel) {
                    $img = $item->commonLabel->fetchLabel(false);
                    if ($item->commonLabel->courier_label_new_id !== NULL)
                        $oversized = $item->commonLabel->courierLabelNew->isOversized();
                } else
                    return false;

            } else {

                if ($item->delivery_operator_ordered == CourierTypeInternal::OPERATOR_OWN) {
                    // for compatibility with packages before implementing saving own labels
                    if ($item->deliveryLabel !== NULL)
                        $img = $item->deliveryLabel->fetchLabel(false);
                    else
                        $img = KaabLabel::generate($item->courier, $item->family_member_number, $item->deliveryHub->address, $item->courier->receiverAddressData, true);

                } else if ($item->deliveryLabel instanceof CourierLabel) {
                    $img = $item->deliveryLabel->fetchLabel(false);
                    if ($item->deliveryLabel->courier_label_new_id !== NULL)
                        $oversized = $item->deliveryLabel->courierLabelNew->isOversized();
                } else
                    return false;

            }

        }



        return [
            'img' => $img,
            'oversized' => $oversized
        ];
    }
}