<?php

if($item['M'] && (S_Useful::sizeof($item['PV']) > 1 OR $item['PV'][key(($item['PV']))]->weight_top > 0)):

    echo $form->error($item['M'], '_maxWeightErrorContainer');
    ?>
    <table class="table table-striped table-bordered" style="background: white;">
        <tr>
            <th>Weight to</th>
            <th style="text-align: center;">Price</th>
        </tr>        
        <?php

        foreach($item['PV'] AS $key => $priceItem):

            if($priceItem->hasErrors() OR !$priceItem->price OR $priceItem->price->hasErrors())
                $error = true;
            ?>
            <tr>
                <td><?php echo $form->textField($priceItem, '['.$key.']weight_top', ['style' => 'width: 40px;', 'disabled' => true]); ?><?php echo $form->error($priceItem,'['.$i.']weight_top'); ?></td>
                <td>
                    <?php $this->widget('PriceForm', array(
                        'form' => $form,
                        'model' => $priceItem->price,
                        'formFieldPrefix' => '['.$key.']',
                        'vertical' => false,
                        'readonly' => true
                    ));?>
                    <?= $priceItem->price ? $form->error($priceItem->price, 'price_id') : ''; ?>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
    </table>

<?php
else:
    ?>
    - NONE -
<?php
endif;
?>