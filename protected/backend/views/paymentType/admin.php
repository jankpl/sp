<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
	);


?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'payment-type-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'name',
        array(
            'name' => 'commission_percentage',
            'value' => '$data->commission_percentage."%"',
        ),
        array(
            'header' => 'Commision amount',
            'value' =>  'S_Price::formatPrice($data->commissionPrice->pln) ." PLN / ".
                    S_Price::formatPrice($data->commissionPrice->eur) ." EUR / ".
                    S_Price::formatPrice($data->commissionPrice->gbp) ." GBP"',
        ),
		/*
		'available_level',
		*/
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update} {view}',
            'htmlOptions'=> array('style'=>'width:80px'),
            'buttons'=>array(
                'status'=>array(
                    'visible' => '$data->stat==1',
                    'label'=>'deactivate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_active.png',
                    'url' => 'Yii::app()->createUrl("admin/stat",array("id" => $data->id))',
                ),
                'status_a'=>array(
                    'visible' => '$data->stat==0',
                    'label'=>'activate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_not_active.png',
                    'url' => 'Yii::app()->createUrl("admin/stat",array("id" => $data->id))',
                ),
            ),
        ),
	),
)); ?>