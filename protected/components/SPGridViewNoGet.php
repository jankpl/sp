<?php

Yii::import('ext.selgridviewnoget.BootSelGridViewNoGet');

/**
 * Class SPGridViewNoGet
 * This class add extra row in gridViewHeader
 * Modified 04.10.2016 - keep selected ids in hidden field, not URL
 */
class SPGridViewNoGet extends BootSelGridViewNoGet {

    public $addingHeaders = array();
    public $hiddenColumns = [];

    /**
     * Renders the data items for the grid view.
     */
    public function renderItems()
    {
        if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
        {
            echo "<table class=\"{$this->itemsCssClass}\" style=\"table-layout: fixed;\">\n";

            if(is_array($this->columns))
                foreach($this->columns AS $column)
                {
                    if(isset($column->name) && in_array($column->name, $this->hiddenColumns))
                        continue;

                    $option = $column->htmlOptions['width'] != '' ? "width=\"{$column->htmlOptions['width']}\"" : "";

                    echo "<col {$option}/>\n";
                }

            $this->renderTableHeader();
            ob_start();
            $this->renderTableBody();
            $body=ob_get_clean();
            echo $body;
            $this->renderTableFooter();
            echo "</table>";
        }
        else
            $this->renderEmptyText();
    }

    public function renderTableHeader() {
        if (!empty($this->addingHeaders))
            $this->multiRowHeader();

        parent::renderTableHeader();
    }

    protected function multiRowHeader() {
        echo CHtml::openTag('thead') . "\n";
        foreach ($this->addingHeaders as $row) {
            $this->addHeaderRow($row);
        }
        echo CHtml::closeTag('thead') . "\n";
    }

    // each cell value expects array(array($text,$colspan,$options), array(...))
    protected function addHeaderRow($row) {
        // add a single header row
        echo CHtml::openTag('tr') . "\n";
        // inherits header options from first column
        $fcol = 0;
        while (!isset($this->columns[$fcol])) {
            $fcol++;
        }
        $options = $this->columns[$fcol]->headerHtmlOptions;

        foreach ($row as $header) {
            $options['colspan'] = $header['colspan'];
            $cellOptions=($header['options'] + $options);
            echo CHtml::openTag('th', $cellOptions);
            echo $header['text'];
            echo CHtml::closeTag('th');
        }
        echo CHtml::closeTag('tr') . "\n";
    }

    /**
     * Renders the table footer.
     */
    public function renderTableFooter()
    {
        $hasFilter=$this->filter!==null && $this->filterPosition===self::FILTER_POS_FOOTER;
        $hasFooter=$this->getHasFooter();
        if($hasFilter || $hasFooter)
        {
//            echo "<tfoot style=\"display: block;\">\n";
            if($hasFooter)
            {
                echo "<tr class=\"gv_footer\">\n";
                foreach($this->columns as $column) {
                    if(isset($column->name) && in_array($column->name, $this->hiddenColumns))
                        continue;

                    $column->renderFooterCell();
                }
                echo "</tr>\n";
            }
            if($hasFilter)
                $this->renderFilter();
//            echo "</tfoot>\n";
        }
    }


}
?>