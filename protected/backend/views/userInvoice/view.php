<?php
/* @var $this UserInvoiceController */
/* @var $model UserInvoice */

$this->breadcrumbs=array(
    'User Messages'=>array('index'),
    $model->id,
);

$this->menu=array(
    array('label'=>'List UserInvoice', 'url'=>array('index')),
    array('label'=>'Create UserInvoice', 'url'=>array('create')),
    array('label'=>'Update UserInvoice', 'url'=>array('update', 'id'=>$model->id)),
);
?>

<h1>View UserInvoice #<?php echo $model->id; ?></h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'date_entered',
        'date_updated',
        'text',
        'date',
        'user.login',
        array(
            'name' => 'seen',
            'value' => UserInvoice::getStatArray()[$model->stat],
        ),
        'no',
    ),
)); ?>

<?php if($model->inv_file_name != ''): ?>
    <br/>
    <br/>
    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file - Invoice', array('/userInvoice/getFile', 'id' => $model->id, 'type' => UserInvoice::FILE_TYPE_INV), array('class' => 'btn btn-primary')), array('closeText' => false));?>
<?php endif; ?>
<?php if($model->sheet_file_name != ''): ?>
    <br/>
    <br/>
    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file - Sheet', array('/userInvoice/getFile', 'id' => $model->id, 'type' => UserInvoice::FILE_TYPE_SHEET), array('class' => 'btn btn-primary')), array('closeText' => false));?>
<?php endif; ?>



<?php if($model->stat == UserInvoice::UNPUBLISHED): ?>
    <br/>
    <br/>
    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, CHtml::link('Publish', array('/userInvoice/publish', 'id' => $model->id), array('class' => 'btn btn-larget btn-success')), array('closeText' => false));?>

<?php endif; ?>

<h3>Create credit note by Fakturownia</h3>
<a href="<?= Yii::app()->createUrl('/fakturownia/createByUserInvoice', ['id' => $model->id]);?>" class="btn" target="_blank">With user data *</a><br/><br/>

<h3>Create accountant note</h3>
<a href="<?= Yii::app()->createUrl('/correctionNote/createByUserInvoice', ['id' => $model->id]);?>" class="btn" target="_blank">With user data *</a>
<br/><br/>(*) May be differend than on invoice!

<h3>History</h3>
<table class="table table-striped table-bordered table-condensed">
    <tr>
        <th>#</th>
        <th>Date</th>
        <th>Action</th>
        <th>Author</th>
    </tr>
    <?php
    $i = 1;
    foreach($model->userInvoiceHistory AS $item):
        ?>
        <tr>
            <td><?= $i;?></td>
            <td><?= $item->date_entered;?></td>
            <td><?= $item->text;?></td>
            <td><?= $item->admin_id ? $item->admin->login : '-';?></td>
        </tr>
        <?php
        $i++;
    endforeach;
    ?>
</table>