<?php

class MyDump
{
    public static function dump($filename, $content, $addToFile = true, $addNewLineAtEnd = true, $addDateAtBeginning = true, $withLineSeparators = true)
    {
        $baseWithDate = self::getTodaysDir();

        if(!is_dir($baseWithDate))
            @mkdir($baseWithDate);

        $filename = $baseWithDate . DIRECTORY_SEPARATOR . $filename;

        if($addDateAtBeginning)
            $content = date('Y-m-d H:i:s').' : '.$content;

        if($addNewLineAtEnd)
        {
            if($withLineSeparators)
                $content .= "\r\n----\r\n";
            else
                $content .= "\r\n";
        }

        if($addToFile)
            @file_put_contents($filename, $content, FILE_APPEND);
        else
            @file_put_contents($filename, $content);
    }

    public static function getTodaysDir()
    {
        $base = Yii::app()->basePath . DIRECTORY_SEPARATOR .'_dump';
        $baseWithDate = $base.'/'.date('Ymd');

        return $baseWithDate;
    }

}