<?php
/**
 * This is the configuration for generating message translations
 * for the Yii framework. It is used by the 'yiic message' command.
 */
return array(
    'sourcePath'=>'c:/Dane/WWW/localhost/httpd/SwiatPrzesylek.pl/swiatPrzesylek_ms5/protected/', // eg. '/projects/myproject/protected'
    'messagePath'=>'c:/Dane/WWW/localhost/httpd/SwiatPrzesylek.pl/swiatPrzesylek_ms5/protected/messages/', // '/projects/myproject/protected/messages'
    'languages'=>array(
        'pl',
        'en',
        'en_rs',
//        'en_gb', // used CQL version instead
        'pl_ruch',
        'pl_quriers',
        'en_cql',
        'de',
        'uk',
        'en_orangep'
        ), // according to your translation needs
    'fileTypes'=>array('php'),
    'overwrite'=>true,
    'removeOld'=>true,
    'sort'=>true,
    'fileHeader'=>false,
    'exclude'=>array(
        '.idea',
		'.svn',
        '.gitignore',
        'yiilite.php',
        'yiit.php',
        '/i18n/data',
        '/messages',
        '/vendors',
        '/web/js',
        '/backend',
        '/extensions',
        'UserModule.php', // if you are using yii-user ...
    ),
);