<?php
/* @var $this UserInvoiceController */
/* @var $model UserInvoice */

?>

<h1>Manage User Debt Collection</h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php $this->widget('ext.selgridview.BootSelGridView', array(
    'id'=>'user-debt-collection-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'selectableRows'=>2,
    'afterAjaxUpdate'=>"function(){
        $('[name=\'UserDebtCollectionGridView[user_id]\']').chosen({allow_single_deselect:true});
        $('.chosen-drop').css('width', '300px');
    }",
    'columns'=>array(
        [
            'name' => 'id',
            'htmlOptions' => [
                'width' => '30',
            ],
        ],
        array(
            'name' => 'user_id',
            'value' => '$data->user->login',
            'filter' => CHtml::listData(User::model()->findAll(['order' => 'login ASC']),'id', 'login'),
            'htmlOptions' => [
                'width' => '100',
            ],
        ),
        [
            'name' => 'date_entered',
            'value' => 'substr($data->date_entered, 0, 10)',
        ],
        [
            'name' => 'date_updated',
            'value' => 'substr($data->date_updated, 0, 10)',
        ],
        [
            'name' => 'no',
            'htmlOptions' => [
                'width' => '30',
            ],
        ],
        'title',
        'inv_date',
        'inv_payment_date',
        'inv_value',
        'paid_value',
        [
            'name' => 'inv_currency',
            'value' => 'Currency::nameById($data->inv_currency)',
            'filter' => CHtml::listData(Currency::model()->findAll(),"id", "short_name"),
        ],
        [
            'name' => '_isPaid',
            'value' => '$data->isPaidWithPartialName()',
            'filter' => [
                0 => 'No',
                1 => 'Yes',
                2 => 'Partialy'
            ],
        ],
        [
            'name' => 'stat_inv',
            'value' => 'UserInvoice::getStatInvArray()[$data->stat_inv]',
            'filter' => UserInvoice::getStatInvArray(),
        ],
        [
            'name' => 'stat',
            'value' => 'UserInvoice::getStatArray()[$data->stat]',
            'filter' => UserInvoice::getStatArray(),
        ],
        [
            'name' => '_daysAfterPayment',
            'value' => '$data->daysAfterPayment()',
            'filter' => false,
        ],
        [
            'name' => '_salesman_email',
            'value' => '$data->user->salesman->email',
        ],
        [
            'name' => '_debt_status',
            'value' => '$data->userInvoiceDebtOptionsLast ? $data->userInvoiceDebtOptionsLast->getStatName() : ""',
            'filter' => UserInvoiceDebtOptions::getStatList()
        ],
        [
            'name' => '_debt_status_date',
            'value' => '$data->userInvoiceDebtOptionsLast ? $data->userInvoiceDebtOptionsLast->date_entered : ""',
        ],
        [
            'name' => '_contract_type',
            'value' => '$data->user->getContractTypeName()',
            'filter' => User::getContractTypeList(),
        ],
        [
            'name' => '_source',
            'value' => '$data->user->getSourceDomainName()',
            'filter' => User::sourceDomainList(),
        ],

        /*
        'user_id',
        'seen',
        */

        array(
            'class'=>'CButtonColumn',
            'template' => '{view}{update}',
            'buttons' => array(
                'update' => array(
                    'url' => function($data) {
                        return Yii::app()->controller->createUrl('/userInvoice/update', ['id' => $data->id]);
                    },
                ),
            ),
        ),
        [
                'class' => 'CCheckBoxColumn',
        ]
    ),
)); ?>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/chosen/chosen.jquery.min.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/chosen/chosen.min.css');
$script = "
$('[name=\'UserDebtCollectionGridView[user_id]\']').chosen({allow_single_deselect:true});
$('.chosen-drop').css('width', '300px');
";
Yii::app()->clientScript->registerScript('chosen-js', $script, CClientScript::POS_READY);

?>
<br/>
<br/>
<a href="<?= Yii::app()->urlManager->createUrl('/userDebtCollection/exportDebts');?>" class="btn btn-primary">Export debts to XLS</a>
<br/>
<br/>

<h4>Set debt collection status</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-labels',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/userDebtCollection/setStatusByGridView'),
));
?>


<?php echo CHtml::dropDownList('status','',UserInvoiceDebtOptions::getStatList());?>

<?php
echo TbHtml::submitButton('Set status', array(
    'onClick' => 'js:
    $("#debts-ids").val($("#user-debt-collection-grid").selGridView("getAllSelection").toString());   
    ',
    'name' => 'DebtIds', 'size' => TbHtml::BUTTON_SIZE_LARGE, 'confirm' => 'Are you sure?'));
?>

<?php echo CHtml::hiddenField('Debt[ids]','', array('id' => 'debts-ids')); ?>
<?php
$this->endWidget();
?>
