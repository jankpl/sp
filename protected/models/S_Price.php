<?php

class S_Price
{

    private static $_roundPrecision = 2;
    private static $_vat = 0.23;

    public static function getVatRate()
    {
        return self::$_vat;
    }

    public static function getVat($netto)
    {
        $vat = $netto * self::$_vat;
        $vat = round($vat,self::$_roundPrecision);
        return $vat;
    }

    public static function priceWithVat($netto)
    {
        $brutto = $netto + self::getVat($netto);
        return $brutto;
    }

    public static function formatPrice($price)
    {
        $price = number_format($price,2,',',' ');
        return $price;
    }

    public static function formatPercentage($percentage)
    {
        $percentage = $percentage * 100;
        $percentage = $percentage.'%';
        return $percentage;
    }
}