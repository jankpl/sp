<?php

    class bUniversalStatManager extends bBaseBehavior
    {
        protected $_allowedClass = array('CourierStatHistory', 'HybridMailStatHistory', 'InternationalMailStatHistory', 'PostalStatHistory',);

        protected function getStatClass()
        {
            $statClass = NULL;

            switch ($this->getClass()) {
                case 'CourierStatHistory':
                    $statClass = 'CourierStat';
                    break;
                case 'HybridMailStatHistory':
                    $statClass = 'HybridMailStat';
                    break;
                case 'InternationalMailStatHistory':
                    $statClass = 'InternationalMailStat';
                    break;
                case 'PostalStatHistory':
                    $statClass = 'PostalStat';
                    break;
            }

            return $statClass;
        }

        protected function getParentRelation()
        {
            $relation = NULL;

            switch ($this->getClass()) {
                case 'Courier':
                    $relation = 'courier';
                    break;
                case 'HybridMail':
                    $relation = 'hybridMail';
                    break;
                case 'InternationalMail':
                    $relation = 'internationalMailStat';
                    break;
                case 'Postal':
                    $relation = 'postal';
                    break;
            }

            return $relation;
        }

        protected function getParentStatAttribute()
        {
            $relation = NULL;

            switch ($this->getClass()) {
                case 'CourierStatHistory':
                    $relation = 'courier_stat_id';
                    break;
                case 'HybridMailStatHistory':
                    $relation = 'stat';
                    break;
                case 'InternationalMailStatHistory':
                    $relation = 'international_mail_stat_id';
                    break;
                      case 'PostalStatHistory':
                          $relation = 'postal_stat_id';
                          break;
            }

            return $relation;
        }

        protected function getStatHistoryClass()
        {
            $statClass = NULL;
            switch ($this->getClass()) {
                case 'CourierStatHistory':
                    $statClass = 'CourierStatHistory';
                    break;
                case 'HybridMailStatHistory':
                    $statClass = 'HybridMailStatHistory';
                    break;
                case 'InternationalMailStatHistory':
                    $statClass = 'InternationalMailStatHistory';
                    break;
                case 'PostalStatHistory':
                    $statClass = 'PostalStatHistory';
                    break;
            }

            return $statClass;
        }

        protected function getForeignKeyClassAttribute()
        {
            $attribute = NULL;
            switch ($this->getClass()) {
                case 'CourierStatHistory':
                    $attribute = 'courier_id';
                    break;
                case 'HybridMailStatHistory':
                    $attribute = 'hybrid_mail_id';
                    break;
                case 'InternationalMailStatHistory':
                    $attribute = 'international_mail_id';
                    break;
                case 'PostalStatHistory':
                    $attribute = 'postal_id';
                    break;
            }
            return $attribute;
        }


        public function updateStatUSM()
        {
            $statClass = $this->getStatClass();

            $newStat = $statClass::model()->findByPk($this->owner()->current_stat_id);
            if ($newStat === null)
                return false;

            if ($this->owner()->status_date == '') {
                $status_date = date('Y:m:d h:i:s');
                $this->owner()->status_date = $status_date;
            }

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            //$statHistoryClass = $this->getStatHistoryClass();
            //$lastStat = $statHistoryClass::model()->find(array('order' => 'id DESC'));


//            if ($lastStat->id == $this->owner()->id) {
//                // this is current status for item, so change also item's status with history change
//                $relation = $this->getParentRelation();
//                $attribute = $this->getParentStatAttribute();
//                $this->owner()->$relation->$attribute = $newStat;
//
//                if (!$this->owner()->$relation->update(array($attribute)))
//                    $errors = true;
//            }

            if (!$this->owner()->update(array('current_stat_id', 'current_stat_name', 'status_date')))
                $errors = true;


            if ($errors) {
                $transaction->rollback();
                return false;
            } else {
                $transaction->commit();
                return true;
            }

        }

        public function hideStatUSM()
        {

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;


            $this->owner()->hidden = 1;

            if (!$this->owner()->update(array('hidden')))
                $errors = true;


            if ($errors) {
                $transaction->rollback();
                return false;
            } else {
                $transaction->commit();
                return true;
            }

        }

        public function showStatUSM()
        {

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;


            $this->owner()->hidden = 0;

            if (!$this->owner()->update(array('hidden')))
                $errors = true;


            if ($errors) {
                $transaction->rollback();
                return false;
            } else {
                $transaction->commit();
                return true;
            }

        }

    }