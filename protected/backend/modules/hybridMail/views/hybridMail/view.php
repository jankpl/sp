<?php
/* @var $model HybridMail */
?>

<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>'User details', 'url'=>array('/user/view', 'id' => $model->user_id),'visible'=>$model->user_id!==null),
    array('label'=>'Order details', 'url'=>array('/order/view', 'id' => $model->order_id),'visible'=>$model->order_id!==null),
);
?>

<h1>HybridMail #<?php echo $model->local_id; ?></h1>


<h3>Mail details</h3>
<table class="list hLeft" style="width: 500px;">
    <tr>
        <td>User</td>
        <td><?php echo $model->user_id !== null ? CHtml::link($model->user->login, array('/user/view', 'id' => $model->user_id)) : '-';?></td>
    </tr>
    <tr>
        <td>Creation date</td>
        <td><?php echo substr($model->date_entered,0,16);?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td>[<?php echo $model->stat;?>] <?php echo $model->stat->name;?></td>
    </tr>
    <tr>
        <td>Order</td>
        <td><?php echo ($model->order_id != '')? CHtml::link('Order #'.$model->order_id, array('/order/view', 'id' => $model->order->id)):'-';?></td>
    </tr>
    <tr>
        <td>Local ID</td>
        <td><?php echo $model->local_id;?></td>
    </tr>
    <tr>
        <td>Pages</td>
        <td><?php echo $model->getTotalPages();?></td>
    </tr>
</table>

    <h3>Additions</h3>
    <table class="list hTop" style="width: 500px;">
        <tr>
            <td>Name</td>
            <td>Description</td>
        </tr>
        <?php

        /* @var $item HybridMailAdditionList */
        if(!S_Useful::sizeof($model->listAdditions()))
            echo '<tr><td colspan="2">none</td></tr>';
        else
            foreach($model->listAdditions() AS $item): ?>
                <tr>
                    <td><?php echo $item->name; ?></td>
                    <td><?php echo $item->description; ?></td>
                </tr>

            <?php endforeach; ?>
    </table>

<h3>Mail content</h3>

    <a href="<?php echo Yii::app()->createUrl('/hybridMail/hybridMail/getFile', array('urlData' => $model->hash));?>" target="_blank" class="btn btn-primary">Download file</a>

<h3>Address labels</h3>

    <a href="<?php echo Yii::app()->createUrl('/hybridMail/hybridMail/getAddressLabels', array('urlData' => $model->hash));?>" target="_blank" class="btn btn-primary">Download address labels</a>

<h3>Mail sender</h3>
<?php

$this->renderPartial('//_addressData/_listInTable',
    array('model' => $model->senderAddressData,
    ));

?>

    <h3>Receivers (<?php echo S_Useful::sizeof($model->hybridMailReceivers); ?>)</h3>
<?php $i = 1; ?>
<?php foreach($model->hybridMailReceivers AS $receiver):?>

    <h4>Receiver no <?php echo $i++; ?></h4>
    <?php

    $this->renderPartial('//_addressData/_listInTable',
        array('model' => $receiver->addressData,
        ));
    ?>

<?php endforeach; ?>


<h3>Change status</h3>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'change-status',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/hybridMail/hybridMail/changeStat'),
));
?>

<?php echo $form->hiddenField($model,'id'); ?>

<div class="form">

    <?php echo $form->errorSummary($model); ?>

    <table class="list hTop" style="width: 500px;">
        <tr>
            <td>Current status</td>
        </tr>
        <tr>
            <td>[<?php echo $model->stat;?>] <?php echo $model->stat0->name;?></td>
        </tr>
    </table>


    <table class="list hTop" style="width: 500px;">
        <tr>
            <td>New status</td>
        </tr>
        <tr>
            <td>
                <?php echo $form->dropDownList($model, 'stat', CHtml::listData(HybridMailStat::model()->findAll(), 'id', 'name')); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo CHtml::submitButton(Yii::t('app', 'Save'),
                    array(
                        'confirm'=>'Are you sure?'
                    ));
                $this->endWidget();
                ?>
            </td>
        </tr>
    </table>

</div><!-- form -->

<h3>History</h3>

<?php
$this->renderPartial('/_statHistory/_statHistory', array(
    'model' => $model->hybridMailStatHistories,
    'type' => 2,
));
?>