<?php

Yii::import('application.modules.courier.models._base.BaseCourierLabel');

class CourierLabel extends BaseCourierLabel
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
//            array('date_entered', 'default',
//                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('courier_label_new_id', 'required'),
            array('courier_label_new_id', 'numerical', 'integerOnly' => true),
//            array('source', 'length', 'max'=>32),
//            array('track_id', 'length', 'max'=>64),
//            array('id, date_entered, label, source, track_id', 'safe', 'on'=>'search'),
//            array('id,label, source, track_id', 'safe', 'on'=>'search'),
        );
    }


    public function fetchLabel($omitAdditionalPages = true)
    {
//
//        if($this->label_old !='')
//            return $this->label_old;

        return $this->courierLabelNew->getFileAsString(false, $omitAdditionalPages);

    }

    public function getTrack_id()
    {
//        if($this->track_id_old != '')
//            return $this->track_id_old;

        if($this->courierLabelNew !== NULL)
            return $this->courierLabelNew->track_id;

        return '';
    }


}

