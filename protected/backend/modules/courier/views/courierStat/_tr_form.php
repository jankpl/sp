<div class="form">
    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']short_text'); ?>
        <?php echo $form->textField($model, '['.$i.']short_text', array('maxlength' => 32)); ?>
        <?php echo $form->error($model,'['.$i.']short_text'); ?>
    </div><!-- row -->

    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']full_text'); ?>
        <?php echo $form->textField($model, '['.$i.']full_text', array('maxlength' => 512)); ?>
        <?php echo $form->error($model,'['.$i.']full_text'); ?>
    </div><!-- row -->


</div><!-- form -->