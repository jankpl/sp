<?php

class GlsNlClient
{
    const TEST_MODE = GLOBAL_CONFIG::GLSNL_TEST;


    const T8915 = GLOBAL_CONFIG::GLSNL_SAPID; // SAP ID
    const T8914 = GLOBAL_CONFIG::GLSNL_CONTACTID; // ContactID. Fixed value “5280000000” for the time being.

    const FTP_ADDRESS = GLOBAL_CONFIG::GLSNL_FTP_ADDRESS;
    const FTP_LOGIN = GLOBAL_CONFIG::GLSNL_FTP_LOGIN;
    const FTP_PASSWORD = GLOBAL_CONFIG::GLSNL_FTP_PASSWORD;


    const FTP_DIR = '/out';
    const T8700 = 'NL1000'; // Location Code Of The Inbound GLS Depot

    const SERVICE_BUSINESS = 'BP';
    const SERVICE_BUSINESS_EURO = 'EBP';

    public $sender_name = '';
    public $sender_company = '';
    public $sender_address_line_1 = '';
    public $sender_address_line_2 = '';
    public $sender_zip_code = '';
    public $sender_city = '';
    public $sender_country = '';
    public $sender_tel = '';

    public $receiver_name = '';
    public $receiver_company = '';
    public $receiver_address_line_1 = '';
    public $receiver_address_line_2 = '';
    public $receiver_zip_code = '';
    public $receiver_city = '';
    public $receiver_country = '';
    public $receiver_tel = '';

    public $pickup_name = '';
    public $pickup_company = '';
    public $pickup_address_line_1 = '';
    public $pickup_address_line_2 = '';
    public $pickup_zip_code = '';
    public $pickup_city = '';
    public $pickup_country = '';
    public $pickup_tel = '';

    public $reference;
    public $package_weight = '';
    public $package_no = 1;
    public $package_content;

    public $pickAndReturn = false;
    public $pickAndShip = false;

    public $modeEuro = false;

    protected static function getBaseAddress()
    {
        $address = new AddressData();
        $address->name = 'Orange Post NV';
        $address->address_line_1 = 'Westerdreef 5K';
        $address->zip_code = '2152 CS';
        $address->city = 'Nieuw-Vennep';
        $address->country0 = new CountryList();
        $address->country0->code = 'NL';

        return $address;
    }

    public static function createByCourierInternal(CourierTypeInternal $courier, AddressData $from, AddressData $to, $operator_id, $withCollect = false)
    {

        $client = new self;

//        if($courier->courier->senderAddressData->country_id == CountryList::COUNTRY_PL && $courier->courier->receiverAddressData->country_id != CountryList::COUNTRY_PL)
//        {
//            $from = self::getBaseAddress();
//        }
//        else if($courier->courier->senderAddressData->country_id != CountryList::COUNTRY_PL && $courier->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
//        {
//            $to = self::getBaseAddress();
//        }

        if($from->id != CourierHub::HUB_NL_ADDRESS_DATA_ID && $to->id != CourierHub::HUB_NL_ADDRESS_DATA_ID)
        {
            // DO NOT CALL COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
            if($withCollect && $courier->courier->user != NULL && !$courier->courier->user->getUpsCollectBlock() && $courier->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR)
                $client->pickAndShip = true;

        }
        else if($from->id != CourierHub::HUB_NL_ADDRESS_DATA_ID && $to->id == CourierHub::HUB_NL_ADDRESS_DATA_ID)
        {
            if($withCollect && $courier->courier->user != NULL && !$courier->courier->user->getUpsCollectBlock() && $courier->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR)
                $client->pickAndReturn = true;

        }

        if($operator_id == CourierOperator::OPERATOR_GLS_NL_EURO)
            $client->modeEuro = true;

        $client->reference = 'SP #'.$courier->courier->local_id;
        $client->package_weight = $courier->courier->getWeight(true);
        $client->package_no = $courier->courier->packages_number;
        $client->package_content = $courier->courier->package_content;

        $client->sender_name = $from->name;
        $client->sender_company = $from->company;
        $client->sender_address_line_1 = $from->address_line_1;
        $client->sender_address_line_2 = $from->address_line_2;
        $client->sender_zip_code = $from->zip_code;
        $client->sender_city = $from->city;
        $client->sender_country = $from->country0->code;
        $client->sender_tel = $from->tel;

        $client->receiver_name = $to->name;
        $client->receiver_company = $to->company;
        $client->receiver_address_line_1 = $to->address_line_1;
        $client->receiver_address_line_2 = $to->address_line_2;
        $client->receiver_zip_code = $to->zip_code;
        $client->receiver_city = $to->city;
        $client->receiver_country = $to->country0->code;
        $client->receiver_tel = $to->tel;

        return $client->orderPackage($courier->courier, $from, $to);
    }


    public static function createByCourierU(CourierTypeU $courierTypeU)
    {

        $client = new self;


        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;


        if($courierTypeU->collection)
            $client->pickAndShip = true;

        if($courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_GLSNL_EURO)
            $client->modeEuro = true;

        $client->reference = 'SP #'.$courierTypeU->courier->local_id;
        $client->package_weight = $courierTypeU->courier->package_weight;
        $client->package_no = $courierTypeU->courier->packages_number;
        $client->package_content = $courierTypeU->courier->package_content;

        $client->sender_name = $from->name;
        $client->sender_company = $from->company;
        $client->sender_address_line_1 = $from->address_line_1;
        $client->sender_address_line_2 = $from->address_line_2;
        $client->sender_zip_code = $from->zip_code;
        $client->sender_city = $from->city;
        $client->sender_country = $from->country0->code;
        $client->sender_tel = $from->tel;

        $client->receiver_name = $to->name;
        $client->receiver_company = $to->company;
        $client->receiver_address_line_1 = $to->address_line_1;
        $client->receiver_address_line_2 = $to->address_line_2;
        $client->receiver_zip_code = $to->zip_code;
        $client->receiver_city = $to->city;
        $client->receiver_country = $to->country0->code;
        $client->receiver_tel = $to->tel;

        return $client->orderPackage($courierTypeU->courier, $from, $to);
    }

    public static function updateTtData($justReturnData = false)
    {
        $CACHE_NAME = 'GLS_NL_UPDATE_TT_FTP';

//        file_put_contents('glsnl_tt_log.txt', "\r\n".date('Y-m-d H:i:s').' START'."\r\n", FILE_APPEND);

        MyDump::dump('glsnl_tt_log.txt', 'START');

        if(!$justReturnData) {
//            // prevent running mechanism too often
            $flag = Yii::app()->cache->get($CACHE_NAME);
            if ($flag) {
                MyDump::dump('glsnl_tt_log.txt', 'LOCKED!');
                return true;
            }
//
            Yii::app()->cache->set($CACHE_NAME, true, 60 * 15);
        }

        MyDump::dump('glsnl_tt_log.txt', ' FTP...');
//        file_put_contents('glsnl_tt_log.txt', date('Y-m-d H:i:s').' FTP...'."\r\n", FILE_APPEND);

        $conn_id = ftp_connect(self::FTP_ADDRESS);

        if(!ftp_login($conn_id, self::FTP_LOGIN, self::FTP_PASSWORD))
        {
            Yii::log('GLS_NL TT FTP LOGIN ERROR!', CLogger::LEVEL_ERROR);
            return false;
        }

        $contents = ftp_nlist($conn_id, self::FTP_DIR);

        if(!S_Useful::sizeof($contents))
        {
            Yii::log('GLS_NL TT FTP FILES NOT FOUND!', CLogger::LEVEL_ERROR);
            return false;
        }

        $ttData = [];
        if(is_array($contents))
            foreach($contents AS $file)
            {
                $array = explode('.', $file);
                $extension = end($array);

                $file = self::FTP_DIR.'/'.$file;

                if(strcasecmp($extension, 'xml') == 0)
                {
                    if(!TtFileProcessingLog::isProcessed(TtFileProcessingLog::GLS_NL, $file))
                    {
                        // new file found - lock for 3 hours
                        Yii::app()->cache->set($CACHE_NAME, true, 60 * 60 * 3);
//                        file_put_contents('glsnl_tt_log.txt', date('Y-m-d H:i:s').' FOUND NEW FILE: '.$file."\r\n", FILE_APPEND);
                        MyDump::dump('glsnl_tt_log.txt', ' FOUND NEW FILE: '.$file);

                        ob_start();
                        $result = ftp_get($conn_id, "php://output", $file, FTP_BINARY);
                        $data = ob_get_contents();
                        ob_end_clean();

                        $xml = simplexml_load_string($data);

                        if($xml instanceof SimpleXMLElement && isset($xml->Events) && isset($xml->Events->Event))
                            foreach($xml->Events->Event AS $event)
                            {
                                $ttNo = (string) $event->EventIdentification['ParcelNumber'];
                                $time = (string) $event['EventTimeStamp'];
                                $location = (string) $event->Location['Name'];
                                $country = (string) $event->Location->LocationCountry['Code'];
                                $status = (string) $event->EventReason['Description'];


                                if($status == 'The parcel was handed over to the consignee.')
                                {
                                    $byWho = (string) $event->EventReason['Info'];
                                    $location .= ' (by: '.$byWho.')';
                                }


                                $time = str_replace('T', ' ', $time);
                                $time = substr($time,0,19);

                                if(!is_array($ttData[$ttNo]))
                                    $ttData[$ttNo] = [];

                                $ttData[$ttNo][] = [
                                    'name' => $status,
                                    'date' => $time,
                                    'location' => $location.' ('.$country.')',
                                ];
                            }

                        if(!$justReturnData)
                            TtFileProcessingLog::markAsProcessed(TtFileProcessingLog::GLS_NL, $file);
                    }
                }
            }

//        file_put_contents('glsnl_tt_log.txt', date('Y-m-d H:i:s').' DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData)."\r\n", FILE_APPEND);
        MyDump::dump('glsnl_tt_log.txt', 'DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData));

        if($justReturnData)
            return $ttData;


        foreach($ttData AS $ttNumber => $ttDataForItem)
        {
//                        $searchForItem
            $couriers = CourierExternalManager::findCourierIdsByRemoteId($ttNumber, false, false, CourierLabelNew::OPERATOR_GLS_NL);


            /* @var $courier Courier */
            foreach($couriers AS $courier)
            {
                if($courier->isStatChangeByTtAllowed())
                {
                    // additional data
                    $temp = [];

                    // check if it's pickup label for internal and there is also delivery label
                    if($courier->getType() == Courier::TYPE_INTERNAL)
                    {
                        if($courier->courierTypeInternal->pickupLabel && $courier->courierTypeInternal->pickupLabel->getTrack_id() == $ttNumber && $courier->courierTypeInternal->delivery_operator_ordered)
                            $temp['convertDeliveryStatus'] = true;
                    }

                    CourierExternalManager::processUpdateDate($courier, $ttDataForItem, false, CourierExternalManager::TYPE_GLS_NL, $temp);
                }
            }
        }

    }

    public static function track($id)
    {

        // method only calls updating method which goes another way round - makes updates for all new stats found
        self::updateTtData();

        return false;
    }

    public static function calculateT620CN($number)
    {
        $number = (string) $number;

        $sum = 0;
        for($i = 0; $i < strlen($number); $i++)
        {
            $val = $number[$i];

            if($i%2 == 0)
                $val *= 3;

            $sum += $val;
        }

        $sum = $sum % 10;

        if($sum)
            $sum = 10 - $sum;

        return $sum;
    }

    protected static function convertNames($name, $company)
    {
        $name1 = $name2 = '';
        if($name == '')
            $name1 = $company;
        else if($company == '')
            $name1 = $name;
        else if($name == $company)
            $name1 = $name;
        else
        {
            $name1 = $name;
            $name2 = $company;
        }

        return [
            $name1,
            $name2,
        ];
    }

    public function createMessage($i = 1)
    {

        $senderNames = self::convertNames($this->sender_name, $this->sender_company);
        $receiverNames = self::convertNames($this->receiver_name, $this->receiver_company);

        $domesticParcelNumber = NumbersStorehouse::_getNumber(NumbersStorehouse::GLS_NL, true);
        $domesticParcelNumber .= self::calculateT620CN($domesticParcelNumber);


        $receiverAddress = trim($this->receiver_address_line_1.' '.$this->receiver_address_line_2);

        $hasNumber = filter_var($receiverAddress, FILTER_SANITIZE_NUMBER_INT);

        if(!strlen($hasNumber))
            throw new Exception('Please provide receiver house number!');


        if(preg_match('/(po\-box)|(pobox)|(postbus)|(postbox)/i', ($this->receiver_address_line_1.' '.$this->receiver_address_line_2), $match))
            throw new Exception('Cannot delivery to: '.$match[0]);

        $data = [
            'T530' => str_replace('.',',', $this->package_weight), // Package weight (with ','!)
            'T620' => $domesticParcelNumber, // Domestic Parcel Number NL
            'T853' => 'Ref. no:', // Description of ID number
            'T854' => $this->reference, // Reference number


            'T100' => $this->receiver_country, // Receiver country code
            'T330' => $this->receiver_zip_code, // Receiver zip code

            'T800' => 'Sender:', // Sender desciption
            'T810' => substr($senderNames[0], 0,30), // Sender name 1
            'T811' => substr($senderNames[1], 0,30), // OPTIONAL // Sender name 2
            'T820' => trim($this->sender_address_line_1.' '.$this->sender_address_line_2), // Sender street
            'T821' => $this->sender_country, // Sender country
            'T822' => $this->sender_zip_code, // Sender zip code
            'T823' => $this->sender_city, // Sender cit

            'T860' => substr($receiverNames[0], 0,30), // Receiver name 1
            'T861' => substr($receiverNames[1], 0,30), // OPTIONAL // Receiver name 2
            'T863' =>$receiverAddress, // Receiver street
            'T758' => $this->receiver_tel, // OPTIONAL // Receiver tel
            'T864' => $this->receiver_city, // Receiver city
            'T8700' => self::T8700, // Location Code Of The Inbound GLS Depot
            'T8914' => self::T8914, // ContactID.
            'T8915' => self::T8915, // SAP ID
            'T805' => substr($domesticParcelNumber,0,8), // GLS Customer Number

            'T922' => $this->package_content,
//            'T021' => 'zebrazpl', // OPTIONAL
//            'T050' => 'Label-Lite', // OPTIONAL
//            'T8904' => '001' // OPTIONAL // Parcel sequence
//            'T8905' => '001', // OPTIONAL // Parcel quantity

            'T206' => $this->modeEuro ? 'EBP' : 'BP', // Product code

            'T090' => 'NOPRINT', // OPTIONAL // We'll print own label
//
//            'T200' => 'QR',
//            'T207' => 'P&S',
//            'T900' => '', // IN return Services // Pickup Name 1
//            'T901' => '', // IN return Services // Pickup Name 2
//            'T902' => '', // IN return Services // Pickup Name 3
//            'T903' => '', // IN return Services // Pickup Address
//            'T904' => '', // IN return Services // Pickup Country
//            'T905' => '', // IN return Services // Pickup Zip Code
//            'T906' => '', // IN return Services // Pickup City
//            'T907' => '', // IN return Services // Pickup Tel
////            'T908' => '', // IN return Services // Pickup Date
////            'T909' => '', // IN return Services // Pickup Info

            'T8904' => $i,
            'T8905' => $this->package_no,
        ];

        if($this->pickAndReturn OR $this->pickAndShip)
        {
            $pickupNames = self::convertNames($this->pickup_name, $this->pickup_company);

            $data['T200'] = 'QR';
            $data['T207'] = $this->pickAndReturn ? 'P&R' : 'P&S';
            $data['T900'] = substr($pickupNames[0], 0,30);
            $data['T901'] = substr($pickupNames[1], 0,30);
            $data['T902'] = '';
            $data['T903'] = trim($this->pickup_address_line_1.' '.$this->pickup_address_line_2);
            $data['T904'] = $this->pickup_country;
            $data['T905'] = $this->pickup_zip_code;
            $data['T906'] = $this->pickup_city;
            $data['T907'] = $this->pickup_tel;
        }


        $messages = [];
        foreach($data AS $key => $item)
        {
            $item = str_replace('|','', $item);
            $item = str_replace(':','', $item);
            $messages[] = $key.':'.$item;
        }
        $message = implode('|', $messages);

        $message = '\\\\\\\\\GLS\\\\\\\\\\'.$message.'/////GLS/////';

        return $message;
    }

    public static function parseResponse($response)
    {
        $resp = false;
        if (preg_match('@\\\\\\\\\\\\\\\\\\\\GLS\\\\\\\\\\\\\\\\\\\\(.*)/////GLS/////@', $response, $found) === 1) {
            $resp = $found[1];

            $temp = explode('|', $resp);

            if(S_Useful::sizeof($temp)) {
                $resp = [];

                foreach ($temp AS $item) {
                    $item = explode(':', $item);

                    $key = $item[0];
                    unset($item[0]);

                    $value = implode(':', $item);

                    if($key == 'T8903')
                        $value = str_replace("\xAC","\x7c", $value);

                    $resp[$key] = $value;
                }
            }
        }

        return $resp;
    }

    public function orderPackage(Courier $courier, AddressData $from = NULL, AddressData $to = NULL)
    {

        $response = [];

        for($i = 1; $i <= $this->package_no; $i++) {
            $return = $this->go($i);


            if($return['success']) {

                $result = $return['response'];

//                $trackID = $result['T8913'];
                $trackID = $result['T8975'];

                $t8913 = $result['T8913'];
                $t400 = $result['T400'];

                $answ = [];
                $answ['track_id'] = $trackID;
                $answ['t8913'] = $t8913;
                $answ['t400'] = $t400;

                if ($this->pickAndReturn) {
                    // GENERATE KAAB LABEL IN CASE OF PICK & RETURN BECAUSE GLS IS WEIRD...

                    $courier->note = 'VIA GLS NL (' . $trackID . ')';

                    if ($from == NULL)
                        $from = $courier->senderAddressData;

                    if ($to == NULL)
                        $to = $courier->receiverAddressData;

                    $answ['label'] = KaabLabel::generate($courier, $i, $from, $to, false, false, false);
                } else {
                    // OTHERWISE GENERATE GLS NL LABEL
                    $answ['label'] = self::generateLabel($result);
                }

            } else {
                $answ = [
                    'error' => $return['error']
                ];
            }

            $response[] = $answ;
        }

        return $response;
    }

    protected function go($i = 1)
    {
        $return = [
            'success' => false,
            'response' => false,
        ];

        try {
            $message = $this->createMessage($i);
        }
        catch (Exception $ex)
        {
            $return['error'] = $ex->getMessage();
            return $return;
        }

        $response = '';
        $socket = socket_create(AF_INET,SOCK_STREAM,0);
        if(socket_connect($socket, 'unibox.gls-netherlands.com', self::TEST_MODE ? 3032 : 3033)) {
            socket_send($socket, $message, strlen($message), 0);

            for ($i = 0; $i < 10; $i++) {
                $read = socket_read($socket, 2048);
                $response = $response . $read;
                if (substr($read, -13) == '/////GLS/////') {
                    break;
                }
            }

            socket_close($socket);
        } else {
            $return['error'] = socket_last_error($socket);
            return $return;
        }

//        file_put_contents('glsnl_dump.txt', date('Y-m-d H:i:s').print_r($response,1)."\r\n\r\n", FILE_APPEND);
        MyDump::dump('glsnl_dump.txt', print_r($response,1));

        $response = self::parseResponse($response);

        MyDump::dump('glsnl_dump.txt', print_r($response,1));
//        file_put_contents('glsnl_dump.txt', date('Y-m-d H:i:s').print_r($response,1)."\r\n\r\n", FILE_APPEND);

        if(!is_array($response) OR (is_array($response) && !S_Useful::sizeof($response)))
        {
            $return['error'] = 'Error parsing response!';
            return $return;
        }

        $glsResult = $response['RESULT'];
        $glsResultCode = explode(':', $glsResult);
        $glsResultCode = $glsResultCode[0];


        if($glsResultCode == 'E000')
        {
            $return['success'] = true;
            $return['error'] = '';
            $return['response'] = $response;
        } else {

            if($glsResultCode == 'E013')
                $glsResult = 'E013:The error message E013 is displayed if a product code which is not permitted in the consignee’s country is transmitted.';

            $return['error'] = $glsResult;

            return $return;
        }

        return $return;
    }

    protected static $labelBg = 'iVBORw0KGgoAAAANSUhEUgAAAvIAAAR6AQMAAAATZgGAAAAABlBMVEUAAAD///+l2Z/dAAAGA0lEQVR4XuzZMREAIAwAsTrDv4tKqIMysXbjYMgLiIGPjKt97fP5PVUHWD1XfD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6f//if8jd7d2yCMBSEARhJYekIIi7gDi5iNkhpIVhYuFbkFVkj4AKWFoKWVoogPyH6/QN8zR2Pgwd392haPv+T8Pl8Pp/P5/P5fD6fz+fz+Xw+n8/n8/l8/vD/7/zXxZl8Xd+hff474BTw+Xw+n28+5PP5fD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+Xw+n8/n8/mj26/C5/P5fPsP+dH8k+++Kp9/2/+2z7/to/62WWb9TRX1mz7r1+UQ9dddifpV2J9251H71cj9Rdivw/3TpP2S9bd1Ner3c7eK+M9cl1n/XrJ+12X9csz67TTr97Osf5nH53M+n8/nDxt5sHOHRgDAIBAES6Ak+u+JmeAjot5lTyJWob9Orun78KnP5/P5fD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+fxs/Hf+k8/n8/l8Pp/P5/P5fD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+Xw+n28fzD5YKD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+Xw+n8/n8/l8Pp/P5/P5fD6fz+fz+Xz+tmPHLHIbURzA//KYlQvjaVUsNyF1CHInwxEZ8ilSBEIOnCbFGhc5QxLNeeGuCDhtihTGafIRXMq+4IUU9leY5YqkSSIIJCqElB1pxlqxsCPduzEJ5HGw3C78dua9+c+yi39R/V/NVVc99Llnn3n2kXr2uWcfmWdfePZDDz69QdzhEydcCYdPzPDa0R9qBM4dPjHDtZjig/ap5fZDUvvdfuDBJ2X4bKIfXiZdEnZtBTZ/mk6NTzyilRj4NRCojZ+DG5+Y4TIb+CUA7cvep11CRTPwc73+QMvGp054PfRl51e9T8zY89aH8Wt0fml98oSXA78yfgEI4xMzdrbtp6XxlfXJEw63g5QWOz7xiNZ84Cvt50GT7/cx3hc7PsuZ9tN9vricn+XWx6HxiRsoh74Er5nc+IF9mnhEy3ToI22WY3xczm83LsNGMpefTvErA3yNwPj9+mkZK2xUVOeHxofLR0rzM+MTN6Csn2ugAre+cPlIJ/my96F97vSDSX4LlBCdn4MZn7gBlW75gfbBwcf5mOBX1q9xq/UxwhcjfDH0085Xo3xko/2i9w8g9L/ZCJ+P9tUenzLiIux8af2q9SsgHePzsT6Mn3V+PdKH02ctX2PoN2N95vDLoHsAcAPsTX+a3PrEI1Qis+ONjD/XvtryKbdEBWHHG2tfVDjUdNH7pJDVCG37Fwh3fWKHNGFPf6F9XiBx+NO+0ACia0/Q+qH1Tb7IHZIWCFsfwDFwwGB96ohzC/BtH4P7jRKCwgLplj8f+pRrqLRAphBWOz75DawRNAq8Nv4hELwdnz4BAzDrY9H6zPjkEav+qylvjJ/AxpreocK+rn0JoGh9PtEH3zsAppOg99D6x0A61cfehPHOV0CgHP7Ui64249F+BYjWZ4316TMugNT4jUTT+nzo0y46tfOCapw++benof8OaQduPx45A88+uGcfyDz7mOTfmt+LPr+fN3HBqo+qxesyvIjgqgn+9eRe8vsd+XMsWVI+W6yOrx/N6T3q/dkHn3318qFc3ZazaH26eJXcPLpJjMLQf//T904fyhdx/m50MVu8jMSDOX0MvR/+df7b6ceyjv/8pTyabdZ/8ENEz3Pvs6fLPx59I8s4YWqz/p+iOZtjQjn91cnq7Nsoj2OG598/eHXnMfsOV1jBxclq+ThRcXwN6x+/WN1fffjllfrnJ6vgNGpuK1Z9Uhy8Xi/Xv+Lq6xy4Fuk4wEsFG38213GGn3oBzG4AmHny/wbCY4/rTwD2xEP//dZdyP+4n/vkgxzKs1/69MP8H0+u2Jaxy0NqAAAAAElFTkSuQmCC';

    public static function generateLabel(array $response, $asImageBlob = true)
    {

        $t110 = isset($response['T110']) ? $response['T110'] : '';
        $t310 = isset($response['T310']) ? $response['T310'] : '';
        $t100 = isset($response['T100']) ? $response['T100'] : '';
        $t101 = isset($response['T101']) ? $response['T101'] : '';
        $t8952 = isset($response['T8952']) ? $response['T8952'] : '';
        $t200 = isset($response['T200']) ? $response['T200'] : '';
        $t201 = isset($response['T201']) ? $response['T201'] : '';
        $t202 = isset($response['T202']) ? $response['T202'] : '';
        $t203 = isset($response['T203']) ? $response['T203'] : '';
        $t204 = isset($response['T204']) ? $response['T204'] : '';
        $t205 = isset($response['T205']) ? $response['T205'] : '';
        $t8913 = isset($response['T8913']) ? $response['T8913'] : '';
        $t400 = isset($response['T400']) ? $response['T400'] : '';
        $t8903 = isset($response['T8903']) ? $response['T8903'] : '';
        $e2 = 'B2.00.1';
        $t520 = isset($response['T520']) ? $response['T520'] : '';
        $t8904 = isset($response['T8904']) ? $response['T8904'] : '';
        $t8905 = isset($response['T8905']) ? $response['T8905'] : '';
        $t530 = isset($response['T530']) ? $response['T530'] : '';
        $t541 = isset($response['T541']) ? $response['T541'] : '';;
        $t540 = isset($response['T540']) ? $response['T540'] : '';
        $t510 = isset($response['T510']) ? $response['T510'] : '';
        $t500 = isset($response['T500']) ? $response['T500'] : '';;
        $t8902 = isset($response['T8902']) ? $response['T8902'] : '';
        $t330 = isset($response['T330']) ? $response['T330'] : '';
        $t8951 = isset($response['T8951']) ? $response['T8951'] : '';;
        $t320 = isset($response['T320']) ? $response['T320'] : '';;
        $t750 = isset($response['T750']) ? $response['T750'] : '';;
        $t751 = isset($response['T751']) ? $response['T751'] : '';;
        $t752 = isset($response['T752']) ? $response['T752'] : '';
        $t753 = isset($response['T753']) ? $response['T753'] : '';
        $t860 = isset($response['T860']) ? $response['T860'] : '';
        $t861 = isset($response['T861']) ? $response['T861'] : '';
        $t862 = isset($response['T862']) ? $response['T862'] : '';
        $t863 = isset($response['T863']) ? $response['T863'] : '';
        $t864 = isset($response['T864']) ? $response['T864'] : '';
        $t8958 = isset($response['T8958']) ? $response['T8958'] : '';
        $t8959 = isset($response['T8959']) ? $response['T8959'] : '';
        $t8960 = isset($response['T8960']) ? $response['T8960'] : '';
        $t853 = isset($response['T853']) ? $response['T853'] : '';
        $t759 = isset($response['T759']) ? $response['T795'] : '';
        $t758 = isset($response['T758']) ? $response['T758'] : '';
        $t922 = isset($response['T922']) ? $response['T922'] : '';
        $t923 = isset($response['T923']) ? $response['T923'] : '';
        $t854 = isset($response['T854']) ? $response['T854'] : '';
//        $t8965 = isset($response['T8965']) ? $response['T8965'] : '';
//        $t8914 = isset($response['T8914']) ? $response['T8914'] : '';
        $t620 = isset($response['T620']) ? $response['T620'] : '';
        $t8957 = isset($response['T8957']) ? $response['T8957'] : '';
        $t8915 = isset($response['T8915']) ? $response['T8915'] : '';
        $t800 = isset($response['T800']) ? $response['T800'] : '';
        $t810 = isset($response['T810']) ? $response['T810'] : '';
        $t811 = isset($response['T811']) ? $response['T811'] : '';
        $t812 = isset($response['T812']) ? $response['T812'] : '';
        $t820 = isset($response['T820']) ? $response['T820'] : '';
        $t821 = isset($response['T821']) ? $response['T821'] : '';
        $t822 = isset($response['T822']) ? $response['T822'] : '';
        $t823 = isset($response['T823']) ? $response['T823'] : '';

        $t805 = isset($response['T805']) ? $response['T805'] : '';

        $pageW = 100;
        $pageH = 150;

        $margins = 1;

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $yBase = 0;
        $xBase = 0;

        $border = 0;

        // GENERATE OWN:
        $pdf = PDFGenerator::initialize();
        $pdf->addPage('H', array($pageW, $pageH));
        $pdf->setFontSubsetting(true);

        $pdf->Image('@' . base64_decode(self::$labelBg), $xBase + $margins, $yBase + $margins, $blockWidth, $blockHeight, '', 'N', false);

        $pdf->SetFont('freesans', '', 8);


        //T110
        $pdf->SetFont('freesans', 'B', 25);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(30, 12, $t110, $border, 'L', false, 1, 0, 2,
            true,
            0,
            false,
            true,
            12,
            'M',
            true
        );

        //T310
        $pdf->SetFont('freesans', 'B', 25);
        $pdf->SetTextColor(255, 255, 255);

        $pdf->MultiCell(12, 12, $t310, $border, 'C', false, 1, 30, 2,
            true,
            0,
            false,
            true,
            12,
            'M',
            true
        );

        //T100
        $pdf->SetFont('freesans', 'B', 25);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(30, 12, $t100, $border, 'R', false, 1, 43, 2,
            true,
            0,
            false,
            true,
            12,
            'M',
            true
        );

        //T101
        $pdf->SetFont('freesans', 'B', 25);
        $pdf->SetTextColor(255, 255, 255);

        $pdf->MultiCell(30, 12, $t101, $border, 'L', false, 1, 75, 2,
            true,
            0,
            false,
            true,
            12,
            'M',
            true
        );

        //T320
        $pdf->SetFont('freesans', '', 20);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(30, 12, $t320, $border, 'L', false, 1, 0, 14,
            true,
            0,
            false,
            true,
            12,
            'M',
            true
        );

        //T8951
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(30, 5, $t8951, $border, 'L', false, 1, 24, 11.5,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );


        //T330
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(30, 5, $t330, $border, 'L', false, 1, 24, 18,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T8952
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(30, 5, $t8952, $border, 'L', false, 1, 44, 11.5,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T8913
        $pdf->SetFont('freesans', '', 10);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(30, 5, $t8913, $border, 'L', false, 1, 44, 18,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T200-T205
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(10, 12, $t200.$t201.$t202.$t203.$t204.$t205, $border, 'R', false, 1, 88, 12,
            true,
            0,
            false,
            true,
            12,
            'M',
            true
        );



        //T8902
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetTextColor(0, 0, 0);

        $style2d = array(
            'border' => 0,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );
        $pdf->write2DBarcode($t8902, 'DATAMATRIX', 4, 25, 26, 26, $style2d, 'N');



        //T400
        $style1d = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        if(is_numeric($t400)) {

            $pdf->write1DBarcode($t400, 'I25', 32, 25.5, 40, 22, 0.4, $style1d, 'N');

            $pdf->SetFont('freesans', '', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->MultiCell(34.5, 5, $t400, $border, 'C', false, 1, 35, 47,
                true,
                0,
                false,
                true,
                5,
                'M',
                true
            );
        }


        //T8903
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetTextColor(0, 0, 0);

        $style = array(
            'border' => 0,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );
        $pdf->write2DBarcode($t8903, 'DATAMATRIX', 70, 25, 26, 26, $style2d, 'N');


        //T500
        $pdf->SetFont('freesans', '', 9);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(13, 5, $t500, $border, 'L', false, 1, 1, 53,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T510
        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(6, 5, $t510, $border, 'C', false, 1, 14, 53,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T540
        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(15, 5, $t540, $border, 'L', false, 1, 20, 53,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T541
        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(7, 5, $t541, $border, 'L', false, 1, 35, 53,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T2530
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(11, 5, $t530, $border, 'R', false, 1, 42, 53,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T8904/T8905
        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(11, 5, $t8904.'/'.$t8905, $border, 'L', false, 1, 57, 53,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T520
        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(20, 5, 'RTG '.$t520, $border, 'L', false, 1, 68, 53,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //E2
        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(10, 5, $e2, $border, 'R', false, 1, 90, 53,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );


        $pdf->StartTransform();
        $pdf->Rotate(-90);

        //T805
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(15, 3, $t805, $border, 'R', false, 1, 30, -40.5,
            true,
            0,
            false,
            true,
            3,
            'M',
            true
        );


        //T8957
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(15, 3, $t8957.':', $border, 'R', false, 1, 50, -40.5,
            true,
            0,
            false,
            true,
            3,
            'M',
            true
        );


        //T8915
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(15, 3, $t8915, $border, 'L', false, 1, 65, -40.5,
            true,
            0,
            false,
            true,
            3,
            'M',
            true
        );


//        //T8965
//        $pdf->SetFont('freesans', '', 5);
//        $pdf->SetTextColor(0, 0, 0);
//
//        $pdf->MultiCell(15, 3, $t8965, $border, 'R', false, 1, 50, -40.5,
//            true,
//            0,
//            false,
//            true,
//            3,
//            'M',
//            true
//        );
//
//
//        //T8914
//        $pdf->SetFont('freesans', '', 5);
//        $pdf->SetTextColor(0, 0, 0);
//
//        $pdf->MultiCell(15, 3, $t8914, $border, 'L', false, 1, 65, -40.5,
//            true,
//            0,
//            false,
//            true,
//            3,
//            'M',
//            true
//        );

        //T800
        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(25, 3, $t800, $border, 'L', false, 1, 2, -40.5,
            true,
            0,
            false,
            true,
            3,
            'M',
            true
        );

        //T810
        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(38, 4, $t810, $border, 'L', false, 1, 2, -38,
            true,
            0,
            false,
            true,
            4,
            'M',
            true
        );

        //T811
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(38, 3, $t811, $border, 'L', false, 1, 2, -35,
            true,
            0,
            false,
            true,
            3,
            'T',
            true
        );

        //T812
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(38, 3, $t812, $border, 'L', false, 1, 2, -33,
            true,
            0,
            false,
            true,
            3,
            'T',
            true
        );

        //T820
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(38, 3, $t820, $border, 'L', false, 1, 2, -30,
            true,
            0,
            false,
            true,
            3,
            'M',
            true
        );


        //T821
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(6, 3, $t821, $border, 'L', false, 1, 2, -26,
            true,
            0,
            false,
            true,
            3,
            'M',
            true
        );

        //T822
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(10, 3, $t822, $border, 'L', false, 1, 7, -26,
            true,
            0,
            false,
            true,
            3,
            'M',
            true
        );

        //T823
        $pdf->SetFont('freesans', '', 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(23, 4, $t823, $border, 'L', false, 1, 20, -27,
            true,
            0,
            false,
            true,
            4,
            'M',
            true
        );

        // SPECIAL ORANGE POST LABEL:
        $pdf->write1DBarcode('OP'.$t620, 'c128', 39, -38, 40, 12, 0.4, $style1d, 'N');

        $pdf->SetFont('freesans', '', 7);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(34.5, 5, 'OP'.$t620, $border, 'C', false, 1, 43, -27.5,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        $pdf->StopTransform();


        //T750
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(80, 5, $t750, $border, 'L', false, 1, 1, 60,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T751
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(80, 5, $t751, $border, 'L', false, 1, 1, 64,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T752
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(80, 5, $t752, $border, 'L', false, 1, 1, 68,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T753
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(80, 5, $t753, $border, 'L', false, 1, 1, 72,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );


        //T860
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(80, 5, $t860, $border, 'L', false, 1, 1, 91,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T861
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(80, 5, $t861, $border, 'L', false, 1, 1, 96,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T862
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(80, 5, $t862, $border, 'L', false, 1, 1, 100,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );


        //T863
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(80, 5, $t863, $border, 'L', false, 1, 1, 106,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T100
        $pdf->SetFont('freesans', '', 10);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(8, 5, $t100, $border, 'L', false, 1, 1, 111,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T330
        $pdf->SetFont('freesans', '', 10);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(20, 5, $t330, $border, 'L', false, 1, 10, 111,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );


        //T864
        $pdf->SetFont('freesans', '', 10);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->MultiCell(45, 5, $t864, $border, 'L', false, 1, 35, 111,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );


        //T620

        $t620_withoutChecksum = substr($t620,0, strlen($t620) - 1);

        $pdf->write1DBarcode($t620_withoutChecksum, 'I25+', 30, 76, 50, 12, 0.4, $style1d, 'N');

        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(34.5, 5, $t620, $border, 'C', false, 1, 38, 85.5,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T8958
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(20, 5, $t8958, $border, 'L', false, 1, 1, 116,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T759
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(60, 5, $t759, $border, 'L', false, 1, 21, 116,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );


        //T8959
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(20, 5, $t8959, $border, 'L', false, 1, 1, 120,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T758
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(60, 5, $t758, $border, 'L', false, 1, 21, 120,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );


        //T8960
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(20, 5, $t8960, $border, 'L', false, 1, 1, 124,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T922
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(60, 5, $t922, $border, 'L', false, 1, 21, 124,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T8960
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(20, 5, $t8960, $border, 'L', false, 1, 1, 128,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T923
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(60, 5, $t923, $border, 'L', false, 1, 21, 128,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T853
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(20, 5, $t853, $border, 'L', false, 1, 1, 132,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        //T854
        $pdf->SetFont('freesans', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->MultiCell(60, 5, $t854, $border, 'L', false, 1, 21, 132,
            true,
            0,
            false,
            true,
            5,
            'M',
            true
        );

        $pdfString = $pdf->Output('asString', 'S');

        if($asImageBlob) {
            $img = ImagickMine::newInstance();
            $img->setResolution(300, 300);
            $img->readImageBlob($pdfString);
            $img->setImageFormat('png8');
            $img->stripImage();
            $image = $img->getImageBlob();
        } else
            $image = $pdfString;

        return $image;
    }


}