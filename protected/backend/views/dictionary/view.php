<?php


$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
);
?>

    <h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

    <h3>Podsumowania</h3>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'source_text',
        'date_entered',
        'date_updated',
        'admin.login',
        'typeName',
        'sourceIdName',
    ),
)); ?>

    <h3>Wersje językowe</h3>

<?php
if(!sizeof($model->dictionaryTrs))
    echo '-';
else
    foreach($model->dictionaryTrs AS $item):

        echo '<h5>'.$item->language->name.'</h5>';

        $this->widget('zii.widgets.CDetailView', array(
            'data' => $item,
            'attributes' => array(
                'target_text',

            ),
        ));


    endforeach;
?>