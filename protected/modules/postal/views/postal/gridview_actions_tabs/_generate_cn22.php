<?php
/* @var $id_prefix string */
?>
    <br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-kn',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/cn22Multi'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('postal', 'Generate CN22'), array(
    'onClick' => 'js:
    $("#'.$id_prefix.'postal-ids-cd22").val($("#postalgrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => $id_prefix.'postal-ids-cd22')); ?>
<?php
$this->endWidget();
?>