<?php

class NumbersStorehouse
{
    const GLS_NL = 1;
    const GLS_NL_WITH_COD = 2;
    const HUNGARIAN_POST = 3;
    const CORRECTION_NOTE = 4;
    const POST11 = 5;
    const POST11_AIRWAY_DOCUMENT = 6;
    const SWEEDEN_POST_CA_AU = 7;
    const SWEEDEN_POST = 8;

    const ANPOST_CA = 9;
    const ANPOST_CE = 10;
    const ANPOST_LX = 11;

    const POSTAL_NONREG_CZECH_POST = 12;
    const POSTAL_CZECH_POST_RB = 13;

    public static function _getNumber($what, $useNumber = false, $hasOwnTransaction = false)
    {

        /* @var $cmd CDbCommand */

        if($useNumber && !$hasOwnTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $sql = 'SELECT * FROM numbers_storehouse WHERE what = :what';

        if($useNumber)
            $sql .= ' FOR UPDATE';

        $cmd = Yii::app()->db->createCommand($sql);
        $cmd->bindParam(':what', $what);
        $result = $cmd->queryRow();

        $current = trim($result['current']);
        $prefix = trim($result['prefix']);
        $end = trim($result['end']);



        if($current > $end) {
            if($useNumber && !$hasOwnTransaction)
                $transaction->rollback();
            throw new Exception('No more numbers is storehouse for: ' . $what . '!');
        }

        if($useNumber)
        {
            $next = $current + 1;
            $sql = 'UPDATE numbers_storehouse SET current = :current WHERE what = :what';

            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindParam(':what', $what);
            $cmd->bindParam(':current', $next);
            $cmd->execute();
        }

        if($useNumber && !$hasOwnTransaction)
            $transaction->commit();

        $fixedLength = strlen($end);

        $current = str_pad($current, $fixedLength, "0", STR_PAD_LEFT);

        $number = $prefix.$current;

        return $number;
    }

    public static function calculateControlDigitS10($base)
    {
        $factors = [8,6,4,2,3,5,9,7];

        $sum = 0;
        for ($i = 0; $i < strlen($base); $i++) {
            $num = $base[$i];
            $sum += $factors[$i] * $num;
        }

        $result = intval($sum / 11);

        $reminder = $sum - ($result * 11);

        if($reminder == 0)
            $number = 5;
        else if($reminder == 1)
            $number = 0;
        else
            $number = 11 - $reminder;

        return $number;
    }
}