<?php

class FaqController extends Controller {

    public $layout = 'other';

    public function init()
    {
        parent::init();

        $this->layout = 'other';
    }

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function actionIndex() {

        $models = Faq::model()->with('faqTrs')->findAll(array('condition' => 't.stat = :stat AND faqTrs.stat = :stat AND language_id = :language_id', 'params' => array(':stat' => S_Status::ACTIVE, ':language_id' => Yii::app()->params['language']), 'order' => 'pos'));

		$this->render('index', array(
			'models' => $models,
		));
	}

}