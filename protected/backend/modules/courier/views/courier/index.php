<?php

$this->breadcrumbs = array(
    Courier::label(2),
    Yii::t('app', 'Index'),
);


?>

    <h1><?php echo GxHtml::encode(Courier::label(2)); ?></h1>

<?php echo CHtml::link('manage',array('/courier/courier/admin'), array('class' => 'btn btn-large')); ?>

<?php //echo CHtml::link('create',array('/courier/courier/createExternal'), array('class' => 'btn btn-large')); ?>

<?php //echo CHtml::link('scanner',array('courier/fastAssignCarrier'), array('class' => 'btn btn-large')); ?>

<?php echo CHtml::link('import packages from file',array('/courier/importCourierExternalFromFile'), array('class' => 'btn btn-large')); ?>

<?php echo CHtml::link('update statuses from file',array('/courier/importCourierStatFromFile'), array('class' => 'btn btn-large')); ?>

<?php echo CHtml::link('create type external return',array('/courier/courierTypeExternalReturn/create'), array('class' => 'btn btn-large')); ?>

    <br/>
    <br/>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>


<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/searchByRemote'),
));
?>
    <h3>Search for package by local/ref/ext ID:</h3>
<?php
$periods = [];
$time = time();
for($i = 0; $i <= 36; $i ++)
{
    $temp = date("m.Y", strtotime("-$i month", $time));
    $temp2 = str_replace('.','', $temp);
    $periods[$temp2] = $temp;
}

echo Chtml::textField('remoteId', '', ['placeholder' => 'Remote ID', 'style' => 'height: 30px; margin-right: 10px;']);
echo TbHtml::submitButton('Search', ['style' => 'margin-top: -10px;']);
$this->endWidget();
?>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/exportToFileAll', ['nm' => isset($_GET['nm'])]),
));
?>
    <h3>Export all to file:</h3>
<?php
$periods = [];
$time = time();
$year = date('Y');
$month = date('m');

for($i = 0; $i <= 36; $i ++)
{

    if($i) {
        $month--;
        if (!$month) {
            $year--;
            $month = 12;
        }
    }

    $month = str_pad($month,2,'0',STR_PAD_LEFT);

    $temp = $month.'.'.$year;

    $temp2 = str_replace('.','', $temp);
//    $periods[$temp2.'_4'] = $temp.' (4th part)';
//    $periods[$temp2.'_3'] = $temp.' (3rd part)';
//    $periods[$temp2.'_2'] = $temp.' (2nd part)';
//    $periods[$temp2.'_1'] = $temp.' (1st part)';
    $periods[$temp2] = $temp;
}

echo Chtml::dropDownList('period', '', $periods, ['placeholder' => 'Remote ID', 'style' => 'height: 30px; margin-right: 10px;']);
echo TbHtml::submitButton('Export to .xls', ['style' => 'margin-top: -10px;']);
$this->endWidget();
?>