<?php

if($listAddressData && S_Useful::sizeof($listAddressData)):
    ?>

    <?php echo CHtml::dropDownList('listAddressData','', CHtml::listData($listAddressData,'id','usefulName'), array('id' => 'addressDataId'.($i!=''?'_'.$i:''),'empty' => '--select address--'));?>


<br/>
    <?php echo CHtml::button('Pobierz dane adresowe', array('id' => 'getAddressData'.($i!=''?'_'.$i:'')));?>

    <script>
        $(document).ready(function(){

            $("#getAddressData<?php echo ($i!=''?'_'.$i:'');?>").click(function(){

                <?php echo CHtml::ajax(array(
                        'dataType' => 'JSON',
                        'data' => 'js:{id : $("#addressDataId'.($i!=''?'_'.$i:'').'").val()}',
                        'url' => Yii::app()->createUrl('addressData/ajaxGetAddressData'),
                        'type' => 'POST',
                        'success' => 'function(result) {
                      $.each(result, function(i,v)
                {
                    $("#AddressData_'.($i!=''?$i.'_':'').'" + i).val(v);
                    $("#AddressData_'.($i!=''?$i.'_':'').'" + i).trigger("change");
                });

                }',
                'error' => 'function(e){
                console.log(e);
                }'

                    ));
                ?>
            });

        });
    </script>


<?php

endif;
$array = [];

?>


    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'name'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'name', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'name'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'company'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'company', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'company'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'country_id'); ?>
        <?php echo $form->dropDownList($model,(isset($i)?'['.$i.']':'').'country_id', CHtml::listData(CountryList::getActiveCountries(),'id','translatedName','firstLetter'), array('readOnly' => $readOnly, 'disabled' => $disabled, 'empty'=>'--Select a country--')); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'country_id'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'zip_code', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'city'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'city', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'city'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_1', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_2', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'tel'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'tel', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'tel'); ?>
    </div><!-- row -->
<?php
if(!$noEmail):
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'email'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'email', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'email'); ?>
    </div><!-- row -->
<?php
endif;
?>