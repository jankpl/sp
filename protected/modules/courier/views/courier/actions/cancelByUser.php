<div class="alert alert-danger text-center">
    <h4><?php echo Yii::t('courier','Anuluj paczkę');?></h4>
    <?= Yii::t('courier', 'Przesyłki mogą być anulowanie w ciągu {n} dni.', ['{n}' => '<strong>'.Courier::PACKAGE_CANCEL_DAYS.'</strong>']);?>
    <br/>
    <?= Yii::t('courier', 'Pozostało: {n}', ['{n}' => '<strong>'.$model->countTimeLeftForCancel().'</strong>']);?>
    <br/>
    <br/>
    <?php echo CHtml::link(Yii::t('courier','Anuluj'), array('/courier/courier/cancelByUser', 'urlData' => $model->hash), array('class' => 'btn', 'onclick' => 'return confirm("'.Yii::t('site', "Czy jesteś pewien?").'")'));?>

</div>
