<?php

/* @var $model UserGroup[] */

?>

<h2>Prices for groups</h2>


<table class="list hTop" style="width: 500px;">
    <tr>
        <td>Group name</td>
        <td>Members</td>
        <td>Own prices</td>
        <td>Action</td>
    </tr>

    <?php
    foreach($model AS $item):
    ?>
    <tr>
        <td><?php echo CHtml::link($item->name,Yii::app()->createUrl('/userGroup/view', array('id' => $item->id))); ?></td>
        <td><?php echo S_Useful::sizeof($item->users);?></td>
        <td><?php echo S_Useful::sizeof($item->courierPrices);?></td>
        <td><?php echo CHtml::link('edit',Yii::app()->createUrl('/courier/courierPrice/index/', array('id' => $item->id)), array('class' => 'likeButton')); ?></td>
    </tr>

    <?php
    endforeach;
    ?>



    </table>
