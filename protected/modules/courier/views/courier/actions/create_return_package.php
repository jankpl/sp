<div class="alert alert-success text-center">
    <h4><?php echo Yii::t('courier','Paczka zwrotna');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t("courier", "Wygeneruj etykietę zwrotną dla tej paczki"),array("/courier/courierReturn/createFor", "hash" => $model->hash), ["target" => "_blank", 'class' => 'btn', "onclick" => "return confirm(\"".Yii::t("site", "Czy jesteś pewien?")."\")"]);?>
</div>
