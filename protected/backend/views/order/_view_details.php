<table class="list hLeft" style="width: 550px;">
    <tr>
        <td>User</td>
        <td><?php echo $model->user_id !== null ? CHtml::link($model->user->login, array('user/view', 'id' => $model->user_id)) : '-';?></td>
    </tr>
    <tr>
        <td>Creation date</td>
        <td><?php echo substr($model->date_entered,0,16);?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td>[<?php echo $model->order_stat_id;?>] <?php echo $model->orderStat->name;?></td>
    </tr>
    <tr>
        <td>Product</td>
        <td><?php echo Order::getProductName($model->product_type) ?></td>
    </tr>
    <tr>
        <td>Payment type</td>
        <td><?php echo $model->paymentType !== null ? $model->paymentType->name:'-';?></td>
    </tr>
    <tr>
        <td>Total payment amount (netto/brutto)</td>
        <td><?php echo S_Price::formatPrice($model->value_netto);?> / <?php echo S_Price::formatPrice(($model->value_brutto));?> <?php echo $model->currency;?></td>
    </tr>

</table>


<h3>Price details</h3>
<table class="list hLeft" style="width: 500px;">
    <tr>
        <td>Discount</td>
        <td><?php echo S_Price::formatPrice($model->discount_value);?> <?php echo $model->currency;?></td>
    </tr>
    <tr>
        <td>Payment commission</td>
        <td><?php echo S_Price::formatPrice($model->payment_commision_value);?> <?php echo $model->currency;?></td>
    </tr>
</table>