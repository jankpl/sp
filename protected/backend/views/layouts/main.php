<?php Yii::app()->bootstrap->register(); ?>

<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/backend/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/backend/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/backend/preloader.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="robots" content="noindex, nofollow" />
    <link href="https://fonts.googleapis.com/css?family=Oswald:700&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
	<?php echo $content; ?>
	<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/verify-login-status.js', CClientScript::POS_END);
	?>
<?php
if(!YII_LOCAL):
    ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-52271630-1', 'swiatprzesylek.pl');
        ga('send', 'pageview');
    </script>
<?php
endif;
?>
</body>
</html>
