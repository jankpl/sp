<?php

class DaApiController extends Controller {

    const DOMAIN_SP = 1;
    const DOMAIN_KAAB = 2;
    const DOMAIN_CQL = 3;
    const DOMAIN_FB = 4;

    public static function domainIdToEmailDomain($domain_id)
    {
        switch($domain_id)
        {
            case self::DOMAIN_SP:
                return 'swiatprzesylek.pl';
                break;
            case self::DOMAIN_KAAB:
                return 'kaabpl.pl';
                break;
            case self::DOMAIN_CQL:
                return 'cql.global';
                break;
            case self::DOMAIN_FB:
                return 'fire-business.com.pl';
                break;
        }
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
//                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionVacation($domain, $account)
    {

        if(!in_array($domain, [self::DOMAIN_SP, self::DOMAIN_KAAB, self::DOMAIN_CQL, self::DOMAIN_FB]))
            throw new CHttpException(403);

        $domain = intval($domain);
        $account = strip_tags($account);

        if ($domain == self::DOMAIN_SP) {
            $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
            $domainContext = $userContext->getDomain('swiatprzesylek.pl');

        }
        else if ($domain == self::DOMAIN_CQL) {
            $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
            $domainContext = $userContext->getDomain('cql.global');
        }
        else if ($domain == self::DOMAIN_KAAB) {
            $userContext = DirectAdminMine::connectUser('https://da02.vipanel.pl:2222', 'kaabplxc', 'MfkWUJsD');
            $domainContext = $userContext->getDomain('kaabpl.pl');
        }
        else if ($domain == self::DOMAIN_FB) {
            $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
            $domainContext = $userContext->getDomain('fire-business.com.pl');
        }

        $currentVM = $domainContext->invokePost('EMAIL_VACATION_MODIFY', '', ['user' => $account]);

        $is_active = false;
        if($currentVM['text'])
        {
            $is_active = true;
            $title = $currentVM['reply_subject'];
            $text = $currentVM['text'];
            $start = $currentVM['startyear'].'-'.$currentVM['startmonth'].'-'.$currentVM['startday'];
            $end = $currentVM['endyear'].'-'.$currentVM['endmonth'].'-'.$currentVM['endday'];

            $text = html_entity_decode($text);
            $title = html_entity_decode($title);
        }

        $catched = false;
        $validated = true;

        if(isset($_POST)) {
            if (isset($_POST['delete'])) {
                $password = $_POST['password'];

                if(!AdminRestrictions::isUltraAdmin()) {
                    try {
                        $resp = $domainContext->invokePost('EMAIL_AUTH', 'POST', ['email' => $account . '@' . self::domainIdToEmailDomain($domain), 'passwd' => $password]);
                    } catch (Exception $ex) {
                        $validated = false;
                        $catched = true;
                        Yii::app()->user->setFlash('error', $ex->getMessage());
                    }
                }

                if ($validated) {
                    try {
                        $resp = $domainContext->invokePost('EMAIL_VACATION', 'delete', ['select0' => $account]);
                    } catch (Exception $ex) {
                        $catched = true;
                        Yii::app()->user->setFlash('error', $ex->getMessage());
                    }

                    if(!$catched)
                        SystemLog::addToLog(SystemLog::TYPE_AUTORESPONDER_DELETE, $account . '@' . self::domainIdToEmailDomain($domain));
                }
            } else if (isset($_POST['set'])) {
                $password = $_POST['password'];

                $title = $_POST['title'];
                $text = $_POST['text'];
                $start = $_POST['start'];
                $end = $_POST['end'];

                $title = strip_tags($title);
                $title = S_Useful::removeNationalCharacters($title);

                $text = strip_tags($text);
                $text = S_Useful::removeNationalCharacters($text);

                $startArray = explode('-', $start);
                $endArray = explode('-', $end);

                $data = [
                    'user' => $account,
                    'subject' => $title,
                    'reply_encoding' => 'UTF-8',
                    'reply_content_type' => 'text-plain',
                    'text' => $text,
                    'starttime' => 'morning',
                    'startmonth' => $startArray[1],
                    'startday' => $startArray[2],
                    'startyear' => $startArray[0],
                    'endtime' => 'evening',
                    'endmonth' => $endArray[1],
                    'endday' => $endArray[2],
                    'endyear' => $endArray[0],
                ];

                if(!AdminRestrictions::isUltraAdmin()) {
                    try {
                        $resp = $domainContext->invokePost('EMAIL_AUTH', 'POST', ['email' => $account . '@' . self::domainIdToEmailDomain($domain), 'passwd' => $password]);
                    } catch (Exception $ex) {
                        $validated = false;
                        $catched = true;
                        Yii::app()->user->setFlash('error', $ex->getMessage());
                    }
                }

                if ($validated) {
                    try {
                        if ($is_active)
                            $action = 'modify';
                        else
                            $action = 'create';

                        $resp = $domainContext->invokePost('EMAIL_VACATION', $action, $data);
                    } catch (Exception $ex) {
                        $catched = true;
                        Yii::app()->user->setFlash('error', $ex->getMessage());
                    }

                    if(!$catched)
                    {
                        if ($is_active)
                            SystemLog::addToLog(SystemLog::TYPE_AUTORESPONDER_CHANGE, $account . '@' . self::domainIdToEmailDomain($domain));
                        else
                            SystemLog::addToLog(SystemLog::TYPE_AUTORESPONDER_SET, $account . '@' . self::domainIdToEmailDomain($domain));
                    }

                }

            }

            if(!$catched)
                if($resp['error'] == '0')
                {
                    Yii::app()->user->setFlash('success', $resp['text'] );
                    $this->redirect(['/daApi']);
                    Yii::app()->end;
                } else {
                    Yii::app()->user->setFlash('error', $resp['text']);
                }
        }

        $this->render('vacationForm',
            [
                'title' => $title,
                'text' => $text,
                'start' => $start,
                'end' => $end,
                'account' => $account,
                'domain_id' => $domain,
                'is_active' => $is_active,
            ]);
    }

    public function actionForwarder($domain, $account)
    {

        if(!in_array($domain, [self::DOMAIN_SP, self::DOMAIN_KAAB, self::DOMAIN_CQL, self::DOMAIN_FB]))
            throw new CHttpException(403);

        if(!AdminRestrictions::isUltraAdmin())
            throw new CHttpException(403);

        $domain = intval($domain);
        $account = strip_tags($account);

        if ($domain == self::DOMAIN_SP) {
            $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
            $domainContext = $userContext->getDomain('swiatprzesylek.pl');

        }
        else if ($domain == self::DOMAIN_CQL) {
            $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
            $domainContext = $userContext->getDomain('cql.global');
        }
        else if ($domain == self::DOMAIN_KAAB) {
            $userContext = DirectAdminMine::connectUser('https://da02.vipanel.pl:2222', 'kaabplxc', 'MfkWUJsD');
            $domainContext = $userContext->getDomain('kaabpl.pl');
        }
        else if ($domain == self::DOMAIN_FB) {
            $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
            $domainContext = $userContext->getDomain('fire-business.com.pl');
        }

        $forwarders = $domainContext->invokePost('EMAIL_FORWARDERS', NULL);

        $is_active = false;
        if(isset($forwarders[$account]))
        {
            $is_active = true;
            $to = $forwarders[$account];
        }

        $catched = false;

        if(isset($_POST)) {
            if (isset($_POST['delete'])) {

                try {
                    $resp = $domainContext->invokePost('EMAIL_FORWARDERS', 'delete', ['select0' => $account]);
                } catch (Exception $ex) {
                    $catched = true;
                    Yii::app()->user->setFlash('error', $ex->getMessage());
                }

                if(!$catched)
                    SystemLog::addToLog(SystemLog::TYPE_FORWARDER_DELETE, $account . '@' . self::domainIdToEmailDomain($domain));

            } else if (isset($_POST['set'])) {

                $to = $_POST['to'];

                $data = [
                    'user' => $account,
                    'email' => $to,
                ];

                    try {
                        if ($is_active)
                            $action = 'modify';
                        else
                            $action = 'create';

                        $resp = $domainContext->invokePost('EMAIL_FORWARDERS', $action, $data);
                    } catch (Exception $ex) {
                        $catched = true;
                        Yii::app()->user->setFlash('error', $ex->getMessage());
                    }

                    if(!$catched)
                    {
                        if ($is_active)
                            SystemLog::addToLog(SystemLog::TYPE_FORWARDER_CHANGE, $account . '@' . self::domainIdToEmailDomain($domain));
                        else
                            SystemLog::addToLog(SystemLog::TYPE_FORWARDER_SET, $account . '@' . self::domainIdToEmailDomain($domain));
                    }



            }

            if(!$catched)
                if($resp['error'] == '0')
                {
                    Yii::app()->user->setFlash('success', $resp['text'] );
                    $this->redirect(['/daApi']);
                    Yii::app()->end;
                } else {
                    Yii::app()->user->setFlash('error', $resp['text']);
                }
        }

        $this->render('forwarderForm',
            [
                'to' => $to,
                'account' => $account,
                'domain_id' => $domain,
                'is_active' => $is_active,
            ]);
    }

    public function actionIndex($domain = false)
    {

        if(!in_array($domain, [self::DOMAIN_SP, self::DOMAIN_KAAB, self::DOMAIN_CQL, self::DOMAIN_FB]))
            $domain = false;

        $emails = [];
        $vacations = [];

        if($domain) {
            if ($domain == self::DOMAIN_SP) {
                $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
                $domainContext = $userContext->getDomain('swiatprzesylek.pl');
            }
            else if ($domain == self::DOMAIN_CQL) {
                $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
                $domainContext = $userContext->getDomain('cql.global');

            } else if ($domain == self::DOMAIN_KAAB) {
                $userContext = DirectAdminMine::connectUser('https://da02.vipanel.pl:2222', 'kaabplxc', 'MfkWUJsD');
                $domainContext = $userContext->getDomain('kaabpl.pl');
            }
            else if ($domain == self::DOMAIN_FB) {
                $userContext = DirectAdminMine::connectUser('https://swiatprzesylek.pl:2222', 'kaab', 'SgYb64fYun');
                $domainContext = $userContext->getDomain('fire-business.com.pl');
            }

            $emails = $domainContext->getMailboxes();
            $vacations = $domainContext->invokePost('EMAIL_VACATION', NULL);


            $forwarders = $domainContext->invokePost('EMAIL_FORWARDERS', NULL);
        }


        $this->render('index', [
            'domain' => $domain,
            'emails' => $emails,
            'vacations' => $vacations,
            'forwarders' => $forwarders,
        ]);
    }

}