<?php
/* @var $items_ids [] */
/* @var $form CActiveForm */

?>

<h2>Poczta Polska Submit - Scanner</h2>

<div class="form" style="position: relative;" id="show-form">
    <div class="ajax-preloader"><div></div></div>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'=>'scanner-form',
        'enableClientValidation' => false,
        'htmlOptions' => [
            '_target' => '_blank',
        ]
    )); ?>

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <table class="table table-striped" style="margin: 0 auto; width: 500px;">
        <tr>
            <td style="text-align: center;"><?= CHtml::textArea('items_ids', $items_ids, ['id' => 'item-id', 'style' => 'padding: 15px 5px; text-align: center; width: 400px; font-size: 1.1em; font-weight: bold; height: 300px;', 'placeholder' => 'Items Id(s) (local or remote). Every ID must be in new line.']);?></td>
        </tr>
        <tr>
            <td style="text-align: center;"><?= CHtml::submitButton('Send data to PP system', ['class' => 'btn btn-large btn-primary']);?></td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>
</div>
<div class="text-center" id="again-container" style="display: none;">
    Processing... (finished if file is ready to download)<br/><br/>
    <input type="button" id="again" class="btn btn-primary btn-large" value="Input next data"/>
</div>

<br/><br/>
<div style="text-align: center;">
    <a href="<?= Yii::app()->createUrl('/pocztaPolskaSubmit/');?>" class="btn btn-mini">Restart</a>
</div>
<script>
    $('#scanner-form').on('submit', function(e){
        $('#show-form').hide();
        $('#again-container').show();
    });

    $('#again').on('click', function(){
        window.location.reload();
    })
</script>