<?php

class _CourierLabelNewGridView extends CourierLabelNew
{
    public $__courier_id;

    public function rules() {

        $array = array(
            array('__courier_id','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('courier_id', $this->courier_id);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('data', $this->data, true);
        $criteria->compare('file_type', $this->file_type, true);
        $criteria->compare('file_path', $this->file_path, true);
        $criteria->compare('track_id', $this->track_id, true);
        $criteria->compare('operator', $this->operator);
        $criteria->compare('downloaded', $this->downloaded);
        $criteria->compare('stat', $this->stat);
        $criteria->compare('log', $this->log, true);

        // $criteria->compare('__type', $this->__type);

        if($this->__courier_id)
        {
            $criteria->together  =  true;
            $criteria->with = array('courier');
            $criteria->compare('courier.local_id',$this->__courier_id);
        }


        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR) {
            $criteria->together  =  true;
            $criteria->with = array('courier');
            $criteria->with = array('courier.user');
            $criteria->compare('courier.user.salesman_id', Yii::app()->user->model->salesman_id);

        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes = array(
            '__courier_id'=>array(
                'asc'=>'courier.local_id ASC',
                'desc'=>'courier.local_id DESC',
            ),
            '*', // add all of the other columns as sortable
        );


        return new CActiveDataProvider($this, array(
            'criteria' => $this->with('courier')->searchCriteria(),
            'sort'=> $sort,
            'Pagination' => array (
                'PageSize' => 50,
            ),
        ));

    }



}