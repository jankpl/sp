<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'change-status',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('order/changeStat'),
));
?>

<?php echo $form->hiddenField($model,'id'); ?>

<div class="form">

    <?php echo $form->errorSummary($model); ?>

    <table class="list hTop" style="width: 500px;">
        <tr>
            <td>Current status</td>
        </tr>
        <tr>
            <td>[<?php echo $model->order_stat_id;?>] <?php echo $model->orderStat->name;?></td>
        </tr>
    </table>


    <table class="list hTop" style="width: 500px;">
        <tr>
            <td>New status</td>
        </tr>
        <tr>
            <td>
                <?php echo $form->dropDownList($model, 'order_stat_id', CHtml::listData(OrderStat::model()->findAll(), 'id', 'name')); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo CHtml::submitButton(Yii::t('app', 'Save'),
                    array(
                        'confirm'=>'Are you sure?'
                    ));
                $this->endWidget();
                ?>
            </td>
        </tr>
    </table>

</div><!-- form -->