<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'notes',
	)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textarea($model,'notes', array('style' => 'width: 550px; height: 100px; resize: none;')); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>


	<div class="navigate">
		<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-large')); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>