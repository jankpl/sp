<?php

/* @var $this PostalController */
/* @var $model Postal */
/* @var $withClone boolean */
?>


<h3>Label printer <?= $withClone ? 'WITH CLONE' : '';?></h3>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'label printer',
    'enableAjaxValidation' => false,
));
?>



<div class="form">

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, 'Type in or paste postal local id', array('closeText' => false)); ?>

    <table class="list hLeft" style="width: 500px; margin: 0 auto;">
        <tr>
            <td><?= $withClone ? 'Remote ID' : 'Local ID';?></td>
            <td><?php echo CHtml::textField('local_id', '', array('id' => 'local_id')); ?></td>
            <td><img src="<?php echo Yii::app()->baseUrl;?>/images/layout/preloader.gif" id="ajax-preloader" style="display: none;"/></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><?php echo CHtml::checkBox('automatic_roll_label', '', array('id' => 'automatic_roll_label')); ?> <label for="automatic_roll_label" style="display: inline;">Automaticaly generate roll label</label></td>
        </tr>
    </table>
</div><!-- form -->

<?php
$this->endWidget();
?>


<table class="" style="width: 500px; margin: 0 auto;">
    <tr>
        <td id="labels-placeholder" class="text-center" style="vertical-align: top;">

            <?php
            if(isset($_POST['local_id']))
            {
                $this->renderPartial('label_printer_buttons', array('model' => $model));
            }
            ?>
        </td>
    </tr>
</table>

<?php
Yii::app()->clientScript->registerCoreScript('jquery');
?>

<script>
    $(document).ready(function() {

        $("#local_id").focus();

        $("#local_id").on("change", function (e) {
            $("#ajax-preloader").show();
            $("#labels-placeholder").html('');

            $.ajax({
                method: "POST",
                dataType : 'json',
                url: "<?php echo Yii::app()->createUrl('/postal/postal/labelPrinter', ['withClone' => $withClone]); ?>",
                data: {local_id: $("#local_id").val(), ajax: 1, automatic_roll_label : $("#automatic_roll_label").is(':checked')},
                success: function (val) {

                    if(val.redirect)
                    {
                        window.location.href = val.url;
                    } else {
                        $("#ajax-preloader").show();
                        $("#labels-placeholder").html(val.html);
                        $("#ajax-preloader").fadeOut('');
                    }
                }
            });


        });

    });
</script>


