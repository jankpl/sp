<?php

class CourierHubController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'CourierHub'),
		));
	}

	public function actionCreate() {
		$model = new CourierHub;
        $model->address = new AddressData('with_email');


		if (isset($_POST['CourierHub'])) {
			$model->setAttributes($_POST['CourierHub']);

            $model->address->setAttributes($_POST['AddressData']);


			if ($model->saveWithAddress()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {

        /* @var $model CourierHub */
        $model = $this->loadModel($id, 'CourierHub');

        if($model->address === NULL)
            $model->address = new AddressData('with_email');

		$model->address->scenario = 'with_email';

//		$this->performAjaxValidation($model, 'courier-hub-form');

		if (isset($_POST['CourierHub'])) {
			$model->setAttributes($_POST['CourierHub']);
            $model->address->setAttributes($_POST['AddressData']);




			if ($model->saveWithAddress()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
//		if (Yii::app()->getRequest()->getIsPostRequest()) {
//			$this->loadModel($id, 'CourierHub')->delete();
//
//			if (!Yii::app()->getRequest()->getIsAjaxRequest())
//				$this->redirect(array('admin'));
//		} else
//			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {

        $this->redirect(array('admin'));
	}

	public function actionAdmin() {
		$model = new CourierHub('search');
		$model->unsetAttributes();

		if (isset($_GET['CourierHub']))
			$model->setAttributes($_GET['CourierHub']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}