<?php

Yii::import('application.models._base.BaseOrderStatHistory');

class OrderStatHistory extends BaseOrderStatHistory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

            array('date_entered, current_stat, order_id', 'required'),
            array('current_stat', 'numerical', 'integerOnly'=>true),
            array('previous_stat, author', 'length', 'max'=>45),
            array('order_id', 'length', 'max'=>10),
            array('previous_stat', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, current_stat, previous_stat, author, order_id', 'safe', 'on'=>'search'),
        );
    }

}