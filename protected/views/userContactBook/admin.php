<h2><?php echo Yii::t('site', 'Manage your Contact Book'); ?></h2>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'user-contact-book-grid',
	'dataProvider' => $model->searchForUser(),
	'filter' => $model,
	'columns' => array(
        array(
            'name'=>'date_entered',
            'value'=>'$data->dateEntered',
        ),
        array(
            'name'=>'type',
            'value'=>'UserContactBook::getTypes_enum()[$data->type]',
            'filter'=>UserContactBook::getTypes_enum(),
        ),
        array(
            'name'=>'__name',
            'value'=>'$data->addressData->name',
        ),
        array(
            'name'=>'__company',
            'value'=>'$data->addressData->company',
        ),
        array(
            'name'=>'__country',
            'value'=>'$data->addressData->country',
        ),
        array(
            'name'=>'__city',
            'value'=>'$data->addressData->city',
        ),
        array(
            'name'=>'__zip_code',
            'value'=>'$data->addressData->zip_code',
        ),
        array(
            'name'=>'__address_line_1',
            'value'=>'$data->addressData->address_line_1',
        ),
        array(
            'name'=>'__tel',
            'value'=>'$data->addressData->tel',
        ),
        array(
            'name'=>'__email',
            'value'=>'$data->addressData->email',
        ),
		array(
			'class' => 'CButtonColumn',
            'htmlOptions'=>array('width'=>'40px'),
        ),
	),
)); ?>

<?php $this->widget('FlashPrinter'); ?>

<div class="navigate">
    <?php echo TbHtml::button(Yii::t('userContactBook','Dodaj nową pozycję'), array('onclick' => 'window.location="'.Yii::app()->createUrl('userContactBook/create').'"'));?>
    <?php echo TbHtml::button(Yii::t('userContactBook','Importuj z pliku'), array('onclick' => 'window.location="'.Yii::app()->createUrl('userContactBook/import').'"'));?>
</div>

<?php $this->widget('BackLink');?>