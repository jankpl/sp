<?php
/* @var $this PrintServerController */
/* @var $model PrintServer */

?>
<?php
if($model->isNewRecord):
    ?>
    <h2><?= Yii::t('user', 'Print server'); ?> | <?= Yii::t('printServer', 'Dodaj nową drukarkę');?></h2>
<?php else: ?>
    <h2><?= Yii::t('user', 'Print server'); ?> | <?= $model->getFullAddress();?></h2>
    <?php
endif;
?>

    <br/>
<?php
$this->widget('FlashPrinter');
?>

<?php
if($model->isNewRecord):
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="form">
                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'print-server-form'     ,
                    'enableAjaxValidation'=>false,
                )); ?>

                <?php echo $form->errorSummary($model); ?>

                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th colspan="2"><?= Yii::t('printServer', 'Dodaj nową drukarkę');?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-right" style="vertical-align: middle;"><?php echo $form->labelEx($model,'address_ip', ['style' => 'width: 100%;']); ?></td>
                        <td><?php echo $form->textField($model,'address_ip',array('size'=>15,'maxlength'=>15, 'date-ip' => 'true', 'placeholder' => 'xxx.xxx.xxx.xxx')); ?>
                            <?php echo $form->error($model,'address_ip'); ?></td>
                    </tr>
                    <tr>
                        <td class="text-right" style="vertical-align: middle;"><?php echo $form->labelEx($model,'address_port', ['style' => 'width: 100%;']); ?></td>
                        <td><?php echo $form->numberField($model,'address_port',array('size'=>5,'min' => 1000, 'max' => 50000, 'placeholder' => 9100)); ?>
                            <?php echo $form->error($model,'address_port'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center"><?= Yii::t('printServer', 'Po zapisaniu na drukarkę zostanie przesłany kod aktywacyjny.');?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><?php echo CHtml::submitButton(Yii::t('printServer', 'Zapisz'), ['class' => 'btn btn-primary']); ?></td>
                    </tr>
                    </tbody>
                </table>


                <?php $this->endWidget(); ?>
            </div><!-- form -->

        </div>
    </div>

    <?php
elseif($model->stat == PrintServer::STAT_ACTIVE):
    ?>
    <div class="alert alert-success text-center"><?= Yii::t('user', 'Print Serwer jest aktywny.');?><br/><br/>
        <?= Yii::t('printServer', 'Adres drukarki');?> : <strong><?= $model->address_ip;?>:<?= $model->address_port;?></strong>
    </div>

    <div class="text-center">
        <?= CHtml::link(Yii::t('printServer','Wyłącz'), ['/printServer/toggle', 'no' => $model->no], ['class' => 'btn btn-warning', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Na pewno?').'")']); ?>
        <?= CHtml::link(Yii::t('printServer','Skasuj'), ['/printServer/delete', 'no' => $model->no], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Na pewno?').'")']); ?>
    </div>

    <?php
elseif($model->stat == PrintServer::STAT_NEW):
    ?>
    <div class="alert alert-warning text-center"><?= Yii::t('user', 'Print Serwer oczekuje na weryfikację');?><br/><br/>
        <?= Yii::t('printServer', 'Adres drukarki');?> : <strong><?= $model->address_ip;?>:<?= $model->address_port;?></strong></div>


    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="form">
                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'print-server-form'     ,
                    'enableAjaxValidation'=>false,
                )); ?>

                <table class="table table-striped table-bordered table-condensed">
                    <tr>
                        <td class="text-right" style="vertical-align: middle;"><?php echo CHtml::label(Yii::t('verificationCode', 'Kod weryfikacyjny'), 'verification-code', ['style' => 'width: 100%;']); ?></td>
                        <td> <?= CHtml::textField('verification_code', '', ['id' => 'verification-code', 'maxlenght' => 10]);?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><?php echo CHtml::submitButton(Yii::t('printServer', 'Aktywuj'), ['class' => 'btn btn-primary', 'name' => 'CheckVerification']); ?></td>
                    </tr>
                </table>

                <?php
                if($model->isResendPossible()):
                    ?>
                    <table class="table table-striped table-bordered table-condensed">
                        <tr>
                            <td class="text-center"><?= Yii::t('printServer', 'Jeżeli nie otrzymałeś kodu, możesz wysłać go ponownie. Po przekroczeniu limitu prób, adres zostanie zablokowany.');?></td>
                        </tr>

                        <tr>
                            <td class="text-center"><?php echo CHtml::submitButton(Yii::t('printServer', 'Wyślij ponownie kod'), ['class' => 'btn btn-warning', 'name' => 'Resend']); ?></td>
                        </tr>
                        <tr>
                            <td class="text-center"> <strong><?= Yii::t('verificationCode', 'Pozostało prób');?> : <?= (PrintServer::MAX_TRIES_ACTIVATION - $model->verify_counter);?></strong></td>
                        </tr>
                    </table>
                    <?php
                endif;
                ?>

                <table class="table table-striped table-bordered table-condensed">
                    <tr>
                        <td class="text-center"><?php echo CHtml::submitButton(Yii::t('printServer', 'Usuń drukarkę'), ['class' => 'btn btn-danger', 'name' => 'Delete', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Na pewno?').'")']); ?></td>
                    </tr>
                </table>


                <?php $this->endWidget(); ?>
            </div><!-- form -->

        </div>
    </div>

    <?php
elseif($model->stat == PrintServer::STAT_DISABLED):
    ?>
    <div class="alert alert-danger text-center"><?= Yii::t('user', 'Usługa jest wstrzymana');?><br/><br/>
        <?= Yii::t('printServer', 'Adres drukarki');?> : <strong><?= $model->address_ip;?>:<?= $model->address_port;?></strong>
    </div>

    <div class="text-center">
        <?= CHtml::link(Yii::t('printServer','Włącz'), ['/printServer/toggle'], ['class' => 'btn btn-success', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Na pewno?').'")']); ?>
        <?= CHtml::link(Yii::t('printServer','Skasuj'), ['/printServer/delete'], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Na pewno?').'")']); ?>
    </div>
    <?php
endif;
?>

    <br/>
    <br/>
    <div class="text-center">
        <?= CHtml::link(Yii::t('printServer','Powrót do listy'), ['/printServer/'], ['class' => 'btn btn-primary']); ?>
    </div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.mask.min.js');
$script = " $('[date-ip]').mask('099.099.099.099');";
Yii::app()->clientScript->registerScript('mask-ip', $script, CClientScript::POS_READY);
