<?php

class CourierCodSettledScannerController extends Controller
{


    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_COURIER_SCANNER.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($hash = NULL, $a = false)
    {
        Yii::app()->getModule('courier');

        $automatic = $a;

        $model = new CourierCodSettledScannerForm();

        if (isset($_POST['CourierCodSettledScannerForm']['package_ids'])) {
            $automatic = $_POST['automatic'];

            $packagesIds = $_POST['CourierCodSettledScannerForm']['package_ids'];
            $packagesIds = explode(PHP_EOL, $packagesIds);

            $model->package_ids = $packagesIds;
            $model->settle_date = $_POST['CourierCodSettledScannerForm']['settle_date'];
            $model->type = $_POST['CourierCodSettledScannerForm']['type'];

            MyDump::dump('admin_cod_settle_scanner.txt', print_r($_POST,1));


            if ($model->validate()) {
                $courierModels = $model->returnCourierModels(true);

                MyDump::dump('admin_cod_settle_scanner.txt', print_r($courierModels,1));


                $errorPackages = [];
                $changedCounter = 0;
                $toProcess = [];
                foreach ($courierModels AS $key => $courierModel) {

                    if (is_array($courierModel) && S_Useful::sizeof($courierModel) == 1)
                        $courierModel = $courierModel[0];

                    if (is_array($courierModel) && S_Useful::sizeof($courierModel) > 1) {
                        $errorPackages[] = $model->package_ids[$key] . ' : More than one package found with this ID!';
                    }
                    else if ($courierModel === false) {
                        $errorPackages[] = $model->package_ids[$key] . ' : Package has no COD!';
                    }
                    else if (!($courierModel instanceof Courier))
                    {
                        $errorPackages[] = $model->package_ids[$key] . ' : Package not found!';
                    }
                    else if (CourierCodSettled::ifCodSettled($courierModel->id))
                    {
                        $errorPackages[] = $model->package_ids[$key] . ' : Package has COD already settled!';
                    }
                    else
                    {
                        $toProcess[] = $courierModel->id;
                    }
                }

                MyDump::dump('admin_cod_settle_scanner.txt', 'TO PROCESS:'. print_r($toProcess,1));

                if(S_Useful::sizeof($toProcess))
                    $changedCounter = CourierCodSettled::addListOfItems($toProcess, $model->type, $model->settle_date, CourierCodSettled::SOURCE_OWN_SCANNER);


                MyDump::dump('admin_cod_settle_scanner.txt', 'ERROR ITEMS:'. print_r($errorPackages,1));


                if (S_Useful::sizeof($errorPackages))
                    Yii::app()->user->setFlash('error', implode('<br/>', $errorPackages));

                MyDump::dump('admin_cod_settle_scanner.txt', 'CHANGES ITEMS:'. print_r($changedCounter,1));

                if ($changedCounter)
                    Yii::app()->user->setFlash('success', 'Packages with COD status settled: ' . $changedCounter);

                $this->redirect(['/courierCodSettledScanner/index', 'a' => $automatic]);
                Yii::app()->end();
            }
        }

        if(is_array($model->package_ids))
            $model->package_ids = implode(PHP_EOL, $model->package_ids);


        $this->render('index',
            [
                'model' => $model,
                'automatic' => $automatic,
            ]);
    }
}