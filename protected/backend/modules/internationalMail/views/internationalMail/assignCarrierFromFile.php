<?php
/* @var $this InternationalMailController */
/* @var $models S_InternationalMail_Id_From_File[] */
/* @var $fileModel F_FileModel */
?>


<?php
$this->renderPartial('_assignCarrierFromFile_UploadForm',
    array(
        'model' => $fileModel,
    ));
?>


<?php

if(S_Useful::sizeof($models))
    $this->renderPartial('_assignCarrierFromFile_List',
        array(
            'models' => $models,
        ));
?>






