<?php


class postalRefValidator extends CValidator
{

    const MIN = 5;
    const MAX = 30; // last characters are reserved for multipackages suffix

    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object,$attribute)
    {
        if($object->$attribute == '')
            return;

        if($object->user_id == '')
        {
            $this->addError($object,$attribute, 'Package without user cannot have ref number!');
            return;
        }

        $value = $object->$attribute;
        $value = strtoupper($value);

        if(!preg_match('/^[a-z0-9]{'.self::MIN.','.self::MAX.'}$/i', $value))
        {
            $this->addError($object,$attribute, Yii::t('postal', 'Pole REF może zawierać tylko litery i cyfry w ilości od {n} do {m} znaków!', ['{n}' => self::MIN, '{m}' => self::MAX]));
            return;
        }

        $postalRefPrefix = $object->user->getPostalRefPrefix();

        if(!$postalRefPrefix OR $postalRefPrefix == '')
        {
            $this->addError($object,$attribute, Yii::t('postal', 'Nie masz uprawnień do pola REF!'));
            return;
        }

        $rule = '^'.$postalRefPrefix.'.*';

        // @ 21.02.2019
        // Special rule for SF Express
        if(in_array($object->user->id, [1289,812,1534,2813,2404]))
            $rule = '(^'.$postalRefPrefix.'|^SF|^46|^99).*';


        if(!preg_match('/'.$rule.'/i', $value))
        {
            $this->addError($object,$attribute, Yii::t('postal', 'Pole REF nie jest zgodne z Twoim prefiksem!'));
            return;
        }

        $cmd = Yii::app()->db->createCommand();
        if($cmd->select('id')->from('postal')->where('ref = :ref', [':ref' => $value])->limit(1)->queryScalar())
        {
            $this->addError($object,$attribute, Yii::t('postal', 'Ten numer REF został juz wykorzystany!'));
            return;
        }
    }
}