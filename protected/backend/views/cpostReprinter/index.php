<?php

/* @var $this CourierController */
/* @var $model Postal */
?>


<h3>CPost reprinter</h3>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'label printer',
    'enableAjaxValidation' => false,
));
?>



<div class="form">

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, 'Paste package ext id or type and hit [enter] key', array('closeText' => false)); ?>

    <table class="list hLeft" style="width: 500px; margin: 0 auto;">
        <tr>
            <td>Package ext ID</td>
            <td><?php echo CHtml::textField('local_id', '', array('id' => 'local_id')); ?></td>
            <td><img src="<?php echo Yii::app()->baseUrl;?>/images/layout/preloader.gif" id="ajax-preloader" style="display: none;"/></td>
        </tr>
        <tr>
            <td>With CN22?</td>
            <td><?php echo CHtml::checkBox('with_cn22', '', array('id' => 'with_cn22')); ?></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><?php echo CHtml::radioButton('print_option', true, array('id' => 'print_option_1', 'value' => 1)); ?> <label for="print_option_1" style="display: inline;">Download PDF with label</label></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <?php echo CHtml::radioButton('print_option', '', array('id' => 'print_option_2', 'value' => 2)); ?> <label for="print_option_2" style="display: inline;">Automaticaly send roll label to Zebra printer:</label>
                <br/>
                IP: <?php echo CHtml::textField('ip', $_SERVER['REMOTE_ADDR'], array('style' => 'width: 150px;', 'id' => 'ip')); ?>:<?php echo CHtml::textField('port', 51000, array('style' => 'width: 40px;', 'id' => 'port')); ?>
            </td>
        </tr>
    </table>
</div><!-- form -->


<table class="" style="width: 500px; margin: 0 auto;">
    <tr>
        <td id="labels-placeholder" class="text-center" style="vertical-align: top;">
        </td>
    </tr>
</table>
<?php
$this->endWidget();
?>


<?php
Yii::app()->clientScript->registerCoreScript('jquery');
?>

<script>
    $(document).ready(function() {

        $("#local_id").focus();

        $("#local_id").on("change", function (e) {

            if($("#local_id").val() != '') {

                $("#ajax-preloader").show();
                $("#labels-placeholder").html('');

                $.ajax({
                    method: "POST",
                    dataType: 'json',
                    url: "<?php echo Yii::app()->createUrl('/cpostReprinter'); ?>",
                    data: {
                        local_id: $("#local_id").val(),
                        ajax: 1,
                        print_option: $("[name='print_option']:checked").val(),
                        ip: $('#ip').val(),
                        port: $('#port').val(),
                        with_cn22: $('#with_cn22').is(':checked'),
                    },
                    success: function (val) {

                        if (val.redirect) {
                            window.location.href = val.url;
                        } else {
                            $("#ajax-preloader").show();
                            $("#labels-placeholder").html(val.html);
                            $("#local_id").val('');
                            $("#ajax-preloader").fadeOut('');
                        }
                    }
                });
            }

        });

    });
</script>


