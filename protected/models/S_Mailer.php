<?php

class S_Mailer
{
    const TESTING = false;

    const LIMIT_BCCS_AT_ONCE = 99;

    public static function sendToAdministrator($subject, $text, array $attachmentsPath = [], array $attachementsName = [], $customReceiverAddress = false, $replyTo = false)
    {

        if (!$customReceiverAddress) {
            $to = Config::getData('admin_email');
            $toName = 'Administrator SwiatPrzesylek.pl';
        } else {
            $to = $customReceiverAddress;
            $toName = $customReceiverAddress;
        }

        return self::send($to,$toName, $subject, $text, false, null, null, false, $attachmentsPath, $attachementsName, false, false, $replyTo);
    }

    public static function sendToCustomer($email, $name, $subject, $text, $ignoreNotificationBlock = false, $lang = NULL, $sentByAutomat = true, $attachmentPath = false, $attachmentName = false, $senderAddress = false, $senderName = false, $pv = NULL, $attachmentString = false)
    {
        // dont send email to fake address

        if(is_array($email))
        {
            foreach($email AS $key => $item)
            {
                if(!strcasecmp($item,AddressData::FAKE_EMAIL))
                    unset($email[$key]);

                if(!$ignoreNotificationBlock) {

                    // user has selected blocking email notifications
                    if(User::isEmailBlockedForNotification($item))
                        unset($email[$key]);
                }
            }
        }
        else {
            if (!strcasecmp($email, AddressData::FAKE_EMAIL))
                return true;

            if(!$ignoreNotificationBlock) {

                // user has selected blocking email notifications
                if(User::isEmailBlockedForNotification($email))
                {
                    MyDump::dump('email_ignore.txt', $email);
                    return false;
                }
            }
        }

        if($pv === NULL && Yii::app()->params['frontend'] == true)
            $pv = Yii::app()->PV->get();


        self::send($email, $name, $subject, $text, true, $lang, $pv, $sentByAutomat, $attachmentPath, $attachmentName, $senderAddress, $senderName, false, $attachmentString);
    }

    public static function send($to_email, $to_name, $subject, $text, $queque = true, $lang = NULL, $pv = NULL, $sentByAutomat = true, $attachmentPath = false, $attachmentName = false, $senderAddress = false, $senderName = false, $replyTo = false, $attachmentString = false)
    {
        if(!is_array($to_email))
            if(in_array(strtolower($to_email), [
                'empty@emptymail.pl',
                AddressData::FAKE_EMAIL,
                '1@11.pl',
                '1@1.pl',
                'test@kaabnl.nl',
                'kaab@kaabnl.nt',
                'it2@kaabnl.nl',
                'test@wpppppp.pl',
                'test@oppp.pl',
                'test@oppppp.pl',
                'testtt@oppp.pl',
                'pmedia@wp.pl',
            ])) {
                MyDump::dump('email_ignore.txt', $to_email);
                return true;
            }

        $testing = self::TESTING;

        // do not queque mails with attachments string, beacause they are too big...
        if($queque AND !$attachmentString) {

            $data = array(
                'to_email' => $to_email,
                'to_name' => $to_name,
                'subject' => $subject,
                'text' => $text,
                'queque' => $queque,
                'lang' => $lang,
                'pv' => $pv,
                'sentByAutomat' => $sentByAutomat,
                'attachmentPath' => $attachmentPath,
                'attachmentName' => $attachmentName,
                'senderAddress' => $senderAddress,
                'senderName' => $senderName,
                'replyTo' => $replyTo,
            );

            return Yii::app()->Queque->scheudleItem(Queque::TYPE_EMAIL, $data);
        }

        $tempLangChange = new TempLangChange;
        $tempLangChange->temporarySetLanguage($lang);

        if($pv == PageVersion::PAGEVERSION_RUCH)
        {
            $url = 'https://pwri.pl';
            $name = 'PWRI.pl';
            $host = 'mail.pwri.pl';
            $login = 'do-not-reply@pwri.pl';
            $pass = 'lgb0hUNIw';
            $logo = Yii::app()->getBasePath().'/../theme/ruch/images/logo.png';
        }
        else if($pv == PageVersion::PAGEVERSION_PLI)
        {
            $url = 'https://pli-uk24.co.uk.pl';
            $name = 'pli-uk24.co.uk';
            $host = 'mail.pli-uk24.co.uk';
            $login = 'do-not-reply@pli-uk24.co.uk';
            $pass = '8kVTiS4y';
            $logo = Yii::app()->getBasePath().'/../theme/pli/images/logo.png';
        }
        else if($pv == PageVersion::PAGEVERSION_CQL)
        {
            $url = 'https://cql.global';
            $name = 'cql.global';
            $host = 'mail.cql.global';
            $login = 'do-not-reply@cql.global';
            $pass = 'hqBN9GSL';
            $logo = Yii::app()->getBasePath().'/../theme/cql/images/logo.png';
        }
        else if($lang == Language::LANG_EN OR $lang == Language::LANG_DE)
        {
            $url = 'https://royalshipments.com';
            $name = 'RoyalShipments.com';
            $host = 'mail.royalshipments.com';
            $login = 'do-not-reply@royalshipments.com';
            $pass = 'foeYEn0i';
            $logo = Yii::app()->getBasePath().'/../images/mail/logo_en.png';
        } else {
            $url = 'https://swiatprzesylek.pl';
            $name = 'SwiatPrzesylek.pl';
            $host = 'mail.swiatprzesylek.pl';
            $login = 'do-not-reply@swiatprzesylek.pl';
            $pass = 'foeYEn0i';
            $logo = Yii::app()->getBasePath().'/../images/mail/logo.png';
        }

        $from = $login;
        if($sentByAutomat)
            $footer_info = Yii::t('mail','Wiadomosć wygenerowana automatycznie');
        else
            $footer_info = '';

        if(isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $contentOpen = $controller->renderInternal(Yii::getPathOfAlias('application.views._mailTemplate._header').'.php', [
            'url' => $url, 'name' => $name], true);

        $contentClose = $controller->renderInternal(Yii::getPathOfAlias('application.views._mailTemplate._footer').'.php', [
            'url' => $url, 'name' => $name, 'footer_info' => $footer_info], true);

        $baseSubject = '['.$name.'] ';

        $body = $contentOpen.$text.$contentClose;


//        $mail = Yii::createComponent('application.extensions.mailer.EMailer');
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        $mail->SMTPAuth = true;
        $mail->SMTPOptions = ['ssl'=> [
            'allow_self_signed' => true,
            'verify_peer' => false,
            'verify_peer_name' => false,
        ]];

        $mail->CharSet = 'UTF-8';

        if($testing) {
            $mail->Mailer = 'smtp';
            $mail->Host = 'mail.hss.pl';
            $mail->Username = 'test@hss.pl';
            $mail->Password = 'uciEZMEzTQ';
            $mail->Port = 587;

//            $to_email = 'jtk@hss.pl';
//            $to_name = 'jtk@hss.pl';
        } else {
            $mail->Mailer = 'smtp';
            $mail->Host = $host;
            $mail->Username = $login;
            $mail->Password = $pass;
            $mail->Port = 587;
        }


        if(!is_array($to_email))
            $mail->addAddress($to_email, $to_name);
        else
            foreach($to_email AS $email)
                $mail->addAddress($email);

        if($senderAddress && $senderName)
        {
            $from = $senderAddress;
            $name = $senderName;
        }

        if($replyTo)
            $mail->addReplyTo($replyTo);


        $mail->From = $from;
        $mail->FromName = $name;
        $mail->Subject = $baseSubject.$subject;
        $mail->Body = $body;

        $mail->AddEmbeddedImage($logo, 'logo', 'attachment', 'base64', 'image/png');

        $mail->isHTML(true);


        if($attachmentPath && $attachmentName) {

            if (!is_array($attachmentPath))
                $attachmentPath = [$attachmentPath];

            if (!is_array($attachmentName))
                $attachmentName = [$attachmentName];

            foreach ($attachmentPath AS $key => $item) {
                if (is_file($item))
                    $mail->addAttachment($item, $attachmentName[$key]);
            }
        }

        if($attachmentString) {
            if (!is_array($attachmentString))
                $attachmentString = [$attachmentString];

            foreach ($attachmentString AS $key => $item) {

                if(is_array($attachmentName))
                    $name = $attachmentName[$key];
                else
                    $name = $attachmentName;

                $mail->addStringAttachment($item, $name);
            }
        }

        $result = $mail->send();

        MyDump::dump('emails.txt', print_r($to_email,1) . ' | ' . $baseSubject.$subject . ' | ' . print_r($text,1));

        $tempLangChange->revertToBaseLanguage();

        return $result;
    }


    public static function sendToCustomerGroup(array $emails, $subject, $text, $attachmentPath = false, $attachmentName = false)
    {
        $testing = self::TESTING;

        $url = 'https://swiatprzesylek.pl';
        $name = 'SwiatPrzesylek.pl';
        $host = 'mail.swiatprzesylek.pl';
        $login = 'do-not-reply@swiatprzesylek.pl';
        $pass = 'foeYEn0i';
        $logo = Yii::app()->getBasePath().'/../images/mail/logo.png';

        $footer_info = '';

        if(isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $contentOpen = $controller->renderInternal(Yii::getPathOfAlias('application.views._mailTemplate._header').'.php', [
            'url' => $url, 'name' => $name], true);

        $contentClose = $controller->renderInternal(Yii::getPathOfAlias('application.views._mailTemplate._footer').'.php', [
        'url' => $url, 'name' => $name, 'footer_info' => $footer_info], true);

        $baseSubject = '['.$name.'] ';

        $body = $contentOpen.$text.$contentClose;


//        $mail = Yii::createComponent('application.extensions.mailer.EMailer');

        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        $mail->SMTPAuth = true;
        $mail->SMTPOptions = ['ssl'=> [
            'allow_self_signed' => true,
            'verify_peer' => false,
            'verify_peer_name' => false,
        ]];

        $mail->CharSet = 'UTF-8';

        if($testing) {
            $mail->Mailer = 'smtp';
            $mail->Host = 'mail.hss.pl';
            $mail->Username = 'test@hss.pl';
            $mail->Password = 'uciEZMEzTQ';
            $mail->Port = 587;

            //$to_email = 'jtk@hss.pl';

//            $emails = [
//                'Test 1' => 'jtk@hss.pl',
//                'Test 2' => 'info@jtkprojekt.pl',
//            ];
        } else {
            $mail->Mailer = 'smtp';
            $mail->Host = $host;
            $mail->Username = $login;
            $mail->Password = $pass;
            $mail->Port = 587;
        }

        $mail->addAddress($login, $name);
        $mail->addCC($login, $name);

        $mail->From = $login;
        $mail->FromName = $name;
        $mail->Subject = $baseSubject.$subject;
        $mail->Body = $body;


        $mail->AddEmbeddedImage($logo, 'logo', 'attachment', 'base64', 'image/png');
        $mail->isHTML(true);

        if($attachmentPath && $attachmentName)
        {
            if(is_file($attachmentPath))
                $mail->addAttachment($attachmentPath, $attachmentName);
        }

        $i = 0;
        foreach($emails AS $login => $adres) {
            if (filter_var($adres, FILTER_VALIDATE_EMAIL)) {
                $mail->AddBCC($adres, $login);
            }

            $i++;
            if($i > self::LIMIT_BCCS_AT_ONCE) {
                $mail->send();
                $mail->clearBCCs();
                $i = 0;
                MyDump::dump('emailsGroup.txt', 'SENDING...');
            }
        }

        $mail->send();

        MyDump::dump('emailsGroup.txt', 'SENDING...');
        MyDump::dump('emailsGroup.txt', print_r($emails,1) . ' | ' . $baseSubject.$subject . ' | ' . print_r($text,1));
    }

}