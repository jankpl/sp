<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailPriceValue');

class HybridMailPriceValue extends BaseHybridMailPriceValue
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function afterConstruct()
    {
        $this->nextPagePrice = new Price();
        $this->firstPagePrice = new Price();

        return parent::afterConstruct();
    }


    public function atLeastOnePrice($attribute, $params)
    {

        $price1 = 'next_page_price';
        $price2 = 'first_page_price';

        if($this->$price1->value == 0 && $this->$price2->value == 0)
        {
            $this->addError($price1, 'Provide at least one price!');
            $this->addError($price2, 'Provide at least one price!');
        }
    }

    public function rules() {
        return array(
            //array('*', 'atLeastOnePrice'),


            array('hybrid_mail_price_id, first_page_price, next_page_price', 'required'),
            array('hybrid_mail_price_id', 'numerical', 'integerOnly'=>true),
            array('next_page_price', 'numerical', 'integerOnly'=>true),
            array('first_page_price', 'numerical', 'integerOnly'=>true),


            array('id, first_page_price, next_page_price, hybrid_mail_price_id', 'safe', 'on'=>'search'),
        );
    }


    public function gridViewData($id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition("hybrid_mail_price_id = '$id'");

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pagesize' => 5,
            )

        ));
    }
}