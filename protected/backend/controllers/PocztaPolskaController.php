<?php

class PocztaPolskaController extends Controller
{


    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_ADMIN.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    function actionIndex()
    {
        $this->render('index',
            [
            ]);

    }
}