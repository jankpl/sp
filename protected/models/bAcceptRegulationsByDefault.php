<?php

/**
 * Class bAcceptRegulationsByDefault
 *
 * Behavior accepts regulations by default
 */
class bAcceptRegulationsByDefault extends bBaseBehavior
{
    const REGULATIONS = 'regulations';
    const REGULATIONS_RODO = 'regulations_rodo';

    protected $_allowedClass = [
        'CourierTypeInternalSimple',
        'CourierTypeInternal',
        'CourierTypeOoe',
        'CourierTypeDomestic',
        'CourierTypeU',
        'CourierTypeU',
        'Postal',
        'PostalBulkForm',
        'CourierTypeExternalReturn',
        'CourierTypeReturn',
    ];

    public function afterConstruct($event)
    {
        /* @var $that AddressData */
        $that = $this->owner();

        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest && Yii::app()->user->model->getAcceptRegulationsByDefault())
        {
            if(property_exists(get_Class($that), self::REGULATIONS)) {

                $attr = self::REGULATIONS;
                $that->$attr = 1;
            }

            if(property_exists(get_Class($that), self::REGULATIONS_RODO)) {

                $attr = self::REGULATIONS_RODO;
                $that->$attr = 1;
            }
        }

        return parent::afterConstruct($event);
    }
}