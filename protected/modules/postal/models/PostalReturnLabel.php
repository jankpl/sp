<?php

class PostalReturnLabel
{

    static function generateMulti(array $postals, AddressData $address = NULL)
    {
        $done = [];

        // GENERATE OWN:
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        foreach($postals AS $postal)
        {
            $pdf = self::generate($postal, NULL, true, $pdf);
            $done[] = $postal->id;
        }

        $pdf->Output('RL.pdf', 'D');

        return $done;
    }


    /**
     * Generates Return own label and returns it as Image
     * @param Postal $item
     * @param AddressData|NULL $address

     * @return String
     */
    static function generate(Postal $item, AddressData $address = NULL, $asImageBlob = true, $ownPdf = false)
    {

        $pageW = 100;
        $pageH = 150;


        if(!$ownPdf) {

            // GENERATE OWN:
            $pdf = PDFGenerator::initialize();
            $pdf->setFontSubsetting(true);


        } else {
            $pdf = $ownPdf;
        }

        $pdf->addPage('H', array($pageW,$pageH));

        $margins = 1;

        /* @var $pdf TCPDF */
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $yBase = 0;
        $xBase = 0;

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

        if($item->target_sp_point)
            $pdf->MultiCell(30,5, $item->targetSpPoint->name, 1, 'L', false, 1, $xBase + $margins, $yBase + $margins, true, 0, false, true, 5, '', true);

        $pdf->SetFillColor(0,0,0);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFont('freesans', 'B', 23);
        $pdf->MultiCell($blockWidth, 10, 'RETURN', 1, 'C', true, 0, $xBase + $margins, 7, true, 0, false, true, 0);
        $pdf->MultiCell($blockWidth, 10, 'RETURN', 1, 'C', true, 0, $xBase + $margins, 109, true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 10);

        $pdf->SetTextColor(0,0,0);


        $pdf->SetFont('freesans', 'B', 8);

        $pdf->MultiCell($blockWidth,5, 'Nadawca:', 0, 'L', false, 1, $xBase + $margins, 18, true, 0, false, true, 0);

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(12,10, 'Konto / Login:', 0, 'L', false, 1, $xBase + $margins, 22, true, 0, false, true, 10, 'M');

        $pdf->SetFont('freesans', '', 14);
        if($item->user_id)
            $pdf->MultiCell(85,10, $item->user->login, 1, 'C', false, 1, 13, 22, true, 0, false, true, 10, 'M', true);

        $pdf->SetFont('freesans', 'B', 8);

        $pdf->MultiCell($blockWidth,5, 'Adres zwrotu:', 0, 'L', false, 1, $xBase + $margins, 33, true, 0, false, true, 0);

        $addressData = NULL;
        if($item->user_id != '')
            $addressData = $item->user->getReturnAddressModel();
        if($addressData == NULL)
            $addressData = $item->senderAddressData;

        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(20,4, 'Nazwa:', 0, 'L', false, 1, $xBase + $margins, 37, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(74,4, $addressData->name, 0, 'L', false, 1, 25, 37, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(20,4, 'Firma:', 0, 'L', false, 1, $xBase + $margins, 42, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(74,4, $addressData->company, 0, 'L', false, 1, 25, 42, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(20,4, 'Adres:', 0, 'L', false, 1, $xBase + $margins, 47, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(74,4, $addressData->address_line_1, 0, 'L', false, 1, 25, 47, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(20,4, $addressData->address_line_2, 0, 'L', false, 1, $xBase + $margins, 52, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(74,4, '', 0, 'L', false, 1, 25, 52, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(20,4, 'Kod-pocztowy:', 0, 'L', false, 1, $xBase + $margins, 57, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(20,4, $addressData->zip_code, 0, 'L', false, 1, 25, 57, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(10,4, 'Miasto:', 0, 'L', false, 1, 49, 57, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(39,4, $addressData->city, 0, 'L', false, 1, 60, 57, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(20,4, 'Kraj:', 0, 'L', false, 1, $xBase + $margins, 62, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(74,4, $addressData->country0->name, 0, 'L', false, 1, 25, 62, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(20,4, 'Data:', 0, 'L', false, 1, $xBase + $margins, 67, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(74,4, date('Y-m-d H:i'), 0, 'L', false, 1, 25, 67, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(60,4, 'Powód zwrotu (opcjonalnie):', 0, 'L', false, 1, $xBase + $margins, 72, true, 0, false, true, 4, '', true);
        $pdf->MultiCell(96,30, '', 1, 'L', false, 1, $xBase + $margins + 1, 77, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);


        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins, $yBase + $margins + 120, $xBase + $blockWidth + $margins, $yBase + $margins + 120);

        $pdf->write1DBarcode($item->local_id, 'C128',  $xBase + $margins + 25, $yBase + $margins + 120, '', 18, 0.4, $barcodeStyle, 'N');

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins, $yBase + $margins + 140, $xBase + $blockWidth + $margins, $yBase + $margins + 140);

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell($blockWidth,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, $xBase + $margins , $yBase + 140 + $margins, true, 0, false, true, 0);
        $pdf->MultiCell($blockWidth,6, 'Member of KAAB GROUP', 0, 'C', false, 1, $xBase + $margins , $yBase + 144 + $margins, true, 0, false, true, 0);

        if(!$ownPdf) {
            $pdf->Output('RL_'.$item->local_id.'.pdf', 'D');
        } else {
            return $pdf;
        }


    }
}