<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label' =>'List ' . $model->label(2), 'url'=>array('index')),
    array('label' =>'Create ' . $model->label(), 'url'=>array('create')),
    array('label' =>'View ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
);
?>

    <h1>Update <?php echo GxHtml::encode($model->label()) . ' "' . GxHtml::encode(GxHtml::valueEx($model)); ?>"</h1>

<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'modelTrs' => $modelTrs,
));
?>


<h2>Stat Map</h2>


<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'mass-assign-stat',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/statMapMapping/setNewMapping', ['type' => StatMapMapping::TYPE_POSTAL, 'stat_id' => $model->id]),
));
?>
<table>
    <thead>
    <tr>
        <td>Stat Map</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>  <?php echo CHtml::dropDownList('stat_map_id', $model->statMapMapping ? $model->statMapMapping->map_id : NULL, StatMap::mapNameList(), ['prompt' => '-']); ?></td>
    </tr>
    </tbody>
</table>
<?php
echo TbHtml::submitButton('Set',['size' => TbHtml::BUTTON_SIZE_LARGE]);
?>

<?php
$this->endWidget();
?>

