<?php

class CpostReprinterController extends Controller
{


    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_ADMIN.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($downloadPdf = false, $hash = false, $withCn22 = false)
    {
        Yii::app()->getModule('postal');


        if($downloadPdf && $hash)
        {
            $model = Postal::model()->findByAttributes(['hash' => $hash]);

            if($model->postal_operator_id == 78) {

                $imgs = CzechPostClient::generateLabelByPostal($model, $withCn22, 'CN', 'USD');
                $pdf = PDFGenerator::initialize();
                $pdf->setFontSubsetting(true);

                $pageW = 100;
                $xBase = 1;
                $yBase = 1;
                $blockWidth = $pageW - (2 * $xBase);
                $margins = 0;

                foreach ($imgs AS $key => $item) {
                    $pdf->AddPage('H', array(100, 150), true);
                    $pdf->Image('@' . $item, $xBase + $margins, $yBase + $margins, $blockWidth, 150, '', 'N', false);
                }


                $pdf->Output('cpost.pdf', 'D');
                exit;
            } else if ($model->postal_operator_id == 28) {

                $pdf = PDFGenerator::initialize();
                $pdf->setFontSubsetting(true);
                $pdf = PostalLabelPrinter::generateLabels([$model], PostalLabelPrinter::PDF_ROLL, true, false, false, false, $pdf);
                if($withCn22)
                    $pdf = CzechPostClient::generateCn22($model, 'CN', $pdf, $asImg = false, 'USD');
                $pdf->Output('cpost.pdf', 'D');
                exit;

            } else if ($model->postal_operator_id == 100) {

                $pdf = PDFGenerator::initialize();
                $pdf->setFontSubsetting(true);
                $pdf = PostalLabelPrinter::generateLabels([$model], PostalLabelPrinter::PDF_ROLL, true, false, false, false, $pdf);
                if($withCn22)
                    $pdf = CzechPostClient::generateCn22($model, 'CN', $pdf, $asImg = false, 'USD');
                $pdf->Output('cpost.pdf', 'D');
                exit;
            }


        }



        $model = false;
        if (isset($_POST['local_id'])) {


            $withCn22Checkbox = (!$_POST['with_cn22'] OR $_POST['with_cn22'] == 'false') ? false : true;

            /* @var $model PostalLabel */
            $model = PostalLabel::model()->find(['condition' => 'track_id = :track_id', 'params' => [':track_id' => $_POST['local_id']], 'order' => 'id DESC']);

            if($model === NULL)
            {
                $model = Postal::model()->find(['condition' => 'external_id = :external_id', 'params' => [':external_id' => $_POST['local_id']], 'order' => 'id DESC']);

            } else {
                $model = $model->postal;
            }

            if(!in_array($model->postal_operator_id, [78,28,100]))
                $model = NULL;

            if($model === NULL) {
                echo CJSON::encode(array('html' => TbHtml::blockAlert('error', 'Item not found!')));
                exit;
            }

            MyDump::dump('cpost_relabel.txt', $_POST['local_id']);

            if(intval($_POST['print_option']) === 1 && $model)
            {
                echo CJSON::encode(['redirect' => true, 'url' => Yii::app()->createAbsoluteUrl('/cpostReprinter/index', ['hash' => $model->hash, 'downloadPdf' => true, 'withCn22' => $withCn22Checkbox])]);

                exit;
            }

            if(intval($_POST['print_option']) === 2 && $model)
            {
                $ip = $_POST['ip'];
                $port = $_POST['port'];
                {

                    if($model->postal_operator_id == 78)
                        $imgs = CzechPostClient::generateLabelByPostal($model, $withCn22Checkbox, 'CN', 'USD', true);
                    else if ($model->postal_operator_id == 28) {
                        $imgs = PostalLabelPrinter::generateLabels([$model], PostalLabelPrinter::PDF_ROLL, true, true);
                        if($withCn22Checkbox)
                            $imgs[] = CzechPostClient::generateCn22($model, 'CN', NULL, $asImg = true, 'USD', true);
                    }
                    else if ($model->postal_operator_id == 100) {
                        $imgs = PostalLabelPrinter::generateLabels([$model], PostalLabelPrinter::PDF_ROLL, true, true, false, true);
                        if($withCn22Checkbox)
                            $imgs[] = CzechPostClient::generateCn22($model, 'CN', NULL, $asImg = true, 'USD', true);
                    }

                    foreach($imgs AS $key => $item)
                    {
                        Yii::app()->PrintServerQueque->instantRunImgString($item, $ip.':'.$port);
                    }

                    echo CJSON::encode(array('html' => TbHtml::blockAlert('success', 'Label sent to printer!')));
                    exit;
                }
            }

        }

        $this->render('index', array(
            'model' => $model,
            'withCn22' => $withCn22
        ));
    }


}