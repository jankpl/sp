<?php


class UserSendMailGroup extends CFormModel
{
    const TO_ALL = 1;
    const TO_ALL_ACTIVE = 2;

    public $title;
    public $text;
    public $user_ids;
    public $attachment;
    public $include_salesman = false;
    public $toAll = false;

    protected $_userModels = false;

    public function clear()
    {
        $this->user_ids = NULL;
        $this->_userModels = false;
    }

    public function setIds($ids)
    {
        $this->user_ids = $ids;
    }

    public function getUserList($separator = ',')
    {

        $list = [];

        foreach($this->getUserModels() AS $item)
            $list[] = $item->login;

        $list = implode($separator, $list);

        return $list;
    }

    public function getEmailsArray()
    {

        $list = [];

        foreach($this->getUserModels() AS $item)
            $list[$item->login] = $item->email;

        return $list;
    }

    public function getUserNumber()
    {
        return S_Useful::sizeof($this->getUserModels());
    }

    public function getUserModels()
    {
        if(!$this->_userModels)
        {
            $ids = explode(',', $this->user_ids);

            $models = User::model()->findAllByAttributes(['id' => $ids]);

            $this->_userModels = $models;
        }

        return $this->_userModels;
    }

    public function rules()
    {
        return array(
            array('title, text, user_ids', 'required'),
            array('include_salesman, toAll', 'boolean'),
            array('attachment', 'file', 'maxFiles' => 1, 'maxSize' => 5242880 /* 5 MB */, 'allowEmpty' => true ),
        );
    }


    public function attributeLabels()
    {

    }

    public function send()
    {

        /* @var $file CUploadedFile */
        $file = $this->attachment;
        $fileName = false;
        $filePath = false;

        if($file) {
            $fileName = $file->name;
            $filePath = $file->tempName;
        }

        $emails = $this->getEmailsArray();


        if($this->include_salesman)
        {
            $models = Salesman::model()->findAllByAttributes(['stat' => 1]);

            foreach($models AS $model)
                $emails[$model->name] = $model->email;
        }

        S_Mailer::sendToCustomerGroup($emails, $this->title, $this->text, $filePath, $fileName);
    }

    /**
     * @return []
     */
    public static function getAllUsersIds()
    {
        $cmd = Yii::app()->db->cache(60*5)->createCommand();
        $cmd->select('id')->from('user')->where('activated = 1');
        return $cmd->queryColumn();
    }

    /**
     * @return []
     */
    public static function getActiveUserIds()
    {
        $cmd = Yii::app()->db->cache(60*5)->createCommand();
        $cmd->select('id')->from('user')->where('activated = 1');
        $cmd->having('(SELECT COUNT(id) FROM courier WHERE user_id = user.id AND stat_map_id != 90 AND courier.date_entered > (NOW() - INTERVAL 1 MONTH)) > 100 OR (SELECT COUNT(id) FROM postal WHERE user_id = user.id AND stat_map_id != 90 AND postal.date_entered > (NOW() - INTERVAL 1 MONTH)) > 100');
        return $cmd->queryColumn();
    }

}
