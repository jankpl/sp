<?php

class PHPWord extends CComponent
{

	const A4_W = 11906;
	const A4_H = 16838;

	protected $loaded = false;
	protected $handler = NULL;
	protected $fileHandler = NULL;

	public function init() {
		require_once 'PHPWord/PhpWord/Autoloader.php';
		\PhpOffice\PhpWord\Autoloader::register();
	}

	public function loadFile(CUploadedFile $src)
	{

		$this->fileHandler = $src;

		if ($src->extensionName == 'docx')
			$reader = 'Word2007';
		else if ($src->extensionName == 'doc')
			$reader = 'MsDoc';
		else
			throw new Exception('Wrong file extension!');

		try
		{
			$this->handler = \PhpOffice\PhpWord\IOFactory::load($src->tempName, $reader);
			$this->loaded = true;

		}
		catch(Exception $ex)
		{
			return false;
		}
	}

	public function toPdf($savePath)
	{
        if(!$this->loaded)
			throw new Exception('Load file first!');

		define('PHPWORD_BASE_DIR', realpath(__DIR__));
		$tcpdfPath = realpath(PHPWORD_BASE_DIR . '/../extensions/tcpdf');

		\PhpOffice\PhpWord\Settings::setPdfRenderer(\PhpOffice\PhpWord\Settings::PDF_RENDERER_TCPDF, $tcpdfPath);
		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->handler , 'PDF');

		$xmlWriter->save($savePath);
	}


	public function isA4()
	{
		if(!$this->loaded)
			throw new Exception('Load file first!');

		try
		{
			$pageSizeW = NULL;
			$pageSizeH = NULL;

			if(!S_Useful::sizeof($this->handler->getSections()))
				return false;

			foreach($this->handler->getSections() AS $section)
			{
				$w = $section->getStyle()->getPageSizeW();
				$h = $section->getStyle()->getPageSizeH();

				if($pageSizeH && $pageSizeH != $h)
					return false;


				if($pageSizeW && $pageSizeW != $w)
					return false;


				$pageSizeH = $h;
				$pageSizeW = $w;
			}
		}
		catch(Exception $ex)
		{
			return false;
		}


		if($pageSizeH == self::A4_H AND $pageSizeW == self::A4_W)
			return true;
		else
			return false;
	}




}