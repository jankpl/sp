<?php

class Translation extends CModel
{
    public function attributeNames()
    {
        return array();
    }

    public static function loadFilesList($language_id)
    {
        $files = [];

        if($language_id != '')
        {

            $selectedLaguage = Language::model()->findByPk($language_id);

            $dirPath = Yii::app()->basePath.'/messages/';
            $dirPath .= $selectedLaguage->code;

            $di = new DirectoryIterator($dirPath);

            /* @var $item DirectoryIterator */
            foreach($di AS $item)
            {
                if($item->isFile())
                    $files[$item->getFilename()] = $item->getFilename();
            }

        }

        ksort($files);

        return $files;
    }

    public static function loadFileContentArray($language_id, $file)
    {
        $selectedLaguage = Language::model()->findByPk($language_id);

        $dirPath = Yii::app()->basePath.'/messages/';
        $dirPath .= $selectedLaguage->code;
        $path = $dirPath.'/'.$file;

        $array = require_once($path);

        return array($path, $array);
    }

    public static function saveTranslationToFile($path, $array)
    {
        $print = "<?php \n\r";
        $print .= 'return ';

        $print .= var_export($array, true);
        $print .= ';';

        return(file_put_contents($path, $print));
    }

}