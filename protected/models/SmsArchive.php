<?php

Yii::import('application.models._base.BaseSmsArchive');

class SmsArchive extends BaseSmsArchive
{
    const TYPE_COURIER = 0;
    const TYPE_POSTAL = 1;

    public $_local_id;

    protected $mode;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function setMode($type)
    {
        $this->mode = $type;
    }

    public function rules() {
        return array(
            array('courier_id, text, number', 'required'),
            array('courier_id', 'numerical', 'integerOnly'=>true),
            array('text', 'length', 'max'=>256),
            array('number', 'length', 'max'=>32),
            array('id, courier_id, date_entered, text, number, _local_id', 'safe', 'on'=>'search'),
        );
    }


    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),
        );
    }

    public function getTypeList()
    {
        return [
            self::TYPE_COURIER => 'Courier',
            self::TYPE_POSTAL => 'Postal',
        ];
    }

    public function getTypeName()
    {
        return self::getTypeList()[$this->type];
    }

    public static function addToArchive($courier_id, $number, $text, $type = self::TYPE_COURIER)
    {
        $model = new self;
        $model->courier_id = $courier_id;
        $model->number = $number;
        $model->text = $text;
        $model->type = $type;

        return $model->save();
    }


    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('courier_id', $this->courier_id);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('text', $this->text, true);

        $criteria->compare('number', $this->number, true);


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';

        if($this->mode == self::TYPE_COURIER) {
            $criteria->compare('courier.local_id', $this->_local_id, true);
            $criteria->compare('type', self::TYPE_COURIER);
            $model = $this->with('courier');
        }
        else if($this->mode == self::TYPE_POSTAL) {
            $criteria->compare('postal.local_id', $this->_local_id, true);
            $criteria->compare('type', self::TYPE_POSTAL);
            $model = $this->with('postal');
        }


        return new CActiveDataProvider($model, array(
            'criteria' => $criteria,
            'sort'=> $sort,
            'Pagination' => array (
                'PageSize' => 50,
            ),
        ));
    }


}