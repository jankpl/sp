<?php

class LinkerManager
{
    const BASE_URL = 'https://api.swiatprzesylek.linker.shop';
    const BASE_APP = '/core/v1';

    protected $_depotId = false;
    protected $_userId = false;

    public function getdepotId()
    {
        return $this->_depotId;
    }

    protected static function getShortNameByUser(User $user)
    {
        if(YII_LOCAL)
            return 'TESTSP'.$user->id;
        else
            return 'SP'.$user->id;
    }

    public static function init(User $user)
    {
        if(!$user->getApiAccessActive())
            throw new Exception('Customer must have API access active!');

        $model = new self;
        $model->_userId = $user->id;

        $CACHE = Yii::app()->cache;
        $CACHE_NAME = 'LINKER_MANAGER_USER_'.$user->id;

        $depotId = $CACHE->get($CACHE_NAME);

        if($depotId === false) {

            $clients = $model->listClients();

            if (!$clients)
                throw new Exception('Cannot connect to Linker!');

            $found = false;
            if(S_Useful::sizeof($clients->items))
                foreach ($clients->items AS $client) {
                    if ($client->shortName == self::getShortNameByUser($user)) {
                        $found = true;
                        $model->_depotId = $client->external_id;
                        break;
                    }
                }

            if (!$found) {
                $data = [
                    'name' => $user->getUsefulName() ? $user->getUsefulName() : $user->login,
                    'shortName' => self::getShortNameByUser($user),
                    'apiKey' => $user->apikey,
                ];

                $resp = $model->addClient($data);

                if ($resp->message)
                    throw new Exception($resp->message);
                else
                    $model->_depotId = $resp->external_id;
            }

            $CACHE->set($CACHE_NAME, $model->_depotId, 60*60);

        } else {
            $model->_depotId = $depotId;
        }

        return $model;
    }


    protected function _getToken()
    {

        $CACHE = Yii::app()->cache;
        $CACHE_NAME = 'LINKER_TOKEN';

        $token = $CACHE->get($CACHE_NAME);

        if($token === false) {

            $headers = [];
            $headers[] = 'Content-Type: application/json';
            $data = [
                'client_id' => '5_3zofwxyqke68oo4g0c00g804gw488c0k0w8cwccs4s40ckgcwg',
                'client_secret' => '3e79mqrnhjsw8kkcw8okgo8kc0sksckwskcgw8k4oc8os4wkg4',
                'grant_type' => 'client_credentials'
            ];

            $data = json_encode($data);

            $ch = curl_init(self::BASE_URL . '/oauth/token');
            curl_setopt_array($ch, [
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_POSTFIELDS => $data,
            ]);

            $resp = curl_exec($ch);
            $resp = json_decode($resp);
            $token = $resp->access_token;
            $CACHE->set($CACHE_NAME, $token, 60*15);
        }

        return $token;
    }

    protected function _request($method, $data = false, $assoc = false, $disableDumpToFile = false, $forcePUT = false)
    {
        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $this->_getToken();

        $ch = curl_init(self::BASE_URL.self::BASE_APP.$method);

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "UTF-8",
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ]);

        if($data) {

            if($forcePUT)
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            else
                curl_setopt($ch, CURLOPT_POST, true);

            $data = json_encode($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        $resp = curl_exec($ch);

        if(!$disableDumpToFile) {
            MyDump::dump('linkerManager.txt', 'REQ (' . $this->_userId . '/' . $this->_depotId . '): ' . self::BASE_URL . self::BASE_APP . $method . ' : ' . print_r($data, 1), true, true, true, false);
            MyDump::dump('linkerManager.txt', 'RESP (' . $this->_userId . '/' . $this->_depotId . '): ' . self::BASE_URL . self::BASE_APP . $method . ' : ' . print_r($resp, 1));
        }

        $resp = json_decode($resp, $assoc);

        return $resp;
    }

    public function addClient($data)
    {
        return $this->_request('/clients', $data);
    }

    public function listClients()
    {
        return $this->_request('/clients', false, false, true);
    }

    public function listIntegrations()
    {
        return $this->_request('/configurations/integration/services', false, false, true);
    }

    public function getForm($integration, $step = 1, $internalName = NULL)
    {
        return $this->_request('/configurations/integration/forms/'.$integration.'?depotId='.$this->_depotId.'&internalName='.$internalName.'&step='.$step, false, false);
    }

    public function submitForm(array $data, $step2Id = false)
    {
        if($step2Id)
            return $this->_request('/configurations/'.$step2Id, $data, true, false, true);
        else
            return $this->_request('/configurations/creates', $data, true);
    }

    public function listMyIntegrations()
    {
        $resp = $this->_request('/configurations?filters%5BdepotId%5D='.$this->_depotId);
        $resp = $resp->items;
        foreach($resp AS $key => $item)
        {
            if($item->type != 'integration')
                unset($resp[$key]);
        }

        return $resp;
    }

    public function createScheudle($internalName)
    {
        $data = '{"type":"ecommerce_orders_import","schedules":[{"type":"cron","expression":"0 * * * *"},{"type":"cron","expression":"20 * * * *"},{"type":"cron","expression":"40 * * * *"}],"parameters":[{"name":"'.$internalName.'"}],"templateUrl":"app/jobs/views/templates/form-ecommerce_orders_import.html","id":"'.$internalName.'"}';
        $data = json_decode($data); // as it will be encoded in next method

        return $this->_request('/integrations/starts', $data, true);
    }

}
