<?php

class CourierController extends Controller {

    public function filters(){
        return array(
            'accessControl',
//            'backend.filters.GridViewHandler' //path to GridViewHandler.php class
        );
    }

    public function accessRules() {
        return array(
//            array('allow',
//                'users'=>array('@'),
//
//            ),
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => ['exportToFileAll', 'searchByRemote','generateLabelsByGridView','generateReturnLabel','generateAckCardsByGridView','carriageTicket', 'view', 'index', 'admin', 'labelPrinter'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

//    public function actionCreateExternal()
//    {
//        if(isset($_POST['save_no_validation']))
//            $mode = Courier::MODE_SAVE_NO_VALIDATE;
//        else if(isset($_POST['validate']))
//            $mode = Courier::MODE_JUST_VALIDATE;
//        else
//            $mode = Courier::MODE_SAVE;
//
//        $model = new Courier;
//        $model->courierTypeExternal = new CourierTypeExternal();
//        $model->senderAddressData = new AddressData();
//        $model->receiverAddressData = new AddressData();
//
//        if (isset($_POST['Courier'])) {
//            $model->setAttributes($_POST['Courier']);
//
//            $senderAddressData = new AddressData();
//            $senderAddressData->setAttributes($_POST['AddressData']['sender']);
//            $model->senderAddressData = $senderAddressData;
//
//            $receiverAddressData = new AddressData;
//            $receiverAddressData->setAttributes($_POST['AddressData']['receiver']);
//            $model->receiverAddressData = $receiverAddressData;
//
//            $courierTypeExternal = new CourierTypeExternal;
//            $courierTypeExternal->setAttributes($_POST['CourierTypeExternal']);
//            $model->courierTypeExternal = $courierTypeExternal;
//
//            if($mode == Courier::MODE_JUST_VALIDATE)
//            {
//                $model->validateExternalType($mode);
//            } else {
//
//                if ($model->saveExternalType(true,$mode)) {
//
//                    $this->redirect(array('/courier/courier/view',
//                        'id' => $model->id
//                    ));
//                }
//            }
//        }
//
//        $this->render('createExternal', array( 'model' => $model));
//    }

    public function actionUpdateExternal($id)
    {

        $model = Courier::model()->findByPk($id);

        if($model ===  NULL)
            throw new CHttpException(404);

        if($model->courierTypeExternal === NULL)
            throw new CHttpException(403, 'You can edit only external type packages!');


        //$model->courierTypeExternal = new CourierTypeExternal();
        if($model->senderAddressData == NULL)
            $model->senderAddressData = new AddressData('optional');


        if (isset($_POST['Courier'])) {
            $model->setAttributes($_POST['Courier']);

            $senderAddressData = new AddressData('optional');
            $senderAddressData->setAttributes($_POST['AddressData']['sender']);
            $model->senderAddressData = $senderAddressData;

            $receiverAddressData = new AddressData;
            $receiverAddressData->setAttributes($_POST['AddressData']['receiver']);
            $model->receiverAddressData = $receiverAddressData;

            $courierTypeExternal = new CourierTypeExternal;
            $courierTypeExternal->setAttributes($_POST['CourierTypeExternal']);
            $model->courierTypeExternal = $courierTypeExternal;

            if ($model->saveExternalType()) {

                $this->redirect(array('/courier/courier/view',
                    'id' => $model->id
                ));
            }
        }

        $this->render('updateExternal', array( 'model' => $model));
    }

    public function actionCreate() {
        $model = new Courier;
        $model->senderAddressData = new AddressData();
        $model->receiverAddressData = new AddressData();

        if (isset($_POST['Courier'])) {
            $model->setAttributes($_POST['Courier']);

            $senderAddressData = new AddressData;
            $senderAddressData->setAttributes($_POST['AddressData']['sender']);
            $model->senderAddressData = $senderAddressData;

            $receiverAddressData = new AddressData;
            $receiverAddressData->setAttributes($_POST['AddressData']['receiver']);
            $model->receiverAddressData = $receiverAddressData;


            if ($model->saveWithAddressData()) {

                $this->redirect(array('/courier/courier/view',
                    'id' => $model->id
                ));
            }
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionAssingStatusByGridView()
    {

        if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER]))
            throw new Exception(403);


        /* @var $model Courier */
        $i = 0;
        if (isset($_POST['Courier'])) {

            $courier_stat_id = (int) $_POST['courier_stat_id'];
            $courier_ids =  $_POST['Courier']['ids'];

            $courier_ids = explode(',', $courier_ids);

            $date = $_POST['stat_date'];
            $location = $_POST['location'];

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {

                $models = [];
                foreach($courier_ids AS $id)
                {
                    $model = Courier::model()->findByPk($id);

                    if($model !== null)
                        array_push($models, $model);

                }

                $result = Courier::changeStatForGroup($models, $courier_stat_id, $date, $location);

                if($result)
                {

                    Yii::app()->user->setFlash('success','Udało się zmienić statusy. Liczba pozycji: '.$result);
                }
                else
                {
                    Yii::app()->user->setFlash('error','Nie udało się zmienić statusu');
                }
            }
        }

        if(!S_Useful::sizeof($courier_ids))
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');

        $this->redirect(Yii::app()->createUrl('/courier/courier/admin'));

    }

    public function actionSearchByRemote()
    {
        $remoteId = $_POST['remoteId'];
        $remoteId = trim($remoteId);

        if($remoteId == '')
        {
            Yii::app()->user->setFlash('notice', 'Please fill remote Id number');
            $this->redirect(['/courier/courier/index']);
            Yii::app()->end();
        }

        $response = CourierExternalManager::findCourierIdsByRemoteId($remoteId, true);

        $n = S_Useful::sizeof($response);
        if(!$n) {
            Yii::app()->user->setFlash('notice', '0 packages found!');
            $this->redirect(['/courier/courier/index']);
            Yii::app()->end();
        }
        else
        {

            $html = '<ul>';
            foreach($response AS $courier)
            {
                $html .= '<li>'.CHtml::link($courier->local_id, ['/courier/courier/view', 'id' => $courier->id]).'</li>';
            }
            $html .= '</ul>';
            Yii::app()->user->setFlash('success', $n.' package(s) found for ID "'.$remoteId.'":<br/> '.$html);
            $this->redirect(['/courier/courier/index']);
            Yii::app()->end();

        }

    }

    public function actionExportToFileAll()
    {
        $period = $_POST['period'];

        if($period == '' or strlen($period) < 6)
            throw new CHttpException(404);

//        $period = explode('_', $period);
//        $part = isset($period[1]) ? $period[1] : false;
//        $period = isset($period[0]) ? $period[0] : $_POST['period'];

        $period = substr($period, 2,4).'-'.substr($period, 0,2).'-';

//        $firstPartEnd = $period.'08';
//        $secondPartEnd = $period.'16';
//        $thirdPartEnd = $period.'24';

//        echo '1:' . round(memory_get_usage() / (1024*1024)) .'MB<br/>';

        // with() are partly omited due to memory limits
//        $models = Courier::model();
//            ->with('courierTypeInternal')
//            ->with('courierTypeDomestic')
//            ->with('senderAddressData')
//            ->with('receiverAddressData')
//            ->with('courierTypeInternalSimple')
//            ->with('courierTypeInternalSimple.courierExtOperatorDetails.courierLabelNew')
//            ->with(['courierTypeDomestic.courierLabelNew' => ['alias' => 'ctd']])
//            ->with('courierTypeDomestic.courierDomesticOperator')
//            ->with(['courierTypeInternal.commonLabel.courierLabelNew' => ['alias' => 'cticl']])
//            ->with(['courierTypeInternal.pickupLabel.courierLabelNew' => ['alias' => 'ctipl']])
//            ->with(['courierTypeInternal.deliveryLabel.courierLabelNew' => ['alias' => 'ctidl']])
//            ->with(['courierTypeOoe.courierLabelNew' => ['alias' => 'cto']]);

        $cmd = Yii::app()->db->createCommand();
        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
        {
            $models = $cmd->select('courier.id')->from('courier')->join('user', 'courier.user_id = user.id')->where('courier.date_entered LIKE :date AND user.salesman_id = :salesman_id', [':date' => $period . '%', ':salesman_id' => Yii::app()->user->model->salesman_id])->queryColumn();
        } else if(AdminRestrictions::isUltraAdmin())  {
            $models = $cmd->select('id')->from('courier')->where('date_entered LIKE :date', [':date' => $period . '%'])->queryColumn();
        } else {
            $models = $cmd->select('courier.id')->from('courier')->join('user', 'courier.user_id = user.id')->where('courier.date_entered LIKE :date AND user.partner_id = :partner_id', [':date' => $period . '%', ':partner_id' => Yii::app()->user->model->partner_id])->queryColumn();
        }

//
//        if($nm)
//        {


//        } else {
//
////
//            if ($part == 4)
//                $models = $models->findAll('t.date_entered LIKE :date AND t.date_entered > :date2', [':date' => $period . '%', ':date2' => $thirdPartEnd]);
//            else if ($part == 3)
//                $models = $models->findAll('t.date_entered LIKE :date AND t.date_entered > :date2 AND t.date_entered <= :date3', [':date' => $period . '%', ':date2' => $secondPartEnd, ':date3' => $thirdPartEnd]);
//            else if ($part == 2)
//                $models = $models->findAll('t.date_entered LIKE :date AND t.date_entered > :date2 AND t.date_entered <= :date3', [':date' => $period . '%', ':date2' => $firstPartEnd, ':date3' => $secondPartEnd]);
//            else if ($part == 1)
//                $models = $models->findAll('t.date_entered LIKE :date AND t.date_entered <= :date2', [':date' => $period . '%', ':date2' => $firstPartEnd]);
//            else
//                $models = $models->findAll('t.date_entered LIKE :date', [':date' => $period . '%']);
//
//        }

//        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
//        {
//            foreach($models AS $key => $item)
//                if($item->user->salesman_id != Yii::app()->user->model->salesman_id)
//                    unset($models[$key]);
//        }


        if(!S_Useful::sizeof($models)) {
            Yii::app()->user->setFlash('notice', '0 packages found!');
            $this->redirect(['/courier/courier/index']);
            Yii::app()->end();
        } else {
            S_CourierIO::exportToXls($models, true);
        }
    }

    public function actionDeleteLastStatusByGridView()
    {

        if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER]))
            throw new Exception(403);

        /* @var $model Courier */
        $i = 0;
        if (isset($_POST['Courier']))
        {

            $courier_ids =  $_POST['Courier']['ids'];

            $courier_ids = explode(',', $courier_ids);

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {

                $models = [];
                foreach($courier_ids AS $id)
                {
                    $model = Courier::model()->findByPk($id);
                    if($model === null)
                        continue;

                    array_push($models, $model);

                    $i++;
                }
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/courier/courier/admin'));
        } else {
            $result = Courier::deleteLastStatForGroup($models);

            if($result)
            {

                Yii::app()->user->setFlash('success','Udało się zmienić statusy. Liczba pozycji: '.$result);
            }
            else
            {
                Yii::app()->user->setFlash('error','Nie udało się zmienić statusu');
            }

            $this->redirect(Yii::app()->createUrl('/courier/courier/admin'));
        }
    }

    public function actionRefreshStatMapByGridView()
    {
        /* @var $model Courier */
        $i = 0;
        if (isset($_POST['Courier']))
        {

            $courier_ids =  $_POST['Courier']['ids'];

            $courier_ids = explode(',', $courier_ids);

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {

                foreach($courier_ids AS $id)
                {
                    $model = Courier::model()->findByPk($id);
                    if($model === null)
                        continue;

                    if($model->refreshStatMap())
                        $i++;
                }
            }
        }

        if($i)
        {

            Yii::app()->user->setFlash('success','Stat maps refreshed. Number of items affected: '.$i);
        }
        else
        {
            Yii::app()->user->setFlash('error','Stat maps NOT refreshed');
        }

        $this->redirect(Yii::app()->createUrl('/courier/courier/admin'));

    }

    public function actionExportToFileByGridView()
    {
        /* @var $model Courier */
        $i = 0;
        if (isset($_POST['Courier']))
        {


            $courier_ids =  $_POST['Courier']['ids'];

            $courier_ids = explode(',', $courier_ids);

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {

                $models = [];
                foreach($courier_ids AS $id)
                {
                    $model = Courier::model()->findByPk($id);
                    if($model === null)
                        continue;

                    array_push($models, $model);

                    $i++;
                }
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/courier/courier/admin'));
        } else {
            S_CourierIO::exportToXls($models);
        }
    }

    public function actionDeleteExternal($id)
    {
        /* @var $model Courier */
        $model = Courier::model()->findByPk($id);
        if($model == NULL)
            throw new CHttpException(404);


        $return = $model->deleteExternalType();
        if (!Yii::app()->getRequest()->getIsAjaxRequest())
        {
            if($return)
                Yii::app()->user->setFlash('success', 'Package has been deleted');
            else
                Yii::app()->user->setFlash('error', 'Package has not been deleted');

            $this->redirect(Yii::app()->createUrl('/courier/courier/admin'));
        }

    }

    public function actionDeleteLastStatus($id)
    {
        /* @var $model Courier */

        $model = Courier::model()->findByPk($id);
        if($model == NULL)
            throw new CHttpException(404);

        $return = $model->deleteLastStatus();

        if($return)
            Yii::app()->user->setFlash('success', 'Last stat has been deleted');
        else
        {
            Yii::app()->user->setFlash('error', 'Last stat has not been deleted');
        }

        $this->redirect(Yii::app()->createUrl('/courier/courier/view', array('id' => $model->id)));


    }

    public function actionGenerateLabelsByGridView()
    {
        /* @var $model Courier */
        $i = 0;
        if (isset($_POST['Courier']))
        {
            $type = $_POST['Courier']['type'];
            $courier_ids =  $_POST['Courier']['ids'];

            $courier_ids = explode(',', $courier_ids);

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {

                $models = [];
                foreach($courier_ids AS $id)
                {
                    $model = Courier::model()->findByPk($id);
                    if($model === null)
                        continue;

                    if($model->courierTypeOoe !== NULL && $model->courierTypeOoe->courier_label_new_id == NULL)
                        continue;

                    array_push($models, $model);

                    $i++;
                }
            }
        }


        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/courier/courier/admin'));
        } else {

            LabelPrinter::generateLabels($models, $type, true);
        }
    }

    public function actionGenerateReturnLabel($urlData)
    {
        /* @var $model Courier */
        $model = Courier::model()->findByAttributes(['hash' => $urlData]);

        if($model == NULL)
            throw new CHttpException(404);

        ReturnLabel::generate($model);
    }


    public function actionGenerateAckCardsByGridView()
    {
        /* @var $model Courier */

        if (isset($_POST['Courier']))
        {
            $courier_ids =  $_POST['Courier']['ids'];
            $models = Courier::model()
                ->with('senderAddressData')
                ->with('receiverAddressData')
                ->with('courierTypeInternal')
                ->with('courierTypeInternal.order')
                ->with('courierTypeInternal.pickupLabel')
                ->with(array(
                    "courierTypeInternal.pickupLabel.courierLabelNew" => array(
                        'alias' => 'internalPickupLabelNew',
                    ),
                ))
                ->with('courierTypeInternal.commonLabel')
                ->with(array(
                    "courierTypeInternal.commonLabel.courierLabelNew" => array(
                        'alias' => 'internalCommonLabelNew',
                    ),
                ))
                ->with('courierTypeOoe')
                ->with('courierTypeExternal')
                ->with('courierTypeInternalSimple')
                ->with('courierTypeDomestic')
                ->with('courierTypeDomestic.courierDomesticOperator')
                ->with('courierTypeDomestic.courierLabelNew')
                ->with(array(
                    "courierTypeDomestic.courierLabelNew" => array(
                        'alias' => 'domesticLabelNew',
                    ),
                ))
                ->with('courierTypeDomestic.courierLabelNew')
                ->findAllByAttributes(['id' => explode(',', $courier_ids)], 'courier_stat_id != :stat AND courier_stat_id != :stat2 AND courier_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', [':stat' => CourierStat::CANCELLED, ':stat2' => CourierStat::CANCELLED_PROBLEM, ':stat3' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_REQUEST, ':uc2' => Courier::USER_CANCEL_CONFIRMED, ':uc3' => Courier::USER_CANCEL_WARNED]);

        }

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/courier/courier/admin'));
        } else {
            CourierAcknowlegmentCard::generateCardMulti($models);
        }
    }

    public function actionChangeStat()
    {

        if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER]))
            throw new Exception(403);

        /* @var $model Courier */
        if (isset($_POST['Courier'])) {

            $model = Courier::model()->findByPk($_POST['Courier']['id']);

            if($model === null)
                throw new Exception('Błąd!');

            $date = $_POST['status_date'];
            $location = $_POST['location'];

//            $statIdBefore = $model->courier_stat_id;

            if($model->changeStat($_POST['Courier']['courier_stat_id'], '', $date, false, $location, false, false, false, true)) {
//                $statIdAfter = $model->courier_stat_id;

//                $model->addToLog('Status changed form #'.$statIdBefore.' to #'.$statIdAfter, CourierLog::CAT_INFO, Yii::app()->user->name);
                $this->redirect(array('/courier/courier/view', 'id' => $model->id));
            }
            else
                throw new Exception("Błąd!");
        } else
            throw new CHttpException('404');

    }

    public function actionAssingCarrier()
    {
        /* @var $model Courier */


        if (isset($_POST['Courier'])) {

            $model = Courier::model()->findByPk($_POST['Courier']['id']);

            if($model === null)
                throw new Exception('Błąd!');

            if($model->assignCarrier($_POST['Courier']['courier_carrier_id'],$_POST['Courier']['outside_id'], $_POST['Other']['actualizeStatusWithAssignCourrier']))
                $this->redirect(array('/courier/courier/view',
                    'id' => $model->id,
                ));
            else
                throw new Exception("Błąd!");
        } else
            throw new CHttpException('404');

    }


    public function actionFastAssignCarrier()
    {

        $model = new Courier();

        if (isset($_POST['Courier'])) {


            $model = Courier::model()->find('local_id=:local_id', array(':local_id' => $_POST['Courier']['local_id']));

            if ($model !== null AND $model->assignCarrier($_POST['Courier']['courier_carrier_id'], $_POST['Courier']['outside_id'], true))
            {
                Yii::app()->user->setFlash('success','Zapisano!');
            }
            else
            {
                Yii::app()->user->setFlash('error','Zapis nie powiódł się!');

                $model = new Courier;
                $model->courier_carrier_id = $_POST['Courier']['courier_carrier_id'];
            }
            //$this->redirect(array('courier/fastAssignCarrier'));
        }

        $this->render('fastAssingCarrier', array(
            'model' => $model,
        ));

    }


    public function actionAssignCarrierFromFile()
    {

        $S_Courier_Ids_From_File = Array();
        $fileModel = new F_UploadFile();

        if(isset($_POST['F_UploadFile']))
        {
            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if(!$fileModel->validate())
            {
                $this->render('assignCarrierFromFile', array(
                    'models' => $S_Courier_Ids_From_File,
                    'fileModel' => $fileModel,
                ));
                return;
            }

        }

        if($fileModel->file !== null)
        {
            $attributes = Array(
                'local_id' => 0,
                'remote_id' => 1,
            );

            $courierIdsAttributes = $fileModel->returnCSVAsArray($attributes);


            if(S_Useful::sizeof($courierIdsAttributes))
            {
                foreach($courierIdsAttributes AS $item)
                {
                    $S_Courier_Id_From_File = new S_Courier_Id_From_File();
                    $S_Courier_Id_From_File->local_id = $item['local_id'];
                    $S_Courier_Id_From_File->remote_id = $item['remote_id'];
                    $S_Courier_Id_From_File->validate();

                    array_push( $S_Courier_Ids_From_File,  $S_Courier_Id_From_File);
                }
            }
            else
                Yii::app()->user->setFlash('error','Nie znaleziono treści w pliku!');


        }


        if(isset($_POST['S_Courier_Id_From_File']))
        {

            $S_Courier_Ids_From_File = Array();

            $error = false;

            $i = 0;
            $transaction = Yii::app()->db->beginTransaction();
            foreach($_POST['S_Courier_Id_From_File'] AS $key => $item)
            {
                $S_Courier_Ids_From_File[$key] = new S_Courier_Id_From_File();
                $S_Courier_Ids_From_File[$key]->attributes = $item;


                $S_Courier_Ids_From_File[$key]->validate();
                if(!$S_Courier_Ids_From_File[$key]->save())
                    $error = true;
                else $i++;
            }

            if(!$error)
            {
                Yii::app()->user->setFlash('success','Zaktualizowano ID dla przesyłek: '.$i);

                $transaction->commit();
                $this->redirect(array('/courier/courier/admin'));
                return;
            }

        }


        $this->render('assignCarrierFromFile', array(
            'models' => $S_Courier_Ids_From_File,
            'fileModel' => $fileModel,
        ));


    }


    public function actionCarriageTicket($id, $type = NULL, $mode = NULL)
    {
        /* @var Courier $model */

        $model = Courier::model()->findByPk($id);

        if($model === null) throw new CHttpException('404');


        if($model->courierTypeInternal) {
            $internalGetSecond = false;
            if($mode == CourierTypeInternal::GET_LABEL_MODE_BOTH)
            {
                LabelPrinter::generateLabelsForSingleInternal($model->courierTypeInternal, false, CourierTypeInternal::GET_LABEL_MODE_BOTH);
            } else {

                if ($mode == CourierTypeInternal::GET_LABEL_MODE_LAST)
                    $internalGetSecond = true;
                LabelPrinter::generateLabels($model, $type, false, $internalGetSecond);
            }
        }
        else if($model->courierTypeOoe && $model->courierTypeOoe->courier_label_new_id === NULL)
            $model->courierTypeOoe->generateCarriageTicket();
        else
            LabelPrinter::generateLabels($model, $type);


    }

    public function actionView($id = NULL, $urlData = NULL) {

        if($urlData!= NULL)
            $model = Courier::model()->with('stat')->with('courierStatHistories.currentStat')->with('user')->with('senderAddressData')->with('receiverAddressData')->with('courierTypeInternal')->with('courierTypeDomestic')->with('courierTypeOoe')->with('courierTypeInternalSimple')->with('courierTypeExternal')->findByAttributes(array('local_id' => $urlData));
        else
            $model = Courier::model()->with('stat')->with('courierStatHistories.currentStat')->with('user')->with('senderAddressData')->with('receiverAddressData')->with('courierTypeInternal')->with('courierTypeDomestic')->with('courierTypeOoe')->with('courierTypeInternalSimple')->with('courierTypeExternal')->findByPk($id);

        if($model === null)
            throw new CHttpException('404');


        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR && $model->user->salesman_id != Yii::app()->user->model->salesman_id)
            throw new CHttpException(403);


        $this->render('view', array(
            'model' => $model,
        ));
    }

    public function actionIndex() {

        /* @var $model Courier */

        $this->render('index', array(

        ));

        //$this->redirect(array('courier/admin'));
    }

    public function actionAdmin() {


        $preFilter = [];
        $preModels = false;
        if(isset($_POST['preFilter']) && $_POST['preFilter'] != '' OR isset($_POST['preFilterClear'])) {

            if(isset($_POST['preFilterClear']))
            {
                $preFilter = [];
            }
            else
            {
                $preFilter = $_POST['preFilter'];
                $preFilter = explode(PHP_EOL, $preFilter);

                foreach ($preFilter AS $key => $item)
                    $preFilter[$key] = trim($item);

                $preFilter = array_filter($preFilter);

                $preModels = CourierExternalManager::findCourierIdsByRemoteId($preFilter, true);
            }
        }


        $this->render('admin', array(
            'preFilter' => implode(PHP_EOL, $preFilter),
            'preModels' => $preModels,
        ));
    }

    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewCourier($preModels = false, $multiselectAttributes = false, $dataRangeScript = false){
        $model = new _CourierGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }


        if (isset($_GET['CourierGridView']))
            $model->setAttributes($_GET['CourierGridView']);

        $pageSize = Yii::app()->user->getState('pageSize', 50);
        $headers = [];
        $headers[] = array(
            'text'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10 ,20=>20 ,50=>50 , 100=>100, 250 => 250, 500 => 500),array(
                'onchange'=>"$.fn.yiiGridView.update('courier',{ data:{pageSize: $(this).val() }})",
            )),

            'colspan'=>18,
            'options'=>['style' => 'text-align: right;']
        );


        if(is_array($preModels))
        {
            if(S_Useful::sizeof($preModels))
            {
                $preIds = CHtml::listData($preModels, 'id', 'id');
                $model->_multiIds = implode(',', $preIds);
            }
            else
                $model->_multiIds = -1;
        }

        $this->widget('BackendGridView', array(
            'id' => 'courier',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>2,
            'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => $multiselectAttributes, 'afterAjaxUpdate' => true, 'additionScript' => $dataRangeScript), true),
            'selectionChanged'=>'function(id){  
                var number = $("#courier").selGridView("getAllSelection").length;
                $(".selected-no").html(number); 
         }',
            'template'=>"<div class=\"text-center\">{pager}</div>\n{summary}\n{items}\n{summary}\n<div class=\"text-center\">{pager}</div>",
            'htmlOptions' => ['style' => 'table-layout: fixed'],
            'addingHeaders' => array(
                $headers,
            ),
            'columns' => array(
                [
                    'name' => '_multiIds',
                    'htmlOptions'=>array('style'=>'display:none;'),
                    'headerHtmlOptions'=>array('style'=>'display:none;'),
                    'filterHtmlOptions'=>array('style'=>'display:none;'),
                ],
                array(
                    'name'=>'local_id',
                    'header'=>'# local ID',
                    'type'=>'raw',
                    'value'=>'CHtml::link($data->local_id,array("/courier/courier/view/", "id" => $data->id), ["target" => "_blank"])',

                ),
                array(
                    'name' => '__type',
                    'header'=>'Type',
                    'type'=>'raw',
                    'value'=> '"<p class=\"vertical\">".substr($data->typeName, 0, 5)."</p>"',
//                    'value'=> '$data->typeName',
                    'filter' => Courier::getActiveTypes(),

                ),
                array(
                    'name'=>'__sender_country_id',
                    'header'=>'Sender country',
                    'value'=>'$data->senderAddressData->country0->code',
//                    'filter'=>CountryList::getActiveCountries(true),
                    'filter'=> CHtml::activeDropDownList($model, '__sender_country_id', CountryList::getActiveCountries(true), array('size' => 1, 'id' => 'sender-country-selector', 'multiple' => true)),
                ),
                array(
                    'name' => '__sender_usefulName',
                    'header' => 'Sender',
                    'value' => '$data->senderAddressData->getUsefulName(true)',
                ),
                array(
                    'name'=>'__receiver_country_id',
                    'header'=>'Receiver country',
                    'value'=>'$data->receiverAddressData->country0->code',
//                    'filter'=>CountryList::getActiveCountries(true),
                    'filter'=> CHtml::activeDropDownList($model, '__receiver_country_id', CountryList::getActiveCountries(true), array('size' => 1, 'id' => 'receiver-country-selector', 'multiple' => true)),
                ),
                array(
                    'name' => '__receiver_usefulName',
                    'header' => 'Receiver',
                    'value' => '$data->receiverAddressData->getUsefulName(true)',
                ),
                array(
                    'name'=>'date_entered',
                    'header'=>'Created',
                    'value' => 'str_replace(" ", "<br/><i>", substr($data->date_entered, 2 ,14))."</i>"',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'text-align: right;'),
                ),
                array(
                    'name'=>'date_updated',
                    'header'=>'Updated',
                    'value' => 'str_replace(" ", "<br/><i>", substr($data->date_updated, 2 ,14))."</i>"',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'text-align: right;'),
                ),
                array(
                    'name'=>'date_activated',
                    'header'=>'Activated',
                    'value' => 'str_replace(" ", "<br/><i>", substr($data->date_activated, 2 ,14))."</i>"',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'text-align: right;'),
                ),
                array(
                    'name'=>'date_delivered',
                    'header'=>'Delivered',
                    'value' => 'str_replace(" ", "<br/><i>", substr($data->date_delivered, 2 ,14))."</i>"',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'text-align: right;'),
                ),
//                array(
//                    'name'=>'__user_login',
//                    'header'=>'User',
//                    'value' => '$data->user->login',
//                ),
                array(
                    'name'=>'user_id',
                    'header'=>'User',
                    'value' => '$data->user->login',
//                    'filter' => CHtml::listData(User::model()->findAll(['order' => 'login ASC']),'id', 'login'),
                    'filter'=> CHtml::activeDropDownList($model, 'user_id', CHtml::listData(User::model()->findAll(['order' => 'login ASC']),'id', 'login'), array('size' => 1, 'id' => 'user-selector', 'multiple' => 'true',)),
                    'htmlOptions' => [
                        'width' => '100',
                    ],
                ),
//                array(
//                    'name' => 'daysFromStatusChange',
//                    'header' => 'Status age (days)',
//                    'filter' => false,
//                ),
                array(
                    'name'=>'__internal_operator',
                    'header'=>'Operator',
                    'value'=>'$data->getIntOperator()',
//                    'filter' => CHtml::listData(_CourierGridView::getOperatorsList(),'id', 'name', 'type'),
                    'filter'=> CHtml::activeDropDownList($model, '__internal_operator', CHtml::listData(_CourierGridView::getOperatorsList(),'id', 'name', 'type'), array('size' => 1, 'id' => 'operator-selector','multiple' => 'true')),
                    'headerHtmlOptions' => array('style' => 'width: 50px;'),
                    'htmlOptions' => array('style' => 'text-align: center; word-break: normal;'),
                ),
                array(
                    'name'=>'__internal_pickup_type',
                    'header'=>'Pickup type',
                    'value'=>'$data->getIntWithPickup()',
                    'filter' => CourierTypeInternal::getWithPickupList(false),
                    'headerHtmlOptions' => array('style' => 'width: 50px;'),
                ),
                array(
                    'name'=>'courier_stat_id',
                    'value'=>'$data->courier_stat_id."<br/>".$data->stat->name',
//            'filter'=>GxHtml::listDataEx(CourierStat::model()->findAllAttributes(null, true)),
                    'filter'=> CHtml::activeDropDownList($model, 'courier_stat_id', CHtml::listData(CourierStat::getStats(true),'id','adminStatNameShortWithId'), array('multiple' => 'true', 'size' => 1, 'id' => 'stat-selector')),
                    'type'=>'raw',
                    'htmlOptions' => array('style' => 'text-align: right;'),
                ),
                array(
                    'name'=>'stat_map_id',
                    'value'=>'$data->stat_map_id."<br/>".StatMap::getMapNameById($data->stat_map_id)',//
                    'filter'=> StatMap::mapNameList(),
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'text-align: right; word-break: normal;'),
                ),
//                [
//                    'name' => 'cod_value',
//                    'filter' => [1 => '0', 2 => '>0'],
//                ],
//                'location',
//                'packages_number',
                [
                'name' => 'user_cancel',
                'filter' => Courier::getUserCancelList(),
                'value' => 'Courier::getUserCancelListShort()[$data->user_cancel]',
                ],
                [
                    'name' => 'source',
                    'filter' => Courier::getSourceList(),
//                    'value' => '$data->sourceName',
            'type'=>'raw',
            'value'=> '"<p class=\"vertical\">".$data->sourceName."</p>"',
        ],
                /*array(
                    'class' => 'CButtonColumn',
                ),*/
                array(
                    'class'=>'CCheckBoxColumn',
                    'id'=>'selected_rows'
                ),

            ),
        ));
    }

    public function actionCreateExternalFromFile()
    {
        $S_Courier_Ids_From_File = Array();
        $fileModel = new F_UploadFile();

        // mapping attributes to columns in file
        $attributes = Array(
            'courier_stat' => 0,
            'package_weight' => 1,
            'package_size_l' => 2,
            'package_size_w' => 3,
            'package_size_d' => 4,
            'packages_number' => 5,
            'package_value' => 6,
            'package_content' => 7,
            'user_id' => 8,

            'courier_external_operator_id' => 9,
            'external_id' => 10,

            'sender_name' => 11,
            'sender_company' => 12,
            'sender_country' => 13,
            'sender_zip_code' => 14,
            'sender_city' => 15,
            'sender_address_line_1' => 16,
            'sender_address_line_2' => 17,
            'sender_tel' => 18,

            'receiver_name' => 19,
            'receiver_company' => 20,
            'receiver_country' => 21,
            'receiver_zip_code' => 22,
            'receiver_city' => 23,
            'receiver_address_line_1' => 24,
            'receiver_address_line_2' => 25,
            'receiver_tel' => 26,
        );

        if(isset($_POST['F_UploadFile']))
        {
            $fileModel->setAttributes($_POST['F_UploadFile']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if(!$fileModel->validate())
            {
                $this->render('createExternalFromFile/createExternalFromFile', array(
                    'models' => $S_Courier_Ids_From_File,
                    'fileModel' => $fileModel,
                ));
                return;
            }

        }

        if($fileModel->file !== null)
        {


            $data = $fileModel->returnAsArray();

            Yii::app()->session['fileData'] = $data;

            $models = [];
            if(S_Useful::sizeof($data))
            {
                foreach($data AS $item)
                {

                    $courier = new Courier;
                    $courier->courierTypeExternal = new CourierTypeExternal();
                    $courier->senderAddressData = new AddressData();
                    $courier->receiverAddressData = new AddressData();

                    $courier->courier_stat_id = CourierStat::findIdByText($item[$attributes['courier_stat']]);
                    $courier->local_id = $item[$attributes['local_id']];
                    $courier->package_weight = $item[$attributes['package_weight']];
                    $courier->package_size_l = $item[$attributes['package_size_l']];
                    $courier->package_size_w = $item[$attributes['package_size_w']];
                    $courier->package_size_d = $item[$attributes['package_size_d']];
                    $courier->packages_number = $item[$attributes['packages_number']];
                    $courier->package_value = $item[$attributes['package_value']];
                    $courier->package_content = $item[$attributes['package_content']];
                    $courier->user_id = $item[$attributes['user_id']];

                    $courier->courierTypeExternal->external_id = $item[$attributes['external_id']];
                    $courier->courierTypeExternal->courier_external_operator_id = CourierExternalOperator::findIdByText($item[$attributes['courier_external_operator_id']]);

                    $courier->senderAddressData->name = $item[$attributes['sender_name']];
                    $courier->senderAddressData->company = $item[$attributes['sender_company']];
                    $courier->senderAddressData->country_id = CountryList::findIdByText( $item[$attributes['sender_country']]);
                    $courier->senderAddressData->zip_code = $item[$attributes['sender_zip_code']];
                    $courier->senderAddressData->city = $item[$attributes['sender_city']];
                    $courier->senderAddressData->address_line_1 = $item[$attributes['sender_address_line_1']];
                    $courier->senderAddressData->address_line_2 = $item[$attributes['sender_address_line_2']];
                    $courier->senderAddressData->tel = $item[$attributes['sender_tel']];

                    $courier->receiverAddressData->name = $item[$attributes['receiver_name']];
                    $courier->receiverAddressData->company = $item[$attributes['receiver_company']];
                    $courier->receiverAddressData->country_id = CountryList::findIdByText( $item[$attributes['receiver_country']]);
                    $courier->receiverAddressData->zip_code = $item[$attributes['receiver_zip_code']];
                    $courier->receiverAddressData->city = $item[$attributes['receiver_city']];
                    $courier->receiverAddressData->address_line_1 = $item[$attributes['receiver_address_line_1']];
                    $courier->receiverAddressData->address_line_2 = $item[$attributes['receiver_address_line_2']];
                    $courier->receiverAddressData->tel = $item[$attributes['receiver_tel']];

                    $courier->validate();
                    $courier->receiverAddressData->validate();
                    $courier->senderAddressData->validate();
                    $courier->courierTypeExternal->validate();


                    array_push($models, $courier);
                }

            }
            else
                Yii::app()->user->setFlash('error','Nie znaleziono treści w pliku!');


        }


        if(isset($_POST['Courier']))
        {

            if(isset(Yii::app()->session['fileData']))
            {
                $data = Yii::app()->session['fileData'];
                unset(Yii::app()->session['fileData']);
            }


            $models = [];
            foreach($_POST['Courier'] AS $key => $item)
            {
                $courier = new Courier;
                $courier->senderAddressData = new AddressData('optional');
                $courier->receiverAddressData = new AddressData();
                $courier->courierTypeExternal = new CourierTypeExternal();

                $courier->setAttributes($item);

                $courier->senderAddressData->setAttributes($_POST['AddressData'][$key]['sender']);
                $courier->receiverAddressData->setAttributes($_POST['AddressData'][$key]['receiver']);

                $courier->courierTypeExternal->setAttributes($_POST['CourierTypeExternal'][$key]);

                // load data again from file stored in session
                if(is_array($data))
                {
                    $item = $data[$key];
                    $courier->package_weight = $item[$attributes['package_weight']];
                    $courier->package_size_l = $item[$attributes['package_size_l']];
                    $courier->package_size_w = $item[$attributes['package_size_w']];
                    $courier->package_size_d = $item[$attributes['package_size_d']];
                    $courier->packages_number = $item[$attributes['packages_number']];
                    $courier->package_value = $item[$attributes['package_value']];
                    $courier->package_content = $item[$attributes['package_content']];

                    $courier->senderAddressData->name = $item[$attributes['sender_name']];
                    $courier->senderAddressData->company = $item[$attributes['sender_company']];
                    $courier->senderAddressData->country_id = CountryList::findIdByText( $item[$attributes['sender_country']]);
                    $courier->senderAddressData->zip_code = $item[$attributes['sender_zip_code']];
                    $courier->senderAddressData->city = $item[$attributes['sender_city']];
                    $courier->senderAddressData->address_line_1 = $item[$attributes['sender_address_line_1']];
                    $courier->senderAddressData->address_line_2 = $item[$attributes['sender_address_line_2']];
                    $courier->senderAddressData->tel = $item[$attributes['sender_tel']];

                    $courier->receiverAddressData->name = $item[$attributes['receiver_name']];
                    $courier->receiverAddressData->company = $item[$attributes['receiver_company']];
                    $courier->receiverAddressData->country_id = CountryList::findIdByText( $item[$attributes['receiver_country']]);
                    $courier->receiverAddressData->zip_code = $item[$attributes['receiver_zip_code']];
                    $courier->receiverAddressData->city = $item[$attributes['receiver_city']];
                    $courier->receiverAddressData->address_line_1 = $item[$attributes['receiver_address_line_1']];
                    $courier->receiverAddressData->address_line_2 = $item[$attributes['receiver_address_line_2']];
                    $courier->receiverAddressData->tel = $item[$attributes['receiver_tel']];
                }

                array_push($models, $courier);
            }


            $result = Courier::saveExternalTypeGroup($models);

            if(is_array($result))
            {
                $models = $result;
            } else {
                $number = S_Useful::sizeof($models);
                Yii::app()->user->setFlash('success','Dodano nowych przesyłek: '.$number);
                $this->redirect(array('/courier/courier/admin'));
            }

        }


        $this->render('createExternalFromFile/createExternalFromFile', array(
            'models' => $models,
            'fileModel' => $fileModel,
        ));
    }

    public function actionUpdateStatFromFile()
    {
        $S_Courier_Ids_From_File = Array();
        $fileModel = new F_UploadFile();

        if(isset($_POST['F_UploadFile']))
        {
            $fileModel->setAttributes($_POST['F_UploadFile']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if(!$fileModel->validate())
            {
                $this->render('createExternalFromFile/createExternalFromFile', array(
                    'models' => $S_Courier_Ids_From_File,
                    'fileModel' => $fileModel,
                ));
                return;
            }

        }

        if($fileModel->file !== null)
        {
            $attributes = Array(
                'local_id' => 0,
                'courier_stat' => 1,
                'date' => 2,
            );

            $data = $fileModel->returnAsArray();

            $models = [];
            if(S_Useful::sizeof($data))
            {
                foreach($data AS $item)
                {
                    $model = new F_MassStatusChange();
                    $model->status_date_text = $item[$attributes['date']];
                    $model->courier_local_id = $item[$attributes['local_id']];
                    $model->status_text = $item[$attributes['courier_stat']];
                    $model->work();

                    array_push($models, $model);
                }

            }
            else
                Yii::app()->user->setFlash('error','Nie znaleziono treści w pliku!');


        }


        if(isset($_POST['F_MassStatusChange']))
        {

            $models = [];
            foreach($_POST['F_MassStatusChange'] AS $key => $item)
            {

                $model = new F_MassStatusChange();
                $model->setAttributes($item);

                array_push($models, $model);
            }

            $result = F_MassStatusChange::saveGroupOfPackages($models);


            if(!$result)
            {
                Yii::app()->user->setFlash('error','Nie zaktualizowano żadnego statusu. Może duplikaty lub błędne ID?');

            } else {

                Yii::app()->user->setFlash('success','Zaktualizowano statusów: '.$result);
                $this->redirect(array('/courier/courier/admin'));
            }

        }


        $this->render('updateStatFromFile/updateStatFromFile', array(
            'models' => $models,
            'fileModel' => $fileModel,

        ));
    }


    public function actionLabelPrinter()
    {
        $model = false;
        if (isset($_POST['local_id'])) {

            $model = Courier::model()->find('local_id=:local_id', array(':local_id' => $_POST['local_id']));

            if($model === NULL) {

                $remoteId = $_POST['local_id'];
                $response = CourierExternalManager::findCourierIdsByRemoteId($remoteId, true);

                $n = S_Useful::sizeof($response);
                if (!$n) {
                    Yii::app()->user->setFlash('notice', '0 packages found!');
                } else if ($n > 1) {
                    Yii::app()->user->setFlash('notice', 'More than 1 package found!');
                } else {
                    $model = array_pop($response);
                }
            }

            if(intval($_POST['print_option']) === 3 && $model)
            {
                echo CJSON::encode(array('html' => TbHtml::blockAlert('success', 'Content:<br/><br/><span style="font-size: 50px;">'.$model->package_content.'</span><br/><br/><br/>Ref:<br/><br/><span style="font-size: 30px;">'.$model->ref.'</span>')));
                exit;
            }


            if(intval($_POST['print_option']) === 1 && $model)
            {
                echo CJSON::encode(['redirect' => true, 'url' => Yii::app()->createAbsoluteUrl('/courier/courier/CarriageTicket', ['id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_ROLL])]);
                exit;
            }

            if(intval($_POST['print_option']) === 2 && $model)
            {
                $ip = $_POST['ip'];
                $port = $_POST['port'];

                $label = $model->getSingleLabel(false, true);

                if($label)
                {
                    $address = PrintServer::ipAndPortToAddress($ip, $port);
                    Yii::app()->PrintServerQueque->scheudleItem(PrintServerQueque::TYPE_COURIER, $label->id, false, $address);
                    PrintServerQueque::asyncCallForId($label->id, PrintServerQueque::TYPE_COURIER, true);

                    echo CJSON::encode(array('html' => TbHtml::blockAlert('success', 'Label sent to printer!')));
                    exit;
                }

                echo CJSON::encode(array('html' => TbHtml::blockAlert('error', 'Cannot print this label!')));
                exit;
            }

            if(isset($_POST['ajax'])) {
                echo CJSON::encode(array('html' => $this->renderPartial('label_printer_buttons', array('model' => $model), true)));
                exit;
            }


        }

        $this->render('labelPrinter', array(
            'model' => $model,
        ));
    }

//    public function actionUserCancelConfirm($id)
//    {
//        /* @var $model Courier */
//        $model = Courier::model()->findByPk($id);
//
//        if($model === NULL)
//            throw new CHttpException(404);
//
//        if($model->userCancelConfirmByAdmin()) {
//            $model->addToLog('Canell request confimed', CourierLog::CAT_INFO, Yii::app()->user->name);
//            Yii::app()->user->setFlash('success', 'Operation has succeded');
//
//        }
//        else
//            Yii::app()->user->setFlash('error', 'Operation has not succeded');
//
//
//
//        $this->redirect(['/courier/courier/view', 'id' => $model->id]);
//    }
//
//    public function actionUserCancelDismiss($id)
//    {
//        $model = Courier::model()->findByPk($id);
//
//        if($model === NULL)
//            throw new CHttpException(404);
//
//        if($model->userCancelDismiss()) {
//            $model->addToLog('Canell request dismissed', CourierLog::CAT_INFO, Yii::app()->user->name);
//            Yii::app()->user->setFlash('success', 'Operation has succeded');
//        }
//        else
//            Yii::app()->user->setFlash('error', 'Operation has not succeded');
//
//        $this->redirect(['/courier/courier/view', 'id' => $model->id]);
//    }


    public function actionAddRef($urlData)
    {
        /* @var $model Courier */
        $model = Courier::model()->findByAttributes(['hash' => $urlData]);

        if($model === NULL)
            throw new CHttpException(404);

        if($model->ref != '')
            throw new CHttpException(400, 'This package has ref number!');

        if (isset($_POST['Courier'])) {
            $model->ref = $_POST['Courier']['ref'];

            if ($model->validate(['ref']) && $model->update(['ref'])) {

                $model->addToLog('Added REF number: '.$model->ref.' by: '.Yii::app()->user->model->login, CourierLog::CAT_INFO);

                Yii::app()->user->setFlash('success', 'REF number has been updated!');
                $this->redirect(array('/courier/courier/view', 'id' => $model->id));
            }
        }

        $this->render('addRef', array('model' => $model));
    }

    public function actionRemoveRef($urlData)
    {
        /* @var $model Courier */
        $model = Courier::model()->findByAttributes(['hash' => $urlData]);

        if($model === NULL)
            throw new CHttpException(404);

        if($model->ref == '')
            throw new CHttpException(400, 'This package has no ref number!');

        $ref = $model->ref;
        $model->ref = NULL;

        if ($model->update(['ref'])) {

            $model->addToLog('Removed REF number: ' . $ref . ' by: ' . Yii::app()->user->model->login, CourierLog::CAT_INFO);
            Yii::app()->user->setFlash('success', 'REF number has been removed!');
        }

        $this->redirect(array('/courier/courier/view', 'id' => $model->id));
    }

    public function actionUpdateTtOne($id)
    {
        Yii::import('application.components.*');

        $courier = Courier::model()->with('courierTypeInternal')->with('courierTypeDomestic')->with('courierTypeOoe')->with('courierTypeInternalSimple')->with('courierTypeExternal')->findByPk($id);

        /* $item Courier */
        if($courier !== NULL)
            CourierExternalManager::updateStatusForPackage($courier);
        else
            throw new CHttpException(404);

        $this->redirect(['/courier/courier/view', 'id' => $courier->id]);
    }

    public function actionRefreshStatMap($urlData)
    {
        $courier = Courier::model()->findByAttributes(['hash' => $urlData]);
        if($courier === NULL)
            throw new CHttpException(404);
        else
        {
            if($courier->refreshStatMap())
                Yii::app()->user->setFlash('success', 'Stat map refreshed');
            else
                Yii::app()->user->setFlash('error','Stat map NOT refreshed');
        }

        $this->redirect(['/courier/courier/view', 'id' => $courier->id]);


    }

}