<?php

abstract class GlobalOperatorWithReturn extends GlobalOperator
{


    /**
     * @param CourierTypeReturn $courierReturn
     * @param CourierLabelNew $courierLabelNew
     * @param $returnErrors
     * @return GlobalOperatorResponse[]
     */
    abstract public static function orderForCourierReturn(CourierTypeReturn $courierReturn, CourierLabelNew $courierLabelNew, $returnErrors);


    /**
     * @param Courier $courier
     * @return CourierAdditionList|CourierUAdditionList|bool
     * @throws Exception
     */
    protected static function getBaseAdditionsListModel(Courier $courier)
    {
        if($courier->getType() == Courier::TYPE_INTERNAL)
            return new CourierAdditionList();
        else if($courier->getType() == Courier::TYPE_U)
            return new CourierUAdditionList();
        else if($courier->getType() == Courier::TYPE_RETURN)
            return false;
        else
            throw new Exception('Unknown Base Additions List model!');
    }

    /**
     * @param Courier $courier
     * @return string
     * @throws Exception
     */
    protected static function getCacheSuffix(Courier $courier)
    {
        if($courier->getType() == Courier::TYPE_INTERNAL)
            return 'I';
        else if($courier->getType() == Courier::TYPE_U)
            return 'U';
        else if($courier->getType() == Courier::TYPE_RETURN)
            return 'R';
        else
            throw new Exception('Unknown Base Additions List model!');
    }
}