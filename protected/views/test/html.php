<?php
$data = [['be', 'Belgium', 'all retail outlets of bpost/ Parcelshops', '1,300 / Plan: 2,000', '15 x 11 x 1', '120 x 60 x 60', '30'],
['bg', 'Bulgaria', 'all retail outlets of Bulgarian post', '130', '15 x 11 x 1', '120 x 60 x 60', '30'],
['dk', 'Denmark', 'all retail outlets of Post Danmark', '800', '15 x 11 x 1', '100 x 50 x 50', '31,5'],
['es', 'Estonia', 'all retail outlets of Eesti Post', '170', '15 x 11 x 1', '105 x 60 x 60', '31,5'],
['fi', 'Finland', 'all retail outlets of Posti', '1,1', '15 x 11 x 1', '120 x 60 x 60', '30'],
['fr', 'France', 'all retail outlets of La Poste', '17,074', '15 x 11 x 1', '120 x 60 x 60', '20'],
['gr', 'Greece', 'all retail outlets of Elta Post', '2,086', '15 x 11 x 1', '100 x 50 x 50', '20'],
['gb', 'UK', 'all retail outlets of Post Office R', '> 12,000', '15 x 11 x 1', '120 x 60 x 60', '30'],
['ir', 'Ireland', 'all retail outlets of An Post', '1,4', '15 x 11 x 1', '120 x 60 x 60', '30'],
['it', 'Italy', 'all retail outlets of Poste Italiane', '13,991', '15 x 11 x 1', '120 x 60 x 60', '30'],
['hr', 'Croatia', 'all retail outlets of Hrvatska pošta', '1,016', '15 x 11 x 1', '120 x 60 x 60', '30'],
['lv', 'Latvia', 'all retail outlets of Latvijas Pasts', '620', '15 x 11 x 1', '100 x 50 x 50', '30'],
['lt', 'Lithuania', 'all retail outlets of Lietuvos Pasta', '623', '15 x 11 x 1', '120 x 60 x 60', '20'],
['lu', 'Luxembourg', 'all retail outlets of EPT', '> 100', '15 x 11 x 1', '120 x 60 x 60', '31,5'],
['mt', 'Malta', 'alle Filialen der MaltaPost', '40', '15 x 11 x 1', '120 x 60 x 60', '30'],
['nl', 'Netherlands', 'all retail outlets of Postnl', '2,6', '15 x 11 x 1', '120 x 50 x 50', '30'],
['au', 'Austria', 'all retail outlets of Post,at/ DHL Parcel AT', '1,650 / Plan: 2,000', '15 x 11 x 1', '100 x 60 x 60', '31,5'],
['pl', 'Poland', 'all retail outlets of Poczta Polska', '8,24', '15 x 11 x 1', '120 x 60 x 60', '31,5'],
['pt', 'Portugal', 'all retail outlets of CTT Expresso', '1', '15 x 11 x 1', '120 x 60 x 60', '30'],
['ro', 'Romania', 'all retail outlets of Posta Romana', '6,998', '15 x 11 x 1', '105 x 60 x 60', '20'],
['se', 'Sweden', 'all Servicepoints of DHL Sweden', '1,3', '15 x 11 x 3,5', '120 x 50 x 50', '20'],
['ch', 'Switzerland', 'all retail outlets of Swiss Post', '1,86', '15 x 11 x 1', '120 x 60 x 60', '30'],
['sk', 'Slovakia', 'all retail outlets of Slovenska Posta', '1,543', '20 x 15', '120 x 60 x 60', '30'],
['si', 'Slovenia', 'all retail outlets of Posta Slovenije', '559', '15 x 11 x 1', '120 x 60 x 60', '30'],
['es', 'Spain', 'all retail outlets of Correos', '> 10,000', '15 x 11 x 1', '120 x 60 x 60', '30'],
['cz', 'Czech Rep.', 'selected retail outlets of Ceska Posta', '3,179', '21,5 x 15,5', '120 x 60 x 60', '30'],
['hu', 'Hungary', 'all retail outlets of Magyar Posta', '3,867', '15 x 11 x 1', '100 x 50 x 50', '20'],
['cy', 'Cyprus', 'all retail outlets of Cyprus Post', '56', '9 x 14', '120 x 60 x 60', '31,5'],
];
?>


<table class="table table-striped table-condensed">
    <tr>
        <th></th>
        <th><?= Yii::t('home', 'Country');?></th>
        <th><?= Yii::t('home', 'Type of posting location for the end customer');?></th>
        <th  class="text-center"><?= Yii::t('home', 'No. posting locations');?></th>
        <th class="text-center"><?= Yii::t('home', 'Min dimensions [cm]');?></th>
        <th class="text-center"><?= Yii::t('home', 'Max dimensions [cm]');?></th>
        <th class="text-center"><?= Yii::t('home', 'Max weight [kg]');?></th>
    </tr>
    <?php
    foreach($data AS $item):
        ?>
        <tr>
            <td><img src="<?= Yii::app()->baseUrl;?>/images/flags/<?= $item[0];?>.png" style="height: 15px; float: right;"/></td>
            <td style="font-weight: bold;"><?= $item[1];?></td>
            <td><?= $item[2];?></td>
            <td  class="text-center"><?= $item[3];?></td>
            <td class="text-center"><?= $item[4];?></td>
            <td class="text-center"><?= $item[5];?></td>
            <td class="text-center"><?= $item[6];?></td>
        </tr>
    <?php
    endforeach;
    ?>
</table>