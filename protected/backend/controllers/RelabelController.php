<?php

class RelabelController extends Controller
{
//    const URL_PREFIX = 'http://localhost/SwiatPrzesylek.pl/f2/';
    const URL_PREFIX = 'https://api.swiatprzesylek.pl/';

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_COURIER_SCANNER.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        Yii::app()->getModule('courier');
        Yii::app()->getModule('postal');

        $errors = false;

        $ip = $_SERVER['REMOTE_ADDR'];
        $port = '51000';
        $redirectUrl = false;
        $successLog = false;


        if(isset($_POST['track_id']))
        {
            $relabeliongTrackId = uniqid();
            MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : START');
            MyDump::dump('relabeling_tool.txt', print_r($_POST,1));

            $ip = $_POST['ip'];
            $port = $_POST['port'];
            $courier_non_routing_operator_id = $_POST['courier_non_routing_operator_id'];
            $postal_operator_id = $_POST['postal_operator_id'];
            $no_changes = $_POST['no_changes'];
            $label_type = $_POST['label_type'];

            $postal_type = $_POST['postal_type'];
            $postal_size = $_POST['postal_size'];

            $cancel = $_POST['cancel'];

            $default_tel_receiver = strip_tags(trim($_POST['default_tel_receiver']));
            $default_tel_sender = strip_tags(trim($_POST['default_tel_sender']));

            $isCourier = false;
            $isPostal = false;

            $toCourier = false;
            $toPostal = false;

            $id = trim($_POST['track_id']);

            if($id == '') {
                $errors = true;
                $errorLog = 'Provide tracking ID!';
            }

            if($courier_non_routing_operator_id == '' AND $postal_operator_id == '' AND !$no_changes) {
                $errors = true;
                $errorLog = 'Select new item type!';
            }


            if($no_changes)
            {
                if($isCourier)
                    $toCourier = true;
                else if($isPostal)
                    $toPostal = true;
            }
            else if($courier_non_routing_operator_id)
                $toCourier = true;
            else if($postal_operator_id)
                $toPostal = true;

            if(!$errors)
            {

                $models = CourierExternalManager::findCourierIdsByRemoteId($id, true);

                if(!S_Useful::sizeof($models))
                {
                    $models = Postal::model()->findAllByAttributes(['local_id' => $id]);

                    if(!S_Useful::sizeof($models))
                        $models = PostalExternalManager::findPostalIdsByRemoteId($id);

                    if(S_Useful::sizeof($models)) {
                        $isPostal = true;
                    }
                } else {
                    $isCourier = true;

                }

                if(S_Useful::sizeof($models) > 1) {
                    $errors = true;
                    $errorLog = 'More then one item found!';
                }

                if(!S_Useful::sizeof($models)) {
                    $errors = true;
                    $errorLog = 'Item not found!';
                }


                $model = $models[0];



                if(!$errors)
                {
                    $login = $model->user->login;
                    $pass = $model->user->hash;

                    if($toCourier)
                    {
                        $package = [];
                        $options = [];
                        $options2 = [];
                        if($isCourier) {


                            $package['weight'] = $model->getWeight(true);
                            $package['size_l'] = $model->package_size_l;
                            $package['size_w'] = $model->package_size_w;
                            $package['size_d'] = $model->package_size_d;
                            $package['value'] = $model->package_value;
                            $package['value_currency'] = $model->value_currency;
                            $package['content'] = $model->package_content;

                            if($model->cod_value > 0)
                            {
                                $options['cod'] = $model->cod_value;
                                $options['cod_currency'] = $model->cod_currency;
                            }

                        }
                        else if($isPostal)
                        {

                            /* @var $model Postal */
                            $package['weight'] = round($model->weight / 1000,2);
                            $package['size_l'] = 10;
                            $package['size_w'] = 10;
                            $package['size_d'] = 10;
                            $package['value'] = $model->value;
                            $package['value_currency'] = $model->value_currency;
                            $package['content'] = $model->content;
                        }

                        $senderAddressData = $model->senderAddressData;
                        $receiverAddressData = $model->receiverAddressData;

                        $sender = [];
                        $sender['name'] = $senderAddressData->name;
                        $sender['company'] = $senderAddressData->company;
                        $sender['address_line_1'] = $senderAddressData->address_line_1;
                        $sender['address_line_2'] = $senderAddressData->address_line_2;
                        $sender['country'] = $senderAddressData->country0->code;
                        $sender['zip_code'] = $senderAddressData->zip_code;
                        $sender['city'] = $senderAddressData->city;
                        $sender['tel'] = $senderAddressData->tel == '' ? $default_tel_sender : $senderAddressData->tel;
                        $sender['email'] = $senderAddressData->email;

                        $receiver = [];
                        $receiver['name'] = $receiverAddressData->name;
                        $receiver['company'] = $receiverAddressData->company;
                        $receiver['address_line_1'] = $receiverAddressData->address_line_1;
                        $receiver['address_line_2'] = $receiverAddressData->address_line_2;
                        $receiver['country'] = $receiverAddressData->country0->code;
                        $receiver['zip_code'] = $receiverAddressData->zip_code;
                        $receiver['city'] = $receiverAddressData->city;
                        $receiver['tel'] = $receiverAddressData->tel == '' ? $default_tel_receiver : $receiverAddressData->tel;
                        $receiver['email'] = $receiverAddressData->email;

                        $options['number'] = 1;

                        $options['collection'] = false;
                        $options['operator'] = $courier_non_routing_operator_id;

                        $options2['problem_label'] = 1;
                        $options2['label_type'] = 'NONE';

                        $DATA = [
                            'package' => $package,
                            'sender' => $sender,
                            'receiver' => $receiver,
                            'options' => $options,
                            'options2' => $options2,
                        ];


                        $DATA = json_encode($DATA);


                        MyDump::dump('relabeling_tool.txt', print_r($DATA,1));
                        if ($ch = curl_init()) {

                            curl_setopt($ch, CURLOPT_TCP_KEEPALIVE, 1);
                            curl_setopt($ch, CURLOPT_TCP_KEEPIDLE, 2);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $DATA);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));

                            curl_setopt($ch, CURLOPT_URL, self::URL_PREFIX.'V1/courier/create-non-routing?psh=15');
                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($ch, CURLOPT_USERPWD, $login . ':' . $pass);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            $result = curl_exec($ch);
                            $result = json_decode($result);


                            MyDump::dump('relabeling_tool.txt', print_r($result,1));

                            if ($result->result === "OK") {

                                foreach ($result->response->packages AS $package) {

                                    $newModel = Courier::model()->findByAttributes(['local_id' => $package->package_id]);


                                    if($label_type == 1)
                                    {
                                        $redirectUrl = Yii::app()->createAbsoluteUrl('/courier/courier/CarriageTicket', ['id' => $newModel->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_ROLL]);

                                        $successLog = 'Downloading will start soon...';

                                    } else {
                                        $label = $newModel->getSingleLabel(false, true);

                                        if($label)
                                        {
                                            $address = PrintServer::ipAndPortToAddress($ip, $port);
                                            Yii::app()->PrintServerQueque->scheudleItem(PrintServerQueque::TYPE_COURIER, $label->id, false, $address);
                                            PrintServerQueque::asyncCallForId($label->id, PrintServerQueque::TYPE_COURIER, true);

                                            $successLog = 'Label sent to printer!';
                                        }
                                    }

                                    if($isPostal)
                                    {
                                        $successLog .= '<br/> Source item: <a href="'.Yii::app()->createUrl('/postal/postal/view', ['id' => $model->id]).'" target="_blank">#'.$model->local_id.'</a>';
                                        if($cancel) {
                                            $model->changeStat(PostalStat::CANCELLED);
                                            $model->addToLog('Item relabeled with cancel to: #'.$newModel->local_id, PostalLog::CAT_INFO, Yii::app()->user->id);
                                        } else {
                                            $model->addToLog('Item relabeled to: #'.$newModel->local_id, PostalLog::CAT_INFO, Yii::app()->user->id);
                                        }

                                        $newModel->addToLog('Item relabeled from Postal: #'.$model->local_id, CourierLog::CAT_INFO, Yii::app()->user->id);
                                    }
                                    else if($isCourier)
                                    {
                                        $successLog .= '<br/> Source item: <a href="'.Yii::app()->createUrl('/courier/courier/view', ['id' => $model->id]).'" target="_blank">#'.$model->local_id.'</a>';
                                        if($cancel) {
                                            $model->changeStat(CourierStat::CANCELLED);
                                            $model->addToLog('Item relabeled with cancel to: #'.$newModel->local_id, CourierLog::CAT_INFO, Yii::app()->user->id);
                                        } else {
                                            $model->addToLog('Item relabeled to: #'.$newModel->local_id, CourierLog::CAT_INFO, Yii::app()->user->id);
                                        }

                                        $newModel->addToLog('Item relabeled from Courier: #'.$model->local_id, CourierLog::CAT_INFO, Yii::app()->user->id);
                                    }

                                    $successLog .= '<br/> New item: <a href="'.Yii::app()->createUrl('/courier/courier/view', ['id' => $newModel->id]).'" target="_blank"><strong>#'.$newModel->local_id.'</strong></a>';


                                    $newModel->source = Courier::SOURCE_RELABELING;
                                    $newModel->update('source');
                                }


                            } else {
                                $errors = true;
                                $errorLog = print_r($result->error, 1);
                            }


                        }
                    }
                    elseif($toPostal)
                    {

                        $package = [];
                        $options2 = [];


                        if($isCourier) {

                            $package = [];
                            $package['weight'] = $model->getWeight(true) * 1000;
                            $package['size_class'] = $postal_size;
                            $package['type'] = $postal_type;
                            $package['sp_point'] = 1;
                            $package['content'] = mb_substr($model->package_content,0 ,45);
                            $package['value'] = $model->package_value;
                            $package['value_currency'] = $model->value_currency;

                        }
                        else if($isPostal)
                        {
                            /* @var $model Postal */
                            $package['weight'] = $model->weight;
                            $package['size_class'] = $model->size_class;
                            $package['type'] = $model->postal_type;
                            $package['sp_point'] = 1;
                            $package['content'] = $model->content;
                            $package['value'] = $model->value;
                            $package['value_currency'] = $model->value_currency;
                        }

                        $package['operator_id'] = $postal_operator_id;


                        $isOperatorReg = PostalOperator::isOperatorReg($postal_operator_id);
                        if($package['type'] == Postal::POSTAL_TYPE_REG && !$isOperatorReg OR $package['type'] != Postal::POSTAL_TYPE_REG && $isOperatorReg)
                        {
                            $errors = true;
                            $errorLog = 'Invalid operator for this postal type!';
                        }

                        if(!$errors)
                        {

                            $senderAddressData = $model->senderAddressData;
                            $receiverAddressData = $model->receiverAddressData;

                            $sender = [];
                            $sender['name'] = $senderAddressData->name;
                            $sender['company'] = $senderAddressData->company;
                            $sender['address_line_1'] = $senderAddressData->address_line_1;
                            $sender['address_line_2'] = $senderAddressData->address_line_2;
                            $sender['country'] = $senderAddressData->country0->code;
                            $sender['zip_code'] = $senderAddressData->zip_code;
                            $sender['city'] = $senderAddressData->city;
                            $sender['tel'] = $senderAddressData->tel;
                            $sender['email'] = $senderAddressData->email;

                            $receiver = [];
                            $receiver['name'] = $receiverAddressData->name;
                            $receiver['company'] = $receiverAddressData->company;
                            $receiver['address_line_1'] = $receiverAddressData->address_line_1;
                            $receiver['address_line_2'] = $receiverAddressData->address_line_2;
                            $receiver['country'] = $receiverAddressData->country0->code;
                            $receiver['zip_code'] = $receiverAddressData->zip_code;
                            $receiver['city'] = $receiverAddressData->city;
                            $receiver['tel'] = $receiverAddressData->tel;
                            $receiver['email'] = $receiverAddressData->email;

                            $options2['problem_label'] = 1;



                            $DATA = [
                                'postal' => $package,
                                'sender' => $sender,
                                'receiver' => $receiver,
                                'options2' => $options2,
                            ];


                            $DATA = json_encode($DATA);
                            MyDump::dump('relabeling_tool.txt', print_r($DATA,1));
                            if ($ch = curl_init()) {

                                curl_setopt($ch, CURLOPT_TCP_KEEPALIVE, 1);
                                curl_setopt($ch, CURLOPT_TCP_KEEPIDLE, 2);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $DATA);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));

                                curl_setopt($ch, CURLOPT_URL, self::URL_PREFIX.'V1/postal/create-single?psh=15');
                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                curl_setopt($ch, CURLOPT_USERPWD, $login . ':' . $pass);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                                MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : REQUESTING');
                                $result = curl_exec($ch);
                                MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : RECEIVED');
                                $result = json_decode($result);



                                MyDump::dump('relabeling_tool.txt', print_r($result,1));

                                if ($result->result === "OK") {

                                    foreach ($result->response->postals AS $package) {

                                        /* @var $newModel Postal */
                                        $newModel = Postal::model()->findByAttributes(['local_id' => $package->postal_id]);

                                        if($label_type == 1)
                                        {
                                            $redirectUrl = Yii::app()->createAbsoluteUrl('/postal/postal/CarriageTicket', ['id' => $newModel->id, 'type' => PostalLabelPrinter::PDF_ROLL]);
                                            $successLog = 'Downloading will start soon...';

                                        } else {
                                            MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : LABEL GEN START');
                                            $imgs = PostalLabelPrinter::generateLabels([$newModel], PostalLabelPrinter::PDF_ROLL, true, false, true, false, NULL, true);
                                            MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : LABEL GEN END');

                                            $nw = 777;
                                            $nh = 1166;

                                            MyDump::dump('relabeling_tool_track.txt', 'LABEL 200');
                                            $im = ImagickMine::newInstance();
                                            $im->setResolution(300, 300);

//                                            file_put_contents('rel/ab.pdf', $imgs);

                                            $im->readImageBlob($imgs);
                                            MyDump::dump('relabeling_tool_track.txt', 'LABEL 201');
                                            $im->setImageFormat('png8');
                                            MyDump::dump('relabeling_tool_track.txt', 'LABEL 202');
                                            if ($im->getImageAlphaChannel()) {
                                                $im->setImageBackgroundColor('white');
                                                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                                            }
                                            MyDump::dump('relabeling_tool_track.txt', 'LABEL 203');
                                            $im->scaleImage($nw,$nh, true);
//                                            $im->stripImage();
                                            MyDump::dump('relabeling_tool_track.txt', 'LABEL 204');
                                            $img = $im->getImageBlob();
                                            MyDump::dump('relabeling_tool_track.txt', 'LABEL 205');
//                                            file_put_contents('relab4.png', $img);

                                            MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : BEFORE SEND');
                                            Yii::app()->PrintServerQueque->instantRunImgString($img, $ip.':'.$port);

                                            MyDump::dump('relabeling_tool_track.txt', 'LABEL 201');


//                                            foreach($imgs AS $key => $item)
//                                            {
//                                                MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : BEFORE RESIZE');
//                                                $src = imagecreatefromstring($item);
//                                                $dst = imagecreatetruecolor($nw, $nh);
//                                                imagecopyresampled($dst, $src, 0, 0, 0, 0, $nw, $nh, imagesx($src), imagesy($src));
//                                                ob_start();
//                                                imagepng($dst);
//                                                $img =  ob_get_contents();
//                                                ob_end_clean();
//
////                                                file_put_contents('labbbb.png', $img);
//
//                                                MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : BEFORE SEND');
//                                                Yii::app()->PrintServerQueque->instantRunImgString($img, $ip.':'.$port);
//                                                MyDump::dump('relabeling_tool_track.txt', $relabeliongTrackId.' : AFTER SEND');
//                                            }

                                            $successLog = 'Label sent to printer!';
                                        }


                                        if($isPostal)
                                        {
                                            $successLog .= '<br/> Source item: <a href="'.Yii::app()->createUrl('/postal/postal/view', ['id' => $model->id]).'" target="_blank">#'.$model->local_id.'</a>';
                                            if($cancel) {
                                                $model->changeStat(PostalStat::CANCELLED);
                                                $model->addToLog('Item relabeled with cancel to: #'.$newModel->local_id, PostalLog::CAT_INFO, Yii::app()->user->id);
                                            } else {
                                                $model->addToLog('Item relabeled to: #'.$newModel->local_id, PostalLog::CAT_INFO, Yii::app()->user->id);
                                            }

                                            $newModel->addToLog('Item relabeled from Postal: #'.$model->local_id, PostalLog::CAT_INFO, Yii::app()->user->id);

                                        }
                                        else if($isCourier)
                                        {
                                            $successLog .= '<br/> Source item: <a href="'.Yii::app()->createUrl('/courier/courier/view', ['id' => $model->id]).'" target="_blank">#'.$model->local_id.'</a>';
                                            if($cancel) {
                                                $model->changeStat(CourierStat::CANCELLED);
                                                $model->addToLog('Item relabeled with cancel to: #'.$newModel->local_id, CourierLog::CAT_INFO, Yii::app()->user->id);
                                            } else {
                                                $model->addToLog('Item relabeled to: #'.$newModel->local_id, CourierLog::CAT_INFO, Yii::app()->user->id);
                                            }

                                            $newModel->addToLog('Item relabeled from Courier: #'.$model->local_id, PostalLog::CAT_INFO, Yii::app()->user->id);
                                        }

                                        $successLog .= '<br/> New item: <a href="'.Yii::app()->createUrl('/postal/postal/view', ['id' => $newModel->id]).'" target="_blank"><strong>#'.$newModel->local_id.'</strong></a>';

                                        $newModel->source = Postal::SOURCE_RELABELING;
                                        $newModel->update('source');

                                    }


                                } else {
                                    $errors = true;
                                    $errorLog = print_r($result->error, 1);
                                }


                            }
                        }

                    }
                }
            }

            if($errorLog)
                MyDump::dump('relabeling_tool.txt', print_r($errorLog,1));
            else
                MyDump::dump('relabeling_tool.txt', print_r($successLog,1));
        }


        $this->render('index',
            [
                'successLog' => $successLog,
                'errorLog' => $errorLog,
                'errors' => $errors,
                'ip' => $ip,
                'port' => $port,
                'courier_non_routing_operator_id' => $courier_non_routing_operator_id,
                'postal_operator_id' => $postal_operator_id,
                'no_changes' => $no_changes,
                'label_type' => $label_type,
                'redirectUrl' => $redirectUrl,
                'postal_type' => $postal_type,
                'postal_size' => $postal_size,
                'cancel' => $cancel,
                'default_tel_receiver' => $default_tel_receiver,
                'default_tel_sender' => $default_tel_sender,
            ]);
    }
}