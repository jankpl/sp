<?php

class LogxstarClient extends GlobalOperator
{

    const URL = 'https://os.logxstar.com/api/v1/shipping';
    const APIKEY = '2y10RqToLScPImVNKvh7SrLiiuJKCAeVGV8BLkGk53w3xOEZTlL4yCfLW';

    public $label_annotation_text = false;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $packages_number = 1;

    public $package_weight;
//    public $package_size_l;
//    public $package_size_d;
//    public $package_size_w;
//
//    public $package_content;
//    public $package_value;

    public $ref;

    public $user_id;


    public static function getAdditions(Courier $courier)
    {
        return [];
    }

//    public static function test($returnError = true)
//    {
//        $client = new self;
//
//
////        $data = new stdClass();
//
//
//        $data = '{"company":null,"full_name":"GROTH U RADEMACHER ALTE BACKSTUBE FEWO & APP","street":"NIEDERGASSE 3A","address_extended":{"line2":null},"street_number":"","post_code":"55234","city":"55234","country":"DE","email_address":null,"phone_number":"067314509","total_weight":"10","parcel_reference":"483190425479299","parcel_number_of_labels":1,"shipping_carrier_id":"dpd_germany","shipping_carrier_option":71}';
//
////
////                $data = '{
////    "company":"LOGXSTAR - APID",
////    "full_name":"Reno Postema",
////    "street":"SCHILDKRSTRASSE 17-19",
////   "address_extended" : {
////         "line2":"other info",
////         "line3":""
////
////   },
////
////    "street_number":"11",
////
////    "suffix":"-13",
////
////    "post_code":"53175",
////
////    "city":"BONN",
////
////    "country":"DE",
////
////    "email_address":"reno@logxstar.com",
////
////    "phone_number":"085-0160590",
////
////    "total_weight":1.2,
////
////    "parcel_reference":"APID8549",
////
////    "parcel_number_of_labels":1,
////
////    "shipping_carrier_id":"dpd_germany",
////
////    "shipping_carrier_option":71
////}';
//
//        $resp = $client->_curlCall($data);
//
//
//        if($resp->error)
//            echo 'fail';
//
//
//
//        $id = $resp->internal_reference;
//
//
//        $resp = $client->_curlCall(false, '/'.$id.'/print-labels');
//
//
//
//
////        if(is_arra
//
////        file_put_contents('lab.png', base64_decode($resp->label));
//
//
//        var_dump($resp);
//
//        if($resp->error OR $resp == '')
//        {
//            if ($returnError == true) {
//
//                $error = (string) $resp->error;
//                if($error == '')
//                    $error = 'Unknown error';
//
//                return GlobalOperatorResponse::createErrorResponse($error);
//            } else {
//                return false;
//            }
//        }
//exit;
//
//        $label = $resp[0]->label;
//        $tt = $resp[0]->barcode;
//
//        $label = base64_decode($label);
//
//
//        $im = ImagickMine::newInstance();
//        $im->setResolution(300, 300);
//        $im->readImageBlob($label);
//        $im->setImageFormat('png8');
//        if ($im->getImageAlphaChannel()) {
//            $im->setImageBackgroundColor('white');
//            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//        }
//        $im->stripImage();
//
//
//      file_put_contents('lab.png', $im->getImageBlob());
//      var_dump($tt);
//        exit;
//
//        if($resp == '' OR ((string) $resp->status['code']) != 'OK' OR ((string) $resp->response->niceError) != '')
//        {
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse((string) $resp->response->niceError != '' ? (string) $resp->response->niceError : 'Operator internal error.');
//            } else {
//                return false;
//            }
//        }
//
//        foreach($resp->response->job->parcels->barcode AS $item)
//            $ttNumber = (string) $item;
//
//        $label = (string) $resp->response->job->labelData->url;
//        $label = @file_get_contents($label);
//
//        $im = ImagickMine::newInstance();
//        $im->setResolution(300, 300);
//        $im->readImageBlob($label);
//        $im->setImageFormat('png8');
//        if ($im->getImageAlphaChannel()) {
//            $im->setImageBackgroundColor('white');
//            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//        }
//        $im->stripImage();
//
////            $im->trimImage(0);
//
//        file_put_contents('lab.png', $im->getImageBlob());
//
//        $return = [];
//        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);
//
//        return $return;
//    }

    protected function _curlCall($data, $urlSuffix = '')
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'api-key: '.self::APIKEY;


        $curl = curl_init(self::URL.$urlSuffix);

        if($data) {

            $data = json_encode($data);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_POST, TRUE);
        }

        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        );
        $resp = curl_exec($curl);

        MyDump::dump('logxstar.txt', print_r($data,1));
        MyDump::dump('logxstar.txt', print_r($resp,1));

        $resp = @json_decode($resp);

        return $resp;
    }


    public static function clearString($text, $length = false)
    {
        $text = htmlspecialchars($text);

        if($length)
            $text = mb_substr($text, 0, $length);

        return $text;
    }



    protected function createShipment($returnError)
    {
//        if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse('Not active!');
//            } else {
//                return false;
//            }

        $data = new stdClass();
        $data->company = $this->receiver_company;
        $data->full_name = $this->receiver_name;
        $data->street = $this->receiver_address1;
        $data->address_extended = new stdClass();
        $data->address_extended->line2 = $this->receiver_address2;
        $data->street_number = '';
        $data->post_code = $this->receiver_zip_code;
        $data->city = $this->receiver_city;
        $data->country = $this->receiver_country;
        $data->email_address = $this->receiver_mail;
        $data->phone_number = $this->receiver_tel;
        $data->total_weight = $this->package_weight;
        $data->parcel_reference = $this->ref;
        $data->parcel_number_of_labels = 1;
        $data->shipping_carrier_id = 'dpd_germany';
        $data->shipping_carrier_option = 71;

        $resp = $this->_curlCall($data);

        if($resp->error OR $resp == '')
        {
            if ($returnError == true) {

                $error = (string) $resp->error;
                if($error == '')
                    $error = 'Unknown error';

                return GlobalOperatorResponse::createErrorResponse($error);
            } else {
                return false;
            }
        }

        $id = $resp->internal_reference;
        $resp = $this->_curlCall(false, '/'.$id.'/print-labels');

        if($resp->error OR $resp == '')
        {
            if ($returnError == true) {

                $error = (string) $resp->error;
                if($error == '')
                    $error = 'Unknown error';

                return GlobalOperatorResponse::createErrorResponse($error);
            } else {
                return false;
            }
        }


        if(!is_array($resp))
            $resp = [ $resp ];

        $label = $resp[0]->label;
        $ttNumber = $resp[0]->barcode;

        $label = base64_decode($label);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($label);
        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $im->stripImage();

        $return = [];
        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);

        return $return;

    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->email;
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->email;
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierInternal->courier->getWeight(true);
//        $model->package_size_l = $courierInternal->courier->package_size_l;
//        $model->package_size_d = $courierInternal->courier->package_size_d;
//        $model->package_size_w = $courierInternal->courier->package_size_w;
//
//        $model->package_content = $courierInternal->courier->package_content;
//        $model->package_value = $courierInternal->courier->getPackageValueConverted('GBP');
        $model->ref = $courierInternal->courier->local_id;

        $model->user_id = $courierInternal->courier->user_id;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->email;
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->email;
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierU->courier->getWeight(true);
//        $model->package_size_l = $courierU->courier->package_size_l;
//        $model->package_size_d = $courierU->courier->package_size_d;
//        $model->package_size_w = $courierU->courier->package_size_w;
//
//        $model->package_content = $courierU->courier->package_content;
//        $model->package_value = $courierU->courier->getPackageValueConverted('GBP');
        $model->ref = $courierU->courier->local_id;

        $model->user_id = $courierU->courier->user_id;

        return $model->createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        $statuses = new GlobalOperatorTtResponseCollection();

        $model = new self;
        $resp = $model->_curlCall(false, '/'.$no.'/details');

        if($resp->shipmentDetails->status != '')
        {
            $status = $resp->shipmentDetails->extendedStatus != '' ? $resp->shipmentDetails->extendedStatus : $resp->shipmentDetails->status;
            $statuses->addItem($status, $resp->shipmentDetails->status_date, '');
        }

        return $statuses;

    }
}