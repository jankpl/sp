<?php

class HybridMailController extends Controller {

    public function beforeAction($action)
    {

        if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI)
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'HybridMail',
            ));
            exit;
        }

    }


    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('index', 'create',  'ajaxGenerateReceiverField', 'ajaxGetPrice', 'about'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('list', 'view', 'getTmpFile', 'getFile'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionAbout()
    {

        $this->panelLeftMenuActive = 303;

        $this->render('about');
    }


    public function actionAjaxGenerateReceiverField()
    {

        $i = $_POST['i'];
        $form = $_POST['form'];

        $form = base64_decode($form);
        $form = unserialize($form);

        $model = new HybridMailReceiver;

        $return = $this->renderPartial('create/_receiver', array(
            'model' => $model,
            'form' => $form,
            'i' => $i,
        ),true);

        echo CJSON::encode($return);
    }

    public function actionView($urlData) {

        $this->panelLeftMenuActive = 300;

        $model = HybridMail::model()->find('hash=:hash', array(':hash'=>$urlData));

        Yii::app()->session['fh_'.$model->hash] = true;


        $this->render('view', array(
            'model' => $model,
        ));
    }

    protected function create_step_1()
    {
        if(Yii::app()->user->isGuest)
        {
            $this->setPageState('loginProposition',1);

            $model = new LoginForm();
            $model->returnURL = Yii::app()->createUrl('hybridMail/create');

            $this->render('create/loginProposition', array('model' => $model));

        } else {
            $this->setPageState('form_started', true);
            $this->setPageState('start_user_id', Yii::app()->user->id);

            if($this->getPageState('form_started'))
            {
                if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
                    throw new CHttpException(403, 'Sesja została przerwana!');
            }


            $this->setPageState('last_step',1); // save information about last step
            $model=new HybridMail('step1');

            if(!$this->getPageState('file_hash'))
            {
                $this->setPageState('file_hash', $model->file_hash);
                Yii::app()->session['fh_'.$model->file_hash] = true;
            }

            $model->file_hash = $this->getPageState('file_hash');
            $model->attributes = $this->getPageState('step1',array());

            $modelHybridMailAttachments = HybridMailAttachment::model()
                ->findAll('file_hash=:file_hash', array(
                    ':file_hash' => $model->file_hash,
                ));

            $currency = $this->getPageState('step1_currency', NULL);

            $this->render('create/step1',array(
                'model'=>$model,
                'modelHybridMailAttachments' => $modelHybridMailAttachments,
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));

        }
    }

    protected function create_validate_step_1()
    {
        $model=new HybridMail('step1');
        $model->file_hash = $this->getPageState('file_hash');
        $model->attributes = $_POST['HybridMail'];

        $model->document = CUploadedFile::getInstance($model,'document');

        $errors = false;

        if(!$model->validate())
            $errors = true;


        if(!$model->document->size)
        {
            $model->addError('document', Yii::t('hm', 'Dokument jest pusty!'));
            $errors = true;
        }

        if(!$errors) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
            $mimeExt = finfo_file($finfo, $model->document->tempName);

            if (!in_array($mimeExt, array('application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'))) {
                $model->addError('document', Yii::t('hm', 'Dokument posiada nieprawidłowe rozszerzenie!'));
                $errors = true;
            }
        }

        $savePath = HybridMail::getTmpDir($model->file_hash);


        if(!$errors)
        {
            if(in_array($model->document->extensionName, array('doc', 'docx'))) {

                /* @var $phpWord PHPWord */
                $phpWord = Yii::app()->PHPWord;

                $phpWord->loadFile($model->document);

                // check if A$
                if(!$phpWord->isA4()) {
                    $model->addError('document', Yii::t('hm', 'Dokument musi być rozmiaru A4!'));
                    $errors = true;
                }


                // conver to PDF to calculate number of pages
                $phpWord->toPdf($savePath.'.tmp'.'pdf');
                $pdfData = Yii::app()->PDFData;
                $pdfData->loadFile($savePath.'.tmp'.'pdf');
                $model->pages =  $model->pages = $pdfData->getPagesNumer();
                // discard temp pdf file

                @unlink($savePath.'.tmp'.'pdf');

            }
            else
            {
                /* @var $pdfData PDFData */
                $pdfData = Yii::app()->PDFData;
                $pdfData->loadFile($model->document->tempName);


                // check if A$
                if (!$pdfData->getIsA4()) {
                    $model->addError('document', Yii::t('hm', 'Dokument musi być rozmiaru A4!'));
                    $errors = true;
                }

                // count number of pages
                if(!$errors)
                {
                    $model->pages = $pdfData->getPagesNumer();
                }
            }

            if(!$errors)
                $model->document->saveAs($savePath.'.'.$model->document->extensionName);

            $this->setPageState('fileExt', $model->document->extensionName);

            //$model->validate();
            $this->setPageState('step1',$model->getAttributes(array_merge(array_keys($_POST['HybridMail']),array('pages')))); // save previous form into form state
//        $modelHybridMailAttachments = HybridMailAttachment::model()
//            ->findAll('file_hash=:file_hash', array(
//                ':file_hash' => $model->file_hash,
//            ));
//
//
//        $error = false;
        }


        $currency = PriceCurrency::model()->findByPk($_POST['currency']);
        if($currency == NULL)
            $currency = 1;
        else
            $currency = $currency->id;

        $this->setPageState('step1_currency', $currency);

        if($errors OR $model->hasErrors())
        {
            $this->render('create/step1',array(
                'model'=>$model,
//                'modelHybridMailAttachments' => $modelHybridMailAttachments,
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }
        unset($model);
    }

    protected function create_step_1_cont()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 1)
            if($this->create_validate_step_1() == 'break')
                return;

        $model = new HybridMail();

        $model->attributes = $this->getPageState('step1', array());
        $model->file_hash = $this->getPageState('file_hash');
        $model->_file_ext = $this->getPageState('fileExt');

        $this->setPageState('last_step',15); // save information about last step
        $this->render('create/step1cont',array(
            'model'=>$model,
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));
    }

    protected function create_validate_step_1_cont()
    {
        return false;
    }

    protected function create_step_2()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 15)
            if($this->create_validate_step_1_cont() == 'break')
                return;


        $model = new HybridMail_AddressData('sender');
        if(!S_Useful::sizeof($this->getPageState('step2_model',array())))
            $model->loadUserData();
        else
            $model->attributes = $this->getPageState('step2_model',array());

        $model2 = null;
        $saveToContactBook = false;
        if($this->getPageState('step2_saveToContactBook'))
        {
            $model2 = new UserContactBook();
            $model2->attributes = $this->getPageState('step2_saveToContactBook');
            $saveToContactBook = true;
        }

        $userGroup = NULL;
        if(!Yii::app()->user->isGuest)
            $userGroup = Yii::app()->user->getUserGroupId();

        $this->setPageState('last_step',2); // save information about last step
        $this->render('create/step2',array(
            'model'=>$model,
            'model2'=>$model2,
            'saveToContactBook' => $saveToContactBook,
            'type' => 'sender',
            'countries' => HybridMail::listCountries($userGroup, true),
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));

    }

    protected function create_validate_step_2()
    {
        $model=new HybridMail_AddressData('sender');
        $model->attributes = $_POST['HybridMail_AddressData'];
        $model->validate();
        $this->setPageState('step2_model', $model->getAttributes(array_keys($_POST['HybridMail_AddressData']))); // save previous form into form state

        $model2 = null;
        $saveToContactBook = false;
        if($_POST['Other']['saveToContactBook']) // for saving data to userAddress
        {
            $saveToContactBook = true;
            $model2 = new UserContactBook();
            $model2->type = 'sender';
            $model2->setAttributesWithPrefix($model->getAttributes(array_keys($_POST['HybridMail_AddressData'])),'');
            $model2->validate();
        }

        $this->setPageState('step2_saveToContactBook',$saveToContactBook);
        if(isset($model2))
            $this->setPageState('step2_saveToContactBookAddressData',$model2->attributes);

        if($model->hasErrors() OR (isset($model2) AND $model2->hasErrors()))
        {
            $this->render('create/step2',array(
                'model'=>$model,
                'model2'=>$model2,
                'saveToContactBook' => $saveToContactBook,
                'type' => 'sender',
                'countries' => HybridMail::listCountries(),
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }
    }

    protected function create_step_3()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');


        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 2)
            if($this->create_validate_step_2() == 'break')
                return;

        $modelsAddressData = Array();
        $modelsAddressDataData = $this->getPageState('modelsAddressDataAttributes', array());

        if($modelsAddressDataData !== null)
            foreach($modelsAddressDataData AS $key => $item)
            {
                $modelsAddressData[$key] = new HybridMail_AddressData('receiver');
                $modelsAddressData[$key]->attributes = $item;
            }

        if(S_Useful::sizeof($modelsAddressData) < 1)
            array_push($modelsAddressData, new HybridMail_AddressData('receiver'));

        $saveToContactBooks = $this->getPageState('step3_saveToContactBooks');


        $userGroup = NULL;
        if(!Yii::app()->user->isGuest)
            $userGroup = Yii::app()->user->getUserGroupId();

        $this->setPageState('last_step',3); // save information about last step
        $this->render('create/step3',array(
            'modelsAddressData'=>$modelsAddressData,
            'saveToContactBooks' => $saveToContactBooks,
            'countries' => HybridMail::listCountries($userGroup),
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));

    }

    protected function create_validate_step_3()
    {
        $modelsAddressData = Array();
        $oneIsEmpty = false;
        $saveToContactBooks = null;

        foreach($_POST['HybridMail_AddressData'] AS $key => $item)
        {
            $isEmpty = true;

            foreach($item AS $itemAttributeKey => $itemAttribute)
            {
                if($itemAttribute != "")
                {
                    if(!preg_match('/.*_id$/',$itemAttributeKey))
                    {
                        $isEmpty = false;
                    }
                }
                if($isEmpty) $oneIsEmpty = true;
            }
            $saveToContactBooks[$key] = 0;
            if($isEmpty) continue;

            if($_POST['Other']['saveToContactBook'][$key] == 1) $saveToContactBooks[$key] = 1;

            if($modelsAddressData [$key] === null) $modelsAddressData [$key] = new HybridMail_AddressData('receiver');
            $modelsAddressData [$key]->setAttributes($item);

            if(!$modelsAddressData[$key]->validate())
                $errors = true;
        }


        // Clicked button for more receivers
        if(isset($_POST['step4_more']))
        {
            if($oneIsEmpty)
                Yii::app()->user->setFlash('error',"Jeden odbiorca wciąż czeka na wypełnienie danych...");
            else
                Yii::app()->user->setFlash('success',"Dodano odbiorcę.");

            array_push($modelsAddressData, new HybridMail_AddressData('receiver'));
            $this->render('create/step3',array(
                'modelsAddressData'=>$modelsAddressData,
                'saveToContactBooks' => $saveToContactBooks,
                HybridMail::listCountries(),
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }

        if($errors OR !S_Useful::sizeof($modelsAddressData))
        {
            if(!S_Useful::sizeof($modelsAddressData))
            {
                array_push($modelsAddressData , new HybridMail_AddressData('receiver'));
                Yii::app()->user->setFlash('error',"Dodaj przynajmniej jednego odbiorcę!");
            }

            $this->render('create/step3',array(
                'modelsAddressData'=>$modelsAddressData ,
                'saveToContactBooks' => $saveToContactBooks,
                HybridMail::listCountries(),
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }

        $modelsAddressDataAttributes = Array();
        foreach($modelsAddressData  AS $key => $item)
        {
            $modelsAddressDataAttributes[$key] = $item->getAttributes();
        }

        $this->setPageState('modelsAddressDataAttributes',$modelsAddressDataAttributes);
        $this->setPageState('step3_saveToContactBooks',$saveToContactBooks);

    }

    protected function create_step_4()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');


        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 3)
            if($this->create_validate_step_3() == 'break')
                return;

        $model = new HybridMail();
        $model->attributes = $this->getPageState('step1', array());
        $model->file_hash = $this->getPageState('file_hash');

        $modelHybridMailAddition = $this->getPageState('hybrid_mail_additions', array());
        $modelHybridMailAdditionList = HybridMailAdditionList::model()->findAll('stat = '.S_Status::ACTIVE);


        if(Yii::app()->user->isGuest)
        {
            $currency = $this->getPageState('step1_currency', 1);
            Yii::app()->PriceManager->setCurrency($currency);
        }

        $currency = Yii::app()->PriceManager->getCurrency();


        $this->setPageState('last_step',4); // save information about last step
        $this->render('create/step4',array(
            'model' => $model,
            'currency' => $currency,
            'modelHybridMailAdditionList'=>$modelHybridMailAdditionList,
            'hybridMailAddition'=>$modelHybridMailAddition,
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));



    }

    protected function create_validate_step_4()
    {
        $hybridMailAddition = Array();

        if(isset($_POST))
        {

            if(is_array($_POST['HybridMailAddition']))
                foreach($_POST['HybridMailAddition'] AS $item)
                    if(HybridMailAdditionList::model()->findByPk($item)!==null)
                        array_push($hybridMailAddition, $item);

            if(!is_array($hybridMailAddition))
                $hybridMailAddition = Array();

            $this->setPageState('hybrid_mail_additions',$hybridMailAddition);

        }


    }


    protected function create_step_summary()
    {

        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep =  $this->getPageState('last_step');

        switch($lastStep)
        {
            case 1 :
                if($this->create_validate_step_1() == 'break')
                    return;
                break;
            case 2 :
                if($this->create_validate_step_2() == 'break')
                    return;
                break;
            case 3 :
                if($this->create_validate_step_3() == 'break')
                    return;
                break;
            case 4 :
                if($this->create_validate_step_4() == 'break')
                    return;
                break;
        }

        if(Yii::app()->user->isGuest)
        {
            $currency = $this->getPageState('step1_currency', 1);
            Yii::app()->PriceManager->setCurrency($currency);
        }

        $currency = Yii::app()->PriceManager->getCurrency();


        $model = new HybridMail('step4');
        $model->file_hash = $this->getPageState('file_hash');
        $model->_file_ext = $this->getPageState('fileExt');
        if(!Yii::app()->user->isGuest) $model->user_id = Yii::app()->user->id;

        $model->attributes = $this->getPageState('step1',array());

        $hybridMailAdditions = array();
        $modelsAddressData = array();
        $modelsHybridMailReceiver = array();
        $modelSender = new HybridMail_AddressData('sender');

        $modelSender->attributes = $this->getPageState('step2_model',array());
        $model->senderAddressData = $modelSender;

        foreach($this->getPageState('modelsAddressDataAttributes') AS $key => $item)
        {
            $modelsAddressData[$key] = new HybridMail_AddressData('receiver');
            $modelsAddressData[$key]->attributes = $item;
        }

        /* @var $modelsHybridMailReceiver HybridMailReceiver[] */
        foreach($modelsAddressData AS $key => $item)
        {
            $modelsHybridMailReceiver[$key] = new HybridMailReceiver();
            $modelsHybridMailReceiver[$key]->addressData = $modelsAddressData[$key];
        }

        $model->hybridMailReceivers = $modelsHybridMailReceiver;



        $model->attributes = $this->getPageState('step1',array());
        $model->attributes = $this->getPageState('step2',array());
        $model->addAdditions($this->getPageState('hybrid_mail_additions',array()));

//        $modelHybridMailAddition = HybridMailAdditionList::model()->findAllByAttributes(array('id' => $hybridMailAdditions));

        $priceTotal = $model->priceTotal($currency);
        $this->setPageState('price',$priceTotal); // save previous form into form state

        $this->setPageState('last_step',5); // save information about last step

        $this->setPageState('jump_to_end',true);

        $this->render('create/step5',array(
            'model'=>$model,
            'price_total' => $priceTotal,
            'currency' => $currency,
//            'modelHybridMailAddition' => $modelHybridMailAddition,
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));
    }

    protected function create_validate_step_summary()
    {
        $error = false;
        $model=new HybridMail('step5');
        $model->attributes = $_POST['HybridMail'];
        $model->validate();
        $this->setPageState('step4',$model->getAttributes(array_keys($_POST['HybridMail']))); // save previous form into form state

        if($model->hasErrors() OR $error)
        {
            $model->file_hash = $this->getPageState('file_hash');
            if(!Yii::app()->user->isGuest) $model->user_id = Yii::app()->user->id;

            $model->attributes = $this->getPageState('step1',array());

            $hybridMailAdditions = array();
            $modelsAddressData = array();
            $modelsHybridMailReceiver = array();
            $modelSender = new HybridMail_AddressData('sender');

            $modelSender->attributes = $this->getPageState('step2_model',array());
            $model->senderAddressData = $modelSender;

            foreach($this->getPageState('modelsAddressDataAttributes') AS $key => $item)
            {
                $modelsAddressData[$key] = new HybridMail_AddressData('receiver');
                $modelsAddressData[$key]->attributes = $item;
            }

            /* @var $modelsHybridMailReceiver HybridMailReceiver[] */
            foreach($modelsAddressData AS $key => $item)
            {
                $modelsHybridMailReceiver[$key] = new HybridMailReceiver();
                $modelsHybridMailReceiver[$key]->addressData = $modelsAddressData[$key];
            }

            $model->hybridMailReceivers = $modelsHybridMailReceiver;


            $model->attributes = $this->getPageState('step1',array());
            $model->attributes = $this->getPageState('step2',array());
            $model->addAdditions($this->getPageState('hybrid_mail_additions',array()));

            $modelHybridMailAddition = HybridMailAdditionList::model()->findAllByAttributes(array('id' => $hybridMailAdditions));

            if(!Yii::app()->user->isGuest) $model->user_id = Yii::app()->user->id;


            $this->setPageState('last_step',5); // save information about last step

            $this->setPageState('jump_to_end',true);

            $this->render('create/step5',array(
                'model'=>$model,
                'modelHybridMailAddition' => $modelHybridMailAddition,
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }
    }

    protected function create_finish()
    {
        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 5)
        {
            if($this->create_validate_step_summary() == 'break')
                return;

            $this->setPageState('last_step',6); // save information about last step

            $model = new HybridMail('insert');
            $model->file_hash = $this->getPageState('file_hash');
            $model->attributes = $this->getPageState('step1',array()); //get the info from step 1

            $model->senderAddressData = new HybridMail_AddressData('sender');
            $model->senderAddressData->attributes = $this->getPageState('step2_model',array());
            $model->addAdditions($this->getPageState('hybrid_mail_additions',array()));

            $modelsAddressDataAttributes = $this->getPageState('modelsAddressDataAttributes');

            $modelsReceivers = [];
            foreach($modelsAddressDataAttributes AS $key => $item)
                $modelsReceivers[$key] = HybridMailReceiver::createWithAddressDataAttributes($item);

            $model->hybridMailReceivers = $modelsReceivers;
            $saveSenderToContactBook = $this->getPageState('step2_saveToContactBook');
            $saveReceiversToContactBooks  =  $this->getPageState('step3_saveToContactBooks',array());

            $model->_file_ext = $this->getPageState('fileExt');

            $model->user_id = Yii::app()->user->id;
            $return = $model->saveNewHM($saveSenderToContactBook, $saveReceiversToContactBooks);

            if($return instanceof Order)
            {
                Yii::app()->session['hybridMailFormFinished'] = true;

                /* @var $orderProduct OrderProduct */
                $order = Order::model()->findByPk($return->id); // to get proper hash
                Yii::app()->session['orderHash'] = $order->hash;

                $this->redirect(array('/order/finalizeOrder','urlData' => $order->hash));
            }
            else
                throw new CHttpException(403, 'Wystapił nieznany błąd');

        } else {

            throw new CHttpException(null, 'Sesja została przerwana!');
        }
    }


    public function actionCreate() {

        $this->panelLeftMenuActive = 301;

        if(Yii::app()->session['hybridMailFormFinished']) // prevent going back and sending the same form again
        {
            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['hybridMailFormFinished']);
            $this->refresh(true);
        }

        //for AJAX validation
        {

            if(isset($_POST['HybridMail']))
            {

                $model=new HybridMail('insert');
                $this->performAjaxValidation($model, 'hybrid-mail-form');
                unset($model);
            }

            if(isset($_POST['HybridMailReceiver']))
            {
                $model=new HybridMailReceiver('insert');
                $model->attributes = $_POST['HybridMailReceiver'];
                $this->performAjaxValidation($model, 'hybrid-mail-form');
                unset($model);
            }
        }

        if(isset($_POST['step1cont']))
            $this->create_step_1_cont(); // Verify file
        elseif(isset($_POST['step2']))
            $this->create_step_2(); // Validate HM data, sender
        elseif(isset($_POST['step3']))
            $this->create_step_3(); // Validate sender, receivers
        elseif(isset($_POST['step4']) OR isset($_POST['step4_more']))
            $this->create_step_4(); // Validate receivers, additions
        elseif(isset($_POST['step5']))
            $this->create_step_summary(); // Validate additions, summary
        elseif (isset($_POST['finish']))
            $this->create_finish();
        else
            $this->create_step_1(); // this is the default, first time (step1)

    }

    public function actionList()
    {
        $this->panelLeftMenuActive = 302;


        $model = new HybridMail('search');
        $model->unsetAttributes();

        if (isset($_GET['HybridMail']))
            $model->setAttributes($_GET['HybridMail']);

        $this->render('list', array(
            'model' => $model,
        ));

    }

    public function actionIndex() {

        $this->panelLeftMenuActive = 303;

        $this->render('index', array(
        ));
    }

    public function actionGetFile($urlData)
    {
        /* @var $model HybridMail */
        $model = HybridMail::model()->findByAttributes(array('hash' => $urlData));
        $file = $model->hybridMailAttachments[0];

        if(!isset(Yii::app()->session['fh_'.$model->hash]))
            throw new CHttpException('403', 'Nie masz dostępu do tego pliku!');

        $path = $file->path;

        return Yii::app()->getRequest()->sendFile($file->name, @file_get_contents($path), $file->type);
    }

    public function actionGetTmpFile($urlData, $e)
    {
        $ext = basename($e);
        $urlData = basename($urlData);

        $path = HybridMail::getTmpDir($urlData.'.'.$e);

        if(!is_file($path))
            throw new CHttpException(404);

        $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
        $mimeExt = finfo_file($finfo, $path);


        return Yii::app()->getRequest()->sendFile('document.'.$ext, @file_get_contents($path), $mimeExt);
    }


    public function actionAjaxGetPrice()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $pages = $_POST['pages'];
            $receiver_country_id = $_POST['receiver_country_id'];
            $noTax = $_POST['noTax'] ? true : false;

            $model = new HybridMail();
            $model->user_id = Yii::app()->user->id;
            $model->pages = $pages;
            $model->hybridMailReceivers = [];

            $hmReceiver = new HybridMailReceiver();
            $hmReceiver->addressData = new HybridMail_AddressData();
            $hmReceiver->addressData->country0 = new CountryList();
            $hmReceiver->addressData->country0->id = $receiver_country_id;
            $hmReceiver->addressData->country_id = $receiver_country_id;

            $model->hybridMailReceivers = [$hmReceiver];

            $currency = Yii::app()->PriceManager->getCurrency();
            $currencySymbol = Yii::app()->PriceManager->getCurrencySymbol();

            $price = $model->getTotalPrice($currency);

            if ($price == 0)
                $available = false;
            else {
                $available = true;
                $price = Yii::app()->PriceManager->getFinalPrice($price, $noTax) . ' ' . $currencySymbol;
            }

            echo CJSON::encode([
                    'available' => $available,
                    'price' => $price,
                ]
            );
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }
    }

}