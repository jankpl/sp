<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'update-trakcing-id',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'track_id'); ?>
        <?php echo $form->textField($model, 'track_id', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'track_id'); ?>
    </div><!-- row -->

    <?php
    if($model->operator != CourierLabelNew::OPERATOR_OWN_EXTERNAL_RETURN):
        ?>
        <div class="row">
            <?php echo $form->labelEx($model,'operator'); ?>
            <?php echo $form->dropDownList($model, 'operator', CourierLabelNew::getOperators()); ?>
            <?php echo $form->error($model,'operator'); ?>
        </div><!-- row -->
    <?php
    else:
        ?>
        <div class="row">
            <?php echo $form->labelEx($model,'_ext_operator_name'); ?>
            <?php echo $form->textField($model, '_ext_operator_name'); ?>
            <?php echo $form->error($model,'_ext_operator_name'); ?>
        </div><!-- row -->
    <?php
    endif;
    ?>

    <?php
    echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING,'Label will be removed from this package');
    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->