<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
    public $name;
    public $email;
    public $body;
    public $verifyCode;
    public $accept;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('name, body', 'required'),
            // email has to be a valid email address
            array('email', 'email'),
            // verifyCode needs to be entered correctly
            array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
            array('accept', 'compare', 'compareValue' => true, 'message' => Yii::t('rodo', 'Potwierdzenie jest wymagane')),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'name'=>Yii::t('m_ContactForm','Imię i nazwisko'),
            'email'=>Yii::t('m_ContactForm','Email'),
//			'subject'=>Yii::t('m_ContactForm','Temat'),
            'body'=>Yii::t('m_ContactForm','Treść'),
            'verifyCode'=>Yii::t('m_ContactForm','Kod sprawdzający'),
            'accept'=> Yii::t('rodo', 'Zapoznałem się i akcetpuję {tooltip}informację o administratorze i warunki przetwarzaniu danych{/tooltip} ', ['{tooltip}' => '<a rel="tooltip" title="" data-original-title="'.self::getRodoFullText().'" style="text-decoration: underline;">', '{/tooltip}' => '</a>'])
        );
    }

    public static function getRodoFullText()
    {
        return Yii::t('rodo', 'Wyrażam zgodę na przetwarzanie danych osobowych zgodnie z ustawą o ochronie danych osobowych w związku z zapytaniem przez formularz kontaktowy. Podanie danych jest dobrowolne, ale niezbędne do przetworzenia zapytania. Zostałem poinformowany, że przysługuje mi prawo dostępu do swoich danych, możliwości ich przetwarzania i żądania zaprzestania ich przetwarzania. Administratorem danych osobowych jest firma Świat Przesyłek.');

    }
}