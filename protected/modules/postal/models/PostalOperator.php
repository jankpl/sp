<?php

Yii::import('application.modules.postal.models._base.BasePostalOperator');

class PostalOperator extends BasePostalOperator
{
    const STAMP_MAX_WIDTH = 500;
    const STAMP_MAX_HEIGHT = 500;

    const LABEL_MODE_WHOLE_EXTERNAL = 1;
    const LABEL_MODE_STAMP = 2;
    const LABEL_MODE_WHOLE_INTERNAL = 3;
    const LABEL_MODE_CN23 = 4;
    const LABEL_MODE_CN22_CPOST = 5;

    public $_ooe_login;
    public $_ooe_account;
    public $_ooe_pass;
    public $_ooe_service;

    const OPERATOR_PP_WROCLAW_Polecona_Firmowa = GLOBAL_CONFIG::POSTAL_OPERATOR_PP_WROCLAW_Polecona_Firmowa;
    const OPERATOR_PP_WROCLAW_Polecona_Firmowa_PRIO = GLOBAL_CONFIG::POSTAL_OPERATOR_PP_WROCLAW_Polecona_Firmowa_PRIO;
    const OPERATOR_PP_NYSA_Polecona_Firmowa = GLOBAL_CONFIG::POSTAL_OPERATOR_PP_NYSA_Polecona_Firmowa;
    const OPERATOR_PP_NYSA_Polecona_Firmowa_PRIO = GLOBAL_CONFIG::POSTAL_OPERATOR_PP_NYSA_Polecona_Firmowa_PRIO;
    const OPERATOR_PP_BOCHNIA_Polecona_Firmowa = GLOBAL_CONFIG::POSTAL_OPERATOR_PP_BOCHNIA_Polecona_Firmowa;
    const OPERATOR_PP_BOCHNIA_Polecona_Firmowa_PRIO = GLOBAL_CONFIG::POSTAL_OPERATOR_PP_BOCHNIA_Polecona_Firmowa_PRIO;
    const OPERATOR_PP_WARSZAWA_Polecona_Firmowa = GLOBAL_CONFIG::POSTAL_OPERATOR_PP_WARSZAWA_Polecona_Firmowa;
    const OPERATOR_PP_WARSZAWA_Polecona_Firmowa_PRIO = GLOBAL_CONFIG::POSTAL_OPERATOR_PP_WARSZAWA_Polecona_Firmowa_PRIO;

    const UNIQID_OPERATOR_PP_FIREBUSINESS_Polecona_Firmowa = 10;
    const UNIQID_OPERATOR_PP_FIREBUSINESS_Polecona_Firmowa_PRIO = 11;
    const UNIQID_OPERATOR_PP_SP_Nysa_Polecona_Firmowa = 12;
    const UNIQID_OPERATOR_PP_SP_Zabrze_Polecona_Firmowa = 13;
    const UNIQID_OPERATOR_PP_SP_Zabrze_Polecona_Firmowa_OWN_LABEL = 14;


    const UNIQID_OPERATOR_DHLDE_PAKET = 20;
//    const UNIQID_OPERATOR_SWEDEWN_POST = 30;


    public function getNameWithBroker()
    {
        $name = ''.$this->id.') '.$this->name;
        if($this->broker_id)
            $name .= ' ['.self::getBrokerNameById($this->broker_id).']';

        return $name;
    }

    public static function isOperatorReg($operator_id)
    {
        $cmd = Yii::app()->db->cache(60)->createCommand();
        $cmd->select('COUNT(id)')->from('postal_operator')->where('id = :id AND label_mode = :label_mode', [':id' => $operator_id, ':label_mode' => self::LABEL_MODE_WHOLE_EXTERNAL]);
        return $cmd->queryScalar() ? true : false;
    }

    public static function getBrokerNameById($broker_id)
    {
        return isset(self::getBrokerList()[$broker_id]) ? self::getBrokerList()[$broker_id] : NULL;
    }

    public static function getOperators($onlyReg = false, $exceptReg = false)
    {
        $models = self::model();

        if($onlyReg)
            $models = $models->findAllByAttributes(['label_mode' => self::LABEL_MODE_WHOLE_EXTERNAL]);
        else if($exceptReg)
            $models = $models->findAll('label_mode != :lm', [':lm' =>  self::LABEL_MODE_WHOLE_EXTERNAL]);
        else
            $models = $models->findAll();

        return $models;
    }

    /**
     * Returns array with ID's of operators for giver broker
     * @param $broker_id
     * @return array|CDbDataReader
     */
    public static function getOperatorsForBroker($broker_id)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        $cmd->select('id');
        $cmd->from((new self)->tableName());
        $cmd->where('broker_id = :broker', [':broker' => $broker_id]);
        return $cmd->queryColumn();
    }

    /**
     * Returns uniq_id by operator_id
     * @param $operator_id
     * @return array|CDbDataReader
     */
    public static function getOperatorUniqIdForId($operator_id)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        $cmd->select('uniq_id');
        $cmd->from((new self)->tableName());
        $cmd->where('id = :id', [':id' => $operator_id]);
        $result = $cmd->queryScalar();

        return $result ? $result : NULL;
    }

    /**
     * Returns uniq_id by operator_id
     * @param $uniq_id
     * @return array|CDbDataReader
     */
    public static function getOperatorIdIdForUniqId($uniq_id)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        $cmd->select('id');
        $cmd->from((new self)->tableName());
        $cmd->where('uniq_id = :uniq_id', [':uniq_id' => $uniq_id]);
        $result = $cmd->queryScalar();

        return $result ? $result : NULL;
    }

    public static function getPocztaPolskaPrioOperators($byUniqId = false)
    {
        if($byUniqId)
        {

            return [
                self::UNIQID_OPERATOR_PP_FIREBUSINESS_Polecona_Firmowa_PRIO
            ];

        } else {

            return [
                self::OPERATOR_PP_WROCLAW_Polecona_Firmowa_PRIO,
                self::OPERATOR_PP_BOCHNIA_Polecona_Firmowa_PRIO,
                self::OPERATOR_PP_NYSA_Polecona_Firmowa_PRIO,
                self::OPERATOR_PP_WARSZAWA_Polecona_Firmowa_PRIO,
            ];
        }

    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getBrokerList($onlyAvailableToAdd = false)
    {
        return Postal::getBrokers($onlyAvailableToAdd);
    }

    public static function getStampModeList()
    {
        return [
            self::LABEL_MODE_WHOLE_EXTERNAL => 'External',
            self::LABEL_MODE_STAMP => 'Stamp/PPI',
            self::LABEL_MODE_WHOLE_INTERNAL => 'Internal',
            self::LABEL_MODE_CN23 => 'CN23',
            self::LABEL_MODE_CN22_CPOST => 'CN22_CPOST',
        ];
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            )
        );
    }

    public function rules()
    {
        return array(
            array('partner_id', 'in', 'range' => array_keys(Partners::getParntersList())),


            array('return_address_data_id, stat', 'numerical', 'integerOnly'=>true),

            array('name, stat', 'required'),
            array('broker_id, stat, label_mode', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 64),
            array('_ooe_login, _ooe_account, _ooe_pass, _ooe_service', 'length', 'max' => 64),
            array('stamp', 'safe'),
            array('stamp, uniq_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, date_updated, broker_id, stat', 'safe', 'on' => 'search'),
        );
    }

    public static function getModels($postalType)
    {
        if($postalType == Postal::POSTAL_TYPE_REG)
            $models = self::model()->findAll('stat = :stat AND label_mode = :label_mode', [':stat' => S_Status::ACTIVE, ':label_mode' => self::LABEL_MODE_WHOLE_EXTERNAL]);
        else
//            $models = self::model()->findAll('stat = :stat AND label_mode != :label_mode', [':stat' => S_Status::ACTIVE, ':label_mode' => self::LABEL_MODE_WHOLE_EXTERNAL]);
            $models = self::model()->findAll('stat = :stat', [':stat' => S_Status::ACTIVE]); // REG operators are also allwoed since changes in law

        return $models;
    }

    public function wrapParams()
    {
        $data = [
            '_ooe_login' => $this->_ooe_login,
            '_ooe_account' => $this->_ooe_account,
            '_ooe_pass' => $this->_ooe_pass,
            '_ooe_service' => $this->_ooe_service,
        ];

        $data = serialize($data);
        $data = base64_encode($data);
        $this->params = $data;
    }

    protected function unwrapParams()
    {
        $data = $this->params;
        $data = base64_decode($data);
        $data = unserialize($data);

        if(is_array($data))
            foreach($data AS $key => $item)
            {
                if(property_exists(self::class, $key))
                    $this->$key = $item;
            }

    }

    public function beforeSave()
    {
        if(in_array($this->label_mode, [self::LABEL_MODE_STAMP, self::LABEL_MODE_WHOLE_INTERNAL]))
        {
            $this->broker_id = NULL;
            $this->_ooe_account = NULL;
            $this->_ooe_login = NULL;
            $this->_ooe_pass = NULL;
            $this->_ooe_service = NULL;
        }
        else if(in_array($this->label_mode, [self::LABEL_MODE_WHOLE_EXTERNAL, self::LABEL_MODE_WHOLE_INTERNAL]))
        {
            $this->stamp = NULL;
        } else {
            $this->broker_id = NULL;
            $this->_ooe_account = NULL;
            $this->_ooe_login = NULL;
            $this->_ooe_pass = NULL;
            $this->_ooe_service = NULL;
            $this->stamp = NULL;
        }

        $this->wrapParams();
        return parent::beforeSave();
    }

    public function afterFind()
    {
        $this->unwrapParams();
        return parent::afterFind();
    }


    public function saveWithRelatedModels($ownTransaction = true)
    {
        $errors = false;

        if($this->returnAddressData === null)
            return false;

        $transaction = null;
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();


        if(!$this->returnAddressData->save())
            $errors = true;

        if(!$errors)
        {
            $this->return_address_data_id = $this->returnAddressData->id;

            if(!$this->save())
                $errors = true;
        } else
            $this->validate();

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();
            return false;
        }

        if($ownTransaction)
            $transaction->commit();
        return true;
    }

    public function deleteWithRelatedModels()
    {
        $transaction = Yii::app()->db->beginTransaction();

        try
        {
            AddressData::model()->findByPk($this->address_data_id)->delete();

            parent::delete();
            $transaction->commit();
            return true;
        }
        catch(Exception $ex)
        {
            $transaction->rollBack();
            return false;
        }
    }

    public function getUsefulName()
    {
        $name = '(#'. $this->id .') '.$this->name;

        if(isset(self::getBrokerList()[$this->broker_id]))
            $name .= ' ['.self::getBrokerList()[$this->broker_id].']';

        return $name;
    }

}