<?php

/**
 * CourierScannerForm class.
 * CourierScannerForm is the data structure for CourierScanner form.
 */
class CourierScannerForm extends CFormModel
{
    public $location;
    public $stat_id;
    
    public $package_ids;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(

            array('stat_id', 'required'),
            array('stat_id', 'exist', 'allowEmpty' => true, 'attributeName' => 'id', 'className' => 'CourierStat'),
            
            array('package_ids', 'required', 'on' => 'step2'),
            array('location', 'length', 'max' => 128, 'min' => 1, 'on' => 'step2'),

            array('location', 'application.validators.lettersNumbersBasicValidator'),
            array('location', 'length', 'max' => 128),
        );
    }

    /**
     * Function returns selected status name
     *
     * @return string
     */
    public function getStatusName()
    {
        if($this->stat_id != NULL)
            return CourierStat::model()->findByPk($this->stat_id)->name;
    }

    /**
     * Function return model or array of models of found packages
     *
     * @return Courier[]|NULL|boolean
     */
    public function returnCourierModels()
    {
        if(!is_array($this->package_ids))
            return [];

        $models = [];

        foreach($this->package_ids AS $key => $package_id)
        {
            $package_id = trim($package_id);

            // @ 24.04.2018 - prefilter list
            $package_id = explode(',', $package_id);
            $package_id = end($package_id);

            if($package_id == '')
                continue;

            $model = Courier::model()->findByAttributes(['local_id' => $package_id]);

            if($model === NULL)
                $model = CourierExternalManager::findCourierIdsByRemoteId($package_id, true);

            $models[$key] = $model;
        }


        return $models;
    }


}