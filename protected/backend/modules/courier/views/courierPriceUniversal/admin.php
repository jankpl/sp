<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);
?>

<h1>Manage Courier Price : <?= CourierPriceUniversal::getTypeName($type);?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-country-group-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'date_updated',
        [
            'name' => 'hasPrice',
            'value' => function($data,$row) use($type)
            {
                return $data->getHasPrice($type);
            },
            'filter' => false,
        ],
        [
            'name' => 'noOfGroupPrices',
            'value' => function($data,$row) use($type)
            {
                return $data->getNoOfGroupPrices($type);
            },
            'filter' => false,
        ],
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons'=>array(
                'update'=>array(
                    'url' => 'Yii::app()->createUrl("/courier/courierPriceUniversal/price",array("id" => $data->id, "type" => '.$type.'))',
                ),
            ),
        ),
    ),
)); ?>

<h2>Import / export</h2>

<?php echo CHtml::link('Import',array('import', 'type' => $type),array('class' => 'likeButton'));?> <?php echo CHtml::link('Export',array('export', 'type' => $type),array('class' => 'likeButton'));?>
