<?php
/* @var $model Courier */

$type = $model->getType();
?>

<table class="table table-bordered table-condensed gridview-show-details" style="margin-bottom: 0; table-layout: fixed;">
    <thead>
    <tr>
        <th style="width: 10%;">
            <?= Yii::t('courier', 'Paczka');?>
        </th>
        <th style="width: 30%;">
            <?= Yii::t('courier', 'Szczegóły');?>
        </th>
        <th style="width: 15%;">
            <?= Yii::t('courier', 'Nadawca');?>
        </th>
        <th style="width: 15%;">
            <?= Yii::t('courier', 'Odbiorca');?>
        </th>
        <th style="width: 30%;">
            <?= Yii::t('courier', 'Akcja');?>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="vertical-align: top;">
            <strong><?= $model->getTypeName(true);?></strong><br/>
            <?= $model->package_weight.' '.Courier::getWeightUnit(). ($model->package_weight != $model->package_weight_client && $model->package_weight_client ? ' ('.Yii::t('courier', 'deklarowana').': '.$model->package_weight_client.' '.Courier::getWeightUnit().')' : '');?><br/>
            <?= $model->package_size_l;?> x <?= $model->package_size_w;?> x <?= $model->package_size_d;?> <?= Courier::getDimensionUnit();?><br/>
            <?= $model->package_content;?><br/>
            <?php
            if($model->ref !=''):
                ?>
                <?= Yii::t('courier', 'Ref:');?> <strong><?= $model->ref;?></strong> [<a target="_blank" href="<?= Yii::app()->createUrl('/courier/courier/removeRef', ['hash' => $model->hash]);?>" onclick="return confirm('<?= Yii::t('site', 'Czy jesteś pewien?');?>')"><u><?= Yii::t('courier', 'skasuj');?></u></a>]
            <?php
            endif;
            ?>
        </td>
        <td style="vertical-align: top;">
            <?= Yii::t('courier', 'Status');?>: <strong><?= StatMap::getMapNameById($model->stat_map_id);?></strong><br/>
            <?php if($type == Courier::TYPE_INTERNAL):?>
                <?= $model->courierTypeInternal->getAttributeLabel('with_pickup'); ?>: <strong><?= $model->courierTypeInternal->getWithPickupName(); ?></strong><br/>
                <?= Yii::t('courier', 'Paczka');?> <?= $model->courierTypeInternal->family_member_number;?>/<?= $model->packages_number;?><br/>
                <?= ($model->cod_value > 0)? Yii::t('courier', 'COD').': '.$model->cod_value.' '.$model->cod_currency.'<br/>' : '';?>
                <?= Yii::t('courier', 'Order');?>: #<a href="<?= Yii::app()->urlManager->createUrl('/order/view', ['urlData' => $model->courierTypeInternal->order->hash]);?>" target="_blank"><?= $model->courierTypeInternal->order->local_id; ?></a><br/>
                <?php
                if(S_Useful::sizeof($model->courierTypeInternal->listAdditions())):
                    ?>
                    <?= Yii::t('courier', 'Dodatki');?>:
                    <ul style="padding: 0 0 0 15px; margin: 0;">
                        <?php
                        foreach($model->courierTypeInternal->listAdditions() AS $item): ?>
                            <li><?php echo $item->getClientTitle(); ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif;?>
                <?php
                if(!Yii::app()->user->isGuest && Yii::app()->user->model->getShowCourierInternalOperatorsData()):
                    $operatorsList = $model->courierTypeInternal->listOperatorsWithTt();
                    $operatorsNo = S_Useful::sizeof($operatorsList) ;
                    if($operatorsNo):
                        ?>

                        <?= Yii::t('courier', 'Operatorzy');?>:
                        <?php
                        $i = 1;

                        foreach($operatorsList AS $operator) {
                            echo $operator['name'] . ($operator['tt'] != '' ? ' (' . $operator['tt'] . ')' : '');
                            if($i < $operatorsNo)
                                echo ', ';
                            $i++;
                        }
                        ?>
                    <?php
                    endif;
                    ?>
                <?php
                endif;
                ?>

            <?php elseif($type == Courier::TYPE_RETURN):?>
                <?= $model->courierTypeReturn->getAttributeLabel('base_courier_id'); ?>: <strong><a target="_blank" href="<?= Yii::app()->createUrl('/courier/courier/view', ['urlData' => $model->courierTypeReturn->baseCourier->hash]);?>">#<?= $model->courierTypeReturn->baseCourier->local_id;?></a></strong><br/>
                <?= Yii::t('courier', 'Operator');?>: <strong><?= CourierLabelNew::getOperators()[$model->courierTypeReturn->courierLabelNew->operator] ;?></strong><br/>
                <?= Yii::t('courier', 'Nr zew.');?>: <strong><?= $model->courierTypeReturn->courierLabelNew->track_id ;?></strong><br/>
            <?php elseif($type == Courier::TYPE_U):?>
                <?= Yii::t('courier', 'Paczka');?> <?= $model->courierTypeU->family_member_number;?>/<?= $model->packages_number;?><br/>
                <?= ($model->cod_value > 0)? Yii::t('courier', 'COD').': '.$model->cod_value.' '.$model->cod_currency.'<br/>' : '';?>
                <?= Yii::t('courier', 'Order');?>: #<a href="<?= Yii::app()->urlManager->createUrl('/order/view', ['urlData' => $model->courierTypeU->order->hash]);?>" target="_blank"><?= $model->courierTypeU->order->local_id; ?></a><br/>
                <?php
                if(S_Useful::sizeof($model->courierTypeU->listAdditions())):
                    ?>
                    <?= Yii::t('courier', 'Dodatki');?>:
                    <ul style="padding: 0 0 0 15px; margin: 0;">
                        <?php
                        foreach($model->courierTypeU->listAdditions() AS $item): ?>
                            <li><?php echo $item->getClientTitle(); ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif;?>
                <?= Yii::t('courier', 'Operator');?>: <strong><?= $model->courierTypeU->courierUOperator->name_customer ;?></strong><br/>
                <?= Yii::t('courier', 'Nr zew.');?>: <strong><?= $model->courierTypeU->courierLabelNew->track_id ;?></strong><br/>
            <?php elseif($type == Courier::TYPE_EXTERNAL_RETURN):?>
                <?= $model->courierTypeExternalReturn->courierLabelNew->_ext_operator_name  ? Yii::t('courier', 'Operator').' : <strong>'.$model->courierTypeExternalReturn->courierLabelNew->_ext_operator_name.'</strong><br/>' : ''; ?>
                <?= Yii::t('courier', 'Nr zew.');?>: <strong><?= $model->courierTypeExternalReturn->courierLabelNew->track_id ;?></strong><br/>
            <?php elseif($type == Courier::TYPE_RETURN):?>
                <?= $model->courierTypeReturn->courierOperator->name  ? Yii::t('courier', 'Operator').' : <strong>'.$model->courierTypeReturn->courierOperator->name.'</strong><br/>' : ''; ?>
                <?= Yii::t('courier', 'Nr zew.');?>: <strong><?= $model->courierTypeReturn->courierLabelNew->track_id ;?></strong><br/>
            <?php elseif($type == Courier::TYPE_DOMESTIC):?>
                <?= Yii::t('courier', 'Operator');?>: <strong><?= $model->courierTypeDomestic->getOperator() ;?></strong><br/>
                <?= Yii::t('courier', 'Nr zew.');?>: <strong><?= $model->courierTypeDomestic->courierLabelNew->track_id ;?></strong><br/>
                <?= Yii::t('courier', 'Paczka');?> <?= $model->courierTypeDomestic->family_member_number;?>/<?= $model->packages_number;?><br/>
                <?= ($model->courierTypeDomestic->cod_value > 0)? Yii::t('courier', 'COD').': '.$model->courierTypeDomestic->cod_value.' '.$model->courierTypeDomestic->cod_currency.'<br/>' : '';?>
                <?php
                if(S_Useful::sizeof($model->courierTypeDomestic->listAdditions())):
                    ?>
                    <?= Yii::t('courier', 'Dodatki');?>:
                    <ul style="padding: 0 0 0 15px; margin: 0;">
                        <?php
                        foreach($model->courierTypeDomestic->listAdditions() AS $item): ?>
                            <li><?php echo $item->getClientTitle(); ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif;?>
            <?php elseif($type == Courier::TYPE_INTERNAL_SIMPLE):?>
                <?= Yii::t('courier', 'Operator');?>: <strong><?= $model->courierTypeInternalSimple->getOperator();?></strong><br/>
                <?= Yii::t('courier', 'Nr zew.');?>: <strong><?= $model->courierTypeInternalSimple->courierExtOperatorDetails->courierLabelNew->track_id;?></strong><br/>
                <?= $model->courierTypeInternalSimple->getAttributeLabel('with_pickup'); ?>: <strong><?= $model->courierTypeInternalSimple->with_pickup ? Yii::t('site', 'tak') : Yii::t('site', 'nie'); ?></strong>
            <?php elseif($type == Courier::TYPE_INTERNAL_OOE):?>
                <?= Yii::t('courier', 'Operator');?>: <strong><?= $model->courierTypeOoe->findServiceName() ;?></strong><br/>
                <?= Yii::t('courier', 'Nr zew.');?>: <strong><?= $model->courierTypeOoe->findRemoteId();?></strong><br/>
                <?php
                if(S_Useful::sizeof($model->courierTypeOoe->listAdditions())):
                    ?>
                    <?= Yii::t('courier', 'Dodatki');?>:
                    <ul style="padding: 0 0 0 15px; margin: 0;">
                        <?php
                        foreach($model->courierTypeOoe->listAdditions() AS $item): ?>
                            <li><?php echo $item->getClientTitle(); ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif;?>
            <?php endif;?>
            <?php
            if(!Yii::app()->user->isGuest && (($model->getType() == Courier::TYPE_INTERNAL && Yii::app()->user->model->getShowCourierInternalOperatorsData()) OR $model->getType() != Courier::TYPE_INTERNAL)):
                $collect = CourierCollect::findForCourierId($model->id);
                if($collect !== NULL && $collect->prn != '' && $collect->stat == CourierCollect::STAT_SUCCESS):
                    ?>
                    <?= Yii::t('courier', 'Numer obioru');?>: <?= $collect->prn; ?><br/>
                <?php
                endif;
            endif;
            ?>
        </td>
        <td style="vertical-align: top;">
            <?= $model->senderAddressData->name != '' ? '<strong>'.$model->senderAddressData->name.'</strong><br/>' : '';?>
            <?= $model->senderAddressData->company != '' ? '<strong>'.$model->senderAddressData->company.'</strong><br/>' : '';?>
            <?= $model->senderAddressData->address_line_1; ?><br/>
            <?= $model->senderAddressData->address_line_2 != '' ? '<strong>'.$model->senderAddressData->address_line_2.'</strong><br/>' : '';?>
            <?= $model->senderAddressData->city; ?>, <?= $model->senderAddressData->zip_code;?><br/>
            <?= $model->senderAddressData->country0->getTrName();?> (<?= $model->senderAddressData->country0->code;?>)
        </td>
        <td style="vertical-align: top;">
            <?= $model->receiverAddressData->name != '' ? '<strong>'.$model->receiverAddressData->name.'</strong><br/>' : '';?>
            <?= $model->receiverAddressData->company != '' ? '<strong>'.$model->receiverAddressData->company.'</strong><br/>' : '';?>
            <?= $model->receiverAddressData->address_line_1; ?><br/>
            <?= $model->receiverAddressData->address_line_2 != '' ? '<strong>'.$model->receiverAddressData->address_line_2.'</strong><br/>' : '';?>
            <?= $model->receiverAddressData->city; ?>, <?= $model->receiverAddressData->zip_code;?><br/>
            <?= $model->receiverAddressData->country0->getTrName();?> (<?= $model->receiverAddressData->country0->code;?>)
        </td>
        <td style="vertical-align: top;">
            <ul style="padding: 0; margin: 0;">
                <li><a href="<?= Yii::app()->createUrl('/site/tt',['urlData' => $model->local_id]);?>" target="_blank"><?= Yii::t('courier', 'Track&trace');?></a></li>
                <?php if($model->courier_stat_id != CourierStat::CANCELLED && $model->courier_stat_id != CourierStat::CANCELLED_PROBLEM && $model->courier_stat_id != CourierStat::CANCELLED_USER && !in_array($model->user_cancel, [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED, Courier::USER_CANCEL_CONFIRMED])): ?><li><?php echo CHtml::link(Yii::t('courier','Generuj kartę potwierdzenia'), array('/courier/courier/ackCard', 'hash' => $model->hash), array('target' => '_blank', 'data-popup-me' => "AckCard ".$model->local_id));?></li><?php endif;?>
                <li><?php echo CHtml::link(Yii::t('courier','Generuj kartę potwierdzenia statusu'), array('/courier/courier/statConf', 'hash' => $model->hash), array('target' => '_blank', 'data-popup-me' => "AckCard ".$model->local_id));?></li>
                <?php if($model->isLabelGeneratingAvailableBaseOnStat()
                    && ((($model->courierTypeInternalSimple !== NULL OR $model->courierTypeInternal != NULL OR $model->courierTypeDomestic != NULL OR ($model->courierTypeOoe != NULL AND $model->courierTypeOoe->courier_label_new_id != NULL)) AND ($model->courier_stat_id != CourierStat::PACKAGE_PROCESSING AND $model->courier_stat_id != CourierStat::NEW_ORDER)) OR $model->courierTypeExternal != NULL OR $model->courierTypeU != NULL) && !in_array($model->user_cancel, [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED, Courier::USER_CANCEL_CONFIRMED])):?><li><strong><?= Yii::t('courier', 'Generate label');?>:</strong><br/>
                    <?php
                    $printLinks = [];
                    foreach(LabelPrinter::getLabelGenerateTypes(false) AS $type => $typeName)
                        $printLinks[] = '<a href="'.Yii::app()->createUrl('/courier/courier/CarriageTicket', ['urlData' => $model->hash, 'type' => $type]).'" target="_blank" class="btn btn-xs" data-popup-me="Label '.$model->local_id.' '.$type.'">'.$typeName.'</a>';
                    $printLinks = implode(' | ', $printLinks);
                    echo $printLinks;
                    ?>
                    </li>
                    <?php
                    if(!in_array($model->receiverAddressData->country_id, CountryList::getUeList(true))):
                        ?>
                        <li><?= CHtml::link(Yii::t('courier','Generuj dokumenty celne'), array('/courier/courier/customsPacket', 'hash' => $model->hash), array('target' => '_blank'));?></li>
                    <?php
                    endif;
                    ?>

                <?php endif;?>
            </ul>
        </td>
    </tr>
    </tbody>
</table>