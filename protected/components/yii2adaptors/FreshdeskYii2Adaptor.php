<?php

class FreshdeskYii2Adaptor
{
    const TYPE_COURIER = 1;
    const TYPE_POSTAL = 2;


    public static function checkIfHasTicket($type, $product_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id')->from('ticket')->where('package_type = :type AND package_id = :product_id', [
            ':type' => $type,
            ':product_id' => $product_id
        ]);
        return $cmd->queryScalar();
    }


}