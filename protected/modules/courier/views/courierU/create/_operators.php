<?php
/* @var $operators [] */
/* @var $selectedOperator int */
?>
<div class="inline-form">
    <div style="text-align: center;">
        <span class="courier-u-operators-selector-form">
            <?php
            if(!S_Useful::sizeof($operators)):
                ?>
                <span class="badge"><?= Yii::t('courier', 'Brak operatorów dla podanych parametrów paczki');?></span>
                <br/>
                <br/>
            <?php
            else:
                foreach($operators AS $operator):
                    $available = true;
                    if(!$operator['price'] OR $operator['price'] == 'false')
                        $available = false;
                    ?>
                    <label data-price="<?= (($operator['price'] && $operator['price'] != 'false') ? Yii::app()->PriceManager->getFinalPrice($operator['price']) : '');?>" data-name="<?= $operator['operatorModel']->name_customer;?>" data-time="<?= $operator['delivery_hours'];?>" data-cod="<?= $operator['cod'];?>" data-broker-id="<?= $operator['operatorModel']->broker_id;?>" data-operator-id="<?= $operator['operatorModel']->id;?>" data-dim-weight-mode="<?= $operator['operatorModel']->dimWeightMode;?>" data-collection="<?= $operator['operatorModel']->isCollectable();?>" class="<?= !$available ? 'not-available' : '';?>" data-available="<?= $available;?>" data-cod-n="<?= S_Price::formatPrice($operator['cod_price']);?>" data-cod-b="<?= S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($operator['cod_price']));?>">
                       <span <?= $operator['text'] ? 'rel="tooltip" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-html="true" data-title="'.$operator['text'].'"' : '';?>>
                        <?php
                        if(!$available):
                            ?>
                            <a class="not-available-wrapper" href="<?= Yii::app()->urlManager->createUrl('/site/contact');?>" target="_blank"><span><?= Yii::t('courier','Zapytaj o aktywację usługi');?></span></a>
                        <?php
                        endif;
                        ?>
                           <?= CHtml::radioButton('CourierTypeU[courier_u_operator_id]', $operator['operatorModel']->id == $selectedOperator && $available, ['disabled' => $available ? false : true, 'data-cod' => $operator['cod'], 'data-broker' => $operator['operatorModel']->broker_id, 'value' => $operator['operatorModel']->id]); ?>


                           <div class="operator-content-div" style="background-color: #<?= $operator['operatorModel']->getBgColor();?>">
    <div class="operator-u-name" style=" color: #<?= $operator['operatorModel']->getFontColor();?>;"><?= $operator['operatorModel']->name_customer;?></div>
                               <?php
                               if(!Yii::app()->user->model->getHidePrices()):
                                   ?>
                                   <div style="position: absolute; bottom: 10px; left: 0; right: 0;">
        <p class="netto"><span><?= (($operator['price'] && $operator['price'] != 'false')  ? S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($operator['price'], true)) : '?').'</span> '.CourierTypeU::getCurrencySymbolForCurrentUser().' '.Yii::t('courier','netto').'</p>
                    <p class="brutto"><span>'.(($operator['price'] && $operator['price'] != 'false') ? S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($operator['price'])) : '?').'</span> '.CourierTypeU::getCurrencySymbolForCurrentUser(); ?> <?= Yii::t('courier','brutto'); ?></p>
        <p class="time" title="<?= Yii::t('courier', 'Średni czas dostawy paczki');?>">~ <?= $operator['delivery_hours'] ? $operator['delivery_hours'] : '?';?> h</p>
    </div>
                               <?php
                               endif;
                               ?>
</div>

                        </span>
                    </label>



                    <?php
                    $i++;
                endforeach;
            endif;
            ?>
    </div>
</div>

