<?php
/* @var $model Postal */
/* @var $e boolean */
/* @var $l boolean */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'courier-edit-form', // imporant for importListModels.js!
    'htmlOptions' => ['data-edit-form-id' => $id],
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'stateful'=>false,
    'clientOptions' => [
        'validateOnSubmit'=> true,
        'afterValidate'=>'js:function(form,data,hasError){
                        if(!hasError){
                             onItemEditSubmit("'.Yii::app()->createUrl("postal/postal/ajaxSaveRowOnImport", ['e' => $e, 'l' => $l]).'");
                        } else {
                            $(".overlay").hide();
                            alert("'.Yii::t('courier','Proszę poprawić błędy formularza!').'");
                         }
                        }'
    ],
));
?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= Yii::t('courier', 'Edycja danych');?></h4>
    </div>
    <div class="modal-body" id="edit-modal-content">

        <div class="form" id="postal-edit-item-form">

            <h4><?php echo Yii::t('postal','Package data');?></h4>
            <div class="inline-form">
                <div style="float: left; width: 48%;">
                    <div class="row">
                        <?php echo $form->labelEx($model,'postal_type'); ?>
                        <?php echo $form->dropDownList($model, 'postal_type', Postal::getPostalTypeList(), ['prompt' => '--']); ?>
                        <?php echo $form->error($model,'postal_type'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'size_class'); ?>
                        <?php echo $form->dropDownList($model, 'size_class', Postal::getSizeClassList(), ['prompt' => '--']); ?>
                        <?php echo $form->error($model,'size_class'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'weight'); ?>
                        <?php echo $form->textField($model, 'weight'); ?>
                        <?php echo $form->error($model,'weight'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'value'); ?>
                        <?php echo $form->textField($model, 'value'); ?>
                        <?php echo $form->error($model,'value'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'value_currency'); ?>
                        <?php echo $form->dropDownList($model, 'value_currency', Postal::getCurrencyList(), ['prompt' => '-']); ?>
                        <?php echo $form->error($model,'value_currency'); ?>
                    </div><!-- row -->
                </div>
                <div style="float: right; width: 48%;">
                    <div class="row">
                        <?php echo $form->labelEx($model,'target_sp_point'); ?>
                        <?php echo $form->dropDownList($model, 'target_sp_point', CHtml::listData($points, 'id' ,'spPointsTr.title'), ['prompt' => '-- '.Yii::t('site', 'wybierz punkt z listy').' --', 'data-sp-points-selector' => true]); ?>
                        <?php echo $form->error($model,'target_sp_point'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'content'); ?>
                        <?php echo $form->textField($model, 'content'); ?>
                        <?php echo $form->error($model,'content'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'ref'); ?>
                        <?php echo $form->textField($model, 'ref'); ?>
                        <?php echo $form->error($model,'ref'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'note1'); ?>
                        <?php echo $form->textField($model, 'note1'); ?>
                        <?php echo $form->error($model,'note1'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'note2'); ?>
                        <?php echo $form->textField($model, 'note2'); ?>
                        <?php echo $form->error($model,'note2'); ?>
                    </div><!-- row -->
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="inline-form">
                <div style="width: 48%; float: left">
                    <h4><?= Yii::t('courier','Nadawca');?></h4>
                    <?php
                    $this->renderPartial('_addressData/_form',
                        array(
                            'model' => $model->senderAddressData,
//                    'listAddressData' => UserContactBook::listContactBookItems('sender'),
                            'form' => $form,
                            'noEmail' => false,
                            'i' => 'sender',
//                        'blockAddressSuggester' => true,
                            'noRequiredInfo' => true,
                        )
                    );
                    ?>
                </div>
                <div style="width: 48%; float: right">
                    <h4><?= Yii::t('courier','Odbiorca');?></h4>
                    <?php
                    $this->renderPartial('_addressData/_form',
                        array(
                            'model' => $model->receiverAddressData,
//                    'listAddressData' => UserContactBook::listContactBookItems('receiver'),
                            'form' => $form,
                            'noEmail' => false,
                            'i' => 'receiver',
//                        'blockAddressSuggester' => true,
//                        'countries' => $countriesReceiver,
//                    'focusFirst' => true,
                            'noRequiredInfo' => true,
                        ));?>
                </div>
                <div style="clear: both;"></div>
                <p class="note text-left">
                    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
                </p>
            </div>

        </div>

    </div>
    <div class="modal-footer">
        <?php echo TbHtml::button(Yii::t('app', 'Cancel'), array('class' => 'btn btn-warning', 'data-cancel' => 'true', 'data-dismiss' => 'modal')); ?>
        <?php echo TbHtml::submitButton(Yii::t('app', 'Save changes'), array('class' => 'btn-lg btn-primary', 'data-save-row-changes' => $id)); ?>
    </div>

<?php echo CHtml::hiddenField('hash', $hash); ?>
<?php echo CHtml::hiddenField('id', $id); ?>
<?php $this->endWidget();
?>

<?php
// DOUBLE LOADING OF JQUERY WAS BREAKING SCRIPTS
Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.yiiactiveform.js'] = false;