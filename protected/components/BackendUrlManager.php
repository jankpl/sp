<?php

class BackendUrlManager extends CUrlManager
{

    public static function createAbsoluteUrl($url, $params=array(), $absolute=true)
    {
        // get the current base url
        $thisBaseUrl = Yii::app()->urlManager->getBaseUrl();
        // find the frontend base url.

        if(!strpos('index.php',$thisBaseUrl))
            $frontEndBaseUrl = $thisBaseUrl.'/backend.php';
        else
            $frontEndBaseUrl = str_replace('index.php','backend.php', $thisBaseUrl);

        // temporary set the base url for frontend.
        Yii::app()->urlManager->setBaseUrl($frontEndBaseUrl);
        // create the url
        if(!$absolute)
            $url=Yii::app()->createUrl($url, $params);
        else
            $url=Yii::app()->createAbsoluteUrl($url, $params);
        // put back the original url
        Yii::app()->urlManager->setBaseUrl($thisBaseUrl);
        // return the generated one.
        return $url;
    }

}