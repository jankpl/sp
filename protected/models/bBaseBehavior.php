<?php

abstract class bBaseBehavior extends CActiveRecordBehavior
{

    protected $_allowedClass;
    protected $_owner;

    protected function loadOwner()
    {
        $this->_owner = $this->getOwner();

        $validClass = false;

        if(!$this->_allowedClass)
            $validClass = true;
        else
            if(is_array($this->_allowedClass))
                foreach($this->_allowedClass AS $item)
                {
                    if($this->_owner instanceof $item)
                        $validClass = true;
                }

        if(!$validClass)
            throw new CException('Class '.get_class($this->_owner).' cannot use behavior '.get_class($this).'!');

    }

    protected function owner()
    {
        if(!$this->_owner)
            $this->loadOwner();

        return $this->_owner;
    }

    protected function getClass()
    {
        return get_class($this->owner());
    }


}