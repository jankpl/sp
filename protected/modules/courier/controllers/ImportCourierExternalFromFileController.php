<?php

class ImportCourierExternalFromFileController extends Controller {

    public function filters() {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        $this->panelLeftMenuActive = 132;

        $fileModel = new F_UploadFile();

        // mapping attributes to columns in file
        $attributes = Array(
            'package_weight' => 0,
            'package_size_l' => 1,
            'package_size_w' => 2,
            'package_size_d' => 3,
            'packages_number' => 4,
            'package_value' => 5,
            'package_content' => 6,

            'sender_name' => 7,
            'sender_company' => 8,
            'sender_country' => 9,
            'sender_zip_code' => 10,
            'sender_city' => 11,
            'sender_address_line_1' => 12,
            'sender_address_line_2' => 13,
            'sender_tel' => 14,

            'receiver_name' => 15,
            'receiver_company' => 16,
            'receiver_country' => 17,
            'receiver_zip_code' => 18,
            'receiver_city' => 19,
            'receiver_address_line_1' => 20,
            'receiver_address_line_2' => 21,
            'receiver_tel' => 22,
        );

        $hash = $_REQUEST['hash'];
        $step = $_REQUEST['step'];
        $part = $_REQUEST['part'];
        $ajax = $_GET['ajax'];

        ///////////////

        if($hash == NULL)
        {
            if(Yii::app()->session['lffs']['hash'] == '')
            {
                $hash = md5(time()).Yii::app()->user->id;
                $session = Yii::app()->session['lffs'];
                $session['hash'] = $hash;
                $session['step'] = 1;
                $session['alreadySaved'] = 0;
                Yii::app()->session['lffs'] = $session;
            }
            else
            {
                $hash = Yii::app()->session['lffs']['hash'];
                $session = Yii::app()->session['lffs'];
                $session['step'] = 1;
                $session['alreadySaved'] = 0;
                Yii::app()->session['lffs'] = $session;
            }
        }

        // file upload
        if(isset($_POST['F_UploadFile']))
        {
            $fileModel->setAttributes($_POST['F_UploadFile']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if(!$fileModel->validate())
            {
                $this->render('createExternalFromFile/index', array(
                    'fileModel' => $fileModel,
                ));
                return;
            } else {
                // file upload success:

                $inputFile = $fileModel->file->tempName;

//                Yii::import('application.extensions.phpexcel.XPHPExcel');
//                XPHPExcel::init();

                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFile);
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($inputFile);

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $data = [];
                $start = 1;

                if($fileModel->omitFirst)
                {
                    $headerSeen = false;
                } else
                    $headerSeen = true;

                for ($row = $start; $row <= $highestRow; $row++){
                    if(!$headerSeen)
                    {
                        $headerSeen = true;
                        continue;
                    }

                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                    array_push($data, $rowData[0]);
                }
                Yii::app()->cacheUserData->set('tempInput_'.$hash, $data, 86400);

                switch($fileModel->split)
                {
                    case F_UploadFile::SPLIT_FULL_EDIT:
                        $splitAfter = F_UploadFile::SPLIT_AFTER_FULL_EDIT;
                        break;
                    case F_UploadFile::SPLIT_DEFAULT:
                    default:
                        $splitAfter = F_UploadFile::SPLIT_AFTER_DEFAULT;
                        break;
                }

                $session = Yii::app()->session['lffs'];
                $session['split'] = $fileModel->split;
                $session['splitAfter'] =  $splitAfter;
                $session['lines'] = S_Useful::sizeof($data);
                $session['parts'] = ceil(S_Useful::sizeof($data) / $splitAfter);

                Yii::app()->session['lffs'] = $session;

//                Yii::log('Sesja:'.print_r($session['split'],1),CLogger::LEVEL_ERROR);

                $this->render('createExternalFromFile/index', array(
                    'fileModel' => $fileModel,
                    'uploadSuccess' => true,
                ));
                return;
            }

        } else if($ajax == 1) {

            // because models are loaded from cacheUserData:
            require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
            Yii::import('zii.behaviors.CTimestampBehavior');
            //

            if($step == NULL OR $step == 1)
            {

                $return = array(
                    'hash' => $hash,
                    'lastStep' => 1,
                    'parts' => $session['parts'],
                    'part' => $part,
                );

                echo CJSON::encode($return);
                return;
            }
            else if($step == 2)
            {
//                Yii::log('step 2', CLogger::LEVEL_ERROR);

                $models = [];
                $data = Yii::app()->cacheUserData->get('tempInput_'.$hash);

                $session = Yii::app()->session['lffs'];

                $start =  $session['splitAfter'] * ($part - 1);
                $end = $start + $session['splitAfter'];

                if(S_Useful::sizeof($data))
                {
                    $i = 0;
                    foreach($data AS $item)
                    {
                        $i++;
                        if($i < $start OR $i >= ($end))
                            continue;

                        $courier = new Courier;
                        $courier->courierTypeExternal = new CourierTypeExternal();
                        $courier->senderAddressData = new AddressData();
                        $courier->receiverAddressData = new AddressData();

                        $courier->package_weight = $item[$attributes['package_weight']];
                        $courier->package_size_l = $item[$attributes['package_size_l']];
                        $courier->package_size_w = $item[$attributes['package_size_w']];
                        $courier->package_size_d = $item[$attributes['package_size_d']];
                        $courier->packages_number = $item[$attributes['packages_number']];
                        $courier->package_value = $item[$attributes['package_value']];
                        $courier->package_content = $item[$attributes['package_content']];

                        $courier->senderAddressData->name = $item[$attributes['sender_name']];
                        $courier->senderAddressData->company = $item[$attributes['sender_company']];
                        $courier->senderAddressData->country_id = CountryList::findIdByText( $item[$attributes['sender_country']]);
                        $courier->senderAddressData->zip_code = $item[$attributes['sender_zip_code']];
                        $courier->senderAddressData->city = $item[$attributes['sender_city']];
                        $courier->senderAddressData->address_line_1 = $item[$attributes['sender_address_line_1']];
                        $courier->senderAddressData->address_line_2 = $item[$attributes['sender_address_line_2']];
                        $courier->senderAddressData->tel = $item[$attributes['sender_tel']];

                        $courier->receiverAddressData->name = $item[$attributes['receiver_name']];
                        $courier->receiverAddressData->company = $item[$attributes['receiver_company']];
                        $courier->receiverAddressData->country_id = CountryList::findIdByText( $item[$attributes['receiver_country']]);
                        $courier->receiverAddressData->zip_code = $item[$attributes['receiver_zip_code']];
                        $courier->receiverAddressData->city = $item[$attributes['receiver_city']];
                        $courier->receiverAddressData->address_line_1 = $item[$attributes['receiver_address_line_1']];
                        $courier->receiverAddressData->address_line_2 = $item[$attributes['receiver_address_line_2']];
                        $courier->receiverAddressData->tel = $item[$attributes['receiver_tel']];

                        $courier->validate();
                        $courier->receiverAddressData->validate();
                        $courier->senderAddressData->validate();
                        $courier->courierTypeExternal->validate();


                        array_push($models, $courier);
                    }
                    Yii::app()->cacheUserData->set('tempModels_'.$hash.'_'.$part, $models,86400);

                    $return = array(
                        'hash' => $hash,
                        'lastStep' => 2,
                        'part' => $part,
                        'size' => S_Useful::sizeof($models),
                    );

                    echo CJSON::encode($return);
                    return;
                }


            }
            else if($step == 3)
            {

                $models = Yii::app()->cacheUserData->get('tempModels_'.$hash.'_'.$part);

                $session = Yii::app()->session['lffs'];
                $remaining = $session['parts'] - $session['alreadySaved'];



                $html = $this->renderPartial('createExternalFromFile/createExternalFromFile', array(
                    'models' => $models,
                    'remaining' => $remaining,
                    'total' => $session['parts'],
                    'saved' => $session['alreadySaved'],
                    'split' => $session['split'],
                ), true, false);


                echo CJSON::encode(array('html' => $html));
                return;
            }
            else if($step == 4)
            {


                $mode = $_POST['mode'];


                $session = Yii::app()->session['lffs'];
                $data = Yii::app()->cacheUserData->get('tempModels_'.$hash.'_'.$part);

                $models = [];
                if(is_array($_POST['Courier']))
                    foreach($_POST['Courier'] AS $key => $item)
                    {

                        $courier = new Courier();
                        $courier->senderAddressData = new AddressData();
                        $courier->receiverAddressData = new AddressData();
                        $courier->courierTypeExternal = new CourierTypeExternal();

                        $courier->setAttributes($item);

                        $courier->senderAddressData->setAttributes($_POST['AddressData'][$key]['sender']);
                        $courier->receiverAddressData->setAttributes($_POST['AddressData'][$key]['receiver']);

                        $courier->courierTypeExternal->setAttributes($_POST['CourierTypeExternal'][$key]);

                        // load data again from file stored in session


                        /* @var $item Courier */
                        if(is_array($data) && $session['split'] != F_UploadFile::SPLIT_FULL_EDIT)
                        {
                            $item = $data[$key];

                            $courier->package_weight = $item->package_weight;
                            $courier->package_size_l = $item->package_size_l;
                            $courier->package_size_w = $item->package_size_w;
                            $courier->package_size_d = $item->package_size_d;
                            $courier->package_value = $item->package_value;
                            $courier->package_content = $item->package_content;

                            $courier->senderAddressData->name = $item->senderAddressData->name;
                            $courier->senderAddressData->company = $item->senderAddressData->company;
                            $courier->senderAddressData->country_id = CountryList::findIdByText($item->senderAddressData->country_id);
                            $courier->senderAddressData->zip_code = $item->senderAddressData->zip_code;
                            $courier->senderAddressData->city = $item->senderAddressData->city;
                            $courier->senderAddressData->address_line_1 = $item->senderAddressData->address_line_1;
                            $courier->senderAddressData->address_line_2 = $item->senderAddressData->address_line_2;
                            $courier->senderAddressData->tel = $item->senderAddressData->tel;

                            $courier->receiverAddressData->name = $item->receiverAddressData->name;
                            $courier->receiverAddressData->company = $item->receiverAddressData->company;
                            $courier->receiverAddressData->country_id = CountryList::findIdByText($item->receiverAddressData->country_id);
                            $courier->receiverAddressData->zip_code = $item->receiverAddressData->zip_code;
                            $courier->receiverAddressData->city = $item->receiverAddressData->city;
                            $courier->receiverAddressData->address_line_1 = $item->receiverAddressData->address_line_1;
                            $courier->receiverAddressData->address_line_2 = $item->receiverAddressData->address_line_2;
                            $courier->receiverAddressData->tel = $item->receiverAddressData->tel;
                        }

                        array_push($models, $courier);
                    }


                try
                {


                    if($mode == Courier::MODE_JUST_VALIDATE)
                    {

                        $result = Courier::validateExternalTypeGroup($models, $mode);
                    } else {
                        $result = Courier::saveExternalTypeGroup($models, $mode);
                    }

                }



                catch (Exception $ex)
                {


                    $session = Yii::app()->session['lffs'];
                    $remaining = $session['parts'] - $session['alreadySaved'];

                    $html = $this->renderPartial('createExternalFromFile/createExternalFromFile', array(
                        'models' => $models,
                        'remaining' => $remaining,
                        'total' => $session['parts'],
                        'saved' => $session['alreadySaved'],
                        'split' => $session['split'],
                        'errors' => true,
                    ), true, false);

                    echo CJSON::encode(array('html' => $html));
                    return;
                }

                if(is_array($result) OR $mode == Courier::MODE_JUST_VALIDATE)
                {

                    if($mode == Courier::MODE_JUST_VALIDATE)
                    {

                        $session = Yii::app()->session['lffs'];
                        $remaining = $session['parts'] - $session['alreadySaved'];

                        $html = $this->renderPartial('createExternalFromFile/createExternalFromFile', array(
                            'models' => $models,
                            'remaining' => $remaining,
                            'total' => $session['parts'],
                            'saved' => $session['alreadySaved'],
                            'split' => $session['split'],
                            'errors' => false,
                        ), true, false);

                        echo CJSON::encode(array('html' => $html));
                        return;

                    } else {

                        $session = Yii::app()->session['lffs'];
                        $remaining = $session['parts'] - $session['alreadySaved'];

                        $models = $result;

                        $html = $this->renderPartial('createExternalFromFile/createExternalFromFile', array(
                            'models' => $models,
                            'remaining' => $remaining,
                            'total' => $session['parts'],
                            'saved' => $session['alreadySaved'],
                            'split' => $session['split'],
                            'errors' => true,
                        ), true, false);

                        echo CJSON::encode(array('html' => $html));
                        return;
                    }


                } else {
                    Yii::app()->cacheUserData->set('tempModels_'.$hash.'_'.$part, true, 86400);

                    $session = Yii::app()->session['lffs'];
                    $session['alreadySaved'] += 1;
                    Yii::app()->session['lffs'] = $session;

                    $remaining = $session['parts'] - $session['alreadySaved'];

                    $number = S_Useful::sizeof($models);
                    echo CJSON::encode(array('html' =>  TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, 'Saved new packages in number:'.$number, array('style' => 'font-size: 16px; text-align: center;', 'closeText' => false)).'<br/>'.TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, 'Remaining to save: <strong>'.$remaining.'</strong>/'.$session['parts'], array('style' => 'font-size: 16px; text-align: center;', 'closeText' => false))));
                    return;
                }



            }
        }


        $this->render('createExternalFromFile/index', array(
            'fileModel' => $fileModel,
        ));
    }


}