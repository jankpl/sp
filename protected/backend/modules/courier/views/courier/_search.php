<div class="form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>



    <div class="row">
        <?php echo $form->label($model, 'local_id'); ?>
        <?php echo $form->textField($model, 'local_id', array('maxlength' => 12)); ?>
    </div>

	<div class="row">
		<?php echo $form->label($model, 'date_entered'); ?>
		<?php echo $form->textField($model, 'date_entered'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'date_updated'); ?>
		<?php echo $form->textField($model, 'date_updated'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model, '__stat'); ?>
        <?php echo $form->dropDownList($model, '__stat',CHtml::listData(CourierStat::model()->findAll(),'id','name')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'package_weight'); ?>
        <?php echo $form->textField($model, 'package_weight'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'package_size_l'); ?>
        <?php echo $form->textField($model, 'package_size_l'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'package_size_w'); ?>
        <?php echo $form->textField($model, 'package_size_w'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'package_size_d'); ?>
        <?php echo $form->textField($model, 'package_size_d'); ?>
    </div>

	<div class="row">
		<?php echo $form->label($model, '__sender_name'); ?>
		<?php echo $form->textField($model, '__sender_name', array('maxlength' => 45)); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model, '__sender_company'); ?>
        <?php echo $form->textField($model, '__sender_company', array('maxlength' => 45)); ?>
    </div>

	<div class="row">
		<?php echo $form->label($model, '__sender_country_id'); ?>
		<?php echo $form->dropDownList($model, '__sender_country_id', CHtml::listData(CountryList::model()->findAll(),'id','name'), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, '__sender_zip_code'); ?>
		<?php echo $form->textField($model, '__sender_zip_code', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, '__sender_city'); ?>
		<?php echo $form->textField($model, '__sender_city', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, '__sender_address'); ?>
		<?php echo $form->textField($model, '__sender_address', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, '__sender_tel'); ?>
		<?php echo $form->textField($model, '__sender_tel', array('maxlength' => 45)); ?>
	</div>


    <div class="row">
        <?php echo $form->label($model, '__receiver_name'); ?>
        <?php echo $form->textField($model, '__receiver_name', array('maxlength' => 45)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '__receiver_company'); ?>
        <?php echo $form->textField($model, '__receiver_company', array('maxlength' => 45)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '__receiver_country_id'); ?>
        <?php echo $form->dropDownList($model, '__receiver_country_id', CHtml::listData(CountryList::model()->findAll(),'id','name'), array('prompt' => Yii::t('app', 'All'))); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '__receiver_zip_code'); ?>
        <?php echo $form->textField($model, '__receiver_zip_code', array('maxlength' => 45)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '__receiver_city'); ?>
        <?php echo $form->textField($model, '__receiver_city', array('maxlength' => 45)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '__receiver_address'); ?>
        <?php echo $form->textField($model, '__receiver_address', array('maxlength' => 45)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '__receiver_tel'); ?>
        <?php echo $form->textField($model, '__receiver_tel', array('maxlength' => 45)); ?>
    </div>


	<div class="row">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>


	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
