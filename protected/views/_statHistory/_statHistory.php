<?php
/* @var $model HybridMailStatHistory[] */
/* @var $temp OrderStatHistory[] */

$temp = $model;
reset($temp);
$first = key($temp);
?>

<table class="list hTop" style="width: 100%;">
    <tr class="header"><td style="width: 140px;"><?php echo $temp[$first]->getAttributeLabel('date_entered');?></td><td><?php echo Yii::t('_statHistory','Status');?></td></tr>
    <?php
    $i = 0;
    foreach($model AS $itemStatHistory):
        ?>
        <tr>
            <td><?php echo $itemStatHistory->status_date;?></td>
            <td><?php echo (($itemStatHistory->hasAttribute('location') && $itemStatHistory->location != '') ? ( $itemStatHistory->currentStat ? $itemStatHistory->currentStat->getStatText(true) .' ('.$itemStatHistory->location.')' : '-') : ($itemStatHistory->currentStat ? $itemStatHistory->currentStat->getStatText(true) : '-')) ;?></td>
        </tr>
    <?php
    endforeach;
    ?>
</table>