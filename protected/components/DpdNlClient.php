<?php

class DpdNlClient
{
    const WSDL_LOGIN = GLOBAL_CONFIG::DPDNL_WSDL_LOGIN;
    const WSDL_SHIP = GLOBAL_CONFIG::DPDNL_WSDL_SHIP;
    const WSDL_LIFECYCLE = GLOBAL_CONFIG::DPDNL_WSDL_LIFECYCLE;

    const DELIS_ID = GLOBAL_CONFIG::DPDNL_DELIS_ID;
    const PASWORD = GLOBAL_CONFIG::DPDNL_PASSWORD;
    const SENDING_DEPOT = GLOBAL_CONFIG::DPDNL_SENDING_DEPOT;

    const RETURN_PREFIX = '[RETURN] ';

    public $reference1;
    public $reference2;

    public $package_value;

    public $sender_name;
    public $sender_company;
    public $sender_address;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_tel;
    public $sender_email;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $packages_number = 1;
    public $package_content;

    public $cod_value = 0;
    public $cod_currency = 0;


    public $saturdayDelivery = false;
    public $tyres = false;

    public $pickup = false;

    public $express10 = false;
    public $express12 = false;
    public $guarantee18 = false;
    public $internationalExpress = false;


    public $label_annotation_text = false;

    public $user_id;

    protected static function getAuthToken($force = false)
    {
        $CACHE_NAME = 'DPDNL_AUTH_TOKEN';

        $authToken = Yii::app()->cache->get($CACHE_NAME);
        if($force OR !$authToken)
        {
            $model = new self;

            $data = new stdClass();
            $data->delisId = self::DELIS_ID;
            $data->password = self::PASWORD;
            $data->messageLanguage = 'en_US';

            $resp = $model->_soapOperation('getAuth', self::WSDL_LOGIN, $data, false);

            if($resp->success)
            {
                $authToken = $resp->data->return->authToken;

                if($authToken)
                    Yii::app()->cache->set($CACHE_NAME, $authToken, 82800);
            }
        }

        return $authToken;
    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    protected static function _ownExtractNumber($text, $defaultValue = '0')
    {
        $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-]+)((\s)+|$))*/i';
        $building = '';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[0][0];

        $building = trim($building);

        if(mb_strlen($building) > 8)
        {
            // max length is 8
            // if it's longer, use older, more choosy rexexp
            $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';
            if(preg_match_all($REG_PATTERN, $text, $result2))
                $building = $result2[2][0];

            $building = trim($building);
        }

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }

    public function _createShipment($returnError = false, $retry = 0)
    {
        if(preg_match('/(po\-box)|(pobox)|(postbus)|(postbox)/i', ($this->receiver_address), $match)) {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'Cannot delivery to: ' . $match[0],
                )];
                return $return;
            } else {
                return false;
            }
        }


        $model = new self;
        $data = new stdClass();

        $data->printOptions = new stdClass();
        $data->printOptions->printerLanguage = 'PDF';
        $data->printOptions->paperFormat = 'A6';


        if(mb_strlen($this->sender_name) > mb_strlen($this->sender_company)) {
            $senderName1 = $this->sender_name;
            $senderName2 = $this->sender_company;
        } else {
            $senderName1 = $this->sender_company;
            $senderName2 = $this->sender_name;
        }

        if(mb_strlen($this->receiver_name) > mb_strlen($this->receiver_company)) {
            $receiverName1 = $this->receiver_name;
            $receiverName2 = $this->receiver_company;
        } else {
            $receiverName1 = $this->receiver_company;
            $receiverName2 = $this->receiver_name;
        }

        $senderStreetNo = trim(self::_ownExtractNumber($this->sender_address));
        $senderAddressName = trim(str_replace($senderStreetNo, '', $this->sender_address));

        $receiverStreetNo = trim(self::_ownExtractNumber($this->receiver_address));
        $receiverAddressName = trim(str_replace($receiverStreetNo, '', $this->receiver_address));

        $data->order = new stdClass();
        $data->order->generalShipmentData = new stdClass();
        $data->order->generalShipmentData->sendingDepot = self::SENDING_DEPOT;

        $international = false;
        if($this->sender_country != $this->receiver_country)
            $international = true;

        if($this->internationalExpress)
            $product = 'IE2';
        else if($this->express10)
            $product = 'E10';
        else if($this->express12)
            $product = 'E12';
        else if($this->guarantee18)
            $product = 'E18';
        else
            $product = 'CL';

        $data->order->generalShipmentData->product = $product;

        $data->order->generalShipmentData->sender = new stdClass();
        $data->order->generalShipmentData->sender->name1 = $senderName1;
        $data->order->generalShipmentData->sender->name2 = $senderName2;
        $data->order->generalShipmentData->sender->street = $senderAddressName;
        $data->order->generalShipmentData->sender->houseNo = $senderStreetNo;
        $data->order->generalShipmentData->sender->country = $this->sender_country;
        $data->order->generalShipmentData->sender->zipCode = $this->sender_zip_code;
        $data->order->generalShipmentData->sender->city = $this->sender_city;
        $data->order->generalShipmentData->sender->phone = $this->sender_tel;
        $data->order->generalShipmentData->sender->contact = $senderName1;

        $data->order->generalShipmentData->recipient = new stdClass();
        $data->order->generalShipmentData->recipient->name1 = $receiverName1;
        $data->order->generalShipmentData->recipient->name2 = $receiverName2;
        $data->order->generalShipmentData->recipient->street = $receiverAddressName;
        $data->order->generalShipmentData->recipient->houseNo = $receiverStreetNo;
        $data->order->generalShipmentData->recipient->country = $this->receiver_country;
        $data->order->generalShipmentData->recipient->zipCode = $this->receiver_zip_code;
        $data->order->generalShipmentData->recipient->city = $this->receiver_city;
        $data->order->generalShipmentData->recipient->phone = $this->receiver_tel;
        $data->order->generalShipmentData->recipient->contact = $receiverName1;

        $data->order->parcels = [];

        for($i = 0; $i < $this->packages_number; $i++) {
            $parcel = new stdClass();
            $parcel->customerReferenceNumber1 = $this->reference1;
            $parcel->customerReferenceNumber2 = $this->reference2;
            $parcel->customerReferenceNumber3 = $i;
            $parcel->weight = $this->package_weight * 100;

            if($this->cod_value && !$i)
            {
                $parcel->cod = new stdClass();
                $parcel->cod->amount = $this->cod_value * 100;
                $parcel->cod->currency = $this->cod_currency;
                $parcel->cod->inkasso = 0;
            }

            if($international)
            {
                $parcel->international = new stdClass();
                $parcel->international->parcelType = true;
                $parcel->international->customsAmount = $this->package_value;
                $parcel->international->customsCurrency = 'EUR';
                $parcel->international->customsTerms = '06';
                $parcel->international->customsContent = $this->package_content;
                $parcel->international->commercialInvoiceConsignee = new stdClass();

                $parcel->international->commercialInvoiceConsignee->name1 = $senderName1;
                $parcel->international->commercialInvoiceConsignee->name2 = $senderName2;
                $parcel->international->commercialInvoiceConsignee->street = $senderAddressName;
                $parcel->international->commercialInvoiceConsignee->houseNo = $senderStreetNo;
                $parcel->international->commercialInvoiceConsignee->country = $this->sender_country;
                $parcel->international->commercialInvoiceConsignee->zipCode = $this->sender_zip_code;
                $parcel->international->commercialInvoiceConsignee->city = $this->sender_city;
                $parcel->international->commercialInvoiceConsignee->phone = $this->sender_tel;
                $parcel->international->commercialInvoiceConsignee->contact = $senderName1;

            }

//
//            if($this->pickup && !$i)
//            {
//                $parcel->pickup = new stdClass();
//                $parcel->pickup->tour = '000';
//                $parcel->pickup->quantity = $this->packages_number;
//                $parcel->pickup->date = '20171101';
//                $parcel->pickup->day = 3;
//                $parcel->pickup->fromTime1 = '0700';
//                $parcel->pickup->toTime1 = '1700';
//                $parcel->pickup->fromTime2 = '0700';
//                $parcel->pickup->toTime2 = '1700';
//            }

            $data->order->parcels[] = $parcel;
        }

        $data->order->productAndServiceData = new stdClass();
        $data->order->productAndServiceData->orderType = 'consignment';

        if($this->saturdayDelivery)
            $data->order->productAndServiceData->saturdayDelivery = true;

        if($this->tyres)
            $data->order->productAndServiceData->tyres = true;

        $rand = rand(0,40);
        usleep($rand * 25000); // resovling DPD NL's duplication tracking number bug

        $resp = $model->_soapOperation('storeOrders', self::WSDL_SHIP, $data);

        if ($resp->success == true) {
            $return = [];

            $basePath = Yii::app()->basePath . '/../';
            $dir = 'uplabels/';
            $temp = $dir . 'temp/';

            $fileName = $basePath . $temp . $resp->data->orderResult->shipmentResponses->mpsId . '.pdf';

            $labels = [];
            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            @file_put_contents($fileName, $resp->data->orderResult->parcellabelsPDF);
            for ($i = 0; $i < $this->packages_number; $i++) {
                $im->readImage($fileName.'[' . $i . ']');
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }
//                $im->trimImage(0);
//                $im->stripImage();

                if($this->label_annotation_text)
                    LabelAnnotation::createAnnotation($this->label_annotation_text, $im, 58);

                $im->trimImage(0);
                $im->stripImage();

                $labels[$i] = $im->getImageBlob();
            }
            @unlink($fileName);

            $i = 0;
            if(is_array($resp->data->orderResult->shipmentResponses->parcelInformation))
                foreach($resp->data->orderResult->shipmentResponses->parcelInformation AS $item)
                {
                    $return[] = [
                        'status' => true,
                        'label' => $labels[$i],
                        'track_id' => $item->parcelLabelNumber,
                    ];

                    // PREVENT USING DUPLICATED DPD NL NO - DPD is buggy
                    $ttCheck = self::track($item->parcelLabelNumber);
                    if(is_array($ttCheck) && S_Useful::sizeof($ttCheck) > 1 && $retry < 5)
                    {
                        MyDump::dump('DPD_NL_RETRY.txt', $this->reference2 .' : '. $retry.' : '.$item->parcelLabelNumber);
                        $retry++;
                        return $this->_createShipment($returnError, $retry);
                    }
                    //

                    $i++;
                }
            else
            {
                $return[] = [
                    'status' => true,
                    'label' => $labels[$i],
                    'track_id' => $resp->data->orderResult->shipmentResponses->parcelInformation->parcelLabelNumber,
                ];

                // PREVENT USING DUPLICATED DPD NL NO - DPD is buggy
                $ttCheck = self::track($resp->data->orderResult->shipmentResponses->parcelInformation->parcelLabelNumber);
                if(is_array($ttCheck) && S_Useful::sizeof($ttCheck) > 1 && $retry < 5)
                {
                    MyDump::dump('DPD_NL_RETRY.txt', $this->reference2 .' : '.$retry.' : '.$resp->data->orderResult->shipmentResponses->parcelInformation->parcelLabelNumber);
                    $retry++;
                    return $this->_createShipment($returnError, $retry);
                }
                //

            }


            return $return;
        }
        else {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $resp->errorDesc,
                )];
                return $return;
            } else {
                return false;
            }
        }

    }

    protected function _soapOperation($method, $wsdl, $data, $withAuthenticationHeader = true)
    {
        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
        );

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        try {
            $client = new SoapClient($wsdl, $mode);

            if($withAuthenticationHeader) {
                $soapHeaderBody = [
                    'delisId' => self::DELIS_ID,
                    'authToken' => self::getAuthToken(),
                    'messageLanguage' => 'en_US',
                ];


                $header = new SOAPHeader('http://dpd.com/common/service/types/Authentication/2.0', 'authentication', $soapHeaderBody, false);

                $client->__setSoapHeaders($header);
            }

            $resp = $client->__call($method, [$method => $data]);


        } catch (SoapFault $E) {
            MyDump::dump('dpdnl.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('dpdnl.txt', print_r($client->__getLastResponse(),1));


            $code = $E->detail->faultCodeType->faultCodeField ? $E->detail->faultCodeType->faultCodeField : $E->faultcode;
            $desc = $E->detail->faultCodeType->messageField ? $E->detail->faultCodeType->messageField : $E->faultstring;

            $return->error = $code . '(' . $desc . ')';
            $return->errorCode = $code;
            $return->errorDesc = $desc;
            return $return;

        } catch (Exception $E) {
            MyDump::dump('dpdnl.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('dpdnl.txt', print_r($client->__getLastResponse(),1));

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;
            return $return;
        }

        if($method != 'getTrackingData') {
            MyDump::dump('dpdnl.txt', print_r($client->__getLastRequest(), 1));
            MyDump::dump('dpdnl.txt', print_r($client->__getLastResponse(), 1));
        }

        $return->success = true;
        $return->data = $resp;

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $operator_uniq_id, $returnError = false)
    {

        $model = new self;
        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($operator_uniq_id);

        $model->user_id = $courierInternal->courier->user_id;


//        if($operator_uniq_id == CourierOperator::UNIQID_DPDNL_INT)
//            $model->international = true;


        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDNL_TYRES))
            $model->tyres = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDNL_SATURDAY))
            $model->saturdayDelivery = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDNL_E10))
            $model->express10 = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDNL_E12))
            $model->express10 = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDNL_G18))
            $model->guarantee18 = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDNL_INTERNATIONAL_EXPRESS))
            $model->internationalExpress = true;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->sender_tel = $from->tel;
        $model->sender_country = strtoupper($from->country0->code);
//        $model->sender_email = $from->fetchEmailAddress(true);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
//        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierInternal->courier->getWeight(true);;
        $model->packages_number = $courierInternal->courier->packages_number;

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_value = $courierInternal->courier->getPackageValueConverted('EUR');
        $model->package_content = mb_substr($courierInternal->courier->package_content,0,35);
        $model->reference1 = mb_substr($courierInternal->courier->package_content,0,35);
        $model->reference2 = $courierInternal->courier->local_id;

        $model->cod_value = 0;

        if($courierInternal->courier->cod && $courierInternal->courier->cod_value > 0)
        {
            $model->cod_value = $courierInternal->courier->cod_value;
            $model->cod_currency = $courierInternal->courier->cod_currency;
        }

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierU, $returnError = false)
    {

        $model = new self;
        $model->label_annotation_text = $courierU->courierUOperator->label_symbol;

        $model->user_id = $courierU->courier->user_id;

//        if($courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DPDNL_INT)
        $model->international = true;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDNL_TYRES))
            $model->tyres = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDNL_SATURDAY))
            $model->saturdayDelivery = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDNL_E10))
            $model->express10 = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDNL_E12))
            $model->express10 = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDNL_G18))
            $model->guarantee18 = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDNL_INTERNATIONAL_EXPRESS))
            $model->internationalExpress = true;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->sender_tel = $from->tel;
        $model->sender_country = strtoupper($from->country0->code);
//        $model->sender_email = $from->fetchEmailAddress(true);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
//        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierU->courier->getWeight(true);
        $model->packages_number = $courierU->courier->packages_number;

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_value = $courierU->courier->getPackageValueConverted('EUR');
        $model->package_content = mb_substr($courierU->courier->package_content,0,35);
        $model->reference1 = mb_substr($courierU->courier->package_content,0,35);
        $model->reference2 = $courierU->courier->local_id;

        $model->cod_value = 0;

        if($courierU->courier->cod && $courierU->courier->cod_value > 0)
        {
            $model->cod_value = $courierU->courier->cod_value;
            $model->cod_currency = $courierU->courier->cod_currency;
        }

        return $model->_createShipment($returnError);
    }

    public static function track($number)
    {

        $data = new stdClass();
        $data->parcelLabelNumber = $number;

        $model = new self;
        $resp = $model->_soapOperation('getTrackingData', self::WSDL_LIFECYCLE, $data);

        $statuses = [];
        if($resp)
        {
            $data = $resp->data->TrackingResult->statusInfo;

            $returnPrefix = '';
            if(is_array($resp->data->TrackingResult->shipmentInfo->errorItems))
            {
                foreach($resp->data->TrackingResult->shipmentInfo->errorItems AS $temp)
                    if($temp->content->content == 'Return')
                    {
                        $returnPrefix = self::RETURN_PREFIX;
                        break;
                    }
            } else if($resp->data->TrackingResult->shipmentInfo->errorItems->content->content == 'Return')
                $returnPrefix = self::RETURN_PREFIX;

            if($data)
                foreach($data AS $node) {

                    if(!isset($node->date))
                        continue;

                    $status = (string) $node->description->content->content;
                    $date = (string) $node->date->content;
                    $location = (string) $node->location->content;

                    $date = trim(str_replace(',', '', $date));
                    $date = explode(' ', $date);
                    $temp = $date[0];
                    $temp = explode('/', $temp);

                    $date = $temp[2].'-'.$temp[1].'-'.$temp[0].' '.$date[1];

                    if($status == '')
                        continue;

                    $status = $returnPrefix.$status;

                    if
                    ($status == self::RETURN_PREFIX.'DPD has received your parcel.' OR
                        $status == self::RETURN_PREFIX.'The parcel is at the parcel dispatch centre.')
                        continue;

                    array_push($statuses, [
                        'name' => $status ,
                        'date' => $date,
                        'location' => $location,
                    ]);
                }

            return $statuses;

        }
        return false;
    }

}