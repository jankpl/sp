<h1>API</h1>

<div class="alert alert-info text-center">
    <?= Yii::t('site', 'Aktualna wersja API: ');?> <strong>1.12</strong>
    <br/>
    <br/>
    (06.06.2018)
    <br/><br/>
    <a href="/uploads/apiDoc/api.pdf" target="_blank" class="btn btn-lg btn-primary"><?= Yii::t('site', 'Pobierz dokumentację');?></a>
    <br/>
    <br/>
</div>