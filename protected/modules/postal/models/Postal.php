<?php

Yii::import('application.modules.postal.models._base.BasePostal');

class Postal extends BasePostal
{
    protected $_afterfind_ref;
    protected $_afterfind_external_id;

    const NONREG_DEFAULT_OPERATOR_NAME = 'SwiatPrzesylek';

    const SOURCE_EBAY = 10;
    const SOURCE_IAI = 15;
    const SOURCE_FILE_IMPORT = 20;
    const SOURCE_API = 30;
    const SOURCE_CLONE = 40;
    const SOURCE_LINKER = 60;
    const SOURCE_RELABELING = 80;

    const WEIGHT_CLASS_1 = 1;
    const WEIGHT_CLASS_2 = 2;
    const WEIGHT_CLASS_3 = 3;
    const WEIGHT_CLASS_4 = 4;
    const WEIGHT_CLASS_5 = 5;
    const WEIGHT_CLASS_6 = 6;
    const WEIGHT_CLASS_7 = 7;
    const WEIGHT_CLASS_8 = 8;
    const WEIGHT_CLASS_9 = 9;
    const WEIGHT_CLASS_10 = 10;
    const WEIGHT_CLASS_11 = 11;
    const WEIGHT_CLASS_12 = 12;
    const WEIGHT_CLASS_13 = 13;

    const SIZE_CLASS_1 = 1;
    const SIZE_CLASS_2 = 2;
    const SIZE_CLASS_3 = 3;

    // @05.12.2017
    // dom and int size class are merged
    const SIZE_CLASS_DOM_1 = 1;
    const SIZE_CLASS_DOM_2 = 2;
    const SIZE_CLASS_DOM_3 = 3;
    const SIZE_CLASS_INT_1 = 100;
    const SIZE_CLASS_INT_2 = 101;
    const SIZE_CLASS_INT_3 = 102;

    const POSTAL_TYPE_STD = 1;
    const POSTAL_TYPE_PRIO = 2;
    const POSTAL_TYPE_REG = 3;

    const SCENARIO_REG = 1;
    const SCENARIO_NOT_REG = 2;
    const SCENARIO_BULK = 10;

    const BULK_MAX_NUMBER = 1000000;
    const BULK_ITEMS_ON_FORM_NUMBER = 15;

    const PDF_FORMAT_65x82 = 1;
    const PDF_FORMAT_90x45 = 2;

    // when packages labels are no longer available
    const ARCHIVED_LABEL_EXPIRED = 5;
    const ARCHIVED_HISTORY = 7;


    const USER_CANCEL_REQUEST = 1;
    const USER_CANCEL_CONFIRMED = 5;
    const USER_CANCEL_DISMISSED = 20;
    const USER_CANCEL_WARNED = 9;

    const PACKAGE_CANCEL_DAYS = 4;

    // NEW FLAGS:
    const USER_CANCELL_REQUESTED = 50;
    const USER_CANCELL_REQUESTED_FOUND_TT = 60;
    const USER_CANCELL_COMPLAINED = 70;


    const POSTAL_BROKER_OOE = 2;
    const POSTAL_BROKER_POCZTA_POLSKA = 3;
    const POSTAL_BROKER_DHLDE = 4;
    const POSTAL_BROKER_HUNGARY_POST = 5;
    const POSTAL_BROKER_POST11 = 6;
    const POSTAL_BROKER_SWEDEN_POST = 7;
    const POSTAL_BROKER_ROYAL_MAIL = 8;


    const TIMER_IGNORE_TT_AFTER = 31; // days // afterwards stop running TT cron and prevent updates by TT

    public $regulations;
    public $regulations_rodo;
    protected $_routing = NULL;


    public $_import_raw_data_key = false;

    public $_first_status_date;


    // used only for import:
    public $_price_total;
    public $_price_currency;
    //

    public $_cancel_package_on_fail;


    public $_ebay_order_id;

    public $_ebay_export_status;

    public $_pp_forceTTNumber; // used for forcing TT number in PocztaPolska requests


    // not savable into DB. Just temporary.
    public $__temp_ebay_update_stat;

    public function clearRouting()
    {
        $this->_routing = NULL;
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return CMap::mergeArray(parent::attributeLabels(), RodoRegulations::attributeLabels());
    }

    public function init()
    {
        $this->target_sp_point = SpPoints::CENTRAL_SP_POINT_ID;

        if(Yii::app()->params['frontend']) {
            if (Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS)
                $this->target_sp_point = SpPoints::QURIERS_SP_POINT_ID;
            else if (Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                $this->target_sp_point = SpPoints::CQL_SP_POINT_ID;

            if (!Yii::app()->user->isGuest) {
                $data = User::getDefaultPostalDimensionsForUser(Yii::app()->user->id);

                $this->postal_type = $data->postal_type;
                $this->size_class = $data->postal_size;
                $this->weight = $data->weight;
                $this->value = $data->value;
                $this->value_currency = $data->value_currency;
                $this->content = $data->content;
            }
        }
        parent::init();

    }

    public static function getStatUpdateIgnoredStats()
    {
        $_ingoredStats = [
            PostalStat::CANCELLED,
            PostalStat::CANCELLED_PROBLEM,
            PostalStat::CANCELLED_USER,
            PostalStat::NEW_ORDER,
            PostalStat::PACKAGE_PROCESSING
        ];

        $data = array_merge($_ingoredStats, PostalStat::getKnownDeliveredStatuses());
        return $data;
    }


    public function isStatChangeByTtAllowed($ignoreTime = true)
    {
        if(in_array($this->postal_stat_id, self::getStatUpdateIgnoredStats()))
            return false;

        if($this->stat_map_id == StatMap::MAP_DELIVERED)
            return false;

        if($ignoreTime)
            return true;


            if ($this->date_entered < date('Y-m-d', strtotime('-' . self::TIMER_IGNORE_TT_AFTER . ' days')) && ($this->date_updated != '' && $this->date_updated < date('Y-m-d', strtotime('-' . self::TIMER_IGNORE_TT_AFTER . ' days'))))
                return false;


        return true;
    }

    public static function cloneItem(self $source, $forceOperatorId = false)
    {
        $new = new self;
        $new->attributes = $source->attributes;
        $new->senderAddressData = new Postal_AddressData();
        $new->senderAddressData->attributes = $source->senderAddressData->attributes;
        $new->receiverAddressData = new Postal_AddressData();
        $new->receiverAddressData->attributes = $source->receiverAddressData->attributes;

        $new->isNewRecord = true;
        $new->senderAddressData->isNewRecord = true;
        $new->receiverAddressData->isNewRecord = true;

        $new->source = self::SOURCE_CLONE;

        if($forceOperatorId)
            $new->postal_operator_id = $forceOperatorId;

        try {
            $transaction = Yii::app()->db->beginTransaction();
            $return = $new->saveNewPostal(false, $forceOperatorId);

            if ($return instanceof Postal)
                $return = $return->createAndFinalizeOrder();

        } catch (Exception $ex) {
            $transaction->rollback();
            return false;
        }

        if ($new->postalLabel !== NULL)
        {
            $sourceTrackId = $source->postalLabel->track_id;
            if($sourceTrackId) {
                PostalAdditionalExtId::add($new, $sourceTrackId);
                $source->postalLabel->track_id = 'x' . $sourceTrackId;
                $source->postalLabel->update(['track_id']);
            }
        }

        $transaction->commit();
        if ($return) {
            if ($new->postalLabel !== NULL)
                PostalLabel::asyncCallForId($new->postalLabel->id);
        }

        $new->refresh();
        $source->addToLog('Item CLONED with moving TT number. New item no: '.$new->local_id);
        $new->addToLog('Item CLONED with moving TT number. Source item no: '.$source->local_id);

        $stat_id = PostalStat::getOrCreateAndGetId('Added new external ID: '.$sourceTrackId, 0, false);
        $new->changeStat($stat_id, 0, NULL, false, NULL, false, false, true);

        return $new;
    }

    public static function getCurrencyList()
    {
        return User::getValueCurrencyList();
    }

    public function getSourceName()
    {
        $list = self::getSourceList();

        return isset($list[$this->source]) ? $list[$this->source] : '-';
    }

    public static function getSourceList()
    {
        return [
            self::SOURCE_FILE_IMPORT => 'File import',
            self::SOURCE_API => 'API',
            self::SOURCE_CLONE => 'Clone',
            self::SOURCE_EBAY => 'eBay',
            self::SOURCE_IAI => 'IAI',
            self::SOURCE_LINKER => 'Linker',
            self::SOURCE_RELABELING => 'Relabeling',
        ];
    }

    public static function isAvailableForUser()
    {
        return Yii::app()->user->model->payment_on_invoice && Yii::app()->user->model->getPostalAvailable();
    }

    public static function getBrokers($onlyAvailableToAdd = false)
    {
        $data = array(
            self::POSTAL_BROKER_OOE => 'OOE',
        );

        if(!$onlyAvailableToAdd) {
            $data[self::POSTAL_BROKER_POCZTA_POLSKA] = 'PocztaPolska';
            $data[self::POSTAL_BROKER_DHLDE] = 'DHL_DE';
            $data[self::POSTAL_BROKER_HUNGARY_POST] = 'Hungary Post';
            $data[self::POSTAL_BROKER_POST11] = 'Post11';
            $data[self::POSTAL_BROKER_SWEDEN_POST] = 'Sweden Post';
            $data[self::POSTAL_BROKER_ROYAL_MAIL] = 'Royal Mail';
        }

        $data = $data + GLOBAL_BROKERS::getBrokersListPostal();

        asort($data);

        return $data;
    }

    public static function getUserCancelList()
    {
        return [
            0 => '-',
            self::USER_CANCELL_REQUESTED => '['.self::getUserCancelListShort()[self::USER_CANCELL_REQUESTED].'] User cancelled',
            self::USER_CANCELL_REQUESTED_FOUND_TT => '['.self::getUserCancelListShort()[self::USER_CANCELL_REQUESTED_FOUND_TT].'] Warning - T&T found new status',
            self::USER_CANCELL_COMPLAINED => '['.self::getUserCancelListShort()[self::USER_CANCELL_COMPLAINED].'] User complained',
        ];
    }

    public static function getUserCancelListShort()
    {
        return [
            0 => '-',
            self::USER_CANCELL_REQUESTED => 'C',
            self::USER_CANCELL_REQUESTED_FOUND_TT => 'W',
            self::USER_CANCELL_COMPLAINED => 'R',
        ];
    }


    /**
     * Method returns new model with Bulk attributes set
     * @return Postal
     */
    public static function newBulk()
    {
        $model = new self;
        $model->bulk = 1;
        $model->scenario = self::SCENARIO_BULK;
        return $model;
    }

    public static function getWeightUnit()
    {
        return 'g';
    }

    public static function getDimensionUnit()
    {
        return 'cm';
    }

    public static function postalTypeIdToShortName($postal_type_id)
    {
        switch($postal_type_id)
        {
            case self::POSTAL_TYPE_STD:
                return 'STD';
                break;
            case self::POSTAL_TYPE_PRIO:
                return 'PRIO';
                break;
            case self::POSTAL_TYPE_REG:
                return 'REG';
                break;
        }
    }

    public static function getPostalTypeList($justForBulk = false)
    {
        $data = [
            self::POSTAL_TYPE_STD => Yii::t('postal', 'STD'),
            self::POSTAL_TYPE_PRIO => Yii::t('postal', 'PRIO'),
            self::POSTAL_TYPE_REG => Yii::t('postal', 'REG'),
        ];

        if($justForBulk)
            unset($data[self::POSTAL_TYPE_REG]);

        return $data;
    }

    public static function getPostalTypeListDesc()
    {
        $data = [
            self::POSTAL_TYPE_STD => Yii::t('postal', 'Listy standardowe - opis'),
            self::POSTAL_TYPE_PRIO => Yii::t('postal', 'Listy priorytetowe - opis'),
            self::POSTAL_TYPE_REG => Yii::t('postal', 'Listy rejestrowane - opis'),
        ];

        return $data;
    }


    public static function getBulkList()
    {
        $data = [
            0 => 'No',
            1 => 'Yes',
        ];

        return $data;
    }

    /**
     * Returns labels status - for not-regustered postals status is always success because labels are generated locally
     *
     * @return int
     */
    public function getUsefulLabelStat()
    {
        if($this->postal_type == self::POSTAL_TYPE_REG)
            if($this->postalLabel !== NULL)
                return $this->postalLabel->stat;
            else
                return PostalLabel::STAT_FAILED;
        else
            return PostalLabel::STAT_SUCCESS;
    }

    public function getBulkName()
    {
        return self::getBulkList()[$this->bulk];
    }


    public static function getPostalTypeBulkList()
    {
        $data = [
            self::POSTAL_TYPE_STD => self::getPostalTypeList()[self::POSTAL_TYPE_STD],
            self::POSTAL_TYPE_PRIO=> self::getPostalTypeList()[self::POSTAL_TYPE_PRIO],
        ];

        return $data;
    }


    protected static $_countryListModel = [];
    public function getReceiverCountryName()
    {

        if(!isset(self::$_countryListModel[$this->getReceiverCountryId()]))
            self::$_countryListModel[$this->getReceiverCountryId()] = CountryList::model()->with('countryListTr')->findByPk($this->getReceiverCountryId());
        $model = self::$_countryListModel[$this->getReceiverCountryId()];

        if($model !== NULL)
            return $model->trName;
        else
            return '';
    }

    public function getReceiverCountryCode()
    {
        if(!isset(self::$_countryListModel[$this->getReceiverCountryId()]))
            self::$_countryListModel[$this->getReceiverCountryId()] = CountryList::model()->with('countryListTr')->findByPk($this->getReceiverCountryId());

        $model = self::$_countryListModel[$this->getReceiverCountryId()];

        if($model !== NULL)
            return $model->code;
        else
            return '';
    }

    public function getPostalTypeName()
    {
        return self::getPostalTypeList()[$this->postal_type];
    }


    public static function getWeightClassList()
    {

        $data = [];
        foreach(self::getWeightClassValueList() AS $key => $item)
            $data[$key] = Yii::t('postal', 'do {n}g', ['{n}' => $item]);

        return $data;
    }

    public function getWeightClassName()
    {
        return self::getWeightClassList()[$this->weight_class];
    }

    public static function countWeightClassesForSizeClass($size_class)
    {
        $max = self::getMaxWeightForSizeClass($size_class);
        $i = 0;
        foreach(self::getWeightClassValueList() AS $value)
        {
            if($value > $max)
                break;

            $i++;
        }

        return $i;
    }

    public static function getMaxWeightValue()
    {
        $data = self::getWeightClassValueList();
        return array_pop($data);
    }

    /**
     * Returns weight class weight value in grams
     *
     * @return array
     */
    public static function getWeightClassValueList()
    {

        $data = [
            self::WEIGHT_CLASS_1 => 20,
            self::WEIGHT_CLASS_2 => 50,
            self::WEIGHT_CLASS_3 => 100,
            self::WEIGHT_CLASS_4 => 200,
            self::WEIGHT_CLASS_5 => 300,
            self::WEIGHT_CLASS_6 => 400,
            self::WEIGHT_CLASS_7 => 500,
            self::WEIGHT_CLASS_8 => 750,
            self::WEIGHT_CLASS_9 => 1000,
            self::WEIGHT_CLASS_10 => 1250,
            self::WEIGHT_CLASS_11 => 1500,
            self::WEIGHT_CLASS_12 => 1750,
            self::WEIGHT_CLASS_13 => 2000,
        ];

        return $data;
    }

    public static function getWeightClassValueByClass($weight_class)
    {
        return self::getWeightClassValueList()[$weight_class];
    }

    public function getWeightClassValue()
    {
        return self::getWeightClassValueList()[$this->weight_class];
    }

    public static function getSizeClassList()
    {
        $data = [
            self::SIZE_CLASS_1 => Yii::t('postal', 'Gab. A'),
            self::SIZE_CLASS_2 => Yii::t('postal', 'Gab. B'),
            self::SIZE_CLASS_3 => Yii::t('postal', 'Gab. C'),
        ];

        return $data;
    }

    public static function getSizeClassListDesc()
    {
        $data = [
            self::SIZE_CLASS_1 => Yii::t('postal', 'Żaden z wymiarów nie może przekroczyć wysokości 5 mm, długości 245 mm i szerokości 165 mm  / waga maksymalna: 100 gram'),
            self::SIZE_CLASS_2 => Yii::t('postal', 'Żaden z wymiarów nie może przekroczyć wysokości 20 mm, długości 305 mm i szerokości 381 mm  /  waga maksymalna: 500 gram'),
            self::SIZE_CLASS_3 => Yii::t('postal', 'Najdłuższy bok nie może przekroczyć 600 mm, suma długości nie może przekroczyć 900 mm  /  waga maksymalna: 2000 gram'),
        ];

        return $data;
    }

    /**
     * Method returns weight classes available for selected size classes
     *
     * @param $sizeClass int Id of selected size class
     * @return array Array of available weight classes
     */
    public function getAvailableWeightClassForSizeClass($sizeClass)
    {
        if($sizeClass == self::SIZE_CLASS_1)
        {
            return [
                self::WEIGHT_CLASS_1,
                self::WEIGHT_CLASS_2,
                self::WEIGHT_CLASS_3,
            ];
        }
        else if($sizeClass == self::SIZE_CLASS_2)
        {
            return [
                self::WEIGHT_CLASS_1,
                self::WEIGHT_CLASS_2,
                self::WEIGHT_CLASS_3,
                self::WEIGHT_CLASS_4,
                self::WEIGHT_CLASS_5,
                self::WEIGHT_CLASS_6,
                self::WEIGHT_CLASS_7,
            ];
        } else
            return array_keys(self::getWeightClassList());
    }

    public static function getMaxWeightForSizeClassList()
    {
        $data = [
            self::SIZE_CLASS_1 => 100,
            self::SIZE_CLASS_2 => 500,
            self::SIZE_CLASS_3 => 2000,
        ];

        return $data;
    }

    public static function getMaxWeightForSizeClass($sizeClass)
    {
        return isset(self::getMaxWeightForSizeClassList()[$sizeClass]) ? self::getMaxWeightForSizeClassList()[$sizeClass] : NULL;
    }

    public function getSizeClassName()
    {
        return self::getSizeClassList()[$this->size_class];
    }

    public static function getSizeClassListDom()
    {
        return self::getSizeClassListInt();
    }

    public static function getSizeClassListInt()
    {
        $data = [
            self::SIZE_CLASS_INT_1 => self::getSizeClassList()[self::SIZE_CLASS_INT_1],
            self::SIZE_CLASS_INT_2 => self::getSizeClassList()[self::SIZE_CLASS_INT_2],
            self::SIZE_CLASS_INT_3 => self::getSizeClassList()[self::SIZE_CLASS_INT_3],
        ];

        return $data;
    }


    public function afterValidate()
    {
        if($this->weight)
            $this->weight_class = self::getWeightClassForWeight($this->weight);

        if(!$this->hasErrors()) {

            if ($this->size_class) {
                if ($this->weight && $this->weight > self::getMaxWeightForSizeClass($this->size_class))
                    $this->addError('size_class', Yii::t('postal', 'Aby wybrać tę klasę wagową musisz wybrać wyższą klasę gabarytową!'));
            }

            if($this->value && $this->value_currency == '')
                $this->addError('value_currency', Yii::t('postal', 'Podaj walutę dla wartości!'));

            if ($this->bulk) {
                if ($this->getPrice() === NULL)
                    $this->addError('receiver_country_id', Yii::t('postal', 'Ten kierunek nie jest przez nas obsługiwany.'));
            }

            // size class
//            if ($this->senderAddressData !== NULL && $this->getReceiverCountryId() == CountryList::COUNTRY_PL) {
//                if (!in_array($this->size_class, array_keys(self::getSizeClassListDom())))
//                    $this->addError('size_class', Yii::t('postal', 'Proszę wybrać właściwą kategorię gabarytową!'));
//            } else
//            if ($this->senderAddressData !== NULL) {
//                if (!in_array($this->size_class, array_keys(self::getSizeClassListInt())))
//                    $this->addError('size_class', Yii::t('postal', 'Proszę wybrać właściwą kategorię gabarytową!'));
//            }
        }

        return parent::afterValidate();
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
            'bLocalIdGenerator' =>
                array('class'=>'application.models.bLocalIdGenerator'
                ),
            'bAcceptRegulationsByDefault' =>
                [
                    'class'=>'application.models.bAcceptRegulationsByDefault'
                ],
            'bFilterString' =>
                [
                    'class'=>'application.models.bFilterString'
                ],
        );
    }

    public function getReceiverCountryId()
    {
        if($this->receiverAddressData !== NULL)
            return $this->receiverAddressData->country_id;
        else
            return $this->receiver_country_id;
    }

    public function beforeSave()
    {
        $this->params = array(
            '_ebay_order_id' => $this->_ebay_order_id,
            '_cancel_package_on_fail' => $this->_cancel_package_on_fail,
            '_ebay_export_status' => $this->_ebay_export_status,
            '_pp_forceTTNumber' => $this->_pp_forceTTNumber,
        );

        if($this->params !== NULL)
            $this->params = self::serialize($this->params);

        if($this->isNewRecord)
        {
            if($this->user && $this->user->getDisableOrderingNewItems())
                throw new AccountLockedException('Locked!');

            if($this->local_id == '')
                $this->local_id = $this->generateLocalId();

            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => $this->user_id.$this->local_id.$this->date_entered.microtime()));


            if(!$this->bulk && $this->receiverAddressData !== NULL)
                $this->receiver_country_id = $this->receiverAddressData->country_id;

            if($this->postal_operator_id == '')
                $this->postal_operator_id = $this->getRouting()->postal_operator_id;


            $this->user_id = Yii::app()->user->id;

            $this->ref = strtoupper($this->ref);

            if($this->ref == '')
                $this->ref = NULL;

            $this->weight_class = self::getWeightClassForWeight($this->weight);

            if(!$this->bulk && $this->postal_type != Postal::POSTAL_TYPE_REG)
            {
                if(in_array($this->postalOperator->uniq_id, [1,2]) && $this->_pp_forceTTNumber)
                    $this->external_id = $this->_pp_forceTTNumber;
                // for packages crossing UE border:
                else if(CountryList::isUe($this->senderAddressData->country_id) != CountryList::isUe($this->receiverAddressData->country_id))
                    $this->external_id = PostalExternalIdStorehouse::operatorToExternalId($this->postalOperator->uniq_id);
            }

            if($this->postalOperator)
                $this->operator_partner_id = $this->postalOperator->partner_id;

        }

        // save real size

        return parent::beforeSave();
    }

    public static function getWeightClassForWeight($weight)
    {

        $found = false;
        foreach(self::getWeightClassValueList() AS $weightClass => $value) {
            $found = $weightClass;

            if ((double) $value >= (double) $weight)
                break;
        }
        return $found;
    }

    protected function _random_key($length,$base) {
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $key = '';
        for ( $i=0; $i<$length; $i++ ) {
            $key .= $chars[ (mt_rand( 0, ($base-1) ))];
        }
        return $key;
    }

    protected static function weightClassToLetter($weight_class)
    {

        $ascii_start = 65;
        $letter = chr($weight_class + $ascii_start - 1);
        return $letter;
    }

    protected function generateLocalId()
    {
        $is_it_unique = false;

        $today = date('ymd');
        $local_id = null;
        while(!$is_it_unique)
        {
            $local_id = '4'.$today.$this->_random_key(5,10);

            switch($this->postal_type)
            {
                case self::POSTAL_TYPE_STD:
                    $local_id .= 'S';
                    break;
                case self::POSTAL_TYPE_PRIO:
                    $local_id .= 'P';
                    break;
                case self::POSTAL_TYPE_REG:
                    $local_id .= 'R';
                    break;
            }

            if($this->weight_class === NULL)
                $this->weight_class = self::getWeightClassForWeight($this->weight);

            $local_id .= $this->size_class;
            $local_id .= self::weightClassToLetter($this->weight_class);

            $cmd = Yii::app()->db->createCommand();
            $cmd->select = 'COUNT(*)';
            $cmd->from = 'postal';
            $cmd->where = "local_id = :local_id";
            $cmd->bindParam(':local_id', $local_id);
            $num = $cmd->queryScalar();

            if($num == 0)
                $is_it_unique = true;
        }

        return $local_id;
    }

    public function afterSave()
    {
        if($this->isNewRecord)
        {
            $this->isNewRecord = false;

            /* @var $statHistory PostalStatHistory */

            $statHistory = new PostalStatHistory();
            $statHistory->postal_id = $this->id;
            $statHistory->previous_stat_id = NULL;
            $statHistory->current_stat_id = $this->postal_stat_id == ''? PostalStat::NEW_ORDER : $this->postal_stat_id;
            $statHistory->status_date = $this->_first_status_date;

            if(!$statHistory->save())
                return false;

            IdStore::createNumber(IdStore::TYPE_POSTAL, $this->id, $this->local_id);
            if($this->ref)
                IdStore::createNumber(IdStore::TYPE_POSTAL, $this->id, $this->ref);
            if($this->external_id)
                IdStore::createNumber(IdStore::TYPE_POSTAL, $this->id, $this->external_id);
        }

        if($this->ref != $this->_afterfind_ref)
        {
            if($this->ref)
                IdStore::createNumber(IdStore::TYPE_POSTAL, $this->id, $this->ref);
            else
                IdStore::removeNumber(IdStore::TYPE_POSTAL, $this->id, $this->_afterfind_ref);
        }

        if($this->external_id != $this->_afterfind_external_id)
        {
            if($this->external_id)
                IdStore::createNumber(IdStore::TYPE_POSTAL, $this->id, $this->external_id);
            else
                IdStore::removeNumber(IdStore::TYPE_POSTAL, $this->id, $this->_afterfind_external_id);
        }

        parent::afterSave();
    }

    protected static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    protected static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    /**
     * Unserialize additional params (ex. Palletway size)
     * @return bool
     */
    public function afterFind()
    {
        $this->_afterfind_ref = $this->ref;
        $this->_afterfind_external_id = $this->external_id;


        if($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if(is_array($this->params))
            foreach($this->params AS $key => $item)
            {
                try {
                    $this->$key = $item;
                }
                catch (Exception $ex)
                {}
            }

        parent::afterFind();
    }


    public static function createAndFinalizeOrderForBulkId($bulk_id)
    {
        $order = false;
        $errors = false;
        $models = Postal::model()->findAllByAttributes(['bulk_id' => $bulk_id]);

        $orderProducts = [];

        /* @var $item Postal */
        foreach ($models AS $item) {
            $product = OrderProduct::createNewProduct(
                OrderProduct::TYPE_POSTAL,
                $item->id,
                Yii::app()->PriceManager->getCurrencyCode(),
                $item->getPrice(Yii::app()->PriceManager->getCurrency()),
                Yii::app()->PriceManager->getFinalPrice($item->getPrice(Yii::app()->PriceManager->getCurrency()))
            );

            if (!$product)
                $errors = true;

            array_push($orderProducts, $product);
        }

        if (!$errors) {
            $order = Order::createOrderForProducts($orderProducts, true);
        }

        if($order instanceof Order)
        {
            if(Yii::app()->user->model->payment_on_invoice)
                if($order->fastFinalizeOrder(false))
                    return true;
            return true;
        }
        else
            return false;
    }


    public function createAndFinalizeOrder()
    {
        $order = false;
        $errors = false;
        $orderProducts = [];

        $product = OrderProduct::createNewProduct(
            OrderProduct::TYPE_POSTAL,
            $this->id,
            Yii::app()->PriceManager->getCurrencyCode(),
            $this->getPrice(Yii::app()->PriceManager->getCurrency()),
            Yii::app()->PriceManager->getFinalPrice($this->getPrice(Yii::app()->PriceManager->getCurrency()))
        );

        array_push($orderProducts, $product);

        if (!$errors) {
            $order = Order::createOrderForProducts($orderProducts, true);
        }

        if($order instanceof Order)
        {
            if(Yii::app()->user->model->payment_on_invoice)
                if($order->fastFinalizeOrder(false))
                    return true;
            return true;
        }
        else
            return false;
    }

    public function onOrderPaid()
    {
        if($this->bulk)
            $this->changeStat(PostalStat::PACKAGE_RETREIVE);
        else if($this->postalOperator->label_mode == PostalOperator::LABEL_MODE_WHOLE_EXTERNAL)
            $this->changeStat(PostalStat::PACKAGE_PROCESSING);
        else
            $this->changeStat(PostalStat::PACKAGE_RETREIVE);

        if($this->postal_type != Postal::POSTAL_TYPE_REG && $this->_ebay_order_id != '')
        {
            $this->__temp_ebay_update_stat = self::updateEbayData($this,self::NONREG_DEFAULT_OPERATOR_NAME, $this->local_id, $this->_ebay_order_id);
        }
    }


    public function rules() {
        return
            array_merge(RodoRegulations::regulationsRules(),

                array(

                    array('note1, note2', 'length', 'max'=>50),

                    array('ref', 'application.validators.postalRefValidator'),
                    array('ref', 'length', 'max'=>64),
                    array('external_id', 'length', 'max'=>32),

//            array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('postal', 'Musisz zaakceptować regulamin.'), 'on' => 'summary'),
                    array('target_sp_point, user_cancel', 'numerical', 'integerOnly' => true),
                    array('weight', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => self::getMaxWeightValue()),

                    array('value', 'numerical', 'integerOnly' => false, 'min' => 0.01, 'max' => 99999999),
//            array('value', 'match', 'pattern'=> '/^[0-9]+(\.[0-9]{1,2})?$/'),
                    array('value_currency', 'in', 'range' => self::getCurrencyList()),
                    array('value_currency', 'default', 'value' => 'PLN'),

                    array('size_class, postal_type, target_sp_point', 'required'),
                    array('weight', 'required', 'except' => 'default_package_data'),

                    array('postal_type', 'in', 'range' => [self::POSTAL_TYPE_STD, self::POSTAL_TYPE_PRIO], 'on' => self::SCENARIO_BULK),
                    array('postal_type', 'in', 'range' => [self::POSTAL_TYPE_STD, self::POSTAL_TYPE_PRIO, self::POSTAL_TYPE_REG], 'except' => self::SCENARIO_BULK),
                    array('weight_class', 'in', 'range' => array_keys(self::getWeightClassList())),
                    array('size_class', 'in', 'range' => array_keys(self::getSizeClassList())),
                    array('target_sp_point', 'in', 'range' => CHtml::listData(SpPoints::getPoints(),'id','id')),

//            array('sender_address_data_id, receiver_address_data_id', 'required', 'on' => self::SCENARIO_REG), // pointless?

                    array('bulk', 'default', 'value' => 0, 'except' => self::SCENARIO_BULK),
                    array('bulk', 'default', 'value' => 1, 'on' => self::SCENARIO_BULK),
                    array('bulk_number', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => self::BULK_MAX_NUMBER, 'on' => self::SCENARIO_BULK),
                    array('bulk_number', 'required', 'on' => self::SCENARIO_BULK),
                    array('receiver_country_id', 'required', 'on' => self::SCENARIO_BULK),

                    array('postal_stat_id', 'default', 'setOnEmpty' => true, 'value' => PostalStat::NEW_ORDER),

                    array('source', 'numerical', 'integerOnly'=>true),

                    array('user_id, size_class, weight_class, postal_type, sender_address_data_id, receiver_address_data_id, receiver_country_id, bulk, bulk_id, bulk_number, postal_stat_id, postal_operator_id', 'numerical', 'integerOnly'=>true),
                    array('size_and_weight', 'length', 'max'=>128),
                    array('content', 'length', 'max'=> 256),
                    array('sender_address_data_id, receiver_address_data_id, bulk_id, bulk_number, postal_operator_id', 'default', 'setOnEmpty' => true, 'value' => null),
                    array('id, date_entered, date_updated, user_id, size_class, weight_class, size_and_weight, postal_type, sender_address_data_id, receiver_address_data_id, receiver_country_id, bulk, bulk_id, bulk_number, content, postal_stat_id, postal_operator_id, user_cancel, stat_map_id, source, weight, external_id, note1, note2', 'safe', 'on'=>'search'),

                    array('content, note1, note2', 'filter', 'filter' => array( $this, 'filterStripTags')),
//                    array('content', 'application.validators.lettersNumbersBasicValidator'),

                ));
    }


    public static function saveNewMassPostal(array $models)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(DISTINCT(bulk_id))');
        $cmd->from('postal');
        $cmd->where('date_entered LIKE :date', [':date' => date('Y-m-d').'%']);
        $bulk_id = $cmd->queryScalar();

        $bulk_id = date('ymd').str_pad($bulk_id,6,'0',STR_PAD_LEFT);

        $errors = false;

        /* @var $model Postal */
        foreach($models AS $model)
        {
            $model->bulk_id = $bulk_id;
            if(!$model->saveNewPostal())
                $errors = true;
        }

        if($errors)
        {
            return false;
        } else
        {

            return $bulk_id;
        }

    }

    /**
     * @return self|bool
     */
    public function saveNewPostal($cancelPackageOnFailure = false, $forceOperatorId = false)
    {

        if($forceOperatorId)
            $postal_operator_id = $forceOperatorId;
        else
            $postal_operator_id = $this->getRouting()->postal_operator_id;

       $po = PostalOperator::model()->findByPk($postal_operator_id);

        if($this->postal_type == self::POSTAL_TYPE_REG OR $po->label_mode == PostalOperator::LABEL_MODE_WHOLE_EXTERNAL)
            return $this->_saveNewPostalReg($cancelPackageOnFailure, $forceOperatorId);
        else
            return $this->_saveNewPostalNotReg($cancelPackageOnFailure, $forceOperatorId);

    }

    /**
     * @return self|bool
     */
    protected function _saveNewPostalReg($cancelPackageOnFailure = false, $forceOperatorId = false)
    {
        $errors = false;

        $label = new PostalLabel();

        if($this->_pp_forceTTNumber) {
            $label->_pp_forceTTNumber = $this->_pp_forceTTNumber;
        }
        // @ deactivated @ 17.07.2018
//            $postal_operator_uniq_id = false;
//            // @ 25.04.2018
//            // Detect operator by ext number - ignore routing for PP packages
//
//            if(substr($label->_pp_forceTTNumber,0,12) == '005590077319' OR substr($label->_pp_forceTTNumber,0,13) == '0075900773112')
//                $postal_operator_uniq_id = PostalOperator::UNIQID_OPERATOR_PP_SP_Zabrze_Polecona_Firmowa;
//            else if(substr($label->_pp_forceTTNumber,0,7) == '0055900') // probably its PP FireBusiness
//                $postal_operator_uniq_id = PostalOperator::UNIQID_OPERATOR_PP_FIREBUSINESS_Polecona_Firmowa;
//
//            if($postal_operator_uniq_id)
//                $postal_operator_id = PostalOperator::getOperatorIdIdForUniqId($postal_operator_uniq_id);
//            else
//            {
//                $postal_operator_id = $this->getRouting()->postal_operator_id;
//                $postal_operator_uniq_id = PostalOperator::getOperatorUniqIdForId($postal_operator_id);
//            }

//        } else {

        if($forceOperatorId)
            $postal_operator_id = $forceOperatorId;
        else
            $postal_operator_id = $this->getRouting()->postal_operator_id;

        $postal_operator_uniq_id = PostalOperator::getOperatorUniqIdForId($postal_operator_id);
//        }

        $label->source_postal_operator_id = $postal_operator_id;
        $label->source_postal_operator_uniq_id = $postal_operator_uniq_id;
        $label->stat = PostalLabel::STAT_NEW;
        $label->postal = $this;



//		if($this->_ebay_order_id != '')
//			$label->_ebay_order_id = $this->_ebay_order_id;

        $label->_cancel_package_on_fail = $cancelPackageOnFailure;

        if($label->save())
        {
            $this->postal_label_id = $label->id;
        } else {
            $errors = true;
        }

        if($this->senderAddressData !== NULL)
        {
            if($this->senderAddressData->save())
                $this->sender_address_data_id = $this->senderAddressData->id;
            else
                $errors = true;
        }

        if($this->receiverAddressData !== NULL)
        {
            if($this->receiverAddressData->save())
                $this->receiver_address_data_id = $this->receiverAddressData->id;
            else
                $errors = true;
        }

        if($errors)
            return false;

        if($this->save())
            return $this;
        else
            return false;
    }

    /**
     * @return self|bool
     */
    protected function _saveNewPostalNotReg($cancelPackageOnFailure = false, $forceOperatorId = false)
    {
        if($forceOperatorId)
            $this->postal_operator_id = $forceOperatorId;

        $errors = false;

        if($this->senderAddressData !== NULL)
        {
            if($this->senderAddressData->save())
                $this->sender_address_data_id = $this->senderAddressData->id;
            else
                $errors = true;
        }

        if($this->receiverAddressData !== NULL)
        {
            if($this->receiverAddressData->save())
                $this->receiver_address_data_id = $this->receiverAddressData->id;
            else
                $errors = true;
        }


        if($errors)
            return false;

        if($this->save())
            return $this;
        else
            return false;
    }



    public static function saveWholeGroupNotMass(array $postals, $cancelPackageOnFailure = false, $misc = false, $source = false)
    {


        $packages = [];

        /* @var $postal Postal */
        foreach($postals AS $key => $postal)
        {

            $return = $postal->saveNewPostal($cancelPackageOnFailure);


            if($return instanceof Postal) {

                if($return->_import_raw_data_key !== false)
                {
                    if(isset($packages[$return->_import_raw_data_key])) // multipackage
                        $packages[$return->_import_raw_data_key.'|'.$return->id] = $return;
                    else
                        $packages[$return->_import_raw_data_key] = $return;
                }
                else
                    $packages[] = $return;

                if ($source == self::SOURCE_EBAY OR $source == self::SOURCE_LINKER)
                {

                    // maybe already exists? Then o
                    if($source == self::SOURCE_LINKER)
                        $ebayImportSource = EbayImport::SOURCE_LINKER;
                    else
                        $ebayImportSource = EbayImport::SOURCE_EBAY;

                    // maybe already exists? Then o
                    $ebayImport = EbayImport::model()->findByAttributes(['ebay_order_id' => $return->_ebay_order_id, 'user_id' => $return->user_id, 'target' => EbayImport::TARGET_POSTAL, 'source' => $ebayImportSource]);
                    if($ebayImport === NULL) {
                        $ebayImport = new EbayImport();
                        $ebayImport->user_id = $return->user_id;
                        $ebayImport->source = $ebayImportSource;
                    }
                    $ebayImport->target_item_id = $return->id;
                    $ebayImport->ebay_order_id = $return->_ebay_order_id;
                    $ebayImport->stat = EbayImport::STAT_NEW;
                    $ebayImport->authToken = $misc;
                    $ebayImport->target = EbayImport::TARGET_POSTAL;
                    $ebayImport->save();
                }

            }

        }

        $orderProducts = [];
        foreach($packages AS $packageItem)
        {
            $product = OrderProduct::createNewProduct(
                OrderProduct::TYPE_POSTAL,
                $packageItem->id,
                Yii::app()->PriceManager->getCurrencyCode(),
                $packageItem->getPrice(Yii::app()->PriceManager->getCurrency()),
                Yii::app()->PriceManager->getFinalPrice($packageItem->getPrice(Yii::app()->PriceManager->getCurrency()))
            );

            array_push($orderProducts, $product);
        }


        $order = Order::createOrderForProducts($orderProducts, true);

        if($order->fastFinalizeOrder(true))
        {
            return [
                'success' => true,
                'data' => $packages,
            ];
        }

        return false;
    }


    /**
     * Returns routing model for package
     *
     * @param bool|false $force_user_id If true, price will be calculated for particular user, not logged one
     * @return null|PostalRouting
     */
    public function getRouting($forceWeightClass = false, $force_user_id = false, $force_user_group_id = false)
    {

        if($this->weight_class === NULL)
            $this->weight_class = self::getWeightClassForWeight($this->weight);

        $weightClass = $this->weight_class;
        if($forceWeightClass)
            $weightClass = $forceWeightClass;

        if(!$force_user_id)
            $user_id = Yii::app()->user->id;
        else
            $user_id = $force_user_id;

        if($this->user_id === NULL)
            $this->user_id = $user_id;

        if($this->_routing === NULL)
        {
            $groupRouting = NULL;

            if($force_user_group_id)
                $user_group = $force_user_group_id;
            else
                $user_group = PostalUserGroup::getPostalGroupIdByUserId($this->user_id);

            if($user_group) {
                $groupRouting = PostalRouting::model()->cache(60)->findByAttributes([
                    'postal_type' => $this->postal_type,
                    'size_class' => $this->size_class,
                    'weight_class' => $weightClass,
                    'country_group_id' => CountryGroup::getGroupForCountryId($this->getReceiverCountryId()),
                    'postal_user_group_id' => $user_group,
                ]);
            }

            // found routing for user group
            if($groupRouting !== NULL) {
                $this->_routing = $groupRouting;
            }
            // routing for user's group not found
            else {
                $this->_routing = PostalRouting::model()->cache(60)->find(
                    'postal_type = :postal_type AND size_class = :size_class AND weight_class = :weight_class AND country_group_id = :country_group_id AND postal_user_group_id IS NULL',
                    [
                        ':postal_type' => $this->postal_type,
                        ':size_class' => $this->size_class,
                        ':weight_class' => $weightClass,
                        ':country_group_id' => CountryGroup::getGroupForCountryId($this->getReceiverCountryId()),
                    ]);
            }
        }


        // SPECIAL RULE
        // 11.08.2016
        // FOR WEIGHT CLASS 20g - if not available - get 50g
//        if(!$this->_routing && $weightClass == self::WEIGHT_CLASS_7)
//            $this->getRouting(self::WEIGHT_CLASS_1);

        // SPECIAL RULE FOR NEW CATEGORIES
        if(!$this->_routing && $weightClass == self::WEIGHT_CLASS_1)
            $this->getRouting(self::WEIGHT_CLASS_2);
        else if(!$this->_routing && $weightClass == self::WEIGHT_CLASS_8)
            $this->getRouting(self::WEIGHT_CLASS_9);
        else if(!$this->_routing && $weightClass == self::WEIGHT_CLASS_6)
            $this->getRouting(self::WEIGHT_CLASS_7);
        else if(!$this->_routing && $weightClass == self::WEIGHT_CLASS_4)
            $this->getRouting(self::WEIGHT_CLASS_5);
        else if(!$this->_routing && $weightClass == self::WEIGHT_CLASS_12)
            $this->getRouting(self::WEIGHT_CLASS_13);
        else if(!$this->_routing && $weightClass == self::WEIGHT_CLASS_11)
            $this->getRouting(self::WEIGHT_CLASS_13);
        else if(!$this->_routing && $weightClass == self::WEIGHT_CLASS_10)
            $this->getRouting(self::WEIGHT_CLASS_11);



        return $this->_routing;
    }

    /**
     * @param $postal_type int
     * @return null|CountryList[]
     */
    public static function getAvailableCountryList($postal_type)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->selectDistinct('country_list.id');
        $cmd->from('country_list');
        $cmd->join('country_group_has_country', 'country_list.id = country_group_has_country.country');
        $cmd->join('postal_routing', 'postal_routing.country_group_id = country_group_has_country.group');
        $cmd->where(['postal_tyle' => $postal_type]);
        $result = $cmd->queryAll();

        if(S_Useful::sizeof($result))
            return CountryList::model()->findAllByAttributes(['id' => $result]);
        else
            return NULL;
    }

    /**
     * Returns true if provided direction is available
     *
     * @param $receiver_country_id int
     * @param $postal_type int
     * @param $weight_class int
     * @param $size_class int
     * @param $user_id int
     * @param $force_no_group int Do not check price for group - just regular
     * @return boolean
     */
    public static function checkIfAvailable($receiver_country_id, $postal_type, $weight_class, $size_class, $user_id, $force_no_group = false)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->selectDistinct('postal_routing.id');
        $cmd->from('postal_routing');
        $cmd->join('country_group_has_country', 'country_group_has_country.group = postal_routing.country_group_id');
        $cmd->where([
            'postal_type' => $postal_type,
            'weight_class' => $weight_class,
            'size_class' => $size_class,
            'country_group_has_country.country' => $receiver_country_id,
        ]);

        if(!$force_no_group)
            $postalUserGroupId = PostalUserGroup::getPostalGroupIdByUserId($user_id);
        else
            $postalUserGroupId = false;

        if(!$postalUserGroupId)
            $cmd->andWhere('postal_user_group_id IS NULL');
        else {
            $cmd->andWhere('postal_user_group_id = :user_group_id', [':user_group_id' => $postalUserGroupId]);
        }

        $result = $cmd->queryScalar();

        if(!$force_no_group && !$result)
            $result = self::checkIfAvailable($receiver_country_id, $postal_type, $weight_class, $size_class, $user_id, true);


        // SPECIAL RULE
        // 11.08.2016
        // FOR WEIGHT CLASS 20g - if not available - get 50g
        if(!$result && $weight_class == self::WEIGHT_CLASS_7)
            $result = self::checkIfAvailable($receiver_country_id, $postal_type, self::WEIGHT_CLASS_1, $size_class, $user_id, $force_no_group);


        return $result;
    }

    /**
     * Returns available country list
     *
     * @param $domestic bool
     * @param bool|false $sender True if countries are for sender
     * @return static[]
     */
    public static function getCountryList($domestic = false, $sender = false)
    {
        return CountryList::model()->with('countryListTr')->findAll();
    }

    /**
     * Returns NULL if no price found
     *
     * @param bool|false $currency_id
     * @param bool|false $perItem
     * @param bool|false $force_user_id If true, price will be calculated for particular user, not logged one
     * @return float|null
     */
    public function getPrice($currency_id = false, $perItem = false, $force_user_id = false)
    {
        if(!$currency_id)
            $currency_id = Yii::app()->PriceManager->getCurrency();

        $price_id = $this->getRouting(false, $force_user_id)->price_id;

        $value = Price::getValueInCurrency($price_id, $currency_id);

        if($value !== NULL && $this->bulk && $this->bulk_number && !$perItem)
            $value *= $this->bulk_number;

        return $value;
    }

    public function deleteLastStatus($ownTransaction = true)
    {
        if($this->isStatHistoryArchived())
            return false;

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        /* @var $history PostalStatHistory */
        try
        {
            $history = $this->postalStatHistories(array('order' => 'status_date DESC'));
            if(!S_Useful::sizeof($history))
                return false;

            $history = $history[0];

            $this->postal_stat_id = $history->previous_stat_id;
            $history->delete();

            $this->save(false, array('postal_stat_id'));
        }
        catch(Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            if($ownTransaction)
                $transaction->rollback();

            return false;
        }

        if($ownTransaction)
            $transaction->commit();
        return true;
    }

    /**
     * @return PostalOperator
     */
    public function getOperator()
    {
        $operator = $this->getRouting()->operator;
        return $operator;
    }

    /**
     * Returns date of last stat
     * @return string
     */
    public function getLastStatUpdate()
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('status_date')->from('postal_stat_history')->where('postal_id = :id', [':id' => $this->id])->order('status_date DESC')->limit(1);

        return $cmd->queryScalar();
    }

    /**
     * @param $newStat
     * @param int $author
     * @param null $status_date
     * @param bool $ownTransaction
     * @param null $location
     * @param $checkDate If true, system won't change status if date is older that the newsets status and just add item to history
     * @param $doNotNotify
     * @param $viaAuctiomaticTt bool If true, status change is generated by automatic T&T system
     * @return bool
     */
    public function changeStat($newStat, $author = 0, $status_date = NULL, $ownTransaction = false, $location = NULL, $checkDate = false, $doNotNotify = false, $viaAutomaticTt = false, $overrideStatBlock = false)
    {
        // do not change stat if package is CANCELLED
        if(($this->stat_map_id == StatMap::MAP_CANCELLED OR $this->postal_stat_id == PostalStat::CANCELLED) && !$overrideStatBlock)
            return false;

        if($this->isStatHistoryArchived())
            return false;

        if($viaAutomaticTt)
        {
            if($status_date && strtotime(date('Ymd', strtotime($status_date)))-strtotime(date('Ymd', strtotime($this->date_entered))) < 0) // date from the past
            {
                MyDump::dump('postal_past_stat.txt', print_r($status_date,1).' : '.$this->id);
                return false;
            }
        }

        if($viaAutomaticTt) {

            if($this->postal_stat_id == PostalStat::CANCELLED_USER)
            {

                if($this->user_cancel == Postal::USER_CANCELL_REQUESTED)
                {
                    $this->user_cancel = Postal::USER_CANCELL_REQUESTED_FOUND_TT;
                    $this->update(['user_cancel']);
                    $this->addToLog('Found new status by TT on user cancelled package: #'.$newStat);
                }

                return false;
            }


//            // if package is cancelled by user
//            if ($this->isUserCancelConfirmed()) {
//                $this->userCancelWarn();
//                return false;
//            }
//
//            if ($this->isUserCancelWarned())
//                return false;
        }


        if(!$viaAutomaticTt && $this->user_cancel)
        {
            $this->user_cancel = NULL;
            $this->update(['user_cancel']);
            $this->addToLog('Manual change of status cleared User Cancel flag!');
        }

        $location = trim(strip_tags($location));

        $status_date = PostalStat::convertDate($status_date);

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $newStat = PostalStat::model()->findByPk($newStat);
        if($newStat === null)
            return false;

//        if($this->postal_stat_id == $newStat->id)
//            return true;


        $statHistory = new PostalStatHistory();
        $statHistory->author = $author;
        $statHistory->postal_id = $this->id;
        $statHistory->previous_stat_id = $this->postal_stat_id;
        $statHistory->current_stat_id = $newStat->id;
        $statHistory->status_date = $status_date;
        $statHistory->location = $location;


        $statHistory->validate();

        if(!$statHistory->save())
            $errors = true;



        $justHistory = false;
        if($checkDate)
        {
            // to get proper date
            $statHistory = PostalStatHistory::model()->findByPk($statHistory->id);


            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from('postal_stat_history');
            $cmd->where('author = :author AND status_date > :status_date AND id != :id AND postal_id = :postal_id', array(':author' => $author, ':status_date' => $statHistory->status_date, ':id' => $statHistory->id, ':postal_id' => $this->id));
            $res = $cmd->queryScalar();
            if($res)
                $justHistory = true;

        }

        if(!$justHistory) {
            $this->postal_stat_id = $newStat->id;
            $this->location = $location;


            $doNotChangeMap = false;
            // do not change stat map if package is IN RETURN by automatic TT
            if($this->stat_map_id == StatMap::MAP_RETURN && $viaAutomaticTt)
                $doNotChangeMap = true;

            // do not change stat map if package CANCELLED and new map id ID = 1
            else if($this->stat_map_id == StatMap::MAP_CANCELLED)
            {
                $newStatMap = StatMapMapping::findMapForStat($newStat->id, StatMapMapping::TYPE_POSTAL);
                if($newStatMap == StatMap::MAP_NEW)
                    $doNotChangeMap = true;
            }

//            do not change stat map if package DELIVERED with exception for RETURNED map
            else if($this->stat_map_id == StatMap::MAP_DELIVERED)
            {
                $newStatMap = StatMapMapping::findMapForStat($newStat->id, StatMapMapping::TYPE_POSTAL);
                if($newStatMap != StatMap::MAP_RETURN)
                    $doNotChangeMap = true;
            }

            $currentStatMap = $this->stat_map_id;

            if(!$doNotChangeMap)
                $statMap = StatMapMapping::findMapForStat($this->postal_stat_id, StatMapMapping::TYPE_POSTAL);

            if(!$doNotChangeMap && $statMap && $statMap != $this->stat_map_id)
            {
                $oldStatMap = $this->stat_map_id;
                $this->stat_map_id = $statMap;
                if (!$this->save(false, ['postal_stat_id', 'location', 'date_updated', 'stat_map_id']))
                    $errors = true;
                else {
                    $this->afterStatMapChange($oldStatMap, $statMap, true, false, $location, $status_date, false, $this->postal_stat_id);
                }

//                if(!$errors)
//                    StatMapHistory::addItem(StatMapMapping::TYPE_POSTAL, $this->id, $this->stat_map_id, $currentStatMap, $status_date, $location);

                if (!$errors && !$doNotNotify) {
                    $_StatNotifer = new S_StatNotifer(S_StatNotifer::SERVICE_POSTAL, $this->id, $newStat->id, $status_date, $location);
                    $_StatNotifer->onStatusChange();
                }

            } else {


                if (!$this->save(false))
                    $errors = true;

//				if (!$errors) {
//					if (!$doNotNotify) {
//						$_StatNotifer = new S_StatNotifer(S_StatNotifer::SERVICE_POSTAL, $this->id, $newStat->id, $status_date, $location);
//						$_StatNotifer->onStatusChange();
//					}
//				}
            }
        } else {

            // correct stat map
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('previous_stat_map_id');
            $cmd->from((new StatMapHistory())->tableName());
            $cmd->where('type = :type AND type_item_id = :type_item_id AND stat_map_id = :stat_map_id',
                [
                    ':type' => StatMapMapping::TYPE_POSTAL,
                    ':type_item_id' => $this->id,
                    ':stat_map_id' => $this->stat_map_id,
                ]
            );

            $previousStatMap = $cmd->queryScalar();

            $mapForThisStat = $newStat->statMapMapping->map_id;

            if($previousStatMap != $mapForThisStat)
            {
                $statMapHistory = new StatMapHistory();
                $statMapHistory->type = StatMapMapping::TYPE_POSTAL;
                $statMapHistory->type_item_id = $this->id;
                $statMapHistory->stat_date = $status_date;
                $statMapHistory->location = $location;
                $statMapHistory->previous_stat_map_id = $previousStatMap;
                $statMapHistory->stat_map_id = $mapForThisStat;
                $statMapHistory->date_entered = date('Y-m-d H:i:s');
                $statMapHistory->save();
            }
            //
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();
            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();
            return true;
        }
    }

    public function isStatHistoryArchived()
    {
        if(in_array($this->archive, [self::ARCHIVED_HISTORY, self::ARCHIVED_HISTORY + self::ARCHIVED_LABEL_EXPIRED]))
            return true;
        else
            return false;
    }

    public function isLabelArchived()
    {
        if(in_array($this->archive, [self::ARCHIVED_LABEL_EXPIRED, self::ARCHIVED_HISTORY + self::ARCHIVED_LABEL_EXPIRED]))
            return true;
        else
            return false;
    }

    public function getPostalStatHistoryLast()
    {
        return PostalStatHistory::model()->find(['condition' => 'postal_id = :postal_id AND current_stat_id = :stat', 'params' => [':postal_id' => $this->id, ':stat' => $this->postal_stat_id], 'order' => 'status_date DESC']);
    }

    /**
     * Function saving log information to package
     * @param string $text Content of log
     * @param integer $cat Category of log - categories available in PostaLog class
     * @param string $source Source of log - string
     * @return bool
     */
    public function addToLog($text, $cat = NULL, $source = NULL)
    {
        $postalLog = new PostalLog;
        $postalLog->text = $text;
        $postalLog->cat = $cat;
        $postalLog->source = $source;
        $postalLog->postal_id = $this->id;

        return $postalLog->save();
    }

    /**
     * Checks if user is able to cancel this package
     *
     * @return bool
     */
    public function isUserCancelByUserPossible()
    {
        if($this->user_cancel OR $this->postal_stat_id == PostalStat::CANCELLED OR $this->postal_stat_id == PostalStat::CANCELLED_USER OR in_array($this->stat_map_id, [StatMap::MAP_DELIVERED, StatMap::MAP_CANCELLED]))
            return false;

        $now = new DateTime();
        $packageDay = new DateTime($this->date_entered);

        $diff = $now->diff($packageDay)->format("%a");

        if($diff > (Postal::PACKAGE_CANCEL_DAYS - 1))
            return false;

        return true;
    }

    public function countTimeLeftForCancel()
    {
        $diff = strtotime($this->date_entered) + (self::PACKAGE_CANCEL_DAYS * 24 * 60 * 60) - strtotime(date('Y-m-d H:i:s'));

        $h = floor($diff/3600);
        $min = ($diff/60)%60;

        return Yii::t('panel', '{h}g. {m}min.', ['{h}' => $h, '{m}' => $min]);

    }

    /**
     * Checks if user is able to cancel this package
     *
     * @return bool
     */
    public function isUserComplaintPossible()
    {

        if($this->isUserCancelByUserPossible())
            return false;

        if($this->user_cancel OR $this->postal_stat_id == PostalStat::CANCELLED OR $this->postal_stat_id == PostalStat::CANCELLED_USER OR in_array($this->stat_map_id, [StatMap::MAP_CANCELLED]))
            return false;

        return true;
    }

    /**
     * Mark package as complained by user
     * @return bool
     */
    public function markUserComplaint()
    {
        $this->addToLog('Klient przesłał formularz reklamacji.');
        $this->user_cancel = self::USER_CANCELL_COMPLAINED;
        return $this->update(['user_cancel', 'date_updated']);
    }

    /**
     * Make request by user to cancel this package
     *
     * @return bool
     * @throws CDbException
     */
    public function userCancelRequestByUser()
    {
        if($this->isUserCancelByUserPossible())
        {
            $this->changeStat(PostalStat::CANCELLED_USER, 0);

            $this->user_cancel = self::USER_CANCELL_REQUESTED;
            $this->update(['user_cancel', 'date_updated']);

            return true;
        }
        return false;

    }

    /**
     * Cancel group of packages
     *
     * @param Postal[] $models
     * @return int Number of cancelled packages
     */
    public static function userCancelMulti(array $models, &$done = [])
    {
        $counter = 0;

        /* @var $model Postal */
        foreach($models AS $key => $model)
        {
            if($model->userCancelRequestByUser()) {
                $done[$key] = $model->id;
                $counter++;
            }
        }

        return $counter;
    }

    public static function changeStatForGroup(array $models, $stat_id, $date = NULL, $location = NULL)
    {

        /* @var $model Postal */

        $postalStat = PostalStat::model()->findByPk($stat_id);
        if($postalStat == NULL)
            return false;

        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $i = 0;
        foreach($models AS $model)
        {
            if(!$model->changeStat($stat_id,'',$date,false, $location))
                $errors = true;

            $i++;
        }


        if(!$errors)
        {
            $transaction->commit();
            return $i;
        } else {
            $transaction->rollback();
            return false;
        }
    }


    public static function deleteLastStatForGroup(array $models)
    {
        /* @var $model Postal */

        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $i = 0;
        foreach($models AS $model)
        {
            if(!$model->deleteLastStatus(false))
                $errors = true;

            $i++;
        }


        if(!$errors)
        {
            $transaction->commit();
            return $i;
        } else {
            $transaction->rollback();
            return false;
        }
    }

    /**
     * @param $bulkId
     * @return Postal[]
     */
    public static function getPostalsForBulkId($bulkId, $onlyActive = false)
    {

        if($onlyActive)
            $models = self::model()->findAll(['condition' => 'bulk_id = :bulk_id AND postal_stat_id != :stat1 AND postal_stat_id != :stat2 AND postal_stat_id != :stat3', 'params' => [
                ':bulk_id' => $bulkId,
                ':stat1' => PostalStat::CANCELLED_PROBLEM,
                ':stat2' => PostalStat::CANCELLED,
                ':stat3' => PostalStat::CANCELLED_USER,
            ], 'order' => 'receiver_country_id ASC, weight_class ASC, postal_type ASC, size_class ASC']);
        else
            $models = self::model()->findAllByAttributes(['bulk_id' => $bulkId]);

        return $models;
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * After creating label, send to eBay.com data about package and update import status
     *
     * @param Postal $postal Package model
     * @param $operator string Operator name to be send to eBay.com
     * @param $ttNumber string Package number to be send to eBay.com
     * @param $ebay_order_id string Ebay order ID
     * @param bool|false $forceCancelled If true, just update EbayImport data with cancelled stat - for example whet ordering label failed
     * @return bool
     */
    public static function updateEbayData(Postal $postal, $operator, $ttNumber, $ebay_order_id, $forceCancelled = false)
    {

        if(!$forceCancelled) {

            $ebayImport = EbayImport::model()->find(['condition' => 'ebay_order_id = :ebay_order_id AND user_id = :user_id AND target = :target', 'params' => [':ebay_order_id' => $ebay_order_id, ':user_id' => $postal->user_id, ':target' => EbayImport::TARGET_POSTAL], 'order' => 'id DESC']);

            if($ebayImport->source == EbayImport::SOURCE_LINKER)
            {
                $ebayUpdated = LinkerClient::updateItem($ebay_order_id, $ttNumber, $operator, $postal->user->apikey, $postal->getReceiverCountryId(), $postal->user->source_domain);
            } else {

                $ebayClient = ebayClient::instance($ebayImport->authToken);
                $ebayUpdated = $ebayClient->updateTt($ebay_order_id, $ttNumber, $operator);
            }

            if ($ebayUpdated)
                $ebayStat = EbayImport::STAT_SUCCESS;
            else
                $ebayStat = EbayImport::STAT_ERROR;



            $postal->_ebay_export_status = $ebayStat;
            $postal->update();


        } else
            $ebayStat = EbayImport::STAT_CANCELLED;

        $cmd = Yii::app()->db->createCommand();
        $cmd->update((new EbayImport())->tableName(), [
            'stat' => $ebayStat,
            'authToken' => NULL,
            'date_updated' => date('Y-m-d H:i:s'),
        ],
            'target_item_id = :postal_id AND target = :target',
            [
                ':postal_id' => $postal->id,
                ':target' => EbayImport::TARGET_POSTAL
            ]
        );


        return $ebayStat == EbayImport::STAT_SUCCESS ? true : false;
    }

    public function getBrokerId()
    {
        $this->postalOperator->broker_id;
    }

    public function onAfterAckGeneration()
    {
        $this->ack_generated = new CDbExpression('NOW()');
        $this->update(['ack_generated']);
    }

    public static function onAfterAckGenerationGroup(array $ids)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->update((new self)->tableName(),[
            'ack_generated' => new CDbExpression('NOW()')
        ],
            ['in', 'id', $ids]
        );
    }


    public static function automaticDevlieryStat()
    {
        $days_PL = 30; // days
        $days_UE = 40; // days
        $days_REST = 48; // days

        $ueListNoPl = implode(',', CountryList::getUeList(false));
        $ueList = implode(',', CountryList::getUeList());

        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('postal.id');
        $cmd->from('postal');
//        $cmd->leftJoin('stat_map_history', 'stat_map_history.type = :type AND stat_map_history.type_item_id = postal.id AND stat_map_history.stat_map_id = postal.stat_map_id', [':type' => StatMapMapping::TYPE_POSTAL]);
//        $cmd->where('(postal.stat_map_id = :stat_map_id_1 OR postal.stat_map_id = :stat_map_id_2) AND ((
//       ( stat_map_history.date_entered < (NOW() - INTERVAL :days_pl DAY) AND receiver_country_id = :country_pl)
//        OR
//        ( stat_map_history.date_entered < (NOW() - INTERVAL :days_ue DAY) AND receiver_country_id IN ('.$ueListNoPl.'))
//        OR
//        ( stat_map_history.date_entered < (NOW() - INTERVAL :days_rest DAY) AND receiver_country_id NOT IN ('.$ueList.'))
//        ) OR stat_map_history.date_entered IS NULL AND postal.date_entered < ( NOW( ) - INTERVAL 90 DAY )
//        )',
//            [
//                ':stat_map_id_1' => StatMap::MAP_IN_TRANSPORTATION,
//                ':stat_map_id_2' => StatMap::MAP_IN_DELIVERY,
//                ':days_pl' => $days_PL,
//                ':days_ue' => $days_UE,
//                ':days_rest' => $days_REST,
//                ':country_pl' => CountryList::COUNTRY_PL
//            ]);

        $cmd->leftJoin('postal_stat_history', 'postal_stat_history.postal_id = postal.id');


        /* @ 19.07.2018 - disable automatic stat for user Post11 (#1289) */
        $cmd->where('(postal.stat_map_id = :stat_map_id_1 OR postal.stat_map_id = :stat_map_id_2) AND ((        
       ( postal_stat_history.date_entered < (NOW() - INTERVAL :days_pl DAY) AND receiver_country_id = :country_pl)
        OR
        ( postal_stat_history.date_entered < (NOW() - INTERVAL :days_ue DAY) AND receiver_country_id IN ('.$ueListNoPl.'))
        OR
        ( postal_stat_history.date_entered < (NOW() - INTERVAL :days_rest DAY) AND receiver_country_id NOT IN ('.$ueList.'))
        ) OR postal_stat_history.date_entered IS NULL AND postal.date_entered < ( NOW( ) - INTERVAL 90 DAY )
        )
        AND postal_stat_history.id IN (SELECT MAX(id) FROM postal_stat_history psh WHERE psh.postal_id = postal.id
        AND postal.user_id != :post11_id
        )
        ',
            [
                ':stat_map_id_1' => StatMap::MAP_IN_TRANSPORTATION,
                ':stat_map_id_2' => StatMap::MAP_IN_DELIVERY,
                ':days_pl' => $days_PL,
                ':days_ue' => $days_UE,
                ':days_rest' => $days_REST,
                ':country_pl' => CountryList::COUNTRY_PL,
                ':post11_id' => 1289,
            ]);
        $cmd->limit = 100;
        $result = $cmd->queryColumn();

        $models = Postal::model()->findAllByPk($result);

        /* @var $models Postal[] */
        foreach($models AS $model) {

            $status_date = $model->postalStatHistoryLast ? $model->postalStatHistoryLast->status_date : '';

            if($model->receiver_country_id == CountryList::COUNTRY_PL)
                $days = $days_PL;
            else if(in_array($model->receiver_country_id, CountryList::getUeList(false)))
                $days = $days_UE;
            else
                $days = $days_REST;

            if($status_date == '')
                $status_date = 'now';


            $text = $model->id.' : ';

            $status_date = new DateTime($status_date);
            $status_date->modify('+'.$days.' day');

            if($status_date > new DateTime())
            {
                $text .= 'Data z przyszłości!';
            } else {

                $status_date = $status_date->format('Y-m-d H:i:s');

                if ($model->isStatHistoryArchived()) {
                    $model->postal_stat_id = PostalStat::STAT_PROBABLY_DELIVERED;
                    $model->stat_map_id = StatMap::MAP_DELIVERED;
                    $model->location = '';
                    if ($model->update(['postal_stat_id', 'stat_map_id', 'date_updated', 'location']))
                        $text .= 'OK - historyczny';
                    else
                        $text .= 'BŁĄD - historyczny';

                } else {
                    if ($model->changeStat(PostalStat::STAT_PROBABLY_DELIVERED, 1000, $status_date, false, NULL, false, true))
                        $text .= $status_date;
                    else
                        $text .= 'BŁĄD';
                }
            }

            MyDump::dump('apcs.txt', $text);
        }

        return $result;

    }

    public function getStatMapName()
    {
        if($this->stat_map_id)
            return StatMap::getMapNameById($this->stat_map_id);
        else
            return false;
    }

    /**
     * @return null|Order
     */
    public function getOrder()
    {
        $model = OrderProduct::model()->findByAttributes(['type' => OrderProduct::TYPE_POSTAL, 'type_item_id' => $this->id]);
        if($model)
            return $model->order;
        else
            return $model;
    }

    public function getValueCurrency()
    {
        if($this->value_currency == NULL)
            return 'PLN';
        else
            return $this->value_currency;
    }

    public function getValueConverted($targetCurrency)
    {
        return Yii::app()->NBPCurrency->getCurrencyValue($this->value, $this->value_currency, $targetCurrency);
    }

    public function afterStatMapChange($map_id_before, $map_id_after, $addToHistory = false, $saveInLog = false, $historyLocation = NULL, $historyDate = NULL, $logAuthor = false, $stat_id = false)
    {
        StatMapMapping::logToFile($this->local_id, $map_id_after, StatMapMapping::TYPE_POSTAL, $stat_id, S_Useful::sizeof($this->smsArchive));

        if($saveInLog)
            $this->addToLog($saveInLog, CourierLog::CAT_INFO, $logAuthor);


        if($addToHistory)
            StatMapHistory::addItem(StatMapMapping::TYPE_POSTAL, $this->id, $this->stat_map_id, $map_id_before, $historyDate, $historyLocation);

    }


    public static function groupChangeStatusByIds($ids, $stat_id, $author = 0, $location = NULL)
    {
        $models = self::model()->findAllByPk($ids);

        $transaction = Yii::app()->db->beginTransaction();

        try {
            /* @var $models Postal[] */
            foreach ($models AS $model)
                $model->changeStat($stat_id, $author, NULL, false, $location);
        }
        catch(Exception $ex)
        {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }
}
