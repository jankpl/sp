<?php

Yii::import('application.modules.internationalMail.models._base.BaseInternationalMail');

class InternationalMail extends BaseInternationalMail
{
    const PDF_6_ON_1 = 1;
//    const PDF_2_ON_1 = 2;
    const PDF_ROLL = 3;

    protected static $_weight_unit = 'g';
    protected static $_dimension_unit = 'cm';
    public $regulations;

    public $_international_mail_additions;

    public $_first_status_date = NULL;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'bLocalIdGenerator' =>
                array('class'=>'application.models.bLocalIdGenerator'
                ),
//            'bRating'=>
//                array('class'=>'application.models.bChangeStat'
//                ),
            'bAddressDataSenderCompatilibity' =>
                array('class' => 'application.models.bAddressDataSenderCompatilibity'
                ),
            'bAddressDataReceiverCompatilibity' =>
                array('class' => 'application.models.bAddressDataReceiverCompatilibity'
                ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            )
        );
    }

    public function changeStat($newStat, $author = 0, $status_date = NULL, $ownTransaction = false)
    {

        $status_date = InternationalMailStat::convertDate($status_date);

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $newStat = InternationalMailStat::model()->findByPk($newStat);
        if($newStat === null)
            return false;

//        if($this->international_mail_stat_id == $newStat->id)
//            return true;

        $statHistory = new InternationalMailStatHistory();
        $statHistory->author = $author;
        $statHistory->international_mail_id = $this->id;
        $statHistory->previous_stat_id = $this->international_mail_stat_id;
        $statHistory->current_stat_id = $newStat->id;
        $statHistory->status_date = $status_date;


        $statHistory->validate();

        if(!$statHistory->save())
            $errors = true;

        $this->international_mail_stat_id  = $newStat->id;

        if(!$this->save(false))
            $errors = true;

        if(!$errors) {
            $_StatNotifer = new S_StatNotifer(S_StatNotifer::SERVICE_IM, $this->id, $newStat->id, $status_date);
            $_StatNotifer->onStatusChange();
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();
            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();
            return true;
        }
    }


    public static function listCountries($user_group_id = NULL)
    {


        $cmd = Yii::app()->db->createCommand();
        $cmd->select = 'country_list.name, country_list.id';
        $cmd->distinct = true;
        $cmd->from = 'country_list';
        $cmd->join('country_group_has_country','country_group_has_country.country = country_list.id');
        $cmd->join('country_group','country_group_has_country.group = country_group.id');
        $cmd->join('international_mail_price_has_group','international_mail_price_has_group.country_group = country_group.id');
        $cmd->where('country_list.stat = '.S_Status::ACTIVE);
        if($user_group_id === NULL OR !$user_group_id)
            $cmd->andWhere('international_mail_price_has_group.user_group_id IS NULL');
        else
            $cmd->andWhere('international_mail_price_has_group.user_group_id = '.$user_group_id.' OR international_mail_price_has_group.user_group_id IS NULL');

        $cmd->order('country_list.name DESC');


        $countries = CountryList::model()->with('countryListTr')->findAllBySql($cmd->getText());

        return $countries;
    }

    public function getDaysFromStatusChange()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select("(SELECT TIMESTAMPDIFF(DAY , date_entered, CURRENT_TIMESTAMP) FROM international_mail_stat_history
WHERE international_mail_id = international_mail.id ORDER BY date_entered DESC LIMIT 0,1)");
        $cmd->from("international_mail");
        $cmd->andWhere("international_mail.id = '".$this->id."'");
        return $cmd->queryScalar();
    }

    public static function getDimensionUnit()
    {
        return self::$_dimension_unit;
    }

    public static function getWeightUnit()
    {
        return self::$_weight_unit;
    }

    public function afterSave()
    {

        if($this->isNewRecord)
        {
            $this->isNewRecord = false;

            /* @var $statHistory InternationalMailStatHistory() */

            $statHistory = new InternationalMailStatHistory();
            $statHistory->international_mail_id = $this->id;
            $statHistory->previous_stat_id = NULL;
            $statHistory->current_stat_id = $this->international_mail_stat_id == ''?InternationalMailStat::NEW_ORDER:$this->international_mail_stat_id;
            $statHistory->status_date = $this->_first_status_date;

            if(!$statHistory->save())
                return false;
        }

        return parent::afterSave();
    }

    protected function beforeSave()
    {

        return parent::beforeSave();
    }

    public function rules() {
        return array(


            array('international_mail_stat_id',
                'default',
                'value'=>InternationalMailStat::NEW_ORDER,
                'on'=>'insert'),

            array('user_id',
                'default',
                'value'=>Yii::app()->user->isGuest?null:Yii::app()->user->id,
                'on'=>'insert'),

            array('mail_weight,
            mail_size_l,
            mail_size_d,
            mail_size_w,
            ', 'required',
                'on' => 'step1'),

            array('regulations', 'boolean', 'falseValue' => 'true', 'message' => 'Musisz zaakceptować regulamin, aby móc kontynować!', 'on' => 'step5'),

            array('sender_address_data_id, receiver_address_data_id, international_mail_stat_id, mail_weight, mail_size_l, mail_size_w, mail_size_d, mails_number', 'required', 'on'=>'insert, admin'),

            array('mail_weight, mail_size_l, mail_size_d, mail_size_w,', 'numerical', 'min' => 0.1),

            array('international_mail_stat_id, sender_address_data_id, receiver_address_data_id, mails_number', 'numerical', 'integerOnly'=>true),

            array('user_id, order_id', 'length', 'max'=>10),

            array('local_id', 'length', 'max'=>12),
            array('date_updated', 'safe'),
            array('date_updated, user_id, order_id', 'default', 'setOnEmpty' => true, 'value' => null),

            array('id, date_entered, date_updated, user_id,order_id, international_mail_stat_id, local_id, sender_address_data_id, receiver_address_data_id, mail_weight, mail_size_l, mail_size_w, mail_size_d, mails_number,
               __user_login,
                 __sender_country_id,
                 __receiver_country_id,
                 __stat,
                daysFromStatusChange,
            ', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Returns pickup price for one package
     *
     * @return float
     */
    public function price($currency)
    {

        $user_group = User::getUserGroupId($this->user_id);

        $price = InternationalMailPrice::calculatePriceEach($currency, $this->mail_weight, $this->receiverAddressData->country_id, $user_group);

        return $price;
    }


    /**
     * Returns price for one package
     *
     * @return float
     */
    public function priceEach($currency)
    {
        $price = 0;

        $price += $this->price($currency);

        //$price += $this->deliveryPrice($currency);
        $price += $this->additionsPrice();

        return $price;
    }

    /**
     * Return price for all packages
     *
     * @return float
     */
    public function priceTotal($currency)
    {

        $price = $this->priceEach($currency) * $this->mails_number;

        return $price;
    }

    public function isItTooBig()
    {
        return S_InternationalMailMaxDimensions::isTooBig($this);
    }

    public function isItTooHeavy()
    {
        return S_InternationalMailMaxDimensions::isTooHeavy($this);
    }


    public function getMaxDimensions()
    {
        return S_InternationalMailMaxDimensions::getMaxDimensions();
    }

    public function getMaxWeight()
    {
        return S_InternationalMailMaxDimensions::getMaxDimensions()['weight'];
    }


    public function generateCarriageTicket($type)
    {
        self::generateCarriageTickets($this, $type);
    }

    public function saveNewIM($saveSenderToBook = false, $saveReceiverToBook = false, $ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        /**
         * RECEIVER DATA
         */
        if($this->receiverAddressData === NULL)
            $this->receiverAddressData = new InternationalMail_AddressData('receiver');


        $this->receiverAddressData->validate();
        if($this->receiverAddressData->hasErrors())
            $errors = true;
        else
        {
            $this->receiverAddressData->save();
            $this->receiver_address_data_id = $this->receiverAddressData->id;


            if($saveReceiverToBook)
                UserContactBook::createAndSave(false, $this->receiverAddressData->attributes, 'receiver');
        }


        /**
         * SENDER DATA
         */
        if($this->senderAddressData === NULL)
            $this->senderAddressData = new InternationalMail_AddressData('sender');

        $this->senderAddressData->validate();
        if($this->senderAddressData->hasErrors())
            $errors = true;
        else
        {
            $this->senderAddressData->save();
            $this->sender_address_data_id = $this->senderAddressData->id;

            if($saveSenderToBook)
                UserContactBook::createAndSave(false, $this->senderAddressData->attributes, 'sender');
        }


        /**
         * ORDER DATA
         */
        $this->scenario = 'insert';
        if(!$this->save())
            $errors = true;

        /* @var $item InternationalMailAdditionList */
        foreach($this->listAdditions(true) AS $item)
        {
            $addition = new InternationalMailAddition();
            $addition->international_mail_id = $this->id;
            $addition->description = $item->internationalMailAdditionListTr->description;
            $addition->price = Yii::app()->PriceManager->getFinalPrice($item->getPrice());
            $addition->name = $item->internationalMailAdditionListTr->title;

            if(!$addition->save())
                $errors = true;
        }

        $orderProducts = [];

        if(!$errors)
        {

            $product = OrderProduct::createNewProduct(
                OrderProduct::TYPE_INTERNATIONAL_MAIL,
                $this->id,
                Yii::app()->PriceManager->getCurrencyCode(),
                $this->priceTotal(Yii::app()->PriceManager->getCurrency()),
                Yii::app()->PriceManager->getFinalPrice($this->priceTotal(Yii::app()->PriceManager->getCurrency())));



            if(!$product)
                $errors = true;

            array_push($orderProducts, $product);
        }


        if(!$errors)
        {
            $order = Order::createOrderForProducts($orderProducts, true);

            if(!$order)
                $errors = true;
        }

        if(!$errors)
        {
            if($ownTransaction)
                $transaction->commit();
            return $order;
        }
        else
        {
            if($ownTransaction)
                $transaction->rollBack();
            return false;
        }
    }

    public function assingToOrder($order_id)
    {
        $this->order_id = $order_id;
        return $this->update(array('order_id'));
    }

    public function assignCarrier($carrier_id = NULL , $outside_id, $update_status = false)
    {
        /* @var $model InternationalMail */
        $model = InternationalMail::model()->findByPk($_POST['InternationalMail']['id']);

        if($model === NULL)
            return false;


        $transaction = Yii::app()->db->beginTransaction();

        $model->outside_id = $_POST['InternationalMail']['outside_id'];



        if($model->changeStat(3) && $model->update('outside_id'))
        {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollback();
            return false;
        }

    }

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

    static public function generateCarriageTickets($mails, $type = NULL)
    {

        try{

            if(!is_array($mails))
            {
                $temp = $mails;
                $mails = Array();
                array_push($mails, $temp);
            }

            $margins = 10;

            /* @var $pdf TCPDF */
            $pdf = PDFGenerator::initialize();

            $blockWidth = 90;
            $blockHeight = 65;

            switch($type)
            {
                case InternationalMail::PDF_ROLL:
                    $pdf = self::_pdf_roll($pdf, $mails, $blockWidth, $blockHeight, $margins);
                    break;
//                case InternationalMail::PDF_2_ON_1:
//                    $pdf = self::_pdf_2_on_1($pdf, $mails, $blockWidth, $blockHeight, $margins);
//                    break;
                case InternationalMail::PDF_6_ON_1:
                default:
                    $pdf = self::_pdf_6_on_1($pdf, $mails, $blockWidth, $blockHeight, $margins);
                    break;
            }

            $pdf->Output('KAAB-labels.pdf', 'D');

        }
        catch(CHttpException $ex)
        {

            throw new CHttpException('403', $ex->getMessage());
        }

    }


    static protected function _pdf_6_on_1(&$pdf, $mails, $blockWidth, $blockHeight, $margins)
    {
        $pdf->AddPage('H','A4', true);
        $pdf->setMargins(0,0,0,true);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);

        $rightBlockXPosition = $pdf->getPageWidth(0) - $blockWidth - 2 * $margins;
        $bottomBlockYPosition = $pdf->getPageHeight(0) - $blockHeight - 2 * $margins - 25;

        $totalCounter = 0;

        $j = 0;
        /* @var $item InternationalMail */
        $ids = [];
        foreach($mails AS $item)
        {
            array_push($ids, $item->id);

            for($i = 1; $i <= $item->mails_number; $i++)
            {
                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('internationalMail','Too many labels!'));

                if($j%6 == 0 AND $j > 0)
                {
                    $pdf->AddPage('H','A4');
                    $j = 0;
                }

                $xBase = ($j%2) ? $rightBlockXPosition - 2 : 2;
//                $yBase = ($j%4 == 0 OR $j%4 == 1)? $baseY = 24 : $baseY = $bottomBlockYPosition + 26;

                if($j == 2 || $j == 3) $yBase = 120;
                else if($j == 4 || $j == 5) $yBase = $bottomBlockYPosition + 26;
                else $yBase = 24;

                $pdf = self::_labelBody($pdf, $xBase, $yBase, $margins, $blockWidth, $blockHeight, $item, $i);

                $j++;
            }
        }

        return $pdf;
    }

    /**
     * Returns price of additions for one package
     *
     * @return float
     */
    public function additionsPrice()
    {

        $additions = $this->_international_mail_additions;

        if(!is_array($additions))
            return 0;

        $price = 0;
        foreach($additions AS $item)
        {
            /* @var $model InternationalMailAdditionList */
            $model = InternationalMailAdditionList::model()->findByPk($item);
            if($model === null)
                continue;

            $price += $model->getPrice();
        }

        return $price;
    }

    public function addAdditions($additions)
    {
        $this->_international_mail_additions = $additions;
    }

    /**
     * @param bool $forceNotSaved Ignore that model has already ID and try to get additions by array of additions ids
     * @return array|mixed|null|static
     */
    public function listAdditions($forceNotSaved = false)
    {

        if($this->id == NULL OR $forceNotSaved)
            $additions = InternationalMailAdditionList::model()->findAllByAttributes(array('id' => $this->_international_mail_additions));
        else
            $additions = InternationalMailAddition::model()->findAllByAttributes(array('international_mail_id' => $this->id));

        return $additions;
    }


//    static protected function _pdf_2_on_1($pdf, $mails, $blockWidth, $blockHeight, $margins)
//    {
//        $pdf->AddPage('L','A4', true);
//        $rightBlockXPosition = $pdf->getPageWidth(0) - $blockWidth - 2 * $margins;
//        $bottomBlockYPosition = $pdf->getPageHeight(0) - $blockHeight - 2 * $margins - 25;
//        $pdf->setMargins(0,0,0,true);
//        $pdf->setPrintFooter(false);
//        $pdf->SetAutoPageBreak(false);
//
//
//        $j = 0;
//        /* @var $item InternationalMail */
//
//        $totalCounter = 0;
//
//        $ids = [];
//        foreach($mails AS $item)
//        {
//            array_push($ids, $item->id);
//
//            for($i = 1; $i <= $item->mails_number; $i++)
//            {
//                $totalCounter++;
//                if($totalCounter > 1000)
//                    throw new CHttpException(403, Yii::t('internationalMail','Too many labels!'));
//
//                if($j%2 == 0 AND $j > 0)
//                {
//                    $pdf->AddPage('L','A4');
//                    $j = 0;
//                }
//
//                $xBase = ($j%2 && $j>0) ? $rightBlockXPosition - 45 : 45;
//                $yBase = 43;
//
//
//                $pdf = self::_labelBody($pdf, $xBase, $yBase, $margins, $blockWidth, $blockHeight, $item, $i);
//
//                $j++;
//            }
//        }
//
//        return $pdf;
//    }

    static protected function _pdf_roll($pdf, $mails, $blockWidth, $blockHeight, $margins)
    {
        /* @var $pdf TcPDF */

        $pdf->AddPage('H',array(100,150), true);
        $rightBlockXPosition = $pdf->getPageWidth(0) - $blockWidth - 2 * $margins;
        $bottomBlockYPosition = $pdf->getPageHeight(0) - $blockHeight - 2 * $margins - 25;
        $pdf->setMargins(0,0,0,true);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);

        $margins -= 4;

        $totalCounter = 0;

        $j = 0;
        /* @var $item InternationalMail */

        $ids = [];
        foreach($mails AS $item)
        {
            array_push($ids, $item->id);

            for($i = 1; $i <= $item->mails_number; $i++)
            {
                $totalCounter++;
                if($totalCounter > 1000)
                    throw new CHttpException(403, Yii::t('internationalMail','Too many labels!'));

                if($j > 0)
                    $pdf->AddPage('H',array(100,150), true);

                $xBase = 2;
                $yBase = 25;



                $pdf = self::_labelBody($pdf, $xBase, $yBase, $margins, $blockWidth, $blockHeight, $item, $i);

                $j++;
            }
        }


        return $pdf;
    }




    static public function _labelBody($pdf, $xBase, $yBase, $margins, $blockWidth, $blockHeight, InternationalMail $item, $i, AddressData $senderData = NULL, AddressData $receiverData = NULL)
    {

        if($senderData !== NULL)
            $item->senderAddressData = $senderData;

        if($receiverData !== NULL)
            $item->receiverAddressData = $receiverData;


        /* @var $pdf TCPDF */

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $yBase = $yBase - 5;
        $xBase = $xBase - 3;



        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->MultiCell($blockWidth, $blockHeight, '', 1, 'J', false, 0, $xBase + $margins, $yBase + $margins, true, 0, false, true, 0);

        //$pdf->Image('@'.$image,$xBase + $margins,$yBase + $margins,30,10);

        $pdf->Image('@'.PDFGenerator::getLogoPl(), $xBase + $margins + 4, $pdf->GetY() + 1, 23,'','','N',false);

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell($blockWidth - 40,10, "#".$item->local_id, 0, 'R', false, 1, $xBase + 40 + $margins - 2, $yBase + $margins + 1, true, 0, false, true, 0);

        $text = $i.'/'.$item->mails_number;
        $pdf->MultiCell($blockWidth - 40,10, $text, 0, 'R', false, 1, $xBase + 20, $yBase + $margins + 1, true, 0, false, true, 0);



        $text = "Grodkowska 40, 48-300 Nysa. Tel. 22 122 12 18";
        $pdf->SetFont('freesans', '', 6);
        $pdf->MultiCell($blockWidth - 40,10, $text, 0, 'R', false, 1, $xBase + 40 + $margins - 2, $yBase + $margins + 5, true, 0, false, true, 0);
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins,$yBase + $margins + 8,$xBase + $blockWidth + $margins, $yBase + $margins + 8);

        $text = $item->senderAddressData->name!=''?$item->senderAddressData->name."\r\n":'';
        $text .= $item->senderAddressData->company!=''? $item->senderAddressData->company."\r\n":'';
        $text .= $item->senderAddressData->address_line_1!=''? $item->senderAddressData->address_line_1."\r\n":'';
        $text .= $item->senderAddressData->address_line_2!=''? $item->senderAddressData->address_line_2."\r\n":'';
        $text .= $item->senderAddressData->zip_code." ".$item->senderAddressData->city."\r\n";
        $text .= $item->senderAddressData->country."\r\n";
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(35,30, $text, 0, 'L', false, 1, $xBase + $margins + 3, $yBase + 12 + $margins, true,
            0,
            false,
            true,
            30,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 8);
        $text = "Odbiorca:";
        $pdf->MultiCell($blockWidth - 15,10, $text, 0, 'L', false, 1, $xBase + $margins + 50, $yBase + 20 + $margins, true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 15);
        $text = $item->receiverAddressData->name!=''?$item->receiverAddressData->name."\r\n":'';
        $text .= $item->receiverAddressData->company!=''? $item->receiverAddressData->company."\r\n":'';
        $text .= $item->receiverAddressData->address_line_1!=''? $item->receiverAddressData->address_line_1."\r\n":'';
        $text .= $item->receiverAddressData->address_line_2!=''? $item->receiverAddressData->address_line_2."\r\n":'';
        $text .= $item->receiverAddressData->zip_code." ".$item->receiverAddressData->city."\r\n";
        $text .= $item->receiverAddressData->country."\r\n";
        $pdf->MultiCell(48,30, $text, 0, 'R', false, 1, $xBase + $margins + 39, $yBase + 23 + $margins,true,
            0,
            false,
            true,
            30,
            'B',
            true
        );
        $pdf->Line($xBase + $margins, $yBase + $margins + 55,$xBase + $blockWidth + $margins, $yBase + $margins + 55);

        $pdf->write1DBarcode($item->local_id, 'C128', $xBase + $margins + 8, $yBase + $margins + 53, $blockWidth - 20, 14,1, $barcodeStyle);




        return $pdf;
    }
}