<?php

$this->breadcrumbs = array(
	InternationalMail::label(2),
	Yii::t('app', 'Index'),
);


?>

<h2><?php echo Yii::t('internationalMail','International Mail');?></h2>


<div style="text-align: center;">
    <?php echo CHtml::link(Yii::t('internationalMail','Utwórz nowy list'),array('/internationalMail/internationalMail/create'), array('class' => 'btn btn-primary btn-lg')); ?>
    <?php echo CHtml::link(Yii::t('internationalMail','Lista Twoich listów'),array('/internationalMail/internationalMail/list'), array('class' => 'btn')); ?>

    <?php echo CHtml::link(Yii::t('internationalMail','Zapytaj o specjalną ofertę'),array('/site/contact'), array('class' => 'btn')); ?>
</div>