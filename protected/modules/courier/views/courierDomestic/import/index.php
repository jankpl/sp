<div id="upload-form">
    <div class="form">
        <?php

        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
            array(
                'enableAjaxValidation' => false,
                'htmlOptions' =>
                    array('enctype' => 'multipart/form-data'),
            )
        );
        ?>

        <h2><?php echo Yii::t('courier','Import packages from file');?></h2>

        <?php
        $this->widget('FlashPrinter');
        ?>

        <?php

        echo $form->errorSummary($model);
        ?>

        <table class="table table-striped" style="margin: 10px auto;">
            <tr>
                <td><?php echo $form->labelEx($model,'file'); ?></td>
                <td><?php echo $form->fileField($model, 'file'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'omitFirst'); ?></td>
                <td><?php echo $form->checkBox($model, 'omitFirst'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'domestic_operator_id'); ?></td>
                <td><?php echo $form->dropDownList($model, 'domestic_operator_id', CHtml::listData(CourierDomesticOperator::getActiveOperators(),'id','courierDomesticOperatorTr.title'), array('prompt' => Yii::t('courierDomestic', 'Wybierz operatora'))); ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><?php echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('class' => 'btn btn-lg btn-primary'));?></td>
            </tr>
        </table>
        <?php
        $this->endWidget();
        ?>

        <div class="info">
            <div class="alert alert-info">
                <?php echo Yii::t('courier', 'Maksymalna liczba wierszy w pliku to: {number1}. Maksymalna liczba paczek w pliku to: {number2}.', array('{number1}' => '<strong>'.F_DomesticImport::MAX_LINES.'</strong>', '{number2}' => '<strong>'.F_DomesticImport::MAX_PACKAGES.'</strong>')); ?>
            </div>
            <p><?php echo Yii::t('courier','Allowed file type:');?> .csv,.xls,.xlsx.</p>
            <br/>
            <p><?php echo Yii::t('courier','The file must contain following columns:');?><br/>
                [package_weight],
                [package_size_l],
                [package_size_w],
                [package_size_d],
                [packages_number],
                [package_value],
                [package_content],
                [cod],
                [sender_name],
                [sender_company],
                [sender_zip_code],
                [sender_city],
                [sender_address_line_1],
                [sender_address_line_2],
                [sender_tel],
                [sender_email],
                [receiver_name],
                [receiver_company],
                [receiver_zip_code],
                [receiver_city],
                [receiver_address_line_1],
                [receiver_address_line_2],
                [receiver_tel],
                [receiver_email],
                [cod_bank_account],
                [ref],
                [options]
            </p>
            <br/>
            <?php echo CHtml::link(Yii::t('courier','Download file template'), Yii::app()->baseUrl.'/misc/ipff_template_domestic.xlsx', array('target' => '_blank', 'class' => 'btn btn-success', 'download' => 'ipff_template_domestic.xlsx'));?>
            <br/>
            <br/>
            <div class="alert alert-info">
                <?php echo Yii::t('courier', 'W {a}ustawieniach profilu{/a} istnieje możliwość wyłączenia powiadomień SMS oraz email.', array('{a}' => '<a href="'.Yii::app()->createUrl('/user/update').'#block-notify-form" target="_blank">', '{/a}' => '</a>')); ?>
            </div>
        </div>
    </div>
</div>
