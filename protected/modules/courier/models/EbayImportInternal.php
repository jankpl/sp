<?php

class EbayImportInternal extends CFormModel
{
    CONST MAX_IMPORT_NUMBER = 250;

    public $with_pickup;

    public function rules() {
        return array(
            ['with_pickup', 'in', 'allowEmpty' => false, 'range' => array_keys(CourierTypeInternal::getWithPickupList(true,true)), 'message' => Yii::t('courier', 'Twoje konto nie ma dostępu do tego typu nadania.')],
        );
    }

    public function attributeLabels() {
        return array(
            'with_pickup' => Yii::t('app', 'With Pickup'),
        );
    }
}