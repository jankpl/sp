<?php

class AnPostClient extends GlobalOperator
{

    const CUSTOMER_NUMBER = '3732995';
    const SITE_CODE = '800015';
    const DELIMITER = '+';

    const FTP = 'ftp.anpost.ie';
    const FTP_USER = 'tt_monlog';
    const FTP_PASSWORD = 'Dn8k5V:5dr2D';
    const FTP_DIR = '.';

    const TYPE_PRIORITY = 'PRI'; // INT
    const TYPE_EXPRESS = 'EXP'; // ROI ONLY
    const TYPE_PXP = 'PXP'; // WORKS AS SATURDAY DELIVERY // ROI ONLY
    const TYPE_EXPRESS_INT = 'EINT';

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $weight;
    public $value;
    public $content;

    public $saturdayDelivery = false; // WITH PXP

    public $intExpress = false;

    public $local_id;
//
//    public static function test()
//    {
//        $model = new self;
//
//        $model->sender_name = 'Jan Kowalski';
//        $model->sender_address1 = 'Ul. Testowa 1/2';
//        $model->sender_zip_code = '00-001';
//        $model->sender_city = 'Warszawa';
//        $model->sender_country = 'Poland';
//
//
//        $model->content = 'Test 123';
//        $model->value = '123';
//
//
//        $model->weight = 1;
////
//        $model->receiver_name = 'Jan Kowalski';
//        $model->receiver_address1 = 'Test Str. 1/2';
//        $model->receiver_zip_code = '00-12';
//        $model->receiver_city = 'Oslo';
//        $model->receiver_country = 'Norway';
//
//        $model->_generateLabel('123123', self::TYPE_EXPRESS);
//
//    }

    public static function getAdditions(Courier $courier)
    {

        $is_receiver_ie = $courier->receiverAddressData->country_id == CountryList::COUNTRY_IE ? 1 : 0;
        $weightBelow2 = $courier->weight <= 2 ? true : false;
        $allowedCountry = in_array(strtoupper($courier->receiverAddressData->country0->code), [
                'AT',
                'AU',
                'BE',
                'CA',
                'CH',
                'DE',
                'DK',
                'EE',
                'ES',
                'FI',
                'FR',
                'GB',
                'HR',
                'HU',
                'IS',
                'LT',
                'LU',
                'LV',
                'MT',
                'NL',
                'NZ',
                'PL',
                'PT',
                'SE',
                'SG',
                'SK',
                'US',
                'XA',
                'CS',
                'CW'
            ]
        );

        $CACHE_NAME = 'ANPOST_ADDITIONS_'.$is_receiver_ie.'_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

//            $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_NOTIFY_SMS;
//            $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_NOTIFY_EMAIL;

            if($is_receiver_ie)
                $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_SATURDAY_PREMIUM;
            else {
                if($weightBelow2 && $allowedCountry)
                    $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_INT_EXPRESS;
            }



            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }



    protected static function clearData($text)
    {
        $text = str_replace('+', '', $text);
        return $text;
    }

    public static function runManifestQueque(array $products)
    {

        $fileName = 'ISF'.self::CUSTOMER_NUMBER.date('ymd').'.001';

        $no = S_Useful::sizeof($products);
        $content = self::SITE_CODE.self::DELIMITER.$no.self::DELIMITER.date('YmdHis')."\r\n";

        /* @var $products UniversalProduct[] */
        foreach($products AS $product) {

            $notification = '3'; // always 3

            $item = [
                '01', // type
                substr($product->remote_id,0,2), // product code
                substr($product->remote_id,2,9), // item number
                'IE', // country code
                self::CUSTOMER_NUMBER, // customer id
                '', // customer batch no
                round($product->weight, 2), // weight
                strtoupper($product->toAddressData->country0->code), // iso country code
                '', // zone
                '', // item price
                '', // value of contents
                $product->local_id, // customer item id
                strtoupper($product->content), // desc of contents
                strtoupper($product->toAddressData->name), // contact name
                strtoupper($product->toAddressData->company), // company name
                strtoupper(trim($product->toAddressData->address_line_1 . ' ' . $product->toAddressData->address_line_2)), // address_1
                '', // address_2
                '', // address_3
                '', // address_4
                strtoupper($product->toAddressData->zip_code), // postcode
                strtoupper($product->toAddressData->country0->name), // country name
                $product->toAddressData->tel, // tel no
                '', // sort code
                '', // route code
                $product->toAddressData->tel, // mobile no
                strtoupper($product->toAddressData->fetchEmailAddress(true)), // email
                $notification, // notification flag
                'N', // saturday premium
                'N', // cod
                'I', // pallet
                '', // collection point
                '', // ship method
                '', // routing code
                '', // manifest pickup date
                strtoupper(trim($product->fromAddressData->name . ' ' . $product->fromAddressData->company)), // sender
                strtoupper(trim($product->fromAddressData->address_line_1 . ' ' . $product->fromAddressData->address_line_2)), // sender address 1
                '', // sender address 2
                '', // sender address 3
                '', // sender address 4
                '', // product name
                '', // return indicator
                '', // parcel orgin
                '', // delivery instructions
            ];

            $content .= implode(self::DELIMITER, $item) . "\r\n";
        }

        MyDump::dump('anpost_post.txt', print_r($fileName.' : '.$content,1));

        $temp = tmpfile();
        fwrite($temp, $content);

        $path = stream_get_meta_data($temp)['uri'];

        $conn_id = ftp_connect(self::FTP);
        $login_result = ftp_login($conn_id, self::FTP_USER, self::FTP_PASSWORD);

        $result = false;
        if ($login_result && ftp_put($conn_id, $fileName, $path, FTP_ASCII)) {
            $result = true;
        }

        ftp_close($conn_id);
        fclose($temp);

        MyDump::dump('anpost_post.txt', $result ? 'OK!' : 'FAIL!');

//        return $content;

        return true;

    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, CourierLabelNew $cln, $returnError = false)
    {
        $model = new self;

        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model->weight = $courierTypeU->courier->getWeight(true);
        $model->value = $courierTypeU->courier->getPackageValueConverted('EUR');
        $model->content = $courierTypeU->courier->package_content;

        $model->local_id = $courierTypeU->courier->local_id;

        $model->sender_name = trim($from->name.' '.$from->company);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = strtoupper($from->country0->code);
        $model->sender_country_id = $from->country_id;
        $model->sender_email = $from->fetchEmailAddress(true);

        $model->receiver_name = trim($to->name.' '.$to->company);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_country_id = $to->country_id;
        $model->receiver_email = $to->fetchEmailAddress(true);

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_SATURDAY_PREMIUM))
            $model->saturdayDelivery = true;

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_INT_EXPRESS))
            $model->intExpress = true;

        return $model->_go(ManifestQueque::TYPE_COURIER_LABEL, $cln->id, $cln->courier->local_id);
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;

        $model->weight = $courierInternal->courier->getWeight(true);
        $model->value = $courierInternal->courier->getPackageValueConverted('EUR');
        $model->content = $courierInternal->courier->package_content;
        $model->local_id = $courierInternal->courier->local_id;

        $model->sender_name = trim($from->name.' '.$from->company);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = strtoupper($from->country0->code);
        $model->sender_country_id = $from->country_id;
        $model->sender_email = $from->fetchEmailAddress(true);

        $model->receiver_name = trim($to->name.' '.$to->company);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_country_id = $to->country_id;
        $model->receiver_email = $to->fetchEmailAddress(true);

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_SATURDAY_PREMIUM))
            $model->saturdayDelivery = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_INT_EXPRESS))
            $model->intExpress = true;

        return $model->_go(ManifestQueque::TYPE_COURIER_LABEL, $courierLabelNew->id, $courierLabelNew->courier->local_id);
    }

    public static function orderForPostal(Postal $postal)
    {
        $to = $postal->receiverAddressData;
        $from = $postal->senderAddressData;

        $model = new self;

        $model->weight = $postal->weight / 1000;
        $model->value = $postal->getValueConverted('EUR');
        $model->content = $postal->content;
        $model->local_id = $postal->local_id;

        $model->sender_name = trim($from->name.' '.$from->company);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = strtoupper($from->country0->code);
        $model->sender_country_id = $from->country_id;
        $model->sender_email = $from->fetchEmailAddress(true);

        $model->receiver_name = trim($to->name.' '.$to->company);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_country_id = $to->country_id;
        $model->receiver_email = $to->fetchEmailAddress(true);


        return $model->_go(ManifestQueque::TYPE_POSTAL_LABEL, $postal->postal_label_id, $postal->local_id);
    }

    protected function _go($type, $type_id, $local_id)
    {

        if($this->sender_country_id == $this->receiver_country_id && $this->receiver_country_id == CountryList::COUNTRY_IE)
            $service = self::TYPE_EXPRESS;
        else
            $service = self::TYPE_PRIORITY;

        if($this->saturdayDelivery)
            $service = self::TYPE_PXP;
        else if($this->intExpress)
            $service = self::TYPE_EXPRESS_INT;

        $prefix = false;
        if($service == self::TYPE_EXPRESS OR $service == self::TYPE_PXP) {
            $prefix = 'CE';
            $range = NumbersStorehouse::ANPOST_CE;
        }
        else if($service == self::TYPE_PRIORITY) {
            $prefix = 'CA';
            $range = NumbersStorehouse::ANPOST_CA;
        }
        else if($service == self::TYPE_EXPRESS_INT) {
            $prefix = 'LX';
            $range = NumbersStorehouse::ANPOST_LX;


            if($this->weight > 2 OR !in_array($this->receiver_country, [
                        'AT',
                        'AU',
                        'BE',
                        'CA',
                        'CH',
                        'DE',
                        'DK',
                        'EE',
                        'ES',
                        'FI',
                        'FR',
                        'GB',
                        'HR',
                        'HU',
                        'IS',
                        'LT',
                        'LU',
                        'LV',
                        'MT',
                        'NL',
                        'NZ',
                        'PL',
                        'PT',
                        'SE',
                        'SG',
                        'SK',
                        'US',
                        'XA',
                        'CS',
                        'CW'
                    ]
                ))
                return GlobalOperatorResponse::createErrorResponse('Service unavailable for provided data!');
        }



        $trackId =  NumbersStorehouse::_getNumber($range, true);
        $trackId .= NumbersStorehouse::calculateControlDigitS10($trackId);
        $trackId = $prefix.$trackId.'IE';

        $label = $this->_generateLabel($trackId, $service);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($label);
        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
//        $im->trimImage(0);
        $im->stripImage();

        $return = [];
        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $trackId);

        ManifestQueque::addToQueque($type, $type_id, GLOBAL_BROKERS::BROKER_ANPOST, $local_id);
        return $return;

    }

    protected function _generateLabel($trackId, $service)
    {
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pageW = 100;
        $pageH = 140;

        $border = 0;

        /* @var $pdf TCPDF */

        $pdf->AddPage('H', array($pageW, $pageH), true);

        $pdf->SetLineWidth(0.2);
        $pdf->MultiCell(98, $pageH - 1, '', 1, 'L', false,1, 1, 0.5);
        $pdf->SetLineWidth(0.1);


        $pdf->Image('@' . self::_getStamp(), 2, 2, 40, '', '', 'N', false);

        $text3 = 'SIGNATURE REQUIRED';
        if($service == self::TYPE_PRIORITY) {
            $text = 'PRI';
            $text2 = 'PRIORITY PARCEL POST';
        }
        else if($service == self::TYPE_EXPRESS)
        {
            $text = 'EXP';
            $text2 = 'EXPRES WITH SIGNATURE'."\r\n".'PARCEL POST';
        }
        else if($service == self::TYPE_PXP)
        {
            $text = 'PXP';
            $text2 = 'EXPRES WITH SIGNATURE'."\r\n".'PRISM SERVICE';
        }



        if($service == self::TYPE_EXPRESS_INT)
        {
            $text = false;
            $text2 = 'EXPRES INTERNATIONAL'."\r\n".'PACKET SERVICE';
            $text3 = 'NO SIGNATURE REQUIRED';

            $pdf->SetFont('freesans', '', 7);
            $pdf->MultiCell(40, 7, $text2, 0, 'C', false, 1, 43, 4,
                true,
                0,
                false,
                true,
                7,
                'M',
                true
            );
            $pdf->MultiCell(40, 6, $text3, 0, 'C', false, 1, 43, 13,
                true,
                0,
                false,
                true,
                6,
                'T',
                true
            );

            $pdf->Image('@' . self::_getPrimeIco(), 85, 2, '', 15, '', 'N', false);

        } else {

            $pdf->SetFont('freesans', 'B', 25);
            $pdf->MultiCell(40, 9, $text, 0, 'C', false, 1, 50, 1,
                true,
                0,
                false,
                true,
                9,
                'T',
                true
            );

            $pdf->SetFont('freesans', '', 7);
            $pdf->MultiCell(40, 7, $text2, 0, 'C', false, 1, 50, 9,
                true,
                0,
                false,
                true,
                7,
                'M',
                true
            );
            $pdf->MultiCell(40, 6, $text3, 0, 'C', false, 1, 50, 16,
                true,
                0,
                false,
                true,
                6,
                'T',
                true
            );

        }

        $pdf->SetLineWidth(0.1);
        $pdf->Line(1, 20, 99, 20);


        $pdf->Image('@' . self::_getFrom(), 2, 25, 3, '', '', 'N', false);


        $text = '';
        $text .= $this->sender_name ? $this->sender_name."\r\n" : '';
        $text .= $this->sender_company ? $this->sender_company."\r\n" : '';
        $text .= $this->sender_address1 ? $this->sender_address1."\r\n" : '';
        $text .= $this->sender_address2 ? $this->sender_address2."\r\n" : '';
        $text .= $this->sender_zip_code.' '.$this->sender_city."\r\n";
        $text .= $this->sender_country;

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(50, 18, $text, 0, 'L', false, 1, 10, 22,
            true,
            0,
            false,
            true,
            18,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 6);
        $text = 'Tel: '.$this->sender_tel."\r\n".'Cust. Acc.: '.self::CUSTOMER_NUMBER;
        $pdf->MultiCell(27, 18, $text, 0, 'L', false, 1, 70, 22,
            true,
            0,
            false,
            true,
            18,
            'T',
            true
        );


        $pdf->SetLineWidth(0.1);
        $pdf->Line(1, 41, 99, 41);
        $pdf->Line(10, 41, 10, 80);



        $text = 'To';
        $pdf->SetFont('freesans', 'B', 14);
        $pdf->MultiCell(53, 10, $text, 0, 'L', false, 1, 1, 50,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        $pdf->SetLineWidth(0.1);
        $pdf->Line(1, 80, 99, 80);


        $text = '';
        $text .= $this->receiver_name ? $this->receiver_name."\r\n" : '';
        $text .= $this->receiver_company ? $this->receiver_company."\r\n" : '';
        $text .= $this->receiver_address1 ? $this->receiver_address1."\r\n" : '';
        $text .= $this->receiver_address2 ? $this->receiver_address2."\r\n" : '';
        $text .= $this->receiver_zip_code.' '.$this->receiver_city."\r\n";
        $text .= $this->receiver_country;

        $pdf->SetFont('freesans', '', 16);
        $pdf->MultiCell(55, 35, $text, 0, 'L', false, 1, 15, 45,
            true,
            0,
            false,
            true,
            35,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 8);
        $text = 'Tel: '.$this->receiver_tel;
        $pdf->MultiCell(27, 18, $text, 0, 'L', false, 1, 70, 45,
            true,
            0,
            false,
            true,
            18,
            'T',
            true
        );

        $text = 'Weight: '.$this->weight.' kg';
        $pdf->SetFont('freesans', 'B', 10);
        $pdf->MultiCell(30, 6, $text, 0, 'L', false, 1, 1, 82,
            true,
            0,
            false,
            true,
            6,
            'M',
            true
        );
        if($service != self::TYPE_EXPRESS_INT)
            $pdf->Image('@' . self::_getSignatureIco(), 77, 81, '', 8, '', 'N', false);
        $pdf->Image('@' . self::_getScanIco(), 87, 81, '', 8, '', 'N', false);

        $pdf->SetLineWidth(0.1);
        $pdf->Line(1, 90, 99, 90);

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $pdf->write1DBarcode($trackId, 'C128', 10, 100, 90, 30, 0.4, $barcodeStyle, 'N');

        $pdf->SetLineWidth(0.1);
        $pdf->Line(90.1, 41, 143.8, 41);
        $pdf->SetLineWidth(0.1);

        $text = 'Ref: '.$this->content;
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(100, 6, $text, 0, 'L', false, 1, 1, 135,
            true,
            0,
            false,
            true,
            6,
            'M',
            true
        );

        return $pdf->Output('AP.pdf', 'S');
    }

    public static function _getStamp()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAALUAAABICAMAAACHgURcAAAA4VBMVEUAAAD///8FBQX9/f36+vr8/Pz4+Pj09PQwMDDx8fHv7+/39/f29vYJCQnDw8MRERFoaGjs7OwtLS3z8/Pq6uqenp5qamplZWUjIyPb29tcXFwfHx8zMzMLCwvn5+dvb29iYmJOTk4qKiqioqJLS0tFRUUXFxcNDQ3Y2NjNzc2MjIw8PDwoKCiwsLBsbGxSUlIbGxvIyMinp6dfX19AQEC4uLirq6uCgoLf39+9vb2Xl5d9fX12dnZycnJVVVWQkJA3Nzfi4uJHR0fV1dXAwMCIiIh6enpYWFiamprR0dGUlJQ11imcAAAH3UlEQVRo3u2aZ3vaQAzH/fedz5iNgTDDCHsnQMgme7Tf/wNVOkNDIKGQvmh5HpQFZ/n0O1nSyQ5GxNw1MYy6YRo7J+YOMjP2LvraODR2U3bR17sZIXvqL2VPvadeL3vqPfV62VPvqdfLnnpj6v+vyfwG9XH3rjrNjrKDaqcb+X123TeXvM/t1kOf2cp1BtnRqOY2lsYjaTefz/uWJO+69JPO/SU12y2/nUMEpLCkJYUA/MmyN20JAvQdoB9pS9yfpZfs5KIZSfoQccRuHxYOdXoOlMC7WPSFOFgErOim1IfmOLKqGbk7ywCPp2c3r2N2sntXLZ317mG3SdmMSghhQ0IpALbfwlN9PnUoZJi3foGYDRbHRuzp6HB2bOJoPMvCTAJELRlcKiGl7G/s6248kczW3y+/eVy7msCKPV27oaXLG0oPs/z34LdV8rUALDuuxsY8UB4KMUgwiKU8dHXjzX5B7hSKHSxmwh6W+rcAycHmEfLca7JTMk+TSuWilQBE7P7nNGJ8LSlh21ABJaDAFrWMZzamSiIGWy+M6c/5d5tNv+gBP4O/iwPCFsrj3jRCZiHcP0uenvxoZk5OK+0ihcx6ic5AbT8sqR2uyHpXH6vSsAcTz2Qc0JEgfcm2YaQLNMiLKbROwjO5gM20yn9yQWMno42pQ8bWciBkXEpLXU7Hg+eW52+Flo43v8NpKiErHdOsR2NKggNXdYwHVrNFIHz0PlNV6VxUtw1KMKOxZeUzv3wbqR+nj9z0cSP0PlhiEsBK8Zv8kD1L1u0UJd2TZM/RmzPvguUyhCWARNMozy7QZMFQndVBS1zFWKTervTdnBQATh12Zuus5ptFiHauRFFfqW6Pg9ISeDSMmg2bMGxrODc2sIUFOqjGRQfCBkR4Yf0uh5MKisv1u8ymIRKpPT8C8qTXvr4p9qPRq/blaQLivqyzkWsr5Dzl80AAILiqcVqg1xyox8ZcJloVwbeo4tPoV7LLMPr7yPLqR3JLatMt9Mppc/FBYH08olJin1y5S2fni/cvdXrZlx+oI2FL4+AqlwARE8bp+3klvYn4UehDwbK40DUHh96U36RmmxdNB/HWz+jrdFAdRNsXBTL8mCyveb62RG1cW9AbTo9DV28hxYWmAMoL3lQTNis6toRop7/t65mEBjfDp3P2iBSBwo/T22nucz19VVcjxBhz7FuwHw+E4vgQVm3BahBC7znRV50OyiuNimvz96lnx0Km2ThOLz8uNldzm1RSS772eaadWBuCoaX0LahnaECIAK6MIRATxA+JgkL729QMGVmiWtYwV8CLS76+k3og6CQ1gxRILzQ6LX1MEHX3J0gsodcmE2fbx/XqoLni1HSNWqam34kVnET4uVR2Zyop+yN1VWrTsvlMTpWKkDoLeXMuwCKpnB8eZKC0spQJ+N3vUn/p5+512CG0TOUlmooepIpvTSmDIvyg98YlX2e9nTrQywp4hKPFjUSAtWVWGyuHJR3n0Hbk6fepc63b/FLjkc4mM8CPdjkXWoz8SO0soKe/0YYVUrNpw5bUbdGbT0HFeUkLu0ZHNyIJJObGyxkmpTEbZpri3Q+opLlVXJMvmpD+i0n7YFAtD0al6+RF04Y9rNY/m6gR0REiglzCRNS7UneETFhKjMxMDLBlEJm5NW62wb2G9MB4PB1WUoKg/bWInyPeQVjrb0qt07Hx8JJJcDcgdQU4r2SPcua6OOrrgJCi6K2kEofeXJyG0SffkYjA1Xz2owIQUzqcjs33iILFKfraUAjyFiTzbGFT6oV7AZfu3u58R97U67usoo4Q4JrChrsSIW1CwyVRngs+YCPhmwXbRBNC/ggZT6d33gwdXqQFB3kz4derFBWevPGX2bieO8XUliwc8Gp7kGy5gARv9gN6JQRH+ZVJ9sZDwGbfq6lhPkO1bh8easWMFee1OH7TuFS65xO47NCa11BvTm7mstc/h8PJaXgyvHwpjSOziaOIWwLk3vAkIxGIaXdSjWD5GZvdvTrnrcmjH6wnoH15DRskfk5dh1dGg3dehVc2rHunvAX14afYZv6FmjzJt2Z0l9FMcGW7/5HKs26fUYJx21IgRkc7Xl3piYzQWZAo4lRRoK++AyakxrVuZIOAAg0mJJQSKHRosgsB2AIJDjF3c2r3qrZaKrLJcwv3lw++nPk78Lu19iPedDZaynMhBEPo2/XibydEbRq2EQiQ0x2v6WjrCjolZSEZj1Mw4CVzR3obqwhChjbPRjeO2MVtvxPRKX/cGdxUmkHEhqP0J2u865qz7kknpNQ7tBRodRaUfBUelkTicNeNk1dvfFrgGKYV8zrRGnnNGG2XLLTU823iOl27bXL3CKGo1AZ4jyu6uXVJmtLMDqMJQpa9UeNDejR8zyeExw6F6lV/T1UtnTtgUWi9uPNT0hVH+nnbCR9uSh2apV2+mi2VUqlStupG/lhVokIxMZKlt8plv9Y1VzPj8Kj2WsqWB/nQB0uH6XH5dZSdHkX4lPlZuWrxOloq57fY0Xl8Sykpjk+Fg+9+HGLV/LY93+H2dopgCSC6bNX8I6H59QDjbfUUZwvHs0I/KDnX1MEanPWzmf/g+bV7c31TShVvfCsI5hZcqw77X566f0Ayd+V/Bbv5H4499U7JnvpL2VPvqdfLnnpPvV721Hvq9bKn3tlPX+/up91D5q6JYeZ+AXzolfplNc5cAAAAAElFTkSuQmCC';

        return base64_decode($data);
    }

    public static function _getFrom()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAABAAAAA2CAMAAAD52Ol4AAAA51BMVEX///8BAQH8/Pz5+fn29vbv7+8HBwcLCwv7+/v09PQfHx8TExNGRkbx8fFBQUFSUlKmpqY+Pj4bGxsXFxeMjIyCgoJ6enpsbGzr6+vU1NTR0dG5ubmPj4/m5ubOzs6VlZWIiIhmZmZLS0s1NTUjIyPz8/Pf39/Y2NjX19esrKyoqKijo6Oenp6FhYVpaWlfX19OTk46Ojrh4eHc3NzIyMivr6+ampp+fn52dnZwcHBWVlYxMTHDw8O2tra1tbWRkZFJSUktLS0rKysnJye/v7+9vb2ysrKZmZlkZGRiYmJbW1vJycnHx8e3muFHAAACaUlEQVQoz0VSh3LbUAwD39DeyzveTh2vDMcre4/2/7+nfE7vCp3EI04iCOgBqqOkdEMdjlWoBPBre5EO04ARRWkMoHlORJafZbnvj4YQOIm5z6J+b9a6/XMKoPMRX3i+7d+siuTWdQGIzknSWw0z27ZHe+lCMQUh6+v2ICcPISQgnbDTuFsEOeUQ3E7ePlYvvkXkpz3NstUL69peUL5d1gV4sZjbqOrOkmQ+bTkOmgEZWJbtZ9aNA/w+547+4UkA+rosd4uityjaRbnRzKjQdSBcPXagJACHuUZSVtV28yoUBFzUp+c/Qy5KKH5Ddn0em3v8zOeCbdw/EQXFXat7sGkACV0jv6xDKcjuy7PmgAKKgDHrA7v8zMGdR6WJQArIjb0FWhn1AGb46loPMJkOpYYwC1TWPRAuiJZXgm3qXuYZ/+83RINd0d5+2xQbQp4xQ7bND+8aAmwsiT3LtHEbDB6ITqtb7cukoeDCwIFUgCuBOt/Q08Wh9jAJX5OiffxmYx2tD0x5FiH0kuxVLeIuf8qWIXB6oKCDJlF/fTmZQKER0CegiYrwx9JVSjXWIYuX0tIEHNGOK9H0aBhoPtLgdnpGVL3ft1o/Z8yORhlvPhp5gQDWMf1HBpatovSRz2WUpkFaC4/upKugtCND1CEBPXFgQjYSCpzpwVsKpddNIaUxjMYFL4Yz8k7AMIsNaK8McQnXVRK4iqmP8ZyeTwWO+PVIFcdKo1OwGUNEPGN8TfklBJQ4EtHsYU9+ezafJeAZ5+TnlmX+jO8P4aDBXo6tKZ4UqM/7ta/+atn//vyqFUbJ1YKLyUrUXeAvMOI1Z4uvdJcAAAAASUVORK5CYII=';

        return base64_decode($data);
    }

    public static function _getSignatureIco()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAMAAACfWMssAAABC1BMVEX///8gIB79/f37+/shISEjIiD5+fkaGRgeHR0cHBomJST39/cXFhb19fUrKyv09PMnJyYjIyMUFBOXl5cSERAuLi0PDw8xMTAqKShUVFPy8vK3t7Y3NjYoKCiAgH/v7+5QUE8NDQzr6+vg4OBxcG/U09K6urqrq6qjo6JgYF8/Pj45OTnt7ezc3NvIyMezs7KEg4N2dXVIR0bk5OTj4uLZ2djExMOwr6/m5ubLy8rBwcGUlJOOjo6Kiopra2pnZ2dcXFtOTU00MzPp6ejX19fR0NC+vbyHh4d8fHt6eXljY2JLSkre3t3Ozs6mpqagoJ+cnJySkZB0c3NFRURDQ0KZmZlubWwFBQRZWVkL82LbAAAFFElEQVRIx+WWZ3PbMAyGQYni0pYlecR7xCt2bGfv0ey0zez4/7+kYJz0mvai3PVLPxSW6RPlB8ALUKLgfzLDAMPE0dj8vAAmMBNPcRZ/9bW3sTnLDFjbj4YLL9NPnkyE3za8Zuuxey0kvWr5GAuYO3dqGhkgUvi31uQ4osciPC8BMD2pk8w2V6dbWwxlnqRSJIuunmFImjrPjFyZiaB9MqUNGgYhn22D4eP0vDyZZts6q0/FNElSXonvT56S9A1EM7En5zhuHdKI5mMh+ltQ+rR42tSJmub73YRmXynCqUiL6/ZGGExPgDFk8ciMbTKjmXDPk56k+6PSHq0crgPUEMoGdRP87XKh4QQ0VTvuUpFGxWUbTAPzzSyur4W2eT7gMpTDHmwXY2eQKyHyDgg2Cto84J7klTA9+gS5RIrKxYd3qCcV6xvQeYwkFaEMr5bcPcmdsL1moPxMcPVyGKywbl4qKgpETmCzSDzh3I8BMsHeAGs6axrVkAYxLYj8ZxjPHCoaez78sfR0/nrQ3U+UIyzRNja/CgsDEmuwDr08sWS5CtAE+1fMZO72tHp/eTG9uLIswi3lPN7AWT1yLEsJ0f7gVy1Hho8r9uuIqNnORYoTxaUVcSKtisMHHbZxHClLWJycw8dDSdBDC7nXhWS5VDgIcqWEIlIRh/dX7YkniLCSqNCFs5lTkfHnP+pa2uHEUgq/hCjCSWBxvlta2xeO4qrgzDpGzwuFvF5jv5XG33EEipPKwmwtKS0lo2EXxkXOCw2l4vYS7EXKiy/t15miRsdSliS6GJrEupKoPmYbD1ESSxUHd/7msMIjulx6RbJSzpEWGi4zJeZwgccHWM4Kr1iqEJdvzConXnw0fh3SzTnkN9N+vjK3TzwnCrxG/XQtLMSc7/nYcSMTJJYYdGudcuQpzJb0mzsiTyukVwOGrP02iDr50SKsl7HgshJ762t1Lik/2tIRTfYmqASxHHF1Zo+nFqI87JtbQ5kmdLKKhJGlEQ8eXN/VPvTq8lshLKxCL5GEl28ZuObbIPZU6Jbm28tGp3pMMGTTvk8imh52MNkMEFUiKWI12zn9sDArkLQHJ9855Um1lVVVwQXGI7j6Ymu6vDSqekouwsYj99L88nNI5pduI/psglLpOJJ6D/WDIhX6nDvl3VZpGuTLI8h5Iaf1GmjQwONL4ydIqPLKs+LktOZ3LsucCkmJUygu+xMaTVrNCfoqfDTZfJEv5YIXq1S8wcHNCMC3wVxZrnPPo4GIiHe20s07OejUJX1Y/XmDfNxYeLHxKtPLF8xS96G+8XG9msiExmEqdvwbWr5YbVXPt21gGvv9vja1dB8WrhWPjs+XIDekYThM1SkrNkTb8F0A15hvRcx2X6FoAN08z4cBRXUn00Ij5nITroWK7hgGsv+Mp6M9pcqaF41vkTwKB7etlW6f0kO2WBY8KG7Z/vOaYyZjYLyYac5Zm43PD/ZzbS8N+ovNT192Wyv1fJpE5ZwJLhiZW3ttrWX3klQ2ih2UBbsipNjVfdM12DvvSyUfRm0qB7sjF0XclZVDJC+O8GItA3xeyaPbvYUVlOJC6fLY4x7HTcHO3Lie3qdQPas9PXrxYy7ue2noXenHR9Z2Z8z3FBwxnoEtwG91KBpHtpEBspdXP3vufgkMWxds+8DJn7kZxdFN0TKZDqtJv4ZODPTwZfAVzzKrioP++DprGzE9qZXuuqW/ea1lts0M+BtSq/gn9gN3Tno25EO2fgAAAABJRU5ErkJggg==';

        return base64_decode($data);
    }

    public static function _getScanIco()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAAEAAAAA4CAMAAAB6xg5AAAAA/1BMVEX////8+/v//v79/f0kJCQcHBz5+PgZGBggICAjIiIWFRb6+voaGhr39vceHh47Ojp/fn9qaWrZ2Nnz8/Pw8PBfXl/n5uZ9fHytq6xycnInJib19fUrKyvd3d0pJyjv7u7S0tIUExTy8fI+Pj4REBHt7O3KysqmpaZRUFHNzc2ura5ubm5cW1zq6uri4uKpqalEQkM2Nja5ubl0c3MyMTIvLy8tLS3o5+jW1tbCwsK7u7uioqKbm5uQj496enpWVlZHR0dNTU3Pz8+3treysbKSkpKIiIhjY2NJSUnl5eXf39/Gxsa+vr6dnZ2Dg4MNDQ29vb2Xl5eKiop1dXUGBgZhv2cOAAAFF0lEQVRIx+2XZ3faMBiFtWxLsi0gJiSx2XuPTAg7ezRJ1///LX3l0BJO0wA9/dgL+OiA9OjqvpZ8QP+1KoIwQi5Fbjtx1m6n2imt9n2iVfLgB4oRtgmipmmSRfffZdpwaQ2PYtVqs3BXKMzuqs3b29topdzOeF6GImQv+tn2e8MRgi7e8NSQUviSKy3DcJJpoVih+a1y4WLTNj2sLdj4XQDMULpWJ5afzDnSMLgQgnPpKM65YMIvHJybP+2TdwiIUg+hbCGtIo0vw2Gl8vTla/RbYzrrMcNxnKTPHScSPXcRCqAffSdFijC896Tl36CzVikTz+fzmVJ7p/a5M240mSUFU35v8om8N3wRkInsQVepRvwn1H417bX75c5k7ucslqtW7jG2360CDoMYpWX6GbsYY6JFoXeYGfXua5WD2WmRH10QmO9Pwug5xyIVXVEo+xuzWJfZLH05lSLWRh8ofjUvFpsorxcENn7506YJcb09//v0Ik//DKDus2THn8Cknh8vV+iarobeGk738wcGYL2VHrPmKBO2lw4CFN7stZny7w7B4AfyxjknUiZgIQQshYOdei/5Im+8D0abOI8HTLJmOOEy7ADAqc5UFp10o4888wMENeNffUOUAbBi4LERMWRapPfu0UfCFJl0kFPiIIOoDnIh0o9Zikln2gmQ7aKloG1TRHULQzddO0RT10XG9y8rT5NoY4gJwYTaO43vp2K38xAQ7RL/npz+1rYXN/VnJq1qN907TibnFZcAAyWuZfHkUk+IVzeDa1K9iRaJ6SvxUCsmLYc7nBdzRvMVENSPi8mncPZVA3Y4mgAY4IQullWrpiXrGdxPG70SoRR+qO0qaFOgIWK+DV1/iMaSVwYh2sZgPL6ZAIQ57NHEFIbgkaFkyw4B9tttbGZcNx/kAY1dAARB3sZxQs4I+dS14FBj13lC9E59FEKNYW7ArWyF2qjSeersP2QGo2ENpu4MH1Gms/9UNzs9A8Ykd+MakCH3MWWoBABWS9CKcsjKiNRTk6J1E8/sSvkN1SzupONXXQGl58cJvSzQJ8u3LgKk28sQKD5gBjOMyH4iZomoa+5acg/vONwQ+auCxZgh/HOX6huEZJM5NmnBGHitB3DtwDGUAQsPwrONxJ8ddrITLgGvB4h0a7/rOMpgzjRDzNBz2VIvl9jWyW/gwC/Vu8pnPM1OUjp2eD9EhHOQggQ2WIJQvncVKSrLUml+CZXT2XnXjPv6XLfXA5gSmXqkmJNKGsZzibwe2ymuihXPRXgDB5y3yjdHjaMJJFl90ABiIm/OrFg2QHh9iIoJyJ4QgsdJcZrFyNMb3Rwxi9cxIpuUkcVpaLzMLXmoDRBKMS7Il6+Q4eYAgkp3Pp+kNEEjqr6clhGmmzug3q1k1QWA0C+FF+Ma2hsDYKp6kolxdqHBlPNmDdFtAKWZyhWSoFNQIZdTcm+LDCh27Vtm5QwtpeWLlwbyNgZgfcQ2uskT0DEoeXLcm15u4QDrE7J/OTjUOgcdHl7t4MxbwtEroH4Ws1jUtQEQJQBQAEBr9G8AeBUQLmFrgNAZrACsjQHkFeAsQgxCAN0KcMCcFYDcDgBlFI5wloCmzO3R/t8B5E8A3gKASqPqXXU2b56fjQqzYTy4KdyN8M58Vt1t64f+WtkoUc72y7V+3nwoZ+EOS11kYe+WaxePukTrAR4wwo7YXoQKTaw3oYmIuQFBP6up6+lBiHomXFw7/FeCkbtu+A+jH5XYMLzVtwAAAABJRU5ErkJggg==';

        return base64_decode($data);
    }

    public static function _getPrimeIco()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAAI0AAADXCAAAAAACII3oAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFwmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDIgNzkuMTYwOTI0LCAyMDE3LzA3LzEzLTAxOjA2OjM5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAxNy0xMi0xNVQwOToxOTo0MyswMTowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxNy0xMi0xNVQwOToxOTo0MyswMTowMCIgeG1wOk1vZGlmeURhdGU9IjIwMTctMTItMTVUMDk6MTk6NDMrMDE6MDAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6YWQyZDA3N2YtZTQ3YS1kZDRmLTljY2EtNzAxMjk4YjI1NzRlIiB4bXBNTTpEb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MzVmN2Q1NjQtMWVlMS1iMzQ5LTg3ODYtNDdmODZlYjNmNTJiIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MjgxMGExNjMtYWY5ZC1hODRjLWEzMDMtNTM5ZDllY2UwYmNhIiBkYzpmb3JtYXQ9ImltYWdlL3BuZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMSI+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6MjgxMGExNjMtYWY5ZC1hODRjLWEzMDMtNTM5ZDllY2UwYmNhIiBzdEV2dDp3aGVuPSIyMDE3LTEyLTE1VDA5OjE5OjQzKzAxOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDphZDJkMDc3Zi1lNDdhLWRkNGYtOWNjYS03MDEyOThiMjU3NGUiIHN0RXZ0OndoZW49IjIwMTctMTItMTVUMDk6MTk6NDMrMDE6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+DF6xzwAAMORJREFUeNrNfWegVtWV9nP2qW+/DRAdM8kkmZmMKXYssRtTTDMmjhpjNMaKhV4ExK5YYou9GxRQpIiKghhRVFB6l95ub289fe/n+3EBUTMRuRi/9YM/l3PO8z5r77VX2XttkGTkS8Yx2caNLouMqfIxKSkVySggyyWGdJmPGJdZYJ6BF7E9ZMmnJFksk1FMUlVi+lEYsIWyVfpsl8xHZEEqSrpFtnSymR2kCiMydllWrFCRpAwZk+D2vynFCteSxXYW2SoZtUf0vTisRJT5PMsewxIZsZNhyBKZj1khGVFW2MyWfEz6saLbUiZlwDDP2CcbyA6vmS5VqYMMYzaziZKUkoxJuh5buJWlIltZkQok40h1/d0jKx1kGNBrKjNmgWwn18fkagaKQURJSTIqsYMqZHPoFRmrZn5E6QcBowKbSUpSVchOfwv9zZWIcZFbPY8hPRWHnvSKYdf/ocuoRLqsSG70KgxikJSKjIMwpre0/+ChowYPHXzNyJEk/YgMGTeq21BnAY7pwH7RjcssstTOFm4YNWzk4JFXjB44fDDpMVaBLDFcDg1pGBYSiZ8tJ6nyLAfs5L1XDR889Ko7Bl97VVNJKqnIqEB2fhu6CSAtTOGFIEmqOIwVffcIkRRABkksamdYoldmZz1nHu9Hmxg3S750AQuSHllmcRsvtoUJE04WL7QoyZgskU2HrGQ7Y5bC9kM/CNrJmGW2kw/DNJAw0RPntXqkUlSd9Ldd+lqZFcbr2XrejB1oZKRI8mpoAkndqMGzZEnRk2GBwduilUFIn1xxcH1nyBYyZOzzhiroQFI3MYR5shSR5TDoP6EpCt1AyfIFT5EFSqqAcfNWR7eBRBL40fpgOwNxZzD1shY2lD1SPtJ/u6YU44iUpeeRNaGlgez5VAUG7CSb2XTBVKoyN7Gj5aBlAdlAyli2c7kNp8rWLA3HebFHdrAiYz41aAMDUimOP1eWYkYuY5LRH5ECdCBR9xYVqbpG8Zxvt3Wyg37kvvWfbtA1irvGcXEzktCRBpyDN/sBGeTp+1x7/3VKMZTM8/oHwpAB3TKZZ9P3oFmACSFWtZIBWSHV1sM2hJ5bUSwvOrhEn4UKy2SJd6MqoScgTNwehlEceiGp/PqDl7DUoaIKFx+5gl7XDA/ZHrFFHmpAJGBnYb7FOC6TJdLj3J9USgzYEfKZAVvokkUyCun+oRYmemqAGM+iZAcrEWXpu0spJcM8Vx+0NqCk7HDpsmG2YwoAWeinlcKYHZStZMCznyLlVpLygrH02YWGoSwFHKpjH6AKInl7hYo+K2XJsOWI1RXmvQLDRUcuJCtkOSpSVp6EmTLSEAnnvGKJlSLLIckbbpQV5kOG7PtURQUVFZBStYVIaKjVc4C+mRVKqk62dHL8uSGDRrYxuO0SVpTqQuMqtvAZAVTDRjX65+lF9Biwxedvn3NJcnO+s88c0iuTEWMZfJB2kErBAY7ZxiLjCik71bQTVZlNDMknLqAiQ/qRWx+194WNnnC0nPNKKaSK8h6LDN4+dlvMIFJhtPRrii30u9B4YcQP6+BoyORQ02eDigsqKpBltt9xJZVkZxO9EXdSun4c+CTDhhNQlYDQhGm+yaIKyHa2cRvcSugyJN85qS2gKxVZKZDXIWs6EKjGbWGRlQIZR3GeJ05jp0eGnRsP3epStezQVGdHfAz2h6ZZOlJvV1QYMXQZlbylX2epSI8N/tu/iItBTLo+oxIvQw1sEzngIemXGQUMgqI8fVqFpOfHwQ/fjtgRuQxYiPm0ZQAJpGycWWxhWGBAFtlx/Wi2M/Do8sIJZDHmdk1F5GjUmIZwYKWuo0epKEmPDQesK9GLIkYbxMYKSaqArPBB7IM0bAs4i25IlmMG5KCbGLMQx4wG3kuqEiO6vs95GegpoE5DrrPMsIGM1pJq3JncRsoK40evJclgx6rZySmOnkWtAWTPj0kVxaGklDz7SfpkqcjiqTN85VH5niSDpRqSImFDx9H5VhYYVxQLnPaLzWzLcx3lpEt8+iwz8mOyaSBsOJqGlD2lzM6YqoNlcu5JawJ6LLG87pvNAekzv0NTGzQIVMPQhLHZZ8WPSRUUec/FjBvZphiNGcWwQjJSjF15LCyYEBpSCyKqclBigWpjagN9GZNcfmy967KRJTaQ+buFiYQBM4XrQspIlZX0fMkD36eqhGS+6YjlIV3ZTEZdaEq/hQ2kDABvkpSMKywUOOPEbfSkZCV+/zRZJj26jPJsHSW2C56nFzEi85L1P1lUZD25hfUnLe0glVKhxxa+lIHmwEhDXFrP7RLEPPsxGcX0JHn2tIA7BSTfGdV39OB+Q/oOu/s+jy0+fVZcxfUn/nD0wP4jLhs0eDi2NIVkXM6TXuXDYUO2y20NDEg/CMjWMdpZw6+4+YpLbul72uh8zDggZcCW+skXDRowqG//0df/Zc7ObwaFv2XOvW3I6AH9B1148YBdwBAki4WYkWLY7lfIUiwLpHLdYM7mxgUbGhetWLmujaS7jQz8wJUs7HjWJX0yjBl2sn5+w4b5LSs+rF+2kCT9gIzLZFQskWQsldrlo2x4e+3Kde+tyG9avqI+LH8SjSQrHilJtivSU7JItkesxC7p0Y/Z0BpIhlLGkopU24WBF0eBImPFMqXrBozzZKNUcahIv72oSIZRTMowKHfs+GbFk/RaqAqypMj2T3ETU7pkEMgS22WoVEzPpfLzMalKHsmPSKnY3i5Jys78TjSeoowZlvJUXiy7Xui7yo+VUlQRI1KqMqNYkZ/gpliQfjOZZ6XRZfGTaDySjEPGZQYMJMtkpaJIVsoy9KKoiS1KuQVGDMty15dGkiTLLKpOMnDDsBLSL5OkiiKy5Eb5AkmqUDFy3R2PKZ+SDOgXqRjvinO7J6oCT5JREJIMWPZIFtpV2SPph2ykDFkKu3zrcke8XRgHIcko8lmWoUtWJAOylaSScdeHpWI5CoOYXV55lxQYtFbICt0iC5/WlAwVFemFlDKsUKqQshyREVnyGCqWFZsZK5ZdMpY7f4silXJdemQoGZGKlCWXYRiTVJJxnvQpfTL0yXDHc52MOumXSCqWIrZ9Es1HX7FsWruEqsRyFxp85dJrdhTsWDXNr1j2QdV6sqPj/xdutIX5neNG/4oF6eSaImP//xdusJGM/j9Bk9Gxnv4OTWlfscC21rCTci9zY6ZsoVuWISAMU8AUAKBpn/ucgXVUextNLrXLh7WdSHYLjdrraHQBmKmkaeiaMC3LEoapA9C/GjQQ5me/rJnOV4PGMnQN0C1dFxoALZd2bMcW+GrQCMBMJTQAYsdYEZapfUVougDBBISha5pIpZMGYGW+IjSGI1D9k6uffHbilMmTJk1+9v6BJ9YBxr8KjXAcDRoMpDStGvjViJnbGCjlRgwiylixEtXPvO3kFJKADYFqwPoHM37v2Bvb7PrX0pDGKY80tpOlDz6Yev+AgdcPGzbqyqvGPlq/oYnkijfOyRlGCvsj/W3A+nK42a8KSGnICFSd8tBGhnzzwT9aSVtHCuhhI2klAQf9nnlvMyulJ481HWhAsir5JWmqRzWMFICjHqfKz7zzoEwygR+equMn41e8+7d+RwLpnF4NW3zthvfKHqdfB/QWAuLLQaML6CaSh48l14z/PZKJ4/+Ct1cAhw/8OZDI/vdNt//tuecnnwygB45/qFGy5SwAafvLQeNAT6LXbVRtD8AyDRz599d7HA7UIlMncheMHz9+XuN6Svn39A9X3dlH4OaPYjn9ZMD8ctCkgOrLW7j4JsOovueNBbegVwpi+yh1HD2BKjg4bvC4QXiIm97/k4n76anntC9phuv48etR+FC1/b0pHzGONj4MB6iG5thnvrN52bLVm55/457+R0MzcchdCyP//fNhjvX54Ul7GU2dAxuamfx1kQtP1/Tb6Oc3Pn4kktA13fz26AR64tJZ7UqGMmbUuPXZU5PAKWPyxVnfcU5vjIIhugZrV411y97oSUCr7o2/kE/2xMWbyBd+WQ2ktPSROP171p+L7c/82rFx6XtFtvOtxg7i5pV3f99w7l0TjAFmycJTMJHaW9xUATCARV50msCDzD/67d7QrZrDEtjc+/rWG3BwE+P690bBPuyRNnIZ38VjZLT4p8CNpQ8PxY0xX0Mig+Re0pSRFvoxH3Db99PVc3g/cMD9v8IPpl2gzSN+wY6Hqr63iG65I3ygj41H2uq9HxwV8G8bJVedBePJxt/lztyg5gP77SVuLCOBn2xTMxMCb03/L/zqyfWnYnjTi7iA/vBEnhynH7KRW98MZfPMi4FnByYmFd/XcPXrUbzgWBy46rb08Su58FPjphvcmDhkaWUtkH36VPR5q3VFTzyhGq2DozIHgK0fcUry6HXyfTzMTjXrZ3X4JbngopxhXb6gHE1CcsRwnNrIx3ZZ27s7w7dwCTKAEGPy9XPRZwHrR+kvV1ZzIJbzpFf815M/Wc2nzItdtfJ71jecE1YV3Wdhwh7g+Rt/lrzwV7hsC39s7R00GuZyrHH4gJT1P0sZPooTOjoqWzBQBTeW/o4tRGYZ78KVzareLUzI4dFtQ3rjxUK84ihbx3efZfO9yJ6Ep/iXbnNjwoHhWNO4BH0WHIL+cZ4P60c2SMohyMcPHMQJxmJ5qXHItuITev+4ff7pOHapVPGy/8QFa7j8YjNlYkR9OP00pI6JN3XTv0nBFnoPYCRXY/83AQQxX8F/bGtvKK2uvkl9aKZb3us1PhyC5GVsOTd559AavFQobTztPfIGZCayPAjWf+GEGfyon56TFad73AjNAjI4Ta0+2bwGVbfifk63j1/FVQ9xDD7ybwXUBmdi6blZSwLGz/YAhrYx/wKSGFAszzlw3/sDPgSYX9Pu7vDORsW3u6mpBGBjv0XhMJygHbeq+aTsg9ViU7zm+JYGXMU27KttXIdpbA+jlrsB4FgWXjvDsZDLHbQiaDsJA1vc12tSEL2ei/qBUTftjSGQzJrXcoZp4sC2OHD/w8AgkuS9+uywbUm5haq638rZd+SA85/ly7iszD5ANguBJ11ejL4B30aqBq/KIw5SW7qJJoFUCif4rX2gpTaWlrS3jKjWtaNHzvhg3Q+/RkZy/bYXH6+GDZz2aEcr85XfOi8XW5HUoFtVGNrR/jDOa1HzLBwuO3FX9Hyqm+MGOQ1P8sakiWXupif5Fs6ojDnANJ1Mb9jQYQGG4dwyt6DY+v4D/cPC4chzpAW7F6BXnUGOcS7ZxJn4Oy/EGp6X7u640XBisASaNsFvuFrV48RCZ4lr3plwKm4Y3G/otU+9MnfRrXqHH7/5wJGZXpjMaal+efaBQC9kEjhiOW/CwI6297nE/GPQYOndRKMn8CovTOJKuj+fx7PsSfG82xd4st0+yWMcS5+tfBWb5P9mIWBCrCrejFfVy4kMkknotnVgfXgfhhRZPim7On+/I7pnbyxYP+rYZOLQjqY77uSzxkiv+J1e1neit+yX5LznZjx5xZCfu3MxvXy1Ax0C2u/aO4wDyDPQG4YNHdqR2zgU1wS31t4rtxrIdo8bsxb3y/tyeKOy8Kh43Y++18nLdOB2PoDG8HQIXe+J/Db8hS/ANAGBqgf5Ah5Uq2HBsZCwa/DjVl6kn4Lzi+6lQuuuR5FK1nfqGMa1R03378XMaIlmp7Exf8B3uDWbSmtIiiV+ri+XwjQBA9h3U+k8rIzvhA0nm4XWG6PJb9rHbOJd+q4x5x6hSeFyNR/pzXLUgZ2+dVq+7QAgi/w26w6+DEfAgvm0/NmRbiOEgGYiiSGdy3EZ3eMAkbKzGfvCbeE9CRyziXcb3fZvqjCT5ziPV5akx3MYFspZ6FmFn6vn8K4aUgu7F+Dcrm5JFor/1pUz6aFZH3AMlnIqTAfI4mrFv1n7zML55eByo9uaqlpDoMhh+3OZ+VtuRh2AJ9WduufVASZg5s5RE7FB/liHbsOErh/ZuqH3STEP02r0HodvDtYPxpWV/C1197PJRqabtvgyvqqNVB9prxZvNN7kPWZSJPFy/N2juRKOQDalIynfTE7jxRZ0B45lAlN5S/KV8MUEjIfjykcHp0a1U5ZOrt5QeSjRXW4meLdaC6Jhems9zqALzbJgLymjX/QmoOvQoaN9ZfJR9WgaFoQheiLxHbcD/yOjPoPnseU6G8+x7e4BciXOLm89z9QhYKch9szeVC8Noa0r//6vfESbxgc0HUm9Kp5vPMeHHeiAZqFqeWmfAZwnDOgOAB37juSDmMjl5OwT8Zulsv3sDOb4f8VkznHMbBowkNkzbvR8UwbvRE8sqRx9OL3vCAHbOZ4TxJL4VxZMGBBIT+Mxh7KANIyUDqsWiR6NpWMOLnLTGfvj5Q4uPdA0ep+4lgdltrYNhpmEbiC9Z2j+ED/liJ+x3X9PHyPfgIAO/WbvLrS2Vws4MHQkcTeHOZ2BaQNwYAM4YBGfxx0jgCtZ6LhdQyoJoy/fwc1s6AkIYcDcMzTD4zHm1XiNPDfR6p5vQLMgJnee2YMtsOEAFnL4fTwZ63gKDAM1tXBGvFdyK7Nh6AM2sePdo6GnkatGZk35PH1dY98UbCAp9gzNC7wKxLHbmsSvuRyaaUO3Z8S1v+RK2DANQEDbt/gBXuCN0KuB/q/5lUL87FE59LjFrSw+E71gGz0B4Lxo9bFzOSSJKujpPZxT7xE38g0c9QSml8bBTlgw7EV53BK+DGHD0gDo2LhBPCXv7XXyA4tJFS79LcRhd27AceQ3dIFeEOjpJFOYSM+LDgXStiP2EM3yDeZ1DN6CCcX1444GbCC/IPUcx0KvhqEbSENfXLavoreZbGx6ZQAM8+J3OxncixnFSTAMaEYWgJXRJy1ecb6tCwgDVV8QjSkMQHc6tuAN1vPvGv4wL/K4cfKYKxLFBbmx0YgsqjOowlVD739xDS/W6OY/uOknCS173mvrY257IWpInMUWpCCQQkKzBGBk7D2NpxK2YdkmwoViQsvDXCVOBs6btoAMAnI21nDT4qXr2jySzSG56SL9zj4mTOPWlxaTTUtHo2Yyb8Na3gwNMIUNmE4OwJ7GU44JMwGUpollvN2bIOZufSwJE1e99vrK1Y/WvV8OyXJH+8IFL4y/6+xDstBQO+iFDo/smH91HRyBPkE9hnBtyhACjgGhIykye5xN0gUA9OQMbOUj6iYUqNpX3PxjOCKTgGnqJmwLGbsOSSN7wsVjaiaxHActU/7cC5ZjmKjGe/wzNqszYJtwANQmU8joe4jGggmB1Bnxm9rq5md4JqI1DzW6nXS3TZ141Q9qrhh03YDLrrr8+udnzF68pUgWMaS95aGjYNbougHogHEqXzUf5sswTV3f7tekTbFnaEwYwkTNNe0v5hbzbaYGcm6v6hPHvbU8lgFvACs+QzLPQJZV05y1hYMPZB7V6a4iYrpKQ29s9E88pq3wX9B16Ik6y0gkgT1Eo8OEBWcAJ+pzuS7AAPcVAEnAOveJcATI8c9PfmH84EuHn2AANWN4jbZV1qZhpgQ0QNNRlxjNodq7HANLwLKAHBzsqUfhiBQSOs4MpugNpU0b9amcYiJh6wYwsDA84y9MIo0sMibsHLRL5GQslBdu/+mabcEGUGi0BsQbkdFgow4w71zxwZ+659/404wFeS7SnldX6wYsoVXjcvYzS2sBoDoB3QBgCn9h4vH46V0CFOFAnykvz2zhj5HOAVntkPddL4xPRxLQbWvP/Bu+n36JnU2YwqG2aQggZY1kP5PM1mWQEYBuaADWFTCEM3vt4mkiafw+fhOvBhO0HFCHoXl/7e2zKg9Wa2Ya0PeMm02b8LgMGjFN9bdFEoCBYaUBggqOgCMEDEMAmM5DDlKbP1EGzyGxkbnL6xWQtb4zk4WpwDrek4OeAaw9QzPbT58TchVeCvvpyEKkgEHRbWgjkqYwoFkGAM24LR6Ra3XTVbvU+GzgEV6OTnWOjsc9b/1PcUJjtPloADCNPfRvpvL3gqVl9ktytIY0DAP2AI7D6sjWhK5DOAIQQv95PCG5yO2Pnh/nfUwYP62813t2eMewZapjWgK3lCsfHWEh4cAy9nAUX8xHsZEL9ef5qA4HAkieIica65v/A4CuGQKwLaAq34jx4Rvo8bH1TCZQtakdvw8LSr3zA5y6sqyeM6EDaeh7OqdqSm9UPcZ1+Gs8TYcQEELLdUw1l/LSVLprQmsJAQvzmDmDK3Y1J2YCmO72g8d3zks4bwRcd1IKMIAE9D0dxWjZaF/ZQGNEYRZ0mIAG8D1MCUZqBoQOTRMm0APPRueCJeNjW1sHUYUxagIeH2qYwxuL4QNJwAGySMHS9xCN9nJ8GTbJQ/6X81EH2A4E/HmJKfFD6IWMBqHrhmHoYmD4nL6E52hp5FKoTSIN1N63ku1zc3AuWSUrs04DqgW6mb9BXz6TOX7tzSgxs92frdu0BSOjsUZXXXFH9oALMFWOrOraw4AMBrxZiOM3rtJRvZScf21CAND07mb2s82rUxj8V3154RDLxP6WA+0NHtCX72aydboBIYTQTcvGlpbcH7kESVi1SFw2s91j6+TvWXq/M+C3DckCVs7cXtnvVmb/Zf4+ocF+gdOBLByRwCs8LsMNcADU7EgQ1UwNT8l4RWRQN2jiaubl+wOA3H3r+QKWBVcb36q2AWHoontoRM1Pyx8incLozdzwzImAlsFNlSHZkuxtwoSlaZomhG5gJB/EZj76zOaWkO7b1wCpP73colhea4zlOA0ADG1X1e5Zpk0YS6Ojvj3ziNQfJtIvLLz7DA0XcyLm8lyYyZ3GDj+9Wq7ARKoyOyb/0oJ2znMbAjaP/eUCpQ2PZiU1kcxY3a/56qi7S72Mvz0IHb+dsY0h6z98tfRhYgofzNmwce5l1z46fdGmZq5Rzb2vnD5pRB1Qddbr7fSjGdcC2Td50M+4RKSgoctOdjOXnkFjU/Z/XsPN42Ekf/XUB1IpWdIuI/PcfkCQkVtaOPvGKywY0E98emEni1z86GECdhLjOofZqqIBup0w99j3+9jgZPAYx2iP2meVOfPslAn9tAsnPQ+sa1269qMP87NfG3fHJcfuo2E/WCdizNIWMt8297Yf7Hj8Qt6nb5EpVEHkADMLB9ATXVmBPaqH2yjH3z7wIiwtNbH1g7EXn5JBjZXQsC+SNQAg6o4Z+PCrb3uck3mOpRmPnKfvkt47i49hTXSKjV4GxICbYCFrCcDYw5xoQjO0WzgZZ+A+rh6/maRXXD/nYefqWQ/PmTlu+uqPNjSVIhnTb8i31x5RWJZDGrax07VI5f9uzw+GAlU4dR478xchBej6nlaEUAe7ennwS/TGVvcP9k+vnDx3RSu36qeyyKCDDArtDUumPnhlEk/xQof8vp50dt23xQ2YyuuA5EXLvHXNnGdrELq151VWzcAFbBGG82T8HgArrTv3t/QzN/K2UYPPhxBdAY/p/KyyAq/yZgGj5mOH1IrXYRLPTx87pY2TxIj1Kw3NtCH2cNU0q5HopdlPNk+qQqKFJ9s1QDXO4hQs4sPQAd1JpRxDc1JIshVjioudGiR2cdfXL0tPCm65wyt558G8kW+loFsw9tSjwH4OnH2wlb8z8ZT3EQwbafRY7eNMNsB2kjpgWkBP2NZYnifYbMKq+dhBzmxY7Tztt9CbAjOVmiEHZwFYyT3UlGUhmzBQPaClcCzMzvgKQwAZTCiPthvd8zXASNqASMCwcA6fwuscbQO1H79g2UfJySqa8gP87kn9Qq4VSOWgOd3dK3AHN8O8Md98uIClpU7mcvzFewdG1gSSQDIFOwuuwiguArqyRSnNEPr/8gM8N/lE7cipLb+wVhX+unf239ThLTkfyYV8w7IApBcVjrPInyZQXSuQcGAAGj6Uxx3NfKI2a+JryCB90PMrGU9KAd99rbXykhjEpXtrb9J/4k3O1L65tVh/OsxvYCgn4gEuRu+uEVKnOUKk+vL65DtyJHQ7gfRRf3vbj/j6zUh971V2hltxgNt4hb130FShB/Ly8R4H1rN9xhHoibbyT44q8teABtgCwgawn7cJt3NObQY/f34TG1XTnYf+GwyrKZbc9N/J5Rzb3arHx5GjUfPdBvkGah+osPXV43rc4I1LPc65tbBtCDjQkvtomKVOSgRtN7xUibhx66RzDHz/hvy1IDdtPBkfcvk39touMj2N7DEr+D56HPhqEzl/fMU/DD4vTcM0NdhJwEL63vA5azYVN6685ygTuP0jl8Q1Za45GvMr246A3ntv7b/JJYD0bLYdAZx6z+IG5vkeRsaNt51q2mcPHTb03qnLt25r5/qaESvvPL8aVd96bB4ZBt6cqunl63SsiJrPFB8Hf91Ek03CQUrHONl5qwFLP/2x+cVotDWRjQzqKWVEv5Pe/AnXI2chcfbEDxU7S6+fO4UDMOs31m+2lLf9Goae3Fs7pXZkmW6W4VSY0PdH3aHHWodPeunVLfPHzZw4Ydith2oWAJiXbXQVueyx89L6TXFDqk7D+KK/l+KpzxSsjmzixkus5DnPXvW7GhvQdVhpAHX49fDbpm1ZIY0jyddGH1KL4x8psHAHcMEsFsbupXjqMy6GhUmKU3+QPOyuNq65OXH5qglzN85r3tShKJsYkMMz83i5ffj9K8vsfPIwceK0Mtecs9fiqU9tL0YG5pnzqCb/EBiyuXgR7nZZZixX1U955qFLtG82ra/qFxVXkVzzgC2++2gcNVwD3dg78dRnnB0N/w6BazdX3Nl/BrRb8e9/ZeiXlm164eXJMye/NF1WTssGUbR67A+hHTaObJx1/Hb/Yi/EU5+WWtQAmoPUX9cybhr/W9QBg6as6mS5q+EJXT6B0SMEcvv8ZQHDcOKPYOiwEnsnnvqM95WFblSjGjYGzYhYbHpxzE819AAwaujl/a4ZNQxJaF/Hr55ZvoXF5vsOrELaFnXA3omnPhtBaFUwgQyEbWHM/M6Abvuypx4b/idYBtLARRc/PXG5r8Iy3+yLhK6lAWjZvRVPfV6cfv6Tq5taGAaRImXgMZB5qrY5D/2mCkL7J6csvpTzU+ipwfz6JVe/OvOd+cvXrN+4aPKE+y5KI5EBNFv8g/3oXyo3SMHSUWV/HLens2kgC5HIOP88NPoS0KRTXRgMw9B1wzAgANTWIJ0QgG4a/2JugGyVuUMhQncsM21trxhqjv0v5qbLEdXtXCZpb99ekxZAWqSq0sa/XFOannASye1pAE0IWBDI6TtKkYl/LRpDE4AjAE3rOoxow4YDzQZ06x+df/nnaDShIw0kBPYFTCsBRxMpOBlo1UBCOOglTAA2NB2ZXcO27v6Mf2RvMl0uko6k0VUCzeZSAkkTNT1Q3UW1QI2dTBsmkKvG3kTzWW4MZJMZ28kCFjIa6qoMIGEK1MJ2EqgxYNZYXUtv0tKQNb9cNDktrSMF2MKEUQXA6GXB1HLIwIEDJNJwqvCtVJUD1ObwJXOjCyRTj058fepr43+vIZVInXD9B5te+lMtUkD63i1bZj9gQTgPj3t4ypTJ05/enYOY3UEjEtCNjzy2lVgYk4V28FqS5DXYV9jv+aW4xBm6g1gxLtD/AM6XiwY2NO0jN6QfsKkGiVVBx+Y1DYxGAT8n29+oUELAl4X2gHLxv33JmoKehL1SEbcpxkj0KTE+KDW1iTNMfZSn4tenDR9i1aIar5HDkNS/bDQZ2OllfvAcldecxSgV1SfSVzdxjZlBI30Z8zVoWRgvecEgezcOWnfP3sCBrq8nGbh8AskrFVfCGR7J5WnoR9yVVyyV7wdEbrLiMOMTIciXwo0BC+vKhcVL37knaWRQrPByvE8+29N+rm3NfX0WkM8k9gMmVLzLIRJfsqY06A62KDrplIla4FV2dNDl1rOAOzpDrvVUdFMOds2r5BB9N47vdhcNNH2+yyyQ7l2H7PeWRGUWW/+ip9D7DSWZ5zQIDfbEYjjCAewv2fpB03DJiJE6agHLRA2GvvHQEyfBgQWz798mTTg1CwgLvxs8/DDNEF82N45lIGkji7QJCNvSalCHFCD0LCxkDAsmspm0JUwg/WWvDLAgsrBMVAOZDLoOUdcJCBsCThKGgKVBA5C0tX/mPe1dNF1u7c5hmoQFzfgC9iVjIGsh1XX8I7WH9maHpgxD1zV0uXCapmmwIADs9kDRoVuArmdTBmDC7N4o7td/4KBBgwb0H7xdRvYFvsiozaUEDMeABVj7Iie6N8M/LdEKG5r1BUyveXj/a/tedt3Qq5E0/tFZ+S9k/UgVhVEch9sl3maagNjtZfKb+MmygGwpBBdjX2dPNbVjZVBKxvEuXbCCNdDEFzF2tdj3WbesVCF/MpDZ0zm1Y9Xs6uoZ7uyrtQSw9C9gYLRa4IoWxZBLd7fHy//tUZAqLDasXb52u3z0imFocHZ/ijswsuLol0P6nKYl99QTFRBYSdnJmJvTBgwYKaAOJoCkgQQSmgMHVU4SBixNZBKAkQA0CMPSoekwYSINA0gCjnneyJF9rxLQhIUEMnAAPWkASRMZB7ns59ibHWhIVV4NmNgPAmc8+9orE14bO+7pDG554dlxk6aOn/DAy89PffGl8RPRdyNLrto86eBUAv3feebVsS++drc5alE73ev3P+KeBS1x3B5seHlkHf4DFr6FTN9pK3y6nU3LHzoEu/bF+afcBAXKBt3OQUsncHIxDgNWFI+vXtfVl857ZXt3v630y1HEOC4MT4iXWumTxcIkSuk3TRiXZ0SSPoO2OYOQTAJXrYkY+6QiG+JHYdftHhr6dOdngEQdqizMYMByRHXDsR4ZRe1ccl0ldOOoqMqqQLaExXY2jzQnRJIFz3cVyYh+iW7IKB/6kqR7JYT4I6PWkDFZKEQsclb1bo6byA9YmvjC6y9O+PuL42ZiG72AMpgzmgGDStx51QDFkDIgfY+lkO3cxNIJT9FnGxm2b3DZWvDITlYY5hlFTXnmz8bxha5uwPXbyHVSkjf02E1u6DOMI/psbWKIe9hJ12XTnLhMT3JaYhApVRirMqMSi9tIkmNfDOIOxYBFBp0kGfgM3rr73tVkSDY05O7j5nqWVf78noNK7GBZbd7NccOQDBm6LDNiJ/ABGVRYaop8Sm75DYaQKvbp8Z377q9ni+yk4spp9MmoHDazfo70ylHEysJvwTyvIktNlJXzniXJcrz1V/j61Y/d2X/4MZ+3S34nmgIDSRXEDGOXKfsXcjNZYcAoZPgkqobEEZWk1x8piBfZHrM55PiYDEjJ0blBDWTcxPLlqDOxskSWXb430auEkgzbJv5vGhqQtXdz3ASxJAsMWCALBDCJRZ8kPQZLTWSGyq7WljAM4B5Jn1HEORVSlRn8JeE8SKUoAw4f0e/PA2dIlWfA1lvpMSaDkBEXPH4krE+eAvxn9oZe51MTH35+/NSJLzyra6K2cRvjdpZ9loehDjvQ5HQL1WeoKGYc8/2Abcrzl38DxtiuzpDs2N6WnR2VkN/vUAwDRTJuD1mYc+7u2mKG9Fc5iVwyZSGbgWHhfrJMUoYboSedndyIfWBURXnFKOb7MQO6cROM3JQdrSdLXlxo8xgEJI+/NA4kqZSfJwsh11whdlNTZYbNgAEI2LATVZkVrSEVXcn8D3Sxk5t9AUDvYKwixXelVGyPK8joUxmqkKRfViTDSshy7N2J368N6BVDshJ7QQebj9vdGc7mxmwtLCOdEhaAG+iGdP0OqmC+Xb0TjQZhWpdGFRkrxu+oyKfkWgBPkzJQ5A3DBlw0aPglI66/euiQQUfWAKePXx3S7wgkWQx4y+6h8QuK6/XtQUE1qr7rd7AckYwZqsuQ2qmpVDppPyO7mrXO6lLOwjSMoTFlzJB/QrIXRJ2mI4MsNGHngH/7wxRFbumQjKbuJjfFkG9lkYGhJ1MZ4KUCtzKUMiyS3NxrJzebf44+D7osUJKNLykyKPvL0hYOb2RAljinGsn97tmwsHHB62MfuQgQBnSBr49qol8hwxm7h0ZGIVlmpcJKhR4Zs1HS44dltm5j+/M7uaFHyQoDhtJbMy4kleJmpKAtjopknuVhFm6iL13m2fripg3tLv1neuIUFgN6LDzxOd6WmYK5nuXPuOksx2xserDkk50s/3oYWSbd7b1lJbmV0e9ebiVZ4AYga53gsyP2qcJtPl1GRcrGq0bLmJT0F73XVmaZFYY//+f2Jm1A2GuCnT14d4hLlmI+3p8sKj/k7Ks6QjIusxIqGfihDFh8F7O9YkxV2pS0oDv3l0lGIcm4ECkWO66CNTWScUgGHtlAVtwxn3d+Kgehbyx3foYa2cnW4Eq7jZ0xy+TrzIfsauTolSMq2TmrTn+KPl26a2H1qIJ9XbPsUAxjN6SUhTXn58yU/UEUBK5LFgqdbOuc8nkeBRJAdikpPyVFSY/cH4uVVAyicAVLXkSqETN8MiY7rnEE3mVF+eTyDKCZeuaASWFMn17I+rXbt0QlbnxdsVIgqVrf+C0+7+x8GgkbmY8j3h1iZZGGbQPJlA4ji5sKpOuTWt3B5975xKhfmXoCNcB+wgJsZNLQYeUy+NGAW6Y+PfqsHugpUloynQbwjTOvfXrCX/udA9TCTvxzNEk4ELCrPluUy6E6jW9WQ5hAWlzSTkaMfQgDWiLlmNAyhpMBbNhIAbVIAv+Wgm0mAV0ksD/SSGhWtQYAZjoFsw6i5+fWYLLAP4i3HRMpgWo46GqDeT2VS/oV0zC6uh1+TQeqdcuAmQLQE8mcXdWVnHCqHBO1APbrsqXJHhlA01PQUvicDhCproxN6rM7FGwNtdBhWzAdOAOC2KUqsStvoadhCBOwIFADdFUMzRR6Q0shrcNKQdeSMGEi50DXkIQNWPb/eXbecoT4AgnXy9gZkZ7b3SSSDd2EMJFGegNVZXv3dhMAtMRuV7/6dzXF3ws1PxNA0rE0e36b3NH5FUauJvkF3nFZ6IfFIJTdrlgDqR4pANDWkqqzC40lAOSyu50RuS7oWqa6i8ayBACtap8MVjEIoh2j2EgmvkBa7+zxE18cP3HS2G4rqq4aZl0K0DIL4oLcrqkHfmZDwKzb/SyjpUPvfkpUS+pGBjCrTru2jf6OmzQYr3/ipxZ2W1NpB7qG7hc9dAOw7B8+OK9NRizvuBGhkwzjtr/9erepARJJ7IX6VArVgxfmKSO2dDW0xy5LddMzpyQAQ+gZZ/vZSx1GLtntj24/YKwBmtA0TdOhVSOp6xiwoCUmSVXxy2z/FBqPbBz/h/2RA+w0tJxlORawN+qXRiplanrK0TVNAyytGpb4wwsNpKQseWsl2V5h2yfRRKUSydWv/G4/mAA0ASCRtPZSeUNL6F3uroYEfjypgQFlsRyVXZKliKH3KW7IcsGVJDdPu6hOr7b0xN4pJSTSVdUZCzAsx9Y1Yfzur2W2cD27bq0Iw0qbij2WPoWmsUIGJZ+BT26Z9WcAdsaCqNkru2Esy4SVhNnnrlX0WK5sZXnxI8fBvvidZhaLbKJ0P8WNLHtkXCIpO0Jy7h8NpKy9MHVSXfmIRG2fkR/EJFmvyPV/Om9sCytr7u4znR2R8tWnuImlUlIqKi8gw0olDDr+/ucsuo/HSAjAydz8UiMZVnxW2LGuMurBfEgWgmDTwa+wKeSOrv+7kLM9UgpDUtIno6h9+shua8kE9r3gDY+UURiTHuvJYXex6fVnXnhjQyTbD17ZlZ/8FJr/Q2YN3A8assICdENY0HUAlg3d/ETVKqFBEzBSSdhZIAULMGttmOh55pTCLrdFxJLxgrNKakH60suvxWvkpFFhmcrdPTSuz9LcMf8JmF0b5RIa7J2ZqV07VcHennDXdFgpiFy1BdSd+OKKgGG4yx0sgeTTz6h4jWCRs/9rm8d/JxkWdw+N8mLXC+vX3/J1oGdNygKEKeD0MLq2Z+2E4xgA7HTaARJVAjCA7K8fXcsiyUq0ywsDqX7WzKgVjIO303TVzbNZ2ry7mupiKO/7G67JQAC6IQxLg6VrutilpqcZOoQBoGcSWtowcMwdi8mgjR0NRSXDj28ZUfTBslqWG3zjcfbkepZfnuCyHOwmGllxg0CpUNLbtvr2WgEIDSKjQbf0Xftm6bYOGFbG0PbdD7Wj18WMisWIVDEZ7npDDwMnLnFlYumCDX8+hAyfmVqk9HaXG58MSBZ8RqVK55IrqmHpSKAr3rE/9roB0wTgwL54XpmeClyy1FwhPe8Tqvd50fKQa8Eo+CDR7sV9F8f5wm5qKqRHulRR122RRRmWl9/RR6BmeyeOXSqRqbQGPX3l7EYydkkGUkqSUTngx3fiKLp8f1Sn3JjwXbY4Ra/+AFLuuPPkcxVFl8xTkTEZVVREVhSXPvjfOcDSP7EwGkj/aUZb2HVPTKzCiMqL6Aek7+68ZidmqNz/XuM3P8VC1PTgFnXh9AKl/4VG8T/gbNmYw3UYqa7ijrCB3ClPtu5yJ87/IVHEIuf+TwddKp/kvX9qZDMr3UQjyXD5vUfC1Ls2U5/xwAZS+eXPfbCZLHM6JrshGa86qy+5qeBF3UTDoLMjor/tvhOBuuPv2qAovWA3Hmul16gauW5oYuBDo886YO4ariizLY66h8ZXXT5aGK6ZPKOdriRjRVn53AdjFuhWIhbfnfnOOrazvcRiB4PuoQlIFQSKLJdIJSnJ0A1341fUs9QaxuzwWWkiWQ7oN0h2V1Ndd6gyJL2KjDoZVCLSbfz85zpJubXYdQGtV2HYuv1isW6hCV2fVBELBdL3STJ2K/JzH8szDje4dF2vFCuy6HatYwXy/wE8Yu9ESy3tpQAAAABJRU5ErkJggg==';

        return base64_decode($data);
    }


    public static function trackForPostal($no)
    {

        // method only calls updating method which goes another way round - makes updates for all new stats found
        self::updateTtData();

        return false;
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        // method only calls updating method which goes another way round - makes updates for all new stats found
        self::updateTtData();

        return false;
    }

    protected static function updateTtData($justReturnData = false)
    {

        $CACHE_NAME = 'ANPOST_UPDATE_TT_FTP';

        MyDump::dump('anpost_tt_log.txt', 'START');

        if(!$justReturnData) {
//            // prevent running mechanism too often
            $flag = Yii::app()->cache->get($CACHE_NAME);
            if ($flag) {
                MyDump::dump('anpost_tt_log.txt', 'LOCKED!');
                return true;
            }
//
            Yii::app()->cache->set($CACHE_NAME, true, 60 * 15);
        }

        MyDump::dump('anpost_tt_log.txt', ' FTP...');

        $conn_id = ftp_connect(self::FTP);

        if(!ftp_login($conn_id, self::FTP_USER, self::FTP_PASSWORD))
        {
            Yii::log('ANPOST TT FTP LOGIN ERROR!', CLogger::LEVEL_ERROR);
            return false;
        }

        $contents = ftp_nlist($conn_id, self::FTP_DIR);

        if(!S_Useful::sizeof($contents))
        {
            Yii::log('ANPOST TT FTP FILES NOT FOUND!', CLogger::LEVEL_ERROR);
            return false;
        }

        $ttData = [];
        if(is_array($contents))
            foreach($contents AS $file)
            {
                $array = explode('.', $file);
                $extension = end($array);

                // files with TT data starts with "cdt"
                if(strcasecmp($extension, 'txt') == 0 && strcasecmp(substr($file, 0, 3), 'cdt') == 0)
                {
                    if(!TtFileProcessingLog::isProcessed(GLOBAL_BROKERS::BROKER_ANPOST, $file))
                    {
                        // new file found - lock for 3 hours
                        Yii::app()->cache->set($CACHE_NAME, true, 60 * 60 * 3);//
                        MyDump::dump('anpost_tt_log.txt', ' FOUND NEW FILE: '.$file);

                        ob_start();
                        $result = ftp_get($conn_id, "php://output", $file, FTP_BINARY);
                        $data = ob_get_contents();
                        ob_end_clean();

                        $data = explode(PHP_EOL, $data);
                        array_shift($data);
                        array_pop($data);

                        foreach($data AS $item)
                        {
                            $item = explode('+', $item);
//
                            $ttNo = $item[4];
                            $ttStatus = $item[5];

                            $receivedBy = $item[11];

                            $reason = $item[10];

                            if($ttNo == '')
                                continue;

                            if($ttStatus == 'POD PROCESSED')
                                $ttStatus = $item[10].' / '.$ttStatus;

                            $ttDate = substr($item[6],0,4).'-'.substr($item[6],4,2).'-'.substr($item[6],6,2).' '.substr($item[6],8,2).':'.substr($item[6],10,2).':'.substr($item[6],12,2);
                            $ttLocation = $item[7];

                            if($receivedBy != '' && $ttStatus == 'DELIVERED')
                                $ttLocation .= ' (received by: '.$receivedBy.')';


                            if($reason != '' && $ttStatus == 'ATTEMPTED DELIVERY')
                                $ttLocation .= ' ('.$receivedBy.')';

                            if(!isset($ttData[$ttNo]))
                                $ttData[$ttNo] = [];

                            $ttData[$ttNo][] = [
                                'name' => $ttStatus,
                                'date' => $ttDate,
                                'location' => trim($ttLocation),
                            ];
                        }
//
                        if(!$justReturnData)
                            TtFileProcessingLog::markAsProcessed(GLOBAL_BROKERS::BROKER_ANPOST, $file);
                    }
                }
            }

//        file_put_contents('glsnl_tt_log.txt', date('Y-m-d H:i:s').' DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData)."\r\n", FILE_APPEND);
        MyDump::dump('anpost_tt_log.txt', 'DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData));

        if($justReturnData)
            return $ttData;

        Yii::app()->getModule('courier');
        Yii::app()->getModule('postal');
        MyDump::dump('anpost_tt_log.txt', print_r($ttData,1));
        foreach($ttData AS $ttNumber => $ttDataForItem)
        {
            //searchForItem
            $couriers = CourierExternalManager::findCourierIdsByRemoteId($ttNumber, false, false, GLOBAL_BROKERS::BROKER_ANPOST);

            if(S_Useful::sizeof($couriers))
                unset($ttData[$ttNumber]); // orevebt searching again in postal

            /* @var $courier Courier */
            foreach($couriers AS $courier)
            {
                MyDump::dump('anpost_tt_log.txt', 'PROCESSED COURIER #'.$courier->id);
                if($courier->isStatChangeByTtAllowed())
                {
                    $temp = [];
                    CourierExternalManager::processUpdateDate($courier, $ttDataForItem, false, GLOBAL_BROKERS::BROKER_ANPOST, $temp);
                }
            }
        }

        foreach($ttData AS $ttNumber => $ttDataForItem)
        {
            //searchForItem
            $postals = PostalExternalManager::findPostalIdsByRemoteId($ttNumber);

            /* @var $postal Postal */
            foreach($postals AS $postal)
            {
                MyDump::dump('anpost_tt_log.txt', 'PROCESSED POSTAL #'.$postal->id);
                if($postal->isStatChangeByTtAllowed())
                {
                    PostalExternalManager::processUpdateDate($postal, $ttDataForItem, false, GLOBAL_BROKERS::BROKER_ANPOST);
                }
            }
        }

    }



}