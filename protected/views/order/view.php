<?php

/* @var $model Order */
/* @var $product S_Product */


/*$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    $model->id,
);*/

?>

<h2><?php echo Yii::t('order','Zamówienie #{id}', array('{id}' => $model->local_id));?></h2>

<?php
if(Yii::app()->user->hasFlash('payment'))
{
    echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, Yii::app()->user->getFlash('payment'), array('closeText' => false));
}
?>

<?php echo $actions; ?>

<h3 class="text-center"><?php echo Yii::t('order','Szczegóły zamówienia');?></h3>
<table class="list hLeft" style="width: 500px; margin: 0 auto;">
    <tr>
        <td><?php echo $model->getAttributeLabel('local_id');?></td>
        <td><?php echo $model->local_id;?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('date_entered');?></td>
        <td><?php echo substr($model->date_entered,0,16);?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('status');?></td>
        <td><?php echo $model->orderStat->nameTr;?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('paymentType');?></td>
        <td><?php echo $model->paymentType->name;?></td>
    </tr>
    <?php
    if(!Yii::app()->user->model->getHidePrices()):
        ?>
        <tr>
            <td><?php echo Yii::t('order','Wartość');?> (netto/brutto)</td>
            <td><?php echo $model->value_netto.' '.$model->currency.' / '.$model->value_brutto.' '.$model->currency;?></td>
        </tr>
    <?php
    endif;
    ?>
</table>


<h3 class="text-center"><?php echo Yii::t('order', 'Produkt');?></h3>
<table class="list hTop" style="margin: 0 auto;">
    <tr>
        <td><?php echo Yii::t('order', 'Lista produktów');?></td>
    </tr>
    <?php
    foreach($products AS $product): ?>
        <tr>
            <td style="text-align: center;">
                <?php echo CHtml::link($product->name, $product->url, array('class' => 'btn btn-small'));?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>


<h3 class="text-center"><?php echo Yii::t('order','Historia zamówienia');?></h3>

<?php
$temp = $model->orderStatHistories;
reset($temp);
$first = key($temp);
?>


<table class="list hTop" style="width: 500px; margin: 0 auto;">
    <tr class="header"><td><?php echo $temp[$first]->getAttributeLabel('date_entered');?></td><td><?php echo Yii::t('_statHistory','Status');?></td></tr>
    <?php
    $i = 0;
    foreach($model->orderStatHistories AS $itemStatHistory):
        ?>
        <tr>
            <td><?php echo substr($itemStatHistory->date_entered,0, 10);?></td>

            <td><?php echo $itemStatHistory->currentStat->nameTr;?></td>
        </tr>
    <?php
    endforeach;
    ?>
</table>

<br/>
<br/>
<div class="text-center">
    <?php echo CHtml::link(Yii::t('order','Twoje zamówienia'), array('/order/'), array('class' => 'btn btn-small'));?>
</div>
