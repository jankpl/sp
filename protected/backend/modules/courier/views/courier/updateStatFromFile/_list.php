<div class="form">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'user-address-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php $modelTemp = new Courier;?>
    <?php $modelTemp2 = new AddressData();?>
    <?php $modelTemp3 = new CourierTypeExternal();?>




            <table class="list hTop">
                <tr>
                    <td>
                        Local Id
                    </td><td>
                        Duplicate
                    </td><td>
                        Package found
                    </td><td>
                        Current Status
                    </td><td>
                        New Status
                    </td><td>
                        Data of status
                    </td>
                </tr>

                <?php


                foreach($models AS $key => $item):
                    $this->renderPartial('updateStatFromFile/_item',
                        array(
                            'model' => $item,
                            'i' => $key,
                            'form' => $form,
                        ));

                endforeach;
                ?>
            </table>


    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <div class="navigate">

        <?php
        echo GxHtml::submitButton(Yii::t('app', 'Save'));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->