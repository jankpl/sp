<?php

Yii::import('application.modules.courier.models._base.BaseCourierDomesticOperatorTr');

class CourierDomesticOperatorTr extends BaseCourierDomesticOperatorTr
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('courier_domestic_operator_id, language_id, text, title', 'required'),
			array('id, courier_domestic_operator_id, language_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>45),
			array('id, courier_domestic_operator_id, language_id, title, text', 'safe', 'on'=>'search'),
		);
	}

}