<?php

class PocztaPolskaWaitingController extends Controller
{


    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_ADMIN.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    function actionIndex()
    {

        $this->render('index',[

        ]);
    }

    function actionAdmin()
    {
        $model = new PocztaPolskaWaiting('search');
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['PocztaPolskaWaiting']))
            $model->attributes = $_GET['PocztaPolskaWaiting'];

        $this->render('admin',[
            'model' => $model,
        ]);
    }

    function actionScanner()
    {
        if (isset($_POST['items_ids'])) {
            $items_ids = $_POST['items_ids'];
            $items_ids = explode(PHP_EOL, $items_ids);

            foreach ($items_ids AS $key => $item) {

                $item = trim(preg_replace("/[^\w]+/", "", $item));

                if ($item == '')
                    unset($items_ids[$key]);
                else
                    $items_ids[$key] = "'" . $item . "'";
            }

            $models = [];

            if (S_Useful::sizeof($items_ids) > 500)
            {
                Yii::app()->user->setFlash('error', 'Maximum 500 items (it\'s PP limit ;)');
                $this->refresh();
                exit;
            }

            if (S_Useful::sizeof($items_ids)) {

                $items_ids = implode(',', $items_ids);

                $models = PocztaPolskaWaiting::model()->findAll('remote_id IN (' . $items_ids . ')  OR local_id IN (' . $items_ids . ')', []);

//                $acc_id = false;

                if(!S_Useful::sizeof($models))
                {
                    Yii::app()->user->setFlash('error', 'Not a single item found!');
                    $this->refresh();
                }

                $modelsGroup = [];
                // group models by pp_account_id
                foreach ($models AS $model) {

                    if(!isset($modelsGroup[$model->pp_account_id]))
                        $modelsGroup[$model->pp_account_id] = [];

                    $modelsGroup[$model->pp_account_id][] = $model;
                }

                $resp = [];
                $errors = [];

                foreach($modelsGroup AS $acc_id => $modelSubroup) {

                    // group models by date_submit
                    $temp = [];

                    foreach($modelSubroup AS $item)
                    {
                        $date = $item->date_posting;
                        $dateT = strtotime($date);
                        $todayT = strtotime(date("Y-m-d"));

                        $datediff = $dateT - $todayT;
                        $difference = floor($datediff/(60*60*24));

                        // if submit date is not next day or later, set next day
                        if($difference < 1)
                            $date = S_Useful::workDaysNextDate(date('Y-m-d'), 1);

                        if(!isset($temp[$date]))
                            $temp[$date] = [];

                        $temp[$date][] = $item;
                    }

                    $resp[$acc_id] = [];
                    $errors[$acc_id] = [];
                    foreach($temp AS $date => $items)
                    {
                        try {
                            $result = PocztaPolskaClient::moveAndSendItems($items, $acc_id, $date);

                            if($result)
                                $resp[$acc_id][$date] = $result;
                        } catch (Exception $ex) {
                            $errors[$acc_id][$date] = $ex->getMessage();

                        }
                    }
                }


                if(count($errors, COUNT_RECURSIVE) - count($errors))
                    Yii::app()->user->setFlash('error', 'Found errors: '.print_r($errors,1));

                if(count($resp, COUNT_RECURSIVE) - count($resp))
                    Yii::app()->user->setFlash('success', 'Operation succeded! Envelope name(s): '.print_r($resp,1));

                $this->refresh();
                Yii::app()->end();
            }


            if(!S_Useful::sizeof($models)) {
                Yii::app()->user->setFlash('error', 'Zero items found!');
                $this->refresh();
                exit;
            }


        }

        $this->render('scanner', [

        ]);
    }


}