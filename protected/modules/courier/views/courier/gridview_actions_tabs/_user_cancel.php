<?php
/* @var $id_prefix string */
?>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'user-calcel',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/cancelByUserMulti'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('courier', 'Anuluj paczki'), array(
    'onClick' => 'js:

    if(!confirm("'.Yii::t("site", "Czy jesteś pewien?").'"))
         return false;

    $("#'.$id_prefix.'courier-ids-cancel-multi").val($("#couriergrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => $id_prefix.'courier-ids-cancel-multi')); ?>
<?php
$this->endWidget();
?>
<br/>
<?= Yii::t('courier', 'Przesyłki mogą być anulowanie w ciągu {n} dni.', ['{n}' => '<strong>'.Courier::PACKAGE_CANCEL_DAYS.'</strong>']);?>

