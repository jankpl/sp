<?php

/**
 * Class bDefaultTelNumberAddressData
 *
 * Behavior clears unwanted characters from tel number
 */
class bFilterTelNumberAddressData extends bBaseBehavior
{

    protected $_allowedClass = false;

    public function beforeValidate($event)
    {
        /* @var $that AddressData */
        $that = $this->owner();

        $that->tel = preg_replace("/[^0-9|+]/","",$that->tel);

        return parent::beforeValidate($event);
    }
}