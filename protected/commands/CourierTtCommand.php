<?php

class CourierTtCommand extends ConsoleCommand {

//    const PACKAGE_DAYS_IGNORE = 31; // for how many days check statuses

//    const PACKAGE_DAYS_IGNORE_INTERNAL_RETURN = 62; // for how many days check statuses of Internal Return packages

    const PACKAGE_DAYS_FRESH = 4; // for how many days package is considered as fresh

    const TTLOG_BLOCK_MINUTES = 360; // how long to waint between checks for not-fresh packages
    const TTLOG_BLOCK_MINUTES_FRESH = 180; // how long to wait between checks for fresh packages
    const DELAY_NEW_MINUTES = 240; // how long to wait before checking TT for new packages

    const STRIP_SIZE = 50;

//    const STRIP_SIZE_EXPIRE_LABELS = 500;
//    const STRIP_SIZE_ARCHIVE_HISTORY = 5000;

    const TYPE_MAIN = 'main';
    const TYPE_OLDER = 'older';
    const TYPE_CANCELLED = 'cancelled';
    const TYPE_RETURNS = 'returns';

    const INSTANCES_MAIN = 12;
    const INSTANCES_OLDER = 6;
    const INSTANCES_CANCELLED = 6;
    const INSTANCES_RETURNS = 6;

    const TIMEOUT = 900;

    public function init()
    {
        Yii::app()->getModule('courier');
        parent::init();
    }

    public function filters()
    {
        return array();
    }


    function actionIndex()
    {
        if (date('G') <= 23 && date('G') >= 5) 
            for ($i = 1; $i <= self::INSTANCES_MAIN; $i++) 
                exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic courierTt run --type=".self::TYPE_MAIN." --mod=".$i." > /dev/null &");

        if (date('G') <= 23 && date('G') >= 5) 
            for ($i = 1; $i <= self::INSTANCES_OLDER; $i++) 
                exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic courierTt run --type=".self::TYPE_OLDER." --mod=".$i." > /dev/null &");

        if (date('G') > 23 OR date('G') < 5) 
            for ($i = 1; $i <= self::INSTANCES_CANCELLED; $i++) 
                exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic courierTt run --type=".self::TYPE_CANCELLED." --mod=".$i." > /dev/null &");

        if (date('G') > 23 OR date('G') < 5) 
            for ($i = 1; $i <= self::INSTANCES_RETURNS; $i++) 
                exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic courierTt run --type=".self::TYPE_RETURNS." --mod=".$i." > /dev/null &");
    }


    protected function _modelsMain($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)

WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
    (courier.date_updated > (NOW() - INTERVAL :past_date_fresh DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date_fresh DAY))
)
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
AND ((courier_type != :courier_returns) OR (courier_type = :courier_returns AND date_activated IS NOT NULL))
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block_fresh MINUTE))
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,

                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_returns' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }

    protected function _modelsCancelled($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0, $instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE courier_stat_id IN ($ignoredStats)
AND
	(courier.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date2 DAY))
 AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 24 HOUR))
AND courier.stat_map_id = :ignored_map AND courier.courier_stat_id != :canceled_problem
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [

                    ':past_date2' => Courier::TIMER_IGNORE_TT_AFTER,
//                    ':ttLog_block' => 24,
                    ':type' => TtLog::TYPE_COURIER,
                    ':ignored_map' => StatMap::MAP_CANCELLED,
                    ':canceled_problem' => CourierStat::CANCELLED_PROBLEM,
                ]);

        return $models;
    }

    protected function _modelsMainSlower($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE courier_stat_id NOT IN ($ignoredStats)
AND
(
(courier.date_updated >= (NOW() - INTERVAL :past_date DAY) AND courier.date_updated < (NOW() - INTERVAL :past_date_fresh DAY)) OR (courier.date_updated IS NULL AND courier.date_entered >= (NOW() - INTERVAL :past_date DAY) AND courier.date_entered < (NOW() - INTERVAL :past_date_fresh DAY))
	)
AND (courier.courier_type != :courier_type OR (courier.courier_type = :courier_type AND date_activated IS NOT NULL) )
 AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER,

                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
//                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,

                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,

                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }

    protected function _modelsOld($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
(courier.date_updated >= (NOW() - INTERVAL :past_date_old MONTH) AND courier.date_updated < (NOW() - INTERVAL :past_date DAY)) OR (courier.date_updated IS NULL AND courier.date_entered >= (NOW() - INTERVAL :past_date MONTH) AND courier.date_entered < (NOW() - INTERVAL :past_date DAY))
	)
AND (courier.courier_type != :courier_type OR (courier.courier_type = :courier_type AND ((
courier.date_updated < (NOW() - INTERVAL :past_date_returns DAY)) OR courier.date_entered < (NOW() - INTERVAL :past_date_returns DAY))
	)) 
 AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 7 DAY))
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2 AND courier.stat_map_id != :ignored_map3
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER,
                    ':past_date_returns' => Courier::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS,
                    ':past_date_old' => 6,

//                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
//                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,

                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,

                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':ignored_map3' => StatMap::MAP_RETURN,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }


    protected function _modelsReturnNotActivated($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0, $instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_type = :courier_type
AND date_activated IS NULL
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
	(courier.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date2 DAY))
)
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
 AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 12 HOUR))
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':past_date2' => Courier::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS,
//                    ':ttLog_block' => 24,
                    ':type' => TtLog::TYPE_COURIER,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }

    public function actionRun($type, $mod = 0, $nextStep = false)
    {

        echo 'START';
        echo "\r\n";
        echo 'TYPE: ' . $type;
        echo "\r\n";
        echo 'MOD: ' . $mod;
        echo "\r\n";
        echo 'NEXT STEP: ' . ($nextStep ? 'T' : 'N');
        echo "\r\n";
        echo "\r\n";

        $filename = 'cmd_courierTt_log_' . $type.'.txt';

        $start_time = time();
        $limit_90 = 0.9 * self::TIMEOUT;

        $CACHE_NAME = 'CMD_COURIER_TT' . $type.'_'.$mod;
        $CACHE = Yii::app()->cacheFile;

        MyDump::dump('cmd_courierTt.txt', 'START: T='.$type.' : M='.$mod.' : NS='.$nextStep);

        if (!$nextStep && $CACHE->get($CACHE_NAME)) {
            MyDump::dump('cmd_courierTt.txt', 'AIP! '.$mod.' : '.$type);
            echo 'Already in progress!';
            Yii::app()->end();
        }
        $CACHE->set($CACHE_NAME, 10, $limit_90);


//        sleep(2 * $mod);

        if((date('G') > 23 OR date('G') < 5) && in_array($type, [self::TYPE_MAIN, self::TYPE_OLDER]))
            exit;

        if((date('G') >= 5 AND date('G') <= 23) && in_array($type, [self::TYPE_CANCELLED, self::TYPE_RETURNS]))
            exit;

        if(!$mod)
            exit;

        $start = date('Y-m-d H:i:s');

        $j = 0;
        while (S_SystemLoad::isHigh())
        {
            echo 'HIGH LOAD! Sleep for a while...'."\r\n";
            sleep(60);

            $j++;
            if($j > 10)
            {
                echo 'STILL HIGH LOAD! GOODBYE!'."\r\n";
                MyDump::dump($filename, "\r\n".$start." : [$mod] : HIGH LOAD");
                $CACHE->delete($CACHE_NAME);
                Yii::app()->end();
            }
        }
        echo 'Here we go...'."\r\n";
        
        if($type == self::TYPE_MAIN)
            $couriersNew = self::_modelsMain(self::INSTANCES_MAIN, $mod);
        else if($type == self::TYPE_OLDER)
            $couriersNew = self::_modelsMainSlower(self::INSTANCES_OLDER, $mod);
        else if($type == self::TYPE_CANCELLED)
            $couriersNew = self::_modelsCancelled(self::INSTANCES_CANCELLED, $mod);
        else if($type == self::TYPE_RETURNS)
            $couriersNew = self::_modelsReturnNotActivated(self::INSTANCES_RETURNS, $mod);
        else
            throw new Exception('Unknowny type!');

        $log = $start.' ['.print_r($mod,1).'] : '." --- ". sprintf("%03d",S_Useful::sizeof($couriersNew))." --- ";
        echo 'TO PROCESS '.sizeof($couriersNew)."\r\n";
        
        try {
            /* $item Courier */
            if (is_array($couriersNew))
                foreach ($couriersNew AS $item) {
                    echo 'Process: '.$item->local_id."\r\n";
                    CourierExternalManager::updateStatusForPackage($item);
                    MyDump::dump('cmd_courierTt_full_log_divided.txt', $item->id.' : '.print_r($type,1).' : '.print_r($mod,1));
                    if(time() - $start_time > $limit_90)
                    {
                        MyDump::dump($filename, 'TIMEOUTED : '.print_r($type,1).' : '.print_r($mod,1));
                        $CACHE->set($CACHE_NAME, false);
                        Yii::app()->end();
                    }
                }
        }
        catch(Exception $ex)
        {
            var_dump($ex);
            MyDump::dump('cj_cmd_exception.txt', print_r($ex,1));
            Yii::log('COURIER TT CMD LOG: '.print_r($ex,1), CLogger::LEVEL_ERROR);
            $CACHE->set($CACHE_NAME, false);
            Yii::app()->end();
        }

        // there is propably more
        if(S_Useful::sizeof($couriersNew) == self::STRIP_SIZE)
        {
            MyDump::dump($filename, $log);
            $this->actionRun($type, $mod , true);
        } else {
            MyDump::dump($filename, $log);

            Yii::app()->cache->delete($CACHE_NAME);
            Yii::app()->end();
        }

    }


}