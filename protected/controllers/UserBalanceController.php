<?php
//
//class UserBalanceController extends Controller {
//
//    public function filters()
//    {
//        return array_merge(array(
//            'accessControl', // perform access control for CRUD operations
//            array(
//                'application.filters.HttpsFilter',
//                'bypass' => false,
//            ),
//        ));
//    }
//
//    public function accessRules() {
//        return array(
//            array('allow',
//                'actions'=>array('index'),
//                'users'=>array('@'),
//            ),
//            array('deny',
//                'users'=>array('*'),
//            ),
//        );
//    }
//
//
//
//    public function actionIndex($page = 1, $currency = NULL) {
//
//        if($currency === NULL) {
//            $currency = Yii::app()->user->getModel()->price_currency_id;
//            $currency = PriceCurrency::model()->findByPk($currency);
//            $currency = $currency->short_name;
//        }
//
//        if(!in_array($currency, Currency::listIdsWithNames()))
//            throw new CHttpException(404);
//
//        $this->panelLeftMenuActive = 704;
//
//        $page_size = 50;
//
//        $page = intval($page) - 1;
//
//        $model = new UserBalance();
//        $model->setUser(Yii::app()->user->id);
//        $model->setCurrency($currency);
//
//        if(!Yii::app()->user->getModel()->payment_on_invoice)
//        {
//            $this->render('//site/notify', array(
//                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
//                'header' => Yii::t('user','Bilans'),
//
//            ));
//            exit;
//        }
//
//        $item_count = $model->getItemsCount();
//
//        if($page < 0 OR ($page & $page_size) > $item_count) {
//            $page = 1;
//        }
//
//        $models = $model->getItemsWithBalance($currency, $page,$page_size);
//
//        $this->render('index', array(
//            'model' => $model,
//            'models' => $models,
//            'current_page' => $page,
//            'item_count' => $item_count,
//            'page_size' => $page_size,
//            'currencySelected' => $currency,
//        ));
//    }
//
//
//
//}