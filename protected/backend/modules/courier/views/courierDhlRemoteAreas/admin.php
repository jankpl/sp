<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
	);

?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'courier-addition-list-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'date_entered',
		'date_updated',
		array(
            'name' => 'country_list_id',
            'value' => '$data->countryList->name',
            'filter' => CHtml::listData(CountryList::model()->findAll(), 'id', 'name'),
        ),
        'zip_from',
        'zip_to',
        'zip_txt',
        'price_group',
        array(
            'header' => 'Own Price',
            'value' => '$data->price_id !=""?"yes":"no"',
            'filter' => ['2' => 'No', '1' => 'Yes'],
            'name' => '_own_price',
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update}{view}{delete}',
            'htmlOptions'=> array('style'=>'width:80px'),
        ),
	),
)); ?>