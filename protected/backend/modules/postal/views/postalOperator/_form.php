<?php
/* @var $this PostalOperatorController */
/* @var $model PostalOperator */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'postal-operator-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'label_mode'); ?>
		<?php
		if($model->isNewRecord):
			?>
			<?php echo $form->dropDownList($model,'label_mode', PostalOperator::getStampModeList(), ['data-stamp-mode-selector' => "true"]); ?>
			<?php
		else:
			?>
			<?php echo CHtml::dropDownList('_label_mode', $model->label_mode, PostalOperator::getStampModeList(), ['disabled' => 'disabled', 'data-stamp-mode-selector' => "true"]);?>
			<?php echo $form->hiddenField($model,'label_mode'); ?>
			<?php
		endif;
		?>
		<?php echo $form->error($model,'label_mode'); ?>
	</div>

	<div data-stamp-mode="<?= PostalOperator::LABEL_MODE_WHOLE_EXTERNAL;?>">

		<div class="row">
			<?php echo $form->labelEx($model,'broker_id'); ?>
			<?php echo $form->dropDownList($model,'broker_id', PostalOperator::getBrokerList(false), ['prompt' => '-', 'id' => 'broker-option', 'disabled' => ($model->isNewRecord ? false : true ) ]); ?>
			<?php echo $form->error($model,'broker_id'); ?>
		</div>

		<div class="ooe-details">
			<div class="row">
				<?php echo $form->labelEx($model,'_ooe_login'); ?>
				<?php echo$form->textField($model,'_ooe_login', array('id' => 'ooe_login')) ?>
				<?php echo $form->error($model,'_ooe_login'); ?>
			</div><!-- row -->

			<div class="row">
				<?php echo $form->labelEx($model,'_ooe_account'); ?>
				<?php echo$form->textField($model,'_ooe_account', array('id' => 'ooe_account')) ?>
				<?php echo $form->error($model,'_ooe_account'); ?>
			</div><!-- row -->

			<div class="row">
				<?php echo $form->labelEx($model,'_ooe_pass'); ?>
				<?php echo$form->passwordField($model,'_ooe_pass', array('id' => 'ooe_pass')) ?>
				<?php echo $form->error($model,'_ooe_pass'); ?>
			</div><!-- row -->

			<div class="row">
				<?php echo $form->labelEx($model,'_ooe_service'); ?>
				<?php echo$form->textField($model,'_ooe_service', array('id' => 'ooe_service')) ?>
				<?php echo $form->error($model,'_ooe_service'); ?>
			</div><!-- row -->
		</div>

	</div>

	<div data-stamp-mode="<?= PostalOperator::LABEL_MODE_STAMP;?>">

		<div class="row" id="image-uploader">
			<?php echo $form->labelEx($model,'stamp'); ?>
			<p>Image will be scalled to max. width: <?= PostalOperator::STAMP_MAX_WIDTH;?>px and max. height: <?= PostalOperator::STAMP_MAX_HEIGHT;?>.</p>
			<div class="img-container" style="width: 50%;">
				<div class="drop_zone" id="drop_zone_multi">
					<div class="dz_waiting">
						Przeciągnij tutaj zdjęcie lub kliknij <span class="glyphicon glyphicon-share-alt"></span>
					</div>
					<div class="dz_drop">
						Upuść obrazek <span class="glyphicon glyphicon-arrow-down"></span>
					</div>
				</div>
				<input type="file" data-id="imageLoaderInput" name="imageLoaderInput" style="display: none;" accept="image/gif, image/jpeg, image/jpg, image/png"/>
			</div>
			<?= $form->hiddenField($model, 'stamp', ['data-img-data' => 'true']); ?>
			<?= $form->error($model, 'stamp'); ?>

		</div>
		<p>Preview:</p>
		<div class="row" id="image-preview" style="width: <?= (PostalOperator::STAMP_MAX_WIDTH + 30);?>px; height: <?= (PostalOperator::STAMP_MAX_HEIGHT + 30);?>px;">
		</div>

	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stat'); ?>
		<?php echo $form->dropDownList($model,'stat', CHtml::listData(S_Status::listStats(), 'id', 'name')); ?>
		<?php echo $form->error($model,'stat'); ?>
	</div>


	<p><strong>Return address data:</strong></p>

	<?php


	$this->widget('application.backend.components.AddressDataFrom', array(
		'addressData' => $model->returnAddressData,
		'form' => $form,
		'noEmail' => true,
	));

	?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/dropImage.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/preloader.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/backend/dropImage.css');
Yii::app()->clientScript->registerScript('jsHelpers', '
          jsHelpers = {
              url: {
                  resizeImageUrl: '.CJSON::encode(Yii::app()->createAbsoluteUrl('/postal/postalOperator/ajaxPrepareImage')).',
              },
               text: {
                  imageSaved: '.CJSON::encode('Twój obrazek został zapisany!').',
                  imageSaveError: '.CJSON::encode('Ups, coś się nie udało...').',
                  onlyImagesError: '.CJSON::encode('Dozwolone są jedynie obrazki').',
                  imageTooBigError: '.CJSON::encode('Maksymanla dozwolona waga pliku to: ').',
              },
          };
      ', CClientScript::POS_HEAD);
?>


<script>
	var $brokerOption;
	var $ooeDetails;
	var $stampModeSelector;
	var $stampModeStamp;
	var $stampModeExternal;

	$(document).ready(function(){

		$brokerOption = $("#broker-option");
		$ooeDetails = $(".ooe-details");

		$stampModeSelector = $('[data-stamp-mode-selector]');
		$stampModeStamp = $('[data-stamp-mode="<?= PostalOperator::LABEL_MODE_STAMP;?>"]');
		$stampModeExternal = $('[data-stamp-mode="<?= PostalOperator::LABEL_MODE_WHOLE_EXTERNAL;?>"]');

		ooeToggler();
		$brokerOption.on('change', ooeToggler);

		stampModeToggler();
		$stampModeSelector.on('change', stampModeToggler);

	});

	function ooeToggler()
	{

		if($brokerOption.val() == <?= Postal::POSTAL_BROKER_OOE;?>) {
			$ooeDetails.show();
		}
		else
		{
			$ooeDetails.hide();
		}
	}

	function stampModeToggler()
	{

		if($stampModeSelector.val() == <?= PostalOperator::LABEL_MODE_STAMP;?>) {
			$stampModeStamp.show();
			$stampModeExternal.hide();
		}
		else if($stampModeSelector.val() == <?= PostalOperator::LABEL_MODE_WHOLE_EXTERNAL;?>)
		{
			$stampModeStamp.hide();
			$stampModeExternal.show();
		} else {
			$stampModeStamp.hide();
			$stampModeExternal.hide();
		}

	}

</script>
