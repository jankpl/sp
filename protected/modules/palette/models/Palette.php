<?php

Yii::import('application.modules.palette.models._base.BasePalette');

class Palette extends BasePalette
{
    CONST STAT_NEW = 1;
    CONST STAT_SEEN = 2;
    CONST STAT_CANCELLED = 9;

    public $regulations;
    public $regulations_rodo;

    CONST HEIGHT_UNIT = 'cm';

    public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
        );
    }


    public static function numberOfItemsByStat($stat)
    {
        $models = Palette::model()->findAll('stat=:stat',array(':stat' => $stat));

        return S_Useful::sizeof($models);
    }


    protected function beforeSave()
    {
        if ($this->isNewRecord)
        {
            if($this->user && $this->user->getDisableOrderingNewItems())
                throw new AccountLockedException('Locked!');

            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => $this->user_id.$this->date_entered.microtime()));

        }

        return parent::beforeSave();
    }

    public function rules() {
        return CMap::mergeArray([
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'except'=>'insert'),

            array('stat', 'default',
                'value'=> Palette::STAT_NEW, 'on'=>'insert'),

            array('user_id', 'default',
                'value'=>Yii::app()->user->isGuest?null:Yii::app()->user->id, 'on'=>'insert'),

            array('type', 'required'),
            array('sender_address_data_id, receiver_address_data_id, type', 'numerical', 'integerOnly'=>true),

            array('height', 'numerical', 'integerOnly'=>false, 'min' => 0.01, 'max' => 200),

            array('customer_notes', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('hash', 'length', 'max'=>64),
            array('user_id', 'length', 'max'=>10),
            array('date_updated', 'safe'),
            array('date_updated, notes', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, hash, sender_address_data_id, receiver_address_data_id, notes, type, user_id, height, notes, customer_notes
                stat,
                __stat,
                __user_login,
                __sender_country_id,
                __receiver_country_id','safe', 'on'=>'search'),

        ],
            RodoRegulations::regulationsRules('insert'));
    }

    public function attributeLabels()
    {
        return CMap::mergeArray(
            parent::attributeLabels(),
            RodoRegulations::attributeLabels()
        );

    }


    public static function types()
    {
        $types = Array();
        $types[1] = 'PALETY EURO (120X80)';
        $types[2] = '1/2 PALETY (60X80)';
        $types[3] = '1/4 PALETY (60X40)';
        $types[4] = 'BLOCK PLT (120X100)';
        $types[5] = 'PALETY MODUŁOWE ';
        $types[7] = 'PALETY MAX (120X120)';

        return $types;
    }

    public static function stat()
    {
        $stat = Array();
        $stat[Palette::STAT_NEW] = 'new';
        $stat[Palette::STAT_SEEN] = 'processed';
        $stat[Palette::STAT_CANCELLED] = 'cancelled';

        return $stat;
    }
}