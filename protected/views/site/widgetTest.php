<?php
Yii::app()->clientScript->registerScriptFile('https://wchat.freshchat.com/js/widget.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('FC', '
  window.fcWidget.init({
    token: "f8fb9d92-9198-43b5-aeab-c17185a999fd",
    host: "https://wchat.freshchat.com"
  });
', CClientScript::POS_END);
?>