<?php

Yii::import('application.modules.courier.models._base.BaseCourierStat');

class CourierStat extends BaseCourierStat
{
    const STAT_NAME_MAX_LENGTH = 384;


    const EXPIRATION_DAYS = 90;

    const NEW_ORDER = 1;
    const PACKAGE_RETREIVE = 2;
    const FINISHED = 3;
//    const NEW_ACCEPTED = 4;
    const CANCELLED = 99;

    const CANCELLED_USER = 95;
    const CANCELLED_PROBLEM = 97;

    const PACKAGE_PROCESSING = 50;

    const PACKAGE_PROBLEMS = 90;


    const RETURNED_TO_SENDER = 150;

    const PACKAGE_DELIVERED_TO_HUB = 70;

    const UPS_PACKAGE_ACCEPTED = 297;

    const EXTERNAL_SERVICE_17TRACK = 1;

    const EXTERNAL_SERVICE_UPS = 10;
    const EXTERNAL_SERVICE_OOE = 11;

    const EXTERNAL_SERVICE_INPOST = 20;

    const EXTERNAL_SERVICE_SAFEPAK = 30;

    const EXTERNAL_SERVICE_GLS = 40;

    const EXTERNAL_SERVICE_RUCH = 50;
    const EXTERNAL_SERVICE_PETER_SK = 51;

    const EXTERNAL_SERVICE_DHL = 55;


    const EXTERNAL_SERVICE_POCZTA_POLSKA = 60;

    const EXTERNAL_SERVICE_GLS_NL = 65;

    const EXTERNAL_SERVICE_ORANGE_POST_API = 70;

    const EXTERNAL_SERVICE_DHLDE = 75;
    const EXTERNAL_SERVICE_DHLEXPRESS = 76;
    const EXTERNAL_SERVICE_UK_MAIL = 77;
    const EXTERNAL_SERVICE_DPD_DE = 78;
    const EXTERNAL_SERVICE_ROYAL_MAIL = 79;
    const EXTERNAL_SERVICE_DPDPL = 80;
    const EXTERNAL_SERVICE_HUNGARIAN_POST = 81;
    const EXTERNAL_SERVICE_DPDNL = 82;
    const EXTERNAL_SERVICE_POST11 = 83;
    const EXTERNAL_SERVICE_LP_EXPRESS = 84;

    const STAT_MANIFEST_GENERATED = 14038;
    const STAT_RETURN_LABEL_GENERATED = 14039;
    const STAT_RETURN_MANIFEST_GENERATED = 14041;

    const STAT_MANIFEST_GENERATED_CUSTOMER = 17506;

    const STAT_COD_PAID = 6915;

    public $_map_date;
    public $_map_admin_id;

    protected static function getServicesList(){

        $data = [
            self::EXTERNAL_SERVICE_OOE => 'OOE',
            self::EXTERNAL_SERVICE_UPS => 'UPS',
            self::EXTERNAL_SERVICE_INPOST => 'Inpost',
            self::EXTERNAL_SERVICE_SAFEPAK => 'Safepak',
            self::EXTERNAL_SERVICE_GLS => 'GLS',
            self::EXTERNAL_SERVICE_RUCH => 'Ruch',
            self::EXTERNAL_SERVICE_DHL => 'DHL',
            self::EXTERNAL_SERVICE_PETER_SK => 'KAAB_SK',
            self::EXTERNAL_SERVICE_POCZTA_POLSKA => 'Poczta Polska',
            self::EXTERNAL_SERVICE_GLS_NL => 'GLS NL',
            self::EXTERNAL_SERVICE_DHLDE => 'DHL DE',
            self::EXTERNAL_SERVICE_DHLEXPRESS => 'DHL EXPRESS',
            self::EXTERNAL_SERVICE_UK_MAIL => 'Uk Mail',
            self::EXTERNAL_SERVICE_DPD_DE => 'DPD DE',
            self::EXTERNAL_SERVICE_ROYAL_MAIL => 'Royal Mail',
            self::EXTERNAL_SERVICE_DPDPL => 'DPD PL',
            self::EXTERNAL_SERVICE_HUNGARIAN_POST => 'HUNGARIAN POST',
            self::EXTERNAL_SERVICE_DPDNL => 'DPD NL',
            self::EXTERNAL_SERVICE_POST11 => 'Post11',
            self::EXTERNAL_SERVICE_LP_EXPRESS => 'LP Express',
        ];

        $data = $data + GLOBAL_BROKERS::getBrokersList();

        return $data;
    }

    /*
     * List of known Ids of delivered statuses generated by various operator and saved into system. Used for ignoring T&T updated for packages with this status and for converting this status to "PACKAGE_DELIVERED_TO_HUB" on mutlioperators packages.
     */
    public static function getKnownDeliveredStatuses()
    {
        $data = [
            3,
            552,
            651,
            659,
            929,
            949,
            967,
            984,
            999,
            1115,
            1420,
            1943,
            2039,
            1015,
            5167
        ];

        // DATA FOR TESTING
        //
//        $data = [
//            3,
//            299
//        ];

        return $data;
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=> S_Status::ACTIVE, 'on'=>'insert'),

            array('editable', 'default',
                'value'=> S_Status::ACTIVE, 'on'=>'insert'),

            array('external_service_id', 'default',
                'value'=> NULL),

            array('notify', 'default',
                'value'=> 0),

            array('name', 'required'),
            array('stat, editable, external_service_id, notify', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>384),
            array('id, name, date_entered, date_updated, stat, editable, external_service_id, notify', 'safe', 'on'=>'search'),
        );
    }


    public static function getNameById($id)
    {
        $model = self::model()->findByPk($id);

        return $model->name;
    }

    public static function getExternalServiceNameById($id)
    {
        return isset( self::getServicesList()[$id]) ?  self::getServicesList()[$id] : NULL;
    }

    public static function getExternalServiceNameArray()
    {
        return self::getServicesList();
    }

    /**
     * Function returns list of available stats for all packages of particular user
     * @param null $user_id
     * @return CourierStat[]|NULL
     */
    public static function getStatsForUser($user_id = NULL)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        $models = Yii::app()->cache->get('statsForUser_'.$user_id);
        if($models === false) {

            $cmd = Yii::app()->db->createCommand();
            $cmd->distinct = true;
            $cmd->select('courier_stat_id');
            $cmd->from((new Courier())->tableName());
            $cmd->where('user_id = :user_id AND courier_stat_id IS NOT NULL', [':user_id' => $user_id]);
            $stats = $cmd->queryAll(false);

            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($stats));
            $stats = iterator_to_array($it, false);

            $models = CourierStat::model()->with('courierStatTr')->findAllByAttributes(['id' => $stats], ['order' => 'courierStatTr.short_text ASC, t.name']);
            Yii::app()->cache->set('statsForUser_'.$user_id, $models, 60*60);
        }

        return $models;
    }

    public static function findIdByText($text)
    {

        $cmd = Yii::app()->db->cache(60*15)->createCommand();
        $cmd->select('courier_stat.id');
        $cmd->from('courier_stat');
        $cmd->join('courier_stat_tr', 'courier_stat_tr.courier_stat_id = courier_stat.id');
        $cmd->where('courier_stat.id = :text', array(':text' => $text));
        $cmd->orWhere('courier_stat.name = :text', array(':text' => $text));
        $cmd->orWhere('short_text = :text', array(':text' => $text));
        $cmd->orWhere('full_text = :text', array(':text' => $text));
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if(!$result)
            return NULL;

        return $result;
    }

    public static function convertDate($text)
    {
        if($text != '')

            try
            {
                $date = new DateTime($text);
                $text = $date->format('Y-m-d H:i:s');
            }
            catch(Exception $ex)
            {
                $text = NULL;
            }

        return $text;
    }


    /**
     * @param $name
     * @param $service_id
     * @param bool|false $doNotCreate
     * @param bool|false $convertDeliveryStatus  // if delivered stat generated by external operator is known and this operator if first of two for this package, then convert this status to "DELIVERED_TO_HUB".
     * @return bool|int
     * @throws CDbException
     */
    public static function getOrCreateAndGetId($name, $service_id, $doNotCreate = false, $convertDeliveryStatus = false)
    {
        $stat = self::model()->findByAttributes(array('name' => $name, 'external_service_id' => $service_id));

        if($stat != NULL)
        {
            $stat_id = $stat->id;

            // if delivered stat generated by external operator is known and this operator if first of two for this package, then convert this status to "DELIVERED_TO_HUB".
            if($convertDeliveryStatus && (in_array($stat_id, self::getKnownDeliveredStatuses()) OR StatMapMapping::findMapForStat($stat_id, StatMapMapping::TYPE_COURIER) == StatMap::MAP_DELIVERED))
            {
                $stat_id = CourierStat::PACKAGE_DELIVERED_TO_HUB;
            }

            return $stat_id;
        }
        else
        {
            if($doNotCreate)
                return false;


            // create stat if not found
            if(strlen($name) > self::STAT_NAME_MAX_LENGTH)
                Yii::log('COURIER STAT TOO LONG! ['.$name.']', CLogger::LEVEL_ERROR);

            $name = substr($name,0,self::STAT_NAME_MAX_LENGTH);

            $stat = new self;
            $stat->external_service_id = $service_id;
            $stat->name = $name;
            $stat->stat = 1;
            $stat->editable = 1;

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            if(!$stat->save())
                $errors = true;

            if(!$errors)
                foreach(Language::model()->findAll('stat = 1') AS $item)
                {

                    $modelTrs = new CourierStatTr;

                    $modelTrs->short_text = substr($name,0,32);
                    $modelTrs->full_text = substr($name,0, 512);
                    $modelTrs->courier_stat_id = $stat->id;
                    $modelTrs->language_id = $item->id;

                    if(!$modelTrs->save())
                        $errors = true;
                }



            if (!$errors) {
                $transaction->commit();
                return $stat->id;

            }
            else
            {
                $transaction->rollback();
                return false;
            }
        }
    }


    /**
     * Function for coloring gridView cells by courier stat
     * @return string
     */
    public function getGvItemCssClassByStat()
    {

        if($this->id == CourierStat::NEW_ORDER OR $this->id == CourierStat::PACKAGE_PROCESSING)
            return 'gv-courier-new';
        else if(in_array($this->id, CourierStat::getKnownDeliveredStatuses()))
            return 'gv-courier-finished';
        else if($this->id == CourierStat::CANCELLED OR $this->id == CourierStat::CANCELLED_USER OR $this->id == CourierStat::CANCELLED_PROBLEM)
            return 'gv-courier-cancelled';
        else if($this->statMapMapping->map_id == StatMap::MAP_PROBLEM)
            return 'gv-courier-problem';
        else
            return 'gv-courier-in-progress';
    }

    /**
     * Function for coloring gridView cells by courier stat
     * @return string
     */
    public static function getGvItemCssClassByStatMapId($stat_map_id)
    {

        if($stat_map_id == StatMap::MAP_NEW)
            return 'gv-courier-new';
        else if($stat_map_id == StatMap::MAP_DELIVERED)
            return 'gv-courier-finished';
        else if($stat_map_id == StatMap::MAP_CANCELLED)
            return 'gv-courier-cancelled';
        else if($stat_map_id == StatMap::MAP_PROBLEM)
            return 'gv-courier-problem';
        else
            return 'gv-courier-in-progress';
    }



    /**
     * Method returns shortened version od admins stat name
     * @return string
     */
    public function getAdminStatNameShort()
    {

        $text = substr($this->name,0,64);
        if(strlen($this->name)>64)
            $text = $text.'...';

        return $text;
    }

    /**
     * Method returns shortened version od admins stat name with ID of stat
     * @return string
     */
    public function getAdminStatNameShortWithId()
    {
        $text = $this->id.'] '.$this->getAdminStatNameShort();
        return $text;
    }

    /**
     * Fixed replacement for relation getStatText due to some problems with dynamic language changing for mailing
     * @return array|mixed|null
     */
    public function getStatTextOne()
    {
        return CourierStatTr::model()->findByAttributes(['language_id' => Yii::app()->params['language'], 'courier_stat_id' => $this->id]);
    }

    /**
     * Function tries to return best matched stat description
     * @param bool|false $full
     * @return string
     */
    public function getStatText($full = false)
    {
        $statTr = $this->getStatTextOne();

        if($statTr === NULL)
            return $this->name;
        else
        {
            if($full)
            {
                if($statTr->full_text != '')
                    return $statTr->full_text;
                else
                    return $statTr->short_text;
            } else
                return $statTr->short_text;
        }
    }

    /**
     * Function returns statuses
     *
     * @return self[]
     */
    public static function getStatuses()
    {
        $models = self::model()->findAll(['order' => 'name ASC']);
        return $models;
    }


    public static function expireUnusedOldStats()
    {
        $toArchive = self::model()->findAll('(date_updated IS NOT NULL AND date_updated < (NOW() - INTERVAL :days DAY) OR (date_updated IS NULL AND date_entered < (NOW() - INTERVAL :days DAY))) AND ((notify = 0 AND external_service_id IS NOT NULL) AND t.id NOT IN (SELECT DISTINCT courier_stat_id FROM courier WHERE courier_stat_id IS NOT NULL) AND t.id NOT IN (SELECT DISTINCT previous_stat_id FROM courier_stat_history WHERE previous_stat_id IS NOT NULL) AND t.id NOT IN (SELECT DISTINCT current_stat_id FROM courier_stat_history  WHERE current_stat_id IS NOT NULL)) LIMIT 10000', [
            ':days' => self::EXPIRATION_DAYS,
        ]);


        /* @var $item CourierStatHistory */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {

                if(is_array($item->courierStatTrs))
                    foreach($item->courierStatTrs AS $subitem)
                        $subitem->delete();

                $item->delete();
            }
    }

    /**
     * Returns stat map for stat
     * @return bool|int
     */
    public function getStatMapId()
    {
        return StatMapMapping::findMapForStat($this->id, StatMapMapping::TYPE_COURIER);
    }

    /**
     * Returns stat map for stat
     * @return bool|int
     */
    public function getStatMapName()
    {
        $id = $this->getStatMapId();
        if($id)
            return StatMap::getMapNameById($id);
        else
            return false;

    }

    /**
     * Return list of status. If only popular - only statuses with more than zero packages
     * @param bool|false $onlyPopular
     * @return static[]
     */
    public static function getStats($onlyPopular = false)
    {

        if($onlyPopular)
            $models = self::model()->cache(300)->findAllBySql('SELECT * FROM courier_stat WHERE (SELECT COUNT(id) FROM courier WHERE courier_stat_id = courier_stat.id) > 1');
        else
            $models = self::model()->findAll();


        return $models;
    }

    public function getNameWithId()
    {
        return $this->id.') '.$this->name;
    }


    public function isPlTranslationDifferent()
    {
        if($this->statTrPl && substr($this->statTrPl->full_text, 0, 384) != $this->name)
            return true;
        else
            false;
    }

}