<div class="form">

    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'update-ref-id',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'ref'); ?>
        <?php echo $form->textField($model, 'ref', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'ref'); ?>
    </div><!-- row -->

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->