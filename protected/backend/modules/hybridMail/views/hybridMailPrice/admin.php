<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);


?>

<h1>Manage hybridMail price</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'hybrid-mail-country-group-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'date_updated',
        array(
            'header' => 'Price',
            'value' => '($data->hybridMailPriceHasGroupsSTAT)>0?"yes":"no"',
        ),
        array(
            'header' => 'Group prices',
            'value' => '($data->hybridMailPriceHasGroupsSTATGroup)',
        ),
        array(
            'header' => 'Countries',
            'name' => 'countryListsCOUNT',
            'filter' => false,
        ),


        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        ),
    ),
)); ?>
