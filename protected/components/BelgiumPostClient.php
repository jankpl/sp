<?php

class BelgiumPostClient extends GlobalOperatorWithPostal
{

    const URL = 'https://mercury.landmarkglobal.com/api/api.php';
    const LOGIN = 'RomadAPI';
    const PASSWORD = 'Rom@dLtd1234';
    const TEST_MODE = GLOBAL_CONFIG::BELGIUM_POST_TEST_MODE;
    const CLIENT_ID = 1099;

    const SERVICE_BPOST_INT_EU = 'LGINTBPMP';
    const SERVICE_BPOST_INT_NON_EU = 'LGINTBPMU';

    const SERVICE_BPOST_INT_EU_MINIPACK = 'LGINTBPIP';
    const SERVICE_BPOST_INT_NON_EU_MINIPACK = 'LGINTBPIU';

    const SERVICE_LANDMARK_ROUTED = 'LGINTSTDU';
    const SERVICE_LANDMARK_ROUTED_DDP = 'LGINTSTD';

    public $_service = false;

    public $label_annotation_text = false;

    public $package_value;


    public $receiver_name;
    public $receiver_name2;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_mail;


    public $sender_name;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;
    public $local_id;

    public $packages_number = 1;

    public $package_content;
    public $ref;


    public static function test()
    {
        $model = new self;
        $model->receiver_country = 'PL';
        $model->receiver_name = 'Test Company';
        $model->receiver_address1 = 'Piekna 2';
        $model->receiver_city = 'Rybnik';
        $model->receiver_zip_code = '44-200';
        $model->package_weight = 1;

        $model->sender_country = 'PL';
        $model->sender_name = 'Test Company';
        $model->sender_address1 = 'Testowa 10';
        $model->sender_city = 'Warsaw';
        $model->sender_zip_code = '00-001';

        $model->package_content = 'Test 123';
        $model->local_id = '8191941991981';

        $model->_service = self::SERVICE_LANDMARK_ROUTED;
        var_dump($model->createShipment());
    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    protected function setService($operator_uniq_id, $receiver_country_id)
    {
        if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_BELGIUM_POST_MAXI_SIGN)
        {
            if(in_array($receiver_country_id, CountryList::getUeList()))
                $this->_service = self::SERVICE_BPOST_INT_EU;
            else
                $this->_service = self::SERVICE_BPOST_INT_NON_EU;

        }
        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_BELGIUM_POST_MAXI_SCAN)
            $this->_service = self::SERVICE_LANDMARK_ROUTED;
        else  if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_BELGIUM_POST_MINI_SCAN)
        {
            if(in_array($receiver_country_id, CountryList::getUeList()))
                $this->_service = self::SERVICE_BPOST_INT_EU_MINIPACK;
            else
                $this->_service = self::SERVICE_BPOST_INT_NON_EU_MINIPACK;
        }

    }

    public function createShipment($returnError = false)
    {

        $state = '';
        if(strtoupper($this->receiver_country) == 'US')
            $state = self::_cdata(UsaZipState::getStateByZip($this->receiver_zip_code));
        else if(strtoupper($this->receiver_country) == 'AU')
            $state = self::_cdata(AuZipState::getStateByZip($this->receiver_zip_code));
        else if(strtoupper($this->receiver_country) == 'CA')
            $state = self::_cdata(CanadaZipState::getStateByZip($this->receiver_zip_code));

        $data = '<?xml version="1.0" encoding="UTF-8"?><ImportRequest>
        <Login>
                <Username>'.self::LOGIN.'</Username>
                <Password>'.self::PASSWORD.'</Password>
        </Login>
        <Test>'.self::TEST_MODE.'</Test>  
        <ClientID>'.self::CLIENT_ID.'</ClientID>
        <Reference>'.$this->local_id.'</Reference>
        <ShipTo>
                <Name>'.self::_cdata($this->receiver_name).'</Name>
                <Attention>'.self::_cdata($this->receiver_name2).'</Attention> <!-- Optional -->
                <Address1>'.self::_cdata($this->receiver_address1).'</Address1>
                <Address2>'.self::_cdata($this->receiver_address2).'</Address2> <!-- Optional -->
                <Address3></Address3> <!-- Optional -->
                <City>'.self::_cdata($this->receiver_city).'</City>
                <State>'.$state.'</State>
                <PostalCode>'.self::_cdata($this->receiver_zip_code).'</PostalCode>
                <Country>'.$this->receiver_country.'</Country> <!-- 2 character ISO Code -->
                <Phone>'.self::_cdata($this->receiver_tel).'</Phone> <!-- Optional, but encouraged -->
                <Email>'.self::_cdata($this->receiver_mail).'</Email> <!-- Optional --> 
                <Region>Landmark UK</Region>
                <Residential>true</Residential> <!-- Optional, requests B2C or B2B clearance method. Defaults to \'true\' which clears shipments as B2C.-->
        </ShipTo>        
        <ShipMethod>'.$this->_service.'</ShipMethod>
        <ShipmentInsuranceFreight>20.65</ShipmentInsuranceFreight>   <!-- Amount charged for shipping and any insurance for the whole order. This value does not include the amount charged for purchased items (i.e. Item Unit Value).-->
        <ItemsCurrency>PLN</ItemsCurrency>  <!-- Optional, and only used when client uses multiple currencies.  If used, pass the 3 character ISO 4217 code to represent the currency (i.e. USD, CAD)  -->
        <ProduceLabel>true</ProduceLabel>
        <LabelFormat>PNG</LabelFormat> 
        <LabelEncoding>BASE64</LabelEncoding>       
        <VendorInformation>  
                <VendorName>'.self::_cdata($this->sender_name).'</VendorName>
                <VendorAddress1>'.self::_cdata($this->sender_address1).'</VendorAddress1>
                <VendorAddress2>'.self::_cdata($this->sender_address2).'</VendorAddress2>
                <VendorCity>'.self::_cdata($this->sender_city).'</VendorCity>
                <VendorState></VendorState>
                <VendorPostalCode>'.self::_cdata($this->sender_zip_code).'</VendorPostalCode>
                <VendorCountry>'.$this->sender_country.'</VendorCountry>
        </VendorInformation>    
        <Packages> 
                <Package>                                  
                        <WeightUnit>KG</WeightUnit> 
                        <Weight>'.$this->package_weight.'</Weight>                                    
                        <DimensionsUnit>CM</DimensionsUnit>
                        <Length>'.$this->package_size_l.'</Length>
                        <Width>'.$this->package_size_w.'</Width>
                        <Height>'.$this->package_size_d.'</Height>
                        <PackageReference>'.$this->package_content.'</PackageReference>
                </Package>             
        </Packages>
        <Items>';
        if($this->sender_country_id != $this->receiver_country_id)
            $data .= '<Item>
                        <Sku></Sku>
                        <Quantity>1</Quantity>
                        <UnitPrice>'.$this->package_value.'</UnitPrice> <!-- (USD) -->
                        <Description>'.self::_cdata($this->package_content).'</Description>       
                        <CountryOfOrigin>'.$this->sender_country.'</CountryOfOrigin>
                </Item>';
        $data .= '</Items>
</ImportRequest>';

        $resp = $this->_call($data);

        if(!$resp->success)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->Errors->Error->ErrorCode.' : '.$resp->Errors->Error->ErrorMessage);
            } else {
                return false;
            }
        }
        else if($resp->data->Errors)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->data->Errors->Error->ErrorMessage);
            } else {
                return false;
            }
        }

        $return = [];
        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        $ttNumber = (string) $resp->data->Result->Packages->Package->TrackingNumber;

        $im->readImageBlob(base64_decode($resp->data->Result->Packages->Package->LabelImages->LabelImage));
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

//        if($this->label_annotation_text)
//            LabelAnnotation::createAnnotation($this->label_annotation_text, $im, 1015,730);

        $im->trimImage(0);
        $im->stripImage();


        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);

        return $return;
    }

    public static function getAdditions(Courier $courier)
    {
        $ddpAvailable = false;
        if($courier->getType() == Courier::TYPE_INTERNAL)
        {
            $deliveryOperator = $courier->courierTypeInternal->getDeliveryOperator(true);
            $pickupOperator = $courier->courierTypeInternal->getPickupOperator(true);

            if($deliveryOperator->uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_BELGIUM_POST_MAXI_SCAN OR $pickupOperator->uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_BELGIUM_POST_MAXI_SCAN)
                $ddpAvailable = true;
        }
        else if($courier->getType() == Courier::TYPE_U)
        {
            if($courier->courierTypeU->courierUOperator->uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_BELGIUM_POST_MAXI_SCAN)
                $ddpAvailable = true;
        }

        $CACHE_NAME = 'BELGUIM_POST_ADDITIONS_'.self::getCacheSuffix($courier).'_'.print_r($ddpAvailable,1);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            if($ddpAvailable)
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_BELGIUM_POST_DDP;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;

        $model->setService($uniq_id, $to->country_id);

        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id, GLOBAL_BROKERS::BROKER_BELGIUM_POST);

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;

        $model->receiver_name = $to->company ? $to->company : $to->name;
        $model->receiver_name2 = $to->company ? $to->name : '';
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierInternal->courier->getWeight(true);

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->local_id = $courierInternal->courier->local_id;

        $model->package_value = $courierInternal->courier->getPackageValueConverted('USD');

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_BELGIUM_POST_DDP))
        {
            if($model->_service == self::SERVICE_LANDMARK_ROUTED)
                $model->_service = self::SERVICE_LANDMARK_ROUTED_DDP;
        }


        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;
        $model->setService($courierU->courierUOperator->uniq_id, $courierU->courier->receiverAddressData->country_id);

        $model->label_annotation_text = $courierU->courierUOperator->label_symbol;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;


        $model->receiver_name = $to->company ? $to->company : $to->name;
        $model->receiver_name2 = $to->company ? $to->name : '';
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierU->courier->getWeight(true);

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->local_id = $courierU->courier->local_id;


        $model->package_value = $courierU->courier->getPackageValueConverted('USD');


        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_BELGIUM_POST_DDP))
        {
            if($model->_service == self::SERVICE_LANDMARK_ROUTED)
                $model->_service = self::SERVICE_LANDMARK_ROUTED_DDP;
        }

        return $model->createShipment($returnErrors);
    }


    public static function orderForPostal(Postal $postal, $returnErrors = false)
    {
        $model = new self;

        $model->setService($postal->postalOperator->uniq_id, $postal->receiverAddressData->country_id);

        $from = $postal->senderAddressData;
        $to = $postal->receiverAddressData;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;

        $model->receiver_name = $to->company ? $to->company : $to->name;
        $model->receiver_name2 = $to->company ? $to->name : '';
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $weight = round($postal->weight / 1000,3);
        $weight = $weight < 0.001 ? 0.001 : $weight;

        $model->package_weight = $weight;

        $model->package_size_l = 10;
        $model->package_size_d = 10;
        $model->package_size_w = 10;

        $model->package_content = $postal->content;
        $model->local_id = $postal->local_id;

        $model->package_value = $postal->getValueConverted('USD');

        return $model->createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return self::_track($no);
    }

    public static function trackForPostal($no)
    {
        return self::_track($no);
    }

    protected static function _track($no)
    {
        $data = '<?xml version="1.0" encoding="UTF-8"?>
        <TrackRequest>
        <Login>
                <Username>'.self::LOGIN.'</Username>
                <Password>'.self::PASSWORD.'</Password>
        </Login>
        <Test>'.self::TEST_MODE.'</Test>  
        <ClientID>'.self::CLIENT_ID.'</ClientID>
        <Reference/>
        <TrackingNumber>'.$no.'</TrackingNumber>
        <PackageReference/>
        <RetrievalType>Historical</RetrievalType>
        </TrackRequest>';

        $model = new self;

        $resp = $model->_call($data);

        $statuses = new GlobalOperatorTtResponseCollection();

        if($resp->success && $resp->data->Result->Success == 'true')
        {
            $relabelingDate = false;

            foreach($resp->data->Result->Packages->Package->Events->Event AS $item)
            {
                $dateTime = (string) $item->DateTime.'-0600';
                $date = new DateTime($dateTime);
                $date->setTimezone(new DateTimeZone('Europe/London'));

                $statuses->addItem((string) $item->Status, $date->format('Y-m-d H:i:s'), (string) $item->Location);

                if((string) $item->EventCode == 225 && !$relabelingDate) {
                    $relabelingDate = $date->getTimestamp();
                    $relabelingDate += 1;
                }
            }

            if($relabelingDate && $resp->data->Result->Packages->Package->TrackingNumber)
                $statuses->addItem('Your package has been re-labelled. The new tracking number is assigned.', date('Y-m-d H:i:s', $relabelingDate), (string) $resp->data->Result->Packages->Package->TrackingNumber);


        }

//        if(0 && $resp->data->Result->Packages->Package->TrackingNumber)
//        {
//            $additional = @file_get_contents('http://www.post.be/bis/parcels/'.$resp->data->Result->Packages->Package->TrackingNumber);
//
//            if($additional)
//            {
//                $additionalXml = simplexml_load_string($additional);
//
//                if($additionalXml->parcel->deliveryStatus)
//                    foreach($additionalXml->parcel->deliveryStatus->status AS $item)
//                    {
//                        $date = (string) $item->dateTime;
//                        $name = (string) $item->status->en;
//
//                        $date = substr($date, 6,4).'-'.substr($date,3,2).'-'.substr($date,0,2).' '.substr($date,11,8);
//                        $statuses->addItem($name, $date, '');
//                    }
//            }
//
//        }

        return $statuses;
    }


    protected function _call($data)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($ch);

        MyDump::dump('belgium_post.txt', print_r($data, 1));
        MyDump::dump('belgium_post.txt', print_r($response, 1));


        // convertingc to XML
        $response = @simplexml_load_string($response);

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        if(curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200)
        {
            $error = curl_error($ch);

            if($error != '')
                $error .= ' : ';

            $error .= $response->Body->Fault->faultcode .' : '.$response->Body->Fault->detail->CifException->Errors->ExceptionData->ErrorMsg;

            $return->error = $error != '' ? $error : 'Unknown error!';
            $return->errorCode = '';
            $return->errorDesc = $error != '' ? $error : 'Unknown error!';
        }
        else
        {
            $return->success = true;
            $return->data = $response;
        }

        curl_close($ch);

        return $return;

    }

}