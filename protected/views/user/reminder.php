<?php
/* @var $model F_UserReminder */
?>

<div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'reminder-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'login'); ?>
        <?php echo $form->textField($model,'login'); ?>
        <?php echo $form->error($model,'login'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <?php if(CCaptcha::checkRequirements()): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'verifyCode'); ?>
            <?php $this->widget('CCaptcha',  array('buttonOptions' => array('style' => 'display:block; margin-left: 255px;'))); ?>

        </div>
        <div class="row">
            <?php echo $form->labelEx($model,''); ?>
            <?php echo $form->textField($model,'verifyCode'); ?>
            <?php echo $form->error($model,'verifyCode'); ?>
        </div>
        <div class="row hint" style="margin-left: 255px;"><?php echo Yii::t('form','Proszę przepisać powyższe litery. Wielkość liter nie jest ważna.');?><br/><br/></div>
    <?php endif; ?>

    <div class="row buttons" style="text-align: center;">
        <?php echo TbHtml::submitButton(Yii::t('form','Dalej')); ?>
    </div>

    <div class="navigate">

    </div>
    <?php if(isset($model->returnURL)) echo CHtml::hiddenField('returnURL',$model->returnURL); ?>



    <?php $this->endWidget(); ?>
</div><!-- form -->