<div class="alert alert-info"><?php $this->widget('ShowPageContent', ['id' => 24]);?></div>

<?php echo CHtml::link(Yii::t('courier','Utwórz nową przesyłkę'),array('/courier/courier/create'), array('class' => 'btn btn-primary')); ?>

<?php if(Yii::app()->user->getModel()->isPremium):?>

    <?php echo CHtml::link(Yii::t('courier','Importuj przesyłki z pliku'),array('/courier/courierInternal/import'), array('class' => 'btn')); ?>

    <?php echo CHtml::link(Yii::t('courier','Importuj przesyłki z eBay.com'),array('/courier/courierInternal/ebay'), array('class' => 'btn')); ?>

    <br/>
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'export-to-file-internal',
        'action' => Yii::app()->createUrl('/courier/courier/exportToFileAll', ['mode' => Controller::EXPORT_MODE_INTERNAL]),
        'htmlOptions' => [
            'class' => 'do-not-block',
        ]
    ));
    ?>
    <hr/>
    <?php
    echo TbHtml::submitButton(Yii::t('courier','Eksportuj wszyskie przesyłki do .xls').' :', ['style' => 'margin-top: -6px; margin-right: 5px;']);
    echo Chtml::dropDownList('period', '', S_Useful::listPeriods(), ['style' => 'height: 30px;']);
    $this->endWidget();
    ?>
<?php endif; ?>
