<?php
/*@ var $model AddressData */
?>


<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'return-address-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('/user/update', ['id' => intval($_GET['id'])]).'#tab_3',
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <h3>Return address data:</h3>

    <?php


    $this->widget('application.backend.components.AddressDataFrom', array(
        'addressData' => $model,
        'form' => $form,
        'noEmail' => true,
    ));
    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'), ['name' => 'saveReturnAddressData']);
    $this->endWidget();
    ?>
</div><!-- form -->