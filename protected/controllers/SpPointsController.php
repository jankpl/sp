<?php

class SpPointsController extends Controller
{

    public function actionAjaxGetSpPointDetails()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $id = intval($_POST['id']);

            $model = SpPoints::getPoint($id);

            $html = '';
            if($model != NULL) {
                $html = $this->renderPartial('_ajaxGetSpPointDetails', ['model' => $model], true);
            }

            echo CJSON::encode($html);

        } else
            $this->redirect(['/']);
    }



}