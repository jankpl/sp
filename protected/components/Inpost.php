<?php

class Inpost
{
    const SERVICE_ECO = 38;
    const SERVICE_EX_17 = 50;
    const SERVICE_EX_12 = 41;

    const ALT_ACCOUNT_FMS = 2;

    protected $location = GLOBAL_CONFIG::INPOST_LOCATION;
    protected $url = GLOBAL_CONFIG::INPOST_URL;
    protected $password = GLOBAL_CONFIG::INPOST_PASSWORD;
    protected $username = GLOBAL_CONFIG::INPOST_LOGIN;

    protected $cod_bank_account = "26105014901000009137775475";

    protected $service_id;

    public $_sender_name;
    public $_sender_person;
    public $_sender_contact_tel;
    public $_sender_address;
    public $_sender_city;
    public $_sender_postalCode;
    public $_sender_countryCode;
    public $_sender_email;
    public $_sender_is_private;

    public $_receiver_name;
    public $_receiver_person;
    public $_receiver_contact_tel;
    public $_receiver_address;
    public $_receiver_city;
    public $_receiver_postalCode;
    public $_receiver_countryCode;
    public $_receiver_email;
    public $_receiver_is_private;


    public $_package_size_d;
    public $_package_size_w;
    public $_package_size_s;
    public $_package_weight;

    public $_package_desc;

    public $_cod_account_no;
    public $_cod_amount;

    public $_cod_currency;

    public $_insurance;

    public $_number_of_packages = 1;

    public $_packages_no_array;
    public $_total_weight;
    public $_total_quantity;

    public $options = [];

    protected function setAltAccount($account_id)
    {
        if($account_id == self::ALT_ACCOUNT_FMS)
        {
            $this->username = 'klient@fulfillmentms.pl';
            $this->password = 'Klient1234#';
        } else
            throw new Exception('Unknown INPOST alternative ACCOUNT!');
    }

    protected static function zipCodeConverter($zip_code, $country_code)
    {
        if(strtoupper($country_code) == 'PL')
        {
            $zip_code = trim($zip_code);
            if(is_numeric($zip_code) && strlen(strval($zip_code)) == '5')
            {
                $zip_code = strval($zip_code);
                $zip_code = $zip_code[0].$zip_code[1].'-'.$zip_code[2].$zip_code[3].$zip_code[4];
            }
        }

        return $zip_code;
    }


    // TRACK PACKAGE BY ID
    public function track($id)
    {

        try {
            $client  = $this->initSoap();
            $result = $this->callSoapFunction( $client, 'getTracking', array( 'consignmentinformation' => $id ) );
        }
        catch(Exception $ex)
        {
            return false;
        }

        if($result == '')
            return false;

        if(substr($result,0,5) == 'ERROR')
            return false;
        else {

            if($result == '')
                return false;

            // extract data from HTML table to array
            $doc = new DOMDocument();
            libxml_use_internal_errors(true);
            $doc->loadHTML('<?xml encoding="UTF-8">' . $result);

            $doc = $doc->getElementsByTagName('table');

            $table = $doc->item(1);
            if($table === NULL)
                $table = $doc->item(0);


            $doc = $table;

            if($doc === NULL)
                return false;

            $data = [];
            $first = true;

            if(($doc->getElementsByTagName('tr')))
                foreach($doc->getElementsByTagName('tr') AS $td)
                {

                    // omit header
                    if($first)
                    {
                        $first = false;
                        continue;
                    }

                    $date = S_Useful::trimBetter($td->getElementsByTagName('td')->item(0)->nodeValue);
                    $location = S_Useful::trimBetter($td->getElementsByTagName('td')->item(1)->nodeValue);
                    $information = S_Useful::trimBetter($td->getElementsByTagName('td')->item(2)->nodeValue);


                    // sometimes data is in 3rd column
                    if(trim($information) == '')
                    {
                        $temp = S_Useful::trimBetter($td->getElementsByTagName('td')->item(3)->nodeValue);
                        if(trim($temp) != '')
                            $information = $temp;
                    }

                    $name = $information;

                    array_push($data, array('date' => $date, 'location' => $location, 'name' => $name));
                }

            return $data;
        }

    }

    public static function getServicesForCourierTypeDomestic(CourierTypeDomestic $courierDomestic, $returnError = false)
    {
        $model = new self;

        $from = $courierDomestic->courier->senderAddressData;
        $to = $courierDomestic->courier->receiverAddressData;

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_cod_amount = 0;
        if($courierDomestic->courier->cod) {
            $model->_cod_amount = $courierDomestic->courier->cod_value;
            $model->_cod_currency = $courierDomestic->courier->cod_currency;
        }

        $model->_package_size_d = $courierDomestic->courier->package_size_d;
        $model->_package_size_w = $courierDomestic->courier->package_size_l;
        $model->_package_size_s = $courierDomestic->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierDomestic->courier->package_weight;

        $model->_number_of_packages = $courierDomestic->courier->packages_number;

        return $model->getAvailableServices($returnError);
    }

    public static function getServicesForCourierTypeInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $returnError = false)
    {
        $model = new self;

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_cod_amount = 0;
        if($courierInternal->courier->cod) {
            $model->_cod_amount = $courierInternal->courier->cod_value;
            $model->_cod_currency = $courierInternal->courier->cod_currency;
        }

        $model->_package_size_d = $courierInternal->courier->package_size_d;
        $model->_package_size_w = $courierInternal->courier->package_size_l;
        $model->_package_size_s = $courierInternal->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierInternal->courier->package_weight;

        $model->_number_of_packages = $courierInternal->courier->packages_number;

        return $model->getAvailableServices($returnError);
    }

    public static function getServicesForCourierTypeU(CourierTypeU $courierU, $returnError = false)
    {
        $model = new self;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_cod_amount = 0;
        if($courierU->courier->cod) {
            $model->_cod_amount = $courierU->courier->cod_value;
            $model->_cod_currency = $courierU->courier->cod_currency;
        }


        $model->_package_size_d = $courierU->courier->package_size_d;
        $model->_package_size_w = $courierU->courier->package_size_l;
        $model->_package_size_s = $courierU->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierU->courier->package_weight;

        $model->_number_of_packages = $courierU->courier->packages_number;

        return $model->getAvailableServices($returnError);
    }

    protected function _soapCall($data, $requestName, $operationName)
    {
        $return = new stdClass();
        $return->success = true;

        $token = new stdClass;
        $token->UserName = $this->username;
        $token->Password = $this->password;

        $mode = array
        (
            'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
            'trace' => 1,
            "stream_context" => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                    )
                )
            )
        );

        try {
            $client = new SoapClient($this->url, $mode);
            $client->__setLocation($this->location);

            $data = array('token' => $token, $requestName => $data);

            //get response
            $resp = $client->$operationName($data);
        }
        catch (Exception $ex)
        {
            MyDump::dump('inpost.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('inpost.txt', print_r($client->__getLastResponse(),1));


            $return->success = false;
            $return->error = $ex->getMessage();
        }

        if($operationName != 'GetTracking')
        {
            MyDump::dump('inpost.txt', print_r($client->__getLastRequest(), 1));
            MyDump::dump('inpost.txt', print_r($client->__getLastResponse(), 1));
        }

        $return->data = $resp;

        return $return;
    }

    protected function getAvailableServices($returnError = false)
    {
        $this->setAltAccount(self::ALT_ACCOUNT_FMS);

        $requestData = new stdClass();

        $requestData->ShipFrom = new stdClass;
        $requestData->ShipFrom->Name = $this->_sender_name;
        $requestData->ShipFrom->Address = $this->_sender_address;
        $requestData->ShipFrom->City = $this->_sender_city;
        $requestData->ShipFrom->PostCode = self::zipCodeConverter($this->_sender_postalCode, $this->_sender_countryCode);
        $requestData->ShipFrom->CountryCode = $this->_sender_countryCode;
        $requestData->ShipFrom->Person = $this->_sender_person;
        $requestData->ShipFrom->Contact = $this->_sender_contact_tel;
        $requestData->ShipFrom->Email = $this->_sender_email;
        $requestData->ShipFrom->IsPrivatePerson = $this->_sender_is_private;

        $requestData->ShipTo = new stdClass;
        $requestData->ShipTo->Name = $this->_receiver_name;
        $requestData->ShipTo->Address = $this->_receiver_address;
        $requestData->ShipTo->City = $this->_receiver_city;
        $requestData->ShipTo->PostCode = self::zipCodeConverter($this->_receiver_postalCode, $this->_receiver_countryCode);
        $requestData->ShipTo->CountryCode = $this->_receiver_countryCode;
        $requestData->ShipTo->Person = $this->_receiver_person;
        $requestData->ShipTo->Contact = $this->_receiver_contact_tel;
        $requestData->ShipTo->Email = $this->_receiver_email;
        $requestData->ShipTo->IsPrivatePerson = $this->_receiver_is_private;

        for($i = 0; $i < $this->_number_of_packages; $i++) {
            $requestData->Parcels[$i] = new stdClass;
            $requestData->Parcels[$i]->Type = 'Package';
            $requestData->Parcels[$i]->Weight = $this->_package_weight;
            $requestData->Parcels[$i]->D = $this->_package_size_d;
            $requestData->Parcels[$i]->W = $this->_package_size_w;
            $requestData->Parcels[$i]->S = $this->_package_size_s;
        }


//        $requestData->AdditionalServices[0] = new stdClass();
//        $requestData->AdditionalServices[0]->Code = 'SMS';

        $requestData->COD = new stdClass();
        $requestData->COD->Amount = $this->_cod_amount;
        $requestData->COD->RetAccountNo = $this->cod_bank_account;

        $requestData->InsuranceAmount = 0;

        $datetime = new DateTime('now',new DateTimeZone('Europe/Warsaw'));
        $requestData->ReadyDate = substr($datetime->format(DateTime::ISO8601), 0,19);

        $return = [];


        $resp = $this->_soapCall($requestData, 'getAvailableServicesRequest', 'GetAvailableServices');
        if(!$resp->success)
        {
            if($returnError)
            {
                $return = array(
                    'error' => $resp->error,
                );
                return $return;
            } else {
                return false;
            }
        }
        $resp = $resp->data;

        $resp = $resp->GetAvailableServicesResult;

        if($resp->responseDescription == 'Success')
        {
            if(is_array($resp->AvailableServices->Service))
            {
                foreach ($resp->AvailableServices->Service AS $item) {

                    $temp = [];
                    $temp['service_id'] = $item->ID;
                    $temp['service_name'] = $item->Name;
                    $return[$item->ID] = $temp;
                }
            } else {
                $temp = [];

                $temp['service_id'] = $resp->AvailableServices->Service->ID;
                $temp['service_name'] = $resp->AvailableServices->Service->Name;
                $return[$resp->AvailableServices->Service->ID] = $temp;
            }

        } else {
            if($returnError)
            {
                $return = array(0 => array(
                    'error' => $resp->responseDescription .'('.$resp->ResponseCode.')'));
                return $return;
            } else {
                return false;
            }
        }

        return $return;


    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $returnError = false, $internalMode = false, $deliveryHubId = false, $operatorUniqId = false)
    {
        $model = new self;

        if($operatorUniqId == CourierOperator::UNIQID_INPOST_FMS)
            $model->setAltAccount(self::ALT_ACCOUNT_FMS);

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_cod_amount = 0;
        if($courierInternal->courier->cod) {
            $model->_cod_amount = $courierInternal->courier->cod_value;
            $model->_cod_currency = $courierInternal->courier->cod_currency;
        }

        $model->_package_desc = $courierInternal->courier->package_content;

        $model->_package_size_d = $courierInternal->courier->package_size_d;
        $model->_package_size_w = $courierInternal->courier->package_size_l;
        $model->_package_size_s = $courierInternal->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierInternal->courier->getWeight(true);

        $model->_number_of_packages = $courierInternal->courier->packages_number;

        if($courierInternal->hasAdditionById(CourierAdditionList::INPOST_EXPRESS_12))
            $model->service_id = self::SERVICE_EX_12;
        else if($courierInternal->hasAdditionById(CourierAdditionList::INPOST_EXPRESS_17))
            $model->service_id = self::SERVICE_EX_17;
        else
            $model->service_id = self::SERVICE_ECO;

        if($courierInternal->hasAdditionById(CourierAdditionList::INPOST_INSURANCE_5000) OR $courierInternal->hasAdditionById(CourierAdditionList::INPOST_INSURANCE_1000))
            $model->_insurance = 5000;
        else if($courierInternal->hasAdditionById(CourierAdditionList::INPOST_INSURANCE_10000))
            $model->_insurance = 10000;
        else if($courierInternal->hasAdditionById(CourierAdditionList::INPOST_INSURANCE_20000))
            $model->_insurance = 20000;
//        else if($internalMode == CourierLabelNew::INTERNAL_MODE_FIRST OR $internalMode == CourierLabelNew::INTERNAL_MODE_COMMON) // DEFAULT INSURENCE FOR COURIER INTERNAL
//            $model->_insurance = 5000;

        if($courierInternal->hasAdditionById(CourierAdditionList::INPOST_ROD))
            array_push($model->options, 'ROD');


        // SPECIAL RULE - IF PACKAGE IS GOING TO HUB_PL AND WEIGHT IS > 30, SET WEIGHT TO 30 IN REQUEST
        // ALSO IN COLLECT SERVICE!
        if($deliveryHubId == CourierHub::HUB_PL && $courierInternal->courier->package_weight > 30)
            $model->_package_weight = 30;

        return $model->shipmentRequest($returnError);
    }

    public static function orderForCourierDomestic(CourierTypeDomestic $courierDomestic, $returnError = false)
    {
        $model = new self;

        $from = $courierDomestic->courier->senderAddressData;
        $to = $courierDomestic->courier->receiverAddressData;

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == ''  ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_cod_amount = 0;
        if($courierDomestic->courier->cod_value > 0) {
            $model->_cod_amount = $courierDomestic->courier->cod_value;
            $model->_cod_currency = $courierDomestic->courier->cod_currency;
        }

        $model->_package_desc = $courierDomestic->courier->package_content;

        $model->_package_size_d = $courierDomestic->courier->package_size_d;
        $model->_package_size_w = $courierDomestic->courier->package_size_l;
        $model->_package_size_s = $courierDomestic->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierDomestic->courier->package_weight;

        $model->_number_of_packages = $courierDomestic->courier->packages_number;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::INPOST_EXPRESS_12))
            $model->service_id = self::SERVICE_EX_12;
        else if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::INPOST_EXPRESS_17))
            $model->service_id = self::SERVICE_EX_17;
        else
            $model->service_id = self::SERVICE_ECO;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::INPOST_INSURANCE_5000))
            $model->_insurance = 5000;
        else if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::INPOST_INSURANCE_10000))
            $model->_insurance = 10000;
        else if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::INPOST_INSURANCE_20000))
            $model->_insurance = 20000;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::INPOST_ROD))
            array_push($model->options, 'ROD');


        return $model->shipmentRequest($returnError);
    }


    public static function orderForCourierU(CourierTypeU $courierU, $returnError = false)
    {
        $model = new self;

        if($courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_INPOST_FMS)
            $model->setAltAccount(self::ALT_ACCOUNT_FMS);

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->_sender_address = trim($from->address_line_1.' '.$from->address_line_2);
        $model->_sender_city = $from->city;
        $model->_sender_contact_tel = $from->tel;
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_email = $from->fetchEmailAddress();
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_is_private = $from->company == '' ? true : false;
        $model->_sender_name = $from->company == '' ? $from->name : $from->company;
        $model->_sender_person = $from->name == '' ? $from->company : $from->name;

        $model->_receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->_receiver_city = $to->city;
        $model->_receiver_contact_tel = $to->tel;
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_email = $to->fetchEmailAddress();
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_is_private = $to->company == '' ? true : false;
        $model->_receiver_name = $to->company == '' ? $to->name : $to->company;
        $model->_receiver_person = $to->name == '' ? $to->company : $to->name;

        $model->_cod_amount = 0;
        if($courierU->courier->cod_value > 0) {
            $model->_cod_amount = $courierU->courier->cod_value;
            $model->_cod_currency = $courierU->courier->cod_currency;
        }

        $model->_package_desc = $courierU->courier->package_content;

        $model->_package_size_d = $courierU->courier->package_size_d;
        $model->_package_size_w = $courierU->courier->package_size_l;
        $model->_package_size_s = $courierU->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierU->courier->package_weight;

        $model->_number_of_packages = $courierU->courier->packages_number;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_INPOST_EXPRESS_12))
            $model->service_id = self::SERVICE_EX_12;
        else if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_INPOST_EXPRESS_17))
            $model->service_id = self::SERVICE_EX_17;
        else
            $model->service_id = self::SERVICE_ECO;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_INPOST_INSURANCE_5000) OR $courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_INPOST_INSURANCE_1000))
            $model->_insurance = 5000;
        else if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_INPOST_INSURANCE_10000))
            $model->_insurance = 10000;
        else if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_INPOST_INSURANCE_20000))
            $model->_insurance = 20000;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_INPOST_ROD))
            array_push($model->options, 'ROD');


        return $model->shipmentRequest($returnError);
    }

    protected function shipmentRequest($returnError = false)
    {

        $requestData = new stdClass();

        $requestData->ServiceId = $this->service_id;

        $requestData->ShipFrom = new stdClass;
        $requestData->ShipFrom->Name = $this->_sender_name;
        $requestData->ShipFrom->Address = $this->_sender_address;
        $requestData->ShipFrom->City = $this->_sender_city;
        $requestData->ShipFrom->PostCode = self::zipCodeConverter($this->_sender_postalCode, $this->_sender_countryCode);
        $requestData->ShipFrom->CountryCode = $this->_sender_countryCode;
        $requestData->ShipFrom->Person = $this->_sender_person;
        $requestData->ShipFrom->Contact = $this->_sender_contact_tel;
        $requestData->ShipFrom->Email = $this->_sender_email;
        $requestData->ShipFrom->IsPrivatePerson = $this->_sender_is_private;

        $requestData->ShipTo = new stdClass;
        $requestData->ShipTo->Name = $this->_receiver_name;
        $requestData->ShipTo->Address = $this->_receiver_address;
        $requestData->ShipTo->City = $this->_receiver_city;
        $requestData->ShipTo->PostCode = self::zipCodeConverter($this->_receiver_postalCode, $this->_receiver_countryCode);
        $requestData->ShipTo->CountryCode = $this->_receiver_countryCode;
        $requestData->ShipTo->Person = $this->_receiver_person;
        $requestData->ShipTo->Contact = $this->_receiver_contact_tel;
        $requestData->ShipTo->Email = $this->_receiver_email;
        $requestData->ShipTo->IsPrivatePerson = $this->_receiver_is_private;

        for($i = 0; $i < $this->_number_of_packages; $i++) {
            $requestData->Parcels[$i] = new stdClass;
            $requestData->Parcels[$i]->Type = 'Package';
            $requestData->Parcels[$i]->Weight = $this->_package_weight;
            $requestData->Parcels[$i]->D = $this->_package_size_d;
            $requestData->Parcels[$i]->W = $this->_package_size_w;
            $requestData->Parcels[$i]->S = $this->_package_size_s;
        }
//
        $j = 0;
        if(is_array($this->options) && S_Useful::sizeof($this->options))
            foreach($this->options AS $key => $item)
            {
                $requestData->AdditionalServices[$j] = new stdClass();
                $requestData->AdditionalServices[$j]->Code = $item;
                $j++;
            }

        $requestData->COD = new stdClass();
        $requestData->COD->Amount = $this->_cod_amount;
        $requestData->COD->RetAccountNo = $this->cod_bank_account;

        if($this->_cod_amount > 0)
        {
            if(!CourierCodAvailability::compareCurrences($this->_cod_currency, CourierCodAvailability::CURR_PLN)) {
                if ($returnError) {
                    $return = array(0 => array(
                        'error' => 'Incorrect COD currency! Only PLN allowed!'
                    ));
                    return $return;
                } else {
                    return false;
                }
            }
        }


        // insurance is at least cod value
        if($this->_cod_amount > 0 && intval($this->_insurance) <= 0)
            $this->_insurance = $this->_cod_amount;

        $requestData->InsuranceAmount = $this->_insurance;

        $requestData->LabelFormat = 'PDF';

        $requestData->ContentDescription = $this->_package_desc;

//        $data->ShipmentRequest = $requestData;

        MyDump::dump('inpost.txt', 'CREATE SHIPMENT: '.print_r($requestData,1));


        $resp = $this->_soapCall($requestData, 'ShipmentRequest', 'CreateShipment', $returnError);
        if(!$resp->success)
        {
            if($returnError)
            {
                $return = array(
                    'error' => $resp->error,
                );
                return $return;
            } else {
                return false;
            }
        }
        $resp = $resp->data;


        $return = [];

        $resp = $resp->CreateShipmentResult;

        if($resp->responseDescription == 'Success')
        {
            if(is_array($resp->ParcelData->Label))
            {
                foreach ($resp->ParcelData->Label AS $item) {

                    $temp = [];
                    $temp['status'] = true;
                    $temp['track_id'] = $item->ParcelID;
                    $temp['label'] = $item->MimeData;
                    array_push($return, $temp);
                }
            } else {
                $temp = [];
                $temp['status'] = true;
                $temp['track_id'] = $resp->ParcelData->Label->ParcelID;
                $temp['label'] = $resp->ParcelData->Label->MimeData;
                array_push($return, $temp);
            }

        } else {
            if($returnError)
            {
                $return = array(0 => array(
                    'error' => $resp->responseDescription .'('.$resp->ResponseCode.')'));
                return $return;
            } else {
                return false;
            }
        }

        return $return;


    }


    public static function quequeCollectionForCourier(Courier $courier)
    {
        // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
        if($courier->courierTypeInternal != NULL && $courier->courierTypeInternal->parent_courier_id != NULL)
            return true;

        // For Type Internal do not call if without pickup option
        // PICKUP_MOD / 14.09.2016
//        if($courier->courierTypeInternal != NULL && !$courier->courierTypeInternal->with_pickup)
        if($courier->courierTypeInternal != NULL && $courier->courierTypeInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR)
            return true;

        // For Type Internal Simple do not call if withous pickup option
        if($courier->courierTypeInternalSimple != NULL && !$courier->courierTypeInternalSimple->with_pickup)
            return true;


        // For Type U
        if($courier->courierTypeU != NULL && !$courier->courierTypeU->collection)
            return true;

        // DO NOT CALL COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
        if($courier->user != NULL )
        {

            if($courier->getType() == Courier::TYPE_DOMESTIC)
            {
                if ($courier->user->getDomesticCollectBlock())
                    return true;
            }
            else
            {
                // SPECIAL RULE 18.11.2016
                // ALWAYS CALL COLLECT FOR USER "PPHUKIK" (ID = 554) WHEN PACKAGE IS INTERNAL, PL->PL
                if($courier->user_id != 554 OR $courier->getType() != Courier::TYPE_INTERNAL OR $courier->senderAddressData->country_id != CountryList::COUNTRY_PL OR $courier->receiverAddressData->country_id != CountryList::COUNTRY_PL)
                {
                    // END OF SPECIAL RULE


                    if($courier->user->getUpsCollectBlock())
                        return true;

                }
            }
        }

        $courierCollect = new CourierCollect();
        $courierCollect->courier_id = $courier->id;
        $courierCollect->stat =  CourierCollect::STAT_WAITING;
        $courierCollect->collect_operator_id = CourierCollect::OPERATOR_INPOST;
        return $courierCollect->save();
    }


    public static function runCollectionForCourierInternal(CourierTypeInternal $courierInternal)
    {

        // service only for orders with pickup
        // PICKUP_MOD / 14.09.2016
//        if(!$courier->with_pickup)
        if($courierInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR)
            return false;


        $address = $courierInternal->courier->senderAddressData;

        $model = new self;

        if(CourierOperator::getUniqIdById($courierInternal->pickup_operator_id) == CourierOperator::UNIQID_INPOST_FMS)
            $model->setAltAccount(self::ALT_ACCOUNT_FMS);

        $model->_sender_address = trim($address->address_line_1.' '.$address->address_line_2);
        $model->_sender_city = $address->city;
        $model->_sender_contact_tel = $address->tel;
        $model->_sender_countryCode = $address->country0->code;
        $model->_sender_email = $address->fetchEmailAddress();
        $model->_sender_postalCode = $address->zip_code;
        $model->_sender_is_private = $address->company == '' ? true : false;
        $model->_sender_name = $address->company == '' ? $address->name : $address->company;
        $model->_sender_person = $address->name == '' ? $address->company : $address->name;

        $model->_cod_amount = 0;
        if($courierInternal->courier->cod)
            $model->_cod_amount = $courierInternal->courier->cod_value;

        $model->_package_desc = $courierInternal->courier->package_content;

        $model->_package_size_d = $courierInternal->courier->package_size_d;
        $model->_package_size_w = $courierInternal->courier->package_size_l;
        $model->_package_size_s = $courierInternal->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierInternal->courier->package_weight;

        $model->_number_of_packages = $courierInternal->courier->packages_number;

        $packages_ids = [];
        $packages_ids[] = $courierInternal->pickup_label == NULL ? $courierInternal->commonLabel->getTrack_id() : $courierInternal->pickupLabel->getTrack_id();

        if(is_array($courierInternal->childrenCourierInternal))
            foreach($courierInternal->childrenCourierInternal AS $item)
            {
                $packages_ids[] = $item->pickup_label == '' ? $item->commonLabel->getTrack_id() : $item->pickupLabel->getTrack_id();
            }

        // SPECIAL RULE - IF PACKAGE IS GOING TO HUB_PL AND WEIGHT IS > 30, SET WEIGHT TO 30 IN REQUEST
        // ALSO IN LABEL SERVICE!

        // find CourierLabelNew
        /* @var $courierLabelNew CourierLabelNew */
        $courierLabel = $courierInternal->pickup_label == NULL ? $courierInternal->commonLabel : $courierInternal->pickupLabel;
        if($courierLabel->courierLabelNew !== NULL && $courierLabel->courierLabelNew->_deliveryHubId == CourierHub::HUB_PL && $courierInternal->courier->package_weight > 30)
            $model->_package_weight = 30;

        $model->_packages_no_array = $packages_ids;
        $model->_total_weight = round($model->_number_of_packages * $model->_package_weight, 2);
        $model->_total_quantity = $model->_number_of_packages;

        return $model->callPickup(true);
    }

    public static function runCollectionForCourierU(CourierTypeU $courierU)
    {

        // service only for orders with pickup
        // PICKUP_MOD / 14.09.2016
//        if(!$courier->with_pickup)
        if(!$courierU->collection)
            return false;

        $model = new self;
        if($courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_INPOST_FMS)
            $model->setAltAccount(self::ALT_ACCOUNT_FMS);

        $address = $courierU->courier->senderAddressData;

        $model->_sender_address = trim($address->address_line_1.' '.$address->address_line_2);
        $model->_sender_city = $address->city;
        $model->_sender_contact_tel = $address->tel;
        $model->_sender_countryCode = $address->country0->code;
        $model->_sender_email = $address->fetchEmailAddress();
        $model->_sender_postalCode = $address->zip_code;
        $model->_sender_is_private = $address->company == '' ? true : false;
        $model->_sender_name = $address->company == '' ? $address->name : $address->company;
        $model->_sender_person = $address->name == '' ? $address->company : $address->name;

        $model->_cod_amount = 0;
        if($courierU->courier->cod)
            $model->_cod_amount = $courierU->courier->cod_value;

        $model->_package_desc = $courierU->courier->package_content;

        $model->_package_size_d = $courierU->courier->package_size_d;
        $model->_package_size_w = $courierU->courier->package_size_l;
        $model->_package_size_s = $courierU->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierU->courier->package_weight;

        $model->_number_of_packages = $courierU->courier->packages_number;

        $packages_ids = [];
        $packages_ids[] = $courierU->courierLabelNew->track_id;

        if(is_array($courierU->childrenCourierU))
            foreach($courierU->childrenCourierU AS $item)
            {
                $packages_ids[] = $item->courierLabelNew->track_id;
            }

        // SPECIAL RULE - IF PACKAGE IS GOING TO HUB_PL AND WEIGHT IS > 30, SET WEIGHT TO 30 IN REQUEST
        // ALSO IN LABEL SERVICE!

        // find CourierLabelNew
        /* @var $courierLabelNew CourierLabelNew */

        $model->_packages_no_array = $packages_ids;
        $model->_total_weight = round($model->_number_of_packages * $model->_package_weight, 2);
        $model->_total_quantity = $model->_number_of_packages;

        return $model->callPickup(true);
    }

    public static function runCollectionForCourierDomestic(CourierTypeDomestic $courierDomestic)
    {

        $address = $courierDomestic->courier->senderAddressData;

        $model = new self;

        $model->_sender_address = trim($address->address_line_1.' '.$address->address_line_2);
        $model->_sender_city = $address->city;
        $model->_sender_contact_tel = $address->tel;
        $model->_sender_countryCode = $address->country0->code;
        $model->_sender_email = $address->fetchEmailAddress();
        $model->_sender_postalCode = $address->zip_code;
        $model->_sender_is_private = $address->company == '' ? true : false;
        $model->_sender_name = $address->company == '' ? $address->name : $address->company;
        $model->_sender_person = $address->name == '' ? $address->company : $address->name;

        $model->_cod_amount = 0;
        if($courierDomestic->courier->cod_value > 0)
            $model->_cod_amount = $courierDomestic->courier->cod_value;

        $model->_package_desc = $courierDomestic->courier->package_content;

        $model->_package_size_d = $courierDomestic->courier->package_size_d;
        $model->_package_size_w = $courierDomestic->courier->package_size_l;
        $model->_package_size_s = $courierDomestic->courier->package_size_w;

        // Inpost always takes client's weight
        $model->_package_weight = $courierDomestic->courier->package_weight;

        $model->_number_of_packages = $courierDomestic->courier->packages_number;

        $packages_ids = [];
        $packages_ids[] = $courierDomestic->courierLabelNew->track_id;

        /* @var $item CourierTypeDomestic */
        if(is_array($courierDomestic->childrenCourierDomestic))
            foreach($courierDomestic->childrenCourierDomestic AS $item)
            {
                $packages_ids[] = $item->courierLabelNew->track_id;
            }

        $model->_packages_no_array = $packages_ids;
        $model->_total_weight = round($model->_number_of_packages * $model->_package_weight, 2);
        $model->_total_quantity = $model->_number_of_packages;

        return $model->callPickup(true);
    }

    public function getPickupHours()
    {
        $this->setAltAccount(self::ALT_ACCOUNT_FMS);

        $requestData = new stdClass();

        $requestData->PickupLocation = new stdClass;
        $requestData->PickupLocation->Name = $this->_sender_name;
        $requestData->PickupLocation->Address = $this->_sender_address;
        $requestData->PickupLocation->City = $this->_sender_city;
        $requestData->PickupLocation->PostCode = self::zipCodeConverter($this->_sender_postalCode, $this->_sender_countryCode);
        $requestData->PickupLocation->CountryCode = $this->_sender_countryCode;
        $requestData->PickupLocation->Person = $this->_sender_person;
        $requestData->PickupLocation->Contact = $this->_sender_contact_tel;
        $requestData->PickupLocation->Email = $this->_sender_email;
        $requestData->PickupLocation->IsPrivatePerson = $this->_sender_is_private;

        $return = new stdClass();
        $return->success = false;

        $resp = $this->_soapCall($requestData, 'getAvailablePickupsRequest', 'GetAvailablePickups');
        if(!$resp->success)
        {
            $return->error = $resp->error;
            return $return;
        }

        $resp = $resp->data;
        if($resp->GetAvailablePickupsResult->responseDescription != 'Success')
        {
            $return->error = $resp->GetAvailablePickupsResult->responseDescription;

        } else {

            $return->success = true;
            $return->data = $resp->GetAvailablePickupsResult->AvailablePickupDays->AvailablePickupDay[0];
        }

        return $return;
    }

    protected function callPickup($returnError = false)
    {
        $requestData = new stdClass();

        $requestData->PickupLocation = new stdClass;
        $requestData->PickupLocation->Name = $this->_sender_name;
        $requestData->PickupLocation->Address = $this->_sender_address;
        $requestData->PickupLocation->City = $this->_sender_city;
        $requestData->PickupLocation->PostCode = self::zipCodeConverter($this->_sender_postalCode, $this->_sender_countryCode);
        $requestData->PickupLocation->CountryCode = $this->_sender_countryCode;
        $requestData->PickupLocation->Person = $this->_sender_person;
        $requestData->PickupLocation->Contact = $this->_sender_contact_tel;
        $requestData->PickupLocation->Email = $this->_sender_email;
        $requestData->PickupLocation->IsPrivatePerson = $this->_sender_is_private;

        for($i = 0; $i < $this->_number_of_packages; $i++) {
            $requestData->Parcels[$i] = new stdClass;
            $requestData->Parcels[$i]->Type = 'Package';
            $requestData->Parcels[$i]->Weight = $this->_package_weight;
            $requestData->Parcels[$i]->D = $this->_package_size_d;
            $requestData->Parcels[$i]->W = $this->_package_size_w;
            $requestData->Parcels[$i]->S = $this->_package_size_s;
        }


        $requestData->PackageNo = $this->_packages_no_array;

        $requestData->TotalWeight = $this->_total_weight;

        $requestData->TotalQuantity = $this->_total_quantity;



        $pickupDate = $this->getPickupHours();
        if(!$pickupDate->success)
        {
            if($returnError)
            {
                $return = array(
                    'error' => $pickupDate->error,
                    'data' => $requestData,
                );
                return $return;
            } else {
                return false;
            }
        }

//        $datetime = new DateTime('now',new DateTimeZone('Europe/Warsaw'));

        $requestData->ReadyDate = $pickupDate->data->MinReadyDate;

//        $datetime->add(new DateInterval('P2D'));
//        if($datetime->format('w') == 0) // Sunday
//            $datetime->add(new DateInterval('P1D')); // Inpost throws error on maxPickupDate on Sunday

        $requestData->MaxPickupDate = $pickupDate->data->MaxPickupDate;

        MyDump::dump('inpost.txt', 'PICKUP: '.print_r($requestData,1));

        $resp = $this->_soapCall($requestData, 'callPickupRequest', 'CallPickup');
        if(!$resp->success)
        {
            if($returnError)
            {
                $return = array(
                    'error' => $resp->error,
                    'data' => $requestData,
                );
                return $return;
            } else {
                return false;
            }
        }
        $resp = $resp->data;


        $return = [];
        $resp = $resp->CallPickupResult;

        if($resp->responseDescription == 'Success')
        {
            $return = array(
                'desc' => $resp->responseDescription,
                'prn' => $resp->PickupNo,
                'data' => $requestData,
            );


        } else {
            if($returnError)
            {
                $return = array(
                    'error' => $resp->responseDescription .'('.$resp->ResponseCode.')',
                    'data' => $requestData,
                );
            } else {
                return false;
            }
        }

        return $return;

    }


    public static function getTracking($package_id, $returnError = false)
    {
        $model = new self;

        $model->setAltAccount(self::ALT_ACCOUNT_FMS);

        $data = $package_id;

        $resp = $model->_soapCall($data, 'packageNo', 'GetTracking');

        if(!$resp->success)
        {
            if($returnError)
            {
                $return = array(
                    'error' => $resp->error,
                );
                return $return;
            } else {
                return false;
            }
        }
        $resp = $resp->data;


        $statuses = [];

        $resp = $resp->GetTrackingResult;

        if($resp->responseDescription == 'Success')
        {

            $activity = $resp->Status->OrderStatus;

            if(!is_array($activity))
                $activity = array($activity);

            foreach($activity AS $item)
            {

                $name = $item->Description;
                $date = $item->EventTimestamp;

                $date = explode('.', $date);
                $date = $date[0];

                $datetime = DateTime::createFromFormat('Y-m-d\TH:i:s', $date);

                if(preg_match('/^Zwrot do nadawcy \[\d+\]$/i', $name))
                    $name = 'Zwrot do nadawcy';

                $temp = array(
                    'name' => $name,
                    'date' => $datetime->format('Y-m-d H:i:s'),
                    'location' => '',
                );

                array_unshift($statuses, $temp);
            }
            return $statuses;


        } else {
            if($returnError)
            {
                $return = array(
                    'error' => $resp->responseDescription .'('.$resp->ResponseCode.')',
                );
                return $return;
            } else {
                return false;
            }
        }


    }

}