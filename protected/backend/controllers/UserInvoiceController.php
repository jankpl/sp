<?php

class UserInvoiceController extends Controller
{

    public function actionJpk()
{
    $invoices = UserInvoice::model()->findAll('inv_date LIKE "2018-10-%" AND stat_inv NOT IN ('.UserInvoice::INV_STAT_CANCELLED.','.UserInvoice::INV_STAT_CORRECTED.') AND inv_currency = "1" AND title LIKE "2018%"');
//    $invoices = UserInvoice::model()->findAll('inv_date LIKE "%" AND stat_inv NOT IN ('.UserInvoice::INV_STAT_CANCELLED.','.UserInvoice::INV_STAT_CORRECTED.')');
    echo JpkGenerator::generateByInvoices($invoices, '2018-10-01', '2018-10-31', false);
}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority && AdminRestrictions::isFinancialAdmin()',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        /* @var $model UserInvoice */
        $model = $this->loadModel($id, 'UserInvoice');

        $this->render('view',array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new UserInvoice;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['UserInvoice']))
        {
            $model->attributes=$_POST['UserInvoice'];
            $model->_file_sheet = CUploadedFile::getInstance($model,'_file_sheet');
            $model->_file_invoice = CUploadedFile::getInstance($model,'_file_invoice');

            $model->validate();
            $errors = false;

            if(!$model->hasErrors())
            {

                $transaction = Yii::app()->db->beginTransaction();
                if(!$model->save())
                    $errors = true;

                if($model->_file_invoice !== NULL) {
                    // to get proper hash
                    $modelNew = UserInvoice::model()->findByPk($model->id);

                    $path = UserInvoice::getFinalDir($modelNew->hash.'_'.UserInvoice::FILE_TYPE_INV.'.'.$model->_file_invoice->extensionName);
                    if (!$model->_file_invoice->saveAs($path)) {
                        $errors = true;
                    } else {
                        $model->inv_file_path = $path;
                        $model->inv_file_name = $model->_file_invoice->name;
                        $model->inv_file_type = mime_content_type($path);
                        $model->update(array('inv_file_path', 'inv_file_name', 'inv_file_type'));
                    }
                }

                if($model->_file_sheet !== NULL) {
                    // to get proper hash
                    $modelNew = UserInvoice::model()->findByPk($model->id);

                    $path = UserInvoice::getFinalDir($modelNew->hash.'_'.UserInvoice::FILE_TYPE_SHEET.'.'.$model->_file_sheet->extensionName);
                    if (!$model->_file_sheet->saveAs($path)) {
                        $errors = true;
                    } else {
                        $model->sheet_file_path = $path;
                        $model->sheet_file_name = $model->_file_sheet->name;
                        $model->sheet_file_type = mime_content_type($path);
                        $model->update(array('sheet_file_path', 'sheet_file_name', 'sheet_file_type'));
                    }
                }

                if($errors)
                {
                    $transaction->rollback();
                } else {
                    $transaction->commit();

                    $this->redirect(array('view','id'=>$model->id));
                }
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id, 'UserInvoice');
        $model->scenario = 'update';

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['UserInvoice']))
        {
            $model->attributes=$_POST['UserInvoice'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {

        $model = UserInvoice::model()->findByPk($id);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        if($model->stat > UserInvoice::UNPUBLISHED)
            throw new CHttpException(403, 'You can\'t delete published messages!');

        $model->deleteWithFile();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(array('/userInvoice/admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new _UserInvoiceGridView('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['UserInvoiceGridView']))
            $model->attributes = $_GET['UserInvoiceGridView'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }


    public function actionPublish($id)
    {
        $model = UserInvoice::model()->findByPk($id);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        if($model->stat > UserInvoice::UNPUBLISHED)
            throw new CHttpException(403, 'This message has already been published!');

        if($model->publish())
            $model->notifyAboutPublished();

        $this->redirect(array('view','id'=>$model->id));
    }

    public function actionGetFile($id, $type)
    {
        $model = UserInvoice::model()->findByPk($id);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        if($type == UserInvoice::FILE_TYPE_INV) {
            $file_path = $model->inv_file_path;
            $file_name = $model->inv_file_name;
            $file_type = $model->inv_file_type;
        }
        else if($type == UserInvoice::FILE_TYPE_SHEET) {
            $file_path = $model->sheet_file_path;
            $file_name = $model->sheet_file_name;
            $file_type = $model->sheet_file_type;
        }
        else
            throw new CHttpException(404);


        if($file_path == NULL)
            throw new CHttpException(404);


        return Yii::app()->getRequest()->sendFile($file_name, @file_get_contents($file_path), $file_type);
    }

    public function actionAjaxGetUserCurrency()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $user_id = $_POST['user_id'];

            $model = User::model()->findByPk($user_id);
            $currency_id = NULL;

            if ($model)
                $currency_id = $model->price_currency_id;

            echo CJSON::encode($currency_id);
        }
    }

    public function actionExportall()
    {
        if(!AdminRestrictions::isSuperAdmin())
            throw new CHttpException(403);

        $models = UserInvoice::model()->findAll();


        $array = [];

        $temp = $models[0];

        $attributes = $temp->getAttributes();

        unset($attributes['hash']);
        unset($attributes['inv_file_path']);
        unset($attributes['inv_file_name']);
        unset($attributes['inv_file_type']);
        unset($attributes['sheet_file_path']);
        unset($attributes['sheet_file_name']);
        unset($attributes['sheet_file_type']);
        unset($attributes['notifications']);
        unset($attributes['order_id']);
        $attributes['isPaid'] = NULL;

        $array[] = array_keys($attributes);

        /* @var $model UserInvoice */
        foreach($models AS $model)
        {
            $item = $model->getAttributes();

            unset($item['hash']);
            unset($item['inv_file_path']);
            unset($item['inv_file_name']);
            unset($item['inv_file_type']);
            unset($item['sheet_file_path']);
            unset($item['sheet_file_name']);
            unset($item['sheet_file_type']);
            unset($item['notifications']);
            unset($item['order_id']);

            $item['isPaid'] = $model->isPaid();
            $item['source'] = $model->getSourceName();
            $item['stat_inv'] = $model->getStatInvName();
            $item['stat'] = $model->getStatName();
            $item['inv_currency'] = $model->getCurrencyName();

            $array[] = $item;

        }

        S_XmlGenerator::generateXml($array);
    }

    public function actionExportToZipPdfByGridView()
    {
        MyDump::dump('zip_exp.txt', print_r($_POST,1));

        /* @var $model Courier */
        $i = 0;
        if (isset($_POST['UserInvoice']))
        {
            $ids =  $_POST['UserInvoice']['ids'];
            $ids = explode(',', $ids);

            $files = [];
            if(is_array($ids) AND S_Useful::sizeof($ids))
            {
                $models = UserInvoice::model()->findAllByAttributes(['id' => $ids]);

                /* @var $model UserInvoice */
                foreach($models AS $model)
                {
                    $files[$model->inv_file_path] =  preg_replace("/[^a-z0-9\.]/", "", strtolower($model->title)).'_'.$model->id.'_'.$model->inv_file_name;
                    $files[$model->sheet_file_path] = preg_replace("/[^a-z0-9\.]/", "", strtolower($model->title)).'_'.$model->id.'_'.$model->sheet_file_name;
                    $i++;
                }

                MyDump::dump('zip_export.txt', print_r($files,1));
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Files not found!');
            $this->redirect(Yii::app()->createUrl('/userInvoice/admin'));
        } else {
           ZipOutput::generateZipOutputFile($files, 'invoices');
        }
    }

}

