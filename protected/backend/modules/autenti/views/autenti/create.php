<?php
/* @var $model CreateForm */
?>

<?php
/* @var $model FakturowniaForm */


$this->widget('CheckViesWidget');
?>

<h2>Create new document via Autenti.com</h2>

<a href="<?= Yii::app()->createUrl('/autenti/autenti/getTemplate');?>" class="btn btn-primary">Download template</a>

<hr/>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php
$this->widget('backend.components.regon.RegonWidget', [
    'formAttrNames' => 'CreateForm',
    'button' => false,
    'attrMap' => [
//                'nip' => '_nip'
        'country' => 'receiver_country',
    ]
]);
?>


<div class="form" style="position: relative;">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id'=>'autenti-form',
        'htmlOptions' => [
            'enctype' => 'multipart/form-data',
        ]
    )); ?>
    <?php echo $form->errorSummary($model); ?>

    <br/>
    <br/>

    <h3>Document template</h3>

    <?php
    $this->widget('backend.components.regon.RegonWidget', [
        'button' => true,
    ]);
    ?>


    <div style="overflow: auto;">
        <div style="width: 48%; float: left;">

            <div class="row" >
                <?php echo $form->labelEx($model, 'no'); ?>
                <?php echo $form->textField($model, 'no'); ?>
                <?php echo $form->error($model, 'no'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'date'); ?>
                <?php echo $form->textField($model, 'date'); ?>
                <?php echo $form->error($model, 'date'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'company'); ?>
                <?php echo $form->textField($model, 'company'); ?>
                <?php echo $form->error($model, 'company'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'form'); ?>
                <?php echo $form->textField($model, 'form'); ?>
                <?php echo $form->error($model, 'form'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'nip'); ?>
                <?php echo $form->textField($model, 'nip'); ?>
                <?php echo $form->error($model, 'nip'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'address_line_1'); ?>
                <?php echo $form->textField($model, 'address_line_1'); ?>
                <?php echo $form->error($model, 'address_line_1'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'address2'); ?>
                <?php echo $form->textField($model, 'address2'); ?>
                <?php echo $form->error($model, 'address2'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'sad'); ?>
                <?php echo $form->textField($model, 'sad'); ?>
                <?php echo $form->error($model, 'sad'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'krs'); ?>
                <?php echo $form->textField($model, 'krs'); ?>
                <?php echo $form->error($model, 'krs'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'regon'); ?>
                <?php echo $form->textField($model, 'regon'); ?>
                <?php echo $form->error($model, 'regon'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'kapital'); ?>
                <?php echo $form->textField($model, 'kapital'); ?>
                <?php echo $form->error($model, 'kapital'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'account_no'); ?>
                <?php echo $form->textField($model, 'account_no'); ?>
                <?php echo $form->error($model, 'account_no'); ?>
            </div>


        </div>

        <div style="width: 48%; float: right;">
            <div class="row" >
                <?php echo $form->labelEx($model, 'person1'); ?>
                <?php echo $form->textField($model, 'person1'); ?>
                <?php echo $form->error($model, 'person1'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'person2'); ?>
                <?php echo $form->textField($model, 'person2'); ?>
                <?php echo $form->error($model, 'person2'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email'); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'person_contact'); ?>
                <?php echo $form->textField($model, 'person_contact'); ?>
                <?php echo $form->error($model, 'person_contact'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'person_contact_email'); ?>
                <?php echo $form->textField($model, 'person_contact_email'); ?>
                <?php echo $form->error($model, 'person_contact_email'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'person_rozliczenia'); ?>
                <?php echo $form->textField($model, 'person_rozliczenia'); ?>
                <?php echo $form->error($model, 'person_rozliczenia'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'person_rozliczenia_email'); ?>
                <?php echo $form->textField($model, 'person_rozliczenia_email'); ?>
                <?php echo $form->error($model, 'person_rozliczenia_email'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'person_reklamacje'); ?>
                <?php echo $form->textField($model, 'person_reklamacje'); ?>
                <?php echo $form->error($model, 'person_reklamacje'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'person_reklamacje_email'); ?>
                <?php echo $form->textField($model, 'person_reklamacje_email'); ?>
                <?php echo $form->error($model, 'person_reklamacje_email'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'salesman'); ?>
                <?php echo $form->textField($model, 'salesman'); ?>
                <?php echo $form->error($model, 'salesman'); ?>
            </div>

            <div class="row" >
                <?php echo $form->labelEx($model, 'salesman_tel'); ?>
                <?php echo $form->textField($model, 'salesman_tel'); ?>
                <?php echo $form->error($model, 'salesman_tel'); ?>
            </div>
        </div>
    </div>

    <div class="text-center">
        <?php echo TbHtml::submitButton('Download agreement preview', ['name' => 'preview', 'class' => 'btn btn-primary btn-lg']); ?>
    </div>



    <h3>Autenti receiver data</h3>
    <div class="row">
        <?php echo $form->labelEx($model, 'receiver_name'); ?>
        <?php echo $form->textField($model, 'receiver_name'); ?>
        <?php echo $form->error($model, 'receiver_name'); ?>
    </div>
    <div class="row" >
        <?php echo $form->labelEx($model, 'receiver_surname'); ?>
        <?php echo $form->textField($model, 'receiver_surname'); ?>
        <?php echo $form->error($model, 'receiver_surname'); ?>
    </div>
    <div class="row" >
        <?php echo $form->labelEx($model, 'receiver_email'); ?>
        <?php echo $form->textField($model, 'receiver_email'); ?>
        <?php echo $form->error($model, 'receiver_email'); ?>
    </div>
    <div class="row" >
        <?php echo $form->labelEx($model, 'receiver_tel'); ?>
        <?php echo $form->textField($model, 'receiver_tel'); ?>
        <?php echo $form->error($model, 'receiver_tel'); ?>
    </div>
    <div class="row" >
        <?php echo $form->labelEx($model, 'receiver_country'); ?>
        <?php echo $form->textField($model, 'receiver_country'); ?>
        <?php echo $form->error($model, 'receiver_country'); ?>
    </div>

    <h3>Additional files</h3>
    <p>(you can select multiple PDF files at once.)</p>
    <input type="file" name="files[]" multiple accept = "application/pdf">
    <br/>
    <br/>

    <div class="text-center">
        <?php echo TbHtml::submitButton('Send to Autenti', ['name' => 'submit', 'class' => 'btn btn-success btn-lg']); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
