<?php

class F_MassStatusChange extends CFormModel
{

    public $status_text;
    public $status_id;
    public $courier_local_id;
    public $courier_id;
    protected $courierModel;
    public $status_date_text = NULL;
    public $status_date;
    public $duplicate;
    protected $omit = false;


    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
        );
    }

    public function rules() {
        return array(

            array('status_text, status_id, courier_local_id, status_date_text, status_date', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('status_text, status_id, courier_local_id, status_date_text, status_date', 'safe'),
        );

    }

    public function work()
    {
        $courier = Courier::model()->findByAttributes(array('local_id' => $this->courier_local_id));

        $this->courierModel = $courier;
        $this->courier_id = $courier->id;

        if($this->status_id == '')
            $this->status_id = CourierStat::findIdByText($this->status_text);

        if($this->status_date_text != '' && $this->status_date == '')
        {
            try
            {
                $date = new DateTime($this->status_date_text);
                $this->status_date = $date->format('Y-m-d H:i:s');
            }
            catch(Exception $ex)
            {
                $this->status_date = NULL;
            }
        }


        if($this->status_id == '')
        {
            $this->status_id = CourierStat::NEW_ORDER;
        }

        if($this->status_id && $this->courierModel != NULL)
        {

            // because Excel cuts seconds from date

            $status_date = substr($this->status_date,0,strlen($this->status_date)-2);

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id');
            $cmd->from('courier_stat_history');
            $cmd->where('status_date LIKE :stat_date AND courier_id = :courier_id AND current_stat_id = :stat_id', array(':stat_date' => $status_date.'%', ':courier_id' => $this->courierModel->id, ':stat_id' => $this->status_id));
            $result = $cmd->queryScalar();

            if($result)
            {
                $this->omit = true;
                $this->duplicate = true;
            }


        }

    }

    public function Courier()
    {
        return $this->courierModel;
    }

    public function courierFound()
    {

        if($this->courierModel != NULL)
            return true;
        else
            return false;
    }

    public static function saveGroupOfPackages(array $models, $ownTransaction = false)
    {


        if(S_Useful::sizeof($models))
        {
            /* @var $model F_MassStatusChange */
            /* @var $courier Courier */

            if(!$ownTransaction)
                $transaction = Yii::app()->db->beginTransaction();

            $errors = false;
            $i = 0;
            foreach($models AS $model)
            {
                $model->work();

                if($model->courierFound() && !$model->omit)
                {
                    $courier = $model->courierModel;
                    $statIdBefore = $courier->courier_stat_id;
                    if(!$courier->changeStat($model->status_id, 0, $model->status_date, false, false, false, false, false, false, false, Yii::app()->user->getModel()->scanning_sp_point_id))
                    {
                        $errors = true;

                    } else {
                        $statIdAfter = $courier->courier_stat_id;
                        $courier->addToLog('Status changed form #'.$statIdBefore.' to #'.$statIdAfter.' by file', CourierLog::CAT_INFO, Yii::app()->user->name);
                        $i++;
                    }
                }
            }

            if(!$errors)
            {
                if(!$ownTransaction)
                    $transaction->commit();
                return $i;
            }
            else
            {
                if(!$ownTransaction)
                    $transaction->rollback();
                return $models;
            }
        }
        else
        {
            return $models;
        }
    }
}