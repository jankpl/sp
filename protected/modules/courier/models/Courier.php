<?php

Yii::import('application.modules.courier.models._base.BaseCourier');

class Courier extends BaseCourier
{
    protected $_afterfind_ref;


    public $_cod_currency;
    public $_print_server_no;

    public $_sendLabelOnMail = false;

    public $_generate_return;

    public $_points_data = [];

    const SOURCE_EBAY = 10;
    const SOURCE_IAI = 15;
    const SOURCE_FILE_IMPORT = 20;
    const SOURCE_API = 30;
    const SOURCE_CLONE = 40;
    const SOURCE_ADMIN = 50;
    const SOURCE_LINKER = 60;
    const SOURCE_AUTOMATIC = 70;
    const SOURCE_AUTOMATIC_API = 71;
    const SOURCE_RELABELING = 80;


    const PDF_4_ON_1 = 1;
    const PDF_2_ON_1 = 2;
    const PDF_ROLL = 3;
    const PDF_2_ON_1_ONE_RIGHT = 4;
    const PDF_ROLL_WITH_CN_22 = 5;

    const MODE_SAVE = 1;
    const MODE_SAVE_NO_VALIDATE = 2;
    const MODE_JUST_VALIDATE = 3;
    const MODE_SAVE_CLIENT = 4;

    const TYPE_INTERNAL = 1;
    const TYPE_EXTERNAL = 2;

    const TYPE_INTERNAL_SIMPLE = 3;

    const TYPE_INTERNAL_OOE = 4;

    const TYPE_DOMESTIC = 5;
    const TYPE_U = 6;
    const TYPE_EXTERNAL_RETURN = 7;
    const TYPE_RETURN = 8;

    const TIMER_ARCHIVE_HISTORY_AFTER_DAYS = 365; // days // afterwards history of statuses is being removed
    const TIMER_ARCHIVE_LABELS_AFTER_DAYS = 93; // days // afterwards labels are being removed
    const TIMER_CANCEL_UNFINALIZED_ORDERS = 7; // days // afterwards cancel orders
    const TIMER_CANCEL_UNFINALIZED_ITEMS = 8; // days // afterwards cancel couriers - mostly not-paid internals or some crashed orders
    const TIMER_IGNORE_TT_AFTER = 31; // days // afterwards stop running TT cron and prevent updates by TT
    const TIMER_IGNORE_TT_AFTER_CANCELLED = 31; // days // afterwards stop running TT on calcelled packages
    const TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS = 62; // days // afterwards stop running TT cron and prevent updates by TT





    // when packages labels are no longer available
    const ARCHIVED_LABEL_EXPIRED = 5;
    const ARCHIVED_HISTORY = 7;

    protected static $_weight_unit = 'kg';
    protected static $_dimension_unit = 'cm';

    public $_first_status_date = NULL;

    const USER_CANCEL_REQUEST = 1;
    const USER_CANCEL_CONFIRMED = 5;
    const USER_CANCEL_DISMISSED = 20;
    const USER_CANCEL_WARNED = 9;

    // NEW FLAGS:
    const USER_CANCELL_REQUESTED = 50;
    const USER_CANCELL_REQUESTED_FOUND_TT = 60;
    const USER_CANCELL_COMPLAINED = 70;

    const PACKAGE_CANCEL_DAYS = 4;

    // do not calculate dim weight - just return real weight if option is enabled. Used for example for palette
    protected $_disableCalculationDimensionalWeight = false;
    // do not make additional check if option is enabled used by for example API. Used for example for palette
    public $_disableAdditionalWeightAndSizeCheck = false;

    // use different Dimenstional Weight calculation
    protected $_calculationDimensionalWeightDHLMode = false;



    public $_ebay_order_id;
    public $_ebay_export_status;
    public $__temp_ebay_update_stat;
    public $_ebay_source;

    // used for RUCH
    public $_ruch_receiver_point_active = false;
    public $_ruch_receiver_point;
    public $_ruch_receiver_point_address;

    // used for INPOST
    public $_inpost_locker_active = false;
    public $_inpost_locker_delivery_id;
    public $_inpost_locker_delivery_address;


//    public $_linker_order_id;

//    public function defaultScope()
//    {
//        if(Yii::app()->params['frontend'])
//        {
//            if(!Yii::app()->user->isGuest)
//            {
//                $user_id = Yii::app()->user->id;
//
//                return array(
//                    'condition'=> $this->getTableAlias().".user_id = '".$user_id."'",
//                );
//            }
//        }
//
//        return [];
//    }

    public function getSourceName()
    {
        $list = self::getSourceList();

        return isset($list[$this->source]) ? $list[$this->source] : '-';
    }

    public static function getSourceList()
    {
        return [
            self::SOURCE_API => 'API',
            self::SOURCE_FILE_IMPORT => 'File import',
            self::SOURCE_EBAY => 'eBay',
            self::SOURCE_IAI => 'IAI',
            self::SOURCE_CLONE => 'Clone',
            self::SOURCE_ADMIN => 'Admin',
            self::SOURCE_LINKER => 'Linker',
            self::SOURCE_AUTOMATIC => 'Auto',
            self::SOURCE_AUTOMATIC_API => 'Auto (API)',
            self::SOURCE_RELABELING => 'Relabeling',
        ];
    }

    public static function getUserCancelList()
    {
        return [
//            self::USER_CANCEL_REQUEST => 'OLD / ['.self::getUserCancelListShort()[self::USER_CANCEL_REQUEST].'] Request (waiting for admin action)',
//            self::USER_CANCEL_CONFIRMED => 'OLD / ['.self::getUserCancelListShort()[self::USER_CANCEL_CONFIRMED].'] Confirmed (block new T&T statuses)',
//            self::USER_CANCEL_DISMISSED => 'OLD / ['.self::getUserCancelListShort()[self::USER_CANCEL_DISMISSED].'] Dismissed',
//            self::USER_CANCEL_WARNED => 'OLD / ['.self::getUserCancelListShort()[self::USER_CANCEL_WARNED].'] Warning - T&T found new status',
            0 => '-',
            self::USER_CANCELL_REQUESTED => '['.self::getUserCancelListShort()[self::USER_CANCELL_REQUESTED].'] User cancelled',
            self::USER_CANCELL_REQUESTED_FOUND_TT => '['.self::getUserCancelListShort()[self::USER_CANCELL_REQUESTED_FOUND_TT].'] Warning - T&T found new status',
            self::USER_CANCELL_COMPLAINED => '['.self::getUserCancelListShort()[self::USER_CANCELL_COMPLAINED].'] User complained',
        ];
    }

    public static function getUserCancelListShort()
    {
        return [
//            self::USER_CANCEL_REQUEST => 'oR',
//            self::USER_CANCEL_CONFIRMED => 'oC',
//            self::USER_CANCEL_DISMISSED => 'oD',
//            self::USER_CANCEL_WARNED => 'oW',
            0 => '-',
            self::USER_CANCELL_REQUESTED => 'C',
            self::USER_CANCELL_REQUESTED_FOUND_TT => 'W',
            self::USER_CANCELL_COMPLAINED => 'R',
        ];
    }

    public static $types = array(
        self::TYPE_INTERNAL => 'internal',
        self::TYPE_EXTERNAL => 'external',
        self::TYPE_INTERNAL_SIMPLE => 'int. simple.',
        self::TYPE_INTERNAL_OOE => 'OOE',
        self::TYPE_DOMESTIC => 'domestic',
        self::TYPE_U => 'U',
        self::TYPE_EXTERNAL_RETURN => 'ext. ret.',
        self::TYPE_RETURN => 'ret.',
    );

    public static function getActiveTypes()
    {
        return array(
            self::TYPE_INTERNAL => 'internal',
            self::TYPE_EXTERNAL => 'external',
            self::TYPE_U => 'U',
            self::TYPE_EXTERNAL_RETURN => 'ext. ret.',
            self::TYPE_RETURN => 'ret.',
        );
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function getTypesNames()
    {
        return [
            self::TYPE_INTERNAL => Yii::t('courierTypes','internal'),
            self::TYPE_EXTERNAL => Yii::t('courierTypes','external'),
            self::TYPE_INTERNAL_SIMPLE => Yii::t('courierTypes','int. simple.'),
            self::TYPE_INTERNAL_OOE => Yii::t('courierTypes','OOE'),
            self::TYPE_DOMESTIC => Yii::t('courierTypes','domestic'),
            self::TYPE_U => Yii::t('courierTypes','u'),
            self::TYPE_EXTERNAL_RETURN => Yii::t('courierTypes','external return'),
            self::TYPE_RETURN => Yii::t('courierTypes','return'),
        ];
    }

    public function isStatHistoryArchived()
    {
        if(in_array($this->archive, [self::ARCHIVED_HISTORY, self::ARCHIVED_HISTORY + self::ARCHIVED_LABEL_EXPIRED]))
            return true;
        else
            return false;
    }

    public function isLabelArchived()
    {
        if(in_array($this->archive, [self::ARCHIVED_LABEL_EXPIRED, self::ARCHIVED_HISTORY + self::ARCHIVED_LABEL_EXPIRED]))
            return true;
        else
            return false;
    }

    public function isClonable()
    {
        if($this->getType() == Courier::TYPE_EXTERNAL_RETURN OR $this->getType() == self::TYPE_RETURN OR $this->getType() == self::TYPE_INTERNAL_SIMPLE)
            return false;
        else
            return true;
    }


    public static function getStatUpdateIgnoredStats()
    {
        $_ingoredStats = [
            CourierStat::CANCELLED,
            CourierStat::CANCELLED_PROBLEM,
            CourierStat::CANCELLED_USER,
            CourierStat::NEW_ORDER,
            CourierStat::PACKAGE_PROCESSING
        ];

        $data = array_merge($_ingoredStats, CourierStat::getKnownDeliveredStatuses());
        return $data;
    }


    public function isInternalReturn()
    {
        if($this->getType() == self::TYPE_RETURN OR $this->user_id == User::USER_EOBUWIE_ZWROTY)
            return true;
        else
            return false;
    }

    public function isStatChangeByTtAllowed($ignoreTime = false)
    {
        if(in_array($this->courier_stat_id, self::getStatUpdateIgnoredStats()))
            return false;

        if($this->stat_map_id == StatMap::MAP_DELIVERED)
            return false;

        if($ignoreTime)
            return true;

        if($this->isInternalReturn())
        {
            if($this->date_entered < date('Y-m-d', strtotime('-'.self::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS.' days')) && ($this->date_updated != '' && $this->date_updated < date('Y-m-d', strtotime('-'.self::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS.' days'))))
                return false;
        }
        else
        {
            if ($this->date_entered < date('Y-m-d', strtotime('-' . self::TIMER_IGNORE_TT_AFTER . ' days')) && ($this->date_updated != '' && $this->date_updated < date('Y-m-d', strtotime('-' . self::TIMER_IGNORE_TT_AFTER . ' days'))))
                return false;
        }

        return true;
    }

    public function attributeLabels() {

        return CMap::mergeArray(
            [
                '_sendLabelOnMail' => Yii::t('courier','Wyślij etykietę na email'),
                '_generate_return' => Yii::t('courier','Utwórz etykietę zwrotną'),
                '_inpost_locker_active' => Yii::t('site', 'Nadanie do Paczkomatu Inpost'),
            ],
            parent::attributeLabels());
    }

    public function rules() {


        $arrayValidate = array(
            array('value_currency', 'default', 'value' => 'PLN'),
            array('value_currency', 'in', 'range' => self::getCurrencyList(), 'allowEmpty' => true),

            array('cod_value', 'numerical', 'min'=>0),

            array('user_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?NULL:Yii::app()->user->id),
            array('admin_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?Yii::app()->user->id:NULL),

            array('packages_number, package_weight', 'required',
                'except' => Courier::MODE_SAVE_NO_VALIDATE),

            array('courier_type_external_id, courier_type_internal_id, sender_address_data_id, receiver_address_data_id, courier_stat_id, packages_number', 'numerical', 'integerOnly'=>true,
                'except' => Courier::MODE_SAVE_NO_VALIDATE),
            array('package_weight, package_size_l, package_size_w, package_size_d', 'numerical',
                'except' => Courier::MODE_SAVE_NO_VALIDATE),


            array('local_id', 'length', 'max'=>20),

            array('ref', 'length', 'max'=>55),
            array('note1, note2', 'length', 'max'=>50),

            array('package_value', 'length', 'max'=>32),
            array('package_content', 'length', 'max'=>256),

            array('ref', 'application.validators.courierRefValidator'),

            array('source', 'numerical', 'integerOnly'=>true),

            array('_sendLabelOnMail', 'boolean', 'allowEmpty' => true),

            array('date_updated, _generate_return', 'safe'),
            array('courier_type_external_id, courier_type_internal_id, date_updated, sender_address_data_id, package_value, package_content, package_weight, package_size_l, package_size_w, package_size_d', 'default', 'setOnEmpty' => true, 'value' => null),

            array(implode(',', array_keys(self::getAttributes())),'safe', 'on' => 'clone'),

            array('id, courier_type_external_id, courier_type_internal_id, date_entered, date_updated, date_activated, date_delivered, local_id, hash, sender_address_data_id, receiver_address_data_id, courier_stat_id, package_weight, package_size_l, package_size_w, package_size_d, packages_number, package_value, package_content, user_id, admin_id, user_cancel, source, stat_map_id, note1, note2
            ', 'safe', 'on'=>'search'),
        );



        $arrayBasic = array(

            array('packages_number', 'default', 'setOnEmpty' => true, 'value' => 1),


            array('local_id', 'unique'),

            array('local_id', 'match',
                'pattern' => '/^[a-zA-Z0-9\s]+$/',
                'message' => 'wrong characters!'),

            array('local_id', 'length', 'min'=>1),

            array('cod_value', 'numerical', 'min' => 0, 'max' => PaymentType::MAX_COD_AMOUNT),

            array('cod', 'default', 'setOnEmpty' => true, 'value' => false),

            array('courier_type_external_id, courier_type_internal_id, date_entered, date_updated, local_id, hash, sender_address_data_id, receiver_address_data_id, courier_stat_id, package_weight, package_size_l, package_size_w, package_size_d, packages_number, package_value, package_content, cod, ref, user_id, admin_id, _first_status_date, note1, note2', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('courier_type_external_id, courier_type_internal_id, date_updated, sender_address_data_id, receiver_address_data_id, package_weight, package_size_l, package_size_w, package_size_d, package_value, package_content, cod, ref, user_id, admin_id', 'default', 'setOnEmpty' => true, 'value' => null),

            array('courier_stat_id', 'default', 'setOnEmpty' => true, 'value' => CourierStat::NEW_ORDER),
        );


        return array_merge($arrayBasic, $arrayValidate);
    }

    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
            'bAddressDataSenderCompatilibity' =>
                array('class' => 'application.models.bAddressDataSenderCompatilibity'
                ),
            'bAddressDataReceiverCompatilibity' =>
                array('class' => 'application.models.bAddressDataReceiverCompatilibity'
                ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),

        );
    }

    /**
     * Returns children models for multipackage
     * @return Courier[]
     */
    public function findChildren()
    {
        if($this->courierTypeInternal)
            return Courier::model()->with('courierTypeInternal')->findAll('courierTypeInternal.parent_courier_id = :id', [':id' => $this->id]);
        else if($this->courierTypeU)
            return Courier::model()->with('courierTypeU')->findAll('courierTypeU.parent_courier_id = :id', [':id' => $this->id]);
        else
            return [];
    }


    /**
     * @return null|OrderProduct
     */
    public function getOrderProduct()
    {
        if($this->courierTypeInternal OR $this->courierTypeU)
            return OrderProduct::getModelForProductItem(OrderProduct::TYPE_COURIER, $this->id);
        else
            return NULL;
    }


    /**
     * Function saving log information to package
     * @param string $text Content of log
     * @param integer $cat Category of log - categories available in CourierLog class
     * @param string $source Source of log - string
     * @return bool
     */
    public function addToLog($text, $cat = NULL, $source = NULL)
    {
        $courierLog = new CourierLog;
        $courierLog->text = $text;
        $courierLog->cat = $cat;
        $courierLog->source = $source;
        $courierLog->courier_id = $this->id;

        return $courierLog->save();
    }

    public function init()
    {
        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest)
        {
            $data = User::getDefaultPackageDimensionsForUser(Yii::app()->user->id);

            $this->package_weight = $data->package_weight;
            $this->package_size_l = $data->package_size_l;
            $this->package_size_d = $data->package_size_d;
            $this->package_size_w = $data->package_size_w;
            $this->package_value = $data->package_value;
            $this->value_currency = $data->value_currency;
            $this->package_content = $data->package_content;

        }

        $this->cod_value = 0;

        parent::init();
    }

    public function afterFinalInit()
    {
        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest) {

            if(in_array($this->getType(), [self::TYPE_INTERNAL, self::TYPE_U])
                && Yii::app()->user->model->getCourierReturnAvailable()
                && $this->receiverAddressData
                && Yii::app()->user->model->getReturnPackageAutomatic($this->receiverAddressData->country_id)
            )
                $this->_generate_return = true;
        }
    }

    public function beforeSave()
    {
        if ($this->isNewRecord)
        {
            if($this->user && $this->user->getDisableOrderingNewItems())
                throw new AccountLockedException('Locked!');

            $this->local_id = $this->generateLocalId();

            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => $this->user_id.$this->local_id.$this->date_entered.microtime()));

            if(!in_array($this->getType(), [Courier::TYPE_U, Courier::TYPE_INTERNAL])) {
                // dimensional weight:
                $dw = self::calculateDimensionsWeight($this);
                $this->package_weight_client = $this->package_weight;
                if ($dw > $this->package_weight)
                    $this->package_weight = $dw;
            }

            if($this->cod_value > 0)
                $this->cod = true;

            $this->ref = strtoupper($this->ref);
            if($this->ref != '' && $this->packages_number > 1)
            {
                if($this->getType() == self::TYPE_INTERNAL)
                    $suffix_no = $this->courierTypeInternal->family_member_number;
                else if($this->getType() == self::TYPE_DOMESTIC)
                    $suffix_no = $this->courierTypeDomestic->family_member_number;
                else if($this->getType() == self::TYPE_U)
                    $suffix_no = $this->courierTypeU->family_member_number;
                else
                    throw new Exception('Unknown multipackage suffix number!');

                $this->ref = $this->ref.'_'.$suffix_no;
            }

            if($this->ref == '')
                $this->ref = NULL;


            if(!in_array($this->getType(), [self::TYPE_U, self::TYPE_INTERNAL])) {

                $dim_weight = S_PackageMaxDimensions::calculateDimensionsWeight($this);

                if ($this->package_weight < $dim_weight) {
                    $this->package_weight_client = $this->package_weight;
                    $this->package_weight = $dim_weight;
                }
            }

            if($this->_points_data OR $this->_inpost_locker_delivery_id OR $this->_ruch_receiver_point_active)
                $this->to_point_id = 0;

            if($this->courierTypeInternal && in_array($this->courierTypeInternal->with_pickup, [
                    CourierTypeInternal::WITH_PICKUP_RUCH,
                    CourierTypeInternal::WITH_PICKUP_INPOST,
                    CourierTypeInternal::WITH_PICKUP_DHLDE,
                    CourierTypeInternal::WITH_PICKUP_LP_EXPRESS,
                    CourierTypeInternal::WITH_PICKUP_RM,
                    CourierTypeInternal::WITH_PICKUP_UPS
                ]))
                $this->from_point_id = 0;
        }

        $this->params = array(
            self::_paramsToShort('_print_server_no') => $this->_print_server_no,
            self::_paramsToShort('_sendLabelOnMail') => $this->_sendLabelOnMail,
            self::_paramsToShort('_ebay_order_id') => $this->_ebay_order_id,
            self::_paramsToShort('_ebay_source') => $this->_ebay_source, // because we use also Linker via ebay methods/parameters
            self::_paramsToShort('_ebay_export_status') => $this->_ebay_export_status,
            self::_paramsToShort('_generate_return') => $this->_generate_return,

            self::_paramsToShort('_ruch_receiver_point') => $this->_ruch_receiver_point,
            self::_paramsToShort('_inpost_locker_active') => $this->_inpost_locker_active,
            self::_paramsToShort('_inpost_locker_delivery_id') => $this->_inpost_locker_delivery_id,
            self::_paramsToShort('_inpost_locker_delivery_address') => $this->_inpost_locker_delivery_address,
            self::_paramsToShort('_points_data') => $this->_points_data,
        );

        // serialize only if not all values are empty
        if (array_filter($this->params)) {
            $this->params = self::serialize($this->params);

            if(mb_strlen($this->params) > 384) {
                Yii::log('Params too long for Courier model! '.print_r($this->params,1), CLogger::LEVEL_ERROR);
                throw new Exception('Params too long for Courier model!');
            }
        }
        else
            $this->params = NULL;

        $this->package_weight = round($this->package_weight,2);
        $this->package_weight_client = $this->package_weight_client != '' ? round($this->package_weight_client,2) : NULL;

        return parent::beforeSave();
    }



    protected static function _paramsTranslationList()
    {
        $data = [
            '_print_server_no' => 'PSN',
            '_sendLabelOnMail' => 'SLM',
            '_ebay_order_id' => 'EOI',
            '_ebay_source' => 'ES',
            '_ebay_export_status' => 'EXS',
            '_generate_return' => 'GR',

            '_ruch_receiver_point' => 'RRP',
            '_inpost_locker_delivery_id' => 'ILDI',
            '_inpost_locker_delivery_address' => 'ILDA',
            '_inpost_locker_active' => 'ILA',

            '_points_data' => 'PD',
        ];

        return $data;
    }

    protected static function _paramsToShort($name)
    {
        $data = self::_paramsTranslationList();
        return isset($data[$name]) ? $data[$name] : $name;
    }

    protected static function _paramsFromShort($name)
    {
        $data = array_flip(self::_paramsTranslationList());
        return isset($data[$name]) ? $data[$name] : $name;
    }


    public static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    public static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    public static function calculateDimensionsWeight(Courier $courier)
    {


        if($courier->user && $courier->user->getDisableDimensionalWeight() OR (Yii::app()->params['frontend'] && !Yii::app()->user->isGuest && Yii::app()->user->model->getDisableDimensionalWeight()))
            $courier->_disableCalculationDimensionalWeight = true;

        // MOD 06.10.2016
        // Do not calculate dimensional weight for user Cloudpack_poczta (#496)
        if($courier->user_id == 496 OR (Yii::app()->params['frontend'] && !Yii::app()->user->isGuest && Yii::app()->user->id == 496))
            $courier->_disableCalculationDimensionalWeight = true;

        // do not calculate dim weight - just return real weight if option is enabled. Used for example for palette
        if($courier->_disableCalculationDimensionalWeight)
        {
            return $courier->package_weight;
        }

        /* @13.09.2017 - there is only one method of calculating */
        // ups mode
        $dw = (intval($courier->package_size_l) *  intval($courier->package_size_d) *  intval($courier->package_size_w));
        $dw /= 5000;

        $x = round($dw);
        $y = ceil($dw);

        // round up to 0.5
        if ($y - $dw > 0.5 && $y - $dw < 1)
            $dw = $x + 0.5;
        else if ($y - $dw != 0.5) {
            $dw = $y;
        }

        return $dw;

//
//        if($courier->_calculationDimensionalWeightDHLMode)
//        {
//            $dw = ($courier->package_size_l * $courier->package_size_d * $courier->package_size_w);
//            $dw /= 4000;
//            $dw = round($dw,1);
//
//            if($dw > 31.5)
//                $dw = 31.5;
//
//            return $dw;
//
//        } else {
//
//            // ups mode
//            $dw = ($courier->package_size_l * $courier->package_size_d * $courier->package_size_w);
//            $dw /= 5000;
//
//            $x = round($dw);
//            $y = ceil($dw);
//
//            // round up to 0.5
//            if ($y - $dw > 0.5 && $y - $dw < 1)
//                $dw = $x + 0.5;
//            else if ($y - $dw != 0.5) {
//                $dw = $y;
//            }
//
//            return $dw;
//        }


    }

    /**
     * Unserialize additional params (ex. Preffered pickup time)
     * @return bool
     */
    public function afterFind()
    {
        $this->_afterfind_ref = $this->ref;

        if ($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if (is_array($this->params))
            foreach ($this->params AS $key => $item) {
                try {
                    $keyFull = self::_paramsFromShort($key);
                    $this->$keyFull = $item;
                } catch (Exception $ex) {
                }
            }

        parent::afterFind();
    }

    public function afterSave()
    {
        if($this->isNewRecord)
        {
            $this->isNewRecord = false;

            /* @var $statHistory CourierStatHistory */

            $statHistory = new CourierStatHistory();
            $statHistory->courier_id = $this->id;
            $statHistory->previous_stat_id = NULL;
            $statHistory->current_stat_id = $this->courier_stat_id == ''?CourierStat::NEW_ORDER:$this->courier_stat_id;
            $statHistory->status_date = $this->_first_status_date;

            if(!$statHistory->save())
                return false;

            IdStore::createNumber(IdStore::TYPE_COURIER, $this->id, $this->local_id);
            if($this->ref)
                IdStore::createNumber(IdStore::TYPE_COURIER, $this->id, $this->ref);
        }

        if($this->ref != $this->_afterfind_ref)
        {
            if($this->ref)
                IdStore::createNumber(IdStore::TYPE_COURIER, $this->id, $this->ref);
            else
                IdStore::removeNumber(IdStore::TYPE_COURIER, $this->id, $this->_afterfind_ref);
        }

        parent::afterSave();
    }

    public function beforeValidate()
    {
//        if($this->package_weight_client === NULL)
//            $this->package_weight_client = $this->package_weight;

        if(!in_array($this->getType(), [self::TYPE_U, self::TYPE_INTERNAL])) {

            $dim_weight = S_PackageMaxDimensions::calculateDimensionsWeight($this);

            if ($this->package_weight < $dim_weight) {
//                if (!Yii::app()->user->hasFlash('weight-info'))
                Yii::app()->user->setFlash('weight-info', Yii::t('courier', 'Waga została skorygowana, gdyż podana waga jest mniejsza od jej wagi gabarytowej!'));
//                else
//                    Yii::app()->user->setFlash('weight-info', Yii::t('courier', 'Waga co najmniej dwóch paczek ostała skorygowana, gdyż ich  podana waga jest mniejsza ich od wagi gabarytowej!'));

//                $this->package_weight = $dim_weight;

//                if ($this->package_weight > S_PackageMaxDimensions::getPackageMaxDimensions()['weight'])
//                    $this->addError('package_weight', Yii::t('courier', 'Skorygowana waga gabarytowa paczki ({weight} {unit}) jest zbyt duża!', ['{weight}' => $dim_weight, '{unit}' => Courier::$_weight_unit]));
            }
        }
        return parent::beforeValidate();
    }

    // 28.10.2016
    // when duplicate local_id keys error, try to generate new and save again
    protected $_saveCounter = 0;
    public function save($runValidation=true, $attributes=null)
    {
        try {
            return parent::save($runValidation, $attributes);
        }
        catch (CDbException $ex) {

            if($ex->errorInfo[0] == 23000 && $ex->errorInfo[2] == 'Duplicate entry \''.$this->ref.'\' for key \'ref\'')
                throw new Exception('DUPLICATE_REF_VALUE', ErrorCodes::DUPLICATE_REF_VALUE);

//            file_put_contents('csave_log2.txt', date('Y-m-d h:i:s').' : DUPLIKAT: '.$this->local_id."\r\n", FILE_APPEND);

            MyDump::dump('csave.txt', print_r($ex,1));

            if($ex->errorInfo[0] == 23000 && $this->_saveCounter < 3)
            {
//                file_put_contents('csave_log.txt', date('Y-m-d h:i:s').' : DUPLIKAT: '.$this->local_id."\r\n", FILE_APPEND);

                if ($this->isNewRecord) {
                    $this->local_id = $this->generateLocalId();

//                    file_put_contents('csave_log.txt', date('Y-m-d h:i:s').' : NOWY ID: '.$this->local_id."\r\n", FILE_APPEND);

                    $this->_saveCounter++;
                    return self::save($runValidation, $attributes);
                }
            }

            throw new CDbException($ex->getMessage(), $ex->getCode(), $ex->errorInfo);
        }
    }

//    public function afterValidate()
//    {
//
//
//        if($dim_weight > S_PackageMaxDimensions::getPackageMaxDimensions()['weight'])
//            $this->addError('package_weight', Yii::t('courier', 'Dimensional weight is too big!'));
//
//
//        return parent::afterValidate();
//    }

    protected function _random_key($length,$base) {
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $key = '';
        for ( $i=0; $i<$length; $i++ ) {
            $key .= $chars[ (mt_rand( 0, ($base-1) ))];
        }
        return $key;
    }

    protected function generateLocalId()
    {
        $is_it_unique = false;

        $today = date('ymd');

        $local_id = null;
        while(!$is_it_unique)
        {
            $local_id = '483'.$today.$this->_random_key(6,10);

            $cmd = Yii::app()->db->createCommand();
            $cmd->select = 'COUNT(*)';
            $cmd->from = 'courier';
            $cmd->where = "local_id = :local_id";
            $cmd->bindParam(':local_id', $local_id);
            $num = $cmd->queryScalar();

            if($num == 0)
                $is_it_unique = true;
        }

        return $local_id;
    }

    public function getType()
    {
        if($this->courier_type > 0)
            return $this->courier_type;

        // Old way:
        if($this->courierTypeExternal != NULL)
            $type = Courier::TYPE_EXTERNAL;
        else if($this->courierTypeInternal != NULL)
            $type = Courier::TYPE_INTERNAL;
        else if($this->courierTypeInternalSimple !== NULL)
            $type = Courier::TYPE_INTERNAL_SIMPLE;
        else if($this->courierTypeOoe != NULL)
            $type = Courier::TYPE_INTERNAL_OOE;
        else if($this->courierTypeDomestic != NULL)
            $type = Courier::TYPE_DOMESTIC;
        else if($this->courierTypeU != NULL)
            $type = Courier::TYPE_U;
        else if($this->courierTypeExternalReturn != NULL)
            $type = Courier::TYPE_EXTERNAL_RETURN;
        else if($this->courierTypeReturn != NULL)
            $type = Courier::TYPE_RETURN;
        else
            $type = NULL;

        return $type;
    }


    public function getIntOperator()
    {
        if($this->courierTypeInternal)
        {
            if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_CONTRACT)
                return '-C- > ' . $this->courierTypeInternal->delivery_operator_name;
            else if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH)
                return '-R- > ' . $this->courierTypeInternal->delivery_operator_name;
            else if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_RM)
                return '-RM- > ' . $this->courierTypeInternal->delivery_operator_name;
//            else if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_LP_EXPRESS)
//                return '-LPE- > ' . $this->courierTypeInternal->delivery_operator_name;
//            else if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_UPS)
//                return '-UPS- > ' . $this->courierTypeInternal->delivery_operator_name;
            else if($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_NONE)
                return '--- > ' . $this->courierTypeInternal->delivery_operator_name;
            else if($this->courierTypeInternal->pickup_operator_id == $this->courierTypeInternal->delivery_operator_id)
                return $this->courierTypeInternal->pickup_operator_name;
            else
                return $this->courierTypeInternal->pickup_operator_name . ' > ' . $this->courierTypeInternal->delivery_operator_name;
        }
        else if($this->courierTypeU)
        {
            return $this->courierTypeU->courierUOperator->name;
        }
        else if($this->courierTypeReturn)
        {
            return $this->courierTypeReturn->courierLabelNew ? CourierLabelNew::getOperators()[$this->courierTypeReturn->courierLabelNew->operator] : '';
        }
        else if($this->courierTypeInternalSimple != NULL)
        {
            return $this->courierTypeInternalSimple->getOperator();
        }
        else

            return NULL;
    }

    public function getIntWithPickup()
    {
        if($this->courierTypeInternal)
        {
            return $this->courierTypeInternal->getWithPickupName();
        }

        return NULL;
    }


    /**
     * @param bool|false $nice Whether use untranslatable name on translatable (nice)
     * @return null
     */
    public function getTypeName($nice = false)
    {
        $typeId = $this->getType();

        if($typeId != '') {
            if(!$nice)
                $typeName = self::$types[$typeId];
            else
                $typeName = self::getTypesNames()[$typeId];
        }
        else
            $typeName = NULL;

        return $typeName;

    }

    /**
     * Checks if user is able to cancel this package
     *
     * @return bool
     */
    public function isUserCancelByUserPossible()
    {
        if($this->getType() == Courier::TYPE_EXTERNAL_RETURN)
            return false;

        if($this->user_cancel OR $this->courier_stat_id == CourierStat::CANCELLED OR $this->courier_stat_id == CourierStat::CANCELLED_USER OR in_array($this->stat_map_id, [StatMap::MAP_DELIVERED, StatMap::MAP_CANCELLED]))
            return false;

        $now = new DateTime();
        $packageDay = new DateTime($this->date_entered);

        $diff = $now->diff($packageDay)->format("%a");

        if($diff > (Courier::PACKAGE_CANCEL_DAYS - 1))
            return false;

        return true;
    }

    public function countTimeLeftForCancel()
    {
        $diff = strtotime($this->date_entered) + (self::PACKAGE_CANCEL_DAYS * 24 * 60 * 60) - strtotime(date('Y-m-d H:i:s'));

        $h = floor($diff/3600);
        $min = ($diff/60)%60;

        return Yii::t('panel', '{h}g. {m}min.', ['{h}' => $h, '{m}' => $min]);

    }

    /**
     * Checks if user is able to cancel this package
     *
     * @return bool
     */
    public function isUserComplaintPossible()
    {

        if($this->isUserCancelByUserPossible())
            return false;

        if($this->user_cancel OR $this->courier_stat_id == CourierStat::CANCELLED OR $this->courier_stat_id == CourierStat::CANCELLED_USER OR in_array($this->stat_map_id, [StatMap::MAP_CANCELLED]))
            return false;

        return true;
    }

    /**
     * Mark package as complained by user
     * @return bool
     */
    public function markUserComplaint()
    {
        $this->addToLog('Klient przesłał formularz reklamacji.');
        $this->user_cancel = self::USER_CANCELL_COMPLAINED;
        return $this->update(['user_cancel', 'date_updated']);
    }

    /**
     * Make request by user to cancel this package
     *
     * @return bool
     * @throws CDbException
     */
    public function userCancelRequestByUser()
    {
        if($this->isUserCancelByUserPossible())
            return $this->_userCancelConfirm();
    }


    /**
     * Set cancelled by user package warning flag - means that T&T system found new status
     *
     * @return bool
     * @throws CDbException
     */
    public function userCancelWarn()
    {
//        if($this->user_cancel != self::USER_CANCEL_CONFIRMED)
//            return false;
//
//        $this->user_cancel = self::USER_CANCEL_WARNED;

        return $this->update(['user_cancel', 'date_updated']);
    }


    /**
     * Confirm user cancel
     *
     * @param bool|false $byAdmin If true, method can override user cancel status not available for client
     * @return bool
     * @throws CDbException
     */
    protected function _userCancelConfirm()
    {

//        if((!$byAdmin && !$this->user_cancel) OR ($byAdmin && ($this->user_cancel == self::USER_CANCEL_DISMISSED OR $this->user_cancel == self::USER_CANCEL_REQUEST))) {
        $this->changeStat(CourierStat::CANCELLED_USER, 0);

        $this->user_cancel = self::USER_CANCELL_REQUESTED;


        $updateAttr = ['user_cancel', 'date_updated'];

        // @ 16.01.2019 - special rule for ANWISDHL2 - remove REF on package cancels
        if($this->user_id == 2671)
        {
            if($this->ref)
            {
                $this->addToLog('Ref number removed on cancel: '.$this->ref);
                $this->ref = NULL;
                $updateAttr[] = 'ref';
            }
        }

        $this->update($updateAttr);

        return true;
//        }

//        return false;
    }

    /**
     * Cancel group of packages
     *
     * @param Courier[] $models
     * @return int Number of cancelled packages
     */
    public static function userCancelMulti(array $models, &$done = [])
    {
        $counter = 0;

        /* @var $model Courier */
        foreach($models AS $key => $model)
        {
            if($model->userCancelRequestByUser()) {
                $done[$key] = $model->id;
                $counter++;
            }
        }

        return $counter;
    }

    /**
     * @param $newStat
     * @param int $author
     * @param null $status_date
     * @param bool $ownTransaction
     * @param null $location
     * @param $checkDate If true, system won't change status if date is older that the newsets status and just add item to history
     * @param $doNotNotify
     * @param $viaAuctiomaticTt bool If true, status change is generated by automatic T&T system
     * @param $overrideStatBlock bool If true, status will be changes even for Cancelled or Return packages
     * @return bool
     */
    public function changeStat($newStat, $author = 0, $status_date = NULL, $ownTransaction = false, $location = NULL, $checkDate = false, $doNotNotify = false, $viaAutomaticTt = false, $overrideStatBlock = false, $forceOnArchive = false, $scanningPointId = false)
    {
        $updateAttributes = [];
        if($scanningPointId && !$this->first_scan_sp_point_id) {
            $this->first_scan_sp_point_id = $scanningPointId;
            $updateAttributes[] = 'first_scan_sp_point_id';
        }

        $statIdBefore = $this->courier_stat_id;

        if($this->isStatHistoryArchived()) {

            if($forceOnArchive)
            {
                $statMap = StatMapMapping::findMapForStat($newStat, StatMapMapping::TYPE_COURIER);

                $oldStatMap = $this->stat_map_id;
                $this->stat_map_id = $statMap;
                $this->courier_stat_id = $newStat;
                if (!$this->save(false, ['date_updated', 'stat_map_id', 'courier_stat_id', 'first_scan_sp_point_id']))
                    return false;
                else
                {
                    $this->afterStatMapChange($oldStatMap, $statMap, false, false, NULL, NULL, false, $newStat);
                }

            } else
                return false;

        }

        if($viaAutomaticTt)
        {
            if($status_date && strtotime(date('Ymd', strtotime($status_date)))-strtotime(date('Ymd', strtotime($this->date_entered))) < 0) // date from the past
            {
                MyDump::dump('courier_past_stat.txt', print_r($status_date,1).' : '.$this->id);
                return false;
            }
        }

        if($viaAutomaticTt) {

            if(in_array($this->courier_stat_id, [
                CourierStat::CANCELLED_USER,
                CourierStat::CANCELLED,
                CourierStat::CANCELLED_PROBLEM
            ])
            )
            {

                if(!in_array($this->user_cancel, [Courier::USER_CANCELL_REQUESTED_FOUND_TT, Courier::USER_CANCELL_COMPLAINED]))
                {
                    $this->user_cancel = Courier::USER_CANCELL_REQUESTED_FOUND_TT;
                    $this->update(['user_cancel']);
                    $this->addToLog('Found new status by TT on cancelled package: (#'.$newStat.')');
                }

//                return false;
            }
        }


        // do not change stat if package is CANCELLED
//        if((($this->stat_map_id == StatMap::MAP_CANCELLED OR $this->courier_stat_id == CourierStat::CANCELLED) && $this->courier_stat_id != CourierStat::CANCELLED_USER) && !$overrideStatBlock)
//            return false;

        if(!$viaAutomaticTt && $this->user_cancel)
        {
            $this->user_cancel = NULL;
            $this->update(['user_cancel']);
            $this->addToLog('Manual change of status cleared User Cancel flag!');
        }

        $location = trim(strip_tags($location));

        $status_date = CourierStat::convertDate($status_date);

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $newStatModel = CourierStat::model()->findByPk($newStat);
        if($newStatModel === null)
            return false;

//        if($this->courier_stat_id == $newStat->id)
//            return true;


        $statHistory = new CourierStatHistory();
        $statHistory->author = $author;
        $statHistory->courier_id = $this->id;
        $statHistory->previous_stat_id = $this->courier_stat_id;
        $statHistory->current_stat_id = $newStatModel->id;
        $statHistory->status_date = $status_date;
        $statHistory->location = $location;
        $statHistory->validate();

        if(!$statHistory->save())
            $errors = true;



        $justHistory = false;
        if($checkDate)
        {
            // to get proper date
            $statHistory->refresh();

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from('courier_stat_history');
            $cmd->where('author = :author AND status_date > :status_date AND id != :id AND courier_id = :courier_id', array(':author' => $author, ':status_date' => $statHistory->status_date, ':id' => $statHistory->id, ':courier_id' => $this->id));
            $res = $cmd->queryScalar();
            if($res)
                $justHistory = true;

        }

        if(!$justHistory) {
            $this->courier_stat_id = $newStatModel->id;
            $this->location = $location;


            $doNotChangeMap = false;

            // do not change map to RETURN if package is INTERNAL RETURN
            if($this->getType() == self::TYPE_RETURN && in_array(StatMapMapping::findMapForStat($this->courier_stat_id, StatMapMapping::TYPE_COURIER), [StatMap::MAP_RETURN]))
                $doNotChangeMap = true;

            // do not change stat map if package is IN RETURN
            if(in_array($this->stat_map_id, [StatMap::MAP_RETURN]))
                $doNotChangeMap = true;

            // do not change stat map if package CANCELLED and new map id ID = 1
            else if($this->stat_map_id == StatMap::MAP_CANCELLED)
            {
                $newStatMap = StatMapMapping::findMapForStat($newStatModel->id, StatMapMapping::TYPE_COURIER);
                if($newStatMap == StatMap::MAP_NEW)
                    $doNotChangeMap = true;
            }

//             do not change stat map if package DELIVERED with exception for RETURNED map
            else if($this->stat_map_id == StatMap::MAP_DELIVERED)
            {
                $newStatMap = StatMapMapping::findMapForStat($newStatModel->id, StatMapMapping::TYPE_COURIER);
                if($newStatMap != StatMap::MAP_RETURN)
                    $doNotChangeMap = true;
            }

//            $currentStatMap = $this->stat_map_id;

            if(!$doNotChangeMap)
                $statMap = StatMapMapping::findMapForStat($this->courier_stat_id, StatMapMapping::TYPE_COURIER);

//            $updateAttributes[] = 'last_status_update';
//            $updateAttributes[] = 'last_status_update';
//            $this->last_status_update = date('Y-m-d H:i:s');

            if(!$doNotChangeMap && $statMap && $statMap != $this->stat_map_id)
            {
                $oldStatMap = $this->stat_map_id;
                $this->stat_map_id = $statMap;

                $updateAttributes[] = 'courier_stat_id';
                $updateAttributes[] = 'location';
                $updateAttributes[] = 'date_updated';
                $updateAttributes[] = 'stat_map_id';

                if(AttemptedDeliveryStat::isStatAttemptedDelivery($newStatModel->id) && $this->date_attempted_delivery == '')
                {
                    $dateAttemptedDelivery = $status_date;
                    if($dateAttemptedDelivery == '')
                        $dateAttemptedDelivery = date('Y-m-d H:i:s');

                    $updateAttributes[] = 'date_attempted_delivery';
                    $this->date_attempted_delivery = $dateAttemptedDelivery;
                }

                if($statMap > StatMap::MAP_NEW && !in_array($statMap, [StatMap::MAP_CANCELLED, StatMap::MAP_PROBLEM]) && $this->date_activated == '')
                {
                    $dateActivated = $status_date;
                    if($dateActivated == '')
                        $dateActivated = date('Y-m-d H:i:s');

                    $updateAttributes[] = 'date_activated';
                    $this->date_activated = $dateActivated;
                }

                if($this->stat_map_id == StatMap::MAP_DELIVERED)
                {
                    $dateDelivered = $status_date;
                    if($dateDelivered == '')
                        $dateDelivered = date('Y-m-d H:i:s');

                    $updateAttributes[] = 'date_delivered';
                    $this->date_delivered = $dateDelivered;
                }

                if (!$this->save(false, $updateAttributes))
                    $errors = true;
                else {
                    if (Yii::app()->params['backend'] && !$viaAutomaticTt)
                        $logText = 'Status changed form #' . $statIdBefore . ' to #' . $this->courier_stat_id . ' by admin';
                    else
                        $logText = false;

                    $this->afterStatMapChange($oldStatMap, $statMap, true, $logText, $location, $status_date, Yii::app()->params['console'] ? false : Yii::app()->user->name, $this->courier_stat_id);

                }

//                if(!$errors)
//                    StatMapHistory::addItem(StatMapMapping::TYPE_COURIER, $this->id, $this->stat_map_id, $currentStatMap, $status_date, $location);

                if (!$errors && !$doNotNotify) {
                    $_StatNotifer = new S_StatNotifer(S_StatNotifer::SERVICE_COURIER, $this->id, $newStatModel->id, $status_date, $location);
                    $_StatNotifer->onStatusChange();
                }
            } else {

                if (!$this->save(false, ['courier_stat_id', 'location', 'date_updated', 'first_scan_sp_point_id']))
//                if (!$this->save(false, ['courier_stat_id', 'location', 'date_updated', 'first_scan_sp_point_id', 'last_status_update']))
                    $errors = true;
                else if(Yii::app()->params['backend'] && !$viaAutomaticTt)
                    $this->addToLog('Status changed form #' . $statIdBefore . ' to #' . $this->courier_stat_id . ' by admin', CourierLog::CAT_INFO, Yii::app()->params['console'] ? false : Yii::app()->user->name);


//                if (!$errors) {
//                    if (!$doNotNotify) {
//                        $_StatNotifer = new S_StatNotifer(S_StatNotifer::SERVICE_COURIER, $this->id, $newStat->id, $status_date, $location);
//                        $_StatNotifer->onStatusChange();
//                    }
//                }
            }

        } else {

            // @31.07.2017 - set stat map to RETURN even when return status is not last in group
            $statMap = StatMapMapping::findMapForStat($newStatModel->id, StatMapMapping::TYPE_COURIER);
            if($statMap == StatMap::MAP_RETURN)
            {
                $this->stat_map_id = $statMap;
                if (!$this->save(false, ['date_updated', 'stat_map_id', 'first_scan_sp_point_id']))
                    $errors = true;
                else if(Yii::app()->params['backend'] && !$viaAutomaticTt)
                    $this->addToLog('Status changed form #' . $statIdBefore . ' to #' . $this->courier_stat_id . ' by admin', CourierLog::CAT_INFO, Yii::app()->user->name);
            }

            // correct stat map
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('previous_stat_map_id');
            $cmd->from((new StatMapHistory())->tableName());
            $cmd->where('type = :type AND type_item_id = :type_item_id AND stat_map_id = :stat_map_id',
                [
                    ':type' => StatMapMapping::TYPE_COURIER,
                    ':type_item_id' => $this->id,
                    ':stat_map_id' => $this->stat_map_id,
                ]
            );

            $previousStatMap = $cmd->queryScalar();
            $mapForThisStat = $newStatModel->statMapMapping->map_id;

            if($previousStatMap != $mapForThisStat)
            {
                $statMapHistory = new StatMapHistory();
                $statMapHistory->type = StatMapMapping::TYPE_COURIER;
                $statMapHistory->type_item_id = $this->id;
                $statMapHistory->stat_date = $status_date;
                $statMapHistory->location = $location;
                $statMapHistory->previous_stat_map_id = $previousStatMap;
                $statMapHistory->stat_map_id = $mapForThisStat;
                $statMapHistory->date_entered = date('Y-m-d H:i:s');
                $statMapHistory->save();
            }
        }


        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();
            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();
            return true;
        }
    }

    /**
     * Handy for revoking RETURNED map
     * @return bool
     */
    public function refreshStatMap()
    {
        $statMap = StatMapMapping::findMapForStat($this->courier_stat_id, StatMapMapping::TYPE_COURIER);
        $errors = true;

        $oldStatMap = $this->stat_map_id;

        if($statMap && $statMap != $this->stat_map_id) {
            $this->stat_map_id = $statMap;
            if ($this->save(false, ['date_updated', 'stat_map_id'])) {

                $this->afterStatMapChange($oldStatMap, $statMap, true, 'Stat map Refreshed', $this->courierStatHistoryLast->location, $this->courierStatHistoryLast->status_date, Yii::app()->user->name, $this->courier_stat_id);
//                $this->addToLog('Stat map refreshed!', CourierLog::CAT_INFO, Yii::app()->user->name);
                $errors = false;
            }
        }

        return !$errors;
    }


    public function getDaysFromStatusChange()
    {
        // NEW WAY - just count days from date_updated

        $date1 = new DateTime();

        if($this->date_updated == '')
            $date = $this->date_entered;
        else
            $date = $this->date_updated;

        $date2 = new DateTime($date);

        return $date2->diff($date1)->format("%a");

        // OLD WAY:
//                $cmd = Yii::app()->db->cache(60*60*6)->createCommand();
//                $cmd = Yii::app()->db->createCommand();
//                $cmd->select("(SELECT TIMESTAMPDIFF(DAY , date_entered, CURRENT_TIMESTAMP) FROM courier_stat_history
//        WHERE courier_id = courier.id ORDER BY date_entered DESC LIMIT 0,1)");
//                $cmd->from("courier");
//                $cmd->andWhere("courier.id = '".$this->id."'");
//                return $cmd->queryScalar();
    }

    public static function getDimensionUnit()
    {
        return self::$_dimension_unit;
    }

    public static function getWeightUnit()
    {
        return self::$_weight_unit;
    }

    public static function validateExternalTypeGroup(array $couriers, $mode = NULL)
    {
        $errors = false;

        /* @var $courier Courier */
        foreach($couriers AS $key => $courier)
        {

            if(!$courier->validateExternalType(false, $mode))
                $errors = true;

            $couriers[$key] = $courier;
        }

        if($errors)
        {
            return $couriers;
        } else {
            return true;
        }

    }

    public static function saveExternalTypeGroup(array $couriers, $mode = NULL, $setRetreiveStatus = true)
    {
        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;



        /* @var $courier Courier */
        foreach($couriers AS $key => $courier)
        {

            if(!$courier->saveExternalType(false, $mode, $setRetreiveStatus))
                $errors = true;

            $couriers[$key] = $courier;
        }


        if($errors)
        {
            $transaction->rollback();
            return $couriers;
        } else {
            $transaction->commit();
            return true;
        }

    }

    public function validateExternalType($mode = NULL)
    {


        $errors = false;

        if($mode != NULL)
            $this->courierTypeExternal->scenario = $mode;
        $this->courierTypeExternal->validate();

        if($this->courierTypeExternal == NULL OR  $this->courierTypeExternal->hasErrors())
            $errors = true;


        if(!$this->validateWithAddressData($mode))
            $errors = true;



        if($errors)
        {
            return false;
        } else {
            return true;
        }

    }

    public function saveExternalType($ownTransaction = true, $mode = NULL, $setRetreiveStatus = true)
    {

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();
        $errors = false;


        if($mode != NULL)
            $this->courierTypeExternal->scenario = $mode;
        $this->courierTypeExternal->validate();


        $this->courierTypeExternal->validate(array('regulations', 'regulations_rodo'));

        if($this->courierTypeExternal == NULL OR $this->courierTypeExternal->hasErrors())
            $errors = true;

        $this->courierTypeExternal->save();
        $this->courier_type_external_id = $this->courierTypeExternal->id;


        $this->stat_map_id = StatMapMapping::findMapForStat($this->courier_stat_id, StatMapMapping::TYPE_COURIER);

        if(!$this->saveWithAddressData(false, $mode))
            $errors = true;

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();
            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            if($setRetreiveStatus)
                $this->changeStat(CourierStat::PACKAGE_RETREIVE);

            return true;
        }

    }

    public function deleteExternalType()
    {
        $transaction = Yii::app()->db->beginTransaction();

        try
        {
            $this->courierTypeExternal->delete();
            $this->senderAddressData->delete();
            $this->receiverAddressData->delete();
            $this->delete();

        }
        catch(Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            $transaction->rollback();

            return false;
        }
        $transaction->commit();
        return true;

    }

    public function deleteLastStatus($ownTransaction = true, $withStatMapUpdate = false)
    {
        if($this->isStatHistoryArchived())
            return false;


        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        /* @var $history CourierStatHistory */
        try
        {
            $history = $this->courierStatHistories(array('order' => 'status_date DESC'));
            if(S_Useful::sizeof($history) < 2)
                return false;

            $lastHistory = $history[0];
            $historyToRestore = $history[1];

            $lastStatusId = $lastHistory->id;
            $this->courier_stat_id = $historyToRestore->current_stat_id;

            $updateAttributes = [];
            $updateAttributes[] = 'courier_stat_id';

            if($withStatMapUpdate) {
                $this->stat_map_id = StatMapMapping::findMapForStat($this->courier_stat_id, StatMapMapping::TYPE_COURIER);
                $updateAttributes[] = 'stat_map_id';
            }

            $lastHistory->delete();



            $this->save(false, $updateAttributes);

            $this->addToLog('Last status has been deleted. Id of deleted status: #'.$lastStatusId, CourierLog::CAT_INFO, Yii::app()->user->name);

        }
        catch(Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            if($ownTransaction)
                $transaction->rollback();

            return false;
        }

        if($ownTransaction)
            $transaction->commit();
        return true;
    }

    protected function validateWithAddressData($mode = NULL)
    {

        $errors = false;

        // if attributes are set then validate & save

        if($mode != NULL)
            $this->senderAddressData->scenario = $mode;
        $this->senderAddressData->validate();

        if($this->senderAddressData->hasErrors())
            $errors = true;

        // if attributes are set then validate & save
        if($mode != NULL)
            $this->receiverAddressData->scenario = $mode;

        $this->receiverAddressData->validate();

        if($this->receiverAddressData->hasErrors())
            $errors = true;

        if($mode != NULL)
            $this->scenario = $mode;
        $this->validate();

        if($this->hasErrors())
            $errors = true;


        if(!$errors)
        {
            return true;
        } else {
            return false;
        }

    }

    public function saveWithAddressData($ownTransaction = true, $mode = NULL)
    {

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        // if attributes are set then validate & save

        if($mode != NULL)
            $this->senderAddressData->scenario = $mode;

        $this->senderAddressData->validate();


        if($this->senderAddressData->hasErrors())
            $errors = true;
        else
        {
            $this->senderAddressData->save();
            $this->sender_address_data_id = $this->senderAddressData->id;
        }


        //  if attributes are set then validate & save

        if($mode != NULL)
            $this->receiverAddressData->scenario = $mode;

        $this->receiverAddressData->validate();

        if($this->receiverAddressData->hasErrors())
            $errors = true;
        else
        {
            $this->receiverAddressData->save();
            $this->receiver_address_data_id = $this->receiverAddressData->id;
        }

        if($mode != NULL)
            $this->scenario = $mode;
        $this->validate();

        if($this->hasErrors())
            $errors = true;

        $this->save();

        if(!$errors)
        {
            if($ownTransaction)
                $transaction->commit();
            return true;
        } else {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        }

    }

    /**
     * @param bool $forceNotSaved Ignore that model has already ID and try to get additions by array of additions ids
     * @return array|mixed|null|static
     */
    public function listAdditions($forceNotSaved = false)
    {
        if($this->id == NULL OR $forceNotSaved)
            $additions = CourierAdditionList::model()->findAllByAttributes(array('id' => $this->_courier_additions));
        else
            $additions = CourierAddition::model()->findAllByAttributes(array('courier_id' => $this->id));

        return $additions;
    }

    public static function changeStatForGroup(array $models, $stat_id, $date = NULL, $location = NULL)
    {

        /* @var $model Courier */

        $courierStat = CourierStat::model()->findByPk($stat_id);
        if($courierStat == NULL)
            return false;

        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $i = 0;
        foreach($models AS $model)
        {
//            $statIdBefore = $model->courier_stat_id;
            if(!$model->changeStat($stat_id,'',$date,false, $location))
                $errors = true;
//            else {
//                $statIdAfter = $model->courier_stat_id;
//                $model->addToLog('Status changed form #' . $statIdBefore . ' to #' . $statIdAfter . ' by list', CourierLog::CAT_INFO, Yii::app()->user->name);
//            }
            $i++;
        }


        if(!$errors)
        {
            $transaction->commit();
            return $i;
        } else {
            $transaction->rollback();
            return false;
        }
    }


    public static function deleteLastStatForGroup(array $models)
    {
        /* @var $model Courier */

        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $i = 0;
        foreach($models AS $model)
        {
            if(!$model->deleteLastStatus(false))
                $errors = true;

            $i++;
        }


        if(!$errors)
        {
            $transaction->commit();
            return $i;
        } else {
            $transaction->rollback();
            return false;
        }
    }


    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    public function cancelPackage()
    {
        if($this->courier_stat_id == CourierStat::NEW_ORDER)
        {
            return $this->changeStat(CourierStat::CANCELLED,5);
        }
        else
            return false;
    }

    public static function cancelPackages(array $models)
    {

        $i = 0;
        foreach($models AS $model)
        {
            if($model->courier_stat_id == CourierStat::NEW_ORDER)
            {
                if($model->cancelPackage())
                    $i++;
            }
        }

        return $i;
    }




// FOR EXTERNAL
    public static function generateCarriageTicketsNotGenerated($type = NULL)
    {
        $models = Courier::model()->with('courierPdf')->findAllByAttributes(array('user_id' => Yii::app()->user->id), 'courier_stat_id != :cancelled_stat AND courier_stat_id != :cancelled_stat2 AND (courierPdf.stat IS NULL OR courierPdf.stat != 1) AND courier_type_external_id IS NOT NULL', array(':cancelled_stat' => CourierStat::CANCELLED, ':cancelled_stat2' => CourierStat::CANCELLED_USER));

        if(!S_Useful::sizeof($models))
            return false;

        LabelPrinter::generateLabels($models, $type);

        return true;
    }

    // FOR EXTERNAL
    public static function generateCarriageTicketsTodays($type = NULL)
    {
        $models = Courier::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id), 'courier_stat_id != :cancelled_stat AND courier_stat_id != :cancelled_stat2 AND courier_type_external_id IS NOT NULL AND  date_entered >= DATE_SUB( CURDATE( ) , INTERVAL 1 DAY ) AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', array(':cancelled_stat' => CourierStat::CANCELLED, ':cancelled_stat2' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_REQUEST, ':uc2' => Courier::USER_CANCEL_WARNED, ':uc3' => Courier::USER_CANCEL_CONFIRMED));

        if(!S_Useful::sizeof($models))
            return false;

        LabelPrinter::generateLabels($models, $type);

        return true;
    }


    public function saveNewPackageInternal($saveSenderToBook = false, $saveReceiverToBook = false, $ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $courierPackagesFamily = [];
        $courierParentId = NULL;

        if(!$this->packages_number)
        {
            $errors = true;
            $transaction->rollback();
            return false;
        }


        for($i = 0; $i < $this->packages_number; $i++) {

            if($errors)
                continue;

            $courier = clone $this;
            $courier->courierTypeInternal = clone $this->courierTypeInternal;
            $courier->receiverAddressData = clone $this->receiverAddressData;
            $courier->senderAddressData = clone $this->senderAddressData;

            $courier->courierTypeInternal->courier = $courier;

//            $courier->courierTypeInternal->_package_wrapping = $this->courierTypeInternal>_package_wrapping;
            /**
             * RECEIVER DATA
             */
            if ($courier->receiverAddressData === NULL)
                $courier->receiverAddressData = new AddressData();

            $courier->receiverAddressData->validate();
            if ($courier->receiverAddressData->hasErrors()) {
                $errors = true;
                Yii::log('New Courier Internal - after: validate receiver', CLogger::LEVEL_ERROR);
                Yii::log(print_r($courier->receiverAddressData->getErrors(),1), CLogger::LEVEL_ERROR);
            }
            else {
                $courier->receiverAddressData->save();
                $courier->receiver_address_data_id = $courier->receiverAddressData->id;

//                if ($saveReceiverToBook)
//                    UserContactBook::createAndSave(false, $courier->receiverAddressData->attributes, 'receiver');

            }

            /**
             * SENDER DATA
             */
            if ($courier->senderAddressData === NULL)
                $courier->senderAddressData = new AddressData();

            $courier->senderAddressData->validate();
            if ($courier->senderAddressData->hasErrors()) {
                $errors = true;
                Yii::log('New Courier Internal - after: validate sender', CLogger::LEVEL_ERROR);
                Yii::log(print_r($courier->senderAddressData->getErrors(),1), CLogger::LEVEL_ERROR);
            }
            else {
                $courier->senderAddressData->save();
                $courier->sender_address_data_id = $courier->senderAddressData->id;

//                if ($saveSenderToBook)
//                    UserContactBook::createAndSave(false, $courier->senderAddressData->attributes, 'sender');

            }

            $courier->courierTypeInternal->parent_courier_id = $courierParentId;

            if (!$courier->courierTypeInternal->saveNewData($i + 1))
                $errors = true;
            $courier->courier_type_internal_id = $courier->courierTypeInternal->id;

            if($errors)
                Yii::log('New Courier Internal - after: saving courierTypeInternal', CLogger::LEVEL_ERROR);

            //

            /**
             * ORDER DATA
             */
            $courier->scenario = 'insert';

            if (!$courier->save()) {
                Yii::log(print_r($courier->getErrors(),1), CLogger::LEVEL_ERROR);
                $errors = true;
            }

            if($errors)
                Yii::log('New Courier Internal - after: saving Courier', CLogger::LEVEL_ERROR);

            /* @var $item CourierAdditionList */
            foreach($courier->courierTypeInternal->listAdditions(true) AS $item)
            {
                $addition = new CourierAddition();
                $addition->courier_id = $courier->id;
                $addition->description = $item->courierAdditionListTr->description;
                $addition->price = Yii::app()->PriceManager->getFinalPrice($item->getPrice());
                $addition->name = $item->courierAdditionListTr->title;
                $addition->courier_addition_list_id = $item->id;

                if(!$addition->save()) {
                    Yii::log(print_r($addition->getErrors(),1), CLogger::LEVEL_ERROR);
                    $errors = true;
                }

            }

            if(!$i)
                $courierParentId = $courier->id;

            array_push($courierPackagesFamily, $courier);
        }

        if($errors)
            Yii::log('New Courier Internal - after: saving additions', CLogger::LEVEL_ERROR);

        $orderProducts = [];
        if(!$errors)
        {
            foreach($courierPackagesFamily AS $courierMember)
            {
                $product = OrderProduct::createNewProduct(
                    OrderProduct::TYPE_COURIER,
                    $courierMember->id,
                    Yii::app()->PriceManager->getCurrencyCode(),
                    $this->courierTypeInternal->priceEach(Yii::app()->PriceManager->getCurrency()),
                    Yii::app()->PriceManager->getFinalPrice($this->courierTypeInternal->priceEach(Yii::app()->PriceManager->getCurrency()))
                );

                if(!$product)
                    $errors = true;

                array_push($orderProducts, $product);
            }
        }

        if($errors)
            Yii::log('New Courier Internal - after: OrderProduct::createNewProduct', CLogger::LEVEL_ERROR);

        if(!$errors)
        {
            $order = Order::createOrderForProducts($orderProducts, true);

            if(!$order)
                $errors = true;
        }

        if($errors)
            Yii::log('New Courier Internal - after: Order::createOrderForProducts', CLogger::LEVEL_ERROR);

        if(!$errors)
        {
            if($ownTransaction)
                $transaction->commit();
            return
                [
                    'order' => $order,
                    'couriers' => $courierPackagesFamily
                ];
        }
        else
        {
            if($ownTransaction)
                $transaction->rollBack();
            return false;
        }
    }

    /**
     * Check if package has COD. Universal for all package types
     * @return bool
     */
    public function isCod()
    {
        if($this->cod)
            return true;
        else
            return false;

//        $type = $this->getType();
//
//        if($type == self::TYPE_INTERNAL && $this->courierTypeInternal->cod_value > 0)
//            return true;
//        else if($type == self::TYPE_DOMESTIC && $this->courierTypeDomestic->cod_value > 0)
//            return true;
//        else if($this->cod > 0)
//            return true;
//
//        return false;
    }

    /**
     * Return package COD value. Universal for all package types
     * @return bool|float
     */
    public function getCodValue()
    {



        if($this->isCod())
        {
            return $this->cod_value;

//
//            $type = $this->getType();
//
//            if($type == self::TYPE_INTERNAL)
//                return $this->courierTypeInternal->cod_value;
//            else if($type == self::TYPE_DOMESTIC)
//                return $this->courierTypeDomestic->cod_value;
//            else
//                return $this->cod;

        }

        return false;
    }

    /**
     * Return package COD currency. Universal for all package types
     * @return bool|string
     */
    public function getCodCurrency()
    {



        if($this->isCod())
        {
            return $this->cod_currency;

//            $type = $this->getType();
//
//            if($type == self::TYPE_INTERNAL)
//                return $this->courierTypeInternal->cod_currency;
//            else if($type == self::TYPE_DOMESTIC)
//                return $this->courierTypeDomestic->cod_currency;
//            else
//                return '';
        }

        return false;

    }

    public function getCourierStatHistoryLast()
    {
        return CourierStatHistory::model()->find(['condition' => 'courier_id = :courier_id AND current_stat_id = :stat', 'params' => [':courier_id' => $this->id, ':stat' => $this->courier_stat_id], 'order' => 'status_date DESC']);
    }

    /**
     * After creating label, send to eBay.com data about package and update import status.
     * Method also used for linker
     *
     * @param Courier $courier Package model
     * @param $operator string Operator name to be send to eBay.com
     * @param $ttNumber string Package number to be send to eBay.com
     * @param $ebay_order_id string Ebay order ID
     * @param bool|false $forceCancelled If true, just update EbayImport data with cancelled stat - for example whet ordering label failed
     * @return bool
     */
    public static function updateEbayData(Courier $courier, $operator, $ttNumber, $ebay_order_id, $forceCancelled = false)
    {

        if(!$forceCancelled) {

            /* @var $courierEbayImport EbayImport */
            $courierEbayImport = EbayImport::model()->find([
                'condition' => 'ebay_order_id = :ebay_order_id AND user_id = :user_id AND target = :target',
                'params' => [
                    ':ebay_order_id' => $ebay_order_id,
                    ':user_id' => $courier->user_id,
                    ':target' => EbayImport::TARGET_COURIER
                ],
                'order' => 'id DESC']);

            if($courierEbayImport == NULL)
            {
                MyDump::dump('ebay_import_not_found.txt', print_r($courier->id,1));
            }

            if($courierEbayImport->source == EbayImport::SOURCE_LINKER)
            {
                $ebayUpdated = LinkerClient::updateItem($ebay_order_id, $ttNumber, $operator, $courier->user->apikey);
            } else {

                MyDump::dump('ebay_update_tt.txt', print_r($courierEbayImport->attributes,1).' : COURIER ID: '.$courier->id);

                $ebayClient = ebayClient::instance($courierEbayImport->authToken);
                $ebayUpdated = $ebayClient->updateTt($ebay_order_id, $ttNumber, $operator);
            }

            if ($ebayUpdated)
                $ebayStat = EbayImport::STAT_SUCCESS;
            else
                $ebayStat = EbayImport::STAT_ERROR;


            // if internal, save there data about ebay
            if($courier->getType() == Courier::TYPE_INTERNAL)
            {
                $courier->courierTypeInternal->_ebay_export_status = $ebayStat;
                $courier->courierTypeInternal->update();
            }

        } else
            $ebayStat = EbayImport::STAT_CANCELLED;

        $cmd = Yii::app()->db->createCommand();
        $cmd->update((new EbayImport())->tableName(), [
            'stat' => $ebayStat,
            'authToken' => NULL,
            'date_updated' => date('Y-m-d H:i:s'),
        ],
            'target_item_id = :courier_id AND target = :target',
            [
                ':courier_id' => $courier->id,
                ':target' => EbayImport::TARGET_COURIER
            ]
        );

        return $ebayStat == EbayImport::STAT_SUCCESS ? true : false;
    }

    /**
     * Gets label for package
     * @param bool|true $throwErrors Whether to throw errors or just retur false
     * @return CourierLabelNew
     * @throws Exception
     */
    public function getSingleLabel($throwErrors = true, $forceSecond = false, $ignoreErrors = false)
    {

        switch($this->getType())
        {
            case self::TYPE_INTERNAL:

                if($forceSecond)
                    $first = false;
                else {
                    if ($this->user->getAllowCourierInternalExtendedLabel())
                        $first = false;
                    else
                        $first = true;
                }

                $label = $this->courierTypeInternal->getSingleLabel($first)->courierLabelNew;
                break;
            case self::TYPE_DOMESTIC:
                $label = $this->courierTypeDomestic->courierLabelNew;
                break;
            case self::TYPE_INTERNAL_SIMPLE:
                $label = $this->courierTypeInternalSimple->courierExtOperatorDetails->courierLabelNew;
                break;
            case self::TYPE_INTERNAL_OOE:
                $label = $this->courierTypeOoe->courierLabelNew;
                break;
            case self::TYPE_U:
                $label = $this->courierTypeU->courierLabelNew;
                break;
            case self::TYPE_RETURN:
                $label = $this->courierTypeReturn->courierLabelNew;
                break;
            default:
                if($throwErrors)
                    throw new Exception('Unknow package type!');
                else
                    return false;
        }


        if(!($label instanceof CourierLabelNew))
        {
            if($throwErrors)
                throw new Exception('Label not found!');
            else
                return false;
        }


        if($label->stat != CourierLabelNew::STAT_SUCCESS && !$ignoreErrors) {
            if($throwErrors)
                throw new Exception('Label is incorrect!');
            else
                return false;
        }

        return $label;
    }


    /**
     * Returns package owner's user ID
     * @return int|null
     */
    public function getOwnerUserId()
    {
        if($this->isNewRecord)
        {
            if($this->user_id === NULL && !Yii::app()->user->isGuest)
                return Yii::app()->user->id;
            else
                return $this->user_id;

        } else
            return $this->user_id;
    }

    /**
     * Returns package owner's user group ID
     * @return int
     */
    protected $_ownerUserGroupId;
    public function getOwnerUserGroupId()
    {
        if($this->_ownerUserGroupId === NULL)
            $this->_ownerUserGroupId = User::getUserGroupId($this->getOwnerUserId());

        return $this->_ownerUserGroupId;
    }

    /**
     * Override user group id for this package. Used for simulation
     * @param $id User group id
     */
    public function overrideOwnerUserGroupId($id)
    {
        $this->_ownerUserGroupId = $id;
    }

    /**
     * Returns array with packages external operators and ids
     * @param bool|false $internalJustOne
     * @return array First item - array of operators, second item - array of ids
     */
    public function getExternalOperatorsAndIds($internalJustOne = false)
    {
        $remoteIds = [];
        $operators = [];

        if ($this->getType() == self::TYPE_INTERNAL_SIMPLE) {
            array_push($operators, $this->courierTypeInternalSimple->getOperator());
            array_push($remoteIds, $this->courierTypeInternalSimple->courierExtOperatorDetails->getTrack_id());
        }
        else if ($this->getType() == self::TYPE_EXTERNAL) {
            array_push($operators, $this->courierTypeExternal->courierExternalOperator->name);
            array_push($remoteIds, $this->courierTypeExternal->external_id);
        } else if ($this->getType() == self::TYPE_DOMESTIC) {
            array_push($operators, $this->courierTypeDomestic->getOperator()->name);
            array_push($remoteIds, $this->courierTypeDomestic->courierLabelNew->track_id);
        } else if ($this->getType() == self::TYPE_INTERNAL_OOE) {
            array_push($operators, $this->courierTypeOoe->service_code);
            array_push($remoteIds, $this->courierTypeOoe->remote_id);
        } else if ($this->getType() == self::TYPE_INTERNAL) {

            if ($internalJustOne) {

                if($this->courierTypeInternal->common_operator_ordered)
                {
                    // for common label, operator name = delivery name
                    array_push($operators, $this->courierTypeInternal->delivery_operator_name);

                    if ($this->courierTypeInternal->commonLabel !== NULL)
                        array_push($remoteIds, $this->courierTypeInternal->commonLabel->track_id);

                } else {
                    if ($this->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR && ($this->user == NULL OR !$this->user->getAllowCourierInternalExtendedLabel())) {
                        array_push($operators, $this->courierTypeInternal->pickup_operator_name);

                        if ($this->courierTypeInternal->pickupLabel !== NULL)
                            array_push($remoteIds, $this->courierTypeInternal->pickupLabel->track_id);
                    }
                    else
                    {
                        array_push($operators, $this->courierTypeInternal->delivery_operator_name);

                        if ($this->courierTypeInternal->deliveryLabel !== NULL)
                            array_push($remoteIds, $this->courierTypeInternal->deliveryLabel->track_id);

                    }
                }

            } else {

                if ($this->courierTypeInternal->pickup_operator_name != '') {
                    array_push($operators, $this->courierTypeInternal->pickup_operator_name);
                    if ($this->courierTypeInternal->pickupLabel !== NULL)
                        array_push($remoteIds, $this->courierTypeInternal->pickupLabel->track_id);
                }

                if ($this->courierTypeInternal->delivery_operator_name != '' && $this->courierTypeInternal->delivery_operator_name != $this->courierTypeInternal->pickup_operator_name) {
                    array_push($operators, $this->courierTypeInternal->delivery_operator_name);
                    if ($this->courierTypeInternal->deliveryLabel !== NULL)
                        array_push($remoteIds, $this->courierTypeInternal->deliveryLabel->track_id);
                }

                if ($this->courierTypeInternal->commonLabel !== NULL)
                    array_push($remoteIds, $this->courierTypeInternal->commonLabel->track_id);

            }
        }

        return [$operators, $remoteIds];
    }

    /**
     * Returns date of last stat map change
     * @return string
     */
    public function getLastStatMapUpdate()
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('date_entered')->from(StatMapHistory::TABLE_NAME)->where('type = :type AND type_item_id = :id', [':type' => StatMapMapping::TYPE_COURIER, ':id' => $this->id])->order('date_entered DESC')->limit(1);

        return $cmd->queryScalar();
    }

    /**
     * Returns date of last stat
     * @return string
     */
    public function getLastStatUpdate()
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('status_date')->from('courier_stat_history')->where('courier_id = :id', [':id' => $this->id])->order('status_date DESC')->limit(1);

        return $cmd->queryScalar();
    }

    public function onAfterAckGeneration()
    {
        $this->ack_generated = new CDbExpression('NOW()');
        $this->update(['ack_generated']);
    }

    public static function onAfterAckGenerationGroup(array $ids)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->update((new self)->tableName(),['ack_generated' => new CDbExpression('NOW()')], ['in', 'id', $ids]);
    }

    /**
     * List of external Id's based on CourierLabelNew - might not work on veery old packages ;)
     * @param bool $internalJustOne
     * @param string $separator
     * @return string
     */
    public function listExternalIdsByCourierLabelNew($internalJustOne = true, $separator = '; ')
    {

        $type = $this->getType();
        $userAllowExtendedLabelGeneration = $this->user->getAllowCourierInternalExtendedLabel();

        $ids = [];
        foreach($this->courierLabelNew AS $item)
        {
            if($type == self::TYPE_INTERNAL && $internalJustOne)
            {
                if($item->_internal_mode == CourierLabelNew::INTERNAL_MODE_FIRST)
                {
                    if($userAllowExtendedLabelGeneration) // do not show first label ID
                        continue;
                }
                else if($item->_internal_mode == CourierLabelNew::INTERNAL_MODE_SECOND)
                {
                    if(!$userAllowExtendedLabelGeneration) // do not show second label ID
                        continue;
                }
            }

            if($item->track_id != '')
                $ids[] = $item->track_id;
        }

        if($this->courierTypeExternal)
            $ids[] = $this->courierTypeExternal->external_id;

        $ids = implode($separator, $ids);

        return $ids;
    }

    public static function _listOfStatsPreventingGeneratingLabels()
    {
        return [
            CourierStat::CANCELLED,
            CourierStat::NEW_ORDER,
            CourierStat::PACKAGE_PROCESSING,
            CourierStat::CANCELLED_USER,
        ];
    }

    public function isLabelGeneratingAvailableBaseOnStat()
    {
        return !in_array($this->courier_stat_id, self::_listOfStatsPreventingGeneratingLabels());
    }


    public function getStatMapName()
    {
        if($this->stat_map_id)
            return StatMap::getMapNameById($this->stat_map_id);
        else
            return false;
    }

    /**
     * @param bool $forceCustomerWeight
     * @return float
     */
    public function getWeight($forceCustomerWeight = false)
    {
        if($forceCustomerWeight)
        {
            if($this->package_weight_client > 0)
                return $this->package_weight_client;
        }

        return $this->package_weight;
    }

    public function getFamilyMemberNumber()
    {
        switch($this->getType())
        {
            case self::TYPE_DOMESTIC:
                return $this->courierTypeDomestic->family_member_number;
                break;
            case self::TYPE_U:
                return $this->courierTypeU->family_member_number;
                break;
            case self::TYPE_INTERNAL:
                return $this->courierTypeInternal->family_member_number;
                break;
            default:
                return 1;
                break;
        }
    }

    public function getFamilyParentCourierId()
    {
        switch($this->getType())
        {
            case self::TYPE_DOMESTIC:
                return $this->courierTypeDomestic->parent_courier_id;
                break;
            case self::TYPE_U:
                return $this->courierTypeU->parent_courier_id;
                break;
            case self::TYPE_INTERNAL:
                return $this->courierTypeInternal->parent_courier_id;
                break;
        }
    }

    /**
     * @return Courier|NULL
     */
    public function getReturnPackageModel()
    {
        $model = CourierTypeReturn::model()->findByAttributes(['base_courier_id' => $this->id]);
        if($model)
            $model = $model->courier;

        return $model;
    }

    /**
     * @return Courier|NULL
     */
    public function getReturnPackageParentModel()
    {
        if($this->getType() != self::TYPE_RETURN)
            return NULL;

        return $this->courierTypeReturn->baseCourier;
    }

    public static function getCurrencyList()
    {
        return User::getValueCurrencyList();
    }

    public function getPackageValueCurrency()
    {
        if($this->value_currency == NULL)
            return 'PLN';
        else
            return $this->value_currency;
    }

    public function getPackageValueConverted($targetCurrency)
    {
        return Yii::app()->NBPCurrency->getCurrencyValue($this->package_value, $this->getPackageValueCurrency(), $targetCurrency);
    }


    public function afterStatMapChange($map_id_before, $map_id_after, $addToHistory = false, $saveInLog = false, $historyLocation = NULL, $historyDate = NULL, $logAuthor = false, $stat_id = false)
    {

        if($map_id_before == StatMap::MAP_NEW && in_array($map_id_after, [StatMap::MAP_IN_TRANSPORTATION, StatMap::MAP_DELIVERED_TO_HUB]) && in_array(GLOBAL_BROKERS::BROKER_YUNEXPRESS_SYNCH, $this->listBrokers()))
            ManifestQueque::addToQueque(ManifestQueque::TYPE_COURIER_LABEL, $this->getCourierLabelNewForBroker(GLOBAL_BROKERS::BROKER_YUNEXPRESS_SYNCH)->id, GLOBAL_BROKERS::BROKER_YUNEXPRESS_SYNCH, $this->local_id);


        StatMapMapping::logToFile($this->local_id, $map_id_after, StatMapMapping::TYPE_COURIER, $stat_id, S_Useful::sizeof($this->smsArchive));

        if($saveInLog)
            $this->addToLog($saveInLog, CourierLog::CAT_INFO, $logAuthor);


        if($addToHistory)
            StatMapHistory::addItem(StatMapMapping::TYPE_COURIER, $this->id, $this->stat_map_id, $map_id_before, $historyDate, $historyLocation);
    }

    public static function groupChangeStatusByIds($ids, $stat_id, $author = 0, $location = NULL)
    {
        $models = self::model()->findAllByPk($ids);

        $transaction = Yii::app()->db->beginTransaction();

        try {
            /* @var $models Courier[] */
            foreach ($models AS $model)
                $model->changeStat($stat_id, $author, NULL, false, $location);
        }
        catch(Exception $ex)
        {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    public function listBrokers()
    {
        $cmd = Yii::app()->db->cache(60)->createCommand();
        $cmd->select('operator')->from('courier_label_new')->where('courier_id = :courier_id', [':courier_id' => $this->id]);
        return $cmd->queryColumn();
    }

    /**
     * @param int $broker
     * @return CourierLabelNew|null
     */
    public function getCourierLabelNewForBroker($broker) : CourierLabelNew
    {
        return CourierLabelNew::model()->find('courier_id = :courier_id AND operator = :broker_id', [':courier_id' => $this->id, ':broker_id' => $broker]);
    }
}