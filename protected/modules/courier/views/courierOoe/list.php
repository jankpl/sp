<h2><?php echo Yii::t('site','Your packages');?></h2>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'courier-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'selectableRows'=>2,
    'columns' => array(
        array(
            'name'=>'local_id',
            'header'=>Yii::t('courier','#ID'),
            'type'=>'raw',
            'value'=>'CHtml::link($data->local_id,array("/courier/courier/view/", "urlData" => $data->hash))',
        ),
        array(
            'name'=>'date_entered',
            'value' => 'substr($data->date_entered, 0, 10)',
            'header'=>Yii::t('courier','Data utworzenia'),
        ),
        array(
            'name'=>'__stat',
            'type'=>'raw',
            'filter'=>GxHtml::listDataEx(CourierStat::model()->findAllAttributes(null, true)),
            'header' => Yii::t('courier','Status'),
            'value'=>'CHtml::link($data->stat->courierStatTr->short_text,array("/courier/courier/tt", "urlData" => $data->local_id))',
        ),
        array(
            'name'=>'__sender_country_id',
            'header'=>Yii::t('courier','Sender country'),
            'value'=>'$data->senderAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name' => '__sender_usefulName',
            'header' => Yii::t('courier','Sender'),
            'value' => '$data->senderAddressData->usefulName',
        ),
        array(
            'name'=>'__receiver_country_id',
            'header'=>Yii::t('courier','Receiver country'),
            'value'=>'$data->receiverAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name' => '__receiver_name',
            'header' => Yii::t('courier','Receiver'),
            'value' => '$data->receiverAddressData->usefulName',
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}',
            'buttons'=>array(
                'view' => array(
                    'url' => 'Yii::app()->createUrl("/courier/courier/view/", array("urlData" => $data->hash))',
                ),
            ),
        ),
    ),
));
?>


<?php
$this->widget('FlashPrinter');
?>
<?php $this->widget('BackLink'); ?>





