<?php

Yii::import('application.models._base.BasePocztaPolskaWaiting');

class PocztaPolskaWaiting extends BasePocztaPolskaWaiting
{
    public $_is_sent;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    const TYPE_COURIER = 1;
    const TYPE_POSTAL = 2;

    public static function getTypeList()
    {
        return [
            self::TYPE_COURIER => 'Courier',
            self::TYPE_POSTAL => 'Postal',
        ];
    }

    public static function countFromToday($waiting = false)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(id)');
        $cmd->from('poczta_polska_waiting');


        if($waiting)
            $cmd->where('date_sent IS NULL AND date_entered LIKE "'.date('Y-m-d').'%"');
        else
            $cmd->where('date_sent IS NOT NULL AND date_sent LIKE "'.date('Y-m-d').'%"');

        return $cmd->queryScalar();
    }

    public static function countTotal($waiting = false)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(id)');
        $cmd->from('poczta_polska_waiting');

        if($waiting)
            $cmd->where('date_sent IS NULL');
        else
            $cmd->where('date_sent IS NOT NULL');


        return $cmd->queryScalar();
    }

    public static function countFromThisWeek($waiting = false)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(id)');
        $cmd->from('poczta_polska_waiting');

        if($waiting)
            $cmd->where('date_sent IS NULL AND date_entered >= DATE(NOW()) - INTERVAL 7 DAY');
        else
            $cmd->where('date_sent IS NOT NULL AND date_sent >= DATE(NOW()) - INTERVAL 7 DAY');

        return $cmd->queryScalar();
    }


    public function getTypeName()
    {
        return isset(self::getTypeList()[$this->type]) ? self::getTypeList()[$this->type] : '';
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    public function rules() {
        return array(
            array('type, type_id, guid, remote_id, envelope_id, local_id, pp_account_id', 'required'),
            array('type, type_id, envelope_id, pp_account_id', 'numerical', 'integerOnly'=>true),
            array('guid, remote_id, local_id', 'length', 'max'=>64),
            array('date_sent, log', 'safe'),
            array('date_sent, log', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, type, type_id, guid, remote_id, envelope_id, date_entered, date_updated, date_sent, log, local_id, _is_sent, pp_account_id, date_posting', 'safe', 'on'=>'search'),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('type', $this->type);
        $criteria->compare('type_id', $this->type_id);
        $criteria->compare('guid', $this->guid, true);
        $criteria->compare('remote_id', $this->remote_id, true);
        $criteria->compare('envelope_id', $this->envelope_id);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('date_sent', $this->date_sent, true);
        $criteria->compare('log', $this->log, true);
        $criteria->compare('local_id', $this->local_id, true);
        $criteria->compare('pp_account_id', $this->pp_account_id);
        $criteria->compare('date_posting', $this->date_posting, true);

        if($this->_is_sent == 1)
            $criteria->addCondition('date_sent IS NOT NULL');
        else if($this->_is_sent == -1)
            $criteria->addCondition('date_sent IS NULL');

        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
            'sort' => $sort,
        ));
    }

    public static function addToWaitingLine($type, $type_id, $local_id, $guid, $remote_id, $envelope_id, $pp_account_id, $date_posting)
    {
        $model = new self;
        $model->type = $type;
        $model->type_id = $type_id;
        $model->guid = $guid;
        $model->remote_id = $remote_id;
        $model->envelope_id = $envelope_id;
        $model->local_id = $local_id;
        $model->pp_account_id = $pp_account_id;
        $model->date_posting = $date_posting;

        $model->validate();

        return $model->save();
    }

    public static function updateBuffor($guid, $newEnvelope, $log = false)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();

        if($log)
            return $cmd->update('poczta_polska_waiting', ['envelope_id' => $newEnvelope, 'log' => $log, 'date_updated' => date('Y-m-d H:i:s')], 'guid = :guid', [':guid' => $guid]);
        else
            return $cmd->update('poczta_polska_waiting', ['envelope_id' => $newEnvelope, 'date_updated' => date('Y-m-d H:i:s')], 'guid = :guid', [':guid' => $guid]);

    }

    public static function setLog($guid, $text)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        return $cmd->update('poczta_polska_waiting', ['log' => $text, 'date_updated' => date('Y-m-d H:i:s')], 'guid = :guid', [':guid' => $guid]);
    }

    public static function setEnvelopeSent($envelopeId, $envelopeName)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        return $cmd->update('poczta_polska_waiting', ['log' => '', 'date_sent' => date('Y-m-d H:i:s'), 'date_updated' => date('Y-m-d H:i:s'), 'envelope_name' => $envelopeName], 'envelope_id = :envelope_id', [':envelope_id' => $envelopeId]);
    }


}