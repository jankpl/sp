<?php
/* @var $this SpPointsController */
/* @var $model SpPoints */

$this->breadcrumbs=array(
	'Sp Points'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create SpPoints', 'url'=>array('create')),
);

?>

<h1>Manage Sp Points</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'sp-points-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'date_entered',
		'addressData.country0.code',
		'addressData.city',
		'addressData.address_line_1',
		[
			'name' => 'stat',
			'value'=>'S_Status::stat($data->stat)',
			'filter' => CHtml::listData(S_Status::listStats(),'id','name'),
		],
		array(
			'class' => 'CButtonColumn',
			'template'=>'{update} {view} {delete} {status} {status_a}',
			'htmlOptions'=> array('style'=>'width:80px'),
			'buttons'=>array(
				'status'=>array(
					'visible' => '$data->stat==1',
					'label'=>'deactivate',
					'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_active.png',
					'url' => 'Yii::app()->createUrl("spPoints/stat",array("id" => $data->id))',
				),
				'status_a'=>array(
					'visible' => '$data->stat==0',
					'label'=>'activate',
					'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_not_active.png',
					'url' => 'Yii::app()->createUrl("spPoints/stat",array("id" => $data->id))',
				),
			),
		),
	),
)); ?>
