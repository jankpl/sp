<?php
/* @var $this SpPointsController */
/* @var $model SpPoints */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'sp-points-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableClientValidation'=>true,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary([$model, $model->addressData]); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>123)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'stat'); ?>
		<?php echo $form->dropDownList($model,'stat', CHtml::listData(S_Status::listStats(), 'id', 'name')); ?>
		<?php echo $form->error($model,'stat'); ?>
	</div>

	<div id="langVersionMenuContainer">
		<?php
		foreach(Language::model()->findAll('stat = 1') AS $languageItem):



			echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';


		endforeach;
		?>
	</div>


	<div id="langVersionContainer">
		<?php
		foreach(Language::model()->findAll('stat = 1') AS $languageItem):

			echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

			$model_tr = $modelTrs[$languageItem->id];

			if($model_tr === null)
			{
				$model_tr = new SpPointsTr();
				$model_tr->sp_points_id = $model->id;
				$model_tr->language_id = $languageItem->id;
			}

			$this->renderPartial('_tr_form', array(
				'model' => $model_tr,
				'form' => $form,
				'i' => $languageItem->id,
			));

			echo'</div>';

		endforeach;
		?>
	</div>

	<?php

	Yii::app()->clientScript->registerCoreScript('jquery');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

	?>

	<h3>Address data:</h3>

	<?php


	$this->widget('application.backend.components.AddressDataFrom', array(
		'addressData' => $model->addressData,
		'form' => $form,
		'noEmail' => false,
	));

	?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->