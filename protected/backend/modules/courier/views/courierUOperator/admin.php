<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-operator-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'date_entered',
        'date_updated',
//		'name',
//		'description',
        array(
            'name' => 'broker_id',
            'header' => 'Broker',
            'value' => 'CourierUOperator::getBrokerList()[$data->broker_id]',
            'filter' => CourierUOperator::getBrokerList(),
        ),
        [
            'name' => 'partner_id',
            'value' => 'Partners::getPartnerNameById($data->partner_id)',
            'filter' => Partners::getParntersList(),
        ],
        array(
            'name' => 'stat',
            'value' => '$data->statName',
            'filter' => CourierUOperator::getStatList(),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}'
        ),
    ),
)); ?>