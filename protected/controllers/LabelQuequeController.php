<?php

class LabelQuequeController extends CController {

    public function filters()
    {
        return array();
    }

    // run queque
    public function actionIndex($id = NULL, $async = false, $counter = 0, $force = false)
    {

        if($async && $counter)
        {
//            file_put_contents('as.txt','spie '.(10 * $counter)."\r\n", FILE_APPEND);
            // slow down async process to let server rest...
            sleep(1 * $counter);
//            file_put_contents('as.txt','wstaje '.(10 * $counter)."\r\n", FILE_APPEND);
        }

        $counter++;

        if($counter > 60)
            exit;

        Yii::app()->getModule('courier');


        /* @var $parent CourierLabelNew */
        if($id === NULL)
//            $parents = CourierLabelNew::model()->findAll(array('condition' => 'stat = :stat OR stat = :stat2', 'params' => array(':stat' => CourierLabelNew::STAT_NEW, ':stat2' => CourierLabelNew::STAT_NEW_DIRECT), 'order' => 'id ASC', 'limit' => 50));
            $parents = CourierLabelNew::model()->findAll(array('condition' => 'stat = :stat', 'params' => array(':stat' => CourierLabelNew::STAT_NEW), 'order' => 'id ASC', 'limit' => 50));
        else {

            if(!$force)
                $parents = CourierLabelNew::model()->findAll(array('condition' => 'stat = :stat AND (id = :id OR parent_id = :id)', 'params' => array(':stat' => CourierLabelNew::STAT_NEW, ':id' => $id), 'order' => 'id ASC', 'limit' => 1));
            else
                $parents = CourierLabelNew::model()->findAll(array('condition' => '(id = :id OR parent_id = :id)', 'params' => array(':id' => $id), 'order' => 'id ASC', 'limit' => 1));
        }

        // no more found - finish the job...
        if(!S_Useful::sizeof($parents))
            exit;

        foreach($parents AS $parent)
            $parent->runLabelOrdering();
        // fix:
        sleep(1);

        //search for some more labels waiting...
        if($async)
            CourierLabelNew::asyncCallForId($id, $counter);
        else {
            $path = Yii::app()->createAbsoluteUrl('/labelQueque/index', array('id' => $id, 'counter' => $counter));
            header("Location: " . $path);
        }
        exit;
    }

}