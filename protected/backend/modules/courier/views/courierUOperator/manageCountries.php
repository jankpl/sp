<?php
/* @var $model CourierUOperator2CountryGroup */
/* @var $modelOperator CourierUOperator */



?>

    <h2>Custom settings for country group</h2>
    <h3>Operator: <?= $modelOperator->name;?> (#<?= $modelOperator->id;?>)</h3>


<?php
if($userGroup === NULL && !AdminRestrictions::isUltraAdmin()):
    ?>
    <div class="alert alert-warning text-center">You have no authority to modify main routing.</div>
<?php
else:
    ?>

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>


    <div style="word-break: break-all">
        <?php
        $this->widget('MultipleSelect', array('selector' => '#stat-selector'));
        ?>
        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
        <?php $this->_getGridViewManageCountries($operator_id);?>
        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
    </div>

    <h3>Actions for selected</h3>

    <?php
    $content = [];

    echo TbHtml::tabbableTabs(array(
        array('label' => 'Change status', 'active' => true, 'content' => $this->renderPartial('manageCountriesActions/_change_status', ['operator_id' => $operator_id], true)),
        array('label' => 'Set delivery time', 'content' => $this->renderPartial('manageCountriesActions/_set_delivery_time', ['operator_id' => $operator_id] , true)),
        array('label' => 'Set COD', 'content' => $this->renderPartial('manageCountriesActions/_set_cod', ['operator_id' => $operator_id] , true)),
        array('label' => 'Set COD Price', 'content' => $this->renderPartial('manageCountriesActions/_set_cod_price', ['operator_id' => $operator_id] , true)),
    ), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>


    <script>
        $(document).ready(function(){
            $('.confirm-me').on('submit', function(){
                return confirm('Are you sure?');
            });
        })
    </script>

<?php
endif;

