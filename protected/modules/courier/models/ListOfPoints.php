<?php

class ListOfPoints
{
    const DISABLE_CACHE = true;

    public static function get($street, $city, $zip_code, $country, $type = false, $withImages = false)
    {
        $street = trim(strtoupper($street));
        $zip_code = trim(strtoupper($zip_code));
        $city = trim(strtoupper($city));
        $country = trim(strtoupper($country));
        $type = intval($type);

        $CACHE = Yii::app()->cacheFile;

        $CACHE_NAME = 'COURIER_LIST_OF_POINTS_'.$street.'_'.$city.'_'.$zip_code.'_'.$country;
        $CACHE_NAME_WITH_TYPE = $CACHE_NAME.'_'.$type;

        if($type)
            $cacheName = $CACHE_NAME_WITH_TYPE;
        else
            $cacheName = $CACHE_NAME;

        $items = $CACHE->get($cacheName);

        if($items === false && $type) // try to look in general cache
        {
            $cacheName = $CACHE_NAME;
            $items = $CACHE->get($CACHE_NAME);
        }

        if(self::DISABLE_CACHE)
            $items = false;

        if ($items === false OR !S_Useful::sizeof($items)) {
//
            $items = [];
//
//            if($type === CourierTypeInternal::WITH_PICKUP_LP_EXPRESS OR $type == false) {
//
//                $items[CourierTypeInternal::WITH_PICKUP_LP_EXPRESS] = [];
//
//                $terminals = LpExpressClient::getTerminals();
//
//                foreach ($terminals AS $item) {
//
//                    $addressHuman = $item->address;
//                    $addressHuman .= ";" . $item->zip . ', ' . $item->city;
//                    $addressHuman .= ";" . strtoupper($item->address->country0->countryListTr->name);
//
//                    $lat = $item->latitude;
//                    $lng = $item->longitude;
//
//                    $desc = $item->comment . ';';
//                    $desc .= $item->workinghours;
//
//                    $temp = [
//                        'id' => $item->name,
//                        'name' => $item->name,
//                        'lat' => $lat,
//                        'lng' => $lng,
//                        'address' => $addressHuman,
//                        'desc' => $desc,
////                        'type' => CourierTypeInternal::WITH_PICKUP_LP_EXPRESS,
//                    ];
//
//                    if($withImages)
//                        $temp['imgUrl'] = Yii::app()->baseUrl . '/images/v2/lp_express_map_ico.png';
//
//                    $items[CourierTypeInternal::WITH_PICKUP_LP_EXPRESS][] = $temp;
//
//                }
//
//            }
//
//            if($type === CourierTypeInternal::WITH_PICKUP_RUCH OR $type == false) {
//
//                $points = PaczkaWRuchu::getListOfPlacesDetailed();
//
//                foreach ($points AS $point) {
//
//                    $addressHuman = $point['address'];
//
//                    $lat = $point['lat'];
//                    $lng = $point['lng'];
//
//                    $desc = (string)$point['details'] . ';';
//                    $desc .= (string)$point['hours'];
//
//                    $temp = [
//                        'id' => $point['name'],
//                        'name' => $point['name'],
//                        'lat' => $lat,
//                        'lng' => $lng,
//                        'address' => $addressHuman,
//                        'desc' => $desc,
////                        'type' => CourierTypeInternal::WITH_PICKUP_RUCH,
//                    ];
//
//
//                    if($withImages)
//                        $temp['imgUrl'] = Yii::app()->baseUrl . '/images/v2/ruch_map_ico.png';
//
//                    $items[CourierTypeInternal::WITH_PICKUP_RUCH][] = $temp;
//                }
//
//            }
//
//            if($type === CourierTypeInternal::WITH_PICKUP_UPS OR $type == false) {
//
//
//                $zip = $zip_code;
//
//                $aps = UpsApLocation::getPoints($street, $city, $zip, $country);
//                $i = 0;
//                if (is_array($aps))
//                    foreach ($aps AS $item) {
//
//                        $addressHuman = $item['address'];
//                        $addressHuman .= ";" . $item['zip'] . ', ' . $item['city'];
//                        $addressHuman .= ";" . strtoupper($item['country_code']);
//
//                        $temp = [
//                            'id' => $item['name'],
//                            'name' => 'UPS Access Point: '.$item['name'],
//                            'lat' => $item['lat'],
//                            'lng' => $item['lng'],
//                            'address' => $addressHuman,
//                            'desc' => $item['desc'],
////                            'type' => CourierTypeInternal::WITH_PICKUP_UPS,
//                        ];
//
//                        if($withImages)
//                            $temp['imgUrl'] = Yii::app()->baseUrl . '/images/v2/ups_map_ico.png';
//
//                        $items[CourierTypeInternal::WITH_PICKUP_UPS][] = $temp;
//
//                        $i++;
//                    }
//            }
//
//
//
//            if($type === CourierTypeInternal::WITH_PICKUP_RM  OR $type == false) {
//
//
//
//                $offices = RoyalMailClient::getOffices($zip_code);
//                if (is_array($offices))
//                    foreach ($offices AS $office) {
//
//                        $temp = [
//                            'id' => $office['name'],
//                            'name' => $office['name'],
//                            'lat' => $office['lat'],
//                            'lng' => $office['lng'],
//                            'address' => $office['address'],
//                            'desc' => '',
////                        'type' => CourierTypeInternal::WITH_PICKUP_RM,
//                        ];
//
//                        if($withImages)
//                            $temp['imgUrl'] = Yii::app()->baseUrl . '/images/v2/rm_map_ico.png';
//
//                        $items[CourierTypeInternal::WITH_PICKUP_RM][] = $temp;
//
//                    }
//
//
//
//            }
//
//            if($type === CourierTypeInternal::WITH_PICKUP_INPOST OR $type == false) {
//
//                $paczkomats = InpostShipx::getPoints();
//                $j = 0;
//                if (is_array($paczkomats))
//                    foreach ($paczkomats AS $paczkomat) {
//
//                        $temp = [
//                            'id' => $paczkomat['name'],
//                            'name' => $paczkomat['name'],
//                            'lat' => $paczkomat['lat'],
//                            'lng' => $paczkomat['lng'],
//                            'address' => $paczkomat['address'],
//                            'desc' => $paczkomat['desc'],
////                        'type' => CourierTypeInternal::WITH_PICKUP_INPOST,
//                        ];
//
//                        if($withImages)
//                            $temp['imgUrl'] = Yii::app()->baseUrl . '/images/v2/inpost_map_ico.png';
//
//                        $items[CourierTypeInternal::WITH_PICKUP_INPOST][] = $temp;
//                    }
//
//            }

            if($type === DeliveryPoints::TYPE_DHLDE OR $type == false) {

                $points = DhlDeClient::getPoints($street.', '.$zip_code.', '.$city.', '.$country);
                if (is_array($points))
                    foreach ($points AS $point) {

                        $temp = [
                            'id' => $point->id,
                            'pid' => $point->pid,
                            'name' => $point->name,
                            'type' => $point->type,
                            'lat' => $point->lat,
                            'lng' => $point->lng,
                            'street' => $point->street,
                            'zip_code' => $point->zip_code,
                            'city' => $point->city,
                            'country' => $point->country,
                            'other' => $point->other,
                        ];

                        if($withImages)
                            $temp['imgUrl'] = Yii::app()->baseUrl . '/images/v2/dhlde_map_ico.png';

                        $items[DeliveryPoints::TYPE_DHLDE][] = $temp;
                    }

            }

            if($type === DeliveryPoints::TYPE_DIRECTLINK OR $type == false) {

                $points = DirectLinkClient::getPoints($zip_code, $country);
                if (is_array($points))
                    foreach ($points AS $point) {

                        $temp = [
                            'id' => $point->id,
                            'pid' => $point->pid,
                            'name' => $point->name,
                            'type' => $point->type,
                            'lat' => $point->lat,
                            'lng' => $point->lng,
                            'street' => $point->street,
                            'zip_code' => $point->zip_code,
                            'city' => $point->city,
                            'country' => $point->country,
                            'other' => $point->other,
                        ];

//                        if($withImages)
//                            $temp['imgUrl'] = Yii::app()->baseUrl . '/images/v2/dhlde_map_ico.png';

                        $items[DeliveryPoints::TYPE_DIRECTLINK][] = $temp;
                    }

            }

            if($type === DeliveryPoints::TYPE_COLISSIMO OR $type == false) {

                $points = ColissimoClient::getPoints($street, $zip_code, $city, $country);
                if (is_array($points))
                    foreach ($points AS $point) {

                        $temp = [
                            'id' => $point->id,
                            'pid' => $point->pid,
                            'name' => $point->name,
                            'type' => $point->type,
                            'lat' => $point->lat,
                            'lng' => $point->lng,
                            'street' => $point->street,
                            'zip_code' => $point->zip_code,
                            'city' => $point->city,
                            'country' => $point->country,
                            'other' => $point->other,
                        ];

                        if($withImages)
                            $temp['imgUrl'] = Yii::app()->baseUrl . '/images/v2/dhlde_map_ico.png';

                        $items[DeliveryPoints::TYPE_COLISSIMO][] = $temp;
                    }

            }

            $CACHE->set($cacheName, $items, 60 * 60 * 24);
        }

        if($type) // clean uncecessary items if data got from global cache
        {
            if(S_Useful::sizeof($items) > 1)
            {
                foreach($items AS $key => $temp)
                {
                    if($key != $type)
                        unset($items[$key]);
                }
            }
        }

        return $items;
    }

}