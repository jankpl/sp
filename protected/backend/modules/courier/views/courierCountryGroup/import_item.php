<div style="width: 49%; float: left;">
    <h3>Pickup</h3>
    <h4>Default:</h4>
    <table class="table table-striped table-bordered">
        <tr>
            <th>Pickup</th>
            <td><?= $models['M']->isPickupAvailable() ? 'YES' : 'NO';?></td>
        </tr>
        <tr>
            <th>Pickup HUB</th>
            <td><?= $form->dropDownList($models['M'], 'pickup_hub', CHtml::listData(CourierHub::model()->findAll(), 'id', 'name'), ['prompt' => '-', 'disabled' => true]);?><?= $form->error($models['M'], 'pickup_hub'); ?></td>
        </tr>
        <tr>
            <th>Pickup OPERATOR</th>
            <td><?= $form->dropDownList($models['M'], 'pickup_operator', CHtml::listData(CourierOperator::model()->findAll(), 'id', 'name'), ['prompt' => '-', 'disabled' => true]);?><?= $form->error($models['M'], 'pickup_operator'); ?></td>
        </tr>
    </table>

    <h4>Custom:</h4>
    <table class="table table-striped table-bordered">
        <tr>
            <th>From</th>
            <th>To</th>
            <th>Hub</th>
            <th>Operator</th>
        </tr>

        <?php
        /* @var $modelPickup CourierCountryGroupOperator */
        foreach($models['P'] AS $modelPickup):

            if($modelPickup->hasErrors())
                $error = true;
            ?>
            <tr>
                <td><?= $form->textField($modelPickup, 'weight_from', ['disabled' => true, 'style' => 'width: 50px;']);?><?= $form->error($modelPickup, 'weight_from'); ?></td>
                <td><?= $form->textField($modelPickup, 'weight_to', ['disabled' => true, 'style' => 'width: 50px;']);?><?= $form->error($modelPickup, 'weight_to'); ?></td>
                <td><?= $form->dropDownList($modelPickup, 'courier_hub', CHtml::listData(CourierHub::model()->findAll(), 'id', 'name'), ['prompt' => '-', 'disabled' => true, 'style' => 'width: 100px;']);?><?= $form->error($modelPickup, 'courier_hub'); ?></td>
                <td><?= $form->dropDownList($modelPickup, 'courier_operator', CHtml::listData(CourierOperator::model()->findAll(), 'id', 'name'), ['prompt' => '-', 'disabled' => true, 'style' => 'width: 125px;']);?><?= $form->error($modelPickup, 'courier_operator'); ?></td>
            </tr>

        <?php
        endforeach;
        ?>
    </table>

</div>
<div style="width: 49%; float: right;">
    <h3>Delivery</h3>
    <h4>Default:</h4>
    <table class="table table-striped table-bordered">
        <tr>
            <th>-</th>
            <td>-</td>
        </tr>
        <tr>
            <th>Delivery HUB</th>
            <td><?= $form->dropDownList($models['M'], 'delivery_hub', CHtml::listData(CourierHub::model()->findAll(), 'id', 'name'), ['prompt' => '-', 'disabled' => true]);?><?= $form->error($models['M'], 'delivery_hub'); ?></td>
        </tr>
        <tr>
            <th>Delivery OPERATOR</th>
            <td><?= $form->dropDownList($models['M'], 'delivery_operator', CHtml::listData(CourierOperator::model()->findAll(), 'id', 'name'), ['prompt' => '-', 'disabled' => true]);?><?= $form->error($models['M'], 'delivery_operator'); ?></td>
        </tr>
    </table>

    <h4>Custom:</h4>
    <table class="table table-striped table-bordered">
        <tr>
            <th>From</th>
            <th>To</th>
            <th>Hub</th>
            <th>Operator</th>
        </tr>

        <?php
        /* @var $modelDelivery CourierCountryGroupOperator */
        foreach($models['D'] AS $modelDelivery):

            if($modelDelivery->hasErrors())
                $error = true;
            ?>
            <tr>
                <td><?= $form->textField($modelDelivery, 'weight_from', ['disabled' => true, 'style' => 'width: 50px;']);?><?= $form->error($modelDelivery, 'weight_from'); ?></td>
                <td><?= $form->textField($modelDelivery, 'weight_to', ['disabled' => true, 'style' => 'width: 50px;']);?><?= $form->error($modelDelivery, 'weight_to'); ?></td>
                <td><?= $form->dropDownList($modelDelivery, 'courier_hub', CHtml::listData(CourierHub::model()->findAll(), 'id', 'name'), ['prompt' => '-', 'disabled' => true, 'style' => 'width: 100px;']);?><?= $form->error($modelDelivery, 'courier_hub'); ?></td>
                <td><?= $form->dropDownList($modelDelivery, 'courier_operator', CHtml::listData(CourierOperator::model()->findAll(), 'id', 'name'), ['prompt' => '-', 'disabled' => true, 'style' => 'width: 125px;']);?><?= $form->error($modelDelivery, 'courier_operator'); ?></td>
            </tr>

        <?php
        endforeach;
        ?>
    </table>

</div>
