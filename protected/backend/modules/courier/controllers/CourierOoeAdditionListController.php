<?php

class CourierOoeAdditionListController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierOoeAdditionList'),
        ));
    }

    public function actionCreate() {

        $model = new CourierOoeAdditionList;
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $additionTr = new CourierOoeAdditionListTr();
            $additionTr->language_id = $item->id;
            $modelTrs[$item->id] = $additionTr;
        }

        if (isset($_POST['CourierOoeAdditionList'])) {
            $model->setAttributes($_POST['CourierOoeAdditionList']);

            foreach($_POST['CourierOoeAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierOoeAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierOoeAdditionListTrs = $modelTrs;

            if ($model->saveWithTr()) {
                $this->redirect(array('view', 'id' => $model->id));
            }



        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,

        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'CourierOoeAdditionList');

        $modelTrs = Array();


        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = CourierOoeAdditionListTr::model()->find('courier_ooe_addition_list_id=:courier_ooe_addition_list_id AND language_id=:language_id', array(':courier_ooe_addition_list_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $additionTr = new CourierOoeAdditionListTr();
                $additionTr->language_id = $item->id;
                $additionTr->courier_ooe_addition_list_id = $id;
                $modelTrs[$item->id] = $additionTr;
            }
        }


        if (isset($_POST['CourierOoeAdditionList'])) {
            $model->setAttributes($_POST['CourierOoeAdditionList']);


            foreach($_POST['CourierOoeAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierOoeAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierOoeAdditionListTrs = $modelTrs;


            if ($model->saveWithTr()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $this->loadModel($id, 'CourierOoeAdditionList')->delete();
            }
            catch(Exception $ex)
            {
                throw new CHttpException(403,'Nie udało się usunąć tego wpisu!');
            }


            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierOoeAdditionList('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierOoeAdditionList']))
            $model->setAttributes($_GET['CourierOoeAdditionList']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionStat($id)
    {
        /* @var $model CourierOoeAdditionList */


        $model = CourierOoeAdditionList::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}