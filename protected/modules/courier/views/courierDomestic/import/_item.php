<?php
/* @var $model Courier */
/* @var $i Integer */

$rowWithErrors = true;
if(!$model->hasErrors() && !$model->courierTypeDomestic->hasErrors() && !$model->senderAddressData->hasErrors() && !$model->receiverAddressData->hasErrors())
    $rowWithErrors = false;
?>
<tr class="condensed <?= $rowWithErrors ? 'row-errored' : '';?>" data-row-id="<?= $i;?>">
    <td>
        <?= ($i+1);?>
    </td>
    <td>
        <?php if($rowWithErrors)
            echo '<i class="glyphicon glyphicon-remove"></i>';
        else
            echo '<i class="glyphicon glyphicon-ok"></i>';
        ?>
    </td>
    <td>
        <input type="button" value="<?php echo Yii::t('courier', 'Remove');?>" data-remove-this-package="true"/>
    </td>
    <td>
        <input type="button" value="<?php echo Yii::t('courier', 'Edit');?>" data-edit-this-package="true"/>
    </td>
    <td <?php echo ($model->hasErrors('package_weight')?'class="td-with-error"':'');?> title="<?= $model->getError('package_weight');?>">
        <?php echo CHtml::activeNumberField($model,'['.$i.']package_weight', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'package_weight', 'step' => 0.1)); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_l')?'class="td-with-error"':'');?> title="<?= $model->getError('package_size_l');?>">
        <?php echo CHtml::activeNumberField($model,'['.$i.']package_size_l', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'package_size_l')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_w')?'class="td-with-error"':'');?> title="<?= $model->getError('package_size_w');?>">
        <?php echo CHtml::activeNumberField($model,'['.$i.']package_size_w', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'package_size_w')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_d')?'class="td-with-error"':'');?> title="<?= $model->getError('package_size_d');?>">
        <?php echo CHtml::activeNumberField($model,'['.$i.']package_size_d', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'package_size_d')); ?>
    </td>
    <td <?php echo ($model->hasErrors('packages_number')?'class="td-with-error"':'');?> title="<?= $model->getError('packages_number');?>">
        <?php echo CHtml::activeDropDownList($model, '['.$i.']packages_number', S_Useful::generateArrayOfItemsNumber(1,100), array('style' => 'width: 60px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_value')?'class="td-with-error"':'');?> title="<?= $model->getError('package_value');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']package_value', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_content')?'class="td-with-error"':'');?> title="<?= $model->getError('package_content');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']package_content', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('ref')?'class="td-with-error"':'');?> title="<?= $model->getError('ref');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']ref', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('cod_value')?'class="td-with-error"':'');?> title="<?= $model->getError('cod_value');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']cod_value', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('bankAccount')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('bankAccount');?>">
        <?php echo CHtml::activeTelField($model->senderAddressData,'['.$i.']bankAccount', array('style' => 'width: 185px;', 'disabled' => 'disabled')); ?>
    </td>
    <td style="border-left: 2px dashed gray; <?php echo ($model->senderAddressData->hasErrors('name')?'background: red;':'');?>" title="<?= $model->senderAddressData->getError('name');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]name', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.name')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('company')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('company');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]company', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.company')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('zip_code')?'class="td-with-error"':'');?>>
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]zip_code', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.zip_code')); ?>
        <?= $model->senderAddressData->getError('zip_code');?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('city')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('city');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]city', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.city')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('address_line_1')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('address_line_1');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]address_line_1', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.address_line_1')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('address_line_2')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('address_line_2');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]address_line_2', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.address_line_2')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('tel')?'class="td-with-error"':'');?>  title="<?= $model->senderAddressData->getError('tel');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]tel', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.tel')); ?>
    </td>
    <td style="border-right: 2px dashed gray;" <?php echo ($model->senderAddressData->hasErrors('email')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('email');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]email', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.email')); ?>
    </td>
    <td style="border-left: 2px dashed gray; <?php echo ($model->receiverAddressData->hasErrors('name')?'background: red;':'');?>" title="<?= $model->receiverAddressData->getError('name');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][sender]name', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.name')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('company')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('company');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][sender]company', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.company')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('zip_code')?'class="td-with-error"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]zip_code', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.zip_code')); ?>
        <?= $model->receiverAddressData->getError('zip_code');?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('city')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('city'); ;?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]city', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.city')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('address_line_1')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('address_line_1');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]address_line_1', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.address_line_1')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('address_line_2')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('address_line_2');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]address_line_2', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.address_line_2')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('tel')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('tel');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]tel', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.tel')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('email')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('email');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]email', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.email')); ?>
    </td>
</tr>