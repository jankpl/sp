<?php

class GlsItClient extends GlobalOperator
{

    const URL = 'https://weblabeling.gls-italy.com/ilsWebService.asmx?op=';
    const WSDL = 'https://weblabeling.gls-italy.com/IlsWebService.asmx?WSDL';
    const SEDE = 'BZ';

    const CONTRACT_ID = GLOBAL_CONFIG::GLS_IT_CONTRACT_ID;
    const CLIENT_CODE = GLOBAL_CONFIG::GLS_IT_CLIENT_CODE;
    const PASSWORD = GLOBAL_CONFIG::GLS_IT_PASSWORD;

    public $label_annotation_text = false;

    public $receiver_name;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $cod_value = 0;

    public $package_weight;

    public $package_content;
    public $ref;

    public $user_id;
    public $login;

    public $local_id;


    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    public static function test($street = '', $zip = '', $city = '')
    {

        $client = new self;

        $data = new stdClass();
        $data->XMLInfoParcel = '<Info>
        <SedeGls>'.self::SEDE.'</SedeGls>
<CodiceClienteGls>'.self::CLIENT_CODE.'</CodiceClienteGls>
<PasswordClienteGls>'.self::PASSWORD.'</PasswordClienteGls>
<Parcel>

<CodiceContrattoGls>'.self::CONTRACT_ID.'</CodiceContrattoGls>
<RagioneSociale>Jan Nowak</RagioneSociale>
<Indirizzo>'.$street.'</Indirizzo>
<Localita>'.$city.'</Localita>
<Zipcode>'.$zip.'</Zipcode>
<Provincia></Provincia>
<Colli>1</Colli>
<Incoterm>0</Incoterm>
<PesoReale>1</PesoReale>
<ImportoContrassegno>0</ImportoContrassegno>
<NoteSpedizione>test</NoteSpedizione>
<TipoPorto>F</TipoPorto>
<TipoCollo>0</TipoCollo>
<NoteAggiuntive>test</NoteAggiuntive>
<Email>dasasdasdasdasd@sdasdasdasdasd.pl</Email>
<Cellulare1>000111000</Cellulare1>
<ModalitaIncasso>CONT</ModalitaIncasso>
<GeneraPdf>3</GeneraPdf>
<ContatoreProgressivo>1</ContatoreProgressivo>
</Parcel>
</Info>';

        $data->XMLInfoParcel = str_replace("\r\n", "", $data->XMLInfoParcel);

        $resp = $client->_soapOperation('AddParcel', $data);


        $result = (string) $resp->Parcel->DescrizioneTipoPorto;

//if($result == 'ERRORE')
//    return false;
//else
//    return true;

        var_dump((string) $resp->Parcel->DescrizioneTipoPorto);

//        var_dump($resp);
//        exit;

        $trackId = self::SEDE . (string) $resp->Parcel->NumeroSpedizione;
        var_dump($trackId);
        if(preg_match('/Pdf non generato/i', $resp->Parcel->NoteSpedizione))
        echo 'bez pdf';
        else
        echo 'z pdf';

        $trackId = (string) $resp->Parcel->ContatoreProgressivo;

        var_dump($trackId);

        $data = new stdClass();
        $data->SedeGls = self::SEDE;
        $data->CodiceCliente = self::CLIENT_CODE;
        $data->Password = self::PASSWORD;
        $data->CodiceContratto = self::CONTRACT_ID;
        $data->ContatoreProgressivo = $trackId;

        $resp = $client->_soapOperation('GetPdf', $data);

        var_dump($resp);
        var_dump($resp->GetPdfResult);

        file_put_contents('gls_it_.pdf', $resp->GetPdfResult);

        return $resp;
    }

    protected function _soapOperation($method, $data)
    {


        MyDump::dump('glsit.txt','DATA: ' .  print_r($data, 1));

        $mode = array
        (
            'soap_version' =>SOAP_1_1,
            'trace' =>1,
            'stream_context' =>stream_context_create(array('ssl' =>array('verify_peer' =>false, 'verify_peer_name' =>false))),
        );


        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        try {

            $client = new SoapClient(self::WSDL, $mode);
            $client->__setLocation(self::URL.$method);
            $resp = $client->$method($data);

            $resultName = $method.'Result';

            if($method == 'AddParcel' OR $method == 'CloseWorkDay')
                $resp = simplexml_load_string($resp->$resultName->any);

        } catch (SoapFault $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('glsit.txt', $method.' : REQ: ' . print_r($client->__getLastRequest(), 1));
            MyDump::dump('glsit.txt',$method.' : RESP: ' . print_r($client->__getLastResponse(), 1));

            return $return;

        } catch (Exception $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('glsit.txt',$method.' : REQ: ' .  print_r($client->__getLastRequest(), 1));
            MyDump::dump('glsit.txt', $method.' : RESP: ' . print_r($client->__getLastResponse(), 1));
            return $return;
        }

        MyDump::dump('glsit.txt',$method.' : REQ: ' .  print_r($client->__getLastRequest(), 1));
        MyDump::dump('glsit.txt', $method.' : RESP: ' . print_r($client->__getLastResponse(), 1));


        if(isset($resp->Fault)) {
            $return->error = $resp->faultcode.':'.$resp->faultstring;
            $return->errorCode = '';
            $return->errorDesc = $resp->faultcode.':'.$resp->faultstring;
        }
        else
        {
            $return->success = true;
            $return->data = $resp;
        }

        return $resp;
    }


    public static function clearString($text)
    {
        $text = htmlspecialchars($text);
        return $text;
    }

    /**
     * @param self[] $models
     * @return stdClass
     */
    protected static function prepareShipmentData(array $models, $close = false)
    {
        $data = new stdClass();

        if($close)
            $name = 'XMLCloseInfoParcel';
        else
            $name = 'XMLInfoParcel';

        $data->$name = '<Info>
<SedeGls>'.self::SEDE.'</SedeGls>
<CodiceClienteGls>'.self::CLIENT_CODE.'</CodiceClienteGls>
<PasswordClienteGls>'.self::PASSWORD.'</PasswordClienteGls>';

        foreach($models AS $item)
            $data->$name .= $item->prepareParcelData();

        $data->$name .= '</Info>';
        $data->$name = str_replace("\r\n", "", $data->$name);

        return $data;
    }

    protected function prepareParcelData()
    {
        $weight = $this->package_weight;

        if(in_array($this->user_id, [947,948,2036,2037]) && $weight <= 5) // @ 07.11.2018 Special rule for Eobuwie
            $weight = 2;

        $bda = substr($this->local_id,  -11);

        $data = '<Parcel>
<CodiceContrattoGls>'.self::CONTRACT_ID.'</CodiceContrattoGls>
<RagioneSociale>'.self::clearString($this->receiver_name).'</RagioneSociale>
<Indirizzo>'.self::clearString(trim($this->receiver_address1.' '.$this->receiver_address2)).'</Indirizzo>
<Localita>'.self::clearString($this->receiver_city).'</Localita>
<Zipcode>'.self::clearString($this->receiver_zip_code).'</Zipcode>
<Provincia></Provincia>
<Colli>1</Colli>
<Incoterm>0</Incoterm>
<PesoReale>'.$weight.'</PesoReale>
<ImportoContrassegno>'.$this->cod_value.'</ImportoContrassegno>
<NoteSpedizione>'.self::clearString($this->ref).'</NoteSpedizione>
<TipoPorto>F</TipoPorto>
<TipoCollo>0</TipoCollo>
<NoteAggiuntive>'.self::clearString($this->package_content).'</NoteAggiuntive>
<Email>'.self::clearString($this->receiver_mail).'</Email>
<Cellulare1>'.self::clearString($this->receiver_tel).'</Cellulare1>
<ModalitaIncasso>CONT</ModalitaIncasso>
<GeneraPdf>3</GeneraPdf>
<ContatoreProgressivo>'.substr($this->ref,-9,9).'</ContatoreProgressivo>
</Parcel>';

        return $data;

    }

    public static function manifestYesterdaysItems()
    {
        Yii::app()->getModule('courier');
        $yesterday = date('Y-m-d', strtotime("-1 day"));
        $models = CourierLabelNew::model()->findAll('date_entered LIKE :date_entered AND operator = :operator AND stat = :stat', [
            ':date_entered' => $yesterday.'%',
            ':operator' => GLOBAL_BROKERS::BROKER_GLS_IT,
            ':stat' => CourierLabelNew::STAT_SUCCESS,
        ]);

        if(!S_Useful::sizeof($models))
            return true;

        return self::closeDay($models);
    }

    /**
     * @param CourierLabelNew[] $models
     * @throws Exception
     */
    protected static function closeDay(array $models)
    {
        /* @var $item CourierLabelNew */

        $readyModels = [];
        foreach($models AS $item)
        {
            $temp = new self;
            if($item->courier->getType() == Courier::TYPE_INTERNAL)
                $temp->_courierInternalFill($item->courier->courierTypeInternal, $item->addressDataFrom, $item->addressDataTo);
            else if($item->courier->getType() == Courier::TYPE_U)
                $temp->_courierUFill($item->courier->courierTypeU);
            else
                throw new Exception('Unsupported type!');

            $readyModels[] = $temp;
        }

        $data = self::prepareShipmentData($readyModels, true);

        $model = new self;
        return $model->_soapOperation('CloseWorkDay', $data);
    }

    protected function createShipment($returnError)
    {
        $data = self::prepareShipmentData([$this]);

        $resp = $this->_soapOperation('AddParcel', $data);

        if($resp->Parcel->DescrizioneTipoPorto == 'ERRORE')
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->Parcel->NoteSpedizione);
            } else {
                return false;
            }
        }

        $labelNo = (string) $resp->Parcel->ContatoreProgressivo;
        $trackId = self::SEDE . (string) $resp->Parcel->NumeroSpedizione;

        $data = new stdClass();
        $data->SedeGls = self::SEDE;
        $data->CodiceCliente = self::CLIENT_CODE;
        $data->Password = self::PASSWORD;
        $data->CodiceContratto = self::CONTRACT_ID;
        $data->ContatoreProgressivo = $labelNo;

        $resp2 = $this->_soapOperation('GetPdf', $data);

        $annotation = $this->login;
        if(in_array($this->user_id, [947, 948, 2036, 2037])) {
            $annotation = 'Escarpe.it';
        }

        if ($resp2->GetPdfResult) {
            $return = [];

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImageBlob($resp2->GetPdfResult);
            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $im->stripImage();
            $im->trimImage(0);

            LabelAnnotation::createAnnotation($annotation, $im, 400, 25, false, false, 6);


            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $trackId);

            return $return;
        }
        else {

            if ($returnError == true) {
                $error = (string) $resp2->Parcel->NoteSpedizione;

                if($error == '')
                    $error = $resp->errorDesc != '' ? $resp->errorDesc : $resp->error;

                return GlobalOperatorResponse::createErrorResponse($error);
            } else {
                return false;
            }
        }

    }

    protected function _courierInternalFill(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to)
    {
        $this->receiver_name = $to->getUsefulName(true);
        $this->receiver_address1 = $to->address_line_1;
        $this->receiver_address2 = $to->address_line_2;
        $this->receiver_zip_code = $to->zip_code;
        $this->receiver_city = $to->city;
        $this->receiver_country = $to->country0->code;
        $this->receiver_country_id = $to->country_id;
        $this->receiver_mail = $to->fetchEmailAddress(true);
        $this->receiver_tel = $to->tel;

        $this->package_weight = $courierInternal->courier->getWeight(true);

        $this->package_content = $courierInternal->courier->package_content;
        $this->ref = $courierInternal->courier->local_id;
        $this->local_id = $courierInternal->courier->local_id;

        $this->cod_value = $courierInternal->courier->cod_value;

        $this->user_id = $courierInternal->courier->user_id;
        $this->login = $courierInternal->courier->user->login;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;
        $model->_courierInternalFill($courierInternal, $from, $to);

        return $model->createShipment($returnErrors);
    }

    protected function _courierUFill(CourierTypeU $courierU)
    {
        $to = $courierU->courier->receiverAddressData;
        $this->receiver_name = $to->getUsefulName(true);
        $this->receiver_address1 = $to->address_line_1;
        $this->receiver_address2 = $to->address_line_2;
        $this->receiver_zip_code = $to->zip_code;
        $this->receiver_city = $to->zip_code;
        $this->receiver_country = $to->country0->code;
        $this->receiver_country_id = $to->country_id;
        $this->receiver_mail = $to->fetchEmailAddress(true);
        $this->receiver_tel = $to->tel;

        $this->package_weight = $courierU->courier->getWeight(true);

        $this->package_content = $courierU->courier->package_content;
        $this->ref = $courierU->courier->local_id;
        $this->local_id = $courierU->courier->local_id;

        $this->cod_value = $courierU->courier->cod_value;

        $this->user_id = $courierU->courier->user_id;
        $this->login = $courierU->courier->user->login;

    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;

        $model->_courierUFill($courierU);

        return $model->createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        $no = str_replace(self::SEDE, '', $no);
        $resp = @file_get_contents('https://wwwdr.gls-italy.com/XML/get_xml_track.php?locpartenza='.self::SEDE.'&NumSped='.$no.'&Cli='.self::CLIENT_CODE);

        $statuses = new GlobalOperatorTtResponseCollection();
        if($resp)
        {
            $resp = @simplexml_load_string($resp, null, LIBXML_NOCDATA);

            if($resp && ((string) $resp->TESTOERRORE) == '')
            {
                $key = 0;
//                if(is_array($resp->SPEDIZIONE->TRACKING->Data))
                    foreach($resp->SPEDIZIONE->TRACKING->Data AS $item)
                    {
                        if($resp->SPEDIZIONE->TRACKING->Codice[$key] == 901)
                            continue;

                        $date = explode('/', $item);
                        $date = '20'.$date[2].'-'.$date[1].'-'.$date[0];

                        $location = (string) $resp->SPEDIZIONE->TRACKING->Luogo[$key];
                        $date = (string) $date.' '.(string) $resp->SPEDIZIONE->TRACKING->Ora[$key];
                        $status = (string) $resp->SPEDIZIONE->TRACKING->Stato[$key];

                        $statuses->addItem($status, trim($date), $location);
                        $key++;
                    }
            }
        }

        return $statuses;
    }
}