<?php
/* @var $model Postal */
/* @var $i Integer */
/* @var $points array */


$rowWithErrors = true;
if(!$model->hasErrors() && !$model->senderAddressData->hasErrors() && !$model->receiverAddressData->hasErrors())
    $rowWithErrors = false;
?>
<tr class="condensed <?= $rowWithErrors ? 'row-errored' : '';?> postal-import-form" data-row-id="<?= $i;?>">
    <td>
        <?= ($i+1);?>
    </td>
    <td style="text-align: center;">
        <?php if($rowWithErrors)
            echo '<i class="glyphicon glyphicon-remove"></i>';
        else
            echo '<i class="glyphicon glyphicon-ok"></i>';
        ?>
    </td>
    <td>
        <input type="button" value="<?php echo Yii::t('courier', 'Remove');?>" data-remove-this-package="true"/>
    </td>
    <td>
        <input type="button" value="<?php echo Yii::t('courier', 'Edit');?>" data-edit-this-package="true"/>
    </td>
    <td style="text-align: center;">
        <?php
        if(!Yii::app()->user->model->getHidePrices()):
            ?>
            <?= $model->_price_total != '' ? $model->_price_total.' '.$model->_price_currency : '-' ; ?>
        <?php
        endif;
        ?>
    </td>
    <td <?php echo ($model->hasErrors('postal_type')?'class="td-with-error"':'');?> title="<?= $model->getError('postal_type');?>">
        <?php echo CHtml::activeDropDownList($model,'['.$i.']postal_type', Postal::getPostalTypeList(),  array('prompt' => '--', 'style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'postal_type')); ?>
    </td>
    <td <?php echo ($model->hasErrors('size_class')?'class="td-with-error"':'');?> title="<?= $model->getError('size_class');?>">
        <?php echo CHtml::activeDropDownList($model,'['.$i.']size_class', Postal::getSizeClassList(), array('prompt' => '--', 'style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'size_class')); ?>
    </td>
    <td <?php echo ($model->hasErrors('weight')?'class="td-with-error"':'');?> title="<?= $model->getError('weight');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']weight', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'weight')); ?>
    </td>
    <td <?php echo ($model->hasErrors('weight_class')?'class="td-with-error"':'');?> title="<?= $model->getError('weight_class');?>">
        <?php echo CHtml::activeDropDownList($model,'['.$i.']weight_class', Postal::getWeightClassList(), array('prompt' => '--', 'style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('content')?'class="td-with-error"':'');?> title="<?= $model->getError('content');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']content', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('ref')?'class="td-with-error"':'');?> title="<?= $model->getError('ref');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']ref', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('note1')?'class="td-with-error"':'');?> title="<?= $model->getError('note1');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']note1', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('note2')?'class="td-with-error"':'');?> title="<?= $model->getError('note2');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']note2', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('target_sp_point')?'class="td-with-error"':'');?> title="<?= $model->getError('target_sp_point');?>">
        <?php echo CHtml::activeDropDownList($model,'['.$i.']target_sp_point', CHtml::listData($points, 'id' ,'spPointsTr.title'), ['prompt' => '--', 'style' => 'width: 75px;', 'disabled' => 'disabled']); ?>
    </td>
    <td <?php echo ($model->hasErrors('value')?'class="td-with-error"':'');?> title="<?= $model->getError('value');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']value', ['style' => 'width: 75px;', 'disabled' => 'disabled']); ?>
    </td>
    <td <?php echo ($model->hasErrors('value_currency')?'class="td-with-error"':'');?> title="<?= $model->getError('value_currency');?>">
        <?php echo CHtml::activeDropDownList($model,'['.$i.']value_currency', Postal::getCurrencyList(), ['prompt' => '--', 'style' => 'width: 75px;', 'disabled' => 'disabled']); ?>
    </td>
    <td style="border-left: 2px dashed gray; <?php echo ($model->senderAddressData->hasErrors('name')?'background: red;':'');?>" title="<?= $model->senderAddressData->getError('name');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]name', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.name')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('company')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('company');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]company', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.company')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('country_id')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('country_id');?>">
        <?php echo CHtml::activeHiddenField($model->senderAddressData,'['.$i.'][sender]country_id', array('disabled' => 'disabled')); ?>
        <?php echo CHtml::textField('['.$i.'][sender]country_temp', $model->senderAddressData->country0 !== NULL ? $model->senderAddressData->country0->getTrName() : '', array('disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('zip_code')?'class="td-with-error"':'');?>>
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]zip_code', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.zip_code')); ?>
        <?= $model->senderAddressData->getError('zip_code');?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('city')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('city');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]city', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.city')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('address_line_1')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('address_line_1');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]address_line_1', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.address_line_1')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('address_line_2')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('address_line_2');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]address_line_2', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.address_line_2')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('tel')?'class="td-with-error"':'');?>  title="<?= $model->senderAddressData->getError('tel');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]tel', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.tel')); ?>
    </td>
    <td style="border-right: 2px dashed gray;" <?php echo ($model->senderAddressData->hasErrors('email')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('email');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]email', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.email')); ?>
    </td>
    <td style="border-left: 2px dashed gray; <?php echo ($model->receiverAddressData->hasErrors('name')?'background: red;':'');?>" title="<?= $model->receiverAddressData->getError('name');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][sender]name', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.name')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('company')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('company');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][sender]company', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.company')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('country_id')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('country_id');?>">
        <?php echo CHtml::activeHiddenField($model->receiverAddressData,'['.$i.'][receiver]country_id', array('disabled' => 'disabled')); ?>
        <?php echo CHtml::textField('['.$i.'][sender]country_temp', $model->receiverAddressData->country0 !== NULL ? $model->receiverAddressData->country0->getTrName() : '', array('disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('zip_code')?'class="td-with-error"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]zip_code', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.zip_code')); ?>
        <?= $model->receiverAddressData->getError('zip_code');?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('city')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('city'); ;?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]city', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.city')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('address_line_1')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('address_line_1');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]address_line_1', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.address_line_1')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('address_line_2')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('address_line_2');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]address_line_2', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.address_line_2')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('tel')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('tel');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]tel', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.tel')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('email')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('email');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]email', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.email')); ?>
    </td>
</tr>