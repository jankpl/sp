<h4>Generate Manifest/ACK</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-ack-cards',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/generateAckCardsByGridView'),
));
?>

<?php
echo TbHtml::submitButton('Generate Manifest/ACK', array(
    'onClick' => 'js:
    $("#postal-ids-ack").val($("#postal").selGridView("getAllSelection").toString());
    ;',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => 'postal-ids-ack')); ?>
<?php
$this->endWidget();
?>
