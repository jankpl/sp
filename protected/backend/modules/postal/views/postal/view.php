

<?php

/* @var $this PostalController */
/* @var $model Postal */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);



$this->menu=array(

    array('label'=>'User details', 'url'=>array('/user/view', 'id' => $model->user_id),'visible'=>$model->user_id!==null),

);

?>

<h1>Postal #<?php echo $model->local_id; ?></h1>

<?php


foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>


<div>
    <?php
    if($model->user_cancel):
        ?>


        <h3>User cancel</h3>

        <?php
        if($model->user_cancel == Postal::USER_CANCELL_REQUESTED):
            ?>
            <div class="alert alert-warning">Status: <?= Postal::getUserCancelList()[$model->user_cancel];?>
                <p>T&T is disabled, but new statuses will raise flag!</p>
            </div>
        <?php
        elseif($model->user_cancel == Postal::USER_CANCELL_REQUESTED_FOUND_TT):
            ?>
            <div class="alert alert-error">Status: <?= Postal::getUserCancelList()[$model->user_cancel];?>
                <p>Found new T&T status for this package after canceling by user!</p>
            </div>
        <?php
        elseif($model->user_cancel == Postal::USER_CANCELL_COMPLAINED):
            ?>
            <div class="alert alert-warning">Status: <?= Postal::getUserCancelList()[$model->user_cancel];?>
                <p>This package has been complained by customer. T&T is active.</p>
            </div>
        <?php
        endif;
        ?>

        <div class="alert alert-info">Manual change of status will clear User cancel flag.</div>

    <?php
    endif;
    ?>

    <h3>Logs</h3>

    <table class="list hTop" style="width: 100%; table-layout: fixed;">
        <tr>
            <td style="width: 150px;">Date</td>
            <td style="width: 150px;">Cat</td>
            <td style="width: 150px;">Source</td>
            <td>Text</td>
        </tr>
        <?php
        foreach($model->postalLogs AS $item):
            ?>
            <tr>
                <td><?= $item->date_entered;?></td>
                <td><?= $item->getCatName();?></td>
                <td><?= $item->source;?></td>
                <td><?= $item->text;?></td>
            </tr>
        <?php
        endforeach;?>
    </table>

    <div style="float: left; width: 48%;">


        <h3>Postal</h3>

        <table class="list hLeft" style="width: 500px;">
            <tr>
                <td>Type</td>
                <td><?php echo $model->getPostalTypeName();?></td>
            </tr>
            <tr>
                <td>Weight Class</td>
                <td><?php echo $model->getWeightClassName();?></td>
            </tr>
            <tr>
                <td>Weight</td>
                <td><?php echo $model->weight;?> g</td>
            </tr>
            <tr>
                <td>Size Class</td>
                <td><?php echo $model->getSizeClassName();?></td>
            </tr>
            <tr>
                <td>Source</td>
                <td><?php echo $model->sourceName; ?></td>
            </tr>
            <tr>
                <td>Content</td>
                <td><?php echo $model->content;?></td>
            </tr>
            <tr>
                <td>Value</td>
                <td><?php echo $model->value;?> <?= $model->getValueCurrency();?></td>
            </tr>
            <tr>
                <td>Bulk</td>
                <td><?php echo $model->getBulkName();?></td>
            </tr>
            <tr>
                <td>Receiver country</td>
                <td><?php echo $model->getReceiverCountryName();?></td>
            </tr>
            <tr>
                <td>Target SP Point</td>
                <td><?= SpPoints::getPoint($model->target_sp_point)->name;?></td>
            </tr>
            <tr>
                <td>Operator</td>
                <td><?= $model->postalOperator->getNameWithBroker();?></td>
            </tr>
            <tr>
                <td>Ref</td>
                <td><?php echo $model->ref != '' ? $model->ref : '-'; ?> [<?= ($model->ref != '') ? CHtml::link('remove', array('/postal/postal/removeRef', 'urlData' => $model->hash), ['data-confirm' => true, 'data-text' => 'Are you sure?', ]) : CHtml::link('add', array('/postal/postal/addRef', 'urlData' => $model->hash));?>]</td>
            </tr>
            <tr>
                <td>Non-reg external ID</td>
                <td><?php echo $model->external_id ? $model->external_id : '-';?></td>
            </tr>
            <tr>
                <td>Note 1</td>
                <td><?php echo $model->note1;?></td>
            </tr>
            <tr>
                <td>Note 2</td>
                <td><?php echo $model->note2 ;?></td>
            </tr>
        </table>

        <?php
        if($model->postalLabel !== NULL):
            ?>
            <h3>Label details</h3>
            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td>Track ID</td>
                    <td><?php echo $model->postalLabel->track_id;?> <?php echo $model->postalLabel !== NULL ? CHtml::link('[edit]', ['/postal/postal/editTrackingId', 'id' => $model->postal_label_id]) : '' ;?></td>
                </tr>
                <tr>
                    <td>Label ID</td>
                    <td>#<?php echo $model->postalLabel->id;?></td>
                </tr>
            </table>
        <?php
        endif;
        ?>

        <?php
        if($model->bulk):
            ?>
            <h3>Bulk details</h3>

            <table class="list hLeft" style="width: 500px;">
                <tr>
                    <td>Number of postals</td>
                    <td><?php echo $model->bulk_number;?></td>
                </tr>
                <tr>
                    <td>Bulk ID</td>
                    <td><?php echo $model->bulk_id;?></td>
                </tr>
            </table>

        <?php
        endif;
        ?>

        <h3>Postal details </h3>
        <table class="list hLeft" style="width: 500px;">
            <tr>
                <td>User</td>
                <td><?php echo $model->user_id !== null ? CHtml::link($model->user->login, array('/user/view', 'id' => $model->user_id)) : '-';?></td>
            </tr>
            <tr>
                <td>Creation date</td>
                <td><?php echo substr($model->date_entered,0,16);?></td>
            </tr>
            <tr>
                <td>Update date</td>
                <td><?php echo substr($model->date_updated,0,16);?></td>
            </tr>
            <tr>
                <td>Status</td>
                <td>[<?php echo $model->postal_stat_id;?>] <?php echo TbHtml::badge($model->stat->name);?> </td>
            </tr>
            <tr>
                <td>Status Map</td>
                <td>[<?php echo $model->stat_map_id ? $model->stat_map_id : '-';?>] <?php echo  $model->stat_map_id ? TbHtml::badge(StatMap::getMapNameById($model->stat_map_id)) : '';?> </td>
            </tr>
            <tr>
                <td>Status location</td>
                <td><?php echo $model->location ? $model->location : '-';?></td>
            </tr>
        </table>






    </div>
    <div style="float: right; width: 48%;">

        <h3>Carriage ticket</h3>

        <?php
        if(!in_array($model->postal_stat_id, [PostalStat::CANCELLED, PostalStat::NEW_ORDER, PostalStat::PACKAGE_PROCESSING, PostalStat::CANCELLED_USER]) &&
            !$model->isLabelArchived() && !$model->bulk):
            ?>

            <table class="list " style="width: 500px;">
                <tr>
                    <td><?php echo TbHtml::link('4 on 1', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_4_ON_1), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('2 on 1', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_2_ON_1), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('Roll', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_ROLL), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('A4 window left', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_A4_WINDOW_LEFT), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('A4 window right', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_A4_WINDOW_RIGHT), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('C4', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_C4), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('C5', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_C5), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('C6', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_C6), array('class' => 'btn'));?></td>
                    <td><?php echo TbHtml::link('DL', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_DL), array('class' => 'btn'));?></td>
                </tr>
            </table>
        <?php
        else:
            ?>
            <?php
            if($model->isLabelArchived()):?>
                Labels has expired.
            <?php
            else:
                ?>
                With this package status you cannot generate label.
            <?php
            endif;
            ?>


        <?php
        endif;
        ?>


        <h3>Change status</h3>
        <?php
        if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER])):
            ?>
            Option available only for administrators
        <?php
        else:
            if($model->isStatHistoryArchived()):
                ?>
                Status history has expired
            <?php
            else:
                ?>

                <?php $form = $this->beginWidget('GxActiveForm', array(
                'id' => 'change-status',
                'enableAjaxValidation' => false,
                'action' => Yii::app()->createUrl('/postal/postal/changeStat'),
            ));
                ?>

                <?php echo $form->hiddenField($model,'id'); ?>

                <div class="form">

                    <?php echo $form->errorSummary($model); ?>

                    <table class="list hTop" style="width: 500px;">
                        <tr>
                            <td>Current status</td>
                        </tr>
                        <tr>
                            <td>[<?php echo $model->stat->id;?>] <?php echo $model->stat->name;?> <?php echo $model->location ? '('.$model->location.')' : '';?></td>
                        </tr>
                    </table>


                    <table class="list hTop" style="width: 500px;">
                        <tr>
                            <td>New status</td>
                            <td>Location</td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $form->dropDownList($model, 'postal_stat_id', CHtml::listData(PostalStat::getStats(true), 'id', 'name')); ?>
                            </td>
                            <td>
                                <?php echo CHtml::textField('location'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">Date (yyyy-mm-dd hh:mm:ss): *</td>
                            <td>
                                <?php
                                $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                                    'name'=>'status_date',
                                    'options'=>array(
                                        'hourGrid' => 4,
                                        'hourMin' => 0,
                                        'hourMax' => 23,
                                        'showSecond'=>true,
                                        'timeFormat' => 'hh:mm:ss',
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'dateFormat' => 'yy-mm-dd',
                                    ),
                                ));
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?php
                                echo TbHtml::submitButton(Yii::t('app', 'Save'),
                                    array(
                                        'confirm'=>'Are you sure?'
                                    ));
                                $this->endWidget();
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                (*) leave empty for current date
                            </td>
                        </tr>
                    </table>

                    <?php $form = $this->beginWidget('GxActiveForm', array(
                        'id' => 'change-status',
                        'enableAjaxValidation' => false,
                        'action' => Yii::app()->createUrl('/postal/postal/deleteLastStatus', array('id' => $model->id)),
                    ));
                    ?>

                    <table class="list hTop" style="width: 500px;">
                        <tr>
                            <td>Delete last status</td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                echo TbHtml::submitButton(Yii::t('app', 'Delete'),
                                    array(
                                        'confirm'=>'Are you sure?'
                                    ));
                                $this->endWidget();
                                ?>
                            </td>
                        </tr>
                    </table>


                </div><!-- form -->

            <?php
            endif;
        endif;
        ?>

        <h3>Return label</h3>
        <table class="list hTop" style="width: 500px;">

            <tr>
                <td>
                    <a href="<?= Yii::app()->createUrl('/postal/postal/generateReturnLabel', ['urlData' => $model->hash]);?>" target="_blank" class="btn">Generate return label</a>
                </td>
            </tr>
        </table>



        <h3>Create invoice by Fakturownia</h3>
        <a href="<?= Yii::app()->createUrl('/fakturownia/createByPostal', ['id' => $model->id]);?>" class="btn" target="_blank">With sender data</a> <a href="<?= Yii::app()->createUrl('/fakturownia/createByPostal', ['id' => $model->id, 'userData' => true]);?>" class="btn" target="_blank">With user data</a>

    </div>
    <div style="clear: both;"></div>
</div>



<?php
if($model->senderAddressData !== NULL && $model->receiverAddressData !== NULL):
    ?>
    <div>

        <div style="float: left; width: 48%;">
            <h3>Package sender</h3>
            <?php

            $this->Widget('AddressDataList', array(
                'addressData' => $model->senderAddressData
            ));

            ?>
        </div>
        <div style="float: right; width: 48%;">

            <h3>Package receiver</h3>
            <?php

            $this->Widget('AddressDataList', array(
                'addressData' => $model->receiverAddressData
            ));
            ?>
        </div>
        <div style="clear: both;"></div>
    </div>
<?php
endif;
?>


<div>

    <div style="float: left; width: 48%;">

        <h3>Additional external IDs</h3>
        <p>Allows package to be found by this ids</p>

        <table class="list hTop" style="width: 100%; table-layout: fixed;">
            <tr>
                <td style="width: 150px;">External ID</td>
            </tr>
            <?php
            foreach($model->postalAdditionalExtId AS $item):
                ?>
                <tr>
                    <td><?= $item->external_id;?></td>
                </tr>
            <?php
            endforeach;?>
        </table>

        <h3>History</h3>
        <?php
        if($model->isStatHistoryArchived()):?>
            History has expired.

        <?php
        else:
            ?>
            <?php
            $this->renderPartial('/_statHistory/_statHistory', array(
                'model' => $model->postalStatHistories,
                'type' => 4,
            ));

            ?>

        <?php
        endif;
        ?>
    </div>
    <div style="float: right; width: 48%;">

        <h3 style="display: inline-block; margin-right: 5px;">TT Logs</h3>
        <a href="<?= Yii::app()->createUrl('/postal/postal/updateTtOne', ['id' => $model->id]);?>" class="btn btn-mini btn-primary">Force update TT now</a><br/>
        Last TT check(s):
        <?php
        $logs = $model->ttLogs;
        if(!S_Useful::sizeof($logs)):
            ?>
            <br/>- none -
        <?php
        else:
            ?>
            <ul>
                <?php
                foreach($model->ttLogs AS $item)
                    echo '<li>'.$item->date_entered.'</li>';
                ?>
            </ul>
        <?php
        endif;
        ?>
    </div>
    <div style="clear: both;"></div>
</div>

<h3>SMSes</h3>

<table class="list hTop" style="width: 100%; table-layout: fixed;">
    <tr>
        <td style="width: 150px;">Date</td>
        <td style="width: 150px;">Nr</td>
        <td>Text</td>
    </tr>
    <?php
    foreach($model->smsArchive AS $sms):
        ?>
        <tr>
            <td><?= $sms->date_entered;?></td>
            <td><?= $sms->number;?></td>
            <td><?= $sms->text;?></td>
        </tr>
    <?php
    endforeach;?>
</table>

