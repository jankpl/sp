<?php

class LinkerImportU extends CFormModel
{
    CONST MAX_IMPORT_NUMBER = 500;

    public $collection;
    public $package_wrapping;
    public $operator_id;

    public function getOperators()
    {
        return CourierTypeU::findAvailableOperators(false);
    }

    public function rules() {
        return array(
            ['collection', 'boolean'],
            ['package_wrapping', 'in', 'range' => array_keys(User::getWrappingList())],


            array('operator_id', 'required'),
            array('operator_id', 'in', 'range' => CHtml::listData(CourierUOperator::getOperatorsForCountryAndUser(false, Yii::app()->user->id),'id', 'id')),
        );
    }

    public function attributeLabels() {
        return array(
            'collection' => Yii::t('courier', 'Collection'),
            'package_wrapping' => Yii::t('courier', 'Package wrapping'),
            'operator_id' => Yii::t('courier', 'Operator'),
        );
    }
}