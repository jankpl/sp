<h1>Parent accounting user list</h1>
<table class="table table-striped table-bordered table-condensed">
    <tr>
        <th>User</th>
        <th>Parent user</th>
    </tr>
    <?php
    /* @var $models User[] */
   $models = User::model()->withUserSetting('parentAccountingUser')->findAll();
   foreach($models AS $model):
    ?>
       <tr>
           <td><a href="<?= Yii::app()->createUrl('/user/', ['id' => $model->id]);?>" target="_blank"><?= $model->login;?> (#<?= $model->id;?>)</a></td>
           <td><a href="<?= Yii::app()->createUrl('/user/', ['id' => $model->getParentAccountingUser()]);?>" target="_blank"><?= User::model()->findByPk($model->getParentAccountingUser())->login;?> (#<?= $model->getParentAccountingUser();?>)</a></td>
       </tr>
    <?php
    endforeach;
    ?>
</table>
