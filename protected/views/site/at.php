<?php

Yii::app()->getModule('courier');



//            try {


$package = [];
$package['weight'] = 5;
$package['size_l'] = 6;
$package['size_w'] = 7;
$package['size_d'] = 8;
$package['value'] = 5;
$package['content'] = 'Test';

$sender = [];
$sender['name'] = 'abc';
$sender['company'] = 'def';
$sender['address_line_1'] = 'abc 1';
$sender['address_line_2'] = '';
$sender['country'] = 'PL';
$sender['zip_code'] = '44-200';
$sender['city'] = 'abc';
$sender['tel'] = 'abc';

$receiver = [];
$receiver['name'] = 'asd';
$receiver['company'] = 'asd';
$receiver['address_line_1'] = 'abc 1';
$receiver['address_line_2'] = '';
$receiver['country'] = 'PL';
$receiver['zip_code'] = '44200';
$receiver['city'] = 'abc';
$receiver['tel'] = 'abc';

$service['operator'] = 3;

$data = [];
$data['package'] = $package;
$data['sender'] = $sender;
$data['receiver'] = $receiver;


//
$packageData = $data['package'];
$senderData = $data['sender'];
$receiverData = $data['receiver'];
$serviceData = $data['service'];
//
//            file_put_contents('apidump.txt', print_r($_REQUEST, 1));

$courier = new \Courier_CourierTypeInternal();

$courier->user_id = 8;
$courier->source = \Courier::SOURCE_API;
$courier->package_weight = $packageData['weight'];
$courier->package_size_d = $packageData['size_d'];
$courier->package_size_l = $packageData['size_l'];
$courier->package_size_w = $packageData['size_w'];
$courier->package_value = $packageData['value'];
$courier->package_content = $packageData['content'];

$sender = new AddressData();
$sender->name = $senderData['name'];
$sender->company = $senderData['company'];
$sender->country_id = \CountryList::getIdByCode($senderData['country']);
$sender->city = $senderData['city'];
$sender->zip_code = $senderData['zip_code'];
$sender->address_line_1 = $senderData['address_line_1'];
$sender->address_line_2 = $senderData['address_line_2'];
$sender->tel = $senderData['tel'];
$sender->email = $senderData['email'];

$receiver = new AddressData();
$receiver->name = $receiverData['name'];
$receiver->company = $receiverData['company'];
$receiver->country_id = \CountryList::getIdByCode($receiverData['country']);
$receiver->city = $receiverData['city'];
$receiver->zip_code = $receiverData['zip_code'];
$receiver->address_line_1 = $receiverData['address_line_1'];
$receiver->address_line_2 = $receiverData['address_line_2'];
$receiver->tel = $receiverData['tel'];
$receiver->email = $receiverData['email'];

$courier->senderAddressData = $sender;
$courier->receiverAddressData = $receiver;

$courierTypeDomestic = new \CourierTypeDomestic();
$courierTypeDomestic->domestic_operator_id = $serviceData['operator'];


$courier->courierTypeDomestic = $courierTypeDomestic;
$courier->courierTypeDomestic->courier = $courier;


$errors = false;
// validate elements
if(!$courier->validate())
{
    $errors = true;
    $code = 'PACKAGE.INVALID_DATA';
    $details = $courier->getErrors();
}

if(!$errors &&
    !$courier->senderAddressData->validate()
)
{
    $errors = true;
    $code = 'SENDER.INVALID_DATA';
    $details = $courier->getErrors();
}

if(!$errors &&
    !$courier->receiverAddressData->validate()
)
{
    $errors = true;
    $code = 'RECEIVER.INVALID_DATA';
    $details = $courier->receiverAddressData->getErrors();
}

if(!$courier->courierTypeDomestic->validate())
{
    $errors = true;
    $code = 'SERVICE.INVALID_DATA';
    $details = $courier->getErrors();
}


if(!$errors) {

    if($courierDomesticList = $courier->courierTypeDomestic->saveNewPackage())
    {
        /* @var $item \CourierTypeDomestic */
        foreach($courierDomesticList AS $item)
        {
            $item->courierLabelNew->runLabelOrdering();
            $label = $item->courierLabelNew->getFileAsString();

            $status = 1;
            $response = [];
            $response[] = [
                'package_ids' => $item->courier->local_id,
                'label' => base64_encode($label)
            ];
        }
    }


}




//var_dump($response);
//var_dump($code);
//var_dump($details);

//            } catch (\Exception $ex) {
//                $code = 4;
//                $details = print_r($ex, 1);
//            }




?>
abc