<?php
/* @var $model SpPoints */
?>
<div class="well">
    <p><?= $model->spPointsTr->text;?></p>

    <strong><?= $model->addressData->getUsefulName();?></strong><br/>
    <?= $model->addressData->address_line_1;?><br/>
    <?= $model->addressData->address_line_2 != '' ? $model->addressData->address_line_2.'<br/>' : ''; ?>
    <?= $model->addressData->zip_code;?>, <?= $model->addressData->city; ?><br/>
    <?= $model->addressData->country0->getTrName();?>
</div>