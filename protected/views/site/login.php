<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
?>

<?php
$this->widget('FlashPrinter');
?>


<h2><?php echo Yii::t('site','Login');?></h2>

<p><?php echo Yii::t('site','Wypełnij poniższy formularz aby się zalogować:');?></p>

<?php $this->renderPartial('_login', array(
    'model' => $model,
)); ?>
