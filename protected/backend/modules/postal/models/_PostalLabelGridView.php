<?php

class _PostalLabelGridView extends PostalLabel
{
    public $__postal_id;
    public $_date_entered;
    public $_broker;

    public function rules() {

        $array = array(
            array('__postal_id, _broker, _date_entered','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('t.hash', $this->hash, true);
        $criteria->compare('file_type', $this->file_type, true);
        $criteria->compare('file_path', $this->file_path, true);
        $criteria->compare('track_id', $this->track_id, true);
        $criteria->compare('source_postal_operator_id', $this->source_postal_operator_id);
        $criteria->compare('t.stat', $this->stat);

        // $criteria->compare('__type', $this->__type);

        if($this->__postal_id)
        {
            $criteria->together  =  true;
            $criteria->with = array('postal');
            $criteria->compare('postal.local_id',$this->__postal_id);
        }

        if($this->_date_entered)
        {
            $criteria->together  =  true;
            $criteria->with = array('postal');
            $criteria->compare('postal.date_entered',$this->_date_entered, true);
        }

        if($this->_broker)
        {
            $criteria->together  =  true;
            $criteria->with = array('postalOperator');
            $criteria->compare('postalOperator.broker_id',$this->_broker);
        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes = array(
            '__postal_id'=>array(
                'asc'=>'postal.local_id ASC',
                'desc'=>'postal.local_id DESC',
            ),
            '_date_entered'=>array(
                'asc'=>'postal.date_entered ASC',
                'desc'=>'postal.date_entered DESC',
            ),
            '_broker'=>array(
                'asc'=>'postalOperator.broker_id ASC',
                'desc'=>'postalOperator.broker_id DESC',
            ),
            '*', // add all of the other columns as sortable
        );


        return new CActiveDataProvider($this, array(
            'criteria' => $this->with('postal')->searchCriteria(),
            'sort'=> $sort,
            'Pagination' => array (
                'PageSize' => 50,
            ),
        ));

    }



}