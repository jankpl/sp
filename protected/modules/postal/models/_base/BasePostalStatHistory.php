<?php

/**
 * This is the model base class for the table "postal_stat_history".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PostalStatHistory".
 *
 * Columns in table "postal_stat_history" available as properties of the model,
 * followed by relations of table "postal_stat_history" available as properties of the model.
 *
 * @property string $id
 * @property string $date_entered
 * @property string $status_date
 * @property integer $previous_stat_id
 * @property string $previous_stat_name
 * @property integer $current_stat_id
 * @property string $current_stat_name
 * @property string $author
 * @property string $postal_id
 * @property integer $hidden
 * @property string $location
 *
 * @property PostalStat $currentStat
 * @property PostalStat $previousStat
 * @property Postal $postal
 */
abstract class BasePostalStatHistory extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'postal_stat_history';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'PostalStatHistory|PostalStatHistories', $n);
	}

	public static function representingColumn() {
		return 'date_entered';
	}

	public function rules() {
		return array(
			array('date_entered, current_stat_id, current_stat_name, postal_id', 'required'),
			array('previous_stat_id, current_stat_id', 'numerical', 'integerOnly'=>true),
			array('previous_stat_name, current_stat_name', 'length', 'max'=>256),
			array('author', 'length', 'max'=>45),
			array('postal_id', 'length', 'max'=>10),
			array('previous_stat_id, previous_stat_name, author', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, date_entered, previous_stat_id, previous_stat_name, current_stat_id, current_stat_name, author, postal_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'currentStat' => array(self::BELONGS_TO, 'PostalStat', 'current_stat_id'),
			'previousStat' => array(self::BELONGS_TO, 'PostalStat', 'previous_stat_id'),
			'postal' => array(self::BELONGS_TO, 'Postal', 'postal_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'date_entered' => Yii::t('app', 'Date Entered'),
			'status_date' => Yii::t('app', 'Date Of Status'),
			'previous_stat_id' => null,
			'previous_stat_name' => Yii::t('app', 'Previous Stat Name'),
			'current_stat_id' => null,
			'current_stat_name' => Yii::t('app', 'Current Stat Name'),
			'author' => Yii::t('app', 'Author'),
			'postal_id' => null,
			'currentStat' => null,
			'previousStat' => null,
			'postal' => null,
			'hidden' => Yii::t('app', 'Hidden'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('date_entered', $this->date_entered, true);
		$criteria->compare('status_date', $this->status_date, true);
		$criteria->compare('previous_stat_id', $this->previous_stat_id);
		$criteria->compare('previous_stat_name', $this->previous_stat_name, true);
		$criteria->compare('current_stat_id', $this->current_stat_id);
		$criteria->compare('current_stat_name', $this->current_stat_name, true);
		$criteria->compare('author', $this->author, true);
		$criteria->compare('postal_id', $this->postal_id);
		$criteria->compare('hidden', $this->hidden);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}