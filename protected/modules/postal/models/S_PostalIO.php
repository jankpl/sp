<?php

class S_PostalIO
{

    public static function exportToXls(array $models, $modelsAsDataReaderObj = false)
    {
        $postalAttributes = [
            'local_id',
            'date_entered',
            'postal_stat_id',
            'weight_class',
            'weight',
            'size_class',
            'content',
            'bulk',
            'bulk_id',
            'bulk_number',
            'user_id',
            'ref',
        ];

        $addressDataAttributes = [
            'name',
            'company',
            'country',
            'city',
            'zip_code',
            'address_line_1',
            'address_line_2',
            'tel',
        ];

        if($modelsAsDataReaderObj)
            return self::_exportToXls2($models, $postalAttributes, $addressDataAttributes, $modelsAsDataReaderObj, false);
        else
            self::_exportToXls($models, $postalAttributes, $addressDataAttributes, false);
    }

    public static function exportToXlsUser(array $models)
    {

        $postalAttributes = [
            'local_id',
            'date_entered',
            'postal_stat_id',
            'weight_class',
            'weight',
            'size_class',
            'postal_type',
            'bulk',
            'bulk_id',
            'bulk_number',
            'content',
            'ref',
        ];

        $addressDataAttributes = [
            'name',
            'company',
            'country',
            'city',
            'zip_code',
            'address_line_1',
            'address_line_2',
            'tel',
        ];



        return self::_exportToXls($models, $postalAttributes, $addressDataAttributes);

    }

    protected static function _exportToXls(array $models, array $postalAttributes, array $addressDataAttributes, $forUser = true)
    {

        $done = [];

        // specific fileds for particular postal types
        $specificHeader = [];


        $senderHeader = [];
        foreach ($addressDataAttributes AS $key => $item) {
            $senderAttributes[$key] = 'sender ' . $item;
        }

        $receiverHeader = [];
        foreach ($addressDataAttributes AS $key => $item)
            $receiverAttributes[$key] = 'receiver ' . $item;


        $arrayHeader = array_merge($postalAttributes, ['receiver country'], ['weight class name'], ['size class name'], ['stat name'], ['type'], ['external_operator_id'], $senderAttributes, $receiverAttributes, $specificHeader);

        if(!$forUser) {
            $arrayHeader[] = 'salesman';
        }
        $arrayHeader[] = 'additional ext. ids';
        $arrayHeader[] = 'weight_class_value';
        $arrayHeader[] = 'operator';
        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Order ID' : 'note1';
        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Optional Reference' : 'note2';
        $arrayHeader[] = 'value';
        $arrayHeader[] = 'value_currency';


        $array = [];
        $array[0] = $arrayHeader;

        $i = 1;

        /* @var $model Postal */
        foreach ($models AS $key => $model) {


            if ($model->senderAddressData == NULL)
                $model->senderAddressData = new AddressData;

            if ($model->receiverAddressData == NULL)
                $model->receiverAddressData = new AddressData;

            $dataPostal = $model->getAttributes($postalAttributes);

            $dataReceiver = $model->receiverAddressData->getAttributes($addressDataAttributes, true);

            // reset model if not found
            if($model->receiverAddressData->id === NULL)
                $model->receiverAddressData = NULL;

            $temp = [];
            foreach ($addressDataAttributes AS $key2 => $item)
                $temp['sender_' . $key2] = $model->senderAddressData->$item;
            $dataSender = $temp;

            $temp = [];
            foreach ($dataReceiver AS $key3 => $item)
                $temp['receiver_' . $key3] = $item;
            $dataReceiver = $temp;

            $postalType = [$model->getPostalTypeName()];
            $statName = [ $model->stat->getStatText() ];

            $receiverCountry = [ $model->getReceiverCountryName() ];
            $weightClass = [ $model->getWeightClassName() ];
            $sizeClass = [ $model->getSizeClassName() ];

            $externalTrackId = [''];
            if($model->postalLabel !== NULL)
            {
                $externalTrackId = [ $model->postalLabel->track_id ];
            }

            $array[$i] = array_merge($dataPostal, $receiverCountry, $weightClass, $sizeClass, $statName, $postalType, $externalTrackId, $dataSender, $dataReceiver);

            if(!$forUser) {
                $array[$i][] = $model->user->salesman->name;
            }

            $tempExtId = [];
            foreach($model->postalAdditionalExtId AS $extId)
                $tempExtId[] = $extId->external_id;

            $array[$i][] = implode(',', $tempExtId);
            $array[$i][] = $model->getWeightClassValue();

            if($model->postalLabel)
                $array[$i][] = $model->postalOperator->name;
            else
                $array[$i][] = '';

            $array[$i][] = $model->note1;
            $array[$i][] = $model->note2;
            $array[$i][] = $model->value;
            $array[$i][] = $model->value_currency;


            $i++;

            $done[$key] = $model->id;
        }

//        require_once(Yii::getPathOfAlias('application.vendor.excel_xml').'/Excel_XML.php');
//        $xls = new Excel_XML('UTF-8', true, 'Export');
//        $xls->addArray($array);
//        $xls->generateXML('Export');

        S_XmlGenerator::generateXml($array);

        return $done;

    }


    protected static function _exportToXls2(array &$modelsBase, array $postalAttributes, array $addressDataAttributes, $modelsAsDataReaderObj = false, $forUser = true)
    {
        $name = 'export_'.date('Y-m-d');
        $done = [];

        // specific fileds for particular postal types
        $specificHeader = [];


        $senderHeader = [];
        foreach ($addressDataAttributes AS $key => $item) {
            $senderAttributes[$key] = 'sender ' . $item;
        }

        $receiverHeader = [];
        foreach ($addressDataAttributes AS $key => $item)
            $receiverAttributes[$key] = 'receiver ' . $item;


        $arrayHeader = array_merge($postalAttributes, ['receiver country'], ['weight class name'], ['size class name'], ['stat name'], ['type'], ['external_operator_id'], $senderAttributes, $receiverAttributes, $specificHeader);


        if(!$forUser) {
            $arrayHeader[] = 'salesman';
        }
        $arrayHeader[] = 'additional ext. ids';
        $arrayHeader[] = 'weight_class_value';
        $arrayHeader[] = 'operator';
        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Order ID' : 'note1';
        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Optional Reference' : 'note2';
        $arrayHeader[] = 'value';
        $arrayHeader[] = 'value_currency';


//        $array = [];
//        $array[0] = $arrayHeader;

        $writer = Box\Spout\Writer\WriterFactory::create(Box\Spout\Common\Type::XLSX); // for XLSX files
        $writer->openToBrowser('export.xls');
        $writer->addRow($arrayHeader);

        $i = 1;

        $CHUNK_SIZE = 5000;

        if($modelsAsDataReaderObj)
        {
            $modelsContainer = array_chunk($modelsBase, $CHUNK_SIZE);
        } else
            $modelsContainer = [ $modelsBase ];

        unset($modelsBase);


        /* @var $model Postal */
        foreach($modelsContainer AS $keyContainer => &$models) {

//            MyDump::dump('exportOpt2.txt', 'CHUNK ('.$keyContainer.'): '.print_r(self::convert(memory_get_usage(true)),1));

            if($modelsAsDataReaderObj)
                $modelsAr = Postal::model()->with('senderAddressData')->with('receiverAddressData')->with('postalLabel')->findAllByAttributes(['id' => $models]);

            unset($modelsContainer[$keyContainer]);

            foreach ($modelsAr AS $iter => &$model) {

                if($modelsAsDataReaderObj)
                    $iter = $iter + $CHUNK_SIZE * $keyContainer;

                if ($model->senderAddressData == NULL)
                    $model->senderAddressData = new AddressData;

                if ($model->receiverAddressData == NULL)
                    $model->receiverAddressData = new AddressData;

                $dataPostal = $model->getAttributes($postalAttributes);

                $dataReceiver = $model->receiverAddressData->getAttributes($addressDataAttributes, true);

                // reset model if not found
                if($model->receiverAddressData->id === NULL)
                    $model->receiverAddressData = NULL;

                $temp = [];
                foreach ($addressDataAttributes AS $key2 => $item)
                    $temp['sender_' . $key2] = $model->senderAddressData->$item;
                $dataSender = $temp;

                $temp = [];
                foreach ($dataReceiver AS $key3 => $item)
                    $temp['receiver_' . $key3] = $item;
                $dataReceiver = $temp;

                $postalType = [$model->getPostalTypeName()];
                $statName = [ $model->stat->getStatText() ];

                $receiverCountry = [ $model->getReceiverCountryName() ];
                $weightClass = [ $model->getWeightClassName() ];
                $sizeClass = [ $model->getSizeClassName() ];

                $externalTrackId = [''];
                if($model->postalLabel !== NULL)
                {
                    $externalTrackId = [ $model->postalLabel->track_id ];
                }

                $temp = array_merge($dataPostal, $receiverCountry, $weightClass, $sizeClass, $statName, $postalType, $externalTrackId, $dataSender, $dataReceiver);

                if(!$forUser) {
                    $temp[] = $model->user->salesman->name;
                }
                $tempExtId = [];
                foreach($model->postalAdditionalExtId AS $extId)
                    $tempExtId[] = $extId->external_id;

                $temp[] = implode(',', $tempExtId);
                $temp[] = $model->getWeightClassValue();

                if($model->postalLabel)
                    $temp[] = $model->postalOperator->name;
                else
                    $temp[] = '';


                $temp[] = $model->note1;
                $temp[] = $model->note2;
                $temp[] = $model->value;
                $temp[] = $model->value_currency;

                $writer->addRow($temp);
//            $array[$i] = array_merge($dataPostal, $receiverCountry, $weightClass, $sizeClass, $statName, $postalType, $externalTrackId, $dataSender, $dataReceiver);
                $i++;

                $done[$key] = $model->id;
                $modelsAr[$iter] = NULL;
                unset($modelsAr[$iter]);
                $model = NULL;
                unset($model);
            }

            $modelsAr = NULL;
            unset($modelsAr);
            gc_collect_cycles();

        }

        $modelsContainer = NULL;
        unset($modelsContainer);
        gc_collect_cycles();


        $array = NULL;
        unset($array);
        gc_collect_cycles();

//        require_once(Yii::getPathOfAlias('application.vendor.excel_xml').'/Excel_XML.php');
//        $xls = new Excel_XML('UTF-8', true, 'Export');
//        $xls->addArray($array);
//        $xls->generateXML('Export');

//        S_XmlGenerator::generateXml($array);

        $filename = $name . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0'); //no cache

        $writer->close();


        return $done;

    }
}