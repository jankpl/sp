<?php

class Courier_CourierTypeInternalSimple extends Courier
{

    public function rules() {


        $arrayValidate = array(


            array('ref', 'application.validators.courierRefValidator'),

            array('package_value', 'numerical', 'min'=>1, 'max' => 999999),
            array('package_value', 'match', 'pattern'=> '/^[0-9]+(\.[0-9]{1,2})?$/'),

            array('user_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?NULL:Yii::app()->user->id),
            array('admin_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?Yii::app()->user->id:NULL),

            array('packages_number', 'default', 'setOnEmpty' => true, 'value' => 1),
            array('packages_number', 'numerical', 'integerOnly'=>true, 'min' => 1),



            array('package_weight, package_size_l, package_size_w, package_size_d, package_value', 'required'),

            array('courier_type_external_id, courier_type_internal_id, sender_address_data_id, receiver_address_data_id, courier_stat_id', 'numerical', 'integerOnly'=>true),

//            array('package_size_l, package_size_w, package_size_d', 'numerical', 'integerOnly' => true, 'min' => 1),

            array('package_size_l', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => S_PackageMaxDimensions::getPackageMaxDimensions()['l']),
            array('package_size_w', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => S_PackageMaxDimensions::getPackageMaxDimensions()['w']),
            array('package_size_d', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => S_PackageMaxDimensions::getPackageMaxDimensions()['d']),
            array('package_weight', 'numerical',  'min' => 0.01, 'max' => S_PackageMaxDimensions::getPackageMaxDimensions()['weight']),

            array('ref', 'length', 'max'=>64),

            array('package_content', 'length', 'max'=>256),


            array('source', 'numerical', 'integerOnly'=>true),

            array('date_updated', 'safe'),
            array('courier_type_external_id, courier_type_internal_id, date_updated, sender_address_data_id, package_value, package_content, package_weight, package_size_l, package_size_w, package_size_d', 'default', 'setOnEmpty' => true, 'value' => null),
        );

        $arrayBasic = array(
             array('courier_type_external_id, courier_type_internal_id, date_entered, date_updated, local_id, hash, sender_address_data_id, receiver_address_data_id, courier_stat_id, package_weight, package_size_l, package_size_w, package_size_d, packages_number, package_value, package_content, cod, ref, user_id, admin_id, _first_status_date', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('courier_type_external_id, courier_type_internal_id, date_updated, sender_address_data_id, receiver_address_data_id, package_weight, package_size_l, package_size_w, package_size_d, package_value, package_content, cod, ref, user_id, admin_id', 'default', 'setOnEmpty' => true, 'value' => null),

            array('courier_stat_id', 'default', 'setOnEmpty' => true, 'value' => CourierStat::NEW_ORDER),
        );


        return array_merge($arrayBasic, $arrayValidate);
    }

}