<?php

class MailBotController extends CController
{
    const LOCAL = false;

    public function actionIndex()
    {
        if(self::LOCAL)
            $dir = Yii::app()->basePath.'/../ftp2process/';
        else
            $dir = Yii::app()->basePath.'/../../ftp2processTest/';

        $semaphore = new Semaphore(intval(Semaphore::MAILBOT_PREFIX), Yii::app()->basePath . '/_temp/');
        try {
            $semaphore->acquire();
            MyDump::dump('mailbot.txt', $this->id.' : SEM ACQUIRED ('.Semaphore::MAILBOT_PREFIX.')');
        } catch (Exception $e) {
            MyDump::dump('mailbot.txt', $this->id.' : SEM LOCKED ('.Semaphore::MAILBOT_PREFIX.')');
            echo 'SEM LOCKED!';
            Yii::app()->end();
        }

        try{

            $imap = new \SSilence\ImapClient\ImapClient([
                'flags' => [
                    'service' => \SSilence\ImapClient\ImapConnect::SERVICE_IMAP,
                    'encrypt' => \SSilence\ImapClient\ImapConnect::ENCRYPT_NOTLS,
                    'validateCertificates' => \SSilence\ImapClient\ImapConnect::NOVALIDATE_CERT,
                    // Turns debug on or off
                    'debug' => \SSilence\ImapClient\ImapConnect::DEBUG,
                ],
                'mailbox' => [
                    'remote_system_name' => 'mail.swiatprzesylek.pl',
                    'port' => 143,
                ],
                'connect' => [
                    'username' => 'mailbot@swiatprzesylek.pl',
                    'password' => 'rower2019'
                ]
            ]);

        } catch (Exception $error){
            MyDump::dump('mailbot.txt', 'CONNECTION PROBLEM');
            die();
        }

        $imap->selectFolder("Inbox");

        $unreadMessages = $imap->countUnreadMessages();
        $totalMessages = $imap->countMessages();

        MyDump::dump('mailbot.txt', 'MESSAGES: '.$unreadMessages.'/'.$totalMessages);

        if($unreadMessages)
        {
            $messages = $imap->getUnreadMessages(false);
            if(is_array($messages))
                foreach($messages AS $message)
                {
                    $messageId = $message->header->msgno;
                    $fromName  = $message->header->details->from[0]->mailbox;
                    $fromDomain  = $message->header->details->from[0]->host;
                    $title  = $message->header->details->subject;

                    $fromAddress = strtolower($fromName.'@'.$fromDomain);

                    MyDump::dump('mailbot.txt', 'START PROCESS MESSAGE "'.$title.'" FROM "'.$fromAddress.'"');

                    if(!in_array($fromAddress, [
                        'abokhorst@kaabnl.nl',
                        'jankopec@swiatprzesylek.pl',
                        'praczynska@kaabnl.nl',
                        'operations@kaabnl.nl',
                    ])) {

                        S_Mailer::send($fromAddress, $fromAddress, 'BOT FAIL: '.$title, 'Your address it not allowed to use Mailbot!', false);

                        if(!self::LOCAL)
                            $imap->setSeenMessage($messageId);

                        continue;
                    }

                    $forceDdu = false;
                    $forceDdp = false;

                    if(preg_match('/ddu/i', $title))
                        $forceDdu = true;
                    else if(preg_match('/ddp/i', $title))
                        $forceDdp = true;

                    MyDump::dump('mailbot.txt', 'VARIABLES: '.print_r($forceDdu,1).' , '.print_r($forceDdp,1));

                    $i = 0;
                    $processedFiles = 0;
                    do
                    {
                        $section = $imap->getSection($messageId, $i);

                        if($section->structure->disposition && $section->structure->disposition == 'attachment')
                        {
                            $filename = $section->structure->parameters[0]->value;


                            MyDump::dump('mailbot.txt', 'ATTACHMENT "'.$filename.'"');

                            $temp = explode('.', $filename);

                            $ext = strtolower($temp[sizeof($temp)-1]);
                            if (in_array($ext, [
                                'csv',
                                'xls',
                                'xlsx'
                            ]))
                            {
                                $filenameNew = $fromAddress.'%%';
                                if($forceDdu)
                                    $filenameNew .= 'DDU%%';
                                else if($forceDdp)
                                    $filenameNew .= 'DDP%%';

                                $filenameNew .= $filename;
                            }

                            $body = base64_decode($section->body);

                            if($body) {
                                file_put_contents($dir . $filenameNew, $body);
                                $processedFiles++;
                                MyDump::dump('mailbot.txt', 'SAVE ATTACHMENT "'.$filenameNew.'"');
                            } else {
                                MyDump::dump('mailbot.txt', 'SAVE ATTACHMENT ERROR"'.$filenameNew.'"');
                            }
                        }
                        $i++;
                    }
                    while($section->structure);

                    if(!self::LOCAL)
                        $imap->setSeenMessage($messageId);

                    if(!self::LOCAL)
                        S_Mailer::send($fromAddress, $fromAddress, 'BOT SUCCESS: '.$title, 'Your message has been processed. Attachments number found: '.$processedFiles, false);
                }
        }


        try {
            $semaphore->release();
            MyDump::dump('mailbot.txt', 'SEMAPHORE RELEASED');
        } catch (Exception $e) {
            MyDump::dump('mailbot.txt', 'SEMAPHORE RELEASE FAILED');
            die('Could not release the semaphore');
        }
    }




}