<?php

class PostalLabelPrinterController extends Controller
{

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function beforeAction($action)
    {
        if(Postal::isAvailableForUser() OR $action->id == 'ajaxGetPrice')
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('postal', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Postal',

            ));
            exit;
        }
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($withClone = true)
    {
        $uniqid = uniqid();

        $model = false;
        if (isset($_POST['local_id'])) {

//            $CACHE_NAME = 'POST_LABEL_REPRINTER_'.$_POST['local_id'];
//
//            $blockDuplicate = Yii::app()->cache->get($CACHE_NAME);
//
//            if($blockDuplicate)
//            {
//                MyDump::dump('postal_reprinter.txt', $uniqid.':BLOCKED POST: '.print_r($_POST['local_id'],1));
//                return false;
//            }
//            Yii::app()->cache->set($CACHE_NAME, 5, 10);


            MyDump::dump('postal_reprinter.txt', $uniqid.':POST: '.print_r($_POST['local_id'],1));


            $remoteId = $_POST['local_id'];

//            if($withClone)
//            {
//                $response = PostalLabel::model()->findAllByAttributes(['track_id' => $remoteId]);
//            } else {
            /* @var $response Postal[] */
                $response = PostalExternalManager::findPostalIdsByRemoteId($remoteId);
//            }

            $n = S_Useful::sizeof($response);
            if (!$n) {
                Yii::app()->user->setFlash('notice', '0 packages found!');
            } else if ($n > 1) {
                Yii::app()->user->setFlash('notice', 'More than 1 package found!');
            } else {
                $model = array_pop($response);

                if($model->user_id != Yii::app()->user->id) {
                    Yii::app()->user->setFlash('notice', 'Source item belongs to another user!');
                    $model = false;
                }
                else {

                    MyDump::dump('postal_reprinter.txt', $uniqid . ':MODEL ID: ' . print_r($model->id, 1));
                    if ($withClone) {

                        if (in_array($model->postalOperator->id,[78,100]) && strtoupper(substr($remoteId, 0, 2)) == 'RB')
                            $forceOperatorId = 102;
                        else
                            $forceOperatorId = $model->postalOperator->id;

                        $newModel = Postal::cloneItem($model, $forceOperatorId);

                        if (!$newModel) {
                            Yii::app()->user->setFlash('notice', 'Something went wrong...');
                            echo CJSON::encode(['redirect' => true, 'url' => Yii::app()->createAbsoluteUrl('/postal/postalLabelPrinter')]);
                            exit;
                        }

                        $model = $newModel;

                        if($model->postalOperator->broker_id == GLOBAL_BROKERS::BROKER_CZECH_POST) {
                            $newModel->postalLabel->refresh();
                            MyDump::dump('postal_reprinter_cpost.txt', $remoteId . ':' . $newModel->local_id.':'.$newModel->postalLabel->track_id);
                        }

                        MyDump::dump('postal_reprinter.txt', $uniqid . ': NEW MODEL ID: ' . print_r($newModel->id, 1));
                    }
                }
            }

            if($model && $model->postalLabel)
            {
                $counter = 0;
                $labelStat = $model->postalLabel->stat;
                $labelId = $model->postalLabel->id;
                while($labelStat != PostalLabel::STAT_SUCCESS)
                {
                    sleep(2);
                    $counter++;

                    MyDump::dump('postal_reprinter.txt', $uniqid.':LABEL STAT: '.print_r($labelStat,1));

                    $model->postalLabel->refresh();
                    $labelStat = PostalLabel::model()->findByPk($labelId)->stat;

                    if($counter > 120)
                    {
                        Yii::app()->user->setFlash('notice', 'Label timeouted...');
                        echo CJSON::encode(['redirect' => true, 'url' => Yii::app()->createAbsoluteUrl('/postal/postalLabelPrinter')]);
                        exit;
                    }
                }
                MyDump::dump('postal_reprinter.txt', $uniqid.':LABEL STAT: '.print_r($labelStat,1));

                echo CJSON::encode(['redirect' => true, 'hasLabel' => true, 'url' => Yii::app()->createAbsoluteUrl('/postal/postal/CarriageTicket', ['urlData' => $model->hash, 'type' => PostalLabelPrinter::PDF_ROLL])]);
                exit;
            } else {
                echo CJSON::encode(['redirect' => true, 'url' => Yii::app()->createAbsoluteUrl('/postal/postalLabelPrinter')]);
                exit;
            }

        }

        $this->render('index', array(
            'model' => $model,
            'withClone' => $withClone
        ));
    }
}