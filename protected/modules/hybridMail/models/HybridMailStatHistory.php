<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailStatHistory');

class HybridMailStatHistory extends BaseHybridMailStatHistory
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function scopes()
    {
        return array(
            'visible'=>array(
                'condition'=>'hidden  = 0',
            ),
        );
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

            //array('status_date', 'type', 'type' => 'datetime', 'allowEmpty' => true, 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
            array('current_stat_id, hybrid_mail_id', 'required'),
            array('previous_stat_id, current_stat_id', 'length', 'max' => 256),
            array('author', 'length', 'max'=>45),
            array('hybrid_mail_id', 'length', 'max'=>10),
            array('previous_stat_id, author', 'default', 'setOnEmpty' => true, 'value' => null),
            array('hidden', 'default', 'setOnEmpty' => true, 'value' => 0),
            array('id, date_entered, previous_stat_id, current_stat_id, author, hybrid_mail_id, status_date, hidden', 'safe', 'on'=>'search'),
        );
    }

    public function beforeSave()
    {

        if($this->status_date == '' OR $this->status_date == '0000-00-00 00:00:00')
            $this->status_date = date('Y-m-d H:i:s');

        $this->current_stat_name = HybridMailStat::getNameById($this->current_stat_id);

        if($this->previous_stat_id != '')
            $this->previous_stat_name = HybridMailStat::getNameById($this->previous_stat_id);

        return parent::beforeSave();
    }
}