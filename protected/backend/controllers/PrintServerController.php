<?php

class PrintServerController extends Controller
{

    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id,'PrintServer'),
		));
	}


	public function actionIndex()
	{
        $model=new PrintServer('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['PrintServer']))
            $model->attributes=$_GET['PrintServer'];

        $this->render('index',array(
            'model'=>$model,
        ));
	}

    public function actionReset($id)
    {
        $model = $this->loadModel($id,'PrintServer');

        if($model->stat == PrintServer::STAT_NEW)
        $model->resetCounter();

        $this->redirect(['/printServer/view', 'id' => $id]);
    }

    public function actionLock($id)
    {
        $model = $this->loadModel($id,'PrintServer');

        if($model->stat != PrintServer::STAT_LOCKED)
        $model->lock();

        $this->redirect(['/printServer/view', 'id' => $id]);
    }

    public function actionActivate($id)
    {
        $model = $this->loadModel($id,'PrintServer');

        if($model->stat == PrintServer::STAT_NEW)
        $model->activate();

        $this->redirect(['/printServer/view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id,'PrintServer');

        if($model->stat != PrintServer::STAT_DELETED)
        $model->delete();

        $this->redirect(['/printServer/view', 'id' => $id]);
    }

    public function actionDeletePermanently($id)
    {
        $model = $this->loadModel($id,'PrintServer');

        $model->deletePermanently();

        $this->redirect(['/printServer/']);
    }

}
