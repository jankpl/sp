<?php
/* @var $this StatMapMappingController */
/* @var $type int */

$this->breadcrumbs=array(
	'Stat Map Mappings',
);

$this->menu=array(
	array('label'=>'New mapping', 'url'=>array('new', 'type' => $type)),
	array('label'=>'Synchronize', 'url'=>array('synchronize', 'type' => $type)),
);
?>

<h1><?= StatMapMapping::getTypeName($type);?>: Stat Map Mappings</h1>

<?php
$this->widget('FlashPrinter');
?>


<?php
$waiting = S_Useful::sizeof(StatMapMapping::listStatsNotMapped($type));
if($waiting):
	?>
	<div class="alert alert-warning">
		Number of statuses watiting for mapping: <strong><?= $waiting;?></strong>
	</div>
	<?php
endif;
?>

<?php
$waiting = StatMapMapping::countItemsNotMapper($type);
if($waiting):
	?>
	<div class="alert alert-warning">
		Number of items without mapping: <strong><?= $waiting;?></strong>
	</div>
	<?php
endif;
?>


<table class="table table-striped table-bordered">
	<thead>
	<tr>
		<th>Map</th>
		<th>Stats no.</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach(StatMap::mapNameList() AS $key => $item):
		?>
		<tr>
			<td>
				<?php
				echo $item;
				?>
			</td>
			<td>
				<?php
				echo StatMapMapping::numberOfStatsForMap($key, $type);
				?>
			</td>
			<td>
				<?php
				echo CHtml::link('Manage', ['/statMapMapping/manage', 'id' => $key, 'manage_type' => $type])
				?>
			</td>
		</tr>
		<?php
	endforeach;
	?>
	</tbody>
</table>

<h3>Apply current mapping to items:</h3>

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl('/statMapMapping/applyMapping', ['type' => $type]),
));
?>
<div class="text-center">
	<?php
	echo CHtml::submitButton('Map', ['name' => 'apply', 'class' => 'btn btn-warning btn-lg']);
	?>
</div>
<?php
$this->endWidget();
?>



