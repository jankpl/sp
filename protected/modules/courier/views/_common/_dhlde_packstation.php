<?php
$model = new AddressData();
?>
<img src="<?= Yii::app()->homeUrl;?>images/misc/dhl_packstation.png" class="img-repsonsive"/>
<br/>
<br/>
<?= Yii::t('courierDhl', 'Istnieje możliwość nadania paczki do paczkomatu');?>:<br/>
<ul>
    <li><strong>PACKSTATION</strong>
        <ul>
            <li><?= Yii::t('courierDhl', 'W polu {field} wprowadź {i}"PACKSTATION xxx"{/i}', ['{field}' => '<i>"'.$model->getAttributeLabel('address_line_1').'"</i>', '{i}' => '<i>', '{/i}' => '</i>']);?>
            <li><?= Yii::t('courierDhl', 'W polu {field} lub {field2} wprowadź  wprowadź numer identyfikacyjny', ['{field}' => '<i>"'.$model->getAttributeLabel('name').'"</i>', '{field2}' => '<i>"'.$model->getAttributeLabel('company').'"</i>', '{i}' => '<i>', '{/i}' => '</i>']);?></li>
        </ul>
    </li>
    <li><strong>POSTFILIALE</strong>
        <ul>
            <li><?= Yii::t('courierDhl', 'W polu {field} wprowadź {i}"POSTFILIALE xxx"{/i}', ['{field}' => '<i>"'.$model->getAttributeLabel('address_line_1').'"</i>', '{i}' => '<i>', '{/i}' => '</i>']);?>
            <li><?= Yii::t('courierDhl', 'W polu {field} lub {field2} wprowadź wprowadź numer identyfikacyjny', ['{field}' => '<i>"'.$model->getAttributeLabel('name').'"</i>', '{field2}' => '<i>"'.$model->getAttributeLabel('company').'"</i>', '{i}' => '<i>', '{/i}' => '</i>']);?></li>
        </ul>
    </li>
    <li><strong>PARCELSHOP</strong>
        <ul>
            <li><?= Yii::t('courierDhl', 'W polu {field} wprowadź {i}"PARCELSHOP xxx"{/i}', ['{field}' => '<i>"'.$model->getAttributeLabel('address_line_1').'"</i>', '{i}' => '<i>', '{/i}' => '</i>']);?></li>
        </ul>
    </li>

</ul>
<br/>
<?= Yii::t('courierDhl', ', gdzie {i}"xxx"{/i} to numer danego punktu.', ['{i}' => '<i>', '{/i}' => '</i>']);?>
