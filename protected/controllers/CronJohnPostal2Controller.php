<?php

class CronJohnPostal2Controller extends CController {

    const PACKAGE_DAYS_IGNORE = 45; // for how many days check statuses

//    const PACKAGE_DAYS_IGNORE_INTERNAL_RETURN = 62; // for how many days check statuses of Internal Return packages

    const PACKAGE_DAYS_FRESH = 4; // for how many days package is considered as fresh

    const TTLOG_BLOCK_MINUTES = 720; // how long to waint between checks for not-fresh packages
    const TTLOG_BLOCK_MINUTES_FRESH = 360; // how long to wait between checks for fresh packages
    const DELAY_NEW_MINUTES = 360; // how long to wait before checking TT for new packages

    const STRIP_SIZE = 80;

    const MAX_STEPS = 6;

    const STRIP_SIZE_EXPIRE_LABELS = 500;
    const STRIP_SIZE_ARCHIVE_HISTORY = 5000;

    const TYPE_MAIN = 'main';
    const TYPE_OLDER = 'older';
    const TYPE_CANCELLED = 'cancelled';

    const INSTANCES_MAIN = 4;
    const INSTANCES_OLDER = 2;
    const INSTANCES_CANCELLED = 2;

    public function init()
    {
        Yii::app()->getModule('postal');
        parent::init();
    }

    public function filters()
    {
        return array();
    }

    public function actionStats()
    {


        echo '<table style="vertical-align: top;"><tr>';
        echo '<td style="vertical-align: top;">';
        echo 'MAIN:';
        $main = self::_modelsMain(1, 1, false);
        echo (S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';
        echo '<td/><td style="vertical-align: top;">';

        echo '_modelsCancelled:';
        $main = self::_modelsCancelled(1, 1, false);
        echo(S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';
        echo '<td/><td style="vertical-align: top;">';

        echo '_modelsMainSlower:';
        $main = self::_modelsMainSlower(1, 1, false);
        echo(S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';
        echo '<td/><td style="vertical-align: top;">';

        echo '_modelsOld:';
        $main = self::_modelsOld(1, 1, false);
        echo(S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';      
        echo '</td>';
        echo '</tr></table>';


    }

    function actionRun()
    {
        $multiCurl = [];
        $result = [];

        $mh = curl_multi_init();



        $j = 1;
        if (date('G') <= 23 && date('G') >= 5) {
            for ($i = 1; $i <= self::INSTANCES_MAIN; $i++) {
                $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohnPostal2/index', ['mod' => $i, 'type' => self::TYPE_MAIN]);
                $multiCurl[$j] = curl_init();
                curl_setopt($multiCurl[$j], CURLOPT_URL, $fetchURL);
                curl_setopt($multiCurl[$j], CURLOPT_HEADER, 0);
                curl_setopt($multiCurl[$j], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$j], CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($multiCurl[$j], CURLOPT_MAXREDIRS, 10);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT, 0);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT_MS, 0);
                curl_setopt($multiCurl[$j], CURLOPT_CONNECTTIMEOUT, 0);
                curl_multi_add_handle($mh, $multiCurl[$j]);

                $j++;
            }
        }

        if (date('G') <= 23 && date('G') >= 5) {
            for ($i = 1; $i <= self::INSTANCES_OLDER; $i++) {
                $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohnPostal2/index', ['mod' => $i, 'type' => self::TYPE_OLDER]);
                $multiCurl[$j] = curl_init();
                curl_setopt($multiCurl[$j], CURLOPT_URL, $fetchURL);
                curl_setopt($multiCurl[$j], CURLOPT_HEADER, 0);
                curl_setopt($multiCurl[$j], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$j], CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($multiCurl[$j], CURLOPT_MAXREDIRS, 10);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT, 0);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT_MS, 0);
                curl_setopt($multiCurl[$j], CURLOPT_CONNECTTIMEOUT, 0);
                curl_multi_add_handle($mh, $multiCurl[$j]);

                $j++;
            }
        }

        if (date('G') > 23 OR date('G') < 5) {
            for ($i = 1; $i <= self::INSTANCES_CANCELLED; $i++) {
                $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohnPostal2/index', ['mod' => $i, 'type' => self::TYPE_CANCELLED]);
                $multiCurl[$j] = curl_init();
                curl_setopt($multiCurl[$j], CURLOPT_URL, $fetchURL);
                curl_setopt($multiCurl[$j], CURLOPT_HEADER, 0);
                curl_setopt($multiCurl[$j], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$j], CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($multiCurl[$j], CURLOPT_MAXREDIRS, 10);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT, 0);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT_MS, 0);
                curl_setopt($multiCurl[$j], CURLOPT_CONNECTTIMEOUT, 0);
                curl_multi_add_handle($mh, $multiCurl[$j]);

                $j++;
            }
        }

        $index = null;
        do {
            curl_multi_exec($mh, $index);
        } while ($index > 0);

        foreach ($multiCurl as $k => $ch) {
            $result[$k] = curl_multi_getcontent($ch);
            curl_multi_remove_handle($mh, $ch);
        }
        curl_multi_close($mh);

    }

    protected function getIngnoredStats()
    {
        $_ingoredStats = [
            PostalStat::CANCELLED,
            PostalStat::CANCELLED_PROBLEM,
            PostalStat::NEW_ORDER,
            PostalStat::PACKAGE_PROCESSING
        ];

        $data = array_merge($_ingoredStats, PostalStat::getKnownDeliveredStatuses());
        return $data;
    }

    protected function _modelsMain($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', $this->getIngnoredStats());

        $sql = "SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (postal.date_updated > (NOW() - INTERVAL :past_date_fresh DAY) OR postal.date_entered > (NOW() - INTERVAL :past_date_fresh DAY))
AND (postal.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND postal_stat_id NOT IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
AND stat_map_id != :stat_map AND stat_map_id != :stat_map2
AND (postal.id MOD ".$instances.") IN ($mods)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models =  Postal::model()->with('postalLabel')
            ->findAllBySql($sql,
                [
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES_FRESH,
                    ':type' => TtLog::TYPE_POSTAL,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':stat_map' => StatMap::MAP_DELIVERED,
                    ':stat_map2' => StatMap::MAP_CANCELLED,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);


        return $models;
    }

    protected function _modelsCancelled($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', $this->getIngnoredStats());

        $sql = "SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (postal.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR postal.date_entered > (NOW() - INTERVAL :past_date2 DAY))
AND postal_stat_id IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 24 HOUR))
AND postal.stat_map_id = :ignored_map AND postal.postal_stat_id != :canceled_problem
AND (postal.id MOD ".$instances.") IN ($mods)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models =  Postal::model()->with('postalLabel')
            ->findAllBySql($sql,
                [
                    ':past_date2' => self::PACKAGE_DAYS_IGNORE,
//                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':type' => TtLog::TYPE_POSTAL,
//                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_CANCELLED,
                    ':canceled_problem' => PostalStat::CANCELLED_PROBLEM,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);

        return $models;
    }

    protected function _modelsMainSlower($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', $this->getIngnoredStats());
        $sql = "SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (
(postal.date_updated >= (NOW() - INTERVAL :past_date DAY) AND postal.date_updated < (NOW() - INTERVAL :past_date_fresh DAY)) OR (postal.date_updated IS NULL AND postal.date_entered >= (NOW() - INTERVAL :past_date DAY) AND postal.date_entered < (NOW() - INTERVAL :past_date_fresh DAY))
	)
AND postal_stat_id NOT IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
AND stat_map_id != :stat_map AND stat_map_id != :stat_map2
AND (postal.id MOD ".$instances.") IN ($mods)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models =  Postal::model()->with('postalLabel')
            ->findAllBySql($sql,
                [
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':past_date' => self::PACKAGE_DAYS_IGNORE,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':type' => TtLog::TYPE_POSTAL,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':stat_map' => StatMap::MAP_DELIVERED,
                    ':stat_map2' => StatMap::MAP_CANCELLED,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);

        return $models;
    }

    protected function _modelsOld($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', $this->getIngnoredStats());

        $sql = "SELECT DISTINCT postal.* FROM postal
 LEFT JOIN tt_log ON (tt_log.item_id = postal.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (
(postal.date_updated >= (NOW() - INTERVAL :past_date_old MONTH) AND postal.date_updated < (NOW() - INTERVAL :past_date DAY)) OR (postal.date_updated IS NULL AND postal.date_entered >= (NOW() - INTERVAL :past_date MONTH) AND postal.date_entered < (NOW() - INTERVAL :past_date DAY))
	)
AND postal_stat_id NOT IN ($ignoredStats)
AND postal_type = :postal_type
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 7 DAY))
AND stat_map_id != :stat_map AND stat_map_id != :stat_map2
AND (postal.id MOD ".$instances.") IN ($mods)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models =  Postal::model()->with('postalLabel')
            ->findAllBySql($sql,
                [
                    ':past_date_old' => 6,
                    ':past_date' => 62,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':type' => TtLog::TYPE_POSTAL,
//                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':stat_map' => StatMap::MAP_DELIVERED,
                    ':stat_map2' => StatMap::MAP_CANCELLED,
//                    ':user_cancelled_stat' => PostalStat::CANCELLED_USER,
                    ':postal_type' => PostaL::POSTAL_TYPE_REG
                ]);

        return $models;
    }


    
    // update courier packages statuses with external statuses
    public function actionIndex($type, $mod = 0, $set = 0, $ssid = NULL)
    {
        exit;

        $start_time = time();
        $limit = ini_get("max_execution_time");
        $limit90 = round(0.9 * $limit);

//        sleep(2 * $mod);

        if((date('G') > 23 OR date('G') < 5) && in_array($type, [self::TYPE_MAIN, self::TYPE_OLDER]))
            exit;

        if((date('G') >= 5 AND date('G') <= 23) && in_array($type, [self::TYPE_CANCELLED]))
            exit;

        if(!$mod)
            exit;

        $start = date('Y-m-d H:i:s');

        $filename = 'cjplog_'.$type.'.txt';


        $CACHE_NAME = 'cronJohnPostalRun2_'.$type.'_'.$mod;
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== $ssid) {

            MyDump::dump($filename, "\r\n".$start." : AIP[$mod] : ".Yii::app()->session->sessionID." : ".$sessionName." : ". $ssid);
            Yii::app()->end();
        } else
            MyDump::dump($filename, "\r\n".$start." : START[$mod][$set] : ".Yii::app()->session->sessionID." : ".$sessionName." : ". $ssid);

        if(S_SystemLoad::isHigh())
        {

            MyDump::dump($filename, "\r\n".$start." : [$mod] : HIGH LOAD");
            Yii::app()->cache->delete($CACHE_NAME);
            Yii::app()->end();
        }

        if($ssid == '')
            $ssid = Yii::app()->session->sessionID;

        Yii::app()->cache->set($CACHE_NAME, $ssid, 60 * 15);

//        if($mod == 1)
//            self::tt4CancelledPackages();

        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);

        if($type == self::TYPE_MAIN)
            $postals = self::_modelsMain(self::INSTANCES_MAIN, $mod);
        else if($type == self::TYPE_OLDER)
            $postals = self::_modelsMainSlower(self::INSTANCES_OLDER, $mod);
        else if($type == self::TYPE_CANCELLED)
            $postals = self::_modelsCancelled(self::INSTANCES_CANCELLED, $mod);
        else
            throw new Exception('Unknowny type!');

        $log = $start.' ['.print_r($mod,1).'] : '.sprintf("%02d",$set)." --- ". sprintf("%03d",S_Useful::sizeof($postals))." --- ";

        try {
            /* $item Postal */
            if(is_array($postals))
                foreach($postals AS $item)
                {
                    PostalExternalManager::updateStatusForPackage($item);
                    MyDump::dump('cjp_full_log_divided.txt', $item->id.' : '.print_r($type,1).' : '.print_r($mod,1).' : '.print_r($set,1).' : '.print_r($ssid,1));
                    if(time() - $start_time > $limit90)
                    {
                        MyDump::dump('cjp_timeout.txt', 'TIMEOUTED : '.print_r($type,1).' : '.print_r($mod,1).' : '.print_r($set,1).' : '.print_r($ssid,1));
                        continue;
                    }
                }

        }
        catch(Exception $ex)
        {
            MyDump::dump('cjp_exception.txt', print_r($ex->getMessage(),1));
        }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(S_Useful::sizeof($postals) == self::STRIP_SIZE)
        {
            if($set < self::MAX_STEPS)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM : ".Yii::app()->session->sessionID." : ". $ssid;
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohnPostal2/index', array('set' => $set, 'mod' => $mod, 'ssid' => $ssid, 'type' => $type));
                MyDump::dump($filename, $log);
                header("Location: ".$path);
                Yii::app()->end();
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM: ".Yii::app()->session->sessionID." : ". $ssid;
                MyDump::dump($filename, $log);

                Yii::app()->cache->delete($CACHE_NAME);
                Yii::app()->end();
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM : ".Yii::app()->session->sessionID." : ". $ssid;
            MyDump::dump($filename, $log);

            Yii::app()->cache->delete($CACHE_NAME);
            Yii::app()->end();
        }


    }





}