DimensionalWeight = function(){

    function calculate(l,w,d){

        var dw = (l * w * d);
        dw /= 5000;

        var x = Math.round(dw);
        var y = Math.ceil(dw);

        // round up to 0.5
        if(y - dw > 0.5 && y - dw < 1)
            dw = x + 0.5;
        else if(y - dw != 0.5)
        {
            dw = y;
        }

        if(isNaN(dw) || l <= 0 || w <= 0 || d <= 0)
            dw = '0';

        return dw;
    }

    function calculateDhl(l,w,d){

        var dw = (l * w * d);
        dw /= 4000;
        dw = Math.round(dw * 10 ) / 10;

        if(dw > 31.5)
            dw = 31.5;

        if(isNaN(dw) || l <= 0 || w <= 0 || d <= 0)
            dw = '0';

        return dw;
    }

    return{
        calculate:calculate,
        calculateDhl:calculateDhl
    }
}();