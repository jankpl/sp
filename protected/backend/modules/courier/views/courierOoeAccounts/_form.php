<?php
/* @var $form GxActiveForm */
?>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-addition-list-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->dropDownList($model, 'user_id', CHtml::listData(CourierOoeAccounts::listAvailableUsers($model->user_id),'id','login'), array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'login'); ?>
        <?php echo $form->textField($model, 'login', array('maxlength' => 128)); ?>
        <?php echo $form->error($model,'login'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'pass'); ?>
        <?php echo $form->passwordField($model, 'pass', array('maxlength' => 128)); ?>
        <?php echo $form->error($model,'pass'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'account_code'); ?>
        <?php echo $form->textField($model, 'account_code', array('maxlength' => 128)); ?>
        <?php echo $form->error($model,'account_code'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'stat'); ?>
        <?php echo $form->dropDownList($model, 'stat', CHtml::listData(S_Status::listStats(),'id','name')); ?>
        <?php echo $form->error($model,'stat'); ?>
    </div><!-- row -->
    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->