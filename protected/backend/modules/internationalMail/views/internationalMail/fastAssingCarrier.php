<?php

/* @var $this InternationalMailController */
/* @var $model InternationalMail */
?>


<h3>Przypisz zewnętrznego kuriera</h3>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'fast-carrier-assign',
    'enableAjaxValidation' => false,
));
?>

<div class="form">

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <table class="list hTop" style="width: 500px;">
        <tr>
            <td>Local ID</td>
            <td>Remote ID</td>
            <td>Carrier</td>
        </tr>
        <tr>
            <td><?php echo $form->textField($model, 'local_id', array('id' => 'local_id')); ?></td>
            <td><?php echo $form->textField($model, 'outside_id', array('id' => 'outside_id')); ?></td>
        </tr>
        <tr>
            <td colspan="2">
                From responds to paste operation on inputs.
                <?php
                $this->endWidget();
                ?>
            </td>
        </tr>
    </table>

</div><!-- form -->

<?php

Yii::app()->clientScript->registerCoreScript('jquery');

echo CHtml::script('$(document).ready(function(){

$("#local_id").focus();

$("#local_id").bind("input paste", function (e) {

    $(this).val(e.originalEvent.clipboardData.getData("text/plain"));
    e.preventDefault();
    $("#outside_id").focus();

});

$("#outside_id").bind("input paste", function (e) {

    $(this).val(e.originalEvent.clipboardData.getData("text/plain"));
    e.preventDefault();

    $("form#fast-carrier-assign").submit();
});


});');
?>


