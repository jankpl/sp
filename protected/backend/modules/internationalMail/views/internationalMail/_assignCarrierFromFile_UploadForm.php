<div class="form">
    <?php

    $form = $this->beginWidget('CActiveForm',
        array(
            'enableAjaxValidation' => false,
            'htmlOptions' =>
            array('enctype' => 'multipart/form-data'),
        )
    );

    ?>

    <h2>Fetch IDs list from file</h2>

    <?php


    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }


    echo $form->errorSummary($model);
    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'file'); ?>
        <?php echo $form->fileField($model, 'file'); ?>
    </div>

    <div class="navigate">

        <?php
        echo GxHtml::submitButton(Yii::t('app', 'Upload'));
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p>Allowed file type: .csv.</p>
        <p>The file must contain following columns: [local_id] [remote_id]</p>
    </div>
</div>