<?php

class YunExpressSynchronousClient extends GlobalOperator
{
    const URL = 'https://gapi.yunexpressusa.com';
//    const USERNAME = '00001';
//    const PASSWORD = 'Z10paIcRxW4=';

    const USERNAME = '00031';
    const PASSWORD = '4titymLqaG4=';


//    public $_service;

    public $label_annotation_text = false;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_mail;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_tel;
    public $sender_mail;

    public $package_weight;
    public $package_size_l;
    public $package_size_d;
    public $package_size_w;

    public $package_content;
    public $package_value;

    public $ref;


    public $label_annotation_ref;

    public $user_id;

    public $custom_sender_name_addition = false;

//    public $packages_number = 1;


//    public static function test($returnError = true)
//    {
//        $model = new self;
//
//        $no = 'ABV131'.rand(999,99999);
//
//        $data = new stdClass();
//        $data->CustomerOrderNumber = $no;
//        $data->ShippingMethodCode = 'DEDHLV1';
//        $data->TrackingNumber = '';
//        $data->Length = 10;
//        $data->Width = 10;
//        $data->Height = 10;
//        $data->PackageCount = 2;
//        $data->Weight = 1;
//
//        $receiver = new stdClass();
//        $receiver->CountryCode = 'DE';
//        $receiver->FirstName = 'Jan Nowak';
//        $receiver->LastName = 'Jan Nowak';
//        $receiver->Company = '';
//        $receiver->Street = 'GROSSE STEINSTRASSE 60';
//        $receiver->StreetAddress1 = '';
//        $receiver->StreetAddress2 = '';
//        $receiver->City = 'Halle';
//        $receiver->State = '';
//        $receiver->Zip = '06108';
//        $receiver->Phone = '123123123';
//        $receiver->HouseNumber = '';
//        $receiver->Email = '';
//        $data->Receiver = $receiver;
//
//        $sender = new stdClass();
//        $sender->CountryCode = 'DE';
//        $sender->FirstName = 'Tomasz Kowalski';
//        $sender->LastName = 'Tomasz Kowalski';
//        $sender->Company = '';
//        $sender->Street = 'ROSSE STEINSTRASSE 55';
//        $sender->City = 'Halle';
//        $sender->State = '';
//        $sender->Zip = '06108';
//        $sender->Phone = '555222555';
//        $sender->HouseNumber = '';
//
//        $data->Sender = $sender;
//        $data->ApplicationType = 4; // Other
//        $data->IsReturn = 0;
////        $data->InsuranceType = ?;
////        $data->InsureAmount = ?;
//
//        $infos = new stdClass();
//        $infos->EName = 'Test';
//        $infos->Quantity = 1;
//        $infos->UnitPrice = 10; // USD
//        $infos->UnitWeight = 1;
//
//        $data->Parcels = [$infos, $infos];
//
//
////        $resp = $model->_call('api/common/getshippingmethods', NULL);
//        $resp = $model->_call('api/WayBill/CreateOrder', [$data]);
//
//
////        $no = 'ABV13115591';
//
////        $no = $resp->Item[0]->CustomerOrderNumber;
//
//        var_dump($resp);
//
////        $data = new stdClass();
////        $data->OrderNumbers = [ $no ];
//
//        $data = [ $no ];
//
//
//        $resp = $model->_call('api/label/print', $data);
//
//        var_dump($resp);
//        exit;
//
//        $return = [];
//        if($resp->status_code == 200)
//        {
//
//            $item = $resp->_embedded->LabelResponse[0];
//
//            if($item->status == 'error')
//            {
//                if ($returnError == true) {
//                    return GlobalOperatorResponse::createErrorResponse(print_r($item->errors,1));
//                } else {
//                    return false;
//                }
//            }
//
//            $im = ImagickMine::newInstance();
//            $im->setResolution(300, 300);
//
//
//
//            $im->readImage($basePath . $temp . $id . '.pdf['.$i.']');
//            $im->setImageFormat('png8');
//            if ($im->getImageAlphaChannel()) {
//                $im->setImageBackgroundColor('white');
//                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//            }
//
//            if ($im->getImageWidth() > $im->getImageHeight())
//                $im->rotateImage(new ImagickPixel(), 90);
//
//            $im->trimImage(0);
//            $im->stripImage();
//
//            $labels = [ $im->getImageBlob() ];
//            $return[] = GlobalOperatorResponse::createSuccessResponse($labels,  (string) $item->trackingNumber[$i]);
//
//            @unlink($basePath . $temp . $id . '.pdf');
//
//
//            return $return;
//
//        } else {
//
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse(print_r($resp->errors->description,1));
//            } else {
//                return false;
//            }
//
//        }
//    }

    protected static function _ownExtractNumber($text, $REG_PATTERN, $defaultValue = '0')
    {
        $building = '';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[0][0];

        $building = trim($building);

        if(mb_strlen($building) > 5)
        {
            // max length is 5
            // if it's longer, use older, more choosy rexexp
            $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';
            if(preg_match_all($REG_PATTERN, $text, $result2))
                $building = $result2[2][0];

            $building = trim($building);
        }

        if(mb_strlen($building) > 5)
        {
            $building = preg_split( "/(\/|\-|&)/", $building);
            $building = $building[0];
        }

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }

    public static function addressSplit($addressLine)
    {
        $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-\&]+)((\s)+|$))*/i';

        $text = str_replace([' - ', '  ',' & ', ' / '],['-', ' ','&', '/'], $addressLine);

        $house = self::_ownExtractNumber($text, $REG_PATTERN);

        if(preg_match('/(FLAT '.str_replace('/','\/', $house).')/i', $text))
        {
            $tempItem = str_ireplace('FLAT '.$house, '', $text);
            if(preg_match_all($REG_PATTERN, $tempItem, $result2))
            {
                $house = $result2[2][0];
                $house = str_replace(['/', '.'], '', $house);
            }
        }

        $temp = explode($house, $text);

        $addit = '';
        if(S_Useful::sizeof($temp) > 2)
        {
            $i0 = array_shift($temp);
            $i1 = implode($house, $temp);

            $temp[0] = $i0;
            $temp[1] = $i1;
        }

        foreach ($temp AS $key => $temp2)
        {
            $temp[$key] = trim($temp2);

            if($temp[$key] == '')
                unset($temp[$key]);
        }
        // reset indexes
        $temp = array_values($temp);

        if(S_Useful::sizeof($temp) == 1)
            $street = $temp[0];
        else {
            if(preg_match('/(FLAT|APARTMENT|APT|UNIT)/i', $temp[0]))
            {
                $street = $temp[1];
                $addit = $temp[0];
            } else {
                $street = $temp[0];
                $addit = $temp[1];
            }
        }

        if($street == '')
            $street = $text;

        return [$street, $house, $addit];
    }


    public static function _addressLinesMerge($address_line_1, $address_line_2)
    {
        if($address_line_1 != $address_line_2)
            $text = trim($address_line_1.' '.$address_line_2);
        else
            $text = $address_line_1;

        return $text;
    }

    public function createShipment($returnError = false)
    {
//        if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse('Not active!');
//            } else {
//                return false;
//            }

        $model = new self;

        $data = new stdClass();
        $data->CustomerOrderNumber = 'SP'.$this->ref;
        $data->ShippingMethodCode = 'DEDHLV1';
        $data->TrackingNumber = '';
        $data->Length = $this->package_size_l;
        $data->Width = $this->package_size_w;
        $data->Height = $this->package_size_d;
        $data->PackageCount = 1;
        $data->Weight = $this->package_weight;


        list($senderStreet, $senderStreetNo, $senderStreetAddit) = self::addressSplit(self::_addressLinesMerge($this->sender_address1, $this->sender_address2));
        list($receiverStreet, $receiverStreetNo, $receiverStreetAddit) = self::addressSplit(self::_addressLinesMerge($this->receiver_address1, $this->receiver_address2));


        $receiver = new stdClass();
        $receiver->CountryCode = $this->receiver_country;
        $receiver->FirstName = $this->receiver_name;
        $receiver->LastName = '';
        $receiver->Company = $this->receiver_company;
        $receiver->Street = $receiverStreet;
        $receiver->StreetAddress1 = $receiverStreetAddit;
        $receiver->StreetAddress2 = '';
        $receiver->City = $this->receiver_city;
        $receiver->State = '';
        $receiver->Zip = $this->receiver_zip_code;
        $receiver->Phone = $this->receiver_tel;
        $receiver->HouseNumber = $receiverStreetNo;
        $receiver->Email = $this->receiver_mail;
        $data->Receiver = $receiver;

//        $sender = new stdClass();
//        $sender->CountryCode = $this->sender_country;
//        $sender->FirstName = $this->sender_name;
//        $sender->LastName = '';
//        $sender->Company = $this->sender_company;
//        $sender->Street = trim($senderStreet.' '.$senderStreetAddit);
//        $sender->City = $this->sender_city;
//        $sender->State = '';
//        $sender->Zip = $this->sender_zip_code;
//        $sender->Phone = $this->sender_tel;
//        $sender->HouseNumber = $senderStreetNo;

        $sender = new stdClass();
        $sender->CountryCode = 'DE';

        $sender->Company = $this->sender_company;
        $sender->FirstName = $this->sender_name;

        $sender->LastName = '';

        $sender->Street = 'Hessenring';
        $sender->City = 'Mörfelden-Walldorf';
        $sender->State = '';
        $sender->Zip = '64546';
        $sender->Phone = '221221218';
        $sender->HouseNumber = '25';

        $data->Sender = $sender;
        $data->ApplicationType = 4; // Other
        $data->IsReturn = 0;

        $infos = new stdClass();
        $infos->EName = $this->package_content;
        $infos->Quantity = 1;
        $infos->UnitPrice = $this->package_value; // USD
        $infos->UnitWeight = $this->package_weight;

        $data->Parcels = [$infos, $infos];


        $resp = $model->_call('api/WayBill/CreateOrder', [$data]);

        if($resp->Item[0]->Success == 0)
        {
            if ($returnError == true) {

                $error = $resp->Item[0]->Remark;


                return GlobalOperatorResponse::createErrorResponse($error != '' ? $error :  print_r($resp,1));
            } else {
                return false;
            }
        }

        $ttNo = $resp->Item[0]->TrackingNumber;
        $no = $resp->Item[0]->CustomerOrderNumber;

        if($no == '')
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse(print_r($resp,1));
            } else {
                return false;
            }
        }

        $data = [ $no ];

        $resp = $model->_call('api/label/print', $data);

        if($resp->Item[0]->OrderInfos[0]->Error != '')
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->Item[0]->OrderInfos[0]->Error);
            } else {
                return false;
            }
        }

        $label = file_get_contents($resp->Item[0]->Url);
        if($label == '')
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('Could not load label image.');
            } else {
                return false;
            }
        }


        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);



        $im->readImageBlob($label);
        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        if ($im->getImageWidth() > $im->getImageHeight())
            $im->rotateImage(new ImagickPixel(), 90);

        $im->cropImage($im->getImageWidth(), $im->getImageHeight() - 100, 0, 0);
        $im->trimImage(0);


        $text = $this->package_content;

        /* @11.05.2018 - special rule for customer ANWIS2*/
        if($this->user_id == 2671)
            $text = $this->label_annotation_ref;

//        $im->resizeImage($im->getImageWidth()+500,$im->getImageHeight(),Imagick::FILTER_CATROM, 1);

        /* @11.05.2018 - special rule for customer ANWIS2*/
        if($this->user_id == 2671)
            $text = $this->label_annotation_ref;

        $draw = new ImagickDraw();
        $draw->setFontSize(30 * 1);
        $im->annotateImage($draw, 420 * 1, 230, 0,'ref: '.$text);


        if(1) {

//            $draw->setFontSize(30 * 1);
            $draw->setTextUnderColor(new ImagickPixel('white'));
            $draw->setFontSize(40 * 1);

            $name_addition = '';

            if($this->custom_sender_name_addition)
                $name_addition = ' / '.$this->custom_sender_name_addition;

            $im->annotateImage($draw, 110, 280 * 1, 0, str_pad('SP Parcel Distribution'.$name_addition, 50, ' '));
        }


        $im->stripImage();

        $labels = [ $im->getImageBlob() ];
        $return[] = GlobalOperatorResponse::createSuccessResponse($labels, $ttNo);


        return $return;
    }

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;
        $model->login = $courierInternal->courier->user->login;

        $model->user_id = $courierInternal->courier->user_id;
//        $model->setService($uniq_id);

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;

        /* @11.05.2018 - special rule for customer ANWIS*/
        if($courierInternal->courier->user_id == 2671) {
            $from->name = $courierInternal->courier->senderAddressData->name;
            $from->tel = $courierInternal->courier->senderAddressData->tel;
            $model->label_annotation_ref = $courierInternal->courier->ref;
        }

        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierInternal->courier->getWeight(true);

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;
        $model->package_content = $courierInternal->courier->package_content;
        $model->ref = $courierInternal->courier->local_id;
        $model->package_value = $courierInternal->courier->getPackageValueConverted('USD');


//        $model->packages_number = $courierInternal->courier->packages_number;
        $model->custom_sender_name_addition = $courierInternal->courier->user->getYunExpressNameAddition();


        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;
        $model->login = $courierU->courier->user->login;

        $model->user_id = $courierU->courier->user_id;
        $model->setService($courierU->courierUOperator->uniq_id);

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->senderAddressData->name;
        $model->sender_company = $from->senderAddressData->company;

        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierU->courier->getWeight(true);

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;
        $model->package_content = $courierU->courier->package_content;
        $model->ref = $courierU->courier->local_id;
        $model->package_value = $courierU->courier->getPackageValueConverted('USD');


//        $model->packages_number = $courierU->courier->packages_number;
        $model->custom_sender_name_addition = $courierU->courier->user->getYunExpressNameAddition();

        return $model->createShipment($returnErrors);
    }


    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return self::_track($no);
    }


    protected static function _track($no)
    {

        $data = new stdClass();
        $data->OrderNumber = $no;

        $model = new self;
        $resp = $model->_call('api/Tracking/GetTrackInfo?OrderNumber='.$no, false);

        var_dump($resp);
        exit;

        $statuses = new GlobalOperatorTtResponseCollection();

        if($resp->Item)
        {
            foreach($resp->Item->OrderTrackingDetails AS $event)
            {
                $name = (string) $event->ProcessContent;
                $date = (string) $event->ProcessDate;
                $date = mb_substr(str_replace('T', ' ', $date), 0, 19);
                $location = (string) $event->ProcessLocation;

                $statuses->addItem($name, $date, $location);
            }
        }

        return $statuses;
    }

    protected function _call($method, $data, $loginMethod = false)
    {

        $url = self::URL.'/'.$method;

        MyDump::dump('yun_express_synch.txt', 'REQ : '. $url.' : '.print_r(json_encode($data), 1));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        $array = [];
        $array[] = 'Content-Type: application/json';
        $array[] = 'Authorization: Basic ' . base64_encode(self::USERNAME.'&'.self::PASSWORD);
        $array[] = 'Accept: text/json';
        $array[] = 'Language: en';

        if($data) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $array);

        $response = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $error = curl_error($ch);

        MyDump::dump('yun_express_synch.txt', 'RESP : '. print_r($response, 1));
        MyDump::dump('yun_express_synch.txt', 'RESP CODE : '. print_r($httpcode, 1));
        MyDump::dump('yun_express_synch.txt', 'RESP ERROR : '. print_r($error, 1));

        $response = json_decode($response);

        return $response;

    }

    public static function runManifestQueque(array $products)
    {
        $header = [];
        $header[] = 'CustomerOrderNumber';
        $header[] = 'ShipmentsID';
        $header[] = 'x';
        $header[] = 'Date';
        $header[] = 'Long(cm)';
        $header[] = 'Width(cm)';
        $header[] = 'Height(cm)';
        $header[] = 'Weight(kg)';
        $header[] = 'x';
        $header[] = 'x';

        $data = [];
        $data[] = $header;

        if(sizeof($products)) {
            /* @var $products UniversalProduct[] */
            foreach ($products AS $product) {

                $data[] = [
                    $product->local_id,
                    $product->remote_id,
                    '',
                    $product->date_entered,
                    $product->size_l,
                    $product->size_d,
                    $product->size_w,
                    $product->weight,
                    '',
                    '',
                ];
            }

            $filename = 'manifest_' . date('Ymd') . '.xlsx';
            $xls = S_XmlGenerator::generateXml($data, $filename, true);

            $to = [];
            $to[] = 'zhao.weile@miworldtech.com';
            $to[] = '461438622@qq.com';
            $to[] = 'pkocon@kaabnl.nl';
            $to[] = 'abokhorst@kaabnl.nl';
            $to[] = 'jankopec@swiatprzesylek.pl';

            S_Mailer::send($to, $to, 'SP Manifest : '.date('Y-m-d H:i:s'), 'In attachment', false, NULL, NULL, true, false, $filename, false, false, false, $xls);
        }

        return true;
    }

}