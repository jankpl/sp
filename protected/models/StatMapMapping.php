<?php

Yii::import('application.models._base.BaseStatMapMapping');

class StatMapMapping extends BaseStatMapMapping
{

    const TYPE_COURIER = 1;
    const TYPE_POSTAL = 2;


    public static function getTypeList()
    {
        return [
            self::TYPE_COURIER => 'Courier',
            self::TYPE_POSTAL => 'Postal',
        ];
    }

    public static function getTypeName($type)
    {
        return self::getTypeList()[$type];
    }

    public function getName()
    {
        return StatMap::getMapNameById($this->map_id);
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function listStatsNotMapped($type)
    {
        if($type == self::TYPE_COURIER)
        {
            $models = CourierStat::model()->findAllBySql('SELECT * FROM courier_stat WHERE id NOT IN (SELECT stat_id FROM stat_map_mapping WHERE type = '.self::TYPE_COURIER.')');
        } else if($type == self::TYPE_POSTAL) {
            $models = PostalStat::model()->findAllBySql('SELECT * FROM postal_stat WHERE id NOT IN (SELECT stat_id FROM stat_map_mapping WHERE type = '.self::TYPE_POSTAL.')');
        }
        else
            return false;

        return $models;
    }

    public static function countItemsNotMapper($type)
    {

        $cmd = Yii::app()->db->createCommand();


        if($type == self::TYPE_COURIER)
        {
            return $cmd->select('COUNT(*)')->from('courier')->where('stat_map_id IS NULL')->queryScalar();
        } else if($type == self::TYPE_POSTAL) {
            return $cmd->select('COUNT(*)')->from('postal')->where('stat_map_id IS NULL')->queryScalar();
        }
        else
            return false;
    }

    public static function numberOfStatsForMap($map_id, $type)
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->select('COUNT(*)')->from('stat_map_mapping')->where('type = :type AND map_id = :map_id', [':type' => $type, ':map_id' => $map_id])->queryScalar();
    }

    public function beforeSave()
    {
        if(Yii::app()->params['backend'] && !Yii::app()->user->isGuest)
            $this->admin_id = Yii::app()->user->id;

        parent::beforeSave();
    }

    protected static $_mapForStat = [];

    /**
     * Returns map id or false if not found
     * @param $stat_id
     * @param $type
     * @return integer|boolean
     */
    public static function findMapForStat($stat_id, $type)
    {

        if(!is_array(self::$_mapForStat[$type]))
            self::$_mapForStat[$type] = [];

        if(!isset($_mapForStat[$type][$stat_id]))
        {
            $cmd = Yii::app()->db->createCommand();
            $_mapForStat[$type][$stat_id] = $cmd->select('map_id')->from('stat_map_mapping')->where('type = :type AND stat_id = :stat_id', [':type' => $type, ':stat_id' => $stat_id])->queryScalar();
        }

        return $_mapForStat[$type][$stat_id];
    }

    /**
     * Returns stat model by name
     * @param $name
     * @param $type
     * @return CourierStat|PostalStat|null
     */
    public static function getStatByName($name, $type)
    {

        if($type == self::TYPE_COURIER)
        {
            $model = CourierStat::model();
        } else if($type == self::TYPE_POSTAL) {
            $model = PostalStat::model();
        }
        else
            return false;

        return $model->findByAttributes(['name' => $name]);
    }

    public static function logToFile($id, $map_id, $type, $stat_id = NULL, $numberOfSmsesByTheTime = '')
    {
        $externalService = '';
        if($type == self::TYPE_COURIER && $stat_id)
        {
            $cs = CourierStat::model()->cache(60*60)->findByPk($stat_id);

            if($cs)
                $externalService = CourierStat::getExternalServiceNameById($cs->external_service_id);
        }
        else if($type == self::TYPE_POSTAL && $stat_id)
        {
            $cs = PostalStat::model()->cache(60*60)->findByPk($stat_id);

            if($cs)
                $externalService = PostalStat::getExternalServiceNameById($cs->operator_source_id);
        }


        $path = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' .DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'stat_map_logs' . DIRECTORY_SEPARATOR;
        $file = 'mapping_'.date('Ymd').'.txt';

        $line = $map_id.'|'.$id.'|'.date('YmdHis').'|'.$type.'|'.$externalService.'|'.$numberOfSmsesByTheTime."\r\n";

        return @file_put_contents($path.$file, $line, FILE_APPEND);
    }


    public static function clearAbandonedMappings($type)
    {
        if($type == self::TYPE_COURIER)
            $tableName = 'courier_stat';
        else if($type == self::TYPE_POSTAL)
            $tableName = 'postal_stat';
        else
            throw new Exception('Unknown type!');

        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        return $cmd->delete('stat_map_mapping', 'type = :type AND stat_id NOT IN (SELECT id FROM '.$tableName.')', [':type' => $type]);
    }

}