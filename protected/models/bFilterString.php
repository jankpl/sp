<?php

class bFilterString extends bBaseBehavior
{
    public function filterStripTags($value)
    {
        $text = $value;
        $text = trim($text);
        $text = strip_tags($text);
        // remove ASCII control characters:
        $text = preg_replace('/[\x1C\x1D\x1E\x1F]/', '', $text);

        return $text;
    }
}