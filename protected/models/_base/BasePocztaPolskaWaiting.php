<?php

/**
 * This is the model base class for the table "poczta_polska_waiting".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PocztaPolskaWaiting".
 *
 * Columns in table "poczta_polska_waiting" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property integer $type
 * @property integer $type_id
 * @property integer $pp_account_id
 * @property string $guid
 * @property string $remote_id
 * @property integer $envelope_id
 * @property string $envelope_name
 * @property string $date_entered
 * @property string $date_sent
 * @property string $log
 * @property string $local_id
 * @property string $date_posting
  *
 */
abstract class BasePocztaPolskaWaiting extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'poczta_polska_waiting';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'PocztaPolskaWaiting|PocztaPolskaWaitings', $n);
	}

	public static function representingColumn() {
		return 'guid';
	}

	public function rules() {
		return array(
			array('type, type_id, guid, remote_id, envelope_id, date_entered', 'required'),
			array('type, type_id, envelope_id', 'numerical', 'integerOnly'=>true),
			array('guid, remote_id', 'length', 'max'=>64),
			array('date_sent', 'safe'),
			array('date_sent', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, type, type_id, guid, remote_id, envelope_id, date_entered, date_sent, date_posting', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'type' => Yii::t('app', 'Type'),
			'type_id' => Yii::t('app', 'Type'),
			'pp_account_id' => Yii::t('app', 'PP Account'),
			'guid' => Yii::t('app', 'Guid'),
			'remote_id' => Yii::t('app', 'Remote'),
			'envelope_id' => Yii::t('app', 'Envelope Id'),
			'envelope_name' => Yii::t('app', 'Envelope Name'),
			'date_entered' => Yii::t('app', 'Date Entered'),
			'date_updated' => Yii::t('app', 'Date Updated'),
			'date_sent' => Yii::t('app', 'Date Sent'),
			'log' => Yii::t('app', 'Log'),
			'local_id' => Yii::t('app', 'Local id'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('type', $this->type);
		$criteria->compare('type_id', $this->type_id);
		$criteria->compare('guid', $this->guid, true);
		$criteria->compare('remote_id', $this->remote_id, true);
		$criteria->compare('envelope_id', $this->envelope_id);
		$criteria->compare('envelope_name', $this->envelope_name, true);
		$criteria->compare('date_entered', $this->date_entered, true);
		$criteria->compare('date_updated', $this->date_updated, true);
		$criteria->compare('date_sent', $this->date_sent, true);
		$criteria->compare('log', $this->log, true);
		$criteria->compare('local_id', $this->local_id, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}