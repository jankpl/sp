<?php

class postalModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

        $this->defaultController = 'postal';

		// import the module-level models and components
		$this->setImport(array(
			'postal.models.*',
			'postal.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
            if(!Yii::app()->user->isGuest && Yii::app()->user->model->getDebtBlockActive())
            {
                $controller->redirect(['/userInvoice']);
                exit;
            }
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
