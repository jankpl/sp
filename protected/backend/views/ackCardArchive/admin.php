<?php
/* @var $this UserDocumentsController */
/* @var $model UserDocuments */

$this->menu=array(
    array('label'=>'Create UserDocuments', 'url'=>array('create')),
);

?>

<h1>Manage Manifests/ACK Archive</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'user-message-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        [
            'name' => 'id',
            'htmlOptions' => [
                'width' => '30',
            ],
        ],
        'no',
        'date_entered',
        [

            'name' => 'user_id',
            'value' => '$data->user->login',
            'filter' => CHtml::listData(User::model()->findAll(),'id', 'login'),
        ],
        [
            'class'=>'CLinkColumn',
            'label' => 'Get PDF',
            'urlExpression'=>'Yii::app()->createUrl("/ackCardArchive/getPdf", ["id" => $data->id])',
        ],
        [
            'class'=>'CLinkColumn',
            'label' => 'Get XLS',
            'urlExpression'=>'Yii::app()->createUrl("ackCardArchive/getXLS", ["id" => $data->id])',
        ],
        [
            'name' => 'items',
        ],
    ),
)); ?>
