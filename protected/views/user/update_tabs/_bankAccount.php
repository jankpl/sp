<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'bank-account',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'bank']),
    ));
    ?>

    <h3><?php echo Yii::t('user', 'Rachunek bankowy');?></h3>

    <p><?php echo Yii::t('user', 'Możesz podać dane racuhnku bankowego, na który będą przychodziły przelewy z paczek typu COD');?></p>


    <table class="table">
        <tr>
            <td><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'saveBankAccount'));?></td>
            <td><?php echo $form->textField($model, 'bankAccount', array('style' => 'width: 500px;', 'max-length' => 50)); ?></td>
        </tr>
    </table>

    <?php
    $this->endWidget();
    ?>

</div>
