<div style="width: 49%; float: left;" class="item">

    <div style="text-align: right;"><input type="button" class="" value="Show current values" data-old-show="true" style="cursor: alias;"></div>
    <div style="overflow: visible; position: relative;">
        <?php
        $item = $models;
        $itemOld = $modelsOld;
        ?>

        <div style="position: absolute; top: 0; left: 0; right: 0; background: white; border: 1px solid gray; -webkit-box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.75); -moz-box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.75); box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.75); padding: 5px; display: none;" data-old="true">
               <?php
            $this->renderPartial('import_item_item', [
                'form' => $form,
                'item' => $itemOld,
            ]);
            ?>
        </div>

        <?php
        $this->renderPartial('import_item_item', [
            'item' => $item,
            'form' => $form,
            'error' => &$error,
        ]);
        ?>
    </div>
</div>
