
<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
    ));
    ?>


    <div style="overflow: auto; width: 700px; margin: 0 auto;">

        <div style="float: left; width: 48%;">
            <h3>Account data</h3>

            <?php echo $form->errorSummary($model); ?>
            <div class="row">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model, 'email', array('maxlength' => 64)); ?>
                <?php echo $form->error($model,'email'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'pass'); ?>
                <?php echo $form->passwordField($model, 'pass', array('maxlength' => 64)); ?>
                <?php echo $form->error($model,'pass'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'passCompare'); ?>
                <?php echo $form->passwordField($model, 'passCompare', array('maxlength' => 64)); ?>
                <?php echo $form->error($model,'passCompare'); ?>
            </div><!-- row -->

            <div class="row">
                <?php echo $form->labelEx($model,'source_domain'); ?>
                <?php echo $form->dropDownList($model, 'source_domain',User::sourceDomainList()); ?>
                <?php echo $form->error($model,'source_domain'); ?>
            </div><!-- row -->


            <?php
            if($model->verifyStatus() == User::STATUS_NEW):
                ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'price_currency_id'); ?>
                    <?php echo $form->dropDownList($model, 'price_currency_id',CHtml::listData(PriceCurrency::model()->findAll(),'id','symbol')); ?>
                    <?php echo $form->error($model,'price_currency_id'); ?>
                </div><!-- row -->
            <?php
            else:
                ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'price_currency_id'); ?>
                    <?php echo $form->dropDownList($model, 'price_currency_id',CHtml::listData(PriceCurrency::model()->findAll(),'id','symbol'), ['disabled' => true]); ?>
                    <?php echo $form->error($model,'price_currency_id'); ?>
                    <?php echo $form->hiddenField($model, 'price_currency_id'); ?>
                </div><!-- row -->
            <?php
            endif;
            ?>
            <div class="row">
                <?php echo $form->labelEx($model,'salesman_id'); ?>
                <?php echo $form->dropDownList($model, 'salesman_id',CHtml::listData(Salesman::model()->findAll(),'id','name'), ['prompt' => '-']); ?>
                <?php echo $form->error($model,'salesman_id'); ?>
            </div><!-- row -->

            <div class="row">
                <?php echo $form->labelEx($model,'injection_sp_point_id'); ?>
                <?php echo $form->dropDownList($model, 'injection_sp_point_id',CHtml::listData(SpPoints::model()->findAll(),'id','nameWithId'), ['prompt' => '-']); ?>
                <?php echo $form->error($model,'injection_sp_point_id'); ?>
            </div><!-- row -->

            <div class="note">Leave both password fields empty if you don't want to change password.</div>

        </div>
        <div style="float: right; width: 48%;">
            <h3>Personal data</h3>

            <div class="row">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
                <?php echo $form->error($model,'name'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'surname'); ?>
                <?php echo $form->textField($model, 'surname', array('maxlength' => 45)); ?>
                <?php echo $form->error($model,'surname'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'company'); ?>
                <?php echo $form->textField($model, 'company', array('maxlength' => 45)); ?>
                <?php echo $form->error($model,'company'); ?>
            </div><!-- row -->
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->dropDownList($model, 'type', CHtml::listData(
                array(
                    array('value' => User::TYPE_PRIVATE, 'name' => 'Private'),
                    array('value' => User::TYPE_COMPANY, 'name' => 'Company')
                ),'value','name')); ?>
            <?php echo $form->error($model,'type'); ?>
            <div class="row">
                <?php echo $form->labelEx($model,'country_id'); ?>
                <?php echo $form->dropDownList($model, 'country_id', GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true))); ?>
                <?php echo $form->error($model,'country_id'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'city'); ?>
                <?php echo $form->textField($model, 'city', array('maxlength' => 45)); ?>
                <?php echo $form->error($model,'city'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'zip_code'); ?>
                <?php echo $form->textField($model, 'zip_code', array('maxlength' => 10)); ?>
                <?php echo $form->error($model,'zip_code'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'address_line_1'); ?>
                <?php echo $form->textField($model, 'address_line_1', array('maxlength' => 45)); ?>
                <?php echo $form->error($model,'address_line_1'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'address_line_2'); ?>
                <?php echo $form->textField($model, 'address_line_2', array('maxlength' => 45)); ?>
                <?php echo $form->error($model,'address_line_2'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'tel'); ?>
                <?php echo $form->textField($model, 'tel', array('maxlength' => 20)); ?>
                <?php echo $form->error($model,'tel'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model,'nip'); ?>
                <?php echo $form->textField($model, 'nip', array('maxlength' => 13, 'readonly' => $model->verifyStatus() == User::STATUS_NEW ? false : true)); ?>
                <?php echo $form->error($model,'nip'); ?>
            </div><!-- row -->


            <p class="note">
                <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
            </p>

        </div>
    </div>

    <div style="text-align: center;">
        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Save'), ['name' => 'saveUserModel']);
        $this->endWidget();
        ?>
    </div>


</div><!-- form -->