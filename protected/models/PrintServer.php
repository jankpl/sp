<?php

Yii::import('application.models._base.BasePrintServer');

use Zebra\Zpl\Image;
use Zebra\Zpl\Builder;

class PrintServer extends BasePrintServer
{
    const IS_LIVE = true;
    const IS_WINDOWS_LOCAL_ENV = false;

    const ASYNC_MODE = true; // if true, print server will be called directly after adding item to queque

    const STAT_NEW = 0;
    const STAT_ACTIVE = 1;
    const STAT_DISABLED = 3;
    const STAT_LOCKED = 5;
    const STAT_DELETED = 9;

    const MAX_TRIES_ACTIVATION = 5;

    const ZEBRA_PAPER_WIDTH = 810;
    const ZEBRA_PAPER_HEIGHT = 1215;

    const MAX_PRINT_SERVERS = 3;

    public static function getStatList()
    {
        return [
            self::STAT_NEW => Yii::t('printServer', 'Nowy'),
            self::STAT_ACTIVE => Yii::t('printServer', 'Aktywny'),
            self::STAT_DISABLED => Yii::t('printServer', 'Wyłączony'),
            self::STAT_LOCKED => Yii::t('printServer', 'Zablokowany'),
            self::STAT_DELETED => Yii::t('printServer', 'Usunięty'),
        ];
    }

    public function getStatName()
    {
        return isset(self::getStatList()[$this->stat]) ? self::getStatList()[$this->stat] : false;
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    protected static $_address = [];
    public static function getAddressForUser(User $user, $no = false)
    {
        $user_id = $user->id;

        if(!$user->getPrintServerActive())
            return false;

        if(!isset(self::$_address[$user_id]))
            self::$_address[$user_id] = [];

        if(!isset(self::$_address[$user_id][$no]))
        {
            /* @var $cmd CDbCommand */
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('address_ip, address_port');
            $cmd->from((new self)->tableName());
            $cmd->where('user_id = :user_id AND stat = :stat', [':stat' => self::STAT_ACTIVE, ':user_id' => $user_id]);

            if(!$no)
                $cmd->andWhere('is_default = 1');
            else
                $cmd->andWhere('no = :no', [':no' => $no]);

            $result = $cmd->queryRow();

            if($result)
                $address = self::ipAndPortToAddress($result['address_ip'], $result['address_port']);
            else
                $address = false;

            self::$_address[$user_id][$no] = $address;
        }

        return self::$_address[$user_id][$no];
    }

    public function getFullAddress()
    {
        return self::ipAndPortToAddress($this->address_ip, $this->address_port);
    }

    public function checkIp($attribute,$params)
    {
        if($this->$attribute != '') {
            if (filter_var($this->$attribute, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) === false)
                $this->addError($attribute, Yii::t('printServer', 'Podaj poprawny adres IPv4!'));
            else if (in_array($this->$attribute, ['127.0.0.1', '85.10.197.46']))
                $this->addError($attribute, Yii::t('printServer', 'Ten adres IP nie jest dozwolony!'));
        }
    }


    public function rules() {
        return array(
            array('address_port', 'numerical', 'min' => 1000, 'max' => 50000),
            array('address_ip', 'checkIp'),

            array('address_ip, address_port', 'required'),
            array('user_id, stat, no, is_default', 'numerical', 'integerOnly'=>true),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('verify_counter', 'default', 'setOnEmpty' => true, 'value' => 0),
            array('id, date_entered, date_updated, user_id, stat, address_ip, address_port, verify_key, verify_counter, no, is_default', 'safe', 'on'=>'search'),
        );
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function afterValidate()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from((new self)->tableName());
        $cmd->where('address_ip = :address_ip AND user_id = :user_id AND ((verify_counter >= :limit AND stat = :stat) OR stat = :stat_locked)', [
            ':address_ip' => $this->address_ip,
            ':limit' => self::MAX_TRIES_ACTIVATION,
            ':user_id' => Yii::app()->user->id,
            ':stat' => self::STAT_DELETED,
            ':stat_locked' => self::STAT_LOCKED,
        ]);
        $result = $cmd->queryScalar();


        if(!self::isPossibleToAddNew())
            $this->addError('address_ip', Yii::t('printServer', 'Maksymalna liczba Print Serwerów to:').' '.self::MAX_PRINT_SERVERS);

        if($result)
        {
            if ($result)
                $this->addError('address_ip', Yii::t('printServer', 'Ten adres IP jest zablokowany!'));
        }
    }

    public function beforeSave()
    {
        if($this->isNewRecord)
        {
            $length = 10;
            $this->verify_key = substr(str_shuffle(str_repeat($x='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
            $this->user_id = Yii::app()->user->id;

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('verify_counter');
            $cmd->from((new self)->tableName());
            $cmd->where('address_ip = :address_ip AND user_id = :user_id', [
                ':address_ip' => $this->address_ip,
                ':user_id' => Yii::app()->user->id,
            ]);
            $result = $cmd->queryScalar();

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('MAX(no)');
            $cmd->from((new self)->tableName());
            $cmd->where('user_id = :user_id', [
                ':user_id' => Yii::app()->user->id,
            ]);
            $no = $cmd->queryScalar();
            $no++;
            $this->no = $no;

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from((new self)->tableName());
            $cmd->where('user_id = :user_id AND (stat = :stat OR stat =:stat2 OR stat = :stat3)', [
                ':user_id' => Yii::app()->user->id,
                ':stat' => self::STAT_ACTIVE,
                ':stat2' => self::STAT_NEW,
                ':stat3' => self::STAT_DISABLED,
            ]);
            $count = $cmd->queryScalar();

            if(!$count)
                $this->is_default = true;

            $this->verify_counter = $result;
        }

        return parent::beforeSave();
    }

    public static function ipAndPortToAddress($ip, $port)
    {
        $address = $ip.':'.$port;

        if(self::IS_WINDOWS_LOCAL_ENV)
            $address = '\\\\Janek-HP-14\zebra';

        return $address;
    }

    public static function stringImgToZpl($imgString)
    {
        require_once(Yii::getPathOfAlias('application.vendor.zebra.src.Contracts.Zpl').'/Image.php');
        require_once(Yii::getPathOfAlias('application.vendor.zebra.src.Zpl').'/Builder.php');
        require_once(Yii::getPathOfAlias('application.vendor.zebra.src.Zpl').'/Image.php');


        $image = new Image($imgString);
        $zpl = new Builder();
        $zpl->gf($image);
        return $zpl->toZpl();
    }

    public function isResendPossible()
    {
        if($this->verify_counter > self::MAX_TRIES_ACTIVATION)
            return false;
        else
            return true;
    }

    public function sendVerification()
    {
        $this->verify_counter++;

        if(!$this->isResendPossible()) {
            $this->lock();
            return false;
        }

        $this->update(['verify_counter', 'date_updated']);

        return Yii::app()->PrintServerQueque->scheudleItem(PrintServerQueque::TYPE_VERIFICATION, $this->id, $this->user_id, $this->getFullAddress());
    }

    public function generateVerificationMessage()
    {
        return '^XA
                ^LH 10,10
                ^FO20,10
                ^ADN,20,10
                ^FDswiatprzesylek.pl^FS
 ^FO20,80
                ^ADN,40,20
                ^FDCODE:^FS
^FO20,130
                ^ADN,60,30
                ^FD'.$this->verify_key.'^FS
                ^LH0,0
                ^XZ';
    }

    public static function sendToPrinter($data, $address, $throwErrors = false)
    {
        MyDump::dump('printServer.txt', print_r($address,1).' : '. print_r($data,1));

        if(self::IS_WINDOWS_LOCAL_ENV) {
            // FOR LOCAL ADDRESSES-NAMES (in Windows):

//            $address = '\\zebra';

            $fp = @fopen($address, "w");
            if (!$fp){
                if($throwErrors)
                    throw new Exception('Connection failed!');
                else
                    return false;
            }

            if (!fwrite($fp,$data)){
                if($throwErrors)
                    throw new Exception('Connection failed!');
                else
                    return false;
            }

            @fclose($fp);
//
        } else {

            if(!self::IS_LIVE)
                return true;

            // FOR REAL IP:
            $address = explode(':', $address);
            $port = $address[1];
            $ip = $address[0];

            $socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            if (!@socket_connect($socket, $ip, $port))
                if ($throwErrors)
                    throw new Exception('Connection failed!');
                else
                    return false;

            if (!@socket_write($socket, $data, strlen($data)))
                if ($throwErrors)
                    throw new Exception('Write failed!');
                else
                    return false;

            if ($socket)
                socket_close($socket);
        }

        return true;
    }



    public static function newCourierLabelNotify(CourierLabelNew $courierLabelNew, $withImmidiateCall = self::ASYNC_MODE)
    {
        if($courierLabelNew->stat == CourierLabelNew::STAT_SUCCESS OR $courierLabelNew->courier->user_id == User::USER_4VALUES)
        {
            if($courierLabelNew->courier->user_id !== NULL)
            {
                $no = false;
                if($courierLabelNew->courier->_print_server_no)
                    $no = $courierLabelNew->courier->_print_server_no;

                $address = self::getAddressForUser($courierLabelNew->courier->user, $no);

                if($address)
                {
                    $userAllowExtendedLabelGeneration = $courierLabelNew->courier->user->getAllowCourierInternalExtendedLabel();
                    $type = $courierLabelNew->courier->getType();

                    if($type == Courier::TYPE_INTERNAL) {
                        if ($courierLabelNew->_internal_mode == CourierLabelNew::INTERNAL_MODE_FIRST) {
                            if (!$userAllowExtendedLabelGeneration) {
                                Yii::app()->PrintServerQueque->scheudleItem(PrintServerQueque::TYPE_COURIER, $courierLabelNew->id, $courierLabelNew->courier->user_id, $address);

                                if($withImmidiateCall)
                                    PrintServerQueque::asyncCallForId($courierLabelNew->id, PrintServerQueque::TYPE_COURIER);

                                return true;
                            }
                        } else if ($courierLabelNew->_internal_mode == CourierLabelNew::INTERNAL_MODE_SECOND) {
                            if ($userAllowExtendedLabelGeneration) {
                                Yii::app()->PrintServerQueque->scheudleItem(PrintServerQueque::TYPE_COURIER, $courierLabelNew->id, $courierLabelNew->courier->user_id, $address);

                                if($withImmidiateCall)
                                    PrintServerQueque::asyncCallForId($courierLabelNew->id, PrintServerQueque::TYPE_COURIER);

                                return true;
                            }
                        } else if ($courierLabelNew->_internal_mode == CourierLabelNew::INTERNAL_MODE_COMMON) {
                            Yii::app()->PrintServerQueque->scheudleItem(PrintServerQueque::TYPE_COURIER, $courierLabelNew->id, $courierLabelNew->courier->user_id, $address);

                            if($withImmidiateCall)
                                PrintServerQueque::asyncCallForId($courierLabelNew->id, PrintServerQueque::TYPE_COURIER);

                            return true;
                        }
                    } else {
                        Yii::app()->PrintServerQueque->scheudleItem(PrintServerQueque::TYPE_COURIER, $courierLabelNew->id, $courierLabelNew->courier->user_id, $address);

                        if($withImmidiateCall)
                            PrintServerQueque::asyncCallForId($courierLabelNew->id, PrintServerQueque::TYPE_COURIER);

                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function delete()
    {
        $this->stat = self::STAT_DELETED;
        return $this->update(['stat', 'date_updated']);
    }

    public function deletePermanently()
    {
        return parent::delete();
    }


    public function resetCounter()
    {
        $this->verify_counter = 0;
        return $this->update(['verify_counter', 'date_updated']);
    }

    public function lock()
    {
        $this->stat = self::STAT_LOCKED;
        return $this->update(['stat', 'date_updated']);
    }

    public function toggleStat()
    {
        if($this->stat == self::STAT_ACTIVE)
            $this->stat = self::STAT_DISABLED;
        else if($this->stat == self::STAT_DISABLED)
            $this->stat = self::STAT_ACTIVE;
        else
            return false;

        return $this->update(['stat', 'date_updated']);
    }

    public function verify($code)
    {
        if($this->verify_key == $code)
        {
            $this->activate();
            return true;
        }

        return false;
    }

    public function activate()
    {
        $this->verify_counter = 0;
        $this->stat = self::STAT_ACTIVE;

        return $this->update(['verify_counter', 'stat', 'date_updated']);
    }

    public static function numberOfPsForUser($user_id = NULL)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        $cmd = Yii::app()->db->createCommand();
        $result = $cmd->select('COUNT(*)')->from((new PrintServer())->tableName())->where('user_id = :user_id AND stat != :stat AND stat != :stat2', [
            ':user_id' => $user_id,
            ':stat' => PrintServer::STAT_DELETED,
            ':stat2' => PrintServer::STAT_LOCKED,
        ])->queryScalar();

        return $result;
    }

    public static function isPossibleToAddNew($user_id = NULL)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        return (self::numberOfPsForUser($user_id) < self::MAX_PRINT_SERVERS);
    }

    public function setAsDefault()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->update((new self)->tableName(),
            ['is_default' => 0],
            'user_id = :user_id',
            [':user_id' => $this->user_id]
        );

        $this->is_default = true;

        return $this->update('is_default');
    }


    public static function isValidPrinterForUser($user_id, $no)
    {
        $cmd = Yii::app()->db->createCommand();
        $result = $cmd->select('COUNT(*)')->from((new self)->tableName())->where('user_id = :user_id AND no = :no AND stat = :stat', [
            ':user_id' => $user_id,
            ':no' => $no,
            ':stat' => self::STAT_ACTIVE,
        ])->queryScalar();

        return $result;
    }
}