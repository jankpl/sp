<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);


?>
<h2><?php echo Yii::t('app', 'Krok {step}', array('{step}' => '3/3'));?> | <?php echo Yii::t('user','Podsumowanie');?></h2>

<?php
Yii::app()->clientScript->registerCss('label_th_width','
      table.detail-view th
      {
          width:200px;
      }
');
?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'nullDisplay' => '-',
    'attributes' => array(
        'login',
        'email',
        array(
            'name' => 'type',
            'value' => User::getTypeName($model->type),
        ),
    ),
)); ?>

<?php
if($model->type == User::TYPE_PRIVATE):
    $this->widget('bootstrap.widgets.TbDetailView', array(
        'data' => $model,
        'nullDisplay' => '-',
        'attributes' => array(
            'name',
            'surname',
        ),
    ));
else:
    $this->widget('bootstrap.widgets.TbDetailView', array(
        'data' => $model,
        'nullDisplay' => '-',
        'attributes' => array(
            'company',
        ),
    ));
endif;
?>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'nullDisplay' => '-',
    'attributes' => array(
        array(
            'label' => $model->getAttributeLabel('country'),
            'value' => $model->country0->name),
        'city',
        'zip_code',
        'address_line_1',
        'address_line_2',
        'tel',
        'nip',
        array(
            'name' => 'price_currency_id',
            'value' => User::getCurrency($model->price_currency_id),
        ),
    ),
)); ?>

<div class="form">



    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <div class="navigate">

        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step2'));
        echo TbHtml::submitButton(Yii::t('app','Zakończ'),array('name'=>'finish'));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->

