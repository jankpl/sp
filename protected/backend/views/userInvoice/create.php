<?php
/* @var $this UserInvoiceController */
/* @var $model UserInvoice */

$this->breadcrumbs=array(
	'User Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserInvoice', 'url'=>array('index')),
	array('label'=>'Manage UserInvoice', 'url'=>array('admin')),
);
?>

<h1>Create UserInvoice</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>