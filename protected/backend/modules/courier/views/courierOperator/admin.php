<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
	);

?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'courier-operator-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'date_entered',
		'date_updated',
		'name',
		'description',
        array(
            'name' => 'broker',
			'header' => 'Broker',
            'value' => 'CourierOperator::getBrokerList()[$data->broker]',
            'filter' => CourierOperator::getBrokerList(),
        ),
        [
            'name' => 'partner_id',
            'value' => 'Partners::getPartnerNameById($data->partner_id)',
            'filter' => Partners::getParntersList(),
        ],
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>