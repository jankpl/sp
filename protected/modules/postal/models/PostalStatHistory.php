<?php

Yii::import('application.modules.postal.models._base.BasePostalStatHistory');

class PostalStatHistory extends BasePostalStatHistory
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    const EXPIRATION_DAYS = 365;

    public function scopes()
    {
        return array(
            'visible'=>array(
                'condition'=> self::getTableAlias().'.hidden  = 0',
            ),
        );
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

            //array('status_date', 'type', 'type' => 'datetime', 'allowEmpty' => true, 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
            array('current_stat_id, postal_id', 'required'),
            array('previous_stat_id, current_stat_id', 'length', 'max' => 256),
            array('author', 'length', 'max'=>45),
            array('postal_id', 'length', 'max'=>10),
            array('location', 'length', 'max'=>128),
            array('previous_stat_id, author', 'default', 'setOnEmpty' => true, 'value' => null),
            array('hidden', 'default', 'setOnEmpty' => true, 'value' => 0),
            array('id, date_entered, previous_stat_id, current_stat_id, author, postal_id, status_date, hidden', 'safe', 'on'=>'search'),
        );
    }

    public function beforeSave()
    {

        if($this->status_date == '' OR $this->status_date == '0000-00-00 00:00:00')
            $this->status_date = date('Y-m-d H:i:s');

        $this->current_stat_name = PostalStat::getNameById($this->current_stat_id);

        if($this->previous_stat_id != '')
            $this->previous_stat_name = PostalStat::getNameById($this->previous_stat_id);

        return parent::beforeSave();
    }


    /**
     * Function returns last item date
     * @param bool|false $cutSeconds Whether to show seconds (false) or cut them (true)
     * @return string
     */
    public function getLastDate($cutSeconds = false)
    {
        $date = $this->status_date;

        if($cutSeconds)
            $date = substr($date,0,-3);

        return $date;
    }



    public static function expireOldHistory(Postal $item)
    {

        $item->archive += Postal::ARCHIVED_HISTORY;
        $item->update(['archive']);

        $cmd = Yii::app()->db->createCommand();
        $cmd->delete((new PostalStatHistory())->tableName(), 'postal_id = :postal_id', [':postal_id' => $item->id]);

    }
}