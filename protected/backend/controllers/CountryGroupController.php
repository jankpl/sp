<?php

class CountryGroupController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
             array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionListAllGroups() {

        $this->render('listAllGroups',array(
        ));

    }

    public function actionView($id) {

        $model = CountryGroup::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $this->render('view',array(
            'model' => $model,
        ));

    }

    public function actionCreate() {
        $model = new CountryGroup;

        $this->performAjaxValidation($model, 'country-group-form');

        if (isset($_POST['CountryGroup'])) {
            $model->setAttributes($_POST['CountryGroup']);

            $countryList = $_POST['countryList'];
            if(!is_array($countryList))
                $countryList = [];

            if ($model->saveWithCountries($countryList)) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'CountryGroup');

        $this->performAjaxValidation($model, 'country-group-form');

        if (isset($_POST['CountryGroup'])) {
            $model->setAttributes($_POST['CountryGroup']);

            $countryList = $_POST['countryList'];
            if(!is_array($countryList))
                $countryList = [];


            if ($model->saveWithCountries($countryList)) {
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {

        try
        {
            if (Yii::app()->getRequest()->getIsPostRequest()) {
                $this->loadModel($id, 'CountryGroup')->delete();

                if (!Yii::app()->getRequest()->getIsAjaxRequest())
                    $this->redirect(array('admin'));
            } else
                throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));

        }
        catch (Exception $ex)
        {
            throw new CHttpException(403,'Nie można usunąć tej pozycji!');
        }

    }

    public function actionIndex() {

        $this->redirect(array('countryGroup/admin'));
    }

    public function actionAdmin() {
        $model = new CountryGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['CountryGroup']))
            $model->setAttributes($_GET['CountryGroup']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}