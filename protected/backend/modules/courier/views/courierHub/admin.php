<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
	);

?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'courier-hub-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'name',
		'date_entered',
		'date_updated',
		'description',
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>