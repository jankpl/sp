<?php

class CountryListController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CountryList'),
        ));
    }

    public function actionCreate() {
        $model = new CountryList;
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = new CountryListTr;
            $modelTrs[$item->id]->language_id = $item->id;
        }

        $this->performAjaxValidation($model, 'country-list-form');

        if (isset($_POST['CountryList'])) {
            $model->setAttributes($_POST['CountryList']);

            $model->code = strtoupper($model->code);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            if($model->save())
            {
                foreach($_POST['CountryListTr'] AS $key => $item)
                {

                    if($modelTrs[$key] === null) $modelTrs[$key] = new CountryListTr;
                    $modelTrs[$key]->setAttributes($item);
                    $modelTrs[$key]->country_list_id = $model->id;

                    if(!$modelTrs[$key]->save())
                        $errors = true;
                }
            } else $errors = true;

            if (!$errors) {

                $transaction->commit();
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'CountryList');
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = CountryListTr::model()->find('country_list_id=:country_list_id AND language_id=:language_id', array(':country_list_id' => $model->id, ':language_id' => $item->id));
        }

        $this->performAjaxValidation($model, 'country-list-form');

        if (isset($_POST['CountryList'])) {
            $model->setAttributes($_POST['CountryList']);

            $model->code = strtoupper($model->code);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            foreach($_POST['CountryListTr'] AS $key => $item)
            {

                if($modelTrs[$key] === null) $modelTrs[$key] = new CountryListTr;
                $modelTrs[$key]->setAttributes($item);
                $modelTrs[$key]->country_list_id = $id;

                if(!$modelTrs[$key]->save())
                    $errors = true;

            }

            if ($model->save() && !$errors) {
                $transaction->commit();
                $this->redirect(array('admin',));
            }
        }
        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {

            try
            {
                $this->loadModel($id, 'CountryList')->delete();
            } catch (Exception $ex)
            {
                throw new CHttpException('403', 'Nie można usunąć tego państwa!');
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));

    }

    public function actionIndex() {

        $this->redirect(array('countryList/admin'));
    }

    public function actionAdmin() {
        $model = new CountryList('search');
        $model->unsetAttributes();

        if (isset($_GET['CountryList']))
            $model->setAttributes($_GET['CountryList']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionStat($id)
    {
        /* @var $model CountryList */


        $model = CountryList::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        else
            throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}