<?php Yii::app()->clientScript->registerCoreScript('jquery');?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.preventFormMultisubmit.js'); ?>

<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/panel/css/sb-admin-2.min.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/panel/vendor/metisMenu/metisMenu.min.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/panel/js/sb-admin-2.js', CClientScript::POS_END,[], 3);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/panel/vendor/metisMenu/metisMenu.min.js', CClientScript::POS_END,[], 2);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/panel-mine.css');

?>

<?php
$customCqlOrder = (Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL);
?>

<?php $this->beginContent('//layouts/main2'); ?>
<div id="panel-content" class="panel-content <?= isset(Yii::app()->request->cookies['panelClosedSidebar']) && Yii::app()->request->cookies['panelClosedSidebar']->value ? 'closed-sidebar' : '';?>">
    <div class="panel-content-overlay">


        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header text-center">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div id="panel-sidebar-wrapper">

                <div class="navbar-default sidebar" id="sidebar-menu" role="navigation">
                    <div class="sidebar-nav navbar-collapse">

                        <div id="close-panel-menu-wrapper">
                            <button type="button" id="close-panel-menu">
                                <span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span>
                            </button>
                        </div>
                        <?php
                        if(Yii::app()->user->model->clientImg):
                            ?>
                            <div id="panel-client-img-holder">
                                <img src="<?= Yii::app()->createUrl('/panel/getClientImg', ['hash' => Yii::app()->user->model->hash]);?>"/>
                            </div>
                        <?php
                        endif;
                        ?>
                        <ul class="nav" id="side-menu">
                            <li>
                                <?= CHtml::link('<i class="sidebar-main-ico fa fa-dashboard"></i> '.Yii::t('panel', 'Panel'),['/panel'], ['class' => $this->panelLeftMenuActive == 1 ? 'active' : '']); ?>
                            </li>
                            <li>
                                <a class="<?= $this->panelLeftMenuActive == 100 ? 'active' : '';?>" href="#"><i class="sidebar-main-ico fa fa-cube"></i> <?= Yii::t('panel', 'Przesyłki kurierskie');?><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <?php
                                    if(!$customCqlOrder):
                                        ?>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-list-alt"></i> '.Yii::t('panel', 'Lista przesyłek'),['/courier/courier/list'], ['class' => $this->panelLeftMenuActive == 101 ? 'active' : '']); ?>
                                        </li>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-indent"></i> '.Yii::t('panel', 'Masowe zarządzanie'),['/courier/courierScanner'], ['class' => $this->panelLeftMenuActive == 102 ? 'active' : '']); ?>
                                        </li>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-print"></i> '.Yii::t('panel', 'Drukowanie etykiet'),['/courier/courier/labelPrinter'], ['class' => $this->panelLeftMenuActive == 103 ? 'active' : '']); ?>
                                        </li>
                                    <?php
                                    endif;
                                    ?>
                                    <li>
                                        <a class="<?= $this->panelLeftMenuActive == 110 ? 'active' : '';?>" href="#"><?= Yii::t('panel','Standard'); ?> <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową'),['/courier/courier/create'], ['class' => $this->panelLeftMenuActive == 111 ? 'active' : '']); ?>
                                            </li>
                                            <?php if(Yii::app()->user->getModel()->isPremium): ?>
                                                <li>
                                                    <?= CHtml::link('<span class="glyphicon glyphicon-save-file"></span> '.Yii::t('panel','Importuj z pliku'),['/courier/courierInternal/import'], ['class' => $this->panelLeftMenuActive == 112 ? 'active' : '']); ?>
                                                </li>
                                                <?php
                                                if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_CQL):
                                                    ?>
                                                    <li>
                                                        <?= CHtml::link(CHtml::image(Yii::app()->baseUrl.'/images/alt-icons/ebay.png').' '.Yii::t('panel','Importuj z eBay.com'),['/courier/courierInternal/ebay'], ['class' => $this->panelLeftMenuActive == 113 ? 'active' : '']); ?>
                                                    </li>
                                                <?php
                                                endif;
                                                ?>
                                                <?php
                                                if(Yii::app()->user->model->getLinkerImportAvailable()):
                                                    ?>
                                                    <li>
                                                        <?= CHtml::link('<span class="glyphicon glyphicon-link"></span> '.Yii::t('panel','Importuj z Linker'),['/courier/courierInternal/linker'], ['class' => $this->panelLeftMenuActive == 115 ? 'active' : '']); ?>
                                                    </li>
                                                <?php
                                                endif;
                                                ?>
                                                <?php
                                                if(0):
                                                    ?>
                                                    <li>
                                                        <?= CHtml::link(Yii::t('panel','Importuj z RedCart').' <i class="fa fa-external-link" aria-hidden="true"></i>','http://mark.redcart.com/RedCart/admin/', ['target' => '_blank']); ?>
                                                    </li>
                                                    <li>
                                                        <?= CHtml::link(Yii::t('panel','Importuj z Baselinker').' <i class="fa fa-external-link" aria-hidden="true"></i>','https://panel.baselinker.com/login.php', ['target' => '_blank']); ?>
                                                    </li>
                                                <?php
                                                endif;
                                                ?>
                                                <?php
                                                if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_CQL):
                                                    ?>
                                                    <li>
                                                        <?= CHtml::link('<span class="glyphicon glyphicon-copy"></span> '.Yii::t('panel','Eksportuj'),['/courier/courier/export', 'mode' => Controller::EXPORT_MODE_INTERNAL], ['class' => $this->panelLeftMenuActive == 114 ? 'active' : '']); ?>
                                                    </li>
                                                <?php
                                                endif;
                                                ?>
                                            <?php endif; ?>
                                        </ul>

                                    </li>
                                    <?php
                                    if(Yii::app()->user->model->getCourierUAvailable() OR Yii::app()->user->model->getCourierUDomesticAvailable()):
                                        ?>
                                        <li>
                                            <a class="<?= $this->panelLeftMenuActive == 180 && $this->panelLeftMenuActive == 181 ? 'active' : '';?>" href="#"><?= Yii::t('panel','U'); ?> <span class="fa arrow"></span></a>
                                            <ul class="nav nav-third-level">
                                                <?php
                                                if(Yii::app()->user->model->getCourierUAvailable()):
                                                    ?>
                                                    <li>
                                                        <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową'),['/courier/courierU/create'], ['class' => $this->panelLeftMenuActive == 180 ? 'active' : '']); ?>
                                                    </li>
                                                <?php
                                                endif;
                                                ?>
                                                <?php
                                                if(Yii::app()->user->model->getCourierUDomesticAvailable() && Yii::app()->PV->get() != PageVersion::PAGEVERSION_CQL):
                                                    ?>
                                                    <li>
                                                        <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową Krajową'),['/courier/courierU/create', 'domestic' => true], ['class' => $this->panelLeftMenuActive == 181 ? 'active' : '']); ?>
                                                    </li>
                                                <?php
                                                endif;
                                                ?>
                                                <?php if(Yii::app()->user->getModel()->isPremium): ?>
                                                    <li>
                                                        <?= CHtml::link('<span class="glyphicon glyphicon-save-file"></span> '.Yii::t('panel','Importuj z pliku'),['/courier/courierU/import'], ['class' => $this->panelLeftMenuActive == 182 ? 'active' : '']); ?>
                                                    </li>
                                                    <?php
                                                    if(Yii::app()->user->model->getLinkerImportAvailable()):
                                                        ?>
                                                        <li>
                                                            <?= CHtml::link('<span class="glyphicon glyphicon-link"></span> '.Yii::t('panel','Importuj z Linker'),['/courier/courierU/linker'], ['class' => $this->panelLeftMenuActive == 184 ? 'active' : '']); ?>
                                                        </li>
                                                    <?php
                                                    endif;
                                                    ?>
                                                    <?php
                                                    if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_CQL):
                                                        ?>
                                                        <li>
                                                            <?= CHtml::link(CHtml::image(Yii::app()->baseUrl.'/images/alt-icons/ebay.png').' '.Yii::t('panel','Importuj z eBay.com'),['/courier/courierU/ebay'], ['class' => $this->panelLeftMenuActive == 183 ? 'active' : '']); ?>
                                                        </li>
                                                    <?php
                                                    endif;
                                                endif;
                                                ?>
                                            </ul>
                                        </li>
                                    <?php
                                    endif;
                                    ?>
                                    <?php if(Yii::app()->user->getModel()->isPremium && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH && !Yii::app()->user->model->getCourierExternalDisable()):?>
                                        <li>
                                            <a class="<?= $this->panelLeftMenuActive == 130 ? 'active' : '';?>" href="#"><?= Yii::t('panel','Premium'); ?> <span class="fa arrow"></span></a>
                                            <ul class="nav nav-third-level">
                                                <li>
                                                    <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową'),['/courier/courier/createPremium'], ['class' => $this->panelLeftMenuActive == 131 ? 'active' : '']); ?>
                                                </li>
                                                <li>
                                                    <?= CHtml::link('<span class="glyphicon glyphicon-save-file"></span> '.Yii::t('panel','Importuj z pliku'),['/courier/importCourierExternalFromFile'], ['class' => $this->panelLeftMenuActive == 132 ? 'active' : '']); ?>
                                                </li>
                                                <li>
                                                    <?= CHtml::link('<span class="glyphicon glyphicon-copy"></span> '.Yii::t('panel','Eksportuj'),['/courier/courier/export', 'mode' => Controller::EXPORT_MODE_EXTERNAL], ['class' => $this->panelLeftMenuActive == 133 ? 'active' : '']); ?>
                                                </li>
                                            </ul>

                                        </li>
                                    <?php endif; ?>

                                    <?php if(Yii::app()->user->getModel()->isPremium && Yii::app()->user->model->getCourierExternalReturnAvailable()):?>
                                        <li>
                                            <a class="<?= $this->panelLeftMenuActive == 150 ? 'active' : '';?>" href="#"><?=Yii::t('panel','External Return'); ?> <span class="fa arrow"></span></a>
                                            <ul class="nav nav-third-level">
                                                <li>
                                                    <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową'),['/courier/courierExternalReturn/create'], ['class' => $this->panelLeftMenuActive == 151 ? 'active' : '']); ?>
                                                </li>
                                            </ul>

                                        </li>
                                    <?php endif; ?>

                                    <?php if(Yii::app()->user->getModel()->isPremium && Yii::app()->user->model->getCourierReturnAvailable()):?>
                                        <li>
                                            <a class="<?= $this->panelLeftMenuActive == 160 ? 'active' : '';?>" href="#"><?=Yii::t('panel','Return'); ?> <span class="fa arrow"></span></a>
                                            <ul class="nav nav-third-level">
                                                <li>
                                                    <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową'),['/courier/courierReturn/create'], ['class' => $this->panelLeftMenuActive == 161 ? 'active' : '']); ?>
                                                </li>
                                            </ul>

                                        </li>
                                    <?php endif; ?>

                                    <?php
                                    if($customCqlOrder):
                                        ?>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-list-alt"></i> '.Yii::t('panel', 'Lista przesyłek'),['/courier/courier/list'], ['class' => $this->panelLeftMenuActive == 101 ? 'active' : '']); ?>
                                        </li>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-indent"></i> '.Yii::t('panel', 'Masowe zarządzanie'),['/courier/courierScanner'], ['class' => $this->panelLeftMenuActive == 102 ? 'active' : '']); ?>
                                        </li>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-print"></i> '.Yii::t('panel', 'Drukowanie etykiet'),['/courier/courier/labelPrinter'], ['class' => $this->panelLeftMenuActive == 103 ? 'active' : '']); ?>
                                        </li>
                                    <?php
                                    endif;
                                    ?>

                                    <li>
                                        <?= CHtml::link(Yii::t('panel','Potwierdzenie nadania'),['/courier/courier/ack'], ['class' => $this->panelLeftMenuActive == 160 ? 'active' : '']); ?>
                                    </li>
                                    <?php
                                    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL):
                                        ?>
                                        <li>
                                            <?= CHtml::link('<span class="glyphicon glyphicon-copy"></span> '.Yii::t('panel','Eksportuj'),['/courier/courier/export', 'mode' => Controller::EXPORT_MODE_INTERNAL], ['class' => $this->panelLeftMenuActive == 114 ? 'active' : '']); ?>
                                        </li>
                                    <?php
                                    endif;
                                    ?>
                                </ul>

                            </li>

                            <li>
                                <a class="<?= $this->panelLeftMenuActive == 200 ? 'active' : '';?>" href="#"><i class="fa fa-envelope-open"></i> <?= Yii::t('panel', 'Postal');?><span class="fa arrow"></span></a>

                                <ul class="nav nav-second-level collapse">
                                    <?php
                                    if(!$customCqlOrder):
                                        ?>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-list-alt"></i> '.Yii::t('panel','Lista przesyłek'),['/postal/postal/list'], ['class' => $this->panelLeftMenuActive == 201 ? 'active' : '']); ?>
                                        </li>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-indent"></i> '.Yii::t('panel','Masowe zarządanie przesyłkami'),['/postal/postalScanner'], ['class' => $this->panelLeftMenuActive == 202 ? 'active' : '']); ?>
                                        </li>
                                    <?php
                                    endif;
                                    ?>
                                    <li>
                                        <a class="<?= $this->panelLeftMenuActive == 210 ? 'active' : '';?>" href="#"><?= Yii::t('panel','Nadanie pojedyńcze'); ?> <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <?php
                                            if(!Yii::app()->user->model->getPostalInternationalDisable()):
                                                ?>
                                                <li>
                                                    <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Nowa przesyłka'),['/postal/postal/create'], ['class' => $this->panelLeftMenuActive == 212 ? 'active' : '']); ?>
                                                </li>
                                            <?php
                                            endif;
                                            ?>
                                            <li>
                                                <?= CHtml::link('<span class="glyphicon glyphicon-save-file"></span> '.Yii::t('panel','Importuj z pliku'),['/postal/postal/import'], ['class' => $this->panelLeftMenuActive == 213 ? 'active' : '']); ?>
                                            </li>
                                            <?php
                                            if(Yii::app()->user->model->getLinkerImportAvailable()):
                                                ?>
                                                <li>
                                                    <?= CHtml::link('<span class="glyphicon glyphicon-link"></span> '.Yii::t('panel','Importuj z Linker'),['/postal/postal/linker'], ['class' => $this->panelLeftMenuActive == 215 ? 'active' : '']); ?>
                                                </li>
                                            <?php
                                            endif;
                                            ?>
                                            <?php
                                            if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_CQL):
                                                ?>
                                                <li>
                                                    <?= CHtml::link(CHtml::image(Yii::app()->baseUrl.'/images/alt-icons/ebay.png').' '.Yii::t('panel', 'Importuj z eBay.com'), ['/postal/postal/ebay'], ['class' => $this->panelLeftMenuActive == 214 ? 'active' : '']); ?>
                                                </li>
                                            <?php
                                            endif;
                                            ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel', 'Korespondencja masowa'), ['/postal/postal/createBulk'], ['class' => $this->panelLeftMenuActive == 222 ? 'active' : '']); ?>
                                    </li>
                                    <?php
                                    if($customCqlOrder):
                                        ?>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-list-alt"></i> '.Yii::t('panel','Lista przesyłek'),['/postal/postal/list'], ['class' => $this->panelLeftMenuActive == 201 ? 'active' : '']); ?>
                                        </li>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-indent"></i> '.Yii::t('panel','Masowe zarządanie przesyłkami'),['/postal/postalScanner'], ['class' => $this->panelLeftMenuActive == 202 ? 'active' : '']); ?>
                                        </li>
                                    <?php
                                    endif;
                                    ?>
                                    <li>
                                        <?= CHtml::link(Yii::t('panel', 'Potwierdzenia nadania'), ['/postal/postal/ack'], ['class' => $this->panelLeftMenuActive == 240 ? 'active' : '']); ?>
                                    </li>
                                    <li>
                                        <?=CHtml::link(Yii::t('panel','Przygotowanie listów'),['/postal/postal/prepare'], ['class' => $this->panelLeftMenuActive == 241 ? 'active' : '']); ?>
                                    </li>
                                    <li>
                                        <?= CHtml::link('<span class="glyphicon glyphicon-copy"></span> '.Yii::t('panel','Eksport'),['/postal/postal/export'], ['class' => $this->panelLeftMenuActive == 242 ? 'active' : '']); ?>
                                    </li>
                                </ul>

                            </li>

                            <?php
                            if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_QURIERS && !Yii::app()->user->model->getUserMenuSimple()):
                                ?>
                                <li>
                                    <a class="<?= $this->panelLeftMenuActive == 500 ? 'active' : '';?>" href="#"><i class="fa fa-th-large"></i> <?= Yii::t('panel', 'Transport palet');?><span class="fa arrow"></span></a>

                                    <ul class="nav nav-second-level collapse">
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową przesyłkę'), ['/palette/palette/create'], ['class' => $this->panelLeftMenuActive == 501 ? 'active' : '']); ?>
                                        </li>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-list-alt"></i> '.Yii::t('panel','Lista Twoich przesyłek'), ['/palette/palette/list'], ['class' => $this->panelLeftMenuActive == 502 ? 'active' : '']); ?>
                                        </li>
                                    </ul>

                                </li>
                            <?php
                            endif;
                            ?>
                            <?php
                            if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_QURIERS && !Yii::app()->user->model->getUserMenuSimple()):
                                ?>
                                <li>
                                    <a class="<?= $this->panelLeftMenuActive == 600 ? 'active' : '';?>" href="#"><i class="fa fa-ambulance"></i> <?= Yii::t('panel', 'Transport specjalny');?><span class="fa arrow"></span></a>

                                    <ul class="nav nav-second-level collapse">
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową przesyłkę'), ['/specialTransport/specialTransport/create'], ['class' => $this->panelLeftMenuActive == 601 ? 'active' : '']); ?>
                                        </li>
                                        <li>
                                            <?= CHtml::link('<i class="fa fa-list-alt"></i> '.Yii::t('panel','Lista Twoich przesyłek'), ['/specialTransport/specialTransport/list'], ['class' => $this->panelLeftMenuActive == 602 ? 'active' : '']); ?>
                                        </li>
                                    </ul>

                                </li>
                            <?php
                            endif;
                            ?>
                            <li>
                                <a class="<?= $this->panelLeftMenuActive == 700 ? 'active' : '';?>" href="#"><i class="fa fa-user"></i> <?= Yii::t('panel', 'Twoje konto');?><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li>
                                        <?= CHtml::link(Yii::t('panel','Ustawienia konta'), ['/user/update'], ['class' => $this->panelLeftMenuActive == 701 ? 'active' : '']); ?>
                                    </li>
                                    <li>
                                        <?= CHtml::link(Yii::t('panel','Twoje wiadomości ({no})', array('{no}' => UserMessage::getNoOfNetMessagesForUser(Yii::app()->user->id))), ['/userMessage/'], ['class' => $this->panelLeftMenuActive == 702 ? 'active' : '']); ?>
                                    </li>
                                    <li>
                                        <?= CHtml::link(Yii::t('panel','Twoje faktury ({no})', array('{no}' => UserInvoice::getNoOfNewInvoicesForUser(Yii::app()->user->id))), ['/userInvoice/'], ['class' => $this->panelLeftMenuActive == 706 ? 'active' : '']); ?>
                                    </li>
                                    <li>
                                        <?= CHtml::link(Yii::t('panel','Twoje dokumenty'), ['/user/documents/'], ['class' => $this->panelLeftMenuActive == 707 ? 'active' : '']); ?>
                                    </li>
                                    <li>
                                        <?= CHtml::link(Yii::t('panel','Karty potwierdzenia'), ['/user/ackCardArchive/'], ['class' => $this->panelLeftMenuActive == 709 ? 'active' : '']); ?>
                                    </li>
                                    <?php
                                    if(0):
                                        ?>
                                        <li>
                                            <?= CHtml::link(Yii::t('panel','Bilans'), ['/userBalance'], ['class' => $this->panelLeftMenuActive == 704 ? 'active' : '']); ?>
                                        </li>
                                    <?php
                                    endif;
                                    ?>
                                    <li>
                                        <?= CHtml::link(Yii::t('panel','Zamówienia'), ['/order'], ['class' => $this->panelLeftMenuActive == 705 ? 'active' : '']); ?>
                                    </li>
                                    <li>
                                        <a class="<?= $this->panelLeftMenuActive == 800 ? 'active' : '';?>" href="#"><i class="fa fa-address-book" aria-hidden="true"></i> <?= Yii::t('panel', 'Książka adresowa');?><span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <?= CHtml::link('<i class="fa fa-list-alt"></i> '.Yii::t('panel','Lista'), ['/userContactBook'], ['class' => $this->panelLeftMenuActive == 801 ? 'active' : '']); ?>
                                            </li>
                                            <li>
                                                <?= CHtml::link('<i class="fa fa-plus-circle" aria-hidden="true"></i> '.Yii::t('panel','Utwórz nową pozycję'), ['/userContactBook/create'], ['class' => $this->panelLeftMenuActive == 802 ? 'active' : '']); ?>
                                            </li>
                                            <li>
                                                <?= CHtml::link('<span class="glyphicon glyphicon-save-file"></span> '.Yii::t('panel','Importuj z pliku'), ['userContactBook/import'], ['class' => $this->panelLeftMenuActive == 803 ? 'active' : '']); ?>
                                            </li>
                                        </ul>

                                    </li>
                                    <?php if(Yii::app()->user->model->getPrintServerActive()):?>
                                        <li>
                                            <?= CHtml::link(Yii::t('panel','Print Server'),['/printServer'], ['class' => $this->panelLeftMenuActive == 161 ? 'active' : '']); ?>
                                        </li>
                                    <?php endif; ?>
                                    <?php
                                    if(!in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_QURIERS, PageVersion::PAGEVERSION_CQL])):
                                        ?>
                                        <li>
                                            <?= CHtml::link(Yii::t('panel','Instrukcja').' <i class="fa fa-external-link" aria-hidden="true"></i>', Yii::app()->baseUrl.'/misc/INSTRUKCJA_swiatPrzesylek.pdf', ['target' => '_blank']); ?>
                                        </li>
                                    <?php
                                    endif;
                                    ?>
                                    <?php if(Yii::app()->user->model->getApiAccessActive() && in_array(Yii::app()->PV->get(), [
                                            PageVersion::PAGEVERSION_DEFAULT,
                                            PageVersion::PAGEVERSION_RS,
                                        ])):?>
                                        <li>
                                            <?= CHtml::link(Yii::t('panel','API'),['/site/api'], ['class' => $this->panelLeftMenuActive == 162 ? 'active' : '']); ?>
                                        </li>
                                    <?php endif; ?>
                                    <li>
                                        <?= CHtml::link(Yii::t('panel','Integracje/wtyczki'),['/plugins/'], ['class' => $this->panelLeftMenuActive == 708 ? 'active' : '']); ?>
                                    </li>
                                </ul>

                            </li>
                            <li>
                                <?= CHtml::link('<i class="fa fa-map-marker"></i> '.Yii::t('panel', 'Track&trace').' <i class="fa fa-external-link" aria-hidden="true"></i>',['/tt'], ['class' => $this->panelLeftMenuActive == 10 ? 'active' : '', 'target' => '_blank']); ?>
                            </li>
                            <li>
                                <?php
                                if(!in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_QURIERS, PageVersion::PAGEVERSION_RUCH]) && $salesman = Yii::app()->user->model->salesman):
                                    ?>
                                    <div class="panel-contact" id="panel-salesman">
                                        <?= Yii::t('panel', 'Twój opiekun handlowy');?><br/><br/>
                                        <img src="<?= Yii::app()->createUrl('/panel/getSalesmanImg', ['id' => $salesman->id]);?>" style="border-radius: 50%;"/><br/>
                                        <strong><?= $salesman->name;?></strong><br/><br/>

                                        <i class="fa fa-phone" aria-hidden="true"></i> <?= $salesman->tel;?><br/>
                                        <a href="mailto:<?= $salesman->email;?>"><?= strlen($salesman->email) > 30 ? str_replace('swiatprzesylek.pl', 'sw(...)ek.pl', $salesman->email) : $salesman->email;?></a>
                                    </div>
                                <?php
                                endif;
                                ?>
                                <?php
                                if(!in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_QURIERS, PageVersion::PAGEVERSION_RUCH])):
                                    ?>
                                    <div class="panel-contact" id="panel-salesman">
                                        <strong><?= Yii::t('panel', 'Dział Obsługi Klienta');?></strong><br/>
                                        <i class="fa fa-phone" aria-hidden="true"></i> <?= Yii::t('panel', "_221221218");?><br/>
                                        <a href="mailto:<?= Yii::t('panel', 'cs@swiatprzesylek.pl');?>"><?= Yii::t('panel', 'cs@swiatprzesylek.pl');?></a><br/><br/>
                                        <strong><?= Yii::t('panel', 'Dział Księgowości');?></strong><br/>
                                        <i class="fa fa-phone" aria-hidden="true"></i> <?= Yii::t('panel', "_518220294");?><br/>
                                        <a href="mailto:<?= Yii::t('panel', 'elakoncewicz@swiatprzesylek.pl');?>"><?= Yii::t('panel', 'elakoncewicz@swiatprzesylek.pl');?></a><br/><br/>
                                        <strong><?= Yii::t('panel', 'Dział Reklamacji');?></strong><br/>
                                        <a href="mailto:<?= Yii::t('panel', 'martarzepa@swiatprzesylek.pl');?>"><?= Yii::t('panel', 'martarzepa@swiatprzesylek.pl');?></a>
                                    </div>
                                <?php
                                endif;
                                ?>
                            </li>
                        </ul>

                    </div>

                </div>

            </div>

        </nav>

        <div id="page-wrapper" style="background: transparent;">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if(Yii::app()->user->hasFlash('top-info')):?>
                            <div class="alert alert-warning">
                                <?= Yii::app()->user->getFlash('top-info'); ?>
                            </div>
                        <?php endif; ?>
                        <?= $content;?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $this->endContent(); ?>
