<?php

class LinkerOrderItem
{
    const STAT_NOT_PAID = 0;
    const STAT_PAID = 1;

    const STAT_NOT_SHIPPED = 0;
    const STAT_PARTIALY_SHIPPED = 5;
    const STAT_SHIPPED = 10;

    const STAT_PROCESSED = 1;
    const STAT_NOT_PROCESSED = 0;

    const STAT_SHOW_PROCESSED = 3;
    const STAT_SHOW_NOT_PROCESSED = 2;
    const STAT_SHOW_PROCESSED_ALL = 1;

    const STAT_SHOW_LINKER_ALL = 1;
    const STAT_SHOW_LINKER_SENT = 10;
    const STAT_SHOW_LINKER_NOT_SENT = 20;

    public $id;


    public $createdTime;

    public $lastModified;

    public $statPayment;
    public $statShipping;
    public $statProcessed;


    public $shippingAddress;

    public $email;

    public $addr_name;
    public $addr_street;
    public $addr_city;
    public $addr_state;
    public $addr_country;
    public $addr_country_id;
    public $addr_tel;
    public $addr_zip_code;


    public $content;

    public $addr_email;
    public $cod;

    public $carrier;
    public $number;
    public $clientOrderId;
    public $priceGross;
    public $currencySymbol;

    public $origin;

    public $weight = 0;

    public $local_package_id;
    public $local_package_hash; // for link
    public $local_package_target;

    public $orderLines;

    public $product_type;

    public static function getStatsShippingArray()
    {
        $data = [];
        $data[self::STAT_NOT_SHIPPED] = Yii::t('courier_linker', 'Nie wysłane');
        $data[self::STAT_PARTIALY_SHIPPED] = Yii::t('courier_linker', 'Częściowo wysłane');
        $data[self::STAT_SHIPPED] = Yii::t('courier_linker', 'Wysłane');
        return $data;
    }

    public static function getStatsPaymentArray()
    {
        $data = [];
        $data[self::STAT_NOT_PAID] = Yii::t('courier_linker', 'Nie opłacone');
        $data[self::STAT_PAID] = Yii::t('courier_linker', 'Opłacone');
        return $data;
    }

    public static function getStatShowProcessedList()
    {
        $data = [];
        $data[self::STAT_SHOW_PROCESSED_ALL] = Yii::t('courier_linker', '--wszystkie--');
        $data[self::STAT_SHOW_PROCESSED] = Yii::t('courier_linker', 'przetworzone');
        $data[self::STAT_SHOW_NOT_PROCESSED] = Yii::t('courier_linker', 'nieprzetworzone');

        return $data;
    }

    public static function getStatShowLinkerList()
    {
        $data = [];
        $data[self::STAT_SHOW_LINKER_ALL] = Yii::t('courier_linker', '--wszystkie--');
        $data[self::STAT_SHOW_LINKER_SENT] = Yii::t('courier_linker', 'wysłane');
        $data[self::STAT_SHOW_LINKER_NOT_SENT] = Yii::t('courier_linker', 'niewysłane');

        return $data;
    }

    public static function getStatProcessedList()
    {
        $data = [];
        $data[self::STAT_PROCESSED] = Yii::t('courier_linker', 'przetworzone');
        $data[self::STAT_NOT_PROCESSED] = Yii::t('courier_linker', 'nieprzetworzone');

        return $data;
    }


    public function __construct(array $order, $user_id = NULL, $target = LinkerImport::TARGET_COURIER)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        $this->id = $order['id'];


        $this->createdTime = substr(str_replace('T',' ', $order['orderDate']),0,19);

        $this->addr_name = $order['deliveryRecipient'];
        $this->addr_company = $order['deliveryCompany'];
        $this->addr_street = $order['deliveryStreet'];
        $this->addr_city = $order['deliveryCity'];
        $this->addr_country = $order['deliveryCountry'];
        $this->addr_country_id =  CountryList::findIdByText($this->addr_country);
        $this->addr_tel = $order['deliveryPhone'];
        $this->addr_email = $order['deliveryEmail'];
        $this->addr_zip_code = $order['deliveryPostCode'];

        $this->cod = $order['cod'];

        $this->number = $order['number'];
        $this->carrier = $order['carrier'];
        $this->clientOrderId = $order['clientOrderNumber'];
        $this->priceGross = $order['priceGross'];
        $this->currencySymbol = $order['currencySymbol'];

        $this->stat = $order['orderStatus'];

        $this->origin = $order['origin'];


        $this->content = [];

        $this->weight = 0;
        $this->priceGross = 0;

        $this->product_type = [];

        $quantity = 0;
        $orderLines = 0;
        if(is_array($order['items']))
            foreach($order['items'] AS $item) {
                $quantity += intval($item['quantity']);

                $this->content[] = intval($item['quantity']).':'.$item['description'];
                $this->weight += floatval($item['weight']) * intval($item['quantity']);
                $this->priceGross += floatval($item['priceGross']) * intval($item['quantity']);

                $type = trim($item['product_object']['type']);
                if($type != '')
                    $this->product_type[] = $item['product_object']['type'];

                $orderLines++;
            }

        $this->orderLines = $orderLines;

        $this->content = implode('|||', $this->content);
        $this->product_type = implode('|', $this->product_type);

        // maybe package is already processed by our system - search for it and return ID if found
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('target_item_id, target');
        $cmd->from((new EbayImport())->tableName());
        $cmd->where('ebay_order_id = :ebay_order_id AND stat != :stat AND user_id = :user_id', [':ebay_order_id' => $this->id, ':stat' => EbayImport::STAT_CANCELLED, ':user_id' => $user_id]);
        $cmd->order('date_updated DESC, date_entered DESC');
        $cmd->limit(1);
        $res = $cmd->queryRow();

        $this->local_package_id = $res['target_item_id'];
        $this->local_package_target = $res['target'];

        if($this->local_package_id)
        {
            $this->statProcessed = self::STAT_PROCESSED;

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('hash');

            if($target == LinkerImport::TARGET_COURIER)
                $cmd->from((new Courier())->tableName());
            else if($target == LinkerImport::TARGET_POSTAL)
                $cmd->from((new Postal())->tableName());
            else
                throw new Exception;

            $cmd->where('id = :id', [':id' => $this->local_package_id]);
            $cmd->limit(1);

            $this->local_package_hash = $cmd->queryScalar();
        } else
            $this->statProcessed = self::STAT_NOT_PROCESSED;

    }




    public function convertToCourierTypeInternalModel()
    {
        $courier = new Courier_CourierTypeInternal();
        $courier->source = Courier::SOURCE_LINKER;

        $courier->courierTypeInternal = new CourierTypeInternal('import');
        $courier->senderAddressData = new CourierTypeInternal_AddressData();
        $courier->receiverAddressData = new CourierTypeInternal_AddressData();

        $courier->package_weight = $this->weight;
        $courier->package_value = $this->priceGross;
        $courier->value_currency = $this->currencySymbol;

        $courier->package_size_l = 20;
        $courier->package_size_w = 20;
        $courier->package_size_d = 20;
        $courier->packages_number = 1;

        $courier->package_content = $this->content;
//        $courier->package_content = 'L('.$this->clientOrderId.'): '.$this->content;
        $courier->note1 = $this->clientOrderId;

//        $courier->note2 = $this->orderLines;

        $street = mb_substr($this->addr_street,0,45);
        $street2 = mb_substr($this->addr_street,45);

        $courier->receiverAddressData->name = $this->addr_name;
        $courier->receiverAddressData->company = $this->addr_company;
        $courier->receiverAddressData->country_id = $this->addr_country_id;
        $courier->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->receiverAddressData->country_id);
        $courier->receiverAddressData->zip_code = $this->addr_zip_code;
        $courier->receiverAddressData->city = $this->addr_city;
        $courier->receiverAddressData->address_line_1 = $street;
        $courier->receiverAddressData->address_line_2 =  $street2;
        $courier->receiverAddressData->tel = $this->addr_tel;
        $courier->receiverAddressData->email = $this->addr_email;

        $courier->courierTypeInternal->courier = $courier;


        $courier->courierTypeInternal->_ebay_order_id = $this->id;


        return $courier;
    }

    public function convertToCourierTypeUModel()
    {
        $courier = new Courier_CourierTypeU();
        $courier->source = Courier::SOURCE_LINKER;

        $courier->courierTypeU = new CourierTypeU('import');
        $courier->senderAddressData = new CourierTypeU_AddressData();
        $courier->receiverAddressData = new CourierTypeU_AddressData();

        $courier->package_weight = $this->weight;
        $courier->package_value = $this->priceGross;
        $courier->value_currency = $this->currencySymbol;

        $courier->package_size_l = 20;
        $courier->package_size_w = 20;
        $courier->package_size_d = 20;
        $courier->packages_number = 1;

        $courier->package_content = $this->content;
//        $courier->package_content = 'L('.$this->clientOrderId.'): '.$this->content;
        $courier->note1 = $this->clientOrderId;

//        $courier->note2 = $this->orderLines;

        $street = mb_substr($this->addr_street,0,45);
        $street2 = mb_substr($this->addr_street,45);

//        $courier->senderAddressData->name = 'nadawca';
//        $courier->senderAddressData->company = 'nadawca';
//        $courier->senderAddressData->country_id = CountryList::findIdByText('polska');
//        $courier->senderAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->senderAddressData->country_id);
//        $courier->senderAddressData->zip_code = '00-000';
//        $courier->senderAddressData->city = 'miasto';
//        $courier->senderAddressData->address_line_1 = 'adres 1';
//        $courier->senderAddressData->address_line_2 = 'adres 2';
//        $courier->senderAddressData->tel = '111222333';
//        $courier->senderAddressData->email = 'test@test.pl';

        $courier->receiverAddressData->name = $this->addr_name;
        $courier->receiverAddressData->company = $this->addr_company;
        $courier->receiverAddressData->country_id = $this->addr_country_id;
        $courier->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->receiverAddressData->country_id);
        $courier->receiverAddressData->zip_code = $this->addr_zip_code;
        $courier->receiverAddressData->city = $this->addr_city;
        $courier->receiverAddressData->address_line_1 = $street;
        $courier->receiverAddressData->address_line_2 = $street2;
        $courier->receiverAddressData->tel = $this->addr_tel;
        $courier->receiverAddressData->email = $this->addr_email;

        $courier->courierTypeU->courier = $courier;


        $courier->_ebay_order_id = $this->id;


        return $courier;
    }


    public function convertToPostalModel()
    {
        $postal = new Postal();
        $postal->source = Postal::SOURCE_LINKER;

        $postal->senderAddressData = new Postal_AddressData();
        $postal->receiverAddressData = new Postal_AddressData();

        $postal->senderAddressData->_postal = $postal;
        $postal->receiverAddressData->_postal = $postal;

        $postal->content = mb_substr($this->content, 0, 256);
        $postal->note1 = $this->clientOrderId;
//        $postal->note2 = $this->orderLines;


        $postal->weight = $this->weight * 1000;
        $postal->value = $this->priceGross;
        $postal->value_currency = $this->currencySymbol;

        $street = mb_substr($this->addr_street,0,45);
        $street2 = mb_substr($this->addr_street,45);

//        $courier->senderAddressData->name = 'nadawca';
//        $courier->senderAddressData->company = 'nadawca';
//        $courier->senderAddressData->country_id = CountryList::findIdByText('polska');
//        $courier->senderAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->senderAddressData->country_id);
//        $courier->senderAddressData->zip_code = '00-000';
//        $courier->senderAddressData->city = 'miasto';
//        $courier->senderAddressData->address_line_1 = 'adres 1';
//        $courier->senderAddressData->address_line_2 = 'adres 2';
//        $courier->senderAddressData->tel = '111222333';
//        $courier->senderAddressData->email = 'test@test.pl';

        $postal->receiverAddressData->name = $this->addr_name;
        $postal->receiverAddressData->company = $this->addr_company;
        $postal->receiverAddressData->country_id = $this->addr_country_id;
        $postal->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($postal->receiverAddressData->country_id);
        $postal->receiverAddressData->zip_code = $this->addr_zip_code;
        $postal->receiverAddressData->city = $this->addr_city;
        $postal->receiverAddressData->address_line_1 = $street;
        $postal->receiverAddressData->address_line_2 =  $street2;
        $postal->receiverAddressData->tel = $this->addr_tel;
        $postal->receiverAddressData->email = $this->addr_email;

        $postal->postalLabel = new PostalLabel();
        $postal->_ebay_order_id = $this->id;

        return $postal;
    }


}