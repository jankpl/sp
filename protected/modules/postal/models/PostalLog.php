<?php

Yii::import('application.modules.postal.models._base.BasePostalLog');

class PostalLog extends BasePostalLog
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	const CAT_INFO = 1;
	const CAT_ERROR = 9;

	public static function getCatList()
	{
		$data[self::CAT_INFO] = 'Info';
		$data[self::CAT_ERROR] = 'Error';

		return $data;
	}

	public function getCatName()
	{
		return self::getCatList()[$this->cat];
	}

	public function rules() {
		return array(
			array('date_entered', 'default',
				'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

			array('cat', 'default', 'value' => self::CAT_INFO),

			array('postal_id, text', 'required'),
			array('postal_id, cat', 'numerical', 'integerOnly'=>true),
			array('source', 'length', 'max'=>128),
			array('cat, source', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, postal_id, date_entered, text, cat, source', 'safe', 'on'=>'search'),
		);
	}

	public function beforeSave()
	{
		$this->text = strip_tags($this->text);
		$this->text = trim($this->text);

		return parent::beforeSave();
	}
}