<?php

class hybridMailModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

        $this->defaultController = 'hybridMail';

        $this->layoutPath = Yii::getPathOfAlias('application.backend.views.layouts');
        $this->layout = 'column2';

		// import the module-level models and components
		$this->setImport(array(
			'hybridMail.models.*',
			'hybridMail.components.*',
            'application.modules.hybridMail.models.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
		    throw new CHttpException(': R.I.P.');


			// this method is called before any module controller action is performed
            //			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
