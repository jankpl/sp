<?php
/* @result $result S_UniversalTt[] */
?>

    <div class="form">

        <h2><?php echo Yii::t('tt','Track&trace - Multi');?></h2>

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'tt-form',
            'enableAjaxValidation' => false,
            'enableClientValidation'=> false,
        ));
        ?>

        <?php
        $this->widget('FlashPrinter');
        ?>

        <div class="row text-center">
            <?= Yii::t('tt', 'Pozycji:');?> <strong id="numer-of-rows">0</strong>/25<br/>
            <?php echo CHtml::textArea('ids', NULL, ['style' => 'width: 100%; max-width: 500px; height: 250px; text-align: center;', 'id' => 'ids', 'placeholder' => Yii::t('tt','numer przesyłki 1{br}numer przesyłki 2{br}numer przesyłki 3{br}...', ['{br}' => "\r\n"])] ); ?>
            <br/>
            <?php
            echo TbHtml::submitButton(Yii::t('site','Sprawdź'), ['class' => 'btn btn-primary', 'id' => 'action-buttons']);
            $this->endWidget();
            ?>
        </div><!-- row -->

        <div id="too-much-warning" style="display: none;" class="alert alert-danger text-center">
            <?= Yii::t('tt', 'Dozwolonych pozycji na raz: {n}!', ['{n}' => 25]);?>
        </div>

    </div><!-- form -->
    <br/>
    <br/>
<?php
if(is_array($result) && S_Useful::sizeof($result)):
    ?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <?php
    /* @var $item S_UniversalTt */
    $i = 0;
    foreach($result AS $key => $item):

        $found = $item->isFound();
        $statMapId = $item->getCurrentStatMapId();

        if(!$found)
            $style = 'danger';
        else if($statMapId == StatMap::MAP_DELIVERED)
            $style = 'success';
        else if($statMapId == StatMap::MAP_CANCELLED OR $statMapId == StatMap::MAP_PROBLEM)
            $style = 'warning';
        ?>

        <div class="panel panel-<?= $style;?>">
            <div class="panel-heading" role="tab" id="heading<?=$i;?>">
                <h4 class="panel-title">
                    <?php
                    if($found):
                        ?>
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i;?>" aria-expanded="true" aria-controls="collapse<?=$i;?>">
                            <?= $key; ?> - <?= $item->getCurrentStatMapName();?>
                        </a>
                    <?php
                    else:
                        ?>
                        <?= $key; ?> - <?= Yii::t('tt', 'Nie znaleziono');?>
                    <?php
                    endif;
                    ?>
                </h4>
            </div>
            <?php
            if($found):
                ?>
                <div id="collapse<?=$i;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$i;?>">
                    <div class="panel-body">

                        <?= $this->renderPartial('_tt_status', [
                            'S_UniversalTt' => $item,
                            'noMap' => true,
                        ], true);
                        ?>

                    </div>
                </div>
            <?php
            endif;
            ?>
        </div>

        <?php
        $i++;
    endforeach;
    ?>
    <br/>
    <div class="text-center">

    <?php $form = $this->beginWidget('GxActiveForm',
    [
        'id' => 'tt-multi-export',
        'htmlOptions' => [
            'class' => 'do-not-block',
            'target' => '_blank',
        ],
    ]
);
    ?>
    <?= TbHtml::hiddenField('foundIds', $ids); ?>
    <?= TbHtml::submitButton(Yii::t('tt', 'Exportuj do .xls'), ['name' => 'export']); ?>

    <?php
    $this->endWidget();
    ?>


<?php
endif;
?>
<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
])):
    ?>
    <br/>
    <br/>

    <div class="text-center"><a href="https://carrierlist.17track.net/en/international/100033" target="_blank"><img src="<?= Yii::app()->baseUrl;?>/images/misc/17track.png" style="width: 150px;"/></a></div>
<?php
endif;
?>
    </div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/ttMulti.js', CClientScript::POS_END);
