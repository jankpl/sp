<?php
/* @var $models Postal[] */
/* @var $modelBulkForm PostalBulkForm */
/* @var $currency integer */
?>

<div class="form">

    <h2><?php echo Yii::t('postal','Podsumowanie');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '2/2')); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'postal-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

        <table class="table table-striped table-bordered text-center">
            <thead>
            <tr>
                <th class="text-center"><?= Yii::t('postal', 'Kraj odbiorcy');?></th>
                <th class="text-center"><?= (new Postal())->getAttributeLabel('postal_type'); ?></th>
                <th class="text-center"><?= (new Postal())->getAttributeLabel('weight'); ?> [g]</th>
                <th class="text-center"><?= (new Postal())->getAttributeLabel('size_class'); ?></th>
                <th class="text-center"><?= (new Postal())->getAttributeLabel('content'); ?></th>
                <th class="text-center"><?= (new Postal())->getAttributeLabel('bulk_number'); ?></th>
                <th class="text-center"><?= Yii::t('postal', 'Cena za 1 szt.') ?> <?= Yii::t('postal', 'netto/brutto');?></th>
                <th class="text-center"><?= Yii::t('postal', 'Cena') ?> <?= Yii::t('postal', 'netto/brutto');?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            /* @var $model Postal */
            $totalNo = 0;
            $totalPriceBrutto = 0;
            $totalPrice = 0;
            foreach($models AS $id => $model):
                ?>
                <tr>

                    <td><?= $model->getReceiverCountryName();?></td>
                    <td><?= $model->getPostalTypeName();?></td>
                    <td><?= $model->weight;?></td>
                    <td><?= $model->getSizeClassName();?></td>
                    <td><?= $model->content;?></td>
                    <td><?= $model->bulk_number;?></td>
                    <td><?= S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($model->getPrice(false, true), true));?>/<?= S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($model->getPrice(false, true)));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?></td>
                    <td><?= S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($model->getPrice(), true));?>/<?= S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($model->getPrice()));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?></td>
                </tr>
                <?php
                $totalNo += $model->bulk_number;
                $totalPrice += Yii::app()->PriceManager->getFinalPrice($model->getPrice(), true);
                $totalPriceBrutto += Yii::app()->PriceManager->getFinalPrice($model->getPrice());
            endforeach;
            ?>
            <th colspan="5" class="text-right"><?= Yii::t('postal', 'Łącznie:');?></th>
            <th class="text-center"><?= $totalNo;?></th>
            <th class="text-center">&nbsp;</th>
            <th class="text-center"><?= S_Price::formatPrice($totalPrice);?> / <?= S_Price::formatPrice($totalPriceBrutto); ?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?></th>
            </tbody>
        </table>


    <h3 class="text-center"><?php echo Yii::t('site', 'Regulamin');?></h3>

    <div class="row">
        <table class="table">
            <tr>
                <td><?php echo $form->labelEx($modelBulkForm,'regulations'); ?>
                    <?php echo $form->checkbox($modelBulkForm, 'regulations'); ?>
                    <?php echo $form->error($modelBulkForm,'regulations'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($modelBulkForm,'regulations_rodo'); ?>
                    <?php echo $form->checkbox($modelBulkForm, 'regulations_rodo'); ?>
                    <?php echo $form->error($modelBulkForm,'regulations_rodo'); ?></td>
            </tr>
        </table>


    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step4', 'class' => 'btn btn-sm'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę'),array('name'=>'finish', 'class' => 'btn btn-primary'));
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p><?php echo Yii::t('postal','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>
    </div>
</div><!-- form -->