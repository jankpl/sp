<?php
$this->renderPartial('importEbay/_header',[
    'ebayClient' => $ebayClient,
    'step' => 2
]);
?>


<?php
/* @var $package Courier_CourierTypeOoe */
?>
<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'default-data2',
//    'htmlOptions' => ['data-edit-form-id' => $id],
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));
    ?>

    <div id="ebay-form">
        <div style="float: left; width: 48%;">

            <h4><?= Yii::t('courier_ebay', 'Domyślne dane paczki');?></h4>
            <div class="row">
                <?php echo $form->labelEx($package, 'postal_type'); ?>
                <?php echo $form->dropDownList($package, 'postal_type', Postal::getPostalTypeList()); ?>
                <?php echo $form->error($package, 'postal_type'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'weight_class'); ?>
                <?php echo $form->dropDownList($package, 'weight_class', Postal::getWeightClassList()); ?>
                <?php echo $form->error($package, 'weight_class'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'size_class'); ?>
                <?php echo $form->dropDownList($package, 'size_class', Postal::getSizeClassList()); ?>
                <?php echo $form->error($package, 'size_class'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'target_sp_point'); ?>
                <?php
                $points = SpPoints::getPoints();
                echo $form->dropDownList($package, 'target_sp_point', CHtml::listData($points, 'id' ,'spPointsTr.title')); ?>
                <?php echo $form->error($package, 'target_sp_point'); ?>
            </div><!-- row -->
        </div>
        <div style="float: right; width: 48%;">
            <h4><?= Yii::t('courier_ebay', 'Dane nadawcy paczek');?></h4>
            <?php
            $this->renderPartial('_addressData/_form',
                array(
                    'model' => $senderAddressData,
                    'form' => $form,
                    'noEmail' => false,
                    'type' => 'sender',
                    'noErrorSummery' => true,
                )
            );
            ?>
        </div>
    </div>

    <div class="navigate" style="clear: both;">
        <input type="submit" value="<?= Yii::t('courier_ebay', 'Dalej');?>" name="save-default-data" class="btn btn-primary"/>
    </div>
    <?php $this->endWidget(); ?>
    <div style="text-align: center;">
        <a class="btn btn-xs" href="<?= Yii::app()->createUrl('/postal/postal/ebay');?>"><?= Yii::t('courier_ebay','zacznij od nowa');?></a>
    </div>
</div>



