<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Janek
 * Date: 17.09.13
 * Time: 09:54
 * To change this template use File | Settings | File Templates.
 */

?>

<h2><?php echo Yii::t('order','Przy odbiorze');?></h2>
<p class="text-center"><?php echo Yii::t('order','Wybrałeś płatność przy odbiorze.');?></p>
<div class="navigate">
    <?php echo CHtml::link(Yii::t('app', 'Dalej'), Yii::app()->createUrl('order/view', array('urlData' => $model->hash, 'jumpToProduct' => true)), array('class' => 'btn btn-lg btn-success'));?>
</div>