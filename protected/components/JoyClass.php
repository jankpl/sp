<?php


class JoyClass
{

    const URL = 'https://system.fhb.sk/api/joy';
    const URL_DEV = 'https://system-dev.fhb.sk/api/joy';
    const S200_OK = 200;
    const S201_CREATED = 201;
    const S204_NO_CONTENT = 204;
    const S404_NOT_FOUND = 404;

    /** @var string */
    private $endpoint;

    /** @var string */
    private $appId;

    /** @var string */
    private $secret;

    /** @var string */
    private $token;

    public function __construct($appId, $secret, $dev = FALSE)
    {
        $this->appId = $appId;
        $this->secret = $secret;
        $this->endpoint = $dev ? self::URL_DEV : self::URL;
    }


    public function getToken()
    {
        if (!$this->token) {
            $data = [
                'appId' => $this->appId,
                'secret' => $this->secret
            ];

            $curl = $this->createCurl('login');
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            $response = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);


            $json = json_decode($response);
            curl_close($curl);

            if ($httpCode != self::S200_OK) {
                $message = isset($json->message) ? $json->message : "Unknown error. Http code $httpCode.";
                throw new JoiApyException($message, $httpCode);
            }

            $this->token = $json->token;
        }

        return $this->token;
    }

    public function createPackageNew(array $data)
    {
        $data = json_encode($data);

        $curl = $this->login('shipments');
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        MyDump::dump('joy.txt', 'REQ: '.print_r($data,1));
        MyDump::dump('joy.txt', 'RESP: '.print_r($response,1));

        $json = json_decode($response);
        curl_close($curl);

        if ($httpCode != self::S200_OK) {
            $message = isset($json->message) ? $json->message : "Unknown error. Http code $httpCode.";
            throw new JoiApyException($message, $httpCode);
        }

        return $json;
    }


    public function createPackage(array $data)
    {


        $curl = $this->login('packages');
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        MyDump::dump('joy.txt', 'REQ: '.print_r($data,1));
        MyDump::dump('joy.txt', 'RESP: '.print_r($response,1));

        $json = json_decode($response);
        curl_close($curl);

        if ($httpCode != self::S201_CREATED) {
            $message = isset($json->message) ? $json->message : "Unknown error. Http code $httpCode.";
            throw new JoiApyException($message, $httpCode);
        }

        return $json;
    }


    public function getPackage($id)
    {
        $curl = $this->login("packages?id=$id");
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $json = json_decode($response);
        curl_close($curl);

        if ($httpCode == self::S404_NOT_FOUND) {
            $message = isset($json->message) ? $json->message : "Unknown error. Http code $httpCode.";
            throw new JoiApyException($message, $httpCode);
        }

        return $json;
    }


    public function updatePackage($id, array $data)
    {
        $curl = $this->login("packages?id=$id");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $json = json_decode($response);
        curl_close($curl);

        if ($httpCode != self::S200_OK) {
            $message = isset($json->message) ? $json->message : "Unknown error. Http code $httpCode.";
            throw new JoiApyException($message, $httpCode);
        }

        return $json;
    }


    public function deletePackage($id)
    {
        $curl = $this->login("packages?id=$id");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $json = json_decode($response);
        curl_close($curl);

        if ($httpCode != self::S204_NO_CONTENT) {
            $message = isset($json->message) ? $json->message : "Unknown error. Http code $httpCode.";
            throw new JoiApyException($message, $httpCode);
        }

        return $json;
    }


    public function getLabels(array $ids)
    {
        $curl = $this->login('shipments?id=' . join(',', $ids));
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        MyDump::dump('joy.txt', 'RESP: '.print_r($response,1));

        $json = json_decode($response);

        curl_close($curl);

        if ($httpCode != self::S200_OK) {
            $message = isset($json->message) ? $json->message : "Unknown error. Http code $httpCode.";
            throw new JoiApyException($message, $httpCode);
        }

        return $json;
    }


    private function login($action)
    {
        $curl = $this->createCurl($action);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "X-Authentication-Simple: " . base64_encode($this->getToken())
        ));

        MyDump::dump('joy.txt', $this->appId);

        return $curl;
    }


    private function createCurl($action)
    {

        MyDump::dump('joy.txt', print_r("{$this->endpoint}/{$action}",1));

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "{$this->endpoint}/{$action}");
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        return $curl;
    }

    public function getTrackingDirect($id)
    {
        $curl = $this->login('shipment?id='.$id);
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        MyDump::dump('joy.txt', 'RESP: '.print_r($response,1));

        $json = json_decode($response);



        curl_close($curl);

        if ($httpCode != self::S200_OK) {
            $message = isset($json->message) ? $json->message : "Unknown error. Http code $httpCode.";
            throw new JoiApyException($message, $httpCode);
        }

        return $json;
    }





}


class JoiApyException extends Exception
{

}
