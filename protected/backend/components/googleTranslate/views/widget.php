<div class="modal fade " tabindex="-1" role="dialog" id="googleTranslate-widget-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id' => 'googleTranslate-widget-form',
            ));
            ?>
            <div class="form">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" id="edit-modal-content">
                    <div style="text-align: center;">
                        <input type="text" name="text" value="" placeholder="NIP" style="width: 103px;"/>
                    </div>
                    <br/>
                </div>

            </div>
            <div class="modal-footer-loader" style="text-align: center; display: none;">
                ...PROCESSING...
            </div>
            <div class="modal-footer" style="text-align: center;">
                <?php echo TbHtml::submitButton('Go!', array('class' => 'btn-lg btn-primary')); ?>
            </div>
            <?php
            $this->endWidget();
            ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('#googleTranslate-widget-open-button').on('click', function(){
            $('#googleTranslate-widget-modal .modal-footer').show();
            $('#googleTranslate-widget-modal .modal-footer-loader').hide();
            $('#googleTranslate-widget-modal').modal();
        });

        $('#googleTranslate-widget-form input[type="text"]').on('change', function()
        {
            $('#googleTranslate-widget-form input[type="text"]').not($(this)).val('');
        });

        $('#googleTranslate-widget-form').on('submit', function(e){

            $('#googleTranslate-widget-modal .modal-footer').hide();
            $('#googleTranslate-widget-modal .modal-footer-loader').show();
            e.preventDefault();

            var base_attr_name = '<?= $formAttrNames;?>';
            var attrMap = JSON.parse('<?= CJSON::encode($attrMap);?>');

            $('.googleTranslate-widget-result-div').fadeOut();

            $.ajax({
                method: "POST",
                url: '<?= Yii::app()->createUrl('/site/googleTranslate.GoogleTranslateActions');?>',
                data: $('#googleTranslate-widget-form').serialize(),
                dataType: "json",
            })
                .done(function(result){

                    $('#googleTranslate-widget-modal').modal('hide');

                    if(!result.success)
                        alert("Could not find data!");
                    else {

                        var data = result.data;

                        var mode = result.mode;

                        var $table = $('<table>');
                        $table.addClass('table');
                        $table.addClass('table-bordered');
                        $table.addClass('table-striped');
                        $table.addClass('table-condensed');
                        $table.css('width', '500px');
                        $table.css('margin-bottom', '0');

                        for (var property in data) {
                            if (data.hasOwnProperty(property)) {

                                var targetProperty = property;
                                if(attrMap.hasOwnProperty(property)) {
                                    targetProperty = attrMap[property];
                                }

                                $table.append('<tr><th style="text-align: right;">' + targetProperty + '</th><td>' + data[property] + '</td></tr>');

                                if(mode > 1) {

                                    if(mode == 2)
                                        if($('[name="' + base_attr_name + '[' + targetProperty + ']"]').val() != '')
                                            continue;

                                    $('[name="' + base_attr_name + '[' + targetProperty + ']"]').val(data[property]);
                                }
                            }
                        }

                        $div = $('<div>');
                        $div.addClass('well');
                        $div.addClass('googleTranslate-widget-result-div');
                        $div.css('margin', '10px auto');

                        $div.html($table);

                        $('#googleTranslate-widget-open-button').after($div);
                    }

                })
                .fail(function(ex){
                    console.log(ex);
                    alert("Error...");
                });
        });
    });
</script>