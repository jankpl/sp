<?php

/**
 * CourierCodSettledScannerForm class.
 * CourierCodSettledScannerForm is the data structure for CourierCodSettledScannerForm form.
 */
class CourierCodSettledScannerForm extends CFormModel
{
    public $package_ids;
    public $type;
    public $settle_date;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            ['package_ids, type', 'required'],
            [ 'settle_date', 'date', 'format' => 'yyyy-mm-dd'],
            [ 'type', 'in', 'range' => array_keys(CourierCodSettled::listType())],
        );
    }



    /**
     * Function return model or array of models of found packages
     *
     * @return Courier[]|NULL|boolean
     */
    public function returnCourierModels($withChildren = false)
    {
        if(!is_array($this->package_ids))
            return [];

        $models = [];

        /* @var $model Courier */
        foreach($this->package_ids AS $key => $package_id)
        {
            $package_id = trim($package_id);

            if($package_id == '')
                continue;

            $model = Courier::model()->findByAttributes(['local_id' => $package_id]);

            if($model === NULL)
                $model = CourierExternalManager::findCourierIdsByRemoteId($package_id, true);

            if(is_array($model))
                $model = array_shift($model);

            if($model === NULL OR is_array($model) && !S_Useful::sizeof($model))
                $models[$key] = NULL;
            else if($model->cod) {

                if($withChildren && $model->packages_number > 1)
                {
                    $parent = $model;
                    $children = [];
                    if($model->courierTypeInternal)
                    {
                        if($model->courierTypeInternal->family_member_number > 1)
                            $parent = $model->courierTypeInternal->parentCourier;
                        $children = Courier::model()->with('courierTypeInternal')->findAll('courierTypeInternal.parent_courier_id = :id', [':id' => $parent->id]);
                    }
                    else if($model->courierTypeU)
                    {
                        if($model->courierTypeU->family_member_number > 1)
                            $parent = $model->courierTypeU->parentCourier;
                        $children = Courier::model()->with('courierTypeU')->findAll('courierTypeU.parent_courier_id = :id', [':id' => $parent->id]);
                    }

                    $models[$key] = $parent;

                    foreach($children AS $item)
                        $models[] = $item;

                } else
                    $models[$key] = $model;

            }
            else
                $models[$key] = false;
        }


        return $models;
    }


}