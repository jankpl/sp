<?php

class PhpErrorToYiiLog
{
    public static function run($sourceFile, $logPrefix)
    {
        if(is_file($sourceFile) && filesize($sourceFile) > 0) {
            $fp = fopen($sourceFile, 'r+');
            flock($fp, LOCK_EX);
            $str = fread($fp, filesize($sourceFile));
            ftruncate($fp, 0);
            flock($fp, LOCK_UN);
            fclose($fp);

            Yii::log($logPrefix . ': ' . $str, CLogger::LEVEL_ERROR);
            file_put_contents('_' . $sourceFile, $str, FILE_APPEND);
        }
    }
}