<?php
/* @var $this PostalOperatorController */
/* @var $model PostalOperator */

$this->breadcrumbs=array(
	'Postal Operators'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create PostalOperator', 'url'=>array('create')),
);
?>

<h1>Manage Postal Operators</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'postal-operator-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'date_entered',
		'date_updated',
		[
			'name' => 'broker_id',
			'value' => 'PostalOperator::getBrokerList()[$data->broker_id]',
			'filter' => PostalOperator::getBrokerList(),
		],
		[
			'name' => 'label_mode',
			'value' => 'PostalOperator::getStampModeList()[$data->label_mode]',
			'filter' => PostalOperator::getStampModeList(),
		],
        [
            'name' => 'partner_id',
            'value' => 'Partners::getPartnerNameById($data->partner_id)',
            'filter' => Partners::getParntersList(),
        ],
		[

			'name' => 'stat',
			'value' => 'S_Status::stat($data->stat)',
			'filter' => CHtml::listData(S_Status::listStats(), 'id', 'name'),
		],
		/*
		'params',
		'sticker',
		'stat',
		*/
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}',
		),
	),
)); ?>
