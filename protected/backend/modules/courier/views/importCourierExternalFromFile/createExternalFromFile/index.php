<?php echo $this->renderPartial('createExternalFromFile/_uploadForm', array(
    'model' => $fileModel,
)); ?>


<hr/>


<div style="background: url(<?php echo Yii::app()->baseUrl;?>/images/layout/mainBg.png); position: fixed; left: 0; right: 0; top: 0; bottom: 0; z-index: 999; display: none;" id="fader">
    <div style="background: url(<?php echo Yii::app()->baseUrl;?>/images/layout/preloader.gif) no-repeat center center; position: absolute; left: 0; right: 0; top: 0; bottom: 0; "></div>
    <div style="position: absolute; left: 0; right: 0; top: 100px; text-align: center; font-size: 80px; color: orange;" id="fader-text"></div>
    <div style="position: absolute; left: 0; right: 0; bottom: 100px; text-align: center; font-size: 80px; color: orange;" id="fader-progress"></div>
</div>


<form id="form" style="display: none; text-align: center;">
    <!--    <input type="button" id="action" value="go"/>-->
    <label for="part">Select part of file:</label>
    <select name="part" id="part"></select>
    <input type="hidden" id="step" name="step" value="1"/>

    <input type="hidden" name="totalParts" id="totalParts"/>
    <input type="hidden" name="hash" id="hash"/>
    <div id="data"></div>

</form>


<script>
   // $("#action").click(action);

    $fader = $("#fader");
    $faderText = $("#fader-text");
    $faderProgress = $("#fader-progress");

    $("#part").change(action);

   function onSaveButtonClick()
   {

           $("#mode").val($(this).attr('data-mode'));

           action(4);


   }

    function action(forceStep)
    {

        var hash = '';

        forceStep = parseInt(forceStep);

        var step = $("#step").val();
        var part = $("#part").val();
        var parts = parseInt($("#totalParts").val());

        if(!isNaN(forceStep))
        {
            step = forceStep;
            $("#step").val(step);
        }

        console.log(isNaN(forceStep));
        console.log('START: ' + step);

        if(step == 3)
        {
            $faderProgress.html("");
            $faderText.html("loading...");
        }
        else if(step == 4)
        {
            $faderProgress.html("");
            $faderText.html("processing...");
        }

        $fader.fadeIn();
        console.log('DATA:');
        console.log(jQuery("#form").serialize());
        <?php echo CHtml::ajax(
        array(
            'url' => Yii::app()->createUrl('/courier/importCourierExternalFromFile/index', array('ajax' => 1)),
            'dataType' => 'json',
            'type' => 'post',
            'data' => 'js:jQuery("#form").serialize()',
            'async' => true,
            'success' => 'js:function(result){

        console.log("RESPONSE RECEIVED: " + step);

        if(step == 3)
        {

        }
        else if(step == 4)
        {

        } else {

            $faderProgress.html($faderProgress.text() + ".");

            if(isNaN(parts))
                $faderText.html("calculating...");
            else
                $faderText.html(part + "/" + (parts));
       }

    if(step == 1)
    {
        hash = result.hash;
        parts = result.parts;

        $("#totalParts").val(parts);


        for (var i = 1; i <= parseInt(parts); i++) {
        $("#part").append($("<option>", { value : i })
          .text(i + "/" + parts));
            }

            $("#form").show();

        $("#hash").val(hash);

        $("#part").val(1);

        $("#step").val(2);
        action();
    }
    else if(step == 2)
    {
        var totalPartss = parseInt($("#totalParts").val());
        var resultPart = parseInt(result.part);

        if(resultPart < $("#totalParts").val())
        {

        var nextPart = resultPart + 1;

         $("#part").val((nextPart));

          action();
        } else {
           $("#step").val(3);
           $("#part").val(1);
            action();
        }
    }
    else if(step == 3)
    {
        $("#data").html(result.html);

        $(".save-packages").unbind("click").bind("click", onSaveButtonClick);
        $fader.fadeOut();

    }
    else if(step == 4)
    {
        $("#data").html(result.html);
        $(".save-packages").unbind("click").bind("click", onSaveButtonClick);

        $("#step").val(3);
        $fader.fadeOut();
    }



            }',
            'error' => 'js:function(data){
                console.log(data);

            }',
        )
    );?>


    }
</script>

<?php if($uploadSuccess === true):?>
    <script>
        $(document).ready(function(){
            action();
        });
    </script>
<?php endif;?>