<?php
/* @var $this PricingController */
/* @var $priceValues Pricing[] */
?>




<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-price-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
    ));
    ?>


    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>
    <?php

    echo CHtml::submitButton("More",array(
        'name'=>'add_more',
        'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
    ));

    ?>

    <?php

    foreach($priceValues AS $item)
    {
        $firstItem = $item;
        break;
    }


    ?>
    <table class="list hTop" style="width: 700px;">
        <tr>
            <td style="width: 40%;"><?php echo $form->labelEx($firstItem,'top_weight'); ?></td>
            <td style="width: 40%;"><?php echo $form->labelEx($firstItem,'price'); ?></td>
            <td><span style="font-weight: bold;">action</span></td>
        </tr>
        <tr><td></td><td></td><td></td></tr>
        <?php
        foreach($priceValues AS $key => $item)
        {
            $this->renderPartial('_price_value', array(
                'model' => $item,
                'form' => $form,
                'i' => $key,
            ));
        }
        ?>

    </table>


    <?php

    echo CHtml::submitButton("Sort",array(
        'name'=>'sort',
        'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
    ));

    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>

    <?php echo CHtml::script('function removeMe(i)
{
    if($("._price_item").length > 1)
    {
        $("#_price_" + i).remove();
        }
            else
        alert("You cannot delete all prices!");
}
'); ?>
</div><!-- form -->