<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'profile']),
    ));
    ?>


    <h3><?php echo Yii::t('user', 'Dane konta');?></h3>
    <div class="row">
        <?php echo $form->labelEx($model,'login'); ?>
        <?php echo $form->textField($model, 'login', array('disabled' => 'disabled')); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model, 'email', array('maxlength' => 64)); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'pass'); ?>
        <?php echo $form->passwordField($model, 'pass', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'pass'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'passCompare'); ?>
        <?php echo $form->passwordField($model, 'passCompare', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'passCompare'); ?>
    </div><!-- row -->
    <p class="note"><?php echo Yii::t('user','Aby nie zmienić hasła, pozostaw oba pola haseł puste');?>.</p>

    <h3><?php echo Yii::t('user', 'Twoje dane');?></h3>

    <div class="row">
        <?php echo $form->labelEx($model,'price_currency_id'); ?>
        <?php echo $form->dropDownList($model, 'price_currency_id',CHtml::listData(PriceCurrency::model()->findAll(),'id','symbol'), array('disabled' => 'disabled')); ?>
        <?php echo $form->error($model,'price_currency_id'); ?>
    </div><!-- row -->

    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model, 'type', CHtml::listData(
            array(
                array('value' => User::TYPE_PRIVATE, 'name' => User::getTypeName(User::TYPE_PRIVATE)),
                array('value' => User::TYPE_COMPANY, 'name' => User::getTypeName(User::TYPE_COMPANY))
            ),'value','name'), array('disabled' => 'disabled')); ?>
    </div><!-- row -->

    <?php if($model->type == User::TYPE_PRIVATE):?>
        <div class="row">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
            <?php echo $form->error($model,'name'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model,'surname'); ?>
            <?php echo $form->textField($model, 'surname', array('maxlength' => 45)); ?>
            <?php echo $form->error($model,'surname'); ?>
        </div><!-- row -->
    <?php else:?>
        <div class="row">
            <?php echo $form->labelEx($model,'company'); ?>
            <?php echo $form->textField($model, 'company', array('maxlength' => 45)); ?>
            <?php echo $form->error($model,'company'); ?>
        </div><!-- row -->
    <?php endif;?>
    <div class="row">
        <?php echo $form->labelEx($model,'country_id'); ?>
        <?php echo $form->dropDownList($model,'country_id',CHtml::listData(CountryList::model()->findAll(),'id','trName')); ?>
        <?php echo $form->error($model,'country_id'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'city'); ?>
        <?php echo $form->textField($model, 'city', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'city'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'zip_code'); ?>
        <?php echo $form->textField($model, 'zip_code', array('maxlength' => 6)); ?>
        <?php echo $form->error($model,'zip_code'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'address_line_1'); ?>
        <?php echo $form->textField($model, 'address_line_1', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'address_line_1'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'address_line_2'); ?>
        <?php echo $form->textField($model, 'address_line_2', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'address_line_2'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'tel'); ?>
        <?php echo $form->textField($model, 'tel', array('maxlength' => 20)); ?>
        <?php echo $form->error($model,'tel'); ?>
    </div><!-- row -->
    <?php if($model->type == User::TYPE_COMPANY):?>
        <div class="row">
            <?php echo $form->labelEx($model,'nip'); ?>
            <?php echo $form->textField($model, 'nip', array('maxlength' => 13, 'disabled' => 'disabled')); ?>
            <?php echo $form->error($model,'nip'); ?>
        </div><!-- row -->
    <?php endif; ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Save'));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->

<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-form-accounting-custom-emails',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'profile']),
    ));
    ?>

    <h3><?php echo Yii::t('user', 'Dodatkowe adresy e-mail powiadomień księgowych');?></h3>
    <div class="row">
        <?php echo $form->labelEx($modelCAE,'email_1'); ?>
        <?php echo $form->textField($modelCAE, 'email_1'); ?>
        <?php echo $form->error($modelCAE, 'email_1'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($modelCAE,'email_2'); ?>
        <?php echo $form->textField($modelCAE, 'email_2'); ?>
        <?php echo $form->error($modelCAE, 'email_2'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($modelCAE,'email_3'); ?>
        <?php echo $form->textField($modelCAE, 'email_3'); ?>
        <?php echo $form->error($modelCAE, 'email_3'); ?>
    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Save'));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->

<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-form-return-report-emails',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'profile']),
    ));
    ?>

    <h3><?php echo Yii::t('user', 'Dodatkowe adresy e-mail do raportów o zwrotach');?></h3>
    <div class="row">
        <?php echo $form->labelEx($modelCRRE,'email_1'); ?>
        <?php echo $form->textField($modelCRRE, 'email_1'); ?>
        <?php echo $form->error($modelCRRE, 'email_1'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($modelCRRE,'email_2'); ?>
        <?php echo $form->textField($modelCRRE, 'email_2'); ?>
        <?php echo $form->error($modelCRRE, 'email_2'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($modelCRRE,'email_3'); ?>
        <?php echo $form->textField($modelCRRE, 'email_3'); ?>
        <?php echo $form->error($modelCRRE, 'email_3'); ?>
    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Save'));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->