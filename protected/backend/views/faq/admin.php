<?php



$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu=array(
    array('label'=>'Create ' . $model->label(2), 'url'=>array('create')),
);

?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'faq-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
        'name',
		'date_entered',
		'date_updated',
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update} {arrow_top} {arrow_top_empty} {arrow_bottom} {arrow_bottom_empty} {status} {status_a}',
            'htmlOptions'=> array('style'=>'width: 80px'),
            'buttons'=>array(
                'status'=>array(
                    'visible' => '$data->stat==1',
                    'label'=>'deactivate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_active.png',
                    'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/stat",array("id" => $data->id))',
                ),
                'status_a'=>array(
                    'visible' => '$data->stat==0',
                    'label'=>'activate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_not_active.png',
                    'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/stat",array("id" => $data->id))',
                ),
                'arrow_top'=>array(
                    'visible' => '!$data->isFirst()',
                    'label'=> 'move upward',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/arrow_top.gif',
                    'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/moveT",array("id" => $data->id))',
                ),
                'arrow_top_empty'=>array(
                    'visible' => '$data->isFirst()',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/arrow_empty.gif',
                ),
                'arrow_bottom'=>array(
                    'visible' => '!$data->isLast()',
                    'label'=>'move downward',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/arrow_bottom.gif',
                    'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/moveB",array("id" => $data->id))',
                ),
                'arrow_bottom_empty'=>array(
                    'visible' => '$data->isLast()',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/arrow_empty.gif',
                ),
            ),
        ),
	),
)); ?>