<?php

Yii::import('application.models._base.BaseUserGroup');

class UserGroup extends BaseUserGroup
{

    const DEFAULT_GROUP_FOR_PLI = 59;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }


    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),

            array('admin_id', 'numerical', 'integerOnly' => true),
            array('admin_id', 'in', 'range' => CHtml::listData(Admin::model()->findAll(),'id', 'id')),
            array('admin_id', 'default', 'value' => Yii::app()->user->id),

            array('name, date_entered', 'required'),
            array('name', 'length', 'max'=>45),
            array('name', 'unique'),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, date_updated', 'safe', 'on'=>'search'),
        );
    }

    public function getMembers()
    {
        $models = User::model()->findAllBySql('
        SELECT *
        FROM user
        WHERE user.id
        IN
        (SELECT user_id FROM user_has_user_group WHERE user_group_id = :group_id)',
            array(':group_id' => $this->id));

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
        {
            foreach($models AS $key => $model)
            {
                if($model->salesman_id != Yii::app()->user->model->salesman_id)
                    unset($models[$key]);
            }
        }

        return $models;

    }

    public function getNonMembers()
    {

        $models = User::model()->findAllBySql('
        SELECT *
        FROM user
        WHERE user.id
        NOT IN
        (SELECT user_id FROM user_has_user_group)'
        );

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
        {
            foreach($models AS $key => $model)
            {
                if($model->salesman_id != Yii::app()->user->model->salesman_id)
                    unset($models[$key]);
            }
        }

        return $models;
    }

    public function removeMember($id)
    {
        $model = UserHasUserGroup::model()->find('user_id=:user_id AND user_group_id=:user_group_id', array(':user_id' => $id, ':user_group_id' => $this->id));

        if($model === null)
            return false;

        if($model->delete()) {
            UserLog::addToLog('Removed from user group #'.$this->id, $id);
            return true;
        }
        else
            return false;
    }

    public function addMember($id)
    {
        $user = User::model()->findByPk($id);

        if($user === null)
            return false;

        $model = new UserHasUserGroup();
        $model->user_group_id = $this->id;
        $model->user_id = $id;

        if($model->save()) {
            UserLog::addToLog('Added to user group #'.$this->id, $id);
            return true;
        }
        else
            return false;
    }

    public function howManyMembers()
    {
        $models = UserHasUserGroup::model()->findAll('user_group_id=:group_id', array(':group_id' => $this->id));
        return S_Useful::sizeof($models);

    }

    public static function listGroups()
    {
        $models = self::model()->findAll(['order' => 'name ASC']);
        return $models;
    }

    public function getAdminBlockNotifySMS()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from('user_group_options');
        $cmd->where('name = "blockSmsAdmin"');
        $cmd->andWhere('value = 1');
        $cmd->andWhere('user_group_id = :group_id', array(':group_id' => $this->id));
        $result = $cmd->queryScalar();

        return $result;
    }

    public function setAdminBlockNotifySms($value)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->delete('user_group_options', 'name = "blockSmsAdmin" AND user_group_id = :user_id', array(':user_id' => $this->id));


        if($value)
        {
            $cmd->insert('user_group_options', array('name' => 'blockSmsAdmin', 'user_group_id' => $this->id, 'value' => 1));
        }

        return true;
    }

    public function getNameWithId()
    {
        return $this->name.' (#'.$this->id.')';
    }

    public function getNameWithAdmin()
    {
        return $this->id.') '. $this->name.' ['.($this->admin_id ? $this->admin->login : '-').']';
    }

    public static function getListForThisAdmin()
    {
        return self::model()->forThisAdmin()->findAll();
    }

    public function scopes()
    {
        return array(
            'forThisAdmin' => [
                'condition'=> Yii::app()->params['backend'] && AdminRestrictions::isRoutingAdmin() ? '' : 'admin_id = '.intval(Yii::app()->user->id),
            ],
        );
    }

}