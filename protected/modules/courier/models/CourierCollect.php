<?php

Yii::import('application.modules.courier.models._base.BaseCourierCollect');

class CourierCollect extends BaseCourierCollect
{
    const OPERATOR_UPS = 1;
    const OPERATOR_INPOST = 2;
    const OPERATOR_GLS = 3;
    const OPERATOR_DHL = 4;
    const OPERATOR_DPDPL = 5;
    const OPERATOR_APACZKA = 6;


    const STAT_WAITING = 0;
    const STAT_SUCCESS = 1;
    const STAT_ERROR = 9;

    protected $_dataObj = NULL;


    public $_gls_consigns_ids;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function getOperators()
    {
        $data = array(
            self::OPERATOR_UPS => 'UPS',
            self::OPERATOR_INPOST => 'Inpost',
            self::OPERATOR_GLS => 'GLS',
            self::OPERATOR_DHL => 'DHL',
            self::OPERATOR_DPDPL => 'DPDPL',
            self::OPERATOR_APACZKA => 'Apaczka',
        );

        return $data;
    }

    public static function getStats()
    {
        $data = array(
            self::STAT_WAITING => 'Waiting',
            self::STAT_SUCCESS => 'OK',
            self::STAT_ERROR => 'Fail',
        );

        return $data;
    }

    public static function serialize($data)
    {
        $data = serialize($data);
//        $data = base64_encode($data);

        return $data;
    }

    public static function unserialize($data)
    {
//        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    /**
     * Unserialize additional params (ex. Preffered pickup time)
     * @return bool
     */
    public function afterFind()
    {
        if($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if(is_array($this->params))
            foreach($this->params AS $key => $item)
            {
                try {
                    $this->$key = $item;
                }
                catch (Exception $ex)
                {}
            }

        parent::afterFind();
    }

    /**
     * Serialize additional params (ex. Preffered pickup time)
     * @return bool
     */

    public function beforeSave()
    {

        $this->params = array(
            '_gls_consigns_ids' => $this->_gls_consigns_ids,
        );

        // serialize only if not all values are empty
        if(array_filter($this->params))
            $this->params = self::serialize($this->params);
        else
            $this->params = NULL;

        return parent::beforeSave();
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            )
        );
    }

    public function rules() {
        return array(
            array('courier_id, stat', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('courier_id', 'length', 'max'=>10),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, courier_id, data, log, stat, collect_operator_id', 'safe', 'on'=>'search'),
        );
    }

    public function getPackagesNumber()
    {
        $data = self::unserialize($this->data);
        return $data->PickupPiece->Quantity;
    }

    public function getLogNice()
    {
        if($this->stat == self::STAT_SUCCESS)
        {
            return '';
        } else {
            $data = self::unserialize($this->log);
            $data = $data['desc'];

            return $data;
        }
    }

    public function getPrn()
    {
        if($this->stat == self::STAT_SUCCESS)
        {
            $data = self::unserialize($this->log);
            $data = $data['prn'];

            return $data;
        }
    }

    public function getDataAsObj()
    {
        if($this->_dataObj === NULL)
            $this->_dataObj = self::unserialize($this->data);

        return $this->_dataObj;
    }

    public function runOrdering()
    {


        if($this->courier->getType() == Courier::TYPE_INTERNAL)
        {

            if($this->collect_operator_id == self::OPERATOR_UPS) {
                $upsCollect = Yii::createComponent('application.components.UpsCollectClient');
                $return = $upsCollect->callCollectionForCourierInternal($this->courier->courierTypeInternal);
            }
            else if($this->collect_operator_id == self::OPERATOR_INPOST)
                $return = Inpost::runCollectionForCourierInternal($this->courier->courierTypeInternal);
//            else if($this->collect_operator_id == self::OPERATOR_GLS) {
//                $return = GlsClient::runCollection($this->_gls_consigns_ids);
//            }
            else
                return false;

            // unnown error
            if(!$return)
            {
                $this->stat = self::STAT_ERROR;
                $this->log = self::serialize(array('desc' => 'Unknow own error'));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            }
            else if(isset($return['error']))
            {
                $this->stat = self::STAT_ERROR;
                $this->log = self::serialize(array('desc' => $return['error']));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            } else {
                $this->stat = self::STAT_SUCCESS;
                $this->log = self::serialize(array('prn' => $return['prn'], 'response' => $return['desc']));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            }

        }
        else if($this->courier->getType() == Courier::TYPE_U)
        {

            if($this->collect_operator_id == self::OPERATOR_UPS) {
                $upsCollect = Yii::createComponent('application.components.UpsCollectClient');
                $return = $upsCollect->callCollectionForCourierU($this->courier->courierTypeU);
            }
            else if($this->collect_operator_id == self::OPERATOR_INPOST)
                $return = Inpost::runCollectionForCourierU($this->courier->courierTypeU);
//            else if($this->collect_operator_id == self::OPERATOR_GLS) {
//                $return = GlsClient::runCollection($this->_gls_consigns_ids);
//            }
            else
                return false;

            // unnown error
            if(!$return)
            {
                $this->stat = self::STAT_ERROR;
                $this->log = self::serialize(array('desc' => 'Unknow own error'));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            }
            else if(isset($return['error']))
            {
                $this->stat = self::STAT_ERROR;
                $this->log = self::serialize(array('desc' => $return['error']));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            } else {
                $this->stat = self::STAT_SUCCESS;
                $this->log = self::serialize(array('prn' => $return['prn'], 'response' => $return['desc']));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            }

        }
        else if($this->courier->getType() == Courier::TYPE_DOMESTIC)
        {

            if($this->collect_operator_id == self::OPERATOR_UPS) {
                $upsCollect = Yii::createComponent('application.components.UpsCollectClient');
                $return = $upsCollect->callCollectionForCourierDomestic($this->courier->courierTypeDomestic);
            } else if($this->collect_operator_id == self::OPERATOR_INPOST)
                $return = Inpost::runCollectionForCourierDomestic($this->courier->courierTypeDomestic);
//            else if($this->collect_operator_id == self::OPERATOR_GLS)
//            {
//                $return =  GlsClient::runCollection($this->_gls_consigns_ids);
//            }
            else
                return false;


            // unnown error
            if(!$return)
            {
                $this->stat = self::STAT_ERROR;
                $this->log = self::serialize(array('desc' => 'Unknow own error'));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            }
            else if(isset($return['error']))
            {
                $this->stat = self::STAT_ERROR;
                $this->log = self::serialize(array('desc' => $return['error']));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            } else {
                $this->stat = self::STAT_SUCCESS;
                $this->log = self::serialize(array('prn' => $return['prn'], 'response' => $return['desc']));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            }

        }
//        else if($this->courier->getType() == Courier::TYPE_INTERNAL_SIMPLE)
//        {
//
////        $upsCollect->callCollectionForCourierInternal($courierTypeInternal);
//
//            if($this->collect_operator_id == self::OPERATOR_UPS) {
//                $upsCollect = Yii::createComponent('application.components.UpsCollectClient');
//                $return = $upsCollect->callCollectionForCourierInternalSimple($this->courier->courierTypeInternalSimple);
//            }
////            else if($this->collect_operator_id == self::OPERATOR_GLS)
////            {
////                $return =  GlsClient::runCollection($this->_gls_consigns_ids);
////            }
//            else
//                return false;
//
//            // unnown error
//            if(!$return)
//            {
//                $this->stat = self::STAT_ERROR;
//                $this->log = self::serialize(array('desc' => 'Unknow own error'));
//                $this->data = self::serialize($return['data']);
//                $this->update('log', 'stat', 'date_updated', 'data');
//            }
//            else if(isset($return['error']))
//            {
//                $this->stat = self::STAT_ERROR;
//                $this->log = self::serialize(array('desc' => $return['error']));
//                $this->data = self::serialize($return['data']);
//                $this->update('log', 'stat', 'date_updated', 'data');
//            } else {
//                $this->stat = self::STAT_SUCCESS;
//                $this->log = self::serialize(array('prn' => $return['prn'], 'response' => $return['desc']));
//                $this->data = self::serialize($return['data']);
//                $this->update('log', 'stat', 'date_updated', 'data');
//            }
//        }
        else if($this->courier->getType() == Courier::TYPE_U)
        {

            if($this->collect_operator_id == self::OPERATOR_UPS) {
                $upsCollect = Yii::createComponent('application.components.UpsCollectClient');
                $return = $upsCollect->callCollectionForCourierU($this->courier->courierTypeU);
            }
            else
                return false;

            // unnown error
            if(!$return)
            {
                $this->stat = self::STAT_ERROR;
                $this->log = self::serialize(array('desc' => 'Unknow own error'));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            }
            else if(isset($return['error']))
            {
                $this->stat = self::STAT_ERROR;
                $this->log = self::serialize(array('desc' => $return['error']));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            } else {
                $this->stat = self::STAT_SUCCESS;
                $this->log = self::serialize(array('prn' => $return['prn'], 'response' => $return['desc']));
                $this->data = self::serialize($return['data']);
                $this->update('log', 'stat', 'date_updated', 'data');
            }

        }



    }


    /**
     * Find CourierCollect model by courier ID
     * @param $id Courier id
     * @return bool|null|CourierCollect
     */
    public static function findForCourierId($id)
    {
        $courier = Courier::model()->findByPk($id);
        if($courier === NULL)
            return false;

        $parent_id = NULL;
        if($courier->courierTypeInternal)
        {
            if($courier->courierTypeInternal->parent_courier_id != '')
                $parent_id = $courier->courierTypeInternal->parent_courier_id;
        }
        else if($courier->courierTypeU)
        {
            if($courier->courierTypeU->parent_courier_id != '')
                $parent_id = $courier->courierTypeU->parent_courier_id;
        }


        if($parent_id !== NULL)
            $model = self::model()->find('courier_id = :courier_id OR courier_id = :parent_id', array(':courier_id' => $id, ':parent_id' => $parent_id));
        else
            $model = self::model()->find('courier_id = :courier_id', array(':courier_id' => $id));

        return $model;

    }

    /**
     * Method creates collection item for already done collection call (for example - DHL calls collection together with label)
     * @param $confirmation_id
     * @param $courier_id
     * @param $operator_id
     * @return bool
     */
    public static function createReadyCollection($confirmation_id, $courier_id, $operator_id)
    {
        $courierCollect = new self();
        $courierCollect->courier_id = $courier_id;
        $courierCollect->stat = self::STAT_SUCCESS;
        $courierCollect->collect_operator_id = $operator_id;
        $courierCollect->log =  self::serialize(array('prn' => $confirmation_id, 'response' => $confirmation_id));
        return $courierCollect->save();
    }


}