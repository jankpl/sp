<?php

Yii::import('application.modules.courier.models._base.BaseCourierDhlRemoteAreas');

class CourierDhlRemoteAreas extends BaseCourierDhlRemoteAreas
{
	CONST PRICE_GROUP_1 = 1;
	CONST PRICE_GROUP_2 = 2;
	CONST PRICE_GROUP_3 = 3;
	CONST PRICE_GROUP_4 = 4;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('country_list_id, price_group', 'required'),
			array('country_list_id, zip_from, zip_to, stat', 'numerical', 'integerOnly'=>true),
			array('zip_txt', 'length', 'max'=>16),
			array('zip_from, zip_to', 'numerical'),
			array('date_updated', 'safe'),
			array('date_updated, zip_from, zip_to, zip_txt', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, date_entered, date_updated, country_list_id, numerical, literal, zip_from, zip_to, zip_txt, price_id, stat, _own_price', 'safe', 'on'=>'search'),
		);
	}

	public static function getPricesGroups()
	{
		$price[self::PRICE_GROUP_1] = 'GR1 (101 PLN/22 EUR/16 GBP)';

		$price[self::PRICE_GROUP_2] = 'GR2 (45 PLN/12 EUR/9 GBP)';

		$price[self::PRICE_GROUP_3] = 'GR3 (90 PLN/24 EUR/18 GBP)';

		$price[self::PRICE_GROUP_4] = 'GR4 (13 PLN/13 EUR/13 GBP)';

		return $price;
	}


	public static function getPricesByGroup()
	{
		$price[self::PRICE_GROUP_1] =
			[
				Price::PLN => 101,
				Price::EUR => 22,
				Price::GBP => 16,
			];

		$price[self::PRICE_GROUP_2] =
			[
				Price::PLN => 45,
				Price::EUR => 12,
				Price::GBP => 9,
			];

		$price[self::PRICE_GROUP_3] =
			[
				Price::PLN => 90,
				Price::EUR => 24,
				Price::GBP => 18,
			];

		$price[self::PRICE_GROUP_4] =
			[
				Price::PLN => 13,
				Price::EUR => 13,
				Price::GBP => 13,
			];

		return $price;
	}

	public function getPrice($currency)
	{
		if($this->price_id === NULL)
		{
			$price = self::getPricesByGroup()[$this->price_group][$currency];

			return $price;
		} else {

			$price = $this->price->getValue($currency);
			if($price instanceof PriceValue)
				return $price->value;
			else
				return 0;
		}
	}

	public function saveWithPrice($ownTransaction = true)
	{
		if($ownTransaction)
			$transaction = Yii::app()->db->beginTransaction();

		$zip_from = $this->zip_from;
		$zip_to = $this->zip_to;
		$zip_txt = $this->zip_txt;

		if($zip_from == '' AND $zip_to == '' AND $zip_txt == '')
			return false;
		else if($zip_from == '' AND $zip_to == '')
		{
			$zip = $zip_txt;
		}
		else if($zip_from == '' AND $zip_to != '')
		{
			$zip = $zip_to.'-'.$zip_to;
		}
		else if($zip_from != '' AND $zip_to == '')
		{
			$zip = $zip_from.'-'.$zip_from;
		} else
			$zip = $this->zip_from.'-'.$this->zip_to;

		$literal = !preg_match('/[0-9]/',$zip);
		$numerical = !preg_match('/[A-Za-z]/',$zip);

		if($numerical)
			$this->zip_txt = '';

		if($literal)
			$this->zip_from = $this->zip_to = '';

		$this->literal = $literal;
		$this->numerical = $numerical;

		$errors = false;

		if($this->price === NULL)
		{
			$temp = self::model()->findByPk($this->id);
			if(is_array($temp->price->priceValues) && S_Useful::sizeof($temp->price->priceValues))
				foreach($temp->price->priceValues AS $item)
				{
					if($item instanceof PriceValue)
						$item->delete();
				}
			if($temp->price instanceof Price)
				$temp->price->delete();

			$this->price_id = NULL;
		} else {
			$price_id = $this->price->saveWithPrices();

			if(!$price_id)
				$errors = true;

			$this->price_id = $price_id;
		}

		if(!$this->save())
			$errors = true;

		if($errors)
		{
			if($ownTransaction)
				$transaction->rollback();

			return false;
		} else {
			if($ownTransaction)
				$transaction->commit();

			return true;
		}
	}



	public static function cleanZip($zip)
	{
		$zip = trim($zip);
		$zip = preg_replace('/[^a-zA-Z0-9]/', '', $zip);
		return $zip;
	}


	public static function findZip($zip, $country_list_id, $city)
	{
	    /* @ 09.07.2018
            DHL Remote Areas not in UE
         */
	    if(in_array($country_list_id, CountryList::getUeList()))
	        return false;

		$zip = self::cleanZip($zip);

		$numeric = is_numeric($zip);

		$CACHE_NAME = 'DHL_REMOTE_AREA_'.$zip.'_'.$country_list_id;
		$result = Yii::app()->cache->get($CACHE_NAME);

		if($result === false) {

			$cmd = Yii::app()->db->createCommand();
			$cmd->select('id');
			$cmd->from('courier_dhl_remote_areas');
			$cmd->where('country_list_id = :country_list_id', array(':country_list_id' => $country_list_id));
			if ($numeric) {
				$cmd->andWhere('zip_from <= :zip', array(':zip' => $zip));
				$cmd->andWhere('zip_to >= :zip', array(':zip' => $zip));
			} else {
				$cmd->andWhere('zip_txt = :zip', array(':zip' => $zip));
			}
			$cmd->limit = 1;
			$result = $cmd->queryScalar();

			if (!$result)
				$result = false;
			else {
				$result = self::model()->findByPk($result);
			}

			if($result)
				Yii::app()->cache->set($CACHE_NAME, $result, 60);
			else
				Yii::app()->cache->set($CACHE_NAME, -1, 60);
		} else if($result == -1)
			$result = false;

		return $result;

	}

	public function deleteWithPrices()
	{
		$transaction = Yii::app()->db->beginTransaction();

		try {
			foreach ($this->price->priceValues AS $key => $item) {
				$item->delete();
			}

			$price = $this->price;

			$this->delete();

			$price->delete();

		}
		catch (Exception $ex)
		{
			$ex->rollBack();
			return false;
		}
		$transaction->commit();
		return true;


	}
}