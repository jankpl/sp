<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'site-message',
        'enableAjaxValidation' => false,
    ));
    ?>

    <h3>Set site message</h3>
    <table class="table">
        <tr>
            <td>Select to activate</td>
            <td><?php echo CHtml::checkBox('SiteMessage[active]', Config::getData(Config::SITE_MESSAGE_ACTIVE)); ?></td>
        </tr>
        <tr>
            <td>Message content<br/>(polish version)</td>
            <td> <?php
                $this->widget('ext.editMe.widgets.ExtEditMe', array(
                    'name'=>'SiteMessage[text]',
                    'value'=> Config::getData(Config::SITE_MESSAGE_CONTENT),
                    'resizeMode' => 'vertical',
                    'width' => '580',
                    'baseHref' => Yii::app()->baseUrl.'/',
                    'toolbar' => array(
                        array(
                            'NewPage', 'Preview', 'Source',
                        ),
                        array(
                            'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',
                        ),
                        array(
                            'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat',
                        ),
                        array(
                            'Image','Link','NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Table', 'HorizontalRule','SpecialChar', 'Font',  'FontSize', 'TextColor',
                        ),
                    ),
                    'advancedTabs' => false,
                    'ckeConfig' => array(
                        'forcePasteAsPlainText' => true,
                        'allowedContent' => true,
                    )
                ));
                ?>
        </tr>
        <tr>
            <td>Message content<br/>(other versions)</td>
            <td> <?php
                $this->widget('ext.editMe.widgets.ExtEditMe', array(
                    'name'=>'SiteMessage[text_other]',
                    'value'=> Config::getData(Config::SITE_MESSAGE_CONTENT_OTHER),
                    'resizeMode' => 'vertical',
                    'width' => '580',
                    'baseHref' => Yii::app()->baseUrl.'/',
                    'toolbar' => array(
                        array(
                            'NewPage', 'Preview', 'Source',
                        ),
                        array(
                            'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',
                        ),
                        array(
                            'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat',
                        ),
                        array(
                            'Image','Link','NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Table', 'HorizontalRule','SpecialChar', 'Font',  'FontSize', 'TextColor',
                        ),
                    ),
                    'advancedTabs' => false,
                    'ckeConfig' => array(
                        'forcePasteAsPlainText' => true,
                        'allowedContent' => true,
                    )
                ));
                ?>
        </tr>
        <tr>
            <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleSiteMessage'));?>
            </td>
        </tr>
    </table>

    <?php
    $this->endWidget();
    ?>

</div>