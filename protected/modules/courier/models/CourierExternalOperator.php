<?php

Yii::import('application.modules.courier.models._base.BaseCourierExternalOperator');

class CourierExternalOperator extends BaseCourierExternalOperator
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    public function rules() {
        return array(
            array('stat', 'default', 'value'=> S_Status::ACTIVE),

            array('name', 'required'),
            array('name, stat', 'length', 'max'=>45),
            array('date_updated, description', 'safe'),
            array('date_updated, description', 'default', 'setOnEmpty' => true, 'value' => null),
            array('editable', 'default', 'setOnEmpty' => true, 'value' => 1),
            array('id, date_entered, date_updated, name, description, stat, editable', 'safe', 'on'=>'search'),
        );
    }

    public static function getNameById($id)
    {
        $model = self::model()->findByPk($id);

        return $model->name;
    }

    public static function findIdByText($text)
    {
        $cmd = Yii::app()->db->cache(60)->createCommand();
        $cmd->select('id');
        $cmd->from('courier_external_operator');
        $cmd->where('name = :text', array(':text' => $text));
        $cmd->orWhere('id = :text', array(':text' => $text));
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if(!$result)
            return NULL;

        return $result;
    }
}