<h2><?= Yii::t('postal', 'Przygotowanie listów do wysyłki');?></h2>

<?php
$this->widget('ShowPageContent', ['id' => 29]);
?>

<img src="<?= Yii::app()->baseUrl;?>/images/postal-przygotowanie.png" class="img-responsive" style="margin: 0 auto;"/>
<br/>
<br/>
<div class="text-center">
    <a href="<?= Yii::app()->createUrl('/postal/postal/orderCourier');?>" class="btn btn-xl btn-success"><?= Yii::t('postal', 'Zamów kuriera');?></a>
</div>
