<?php

$this->menu = array(
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),);

?>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<div style="word-break: break-all">


    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
    <?php $this->widget('backend.extensions.selgridview.BootSelGridView', array(
        'id' => 'courier-stat-grid',
        'selectableRows'=>2,
        'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'afterAjaxUpdate' => true, 'withChosenPlugin' => true, 'chosenSelector' => 'Dictionary[id]'), true),
        'selectionChanged'=>'function(id){  
                var number = $("#courier-stat-grid").selGridView("getAllSelection").length;
                $(".selected-no").html(number); 
         }',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            'id',
            [
                'name' => 'type',
                'filter' => Dictionary::getTypeList(),
                'value' => '$data->getTypeName()',
            ],
            [
                'name' => 'source_id',
                'filter' => false,
                'value' => '$data->getSourceIdName()',
            ],
            [
                'name' => 'source_text',
                'value' => 'mb_substr($data->source_text,0,30).(mb_strlen($data->source_text) > 30 ? "(...)" : "")',
            ],
            'date_entered',
            'date_updated',
            [
                'name' => 'admin_id',
                'filter' => false,
                'value' => '$data->admin ? $data->admin->login : ""',
            ],
            'hits',
            [
                'header' => 'Translated',
                'value' => '$data->dictionaryTr ? "Y" : "-"',
            ],
            array(
                'class' => 'CButtonColumn',
                'template' => '{update} {view}',
                'buttons' => array(
                    'update' => array(
                        'options' => array('class' => 'newWindow'),
                    ),
                ),
            ),
        ),
    )); ?>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
</div>
