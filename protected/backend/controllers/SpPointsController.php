<?php

class SpPointsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id, 'SpPoints'),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new SpPoints();

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $spPointTr = new SpPointsTr();
            $spPointTr->language_id = $item->id;
            $modelTrs[$item->id] = $spPointTr;
        }

        $model->addressData = new AddressData('with_email');

        if (isset($_POST['SpPoints'])) {
            $model->setAttributes($_POST['SpPoints']);

            $model->addressData->setAttributes($_POST['AddressData']);

            $trModels = [];
            foreach($_POST['SpPointsTr'] AS $key => $item)
            {
                if($item['title'] == '' AND $item['text'] == '')
                    continue;
                else
                {
                    $modelTr = $modelTrs[$item['language_id']];
                    $modelTr->setAttributes($item);
                    array_push($trModels, $modelTr);

                    $modelTrs[$item['language_id']] = $modelTr;
                }
            }

            $model->spPointsTrs = $trModels;


            if ($model->saveWithRelatedModels()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'SpPoints');

        if(isset($_POST['SpPoints']))
        {
            $model->attributes = $_POST['SpPoints'];
            $model->addressData->setAttributes($_POST['AddressData']);

            $trModels = [];
            foreach($_POST['SpPointsTr'] AS $key => $item)
            {
                $trModel = $model->spPointsTrs(['condition' => 'language_id = :language', 'params' => [':language' => $item['language_id']]])[0];

                if($trModel === NULL)
                    $trModel = new SpPointsTr();

                $trModel->setAttributes($item);


                if($trModel->title != '' OR $trModel->text != '')
                    array_push($trModels, $trModel);
            }
            $model->spPointsTrs = $trModels;


            if($model->saveWithRelatedModels())
                $this->redirect(array('view','id'=>$model->id));
        }

        $modelsTrs = [];
        foreach($model->spPointsTrs AS $item)
        {
            $modelsTrs[$item->language_id] = $item;
        }

        $this->render('update',array(
            'model'=>$model,
            'modelTrs' => $modelsTrs,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id, 'SpPoints')->deleteWithRelatedModels();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(['admin']);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new SpPoints('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['SpPoints']))
            $model->attributes=$_GET['SpPoints'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function actionStat($id)
    {
        /* @var $model CourierAdditionList */

        $model = SpPoints::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }
}
