<table class="list hTop" style="width: 500px;">
    <tr class="header"><td>Date</td><td>Change of status</td></tr>
    <?php

    foreach($model->orderStatHistories AS $item):
        ?>
        <tr>
            <td><?php echo $item->date_entered;?></td>
            <td><?php echo $item->previousStat->name;?> -&gt; <strong><?php echo $item->currentStat->name;?></strong></td>
        </tr>
    <?php
    endforeach;
    ?>
</table>