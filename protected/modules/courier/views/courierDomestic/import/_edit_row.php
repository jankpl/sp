<?php
/* @var $model Courier */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'courier-edit-form',
    'htmlOptions' => ['data-edit-form-id' => $id],
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'stateful'=>false,
    'clientOptions' => [
        'validateOnSubmit'=> true,
        'afterValidate'=>'js:function(form,data,hasError){
                        if(!hasError){
                            onItemEditSubmit("'.Yii::app()->createUrl("courier/courierDomestic/ajaxSaveRowOnImport").'");
                        } else {
                            $(".overlay").hide();
                            alert("'.Yii::t('courier','Proszę poprawić błędy formularza!').'");
                         }
                        }'
    ],
));
?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= Yii::t('courier', 'Edycja danych');?></h4>
    </div>
    <div class="modal-body" id="edit-modal-content">

        <div class="form" id="domestic-edit-item-form">

            <h4><?php echo Yii::t('courier','Package data');?></h4>
            <div class="inline-form">
                <div style="float: left; width: 48%;">
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_weight'); ?>
                        <?php echo $form->textField($model, 'package_weight', array('data-additions-dependency' => 'true')); ?> <?php echo Courier::getWeightUnit();?>
                        <?php echo $form->error($model,'package_weight'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_size_l'); ?>
                        <?php echo $form->textField($model, 'package_size_l', array('data-additions-dependency' => 'true')); ?>
                        <?php echo $form->error($model,'package_size_l'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_size_w'); ?>
                        <?php echo $form->textField($model, 'package_size_w', array('data-additions-dependency' => 'true')); ?>
                        <?php echo $form->error($model,'package_size_w'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_size_d'); ?>
                        <?php echo $form->textField($model, 'package_size_d', array('data-additions-dependency' => 'true')); ?>
                        <?php echo $form->error($model,'package_size_d'); ?>
                    </div><!-- row -->
                </div>
                <div style="float: right; width: 48%;">
                    <div class="row">
                        <?php echo $form->labelEx($model,'packages_number'); ?>
                        <?php echo $form->dropDownList($model, 'packages_number', S_Useful::generateArrayOfItemsNumber(1,10)); ?>
                        <?php echo $form->error($model,'packages_number'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'cod_value'); ?>
                        <?php echo $form->textField($model, 'cod_value', array('maxlength' => 32, 'data-additions-dependency' => 'true', 'data-cod-info' => true)); ?> <span id="currency"><?php echo Currency::PLN;?></span>
                        <?php echo $form->error($model,'cod_value'); ?>
                        <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('courier','Paczka będzie automatycznie ubezpieczona do kwoty równiej kwocie COD.'), array('closeText' => false, 'id' => 'cod-info'));?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model->senderAddressData,'[sender]bankAccount'); ?>
                        <?php echo $form->textField($model->senderAddressData, '[sender]bankAccount', array('maxlength' => 50,)); ?>
                        <?php echo $form->error($model->senderAddressData,'[sender]bankAccount'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_value'); ?>
                        <?php echo $form->textField($model, 'package_value'); ?>
                        <?php echo $form->error($model,'package_value'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_content'); ?>
                        <?php echo $form->textField($model, 'package_content', array('maxlength' => 256)); ?>
                        <?php echo $form->error($model,'package_content'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'ref'); ?>
                        <?php echo $form->textField($model, 'ref', array('maxlength' => 64)); ?>
                        <?php echo $form->error($model,'ref'); ?>
                    </div><!-- row -->
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="inline-form">
                <div style="width: 48%; float: left">
                    <h4><?= Yii::t('courier','Nadawca');?></h4>
                    <?php
                    $this->renderPartial('create/_addressDataForm/_form',
                        array(
                            'model' => $model->senderAddressData,
//                    'listAddressData' => UserContactBook::listContactBookItems('sender'),
                            'form' => $form,
                            'noEmail' => false,
                            'i' => 'sender',
                            'type' => 'sender',
//                        'blockAddressSuggester' => true,
                        )
                    );
                    ?>
                </div>
                <div style="width: 48%; float: right">
                    <h4><?= Yii::t('courier','Odbiorca');?></h4>
                    <?php
                    $this->renderPartial('create/_addressDataForm/_form',
                        array(
                            'model' => $model->receiverAddressData,
//                    'listAddressData' => UserContactBook::listContactBookItems('receiver'),
                            'form' => $form,
                            'noEmail' => false,
                            'i' => 'receiver',
                            'type' => 'receiver',
//                        'blockAddressSuggester' => true,
//                    'focusFirst' => true,
                        ));?>
                </div>
                <div style="clear: both;"></div>
                <p class="note text-left">
                    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
                </p>
            </div>

        </div>

    </div>
    <div class="modal-footer">
        <?php echo TbHtml::button(Yii::t('app', 'Cancel'), array('class' => 'btn btn-warning', 'data-cancel' => 'true', 'data-dismiss' => 'modal')); ?>
        <?php echo TbHtml::submitButton(Yii::t('app', 'Save changes'), array('class' => 'btn-lg btn-primary', 'data-save-row-changes' => $id)); ?>
    </div>

<?php echo CHtml::hiddenField('hash', $hash); ?>
<?php echo CHtml::hiddenField('id', $id); ?>
<?php $this->endWidget();
?>
<?php
// DOUBLE LOADING OF JQUERY WAS BREAKING SCRIPTS
Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.yiiactiveform.js'] = false;