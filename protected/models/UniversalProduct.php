<?php

/**
 * Class UniversalProduct
 *
 */
class UniversalProduct
{
    const PRODUCT_TYPE_COURIER = 1;
    const PRODUCT_TYPE_POSTAL = 2;

    public $fromAddressData;
    public $toAddressData;

    public $weight;
    public $size_l;
    public $size_d;
    public $size_w;

    public $date_entered;

    public $content;
    public $value;

    public $local_id;
    public $remote_id;

    public $productType;
    public $productId;

    public $cod_value = 0;
    public $cod_currency = false;

    protected $_productModel = false;

    public function getProductModel()
    {
        if(!$this->_productModel)
        {
            if($this->productType == self::PRODUCT_TYPE_COURIER) {
                Yii::app()->getModule('Courier');
                $this->_productModel = Courier::model()->findByPk($this->productId);
            }
            else if($this->productType == self::PRODUCT_TYPE_POSTAL) {
                Yii::app()->getModule('Postal');
                $this->_productModel = Postal::model()->findByPk($this->productId);
            }
            else
                throw new Exception('Unknown Product type!');
        }

        return $this->_productModel;
    }

    public static function createByTypeAndItemId($type, $item_id)
    {
        switch($type)
        {
            case self::PRODUCT_TYPE_COURIER:
                Yii::app()->getModule('courier');
                $item = CourierLabelNew::model()->findByPk($item_id);
                return self::createByCourierLabelNew($item);
                break;
            case self::PRODUCT_TYPE_POSTAL:
                Yii::app()->getModule('postal');
                $item = PostalLabel::model()->findByPk($item_id);
                return self::createByPostalLabel($item);
                break;
            default:
                throw new Exception('Unknown type!');
                break;
        }
    }

    public static function createByCourierLabelNew(CourierLabelNew $cln)
    {
        $model = new self;

        $model->productType = self::PRODUCT_TYPE_COURIER;

        $model->fromAddressData = $cln->addressDataFrom;
        $model->toAddressData = $cln->addressDataTo;

        $model->weight = $cln->courier->getWeight(true);
        $model->date_entered = substr($cln->courier->date_entered,0, 10);
        $model->size_l = $cln->courier->package_size_l;
        $model->size_d = $cln->courier->package_size_d;
        $model->size_w = $cln->courier->package_size_w;
        $model->content = $cln->courier->package_content;
        $model->value = $cln->courier->package_value;
        $model->local_id = $cln->courier->local_id;
        $model->remote_id = $cln->track_id;

        $model->cod_value = $cln->courier->cod_value;
        $model->cod_currency = $cln->courier->cod_currency;

        return $model;
    }

    public static function createByPostalLabel(PostalLabel $pl)
    {
        $model = new self;

        $model->productType = self::PRODUCT_TYPE_POSTAL;

        $model->fromAddressData = $pl->postal->senderAddressData;
        $model->toAddressData = $pl->postal->receiverAddressData;

        $model->weight = $pl->postal->getWeightClassValue() / 1000;
        $model->date_entered = substr($pl->postal->date_entered,0, 10);
        $model->size_l = NULL;
        $model->size_d = NULL;
        $model->size_w = NULL;
        $model->content = NULL;
        $model->value = NULL;
        $model->local_id = $pl->postal->local_id;
        $model->remote_id = $pl->track_id;

        return $model;
    }
}