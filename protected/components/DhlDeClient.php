<?php

class DhlDeClient
{
    const WSDL = 'https://cig.dhl.de/cig-wsdls/com/dpdhl/wsdl/geschaeftskundenversand-api/2.2/geschaeftskundenversand-api-2.2.wsdl';

    const URL_POINTS = GLOBAL_CONFIG::DHLDE_URL_POINTS;
    const URL = GLOBAL_CONFIG::DHLDE_URL;
    const USERNAME_ACC = GLOBAL_CONFIG::DHLDE_USERNAME_ACC;
    const PASSWORD_ACC = GLOBAL_CONFIG::DHLDE_PASSWORD_ACC;
    const USERNAME = GLOBAL_CONFIG::DHLDE_USERNAME;
    const PASSWORD = GLOBAL_CONFIG::DHLDE_PASSWORD;
    const ACCOUNT_NO = GLOBAL_CONFIG::DHLDE_ACCOUNT_NO;

    // TT:
    const URL_TT = GLOBAL_CONFIG::DHLDE_URL_TT;
    const USERNAME_TT = GLOBAL_CONFIG::DHLDE_USERNAME_TT;
    const PASSWORD_TT = GLOBAL_CONFIG::DHLDE_PASSWORD_TT;

    const BANK_OWNER = 'KAAB GmbH';
    const BANK_IBAN = 'DE80100900002669758017';
    const BANK_NAME = 'Berliner Volksbank eG';

    const SUBACCOUNT_DEFAULT = '01';
    const SUBACCOUNT_CARMAT = '02';
    const SUBACCOUNT_GIMEDIA = '03';

    const TT_GROUP_NO = 2;

    public $user_id;

    public $label_annotation_ref = false;

    public $reference;

    public $parcelshop = false;
    public $parcelshop_no;
    public $parcelshop_street;
    public $parcelshop_street_no;
    public $parcelshop_country;

    public $postfiliale = false;
    public $postfiliale_no;

    public $packstation = false;
    public $packstation_no;

    public $package_value;
    public $package_content;

    public $sender_name;
    public $sender_company;
    public $sender_address;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_tel;
    public $sender_email;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $packages_number = 1;
//    public $package_content;

    public $cod_value = 0;

    public $additional_insurance = false;
    public $notification = false;

    protected $_product;
    protected $_product_code;
    protected $_product_procedure;

    protected $_suffixAccountCode = self::SUBACCOUNT_DEFAULT;

    protected $dont_hide_sender = false;


    protected $_point_type = false;
    protected $_point_id = false;
    protected $_point_postnummer = false;
    protected $_point_zip_code = false;


    const PRODUCT_PAKET = 1;
    const PRODUCT_PAKET_PRIO = 2;
    const PRODUCT_PAKET_TAGGLEICH = 3;
    const PRODUCT_PAKET_INT = 4;
    const PRODUCT_EUROPAKET = 5;
    const PRODUCT_CONNECT = 6;
    const PRODUCT_PAKET_AT = 7;
    const PRODUCT_CONNECT_AT = 8;
    const PRODUCT_INT_AT = 9;

    protected static function productOptList()
    {
        return [
            self::PRODUCT_PAKET =>
                [
                    'procedure' => '01',
                    'code' => 'V01PAK',
                ],
            self::PRODUCT_PAKET_INT =>
                [
                    'procedure' => '53',
                    'code' => 'V53WPAK',
                ],
            self::PRODUCT_PAKET_PRIO =>
                [
                    'procedure' => '01',
                    'code' => 'V01PRIO',
                ],
            self::PRODUCT_PAKET_TAGGLEICH =>
                [
                    'procedure' => '06',
                    'code' => 'V06PAK',
                ],
            self::PRODUCT_EUROPAKET =>
                [
                    'procedure' => '54',
                    'code' => 'V54EPAK',
                ],
            self::PRODUCT_CONNECT =>
                [
                    'procedure' => '55',
                    'code' => 'V55PAK',
                ],
            self::PRODUCT_PAKET_AT =>
                [
                    'procedure' => '86',
                    'code' => 'V86PARCEL',
                ],
            self::PRODUCT_CONNECT_AT =>
                [
                    'procedure' => '87',
                    'code' => 'V87PARCEL',
                ],
            self::PRODUCT_INT_AT =>
                [
                    'procedure' => '82',
                    'code' => 'V82PARCEL',
                ],
        ];
    }



    protected function setProductByCourierOperatorUniqId($operator_uniq_id)
    {
        $product = false;
        switch($operator_uniq_id)
        {
            case CourierOperator::UNIQID_DHLDE_PAKET:
                $product = self::PRODUCT_PAKET;
                break;
            case CourierOperator::UNIQID_DHLDE_PAKET_INT:
                $product = self::PRODUCT_PAKET_INT;
                break;
            case CourierOperator::UNIQID_DHLDE_EUROPAKET:
                $product = self::PRODUCT_EUROPAKET;
                break;
            case CourierOperator::UNIQID_DHLDE_PAKET_AT:
                $product = self::PRODUCT_PAKET_AT;
                break;
            case CourierOperator::UNIQID_DHLDE_PAKET_CONNECT_AT:
                $product = self::PRODUCT_CONNECT_AT;
                break;
            case CourierOperator::UNIQID_DHLDE_PAKET_INT_AT:
                $product = self::PRODUCT_INT_AT;
                break;
            case CourierOperator::UNIQID_DHLDE_CARMAT_PAKET:
                $product = self::PRODUCT_PAKET;
                $this->_suffixAccountCode = self::SUBACCOUNT_CARMAT;
                break;
            case CourierOperator::UNIQID_DHLDE_CARMAT_PAKET_INT:
                $product = self::PRODUCT_PAKET_INT;
                $this->_suffixAccountCode = self::SUBACCOUNT_CARMAT;
                break;
            case CourierOperator::UNIQID_DHLDE_CARMAT_PAKET_AT:
                $product = self::PRODUCT_PAKET_AT;
                $this->_suffixAccountCode = self::SUBACCOUNT_CARMAT;
                break;


            case CourierOperator::UNIQID_DHLDE_GIMEDIA_PAKET:
                $product = self::PRODUCT_PAKET;
                $this->_suffixAccountCode = self::SUBACCOUNT_GIMEDIA;
                break;
            case CourierOperator::UNIQID_DHLDE_GIMEDIA_PAKET_INT:
                $product = self::PRODUCT_PAKET_INT;
                $this->_suffixAccountCode = self::SUBACCOUNT_GIMEDIA;
                break;
            case CourierOperator::UNIQID_DHLDE_GIMEDIA_PAKET_AT:
                $product = self::PRODUCT_PAKET_AT;
                $this->_suffixAccountCode = self::SUBACCOUNT_GIMEDIA;
                break;
        }

        $this->setProduct($product);

    }

    protected function setProductByCourierUOperatorUniqId($operator_uniq_id)
    {
        $product = false;
        switch($operator_uniq_id)
        {
            case CourierUOperator::UNIQID_DHLDE_PAKET:
                $product = self::PRODUCT_PAKET;
                break;
            case CourierUOperator::UNIQID_DHLDE_PAKET_INT:
                $product = self::PRODUCT_PAKET_INT;
                break;
            case CourierUOperator::UNIQID_DHLDE_EUROPAKET:
                $product = self::PRODUCT_EUROPAKET;
                break;
            case CourierUOperator::UNIQID_DHLDE_PAKET_AT:
                $product = self::PRODUCT_PAKET_AT;
                break;
            case CourierUOperator::UNIQID_DHLDE_PAKET_CONNECT_AT:
                $product = self::PRODUCT_CONNECT_AT;
                break;
            case CourierUOperator::UNIQID_DHLDE_PAKET_INT_AT:
                $product = self::PRODUCT_INT_AT;
                break;


            case CourierUOperator::UNIQID_DHLDE_PAKET_CARMAT:
                $product = self::PRODUCT_PAKET;
                $this->_suffixAccountCode = self::SUBACCOUNT_CARMAT;
                break;
            case CourierUOperator::UNIQID_DHLDE_PAKET_INT_CARMAT:
                $product = self::PRODUCT_PAKET_INT;
                $this->_suffixAccountCode = self::SUBACCOUNT_CARMAT;
                break;
            case CourierUOperator::UNIQID_DHLDE_PAKET_AT_CARMAT:
                $product = self::PRODUCT_PAKET_AT;
                $this->_suffixAccountCode = self::SUBACCOUNT_CARMAT;
                break;


            case CourierUOperator::UNIQID_DHLDE_PAKET_GIMEDIA:
                $product = self::PRODUCT_PAKET;
                $this->_suffixAccountCode = self::SUBACCOUNT_GIMEDIA;
                break;
            case CourierUOperator::UNIQID_DHLDE_PAKET_INT_GIMEDIA:
                $product = self::PRODUCT_PAKET_INT;
                $this->_suffixAccountCode = self::SUBACCOUNT_GIMEDIA;
                break;
            case CourierUOperator::UNIQID_DHLDE_PAKET_AT_GIMEDIA:
                $product = self::PRODUCT_PAKET_AT;
                $this->_suffixAccountCode = self::SUBACCOUNT_GIMEDIA;
                break;
        }

        $this->setProduct($product);

    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    protected function setProduct($product_id)
    {
        if(!$product_id OR !isset(self::productOptList()[$product_id]))
            throw new Exception('Unknown DHL DE product!');
        else{
            $data = self::productOptList()[$product_id];
            $this->_product_code = $data['code'];
            $this->_product_procedure = $data['procedure'];
            $this->_product = $product_id;
        }
    }

    protected static function _ownExtractNumber($text, $REG_PATTERN, $defaultValue = '0')
    {
        $building = '';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[0][0];

        $building = trim($building);

        if(mb_strlen($building) > 5)
        {
            // max length is 5
            // if it's longer, use older, more choosy rexexp
            $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';
            if(preg_match_all($REG_PATTERN, $text, $result2))
                $building = $result2[2][0];

            $building = trim($building);
        }

        if(mb_strlen($building) > 5)
        {
            $building = preg_split( "/(\/|\-|&)/", $building);
            $building = $building[0];
        }

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }

    public static function addressSplit($addressLine)
    {
        $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-\&]+)((\s)+|$))*/i';

        $text = str_replace([' - ', '  ',' & ', ' / '],['-', ' ','&', '/'], $addressLine);

        $house = self::_ownExtractNumber($text, $REG_PATTERN);

        if(preg_match('/(FLAT '.str_replace('/','\/', $house).')/i', $text))
        {
            $tempItem = str_ireplace('FLAT '.$house, '', $text);
            if(preg_match_all($REG_PATTERN, $tempItem, $result2))
            {
                $house = $result2[2][0];
                $house = str_replace(['/', '.'], '', $house);
            }
        }

        $temp = explode($house, $text);

        $addit = '';
        if(S_Useful::sizeof($temp) > 2)
        {
            $i0 = array_shift($temp);
            $i1 = implode($house, $temp);

            $temp[0] = $i0;
            $temp[1] = $i1;
        }

        foreach ($temp AS $key => $temp2)
        {
            $temp[$key] = trim($temp2);

            if($temp[$key] == '')
                unset($temp[$key]);
        }
        // reset indexes
        $temp = array_values($temp);

        if(S_Useful::sizeof($temp) == 1)
            $street = $temp[0];
        else {
            if(preg_match('/(FLAT|APARTMENT|APT|UNIT)/i', $temp[0]))
            {
                $street = $temp[1];
                $addit = $temp[0];
            } else {
                $street = $temp[0];
                $addit = $temp[1];
            }
        }

        if($street == '')
            $street = $text;

        return [$street, $house, $addit];
    }


    public static function _addressLinesMerge($address_line_1, $address_line_2)
    {
        if($address_line_1 != $address_line_2)
            $text = trim($address_line_1.' '.$address_line_2);
        else
            $text = $address_line_1;

        return $text;
    }


    protected function _createShipment($returnError = false)
    {

//        if($this->_product_code == 'V53WPAK') // DHL DE PAKET INT // @ 01.03.2018
//        {
//            $weightTable = [
//                1 => 1,
//                2 => 1,
//                3 => 1,
//                4 => 2,
//                5 => 2,
//                6 => 3,
//                7 => 3,
//                8 => 4,
//                9 => 4,
//                10 => 5,
//                11 => 5,
//                12 => 6,
//                13 => 7,
//                14 => 8,
//                15 => 9,
//                16 => 10,
//                17 => 10,
//                18 => 11,
//                19 => 11,
//                20 => 12,
//                21 => 12,
//                22 => 13,
//                23 => 13,
//                24 => 14,
//                25 => 14,
//                26 => 15,
//                27 => 16,
//                28 => 17,
//                29 => 18,
//                30 => 19
//            ];
//
//            $val = (string) $this->package_weight;
//            $val = explode('.', $val);
//
//            $temp = intval($val[0]);
//
//            if(isset($weightTable[$temp]))
//                $val[0] = $weightTable[$temp];
//
//            $val = implode('.', $val);
//            $this->package_weight = (double) $val;
//        }

        $this->receiver_name = trim($this->receiver_name);
        $this->receiver_company = trim($this->receiver_company);

        $this->sender_name = trim($this->sender_name);
        $this->sender_company = trim($this->sender_company);

//        $this->_point_type = 'packStation';
//        $this->_point_id = '8007-478462122';
//        $this->_point_id = '122';
//        $this->_point_postnummer = '78462';
////
//        $this->receiver_city = 'MAINZa';
//        $this->receiver_zip_code = '54130';

//        $this->parcelshop_street = 'abc';
//        $this->parcelshop_street_no = '10';
//        $this->parcelshop_country = 'DE';


        if ($this->sender_name == $this->sender_company) {
            $senderName1 = $this->sender_name;
            $senderName2 = '';
        } else {
            $senderName1 = $this->sender_name;
            $senderName2 = $this->sender_company;
        }

        if ($this->receiver_name == $this->receiver_company) {
            $receiverName1 = $this->receiver_name;
            $receiverName2 = '';
        } else {
            $receiverName1 = $this->receiver_name;
            $receiverName2 = $this->receiver_company;
        }

        if($this->_point_type) // all details for points are provided in dedicated variables, not names fields
        {

            $pointZipCode = $this->_point_zip_code;

            $postNumber = $this->_point_postnummer;
            if($this->_point_type == 'packStation')
            {
                $this->packstation = 1;
                $this->packstation_no = $this->_point_id;
            }
            else if($this->_point_type == 'parcelShop')
            {
                $this->parcelshop = 1;
                $this->parcelshop_no = $this->_point_id;
            }
            else
            {
                $this->postfiliale = 1;
                $this->postfiliale_no = $this->_point_id;
            }

        } else {

            $pointZipCode = $this->receiver_zip_code;

            $postNumber = false;
            if (preg_match('/PACKSTATION (\d{3})/i', $this->receiver_address, $match)) {
                $this->packstation = 1;
                $this->packstation_no = $match[1];

                //postNumber is not required (?)
                if (preg_match('/(\d{1,10})/i', $this->receiver_name, $match)) {
                    $postNumber = $match[1];
                } else if (preg_match('/(\d{1,10})/i', $this->receiver_company, $match)) {
                    $postNumber = $match[1];
                }

            } else if (preg_match('/POSTFILIALE (\d{3})/i', $this->receiver_address, $match)) {
                $this->postfiliale = 1;
                $this->postfiliale_no = $match[1];

                //postNumber is required
                if (preg_match('/(\d{1,10})/i', $this->receiver_name, $match)) {
                    $postNumber = $match[1];
                } else if (preg_match('/(\d{1,10})/i', $this->receiver_company, $match)) {
                    $postNumber = $match[1];
                }
            } else if (preg_match('/PARCELSHOP (\d{3})/i', $this->receiver_address, $match)) {
                $this->parcelshop = 1;
                $this->parcelshop_no = $match[1];

                //no postNumber
            }


            if ($postNumber) {
                foreach (['receiverName1', 'receiverName2', 'senderName1', 'senderName2'] AS $item) {
                    $value = trim(str_replace($postNumber, '', $$item));
                    if ($value == '')
                        $value = $postNumber;

                    $$item = $value;
                }

                if ($receiverName1 == $postNumber && $receiverName2 != '')
                    $receiverName1 = $receiverName2;

            }

        }

        if($senderName1 == '')
        {
            $senderName1 = $senderName2;
            $senderName2 = '';
        }


        if($receiverName1 == '')
        {
            $receiverName1 = $receiverName2;
            $receiverName2 = '';
        }

//        $receiverStreetNo = trim(AddressSplitter::extractBuildingNo($this->receiver_address));
//        $receiver_address = str_replace($receiverStreetNo, '', $this->receiver_address);
//        $receiver_address = trim($receiver_address);
//
//        $receiverStreet = mb_substr($receiver_address,0,35);
//        $receiverStreetAddit = mb_substr($receiver_address,35,35);
//
//        $senderStreetNo = AddressSplitter::extractBuildingNo($this->sender_address);
//        $sender_address = str_replace($senderStreetNo, '', $this->sender_address);
//        $sender_address = trim($sender_address);
//
//        $senderStreet = mb_substr($sender_address,0,35);
//        $senderStreetAddit = mb_substr($sender_address,35,35);

        list($senderStreet, $senderStreetNo, $senderStreetAddit) = self::addressSplit($this->sender_address);
        $senderStreet = mb_substr($senderStreet,0,35);
        $senderStreetAddit = mb_substr($senderStreetAddit,0,35);

        list($receiverStreet, $receiverStreetNo, $receiverStreetAddit) = self::addressSplit($this->receiver_address);
        $receiverStreet = mb_substr($receiverStreet,0,35);
        $receiverStreetAddit = mb_substr($receiverStreetAddit,0,35);

        $data = new stdClass();

        $data->Version = new stdClass();
        $data->Version->majorRelease = "2";
        $data->Version->minorRelease = "2";

        $data->ShipmentOrder = [];

        for($i = 0; $i < $this->packages_number; $i++) {

            $shipmentOrder = new stdClass();
            $shipmentOrder->sequenceNumber = $i;

            $shipmentOrder->Shipment = new stdClass();
            $shipmentOrder->Shipment->ShipmentDetails = new stdClass();
            $shipmentOrder->Shipment->ShipmentDetails->product = $this->_product_code;
            $shipmentOrder->Shipment->ShipmentDetails->accountNumber = self::ACCOUNT_NO . $this->_product_procedure . $this->_suffixAccountCode;
            $shipmentOrder->Shipment->ShipmentDetails->customerReference = $this->reference;
            $shipmentOrder->Shipment->ShipmentDetails->shipmentDate = date('Y-m-d');
            $shipmentOrder->Shipment->ShipmentDetails->ShipmentItem = new stdClass();
            $shipmentOrder->Shipment->ShipmentDetails->ShipmentItem->weightInKG = $this->package_weight;
            $shipmentOrder->Shipment->ShipmentDetails->ShipmentItem->lengthInCM = $this->package_size_l;
            $shipmentOrder->Shipment->ShipmentDetails->ShipmentItem->widthInCM = $this->package_size_w;
            $shipmentOrder->Shipment->ShipmentDetails->ShipmentItem->heightInCM = $this->package_size_d;

            if ($this->notification) {
                $shipmentOrder->Shipment->ShipmentDetails->Notification = new stdClass();
                $shipmentOrder->Shipment->ShipmentDetails->Notification->recipientEmailAddress = $this->receiver_email;
            }

            $services = '';
            if ($this->cod_value > 0) {
                if (!in_array($this->_product, [self::PRODUCT_PAKET, self::PRODUCT_PAKET_PRIO, self::PRODUCT_PAKET_TAGGLEICH, self::PRODUCT_CONNECT_AT, self::PRODUCT_PAKET_AT])) {
                    if ($returnError == true) {
                        $return = [0 => [
                            'error' => 'COD is not allowed for this parameters!'
                        ]];
                        return $return;
                    } else {
                        return false;
                    }
                }

                // Because we need to add 2 EUR for DHL transmital fee
                $realCodValue = $this->cod_value + 2;

                $services .= '<CashOnDelivery active="1" codAmount="' . $realCodValue . '"/>';
                $shipmentOrder->Shipment->ShipmentDetails->BankData = new stdClass();
                $shipmentOrder->Shipment->ShipmentDetails->BankData->accountOwner = self::BANK_OWNER;
                $shipmentOrder->Shipment->ShipmentDetails->BankData->bankName = self::BANK_NAME;
                $shipmentOrder->Shipment->ShipmentDetails->BankData->iban = self::BANK_IBAN;


            }

            if ($this->additional_insurance) {
                $services .= '<AdditionalInsurance active="1" insuranceAmount="' . $this->package_value . '"/>';

                // @19.03.2019 - dont allow insurance > 2500 euro
                if($this->package_value > 2500) {
                    if ($returnError == true) {
                        $return = [0 => [
                            'error' => 'Maximum allowed insurance value is 2500 Euro'
                        ]];
                        return $return;
                    } else {
                        return false;
                    }
                }
            }

            if ($services != '')
                $shipmentOrder->Shipment->ShipmentDetails->Service = new \SoapVar('<Service>' . $services . '</Service>', XSD_ANYXML);


            // for United Arab Emirates clean zip_code
            if (strtoupper($this->receiver_country) == 'AE')
                $this->receiver_zip_code = '';
            if (strtoupper($this->sender_country) == 'AE')
                $this->sender_zip_code = '';

            $shipmentOrder->Shipment->Shipper = new stdClass();
            $shipmentOrder->Shipment->Shipper->Name = new stdClass();

            if ($this->dont_hide_sender) {
                $shipmentOrder->Shipment->Shipper->Name->name1 = $senderName1;
                $shipmentOrder->Shipment->Shipper->Name->name2 = $senderName2;
            } else {
                $shipmentOrder->Shipment->Shipper->Name->name1 = 'KAAB Gmbh'; // @03.01.2019 // Real sender is overprinted on label
                $shipmentOrder->Shipment->Shipper->Name->name2 = 'KAAB Gmbh';
            }
            $shipmentOrder->Shipment->Shipper->Name->name3 = '';
            $shipmentOrder->Shipment->Shipper->Address = new stdClass();
            $shipmentOrder->Shipment->Shipper->Address->streetName = $senderStreet;
            $shipmentOrder->Shipment->Shipper->Address->streetNumber = $senderStreetNo;
            $shipmentOrder->Shipment->Shipper->Address->addressAddition = $senderStreetAddit;
//         $shipmentOrder->Shipment->Shipper->Address->dispatchingInformation = '';
            $shipmentOrder->Shipment->Shipper->Address->zip = $this->sender_zip_code;
            $shipmentOrder->Shipment->Shipper->Address->city = $this->sender_city;
            $shipmentOrder->Shipment->Shipper->Address->Origin = new stdClass();
            $shipmentOrder->Shipment->Shipper->Address->Origin->countryISOCode = $this->sender_country;
            $shipmentOrder->Shipment->Shipper->Communication = new stdClass();
            $shipmentOrder->Shipment->Shipper->Communication->phone = $this->sender_tel;
            $shipmentOrder->Shipment->Shipper->Communication->email = $this->sender_email;

            $shipmentOrder->Shipment->Receiver = new stdClass();
            $shipmentOrder->Shipment->Receiver->name1 = $receiverName1;
            $shipmentOrder->Shipment->Receiver->Communication = new stdClass();
            $shipmentOrder->Shipment->Receiver->Communication->phone = $this->receiver_tel;
            $shipmentOrder->Shipment->Receiver->Communication->email = $this->receiver_email;

            if($this->packstation)
            {
                $shipmentOrder->Shipment->Receiver->Packstation = new stdClass();
                if($postNumber)
                    $shipmentOrder->Shipment->Receiver->Packstation->postNumber = $postNumber;
                $shipmentOrder->Shipment->Receiver->Packstation->packstationNumber = $this->packstation_no;
                $shipmentOrder->Shipment->Receiver->Packstation->zip = $pointZipCode;
                $shipmentOrder->Shipment->Receiver->Packstation->city = $this->receiver_city;
            }
            else if($this->postfiliale) {
                $shipmentOrder->Shipment->Receiver->Postfiliale = new stdClass();
                $shipmentOrder->Shipment->Receiver->Postfiliale->postNumber = $postNumber;
                $shipmentOrder->Shipment->Receiver->Postfiliale->postfilialNumber = $this->postfiliale_no;
                $shipmentOrder->Shipment->Receiver->Postfiliale->zip = $pointZipCode;
                $shipmentOrder->Shipment->Receiver->Postfiliale->city = $this->receiver_city;
            }
            else if($this->parcelshop) {
                $shipmentOrder->Shipment->Receiver->ParcelShop = new stdClass();
                $shipmentOrder->Shipment->Receiver->ParcelShop->parcelShopNumber = $this->parcelshop_no;
                $shipmentOrder->Shipment->Receiver->ParcelShop->streetName = $this->parcelshop_street;
                $shipmentOrder->Shipment->Receiver->ParcelShop->streetNumber = $this->parcelshop_street_no;
                $shipmentOrder->Shipment->Receiver->ParcelShop->zip = $pointZipCode;
                $shipmentOrder->Shipment->Receiver->ParcelShop->city = $this->receiver_city;
                $shipmentOrder->Shipment->Receiver->ParcelShop->Origin = new stdClass();
                $shipmentOrder->Shipment->Receiver->ParcelShop->Origin->countryISOCode = $this->receiver_country;
            } else {
                $shipmentOrder->Shipment->Receiver->Address = new stdClass();
                $shipmentOrder->Shipment->Receiver->Address->name2 = $receiverName2;

                if($this->receiver_country == 'DE')
                    $shipmentOrder->Shipment->Receiver->Address->name3 = $receiverStreetAddit;

                $shipmentOrder->Shipment->Receiver->Address->streetName = $receiverStreet;
                $shipmentOrder->Shipment->Receiver->Address->streetNumber = $receiverStreetNo;
                $shipmentOrder->Shipment->Receiver->Address->addressAddition = $receiverStreetAddit;
                $shipmentOrder->Shipment->Receiver->Address->zip = $this->receiver_zip_code;
                $shipmentOrder->Shipment->Receiver->Address->city = $this->receiver_city;
                $shipmentOrder->Shipment->Receiver->Address->Origin = new stdClass();
                $shipmentOrder->Shipment->Receiver->Address->Origin->countryISOCode = $this->receiver_country;
                $shipmentOrder->Shipment->Receiver->Address->Origin->state = $this->receiver_country == 'US' ? UsaZipState::getStateByZip($this->receiver_zip_code) : '';
            }

            $shipmentOrder->labelResponseType = 'B64';

            if($this->receiver_country != $this->sender_country) {

                $exportDocument = '<ExportDocument>' . "\r\n";
                $exportDocument .= '<exportType>DOCUMENT</exportType>' . "\r\n";
                $exportDocument .= '<additionalFee>0</additionalFee>' . "\r\n";
                $exportDocument .= '<ExportDocPosition>';
                $exportDocument .= '<description>'.self::_cdata($this->package_content).'</description>' . "\r\n";
                $exportDocument .= '<countryCodeOrigin>'.$this->sender_country.'</countryCodeOrigin>' . "\r\n";
                $exportDocument .= '<customsValue>'.$this->package_value.'</customsValue>' . "\r\n";
                $exportDocument .= '<amount>1</amount>' . "\r\n";
                $exportDocument .= '<netWeightInKG>'.$this->package_weight.'</netWeightInKG>' . "\r\n";
                $exportDocument .= '</ExportDocPosition>' . "\r\n";
                $exportDocument .= '</ExportDocument>' . "\r\n";

                $shipmentOrder->Shipment->ExportDocument = new \SoapVar($exportDocument, XSD_ANYXML);
            }

            $data->ShipmentOrder[] = $shipmentOrder;
        }




        MyDump::dump('dhlde.txt', print_r($data,1));

        $customUser = false;

//        if($this->_product_code == 'V53WPAK')
//            $customUser = 'kaabgmbh2';

        $resp = $this->_soapOperation('createShipmentOrder', $data, self::WSDL, false, $customUser);

        if ($resp->success == true) {
            $return = [];

            if(is_array($resp->data->CreationState))
                foreach($resp->data->CreationState AS $item)
                {
                    $return[] = [
                        'status' => true,
                        'label' => $this->_processLabel($item->LabelData->labelData, $this->dont_hide_sender ? false : $senderName1),
                        'track_id' => $item->LabelData->shipmentNumber,

                        'ack' => $item->LabelData->exportLabelData ? $item->LabelData->exportLabelData : NULL,
                        'ack_source' => $item->LabelData->exportLabelData ? CourierExtExtAckCards::SOURCE_DHL_DE_CUSTOMS : NULL,
                        'ack_type' => $item->LabelData->exportLabelData ? CourierExtExtAckCards::TYPE_CUSTOMS_DEC : NULL,
                    ];
                }
            else
                $return[] = [
                    'status' => true,
                    'label' => $this->_processLabel($resp->data->CreationState->LabelData->labelData, $this->dont_hide_sender ? false : $senderName1),
                    'track_id' => $resp->data->CreationState->LabelData->shipmentNumber,

                    'ack' => $resp->data->CreationState->LabelData->exportLabelData ? $resp->data->CreationState->LabelData->exportLabelData : NULL,
                    'ack_source' => $resp->data->CreationState->LabelData->exportLabelData ? CourierExtExtAckCards::SOURCE_DHL_DE_CUSTOMS : NULL,
                    'ack_type' => $resp->data->CreationState->LabelData->exportLabelData ? CourierExtExtAckCards::TYPE_CUSTOMS_DEC : NULL,
                ];



            return $return;
        }
        else {

            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $resp->errorDesc != '' ? $resp->errorDesc : $resp->error,
                )];
                return $return;
            } else {
                return false;
            }
        }

    }

    protected function _processLabel($label, $overwriteSenderName)
    {
        $multiplication = 2;
        $baseDpi = 300;

        $im = ImagickMine::newInstance();
        $im->setResolution($baseDpi * $multiplication, $baseDpi * $multiplication);
        $im->readImageBlob($label);

        $im->cropImage(1195* $multiplication, 2305* $multiplication, 280* $multiplication, 75* $multiplication);

        $draw = new ImagickDraw();
        $draw->setFontSize(35 * $multiplication);
        $draw->setTextUnderColor(new ImagickPixel('white'));

        $text = $this->package_content;

        /* @11.05.2018 - special rule for customer ANWIS2*/
        if($this->user_id == 2671)
            $text = $this->label_annotation_ref;

//        if($this->courier->user_id == 8) {
//            $text = $this->courier->senderAddressData->getUsefulName(true) . ', ' . trim($this->courier->senderAddressData->address_line_1 . ' ' . $this->courier->senderAddressData->address_line_2) . ', ' . $this->courier->senderAddressData->zip_code . ' ' . $this->courier->senderAddressData->city . ', ' . $this->courier->senderAddressData->country0->code;
//            $draw->setFontSize(20 * $multiplication);
//        }

        $im->resizeImage($im->getImageWidth()+500,$im->getImageHeight(),Imagick::FILTER_CATROM, 1);

        if($text != '')
        {
            $y = 168 * $multiplication;

            if($this->_product_code == 'V53WPAK')
                $y = 176* $multiplication;
        }
        $im->annotateImage($draw, 1200 * $multiplication, $y, 0,'ref: '.$text);

        if($overwriteSenderName) {
            $draw->setFontSize(45 * $multiplication);
            $im->annotateImage($draw, 475 * $multiplication, 220 * $multiplication, 0, str_pad($overwriteSenderName, 50, ' '));
        }

        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel())
        {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->trimImage(0);
        $im->stripImage();

        return $im->getImageBlob();
    }

    protected function _soapOperation($method, $data, $wsdl = self::WSDL, $ignoreAuth = false, $customUser = false)
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

//
//        $return->error = 'Please wait...';
//        $return->errorCode = 'Please wait...';
//        $return->errorDesc = 'Please wait...';
//        return $return;

        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
            'login' => self::USERNAME,
            'password' => self::PASSWORD,
            'classmap' => [
                'PrintOnlyIfCodeable' => 'PrintOnlyIfCodeable',
                'CashOnDelivery' => 'CashOnDelivery',
                'AdditionalInsurance' => 'AdditionalInsurance',
            ],
        );

        $headerBody = [
            'user' => $customUser ? $customUser : self::USERNAME_ACC,
            'signature' => self::PASSWORD_ACC,
        ];

        $headerBody = new \SoapVar(new \SoapVar($headerBody, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

        if(!$ignoreAuth)
            $header = new SoapHeader(self::URL, 'Authentification', $headerBody);

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        try {
            $client = new SoapClient($wsdl, $mode);
            $client->__setSoapHeaders($header);
            $client->__setLocation(self::URL);
            $resp = $client->$method($data);

        } catch (SoapFault $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('dhlde.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('dhlde.txt', print_r($client->__getLastResponse(),1));

            return $return;

        } catch (Exception $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('dhlde.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('dhlde.txt', print_r($client->__getLastResponse(),1));
            return $return;
        }

        MyDump::dump('dhlde.txt', print_r($client->__getLastRequest(),1));
        MyDump::dump('dhlde.txt', print_r($client->__getLastResponse(),1));

        if($resp->Status->statusCode == 0)
        {
            $return->success = true;
            $return->data = $resp;
        } else {

            $creationState = $resp->CreationState;
            if(is_array($creationState))
                $creationState = array_shift($creationState);

            $return->error = $resp->Status->statusText . '/' . $resp->Status->statusMessage;
            $return->errorCode = $resp->Status->statusCode;
            $return->errorDesc = is_array($creationState->LabelData->Status->statusMessage) ? implode(';', $creationState->LabelData->Status->statusMessage) : $creationState->LabelData->Status->statusMessage;
        }


        return $return;
    }

    public static function orderForPostal(Postal $postal)
    {
        $to = $postal->receiverAddressData;

        $model = new self;
        $model->setProduct(self::PRODUCT_PAKET);

        if($postal->user->getDhlDeDontHideSenderName())
            $model->dont_hide_sender = true;

        if($model->_product_procedure == '01' && $postal->user->getDhlDeCustomAccountCodeSuffix())
            $model->_suffixAccountCode = $postal->user->getDhlDeCustomAccountCodeSuffix();

        $model->sender_name = 'KAAB GMBH';
        $model->sender_company = 'GEOTTLICH LOGISTICS';
        $model->sender_zip_code = '01458';
        $model->sender_city = 'OTTENDORF-OKRILLA';
        $model->sender_address = 'BERGENER RING 720';
        $model->sender_tel = '0048221221218';
        $model->sender_country = 'DE';
        $model->sender_email = 'INFO@SWIATPRZESYLEK.PL';

        if($postal->courier->user->getForcePackageSenderDataDhlDe())
        {
            $model->sender_name = $postal->senderAddressData->name;
            $model->sender_company = $postal->senderAddressData->company;
        }

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = self::_addressLinesMerge($to->address_line_1, $to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_email = $to->fetchEmailAddress(true);

        $weight = round($postal->getWeightClassValue()/1000, 2);
        if($weight < 0.01)
            $weight = 0.01;

        $model->package_weight = $weight;
        $model->packages_number = 1;

        $model->package_size_l = 10;
        $model->package_size_d = 10;
        $model->package_size_w = 10;

        $model->package_value = $postal->value ? $postal->getValueConverted('EUR') : 100;
        $model->package_content = mb_substr($postal->content,0,35);
        $model->reference = mb_substr($postal->content,0,35);

        $model->cod_value = 0;

        $response = $model->_createShipment(true);
        if(is_array($response))
            return array_pop($response);
        else
            return $response;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $operator_uniq_id, $returnError = false, $withCollect = false)
    {

        $model = new self;
        $model->setProductByCourierOperatorUniqId($operator_uniq_id);

        if($courierInternal->courier->user->getDhlDeDontHideSenderName())
            $model->dont_hide_sender = true;

        if($model->_product_procedure == '01' && $courierInternal->courier->user->getDhlDeCustomAccountCodeSuffix())
            $model->_suffixAccountCode = $courierInternal->courier->user->getDhlDeCustomAccountCodeSuffix();

        $sender_country_code = strtoupper($from->country0->code);
        $receiver_country_code = strtoupper($to->country0->code);

        if($from->id == CourierHub::HUB_DE_ADDRESS_DATA_ID && $courierInternal->courier->user->getDhlDeCustomData('active'))
        {
            $from->name = $courierInternal->courier->user->getDhlDeCustomData('sender_name');
            $from->company = $courierInternal->courier->user->getDhlDeCustomData('sender_company');
            $from->address_line_1 = $courierInternal->courier->user->getDhlDeCustomData('sender_address_line_1');
            $from->address_line_2 = $courierInternal->courier->user->getDhlDeCustomData('sender_address_line_2');
            $from->city = $courierInternal->courier->user->getDhlDeCustomData('sender_city');
            $from->zip_code = $courierInternal->courier->user->getDhlDeCustomData('sender_zipcode');
            $sender_country_code = $courierInternal->courier->user->getDhlDeCustomData('sender_country');
            $from->tel = $courierInternal->courier->user->getDhlDeCustomData('sender_tel');
            $from->email = $courierInternal->courier->user->getDhlDeCustomData('sender_email');
        }
        elseif(!in_array($to->country_id, CountryList::getUeList(true)))
        {
            $from->name = 'KAAB GMBH';
            $from->company = '';
            $from->address_line_1 = 'BERGENER RING 720';
            $from->address_line_2 = '';
            $from->city = 'OTTENDORF-OKRILLA';
            $from->zip_code = '01458';
            $sender_country_code = 'DE';
            $from->tel = '0048221221218';
            $from->email = 'INFO@SWIATPRZESYLEK.PL';
        }

        // @20.12.2017 - special rule for user EOBUWIE (#947)
        if(in_array($courierInternal->courier->user_id , [947, 2036, 2037]))
        {
            if($to->country_id == CountryList::COUNTRY_SE) {
                $from->name = 'ESKOR.SE';
                $from->company = 'ESKOR.SE';
            }
            else if($to->country_id == CountryList::COUNTRY_DE) {
                $from->name = 'ESCHUHE.DE';
                $from->company = 'ESCHUHE.DE';
            }
            else if($to->country_id == CountryList::COUNTRY_IT) {
                $from->name = 'ESCARPE.IT';
                $from->company = 'ESCARPE.IT';
            }
            else if($to->country_id == CountryList::COUNTRY_ES) {
                $from->name = 'ZAPATOS.ES';
                $from->company = 'ZAPATOS.ES';
            }
        }

        // @05.03.2018 - special rule for user EOBUWIE_ZWROTY (#948)
        if($courierInternal->courier->user_id == 948)
        {
            if($to->country_id == CountryList::COUNTRY_DE && $from->country_id == CountryList::COUNTRY_DE) {
                $to->name = 'ESCHUHE.DE';
                $to->company = '';
                $to->address_line_1 = 'BERGENER RING 720';
                $to->address_line_2 = '';
                $to->city = 'OTTENDORF-OKRILLA';
                $to->zip_code = '01458';
                $receiver_country_code = 'DE';
                $to->tel = '033523386886';
                $to->email = 'INFO@SWIATPRZESYLEK.PL';
            }
        }


        /* @11.05.2018 - special rule for customer ANWIS*/
        if($courierInternal->courier->user_id == 2671) {
            $from->name = $courierInternal->courier->senderAddressData->name;
            $from->tel = $courierInternal->courier->senderAddressData->tel;
            $model->label_annotation_ref = $courierInternal->courier->ref;
        }

        $model->user_id = $courierInternal->courier->user_id;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLDE_PAEKET_PRIO))
            $model->setProduct(self::PRODUCT_PAKET_PRIO);

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLDE_INSURANCE))
            $model->additional_insurance = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLDE_NOTIFICATION))
            $model->notification = true;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address = self::_addressLinesMerge($from->address_line_1, $from->address_line_2);
        $model->sender_tel = $from->tel;
        $model->sender_country = $sender_country_code;
        $model->sender_email = $from->fetchEmailAddress(true);

        if($courierInternal->courier->user->getForcePackageSenderDataDhlDe())
        {
            $model->sender_name = $courierInternal->courier->senderAddressData->name;
            $model->sender_company = $courierInternal->courier->senderAddressData->company;
        }

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = self::_addressLinesMerge($to->address_line_1, $to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $receiver_country_code;
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->packages_number = $courierInternal->courier->packages_number;

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_value = $courierInternal->courier->getPackageValueConverted('EUR');
        $model->package_content = mb_substr($courierInternal->courier->package_content,0,35);
        $model->reference = mb_substr($courierInternal->courier->package_content,0,35);


        $model->cod_value = 0;

        if($courierInternal->courier->cod && $courierInternal->courier->cod_value > 0)
        {
            $model->cod_value = $courierInternal->courier->cod_value;
            $model->cod_currency = $courierInternal->courier->cod_currency;
        }

        if($courierInternal->courier->_points_data)
        {
            $pointDetails = DeliveryPoints::getPointById($courierInternal->courier->_points_data[0], true, false, DeliveryPoints::TYPE_DHLDE);

            if(!$pointDetails)
                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => 'Point not found!',
                    )];
                    return $return;
                } else {
                    return false;
                }

            $model->_point_id = $pointDetails[0];
            $model->_point_type = $pointDetails[1];
            $model->_point_postnummer = $courierInternal->courier->_points_data[1];
            $model->_point_zip_code = $pointDetails[2];


            if($model->_point_type == 'parcelShop')
            {
                $model->parcelshop_street = $pointDetails[3];
                $model->parcelshop_street_no = $pointDetails[4];
            }
        }


        return $model->_createShipment($returnError);
    }


    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {

        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $sender_country_code = strtoupper($from->country0->code);
        $receiver_country_code = strtoupper($to->country0->code);

        $model = new self;
        $model->setProductByCourierUOperatorUniqId($courierTypeU->courierUOperator->uniq_id);

        if($courierTypeU->courier->user->getDhlDeDontHideSenderName())
            $model->dont_hide_sender = true;

        if($model->_product_procedure == '01' && $courierTypeU->courier->user->getDhlDeCustomAccountCodeSuffix())
            $model->_suffixAccountCode = $courierTypeU->courier->user->getDhlDeCustomAccountCodeSuffix();

        {
            if($courierTypeU->courier->user->getDhlDeCustomData('active'))
            {
                $from->name = $courierTypeU->courier->user->getDhlDeCustomData('sender_name');
                $from->company = $courierTypeU->courier->user->getDhlDeCustomData('sender_company');
                $from->address_line_1 = $courierTypeU->courier->user->getDhlDeCustomData('sender_address_line_1');
                $from->address_line_2 = $courierTypeU->courier->user->getDhlDeCustomData('sender_address_line_2');
                $from->city = $courierTypeU->courier->user->getDhlDeCustomData('sender_city');
                $from->zip_code = $courierTypeU->courier->user->getDhlDeCustomData('sender_zipcode');
                $sender_country_code = $courierTypeU->courier->user->getDhlDeCustomData('sender_country');
                $from->tel = $courierTypeU->courier->user->getDhlDeCustomData('sender_tel');
                $from->email = $courierTypeU->courier->user->getDhlDeCustomData('sender_email');
            }
            elseif($from->country_id != CountryList::COUNTRY_DE)
            {
                $from->name = 'KAAB GMBH';
                $from->company = '';
                $from->address_line_1 = 'BERGNER RING 720';
                $from->address_line_2 = '';
                $from->city = 'OTTENDORF-OKRILLA';
                $from->zip_code = '01458';
                $sender_country_code = 'DE';
                $from->tel = '0048221221218';
                $from->email = 'INFO@SWIATPRZESYLEK.PL';
            }
        }



        $model->user_id = $courierTypeU->courier->user_id;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLDE_PAKET_PRIO))
            $model->setProduct(self::PRODUCT_PAKET_PRIO);

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLDE_ADDITIONAL_INSURANCE))
            $model->additional_insurance = true;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLDE_RECEIVER_EMAIL_NOTIFICATIOON))
            $model->notification = true;


        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address = self::_addressLinesMerge($from->address_line_1, $from->address_line_2);
        $model->sender_tel = $from->tel;
        $model->sender_country = $sender_country_code;
        $model->sender_email = $from->fetchEmailAddress(true);

        if($courierTypeU->courier->user->getForcePackageSenderDataDhlDe())
        {
            $model->sender_name = $courierTypeU->courier->senderAddressData->name;
            $model->sender_company = $courierTypeU->courier->senderAddressData->company;
        }

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = self::_addressLinesMerge($to->address_line_1, $to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $receiver_country_code;
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierTypeU->courier->getWeight(true);
        $model->packages_number = $courierTypeU->courier->packages_number;

        $model->package_size_l = $courierTypeU->courier->package_size_l;
        $model->package_size_d = $courierTypeU->courier->package_size_d;
        $model->package_size_w = $courierTypeU->courier->package_size_w;

        $model->package_value = $courierTypeU->courier->getPackageValueConverted('EUR');
        $model->package_content = mb_substr($courierTypeU->courier->package_content,0,35);
        $model->reference = mb_substr($courierTypeU->courier->package_content,0,35);

        $model->cod_value = 0;

        if($courierTypeU->courier->cod && $courierTypeU->courier->cod_value > 0)
        {
            $model->cod_value = $courierTypeU->courier->cod_value;
            $model->cod_currency = $courierTypeU->courier->cod_currency;
        }

        if($courierTypeU->courier->_points_data)
        {
            $pointDetails = DeliveryPoints::getPointById($courierTypeU->courier->_points_data[0], true, DeliveryPoints::TYPE_DHLDE);

            if(!$pointDetails)
                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => 'Point not found!',
                    )];
                    return $return;
                } else {
                    return false;
                }

            $model->_point_id = $pointDetails[0];
            $model->_point_type = $pointDetails[1];
            $model->_point_postnummer = $courierTypeU->courier->_points_data[1];
            $model->_point_zip_code = $pointDetails[2];


            if($model->_point_type == 'parcelShop')
            {
                $model->parcelshop_street = $pointDetails[3];
                $model->parcelshop_street_no = $pointDetails[4];
            }
        }

        return $model->_createShipment($returnError);
    }


    public static function orderForCourierReturn(CourierTypeReturn $courierTypeReturn, AddressData $to, $returnError = false)
    {

        $from = $courierTypeReturn->courier->senderAddressData;

        $sender_country_code = strtoupper($from->country0->code);
        $receiver_country_code = strtoupper($to->country0->code);

        $model = new self;
        $model->setProduct(self::PRODUCT_PAKET);

        if($courierTypeReturn->courier->user->getDhlDeDontHideSenderName())
            $model->dont_hide_sender = true;

        if($model->_product_procedure == '01' && $courierTypeReturn->courier->user->getDhlDeCustomAccountCodeSuffix())
            $model->_suffixAccountCode = $courierTypeReturn->courier->user->getDhlDeCustomAccountCodeSuffix();



        $model->user_id = $courierTypeReturn->courier->user_id;

        // @12.09.2018 - special rule for user EOBUWIE (#948)
        if(in_array($courierTypeReturn->courier->user_id, [948 , 2036, 2037]))
        {
            if($to->country_id == CountryList::COUNTRY_DE && $from->country_id == CountryList::COUNTRY_DE) {
                $to->name = 'ESCHUHE.DE';
                $to->company = '';
                $to->address_line_1 = 'BERGENER RING 720';
                $to->address_line_2 = '';
                $to->city = 'OTTENDORF-OKRILLA';
                $to->zip_code = '01458';
                $receiver_country_code = 'DE';
                $to->tel = '033523386886';
                $to->email = 'INFO@SWIATPRZESYLEK.PL';
            }
        }

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address = self::_addressLinesMerge($from->address_line_1, $from->address_line_2);
        $model->sender_tel = $from->tel;
        $model->sender_country = $sender_country_code;
        $model->sender_email = $from->fetchEmailAddress(true);

        if($courierTypeReturn->courier->user->getForcePackageSenderDataDhlDe())
        {
            $model->sender_name = $courierTypeReturn->courier->senderAddressData->name;
            $model->sender_company = $courierTypeReturn->courier->senderAddressData->company;
        }

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = self::_addressLinesMerge($to->address_line_1, $to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $receiver_country_code;
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierTypeReturn->courier->getWeight(true);
        $model->packages_number = $courierTypeReturn->courier->packages_number;

        $model->package_size_l = $courierTypeReturn->courier->package_size_l;
        $model->package_size_d = $courierTypeReturn->courier->package_size_d;
        $model->package_size_w = $courierTypeReturn->courier->package_size_w;

        $model->package_value = $courierTypeReturn->courier->getPackageValueConverted('EUR');
        $model->package_content = mb_substr($courierTypeReturn->courier->package_content,0,35);
        $model->reference = mb_substr($courierTypeReturn->courier->package_content,0,35);

        return $model->_createShipment($returnError);
    }


    // used for Courier - groups packages in groups of 20 to save request to dhl de
    public static function track($number)
    {
        return self::trackSingle($number);


        // TRACK MULTI DO NOT HANDLE RETURNS!!!
        // TRACK MULTI DO NOT HANDLE RETURNS!!!
        // TRACK MULTI DO NOT HANDLE RETURNS!!!
        return false;

        $FLAG_FILE = sys_get_temp_dir().DIRECTORY_SEPARATOR.'DHLDD_TT_MERGE';
        $fp = fopen($FLAG_FILE, "a+");

        if (flock($fp, LOCK_EX)) {  // acquire an exclusive lock
            rewind($fp);
            $ids = fgets($fp);
            $ids = json_decode($ids, true);

            if(!is_array($ids))
                $ids = [];

            $ids[$number] = $number;

            if (S_Useful::sizeof($ids) < (self::TT_GROUP_NO))
            {
//                MyDump::dump('dhlde_tt_merge.txt', 'MERGE: '.print_r($ids,1));
                $ids = json_encode($ids);
                ftruncate($fp, 0);
                fwrite($fp, $ids);
                fflush($fp);            // flush output before releasing the lock
                flock($fp, LOCK_UN);    // release the lock
                fclose($fp);
                return false;
            }

        } else {
            MyDump::dump('dhlde_tt_merge.txt', 'Couldnt get the lock!');
            return false;
        }

        // if real checking data - clear file and release lock
        {
            ftruncate($fp, 0);
            fflush($fp);            // flush output before releasing the lock
            flock($fp, LOCK_UN);    // release the lock
            fclose($fp);

            $requestId = date('Ymdhis').uniqid();
            MyDump::dump('dhlde_tt_merge.txt', 'RUN ('.$requestId.') : '.print_r($ids,1));


            $login = self::USERNAME_TT;
            $pass = self::PASSWORD_TT;

            $ids = implode(';', $ids);

            $xml = '<?xml version="1.0"?>
<data appname="'.$login.'" password="'.$pass.'" request="d-get-piece-detail" language-code="en" piece-code="'.$ids.'"/>
';
            $context = stream_context_create(
                array(
                    'http' => array(
                        'header' => "Authorization: Basic " . base64_encode(self::USERNAME . ":" . self::PASSWORD) . "\r\n" .
                            "Content-type: text/xml \r\n"
                    )
                )
            );

            $xml = urlencode($xml);
            $url = self::URL_TT.'?xml=' . $xml;


            MyDump::dump('dhlde_tt_merge.txt', 'REQUEST ('.$requestId.'): '.print_r($url,1));

            $resp = @file_get_contents($url,false, $context);


            $statuses = [];
            if($resp)
            {
                $xml = @simplexml_load_string($resp);

                if($xml)
                {
                    if($xml->data)
                        foreach($xml->data AS $item) {

                            $packageId = (string) $item['piece-code'];
                            $statuses[$packageId] = [];

                            if($item->data->data)
                                foreach ($item->data->data AS $node) {

                                    $status = (string) $node['event-status'];
                                    $date = (string) $node['event-timestamp'];
                                    $location = (string) $node['event-location'] . '/' . (string)$node['event-country'];

                                    if ($status == '')
                                        continue;

                                    self::_convertStatus($status, $location);

                                    if ($status == '')
                                        continue;



                                    // if package moved to USPS, get tracking from TRACKINGMORE
                                    if($location == 'https://www.usps.com/')
                                    {
                                        $tmStatuses = Client17Track::getTt($no, false, false, false, 'DHLDE');
//                                        $tmStatuses = TrackingmoreClient::getTt($packageId, 'usps');
                                        $statuses = array_merge($statuses[$packageId], $tmStatuses);

                                    } else {

                                        $statuses[$packageId][] = [
                                            'name' => $status,
                                            'date' => $date,
                                            'location' => $location == '/' ? NULL : $location,
                                        ];

                                    }

                                }
                        }

                    Yii::app()->getModule('courier');

                    MyDump::dump('dhlde_tt_merge.txt', 'RESP-CONVERTED ('.$requestId.'): '.print_r($statuses,1));



                    CourierExternalManager::groupUpdateTt($statuses, CourierStat::EXTERNAL_SERVICE_DHLDE);
                }
            }

        }


        return false;
    }

    // used for Postal
    public static function trackSingle($number)
    {
        $tempData = [
            'external ids',
            'CF530954322DE',
            'CF530954336DE',
            'CF530954407DE',
            'CF530954415DE',
            'CF530954490DE',
            'CF530954526DE',
            'CF530954574DE',
            'CF530954628DE',
            'CF530954733DE',
            'CF530954804DE',
            'CF530954870DE',
            'CF530954897DE',
            'CF530954923DE',
            'CF530954968DE',
            'CF530955107DE',
            'CF530955155DE',
            'CF530955226DE',
            'CF530955274DE',
            'CF530955305DE',
            'CF530955478DE',
            'CF530955566DE',
            'CF530955583DE',
            'CF530955654DE',
            'CF530955844DE',
            'CF530955858DE',
            'CF530955932DE',
            'CF530955946DE',
            'CF530956045DE',
            'CF530956062DE',
            'CF530956147DE',
            'CF530956283DE',
            'CF530956323DE',
            'CF530956337DE',
            'CF530956345DE',
            'CF530956368DE',
            'CF530956385DE',
            'CF530956399DE',
            'CF530956408DE',
            'CF530956460DE',
            'CF530956473DE',
            'CF530956513DE',
            'CF530956544DE',
            'CF530956558DE',
            'CF530956629DE',
            'CF530956632DE',
            'CF530956751DE',
            'CF530956819DE',
            'CF530954375DE',
            'CF530956924DE',
            'CF530956955DE',
            'CF530956969DE',
            'CF530957125DE',
            'CF530957187DE',
            'CF530957244DE',
            'CF530957394DE',
            'CF530957403DE',
            'CF530957482DE',
            'CF530957519DE',
            'CF530957686DE',
            'CF530958196DE',
            'CF530958219DE',
            'CF530958298DE',
            'CF530958412DE',
            'CF530958488DE',
            'CF530958678DE',
            'CF530958681DE',
            'CF530958735DE',
            'CF530958766DE',
            'CF530958770DE',
            'CF530958783DE',
            'CF530958806DE',
            'CF530958823DE',
            'CF530958845DE',
            'CF530958899DE',
            'CF530958911DE',
            'CF530958925DE',
            'CF530958939DE',
            'CF530958942DE',
            'CF530958960DE',
            'CF530958995DE',
            'CF530959015DE',
            'CF530959041DE',
            'CF530959086DE',
            'CF530959090DE',
            'CF530959109DE',
            'CF530959126DE',
            'CF530959165DE',
            'CF530959205DE',
            'CF530959262DE',
            'CF530959470DE',
            'CF530959506DE',
            'CF530959510DE',
            'CF530959568DE',
            'CF530959727DE',
            'CF530959735DE',
            'CF530959903DE',
            'CF530960005DE',
            'CF530960036DE',
            'CF530960067DE',
            'CF530960257DE',
            'CF530960331DE',
            'CF530960345DE',
            'CF530960393DE',
            'CF530960495DE',
            'CF530960504DE',
            'CF530960535DE',
            'CF530960549DE',
            'CF530960787DE',
            'CF530960795DE',
            'CF530960835DE',
            'CF530960932DE',
            'CF530960985DE',
            'CF530960994DE',
            'CF530961062DE',
            'CF530961116DE',
            'CF530961120DE',
            'CF530961133DE',
            'CF530961181DE',
            'CF530961195DE',
            'CF530961297DE',
            'CF530961306DE',
            'CF530961371DE',
            'CF530961456DE',
            'CF530961535DE',
            'CF530961544DE',
            'CF530961601DE',
            'CF530961663DE',
            'CF530961694DE',
            'CF530961717DE',
            'CF530961805DE',
            'CF530961822DE',
            'CF530961836DE',
            'CF530961840DE',
            'CF530961875DE',
            'CF530961884DE',
            'CF530961924DE',
            'CF530961955DE',
            'CF530962006DE',
            'CF530962037DE',
            'CF530962045DE',
            'CF530962068DE',
            'CF530962235DE',
            'CF530962258DE',
            'CF530962292DE',
            'CF530962350DE',
            'CF530962385DE',
            'CF530962417DE',
            'CF530962448DE',
            'CF530962505DE',
            'CF530962575DE',
            'CF530962584DE',
            'CF530962598DE',
            'CF530962607DE',
            'CF530962672DE',
            'CF530962726DE',
            'CF530962743DE',
            'CF530962765DE',
            'CF530962774DE',
            'CF530962805DE',
            'CF530962828DE',
            'CF530962831DE',
            'CF530962893DE',
            'CF530963372DE',
            'CF530963412DE',
            'CF530963443DE',
            'CF530963576DE',
            'CF530963602DE',
            'CF530963616DE',
            'CF530963633DE',
            'CF530963655DE',
            'CF530963766DE',
            'CF530963770DE',
            'CF530963911DE',
            'CF530964086DE',
            'CF530964112DE',
            'CF530964130DE',
            'CF530964143DE',
            'CF530964214DE',
            'CF530964276DE',
            'CF530964302DE',
            'CF530964316DE',
            'CF530964466DE',
            'CF530964506DE',
            'CF530964523DE',
            'CF530964695DE',
            'CF530964700DE',
            'CF530964727DE',
            'CF530964789DE',
            'CF530964815DE',
            'CF530964979DE',
            'CF530964982DE',
            'CF530965387DE',
            'CF530965501DE',
            'CF530965577DE',
            'CF530965585DE',
            'CF530965625DE',
            'CF530965705DE',
            'CF530965798DE',
            'CF530965815DE',
            'CF530965869DE',
            'CF530965991DE',
            'CF530966087DE',
            'CF530966263DE',
        ];




        $login = self::USERNAME_TT;
        $pass = self::PASSWORD_TT;

        $xml = '<?xml version="1.0"?>
<data appname="'.$login.'" password="'.$pass.'" request="d-get-piece-detail" language-code="en" piece-code="'.$number.'"/>
';
        $context = stream_context_create(
            array(
                'http' => array(
                    'header' => "Authorization: Basic " . base64_encode(self::USERNAME . ":" . self::PASSWORD) . "\r\n" .
                        "Content-type: text/xml \r\n"
                )
            )
        );

        $xml = urlencode($xml);
        $url = self::URL_TT.'?xml=' . $xml;

        $resp = @file_get_contents($url,false, $context);

        $statuses = [];
        if($resp)
        {
            $xml = @simplexml_load_string($resp);


            if($xml)
            {
                $return = false;
                if(((string)($xml->data['ruecksendung'])) == 'true')
                    $return = true;


                $data = $xml->data->data->data;
                if($data)
                    foreach($data AS $node) {

                        $status = (string) $node['event-status'];
                        $date = (string) $node['event-timestamp'];
                        $location = (string) $node['event-location'].'/'.(string) $node['event-country'];

                        if($status == '')
                            continue;

                        self::_convertStatus($status, $location);

                        if ($status == '')
                            continue;

                        if($return && (!in_array(strtoupper($number), $tempData) OR $date < '2018-11-14'))
                            $status = '[RETURN] '.$status;


                        $add17TrackLastOperator = false;

                        // if package moved to USPS, get tracking from TRACKINGMORE
                        if($location == 'https://www.usps.com/')
                            $add17TrackLastOperator = true;
                        else if($location == '/Sweden' OR preg_match('%postnord\.com%', $location)) // for Sweden @ 18.12.2018...
                            $add17TrackLastOperator = true;
                        else if($location == '/France' OR preg_match('%laposte.\.fr%', $location)) // for France @ 13.05.2019...
                            $add17TrackLastOperator = true;
                        else
                            array_push($statuses, [
                                'name' => $status,
                                'date' => $date,
                                'location' => $location == '/' ? NULL : $location,
                            ]);

                    }

                if($add17TrackLastOperator)
                {
                    $tmStatuses = Client17Track::getTt($number, false, false, true, 'DHLDE');
                    if(is_array($tmStatuses))
                        $statuses = array_merge($statuses, $tmStatuses);
                }

                return $statuses;
            }
        }
        return false;
    }

    protected static function _convertStatus(&$status, &$location)
    {

        if($status == 'The instruction data for this shipment have been provided by the sender to DHL electronically')
        {
            $status = false;
            return;
        }

        $status = strip_tags($status);
        if(preg_match('/The shipment is being brought to retail outlet (.*) for pick-up. The earliest time when it can be picked up can be found on the notification card./i', $status, $matches)) {
            $status = 'The shipment is being brought to retail outlet for pick-up. The earliest time when it can be picked up can be found on the notification card.';
            $location = trim($matches[1]);
        } else if(preg_match('/The shipment has been delivered for pick-up at the postal retail outlet (.*)\./i', $status, $matches)) {
            $status = 'The shipment has been delivered for pick-up at the postal retail outlet.';
            $location = trim($matches[1]);
        } else if(preg_match('/The shipment is ready for pick-up at the (.*)\./i', $status, $matches)) {
            $status = 'The shipment is ready for pick-up.';
            $location = trim($matches[1]);
        } else if(preg_match('/The (.*) requested the preferred delivery day (.*)\./i', $status, $matches)) {
            $status = 'The customer requested the preferred delivery day.';
            $location = trim($matches[2]).' by '. trim($matches[1]);
        }
        else if(preg_match('%The shipment will be transported to the destination country and, from there, handed over to the delivery organization. \(Homepage / online shipment tracking: (.*)\)%i', $status, $matches)) {
            $status = 'The shipment will be transported to the destination country and, from there, handed over to the delivery organization. (Homepage / online shipment tracking)';
            $location = trim($matches[1]);
        } else if(preg_match('/The recipient has selected a new one-time delivery address: (.*)/i', $status, $matches)) {
            $status = 'The recipient has selected a new one-time delivery address.';
            $location = trim($matches[1]);

        } else if(preg_match('/Die Zustellung am Wunschtag (.*) wurde durch den Anonymous customer. beauftragt./i', $status, $matches)) {
            $status = 'Die Zustellung am Wunschtag wurde durch den Anonymous customer. beauftragt.';
            $location = trim($matches[1]);

        } else if(preg_match('/Die Zustellung am Wunschtag (.*) wurde durch den Recipient. beauftragt./i', $status, $matches)) {
            $status = 'Die Zustellung am Wunschtag wurde durch den Recipient. beauftragt.';
            $location = trim($matches[1]);

        } else if(preg_match('/The shipment is currently at the DHL parcel shop (.*) and is ready to be picked up./i', $status, $matches)) {
            $status = 'The shipment is currently at the DHL parcel shop and is ready to be picked up.';
            $location = trim($matches[1]);
        } else if(preg_match('%The shipment will be delivered for pick-up at the postal retail outlet (.*). The earliest possible pick-up time can be found in the text message/e-mail.%i', $status, $matches)) {
            $status = 'The shipment will be delivered for pick-up at the postal retail outlet. The earliest possible pick-up time can be found in the text message/e-mail.';
            $location = trim($matches[1]);
        } else if(preg_match('%The C\.O\.D\. (.*) has been transferred to account (.*) and sort code%i', $status, $matches)) {
            $status = 'The C.O.D. has been transferred to account and sort code';
//                            $location = trim($matches[1]);
        } else if(preg_match('/The shipment is being brought to Filiale (.*) for pick-up. The earliest time when it can be picked up can be found on the notification card./i', $status, $matches)) {
            $status = 'The shipment is being brought to Filiale for pick-up. The earliest time when it can be picked up can be found on the notification card.';
            $location = trim($matches[1]);
        } else if(preg_match('%The shipment will be delivered for pick-up at the Filiale (.*). The earliest possible pick-up time can be found in the text message/e-mail.%i', $status, $matches)) {
            $status = 'The shipment will be delivered for pick-up at the Filiale';
            $location = trim($matches[1]);
        } else if(preg_match('/Die Zustellung am Wunschtag (.*) wurde beauftragt./i', $status, $matches)) {
            $status = 'Die Zustellung am Wunschtag wurde beauftragt.';
            $location = '';
        } else if(preg_match('/The shipment has been delivered for pick-up at the Filiale (.*)./i', $status, $matches)) {
            $status = 'The shipment has been delivered for pick-up at the Filiale';
            $location = trim($matches[1]);
        }

    }


    /**
     * @param string $number
     * @return bool|string Returns false or PDF string with ePod
     */
    public static function epod($number)
    {

        $CACHE_NAME = 'DHLDE_EPOD_'.$number;
        $result = Yii::app()->cacheUserData->get($CACHE_NAME);

        if($result === false) {

            $login = self::USERNAME_TT;
            $pass = self::PASSWORD_TT;

            $xml = '<?xml version="1.0"?>
<data appname="' . $login . '" password="' . $pass . '" request="d-get-signature" language-code="en" piece-code="' . $number . '"/>
';
            $context = stream_context_create(
                array(
                    'http' => array(
                        'header' => "Authorization: Basic " . base64_encode(self::USERNAME . ":" . self::PASSWORD) . "\r\n" .
                            "Content-type: text/xml \r\n"
                    )
                )
            );

            $xml = urlencode($xml);
            $url = self::URL_TT . '?xml=' . $xml;

            $resp = @file_get_contents($url, false, $context);

            if ($resp) {
                $xml = @simplexml_load_string($resp);

                if ($xml) {

                    $img = (string)$xml->data['image'];
                    if($img) {
                        $decoded = '';
                        for ($i = 0; $i < strlen($img); $i += 2)
                            $decoded .= chr(hexdec($img[$i] . $img[$i + 1]));

                        // GENERATE OWN:
                        $pdf = PDFGenerator::initialize();
                        $pdf->setFontSubsetting(false);

                        $pdf->addPage('H', 'A4');
                        $width = $pdf->getPageWidth() - 10;
                        $pdf->Image('@' . ($decoded), 5, 5, $width, '', '', 'N', false);

                        $result = $pdf->Output('epod.pdf', 'S');
                    }
                }
            }

            Yii::app()->cacheUserData->set($CACHE_NAME, $result, (60*60*24));
        }

        return $result;

    }

    /**
     * @param $address
     * @return DeliveryPoints[]
     * @throws CDbException
     * @throws CException
     */
    public static function getPoints($address)
    {

        $CACHE_NAME = 'DHLDE_POINTS_'.$address;

        $data = Yii::app()->cache->get($CACHE_NAME);
//        $data = false;
        if($data === false) {

            $context = stream_context_create(
                array(
                    'http' => array(
                        'header' => "Authorization: Basic " . base64_encode(self::USERNAME . ":" . self::PASSWORD) . "\r\n" .
                            "Content-type: text/xml \r\n"
                    )
                )
            );

            MyDump::dump('dhl_de_points.txt', 'REQ: '.$address);

            $url = self::URL_POINTS.'/parcellocationByAddress/'.urlencode((string)$address);
            $resp = @file_get_contents($url, false, $context);

            MyDump::dump('dhl_de_points.txt', 'RESP: '.print_r($resp,1));

            if ($resp) {
                $resp = json_decode($resp);

                $data = [];
                if (is_array($resp->psfParcellocationList))
                    foreach ($resp->psfParcellocationList AS $item) {

                        if(strtoupper($item->countryCode) == 'pl')
                            continue;

                        if($item->primaryKeyZipRegion == '') // these points are useless
                            continue;

//                        $data[] = [
//                            'id' => $item->primaryKeyPSF,
//                            'lat' => $item->geoPosition->latitude,
//                            'lng' => $item->geoPosition->longitude,
//                            'address' => trim($item->street . ' ' . $item->houseNo . ' ' . $item->additionalStreet) . ', '.$item->zipCode.', ' . $item->city.', '.strtoupper($item->countryCode),
//                            'details' => $item->additionalInfo,
//                            'hours' => '',
//                            'name' => $item->shopType . ' : ' . $item->shopName,
//                        ];



                        $name = $item->shopName != '' ? $item->shopName : $item->shopType;

                        $other = [];
                        $other['additionalInfo'] = NULL;
                        if($item->additionalInfo != '' AND $item->additionalInfo != 'null')
                            $other['additionalInfo'] = $item->additionalInfo;

                        $other['primaryKeyZipRegion'] = $item->primaryKeyZipRegion;
                        $other['street'] = $item->street;
                        $other['house_no'] = $item->houseNo;

                        $dp = DeliveryPoints::getUpdateDeliveryPoints(DeliveryPoints::TYPE_DHLDE, $item->primaryKeyPSF, trim($item->street . ' ' . $item->houseNo . ' ' . $item->additionalStreet), $item->zipCode,  $item->city, strtoupper($item->countryCode), $name, $item->geoPosition->latitude, $item->geoPosition->longitude, $item->shopType, $other);
                        $data[] = $dp;

                    }

                Yii::app()->cache->set($CACHE_NAME, $data, 60*60*24);
            }

        }
        return $data;
    }



}

class PrintOnlyIfCodeable
{
    public $active = 0;

    function __construct($active)
    {
        $this->active = $active;
    }
}

class CashOnDelivery
{
    public $active = 1;
    public $codAmount = 0;

    function __construct($codAmount)
    {
        $this->codAmount = $codAmount;
    }
}

class AdditionalInsurance
{
    public $active = 1;
    public $insuranceAmount = 0;

    function __construct($insuranceAmount)
    {
        $this->insuranceAmount = $insuranceAmount;
    }
}
