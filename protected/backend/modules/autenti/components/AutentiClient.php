<?php

class AutentiClient
{
    const URL = 'https://app.autenti.eu';
//    const URL = 'http://kaabpl.pl';
    const CLIENT_ID = '03876059';
    const CLIENT_SECRET = 'd4r4u1BDMXwyw69Xag6UKeqbolumyIjD';
    const EMAIL = 'marzenamakulewicz@swiatprzesylek.pl';

    public static function createNewAgreement(array $documents, $title, $message, $receiver_email, $receiver_tel, $receiver_name, $receiver_surname, $receiver_company, $receiver_nip, $receiver_country)
    {
        $model = new self;

        $data = new stdClass();
        $data->title = $title;
        $data->message = $message;
        $data->status = 'NEW';
        $data->sender_email = self::EMAIL;
        $data->receivers = [];

        $receiver = new stdClass();
        $receiver->email = $receiver_email;
        $receiver->phone_number = $receiver_tel;
        $receiver->user_first_name = $receiver_name;
        $receiver->user_last_name = $receiver_surname;
        $receiver->company_name = $receiver_company;
        $receiver->company_nip = $receiver_nip;
        $receiver->company_country = $receiver_country;

        $data->receivers[] = $receiver;

        var_dump($model->_getToken());
        var_dump(json_encode($data));

//        try {
//            $resp = $model->_call('api/identities/' . $model->_getToken()[1] . '/documents/add', $data, false);
//        }
//        catch(Exception $ex)
//        {
//            throw new Exception($ex->getMessage());
//        }

//        if($resp)
//        {
//            $resp = json_decode($resp);
//            $documentId = $resp->id;
//
//
//            try {
//                foreach($documents AS $document)
//                    $resp = $model->_call('api/identities/'.$model->_getToken()[1].'/documents/'.$documentId.'/upload-file', false, false, $document);
//            }
//            catch(Exception $ex)
//            {
//                throw new Exception($ex->getMessage());
//            }
//
//            if($resp)
//            {
//                $resp = json_decode($resp);

                $data = new stdClass();
                $data->senderAutosign = true;
                $data->redirectUrl = ('http://www.swiaptrzesylek.pl');
                $data->requireSmsCode = true;

                var_dump(json_encode($data));
                exit;

                try {

                    $resp = $model->_call('api/identities/'.$model->_getToken()[1].'/documents/'.$documentId.'/start', $data, false);
                }
                catch(Exception $ex)
                {
                    throw new Exception($ex->getMessage());
                }

                return $documentId;
//            }
//            else
//                throw new Exception('No response!');
//
//        }
//        else
//            throw new Exception('No response!');

    }

    public static function checkAgreementStatus($id)
    {
        $model = new self;
        $resp = $model->_call('api/identities/'.$model->_getToken()[1].'/documents/'.$id, false);

        $resp = json_decode($resp);

        return $resp;
    }

    public static function test()
    {

        $model = new self;

        $data = new stdClass();
        $data->senderAutosign = true;
//        $data->redirectUrl = urlencode('http://www.swiaptrzesylek.pl');
        $data->redirectUrl = ('http://www.swiaptrzesylek.pl');
        $data->requireSmsCode = false ;


        try {

            $resp = $model->_call('api/identities/'.$model->_getToken()[1].'/documents/167969/start', $data, );
        }
        catch(Exception $ex)
        {
            throw new Exception($ex->getMessage());
        }

    }

    protected function _clearTokenCache()
    {
        $CACHE_NAME = 'AUTENTI_TOKEN';
        Yii::app()->cache->set($CACHE_NAME, false); // 4 hours
    }

    protected function _getToken()
    {
        $CACHE_NAME = 'AUTENTI_TOKEN';

        $token = Yii::app()->cache->get($CACHE_NAME);

        if($token === false)
        {

            $data = new stdClass();
            $data->client_id = self::CLIENT_ID;
            $data->client_secret = self::CLIENT_SECRET;
            $data->grant_type = 'company_key';
            $data->email = self::EMAIL;

            $resp = $this->_call('oauth/token', $data, true);
            $resp = json_decode($resp);
            if($resp->access_token)
            {
                $token = [
                    $resp->access_token,
                    $resp->identity_id
                ];
            }
            Yii::app()->cache->set($CACHE_NAME, $token, 14400); // 4 hours
        }

        return $token;
    }

    public static function downloadFileByUrl($url)
    {
        $model = new self;
        $context = stream_context_create([
            "http" => [
                "header" => 'Authorization: Bearer ' . $model->_getToken()[0],
            ]
        ]);

        return file_get_contents($url, false, $context );
    }

    protected function _call($method, $data, $loginMethod = false, $document = false, $retry = 0)
    {

        $url = self::URL.'/'.$method;


        if(!$document)
            MyDump::dump('autenti.txt', 'REQ : '. $url.' : '.print_r(json_encode($data), 1));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);


        // it's getting token
        if($loginMethod) {

            $array = [];
            $array[] = 'Content-Type: application/x-www-form-urlencoded';

            curl_setopt($ch, CURLOPT_HTTPHEADER, $array);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        } else if($document) {

            $data = [
                'file' => $document
            ];


            MyDump::dump('autenti.txt', 'REQ : '. $url.' : '.print_r(json_encode($data), 1));

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $array = [];
            $array[] = 'Authorization: Bearer ' . $this->_getToken()[0];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $array);

        } else {
            MyDump::dump('autenti.txt', 'Token : '. $this->_getToken()[0]);

            $array = [];
            $array[] = 'Content-Type: application/json; charset=UTF-8';
            $array[] = 'Authorization: Bearer ' . $this->_getToken()[0];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $array);

            if($data) {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            }
        }

        $response = curl_exec($ch);

        $info =  curl_getinfo($ch);

        $request_header_info = curl_getinfo($ch, CURLINFO_HEADER_OUT);
//        var_dump($request_header_info);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $error = curl_error($ch);


        if($httpcode == 401 && $retry <2)
        {
            $this->_clearTokenCache();
            return $this->_call($method, $data, $loginMethod, $document, ++$retry);
        }

        MyDump::dump('autenti.txt', 'RESP : '. print_r($response, 1));
        MyDump::dump('autenti.txt', 'RESP CODE : '. print_r($httpcode, 1));
        MyDump::dump('autenti.txt', 'RESP ERROR : '. print_r($error, 1));


        if(!in_array($httpcode, [200,201]))
        {
            throw new Exception($httpcode);
        }

        return $response;

    }


}