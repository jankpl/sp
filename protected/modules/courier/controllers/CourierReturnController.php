<?php

class CourierReturnController extends Controller {

    const FROM_SCRATCH_ID = 1;

    public function beforeAction($action)
    {

        $this->panelLeftMenuActive = 161;

        if(Yii::app()->user->model->getCourierReturnAvailable())
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier',

            ));
            exit;
        }
    }


    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function accessRules()
    {
        return array(

            array('allow',
//                'actions'=>array('create','created','index', 'list','ajaxGetOperatorList', 'ajaxGetCodCurrency', 'ajaxCheckRemoteArea', 'ajaxCheckAdditions'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect(['/courier/courierReturn/create']);
    }


    protected function createHash($base_package_id)
    {
        $hash = hash('sha256', uniqid().$_SERVER['REMOTE_ADDR'].time());

        $data = new stdClass();
        $data->base_package_id = $base_package_id;

        Yii::app()->cache->set($hash, $data, 60*60);

        $this->redirect( ['/courier/courierReturn/create', 'hash' => $hash]);
        Yii::app()->end();
    }

    protected function getHashData($hash)
    {
        $base_package_id = Yii::app()->cache->get($hash, false);

        if($base_package_id === false)
            throw new Exception('Hash not found');
        else
            return $base_package_id;
    }

    public function actionCreateFor($hash)
    {
        $courier = Courier::model()->findByAttributes(['hash' => $hash, 'user_id' => Yii::app()->user->id]);

        if($courier === NULL)
            throw new HttpException(403);

        try {
            $newCourier = CourierTypeReturn::createPackageOnCourier($courier);
        }
        catch(Exception $ex)
        {
            $this->render('//site/notify', array(
                'text' => $ex->getMessage(),
                'header' => 'Courier',

            ));
        }

        if($newCourier instanceof Courier)
        {
            $newCourier->refresh();
            $this->redirect(['/courier/courier/view', 'urlData' => $newCourier->hash]);
            Yii::app()->end();
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('site', 'Wystątpił błąd.'),
                'header' => 'Courier',

            ));
        }

    }

    public function actionCreate()
    {

        if(Yii::app()->session['courierReturnFormFinished']) // prevent going back and sending the same form again
        {
            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['courierReturnFormFinished']);
            $this->refresh(true);
        }


        //for AJAX validation
//        {
//            $model=new Courier('insert');
//            $this->performAjaxValidation($model, 'courier-form');
//            unset($model);
//        }

        if(isset($_POST['summary']))
            $this->create_step_summary(); // Validate addition list, summary
        elseif (isset($_POST['finish']) OR isset($_POST['finish_goback']))
            $this->create_finish(isset($_POST['finish_goback']) ? true : false);
        else
            $this->create_step_1(); // this is the default, first time (step1)

    }

    protected function create_step_1($validate = false)
    {
        if($validate)
        {
            if($this->create_validate_step_1() == 'break')
                return;
        }

        $this->setPageState('form_started', true);
        $this->setPageState('start_user_id', Yii::app()->user->id);

        $this->setPageState('last_step',1); // save information about last step

        $model = new Courier_CourierTypeReturn();


        $model->senderAddressData = new CourierTypeReturn_AddressData();
        $model->receiverAddressData = new CourierTypeReturn_AddressData();


        $model->senderAddressData->country_id = CountryList::COUNTRY_PL;
        $model->receiverAddressData->country_id = Yii::app()->user->model->getDefaultPackageReceiverCountryId() ? Yii::app()->user->model->getDefaultPackageReceiverCountryId() : CountryList::COUNTRY_PL;

        // load user data as sender
        if(!S_Useful::sizeof($this->getPageState('step1_senderAddressData',array())))
            $model->senderAddressData->loadUserData();
        else
            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());


        $model->packages_number = 1;

        $model->user_id = Yii::app()->user->id;

        $model->courierTypeReturn = new CourierTypeReturn();

        $model->courierTypeReturn->courier = $model;



        $model->attributes = $this->getPageState('step1_model',[]);
        $model->courierTypeReturn->attributes = $this->getPageState('step1_CourierTypeReturn', []);



        $this->setPageState('last_step',1); // save information about last step


//            $model->courierTypeReturn->_selected_operator_id = $model->courierTypeReturn->courier_u_operator_id;

        $this->render('create/step1',
            [
                'model' => $model,
                'countries' => CourierTypeReturn::getAvailableCountryList(true),
            ]);

    }

    protected function create_validate_step_1()
    {
        $model = new Courier_CourierTypeReturn(Courier_CourierTypeReturn::SCENARIO_STEP_1);
        $model->courierTypeReturn = new CourierTypeReturn(CourierTypeReturn::SCENARIO_STEP_1);
        $model->courierTypeReturn->courier = $model;

        $model->user_id = Yii::app()->user->id;
        $model->senderAddressData = new CourierTypeReturn_AddressData();
        $model->receiverAddressData = new CourierTypeReturn_AddressData();

//        $currency = CourierTypeReturn::getCurrencyIdForCurrentUser();
        $errors = false;
        if(isset($_POST['Courier_CourierTypeReturn']))
        {
            $model->attributes = $_POST['Courier_CourierTypeReturn'];
            $model->courierTypeReturn->attributes = $_POST['CourierTypeReturn'];
            $model->senderAddressData->attributes = $_POST['CourierTypeReturn_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['CourierTypeReturn_AddressData']['receiver'];

            $model->package_weight_client = $model->package_weight;


            if($model->packages_number)
                $model->package_weight = (double) $model->_package_weight_total / $model->packages_number;

            if(!isset($_POST['CourierTypeReturn']))
                $_POST['CourierTypeReturn'] = [];

            $this->setPageState('step1_model',$model->getAttributes(array_merge(array_keys($_POST['Courier_CourierTypeReturn']),array('package_weight_client'))));  // save previous form into form state
            $this->setPageState('step1_CourierTypeReturn',$model->courierTypeReturn->getAttributes(array_merge(array_keys($_POST['CourierTypeReturn']),array( '_package_wrapping')))); // save previous form into form state
            $this->setPageState('step1_senderAddressData', $model->senderAddressData->getAttributes(array_keys($_POST['CourierTypeReturn_AddressData']['sender']))); // save previous form into form state
            $this->setPageState('step1_receiverAddressData',$model->receiverAddressData->getAttributes(array_keys($_POST['CourierTypeReturn_AddressData']['receiver']))); // save previous form into form state


            if(!$model->validate())
                $errors = true;

            if(!$model->courierTypeReturn->validate())
                $errors = true;


            if(!$model->senderAddressData->validate())
                $errors = true;

            if(!$model->receiverAddressData->validate())
                $errors = true;


            if(!in_array($model->senderAddressData->country_id, CourierTypeReturn::getAvailableAutomaticCoutryList())) {
                $model->senderAddressData->addError('country_id', Yii::t('courier', 'Ten kraj nie jest obsługiwany'));
                $errors = true;
            }

        }



//        $remoteZipSender = CourierDhlRemoteAreas::findZip($model->senderAddressData->zip_code, $model->senderAddressData->country_id, $model->senderAddressData->city);
//        $remoteZipReceiver = CourierDhlRemoteAreas::findZip($model->receiverAddressData->zip_code, $model->receiverAddressData->country_id, $model->receiverAddressData->city);
//
//        $currencyId = CourierTypeReturn::getCurrencyIdForCurrentUser();

//        $currency = CourierTypeReturn::getCurrencyIdForCurrentUser();
//        $codCurrency = CourierCodAvailability::findCurrencyForCountry($model->receiverAddressData->country_id);



//        $model->courierTypeReturn->_selected_operator_id = $model->courierTypeReturn->courier_u_operator_id;

        if($errors)
        {
            $this->render('create/step1', [

//                'currency' => $currency,
                'model' => $model,
//                'codCurrency' => $codCurrency,
//                'client_cod' => $client_cod,
            ]);
            return 'break';
        }
    }

    protected function create_step_summary()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');

        switch($lastStep)
        {
            case 1 :
                if($this->create_validate_step_1() == 'break')
                    return;
                break;
        }

        $model = new Courier_CourierTypeReturn;
        $model->attributes = $this->getPageState('step1_model',array());
        $model->courierTypeReturn = new CourierTypeReturn('step_1');
        $model->courierTypeReturn->attributes = $this->getPageState('step1_CourierTypeReturn',array());
        $model->senderAddressData = new CourierTypeReturn_AddressData();
        $model->receiverAddressData = new CourierTypeReturn_AddressData();


//        $additions = $this->getPageState('courier_u_additions',array());

//        $model->courierTypeReturn->addAdditions($additions);

        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData',array());

        $model->courierTypeReturn->courier = $model;

        $model->user_id = Yii::app()->user->id;

        $this->setPageState('last_step', 'summary'); // save information about last step

        $this->render('create/summary',array(
            'model'=>$model,
        ));
    }
//
    protected function create_validate_summary()
    {
        $error = false;

        $model = new Courier_CourierTypeReturn;
        $model->attributes = $this->getPageState('step1_model',array());
        $model->courierTypeReturn = new CourierTypeReturn('summary');
        $model->courierTypeReturn->attributes = $this->getPageState('step1_CourierTypeReturn',array());
        $model->senderAddressData = new CourierTypeReturn_AddressData();
        $model->receiverAddressData = new CourierTypeReturn_AddressData();

//        $additions = $this->getPageState('courier_u_additions',array());

//        $model->courierTypeReturn->addAdditions($additions);


        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData',array());

        $model->user_id = Yii::app()->user->id;

        $model->courierTypeReturn->regulations = $_POST['CourierTypeReturn']['regulations'];
        $model->courierTypeReturn->regulations_rodo = $_POST['CourierTypeReturn']['regulations_rodo'];
        $model->courierTypeReturn->validate(array('regulations', 'regulations_rodo'));

        $model->courierTypeReturn->courier = $model;


        if($model->hasErrors() OR $model->courierTypeReturn->hasErrors() OR $error)
        {

            $this->render('create/summary',array(
                'model'=>$model,
            ));
            return 'break';
        }

    }

    protected function create_finish($goBack = true)
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');


        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 'summary')
        {

            if($this->create_validate_summary() == 'break')
                return;

            $this->setPageState('last_step',3); // save information about last step

            $model = new Courier_CourierTypeReturn;
            $model->attributes = $this->getPageState('step1_model',array());
            $model->courierTypeReturn = new CourierTypeReturn('step_1');
            $model->courierTypeReturn->attributes = $this->getPageState('step1_CourierTypeReturn',array());
            $model->courierTypeReturn->courier = $model;
            $model->senderAddressData = new CourierTypeReturn_AddressData();
            $model->receiverAddressData = new CourierTypeReturn_AddressData();


            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());
            $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData',array());

            $model->courierTypeReturn->courier = $model;

            $model->user_id = Yii::app()->user->id;

            try
            {
                $return = $model->courierTypeReturn->createPackageFromScratch();
            }
            catch(Exception $ex)
            {
                Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
                throw new CHttpException(403, 'Wystapił nieznany błąd!');
            }

            if($return)
            {
                // order labels for all of found
                /* @var $item Courier_CourierTypeReturn */

                CourierLabelNew::asyncCallForId($model->courierTypeReturn->courier_label_new_id);

                // to get proper Hash
                $temp = Courier::model()->findByPk($model->id);

                if($goBack)
                    Yii::app()->user->setFlash('success', Yii::t('order','Zamówienie {no} zostało sfinalizowane! {list}', ['{no}' => CHtml::link('#'.$model->local_id, Yii::app()->createUrl('/courier/courier/view', ['urlData' => $model->hash]) ,['target' => '_blank', 'class' => 'btn btn-sm']), '{list}' => CHtml::link(Yii::t('order', 'Lista paczek'), Yii::app()->createUrl('/courier/courier/list', ['#' => 'list']) ,['target' => '_blank', 'class' => 'btn btn-sm'])]));

//                    Yii::app()->user->setFlash('previousModel', Yii::t('courier', 'Paczka została utworzona!{br}{br}{a}szczegóły paczki{/a}', array('{br}' => '<br/>', '{a}' => '<a href="' . Yii::app()->createUrl("/courier/courier/view", ["urlData" => $temp->hash]) . '" class="btn">', '{/a}' => '</a>')));


                // get id's of just created packages
//                $_justCreated = [];
//                foreach($model->courierTypeReturn->order->orderProduct AS $item)
//                    array_push($_justCreated, $item->type_item_id);
//
//                self::addToListOfJustCreated($_justCreated);

                if($goBack)
                    $this->redirect(array('/courier/courierReturn/create'));
                else {
                    $model->refresh(); // to get hash
                    $this->redirect(array('/courier/courier/view', 'urlData' => $temp->hash, 'new' => true));
                }


            } else
                throw new CHttpException(403, 'Wystapił nieznany błąd!');

        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }

    protected static function addToListOfJustCreated(array $packagesIds)
    {

        $justCreated = Yii::app()->session['internalJustCreated'];
        $justCreated = CMap::mergeArray($justCreated, $packagesIds);
        Yii::app()->session['internalJustCreated'] = $justCreated;
    }



}