<?php


class basicTextValidator extends CValidator
{

    protected $pattern = '/^([0-9a-zęóąśłżźćń\s~`!@#$%^&*()_\-=+{}\|\\:;"\',<.>])+$/i';
    protected $errorMessage = 'Wprowadzono niedozwolone znaki w polu ';

    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object,$attribute)
    {

        if($object->$attribute == '') return;

        $pattern = $this->pattern;

        // extract the attribute value from it's model object
        $value=$object->$attribute;
        if(!preg_match($pattern, $value))
        {
            $this->addError($object,$attribute,$this->errorMessage.' '.$object->getAttributeLabel($attribute).'!');
        }
    }

    /**
     * Returns the JavaScript needed for performing client-side validation.
     * @param CModel $object the data object being validated
     * @param string $attribute the name of the attribute to be validated.
     * @return string the client-side validation script.
     * @see CActiveForm::enableClientValidation
     */
    public function clientValidateAttribute($object,$attribute)
    {

        $pattern = $this->pattern;

        $condition="!value.match({$pattern}) && value!=''";

        return "
if(".$condition.") {
    messages.push(".CJSON::encode($this->errorMessage.' '.$object->getAttributeLabel($attribute).'!').");
}
";
    }

}