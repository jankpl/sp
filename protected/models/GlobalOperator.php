<?php

abstract class GlobalOperator
{
    /**
     * @param CourierTypeInternal $courierInternal
     * @param AddressData $from
     * @param AddressData $to
     * @param $uniq_id
     * @param CourierLabelNew $courierLabelNew
     * @param $blockPickup
     * @param $returnErrors
     * @return GlobalOperatorResponse[]
     */
    abstract public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors);

    /**
     * @param CourierTypeU $courierU
     * @param CourierLabelNew $courierLabelNew
     * @param $returnErrors
     * @return GlobalOperatorResponse[]
     */
    abstract public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $courierLabelNew, $returnErrors);

    /**
     * @param $no
     * @param CourierLabelNew $courierLabelNew
     * @return GlobalOperatorTtResponseCollection
     */
    abstract public static function track($no, CourierLabelNew $courierLabelNew);

    /**
     * @param Courier $courier
     * @return []
     */
    abstract public static function getAdditions(Courier $courier);

    /**
     * @param Courier $courier
     * @return CourierAdditionList|CourierUAdditionList
     * @throws Exception
     */
    protected static function getBaseAdditionsListModel(Courier $courier)
    {
        if($courier->getType() == Courier::TYPE_INTERNAL)
            return new CourierAdditionList();
        else if($courier->getType() == Courier::TYPE_U)
            return new CourierUAdditionList();
        else
            throw new Exception('Unknown Base Additions List model!');
    }

    /**
     * @param Courier $courier
     * @return string
     * @throws Exception
     */
    protected static function getCacheSuffix(Courier $courier)
    {
        if($courier->getType() == Courier::TYPE_INTERNAL)
            return 'I';
        else if($courier->getType() == Courier::TYPE_U)
            return 'U';
        else
            throw new Exception('Unknown Base Additions List model!');
    }
}