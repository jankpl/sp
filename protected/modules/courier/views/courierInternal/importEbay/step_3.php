<?php
$bringSenderNameForward = false;

// SPECIAL LAYOUT RULE FOR USER "KWAZAR" && "KWAZAR_DPD" // 29.09.2017
if(in_array(Yii::app()->user->id, [68,937]))
    $bringSenderNameForward = true;


// LAYOUT CONFIGURATION

$colWidth[0] = 20;
$colWidth[1] = 120;
$colWidth[2] = 200;
$colWidth[3] = 90;
$colWidth[4] = 100;
$colWidth[5] = 100;
$colWidth[6] = 120;
$colWidth[7] = 100;
$colWidth[8] = 100;
$colWidth[9] = 100;
$colWidth[10] = 100;
$colWidth[10] = 100;
$colWidth[11] = 120;
$colWidth[12] = 120;
$colWidth[13] = 120;
$colWidth[14] = 240;
$colWidth[15] = 240;
$colWidth[16] = 240;
$colWidth[17] = 240;
$colWidth[18] = 240;
$colWidth[19] = 240;
$colWidth[20] = 240;
$colWidth[21] = 240;
$colWidth[22] = 240;
$colWidth[23] = 240;
$colWidth[24] = 240;
$colWidth[25] = 240;
$colWidth[26] = 240;
$colWidth[27] = 240;
$colWidth[28] = 240;
$colWidth[29] = 240;
$colWidth[30] = 240;
$colWidth[31] = 240;
$colWidth[32] = 300;
$colWidth[33] = 300;

if($bringSenderNameForward)
{
    for($k = 26; $k > 7; $k--)
        $colWidth[$k] = $colWidth[$k - 2];

    $colWidth[6] = 240;
    $colWidth[7] = 240;
}


$totalWidth = 0;
foreach($colWidth As $item)
    $totalWidth += $item;

$totalWidth += 0;

$height = S_Useful::sizeof($models) * 31 + 60;
if($height > 850)
    $height = 850
?>


<?php $modelTemp = new Courier_CourierTypeInternal;?>
<?php $modelTemp2 = new AddressData();?>
<?php $modelTemp3 = new CourierTypeInternal();?>

<?php AddressDataSuggester::registerAssets(); ?>
<?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>

<?php
$this->renderPartial('importEbay/_header',[
    'ebayClient' => false,
    'step' => 3,
]);
?>


<?php
// WE DO NOT NEED FORM HERE - ALL DATA IS IN CACHE
//$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
//    array(
//        'enableAjaxValidation' => false,
//    )
//);
?>

<div class="text-center">
    <a href="<?php echo Yii::app()->createUrl('/courier/courierInternal/ebay', ['al' => true]);?>" class="btn btn-warning"><?php echo Yii::t('courier', 'Od nowa');?></a>
    <br/>
</div>
<hr/>
<div class="text-center">
    <a href="#bottom">[<?php echo Yii::t('courier', 'skocz na dół');?>]</a>
</div>
<br/>
<?php
$this->widget('FlashPrinter');
?>


<div style="position: relative;">
    <div class="overlay"><div class="loader"></div></div>
    <?php
    // for upper scroll:
    ?>
    <div style="width: 100%; position: relative; height: 20px; overflow-x: scroll;" class="dummy-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; height: 20px;">
        </div>
    </div>
    <?php
    // end for upper scroll
    ?>
    <div style="width: 100%; position: relative; height: 60px; overflow: hidden;" class="header-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; overflow: hidden;">
            <table class="list hTop" style="font-size: 10px; text-align: left !important; table-layout: fixed; height: 60px;">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <tr>
                    <?php
                    if(!$bringSenderNameForward):
                        ?>
                        <td colspan="15"><?php echo Yii::t('courier','Packages');?></td>
                        <td colspan="9"><?php echo Yii::t('courier','Sender');?></td>
                        <td colspan="9"><?php echo Yii::t('courier','Receiver');?></td>
                    <?php
                    else:
                        ?>
                        <td colspan="7"><?php echo Yii::t('courier','Packages');?></td>
                        <td colspan="2"><?php echo Yii::t('courier','Receiver');?></td>
                        <td colspan="8"><?php echo Yii::t('courier','Packages');?></td>
                        <td colspan="9"><?php echo Yii::t('courier','Sender');?></td>
                        <td colspan="7"><?php echo Yii::t('courier','Receiver');?></td>
                    <?php
                    endif;
                    ?>
                    <td colspan="2"><?php echo Yii::t('courier','Additions');?></td>
                </tr>
                <tr>
                    <td>#</td>
                    <td><?php echo Yii::t('courier', 'Is valid');?></td>
                    <td><?php echo Yii::t('courier_ebay', 'eBay order ID');?></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: center;">
                        <?php
                        if(!Yii::app()->user->model->getHidePrices()):
                            ?>
                            <?= Yii::t('courier', 'Price'); ?> (*)
                        <?php
                        endif;
                        ?>
                    </td>
                    <?php
                    if($bringSenderNameForward):
                        ?>
                        <td style="border-left: 2px dashed gray;">
                            <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                        </td>
                        <td>
                            <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                        </td>
                    <?php
                    endif;
                    ?>
                    <td <?= $bringSenderNameForward ? 'style="border-left: 2px dashed gray;"' : '';?> title="<?php echo $modelTemp3->getAttributeLabel('with_pickup'); ?>">
                        <?php echo mb_substr($modelTemp3->getAttributeLabel('with_pickup'),0,50); ?>
                    </td>
                    <td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_weight')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_l')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_w')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_d')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_value')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('value_currency')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_content')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp3->getAttributeLabel('_package_wrapping')); ?>
                    </td><td style="border-left: 2px dashed gray;">
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td>
                    <?php
                    if(!$bringSenderNameForward):
                        ?>
                        <td style="border-left: 2px dashed gray;">
                            <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                        </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td>
                    <?php
                    endif;
                    ?>
                    <td <?= $bringSenderNameForward ? 'style="border-left: 2px dashed gray;"' : '';?>>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td>
                    <td style="border-left: 2px dashed gray;">
                        <?= Yii::t('courier', 'Valid - accepted');?>
                    </td>
                    <td>
                        <?= Yii::t('courier', 'Invalid - ignored');?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="width: 100%; position: relative; height: <?= $height; ?>px; overflow-x: scroll;" class="real-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute;">

            <table class="list list-special" style="font-size: 10px; text-align: left !important; table-layout: fixed;" id="data-table">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <?php
                $errors = false;
                $totalPrice = 0;
                $totalPackages = 0;
                foreach($models AS $key => $item):
                    $this->renderPartial('importEbay/_item',
                        array(
                            'model' => $item,
                            'i' => $key,
                            'ebay' => true,
                            'bringSenderNameForward' => $bringSenderNameForward,
                        ));

                    $totalPrice += $item->courierTypeInternal->_price_total;
                    $totalPackages += $item->packages_number;
                    if($item->hasErrors() OR $item->courierTypeInternal->hasErrors() OR $item->senderAddressData->hasErrors() OR $item->receiverAddressData->hasErrors())
                        $errors = true;
                endforeach;
                ?>
            </table>

        </div>
    </div>
    <?php
    if($errors):
        ?>
        <br/>
        <br/>
        <a href="#" id="errors-up" class="btn"><?= Yii::t('courier', 'Błędne wiersze na górę listy');?></a>
        <a href="<?= Yii::app()->createUrl('/courier/courierInternal/exportErroredImportData', ['hash' => $hash, 'e' => true]);?>" class="btn btn-primary"><?= Yii::t('courier', 'Eksport błędnych wierszy do pliku .xls');?></a>
        <a href="<?= Yii::app()->createUrl('/courier/courierInternal/deleteErroredImportData', ['hash' => $hash, 'e' => true]);?>" class="btn btn-warning"><?= Yii::t('courier', 'Usuń błędne wiersze');?></a>
    <?php
    endif;
    ?>
    <br/>
    <br/>
    <div class="alert alert-info">
        <?php
        if(!Yii::app()->user->model->getHidePrices()):
            ?>
            <p><?= Yii::t('courier', 'Łączna cena');?> (*): <strong><?= $totalPrice;?> <?= Yii::app()->PriceManager->getCurrencyCode();?></strong></p>
        <?php
        endif;
        ?>
        <p><?= Yii::t('courier', 'Łączna liczba paczek');?>: <strong><?= $totalPackages;?></strong></p>
        <?php
        if(!Yii::app()->user->model->getHidePrices()):
            ?>
            <p class="text-right" style=""><small>* <?= Yii::t('courier', 'Cena jest szacunkowa i nie uwzględnia indywidualnych rabatów użytkownika');?></small></p>
        <?php
        endif;
        ?>
    </div>

    <div class="alert alert-warning">
        <?= Yii::t('courier', 'Przejście dalej spowoduje zamówienie paczek i naliczenie opłat.'); ?>
    </div>

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
        array(
            'enableAjaxValidation' => false,
            'id' => 'import-form',
        )
    );
    ?>

    <div class="navigate" style="text-align: center;" id="bottom">
        <?php echo TbHtml::submitButton(Yii::t('courier', 'Refresh'), array('name' => 'refresh', 'class' => 'save-packages btn-xs')); ?>
        <br/>
        <br/>
        <?php echo TbHtml::submitButton(Yii::t('courier', 'Save this part of data'), array('name' => 'save', 'class' => 'save-packages btn-primary')); ?>
    </div>

    <?php
    $this->endWidget();
    ?>
</div>

<div class="modal fade " tabindex="-1" role="dialog" id="edit-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="edit-modal-content">
        </div>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  removeRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierInternal/ajaxRemoveRowOnImport', ['e' => true])).',
                  editRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierInternal/ajaxEditRowOnImport', ['e' => true])).',
                  updateRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierInternal/ajaxUpdateRowItemOnImport', ['e' => true])).',
              },
               text: {
                  unknownError: '.CJSON::encode(Yii::t('site', 'Wystąpił nieznany błąd!')).',
                     beforeLeaving: '.CJSON::encode(Yii::t('site', 'Jesteś na pewno chcesz opuścić tę stronę? Wprowadzone dane mogą zostać utracone')).',
              },
              misc: {
                  hash: '.CJSON::encode($hash).',
              },
          };
      ',CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/importListModels.js', CClientScript::POS_HEAD);
?>
<script>
    $(document).ready(function(){
        var hash = location.hash.substr(1);
        if(hash == 'errorsUp')
            errorsUp();
    });

    $('#errors-up').on('click', function(e){

        e.preventDefault();
        window.location.hash = 'errorsUp';
        errorsUp();
    });

    function errorsUp()
    {
        $('tr.row-errored').each(function(i,v){
            $(v).detach().prependTo('#data-table tbody');
        });
    }
</script>
