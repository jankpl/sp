<?php

class PriceManager extends CComponent{

    protected $_currency;
    protected $_no_vat;
    protected $_on_invoice;

    protected $_tax_rate = 0.23;

    public function init()
    {
        if(!Yii::app()->user->isGuest)
        {
            /* @var $user User */
            $user = User::model()->cache(60*60)->findByPk(Yii::app()->user->id);
            $this->_currency = $user->price_currency_id;
            $this->_on_invoice = $user->payment_on_invoice;
            $this->_no_vat = $user->without_vat;

            if($user->getCustomVatRate())
                $this->_tax_rate = $user->getCustomVatRate() / 100;

        } else {
            $this->_currency = User::CURRENCY_PLN;
            $this->_on_invoice = 0;
            $this->_no_vat = 0;
        }
    }

    public function getCurrency()
    {
        return $this->_currency;
    }

    public function getCurrencySymbol()
    {
        /* @var $model PriceCurrency */
        $model = PriceCurrency::model()->findByPk($this->_currency);
//        return $model->symbol;
        return $model->short_name;
    }

    public function getCurrencyCode()
    {
        /* @var $model PriceCurrency */
        $model = PriceCurrency::model()->findByPk($this->_currency);
        return $model->short_name;
    }

    public function getCurrencyIdByCode($code)
    {
        /* @var $model PriceCurrency */
        $model = PriceCurrency::model()->findByAttributes(array('short_name' => $code));
        return $model->id;
    }

    public function getNoVat()
    {
        return $this->_no_vat;
    }

    public function getOnInvoice()
    {
        return $this->_on_invoice;
    }

    public function setCurrency($id)
    {
        $this->_currency = $id;
    }

    public function setCurrencyByCode($code)
    {
        $currency = PriceCurrency::model()->findByAttributes(array('short_name' => $code));

        $this->_currency = $currency->id;

    }

    public function getTax($price)
    {
        if(!$this->_no_vat)
        {
            $tax = $price * $this->_tax_rate;
            $tax = round($tax,2);
        } else {
            $tax = 0;
        }

        return $tax;
    }

    public function getFinalPrice($price, $withoutTax = false)
    {
        if($withoutTax)
            return $price;

        if(!$this->_no_vat)
        {
            $finalPrice = $price + $this->getTax($price);
        } else
            $finalPrice = $price;

        return  $finalPrice;
    }

    public function getTaxRate()
    {
        return $this->_tax_rate;
    }


}