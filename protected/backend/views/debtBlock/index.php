<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'debt-block',
        'enableAjaxValidation' => false,
    ));
    ?>

    <h3>Set debt block message</h3>


    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):



            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';


        endforeach;
        ?>
    </div>


    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

                $this->widget('ext.editMe.widgets.ExtEditMe', array(
                    'name'=>'DebtBlockMessage[text]['.$languageItem->code.']',
                    'value'=> Config::getData(Config::DEBT_BLOCK_CONTENT.'['.$languageItem->code.']'),
                    'resizeMode' => 'vertical',
                    'width' => '580',
                    'baseHref' => Yii::app()->baseUrl.'/',
                    'toolbar' => array(
                        array(
                            'NewPage', 'Preview', 'Source',
                        ),
                        array(
                            'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',
                        ),
                        array(
                            'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat',
                        ),
                        array(
                            'Image','Link','NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Table', 'HorizontalRule','SpecialChar', 'Font',  'FontSize', 'TextColor',
                        ),
                    ),
                    'advancedTabs' => false,
                    'ckeConfig' => array(
                        'forcePasteAsPlainText' => true,
                        'allowedContent' => true,
                    )
                ));

            echo'</div>';

        endforeach;
        ?>
    </div>

    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>


    <table class="table">
        <tr>

        </tr>
        <tr>
            <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDebtBlockMessage'));?>
            </td>
        </tr>
    </table>

    <?php
    $this->endWidget();
    ?>

</div>