<?php

class InternationalMailController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'actions'=> array('index', 'fastAssignCarrier', 'assignCarrierFromFile'),
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionAssingStatusByGridView()
    {
        /* @var $model InternationalMail */
        $i = 0;
        if (isset($_POST['InternationalMail'])) {

            $international_mail_stat_id = (int) $_POST['international_mail_stat_id'];
            $international_mail_ids =  $_POST['InternationalMail']['ids'];

            $international_mail_ids = explode(',', $international_mail_ids);

            if(is_array($international_mail_ids) AND S_Useful::sizeof($international_mail_ids))
            {
                $transaction = Yii::app()->db->beginTransaction();
                $errors = false;


                foreach($international_mail_ids AS $id)
                {
                    $model = InternationalMail::model()->findByPk($id);
                    if($model === null) continue;
                    if(!$model->changeStat($international_mail_stat_id))
                        $errors = true;

                    $i++;
                }

                if(!$errors AND $i > 0)
                {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success','Udało się zmienić statusy. Liczba pozycji: '.$i);
                }
                else
                {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error','Nie udało się zmienić statusu');
                }
            }


        }

        if(!$i) Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');



        $this->redirect(Yii::app()->createUrl('/internationalMail/internationalMail/Admin').'#mass-assign-stat');

    }


    public function actionChangeStat()
    {
        /* @var $model InternationalMail */
        if (isset($_POST['InternationalMail'])) {

            $model = InternationalMail::model()->findByPk($_POST['InternationalMail']['id']);

            if($model === null)
                throw new Exception('Błąd!');

            if($model->changeStat($_POST['InternationalMail']['international_mail_stat_id']))
                $this->redirect(array('view', 'id' => $model->id));
            else
                throw new Exception("Błąd!");
        } else
            throw new CHttpException('404');

    }

    public function actionAssingCarrier()
    {
        /* @var $model InternationalMail */


        if (isset($_POST['InternationalMail'])) {

            $model = InternationalMail::model()->findByPk($_POST['InternationalMail']['id']);

            if($model === null)
                throw new Exception('Błąd!');

            if($model->assignCarrier($_POST['InternationalMail']['international_mail_carrier_id'],$_POST['InternationalMail']['outside_id'], $_POST['Other']['actualizeStatusWithAssignCourrier']))
                $this->redirect(array('view',
                    'id' => $model->id,
                ));
            else
                throw new Exception("Błąd!");
        } else
            throw new CHttpException('404');

    }


    public function actionFastAssignCarrier()
    {

        $model = new InternationalMail();

        if (isset($_POST['InternationalMail'])) {

            $model = InternationalMail::model()->find('local_id=:local_id', array(':local_id' => $_POST['InternationalMail']['local_id']));

            if ($model !== null AND $model->assignCarrier('', $_POST['InternationalMail']['outside_id'], true))
            {
                Yii::app()->user->setFlash('success','Zapisano!');
            }
            else
            {
                Yii::app()->user->setFlash('error','Zapis nie powiódł się!');
            }
            $this->redirect(array('/internationalMail/internationalMail/fastAssignCarrier'));
        }

        $this->render('fastAssingCarrier', array(
            'model' => $model,
        ));

    }


    public function actionAssignCarrierFromFile()
    {

        $S_InternationalMail_Ids_From_File = Array();
        $fileModel = new F_UploadFile();

        if(isset($_POST['F_UploadFile']))
        {
            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if(!$fileModel->validate())
            {
                $this->render('assignCarrierFromFile', array(
                    'models' => $S_InternationalMail_Ids_From_File,
                    'fileModel' => $fileModel,
                ));
                return;
            }

        }

        if($fileModel->file !== null)
        {
            $attributes = Array(
                'local_id' => 0,
                'remote_id' => 1,
            );

            $internationalMailIdsAttributes = $fileModel->returnCSVAsArray($attributes);


            if(S_Useful::sizeof($internationalMailIdsAttributes))
            {
                foreach($internationalMailIdsAttributes AS $item)
                {
                    $S_InternationalMail_Id_From_File = new S_InternationalMail_Id_From_File();
                    $S_InternationalMail_Id_From_File->local_id = $item['local_id'];
                    $S_InternationalMail_Id_From_File->remote_id = $item['remote_id'];
                    $S_InternationalMail_Id_From_File->validate();

                    array_push( $S_InternationalMail_Ids_From_File,  $S_InternationalMail_Id_From_File);
                }
            }
            else
                Yii::app()->user->setFlash('error','Nie znaleziono treści w pliku!');


        }


        if(isset($_POST['S_InternationalMail_Id_From_File']))
        {

            $S_InternationalMail_Ids_From_File = Array();

            $error = false;

            $i = 0;
            $transaction = Yii::app()->db->beginTransaction();
            foreach($_POST['S_InternationalMail_Id_From_File'] AS $key => $item)
            {
                $S_InternationalMail_Ids_From_File[$key] = new S_InternationalMail_Id_From_File();
                $S_InternationalMail_Ids_From_File[$key]->attributes = $item;


                $S_InternationalMail_Ids_From_File[$key]->validate();
                if(!$S_InternationalMail_Ids_From_File[$key]->save())
                    $error = true;
                else $i++;
            }

            if(!$error)
            {
                Yii::app()->user->setFlash('success','Zaktualizowano ID dla przesyłek: '.$i);

                $transaction->commit();
                $this->redirect(array('/internationalMail/internationalMail/admin'));
                return;
            }

        }


        $this->render('assignCarrierFromFile', array(
            'models' => $S_InternationalMail_Ids_From_File,
            'fileModel' => $fileModel,
        ));


    }


    public function actionCarriageTicket($id)
    {

        $model = InternationalMail::model()->findByPk($id);

        if($model === null) throw new CHttpException('404');

        $model->generateCarriageTicket();

    }

    public function actionView($id) {

        $model = InternationalMail::model()->findByPk($id);

        if($model === null)
            throw new CHttpException('404');

        $this->render('view', array(
            'model' => $model,
        ));
    }


    public function actionIndex() {

        $this->render('index', array(

        ));


        //$this->redirect(array('internationalMail/admin'));
    }

    public function actionAdmin() {

        $model = new InternationalMail('search');
        $model->unsetAttributes();

        if (isset($_GET['InternationalMail']))
            $model->setAttributes($_GET['InternationalMail']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}