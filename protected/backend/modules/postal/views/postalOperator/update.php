<?php
/* @var $this PostalOperatorController */
/* @var $model PostalOperator */

$this->breadcrumbs=array(
	'Postal Operators'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PostalOperator', 'url'=>array('index')),
	array('label'=>'Create PostalOperator', 'url'=>array('create')),
	array('label'=>'View PostalOperator', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PostalOperator', 'url'=>array('admin')),
);
?>

<h1>Update PostalOperator <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>