<?php

class S_Status
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    public static function stat($val)
    {
        if($val == 1) return 'active';
        else return 'inactive';
    }

    public static function listStats()
    {
        $list = Array();
        $list[0] = array('id' => 0, 'name' => 'inactive');
        $list[1] = array('id' => 1, 'name' => 'active');

        return $list;
    }
}