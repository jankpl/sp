<?php

Yii::import('application.modules.courier.models._base.BaseCourierTypeInternal');

class CourierTypeInternal extends BaseCourierTypeInternal
{

    const DISABLE_ROUTING_CACHE = GLOBAL_CONFIG::COURIER_INTERNAL_DISABLE_ROUTING_CACHE;

    const WITH_PICKUP_NONE = 0;
    const WITH_PICKUP_REGULAR = 1;
    const WITH_PICKUP_CONTRACT = 2;
    const WITH_PICKUP_RUCH = 3;
    const WITH_PICKUP_RM = 4;
    const WITH_PICKUP_LP_EXPRESS = 5;
    const WITH_PICKUP_UPS = 6;
    const WITH_PICKUP_INPOST = 7;
    const WITH_PICKUP_DHLDE = 8;

    const _WITH_PICKUP_RUCH_RECEIVER_POINT_ID = '0130'; // Warsaw Point
    const _WITH_PICKUP_INPOST_PACZKOMAT_ID = 'NYS231'; // Nysa, Grodkowska


    const SCENARIO_STEP1 = 'step1';
    const SCENARIO_IMPORT = 'import';
    const SCENARIO_API = 'api';

    public $_package_wrapping;

    public $_imported_additions = [];
    public $_imported_additions_ok = [];
    public $_imported_additions_fail = [];


    const MAX_PACKAGE_WEIGHT = 70; // KG


    const NUMBER_OF_MULTIPLE_PACKAGES_ONCE = 25;

    const GET_LABEL_MODE_BOTH = 1;
    const GET_LABEL_MODE_FIRST = 2;
    const GET_LABEL_MODE_LAST = 3;

    public $_courier_additions;
    public $regulations;
    public $regulations_rodo;


// for label generating
    const OPERATOR_OWN = 1;
    const OPERATOR_EXT = 2;

    const SOURCE_EBAY = 1;
    const SOURCE_LINKER = 2;


// for preffered pickup time for UPS
    public $_preffered_pickup_day;
    public $_preffered_pickup_time;


    public $_spPoint;

    public $_ebay_order_id;
    public $_ebay_export_status;
    public $__temp_ebay_update_stat;

    public $_linker_order_id;


// used in import
    public $_price_total;
    public $_price_currency;


// used for RUCH
    public $_ruch_receiver_point;
    public $_ruch_receiver_point_address;

    // used for LP_EXPRESS
    public $_lp_express_receiver_point;
    public $_lp_express_receiver_point_address;

    // used for INPOST
    public $_inpost_locker_active = false;
    public $_inpost_locker_delivery_id;
    public $_inpost_locker_delivery_address;
    public $_inpost_locker_pickup_id;
    public $_inpost_locker_pickup_address;

    public $pickup_point_id;
    public $pickup_point_text;
    public $pickup_point_img;
    public $pickup_point_add_to_fav;

    public $_fav_sp_point;

    public $_pickup_point_country;

    public function getAttributes($names = true)
    {
        $array = parent::getAttributes($names);

        if(!isset($array['_package_wrapping']))
            $array['_package_wrapping'] = null;

        return $array;
    }

    /**
     * @param bool|true $forCurrentUser If true, returned will be only active options for current user
     * @param bool|true $onImport If true, returned will be only options available in inport
     * @return array
     */
    public static function getWithPickupList($forCurrentUser = true, $onImport = false)
    {
        $data = [
            self::WITH_PICKUP_NONE => Yii::t('courier', 'bez pickup'),
            self::WITH_PICKUP_REGULAR => Yii::t('courier', 'pickup'),
            self::WITH_PICKUP_CONTRACT => Yii::t('courier', 'z umową'),
            self::WITH_PICKUP_RUCH => Yii::t('courier', 'Paczka w Ruchu'),
            self::WITH_PICKUP_RM => Yii::t('courier', 'RoyalMail'),
//            self::WITH_PICKUP_LP_EXPRESS => Yii::t('courier', 'LP Express terminal'),
//            self::WITH_PICKUP_UPS => Yii::t('courier', 'UPS Access Point'),
            self::WITH_PICKUP_INPOST => Yii::t('courier', 'Inpost - nadanie w paczkomacie'),
            self::WITH_PICKUP_DHLDE => Yii::t('courier', 'DHL DE - nadanie w punkcie'),
        ];

        if($onImport) {
            unset($data[self::WITH_PICKUP_RUCH]);
            unset($data[self::WITH_PICKUP_RM]);
            unset($data[self::WITH_PICKUP_LP_EXPRESS]);
            unset($data[self::WITH_PICKUP_UPS]);
            unset($data[self::WITH_PICKUP_INPOST]);
            unset($data[self::WITH_PICKUP_DHLDE]);
        }

        if(!$forCurrentUser)
            return $data;

        if(!Yii::app()->user->isGuest && Yii::app()->params['frontend'])
        {
            if(!Yii::app()->user->model->getInternalPickupTypeCustomSettings()) {

                unset($data[self::WITH_PICKUP_CONTRACT]);
                return $data;
            }
            else
            {
                return array_intersect_key($data, array_flip(Yii::app()->user->model->getInternalPickupTypeCustomSettings()));
            }
        }

        return $data;
    }

    public static function getWithPickupMapTypes($forCurrentUser = true, $withoutNoPickup = false)
    {
        $all = self::getWithPickupList($forCurrentUser);

        unset($all[self::WITH_PICKUP_CONTRACT]);
        unset($all[self::WITH_PICKUP_REGULAR]);

        if($withoutNoPickup)
            unset($all[self::WITH_PICKUP_NONE]);

        return $all;
    }

    public static function hasPickupTypeUser($type)
    {
        return isset(self::getWithPickupList(true)[$type]);
    }

    public static function getPickupTypeName($id)
    {
        return isset(self::getWithPickupList(false)[$id]) ? self::getWithPickupList(false)[$id] : NULL;
    }

    public function getPackageWrappingName()
    {
        return isset(User::getWrappingList()[$this->_package_wrapping]) ? User::getWrappingList()[$this->_package_wrapping] : '';
    }

    public function init()
    {
        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest)
        {
            $data = User::getDefaultPackageDimensionsForUser(Yii::app()->user->id);
            $this->_package_wrapping = $data->package_wrapping;
        }

        parent::init();
    }

    /**
     * Returns array with id's of description saved in Page table
     * @return array
     */
    public static function getPageIdWithDescForPickupTypeList()
    {
        return [
            self::WITH_PICKUP_NONE => 30,
            self::WITH_PICKUP_REGULAR => 31,
            self::WITH_PICKUP_CONTRACT => 32,
            self::WITH_PICKUP_RUCH => 33,
            self::WITH_PICKUP_RM => 37,
            self::WITH_PICKUP_LP_EXPRESS => 38,
            self::WITH_PICKUP_UPS => 39,
            self::WITH_PICKUP_INPOST => 40,
            self::WITH_PICKUP_DHLDE => 61,
        ];
    }

    /**
     * Returns id of description saved in Page table for provided pickup type id
     * @param $id
     * @return bool
     */
    public static function getPageIdWithDescForPickupType($id)
    {
        return isset(self::getPageIdWithDescForPickupTypeList()[$id]) ? self::getPageIdWithDescForPickupTypeList()[$id] : false;
    }

    public function getWithPickupName()
    {
        return isset(self::getWithPickupList(false)[$this->with_pickup]) ? self::getWithPickupList(false)[$this->with_pickup] : NULL;
    }


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function afterValidate()
    {
        if($this->_package_wrapping != User::WRAPPING_DEFAULT && $this->_package_wrapping != '')
        {
            if(!is_array($this->_courier_additions) OR (!in_array(CourierAdditionList::PACKAGE_NON_STANDARD_DOMESTIC, $this->_courier_additions) && !in_array(CourierAdditionList::PACKAGE_NON_STANDARD_INT, $this->_courier_additions)))
            {
                $this->addError('_package_wrapping', Yii::t('courier', 'Niestandardowe opakowanie wymaga aktywnej opcji dodatkowej "Przesyłka niestandardowa"'));
            }
        }

        if($this->pickup_point_add_to_fav && $this->pickup_point_id && $this->pickup_point_text)
            CourierSpPointFav::addPoint($this->pickup_point_id, $this->with_pickup, $this->pickup_point_text);


        parent::afterValidate();
    }

    public function processPickupParams()
    {
        if(in_array($this->with_pickup, [self::WITH_PICKUP_REGULAR, self::WITH_PICKUP_CONTRACT]))
        {
            $this->_inpost_locker_pickup_id = false;
            $this->pickup_point_id = false;
            $this->pickup_point_text = false;
            $this->pickup_point_img = false;
        }

        if($this->_fav_sp_point)
        {

            $temp = explode('|', $this->_fav_sp_point);

            $this->with_pickup = intval($temp[0]) - 0;
            $this->pickup_point_id = $temp[1];
            $this->pickup_point_text = CourierSpPointFav::getNameForIds($this->with_pickup, $this->pickup_point_id);
        }
        else if($this->_spPoint)
        {
            $this->with_pickup = self::WITH_PICKUP_NONE;
            $this->pickup_point_id = $this->_spPoint;
            $this->pickup_point_text = CourierSpPointFav::getNameForIds($this->with_pickup, $this->pickup_point_id);
        }
    }

    public function beforeValidate()
    {
        if($this->_pickup_point_country)
            $this->_pickup_point_country = trim($this->_pickup_point_country);

        // DHLDE:
        if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_DHLDE && $this->_pickup_point_country != '')
        {
            if(CountryList::countryCodeToId($this->_pickup_point_country) != $this->courier->senderAddressData->country_id)
                $this->addError('with_pickup', Yii::t('courier', 'Punkt nadania musi znajdować się w tym samym kraju co nadawca.'));

        }

//        if($this->with_pickup == self::WITH_PICKUP_INPOST && !$this->_inpost_locker_active)
//            $this->addError('with_pickup', Yii::t('courier', 'Nadanie w paczkomacie Inpost jest możliwe tylko do innego paczkomatu Inpost.'));

        if(!in_array($this->scenario, [self::SCENARIO_API, self::SCENARIO_IMPORT]) && $this->with_pickup == CourierTypeInternal::WITH_PICKUP_NONE)
        {
            if($this->pickup_point_id == '')
                $this->addError('pickup_point_id', Yii::t('courier', 'Wybierz punkt z listy.'));
        }

        // RUCH:
        if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH && $this->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
        {

            if($this->_ruch_receiver_point == '')
                $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybierz punkt odbioru paczki RUCH!'));

            if(!$this->hasErrors('_ruch_receiver_point')) {
                if ($this->courier->cod_value > 0) {
                    if (!PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, true)) {
                        if (PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, false)) {
                            $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybrany punkt RUCH nie obsługuje paczek COD!'));
                        } else
                            $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybranych punk RUCH nie jest poprawny!'));
                    }
                } else {
                    if (!PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, false))
                        $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybranych punk RUCH nie jest poprawny!'));
                }
            }
        }

        $this->pickup_point_text = nl2br($this->pickup_point_text);
        $this->pickup_point_text = strip_tags($this->pickup_point_text,'<br><br/></strong><strong>');
        $this->pickup_point_img = strip_tags($this->pickup_point_img);

        return parent::beforeValidate();
    }

    public function rules()
    {


        return CMap::mergeArray([
            ['_package_wrapping', 'safe'],
            ['_spPoint', 'numerical', 'integerOnly' => true],
            ['_inpost_locker_active, pickup_point_add_to_fav', 'boolean',],
            ['_ruch_receiver_point, _ruch_receiver_point_address', 'application.validators.lettersNumbersBasicValidator'],
            ['_lp_express_receiver_point, _lp_express_receiver_point_address', 'application.validators.lettersNumbersBasicValidator'],
            ['_inpost_locker_pickup_id, _inpost_locker_pickup_address, _inpost_locker_delivery_address, pickup_point_id, _inpost_locker_delivery_id', 'application.validators.lettersNumbersBasicValidator'],
            ['with_pickup', 'required',],
            ['with_pickup', 'in', 'allowEmpty' => false, 'range' => array_keys(self::getWithPickupList()), 'message' => Yii::t('courier', 'Twoje konto nie ma dostępu do tego typu nadania.')],
            ['pickup_point_text, pickup_point_img,', 'safe'],
        ],

            array(
                array('_pickup_point_country', 'length', 'max'=>2, 'min' => 2),

                array('_fav_sp_point', 'in', 'range' => CHtml::listData(CourierSpPointFav::getPointsForUser(), 'complexId', 'complexId')),
//                array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('courier', 'Musisz zaakceptować regulamin.'), 'on' => 'summary'),
                array('cod', 'numerical', 'integerOnly' => true),

                array('delivery_hub_id, delivery_hub_name, delivery_operator_id, delivery_operator_name', 'required', 'except' => [self::SCENARIO_STEP1, 'step_1', self::SCENARIO_IMPORT, self::SCENARIO_API]),
                array('pickup_hub_id, pickup_operator_id, delivery_hub_id, delivery_operator_id, order_id, _preffered_pickup_time, _preffered_pickup_day', 'numerical', 'integerOnly'=>true),

                array('client_cod_value', 'numerical', 'min' => 0, 'max' => PaymentType::MAX_COD_AMOUNT),

                array('dimensional_weight, final_weight, family_member_number, parent_courier_id, client_cod_value', 'numerical',),
                array('pickup_hub_name, pickup_operator_name, pickup_id, delivery_hub_name, delivery_operator_name, delivery_id', 'length', 'max'=>128),
                array('cod', 'default', 'setOnEmpty' => true, 'value' => 0),
//                array('with_pickup', 'default', 'setOnEmpty' => true, 'value' => CourierTypeInternal::WITH_PICKUP_NONE),
                array('family_member_number', 'default', 'setOnEmpty' => true, 'value' => 1),
                array('pickup_id, delivery_id,  pickup_hub_id, pickup_hub_name, pickup_operator_id, pickup_operator_name, pickup_operator_ordered, pickup_label, delivery_operator_ordered, delivery_label, common_operator_ordered, common_label, operator_mode, cod_value, cod_currency, client_cod_value, parent_courier_id, client_cod_value, params', 'default', 'setOnEmpty' => true, 'value' => null),
                array('id, paid, with_pickup, pickup_hub_id, pickup_hub_name, pickup_operator_id, pickup_operator_name, pickup_id, delivery_hub_id, delivery_hub_name, delivery_operator_id, delivery_operator_name, delivery_id, pickup_operator_ordered, pickup_label, delivery_operator_ordered, delivery_label, common_operator_ordered, common_label, operator_mode, cod_value, cod_currency, order_id, dimensional_weight, final_weight, family_member_number, client_cod_value, params', 'safe', 'on'=>'search'),
            ),
            RodoRegulations::regulationsRules()
        );

    }

    public function attributeLabels()
    {
        return CMap::mergeArray([
            '_spPoint' => Yii::t('site', 'Lista punktów ŚwiatPrzesyłek:'),
            '_inpost_locker_active' => Yii::t('site', 'Nadanie do Paczkomatu Inpost'),
        ],
            parent::attributeLabels(),
            RodoRegulations::attributeLabels()
        );

    }

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bAcceptRegulationsByDefault' =>
                    [
                        'class'=>'application.models.bAcceptRegulationsByDefault'
                    ],
            ]
        );
    }

    public function events()
    {
        return array_merge(parent::events(), array(
            'onAfterNewCourierTypeInternal' => 'afterNewCourierTypeInternal',
        ));
    }

    public function onAfterNearlyEndLicence($event)
    {
        $this->raiseEvent('onAfterNewCourierTypeInternal', $event);
    }

    /*
     * Get children courier packages (from multipackage)
     */
    public function getChildrenCourierInternal()
    {
        return CourierTypeInternal::model()->findAll('parent_courier_id = :courier_id', [':courier_id' => $this->courier->id]);
    }


    public static function listCountries()
    {

        /* return all countries and in case of lack of possibility to carriage, show information page */

        return CountryList::model()->with('countryListTrs')->with('countryListTr')->findAll(array('order' => 't.name ASC'));

        /*
                $cmd = Yii::app()->db->createCommand();
                $cmd->select = 'country_list.name, country_list.id';
                $cmd->distinct = true;
                $cmd->from = 'country_list';
                $cmd->join('country_group_has_country','country_group_has_country.country = country_list.id');
                //$cmd->join('courier_country_group','country_list.country_group_id = courier_country_group.id');
                //$cmd->join('courier_price','courier_price.country_group_id = country_group.id');
                $cmd->where('country_list.stat = '.S_Status::ACTIVE);
                $cmd->order('country_list.name DESC');
                $countries = CountryList::model()->findAllBySql($cmd->getText());

                */

        return $countries;
    }

    public static function listPickupCountries()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select = 'country_list.name, country_list.id';
        $cmd->distinct = true;
        $cmd->from = 'country_list';
        $cmd->join('courier_country_group_has_country','courier_country_group_has_country.country = country_list.id');
        $cmd->join('courier_country_group','courier_country_group.id = courier_country_group_has_country.courier_group');
        $cmd->join('courier_pickup_price_has_group','courier_pickup_price_has_group.courier_country_group = courier_country_group.id');
        $cmd->where('pickup_hub IS NOT NULL');
        $cmd->andWhere('pickup_operator IS NOT NULL');
        //$cmd->where('country_list.stat = '.S_Status::ACTIVE);
        $countries = CountryList::model()->with('countryListTr')->findAllBySql($cmd->getText());

        return $countries;
    }

    public function isWithPickup()
    {
        if($this->with_pickup)
            return 'yes';
        else
            return 'no';
    }

    /**
     * Returns pickup price for one package
     * @param bool|false $force_user_id If true, price will be calculated for particular user, not logged one
     * @return float
     */
    public function pickupPrice($currency, $force_user_id = false, $force_user_group_id = false)
    {
        if(!$force_user_id)
            $user_id = $this->courier->user_id;
        else
            $user_id = $force_user_id;

        if($force_user_group_id)
            $user_group = $force_user_group_id;
        else
            $user_group = User::getUserGroupId($user_id);

        $final_weight = $this->getFinalWeight(true, false);

        $CACHE_NAME = 'PICKUP_PRICE_'.$currency.'_'.$final_weight.'_'.$this->courier->senderAddressData->country_id.'_'.$this->courier->receiverAddressData->country_id.'_'.$user_group.'_'.$this->with_pickup;
        $price = Yii::app()->cache->get($CACHE_NAME);

        if(self::DISABLE_ROUTING_CACHE)
            $price = false;

        if($price === false) {
            $price = 0;
            if ($this->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR)
                $price = CourierPrice::calculatePickupPrice($currency, $final_weight, $this->courier->senderAddressData->country_id, $this->courier->receiverAddressData->country_id, $user_group);
            else if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH)
                $price = CourierCustomPickupPrice::getPrice($this->courier, CourierCustomPickupPrice::TYPE_RUCH, $user_group);
            else if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_RM)
                $price = CourierCustomPickupPrice::getPrice($this->courier, CourierCustomPickupPrice::TYPE_RM, $user_group);
            else if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_LP_EXPRESS)
                $price = CourierCustomPickupPrice::getPrice($this->courier, CourierCustomPickupPrice::TYPE_LP_EXPRESS, $user_group);
            else if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_UPS)
                $price = CourierCustomPickupPrice::getPrice($this->courier, CourierCustomPickupPrice::TYPE_UPS, $user_group);
            else if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_INPOST)
                $price = CourierCustomPickupPrice::getPrice($this->courier, CourierCustomPickupPrice::TYPE_INPOST, $user_group);
            else if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_DHLDE)
                $price = CourierCustomPickupPrice::getPrice($this->courier, CourierCustomPickupPrice::TYPE_DHLDE, $user_group);

            Yii::app()->cache->set($CACHE_NAME, $price, 60);
        }

        return $price;
    }

    /**
     * Returns delivery price for one package
     * @param bool|false $force_user_id If true, price will be calculated for particular user, not logged one
     * @return float
     */
    public function deliveryPrice($currency, $force_user_id = false, $force_user_group_id = false)
    {
        if(!$force_user_id)
            $user_id = $this->courier->user_id;
        else
            $user_id = $force_user_id;

        if($force_user_group_id)
            $user_group = $force_user_group_id;
        else
            $user_group = User::getUserGroupId($user_id);

        $weight = $this->getFinalWeight(false, true);

        $CACHE_NAME = 'DELIVERY_PRICE_'.$currency.'_'.$weight.'_'.$this->courier->senderAddressData->country_id.'_'.$this->courier->receiverAddressData->country_id.'_'.$user_group.'_'.$this->with_pickup;
        $price = Yii::app()->cache->get($CACHE_NAME);

        if(self::DISABLE_ROUTING_CACHE)
            $price = false;

        if($price === false) {

            $price = CourierPrice::calculateDeliveryPrice($this->with_pickup, $currency, $weight, $this->courier->senderAddressData->country_id, $this->courier->receiverAddressData->country_id, $user_group);

            Yii::app()->cache->set($CACHE_NAME, $price, 60);
        }

        return $price;
    }

    /**
     * Returns price of additions for one package
     *
     * @return float
     */
    public function additionsPrice()
    {

        $additions = $this-> _courier_additions;

        if(!is_array($additions))
            return 0;

        $price = 0;
        foreach($additions AS $item)
        {
            /* @var $model CourierAdditionList */
            $model = CourierAdditionList::model()->cache(60)->findByPk($item);
            if($model === null)
                continue;

            $price += $model->getPrice();
        }

        return $price;
    }

    public function addAdditions($additions)
    {

        // find if additons IDs are poroper and remove multiple additions from one group
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, group');
        $cmd->from((new CourierAdditionList())->tableName());
        $cmd->where('stat = :stat', array(':stat' => S_Status::ACTIVE));
        // checking operators is omited
        //$cmd->andWhere('courier_operator_id IS NULL or courier_operator_id = :operator', array(':operator' => $this->operator_id));
        $cmd->andWhere(array('in', 'id', $additions));
        $result = $cmd->queryAll();

        $temp = [];
        $temp2 = []; // for non grouped - not to by filtered by array_unique
        foreach($result AS $key => $item)
        {
            if($item['group'] === NULL)
                $temp2[$item['id']] = $item['group'];
            else
                $temp[$item['id']] = $item['group'];
        }

        // filter grouped additions - only one in group is allowed
        $temp = array_unique($temp);

        $additions = [];
        if(is_array($temp))
            foreach($temp AS $key => $item)
            {
                array_push($additions, $key);
            }

        if(is_array($temp2))
            foreach($temp2 AS $key => $item)
            {
                array_push($additions, $key);
            }

        $this->_courier_additions = $additions;
    }

    /**
     * @param bool $forceNotSaved Ignore that model has already ID and try to get additions by array of additions ids
     * @return array|mixed|null|static
     */
    public function listAdditions($forceNotSaved = false)
    {
        if($this->id == NULL OR $forceNotSaved)
            $additions = CourierAdditionList::model()->findAllByAttributes(array('id' => $this->_courier_additions));
        else
            $additions = CourierAddition::model()->findAllByAttributes(array('courier_id' => $this->courier->id));

        return $additions;
    }


    /**
     * Checks if package has particular addition
     * @param $id Integer Id of CourierAdditionList
     * @return boolean
     */
    public function hasAdditionById($id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from = (new CourierAddition())->tableName();
        $cmd->where('courier_id = :courier_id', [':courier_id' => $this->courier->id]);
        $cmd->andWhere('courier_addition_list_id = :addition_id', [':addition_id' => $id]);
        return $cmd->queryScalar();
    }

    /**
     * Checks if package has particular addition
     * @param $uniq_id Integer uniq_id of CourierAdditionList
     * @return boolean
     */
    public function hasAdditionByUniqId($uniq_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from = (new CourierAddition())->tableName();
        $cmd->join((new CourierAdditionList())->tableName(),'courier_addition_list.id = courier_addition.courier_addition_list_id');
        $cmd->where('courier_id = :courier_id', [':courier_id' => $this->courier->id]);
        $cmd->andWhere('uniq_id = :uniq_id', [':uniq_id' => $uniq_id]);
        return $cmd->queryScalar();
    }

    /**
     * Returns price addition for DHL Remote Area for one package
     *
     * @return float
     */
    public function dhlRemoteArea($currency)
    {

        $price = 0;

        // FOR RECEIVER:
        $receiverRemote = CourierDhlRemoteAreas::findZip($this->courier->receiverAddressData->zip_code, $this->courier->receiverAddressData->country_id, $this->courier->receiverAddressData->city);
        if($receiverRemote)
        {
            /* @var $receiverRemote CourierDhlRemoteAreas */
            $price += $receiverRemote->getPrice($currency);
        }

        // FOR SENDER:
        if($this->with_pickup) {
            $senderRemote = CourierDhlRemoteAreas::findZip($this->courier->senderAddressData->zip_code, $this->courier->senderAddressData->country_id, $this->courier->senderAddressData->city);
            if ($senderRemote) {
                /* @var $receiverRemote CourierDhlRemoteAreas */
                $price += $senderRemote->getPrice($currency);
            }
        }

        return $price;
    }


    /**
     * Returns price for one package
     * @param $currency
     * @param bool|false $force_user_id If true, price will be calculated for particular user, not logged one
     * @return float|int
     */
    public function priceEach($currency, $force_user_id = false, $force_user_group_id = false)
    {
        $price = 0;

        $price += $this->pickupPrice($currency, $force_user_id, $force_user_group_id);
        $price += $this->deliveryPrice($currency, $force_user_id, $force_user_group_id);
        $price += $this->additionsPrice();

        $price += $this->dhlRemoteArea($currency);

        return $price;
    }

    /**
     * Return price for all packages
     *
     * @param bool|false $force_user_id If true, price will be calculated for particular user, not logged one
     * @return float
     */
    public function priceTotal($currency, $force_user_id = false, $force_user_group_id = false)
    {
        $price = $this->priceEach($currency, $force_user_id, $force_user_group_id) * $this->courier->packages_number;


        return $price;
    }

    protected function isPickupPossibleForCountryForUserGroup($userGroupId)
    {
        if($this->with_pickup == self::WITH_PICKUP_NONE OR
            $this->with_pickup == self::WITH_PICKUP_CONTRACT OR
            ($this->with_pickup == self::WITH_PICKUP_RUCH && $this->courier->senderAddressData->country_id == CountryList::COUNTRY_PL) OR
            ($this->with_pickup == self::WITH_PICKUP_RM && $this->courier->senderAddressData->country_id == CountryList::COUNTRY_UK) OR
            ($this->with_pickup == self::WITH_PICKUP_LP_EXPRESS && $this->courier->senderAddressData->country_id == CountryList::COUNTRY_LT) OR
            ($this->with_pickup == self::WITH_PICKUP_INPOST && $this->courier->senderAddressData->country_id == CountryList::COUNTRY_PL) OR
            ($this->with_pickup == self::WITH_PICKUP_DHLDE && in_array($this->courier->senderAddressData->country_id , CountryList::getUeList(false)))
        )
            return true;

        if($this->with_pickup == self::WITH_PICKUP_RUCH && $this->courier->senderAddressData->country_id != CountryList::COUNTRY_PL)
            return false;

        if($this->with_pickup == self::WITH_PICKUP_RM && $this->courier->senderAddressData->country_id != CountryList::COUNTRY_UK)
            return false;

        if($this->with_pickup == self::WITH_PICKUP_LP_EXPRESS && $this->courier->senderAddressData->country_id != CountryList::COUNTRY_LT)
            return false;

        if($this->with_pickup == self::WITH_PICKUP_DHLDE && !in_array($this->courier->senderAddressData->country_id , CountryList::getUeList(false)))
            return false;


        $cmd = Yii::app()->db->createCommand();
        $cmd->select = 'country_list.id';
        $cmd->from = 'country_list';
        $cmd->join('courier_country_group_has_country','courier_country_group_has_country.country = country_list.id');
        $cmd->join('courier_country_group','courier_country_group.id = courier_country_group_has_country.courier_group');
        $cmd->join('courier_pickup_price_has_group','courier_pickup_price_has_group.courier_country_group = courier_country_group.id');
        $cmd->where('country_list.id = :id', array(':id' => $this->courier->sender_country_id));
        $cmd->andWhere('pickup_hub IS NOT NULL');
        $cmd->andWhere('pickup_operator IS NOT NULL');
        $cmd->andWhere('country_list.stat = '.S_Status::ACTIVE);
        $cmd->andWhere('courier_pickup_price_has_group.user_group_id = :group', array(':group' => $userGroupId));
        $result = $cmd->queryRow();

        if($result)
            return true;
        else
            return false;
    }

    public function isPickupPossibleForCountry($userGroupId = NULL)
    {

        if($this->with_pickup == self::WITH_PICKUP_NONE OR
            $this->with_pickup == self::WITH_PICKUP_CONTRACT OR
            ($this->with_pickup == self::WITH_PICKUP_RUCH && $this->courier->senderAddressData->country_id == CountryList::COUNTRY_PL) OR
            ($this->with_pickup == self::WITH_PICKUP_RM && $this->courier->senderAddressData->country_id == CountryList::COUNTRY_UK) OR
            ($this->with_pickup == self::WITH_PICKUP_INPOST && $this->courier->senderAddressData->country_id == CountryList::COUNTRY_PL) OR
            ($this->with_pickup == self::WITH_PICKUP_LP_EXPRESS && $this->courier->senderAddressData->country_id == CountryList::COUNTRY_LT) OR
            ($this->with_pickup == self::WITH_PICKUP_DHLDE && in_array($this->courier->senderAddressData->country_id , CountryList::getUeList(false)))
        )
            return true;

        if($this->with_pickup == self::WITH_PICKUP_RUCH && $this->courier->senderAddressData->country_id != CountryList::COUNTRY_PL)
            return false;

        if($this->with_pickup == self::WITH_PICKUP_RM && $this->courier->senderAddressData->country_id != CountryList::COUNTRY_UK)
            return false;

        if($this->with_pickup == self::WITH_PICKUP_LP_EXPRESS && $this->courier->senderAddressData->country_id != CountryList::COUNTRY_LT)
            return false;

        if($this->with_pickup == self::WITH_PICKUP_DHLDE && !in_array($this->courier->senderAddressData->country_id , CountryList::getUeList(false)))
            return false;

        if($userGroupId)
            if($this->isPickupPossibleForCountryForUserGroup($userGroupId))
                return true;

//        if(Yii::app()->params['frontend'] && Yii::app()->user->model->source_domain == PageVersion::PAGEVERSION_CQL)
//            return false;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = 'country_list.id';
        $cmd->from = 'country_list';
        $cmd->join('courier_country_group_has_country','courier_country_group_has_country.country = country_list.id');
        $cmd->join('courier_country_group','courier_country_group.id = courier_country_group_has_country.courier_group');
        $cmd->join('courier_pickup_price_has_group','courier_pickup_price_has_group.courier_country_group = courier_country_group.id');
        $cmd->where('country_list.id = :id', array(':id' => $this->courier->sender_country_id));
        $cmd->andWhere('pickup_hub IS NOT NULL');
        $cmd->andWhere('pickup_operator IS NOT NULL');
        $cmd->andWhere('country_list.stat = '.S_Status::ACTIVE);
        $cmd->andWhere('courier_pickup_price_has_group.user_group_id IS NULL');
        $result = $cmd->queryRow();

        if($result)
            return true;
        else
            return false;
    }

    protected function isCarriagePossibleForUserGroup($userGroupId)
    {
        if(!$this->isPickupPossibleForCountry($userGroupId))
            return false;

        $cmd = Yii::app()->db->createCommand();

        $final_weight_pick = $this->getFinalWeight(true, false);
        $final_weight_delivery = $this->getFinalWeight(false, true);

        $country_to_group = CourierCountryGroup::getGroupForCountryId($this->courier->receiverAddressData->country_id);
        $country_from_group = CourierCountryGroup::getGroupForCountryId($this->courier->senderAddressData->country_id);
        $pickup_hub = CourierCountryGroup::getPickupHub($country_from_group, $final_weight_pick, $userGroupId);

        $cmd->select('price_per_item, price_per_weight');
        $cmd->from('courier_price_value');
        $cmd->join('courier_price', 'courier_price_value.courier_price_id = courier_price.id');
        $cmd->join('courier_delivery_price_has_hub', 'courier_delivery_price_has_hub.courier_price_item = courier_price.id');
        $cmd->join('courier_country_group', 'courier_delivery_price_has_hub.courier_country_group = courier_country_group.id');
        $cmd->where('weight_top_limit >= :weight', array(':weight' => $final_weight_delivery));
        $cmd->andWhere('courier_country_group.id = :country_group', array(':country_group' => $country_to_group));
        if($this->with_pickup)
            $cmd->andWhere('courier_delivery_price_has_hub.courier_pickup_hub = :hub', array(':hub' => $pickup_hub));
        $cmd->andWhere('courier_delivery_price_has_hub.courier_pickup_hub = :hub', array(':hub' => $pickup_hub));
        $cmd->andWhere('courier_delivery_price_has_hub.user_group_id = :group', array(':group' => $userGroupId));
        $cmd->order('weight_top_limit ASC');
        $result = $cmd->queryRow();

        if($result)
            return true;
        else
            return false;
    }

    public function isCarriagePossible($userGroup = NULL)
    {

        $final_weight_pick = $this->getFinalWeight(true, false);
        $final_weight_delivery = $this->getFinalWeight(false, true);


        $CACHE_NAME = 'INTERNAL_IS_CARRIAGE_POSSIBLE_'.$this->courier->receiverAddressData->country_id.'_'.$this->courier->senderAddressData->country_id.'_'.$userGroup.'_'.$final_weight_pick.'_'.$this->with_pickup.'_'.$final_weight_delivery;
        $result = Yii::app()->cache->get($CACHE_NAME);

        if(self::DISABLE_ROUTING_CACHE)
            $result = false;

        if($result === false)
        {
            if(!$this->isPickupPossibleForCountry($userGroup))
                return false;

            if($userGroup)
                if($this->isCarriagePossibleForUserGroup($userGroup))
                    return true;

//            if(Yii::app()->params['frontend'] && Yii::app()->user->model->source_domain == PageVersion::PAGEVERSION_CQL)
//                return false;

            $cmd = Yii::app()->db->createCommand();

            $country_to_group = CourierCountryGroup::getGroupForCountryId($this->courier->receiverAddressData->country_id);
            $country_from_group = CourierCountryGroup::getGroupForCountryId($this->courier->senderAddressData->country_id);
            $pickup_hub = CourierCountryGroup::getPickupHub($country_from_group, $final_weight_pick, $userGroup);

            $cmd->select('price_per_item, price_per_weight');
            $cmd->from('courier_price_value');
            $cmd->join('courier_price', 'courier_price_value.courier_price_id = courier_price.id');
            $cmd->join('courier_delivery_price_has_hub', 'courier_delivery_price_has_hub.courier_price_item = courier_price.id');
            $cmd->join('courier_country_group', 'courier_delivery_price_has_hub.courier_country_group = courier_country_group.id');
            $cmd->where('weight_top_limit >= :weight', array(':weight' => $final_weight_delivery));
            $cmd->andWhere('courier_country_group.id = :country_group', array(':country_group' => $country_to_group));
            if($this->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR)
                $cmd->andWhere('courier_delivery_price_has_hub.courier_pickup_hub = :hub', array(':hub' => $pickup_hub));
            $cmd->andWhere('courier_delivery_price_has_hub.user_group_id IS NULL');
            $cmd->order('weight_top_limit ASC');

            $result = $cmd->queryRow();

            // not to mess with cache not found response
            if(!$result)
                $result = -1;

            Yii::app()->cache->set($CACHE_NAME, $result, 60);
        }

        if($result == -1)
            $result = false;

        if($result)
            return true;
        else
            return false;
    }

    public function getPickupHub($returnModel = false)
    {

        $final_weight = $this->getFinalWeight(true, false);
        $userGroupId = $this->courier->getOwnerUserGroupId();

        $CACHE_NAME = 'PICKUP_HUB_'.$this->courier->senderAddressData->country_id.'_'.$final_weight.'_'.$returnModel.'_'.$userGroupId;
        $return = Yii::app()->cache->get($CACHE_NAME);

        if(self::DISABLE_ROUTING_CACHE)
            $return = false;

        if($return === false) {

            $countryGroup = CourierCountryGroup::getGroupForCountryId($this->courier->senderAddressData->country_id);

            $return = CourierCountryGroup::getPickupHub($countryGroup, $final_weight, $userGroupId, $returnModel);

            Yii::app()->cache->set($CACHE_NAME, $return, 60);
        }

        return $return;
    }

    public function getPickupOperator($returnModel = false)
    {
        if($this->with_pickup == self::WITH_PICKUP_RUCH)
        {
            $return = CourierOperator::OPERATOR_RUCH;
            if($returnModel)
                $return = CourierOperator::model()->findByPk($return);

            return $return;
        }

        if($this->with_pickup == self::WITH_PICKUP_RM)
        {
            $operator = CourierOperator::model()->findByAttributes(['uniq_id' => CourierOperator::UNIQID_ROYALMAIL_TRACKED_RETURN_48]);

            $return = $operator->id;
            if($returnModel)
                $return = $operator;

            return $return;
        }

        if($this->with_pickup == self::WITH_PICKUP_INPOST)
        {
            $operator = CourierOperator::model()->findByAttributes(['broker' => GLOBAL_BROKERS::BROKER_INPOST_SHIPX]);

            $return = $operator->id;
            if($returnModel)
                $return = $operator;

            return $return;
        }

        if($this->with_pickup == self::WITH_PICKUP_DHLDE)
        {
            $operator = CourierOperator::model()->findByAttributes(['broker' => GLOBAL_BROKERS::BROKER_DHLDE_RETOURE]);

            $return = $operator->id;
            if($returnModel)
                $return = $operator;

            return $return;
        }

        if($this->with_pickup == self::WITH_PICKUP_LP_EXPRESS)
        {
            $operator = CourierOperator::model()->findByAttributes(['uniq_id' => CourierOperator::UNIQID_LP_EXPRESS]);

            $return = $operator->id;
            if($returnModel)
                $return = $operator;

            return $return;
        }


        $final_weight = $this->getFinalWeight(true, false);
        $userGroupId = $this->courier->getOwnerUserGroupId();



        /* @13.09.2017 - Special rule, to set operator as DPDPL_V for bigger packages instead of using base routing from PL */
        $overrideOperatorUniqId = false;
        // cancelled at 27.03.2018
//        if($this->courier->senderAddressData->country_id == CountryList::COUNTRY_PL)
//        {
//            $dimensions = [
//                $this->courier->package_size_l,
//                $this->courier->package_size_d,
//                $this->courier->package_size_w,
//            ];
//
//            // biggest dimension is length and is first
//            rsort($dimensions);
//
//            if($dimensions[0] > 140 OR array_sum($dimensions) > 260)
//            {
//                $overrideOperatorUniqId = CourierOperator::UNIQID_DPDPL_V;
//            }
//        }


        $CACHE_NAME = 'PICKUP_OPERATOR_'.$this->courier->senderAddressData->country_id.'_'.$final_weight.'_'.$returnModel.'_'.$userGroupId.'_'.$overrideOperatorUniqId;
        $return = Yii::app()->cache->get($CACHE_NAME);

        if(self::DISABLE_ROUTING_CACHE)
            $return = false;

        if($return === false) {

            $countryGroup = CourierCountryGroup::getGroupForCountryId($this->courier->senderAddressData->country_id);
            $return = CourierCountryGroup::getPickupOperator($countryGroup, $final_weight, $returnModel, $userGroupId, $overrideOperatorUniqId);

            Yii::app()->cache->set($CACHE_NAME, $return, 60);
        }

        $return = CourierInternalOperatorChange::filterOperator($return);

        return $return;
    }

    public function getDeliveryHub($returnModel = false)
    {
        $final_weight = $this->getFinalWeight(false, true);
        $userGroupId = $this->courier->getOwnerUserGroupId();

        $CACHE_NAME = 'DELIVERY_HUB_'.$this->courier->receiverAddressData->country_id.'_'.$final_weight.'_'.$returnModel.'_'.$userGroupId;

        $return = Yii::app()->cache->get($CACHE_NAME);

        if(self::DISABLE_ROUTING_CACHE)
            $return = false;

        if($return === false) {

            $countryGroup = CourierCountryGroup::getGroupForCountryId($this->courier->receiverAddressData->country_id);

            $return = CourierCountryGroup::getDeliveryHub($countryGroup, $returnModel, $final_weight, $userGroupId);

            Yii::app()->cache->set($CACHE_NAME, $return, 60);
        }

        return $return;
    }

    public function getDeliveryOperator($returnModel = false)
    {
        if($this->with_pickup == self::WITH_PICKUP_RUCH && $this->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
        {
            $return = CourierOperator::OPERATOR_RUCH;
            if($returnModel)
                $return = CourierOperator::model()->findByPk($return);

            return $return;
        }

        if($this->with_pickup == self::WITH_PICKUP_RM && $this->courier->receiverAddressData->country_id == CountryList::COUNTRY_UK)
        {
            $operator = CourierOperator::model()->findByAttributes(['uniq_id' => CourierOperator::UNIQID_ROYALMAIL_TRACKED_RETURN_48]);

            $return = $operator->id;
            if($returnModel)
                $return = $operator;

            return $return;
        }

        if($this->with_pickup == self::WITH_PICKUP_INPOST && $this->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
        {
            $operator = CourierOperator::model()->findByAttributes(['broker' => GLOBAL_BROKERS::BROKER_INPOST_SHIPX]);

            $return = $operator->id;
            if($returnModel)
                $return = $operator;

            return $return;
        }

//        if($this->with_pickup == self::WITH_PICKUP_DHLDE && $this->courier->receiverAddressData->country_id == CountryList::COUNTRY_DE)
//        {
//            $operator = CourierOperator::model()->findByAttributes(['uniq_id' => CourierOperator::UNIQID_DHLDE_PAKET]);
//
//            $return = $operator->id;
//            if($returnModel)
//                $return = $operator;
//
//            return $return;
//        }

        if($this->with_pickup == self::WITH_PICKUP_LP_EXPRESS && $this->courier->receiverAddressData->country_id == CountryList::COUNTRY_LT)
        {
            $operator = CourierOperator::model()->findByAttributes(['uniq_id' => CourierOperator::UNIQID_LP_EXPRESS]);

            $return = $operator->id;
            if($returnModel)
                $return = $operator;

            return $return;
        }


        $final_weight = $this->getFinalWeight(false, true);
        $userGroupId = $this->courier->getOwnerUserGroupId();


        /* @13.09.2017 - Special rule, to set operator as DPDPL_V for bigger packages instead of using base routing to PL */
        $overrideOperatorUniqId = false;
        // cancelled at 07.03.2018
//        if($this->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
//        {
//            $dimensions = [
//                $this->courier->package_size_l,
//                $this->courier->package_size_d,
//                $this->courier->package_size_w,
//            ];
//
//            // biggest dimension is length and is first
//            rsort($dimensions);
//
//            if($dimensions[0] > 140 OR array_sum($dimensions) > 260)
//            {
//                $overrideOperatorUniqId = CourierOperator::UNIQID_DPDPL_V;
//            }
//        }

        if($this->courier->receiverAddressData->country_id == CountryList::COUNTRY_DE && $this->courier->user && (in_array($this->courier->user->getUserGroup(), [262,233]) OR $this->courier->user_id == 8)) {

            $redirectToDhlDe = false;
            if (preg_match('/PACKSTATION (\d{3})/i', $this->courier->receiverAddressData->address_line_1 . ' ' . $this->courier->receiverAddressData->address_line_2))
                $redirectToDhlDe = true;
            else if (preg_match('/POSTFILIALE (\d{3})/i', $this->courier->receiverAddressData->address_line_1 . ' ' . $this->courier->receiverAddressData->address_line_2, $match))
                $redirectToDhlDe = true;
            else if (preg_match('/PARCELSHOP (\d{3})/i', $this->courier->receiverAddressData->address_line_1 . ' ' . $this->courier->receiverAddressData->address_line_2, $match))
                $redirectToDhlDe = true;

            if ($redirectToDhlDe)
                $overrideOperatorUniqId = CourierOperator::UNIQID_DHLDE_PAKET;
        }

        $CACHE_NAME = 'DELIVERY_OPERATOR_'.$this->courier->receiverAddressData->country_id.'_'.$final_weight.'_'.$returnModel.'_'.$userGroupId.'_'.$overrideOperatorUniqId;
        $return = Yii::app()->cache->get($CACHE_NAME);

        if(self::DISABLE_ROUTING_CACHE)
            $return = false;

        if($return === false) {

            $countryGroup = CourierCountryGroup::getGroupForCountryId($this->courier->receiverAddressData->country_id);
            $return = CourierCountryGroup::getDeliveryOperator($countryGroup, $final_weight, $returnModel, $userGroupId, $overrideOperatorUniqId);

            Yii::app()->cache->set($CACHE_NAME, $return, 60);
        }


        $return = CourierInternalOperatorChange::filterOperator($return);

        return $return;
    }


    public function getDimensionWeight()
    {
        return S_PackageMaxDimensions::calculateDimensionsWeight($this->courier);
    }


    /**
     * Used for routing and pricing
     * @param bool $firstOperatorMode
     * @param bool $secondOperatorMode
     * @return float
     * @throws Exception
     */
    public function getFinalWeight($firstOperatorMode = false, $secondOperatorMode = false)
    {
        if($this->courier->receiverAddressData === NULL OR
            $this->courier->senderAddressData === NULL)
            throw new Exception('No sender or receiver model!');

        $this->courier->courierTypeInternal = $this;

        if($firstOperatorMode)
        {
            if($this->courier->senderAddressData->country_id == CountryList::COUNTRY_PL)
                $withDimensionalWeight = false;
            else
                $withDimensionalWeight = true;

        }
        else if($secondOperatorMode)
        {
            if($this->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
                $withDimensionalWeight = false;
            else
                $withDimensionalWeight = true;
        }
        else
        {
            if($this->courier->senderAddressData->country_id == CountryList::COUNTRY_PL && $this->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
                $withDimensionalWeight = false;
            else
                $withDimensionalWeight = true;
        }

        if($withDimensionalWeight) {
            $weight = $this->getDimensionWeight();

            if($weight < $this->courier->package_weight)
                $weight = $this->courier->package_weight;

        }
        else
            $weight = $this->courier->package_weight;

        return $weight;
    }

    public function afterSave()
    {

        return parent::afterSave();
    }

    public function saveNewData($i)
    {

//        $this->performCODCheck();

        if ($this->with_pickup) {
            $pickupHub = self::getPickupHub(true);
            $pickupOperator = self::getPickupOperator(true);

            $this->pickup_hub_id = $pickupHub->id;
            $this->pickup_hub_name = $pickupHub->name;
            $this->pickup_operator_id = $pickupOperator->id;
            $this->pickup_operator_name = $pickupOperator->name;
        }

        $deliveryHub = self::getDeliveryHub(true);
        $deliveryOperator = self::getDeliveryOperator(true);

        $this->delivery_hub_id = $deliveryHub->id;
        $this->delivery_hub_name = $deliveryHub->name;
        $this->delivery_operator_id = $deliveryOperator->id;
        $this->delivery_operator_name = $deliveryOperator->name;

        if ($this->with_pickup == self::WITH_PICKUP_RUCH)
        {
            $this->delivery_hub_id = CourierHub::HUB_PL;
            $this->delivery_hub_name = 'HUB PL (RUCH)';
            $this->delivery_operator_id = CourierOperator::OPERATOR_RUCH;
            $this->delivery_operator_name = 'Ruch';

            $this->pickup_hub_id = NULL;
            $this->pickup_hub_name = NULL;
            $this->pickup_operator_id = CourierOperator::OPERATOR_RUCH;
            $this->pickup_operator_name = 'Ruch';
        }

        if($this->with_pickup == self::WITH_PICKUP_CONTRACT)
        {
            $this->pickup_operator_id = CourierOperator::OPERATOR_OWN;
            $this->pickup_operator_name = 'KAAB';
        }

        $this->dimensional_weight = $this->getDimensionWeight();
        $this->final_weight = $this->getFinalWeight();


        //////////////////////////////////////
        //// SPECJALNY WARUNEK PRZEKIEROWANIA!
        // JEŻELI PACZKA CIĘŻSZA (WAGA ZWYKŁA LUB GABARYTOWA) NIŻ X TO KIERUJ NAJPIERW ZAWSZE DO NYSY!
        ////
        ////

//        /* @var $senderCountryGroup CourierCountryGroup */
//        $sender_country_id = $this->courier->senderAddressData->country_id;
//        $senderCountryGroup = CountryList::model()->findByPk($sender_country_id)->courierCountryGroupHasCountry->courierGroup;
//
//        /* @var $receiverCountryGroup CourierCountryGroup */
//        $receiver_country_id = $this->courier->receiverAddressData->country_id;
//        $receiverCountryGroup = CountryList::model()->findByPk($receiver_country_id)->courierCountryGroupHasCountry->courierGroup;
//
//        if($this->with_pickup
//            &&
//            $this->final_weight > 1
//            &&
//            $senderCountryGroup->id == 1 // PL
//            &&
//            (
//                $receiverCountryGroup->id == 13 // DE
//                ||
//                $receiverCountryGroup->id == 14 // CZ
//            )
//        )
//        {
//            $this->pickup_hub_id = $senderCountryGroup->pickup_hub;
//            $this->pickup_hub_name = $senderCountryGroup->pickupHub->name;
//            $this->delivery_hub_id = $this->pickup_hub_id;
//            $this->delivery_hub_name = $this->pickup_hub_name;
//            $this->pickup_operator_id = CourierOperator::OPERATOR_UPS;
//            $this->pickup_operator_name = 'UPS';
//            $this->delivery_operator_id = $this->pickup_operator_id;
//            $this->delivery_operator_name = $this->pickup_operator_name;
//        }
        //////////////////////////////////////
        //// SPECJALNY WARUNEK PRZEKIEROWANIA!
        // JEŻELI PACZKA CIĘŻSZA (WAGA ZWYKŁA LUB GABARYTOWA) NIŻ X TO KIERUJ NAJPIERW ZAWSZE DO NYSY!
        ////
        ////



        $this->family_member_number = $i;

        if (!$this->save()) {
            Yii::log(print_r($this->getErrors(),1), CLogger::LEVEL_ERROR);
            return false;
        }
        else {
            $this->onAfterNewCourierTypeInternal(new CEvent($this, array('model' => $this)));
            return true;
        }

    }

    public function setPaid()
    {
        $this->paid = 1;

        $this->update(array('paid'));
        return $this->onPaid();
    }

    protected function onPaid()
    {

        $return = CourierOperator::bookOperators($this);

        if($return)
        {
            // search for all labels (one or two - depends how many operators)
            $courierLabels = CourierLabelNew::model()->findAll('courier_id = :courier_id', array(':courier_id' => $this->courier->id));

            // order labels for all of found
            /* @var $item CourierLabelNew */
            foreach($courierLabels AS $item)
            {
                if($this->courier->source == Courier::SOURCE_API && $item->stat == CourierLabelNew::STAT_NEW_DIRECT)
                    $item->runLabelOrdering();
                else
                    CourierLabelNew::asyncCallForId($item->id,0);
            }
        }

        if(!$return)
        {
            Yii::log('Bląd zamawiania kuriera', CLogger::LEVEL_ERROR);
            return false;
        }

        return true;
    }

    public function assingToOrder($order_id)
    {
        $this->order_id = $order_id;
        return $this->update(array('order_id'));
    }


    public function onAfterNewCourierTypeInternal(CEvent $e)
    {
        //file_put_contents('ctie.dat' ,  CVarDumper::dumpAsString($e->params));
    }


    protected static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    protected static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    /**
     * Serialize additional params (ex. Preffered pickup time)
     * @return bool
     */
    public function beforeSave()
    {
        $this->params = array(
            '_preffered_pickup_day' => $this->_preffered_pickup_day,
            '_preffered_pickup_time' => $this->_preffered_pickup_time,
            '_ebay_order_id' => $this->_ebay_order_id,
            '_linker_order_id' => $this->_linker_order_id,
            '_ebay_export_status' => $this->_ebay_export_status,
            '_ruch_receiver_point' => $this->_ruch_receiver_point,
            '_lp_express_receiver_point' => $this->_lp_express_receiver_point,
            '_lp_express_receiver_point_address' => $this->_lp_express_receiver_point_address,
            '_inpost_locker_delivery_id' => $this->_inpost_locker_delivery_id,
            '_inpost_locker_delivery_address' => $this->_inpost_locker_delivery_address,
            '_inpost_locker_pickup_id' => $this->_inpost_locker_pickup_id,
            '_inpost_locker_pickup_address' => $this->_inpost_locker_pickup_address,
            '_inpost_locker_active' => $this->_inpost_locker_active,
            'pickup_point_id' => $this->pickup_point_id,
        );

        if($this->params !== NULL)
            $this->params = self::serialize($this->params);

        return parent::beforeSave();
    }

    /**
     * Unserialize additional params (ex. Preffered pickup time)
     * @return bool
     */
    public function afterFind()
    {
        if($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if(is_array($this->params))
            foreach($this->params AS $key => $item)
            {
                try {
                    $this->$key = $item;
                }
                catch (Exception $ex)
                {}
            }

        parent::afterFind();
    }


    /**
     * Return params as arrays with keys as attribute labels
     * @return array
     */
    public function getParamsWithLabelNames()
    {
        $data = array();
        if(is_array($this->params))
        {

            foreach($this->params AS $key => $item)
            {

                $data[$this->getAttributeLabel($key)] = $item;
            }
        }

        return $data;
    }



    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public static function saveWholeGroup(array $couriers, $source = false, $misc = false, $cancelPackageOnFailure = false)
    {

        $errors = false;

        $packages = [];

        /* @var $courier Courier */
        foreach($couriers AS $key => $courier)
        {
            if($courier->courierTypeInternal->courier === NULL)
                $courier->courierTypeInternal->courier = $courier;

            $return = $courier->courierTypeInternal->saveNewPackageMass(true, $courier->courierTypeInternal->_ebay_order_id, $cancelPackageOnFailure);

            if(is_array($return)) {

                foreach($return AS $item) {

//                    MyDump::dump('ebay_cti.txt', print_r($item->id,1).'||'.print_r($courier->courierTypeInternal->_ebay_order_id,1).'||'.print_r($source,1).'||'.print_r($misc,1));

                    if ($source == self::SOURCE_EBAY OR $source == self::SOURCE_LINKER) {

                        // maybe already exists? Then o
                        if($source == self::SOURCE_LINKER)
                            $ebayImportSource = EbayImport::SOURCE_LINKER;
                        else
                            $ebayImportSource = EbayImport::SOURCE_EBAY;

                        $ebayImport = EbayImport::model()->findByAttributes(['ebay_order_id' => $return->_ebay_order_id, 'user_id' => $item->user_id, 'target' => EbayImport::TARGET_COURIER, 'source' => $ebayImportSource]);
                        if ($ebayImport === NULL) {
                            $ebayImport = new EbayImport();
                            $ebayImport->user_id = $item->user_id;
                            $ebayImport->source = $ebayImportSource;
                        }

                        $ebayImport->target_item_id = $item->id;
                        $ebayImport->ebay_order_id = $item->courierTypeInternal->_ebay_order_id;
                        $ebayImport->stat = EbayImport::STAT_NEW;
                        $ebayImport->authToken = $misc;
                        $ebayImport->target = EbayImport::TARGET_COURIER;
                        $ebayImport->save();

//                        MyDump::dump('ebay_import.txt', 'A: '.print_r($ebayImport->attributes,1));
//                        MyDump::dump('ebay_import.txt', 'E: '.print_r($ebayImport->getErrors(),1));
                    }
                }
                $packages = array_merge($packages, $return);
            }

        }

        $orderProducts = [];
        foreach($packages AS $packageItem)
        {
            $product = OrderProduct::createNewProduct(
                OrderProduct::TYPE_COURIER,
                $packageItem->id,
                Yii::app()->PriceManager->getCurrencyCode(),
                $packageItem->courierTypeInternal->priceEach(Yii::app()->PriceManager->getCurrency()),
                Yii::app()->PriceManager->getFinalPrice($packageItem->courierTypeInternal->priceEach(Yii::app()->PriceManager->getCurrency()))
            );

            array_push($orderProducts, $product);
        }

        $order = Order::createOrderForProducts($orderProducts, true);

        if($order->fastFinalizeOrder(true))
        {
            return [
                'success' => true,
                'data' => $packages,
            ];
        }

        return false;
    }



    public function saveNewPackageMass($ownTransaction = false, $ebayOrderId = false, $cancelPackageOnFailure = false)
    {

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $courierPackagesFamily = [];
        $courierParentId = NULL;


        if(!$this->courier->packages_number)
        {
            $errors = true;
            $transaction->rollback();
            return false;
        }



        for($i = 0; $i < $this->courier->packages_number; $i++) {

            if($errors)
                continue;

            $courier = clone $this->courier;
            $courier->courierTypeInternal = clone $this->courier->courierTypeInternal;
            $courier->receiverAddressData = clone $this->courier->receiverAddressData;
            $courier->senderAddressData = clone $this->courier->senderAddressData;

            $courier->courierTypeInternal->courier = $courier;

            $courier->courierTypeInternal->_package_wrapping = $this->courier->courierTypeInternal->_package_wrapping;

            /**
             * RECEIVER DATA
             */
            if ($courier->receiverAddressData === NULL)
                $courier->receiverAddressData = new AddressData();

            $courier->receiverAddressData->validate();
            if ($courier->receiverAddressData->hasErrors())
                $errors = true;
            else {
                $courier->receiverAddressData->save();
                $courier->receiver_address_data_id = $courier->receiverAddressData->id;

//                if ($saveReceiverToBook)
//                    UserContactBook::createAndSave(false, $courier->receiverAddressData->attributes, 'receiver');

            }

            /**
             * SENDER DATA
             */
            if ($courier->senderAddressData === NULL)
                $courier->senderAddressData = new AddressData();

            $courier->senderAddressData->validate();
            if ($courier->senderAddressData->hasErrors())
                $errors = true;
            else {
                $courier->senderAddressData->save();
                $courier->sender_address_data_id = $courier->senderAddressData->id;

//                if ($saveSenderToBook)
//                    UserContactBook::createAndSave(false, $courier->senderAddressData->attributes, 'sender');


            }

            $courier->courierTypeInternal->parent_courier_id = $courierParentId;

            if (!$courier->courierTypeInternal->saveNewData($i + 1))
                $errors = true;
            $courier->courier_type_internal_id = $courier->courierTypeInternal->id;

            //


            /**
             * ORDER DATA
             */
            $courier->scenario = 'insert';

            if (!$courier->save())
                $errors = true;

            /* @var $item CourierAdditionList */
            foreach($courier->courierTypeInternal->listAdditions(true) AS $item)
            {
                $addition = new CourierAddition();
                $addition->courier_id = $courier->id;
                $addition->description = $item->courierAdditionListTr->description;
                $addition->price = Yii::app()->PriceManager->getFinalPrice($item->getPrice());
                $addition->name = $item->courierAdditionListTr->title;
                $addition->courier_addition_list_id = $item->id;

                if(!$addition->save())
                    $errors = true;
            }

            if(!$i)
                $courierParentId = $courier->id;

            $courier->courierTypeInternal->courier = $courier;

            array_push($courierPackagesFamily, $courier);
        }



        if(!$errors)
        {
            if($ownTransaction)
                $transaction->commit();
            return $courierPackagesFamily;
        }
        else
        {
            if($ownTransaction)
                $transaction->rollBack();
            return false;
        }
    }


    /**
     * Returns status of first label for customer. Used in list of imported packages. If operator if OWN, return true. Statuses used are from CourierLabelNew
     *
     * @return int
     */
    public function getPrimaryLabelStatus()
    {
        if($this->common_operator_ordered) {
            // pickup operator == delivery operator == common operator
            if($this->delivery_operator_id == CourierOperator::OPERATOR_OWN && $this->commonLabel === NULL)
                return CourierLabelNew::STAT_SUCCESS;
            else
                return $this->commonLabel->courierLabelNew->stat;
        }
        else if($this->pickup_operator_id != NULL && (Yii::app()->user->isGuest OR !Yii::app()->user->model->getAllowCourierInternalExtendedLabel())) {
            if ($this->pickup_operator_id == CourierOperator::OPERATOR_OWN && $this->pickupLabel === NULL)
                return CourierLabelNew::STAT_SUCCESS;
            else
                return $this->pickupLabel->courierLabelNew->stat;
        }
        else if($this->delivery_operator_id !== NULL)
        {
            if($this->delivery_operator_id == CourierOperator::OPERATOR_OWN && $this->deliveryLabel === NULL)
                return CourierLabelNew::STAT_SUCCESS;
            else
                return $this->deliveryLabel->courierLabelNew->stat;
        }

        return false;
    }

    /**
     * Method starts label ordering
     *
     * @return array|bool
     */
    public function orderPrimaryLabel()
    {
        if($this->common_operator_ordered) {
            // pickup operator == delivery operator == common operator
//            if($this->delivery_operator_id != CourierOperator::OPERATOR_OWN && $this->commonLabel->courierLabelNew->stat == CourierLabelNew::STAT_NEW)
            if($this->commonLabel->courierLabelNew->stat == CourierLabelNew::STAT_NEW)
                return $this->commonLabel->courierLabelNew->runLabelOrdering(true);
            else
                return true;
        }
        else if($this->pickup_operator_id != NULL && (Yii::app()->user->isGuest OR !Yii::app()->user->model->getAllowCourierInternalExtendedLabel()))
        {
//            if($this->pickup_operator_id != CourierOperator::OPERATOR_OWN && $this->pickupLabel->courierLabelNew->stat == CourierLabelNew::STAT_NEW)
            if($this->pickupLabel->courierLabelNew->stat == CourierLabelNew::STAT_NEW)
                return $this->pickupLabel->courierLabelNew->runLabelOrdering(true);
            else
                return true;
        }
        else if($this->delivery_operator_id !== NULL)
        {
            // pickup operator == delivery operator == common operator
//            if($this->delivery_operator_id != CourierOperator::OPERATOR_OWN && $this->deliveryLabel->courierLabelNew->stat == CourierLabelNew::STAT_NEW)
            if($this->deliveryLabel->courierLabelNew->stat == CourierLabelNew::STAT_NEW)
                return $this->deliveryLabel->courierLabelNew->runLabelOrdering(true);
            else
                return true;
        }

        return false;
    }




    /**
     * Returns array containing operators name and TT number
     *
     * @return array
     */
    public function listOperatorsWithTt()
    {
        $operators = [];


        if ($this->pickup_operator_name != '') {
            $name = $this->pickup_operator_name;

            $tt = '';
            if ($this->pickupLabel !== NULL)
                $tt = $this->pickupLabel->track_id;

            if($tt != '')
                $operators[] = [
                    'name' => $name,
                    'tt' => $tt,
                ];
        }

        if ($this->delivery_operator_name != '' && $this->delivery_operator_name != $this->pickup_operator_name) {
            $name = $this->delivery_operator_name;

            $tt = '';
            if ($this->deliveryLabel !== NULL)
                $tt = $this->deliveryLabel->track_id;

            if($tt != '')
                $operators[] = [
                    'name' => $name,
                    'tt' => $tt,
                ];
        }

        if ($this->commonLabel !== NULL) {
            $tt = $this->commonLabel->track_id;
            // for common label, operator name = delivery name
            $name = $this->delivery_operator_name;

            $operators[] = [
                'name' => $name,
                'tt' => $tt,
            ];
        }

        $operators = array_map("unserialize", array_unique(array_map("serialize", $operators)));

        return $operators;
    }

    /**
     * Return just one label for package
     * @param bool|true $first
     * @return CourierLabel
     */
    public function getSingleLabel($first = true)
    {

        if($first)
        {
            if($this->common_operator_ordered)
            {
                return $this->commonLabel;
            } else {
                if($this->with_pickup && (Yii::app()->user->isGuest OR !Yii::app()->user->model->getAllowCourierInternalExtendedLabel()))
                    return $this->pickupLabel;
                else
                    return $this->deliveryLabel;
            }
        } else {
            if($this->common_operator_ordered)
            {
                return $this->commonLabel;
            } else {
                return $this->deliveryLabel;
            }
        }

    }


}




