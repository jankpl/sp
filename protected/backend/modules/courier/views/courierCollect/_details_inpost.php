<h3>Pickup Date Info</h3>

<?php
$data = $model->getDataAsObj()['callPickupRequest'];
?>


<table class="list hLeft" style="width: 500px;">
    <tr>
        <td>Ready Date</td>
        <td><?php echo $data->ReadyDate; ?></td>
    </tr>
    <tr>
        <td>Max Pickup Date</td>
        <td><?php echo $data->MaxPickupDate;?></td>
    </tr>
    <tr>
        <td>Total Quantity</td>
        <td><?php echo $data->TotalQuantity;?></td>
    </tr>
    <tr>
        <td>Total Weight</td>
        <td><?php echo $data->TotalWeight;?></td>
    </tr>
</table>

<h3>Packages Ids</h3>

<table class="list" style="width: 500px;">
    <?php if(is_array($data->PackageNo))
        foreach($data->PackageNo as $item):?>
            <tr>
                <td style="text-left"><?php echo $item;?></td>
            </tr>
            <?php
        endforeach;
    ?>
</table>

<h3>Parcels</h3>

<?php if(is_array($data->Parcels))
    foreach($data->Parcels as $item):?>
        <table class="list hLeft" style="width: 500px;">
            <tr>
                <td>Type</td>
                <td><?php echo $item->Type;?></td>
            </tr>
            <tr>
                <td>Weight</td>
                <td><?php echo $item->Weight;?></td>
            </tr>
            <tr>
                <td>D</td>
                <td><?php echo $item->D;?></td>
            </tr>
            <tr>
                <td>W</td>
                <td><?php echo $item->W;?></td>
            </tr>
            <tr>
                <td>S</td>
                <td><?php echo $item->S;?></td>
            </tr>
        </table>
        <br/>
        <?php
    endforeach;
?>


<h3>PickupLocation</h3>

<table class="list hLeft" style="width: 500px;">
    <tr>
        <td>Name</td>
        <td><?php echo $data->PickupLocation->Name;?></td>
    </tr>
    <tr>
        <td>Address</td>
        <td><?php echo $data->PickupLocation->Address;?></td>
    </tr>
    <tr>
        <td>City</td>
        <td><?php echo $data->PickupLocation->City;?></td>
    </tr>
    <tr>
        <td>Post Code</td>
        <td><?php echo $data->PickupLocation->PostCode;?></td>
    </tr>
    <tr>
        <td>Country Code</td>
        <td><?php echo $data->PickupLocation->CountryCode;?></td>
    </tr>
    <tr>
        <td>Person</td>
        <td><?php echo $data->PickupLocation->Person;?></td>
    </tr>
    <tr>
        <td>Contact</td>
        <td><?php echo $data->PickupLocation->Contact;?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?php echo $data->PickupLocation->Email;?></td>
    </tr>
    <tr>
        <td>IsPrivatePerson</td>
        <td><?php echo $data->PickupLocation->IsPrivatePerson ? 'yes' : 'no';?></td>
    </tr>
</table>
