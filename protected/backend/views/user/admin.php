<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php
/* @var $model User */

Yii::app()->getModule('courier');
Yii::app()->getModule('palette');
Yii::app()->getModule('specialTransport');
Yii::app()->getModule('postal');

?>


<div style="word-break: break-all">

    <?php
    $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'withChosenPlugin' => true, 'chosenSelector' => 'UserGridView[id]'));
    ?>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
    <?php $this->_getGridViewUser();?>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
</div>


    <h4>Send message</h4>


<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'mass-delete-stat',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/user/emailer'),
));
?>


<?php
echo TbHtml::submitButton('Continue', array(
    'onClick' => 'js:
    $("#user-ids-mailing").val($("#user").selGridView("getAllSelection").toString());
    ;',
    'name' => 'UserIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('User[ids]','', array('id' => 'user-ids-mailing')); ?>
<?php
$this->endWidget();
?>


<h4>Export all users</h4>

<a href="<?= Yii::app()->createUrl('/user/exportAll');?>" class="btn btn-lg">Export all users to XLS</a>
