<?php
/* @var $this UserMessageController */
/* @var $model UserMessage */


?>

<h1><?php echo Yii::t('user_message', 'Your messages');?></h1>


<?php
echo TbHtml::tabbableTabs(array(
	array('label' => Yii::t('user_message', 'Unseen'), 'active' => true, 'content' => $this->renderPartial('index_tabs/_tab', array('model' => $unseen), true), 'visible' => true),
	array('label' => Yii::t('user_message', 'Seen'),  'content' => $this->renderPartial('index_tabs/_tab', array('model' => $seen), true), 'visible' => true),
));
?>


