<?php
/* @var $model Postal */
/* @var $i int */
/* @var $spPoints array */
/* @var $countryList array */
?>

<tr class="postal-bulk-item">
    <td><?= CHtml::activeDropDownList($model, '['.$i.']receiver_country_id', $countryList, ['prompt' => '-']);?><br/><?= CHtml::error($model, 'receiver_country_id');?></td>
    <td><?= CHtml::activeDropDownList($model, '['.$i.']postal_type', Postal::getPostalTypeBulkList(), ['prompt' => '-', 'style' => 'text-align: center;']);?><br/><?= CHtml::error($model, 'postal_type');?></td>
    <td><?= CHtml::activeNumberField($model, '['.$i.']weight', ['min' => 1, 'max' => Postal::getMaxWeightValue(), 'style' => 'text-align: center;', 'data-weight-value' => true]);?><br/><?= CHtml::error($model, 'weight');?> </td>
    <td style="vertical-align: middle; text-align: center;"><span data-weight-class-info="true">-</span></td>
    <td><?= CHtml::activeDropDownList($model, '['.$i.']size_class', Postal::getSizeClassList(), ['prompt' => '-', 'style' => 'text-align: center;', 'data-size-class' => true]);?><br/><?= CHtml::error($model, 'size_class');?></td>
    <td><?= CHtml::activeNumberField($model, '['.$i.']bulk_number', ['min' => 0, 'max' => Postal::BULK_MAX_NUMBER, 'style' => 'text-align: center;']);?><br/><?= CHtml::error($model, 'bulk_number');?></td>
    <td><?= CHtml::activeTextField($model, '['.$i.']content', ['maxlength' => 32]);?><br/><?= CHtml::error($model, 'content');?></td>
    <?php
    // SPECIAL RULE FOR RUCH // 13.09.2016
    if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH):
        ?>
        <td><?= CHtml::activeDropDownList($model, '['.$i.']target_sp_point', CHtml::listData($spPoints, 'id' ,'spPointsTr.title'), ['prompt' => '-- '.Yii::t('site', 'wybierz punkt z listy').' --', 'style' => 'text-align: center;']);?><br/><?= CHtml::error($model, 'target_sp_point');?></td>
    <?php
    else:

        $model->target_sp_point = SpPoints::CENTRAL_SP_POINT_ID;
        echo CHtml::activeHiddenField($model,'['.$i.']target_sp_point');

    endif;
    ?>
</tr>
