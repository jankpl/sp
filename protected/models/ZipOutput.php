<?php

class ZipOutput
{
   public static function generateZipOutputFile(array $files, $resultFileName)
   {

       $fileTmp = tempnam("tmp", "zip");
       $zipname = $resultFileName.'_'.date('Ymdhis').'.zip';


       $zip = new ZipArchive;
       $zip->open($fileTmp, ZipArchive::CREATE);
       $i = 1;
       foreach ($files AS $path => $name) {
           MyDump::dump('zip_export_2.txt', print_r($path,1).':'.print_r($name,1));

           $name = explode('.', $name);
           $ext = array_pop($name);
           $name[] = $i;
           $name[] = $ext;
           $name = implode('.', $name);

           $zip->addFile($path, $name);
           $i++;
       }
       $zip->close();

       header('Content-Type: application/zip');
       header('Content-disposition: attachment; filename='.$zipname);
       header('Content-Length: ' . filesize($fileTmp));
       readfile($fileTmp);

       unlink($fileTmp);
   }
}