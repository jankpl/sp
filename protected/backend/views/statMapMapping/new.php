<?php
/* @var $this StatMapMappingController */
/* @var $type int */

$this->breadcrumbs=array(
    'Stat Map Mappings',
);

$this->menu=array(
    array('label'=>'List of mappings', 'url'=>array('index', 'type' => $type)),
);
?>

    <h1><?= StatMapMapping::getTypeName($type);?>: New Stat Map Mapping</h1>

<?php
$this->widget('FlashPrinter');
?>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'user-group-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));
?>

    <table class="table table-striped table-bordered">
        <tr>
            <th>Map</th>
        </tr>
        <tr>
            <td style="text-align: center !important;">
                <?php
                echo CHtml::dropDownList('Map[id]', '', StatMap::mapNameList());
                ?>
            </td>
        </tr>
        <tr>
            <th>Stats</th>
        </tr>
        <tr>
            <td style="text-align: center !important;">
                    <?php
                    echo CHtml::dropDownList('Map[stats]', '',CHtml::listData(StatMapMapping::listStatsNotMapped($type),'id', 'nameWithId'),['multiple' => true, 'style' => 'height: 350px; width: 100%;', 'id' => 'listOFStats']);
                    ?>
            </td>
        </tr>
        <tr>
            <th>Action</th>
        </tr>
        <tr>
            <td style="text-align: center !important;">
                <?php
                echo CHtml::submitButton('Map', ['class' => 'btn btn-success']);
                ?>
            </td>
        </tr>
    </table>
<br/>
<br/>
<div class="text-center">Last item selected:<br/>
<input type="text" id="lastSelect" style="width: 100%;"/></div>
<?php
$this->endWidget();

$script = "$('option').each(function(i,v)
    {
       $(this).attr('title', $(this).text())
    });";

Yii::app()->clientScript->registerScript('dropdownTitle', $script, CClientScript::POS_READY);
?>
<script>
    $('#listOFStats').on('click', function(){
              $('#lastSelect').val($("option:selected:last",this).text());
    });
</script>
