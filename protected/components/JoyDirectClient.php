<?php

class JoyDirectClient extends GlobalOperator
{

    const DEV = GLOBAL_CONFIG::JOY_TEST;
    const ID = GLOBAL_CONFIG::JOY_ID;
    const SECRET = GLOBAL_CONFIG::JOY_SECRET;

    const ID_D = 'bcbca90ca459cb5c6e3b9c6faacf0e7b';
    const SECRET_D = 'xAuw4R7oHDCrcRc5bf8a8789qd22en1g6tz7nlpp';

    protected $app_id = GLOBAL_CONFIG::JOY_ID;
    protected $app_secret = GLOBAL_CONFIG::JOY_SECRET;

    protected $label_annotation_text = false;

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    protected static function isAltCredentials($operator_uniq_id)
    {
        switch($operator_uniq_id)
        {
            case GLOBAL_BROKERS::OPERATOR_UNIQID_PETERSK_DIRECT_DPD_CZ_SK_D:
            case GLOBAL_BROKERS::OPERATOR_UNIQID_PETERSK_DIRECT_DHL_PARCEL_D:
                return true;
                break;
        }

        return false;
    }


    protected function operator2CustomCredentials($operator_uniq_id)
    {
        if(self::isAltCredentials($operator_uniq_id))
        {
            $this->app_id = self::ID_D;
            $this->app_secret = self::SECRET_D;
        }
    }

    protected static function operatorUniqidToServiceCode($operator_uniq_id)
    {
        switch($operator_uniq_id)
        {
            case GLOBAL_BROKERS::OPERATOR_UNIQID_PETERSK_DIRECT_DPD_CZ_SK_D:
                return 'dpd_cz';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_PETERSK_DIRECT_PPL_CZ:
                return 'pplcz';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_PETERSK_DIRECT_DPD_AT:
                return 'dpd_at';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_PETERSK_DIRECT_DHL_PARCEL:
            case GLOBAL_BROKERS::OPERATOR_UNIQID_PETERSK_DIRECT_DHL_PARCEL_D:
                return 'dhl_parcel';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_PETERSK_DIRECT_FAN_RO:
                return 'fan_ro';
                break;
        }
    }

    /**
     * @param $track_id string Track ID for this operator is our local package ID
     * @return array|bool
     */
    public static function track($track_id, CourierLabelNew $courierLabelNew)
    {
        $altCredentials = false;
        if($courierLabelNew->courier->getType() == Courier::TYPE_U)
        {
            if(self::isAltCredentials($courierLabelNew->courier->courierTypeU->courierUOperator->uniq_id))
                $altCredentials = true;
        }
        else if($courierLabelNew->courier->getType() == Courier::TYPE_INTERNAL) {
            if ($courierLabelNew->_internal_mode == CourierLabelNew::INTERNAL_MODE_FIRST) {
                if (self::isAltCredentials($courierLabelNew->courier->courierTypeInternal->pickupOperator->uniq_id))
                    $altCredentials = true;
            } else {
                if (self::isAltCredentials($courierLabelNew->courier->courierTypeInternal->deliveryOperator->uniq_id))
                    $altCredentials = true;
            }
        } else
            throw new Exception('Unknown KAAB_SK Tracking Account!');

        if($altCredentials)
            $jc = new JoyClass(self::ID_D, self::SECRET_D, self::DEV);
        else
            $jc = new JoyClass(self::ID, self::SECRET, self::DEV);


        try
        {
            // local_id is used for tracking
            $resp = $jc->getTrackingDirect($courierLabelNew->courier->local_id);
//            $resp = $jc->getTrackingDirect($track_id);

            $statuses = new GlobalOperatorTtResponseCollection();

            if($resp->_embedded->trackingEvents)
            {
                foreach($resp->_embedded->trackingEvents AS $item)
                {
                    $location = NULL;
                    $name = $item->message;
                    if(preg_match('/Zásielka doručená. Prijal (.*)/iu', $name, $matches)) {
                        $name = 'Zásielka doručená';
                        $location = 'Prijal '.trim($matches[1]);
                    } else if(preg_match('/Zásilka doručena. Převzal (.*)/iu', $name, $matches)) {
                        $name = 'Zásielka doručená';
                        $location = 'Převzal '.trim($matches[1]);
                    } else if(preg_match('/The shipment was picked up by FAN Courier (.*). \(Fan Ro\)/i', $name, $matches)) {
                        $name = 'The shipment was picked up by FAN Courier. (Fan Ro)';
                        $location = '';
                    } else if(preg_match('/The shipment left the FAN Courier hub (.*) to the destination hub on (.*). \(Fan Ro\)/i', $name, $matches)) {
                        $name = 'The shipment left the FAN Courier hub to the destination hub. (Fan Ro)';
                        $location = trim($matches[1]);
                    } else if(preg_match('/The latest status of the shipment is : (.*) in data (.*). \(Fan Ro\)/iu', $name, $matches)) {
                        $name = trim($matches[1]).' (Fan Ro)';
                        $location = '';
                    } else if(preg_match('/The latest status of the shipment is : delivery in process in data (.*). \(Fan Ro\)/i', $name, $matches)) {
                        $name = 'The latest status of the shipment is : delivery in process. (Fan Ro)';
                        $location = '';
                    } else if(preg_match('/The latest status of the shipment is : recipient notified in data (.*). \(Fan Ro\)/i', $name, $matches)) {
                        $name = 'The latest status of the shipment is : recipient notified. (Fan Ro)';
                        $location = '';
                    } else if(preg_match('/The latest status of the shipment is : delivered in data (.*). \(Fan Ro\)/i', $name, $matches)) {
                        $name = 'The latest status of the shipment is : delivered. (Fan Ro)';
                        $location = '';
                    } else if(preg_match('/The shipment was picked up for delivery by the courier from (.*) on (.*). \(Fan Ro\)/i', $name, $matches)) {
                        $name = 'The shipment was picked up for delivery by the courier. (Fan Ro)';
                        $location = trim($matches[1]);
                    } else if(preg_match('/The latest status of the shipment is : return in data (.*) \(Fan Ro\)/i', $name, $matches)) {
                        $name = 'The latest status of the shipment is : return (Fan Ro)';
                        $location = '';
                    }

                    $statuses->addItem($name, $item->date, $location);
                }
            }


            return $statuses;

        }
        catch(JoiApyException $ex)
        {
//            Yii::log('JoiApiException:'.$ex->getMessage().' ; TRACK ; '.print_r($track_id,1), CLogger::LEVEL_ERROR);
            return false;
        }

    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
//        if($courierInternal->courier->user_id != User::INTEGRATIONS_TEST_USER_ID)
//            if ($returnErrors == true) {
//                return GlobalOperatorResponse::createErrorResponse('Not active!');
//            } else {
//                return false;
//            }


        if ($to->name == $to->company)
            $name = $to->name;
        else
            $name = $to->getUsefulName(true);

        $cod = 0;
        if ($courierInternal->courier->cod_value > 0)
            $cod = $courierInternal->courier->cod_value;

        $parcelService = self::operatorUniqidToServiceCode($uniq_id);
        $data = [
            'id' =>  $courierInternal->courier->local_id,
            'parcelService' => $parcelService,
            'name' => $name,
            'street' => trim($to->address_line_1 . ' ' . $to->address_line_2),
            'city' => $to->city,
            'zip' => preg_replace("/[^0-9a-z]/i", "", $to->zip_code),
            'country' => strtolower($to->country0->code),
            'email' => $to->fetchEmailAddress(true),
            'phone' => str_pad($to->tel, 6, "0", STR_PAD_LEFT),
            'variableSymbol' =>  mb_substr($courierInternal->courier->local_id, -10),
            'cod' => $cod,
            'note' =>  'SP#' . $courierInternal->courier->local_id,
            'numberOfPackages' =>  1,
            'weight' =>  [  $courierInternal->courier->getWeight() ],
        ];

        $ref = $courierInternal->courier->ref;
        $content = $courierInternal->courier->package_content;

        $ref = $courierInternal->courier->user->login.' / '.$ref;

        $model = new self;
        $model->operator2CustomCredentials($uniq_id);

        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id);

        return $model->processOrder($data, $ref, $content, $parcelService, $returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
//        if($courierU->courier->user_id != User::INTEGRATIONS_TEST_USER_ID)
//            if ($returnErrors == true) {
//                return GlobalOperatorResponse::createErrorResponse('Not active!');
//            } else {
//                return false;
//            }


        $to = $courierU->courier->receiverAddressData;

        if($to->name == $to->company)
            $name = $to->name;
        else
            $name = $to->getUsefulName(true);

        $cod = 0;
        if($courierU->courier->cod_value > 0)
            $cod = $courierU->courier->cod_value;


        $parcelService = self::operatorUniqidToServiceCode($courierU->courierUOperator->uniq_id);
        $data = [
            'id' =>  $courierU->courier->local_id,
            'parcelService' => $parcelService,
            'name' => $name,
            'street' => trim($to->address_line_1 . ' ' . $to->address_line_2),
            'city' => $to->city,
            'zip' => preg_replace("/[^0-9a-z]/i", "", $to->zip_code),
            'country' => strtolower($to->country0->code),
            'email' => $to->fetchEmailAddress(true),
            'phone' => str_pad($to->tel, 6, "0", STR_PAD_LEFT),
            'variableSymbol' =>  mb_substr($courierU->courier->local_id, -10),
            'cod' => $cod,
            'note' =>  'SP#' . $courierU->courier->local_id,
            'numberOfPackages' =>  1,
            'weight' =>  [  $courierU->courier->getWeight() ],
        ];

        $ref = $courierU->courier->ref;
        $content = $courierU->courier->package_content;

        $ref = $courierU->courier->user->login.' / '.$ref;

        $model = new self;
        $model->operator2CustomCredentials($courierU->courierUOperator->uniq_id);

        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($courierU->courierUOperator->uniq_id);

        return $model->processOrder($data, $ref, $content, $parcelService, $returnErrors);
    }

    protected function processOrder($data, $ref, $content, $parcelService, $returnErrors)
    {
        $jc = new JoyClass($this->app_id, $this->app_secret, self::DEV);

        $return = [];
        try {
            $resp = $jc->createPackageNew($data);

            $ttNumber = $resp->packages[0]->tracking;
            $label = $resp->label;

            $label = base64_decode($label);

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImageBlob($label);
            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $im->stripImage();
            $im->trimImage(0);

            $baseHeight = $im->getImageHeight();
            $baseWidth = $im->getImageWidth();
            $im->extentImage($im->getImageWidth(),$baseHeight+100,0 ,0);

            $posY = $baseHeight + 80;

            if($parcelService == 'dpd_at' OR $parcelService == 'dpd_cz')
                $posY = $baseHeight + 40;

            LabelAnnotation::createAnnotation('Ref: '.$ref."\r\n"."Content: ".$content, $im, 80, $posY, false, false, 8);

            if($this->label_annotation_text)
                LabelAnnotation::createAnnotation($this->label_annotation_text, $im, $baseWidth*0.80, $posY + 30, false, false, 10, false);
            else
                LabelAnnotation::createAnnotation('P', $im, $baseWidth*0.85, $posY + 40, false, false, 22, true);

            if($parcelService == 'dhl_parcel') {
                $logo = new Imagick();
                $logo->readImageBlob(self::getDhlLogo());
                $im->extentImage($im->getImageWidth(), $baseHeight + 200, 0, -100);
                $im->compositeImage($logo, Imagick::COMPOSITE_DEFAULT, $im->getImageWidth() - 700, 10);
            }

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);

        } catch (JoiApyException $ex) {
//            Yii::log('JoiApiException:' . $ex->getMessage() . ' ; ORDER FOR COURIER INTERNAL ; ' . print_r($courierU->courier->id, 1), CLogger::LEVEL_ERROR);

            if ($returnErrors == true) {
                return GlobalOperatorResponse::createErrorResponse($ex->getMessage());
            } else {
                return false;
            }

        }
        return $return;
    }

    protected static function getDhlLogo()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAAlgAAABYCAMAAAAeJ3nHAAAA4VBMVEX///////8AAAD///9aWlqAgIChoaE4ODisrKyZmZmCgoLIyMjS0tLGxsYBAQG3t7cMDAz9/f0ZGRnc3NwHBwf19fXu7u52dnYgICADAwMsLCz5+flMTExRUVHe3t49PT0zMzPy8vLg4OBwcHDk5OTBwcF6enro6OiwsLCVlZWSkpJmZmZXV1e6urqrq6t+fn5ra2skJCQVFRXw8PDr6+vX19e0tLSPj49hYWFdXV1BQUH39/fMzMyoqKiHh4eLi4tUVFRGRka+vr6fn58RERHPz8/Dw8OlpaU5OTnR0dGbm5sh7dH2AAAAAnRSTlPv4A9/wGMAAAk5SURBVHja7NzrUhoxGMZx+zzUYoHlDMtipayogIdiEVs5eKoH9P4vqDPth3bSUNm3IQlOfxew48h/dt5Jstl4s/Hff8a92aA9e982rZqdPObYJOXONgV/8rfekHpD0eO2zqg6F/4ne8e0ZcNeWVk48DEa97oHFPoKiZsJ9VKQGA+pSENqRks27IVVjeDK5+3nmAInkDitUa8HibBGxU4BQpe0w2pYh3CpclhmUgcVSHSpl4HIlIpiBKFtWmIzrDwcCwZZJlJ8B4l96jVDSGxRNYBQYUhLLIbV+QznKpcTJrALiVbR6OMuqMr7P2DRYlgt+GCe5dLOIVE4oN4xJMIqFdUQQl9pjb2w3sMPQZpLiiuQOKdex9S89gFCUZPWWAvrNoAv+lzODSTq1Ju0DD3uCkKVEe2xFVa7BH9ccRl1SLwrGn3cmKqHtRiwaCusMXxyzpdlIFHZoV4eEndnVLRDCB3SJkthfYJXGh2+pFYymmxcgESeqiMIRVXaZCMsD1awkq8TpiCR4gKmBqw+hIIRrbISVjWEb875d1uQCNvU24XER6pykErTLithHcI7LyxBZwNITKn3AIlCh4pOCUJHtMxGWJvw0HsuJB6wvlCvXTK0gnUIoXBIyyyEdQ8fRUUudmT2rXBkaF5LQ+qWtq0+rGEEL2W5UA8SpxPq9Q3Na3EDQle0btVheTlg/fCWetIBK3ii3r3scet5VuaXVYc1g6euucipDwNWn6oBhEpt2rfqsEb+bBEq5lXqDSDR4gLXhs7KzCAU5OjAisNqnsJXhTK1ZtKlAb2eqQGrslYDFlcc1lv4K0OdjnTvRe9JNmBlqHoHoTGdWG1Ye/DYlBrivRe9YmhoXqtDakQnVhpWrQCP5aixD4l5k3q7hhbE0pB6pBurDKvm11mZZY69pSER7FCvC4lCm4paCUK7dGSVYbVvn7J2PLaQWDTkHzoNowNWXDC0QX4NoWhCR36Gtf5SSGq7SIVwwAr6XGBs6CXTg1CQpSuvJawrA1/rCY8jlvLHeZ1jU/PaNIDQFp15LWGNkVRM1QgiAUwKYirknwsM6M4rCStvYEen7cVxxDxV2xCK6NDrCKtcQUJBmaoUPFCnqrcmX3spHIa1l0vmIcdFWgaWIJ/hgXmRikwAoT5dchfWJRIb31Nry8Bp8qwPu+WNmAr55wItOuUsrD1zn8ZNG0ioUaZi4sVxxBOqPkAoPKBTrsKqnRo7qD4JkdQmVRfwwBFVM0jl6JarsFIQuIup8cHIEqQHwiYVO+t1GPl3jsI6MfcxYNfAgJXxYcDSHMibQ+iG7rkIq9wwNGFJbuOsdKgoenF11xeq9iF016F7DsJqRubW+24MXLqyDw9sU9WF1B494CCsAQQaI2rU//lWO09ulgirVJQD+bvPB/bDOoHEMzW6SKoUUxE34F6QpaLaWtOtnJ+shyX9IS+ocVD55wPJntyN2qeqvj73yujYD6saQeDz9/buRCttIAoDcHt/rbhBwhJEEoqlLMpWEEWqouDW9v0fqKeWtD1DsHq5mRlbvgeYw/KfZDKZuddFhJbIH2iBCVQN4rqBHXQHq0kMySkiDAX2NJySBQolKLzs69wr85vuYCXkasPcFeiF0nUo6lac9+hA5RNTGbbQG6xcVuiFKqvPwz1UZbJADSr+XpkMbKE3WC1icD4gQmrpBkWW1Ead72RxV3iVm5H/pDlYQ+JoYx7jRVoAVYMsMH+R4S+AjGAPncG6Fy049KFZ3ni+/ckACrfZ2zBuvwPVMM0cKijCHhqD5aXt2rHmwk42xYNLa7DWiKHwFSuvkb5gfbO8JeiKII3Buk8SQ//fuC/8h3QF6zhNHH2svE66gvWJeM6MnwpY4XgMVr0SrMUg8DMInRKXs/Rnez+GamftmvF9Ag/Rdq4Zw/mjHBRuPlgzZFIpYsZdOg6tYPMxWJkkxcI/xszHLBnUhoJZG7XiIgK7ln3ehSJP5nRczFRoef5jsEppkqWW1Hc3yKAOVDXZM6BnQpuRU2TOFUIJEnAWZ7B2lV/ekBpUbdE6y7ghjr6HGRsu6wFCx1nbgzVEqEMG+VB9vBW58IUyxHIPRbFMcvitXH2SC1amQPKuEco4ZE7Zg8L9LLqDrvhZ6Ehpl8yp/n58IBHBY7C8o+6WsO6Wh9AXMiedgyovuYOOXWgZqoskieGn/HyrJvL3NzSsY1XInP5XqFKyrSdSQk3Jcz1amlVdnGIP1j2Z4+xA9bEv2vp2cEscl1CNyBjnBOLiD5aXJmOcC8wRmWDJD5egpVnWLzruYAXEJ38fxJVEIZgQe77mQiHx+GRZk4EfwTp+ONoUdLSZQ+iIjCnvQMHuSllFtFPWcNk6VGMyplzEzODwUCwC1Vhe6ZwVMXNBxpRzmJPbEy3neSwzX+MtWcifaWxRSGq5odQnSb1fufJuyZTrA8xrMn+kBXypW89dgaTwU/5AgkbKyrvwQ0aLTBkjwjfiSHuItkkcvRzmtMmUTwgpnX9kVt5vSVDD/ATL6SBCJilabWqb2+TSomDt/0p5rkfiVyzZdy5NhE7IkEkdEc577OP9gsMdwaZg7WDGHZGo1mOwDlIPCSGXjRxC+2RE4RCRmrKr0meCzfOrZMYQoWLiISGn83AR4zpWhYzwLxDpHXHsDRDtQbL8+mCPTAgQn/iClSATsjeI5qVFJ1hfk8Rxj2g1MqDvITbxBavkkAGVEhaYMCeM0Q56sock67ekXxvxiS1Y7hfSLtmaYpGuxCJrSGCCpaqSdluI02Owzu+q2wKmUxehPOnmjLaxUEOmaVIoRRx7HhZLkGY+Qrnq7ras3WpJaLlBPXbSIc2cWh2LsDcjnyKa5xDHFE+Z9kmnvQOEApLX+hmsPknwi5jJZEmnZOvGw1MmsntlfJHhVCcj0ieZQmhIf7LwlU7/AKEy6ZOeHO7gaWPZCVY+rvLrbT9JmnQR2iWVbad0qghdkSbp8rBxgL85JY5kBlHYh8dKeIaTw/dpYuCnfOBQHEZyp3S2GFNbNscplMfj1Dme4YJYLhHtjlg6eKbc5VWQdZwkCYtuVOVtkErylM6wtr6k2thF6N2n9Vh1a+1BfeDimU4r3fUXyx9igXaTM9w3vMSgPpjWuusx6earCGXytXV53XwD/0AT+xUrvXn7ZmVF3NvvyBWoZlEe2m8AAAAASUVORK5CYII=';

        return base64_decode($data);
    }


}