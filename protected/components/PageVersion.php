<?php

class PageVersion extends CApplicationComponent {

    const PAGEVERSION_DEFAULT = 0;
    const PAGEVERSION_RS = 1;
    const PAGEVERSION_PLI = 10;
    const PAGEVERSION_RUCH = 20;
    const PAGEVERSION_QURIERS = 30;
    const PAGEVERSION_ORANGE_POST = 40;
    const PAGEVERSION_CQL = 2;
    const PAGEVERSION_DE = 3;
    const PAGEVERSION_UA = 4;
    const PAGEVERSION_EN = 5;

    protected static $_pv;

    protected static $_pageVersions = [
        self::PAGEVERSION_DEFAULT,
        self::PAGEVERSION_RS,
        self::PAGEVERSION_RUCH,
        self::PAGEVERSION_PLI,
        self::PAGEVERSION_QURIERS,
        self::PAGEVERSION_ORANGE_POST,
        self::PAGEVERSION_CQL,
        self::PAGEVERSION_DE,
        self::PAGEVERSION_UA,
        self::PAGEVERSION_EN,
    ];


    public static function pageVersionToLang($pageVersion)
    {
        switch($pageVersion)
        {
            default:
            case self::PAGEVERSION_DEFAULT:
                return 'pl';
                break;
            case self::PAGEVERSION_RS:
                return 'en_rs';
                break;
            case self::PAGEVERSION_RUCH:
                return 'pl_ruch';
                break;
            case self::PAGEVERSION_PLI:
                return 'en_gb';
                break;
            case self::PAGEVERSION_QURIERS:
                return 'pl_quriers';
                break;
            case self::PAGEVERSION_CQL:
                return 'en_cql';
                break;
            case self::PAGEVERSION_DE:
                return 'de';
                break;
            case self::PAGEVERSION_UA:
                return 'uk';
                break;
            case self::PAGEVERSION_EN:
                return 'en';
                break;
            case self::PAGEVERSION_ORANGE_POST:
                return 'en_orangep';
                break;
        }
    }

    protected static $_domainToPageVersion = [
        'swiatprzesylek.pl' => self::PAGEVERSION_DEFAULT,
        'royalshipments.com' => self::PAGEVERSION_RS,
        'pli-uk24.co.uk' => self::PAGEVERSION_PLI,
        'pwri.pl' => self::PAGEVERSION_RUCH,
        'listy.quriers.pl' => self::PAGEVERSION_QURIERS,
        'cql.global' => self::PAGEVERSION_CQL,
//        'testde.swiatprzesylek.pl' => self::PAGEVERSION_DE,
//        'testua.swiatprzesylek.pl' => self::PAGEVERSION_UA,
        'portal.orange-post.nl' => self::PAGEVERSION_ORANGE_POST,
    ];

    public static function getDomainsList($exceptMain = false)
    {
        $list = array_keys(self::$_domainToPageVersion);

        if($exceptMain)
            array_shift($list);

        return $list;
    }

    public static function pv2Domain($id)
    {
        $data = array_flip(self::$_domainToPageVersion);

        return isset($data[$id]) ? $data[$id] : NULL;
    }

    public static function getDomainsUrlList()
    {
        $data = [
            'pl' => 'swiatprzesylek.pl',
            'en' => 'swiatprzesylek.pl',
            'de' => 'swiatprzesylek.pl',
            'uk' => 'swiatprzesylek.pl', // ukrainin
            'en_rs' => 'royalshipments.com',
            'en_gb' => 'pli-uk24.co.uk',
            'pl_ruch' => 'pwri.pl',
            'pl_quriers' => 'listy.quriers.pl',
            'en_cql' => 'cql.global',
            'en_orangep' => 'portal.orange-post.nl',
        ];

        return $data;
    }

    public static function urlDomainReplace($url, $lang, $forceHttps = false)
    {
        $url = str_replace(self::getDomainsUrlList(), self::lang2domain($lang, false), $url);

        if($forceHttps)
            $url = str_replace('http://', 'https://', $url);

        return $url;
    }

    public static function lang2domain($lang, $withHttps = true)
    {
        $data = self::getDomainsUrlList();

        $domain = isset($data[$lang]) ? $data[$lang] : $data['en'];

        if($withHttps)
            $domain = 'https://'.$domain;

        return $domain;
    }

    public function pageVersionToDomain($pageVersion)
    {
        if(!YII_LOCAL)
            foreach(self::$_domainToPageVersion AS $key => $item)
            {
                if($item == $pageVersion)
                    return $key;
            }
        return NULL;
    }

    public static function pageVersionList()
    {
        return [
            self::PAGEVERSION_DEFAULT => 'ŚP',
            self::PAGEVERSION_RS => 'RS',
            self::PAGEVERSION_PLI => 'PLI',
            self::PAGEVERSION_RUCH => 'RUCH',
            self::PAGEVERSION_QURIERS => 'QURIERS',
            self::PAGEVERSION_CQL => 'CQL',
            self::PAGEVERSION_ORANGE_POST => 'ORANGE_POST',
            self::PAGEVERSION_DE => 'ŚP_DE',
            self::PAGEVERSION_EN => 'ŚP_EN',
            self::PAGEVERSION_UA => 'ŚP_UA',
        ];
    }

    protected static function _domainToPageVersion($domain)
    {
        if(isset(self::$_domainToPageVersion[$domain]))
            return self::$_domainToPageVersion[$domain];
        else
            return self::PAGEVERSION_DEFAULT;
    }

    public function init()
    {
        parent::init();

        if(Yii::app()->params['frontend'] == true) {

            $hostname = strtolower($_SERVER['SERVER_NAME']);
            $hostname = str_replace('www.', '', $hostname);

            $pageVersion = self::_domainToPageVersion($hostname);

            self::_setPageVersion($pageVersion);
        }
    }

    protected static function _setPageVersion($pageVersion)
    {
        $app = Yii::app();

        switch($pageVersion)
        {
            case self::PAGEVERSION_RS:
                $domain = 'royalshipments.com';
                Yii::app()->name = 'Royal Shipments';
                break;
            case self::PAGEVERSION_PLI:
                $domain = 'pli-uk24.co.uk';
                Yii::app()->name = 'PLI';
                break;
            case self::PAGEVERSION_RUCH:
                $domain = 'paczkawruchu-zagranica.pl';
                Yii::app()->name = 'PWRI';
                break;
            case self::PAGEVERSION_QURIERS:
                $domain = 'listy.quriers.pl';
                Yii::app()->name = 'QURIERS';
                break;
            case self::PAGEVERSION_ORANGE_POST:
                $domain = 'portal.orange-post.nl';
                Yii::app()->name = 'ORANGE POST';
                break;
            case self::PAGEVERSION_CQL:
                $domain = 'cql.global';
                Yii::app()->name = 'CQL';
                break;
            case self::PAGEVERSION_DE:
            case self::PAGEVERSION_UA:
            case self::PAGEVERSION_EN:
                $domain = 'swiatprzesylek.pl';
                Yii::app()->name = 'Swiat Przesylek';
                break;
            case self::PAGEVERSION_DEFAULT:
            default:
                $domain = 'swiatprzesylek.pl';
                Yii::app()->name = 'Świat Przesyłek';
                break;
        }

        $_lang = self::pageVersionToLang($pageVersion);

        Yii::app()->params['langCode'] = $_lang;
        Yii::app()->params['domain'] = $domain;

        $app->language = Language::autoLanguageCodeReturn($_lang);

        Yii::app()->params['language'] = Language::autoLanguageIdReturn($app->language);

        if(in_array($pageVersion, self::$_pageVersions))
            self::$_pv = $pageVersion;
        else
            self::$_pv = self::PAGEVERSION_DEFAULT;

    }

    public function set($pageVersion)
    {
        if(Yii::app()->params['frontend'] !== true)
            throw new CHttpException('Method allowed only in frontend!');

        self::_setPageVersion($pageVersion);
    }

    public function get()
    {
        if(Yii::app()->params['frontend'] !== true)
            return self::PAGEVERSION_DEFAULT;

        if(self::$_pv === NULL)
            return self::PAGEVERSION_DEFAULT;

        return self::$_pv;
    }

    /**
     * Extracts just first part of iso code. Used because of different page versions with same language
     * @return array|mixed|string
     */
    public function getLangIsoCode()
    {
        if(Yii::app()->params['frontend'] !== true)
            throw new CHttpException('Method allowed only in frontend!');

        $lang = Yii::app()->language;
        $lang = explode('_', $lang);
        $lang = $lang[0];

        return $lang;
    }

    /**
     * Checks if user is allowed to login to this PageVersion based on user's source domain
     * @param $source_domain
     * @return bool
     * @throws CHttpException
     */
    public function isLoginAllowed($source_domain, $user_id = false)
    {
        if($source_domain == self::PAGEVERSION_RUCH) {
            if ($this->get() != $source_domain)
                return false;
        }
        else if($source_domain == self::PAGEVERSION_PLI)
        {
            if($this->get() != $source_domain)
                return false;
        }
        else if($source_domain == self::PAGEVERSION_QURIERS)
        {
            if($this->get() != $source_domain)
                return false;
        }
        else if($source_domain == self::PAGEVERSION_ORANGE_POST)
        {
            if($this->get() != $source_domain)
                return false;
        }
        else if($source_domain == self::PAGEVERSION_CQL)
        {
            if($this->get() != $source_domain)
                return false;
        }
        else if(in_array($this->get(), [self::PAGEVERSION_RUCH, self::PAGEVERSION_PLI, self::PAGEVERSION_QURIERS, self::PAGEVERSION_CQL]))
            return false;

        return true;
    }

}