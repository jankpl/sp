<div class="form">
    <?php

    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
        array(
            'enableAjaxValidation' => false,
            'htmlOptions' =>
                array('enctype' => 'multipart/form-data'),
        )
    );
    ?>

    <h2><?php echo Yii::t('courier','Import packages from file');?></h2>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <?php

    echo $form->errorSummary($model);
    ?>

    <table class="table table-striped" style="margin: 10px auto;">
        <tr>
            <td><?php echo $form->labelEx($model,'file'); ?></td>
            <td><?php echo $form->fileField($model, 'file'); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->labelEx($model,'split'); ?></td>
            <td><?php echo $form->dropDownList($model, 'split', F_UploadFile::$_splitArray); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->checkBox($model, 'omitFirst'); ?></td>
            <td><?php echo $form->labelEx($model,'omitFirst', array('style' => 'display: inline;')); ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><?php echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('class' => 'btn btn-lg btn-primary'));?></td>
        </tr>
    </table>

    <?php
    $this->endWidget();
    ?>

    <div class="info">
        <div class="alert alert-info">
            <p><?php echo Yii::t('courier','Allowed file type:');?> .csv,.xls,.xlsx.</p>
            <br/>
            <p><?php echo Yii::t('courier','The file must contain following columns:');?><br/>
                [package_weight],
                [package_size_l],
                [package_size_w],
                [package_size_d],
                [packages_number],
                [package_value],
                [package_content],
                [sender_name],
                [sender_company],
                [sender_country],
                [sender_zip_code],
                [sender_city],
                [sender_address_line_1],
                [sender_address_line_2],
                [sender_tel],
                [receiver_name],
                [receiver_company],
                [receiver_country],
                [receiver_zip_code],
                [receiver_city],
                [receiver_address_line_1],
                [receiver_address_line_2],
                [receiver_tel]
            </p>
            <br/>
            <?php echo CHtml::link(Yii::t('courier','Download file template'), Yii::app()->baseUrl.'/misc/ipff_template.xlsx', array('target' => '_blank', 'class' => 'btn btn-success', 'download' => 'ipff_template.xlsx'));?>
        </div>
    </div>
</div>

<br/>
<br/>