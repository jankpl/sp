<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-addition-list-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'country_list_id'); ?>
        <?php echo $form->dropDownList($model, 'country_list_id', CHtml::listData(CountryList::model()->findAll(),'id','name')); ?>
        <?php echo $form->error($model,'country_list_id'); ?>
    </div><!-- row -->

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, 'If zip code contains any letters, please fill field "Zip txt". Range from-to will NOT work for zip codes with letters.', array('closeText' => false)); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'zip_from'); ?>
        <?php echo $form->textField($model, 'zip_from', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'zip_from'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'zip_to'); ?>
        <?php echo $form->textField($model, 'zip_to', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'zip_to'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'zip_txt'); ?>
        <?php echo $form->textField($model, 'zip_txt', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'zip_txt'); ?>
    </div><!-- row -->

    <div class="row">
        <?php echo $form->labelEx($model,'price_group'); ?>
        <?php echo $form->dropDownList($model, 'country_list_id', CourierDhlRemoteAreas::getPricesGroups()); ?>
        <?php echo $form->error($model,'price_group'); ?>
    </div><!-- row -->

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, 'Price provided will replace default group\'s price.', array('closeText' => false)); ?>

    <strong>Price:</strong>

    <div class="row">
        <?php $this->widget('PriceForm', array(
            'form' => $form,
            'model' => $model->price,
            'formFieldPrefix' => '',
        ));?>
    </div>


    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->