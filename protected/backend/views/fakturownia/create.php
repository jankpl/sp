<?php
/* @var $model FakturowniaForm */


$this->widget('CheckViesWidget');
?>

<h2>Create new invoice via Fakturownia.pl</h2>


<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php
$this->widget('backend.components.regon.RegonWidget', [
    'formAttrNames' => 'AddressData_Order',
    'button' => false,
    'attrMap' => ['nip' => '_nip']
]);
?>

<div class="form" style="position: relative;">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id'=>'fakturownia-form',
        'htmlOptions' => [
            'enctype' => 'multipart/form-data',
        ]
    )); ?>
    <?php echo $form->errorSummary($model); ?>

    <?php
    if(!$model->correction):
        ?>
        <div class="text-center">
            <?php echo TbHtml::submitButton('Switch to Accountant invoice', ['class' => 'btn btn-lg', 'name' => 'switch']); ?>
        </div>
    <?php
    endif;
    ?>
    <br/>
    <br/>
    <div style="overflow: auto;">
        <div style="width: 49%; float: left;">

            <div class="row">
                <?php echo $form->labelEx($model, 'saveToUserInvoice'); ?>
                <?php echo $form->dropDownList($model, 'saveToUserInvoice', CHtml::listData(User::model()->findAll(),'id','login'), ['prompt' => '- no -', 'data-user-save' => true, 'data-chosen-widget' => true]); ?> <input type="button" value="Fill user data &gt;&gt;" id="paste-user-data" style="display: none;"/>
                <?php echo $form->error($model,'saveToUserInvoice'); ?>
            </div>

            <div class="row"  style="display: none;" data-requires-user-save="true">
                <?php echo $form->labelEx($model, 'userInvoiceDescription'); ?>
                <?php echo $form->textField($model, 'userInvoiceDescription'); ?>
                <?php echo $form->error($model,'userInvoiceDescription'); ?>
            </div>

            <div class="row"  style="display: none;" data-requires-user-save="true">
                <?php echo $form->labelEx($model, 'userInvoicePublish'); ?>
                <?php echo $form->checkBox($model, 'userInvoicePublish'); ?>
                <?php echo $form->error($model,'userInvoicePublish'); ?>
            </div>

            <div class="row"  style="display: none;" data-requires-user-save="true">
                <?php echo $form->labelEx($model, 'file_sheet'); ?>
                <?php echo $form->fileField($model, 'file_sheet'); ?>
                <?php echo $form->error($model,'file_sheet'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'sellDate'); ?>
                <?php echo $form->textField($model, 'sellDate', ['date-picker' => true, 'id' => 'sell-date']); ?>
                <?php echo $form->error($model,'sellDate'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'issueDate'); ?>
                <?php echo $form->textField($model, 'issueDate', ['date-picker' => true]); ?>
                <?php echo $form->error($model,'issueDate'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'paymentToDate'); ?>
                <?php echo $form->textField($model, 'paymentToDate', ['date-picker' => true]); ?> <input type="button" value="+7days" data-plusdays="7"/> <input type="button" value="+14days" data-plusdays="14"/>
                <?php echo $form->error($model,'paymentToDate'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'paidDate'); ?>
                <?php echo $form->textField($model, 'paidDate', ['date-picker' => true]); ?> <input type="button" value="issue date" data-plusdays="0"/>
                <?php echo $form->error($model,'paidDate'); ?>
                <br/><i>Leave empty if not paid</i>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textField($model, 'description', ['id' => 'inv_desc']); ?>
                <?php echo $form->error($model, 'description'); ?><?php echo TbHtml::button('&lt;&lt; Fill with template:', ['class' => 'btn btn-sm', 'id' => 'fill_desc']); ?><br/>
                <input type="text" id="source_fv_no" placeholder="Corrected inv no" style="width: 120px;"/> <input type="text" id="source_fv_date" placeholder="Corrected inv date" style="width: 120px;"/> <input type="text" id="source_compaint_no" placeholder="Complaint no" style="width: 120px;"/>

            </div><!-- row -->



            <div class="row">
                <?php echo $form->labelEx($model, 'number'); ?>
                <?php echo $form->textField($model, 'number');?>
                <?php echo $form->error($model,'number'); ?>
                <br/><i>If empty, number will be assigned by Fakturownia</i>
            </div>


        </div>
        <div style="width: 49%; float: right;">
            <?php
            $this->widget('backend.components.regon.RegonWidget', [
                'button' => true,
            ]);
            ?>


            <div class="row" data-inv-company-field="true">
                <?php echo $form->labelEx($model->addressData_receiver,'company'); ?>
                <?php echo $form->textField($model->addressData_receiver,'company', array('maxlength' => 90)); ?>
                <?php echo $form->error($model->addressData_receiver,'company'); ?>
            </div><!-- row -->

            <div class="row" data-inv-person-field="true">
                <?php echo $form->labelEx($model->addressData_receiver,'name'); ?>
                <?php echo $form->textField($model->addressData_receiver,'name', array('maxlength' => 90)); ?>
                <?php echo $form->error($model->addressData_receiver,'name'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->addressData_receiver,'address_line_1'); ?>
                <?php echo $form->textField($model->addressData_receiver,'address_line_1', array('maxlength' => 90)); ?>
                <?php echo $form->error($model->addressData_receiver,'address_line_1'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->addressData_receiver,'zip_code'); ?>
                <?php echo $form->textField($model->addressData_receiver,'zip_code', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->addressData_receiver,'zip_code'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->addressData_receiver,'city'); ?>
                <?php echo $form->textField($model->addressData_receiver,'city', array('maxlength' => 45)); ?>
                <?php echo $form->error($model->addressData_receiver,'city'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($model->addressData_receiver,'country_id'); ?>
                <?php echo $form->dropDownList($model->addressData_receiver, 'country_id', CHtml::listData(CountryList::model()->with('countryListTr')->findAll(),'id','trName')); ?>
                <?php echo $form->error($model->addressData_receiver, 'country_id'); ?>
            </div><!-- row -->
            <div class="row" data-inv-company-field="true">
                <?php echo $form->labelEx($model->addressData_receiver,'_nip'); ?>
                <?php echo $form->textField($model->addressData_receiver, '_nip', array('maxlength' => 20, 'placeholder' => '- required with company -', 'data-check-vies-widget' => true)); ?>
                <?php echo $form->error($model->addressData_receiver, '_nip'); ?>
            </div><!-- row -->



        </div>
    </div>


    <hr/>
    <div>
        <?php echo $form->error($model,'items'); ?>

        <table id="table-items" style="width: 100%;">
            <tr>
                <?php
                if($model->correction):
                    ?>
                    <th class="text-left"></th>
                <?php
                endif;
                ?>
                <th class="text-left"><?= $model->getAttributeLabel('name');?></th>
                <th class="text-left"><?= $model->getAttributeLabel('no');?></th>
                <th class="text-left"><?= $model->getAttributeLabel('value_netto');?></th>
                <th class="text-left"><?= $model->getAttributeLabel('tax');?></th>
                <th class="text-left"><?= $model->getAttributeLabel('value_brutto');?></th>
                <th class="text-right"><strong>Total value</strong></th>
                <th></th>
            </tr>
            <?php
            /* @var $item FakturowniaItem */
            $i = 0;
            if($model->correction):
                foreach($model->itemsBefore AS $key => $item):

                    $itemAfter = $model->itemsAfter[$key];
                    ?>
                    <tbody style="border-bottom: 1px solid lightgrey; padding-bottom: 5px;" data-tbody-i="<?= $i;?>">
                    <tr data-before="true">
                        <td>Before:</td>
                        <td><?php echo $form->textField($item, '[before]['.$i.']name', ['style' => 'width: 400px;']); ?></td>
                        <td><?php echo $form->numberField($item, '[before]['.$i.']no', ['data-no' => true, 'min' => 1, 'style' => 'width: 50px;', 'step' => 1]); ?></td>
                        <td><?php echo $form->numberField($item, '[before]['.$i.']value_netto', ['data-value-netto' => true, 'min' => 0, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td><?php echo $form->numberField($item, '[before]['.$i.']tax', ['data-tax' => true, 'min' => 0, 'max' => 100, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td><?php echo $form->numberField($item, '[before]['.$i.']value_brutto', ['data-value-brutto' => true, 'min' => 0, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td class="text-right"><span class="total-value" style="font-weight: bold;"><?= number_format( intval($item->no) * floatval($item->value_brutto), 2);?></span></td>
                        <td style="padding-left: 50px;"></td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td></td>
                        <td><?php echo $form->error($item,'['.$i.']name'); ?></td>
                        <td><?php echo $form->error($item,'['.$i.']nno'); ?></td>
                        <td><?php echo $form->error($item,'['.$i.']nvalue_netto'); ?></td>
                        <td><?php echo $form->error($item,'['.$i.']ntax'); ?></td>
                        <td><?php echo $form->error($item,'['.$i.']nvalue_brutto'); ?></td>
                        <td></td>
                    </tr>
                    <tr data-after="true">
                        <td>After:</td>
                        <td><?php echo $form->textField($itemAfter, '[after]['.$i.']name', ['style' => 'width: 400px;']); ?></td>
                        <td><?php echo $form->numberField($itemAfter, '[after]['.$i.']no', ['data-no' => true, 'min' => 1, 'style' => 'width: 50px;', 'step' => 1]); ?></td>
                        <td><?php echo $form->numberField($itemAfter, '[after]['.$i.']value_netto', ['data-value-netto' => true, 'min' => 0, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td><?php echo $form->numberField($itemAfter, '[after]['.$i.']tax', ['data-tax' => true, 'min' => 0, 'max' => 100, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td><?php echo $form->numberField($itemAfter, '[after]['.$i.']value_brutto', ['data-value-brutto' => true, 'min' => 0, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td class="text-right"><span class="total-value" style="font-weight: bold;"><?= number_format( intval($itemAfter->no) * floatval($itemAfter->value_brutto), 2);?></span></td>
                        <td style="padding-left: 50px;"></td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td></td>
                        <td><?php echo $form->error($itemAfter,'['.$i.']name'); ?></td>
                        <td><?php echo $form->error($itemAfter,'['.$i.']no'); ?></td>
                        <td><?php echo $form->error($itemAfter,'['.$i.']value_netto'); ?></td>
                        <td><?php echo $form->error($itemAfter,'['.$i.']tax'); ?></td>
                        <td><?php echo $form->error($itemAfter,'['.$i.']value_brutto'); ?></td>
                        <td></td>
                    </tr>
                    <tr data-diff="true" data-i="<?= $i;?>">
                        <td></td>
                        <td></td>
                        <td><?php echo CHtml::textField('diff['.$i.'][no]', '', ['disabled' => true, 'style' => 'width: 50px; display: none;']); ?></td>
                        <td><?php echo CHtml::textField('diff['.$i.'][value_netto]', '', ['disabled' => true, 'style' => 'width: 75px;; display: none;']); ?></td>
                        <td></td>
                        <td><?php echo CHtml::textField('diff['.$i.'][value_brutto]', '', ['disabled' => true, 'style' => 'width: 75px;; display: none;']); ?></td>
                        <td class="text-right"></td>
                        <td style="padding-left: 50px;"><a href="#" class="btn" data-delete="true">delete</a></td>
                    </tr>
                    </tbody>
                    <?php
                    $i++;
                endforeach;
            else:
                foreach($model->items AS $item):
                    ?>
                    <tbody>
                    <tr>
                        <td><?php echo $form->textField($item, '['.$i.']name', ['style' => 'width: 400px;']); ?></td>
                        <td><?php echo $form->numberField($item, '['.$i.']no', ['data-no' => true, 'min' => 1, 'style' => 'width: 50px;', 'step' => 1]); ?></td>
                        <td><?php echo $form->numberField($item, '['.$i.']value_netto', ['data-value-netto' => true, 'min' => 0, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td><?php echo $form->numberField($item, '['.$i.']tax', ['data-tax' => true, 'min' => 0, 'max' => 100, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td><?php echo $form->numberField($item, '['.$i.']value_brutto', ['data-value-brutto' => true, 'min' => 0, 'style' => 'width: 75px;', 'step' => 0.01]); ?></td>
                        <td class="text-right"><span class="total-value" style="font-weight: bold;"><?= number_format( intval($item->no) * floatval($item->value_brutto), 2);?></span></td>
                        <td style="padding-left: 50px;"><a href="#" class="btn" data-delete="true">delete</a></td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td><?php echo $form->error($item,'['.$i.']name'); ?></td>
                        <td><?php echo $form->error($item,'['.$i.']no'); ?></td>
                        <td><?php echo $form->error($item,'['.$i.']value_netto'); ?></td>
                        <td><?php echo $form->error($item,'['.$i.']tax'); ?></td>
                        <td><?php echo $form->error($item,'['.$i.']value_brutto'); ?></td>
                        <td></td>
                    </tr>
                    </tbody>
                    <?php
                    $i++;
                endforeach;
            endif;
            ?>



            <tr>
                <td colspan="<?= $model->correction ? 8 : 7;?>" class="text-right"><input type="submit" name="add" value="Add"/></td>
            </tr>
        </table>

    </div>

    <hr/>

    <div class="row">
        <?php echo $form->labelEx($model,'fakturowniaAccountId'); ?>
        <?php echo $form->dropDownList($model, 'fakturowniaAccountId', Fakturownia::listAccounts()); ?>
        <?php echo $form->error($model, 'fakturowniaAccountId'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'currency'); ?>
        <?php echo $form->dropDownList($model, 'currency', Currency::listIdsWithNames()); ?>
        <?php echo $form->error($model, 'currency'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'lang'); ?>
        <?php echo $form->dropDownList($model, 'lang', array_combine(Language::languagesArray(),Language::languagesArray())); ?>
        <?php echo $form->error($model, 'lang'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'paymentType'); ?>
        <?php echo $form->dropDownList($model, 'paymentType', FakturowniaForm::getPaymentTypeList(),['prompt' => '-']); ?>
        <?php echo $form->error($model, 'paymentType'); ?>
    </div><!-- row -->
    <hr/>


    <div class="text-center">
        <?php echo TbHtml::submitButton('Create', ['class' => 'btn btn-primary btn-lg']); ?>
    </div>

    <?= $form->hiddenField($model,'correction');?>

    <?php $this->endWidget(); ?>
</div>

<script>
    $(document).ready(function(){

        $('#source_fv_date').on('change', function(){

            var val = $(this).val();
            val = val.replace(new RegExp('r\.', 'g'), '');
            val = val.replace(new RegExp('r', 'g'), '');
            val = val.replace(new RegExp('\\.', 'g'), '-');
            val = val.trim();
            val = val.split('-');
            val = val[2] + '-' + val[1] + '-' + val[0];

            $('#sell-date').val(val);
        });

        $('#fill_desc').on('click', function(){
            var text = 'Korekta do fv {FVNO} z dn. {FVDATE}.';

            var source_fv_no = $('#source_fv_no').val();
            var source_fv_date = $('#source_fv_date').val();

            var source_compaint_no = $('#source_compaint_no').val();
            if(source_compaint_no != '')
                text = text + " Uznana reklamacja " + source_compaint_no + ".";

            text = text + " Prosimy o pomniejszenie kolejnej płatności o wartość korekty i podanie numeru korekty w tytule płatności."

            if(source_fv_no != '')
                text = text.replace("{FVNO}", source_fv_no);

            if(source_fv_date != '')
                text = text.replace("{FVDATE}", source_fv_date);

            $('#inv_desc').val(text);
        });

        $('#fakturownia-form').on('submit', function(){
            Preloader.show();
        });

        $('#paste-user-data').on('click', function(){

            $.ajax({
                url: '<?= Yii::app()->createUrl('/user/ajaxGetUserAddressData');?>',
                data: { id : parseInt($('[data-user-save]').val())},
                method: 'POST',
                dataType: 'json',
                success: function (result) {

                    if(result)
                    {
                        // console.log(result);
                        $.each(result, function(i,v)
                        {
                            // console.log(i);
                            // console.log(v);
                            if(i == '_vatRate')
                                $('[data-tax]').val(v);
                            else
                                $('#AddressData_Order_' + i).val(v);
                        });
                    }

                },
                error: function (e) {
                    console.log(e);
                }
            });

        });

        $('[data-delete]').on('click', function(){
            $(this).closest('tbody').remove();
        });

        $('[data-value-netto]').on('change', function(){
            calculateTotal($(this));
        });
        $('[data-value-brutto]').on('change', function(){
            calculateTotal($(this));
        });
        $('[data-tax]').on('change', function(){
            calculateTotal($(this));
        });
        $('[data-no]').on('change', function(){
            calculateTotal($(this));
        });


        $('[data-before],[data-after] input').on('change', function(){

            setTimeout(calculateDiff, 500);
        });

        function calculateDiff()
        {
            var j = -1;
            $('[data-before] input').each(function(i,v){


                var attr = $(v).attr('name');
                attr = attr.replace('FakturowniaItem[before]','diff');
                attr = attr.replace('FakturowniaItem[after]','diff');

                var attrBefore = attr.replace('diff','FakturowniaItem[before]');
                var attrAfter = attr.replace('diff','FakturowniaItem[after]');

                val = parseFloat($('[name="' + attrAfter + '"]').val()) - parseFloat($('[name="' + attrBefore + '"]').val());

                $('[name="' + attr + '"]').val(val.toFixed(2));
            });

        }


        function calculateTotal(that)
        {
            var $no = that.closest('tr').find('[data-no]');
            var $netto = that.closest('tr').find('[data-value-netto]');
            var $brutto = that.closest('tr').find('[data-value-brutto]');
            var $tax = that.closest('tr').find('[data-tax]');

            var $total =  that.closest('tr').find('.total-value');

            no = parseInt($no.val());
            netto = parseFloat($netto.val());
            brutto = parseFloat($brutto.val());
            tax = parseFloat($tax.val());

            tax = 1 + (tax/100);

            if(that.is('[data-value-netto]'))
            {
                brutto = (tax * netto);
            }
            else if(that.is('[data-value-brutto]')) {
                if (tax)
                    netto = (brutto / tax);
            }
            else if(that.is('[data-tax]'))
            {
                brutto = (tax * netto);
            }


            var total = no * netto * tax;


            $netto.val(netto.toFixed(2));
            $brutto.val(brutto.toFixed(2));
            $total.html(total.toFixed(2));
        }

        requiresUserSave();
        $('[data-user-save]').on('change', requiresUserSave);

        function requiresUserSave() {
            if (parseInt($('[data-user-save]').val())) {
                $('[data-requires-user-save]').show();
                $('#paste-user-data').show();
            }
            else
            {
                $('[data-requires-user-save]').hide();
                $('#paste-user-data').hide();
            }
        }

        $('[data-plusdays]').on('click', function(){

            var days = $(this).attr('data-plusdays');
            var issueDate = $('#FakturowniaForm_issueDate').val();

            $(this).parent('.row').find('input[type=text]').val(moment(issueDate).add(days, 'days').format('YYYY-MM-DD'))

        })
    })
</script>

<?php
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
$script = "$( function() {
            $('[date-picker]').datepicker({
            dateFormat : 'yy-mm-dd'
            });
                           
                    } );";
Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);

Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl().'/js/moment.js', CClientScript::POS_END);


$script = "
     $('[data-chosen-widget]').chosen({allow_single_deselect:true});
        $('.chosen-drop').css('width', '300px');
        $('[data-chosen-widget]').hide();
    ";
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.jquery.min.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.min.css');
Yii::app()->clientScript->registerScript('chosen-widget', $script, CClientScript::POS_READY);

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/preloader.js', CClientScript::POS_END);



?>

