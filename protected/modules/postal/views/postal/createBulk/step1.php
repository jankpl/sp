<?php
/* @var $models Postal[] */
/* @var $form TbActiveForm */
/* @var $domestic boolean */
?>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  checkIfAvailable: '.CJSON::encode(Yii::app()->urlManager->createUrl('/postal/postalOoe/ajaxCheckIfAvailable')).',
                  serviceDescription: '.CJSON::encode(Yii::app()->createUrl('/postal/postalOoe/ajaxGetServiceDescription')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).'
              },
               loader: {
                  textTop: '.CJSON::encode(Yii::t('site', 'Trwa ładowanie...')).',
                  textBottom: '.CJSON::encode(Yii::t('site', 'Proszę czekać...')).',
                  error:  '.CJSON::encode(Yii::t('site', 'Wystąpił błąd, ale system spróbuje automatycznie przetworzyć formularz. Następnie proszę spróbować kontynuować. Jeżeli błąd będzie się powtarzał, prosimy o kontakt.')).',
              },
          };
      ',CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.bootstrap-touchspin.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/postalForm.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.bootstrap-touchspin.min.css');
?>

<h2><?php echo Yii::t('postal', 'Create Bulk Postal'); ?> | <?= Yii::t('app','Krok {step}', array('{step}' => '1/2')); ?></h2>
<div class="form postal-bulk-form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'postal-form',
        'enableClientValidation' => true,
        'stateful'=>true,
//        'htmlOptions' => [
//            'class' => 'form-horizontal',
//        ],
    ));
    ?>
    <div class="alert alert-info">
        <strong><?= (new Postal)->getAttributeLabel('postal_type');?></strong>
        <br/>
        <ul>
            <?php
            foreach(Postal::getPostalTypeList(true) AS $key => $item):
                ?>
                <li><strong><?= $item;?></strong>: <?= Postal::getPostalTypeListDesc()[$key];?></li>
            <?php
            endforeach;
            ?>
        </ul>
    </div>

    <div class="alert alert-info">
        <strong><?= (new Postal)->getAttributeLabel('size_class');?></strong>
        <br/>
        <ul>
            <?php
            foreach(Postal::getSizeClassList() AS $key => $item):
                ?>
                <li><strong><?= $item;?></strong>: <?= Postal::getSizeClassListDesc()[$key];?></li>
            <?php
            endforeach;
            ?>
        </ul>
    </div>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12">

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><?= Yii::t('postal', 'Kraj odbiorcy');?></th>
                    <th><?= (new Postal())->getAttributeLabel('postal_type'); ?></th>
                    <th><?= (new Postal())->getAttributeLabel('weight'); ?> [g]</th>
                    <th><?= (new Postal())->getAttributeLabel('weight_class'); ?></th>
                    <th><?= (new Postal())->getAttributeLabel('size_class'); ?></th>
                    <th><?= (new Postal())->getAttributeLabel('bulk_number'); ?></th>
                    <th><?= (new Postal())->getAttributeLabel('content'); ?></th>
                    <?php
                    // SPECIAL RULE FOR RUCH // 13.09.2016
                    if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH):
                        ?>
                        <th><?= (new Postal())->getAttributeLabel('target_sp_point'); ?></th>
                    <?php
                    endif;
                    ?>
                </tr>

                <?php

                $spPoints = SpPoints::getPoints();
                $countryList = CHtml::listData(Postal::getCountryList(), 'id', 'trName');
                foreach($models AS $id => $model)
                {
                    $this->renderPartial('createBulk/_bulk_item',
                        [
                            'model' => $model,
                            'i' => $id,
                            'spPoints' => $spPoints,
                            'countryList' => $countryList,
                        ]);
                }
                ?>


                </thead>
            </table>


        </div>
    </div>

    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>
    <div class="row" style="padding: 10px 0;">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('postal', 'Kontynuuj');?></legend>
                <div data-not-available="true" style="<?= ($noCarriage) ? '' : 'display: none;';?>">
                    <div class="alert alert-warning text-center">
                        <?= Yii::t('postal', 'Wybrany operator nie obsługuje tego państwa odbiorcy.');?>
                    </div>
                </div>
                <div class="text-center">
                    <?php echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('name' => 'summary', 'class' => 'btn-lg btn-primary', 'disabled' => ($noCarriage OR $noPickup) ? 'disabled' : '', 'data-continue-button' => 'true')); ?>
                </div>
            </fieldset>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    var classes = '<?= json_encode(array_combine((Postal::getWeightClassValueList()), Postal::getWeightClassList()));?>';
    var maxWeightForSizeClass = '<?= json_encode((Postal::getMaxWeightForSizeClassList()));?>';
    classes = JSON.parse(classes);
    maxWeightForSizeClass = JSON.parse(maxWeightForSizeClass);

    function showWeightClass(item) {

        var $weightInput = $(item);


        var weight = parseFloat($weightInput.val());
        var max = parseFloat($weightInput.attr('max'));

        if (weight > max) {
            $weightInput.val(max);
            weight = max;
        }

        var found = false;

        if(!weight)
            found = '-';
        else
            $.each(classes, function(i,v)
            {
                found = v;
                if(parseFloat(i) >= weight)
                    return false;

            });


        $weightInput.closest('.postal-bulk-item').find('[data-weight-class-info]').html(found);

        $.each(maxWeightForSizeClass, function(i,v){

            var $temp = $weightInput.closest('.postal-bulk-item').find('[data-size-class] option[value=' + i + ']');

            if(v < weight)
                $temp.attr('disabled', true);
            else
                $temp.attr('disabled', false);

        })
    }


    $('[data-weight-value]').on('change', function(){
        showWeightClass(this);
    });

    $(document).ready(function(){

        $('[data-weight-value]').each(function(i,v){
            showWeightClass(v);
        });
    });
</script>