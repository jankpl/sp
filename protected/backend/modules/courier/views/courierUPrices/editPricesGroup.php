<?php
/* @var $model CourierUPriceGroup */
/* @var $priceItems CourierUPriceItem */
/* @var $errors boolean */
/* @var $errorsDesc [] */
?>
    <div class="form">

    <h1>Prices U</h1>

<?php
if($model->user_group_id === NULL && !AdminRestrictions::isUltraAdmin()):
    ?>
    <div class="alert alert-warning text-center">You have no authority to modify main routing.</div>
<?php
else:
?>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<?php
if($errors)
    foreach($errorsDesc AS $errorText) {
        if (!is_array($errorText))
            $errorText = [ $errorText ];

        foreach($errorText AS $errorText2) {
            if (!is_array($errorText2))
                $errorText2 = [ $errorText2 ];

            foreach($errorText2 AS $errorText3) {
                if (!is_array($errorText3))
                    $errorText3 = [$errorText3];
                foreach ($errorText3 AS $errorText4)
                    echo '<div class="alert alert-danger">' . $errorText4 . '</div>';
            }

        }
    }
?>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'courier-u-price-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
));
?>
<?php echo $form->errorSummary($model); ?>
    <table class="list hTop" style="width: 700px;">
        <thead class="prices-header">
        <tr>
            <td style="width: 40%;"><?php echo $form->labelEx((new CourierUPriceItem()),'weight_top_limit'); ?></td>
            <td style="width: 40%;"><?php echo $form->labelEx((new CourierUPriceItem()),'price_per_item'); ?></td>
            <td><span style="font-weight: bold;">action</span></td>
        </tr>
        <tr><td></td><td></td><td></td></tr>
        </thead>
        <?php
        foreach($priceItems AS $key => $item)
        {
            $this->renderPartial('_edit_price_item_value', array(
                'model' => $item,
                'form' => $form,
                'i' => $key,
            ));
        }
        ?>
    </table>
<?php
echo CHtml::submitButton("Add weight group",array(
    'name'=>'add_more',
    'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
    'id' => 'add_more',
    'class' => 'btn btn-primary'
));
?>
<?php
echo CHtml::submitButton("Sort by weights",array(
    'name'=>'sort',
    'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
    'class' => 'btn btn-primary'
));

?>

<input type="button" value="Fill empty prices with zeros" id="auto-fill-zeros" class="btn btn-primary"/><br/><br/>
<input type="button" value="Copy pricing to clipboard" id="auto-copy" class="btn btn-primary"/>
<input type="button" value="Past pricing from clipboard" id="auto-paste" class="btn btn-warning"/><br/><br/>


    <div class="text-center">
        Revision: <?= $model->_openedRevision ? $model->_openedRevision : '-not saved yet-';?><br/>
        <?php
        echo GxHtml::submitButton(Yii::t('app', 'Save'), ['id' => 'save_all', 'name' => 'save', 'class' => 'btn btn-success']);
        echo $form->hiddenField($model, '_openedRevision');
        ?>
    </div>

<?php
$this->endWidget();
?>

    <script>
        String.prototype.replaceAll = function(search, replacement) {
            var target = this;
            return target.split(search).join(replacement);
        };

        $('#auto-fill-zeros').on('click', fill);


        $('#auto-copy').on('click', copy);

        $('#auto-paste').on('click', paste);


        $("[data-delete-button]").on("click", deleteItem);

        function copy()
        {
            var done = 0;
            var data = [];
            $('[data-weight-top-limit]').each(function(i,v){

                done++;

                var temp = [];
                temp[0] = $(v).val();

                $(v).closest('tr').find('[data-price-value]').each(function(ii,vv)
                {
                    var currency = parseInt($(vv).attr('data-price-value'));
                    temp[currency] = parseFloat($(vv).val());
                });

                data.push(temp);
            });

            localStorage.setItem('CourierUPrices', JSON.stringify(data));

            if(done)
                alert("Copied weight groups: " + done);
            else
                alert("I got nothing...");
        }

        function paste()
        {
            var done = 0;
            var data = JSON.parse(localStorage.getItem('CourierUPrices'));


            if(data === null)
            {
                alert("Data not found in clipboard!");
                return true;
            }

            var template = $('._price_item').get(0).outerHTML;
            var templatePriceId = $(template).attr('data-price-id');
            data.reverse();
            $('._price_item').remove();
            $(data).each(function(i,v){

                temp = template.replaceAll('CourierUPriceItem[' + templatePriceId + '][weight_top_limit]', 'CourierUPriceItem[' + i + '][weight_top_limit]');
                temp = temp.replaceAll('CourierUPriceItem_' + templatePriceId + '_weight_top_limit', 'CourierUPriceItem_' + i + '_weight_top_limit');
                temp = temp.replaceAll('PriceValue[' + templatePriceId + '][pricePerItem]', 'PriceValue[' + i + '][pricePerItem]');
                temp = temp.replaceAll('PriceValue_' + templatePriceId + '_pricePerItem', 'PriceValue_' + i + '_pricePerItem');

                temp = $(temp).clone();
                temp.attr('data-price-id', i);
                temp.find('[data-weight-top-limit]').val(v[0]);

                temp.find('[data-price-value]').each(function(ii,vv){
                    var price_id = parseInt($(this).attr('data-price-value'));
                    if(typeof v[price_id] !== 'undefined') {
                        $(this).val(v[price_id]);
                    }

                });

                $('.prices-header').after(temp);
                done++;

            });

            $("[data-delete-button]").on("click", deleteItem);

            if(done)
                alert("Pasted weight groups: " + done);
            else
                alert("I got nothing...");
        }

        function fill()
        {
            var done = 0;
            $('[data-price-value]').each(function(i,v){

                if($(v).val() == '') {
                    $(v).val(0);
                    done++;
                }
            });

            if(done)
                alert("Done times: " + done);
            else
                alert("Nothing changed....");
        }

        function deleteItem()
        {
            if($("._price_item").length > 1)
            {
                var r = confirm("Are you sure?");
                if (r == true) {
                    $("[data-price-id=" + $(this).attr("data-delete-button") +"]").remove();
                }
            }
            else
            {
                alert("You cannot delete all prices!");
            }
        }

    </script>
<?php
if(0):
?>
    <div class="text-center">[<?= CHtml::link('back', ['/courier/courierUPrices']);?>]</div>
<?php
endif;
?>
    </div>

<?php
endif;