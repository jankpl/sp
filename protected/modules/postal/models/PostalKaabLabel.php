<?php

class PostalKaabLabel
{
    const TYPE_82x65 = 1;
    const TYPE_90x45 = 2;
    const TYPE_100x150 = 3;

    /**
     * Generates KAAB own label and returns it as Image
     * @param Postal $item
     * @return String
     */
    public static function generate(Postal $item, $asArray = false, $rotate = false, $type = self::TYPE_82x65, $pdf = false)
    {
        if($type == self::TYPE_90x45)
            return self::_generate90x45($item, $asArray, $rotate);
        if($type == self::TYPE_100x150)
            return self::_generate100x150($item, $asArray, $rotate, $pdf);
        else
            return self::_generate82x65($item, $asArray, $rotate);
    }

    /**
     * Generates KAAB own label and returns it as Image
     * @param Postal $item
     * @return String
     */
    protected static function _generate82x65(Postal $item, $asArray = false, $rotate = false)
    {

        $pageW = 82;
        $pageH = 65;


        // GENERATE OWN:
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pdf->addPage('H', array($pageW, $pageH));

        $margins = 1;

        /* @var $pdf TCPDF */

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $yBase = 0;
        $xBase = 0;

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->MultiCell($blockWidth, $blockHeight, '', 1, 'J', false, 0, $xBase + $margins, $yBase + $margins, true, 0, false, true, 0);


        $logo = PDFGenerator::getLogo();
        $pdf->Image('@'.$logo, $xBase + $margins + 4, $pdf->GetY() + 1, '',6,'','N',false);



        $text = 'Weight: '.$item->getWeightClassName()."\r\n";
        $text .= 'Size: '.$item->getSizeClassName()."\r\n";
        $text .= 'Type: '.$item->getPostalTypeName()."\r\n";
        $text .= 'Content: '.$item->content."\r\n";

        if($item->bulk)
            $text .= 'Bulk ID: '.$item->bulk_id;

        $pdf->SetFont('freesans', '', 6);
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
        $pdf->MultiCell(30,15, $text, 0, 'L', false, 1, $xBase + $margins + 1, $yBase + $margins + 9, true, 0, false, true, 15, 'T', true);

        if($item->postalOperator !== NULL && $item->postalOperator->stamp != '')
            $stamp = $item->postalOperator->stamp;

        $stamp = explode('base64,', $stamp);
        $stamp = $stamp[1];
        $stamp = @base64_decode($stamp);

        if($stamp !='') {
            $pdf->Image(
                '@' . $stamp,
                $xBase + $blockWidth - 39 - $margins,
                $yBase + $margins + 2,
                40,
                29,
                '',
                '',
                'LTR',
                true,
                600,
                '',
                false,
                false,
                0,
                'RT');

            $pdf->MultiCell(40, 29, '', 1, 'L', false, 1, $xBase + $blockWidth - 39 - $margins, $yBase + $margins + 2, true, 0, false, true, 18, 'B', true);
        }

//        $returnAddress = $item->postalOperator->returnAddressData;
//        if($returnAddress === NULL)
//            $text = "Grodkowska 40, 48-300 Nysa. Tel. 22 122 12 18";
//        else {
//            $text = $returnAddress->name != '' ? $returnAddress->name . " " : '';
//            $text .= $returnAddress->company != '' ? $returnAddress->company . " " : '';
//            $text .= $returnAddress->address_line_1 != '' ? $returnAddress->address_line_1 . " " : '';
//            $text .= $returnAddress->address_line_2 != '' ? $returnAddress->address_line_2 . " " : '';
//            $text .= $returnAddress->zip_code . " " . $returnAddress->city . " ";
//            $text .= $returnAddress->country . " ";
//        }

//        $pdf->SetFont('freesans', '', 6);
//        $pdf->MultiCell(50,2, $text, 0, 'R', false, 1, $xBase + 30 + $margins - 2, $yBase + $margins + 5, true,
//            0,
//            false,
//            true,
//            2,
//            'T',
//            true);
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins,$yBase + $margins + 8,$xBase + 40, $yBase + $margins + 8);



//        $text = $item->senderAddressData->name!=''?$item->senderAddressData->name."\r\n":'';
//        $text .= $item->senderAddressData->company!=''? $item->senderAddressData->company."\r\n":'';
//        $text .= $item->senderAddressData->address_line_1!=''? $item->senderAddressData->address_line_1."\r\n":'';
//        $text .= $item->senderAddressData->address_line_2!=''? $item->senderAddressData->address_line_2."\r\n":'';
//        $text .= $item->senderAddressData->zip_code." ".$item->senderAddressData->city."\r\n";
//        $text .= $item->senderAddressData->country."\r\n";

//        $pdf->SetFont('freesans', '', 6);
//        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
//        $pdf->Line($xBase + $margins,$yBase + $margins + 28,$xBase + 40, $yBase + $margins + 28);

        $pdf->SetFont('freesans', '', 8);

        $text = 'SP'.$item->user_id.'P';

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));

        $pdf->MultiCell(45,13, $text, 0, 'L', false, 1, $xBase + $margins + 1, $yBase + 30 + $margins, true,
            0,
            false,
            true,
            13,
            'T',
            true
        );

//        $pdf->SetFont('freesans', '', 8);
//        $text = "Odbiorca:";
//        $pdf->MultiCell(35,10, $text, 1, 'R', false, 1, $xBase + $margins + 40, $yBase + 20 + $margins, true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 15);
        $text = $item->receiverAddressData->name!=''?$item->receiverAddressData->name."\r\n":'';
        $text .= $item->receiverAddressData->company!=''? $item->receiverAddressData->company."\r\n":'';
        $text .= $item->receiverAddressData->address_line_1!=''? $item->receiverAddressData->address_line_1."\r\n":'';
        $text .= $item->receiverAddressData->address_line_2!=''? $item->receiverAddressData->address_line_2."\r\n":'';
        $text .= $item->receiverAddressData->zip_code." ".$item->receiverAddressData->city."\r\n";
        $text .= $item->receiverAddressData->country."\r\n";
        $pdf->MultiCell(40,26, $text, 0, 'R', false, 1, $xBase + $margins + 40, $yBase + 32 + $margins,true,
            0,
            false,
            true,
            26,
            'B',
            true
        );
        $pdf->Line($xBase + $margins, $yBase + $margins + 40, 40, $yBase + $margins + 40);
        $pdf->Line(40, $yBase + $margins + 40, 40, $yBase + $margins + 63);


        $pdf->write1DBarcode($item->external_id ? $item->external_id : $item->local_id, 'C128', $xBase + $margins + 0, $yBase + $margins + 40, 40, 24,1, $barcodeStyle);


        $pdf->SetFont('freesans', '', 5);
        $pdf->MultiCell($blockWidth - 40,5, "#".$item->local_id, 0, 'R', false, 1, $xBase + 40 + $margins, $blockHeight - 2, true, 0, false, true, 5);

        $pdfString = $pdf->Output('asString', 'S');

        $img = ImagickMine::newInstance();
        $img->setResolution(900, 900);
        $img->readImageBlob($pdfString);
        $img->setImageFormat('jpg');

        if($rotate)
            $img->rotateImage(new ImagickPixel(), 90);

        $img->stripImage();
        $image = $img->getImageBlob();

        if($asArray)
            $image = [$image];

        return $image;
    }

    protected static function _generate90x45(Postal $item, $asArray = false, $rotate = false)
    {

        $pageW = 90;
        $pageH = 45;


        // GENERATE OWN:
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pdf->addPage('H', array($pageW, $pageH));

        $margins = 1;

        /* @var $pdf TCPDF */

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $yBase = 0;
        $xBase = 0;

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->MultiCell($blockWidth, $blockHeight, '', 1, 'J', false, 0, $xBase + $margins, $yBase + $margins, true, 0, false, true, 0);

        //$pdf->Image('@'.$image,$xBase + $margins,$yBase + $margins,30,10);

        $logo = PDFGenerator::getLogo();
        $pdf->Image('@'.$logo, $xBase + $margins + 4, $pdf->GetY() + 1, '',4,'','N',false);

//        $pdf->SetFont('freesans', '', 8);
//        $pdf->MultiCell($blockWidth - 49,3, "#".$item->local_id, 0, 'R', false, 1, $xBase + 40 + $margins - 15, $yBase + $margins + 1, true, 0, false, true, 0);

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins,$yBase + $margins + 6,$xBase + $blockWidth + $margins - 45, $yBase + $margins + 6);
        $pdf->Line($xBase + $blockWidth + $margins - 45,$yBase + $margins,$xBase + $blockWidth + $margins - 45, $yBase + $margins + 8);

        if($item->postalOperator !== NULL && $item->postalOperator->stamp != '')
            $stamp = $item->postalOperator->stamp;

        $stamp = explode('base64,', $stamp);
        $stamp = $stamp[1];
        $stamp = @base64_decode($stamp);

        if($stamp !='') {
            $pdf->Image(
                '@' . $stamp,
                $xBase + $blockWidth - 43 - $margins,
                $yBase + $margins + 0,
                45,
                23,
                '',
                '',
                'LTR',
                true,
                600,
                '',
                false,
                false,
                0,
                'RT');

            $pdf->MultiCell(45, 23, '', 1, 'L', false, 1, $xBase + $blockWidth - 43 - $margins, $yBase + $margins + 0, true, 0, false, true, 18, 'B', true);
        }

//        $returnAddress = $item->postalOperator->returnAddressData;
//        if($returnAddress === NULL)
//            $text = "Grodkowska 40, 48-300 Nysa. Tel. 22 122 12 18";
//        else {
//            $text = $returnAddress->name != '' ? $returnAddress->name . " " : '';
//            $text .= $returnAddress->company != '' ? $returnAddress->company . " " : '';
//            $text .= $returnAddress->address_line_1 != '' ? $returnAddress->address_line_1 . " " : '';
//            $text .= $returnAddress->address_line_2 != '' ? $returnAddress->address_line_2 . " " : '';
//            $text .= $returnAddress->zip_code . " " . $returnAddress->city . " ";
//            $text .= $returnAddress->country . " ";
//        }
//
//        $pdf->SetFont('freesans', '', 6);
//        $pdf->MultiCell(40,2, $text, 0, 'L', false, 1, $xBase + 4 + $margins - 4, $yBase + $margins + 6, true,
//            0,
//            false,
//            true,
//            2,
//            'T',
//            true);


//        $text = $item->senderAddressData->name!=''?$item->senderAddressData->name."\r\n":'';
//        $text .= $item->senderAddressData->company!=''? $item->senderAddressData->company."\r\n":'';
//        $text .= $item->senderAddressData->address_line_1!=''? $item->senderAddressData->address_line_1."\r\n":'';
//        $text .= $item->senderAddressData->address_line_2!=''? $item->senderAddressData->address_line_2."\r\n":'';
//        $text .= $item->senderAddressData->zip_code." ".$item->senderAddressData->city."\r\n";
//        $text .= $item->senderAddressData->country."\r\n";


        $text = 'Weight: '.$item->getWeightClassName()."\r\n";
        $text .= 'Size: '.$item->getSizeClassName()."\r\n";
        $text .= 'Type: '.$item->getPostalTypeName()."\r\n";
        $text .= 'Content: '.$item->content."\r\n";

        if($item->bulk)
            $text .= 'Bulk ID: '.$item->bulk_id;

        $pdf->SetFont('freesans', '', 6);
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
        $pdf->MultiCell(37,14, $text, 0, 'L', false, 1, $xBase + $margins, $yBase + $margins + 6, true, 0, false, true, 14, 'T', true);



        $pdf->SetFont('freesans', '', 8);
        $text = 'SP'.$item->user_id.'P';
        $pdf->MultiCell(45,5, $text, 0, 'L', false, 1, $xBase + $margins + 0, $yBase + 20 + $margins, true,
            0,
            false,
            true,
            5,
            'B',
            true
        );

//        $pdf->SetFont('freesans', '', 8);
//        $text = "Odbiorca:";
//        $pdf->MultiCell(35,10, $text, 1, 'R', false, 1, $xBase + $margins + 40, $yBase + 20 + $margins, true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 15);
        $text = $item->receiverAddressData->name!=''?$item->receiverAddressData->name."\r\n":'';
        $text .= $item->receiverAddressData->company!=''? $item->receiverAddressData->company."\r\n":'';
        $text .= $item->receiverAddressData->address_line_1!=''? $item->receiverAddressData->address_line_1."\r\n":'';
        $text .= $item->receiverAddressData->address_line_2!=''? $item->receiverAddressData->address_line_2."\r\n":'';
        $text .= $item->receiverAddressData->zip_code." ".$item->receiverAddressData->city."\r\n";
        $text .= $item->receiverAddressData->country."\r\n";
        $pdf->MultiCell(45,20, $text, 0, 'R', false, 1, $xBase + $margins + 43, $yBase + 23 + $margins,true,
            0,
            false,
            true,
            20,
            'B',
            true
        );
        $pdf->Line($xBase + $margins, $yBase + $margins + 25, $xBase + $margins + 43, $yBase + $margins + 25);
        $pdf->Line($xBase + $margins + 43, $yBase + $margins + 25,$xBase + $margins + 43, $yBase + $margins + 43);

        $pdf->write1DBarcode($item->external_id ? $item->external_id : $item->local_id, 'C128', $xBase + $margins - 2, $yBase + $margins + 25, $blockWidth - 41, 20,1, $barcodeStyle);

        $pdfString = $pdf->Output('asString', 'S');

        $img = ImagickMine::newInstance();
        $img->setResolution(900, 900);
        $img->readImageBlob($pdfString);
        $img->setImageFormat('jpg');

        if($rotate)
            $img->rotateImage(new ImagickPixel(), 90);

        $img->stripImage();
        $image = $img->getImageBlob();

        if($asArray)
            $image = [$image];

        return $image;
    }

    /**
     * Generates KAAB own label and returns it as Image
     * @param Postal $item
     * @return String
     */
    protected static function _generate100x150(Postal $item, $asArray = false, $rotate = false, $pdfHandler = false)
    {

        $pageW = 96;
        $pageH = 146;

        if(!$pdfHandler) {
            // GENERATE OWN:
            $pdf = PDFGenerator::initialize();
            $pdf->setFontSubsetting(true);

            $pdf->addPage('H', array($pageW, $pageH));
        }
        else $pdf = $pdfHandler;

        $margins = 1;

        /* @var $pdf TCPDF */

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $yBase = 0;
        $xBase = 0;

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->MultiCell($blockWidth, $blockHeight, '', 1, 'J', false, 0, $xBase + $margins, $yBase + $margins, true, 0, false, true, 0);

        $logo = PDFGenerator::getLogo();
        $pdf->Image('@'.$logo, $xBase + $margins + 2, $pdf->GetY() + 1, '',6,'','N',false);


        $text = 'Weight: '.$item->getWeightClassName()."\r\n";
        $text .= 'Size: '.$item->getSizeClassName()."\r\n";
        $text .= 'Type: '.$item->getPostalTypeName()."\r\n";
        $text .= 'Content: '.$item->content."\r\n";

        if($item->bulk)
            $text .= 'Bulk ID: '.$item->bulk_id;

        $pdf->SetFont('freesans', '', 8);
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->MultiCell(34,20, $text, 0, 'L', false, 1, $xBase + $margins + 0, $yBase + $margins + 10, true, 0, false, true, 20, 'T', true);


        $pdf->Line($xBase + $margins,$yBase + $margins + 29,$xBase + 34, $yBase + $margins + 29);
//        $pdf->Line($xBase + $margins,$yBase + $margins + 59,$xBase + 34, $yBase + $margins + 59);

        if($item->postalOperator !== NULL && $item->postalOperator->stamp != '')
            $stamp = $item->postalOperator->stamp;

        $stamp = explode('base64,', $stamp);
        $stamp = $stamp[1];
        $stamp = @base64_decode($stamp);

        if($stamp !='') {
            $pdf->Image(
                '@' . $stamp,
                $xBase + $blockWidth - 59 - $margins,
                $yBase + $margins + 1,
                60,
                44,
                '',
                '',
                'LTR',
                true,
                600,
                '',
                false,
                false,
                0,
                'RT');

            $pdf->MultiCell(60, 44, '', 1, 'L', false, 1, $xBase + $blockWidth - 59 - $margins, $yBase + $margins + 1, true, 0, false, true, 18, 'B', true);
        }
//
//        $returnAddress = $item->postalOperator->returnAddressData;
//        if($returnAddress === NULL)
//            $text = "Grodkowska 40, 48-300 Nysa. Tel. 22 122 12 18";
//        else {
//            $text = $returnAddress->name != '' ? $returnAddress->name . " " : '';
//            $text .= $returnAddress->company != '' ? $returnAddress->company . " " : '';
//            $text .= $returnAddress->address_line_1 != '' ? $returnAddress->address_line_1 . " " : '';
//            $text .= $returnAddress->address_line_2 != '' ? $returnAddress->address_line_2 . " " : '';
//            $text .= $returnAddress->zip_code . " " . $returnAddress->city . " ";
//            $text .= $returnAddress->country . " ";
//        }
//
//
//
//        $pdf->SetFont('freesans', '', 8);
//        $pdf->MultiCell(90,6, $text, 0, 'R', false, 1, $xBase + 5 + $margins - 2, $yBase + $margins + 8, true,
//            0,
//            false,
//            true,
//            6,
//            'M',
//            true);

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins,$yBase + $margins + 8,$xBase + 33, $yBase + $margins + 8);

//        $text = $item->senderAddressData->name!=''?$item->senderAddressData->name."\r\n":'';
//        $text .= $item->senderAddressData->company!=''? $item->senderAddressData->company."\r\n":'';
//        $text .= $item->senderAddressData->address_line_1!=''? $item->senderAddressData->address_line_1."\r\n":'';
//        $text .= $item->senderAddressData->address_line_2!=''? $item->senderAddressData->address_line_2."\r\n":'';
//        $text .= $item->senderAddressData->zip_code." ".$item->senderAddressData->city."\r\n";
//        $text .= $item->senderAddressData->country."\r\n";
        $pdf->SetFont('freesans', '', 10);


        $text = 'SP'.$item->user_id.'P';

        $pdf->MultiCell(34,22, $text, 0, 'L', false, 1, $xBase + $margins + 0, $yBase + 30 + $margins, true,
            0,
            false,
            true,
            22,
            'T',
            true
        );

//        $pdf->SetFont('freesans', '', 8);
//        $text = "Odbiorca:";
//        $pdf->MultiCell(35,10, $text, 1, 'R', false, 1, $xBase + $margins + 40, $yBase + 20 + $margins, true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 20);
        $text = $item->receiverAddressData->name!=''?$item->receiverAddressData->name."\r\n":'';
        $text .= $item->receiverAddressData->company!=''? $item->receiverAddressData->company."\r\n":'';
        $text .= $item->receiverAddressData->address_line_1!=''? $item->receiverAddressData->address_line_1."\r\n":'';
        $text .= $item->receiverAddressData->address_line_2!=''? $item->receiverAddressData->address_line_2."\r\n":'';
        $text .= $item->receiverAddressData->zip_code." ".$item->receiverAddressData->city."\r\n";
        $text .= $item->receiverAddressData->country."\r\n";
        $pdf->MultiCell(85,60, $text, 0, 'L', false, 1, $xBase + $margins + 5, $yBase + 45 + $margins,true,
            0,
            false,
            true,
            60,
            'B',
            true
        );
//        $pdf->Line($xBase + $margins, $yBase + $margins + 50, 40, $yBase + $margins + 50);
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line($margins, $yBase + $margins + 105, $pageW - 1, $yBase + $margins + 105);

        $pdf->write1DBarcode($item->external_id ? $item->external_id : $item->local_id, 'C128', $xBase + $margins + 2, $yBase + $margins + 105, 90, 35,1, $barcodeStyle);


        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell($blockWidth - 40,7, "#".$item->local_id, 0, 'R', false, 1, $xBase + 40 + $margins - 2, $blockHeight - 4, true, 0, false, true, 7);

        if($pdfHandler)
            return $pdf;

        $pdfString = $pdf->Output('asString', 'S');

        $img = ImagickMine::newInstance();
        $img->setResolution(900, 900);
        $img->readImageBlob($pdfString);
        $img->setImageFormat('jpg');
//
//        if($rotate)
//            $img->rotateImage(new ImagickPixel(), 90);

        $img->stripImage();
        $image = $img->getImageBlob();

        if($asArray)
            $image = [$image];

        return $image;
    }

}