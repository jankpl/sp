<?php

class CourierDomesticOperatorController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierDomesticOperator'),
        ));
    }

    public function actionCreate() {

        $model = new CourierDomesticOperator;
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $serviceTr = new CourierDomesticOperatorTr();
            $serviceTr->language_id = $item->id;
            $modelTrs[$item->id] = $serviceTr;
        }

        if (isset($_POST['CourierDomesticOperator'])) {
            $model->setAttributes($_POST['CourierDomesticOperator']);

            if($model->broker == CourierDomesticOperator::BROKER_SAFEPAK)
            {
                $model->params = $model->wrapParams($_POST['safepak_service']);
            }

            foreach($_POST['CourierDomesticOperatorTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierDomesticOperatorTr();
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierDomesticOperatorTrs = $modelTrs;

            if ($model->saveWithTr()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,

        ));
    }

    public function actionUpdate($id) {




        $model = $this->loadModel($id, 'CourierDomesticOperator');



        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = CourierDomesticOperatorTr::model()->find('courier_domestic_operator_id=:courier_domestic_operator_id AND language_id=:language_id', array(':courier_domestic_operator_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $serviceTr = new CourierDomesticOperatorTr();
                $serviceTr->language_id = $item->id;
                $serviceTr->courier_domestic_operator_id = $id;
                $modelTrs[$item->id] = $serviceTr;
            }
        }


        if (isset($_POST['CourierDomesticOperator'])) {

            // load model attributes only on aditable models. On other, allow only translations update
            if($model->editable)
                $model->setAttributes($_POST['CourierDomesticOperator']);

            foreach($_POST['CourierDomesticOperatorTr'] AS $key => $item) {
                if ($modelTrs[$key] === null)
                    $modelTrs[$key] = new CourierDomesticOperatorTr;
                $modelTrs[$key]->setAttributes($item);

            }

            $model->courierDomesticOperatorTrs = $modelTrs;

            if($model->broker == CourierDomesticOperator::BROKER_SAFEPAK)
            {
                $model->params = $model->wrapParams($_POST['safepak_service']);
            }

            if ($model->saveWithTr()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model =  $this->loadModel($id, 'CourierDomesticOperator');
            if(!$model->editable)
                throw new CHttpException(400, 'You cannot delete this item.');

            try
            {
                $model->delete();
            }
            catch(Exception $ex)
            {
                throw new CHttpException(403,'Nie udało się usunąć tego wpisu!');
            }


            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierDomesticOperator('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierDomesticOperator']))
            $model->setAttributes($_GET['CourierDomesticOperator']);


        if(isset($_POST['get-ooe-services']))
        {
            $services = Yii::app()->OneWorldExpress->getServicesForUser($_POST['login'], $_POST['password'], true);
        }

        $this->render('admin', array(
            'model' => $model,
            'services' => $services
        ));
    }

    public function actionStat($id)
    {
        /* @var $model CourierDomesticOperator */


        $model = CourierDomesticOperator::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny id!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}