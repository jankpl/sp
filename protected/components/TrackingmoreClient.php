<?php

class TrackingmoreClient
{

    public static function updateTtCarrier($no, $operatorName, $newOperatorName)
    {
        $track = new Trackingmore();
        $track->updateCarrierCode($no, $operatorName, $newOperatorName);
    }

    public static function deleteTt($no, $operatorName)
    {
        $track = new Trackingmore();
        $track->deleteTrackingItem($operatorName, $no);
    }

    public static function getTt($no, $operatorName)
    {

        $track = new Trackingmore;

        $result = $track->createTracking($operatorName,$no);

        if($result['meta']['type'] == 'Success')
            sleep(15); // wait after adding new package

        $result = $track->getSingleTrackingResult($operatorName,$no);

        $response = [];
        if(is_array($result))
        {
            if($result['meta']['type'] == 'Success')
            {
                $data = $result['data']['destination_info']['trackinfo'];
                if(is_array($data))
                    foreach($data AS $item)
                    {
                        $item2 = trim($item['StatusDescription']);
                        $item2 = explode('	', $item2);

                        if(S_Useful::sizeof($item2) > 1) {
                            $name = $item2[1];
                            $location = $item2[0];
                        } else {
                            $name = $item2[0];
                            $location = '';

                            if(preg_match('/(.*) \(([A-Z]{2})\),(.*)/', $name, $match))
                            {
                                $name = trim($match[3]);
                                $location = $match[1].', '.$match[2];
                            }
                        }

                        array_push($response, [
                            'name' => $name,
                            'date' => $item['Date'],
                            'location' => $location,
                        ]);
                    }


                $data = $result['data']['origin_info']['trackinfo'];
                if(is_array($data))
                    foreach($data AS $item)
                    {
                        $item2 = trim($item['StatusDescription']);
                        $item2 = explode('	', $item2);

                        if(S_Useful::sizeof($item2) > 1) {
                            $name = $item2[1];
                            $location = $item2[0];
                        } else {
                            $name = $item2[0];
                            $location = '';

                            if(preg_match('/(.*) \(([A-Z]{2})\),(.*)/', $name, $match))
                            {
                                $name = trim($match[3]);
                                $location = $match[1].', '.$match[2];
                            }
                        }

                        array_push($response, [
                            'name' => $name,
                            'date' => $item['Date'],
                            'location' => $location,
                        ]);
                    }
            }
        }

        return $response;
    }

    public static function getTtGlobalBrokers($no, $operatorName)
    {
        $statuses = new GlobalOperatorTtResponseCollection();

        $track = new Trackingmore;

        $result = $track->createTracking($operatorName,$no);

        if($result['meta']['type'] == 'Success')
            sleep(15); // wait after adding new package

        $result = $track->getSingleTrackingResult($operatorName,$no);

        if(is_array($result))
        {
            if($result['meta']['type'] == 'Success')
            {
                $data = $result['data']['destination_info']['trackinfo'];
                if(is_array($data))
                    foreach($data AS $item)
                    {
                        $status = (string) preg_replace('/\s+/', ' ',$item['StatusDescription']);
                        $location = (string) $item['Details'];

                        self::_processStatus($status, $location);

                        $statuses->addItem($status, $item['Date'], $location);
                    }


                $data = $result['data']['origin_info']['trackinfo'];
                if(is_array($data))
                    foreach($data AS $item)
                    {
                        $status = (string) preg_replace('/\s+/', ' ',$item['StatusDescription']);
                        $location = (string) $item['Details'];

                        self::_processStatus($status, $location);

                        $statuses->addItem($status, $item['Date'], $location);
                    }


            }
        }

        return $statuses;
    }


    protected static function _processStatus(&$status, &$location)
    {
        if(preg_match('/Delivery is available to be picked up at the Post Office- (.*)/i', $status, $matches)) {
            $status = 'Delivery is available to be picked up at the Post Office';
            $location = trim($matches[1]);
        }
    }

}