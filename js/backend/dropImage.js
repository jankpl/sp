(function() {
// multiupload

    var $imageUploader = $('#image-uploader');
    var $imagePreview = $('#image-preview');

    function preprocessFile(file)
    {

        if (file === undefined) {
            Preloader.hide();
            return false;
        }

        if (!file.type.match('image.*')) {
            Preloader.hide();
            alert(jsHelpers.text.onlyImagesError);

            return false;
        }

        if(file.size > 8000000)
        {
            Preloader.hide();
            alert( jsHelpers.text.imageTooBigError + '8 MB');

            return false;
        }

        // if file is ok
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(event) {

            $.ajax({
                url: jsHelpers.url.resizeImageUrl,
                data: {img: event.target.result},
                method: 'POST',
                dataType: 'json',
                success: function (result) {
                    if(result.image)
                        putImgIntoPreview(result.image);
                    else {
                        Preloader.hide();
                        alert(jsHelpers.text.imageSaveError);
                    }
                },
                error: function (e) {
                    Preloader.hide();
                    alert(jsHelpers.text.imageSaveError);
                    console.log(e);
                }
            });
        }
    }

    function putImgIntoPreview(img)
    {
        $imagePreview.html('');
        var $imgHolder = $('<div>');
        $imgHolder.addClass('img-holder');

        var $imgHolderDelete = $('<div>');
        $imgHolderDelete.addClass('img-holder-delete');

        $imgHolder.append($imgHolderDelete);
        $imgHolder.css('background-image', 'url(' + img + ')');

        $imagePreview.append($imgHolder);

        $imgHolderDelete.on('click', deleteImage);

        $('[data-img-data]').val(img);


        Preloader.hide();
    }

    function deleteImage()
    {

        $(this).parent('.img-holder').remove();
        $('[data-img-data]').val('');

    }


// DROPZONE HANDLERS
    function handleFormFileChange(e){

        Preloader.show();
        var file = e.target.files[0];

        preprocessFile(file);
    }

    function handleDZDrop(evt, $that){

        Preloader.show();

        evt.stopPropagation();
        evt.preventDefault();

        var file = evt.dataTransfer.files[0];

        preprocessFile(file);
//        $dropZone.removeClass('active');
    }

    function handleDZClick(evt, $that)
    {
        var $imageLoaderInput = $that.parent().find('[data-id="imageLoaderInput"]');
        $imageLoaderInput.trigger('click');
    }

    function handleDZDragOver(evt, $that) {

        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy';
        $that.addClass('active');
    }

    function handleDZDragOut(evt, $that) {
        evt.stopPropagation();
        evt.preventDefault();

        $that.removeClass('active');
    }

    function initDZ() {

        var $dropZone = $('#drop_zone_multi');

        var dropZone = document.getElementById($dropZone.attr('id'));
        dropZone.addEventListener('dragover', function(evt){
            handleDZDragOver(evt, $dropZone);
        }, false);
        dropZone.addEventListener('dragleave', function(evt){
            handleDZDragOut(evt, $dropZone);
        }, false);
        dropZone.addEventListener('drop', function(evt){
            handleDZDrop(evt, $dropZone);
        }, false);
        dropZone.addEventListener('click', function(evt){
            handleDZClick(evt, $dropZone);
        }, false);

        var $imageLoaderInput = $dropZone.parent().find('[data-id="imageLoaderInput"]');

        $imageLoaderInput.on('change', function(evt){
            handleFormFileChange(evt, $dropZone);
        });
    }


    $(document).ready(function(){
        initDZ();

        $('[data-img-data]').each(function(i,v){
            var img = $(v).val();
            $(v).val('');
            if(img != '')
                putImgIntoPreview(img);

        })
    });
}());