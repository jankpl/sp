<?php
/* @var $model Courier_CourierTypeInternal */
/* @var $currency integer */
/* @var $codIngnored integer */
?>

<div class="form">

    <h2><?php echo Yii::t('courier','Podsumowanie');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '2/2')); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Parametry przesyłki');?></h4>
            <table class="detail-view" style="width: 100%; margin: 0 auto;">
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('package_weight');?>  [<?php echo Courier::getWeightUnit();?>] </th>
                    <td><?php echo $model->package_weight;  ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo $model->courierTypeExternalReturn->courierLabelNew->getAttributeLabel('_ext_operator_name');?></th>
                    <td><?php echo $model->courierTypeExternalReturn->courierLabelNew->_ext_operator_name;  ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->courierTypeExternalReturn->courierLabelNew->getAttributeLabel('track_id');?> </th>
                    <td><?php echo $model->courierTypeExternalReturn->courierLabelNew->track_id;  ?></td>
                </tr>
            </table>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Zwrot od');?></h4>

            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));

            ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Zwrot do');?> </h4>
            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));

            ?>
        </div>

    </div>

    <h3 class="text-center"><?php echo Yii::t('site', 'Regulamin');?></h3>

    <div class="row">
        <table class="detail-view">
            <tr class="odd">
                <td><?php echo $form->labelEx($model->courierTypeExternalReturn,'regulations'); ?>
                    <?php echo $form->checkbox($model->courierTypeExternalReturn, 'regulations'); ?>
                    <?php echo $form->error($model->courierTypeExternalReturn,'regulations'); ?></td>
            </tr>
            <tr class="even">
                <td><?php echo $form->labelEx($model->courierTypeExternalReturn,'regulations_rodo'); ?>
                    <?php echo $form->checkbox($model->courierTypeExternalReturn, 'regulations_rodo'); ?>
                    <?php echo $form->error($model->courierTypeExternalReturn,'regulations_rodo'); ?></td>
            </tr>
        </table>


    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step4', 'class' => 'btn btn-sm'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę'),array('name'=>'finish', 'class' => 'btn btn-success'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę i wróć do formularza'),array('name'=>'finish_goback', 'class' => 'btn-primary'));

        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p><?php echo Yii::t('courier','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>
    </div>
</div><!-- form -->