<?php

class CourierOoeController extends Controller {

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function beforeAction($action)
    {
//        if(Yii::app()->user->id != 8) {
//            $this->render('//site/notify', array(
//                'text' => Yii::t('courier', 'Usługa chwilowo niedostępna. Zaprazamy za chwilę.'),
//                'header' => 'Courier OOE',
//
//            ));
//            exit;
//        }

        if(CourierTypeOoe::isOooAvailableForUser() && Yii::app()->user->model->getCourierOoeAvailable())
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier OOE',
            ));
            exit;
        }

    }

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('create','created','index', 'list', 'view', 'ajaxGetServiceDescription', 'import',  'ajaxRemoveRowOnImport', 'ajaxEditRowOnImport', 'ajaxSaveRowOnImport', 'listImported', 'quequeLabelByLocalId', 'getLabels', 'ebay', 'ajaxUpdateRowItemOnImport', 'ajaxImportModalSettings', 'ajaxCheckIfAvailable'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect('/courier/courier/');
    }

    public function actionAjaxGetServiceDescription()
    {
        if (Yii::app()->request->isAjaxRequest) {

            /* @var $model CourierOoeService */
            $model = CourierOoeService::model()->findByAttributes(array('service_name' => $_POST['service_name']));

            if ($model == NULL)
                return false;

            $description = $model->courierOoeServiceTr->text;
            $description = S_Useful::correctImgPathInText($description);

            $palettways = OneWorldExpressPackage::isPalletways($model->service_name);


            $return = ['desc' => $description, 'palletways' => $palettways ? 1 : 0];


            echo CJSON::encode($return);
        }
        exit;
    }

    /**
     * Fill form with cloned package data
     *
     * @param Courier $model
     */
    protected function _clone_ooe_fill()
    {

        if(isset(Yii::app()->session['cloneTypeOoe'])) {

            $data = Yii::app()->session['cloneTypeOoe'];
            unset(Yii::app()->session['cloneTypeOoe']);

            $this->setPageState('step1_model', $data['model']);
            $this->setPageState('step1_courierTypeOoe', $data['modelTypeOoe']);
            $this->setPageState('step1_senderAddressData', $data['senderAddressData']);
            $this->setPageState('step1_receiverAddressData', $data['receiverAddressData']);
            $this->setPageState('courier_additions', $data['additions']);
        }
    }



    public function actionCreate() {

        $this->panelLeftMenuActive = 121;

        if(!CourierTypeOoe::areServicesAvailableForUser())
        {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada aktywnych opcji dla tej usługi. Skontaktuj się z nami w tej sprawie.'),
                'header' => 'Courier OOE',

            ));
            exit;
        }

        // flashes do not like redirects to clear page states, so keep them in session before that
        $tempFlashes = Yii::app()->session['tempFlashContainer'];
        if(is_array($tempFlashes) && S_Useful::sizeof($tempFlashes))
        {
            unset(Yii::app()->session['tempFlashContainer']);
            foreach($tempFlashes AS $key => $value)
                Yii::app()->user->setFlash($key, $value);
        }

        $this->_clone_ooe_fill();

        if(Yii::app()->session['courierOoeFormFinished']) // prevent going back and sending the same form again
        {
            Yii::app()->session['tempFlashContainer'] = Yii::app()->user->getFlashes();

            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['courierOoeFormFinished']);

            $this->refresh(true);
        }

        if(isset($_POST['summary']))
            $this->create_step_summary(); // Validate addition list, summary
        elseif (isset($_POST['finish']))
            $this->create_finish();
        elseif (isset($_POST['forceReload']))
            $this->create_step_1(true);
        else
            $this->create_step_1(); // this is the default, first time (step1)
    }

    protected function create_step_1($validate = false)
    {
        if($validate)
        {
            if($this->create_validate_step_1() == 'break')
                return;
        }

        $this->setPageState('form_started', true);
        $this->setPageState('start_user_id', Yii::app()->user->id);

        $this->setPageState('last_step',1); // save information about last step

        $model = new Courier_CourierTypeOoe('step1');

        $model->user_id = Yii::app()->user->id;

        $model->courierTypeOoe = new CourierTypeOoe('step_1');

        $model->courierTypeOoe->courier = $model;

        $model->senderAddressData = new CourierTypeOoe_AddressData('insert', UserContactBook::TYPE_SENDER);
        $model->receiverAddressData = new CourierTypeOoe_AddressData('insert', UserContactBook::TYPE_RECEIVER);

        // load user data as sender
        if(!S_Useful::sizeof($this->getPageState('step1_senderAddressData',array())))
            $model->senderAddressData->loadUserData();
        else
            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());


        $model->attributes = $this->getPageState('step1_model',[]);
        $model->courierTypeOoe->attributes = $this->getPageState('step1_courierTypeOoe', []);

        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);

        $modelCourierAddition = $this->getPageState('courier_additions', []);
        $modelCourierOoeAdditionList = CourierOoeAdditionList::model()->findAllByAttributes(['stat' => S_Status::ACTIVE]);

        $additionsHtml = $this->renderPartial('create/_additionList', array(
            'modelCourierOoeAdditionList'=>$modelCourierOoeAdditionList,
            'courierAddition'=>$modelCourierAddition,
        ), true, false);


        $this->setPageState('last_step',1); // save information about last step

        $this->render('create/step1',
            [
                'model' => $model,
                'additionsHtml' => $additionsHtml,
            ]);

    }

    protected function create_validate_step_1()
    {

        $model = new Courier_CourierTypeOoe('step1');
        $model->courierTypeOoe = new CourierTypeOoe('step_1');
        $model->courierTypeOoe->courier = $model;
        $model->user_id = Yii::app()->user->id;

        $model->senderAddressData = new CourierTypeOoe_AddressData();
//        $model->senderAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_SENDER);
        $model->receiverAddressData = new CourierTypeOoe_AddressData();
//        $model->receiverAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_RECEIVER);


        if(isset($_POST['Courier_CourierTypeOoe']))
        {
            $errors = false;
            $noCarraige = false;

            $courierAddition = [];

            $model->attributes = $_POST['Courier_CourierTypeOoe'];
            $model->courierTypeOoe->attributes = $_POST['CourierTypeOoe'];
            $model->senderAddressData->attributes = $_POST['CourierTypeOoe_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['CourierTypeOoe_AddressData']['receiver'];

            // Courier additions:
            if(is_array($_POST['CourierAddition']))
                foreach($_POST['CourierAddition'] AS $item) {
                    $temp = CourierOoeAdditionList::model()->findByPk($item);
                    if ($temp !== null) {

                        array_push($courierAddition, $item);
                    }
                }

            if(!is_array($courierAddition))
                $courierAddition = [];


            $this->setPageState('step1_model',$model->getAttributes(array_keys(($_POST['Courier_CourierTypeOoe'])))); // save previous form into form state
            $this->setPageState('step1_courierTypeOoe',$model->courierTypeOoe->getAttributes((array_keys($_POST['CourierTypeOoe'])))); // save previous form into form state
            $this->setPageState('step1_senderAddressData', $model->senderAddressData->getAttributes(array_keys($_POST['CourierTypeOoe_AddressData']['sender']))); // save previous form into form state
            $this->setPageState('step1_receiverAddressData',$model->receiverAddressData->getAttributes(array_keys($_POST['CourierTypeOoe_AddressData']['receiver']))); // save previous form into form state
            $this->setPageState('courier_additions',$courierAddition);


            if($model->courierTypeOoe->isItTooBig())
            {
                Yii::app()->user->setFlash('error', Yii::t('courier','Paczka jest zbyt duża!'));
                $errors = true;
            }

            if($model->courierTypeOoe->isItTooHeavy())
            {
                Yii::app()->user->setFlash('error', Yii::t('courier','Paczka jest zbyt ciężka!'));
                $errors = true;
            }


            if(!$model->validate())
                $errors = true;

            if(!$model->courierTypeOoe->validate())
                $errors = true;


            if(!$model->senderAddressData->validate())
                $errors = true;

            if(!$model->receiverAddressData->validate())
                $errors = true;

            if(!$errors)
            {
                $countries = CHtml::listData(CourierTypeOoe::getCountryList($model->courierTypeOoe->courier_ooe_service_id),'id', 'id');

                if (!in_array($model->receiverAddressData->country_id, $countries))
                {
                    $noCarraige = true;
                    $errors = true;
                }

            }

        }

        $modelCourierAddition = $this->getPageState('courier_additions', []);
        $modelCourierOoeAdditionList = CourierOoeAdditionList::model()->findAllByAttributes(['stat' => S_Status::ACTIVE]);

        $additionsHtml = $this->renderPartial('create/_additionList', array(
            'modelCourierOoeAdditionList'=>$modelCourierOoeAdditionList,
            'courierAddition'=>$modelCourierAddition,
        ), true, false);

        if($errors)
        {
            $this->render('create/step1',
                [

                    'model' => $model,
                    'noCarriage' => $noCarraige,
                    'additionsHtml' => $additionsHtml,
                ]);
            return 'break';
        }
    }

    protected function create_step_summary()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');

        switch($lastStep)
        {
            case 1 :
                if($this->create_validate_step_1() == 'break')
                    return;
                break;
        }

        $model = new Courier_CourierTypeOoe();
        $model->courierTypeOoe = new CourierTypeOoe('summary');
        $model->courierTypeOoe->courier = $model;

        $model->senderAddressData = new CourierTypeOoe_AddressData();
        $model->receiverAddressData = new CourierTypeOoe_AddressData();

        $model->attributes = $this->getPageState('step1_model',[]);
        $model->courierTypeOoe->attributes = $this->getPageState('step1_courierTypeOoe', []);
        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);

        $additions = $this->getPageState('courier_additions', []);

        $model->courierTypeOoe->addAdditions($additions);

        $model->user_id = Yii::app()->user->id;



        $this->setPageState('last_step', 'summary'); // save information about last step

        $this->render('create/summary',array(
            'model'=>$model,
        ));
    }

    protected function create_validate_summary()
    {
        $errors = false;


        $model = new Courier_CourierTypeOoe();
        $model->courierTypeOoe = new CourierTypeOoe('summary');

        $model->courierTypeOoe->courier = $model;

        $model->senderAddressData = new CourierTypeOoe_AddressData();
        $model->receiverAddressData = new CourierTypeOoe_AddressData();


        $model->attributes = $this->getPageState('step1_model',[]);
        $model->courierTypeOoe->attributes = $this->getPageState('step1_courierTypeOoe', []);
        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);

        $additions = $this->getPageState('courier_additions', []);

        $model->courierTypeOoe->addAdditions($additions);

        $model->user_id = Yii::app()->user->id;


        $model->courierTypeOoe->regulations = $_POST['CourierTypeOoe']['regulations'];
        if(!$model->courierTypeOoe->validate(array('regulations')))
            $errors = true;


        if($errors)
        {
            $this->render('create/summary',array(
                'model'=>$model,
            ));
            return 'break';
        }

    }

    protected function create_finish()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 'summary')
        {

            if($this->create_validate_summary() == 'break')
                return;

            $this->setPageState('last_step',6); // save information about last step

            $model = new Courier_CourierTypeOoe('insert');
            $model->courierTypeOoe = new CourierTypeOoe();
            $model->courierTypeOoe->courier = $model;
            $model->courierTypeOoe->attributes = $this->getPageState('step1_courierTypeOoe', []);

            $model->attributes = $this->getPageState('step1_model',array()); //get the info from step 1
            $model->receiverAddressData = new CourierTypeOoe_AddressData();
            $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);
            $model->senderAddressData = new CourierTypeOoe_AddressData();
            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
            $model->courierTypeOoe->addAdditions($this->getPageState('courier_additions',array()));


            // additional params:
//            $params = $this->getPageState('step4_courierTypeOoe_params', array());
//
//            foreach($params AS $key => $item)
//            {
//                $model->courierTypeOoe->$key = $item;
//            }

            // return order
            $model->user_id = Yii::app()->user->id;

            try
            {
                $return = $model->courierTypeOoe->saveNewPackage(true);
            }
            catch(Exception $ex)
            {
                Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
                throw new CHttpException(403, 'Wystapił nieznany błąd!');
            }

            $message = '';
            // label quequed
            if($return instanceof CourierTypeOoe)
            {
                $status = true;
                // order labels for all of found
                /* @var $item CourierTypeDomestic */
                CourierLabelNew::asyncCallForId($return->courier_label_new_id);

            } else {


                list($status, $message) = $return;
            }

            if($status)
            {
                Yii::app()->session['courierFormFinished'] = true;

                /* @var $order Order */
                $model = Courier::model()->findByPk($model->id); // to get proper hash

                Yii::app()->user->setFlash('success', Yii::t('courier', 'Paczka została zapisana! {link}', ['{link}' => CHtml::link('#'.$model->local_id, Yii::app()->createUrl('/courier/courier/view', ['urlData' => $model->hash]),['target' => '_blank', 'class' => 'btn btn-sm'])]));
                $this->redirect(array('/courier/courierOoe/create'));

            } else {

                $this->render('create/errorPage', array('text' => $message));
                exit;
            }
            throw new CHttpException(403, 'Wystapił nieznany błąd!');


        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }

    public function actionAjaxCheckIfAvailable()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $operator = $_POST['operator'];
            $receiver_country_id = $_POST['receiver_country_id'];

            $countries = CHtml::listData(CourierTypeOoe::getCountryList($operator),'id', 'id');

            if($operator == '')
                $result = false;
            else {

                if (in_array($receiver_country_id, $countries))
                    $result = true;
                else
                    $result = false;
            }

            echo CJSON::encode($result);
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }
    }




    public function actionView($urlData) {
        {
            $this->redirect(Yii::app()->createUrl('/courier/courier/view', array('urlData' => $urlData)));
            exit;
        }
    }

############################################################################################################
############################################################################################################
############################################################################################################


    public function actionQuequeLabelByLocalId()
    {

        $local_id = $_POST['localId'];

        Yii::app()->getModule('courier');

        $courier = Courier::model()->findByAttributes(array('local_id' => $local_id));

        $parent = CourierLabelNew::model()->findByAttributes(array('courier_id' => $courier->id));

        if($parent instanceof CourierLabelNew)
        {
            if($parent->stat == CourierLabelNew::STAT_NEW) {
                $ids = $parent->runLabelOrdering(true);
            } else {
                if($parent->_ebay_order_id != '')
                    $parent->__temp_ebay_update_stat = EbayImport::STAT_UNKNOWN;
                $ids = [$parent->id];
            }

            $log = '';
            if($parent->stat != CourierLabelNew::STAT_SUCCESS)
                $log = $parent->log;

            $return = array(
                'request' => true,
                'stat' => $parent->stat,
                'ids' => $ids,
                'desc' => $log
            );

            if($parent->_ebay_order_id != '')
                $return['ebayImport'] = $parent->__temp_ebay_update_stat;


            echo CJSON::encode($return);
            exit;
        } else {
            echo CJSON::encode(array('request' => false));
            exit;
        }
    }

    public function actionImport($hash = NULL)
    {

        $this->panelLeftMenuActive = 122;

        $fileModel = new F_OoeImport();


        if($hash !== NULL)
        {
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
            Yii::import('zii.behaviors.CTimestampBehavior');


            $models = Yii::app()->cacheUserData->get('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID);

            if(isset($_POST['save']))
            {

                if(F_OoeImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
                    Yii::app()->cacheUserData->set('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                    $this->refresh();
                    exit;
                }


                try
                {
                    $result = CourierTypeOoe::saveWholeGroup($models);
                }
                catch (Exception $ex)
                {
                    Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
                    return;
                }
                // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING


                if($result['success'])
                {
                    $import = CourierImportManager::getByHash($hash);
                    /* @var $item CourierTypeOoe */
                    $ids = array();
                    if(is_array($result['data']))
                        foreach($result['data'] AS $item)
                        {
                            array_push($ids, $item->courier_id);
                        }

                    $import->addCourierIds($ids);
                    $import->save();

                    Yii::app()->cacheUserData->delete('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID);
                    $this->redirect(Yii::app()->createUrl('/courier/courierOoe/listImported', ['hash' => $hash]));
                    exit;
                }

            } else {

                // validation is done before
//                if(is_array($models))
//                    $models = F_OoeImport::validateModels($models);
//                else
//                    $models = false;

                if (!$models OR !S_Useful::sizeof($models)) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Import nie powiódł się!'));
                    $this->redirect(Yii::app()->createUrl('courier/courierOoe/import'));
                }

                $this->render('import/showModels', array(
                    'models' => $models,
                    'hash' => $hash,
                ));
                return;
            }
        }


        // file upload
        if(isset($_POST['F_OoeImport']) ) {
            $fileModel->setAttributes($_POST['F_OoeImport']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if (!$fileModel->validate()) {
                $this->render('import/index', array(
                    'model' => $fileModel,
                ));
                return;
            } else {
                // file upload success:

                if($fileModel->custom_settings)
                    $fileModel->applyCustomData($fileModel->custom_settings, Yii::app()->user->id);

                // generate new hash
                $hash = hash('sha256', time() . Yii::app()->user->id);

                $inputFile = $fileModel->file->tempName;

                Yii::import('application.extensions.phpexcel.XPHPExcel');
                XPHPExcel::init();

                $inputFileType = PHPExcel_IOFactory::identify($inputFile);


                // WITH FIX:
                if(strtoupper(($inputFileType) == 'CSV'))
                {
                    // SET UTF8 ENCODING IN CSV
                    $file_content = file_get_contents( $inputFile );

                    $file_content = S_Useful::forceToUTF8($file_content);
                    file_put_contents( $inputFile, $file_content );

                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                    // FIX ENCODING

                    if($fileModel->_customSeparator)
                        $objReader->setDelimiter($fileModel->_customSeparator);


                } else {
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                }


                // WITHOUT FIX:
//
//                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//                if(strtoupper($inputFileType) == 'CSV' && $fileModel->_customSeparator)
//                {
//                    $objReader->setDelimiter($fileModel->_customSeparator);
//                }

                $objPHPExcel = $objReader->load($inputFile);

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $data = [];
                $start = 1;

                if ($fileModel->omitFirst) {
                    $headerSeen = false;
                } else
                    $headerSeen = true;

                for ($row = $start; $row <= $highestRow; $row++) {
                    if (!$headerSeen) {
                        $headerSeen = true;
                        continue;
                    }

                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                    array_push($data, $rowData[0]);
                }

                // remove empty lines:
                $data = (array_filter(array_map('array_filter', $data)));

                $numberOfLines = S_Useful::sizeof($data);

                // check just by lines number
                if($numberOfLines > F_OoeImport::MAX_LINES)
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość wierszy w pliku to: {no}! Naliczono: {number}', array('{no}' => F_OoeImport::MAX_LINES, '{number}' => $numberOfLines)));
                    $this->refresh(true);
                    exit;
                }

                $numberOfPackages = F_OoeImport::calculateTotalNumberOfPackages($data, $fileModel->_customOrder);
                // check by number of packages
                if($numberOfPackages > F_OoeImport::MAX_PACKAGES) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość paczek w pliku to: {no}! Naliczono: {number}', array('{no}' => F_OoeImport::MAX_PACKAGES, '{number}' => $numberOfPackages)));
                    $this->refresh(true);
                    exit;
                }

                $models = F_OoeImport::mapAttributesToModels($data, $fileModel->ooe_operator_id, $fileModel->_customOrder);
                F_OoeImport::validateModels($models);

                Yii::app()->cacheUserData->set('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                $this->redirect(Yii::app()->createUrl('courier/courierOoe/import', ['hash' => $hash]));
                exit;
            }
        }

        $this->render('import/index', array(
            'model' => $fileModel,
        ));

    }

    public function actionListImported($hash)
    {

        $this->panelLeftMenuActive = 120;

        $import = CourierImportManager::getByHash($hash);

        if($import->user_id != Yii::app()->user->id)
            throw new CHttpException(404);

        if(!$import)
            throw new CHttpException(404);

        // OLDER WAY:
//        $couriers = [];
//        if(is_array($import->courier_ids))
//            foreach($import->courier_ids AS $courier_id)
//            {
//                array_push($couriers, Courier::model()->with('courierTypeOoe')->with('courierTypeOoe.courierLabelNew')->findByPk($courier_id));
//            }


        // FASTER WAY:
        $couriers = [];
        if(is_array($import->courier_ids))
            $couriers = Courier::model()->with('courierTypeOoe')->with('courierTypeOoe.courierLabelNew')->findAllByPk($import->courier_ids);

        $this->render('listImported',array(
            'import' => $import,
            'couriers' => $couriers,
            'hash' => $hash,
        ));

    }

    public function actionAjaxRemoveRowOnImport($e = false)
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        if($e)
        {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImport_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);

            unset($models[$id]);
            Yii::app()->cacheUserData->set('ebayImport_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);

        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID);
            unset($models[$id]);

            Yii::app()->cacheUserData->set('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        }

        echo CJSON::encode(array('success' => true));
    }

    public function actionAjaxEditRowOnImport($e = false)
    {

        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        if($e)
        {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImport_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);

        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID);
        }

        $model = $models[$id];

        if(isset($model)) {

            $countries = courierTypeOoe::getCountryList($model->courierTypeOoe->courier_ooe_service_id);

            if($e)
                $html = $this->renderPartial('importEbay/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'e' => $e], true, true);
            else
                $html = $this->renderPartial('import/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'e' => $e], true, true);

//            file_put_contents('htmldump.txt', print_r($html,1));

            echo CJSON::encode(array('success' => true, 'html' => $html));
        } else {
            echo CJSON::encode(array('success' => false));
        }

    }

    public function actionAjaxUpdateRowItemOnImport($e = false)
    {
        if(Yii::app()->request->isAjaxRequest) {


            $hash = $_POST['hash'];
            $i = $_POST['i'];
            $attribute = $_POST['attribute'];
            $val = $_POST['val'];

            $possibleToEdit = [
                'receiverAddressData.name',
                'receiverAddressData.company',
                'receiverAddressData.zip_code',
                'receiverAddressData.address_line_1',
                'receiverAddressData.address_line_2',
                'receiverAddressData.city',
                'receiverAddressData.tel',
                'receiverAddressData.email',
                'senderAddressData.name',
                'senderAddressData.company',
                'senderAddressData.zip_code',
                'senderAddressData.address_line_1',
                'senderAddressData.address_line_2',
                'senderAddressData.city',
                'senderAddressData.tel',
                'senderAddressData.email',
                'package_weight',
                'package_size_l',
                'package_size_d',
                'package_size_w',
                'courierTypeOoe.courier_ooe_service_id'
            ];

            if(!in_array($attribute, $possibleToEdit))
                return false;

            try {
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
                Yii::import('zii.behaviors.CTimestampBehavior');

                if($e)
                {
                    // from ebay import
                    $models = Yii::app()->cacheUserData->get('ebayImport_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);

                } else {
                    // from file import
                    $models = Yii::app()->cacheUserData->get('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID);
                }

                /* @var $model Courier */
                if (is_array($models)) {
                    $model = $models[$i];

                    $submodel = false;
                    if ($model) {

                        $attribute = explode('.', $attribute);
                        if(S_Useful::sizeof($attribute) > 1)
                        {
                            $submodel = $attribute[0];

                            // $model = $model->$attribute[0];
                            // PHP7 fix
                            $temp = $attribute[0];
                            $model = $model->$temp;

                            $attribute = $attribute[1];
                        } else
                            $attribute = $attribute[0];


                        if ($model->hasAttribute($attribute)) {
                            $backupValue = $model->$attribute;
                            $model->$attribute = $val;

                            $model->validate([$attribute]);
                            if (!$model->hasErrors($attribute)) {

                                if($submodel)
                                    $models[$i]->$submodel = $model;
                                else
                                    $models[$i] = $model;


                                if($e)
                                    Yii::app()->cacheUserData->set('ebayImport_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                                else
                                    Yii::app()->cacheUserData->set('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                                echo CJSON::encode(['success' => true]);
                                return;

                            } else {

                                echo CJSON::encode([
                                    'success' => false,
                                    'msg' => strip_tags($model->getError($attribute)),
                                    'backup' => $backupValue,
                                ]);
                                return;
                            }
                        }
                    }
                }

                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie. {msg}', ['{msg}' => print_r($attribute,1)])
                ]);
                return;

            } catch (Exception $ex) {
                Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);
                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie')
                ]);
                return;

            }
        }
    }

    public function actionAjaxSaveRowOnImport($e = false)
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        if($e)
        {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImport_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);
        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID);
        }
        $model = $models[$id];

        if(isset($model)) {

            if($e)
            {
                /* @var $model Courier_CourierTypeOoe */
                $model->courierTypeOoe->attributes = $_POST['CourierTypeOoe'];
                $model->senderAddressData->attributes = $_POST['CourierTypeOoe_AddressData']['sender'];
                $model->receiverAddressData->attributes = $_POST['CourierTypeOoe_AddressData']['receiver'];

                $model->senderAddressData->country0 = CountryList::model()->findByPk($model->senderAddressData->country_id);
                $model->receiverAddressData->country0 = CountryList::model()->findByPk($model->receiverAddressData->country_id);

                $model->attributes = $_POST['Courier_CourierTypeOoe'];

                $model->validate();
                $model->courierTypeOoe->validate();
                $model->senderAddressData->validate();
                $model->receiverAddressData->validate();

                $models[$id] = $model;


                // from ebay import
                Yii::app()->cacheUserData->set('ebayImport_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            } else {

                /* @var $model Courier_CourierTypeOoe */
                $model->courierTypeOoe->attributes = $_POST['CourierTypeOoe'];
                $model->senderAddressData->attributes = $_POST['CourierTypeOoe_AddressData']['sender'];
                $model->receiverAddressData->attributes = $_POST['CourierTypeOoe_AddressData']['receiver'];

                $model->senderAddressData->country0 = CountryList::model()->findByPk($model->senderAddressData->country_id);
                $model->receiverAddressData->country0 = CountryList::model()->findByPk($model->receiverAddressData->country_id);

                $model->attributes = $_POST['Courier_CourierTypeOoe'];

                $model->validate();
                $model->courierTypeOoe->validate();
                $model->senderAddressData->validate();
                $model->receiverAddressData->validate();

                $models[$id] = $model;

                // from file import
                Yii::app()->cacheUserData->set('ooeImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            }

            echo CJSON::encode(array('success' => true));
        } else {
            echo CJSON::encode(array('success' => false));
        }

    }


    public function actionGetLabels($hash)
    {
        if(isset($_POST['label']))
        {
            $couriers = Courier::model()->findAllByAttributes(['id' => array_keys($_POST['label']), 'user_id' => Yii::app()->user->id]);

            if(S_Useful::sizeof($couriers)) {
                LabelPrinter::generateLabels($couriers, $_POST['Courier']['type'],true);
                exit;
            }
        }

        Yii::app()->user->setFlash('error', Yii::t('courier','Nie znaleziono żadnych etykiet do wygenerowania!'));
        $this->redirect(Yii::app()->createUrl('/courier/courierOoe/listImported', ['hash' => $hash]));
        exit;
    }


    public function actionEbay($al = false, $hash = false)
    {
        $this->panelLeftMenuActive = 123;

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');



        // autoload list once on enter
        if($al)
        {
            Yii::app()->session->add('ebay_autoload', true);
            $this->redirect(['/courier/courierOoe/ebay']);
            exit;
        }


        // initialize ebayClient
        $ebayClient = ebayClient::instance();

        if($hash)
        {
            if(isset($_POST['save']))
            {
                $this->_ebay_step3_process($ebayClient, $hash);
                exit;
            } else
                $this->_ebay_step3($ebayClient, $hash);
        }
        else if(isset($_POST['import']))
        {
            if($this->_ebay_step1_process($ebayClient) === 'true')
                $this->_ebay_step2($ebayClient);
            else
                $this->_ebay_step1($ebayClient);

        }
        else if(isset($_POST['save-default-data']))
        {
            if($this->_ebay_step2_process($ebayClient) === 'true')
                $this->_ebay_step3($ebayClient, $hash);

        }
        else
        {
            $this->_ebay_step1($ebayClient);
        }
    }


    protected function _ebay_step1(ebayClient $ebayClient)
    {
        $orders = [];

        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $stat = (int) $_POST['stat'];
        $statEbay = (int) $_POST['statEbay'];

        if($date_from == '')
            $date_from = Yii::app()->session->get('ebay_date_from', NULL);

        if($date_to == '')
            $date_to = Yii::app()->session->get('ebay_date_to', NULL);

        if(!$stat)
            $stat = Yii::app()->session->get('stat', NULL);

        if(!$statEbay)
            $statEbay = Yii::app()->session->get('statEbay', NULL);

        $date_begining = date('Y-m-d', strtotime('-'.ebayClient::ORDERS_GOBACK_MONTHS.' months'));
        $date_end = date('Y-m-d');

        if (strtotime($date_from) > strtotime($date_to))
            $date_from = $date_to;

        if (!strtotime($date_from) OR strtotime($date_from) < strtotime($date_begining))
            $date_from = $date_end;

        if (!strtotime($date_to) OR strtotime($date_to) > strtotime($date_end))
            $date_to = $date_end;

        if (!in_array($stat, array_keys(EbayOrderItem::getStatShowProcessedList())))
            $stat = EbayOrderItem::STAT_SHOW_PROCESSED_ALL;

        if (!in_array($statEbay, array_keys(EbayOrderItem::getStatShowEbayList())))
            $statEbay = EbayOrderItem::STAT_SHOW_EBAY_READY;

        $more = false;
        // on load order
        if((isset($_POST['get-data']) OR Yii::app()->session->get('ebay_autoload', false)) && $ebayClient->isLogged()) {

            Yii::app()->session->add('ebay_date_from', $date_from);
            Yii::app()->session->add('ebay_date_to', $date_to);
            Yii::app()->session->add('stat', $stat);
            Yii::app()->session->add('statEbay', $statEbay);

            Yii::app()->session->add('ebay_autoload', false);
            list($orders, $more) = $ebayClient->getOrders($date_from, $date_to, $stat, $statEbay);
            Yii::app()->cacheUserData->set('ebayImport_'.$ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $orders, 86400);
        }

        $number = S_Useful::sizeof($orders);

        $this->render('importEbay/step_1', [
            'ebayClient' => $ebayClient,
            'orders' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'stat' => $stat,
            'statEbay' => $statEbay,
            'date_begining' => $date_begining,
            'date_end' => $date_end,
            'number' => $number,
            'more' => $more,
        ]);
    }

    protected function _ebay_step1_process(ebayClient $ebayClient)
    {
        $ordersToImport = [];
        $ordersCached = Yii::app()->cacheUserData->get('ebayImport_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);

        if (is_array($_POST['import'])) {
            foreach ($_POST['import'] AS $importKey => $temp) {
                $temp = $ordersCached[$importKey];

                /* @var $temp EbayOrderItem */
                // prevent already processed in system
//                if(CourierEbayImport::findActivePackageByEbayOrderId($temp->orderID) === false) {
                if($temp instanceof EbayOrderItem) {
                    $temp = $temp->convertToCourierTypeOoeModel($_POST['ooe_operator_id']);
                    array_push($ordersToImport, $temp);
//                }
                } else {
                    throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));
                }

            }

            if(S_Useful::sizeof($ordersToImport) > EbayImportOoe::MAX_IMPORT_NUMBER)
            {
                Yii::app()->user->setFlash('error', Yii::t('courier_ebay', 'Maksymalna ilość paczek do importu to: {no}!', array('{no}' => EbayImportOoe::MAX_IMPORT_NUMBER)));
                Yii::app()->session->add('ebay_autoload', true);
                $this->refresh(true);
                exit;
            }

            Yii::app()->cacheUserData->set('ebayImport_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            return "true";
        }
    }

    protected function _ebay_step2(ebayClient $ebayClient)
    {
        $senderAddressData = new CourierTypeOoe_AddressData;
        $package = new Courier_CourierTypeOoe('default_package_data');
        $ebayImport = new EbayImportOoe();

        $package->package_size_l = 10;
        $package->package_size_w = 10;
        $package->package_size_d = 10;
        $package->package_weight = 1;
        $package->package_value = 10;


        // set default operator
        $ebayImport->ooe_operator_id = 84; // DHLDE

        $senderAddressData->attributes= User::getLoggedUserAddressData();

        $this->render('importEbay/step_2', [
            'ebayClient' => $ebayClient,
            'senderAddressData' => $senderAddressData,
            'package' => $package,
            'ebayImport' => $ebayImport,
        ]);
    }

    protected function _ebay_step2_process(ebayClient $ebayClient)
    {

        $senderAddressData = new CourierTypeOoe_AddressData;
        $package = new Courier_CourierTypeOoe('default_package_data');
        $ebayImport = new EbayImportOoe();

        $senderAddressData->attributes = $_POST['CourierTypeOoe_AddressData'];
        $package->attributes = $_POST['Courier_CourierTypeOoe'];
        $ebayImport->attributes = $_POST['EbayImportOoe'];

        $senderAddressData->validate();
        $package->validate();
        $ebayImport->validate();

        if(!$senderAddressData->hasErrors() && !$package->hasErrors() && !$ebayImport->hasErrors()) {

            $ordersToImport = Yii::app()->cacheUserData->get('ebayImport_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, []);

            if(is_array($ordersToImport) && S_Useful::sizeof($ordersToImport))
            {
                /* @var $model Courier_CourierTypeOoe */
                foreach($ordersToImport AS &$order)
                {
                    $order->senderAddressData = new CourierTypeOoe_AddressData;
                    $order->senderAddressData->attributes = $senderAddressData->attributes;
                    $order->courierTypeOoe->courier_ooe_service_id = $ebayImport->ooe_operator_id;
                    $order->package_weight = $package->package_weight;
                    $order->package_size_l = $package->package_size_l;
                    $order->package_size_w = $package->package_size_w;
                    $order->package_size_d = $package->package_size_d;
                    $order->package_value = $package->package_value;
                }

                F_OoeImport::validateModels($ordersToImport);

                Yii::app()->cacheUserData->delete('ebayImport_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);
                Yii::app()->cacheUserData->set('ebayImport_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            }

            return "true";
        }

        $this->render('importEbay/step_2', [
            'ebayClient' => $ebayClient,
            'senderAddressData' => $senderAddressData,
            'package' => $package,
            'ebayImport' => $ebayImport,
        ]);
    }

    protected function _ebay_step3($ebayClient, $hash)
    {
        if(!$hash)
        {
            // generate new hash
            $hash = hash('sha256', time() . $ebayClient->getSessionId());
            $ordersToImport = Yii::app()->cacheUserData->get('ebayImport_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));

            // rename data by hash and clear previous
            Yii::app()->cacheUserData->set('ebayImport_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            Yii::app()->cacheUserData->delete('ebayImport_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);

            $this->redirect(['/courier/courierOoe/ebay', 'hash' => $hash]);
            exit;

        } else {
            $ordersToImport = Yii::app()->cacheUserData->get('ebayImport_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));

            // validation is done before
//            if (S_Useful::sizeof($ordersToImport))
//                $ordersToImport = F_OoeImport::validateModels($ordersToImport);

            $this->render('importEbay/step_3', [
                'models' => $ordersToImport,
                'hash' => $hash,
            ]);
            return;
        }

    }


    protected function _ebay_step3_process(ebayClient $ebayClient, $hash)
    {
        $models = Yii::app()->cacheUserData->get('ebayImport_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));

        if(F_OoeImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
            Yii::app()->cacheUserData->set('ebayImport_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            $this->refresh();
            exit;
        }
        try
        {
            $result = CourierTypeOoe::saveWholeGroup($models, CourierTypeOoe::SOURCE_EBAY, $ebayClient->getAuthToken(), true);
        }
        catch (Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            return;
        }

        // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING
        if($result['success'])
        {
            $import = CourierImportManager::getByHash($hash);
            /* @var $item CourierTypeOoe */
            $ids = array();
            if(is_array($result['data']))
                foreach($result['data'] AS $item)
                {
                    array_push($ids, $item->courier_id);
                }

            $import->source = CourierImportManager::SOURCE_EBAY;
            $import->addCourierIds($ids);
            $import->save();

            Yii::app()->cacheUserData->delete('ebayImport_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
            $this->redirect(Yii::app()->createUrl('/courier/courierOoe/listImported', ['hash' => $hash]));
            exit;
        }
    }


}