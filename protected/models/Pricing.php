<?php

Yii::import('application.models._base.BasePricing');

class Pricing extends BasePricing
{
	const PRODUCT_PP_LOCAL = 1;
    const PRODUCT_PP_ALTERNATIVE = 2;
    const PRODUCT_COURIER = 3;

    public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function getProductType()
    {
        $product = [];
        $product[self::PRODUCT_PP_LOCAL] = Yii::t('m_pricing', 'Postal Service - local');
        $product[self::PRODUCT_PP_ALTERNATIVE] = Yii::t('m_pricing', 'Postal Service - alternative');
        $product[self::PRODUCT_COURIER] = Yii::t('m_pricing', 'Courier');

        return $product;
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on'=>'insert, update'),

            array('product, top_weight, price', 'required'),
            array('product', 'numerical', 'integerOnly'=>true),
            array('price', 'numerical'),
            array('price, top_weight', 'numerical', 'min' => 0.01),
            array('id, product, top_weight, price, date_entered', 'safe', 'on'=>'search'),
        );
    }


    public function gridViewData($id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition("product = '$id'");

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pagesize' => 25,
            )

        ));
    }

    protected static function caulculatePricePerWeight($product, $weight_id)
    {
        $cmd = Yii::app()->db->createCommand();

        $cmd->select('price');
        $cmd->from('pricing');
        $cmd->andWhere('id = :id', array(':id' => $weight_id));
        $cmd->limit('1');

        $result = $cmd->queryScalar();

        return $result;
    }

    public static function calculatePrice($product, $weight_id, $options)
    {
        $price = 0;

        $price += self::caulculatePricePerWeight($product, $weight_id);
        $price += PricingOptions::caulculatePrice($product, $options, $weight_id);

        return S_Price::formatPrice($price).' zł';
    }

}