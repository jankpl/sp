<?php

class UserController extends Controller {

    public function filters()
    {
        return array_merge(array(
            'accessControl', // perform access control for CRUD operations
            array(
                'application.filters.HttpsFilter',
                'bypass' => false,
            ),
        ));
    }

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('captcha','accountCreated','reminder','resetPassword','captcha', 'ResetPasswordResult','activate','activationResult'),
                'users'=>array('?'),
            ),

            array('allow',
                'actions' => array('panel', 'update','view', 'index', 'complaint', 'documents', 'documentsGetFile', 'ackCardArchive', 'ackCardArchiveGetFile', 'deleteFavPoint'),
                'users' => array('@'),
            ),

            array('allow',
                'actions' => array('create'),
                'users' => array('*'),
            ),

            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }

    public function actionPanel()
    {

        $this->redirect(['/panel']);

//        $this->panelLeftMenuActive = 1;

//        $this->render('panel');
    }

    public function actionCreate()
    {
        if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI)
            throw new CHttpException(404);


        $this->layout = 'other';


        if(!Yii::app()->user->isGuest)
            $this->redirect('/user/panel');

        if(Yii::app()->session['userRegistrationFormFinished']) // prevent going back and sending the same form again
        {
            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['userRegistrationFormFinished']);
            $this->refresh(true);
        }


        //for AJAX validation
        $model=new User('insert');
        $this->performAjaxValidation($model, 'user-form');
        unset($model);



        if (isset($_POST['step2'])) {


            $lastStep =  $this->getPageState('last_step');


            if($lastStep === 1)
            {
                $this->setPageState('step1',$_POST['User']); // save step1 into form state
                $model=new User('step1');
                $model->attributes = $_POST['User'];

                if(!$model->validate())
                {
                    $this->render('create/step1',array('model'=>$model));
                    return;
                }
            }

            unset($model);
            $model=new User('step2');
            $model->attributes = $this->getPageState('step1', array()); // save information about last step

            if($model->type == User::TYPE_PRIVATE)
                $model->scenario = 'step2_private';
            else
                $model->scenario = 'step2_company';

            $this->setPageState('last_step',2); // save information about last step
            $model->attributes = $this->getPageState('step2',array());
            $this->render('create/step2',array('model'=>$model));


        } elseif (isset($_POST['summary'])) {


            $this->setPageState('last_step',3); // save information about last step

            $this->setPageState('step2',$_POST['User']); // save step2 into form state
            $model=new User('step2');


            $model->attributes = $this->getPageState('step1', array()); // save information about last step
            $model->attributes = $_POST['User']; // get the info from step2

            if($model->type == User::TYPE_PRIVATE)
                $model->scenario = 'step2_private';
            else
                $model->scenario = 'step2_company';

            if (!$model->validate())
            {
                $this->render('create/step2',array('model'=>$model));
            } else {
                $model->attributes = $this->getPageState('step1',array()); //get the info from step 1 for summary
                $this->render('create/summary',array('model'=>$model));
            }

        } elseif (isset($_POST['finish'])) {

            $this->setPageState('last_step',4); // save information about last step

            $model = new User('insert');
            $model->attributes = $this->getPageState('step1',array()); //get the info from step 1
            $model->attributes = $this->getPageState('step2',array()); //get the info from step 2

            if ($model->save())
            {
                Yii::app()->session['accountCreated'] = true;
                Yii::app()->session['userRegistrationFormFinished'] = true;
                $this->redirect(array('accountCreated'));
            } else {
                throw new CHttpException(400);
            }


        } else { // this is the default, first time (step1)

            $this->setPageState('last_step',1); // save information about last step

            $model=new User('step1');
            $model->attributes = $this->getPageState('step1',array());

            $this->render('create/step1',array('model'=>$model));
        }


    }



    public function actionAccountCreated()
    {
        $this->layout = 'other';

        if(!Yii::app()->session['accountCreated'])
        {
            $this->redirect(['/']);
        }
        unset(Yii::app()->session['accountCreated']);

        $this->render('create/accountCreated');



    }

    public function actionUpdate() {

        $this->panelLeftMenuActive = 701;

        $id = Yii::app()->user->id;

        /* @var $model User */
        $model = $this->loadModel($id, 'User');
        $model->scenario = 'update';
        $model->pass = '';

        $this->performAjaxValidation($model, 'user-form');


        $modelCAE = new UserCustomAccountingEmailForm;
        $modelCRRE = new UserCustomReturnReportEmailForm();

        $model->_defaultTelNo = $model->getDefaultTelNo();

        if(isset($_POST['toggleBlockNotify']))
        {
            $notify = $_POST['User']['blockNotify'];
            $model->setBlockNotify($notify);

            $notifySms = $_POST['User']['blockNotifySms'];
            $model->setBlockNotifySms($notifySms);

            $clearEmails = $_POST['User']['clearEmails'];
            $model->setClearEmails($clearEmails);

            $disableOrderNotification = $_POST['User']['disableOrderNotification'];
            $model->setDisableOrderNotification($disableOrderNotification);

            Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia blokowania powiadomień zostały zaktualizowane'));
            $this->redirect(array('update'));
            exit;

        }
        else if(isset($_POST['saveOther']))
        {
            $value = $_POST['User']['acceptRegulationsByDefault'];
            $model->setAcceptRegulationsByDefault($value);

            $value = $_POST['User']['fastFinalizeInternal'];
            $model->setFastFinalizeInternal($value);

            $value = $_POST['User']['activateNewPanel'];
            $model->setActivateNewPanel($value);

            Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia zostały zaktualizowane'));
            $this->redirect(array('update'));
            exit;

        }
        else if(isset($_POST['saveBankAccount']))
        {
            $text = $_POST['User']['bankAccount'];
            $model->setBankAccount($text);
            Yii::app()->user->setFlash('success', Yii::t('user', 'Dane Twojego racunku bankowego zostału zapisane.'));
            $this->redirect(array('update'));
            exit;

        }
        else if(isset($_POST['saveDefaultTel']))
        {
            $text = $_POST['User']['_defaultTelNo'];
            $model->scenario = 'updateDefaultTel';
            $model->_defaultTelNo = $text;
            if($model->validate(['_defaultTelNo']))
            {
                $model->setDefaultTelNo($text);
                Yii::app()->user->setFlash('success', Yii::t('user', 'Dane domyślnego numeru telefonu zostału zapisane.'));
                $this->redirect(array('update'));
                exit;
            }
        }
        else if(isset($_POST['savePackageDefault']))
        {
            if($model->setDefaultPackageDimensions(
                $_POST['User']['defaultPackageWeight'],
                $_POST['User']['defaultPackageSizeL'],
                $_POST['User']['defaultPackageSizeD'],
                $_POST['User']['defaultPackageSizeW'],
                $_POST['User']['defaultPackageContent'],
                $_POST['User']['defaultPackageValue'],
                $_POST['User']['defaultPackageWrapping'],
                $_POST['User']['defaultPackageReceiverCountryId'],
                $_POST['User']['defaultPackageValueCurrency']
            ))
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Domyślne dane paczki zostały zapisane.'));
                $this->redirect(array('update'));
                exit;
            }

        }
        else if(isset($_POST['savePackagePostalDefault']))
        {
            if($model->setDefaultPostalDimensions(
                $_POST['User']['defaultPostalType'],
                $_POST['User']['defaultPostalSize'],
                $_POST['User']['defaultPostalWeight'],
                $_POST['User']['defaultPostalContent'],
                $_POST['User']['defaultPostalValue'],
                $_POST['User']['defaultPostalReceiverCountryId'],
                $_POST['User']['defaultPostalValueCurrency']
            ))
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Domyślne dane paczki zostały zapisane.'));
                $this->redirect(array('update'));
                exit;
            }

        }
        else if(isset($_POST['saveCourierTypeInternalPickupDefault']))
        {

            if($model->setCourierTypeInternalDefaultPickup($_POST['User']['courierTypeInternalDefaultPickup']))
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia zostały zapisane.'));
                $this->redirect(array('update'));
                exit;
            }

        }
        else if(isset($_POST['toggleSpZooChangeDate']))
        {

            if($model->setSpZooChangeDate($_POST['User']['spZooChangeDate']))
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia zostały zapisane.'));
                $this->redirect(array('update'));
                exit;
            }

        }
        else if(isset($_POST['saveNotificationSettings']) OR isset($_POST['restoreDefaultNotificationSettings']))
        {
            if($model->setNotificationCustomSettings($_POST['notification_setting_email'], $_POST['notification_setting_sms'], isset($_POST['restoreDefaultNotificationSettings'])))
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Domyślne dane paczki zostały zapisane.'));
                $this->redirect(array('update'));
                exit;
            }

        }
        else if(isset($_POST['saveCourierGridViewSettings']) OR isset($_POST['restoreCourierGridViewSettings']))
        {
            if($model->setCourierGridViewSettings($_POST['courierGridViewSettings'], isset($_POST['restoreCourierGridViewSettings'])))
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia listy przesyłek kurierskich zostały zapisane.'));
                $this->redirect(array('update'));
                exit;
            }

        }
        else if(isset($_POST['savePostalGridViewSettings']) OR isset($_POST['restorePostalGridViewSettings']))
        {
            if($model->setPostalGridViewSettings($_POST['postalGridViewSettings'], isset($_POST['restorePostalGridViewSettings'])))
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia listy przesyłek kurierskich zostały zapisane.'));
                $this->redirect(array('update'));
                exit;
            }

        }
        else if(isset($_POST['saveReturnPackageAutomatic']))
        {
            if($model->setReturnPackageAutomatic($_POST['returnPackageAutomatic']))
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Zmiany zostały zapisane.'));
                $this->redirect(array('update'));
                exit;
            }

        }
        else if (isset($_POST['User'])) {

            // dont update these attributes
            foreach(User::$dontSelfUpdateAttributes AS $item)
                unset($_POST['User'][$item]);

            $model->setAttributes($_POST['User']);

            if($model->validate())
            {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('user', 'Dane zostały zaktualizowane'));
                    $this->redirect(array('update'));
                }
            }
        }
        else if (isset($_POST['UserCustomAccountingEmailForm'])) {


            $modelCAE->setAttributes($_POST['UserCustomAccountingEmailForm']);

            if($modelCAE->validate())
            {
                if ($modelCAE->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('user', 'Dane zostały zaktualizowane'));
                    $this->redirect(array('update'));
                }
            }
        }
        else if (isset($_POST['UserCustomReturnReportEmailForm'])) {

            $modelCRRE->setAttributes($_POST['UserCustomReturnReportEmailForm']);

            if($modelCRRE->validate())
            {
                if ($modelCRRE->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('user', 'Dane zostały zaktualizowane'));
                    $this->redirect(array('update'));
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelCAE' => $modelCAE,
            'modelCRRE' => $modelCRRE,
        ));
    }


    public function actionIndex() {

        $this->redirect(array('panel'));
    }

    public function actionReminder()
    {
        $this->layout = 'other';

        $model = new F_UserReminder;

        if (isset($_POST['F_UserReminder'])) {

            $model->setAttributes($_POST['F_UserReminder']);

            if($model->validate())
            {
                if(User::reminder($model->login, $model->email))
                    Yii::app()->user->setFlash('success',Yii::t('user', 'Na adres email związany z kontem wysłano wiadomość!'));
                else
                    Yii::app()->user->setFlash('error', Yii::t('user', 'Nie znaleziono konta powiązanymi z podanymi danymi!'));
            }
        }

        $this->render('reminder',
            array(
                'model' => $model,
            ));
    }

    public function actionResetPassword($urlData, $urlData2)
    {
        $this->layout = 'other';
        /* @var $model User */

        $model = User::getModelByHash($urlData);

        if($model === NULL)
        {
            Yii::app()->user->setFlash('error', Yii::t('user', 'Podane dane są błędne. Być może link został już wykorzystany lub minął jego termin ważności?'));
        }
        else
        {

            $password = $model->resetPassword($urlData2);

            if(!$password)
            {
                Yii::app()->user->setFlash('error', Yii::t('user', 'Podane dane są błędne. Być może link został już wykorzystany lub minął jego termin ważności?'));
            }
            else
            {
                Yii::app()->user->setFlash('success', Yii::t('user', 'Twoje hasło zostało zresetowane!'));
                Yii::app()->user->setFlash('notice', Yii::t('user', 'Twoje nowe hasło to: {password}', ['{password}' => $password]));
            }
        }
        $this->redirect(array('resetPasswordResult'));
    }

    public function actionResetPasswordResult()
    {
        $this->layout = 'other';

        if(!Yii::app()->user->hasFlash('success') AND !Yii::app()->user->hasFlash('error'))
            throw new CHttpException(403);

        $this->render('resetPasswordResult');
    }

    public function actionActivate($urlData)
    {
        $this->layout = 'other';
        /* @var $model User */

        $model = User::getModelByHash($urlData);

        if($model === NULL)
        {
            Yii::app()->user->setFlash('error', Yii::t('user', 'Podane dane są błędne. Być może minął termin ważności linku?'));
        } else {

            if($model->activated == S_Status::ACTIVE)
                Yii::app()->user->setFlash('notice',Yii::t('user', 'Twoje konto zostało już wcześniej aktywowane!'));
            else
            {
                if($model->activate())
                    Yii::app()->user->setFlash('success',Yii::t('user', 'Aktywacja powiodła się - możesz się już zalogować!'));
                else
                    Yii::app()->user->setFlash('error',Yii::t('user', 'Aktywacja nie powiodła się!'));
            }

            $this->render('activationResult');
        }
    }

    public function actionActivationResult()
    {
        $this->layout = 'other';

        if(!Yii::app()->user->hasFlash('success') AND !Yii::app()->user->hasFlash('error') AND !Yii::app()->user->hasFlash('notice'))
            throw new CHttpException(403);

        $this->render('activationResult');

    }

    public function actionComplaint($type = false, $hash = false)
    {
        if(!$type OR !$hash)
            throw new CHttpException(404);

        try {
            $model = UserComplaintForm::newComplaint($type, $hash);
        }
        catch(Exception $ex)
        {
            if($ex->getCode() == 5000)
            {
                Yii::app()->user->setFlash('error',Yii::t('complaintForm','Zgłoszenie reklamacji nie jest możliwe.'));
                $this->redirect(UserComplaintForm::getRedirectUrl($type));
                Yii::app()->end();
            }


            throw new CHttpException(404);
        }

        if(isset($_POST['UserComplaintForm']))
        {

            $model->attributes = $_POST['UserComplaintForm'];

            $model->file_1 = CUploadedFile::getInstance($model, "file_1");
            $model->file_2 = CUploadedFile::getInstance($model, "file_2");
            $model->file_3 = CUploadedFile::getInstance($model, "file_3");

            if($model->validate())
            {
                if($model->send())
                {
                    Yii::app()->user->setFlash('success',Yii::t('complaintForm','Twoje reklamacja została wysłana!'));
                    $this->redirect($model->getSuccessCustomerUrl());
                    Yii::app()->end;
                } else {
                    Yii::app()->user->setFlash('error',Yii::t('complaintForm','Nie udało się wysłać reklamacji. Spróbuj ponownie lub skontaktuj się z nami bezpośrednio!'));
                    $this->refresh('true');
                }
            }

        }

        $this->render('complaint', [
            'model' => $model,
        ]);


    }

    public function actionDocuments()
    {

        $this->panelLeftMenuActive = 707;

        $models = UserDocuments::model()->findAllByAttributes([
            'user_id' => Yii::app()->user->id,
            'customer_visible' => 1,
            'stat' => 1,
        ]);


        $this->render('documents', [
            'models' => $models,
        ]);
    }

    public function actionDocumentsGetFile($hash)
    {
        $model = UserDocuments::model()->findByAttributes(['hash' => $hash]);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        if($model->user_id != Yii::app()->user->id)
            throw new CHttpException(403);

        $file_path = $model->file_path;
        $file_name = $model->file_name;
        $file_type = $model->file_type;


        if($file_path == NULL)
            throw new CHttpException(404);

        return Yii::app()->getRequest()->sendFile($file_name, @file_get_contents($file_path), $file_type);
    }

    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewUserDocumentsGrid(){

        // for transation bot to prepare this text
//        $temp = Yii::t('courier', 'Nowa paczka typu "{internal}"', ['{internal}' => Courier::getTypesNames()[Courier::TYPE_INTERNAL]]);

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new _UserDocumentsGridViewUser('search');
        $model->unsetAttributes();

        if (isset($_GET['UserDocumentsGridViewUser']))
            $model->setAttributes($_GET['UserDocumentsGridViewUser']);


//        Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);

        $pageSize = Yii::app()->user->getState('pageSize', 50);

        echo '<input type="hidden" class="grid-data"/>';
//$this->widget('zii.widgets.grid.CGridView', array(
//$this->widget('yiiwheels.widgets.grid.WhGridView', array(
//        $this->widget('ext.selgridview.BootSelGridView', array(
        $this->widget('SPGridViewNoGet', array(
            'id' => 'documents-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>false,
//            'enableHistory' => true,
//            'selectionChanged'=>'function(id){ checkNumberOfChecks(id); }',
            'beforeAjaxUpdate' => 'function(id,options){$(\'.gridview-overlay\').show();}',
            'afterAjaxUpdate' => 'function(id,data){
            $(\'.gridview-overlay\').hide();           
            }',
//    'afterAjaxUpdate' => "function(id,data){  $('[name=\"selected_rows[]\"]').on('click', function(){
//        checkNumberOfChecks($(this));
//    });
//      }",
            //'rowCssClassExpression'=>'$data->stat?"row-open":"row-closed"',
            'template'=>"{pager}\n{summary}\n{items}\n{summary}\n{pager}",
            'htmlOptions' => ['style' => 'table-layout: fixed'],
            'columns' => array(
                [
                    'name' => 'date_entered',
                    'class'=>'DataColumn',
//                    'evaluateHtmlOptions'=>true,
                    'htmlOptions' => [
                        'class' => '"text-center',
                        'width' => '80',
                    ],
                ],
                [
                    'type'=>'raw',
                    'header' => Yii::t('userDocuments', 'Pobierz'),

                    'htmlOptions' => [
                        'class' => 'text-center',
                        'width' => '140',
                    ],
                    'value' => 'CHtml::link($data->file_name, ["/user/documentsGetFile", "hash" => $data->hash, ], ["class" => "btn btn-sm"])',
                ],
                [
                    'name' => 'type',
                    'class'=>'DataColumn',
//                    'evaluateHtmlOptions'=>true,
                    'filter' => UserDocuments::getTypeList(),
                    'value' => '$data->getTypeName()',
                    'htmlOptions' => [
                        'class' => '"text-center',
                        'width' => '60',
                    ],
                ],
                [
                    'name' => 'desc_customer',
                    'class'=>'DataColumn',
//                    'evaluateHtmlOptions'=>true,
                    'type'=>'raw',
                    'htmlOptions' => [
                        'width' => '250',
                    ],
                    'value' => '$data->desc_customer != "" ? "<div style=\"overflow-y: scroll; width: 100%; max-height: 30px;\">".nl2br(CHtml::encode($data->desc_customer))."</div>" : "-"',
                ],
            ),
        ));
    }

    public function actionAjaxCheckVies()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $vies = $_POST['vies'];

            $result = ViesCheck::check($vies);
            echo CJSON::encode($result);

        }
    }

    public function actionAckCardArchive()
    {
        $this->panelLeftMenuActive = 709;

        $this->render('ackCardArchive', [
        ]);
    }

    public function actionAckCardArchiveGetFile($hash, $type)
    {
        $model = AckCardArchive::model()->findByAttributes(['hash' => $hash]);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        if($model->user_id != Yii::app()->user->id)
            throw new CHttpException(403);



        if($type == AckCardArchive::TYPE_PDF)
        {
            $file_path = $model->path_pdf;
            $file_name = 'ack_'.$model->date_entered.'.pdf';
            $file_type = 'pdf';
        }
        else if($type == AckCardArchive::TYPE_XLS)
        {
            $file_path = $model->path_xls;
            $file_name = 'ack_'.$model->date_entered.'.xls';
            $file_type = 'xls';
        }

        if($file_path == NULL)
            throw new CHttpException(404);

        return Yii::app()->getRequest()->sendFile($file_name, @file_get_contents($file_path), $file_type);
    }

    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewAckCardArchiveGrid(){

        // for transation bot to prepare this text
//        $temp = Yii::t('courier', 'Nowa paczka typu "{internal}"', ['{internal}' => Courier::getTypesNames()[Courier::TYPE_INTERNAL]]);

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new AckCardArchive('search');
        $model->unsetAttributes();

        $model->user_id = Yii::app()->user->id;

        if (isset($_GET['AckCardArchive']))
            $model->setAttributes($_GET['AckCardArchive']);


//        Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);

        $pageSize = Yii::app()->user->getState('pageSize', 50);

        echo '<input type="hidden" class="grid-data"/>';
//$this->widget('zii.widgets.grid.CGridView', array(
//$this->widget('yiiwheels.widgets.grid.WhGridView', array(
//        $this->widget('ext.selgridview.BootSelGridView', array(
        $this->widget('SPGridViewNoGet', array(
            'id' => 'ack-card-archive-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>false,
//            'enableHistory' => true,
//            'selectionChanged'=>'function(id){ checkNumberOfChecks(id); }',
            'beforeAjaxUpdate' => 'function(id,options){$(\'.gridview-overlay\').show();}',
            'afterAjaxUpdate' => 'function(id,data){
            $(\'.gridview-overlay\').hide();           
            }',
//    'afterAjaxUpdate' => "function(id,data){  $('[name=\"selected_rows[]\"]').on('click', function(){
//        checkNumberOfChecks($(this));
//    });
//      }",
            //'rowCssClassExpression'=>'$data->stat?"row-open":"row-closed"',
            'template'=>"{pager}\n{summary}\n{items}\n{summary}\n{pager}",
            'htmlOptions' => ['style' => 'table-layout: fixed'],
            'columns' => array(
                'no',
                [
                    'name' => 'date_entered',
                    'class'=>'DataColumn',
//                    'evaluateHtmlOptions'=>true,
                    'htmlOptions' => [
                        'class' => '"text-center',
                        'width' => '150',
                    ],
                ],
                [
                    'type'=>'raw',
                    'header' => Yii::t('ackCardArchive', 'Pobierz PDF'),

                    'htmlOptions' => [
                        'class' => 'text-center',
                        'width' => '200',
                    ],
                    'value' => 'CHtml::link(Yii::t("ackCardArchive", "Pobierz PDF"), ["/user/ackCardArchiveGetFile", "hash" => $data->hash, "type" => AckCardArchive::TYPE_PDF, ], ["class" => "btn btn-sm"])',
                ],
                [
                    'type'=>'raw',
                    'header' => Yii::t('ackCardArchive', 'Pobierz XLS'),

                    'htmlOptions' => [
                        'class' => 'text-center',
                        'width' => '200',
                    ],
                    'value' => 'CHtml::link(Yii::t("ackCardArchive", "Pobierz XLS"), ["/user/ackCardArchiveGetFile", "hash" => $data->hash, "type" => AckCardArchive::TYPE_XLS, ], ["class" => "btn btn-sm"])',
                ],
                [
                    'header' => Yii::t('ackCardArchive', 'Pozycji'),
                    'name' => 'items',
                ]
            ),
        ));
    }

    public function actionDeleteFavPoint($id)
    {
        Yii::app()->getModule('courier');

        /* @var $model CourierSpPointFav */
        $model = CourierSpPointFav::model()->findByAttributes([
            'user_id' => Yii::app()->user->id,
            'id' => $id,
        ]);

        if($model)
        {
            if(Yii::app()->user->model->courierTypeInternalDefaultPickup == 'F@@@'.$model->getComplexId())
                Yii::app()->user->model->setCourierTypeInternalDefaultPickup(false);

            $model->delete();
            Yii::app()->user->setFlash('success', Yii::t('user', 'Wpis został skasowany'));
            $this->redirect(['/user/update#dimensions']);
        }
        else
            throw new HttpException(403);
    }


}