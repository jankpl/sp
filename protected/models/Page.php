<?php

Yii::import('application.models._base.BasePage');

class Page extends BasePage
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                 'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),

            array('stat', 'default',
                'value'=>S_Status::ACTIVE),

            array('name', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('name, author', 'length', 'max'=>45),
            array('date_updated', 'safe'),
            array('date_updated, author', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, stat, author', 'safe', 'on'=>'search'),
        );
    }
}