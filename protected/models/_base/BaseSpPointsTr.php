<?php

/**
 * This is the model base class for the table "sp_points_tr".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SpPointsTr".
 *
 * Columns in table "sp_points_tr" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property integer $sp_points_id
 * @property integer $language_id
 * @property string $title
 * @property string $text
 *
 */
abstract class BaseSpPointsTr extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'sp_points_tr';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'SpPointsTr|SpPointsTrs', $n);
	}

	public static function representingColumn() {
		return 'title';
	}

	public function rules() {
		return array(
			array('sp_points_id, language_id, title, text', 'required'),
			array('sp_points_id, language_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>128),
			array('id, sp_points_id, language_id, title, text', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'sp_points_id' => Yii::t('app', 'Sp Points'),
			'language_id' => Yii::t('app', 'Language'),
			'title' => Yii::t('app', 'Title'),
			'text' => Yii::t('app', 'Text'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('sp_points_id', $this->sp_points_id);
		$criteria->compare('language_id', $this->language_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('text', $this->text, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}