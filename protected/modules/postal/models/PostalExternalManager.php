<?php

class PostalExternalManager
{

    const TYPE_17TRACK = 25;

    public static function sortArray(array $data)
    {
        usort($data, array("PostalExternalManager", "sortFunctionReversed"));
        return $data;
    }

    protected static function sortFunction( $a, $b ) {
        return strtotime($b["date"]) - strtotime($a["date"]);
    }

    public static function sortArrayNewestFirst(array $data)
    {

        usort($data, array("PostalExternalManager", "sortFunction"));

        return $data;
    }

    protected static function sortFunctionReversed( $a, $b ) {
        return strtotime($a["date"]) - strtotime($b["date"]);
    }

    public static function sortArrayOldestFirst(array $data)
    {
        usort($data, array("PostalExternalManager", "sortFunctionReversed"));
        return $data;
    }

    public static function updateStatusForPackage(Postal $postal, $blockNotification = false, $forceService = false, $forceTrackingId = false)
    {
        Yii::app()->db->createCommand()->update('tt_log', ['last' => 0], 'item_type = :item_type AND item_id =:item_id AND last = 1', [':item_type' => TtLog::TYPE_POSTAL, ':item_id' => $postal->id]);
        Yii::app()->db->createCommand()->insert('tt_log', ['item_type' => TtLog::TYPE_POSTAL, 'item_id' => $postal->id, 'last' => 1]);

        if($postal->bulk OR $postal->postalLabel == NULL OR $postal->postalLabel->track_id == '')
            return false;

        $convertDeliveryStatus = false;

        if(!$forceService && !$forceTrackingId && in_array($postal->user_id, [
                1289, // POST11
//                8
            ])) // additional tracking based on ref numbers
        {

            if($postal->user_id == 1289)
            {
                $tt = str_replace('SF', '', $postal->ref);
                self::updateStatusForPackage($postal, false, self::TYPE_17TRACK, $tt);
            }
//            else if($postal->user_id == 8)
//            {
//                $tt = str_replace('SF', '', $postal->note1);
//                self::updateStatusForPackage($postal, false, self::TYPE_17TRACK, $tt);
//            }
        }

        $service = $postal->postalLabel->postalOperator->broker_id;
        $trackingId = $postal->postalLabel->track_id;

        if($forceService && $forceTrackingId)
        {
            $trackingId = $forceTrackingId;
            $service = $forceService;
            $convertDeliveryStatus = true;
        }


        $data = false;
        if(in_array($service, array_keys(GLOBAL_BROKERS::getBrokersListPostal()))) {

            $operatorClient = GLOBAL_BROKERS::globalOperatorClientFactory($service);
            $response = $operatorClient::trackForPostal($trackingId);

            if($response instanceof GlobalOperatorTtResponseCollection)
                $data = $response->adapterToPostalExternalManagerArray();

            if(!is_array($data))
                return false;

            $statusServiceId = $service;
        }
        else if($service == self::TYPE_17TRACK) {
            $data = Client17Track::getTtPostal($trackingId, false, false, 'PostalExtMan');
            $statusServiceId = PostalStat::EXTERNAL_SERVICE_17TRACK;
        }
        else if($service == Postal::POSTAL_BROKER_OOE)
        {
            $data = Yii::app()->OneWorldExpress->track($trackingId);
            $statusServiceId = PostalStat::EXTERNAL_SERVICE_OOE;
        }
        else if($service == Postal::POSTAL_BROKER_POCZTA_POLSKA)
        {
            $data = PocztaPolskaClient::track($trackingId);
            $statusServiceId = PostalStat::EXTERNAL_SERVICE_POCZTA_POLSKA;
        }
        else if($service == Postal::POSTAL_BROKER_DHLDE)
        {
            $data = DhlDeClient::trackSingle($trackingId);
            $statusServiceId = PostalStat::EXTERNAL_SERVICE_DHLDE;
        }
        else if($service == Postal::POSTAL_BROKER_POST11)
        {
            $data = Post11Client::track($trackingId);
            $statusServiceId = PostalStat::EXTERNAL_SERVICE_POST11;
        }
        else if($service == Postal::POSTAL_BROKER_SWEDEN_POST)
        {
            Yii::app()->getModule('courier');
            $data = SwedenPostClient::trackPostal($trackingId);
            $statusServiceId = PostalStat::EXTERNAL_SERVICE_SWEDEN_POST;
        }
        else if($service == Postal::POSTAL_BROKER_HUNGARY_POST)
        {
            $data = HuPostClient::track($trackingId);
            $statusServiceId = PostalStat::EXTERNAL_SERVICE_HUNGARY_POST;
        }
        else if($service == Postal::POSTAL_BROKER_ROYAL_MAIL)
        {
            $data = RoyalMailClient::track($trackingId);
            $statusServiceId = PostalStat::EXTERNAL_SERVICE_ROYAL_MAIL;
        }

        if(!is_array($data))
            return false;

        $data = PostalExternalManager::sortArray($data);

        if(!is_array($data) OR !S_Useful::sizeof($data))
            return false;
        else
        {
            self::processUpdateDate($postal, $data, $blockNotification, $statusServiceId, $convertDeliveryStatus);
//            foreach($data AS $item)
//            {
//                $stat_id = PostalStat::getOrCreateAndGetId($item['name'], $statusServiceId, false, $convertDeliveryStatus);
//
//                $date_converted = PostalStat::convertDate($item['date']);
//                $location = trim(strip_tags($item['location']));
//
//                $cmd = Yii::app()->db->createCommand();
//                $cmd->select('COUNT(*)');
//                $cmd->from('postal_stat_history');
//                $cmd->where('postal_id = :postal_id', array(':postal_id' => $postal->id));
//
//                // because for example Safepak do not provide data for status
//                if($date_converted != '')
//                    $cmd->andWhere('status_date = :status_date', array(':status_date' => $date_converted));
//
//
//                $cmd->andWhere('current_stat_id = :status_id', array(':status_id' => $stat_id));
//                //$cmd->andWhere('location = :location', array(':location' => $location));
//                $result = $cmd->queryScalar();
//
////                // test of status name merget with location (older version) - for compatilibity with old packages.
////                if(!$result && $item['location'] != '')
////                {
////                    $stat_with_location = $item['name'].' | '.$item['location'];
////                    $stat_id = PostalStat::getOrCreateAndGetId($stat_with_location, $statusServiceId, true);
////
////                    if($stat_id) {
////                        $date_converted = PostalStat::convertDate($item['date']);
////                        $location = trim(strip_tags($item['location']));
////
////                        $cmd = Yii::app()->db->createCommand();
////                        $cmd->select('COUNT(*)');
////                        $cmd->from('postal_stat_history');
////                        $cmd->where('postal_id = :postal_id', array(':postal_id' => $postal->id));
////                        $cmd->andWhere('status_date = :status_date', array(':status_date' => $date_converted));
////                        $cmd->andWhere('current_stat_id = :status_id', array(':status_id' => $stat_id));
////                        $result = $cmd->queryScalar();
////                    }
////                }
//
//
//                // already in DB, so do not continue to update stat
//                if($result)
//                {
//                    break;
//                } else {
//                    $doNotNotify = $blockNotification;
//
//                    $stat_id = PostalStat::getOrCreateAndGetId($item['name'], $statusServiceId, true, $convertDeliveryStatus);
//
//                    // save status into package
//                    $postal->changeStat($stat_id, $statusServiceId, $item['date'], false, $location, true, $doNotNotify, true);
//                }
//            }


        }

        return true;
    }

    /**
     * Method searches for packages connected to provided remoteId and returns array of found postal Id's
     * @param $remoteId string Remote Id to be found
     * @return Postal[] Array of postal Id's
     */
    public static function findPostalIdsByRemoteId($trackId)
    {
        if(!is_array($trackId))
            $trackIdArray = [ $trackId ];
        else
            $trackIdArray = $trackId;

        $trackIdSortOrder = '';
        foreach($trackIdArray AS $item)
            $trackIdSortOrder .= "'".preg_replace("/[^[:alnum:]]/u", '', $item)."',";

        $trackIdSortOrder = substr( $trackIdSortOrder,0,-1);

        if($trackIdSortOrder=='')
            $trackIdSortOrder = "''";


        $found = [];

        // LABEL NEW
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('postal.id');
        $cmd->from((new PostalLabel())->tableName());
        $cmd->join('postal', 'postal.postal_label_id = postal_label.id');
//        $cmd->where('track_id = :trackId', [':trackId' => $trackId]);
        $cmd->where(['in', 'track_id', $trackIdArray]);
        $cmd->order('FIELD(track_id, '.$trackIdSortOrder.')');

        $result = $cmd->queryColumn();

        if(is_array($result))
            foreach($result AS $item)
            {
                $found[$item] = true;
            }

//        if(is_array($result))
//            foreach($result AS $item)
//            {
//                $cmd = Yii::app()->db->createCommand();
//                $cmd->select('id');
//                $cmd->from((new Postal())->tableName());
//                $cmd->where('postal_label_id = :postal_label_id', [':postal_label_id' => $item['id']]);
//                $result = $cmd->queryScalar();
//
//                $found[$result] = true;
//            }

        // REF
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id');
        $cmd->from((new Postal())->tableName());
//        $cmd->where('ref = :trackId', [':trackId' => $trackId]);
        $cmd->where(['in', 'ref', $trackId]);
        $cmd->orWhere(['in', 'local_id', $trackId]);
        $result = $cmd->queryColumn();

        if(is_array($result))
            foreach($result AS $item)
            {
                $found[$item] = true;
            }


        // PostalAdditionalExtId
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('postal_id');
        $cmd->from((new PostalAdditionalExtId())->tableName());
        $cmd->where(['in', 'external_id', $trackId]);
        $result = $cmd->queryColumn();

        if(is_array($result))
            foreach($result AS $item)
            {
                $found[$item] = true;
            }

        // external_id
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id');
        $cmd->from((new Postal())->tableName());
//        $cmd->where('external_id = :trackId', [':trackId' => $trackId]);
        $cmd->where(['in', 'external_id', $trackId]);
        $result = $cmd->queryColumn();

        if(is_array($result))
            foreach($result AS $item)
            {
                $found[$item] = true;
            }

        $models = Postal::model()->findAllByAttributes(['id' => array_keys($found)]);

        return $models;
    }

    public static function processUpdateDate(Postal $postal, array $data, $blockNotification, $statusServiceId, $convertDeliveryStatus = false)
    {
        // clear empty multidimensional
        $data = array_filter(array_map('array_filter', $data));

        if(!sizeof($data))
            return true;

        // check last status first - it it's already in DB, then we have it ALL
        {
            $mostRecentStatus = end($data);

            $stat_id = PostalStat::getOrCreateAndGetId($mostRecentStatus['name'], $statusServiceId, true, $convertDeliveryStatus);
            if($stat_id) {
                $date_converted = PostalStat::convertDate($mostRecentStatus['date']);

                $cmd = Yii::app()->db->createCommand();
                $cmd->select('COUNT(*)');
                $cmd->from('postal_stat_history');
                $cmd->where('postal_id = :postal_id', array(':postal_id' => $postal->id));

                // because for example Safepak do not provide data for status
                if ($date_converted != '')
                    $cmd->andWhere('status_date = :status_date', array(':status_date' => $date_converted));
                $cmd->andWhere('current_stat_id = :status_id', array(':status_id' => $stat_id));

                $result = $cmd->queryScalar();

                // already in DB, so do not continue to update stat
                if ($result)
                    return true;
            }
        }


        // check if there is "DELIVERED" status, but it's not last
        {
            $hasDeliveredStat = false;
            $deliveredDate = NULL;
            $temp = false;

            $dataBackup = $data; // SHOULD BE COPY?
            foreach ($data AS $key => $item) {
                if (!$hasDeliveredStat) {

                    $stat_id = PostalStat::getOrCreateAndGetId($item['name'], $statusServiceId, true, $convertDeliveryStatus);
                    $map_id = StatMapMapping::findMapForStat($stat_id, StatMapMapping::TYPE_POSTAL);

                    if ($map_id == StatMap::MAP_DELIVERED) {
                        $deliveredDate = PostalStat::convertDate($item['date']);
                        $hasDeliveredStat = true;

                        // remove delivered stat from array
                        $temp = $data[$key];
                        unset($data[$key]);
                    }
                } else {
                    // after we already have delivered stat - check if date is not from future - if so - ignore it
                    if (strtotime(PostalStat::convertDate($item['date'])) > strtotime($deliveredDate)) {
                        unset($data[$key]);
                        MyDump::dump('data_fix_delivered_postal.txt', 'POSTAL: ' . $postal->id . ' :' . print_r($dataBackup, 1));
                    }
                }

                if ($temp) // give back delivered stat to the end
                    $data[] = $temp;
            }
        }


        foreach($data AS $item)
        {

            $stat_id = PostalStat::getOrCreateAndGetId($item['name'], $statusServiceId, false, $convertDeliveryStatus);

            $date_converted = PostalStat::convertDate($item['date']);
            $location = trim(strip_tags($item['location']));

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from('postal_stat_history');
            $cmd->where('postal_id = :postal_id', array(':postal_id' => $postal->id));

            // because for example Safepak do not provide data for status
            if($date_converted != '')
                $cmd->andWhere('status_date = :status_date', array(':status_date' => $date_converted));


            $cmd->andWhere('current_stat_id = :status_id', array(':status_id' => $stat_id));
            //$cmd->andWhere('location = :location', array(':location' => $location));
            $result = $cmd->queryScalar();

            // already in DB, so do not continue to update stat
            if($result)
            {
                break;
            } else {
                $doNotNotify = $blockNotification;

                $stat_id = PostalStat::getOrCreateAndGetId($item['name'], $statusServiceId, true, $convertDeliveryStatus);

                // save status into package
                $postal->changeStat($stat_id, $statusServiceId, $item['date'], false, $location, true, $doNotNotify, true);
            }
        }
    }

}