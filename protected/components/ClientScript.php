<?php

/**
 * Class ClientScript
 * Extended class to allow setting order of scripts
 */

class ClientScript extends CClientScript {

    public $scriptLevels;

    public function registerScriptFile($url,$position=null, array $htmlOptions=array(), $level = 1)
    {
        $this->scriptLevels[$url] = $level;
        return parent::registerScriptFile($url, $position, $htmlOptions);
    }

    public function registerScript($id, $script, $position = null, array $htmlOptions = array(), $level = 1) {
        $this->scriptLevels[$id] = $level;
        return parent::registerScript($id, $script, $position, $htmlOptions);
    }

    public function render(&$output) {
        foreach ($this->scripts as $key => $value) {
            $this->scripts[$key] = $this->reorderScripts($value);
        }

        foreach ($this->scriptFiles as $key => $value) {
            $this->scriptFiles[$key] = $this->reorderScripts($value);
        }

        parent::render($output);
    }

    public function reorderScripts($element) {
        $tempScripts = array();
        $buffer = array();
        foreach ($element as $key => $value) {
            if (isset($this->scriptLevels[$key])) {
                $tempScripts[$this->scriptLevels[$key]][$key] = $value;
            }
        }
        ksort($tempScripts);
        foreach ($tempScripts as $value) {
            foreach ($value as $k => $v) {
                $buffer[$k] = $v;
            }
        }
        return $buffer;
    }

}