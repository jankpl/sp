<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('site','Kontakt');
?>

<h2><?php echo Yii::t('site','Kontakt');?></h2>

<div class="row">
    <div class="col-xs-12 col-md-6 contact-content">
        <?php
        $this->Widget('ShowPageContent', array('id' => 14));
        ?>
    </div>
    <div class="col-xs-12 col-md-6">
        <?php if(Yii::app()->user->hasFlash('contact')): ?>
            <div class="alert alert-success">
                <?php echo Yii::app()->user->getFlash('contact'); ?>
            </div>

        <?php else: ?>

            <div class="form alert alert-info">

                <h3 style="margin-top: 0;"><?= Yii::t('site','Formularz kontaktowy');?></h3>

                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'contact-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                        //'validateOnType' => true,
                    ),
                )); ?>
                <?php echo $form->errorSummary($model); ?>
                <div>
                    <?php echo $form->textField($model,'email', ['placeholder' => $model->getAttributeLabel('email')]); ?>
                </div>
                <div>
                    <?php echo $form->textField($model,'name', ['placeholder' => $model->getAttributeLabel('name')]); ?>
                </div>
                <div>
                    <?php echo $form->textArea($model,'body',['rows'=>6, 'cols'=>50, 'placeholder' => $model->getAttributeLabel('body')]); ?>
                </div>
                <div>
                    <?php echo $form->checkbox($model,'accept', ['style' => 'display: inline-block; width: auto;']); ?> <?= $form->label($model, 'accept', ['style' => 'width: auto; display: inline-block;']);?>
                </div>
                <div class="text-center">
                    <?php echo $form->hiddenField($model, 'verifyCode', ['id' => 'recaptcha_token']); ?>
                    <?php echo TbHtml::submitButton(Yii::t('app','Wyślij'), ['class' => 'btn btn-primary btn-lg']); ?>
                </div>
                <?php $this->endWidget(); ?>
            </div><!-- form -->
            <script src="https://www.google.com/recaptcha/api.js?render=<?= ContactForm::GC_PUBLIC_KEY;?>"></script>
            <script>
                $('#contact-form').on('submit', function(e){
                    if($('#recaptcha_token').val() == '') {
                        e.preventDefault();
                        grecaptcha.ready(function () {
                            grecaptcha.execute('<?= ContactForm::GC_PUBLIC_KEY;?>', {action: 'homepage'}).then(function (token) {
                                $('#recaptcha_token').val(token);
                                $('#contact-form').submit();
                            });
                        });
                    }
                });
            </script>
        <?php endif; ?>
    </div>
</div>