<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'page-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
        'name',
		'date_entered',
		'date_updated',
		array(
			'class' => 'CButtonColumn',
            'template' => '{update}',
		),
	),
)); ?>