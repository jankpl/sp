<?php
/* @var $model CourierCountryGroup */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
);

?>
<h1>Manage Courier Internal/Pre-routing routings</h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<h2>Temporary operator change</h2>

<div class="alert alert-info">Filters all operators determined by routings. Routings remains unchaged.</div>

<?php
if(AdminRestrictions::isUltraAdmin()):
    ?>
    <a href="<?= Yii::app()->createUrl('courier/courierCountryGroup/tempAdd', ['id' => $item->id]);?>" class="btn btn-primary">Add new</a><br/><br/>
<?php
endif;
?>
<table class="table table-bordered table-striped">
    <tr>
        <th>#</th>
        <th>Date</th>
        <th>From</th>
        <th>To</th>
        <th style="text-align: center;">Active</th>
        <?php
        if(AdminRestrictions::isUltraAdmin()):
            ?>
            <th style="text-align: center;">Switch</th>
            <th style="text-align: center;">Delete</th>
        <?php
        endif;
        ?>
    </tr>
    <?php
    /* @var $item CourierInternalOperatorChange */
    foreach(CourierInternalOperatorChange::model()->findAll() AS $key => $item):
        ?>
        <tr>
            <td><?= $item->id;?></td>
            <td><?= $item->date_updated;?></td>
            <td><?= $item->fromOperator->getNameWithIdAndBroker();?></td>
            <td><?= $item->toOperator->getNameWithIdAndBroker();?></td>
            <td style="text-align: center;"><?= $item->active ? '<strong>YES</strong>' : '<span style="color: red;">no</span>';?></td>
            <?php
            if(AdminRestrictions::isUltraAdmin()):
                ?>
                <td style="text-align: center;"><a href="<?= Yii::app()->createUrl('courier/courierCountryGroup/tempSwitch', ['id' => $item->id]);?>" class="btn <?= $item->active ? 'btn-warning' : 'btn-success';?> btn-small"><?= $item->active ? 'turn OFF' : 'turn ON';?></a></td>
                <td style="text-align: center;"><a href="<?= Yii::app()->createUrl('courier/courierCountryGroup/tempDelete', ['id' => $item->id]);?>" class="btn btn-danger btn-small">DELETE</a></td>
            <?php
            endif;
            ?>
        </tr>
    <?php
    endforeach;
    ?>
</table>


<h2>Routings</h2>

<div class="alert alert-info">
    <strong>Requirements & exceptions:</strong>
    <br/>
    <br/>
    <strong>A) WEIGHT</strong><br/>
    PL-PL: <= 50kg<br/>
    Export with pickup: <= 50kg<br/>
    Export without pickup: <= 70kg<br/>
    International: <= 70kg<br/><br/>
    <strong>B) DIMENSIONS</strong><br/>
    PL-PL Standard: longest size =< 175 cm and sum of dimensions =< 300 cm<br/>
    PL-PL Non-standard: longest size =< 250 cm and sum of dimensions =< 330 cm<br/>
    Import/export and INT Standard: longest size =< 120 cm and second size =< 60 m<br/>
    Import/export and INT Non-standard: longest size =< 200 cm and sum of longest size and circumference <= 300 <br/>
    <br/>
    <strong>C) DIMENSIONAL WEIGHT</strong><br/>
    PL-PL: none<br/>
    INT: a*b*c/5000<br/><br/>
    <strong>D) Exceptions</strong><br/>
    PL: If a > 140 or sum of dimensions > 260, then operator is DPDPL_V<br/>
    PL: If DPDPL_V has Non-standard addition, operator is changed to DPDPL<br/><br/>
    <strong>E) Other</strong>
    Dimensional weight is used for routing and pricing<br/>
    Non-standard additions are added automatically and can be only one (INT/DOM) or both<br/>

</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-country-group-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        array(
            'name' => 'countryListsSTAT',
            'filter' => false,
        ),
        array(
            'name' => 'pickup_hub',
            'value' => '$data->pickupHub->name',
        ),
        array(
            'name' => 'pickup_operator',
            'value' => '$data->pickupOperator->nameWithIdAndBroker',
        ),
        array(
            'header' => 'Custom pickup operators',
            'value' => '$data->courierCountryGroupOperatorPickupSTAT',
        ),
        array(
            'name' => 'delivery_hub',
            'value' => '$data->deliveryHub->name',
        ),
        array(
            'name' => 'delivery_operator',
            'value' => '$data->deliveryOperator->nameWithIdAndBroker',
        ),
        array(
            'header' => 'Custom delivery operators',
            'value' => '$data->courierCountryGroupOperatorDeliverySTAT',
        ),
        array(
            'header' => 'Custom settings',
            'value' => '$data->courierCountryGroupCustomUserGroupSTAT',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view} {update} {delete}'
        ),
    ),
)); ?>

<h2>List all groups</h2>

<?php echo CHtml::link('List all groups',array('listAllGroups'),array('class' => 'likeButton'));?>

<h2>Import / export</h2>

<?php echo CHtml::link('Import',array('import'),array('class' => 'likeButton'));?> <?php echo CHtml::link('Export',array('export'),array('class' => 'likeButton'));?>
