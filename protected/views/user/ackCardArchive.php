<?php
/* @var $models UserDocuments[] */
?>
<h2><?= Yii::t('ackCardArchive', 'Archiwum kart potwierdzenia');?></h2>

<div class="alert alert-info"><?= Yii::t('ackCardArchive', 'Karty zostają automatycznie usunięte po {n} dniach', ['{n}' => 7]);?></div>

<div class="gridview-container" style="max-width: 700px; margin: 0 auto;">
    <div class="gridview-overlay"></div>
    <?php
    $this->_getGridViewAckCardArchiveGrid();

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('jquery.ui');
    Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css');
    ?>
</div>
