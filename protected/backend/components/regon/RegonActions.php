<?php

class RegonActions extends CAction{

    public function run()
    {

        Yii::import('application.components.RegonGov');

        $nip = trim($_POST['nip']);
        $regon = trim($_POST['regon']);
        $krs = trim($_POST['krs']);
        $vies = trim($_POST['vies']);

        if($nip != '')
            $resp = RegonGov::getDataByNip($nip, true);
        else if($regon != '')
            $resp = RegonGov::getDataByRegon($regon, true);
        else if($krs != '')
            $resp = RegonGov::getDataByKrs($krs, true);
        else if($vies != '')
            $resp = ViesCheck::getAddressDataAttributes($vies);

        $data['success'] = $resp ? true : false;
        $data['mode'] = $_POST['mode'];
        $data['data'] = $resp;

        echo CJSON::encode($data);
    }
}