<?php
/* @var $this UserInvoiceController */
/* @var $model UserInvoice */

$this->breadcrumbs=array(
    'User Messages'=>array('index'),
    $model->id,
);

$this->menu=array(
    array('label'=>'List UserInvoice', 'url'=>array('index')),
    array('label'=>'Create UserInvoice', 'url'=>array('create')),
    array('label'=>'Update UserInvoice', 'url'=>array('update', 'id'=>$model->id)),
);
?>

<h1>View UserInvoice #<?php echo $model->id; ?></h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'date_entered',
        'date_updated',
        'text',
        'date',
        'user.login',
        array(
            'name' => 'seen',
            'value' => UserInvoice::getStatArray()[$model->stat],
        ),
        'no',
    ),
)); ?>

<?php if($model->inv_file_name != ''): ?>
    <br/>
    <br/>
    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file - Invoice', array('/userInvoice/getFile', 'id' => $model->id, 'type' => UserInvoice::FILE_TYPE_INV), array('class' => 'btn btn-primary')), array('closeText' => false));?>
<?php endif; ?>
<?php if($model->sheet_file_name != ''): ?>
    <br/>
    <br/>
    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file - Sheet', array('/userInvoice/getFile', 'id' => $model->id, 'type' => UserInvoice::FILE_TYPE_SHEET), array('class' => 'btn btn-primary')), array('closeText' => false));?>
<?php endif; ?>



<?php if($model->stat == UserInvoice::UNPUBLISHED): ?>
    <br/>
    <br/>
    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, CHtml::link('Publish', array('/userInvoice/publish', 'id' => $model->id), array('class' => 'btn btn-larget btn-success')), array('closeText' => false));?>

<?php endif; ?>

<h4>History:</h4>
<ul>
<?php
foreach($model->userInvoiceDebtOptions AS $option):
    ?>
<li><?= $option->date_entered;?> : <?= $option->getStatName();?></li>

<?php
endforeach;
?>
</ul>


<h4>Set debt collection status</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'set-debt-status',
    'method' => 'post',
    'enableAjaxValidation' => false,
));
?>

<?php echo CHtml::dropDownList('status','',UserInvoiceDebtOptions::getStatList());?>

<?php
echo TbHtml::submitButton('Set', ['size' => TbHtml::BUTTON_SIZE_LARGE,
    'confirm' => 'Are you sure?'
]);
?>

<?php
$this->endWidget();
?>


