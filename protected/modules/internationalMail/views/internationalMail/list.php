<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'international-mail-grid',
    'dataProvider' => $model->searchForUser(),
    'filter' => $model,
    'selectableRows'=>2,
    'columns' => array(
        array(
            'name'=>'local_id',
            'header'=>'#ID lokalny',
            'type'=>'raw',
            'value'=>'CHtml::link($data->local_id,array("/internationalMail/internationalMail/view/", "urlData" => $data->hash))',
        ),
        array(
            'name'=>'date_entered',
            'header'=>'Data utworzenia',
        ),
        array(
            'name'=>'__stat',
            'type'=>'raw',
            'filter'=>GxHtml::listDataEx(InternationalMailStat::model()->findAllAttributes(null, true)),
            'header' => 'Status',
            'value'=>'CHtml::link($data->stat->name,array("/internationalMail/internationalMail/tt", "id" => $data->local_id))',
        ),
        array(
            'name'=>'__sender_country_id',
            'header'=>'Kraj nadawcy',
            'value'=>'$data->senderAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name'=>'__receiver_country_id',
            'header'=>'Kraj odbiorcy',
            'value'=>'$data->receiverAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'header'=>'Wymiary',
            'value'=>'$data->mail_size_l ." x ". $data->mail_size_w ." x ". $data->mail_size_d',
        ),
        'mail_weight',
        array(
            'name'=>'order_id',
            'type'=>'raw',
            'value'=>'$data->order_id !== null? CHtml::link($data->order_id,array("/order/view/", "id" => $data->order->hash)) : "-"',
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}',
            'buttons'=>array(
                'view' => array(
                    'url' => 'Yii::app()->createUrl("/internationalMail/internationalMail/view/", array("urlData" => $data->hash))',
                ),
            ),
        ),
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'selected_rows'
        ),

    ),
));
?>
<h4><?php echo Yii::t('internationalMail','Generuj listy przewozowe zaznaczonych pozycji ({star})', array('{star}' => '<span class="required">*</span>'));?></h4>

<?php
$this->widget('FlashPrinter');
?>

<div style="text-align: center; margin: 15px 0 0 0;">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'mass-carriage-ticket',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('/internationalMail/internationalMail/massCarriageTicketByGridView'),
        'htmlOptions' => [
            'class' => 'do-not-block',
            'target' => '_blank',
        ],
    ));
    ?>

    <?php
    echo TbHtml::submitButton('Generuj', array(
        'onClick' => 'js:
    $("#international-mail-ids").val($.fn.yiiGridView.getChecked("international-mail-grid","selected_rows").toString());
    ;',
        'name' => 'InternationalMailIds'));
    ?>

    <?php echo CHtml::hiddenField('InternationalMail[ids]','', array('id' => 'international-mail-ids')); ?>
    <?php
    $this->endWidget();
    ?>

    <p class="note"><?php echo Yii::t('internationalMail','({star}) Utworzone zostaną listy jedynie dla przesyłek o statusie "{status}"', array('{star}' => '<span class="required">*</span>', 'status' => InternationalMailStat::model()->findByPk(InternationalMailStat::PACKAGE_RETREIVE)->name));?>.</p>
</div>