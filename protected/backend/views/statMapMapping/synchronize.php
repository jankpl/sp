<?php
/* @var $this StatMapMappingController */
/* @var $sourceType int */
/* @var $targetType int */
/* @var $mode int */
/* @var $statTargets [] */
/* @var $statList [] */

$this->breadcrumbs=array(
    'Stat Map Mappings',
);

$this->menu=array(
    array('label'=>'List of mappings', 'url'=>array('index', 'type' => $type)),
);
?>

    <h1><?= StatMapMapping::getTypeName($type);?>: Synchronize Stat Map Mapping - from <?= StatMapMapping::getTypeName($sourceType);?></h1>

<?php
$this->widget('FlashPrinter');
?>

<?php
if(!$mode):
    ?>
    <?php $form = $this->beginWidget('GxActiveForm');?>
    <br/>
    <br/>
    <div class="text-center">
        <?php
        echo CHtml::submitButton('Preview', ['name' => 'preview', 'class' => 'btn btn-primary btn-lg']);
        ?>
    </div>
    <?php
    $this->endWidget();
    ?>
    <?php
elseif($mode == 1):
    ?>
    <?php $form = $this->beginWidget('GxActiveForm');?>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>SourceStat</th>
            <th>TargetMap</th>
            <th>Synchronize</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        foreach($statList AS $stat):
            $found = false;
            $map_id = StatMapMapping::findMapForStat($statTargets[$stat->id]->id, $sourceType);
            ?>
            <tr>
                <td><?= $i++;?></td>
                <td><?= CHtml::encode($stat->name);?></td>
                <td><?= $map_id ? $map_id.' ('.StatMap::getMapNameById($map_id).')' : '-';?></td>
                <td><?= $map_id ? CHtml::checkBox('Synchronization[toSynchronize]['.$stat->id.']').CHtml::hiddenField('Synchronization[map_id]['.$stat->id.']', $map_id) : '-';?></td>
            </tr>
            <?php
        endforeach;
        ?>
        </tbody>
    </table>
    <div class="text-center">
        <?php
        echo CHtml::submitButton('Synchronize selected', ['name' => 'synchronize', 'class' => 'btn btn-primary btn-lg']);
        ?>
    </div>
    <?php
    $this->endWidget();
    ?>

    <?php
endif;
?>