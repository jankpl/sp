<?php

$this->menu = array(
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-stat-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'date_entered',
        array(
            'name' => 'editable',
            'value' => '$data->editable?"yes":"no"',
            'type' => 'raw',
            'filter' => array(0 => 'no', 1 => 'yes'),
        ),
        array(
            'name' => 'notify',
            'value' => '$data->notify?"yes":"no"',
            'type' => 'raw',
            'filter' => array(0 => 'no', 1 => 'yes'),
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>