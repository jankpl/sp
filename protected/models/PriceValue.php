<?php

Yii::import('application.models._base.BasePriceValue');

class PriceValue extends BasePriceValue
{
	const SCENARIO_VALIDATE_PRICES = 'svp';

    public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(
            array('currency_id, value, currency_name', 'required'),
            array('price_id', 'required', 'except' => self::SCENARIO_VALIDATE_PRICES),
            array('price_id, currency_id', 'numerical', 'integerOnly'=>true),
            array('value', 'numerical'),
            array('currency_name', 'length', 'max'=>16),
            array('id, price_id, currency_id, value, currency_name, date_updated', 'safe', 'on'=>'search'),
        );
    }

    public function beforeValidate()
    {
        $this->value = str_replace(',','.', $this->value);
        return parent::beforeValidate();
    }
}