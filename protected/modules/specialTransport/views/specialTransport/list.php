<h2><?php echo Yii::t('specialTransport','Transport specjalny');?></h2>

<?php
$this->widget('FlashPrinter');
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'courier-grid',
    'dataProvider' => $model->searchForUser(),
    'filter' => $model,
    'selectableRows'=>2,
    'columns' => array(
        array(
            'name'=>'id',
            'header'=>'#ID',
            'type'=>'raw',
            'value'=>'CHtml::link($data->id,array("/specialTransport/specialTransport/view/", "urlData" => $data->hash))',
        ),
        array(
            'name'=>'date_entered',
        ),
        array(
            'name'=>'__sender_country_id',
            'header'=>Yii::t('specialTransport','Kraj nadawcy'),
            'value'=>'$data->senderAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name'=>'__receiver_country_id',
            'header'=>Yii::t('specialTransport','Kraj odbiorcy'),
            'value'=>'$data->receiverAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name'=>'type',
            'value'=> 'SpecialTransport::types()[$data->type]',
            'filter'=>SpecialTransport::types(),
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}',
            'buttons'=>array(
                'view' => array(
                    'url' => 'Yii::app()->createUrl("/specialTransport/specialTransport/view/", array("urlData" => $data->hash))',
                ),
            ),
        ),


    ),
));
?>
