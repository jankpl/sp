<?php

class UpsPackage extends CFormModel
{
    public $_sender_name;
    public $_sender_addressLine;
    public $_sender_addressLine2;
    public $_sender_postalCode;
    public $_sender_countryCode;
    public $_sender_number;
    public $_sender_city;

    public $_receiver_name;
    public $_receiver_addressLine;
    public $_receiver_addressLine2;
    public $_receiver_postalCode;
    public $_receiver_countryCode;
    public $_receiver_number;
    public $_receiver_city;

    public $_package_size_l;
    public $_package_size_w;
    public $_package_size_h;
    public $_package_weight;
    public $_package_desc;

    public $_reference_no;

    public $_reference_no_2;

    public $_cod;
    public $_cod_currency;
    public $_cod_amount;

// number of packages to be sent at once
    public $_number_of_multipackages = 1;
    public $_number = 1;


    public $_is_multipackage = false;
    public $_is_multipackage_parent = false;
    public $_multipackage_parent_id;
    //



    public function rules()
    {
        return array(
//			array('company, contact, address1, city, countryCode, postCode, telephone, numerOfPieces, weight, description, valueOfProduct, currency, senderName, reference, serviceCode, accountCode, accountUsername, accountPassword', 'required'),
        );
    }


    public static function createByCourierTypeInternal(CourierTypeInternal $courier, AddressData $from, AddressData $to)
    {
        $model = new self;

        $model->_cod = $courier->courier->cod;
        $model->_reference_no = $courier->courier->local_id;

        $model->_package_desc = S_Useful::removeNationalCharacters($courier->courier->package_content);
        $model->_package_size_l = $courier->courier->package_size_l;
        $model->_package_size_w = $courier->courier->package_size_w;
        $model->_package_size_h = $courier->courier->package_size_d;
        $model->_package_weight = $courier->courier->package_weight;

        $model->_sender_addressLine = S_Useful::removeNationalCharacters($from->address_line_1);
        $model->_sender_addressLine2 = S_Useful::removeNationalCharacters($from->address_line_2);
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_name = S_Useful::removeNationalCharacters($from->name).' '.S_Useful::removeNationalCharacters($from->company);
        $model->_sender_number = $from->tel;
        $model->_sender_city = S_Useful::removeNationalCharacters($from->city);


        $model->_receiver_addressLine = S_Useful::removeNationalCharacters($to->address_line_1);
        $model->_receiver_addressLine2 = S_Useful::removeNationalCharacters($to->address_line_2);
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_name = S_Useful::removeNationalCharacters($to->name).' '.S_Useful::removeNationalCharacters($to->company);
        $model->_receiver_number = $to->tel;
        $model->_receiver_city = S_Useful::removeNationalCharacters($to->city);


        $model->_number = $courier->courier->packages_number;
        if($courier->cod)
        {
            $model->_cod = true;
            $model->_cod_currency = $courier->cod_currency;
            $model->_cod_amount = $courier->cod_value;
        }

        return $model;
    }

}