<?php

/* @var $model Courier */

/*$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);*/
?>

    <h2><?php echo Yii::t('courier','Przesyłka #{id}', array('{id}' => $model->local_id));?></h2>

<?php
$this->widget('FlashPrinter');
?>

<?php
if($model->courier_stat_id != CourierStat::CANCELLED && $model->courier_stat_id != CourierStat::NEW_ORDER && $model->courier_stat_id != CourierStat::CANCELLED_USER && !in_array($model->user_cancel, [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED, Courier::USER_CANCEL_CONFIRMED])):
    ?>
    <h4><?php echo Yii::t('courier','Etykieta'); ?></h4>

    <?php echo CHtml::link(Yii::t('courier', 'Pobierz etykietę'), array('/courier/courier/carriageTicket', 'urlData' => $model->hash), array('class' => 'btn btn-lg'));?>

    <?php
endif;
?>

    <h4><?php echo Yii::t('courier','Szczegóły przesyłki'); ?></h4>

<?php
$attributes = array(
    array(
        'name' => $model->getAttributeLabel('date_entered'),
        'value' => substr($model->date_entered,0,16)
    ),
    array(
        'name' => $model->getAttributeLabel('stat'),
        'value' => $model->stat->courierStatTr->full_text,
    ),
    array(
        'name' => $model->getAttributeLabel('package_weight'),
        'value' => $model->package_weight.' '.Courier::getWeightUnit(),
    ),
    array(
        'name' => Yii::t('courier','Wymiary'),
        'value' => $model->package_size_d.' '.Courier::getDimensionUnit().' x '.$model->package_size_l.' '.Courier::getDimensionUnit().' x '.$model->package_size_w.' '.Courier::getDimensionUnit(),
    ),
    array(
        'name' => Yii::t('courier','Liczba paczek'),
        'value' => $model->packages_number,
    ),
    array(
        'name' => Yii::t('courier','Nazwa usługi OOE'),
        'value' => $model->courierTypeOoe->service_name,
    )
);


if(OneWorldExpressPackage::isPalletways($model->courierTypeOoe->service_code))
{
    $temp = [
        'name' => $model->courierTypeOoe->getAttributeLabel('_palletway_size'),
        'value' => CourierTypeOoe::getPalletwayTypes()[$model->courierTypeOoe->_palletway_size]
    ];
    array_push($attributes, $temp);
};



?>


<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=> $attributes
));
?>

    <h3><?php echo Yii::t('courier','Dodatki');?></h3>
    <table class="list hTop" style="width: 500px;">
        <tr>
            <td><?php echo Yii::t('courier','Nazwa');?></td>
            <td><?php echo Yii::t('courier','Opis');?></td>
        </tr>
        <?php

        /* @var $item HybridMailAdditionList */
        if(!S_Useful::sizeof($model->courierTypeOoe->listAdditions()))
            echo '<tr><td colspan="2">'.Yii::t('courier','brak').'</td></tr>';
        else
            foreach($model->courierTypeOoe->listAdditions() AS $item): ?>
                <tr>
                    <td><?php echo $item->getClientTitle(); ?></td>
                    <td><?php echo $item->getClientDesc(); ?></td>
                </tr>

            <?php endforeach; ?>
    </table>

    <h4><?php echo Yii::t('courier','Nadawca'); ?></h4>

<?php

$this->renderPartial('//_addressData/_listInTable',
    array('model' => $model->senderAddressData,
    ));

?>

    <h4><?php echo Yii::t('courier','Odbiorca'); ?></h4>
<?php

$this->renderPartial('//_addressData/_listInTable',
    array('model' => $model->receiverAddressData,
    ));

?>

    <h4><?php echo Yii::t('courier','Historia');?></h4>

<?php
$this->renderPartial('//_statHistory/_statHistory',
    array('model' => $model->courierStatHistoriesVisible));
?>

    <br/>
<?php $this->widget('BackLink');?>