<?php

/* @var $this InternationalMailController */
/* @var $model InternationalMail */
/* @var $form CActiveForm */

?>

<div class="form">

    <h2><?php echo Yii::t('internationalMail','International Mail'); ?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '1/6')); ?> | <?php echo Yii::t('internationalMail','Dane listu'); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'international-mail-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));
    ?>

    <div class="flash-notice">
        <p><?php echo Yii::t('internationalMail','List musi spełniać następujące wymogi:');?></p>
        <ul>
            <li><?php echo $model->getAttributeLabel('mail_weight'); ?> < <?php echo S_InternationalMailMaxDimensions::getMaxDimensions()['weight'];?> <?php echo InternationalMail::getWeightUnit();?></li>
            <li><?php echo $model->getAttributeLabel('mail_size_l'); ?> < <?php echo S_InternationalMailMaxDimensions::getMaxDimensions()['l'];?> <?php echo InternationalMail::getDimensionUnit();?></li>
            <li><?php echo $model->getAttributeLabel('mail_size_d'); ?> < <?php echo S_InternationalMailMaxDimensions::getMaxDimensions()['d'];?> <?php echo InternationalMail::getDimensionUnit();?></li>
            <li><?php echo $model->getAttributeLabel('mail_size_w'); ?> < <?php echo S_InternationalMailMaxDimensions::getMaxDimensions()['w'];?> <?php echo InternationalMail::getDimensionUnit();?></li>
            <li><?php echo Yii::t('internationalMail','Suma trzech wymiarów <');?> <?php echo S_InternationalMailMaxDimensions::getMaxDimensions()['combination'];?> <?php echo InternationalMail::getDimensionUnit();?></li>
        </ul>
    </div>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php if(Yii::app()->user->isGuest):?>

        <div class="row">
            <?php echo CHtml::label(Yii::t('pay','Wybierz swoją walutę'),'currency');?>
            <?php echo CHtml::dropDownList('currency', $currency, CHtml::listData(PriceCurrency::model()->findAll(),'id','short_name'), array('id' => 'selected-currency')); ?>
        </div><!-- row -->
        <hr/>

        <script>
            $("#selected-currency").on('change', function(){
                $("#currency").html($(this).find("option:selected").text());
            })
        </script>

    <?php endif; ?>

    <div class="row">
        <?php echo $form->labelEx($model,'mails_number'); ?>
        <?php echo $form->dropDownList($model, 'mails_number', S_Useful::generateArrayOfItemsNumber()); ?>
        <?php echo $form->error($model,'mails_number'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'mail_weight'); ?>
        <?php echo $form->textField($model, 'mail_weight'); ?> <?php echo InternationalMail::getWeightUnit();?>
        <?php echo $form->error($model,'mail_weight'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'mail_size_l'); ?>
        <?php echo $form->textField($model, 'mail_size_l'); ?> <?php echo InternationalMail::getDimensionUnit();?>
        <?php echo $form->error($model,'mail_size_l'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'mail_size_w'); ?>
        <?php echo $form->textField($model, 'mail_size_w'); ?> <?php echo InternationalMail::getDimensionUnit();?>
        <?php echo $form->error($model,'mail_size_w'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'mail_size_d'); ?>
        <?php echo $form->textField($model, 'mail_size_d'); ?> <?php echo InternationalMail::getDimensionUnit();?>
        <?php echo $form->error($model,'mail_size_d'); ?>
    </div><!-- row -->



    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Dalej'),array('name'=>'step2', 'class' => 'btn-primary'));
        echo ' ';
        echo $jumpToEnd? TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'step5', 'class' => 'btn-small')):'';
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->