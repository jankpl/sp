<?php

Yii::import('application.models._base.BaseUserInvoiceHistory');

class UserInvoiceHistory extends BaseUserInvoiceHistory
{
    const WHAT_CHANGE_PAYMENT = 1;
    const WHAT_PAID = 2;
    const WHAT_STAT = 3;
    const WHAT_STAT_INV = 4;
    const WHAT_OTHER = 10;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(
            array('user_invoice_id, what, text', 'required'),
            array('id, user_invoice_id, what, admin_id', 'numerical', 'integerOnly'=>true),
            array('text', 'length', 'max'=>256),
            array('params', 'length', 'max'=>512),
            array('params, admin_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, user_invoice_id, date_entered, what, text, params', 'safe', 'on'=>'search'),
        );
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),
        );
    }

    public static function addOtherLog($invoice_id, $text)
    {
        $model = new self;
        if(Yii::app()->params['backend'])
            $model->admin_id = Yii::app()->user->id;

        $model->user_invoice_id = $invoice_id;
        $model->what = self::WHAT_OTHER;
        $model->text = $text;
        $model->save();
    }

    public static function add(UserInvoice $userInvoice)
    {
        if(!$userInvoice->_oldAttributes)
            return false;

        if($userInvoice->stat != $userInvoice->_oldAttributes['stat'])
        {
            $model = new self;
            if(Yii::app()->params['backend'])
                $model->admin_id = Yii::app()->user->id;

            $model->user_invoice_id = $userInvoice->id;
            $model->what = self::WHAT_STAT;
            $model->text = 'Stat changed: '.$userInvoice->_oldAttributes['stat'].' to '.$userInvoice->stat;
            $model->save();
        }

        if($userInvoice->stat_inv != $userInvoice->_oldAttributes['stat_inv'])
        {
            $model = new self;
            if(Yii::app()->params['backend'])
                $model->admin_id = Yii::app()->user->id;

            $model->user_invoice_id = $userInvoice->id;
            $model->what = self::WHAT_STAT;
            $model->text = 'Stat inv changed: '.$userInvoice->_oldAttributes['stat_inv'].' to '.$userInvoice->stat_inv;
            $model->save();
        }

        if($userInvoice->paid_value != $userInvoice->_oldAttributes['paid_value'])
        {
            $model = new self;

            if(Yii::app()->params['backend'])
                $model->admin_id = Yii::app()->user->id;

            $model->user_invoice_id = $userInvoice->id;

            if($userInvoice->paid_value == $userInvoice->inv_value)
            {
                $model->what = self::WHAT_PAID;
                $model->text = 'Invoice paid: '.$userInvoice->_oldAttributes['paid_value'].' to '.$userInvoice->paid_value;
            } else {
                $model->what = self::WHAT_CHANGE_PAYMENT;
                $model->text = 'Payment changed: '.$userInvoice->_oldAttributes['paid_value'].' to '.$userInvoice->paid_value;
            }

//            $model->validate();

            $model->save();
        }

    }

}