<?php
/* @var $model User */
?>

<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'block-notify-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'notifications']),
    ));
    ?>


    <h3><?php echo Yii::t('user', 'Blokuj powiadomienia');?></h3>

    <table class="table">
        <tr>
            <td><?php echo $form->checkBox($model, 'clearEmails'); ?></td>
            <td><?php echo Yii::t('user', 'Zaznacz, jeżeli chcesz, aby wszystkie wprowadzone i importowane adresy e-mail nadawców i odbiorców były czyszczone.');?></td>
        </tr>
        <tr>
            <td><?php echo $form->checkBox($model, 'blockNotify'); ?></td>
            <td><?php echo Yii::t('user', 'Zaznacz, jeżeli nie chcesz otrzymywać od nas żadnych powiadomień poprzez pocztę e-mail (z wyjątkiem przypomnienia hasła).');?></td>
        </tr>
        <?php
        if($model->hasUserAvailableSmsOptions()):
            ?>
            <tr>
                <td><?php echo $form->checkBox($model, 'blockNotifySms'); ?></td>
                <td><?php echo Yii::t('user', 'Zaznacz, jeżeli nie chcesz otrzymywać od nas żadnych powiadomień poprzez SMS.');?></td>
            </tr>
        <?php
        endif;
        ?>
        <tr>
            <td><?php echo $form->checkBox($model, 'disableOrderNotification'); ?></td>
            <td><?php echo Yii::t('user', 'Zaznacz, jeżeli nie chcesz otrzymywać od nas powiadomień poprzez pocztę e-mail dotyczących Zamówień.');?></td>
        </tr>
        <tr>
            <td colspan="2"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleBlockNotify'));?></td>
        </tr>
    </table>

    <h3><?php echo Yii::t('user', 'Ustawienia powiadomień dla Courier i Postal');?></h3>

    <table class="table table-striped">
        <thead>
        <tr>
            <th rowspan="2" class="text-right"><?= Yii::t('user', 'Status');?></th>
            <th colspan="2" class="text-center"><?= Yii::t('user', 'Account owner');?></th>
            <th colspan="2" class="text-center"><?= Yii::t('user', 'Sender');?></th>
            <th colspan="2" class="text-center"><?= Yii::t('user', 'Receiver');?></th>
        </tr>
        <tr class="text-center">
            <td><a href="#notifications" data-select-all="notification_setting_email[o]"><?= Yii::t('user', 'E-mail');?></a></td>
            <td><a href="#notifications" data-select-all="notification_setting_sms[o]"><?= Yii::t('user', 'SMS');?></a></td>
            <td><a href="#notifications" data-select-all="notification_setting_email[s]"><?= Yii::t('user', 'E-mail');?></a></td>
            <td><a href="#notifications" data-select-all="notification_setting_sms[s]"><?= Yii::t('user', 'SMS');?></a></td>
            <td><a href="#notifications" data-select-all="notification_setting_email[r]"><?= Yii::t('user', 'E-mail');?></a></td>
            <td><a href="#notifications" data-select-all="notification_setting_sms[r]"><?= Yii::t('user', 'SMS');?></a></td>
        </tr>
        </thead>
        <?php
        foreach(StatMap::mapNameList() AS $key => $item):
            ?>
            <tr class="text-center">
                <td class="text-right"><?= $item;?></td>
                <td><?= CHtml::checkBox('notification_setting_email[o]['.$key.']', in_array($key, $model->getNotificationCustomSettings('email', 'o')));?></td>
                <td><?= CHtml::checkBox('notification_setting_sms[o]['.$key.']', in_array($key,$model->getNotificationCustomSettings('sms', 'o')));?></td>
                <td><?= CHtml::checkBox('notification_setting_email[s]['.$key.']', in_array($key, $model->getNotificationCustomSettings('email', 's')));?></td>
                <td><?= CHtml::checkBox('notification_setting_sms[s]['.$key.']', in_array($key,$model->getNotificationCustomSettings('sms', 's')));?></td>
                <td><?= CHtml::checkBox('notification_setting_email[r]['.$key.']', in_array($key, $model->getNotificationCustomSettings('email', 'r')));?></td>
                <td><?= CHtml::checkBox('notification_setting_sms[r]['.$key.']', in_array($key,$model->getNotificationCustomSettings('sms', 'r')));?></td>
            </tr>
        <?php
        endforeach;
        ?>

        <tr>
            <td colspan="3"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'saveNotificationSettings'));?> <?php echo TbHtml::submitButton(Yii::t('app', 'Restore default'), array('name' => 'restoreDefaultNotificationSettings', 'class' => 'btn-xs'));?></td>
        </tr>
    </table>

    <?php
    $this->endWidget();
    ?>
</div>
<script>
    $('[data-select-all]').on('click', function(){
        var option = -1;
        $('input[name^="' + $(this).attr('data-select-all') + '"]').each(function(i,v)
        {
            if(option === -1) {
                if($(v).is(':checked'))
                    option = false;
                else
                    option = true;
            }

            $(v).prop('checked', option);
        });
        option = -1;
    });
</script>