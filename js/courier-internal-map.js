(function($) {

    var firstSelection = true;

    var MAP_COOKIE_NAME = 'INTERNAL_MAP_POSITION';

    var types = [];


    window.CourierInternalMap = function(){};

    var map;
    var dymek = new google.maps.InfoWindow();
    var markersArray = [];

    var markersUniqId = [];

    var selectedType = false;
    var typeChanged = false;

    var mapStarted = false;

    var $showPointsButton;

    var $loader;

    var $locatorInput;

    var spPointsReady = false;

    var selectPointIdleLock = false;


    function addMarker(lat,lng,txt,id, imgUrl, type, uniqId)
    {

        // debugger;
        if(markersArray[type] === undefined)
            return;

        // debugger;
        if ($.inArray(uniqId, markersUniqId[type]) == -1) {
            // console.log('NOT FOUND: ' + uniqId);

            // debugger;
            var img = {
                url: imgUrl,
                size: new google.maps.Size(32,32),
                orgin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(16,32)
            };

            var markerOptions =
                {
                    position: new google.maps.LatLng(lat,lng),
                    map: map,
                    icon: img
                };

            var marker = new google.maps.Marker(markerOptions);
            marker.txt=txt;


            if(selectedType && selectedType != type)
                marker.setVisible(false);

            google.maps.event.addListener(marker,'click',function()
            {
                $('#pickup_point_id').val(uniqId);
                $('#with-pickup').val(outcomingType(type) - 0);

                var $img = $('<img>');
                $img.attr('src', imgUrl);
                $img.css('margin-right', '10px');

                // txt = txt.replace(/\n\s*\n/g, '');
                // txt = txt.replace(/(<br\s*\/?>){3,}/gi, '<br>');

                var $selectedSpPointList = $('#sp-point-list');

                if((outcomingType(type) != yii.pickUp.WITH_PICKUP_NONE || ((outcomingType(type) == yii.pickUp.WITH_PICKUP_NONE && uniqId != $selectedSpPointList.val()))))
                    $selectedSpPointList.val('');

                $('#selected-point-text').html('<strong>' + yii.pickUpNames[(outcomingType(type) - 0)] + '</strong><br/><br/>' + txt).prepend($img);
                $('#pickup_point_text').val(txt);
                $('#pickup_point_img').val(imgUrl);
                $('#add-point-to-fav').show();

                if(outcomingType(type) == yii.pickUp.WITH_PICKUP_DHLDE)
                {
                    var country = txt.slice(-2);
                    $('#pickup_point_country').val(country);
                }

                dymek.close();
                dymek = new google.maps.InfoWindow();
                dymek.setContent(marker.txt);
                dymek.open(map,marker);

                $('[data-pickup-alert]').hide();
                $('[data-pickup-alert="' +(outcomingType(type) - 0) + '"]').show();

                // if(type - 0 == Yii.pickUp.WITH_PICKUP_INPOST)
                //     $('#inpost-receiver-part').hide();

                onSelectType(type, true);

                $('#fav-point-list').val('');

                if((outcomingType(type) != yii.pickUp.WITH_PICKUP_NONE))
                    $('#sp-point-list').val('');

                selectPointIdleLock = true;

            });

            if(type == incomingType(yii.pickUp.WITH_PICKUP_NONE))
                markersArray[type][uniqId] = marker; // for SP Points use ID to index array to allow accesing
            else
                (markersArray[type]).push(marker);

            (markersUniqId[type]).push(uniqId);
        }
        // else
        // console.log('FOUND: ' + uniqId);

    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $.extend(CourierInternalMap, {

        selectSpPoint : function(id)
        {
            console.log('Select SP Point');

            if(!mapStarted)
                return false;

            console.log('Select SP Point - GO');

            var pickupType = incomingType(yii.pickUp.WITH_PICKUP_NONE);

            onSelectType(pickupType, true);

            var point = (markersArray[pickupType][id]);

            var latLng = point.getPosition(); // returns LatLng object
            map.setCenter(latLng);
            new google.maps.event.trigger( point, 'click' );

            // $('[data-point-type=' + yii.pickUp.WITH_PICKUP_NONE + ']').trigger('click');
        },

        mapStart: function(){

            setTimeout(function(){

                if(mapStarted)
                    return false;

                $.each(types, function (i, v) {
                    markersArray[v] = [];
                    markersUniqId[v] = [];
                });

                $loader.hide();
                $locatorInput.prop('disabled', false);

                // if(type > 0)
                //     selectedType = type;

                var position = getCookie(MAP_COOKIE_NAME);

                var lat = 52.887719;
                var lng = 21.390474;
                var zoom = 9;

                var disableGeolocation = false;
                if(position != '')
                {
                    position = position.split(',');

                    if(parseFloat(position[0]) >1 && parseFloat(position[1]) > 0 && parseFloat(position[2]) > 0) {

                        lat = position[0];
                        lng = position[1];
                        zoom = parseInt(position[2]);
                        disableGeolocation = true;

                    }
                }

                var mapOptions =
                    {
                        center: new google.maps.LatLng(lat, lng),
                        zoom: zoom,
                        mapTypeId: google.maps.MapTypeId.MAP
                    };

                if (!disableGeolocation && navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {

                        var lat = position.coords.latitude;
                        var lng = position.coords.longitude;

                        mapOptions =
                            {
                                center: new google.maps.LatLng(lat, lng),
                                zoom: 9,
                                mapTypeId: google.maps.MapTypeId.MAP
                            };
                        map = new google.maps.Map(document.getElementById('sp-points-map'), mapOptions);
                        map.addListener('idle', function() {
                            // getLocalPoints();
                            $loader.hide();
                            console.log('idle1');
                            $showPointsButton.show();
                        });

                    }, function() {

                        map = new google.maps.Map(document.getElementById('sp-points-map'), mapOptions);
                        map.addListener('idle', function() {
                            // getLocalPoints();
                            $loader.hide();
                            console.log('idle2');
                            $showPointsButton.show();
                        });
                    });
                } else {

                    map = new google.maps.Map(document.getElementById('sp-points-map'), mapOptions);

                    map.addListener('idle', function() {
                        // getLocalPoints();
                        $loader.hide();
                        console.log('idle3');
                        if(!selectPointIdleLock)
                            $showPointsButton.show();

                        selectPointIdleLock = false;
                    });
                }
                mapAddPointsSp();


                // mapStarted = true;

            }, 500);
        }


    });


    var currentLat = false
    var currentLng = false;
    var currentZoom = false;
    var cache = [];

    function getLocalPoints()
    {
        console.log('GLP!');


        $showPointsButton.hide();

        $loader.show();
        $locatorInput.prop('disabled', true);

        var center = map.getCenter();
        var lat = center.lat();
        var lng = center.lng();

        var zoom = map.getZoom();

        console.log('add cookie!' + lat + '|' + lng + '|' + zoom);
        // remember last location:
        var d = new Date();
        d.setTime(d.getTime() + (7*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = MAP_COOKIE_NAME + "=" + lat + ',' + lng + ',' + zoom + ";" + expires + ";path=/";

        lat = Math.round(100 * lat) / 100;
        lng = Math.round(100 * lng) / 100;

        getAddress(lat, lng, zoom);
    }

    function getAddress(lat, lng, zoom)
    {
        var $loader = $('#map-data-loader');


        if(currentLat == lat && currentLng == lng && currentZoom == zoom && !typeChanged) {


            console.log('bez zmian...');
            $loader.hide();
            $locatorInput.prop('disabled', false);
            return false;
        }

        currentLat = lat;
        currentLng = lng;
        currentZoom = zoom;
        var cacheReturn = false;

        function checkCache(v,i)
        {
            if(cacheReturn)
                return;

            if(v.lat == lat && v.lng == lng && v.zoom == zoom) {
                console.log('in cache!');

                cacheReturn = true;
                return;
            }
            cacheReturn = false;
        }

        if(!typeChanged)
            cache.forEach(checkCache);

        console.log('CR = ' + cacheReturn);

        if(cacheReturn)
        {
            console.log('cached!');
            $loader.hide();
            return true;
        } else {
            typeChanged = false;
            console.log('query gm...');

            $.ajax({
                url: yii.urls.googleMapsApiByCoordinates,
                data: {lat: lat, lng: lng},
                method: 'POST',
                dataType: 'json',
                success: function (result) {

                    var zip_code = false;

                    console.log('gm ready!');
                    if (result.status == 'OK') {

                        var pglr = parseGeoLocationResults(result.results[0].address_components);

                        if (result.results[0]) {

                            var temp3 = result.results[0];
                            temp3 = temp3.address_components;
                            for (var i = 0; i < temp3.length; i++) {
                                var temp2 = temp3[i];
                                if (temp2.types[0] == 'postal_code') {
                                    zip_code = temp2.long_name;
                                    break;
                                }
                            }
                        }

                        result = result.results[0];

                        var cc = pglr.country;

                        result = result.formatted_address;
                        var temp = [];
                        temp.lat = lat;
                        temp.lng = lng;
                        temp.zoom = zoom;
                        temp.data = result;

                        cache.push(temp);
                    }
                    mapAddPoints(result, zip_code, cc);

                    // $loader.hide();
                },
                error: function (e) {
                    console.log(e);
                    alert("whoooops...");
                }
            });
        }
    }

    function parseGeoLocationResults(result) {
        var parsedResult = new Object();
        var address_components = result;

        for (var i = 0; i < address_components.length; i++) {
            for (var b = 0; b < address_components[i].types.length; b++) {
                // if (address_components[i].types[b] == "street_number") {
                //     //this is the object you are looking for
                //     parsedResult.street_number = address_components[i].long_name;
                //     break;
                // }
                // else if (address_components[i].types[b] == "route") {
                //     //this is the object you are looking for
                //     parsedResult.street_name = address_components[i].long_name;
                //     break;
                // }
                // else if (address_components[i].types[b] == "sublocality_level_1") {
                //     //this is the object you are looking for
                //     parsedResult.sublocality_level_1 = address_components[i].long_name;
                //     break;
                // }
                // else if (address_components[i].types[b] == "sublocality_level_2") {
                //     //this is the object you are looking for
                //     parsedResult.sublocality_level_2 = address_components[i].long_name;
                //     break;
                // }
                // else if (address_components[i].types[b] == "sublocality_level_3") {
                //     //this is the object you are looking for
                //     parsedResult.sublocality_level_3 = address_components[i].long_name;
                //     break;
                // }
                // else if (address_components[i].types[b] == "neighborhood") {
                //     //this is the object you are looking for
                //     parsedResult.neighborhood = address_components[i].long_name;
                //     break;
                // }
                // else if (address_components[i].types[b] == "locality") {
                //     //this is the object you are looking for
                //     parsedResult.city = address_components[i].long_name;
                //     break;
                // }
                // else if (address_components[i].types[b] == "administrative_area_level_1") {
                //     //this is the object you are looking for
                //     parsedResult.state = address_components[i].long_name;
                //     break;
                // }
                //
                // else if (address_components[i].types[b] == "postal_code") {
                //     //this is the object you are looking for
                //     parsedResult.zip = address_components[i].long_name;
                //     break;
                // }
                if (address_components[i].types[b] == "country") {
                    //this is the object you are looking for
                    parsedResult.country = address_components[i].short_name;
                    break;
                }
            }
        }

        console.log('PR:');
        console.log(parsedResult.country);

        return parsedResult;
    }

    function mapAddPointsSp()
    {

        if(spPointsReady)
            return true;

        spPointsReady = true;

        console.log('Add SP Points');
        $.ajax({
            url: yii.urls.listOfPointsSp,
            method: 'POST',
            dataType: 'json',
            success: function (result) {

                // debugger;
                $.each(result, function(i,v){
                    addMarker(v.lat, v.lng,'<strong>' + v.name + '</strong><br />' + v.desc + '<br/><br/>' + v.address, v.id, v.imgUrl, incomingType(v.type), v.uniqId);
                });

                mapStarted = true;


                var activeType = $('[data-active-type]').attr('data-active-type');
                if(activeType)
                    onSelectType(incomingType(activeType), false);
            },
            error: function (e) {

                console.log(e);

                // alert("whoooops...");
            }
        });
    }

    function mapAddPoints(address, zip_code, cc)
    {

        var typeToBackend = outcomingType(selectedType) ? outcomingType(selectedType) : 'ALL';

        console.log('typeToBackend :');
        console.log(typeToBackend);

        var $loader = $('#map-data-loader');
        $.ajax({
            url: yii.urls.listOfPoints,
            data: { address: address, zip_code: zip_code, type: typeToBackend, country_code: cc},
            method: 'POST',
            dataType: 'json',
            success: function (result) {

                // debugger;
                $.each(result, function(i,v){
                    addMarker(v.lat, v.lng,'<strong>' + v.name + '</strong><br />' + v.desc + '<br/><br/>' + v.address, v.id, v.imgUrl, incomingType(v.type), v.uniqId);
                });

                console.log('M:');
                console.log(markersArray);

                $loader.hide();
                $locatorInput.prop('disabled', false);


            },
            error: function (e) {

                console.log(e);

                // alert("whoooops...");
            }
        });
    }

// function onSelectPoint()
// {
//
//     var markerId = $(this).val();
//
//     if(markerId == '')
//     {
//         map.panTo(new google.maps.LatLng(52.887719, 21.390474));
//         map.setZoom(4);
//
//
//     } else {
//         var marker = markersArray[markerId];
//
//         map.panTo(marker.getPosition());
//         map.setZoom(14);
//         google.maps.event.trigger(marker,'click');
//
//         spPointSelector.unbind('change');
//         spPointSelector.val(markerId);
//         spPointSelector.on('change', onSelectPoint);
//     }
//
// }
    function incomingType(val)
    {
        if(val === false || val === '')
            return false;
        else
            return parseInt(val) + 1;
    }

    function outcomingType(val)
    {
        if(val === false || val === '')
            return false;
        else
            return parseInt(val) - 1;
    }

    function onSelectType(type, blockLoading)
    {
        if(type === false)
            type = parseInt(0);

        console.log('NT:' + type);
        console.log('ST: ' + selectedType);

        if(type === selectedType)
            return false;

        $('[data-point-type]').css('font-weight', 'normal');
        $('[data-point-type=' + outcomingType(type) + ']').css('font-weight', 'bold');

        $showPointsButton.show();
        typeChanged = true;

        if(type == '')
        {
            $.each(types, function(i,v){

                $.each(markersArray[v], function(ii,vv){

                    if(vv !== undefined) {
                        vv.setVisible(true);
                    }
                });

            });


        } else {

            // console.log('MA:' + markersArray);

            $.each(markersArray[type], function(i,v){
                if(v !== undefined) {

                    v.setVisible(true);
                }
            });


            $.each(types, function(i,v){

                if(v != type) {

                    // console.log('LEN: ' + v + ' -> ' + markersArray[v].length);

                    $.each(markersArray[v], function (ii, vv) {

                        if(vv !== undefined) {

                            vv.setVisible(false);
                        }
                    });
                }
            });

        }

        selectedType = type;

        console.log('NST:' + type);

        console.log('MS: ' + mapStarted);
        if(mapStarted && !blockLoading) {
            console.log('spb t!');
            $('#show-points-button').trigger('click');
        }



        $('[data-pickup-alert]').hide();
        $('[data-pickup-alert="' +(outcomingType(type) - 0) + '"]').show();



        if((outcomingType(type)  - 0) == yii.pickUp.WITH_PICKUP_RUCH)
        {
            $('#ruch-receiver-part').show();
            $('#inpost-receiver-part').hide();
        } else {
            $('#ruch-receiver-part').hide();


            var sender_country_id = $('[data-country="sender"]').val();
            var receiver_country_id = $('[data-country="receiver"]').val();
            var weight = $('[data-package-weight]').val();
            var size_l = $('#Courier_CourierTypeInternal_package_size_l').val();
            var size_w = $('#Courier_CourierTypeInternal_package_size_w').val();
            var size_d = $('#Courier_CourierTypeInternal_package_size_d').val();


            $.ajax({
                url: yii.urls.checkIfCarriageAvailable,
                data: { sender_country_id: sender_country_id, receiver_country_id: receiver_country_id, weight: weight, size_l : size_l, size_w : size_w, size_d : size_d, with_pickup : outcomingType(type) },
                method: 'POST',
                dataType: 'json',
                success: function(result){

                    if(result.result)
                    {
                        var deliveryBrokerId = result.deliveryBrokerId;

                        if(receiver_country_id && receiver_country_id == yii.ids.countryDe && deliveryBrokerId) {
                            $('[data-delivery-broker-id-show]').not('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideUp();
                            $('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideDown();
                        }
                        else if(receiver_country_id && receiver_country_id == yii.ids.countryLt && deliveryBrokerId) {
                            $('[data-delivery-broker-id-show]').not('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideUp();
                            $('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideDown();
                        }
                        else if(receiver_country_id && receiver_country_id == yii.ids.countryPl && deliveryBrokerId) {

                            $('[data-delivery-broker-id-show]').not('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideUp();
                            $('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideDown();
                        }
                        else
                            $('[data-delivery-broker-id-show]').hide();

                    }
                }

            });

        }
    }

    $(document).ready(function(){


        var availableTypes = yii.pickupTypes;

        if(Array.isArray(availableTypes))
        {
            availableTypes.forEach(function(i,v) {
                types.push(incomingType(i));
            });
        }

        $showAllPointsButton = $('#show-all-points');
        $mapContainer = $('#map-container');
        $favContainer = $('#fav-container');
        $favPointList = $('#fav-point-list');
        $spPointList = $('#sp-point-list');
        $showPointsButton = $('#show-points-button');

        $showAllPointsButton.on('click', function(){
            selectedType = false;
            $(this).hide();
            $favPointList.val('');
            $spPointList.val('');
            $mapContainer.slideDown('normal', function(){
                // $showPointsButton.trigger('click');
            });
        });

        $favPointList.on('change', function(){
            $showAllPointsButton.show();
            $mapContainer.slideUp();

            var val = ($(this).val());
            val = val.split('|');

            onSelectType(incomingType(parseInt(val[0])), true);
        });

        $loader = $('#map-data-loader');

        $locatorInput = $('#map-location');


        $locatorInput.keydown(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                $(this).blur();
            }
        });

        $showPointsButton.on('click', function(){
            $loader.show();
            $(this).hide();
            // alert("AAA");
            getLocalPoints();
        });

        $loader.show();
        $locatorInput = $('#map-location');
        $locatorInput.prop('disabled', true);


        $('[data-point-type]').css('cursor', 'pointer');
        $('[data-point-type]').on('click', function(){

            var type =  $(this).attr('data-point-type');

            type = incomingType(type);

            if(type != selectedType)
                onSelectType(type, false);
            else {
                onSelectType(false, true);
                $(this).css('font-weight', 'normal');
            }
        });


        $locatorInput.on('change', function(){

            $loader.show();
            $locatorInput.prop('disabled', true);

            var address = $(this).val();

            $.ajax({
                url: yii.urls.googleMapsApiByAddress,
                data: {address: address},
                method: 'POST',
                dataType: 'json',
                success: function (result) {

                    console.log(result);

                    if(result.status == 'OK')
                    {

                        var detail = (result.results[0].address_components).length;

                        if(detail == 1)
                            zoom = 6;
                        else if(detail == 5)
                            zoom = 12;
                        else if(detail == 8)
                            zoom = 16;
                        else
                            zoom = 2 + detail * 2;

                        result = result.results[0];

                        var formatted_address = result.formatted_address;
                        var lat = result.geometry.location.lat;
                        var lng = result.geometry.location.lng;


                        $('#map-location').val(formatted_address);
                        map.setCenter({lat: lat, lng: lng} );
                        map.setZoom(zoom);
                    }

                },
                error: function (e) {
                    console.log(e);
                    alert("whoooops...");
                }
            });


        });

    });

    $(document).ready(function(){

        var pt = yii.pickupTypeFav;

        var currentPickupOptions = $('#with-pickup').val();

        if(!currentPickupOptions) {
            if (!pt)
                pt = yii.pickUp.WITH_PICKUP_REGULAR;

            if (pt[0] == 'F') {
                pt = pt.replace('F@@@', '');
                $('[data-with-pickup-id="-1"]').trigger('click');
                $('#fav-point-list').val(pt).trigger('change');
            } else {
                $('[data-with-pickup-id=' + pt + ']').trigger('click');
            }

        }
    });

})(jQuery);