<?php

Yii::import('application.modules.courier.models._base.BaseCourierPriceValue');

class CourierPriceValue extends BaseCourierPriceValue
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function afterConstruct()
    {
        $this->pricePerWeight = new Price();
        $this->pricePerItem = new Price();

        return parent::afterConstruct();
    }

    public function behaviors() {
        return array(
            'ECompositeUniqueKeyValidatable' => array(
                'class' => 'ext.ECompositeUniqueKeyValidatable.ECompositeUniqueKeyValidatable',
                'uniqueKeys' => array(
                    'errorAttributes' => 'weight_top_limit',
                    'attributes' => 'weight_top_limit, courier_price_id',
                    'errorMessage' => 'Górna granica wagi nie może się powtarzać',
                    'skipOnErrorIn' => 'weight_top_limit',
                )
            ),
        );
    }

    /**
     * Validates composite unique keys
     *
     * Validates composite unique keys declared in the
     * ECompositeUniqueKeyValidatable bahavior
     */
    public function compositeUniqueKeysValidator() {
        $this->validateCompositeUniqueKeys();
    }

    public function atLeastOnePrice($attribute, $params)
    {

        $price1 = 'price_per_item';
        $price2 = 'price_per_weight';

        if($this->$price1->value == 0 && $this->$price2->value == 0)
        {
            $this->addError($price1, 'Provide at least one price!');
            $this->addError($price2, 'Provide at least one price!');
        }
    }

    public function rules() {
        return array(
//            array('*', 'compositeUniqueKeysValidator'),
            //array('*', 'atLeastOnePrice'),


            array('courier_price_id, weight_top_limit, price_per_weight, price_per_item', 'required'),
            array('courier_price_id', 'numerical', 'integerOnly'=>true),
            array('price_per_item', 'numerical', 'integerOnly'=>true),
            array('price_per_weight', 'numerical', 'integerOnly'=>true),
            array('weight_top_limit', 'numerical', 'min' => 0.01, 'max' => 9999),

            array('id, weight_top_limit, price_per_weight, price_per_item, courier_price_id', 'safe', 'on'=>'search'),
        );
    }


    public function gridViewData($id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition("courier_price_id = '$id'");

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pagesize' => 5,
            )

        ));
    }
}