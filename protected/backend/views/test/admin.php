<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>


<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO,'You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.', array('closeText' => false));?>

<?php
$this->widget('MultipleSelect', array('selector' => '#stat-selector'));

?>

<br/><br/>
<div style="word-break: break-all">
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'courier-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows'=>2,
        'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'afterAjaxUpdate' => true), true),
        'columns' => array(
            array(
                'name'=>'local_id',
                'header'=>'# local ID',
                'type'=>'raw',
                'value'=>'CHtml::link($data->local_id,array("/courier/courier/view/", "id" => $data->id))',
            ),
            array(
                'name' => '__type',
                'header'=>'Type',
                'type'=>'raw',
                'value'=> '$data->typeName',
                'filter' => Courier::$types,
            ),
            array(
                'name'=>'__sender_country_id',
                'header'=>'Sender country',
                'value'=>'$data->senderAddressData->country',
                'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
            ),
            array(
                'name' => '__sender_usefulName',
                'header' => 'Sender',
                'value' => '$data->senderAddressData->usefulName',
            ),
            array(
                'name'=>'__receiver_country_id',
                'header'=>'Receiver country',
                'value'=>'$data->receiverAddressData->country',
                'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
            ),
            array(
                'name' => '__receiver_name',
                'header' => 'Receiver',
                'value' => '$data->receiverAddressData->usefulName',
            ),
            array(
                'name'=>'date_entered',
                'header'=>'Created',
            ),
            array(
                'name'=>'date_updated',
                'header'=>'Updated',
            ),
            array(
                'name'=>'__user_login',
                'header'=>'User',
                'value' => '$data->user->login',
            ),
            array(
                'name' => 'daysFromStatusChange',
                'header' => 'Status age (days)',
                'filter' => false,
            ),
            array(
                'name'=>'__internal_operator',
                'header'=>'Int. operator',
                'value'=>'$data->getIntOperator()',
            ),
            array(
                'name'=>'__external_operator',
                'header'=>'Ext. operator',
                'value'=>'$data->courierTypeExternal->courier_external_operator_name',
                'filter'=>CHtml::listData(CourierExternalOperator::model()->findAll(),'id','name'),
            ),
            array(
                'name'=>'__stat',
                'value'=>'$data->stat->name',
//                'filter'=>GxHtml::listDataEx(CourierStat::model()->findAllAttributes(null, true)),
                'filter'=> CHtml::activeDropDownList($model, 'courier_stat_id', CHtml::listData(CourierStat::model()->findAll(),'id','name'), array('multiple' => 'true', 'size' => 1, 'id' => 'stat-selector')),
                'headerHtmlOptions' => array('style' => 'width: 150px;'),
            ),
            'packages_number',
            /*array(
                'class' => 'CButtonColumn',
            ),*/
            array(
                'class'=>'CCheckBoxColumn',
                'id'=>'selected_rows'
            ),

        ),
    )); ?>
</div>




