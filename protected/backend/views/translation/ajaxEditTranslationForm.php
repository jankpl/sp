<?php
/* @var $array Array */
/* @var $path String */
?>

<h2>Translate</h2>

<div class="form">

    <?php echo CHtml::form();?>
    <?php echo CHtml::hiddenField('path', $path); ?>

    <table class="list hTop">
        <tr>
            <td style="width: 50%;">Orginal text</td>
            <td>Translation</td>
        </tr>
        <?php
        foreach($array AS $key => $item):
            ?>

            <tr>
                <td><?php echo $key; ?></td>
                <td><?php echo CHtml::textField('data['.$key.']',$item, array('style' => 'width: 350px;')); ?></td>
            </tr>

        <?php
        endforeach;
        ?>
    </table>

    <div class="row">
        <script>
       $(document).ready(function(){

           $("#save").click(function(){
               <?php echo CHTML::ajax(array(
              'dataType' => 'json',
              'url' => Yii::app()->createUrl('translation/ajaxSaveTranslationForm'),
              'type' => 'post',
              //'data' => 'js:{language_id : selector.val()}',
              'success' => 'function(result) {
                  $("#result").html(result);
              }',
              'error' => 'function(xhr, ajaxOptions, thrownError){
                  alert(xhr.status);
                  alert(thrownError);
              }'
              )); ?>
           });

       });

        </script>
        <input type="button" value="Save" id="save"/>

    </div>

    <?php echo CHtml::endForm(); ?>
</div>