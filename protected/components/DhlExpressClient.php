<?php

class DhlExpressClient
{

    const CUSTOMER_PN = 1;
    const CUSTOMER_FB = 2;
    const CUSTOMER_SP = 3;
    const CUSTOMER_ROMAD_EX = 4;
    const CUSTOMER_ROMAD_IM = 5;

    const USERNAME = GLOBAL_CONFIG::DHLEXPRESS_USERNAME;
    const PASSWORD = GLOBAL_CONFIG::DHLEXPRESS_PASSWORD;
    const ACCOUNT_NO = GLOBAL_CONFIG::DHLEXPRESS_ACCOUNT_NO;

    const USERNAME_FB = GLOBAL_CONFIG::DHLEXPRESS_FB_USERNAME;
    const PASSWORD_FB = GLOBAL_CONFIG::DHLEXPRESS_FB_PASSWORD;
    const ACCOUNT_NO_FB = GLOBAL_CONFIG::DHLEXPRESS_FB_ACCOUNT_NO;

    const USERNAME_SP = GLOBAL_CONFIG::DHLEXPRESS_SP_USERNAME;
    const PASSWORD_SP = GLOBAL_CONFIG::DHLEXPRESS_SP_PASSWORD;
    const ACCOUNT_NO_SP = GLOBAL_CONFIG::DHLEXPRESS_SP_ACCOUNT_NO;

    const USERNAME_ROMAD_IM = 'romadltdgb';
    const PASSWORD_ROMAD_IM = 'B#4dX$7dL$1x';
    const ACCOUNT_NO_ROMAD_IM = '965184649';

    const USERNAME_ROMAD_EX = 'romadltdgb';
    const PASSWORD_ROMAD_EX = 'B#4dX$7dL$1x';
    const ACCOUNT_NO_ROMAD_EX = '421355680';

    const URL = GLOBAL_CONFIG::DHLEXPRESS_URL;
    const URL_TRACKING = GLOBAL_CONFIG::DHLEXPRESS_URL_TRACKING;
    const URL_EPOD = GLOBAL_CONFIG::DHLEXPRESS_URL_EPOD;

    protected $_selectedDhlCustomer;
    protected $_account_no;
    protected $_username;
    protected $_password;

    public $reference;

    public $package_value;
    public $value_currency;
    public $package_content;

    public $sender_name;
    public $sender_company;
    public $sender_address_line_1;
    public $sender_address_line_2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_tel;
    public $sender_email;
    public $_sender_country_model;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address_line_1;
    public $receiver_address_line_2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_tel;
    public $receiver_email;
    public $_receiver_country_model;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $packages_number = 1;

    public $with_collect = false;

    public $cod_value = 0;

    public $service_type;


    public $_special_service_id8000 = false;

    public $_isUe;

    public $_user_id;

    public $label_annotation = false;

    const SERVICE_STD_UE = 'U'; // paczlka po UE
    const SERVICE_STD_NON_UE = 'P'; // Paczka poza UE towarowa

    // needs to be check for location:
    const SERVICE_UE_12 = 'T'; // Przesyka po UE do 12 lub tylko dokumenty poza UE
    const SERVICE_UE_9 = 'K'; // Przesylka po UE do 9 lub tylko dokumenty poza EU
    const SERVICE_NON_UE_9 = 'E'; // towar poza UE do 09:00
    const SERVICE_NON_UE_12 = 'Y'; // towar poza UE do 12:00

    const SERVICE_USA_1030 = 'M'; // towar do USA do 10:30

    const SERVICE_BREAK_BULK = 'B'; // Break Bulk
    const SERVICE_XPD = 'X'; // XPD

    const SERVICE_UE_ECONOMY = 'H'; // Economy Select (drogówka) po UE
    const SERVICE_NON_UE_ECONOMY = 'W'; // Economy Select (drogówka) poza UE

    public static function serviceTypeToCourierAdditionId($serviceType)
    {
        Yii::app()->getModule('courier');

        switch($serviceType)
        {
            case self::SERVICE_UE_12:
                return CourierAdditionList::UNIQID_DHLEXPRESS_UE_12;
                break;
            case self::SERVICE_UE_9:
                return CourierAdditionList::UNIQID_DHLEXPRESS_UE_9;
                break;
            case self::SERVICE_NON_UE_12:
                return CourierAdditionList::UNIQID_DHLEXPRESS_NONUE_12;
                break;
            case self::SERVICE_NON_UE_9:
                return CourierAdditionList::UNIQID_DHLEXPRESS_NONUE_9;
                break;
            case self::SERVICE_USA_1030:
                return CourierAdditionList::UNIQID_DHLEXPRESS_USA_1030;
                break;
            case self::SERVICE_BREAK_BULK:
                return CourierAdditionList::UNIQID_DHLEXPRESS_BREAKBULK;
                break;
            case self::SERVICE_XPD:
                return CourierAdditionList::UNIQID_DHLEXPRESS_XPD;
                break;
            case self::SERVICE_UE_ECONOMY;
                return CourierAdditionList::UNIQID_DHLEXPRESS_UE_ECONOMY;
                break;
            case self::SERVICE_NON_UE_ECONOMY:
                return CourierAdditionList::UNIQID_DHLEXPRESS_NONUE_ECONOMY;
                break;
            default:
                return false;
                break;
        }
    }

    public static function serviceTypeToCourierUAdditionId($serviceType)
    {
        Yii::app()->getModule('courier');

        switch($serviceType)
        {
            case self::SERVICE_UE_12:
                return CourierUAdditionList::UNIQID_DHLEXPRESS_UE_12;
                break;
            case self::SERVICE_UE_9:
                return CourierUAdditionList::UNIQID_DHLEXPRESS_UE_9;
                break;
            case self::SERVICE_NON_UE_12:
                return CourierUAdditionList::UNIQID_DHLEXPRESS_NON_UE_12;
                break;
            case self::SERVICE_NON_UE_9:
                return CourierUAdditionList::UNIQID_DHLEXPRESS_NON_UE_9;
                break;
            case self::SERVICE_USA_1030:
                return CourierUAdditionList::UNIQID_DHLEXPRESS_USA_1030;
                break;
            case self::SERVICE_BREAK_BULK:
                return CourierUAdditionList::UNIQID_DHLEXPRESS_BREAK_BULK;
                break;
            case self::SERVICE_XPD:
                return CourierUAdditionList::UNIQID_DHLEXPRESS_XPD;
                break;
            case self::SERVICE_UE_ECONOMY;
                return CourierUAdditionList::UNIQID_DHLEXPRESS_UE_ECONOMY;
                break;
            case self::SERVICE_NON_UE_ECONOMY:
                return CourierUAdditionList::UNIQID_DHLEXPRESS_NON_UE_ECONOMY;
                break;
            default:
                return false;
                break;
        }
    }

    protected function setCustomer($service_id)
    {
        switch($service_id)
        {
            case self::CUSTOMER_PN:
                $this->_username = self::USERNAME;
                $this->_account_no = self::ACCOUNT_NO;
                $this->_password = self::PASSWORD;
                $this->_selectedDhlCustomer = self::CUSTOMER_PN;
                break;
            case self::CUSTOMER_FB:
                $this->_username = self::USERNAME_FB;
                $this->_account_no = self::ACCOUNT_NO_FB;
                $this->_password = self::PASSWORD_FB;
                $this->_selectedDhlCustomer = self::CUSTOMER_FB;
                break;
            case self::CUSTOMER_SP:
                $this->_username = self::USERNAME_SP;
                $this->_account_no = self::ACCOUNT_NO_SP;
                $this->_password = self::PASSWORD_SP;
                $this->_selectedDhlCustomer = self::CUSTOMER_SP;
                break;
            case self::CUSTOMER_ROMAD_EX:
                $this->_username = self::USERNAME_ROMAD_EX;
                $this->_account_no = self::ACCOUNT_NO_ROMAD_EX;
                $this->_password = self::PASSWORD_ROMAD_EX;
                $this->_selectedDhlCustomer = self::CUSTOMER_ROMAD_EX;
                break;
            case self::CUSTOMER_ROMAD_IM:
                $this->_username = self::USERNAME_ROMAD_IM;
                $this->_account_no = self::ACCOUNT_NO_ROMAD_IM;
                $this->_password = self::PASSWORD_ROMAD_IM;
                $this->_selectedDhlCustomer = self::CUSTOMER_ROMAD_IM;
                break;
            default:
                throw new Exception('Unknown DHL Express SERVICE!');
        }
    }


    public static function getRateRequest(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $operator_uniq_id, $withCollect = false)
    {
        $model = new self;
        $model->_fillDataForCourierInternal($courierInternal, $from, $to, $operator_uniq_id, $withCollect);


        $data = new stdClass();
        $data->RequestedShipment = new stdClass();
        $data->RequestedShipment->DropOffType = $withCollect ? 'REQUEST_COURIER' : 'REGULAR_PICKUP';
        $data->RequestedShipment->NextBusinessDay = 'N';

        $data->RequestedShipment->Ship = new stdClass();
        $data->RequestedShipment->Ship->Shipper = new stdClass();
        $data->RequestedShipment->Ship->Shipper->StreetLines = self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,0);
        if(self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,1) != '')
            $data->RequestedShipment->Ship->Shipper->StreetLines2 = self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,1);
        if(self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,2) != '')
            $data->RequestedShipment->Ship->Shipper->StreetLines3 = self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,2);
        $data->RequestedShipment->Ship->Shipper->City = $model->sender_city;
        $data->RequestedShipment->Ship->Shipper->PostalCode = $model->sender_zip_code;
        $data->RequestedShipment->Ship->Shipper->CountryCode = $model->sender_country;
//        $data->RequestedShipment->Ship->Shipper->StateOrProvinceCode = $model->sender_country == 'US' ? UsaZipState::getStateByZip($model->sender_zip_code) : ''; // FOR USA

        $data->RequestedShipment->Ship->Recipient = new stdClass();
        $data->RequestedShipment->Ship->Recipient->StreetLines = self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,0);
        if(self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,1) != '')
            $data->RequestedShipment->Ship->Recipient->StreetLines2 = self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,1);
        if(self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,2) != '')
            $data->RequestedShipment->Ship->Recipient->StreetLines3 = self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,2);
        $data->RequestedShipment->Ship->Recipient->City = $model->receiver_city;
        $data->RequestedShipment->Ship->Recipient->PostalCode = $model->receiver_zip_code;
        $data->RequestedShipment->Ship->Recipient->CountryCode = $model->receiver_country;
//        $data->RequestedShipment->Ship->Recipient->StateOrProvinceCode = $model->receiver_country == 'US' ? UsaZipState::getStateByZip($model->receiver_zip_code) : ''; // FOR USA

        $data->RequestedShipment->Packages = new \SoapVar('<Packages>
<RequestedPackages number="1">
<Weight><Value>'.$model->package_weight.'</Value></Weight>
<Dimensions>
<Length>'.$model->package_size_l.'</Length>
<Width>'.$model->package_size_w.'</Width>
<Height>'.$model->package_size_d.'</Height>
</Dimensions>
</RequestedPackages>
</Packages>', XSD_ANYXML);

        $data->RequestedShipment->ShipTimestamp = self::getShipmentDate();
//        $data->RequestedShipment->ShipTimestamp = '2017-03-10T00:01:00GMT+00:00';
        $data->RequestedShipment->UnitOfMeasurement = 'SI';
        $data->RequestedShipment->Content = 'NON_DOCUMENTS';
        $data->RequestedShipment->PaymentInfo = 'DDU';
        $data->RequestedShipment->Account = $model->_account_no;

        $resp = $model->_soapOperation('getRateRequest', $data);

        $return = [];
        if ($resp->success == true) {

            $services = $resp->data->Provider->Service;

            if(!is_array($services))
                $services = [ $services ];

            $return = [];
            foreach($services AS $item)
            {
                if(self::serviceTypeToCourierAdditionId($item->type))
                    $return[] = self::serviceTypeToCourierAdditionId($item->type);
            }

        }

        return $return;
    }

    public static function getRateRequestCourierTypeU(CourierTypeU $courierTypeU, AddressData $from, AddressData $to, $operator_uniq_id, $withCollect = false)
    {
        $model = new self;
        $model->_fillDataForCourierU($courierTypeU, $from, $to, $operator_uniq_id, $withCollect);


        $data = new stdClass();
        $data->RequestedShipment = new stdClass();
        $data->RequestedShipment->DropOffType = $withCollect ? 'REQUEST_COURIER' : 'REGULAR_PICKUP';
        $data->RequestedShipment->NextBusinessDay = 'N';

        $data->RequestedShipment->Ship = new stdClass();
        $data->RequestedShipment->Ship->Shipper = new stdClass();
        $data->RequestedShipment->Ship->Shipper->StreetLines = self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,0);
        if(self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,1) != '')
            $data->RequestedShipment->Ship->Shipper->StreetLines2 = self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,1);
        if(self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,2) != '')
            $data->RequestedShipment->Ship->Shipper->StreetLines3 = self::addressLinesConverter($model->sender_address_line_1,$model->sender_address_line_2,2);
        $data->RequestedShipment->Ship->Shipper->City = $model->sender_city;
        $data->RequestedShipment->Ship->Shipper->PostalCode = $model->sender_zip_code;
        $data->RequestedShipment->Ship->Shipper->CountryCode = $model->sender_country;
//        $data->RequestedShipment->Ship->Shipper->StateOrProvinceCode = $model->sender_country == 'US' ? UsaZipState::getStateByZip($model->sender_zip_code) : ''; // FOR USA

        $data->RequestedShipment->Ship->Recipient = new stdClass();
        $data->RequestedShipment->Ship->Recipient->StreetLines = self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,0);
        if(self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,1) != '')
            $data->RequestedShipment->Ship->Recipient->StreetLines2 = self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,1);
        if(self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,2) != '')
            $data->RequestedShipment->Ship->Recipient->StreetLines3 = self::addressLinesConverter($model->receiver_address_line_1,$model->receiver_address_line_2,2);
        $data->RequestedShipment->Ship->Recipient->City = $model->receiver_city;
        $data->RequestedShipment->Ship->Recipient->PostalCode = $model->receiver_zip_code;
        $data->RequestedShipment->Ship->Recipient->CountryCode = $model->receiver_country;
//        $data->RequestedShipment->Ship->Recipient->StateOrProvinceCode = $model->receiver_country == 'US' ? UsaZipState::getStateByZip($model->receiver_zip_code) : ''; // FOR USA

        $data->RequestedShipment->Packages = new \SoapVar('<Packages>
<RequestedPackages number="1">
<Weight><Value>'.$model->package_weight.'</Value></Weight>
<Dimensions>
<Length>'.$model->package_size_l.'</Length>
<Width>'.$model->package_size_w.'</Width>
<Height>'.$model->package_size_d.'</Height>
</Dimensions>
</RequestedPackages>
</Packages>', XSD_ANYXML);

        $data->RequestedShipment->ShipTimestamp = self::getShipmentDate();
//        $data->RequestedShipment->ShipTimestamp = '2017-03-10T00:01:00GMT+00:00';
        $data->RequestedShipment->UnitOfMeasurement = 'SI';
        $data->RequestedShipment->Content = 'NON_DOCUMENTS';
        $data->RequestedShipment->PaymentInfo = 'DDU';
        $data->RequestedShipment->Account = $model->_account_no;

        $resp = $model->_soapOperation('getRateRequest', $data);

        $return = [];
        if ($resp->success == true) {

            $services = $resp->data->Provider->Service;

            if(!is_array($services))
                $services = [ $services ];

            $return = [];
            foreach($services AS $item)
            {
                if(self::serviceTypeToCourierUAdditionId($item->type))
                    $return[] = self::serviceTypeToCourierUAdditionId($item->type);
            }

        }

        return $return;
    }

    protected function _createShipment($returnError = false, $shiftDays = 0)
    {

//        if($this->_username == self::USERNAME_ROMAD_IM && $this->_user_id != User::INTEGRATIONS_TEST_USER_ID) {
//            if ($returnError == true) {
//                $return = [0 => array(
//                    'error' => 'Account not active!',
//                )];
//                return $return;
//            } else {
//                return false;
//            }
//        }

        $senderProvince = '';
        if($this->sender_country == 'US')
            $senderProvince = UsaZipState::getStateByZip($this->sender_zip_code);
        else if($this->sender_country == 'CA')
            $senderProvince = CanadaZipState::getStateByZip($this->sender_zip_code);

        $receiverProvince = '';
        if($this->receiver_country == 'US')
            $receiverProvince = UsaZipState::getStateByZip($this->receiver_zip_code);
        else if($this->receiver_country == 'CA')
            $receiverProvince = CanadaZipState::getStateByZip($this->receiver_zip_code);


        $data = new stdClass();
        $data->RequestedShipment = new stdClass();


        $data->RequestedShipment->ShipmentInfo = new stdClass();
        $data->RequestedShipment->ShipmentInfo->DropOffType = $this->with_collect ? 'REQUEST_COURIER' : 'REGULAR_PICKUP';
        $data->RequestedShipment->ShipmentInfo->ServiceType = $this->service_type;
        $data->RequestedShipment->ShipmentInfo->Account = $this->_account_no;
        $data->RequestedShipment->ShipmentInfo->Currency = $this->value_currency;

        // @26.07.2017 - Special rule for 4Values and packages to Wyspy Kanaryjskie
        if($this->_user_id == User::USER_4VALUES && strtoupper($this->receiver_country) == 'IC')
            $data->RequestedShipment->ShipmentInfo->Currency = 'EUR';

        $data->RequestedShipment->ShipmentInfo->UnitOfMeasurement = 'SI';
        $data->RequestedShipment->ShipmentInfo->ShipmentIdentificationNumber = '';
        $data->RequestedShipment->ShipmentInfo->LabelTemplate = 'ECOM26_64_001';

        $data->RequestedShipment->Ship = new stdClass();
        $data->RequestedShipment->Ship->Shipper = new stdClass();
        $data->RequestedShipment->Ship->Shipper->Address = new stdClass();
        $data->RequestedShipment->Ship->Shipper->Address->StreetLines = self::addressLinesConverter($this->sender_address_line_1,$this->sender_address_line_2,0);
        if(self::addressLinesConverter($this->sender_address_line_1,$this->sender_address_line_2,1) != '')
            $data->RequestedShipment->Ship->Shipper->Address->StreetLines2 = self::addressLinesConverter($this->sender_address_line_1,$this->sender_address_line_2,1);
        if(self::addressLinesConverter($this->sender_address_line_1,$this->sender_address_line_2,2) != '')
            $data->RequestedShipment->Ship->Shipper->Address->StreetLines3 = self::addressLinesConverter($this->sender_address_line_1,$this->sender_address_line_2,2);
        $data->RequestedShipment->Ship->Shipper->Address->City = $this->sender_city;
        $data->RequestedShipment->Ship->Shipper->Address->StateOrProvinceCode = $senderProvince; // FOR USA AND CANADA
        $data->RequestedShipment->Ship->Shipper->Address->PostalCode = $this->sender_zip_code == 'IE' ? '' : $this->sender_zip_code; // there is no postal code in Irleand
        $data->RequestedShipment->Ship->Shipper->Address->CountryCode = $this->sender_country;
        $data->RequestedShipment->Ship->Shipper->Contact = new stdClass();
        $data->RequestedShipment->Ship->Shipper->Contact->PersonName = $this->sender_name != '' ? $this->sender_name : $this->sender_company;
        $data->RequestedShipment->Ship->Shipper->Contact->CompanyName = $this->sender_company != '' ? $this->sender_company : $this->sender_name;
        $data->RequestedShipment->Ship->Shipper->Contact->PhoneNumber = $this->sender_tel;
        $data->RequestedShipment->Ship->Shipper->Contact->EmailAddress = $this->sender_email;

        $data->RequestedShipment->Ship->Recipient = new stdClass();
        $data->RequestedShipment->Ship->Recipient->Address = new stdClass();
        $data->RequestedShipment->Ship->Recipient->Address->StreetLines = self::addressLinesConverter($this->receiver_address_line_1,$this->receiver_address_line_2,0);
        if(self::addressLinesConverter($this->receiver_address_line_1,$this->receiver_address_line_2,1) != '')
            $data->RequestedShipment->Ship->Recipient->Address->StreetLines2 = self::addressLinesConverter($this->receiver_address_line_1,$this->receiver_address_line_2,1);
        if(self::addressLinesConverter($this->receiver_address_line_1,$this->receiver_address_line_2,2) != '')
            $data->RequestedShipment->Ship->Recipient->Address->StreetLines3 = self::addressLinesConverter($this->receiver_address_line_1,$this->receiver_address_line_2,2);
        $data->RequestedShipment->Ship->Recipient->Address->City = $this->receiver_city;
        $data->RequestedShipment->Ship->Recipient->Address->StateOrProvinceCode = $receiverProvince; // FOR USA and CANADA
        $data->RequestedShipment->Ship->Recipient->Address->PostalCode = $this->receiver_country == 'IE' ? '' : $this->receiver_zip_code; // there is no postal code in Irleand
        $data->RequestedShipment->Ship->Recipient->Address->CountryCode = $this->receiver_country;
        $data->RequestedShipment->Ship->Recipient->Contact = new stdClass();

        $data->RequestedShipment->Ship->Recipient->Contact->PersonName = $this->receiver_name != '' ? $this->receiver_name : $this->receiver_company;
        $data->RequestedShipment->Ship->Recipient->Contact->CompanyName = $this->receiver_company != '' ? $this->receiver_company : $this->receiver_name;
        $data->RequestedShipment->Ship->Recipient->Contact->PhoneNumber = $this->receiver_tel;
        $data->RequestedShipment->Ship->Recipient->Contact->EmailAddress = $this->receiver_email;

        $xml = '<Packages>';

        for ($i = 1; $i <= $this->packages_number; $i++)
            $xml .= '<RequestedPackages number="' . $i . '">
<Weight>' . $this->package_weight . '</Weight>
<Dimensions>
<Length>' . $this->package_size_l . '</Length>
<Width>' . $this->package_size_w . '</Width>
<Height>' . $this->package_size_d . '</Height>
</Dimensions>
<CustomerReferences>' . $this->reference . '</CustomerReferences>
</RequestedPackages>';

        $xml .= '</Packages>';

        $data->RequestedShipment->Packages = new \SoapVar($xml, XSD_ANYXML);



        $data->RequestedShipment->ShipTimestamp = self::getShipmentDate($shiftDays);

        $data->RequestedShipment->PaymentInfo = 'DDU';
        $data->RequestedShipment->Account = $this->_account_no;

        $data->RequestedShipment->InternationalDetail = new stdClass();
        $data->RequestedShipment->InternationalDetail->Commodities = new stdClass();
        $data->RequestedShipment->InternationalDetail->Commodities->NumberOfPieces = 1;
        $data->RequestedShipment->InternationalDetail->Commodities->Description =  mb_substr($this->package_content, 0,35);
        $data->RequestedShipment->InternationalDetail->Commodities->CountryOfManufacture = $this->sender_country;
        $data->RequestedShipment->InternationalDetail->Commodities->Quantity = $this->packages_number;
        $data->RequestedShipment->InternationalDetail->Commodities->UnitPrice = $this->package_value;
        $data->RequestedShipment->InternationalDetail->Commodities->CustomsValue = $this->package_value * $this->packages_number;
        $data->RequestedShipment->InternationalDetail->Content = $this->_isUe ? 'DOCUMENTS' : 'NON_DOCUMENTS';

//$data->RequestedShipment->Billing = new stdClass();
//$data->RequestedShipment->Billing->ShipperAccountNumber = ACCOUNT;
//$data->RequestedShipment->Billing->ShippingPaymentType = 'S';
//$data->RequestedShipment->Billing->BillingAccountNumber = ACCOUNT;


        $hasSpecialServices = false;

        $xml = '<SpecialServices>';
        // dangerous goods
        if($this->_special_service_id8000) {
            $hasSpecialServices = true;

            $xml .= '<Service>
            <ServiceType>HK</ServiceType>
            </Service>';

        }
        $xml .= '</SpecialServices>';

        if($hasSpecialServices)
            $data->RequestedShipment->ShipmentInfo->SpecialServices = new \SoapVar($xml, XSD_ANYXML);

        MyDump::dump('dhlexpress.txt', print_r($data, 1));

        $resp = $this->_soapOperation('createShipmentRequest', $data);

        MyDump::dump('dhlexpress-resp.txt', print_r($resp, 1));

        if ($resp->success == true) {
            $return = [];


            $packages = $resp->data->PackagesResult->PackageResult;

            $track_id_awbnumber = $resp->data->ShipmentIdentificationNumber;

            if (!is_array($packages))
                $packages = [$packages];

            $labelsPdf = $resp->data->LabelImage->GraphicImage;
            // inquire label images from pdf
            $labelsPng = [];

            $uniqId = uniqid();
            $basePath = Yii::app()->basePath . '/../';
            $dir = 'uplabels/';
            $temp = $dir . 'temp/';

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            @file_put_contents($basePath . $temp . $uniqId . '.pdf', $labelsPdf);


            for ($i = 0; $i < $this->packages_number + 1; $i++) {
                try {
                    $im = ImagickMine::newInstance();
                    $im->setResolution(300, 300);
                    $im->readImage($basePath . $temp . $uniqId . '.pdf[' . $i . ']');
                    $im->trimImage(0);
                    $im->setImageFormat('png8');
                    if ($im->getImageAlphaChannel()) {
                        $im->setImageBackgroundColor('white');
                        $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                    }

                    if($this->label_annotation)
                        LabelAnnotation::createAnnotation($this->label_annotation, $im, 0,159, true);

                    $im->stripImage();

                    $labelsPng[] = $im->getImageBlob();
                } catch (Exception $ex) {
                    // probaby no more labels - stop searching...
                    break;
                }
            }
            @unlink($basePath . $temp . $uniqId . '.pdf');

            $archiveLabel = false;
            // check if there is archive label - not every time there is...
            if(S_Useful::sizeof($labelsPng) > $this->packages_number)
                $archiveLabel = array_pop($labelsPng);

            $i = 0;
            if (is_array($packages))
                foreach ($packages AS $item) {

                    $temp = [];
                    $temp['status'] = true;
                    $temp['track_id'] = $track_id_awbnumber;

                    // only first package get label with archive label, but only if there is archive label...
                    if(!$i && $archiveLabel)
                    {
                        $temp['label'] = [
                            $labelsPng[0],
                            $archiveLabel
                        ];

                    } else {
                        $temp['label'] = [
                            $labelsPng[$i]
                        ];
                    }
                    $return[] = $temp;

                    $i++;
                }


            return $return;
        } else {

            if(preg_match('/Please try again another time/i', $resp->errorDesc) && $shiftDays < 3)
            {
                $shiftDays++;
                return self::_createShipment($returnError, $shiftDays);
            }

            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $resp->errorDesc,
                )];
                return $return;
            } else {
                return false;
            }
        }

    }

    protected static function getShipmentDate($additionalDays = 0)
    {
        $date = S_Useful::workDaysNextDate(date('Y-m-d'), (1 + $additionalDays));
        $date = $date.'T08:00:00 GMT+02:00';

        return $date;
    }

    protected function _soapOperation($method, $data, $customUrl = false)
    {

        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context' => stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))),
            'classmap' => [
            ]
        );

        $wsse_header = new WsseAuthHeaderDhlExpress($this->_username, $this->_password);

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $url = $customUrl ? $customUrl : self::URL;

        try {
            $client = new SoapClient($url, $mode);
            $client->__setSoapHeaders([$wsse_header]);

            $resp = $client->__soapCall($method, [$method => $data]);
        } catch (SoapFault $E) {

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            return $return;

        } catch (Exception $E) {

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            return $return;
        }

        if($method != 'trackShipmentRequest') {
            MyDump::dump('dhlexpress.txt', print_r($client->__getLastRequest(), 1));
            MyDump::dump('dhlexpress.txt', print_r($client->__getLastResponse(), 1));
        }


        if($customUrl)
            return $resp;

        if ($resp->Notification->code == 0 AND !is_array($resp->Notification)) {
            $return->success = true;
            $return->data = $resp;
        } else {
            $notification = $resp->Notification;
            if (!is_array($notification))
                $notification = [$notification];

            $error = '';

            foreach ($notification AS $item)
                $error .= $item->code . '] : ' . $item->Message . '|| ';


            $return->errorDesc = $error;
        }

        return $return;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $operator_uniq_id, $returnError = false, $withCollect = false)
    {
        $model = new self;
        $model->_user_id = $courierInternal->courier->user_id;


        $model->_fillDataForCourierInternal($courierInternal, $from, $to, $operator_uniq_id, $withCollect);

        if($model->cod_value > 0) {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => 'This operator do not support COD!'
                ]];
                return $return;
            } else {
                return false;
            }
        }

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {
        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;


        $model = new self;

        $model->_user_id = $courierTypeU->courier->user_id;

        $model->_fillDataForCourierU($courierTypeU, $from, $to);

        if($model->cod_value > 0) {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => 'This operator do not support COD!'
                ]];
                return $return;
            } else {
                return false;
            }
        }

        return $model->_createShipment($returnError);
    }

    protected function _fillDataForCourierU(CourierTypeU $courierTypeU, AddressData $from, AddressData $to, $withCollect = true)
    {

        $this->label_annotation = $courierTypeU->courierUOperator->label_symbol;


        // verify collection
        if ($courierTypeU->collection) {
            // Do not call for this package if is not first in group. Collection will be ordered for parent package
            if ($withCollect && $courierTypeU->parent_courier_id != NULL)
                $withCollect = false;
        }

        $this->with_collect = $withCollect;

        $this->sender_name = $from->name;
        $this->sender_company = $from->company;
        $this->sender_zip_code = $from->zip_code;
        $this->sender_city = $from->city;
        $this->sender_address_line_1 = $from->address_line_1;
        $this->sender_address_line_2 = $from->address_line_2;
        $this->sender_tel = $from->tel;
        $this->sender_country = strtoupper($from->country0->code);
        $this->sender_email = $from->fetchEmailAddress(true);
        $this->_sender_country_model = $from->country0;

        $this->receiver_name = $to->name;
        $this->receiver_company = $to->company;
        $this->receiver_zip_code = $to->zip_code;
        $this->receiver_city = $to->city;
        $this->receiver_address_line_1 = $to->address_line_1;
        $this->receiver_address_line_2 = $to->address_line_2;
        $this->receiver_tel = $to->tel;
        $this->receiver_country = strtoupper($to->country0->code);
        $this->receiver_email = $to->fetchEmailAddress(true);
        $this->_receiver_country_model = $to->country0;

        $this->package_weight = $courierTypeU->courier->package_weight;
        $this->packages_number = $courierTypeU->courier->packages_number;


        $this->package_size_l = $courierTypeU->courier->package_size_l;
        $this->package_size_d = $courierTypeU->courier->package_size_d;
        $this->package_size_w = $courierTypeU->courier->package_size_w;

//        $this->package_value = $courierTypeU->courier->getPackageValueConverted('PLN');
        $this->package_value = $courierTypeU->courier->package_value;
        $this->value_currency = $courierTypeU->courier->value_currency;
        $this->reference = 'ŚP #' . $courierTypeU->courier->local_id;


        // SPECIAL VALUE FOR 4VALUES - 22.03.2017
        if($courierTypeU->courier->user_id == User::USER_4VALUES)
            if($courierTypeU->courier->ref != '')
                $this->reference = mb_substr($courierTypeU->courier->ref,2);

        $this->package_content = $courierTypeU->courier->package_content;


        if ($courierTypeU->courier->cod && $courierTypeU->courier->cod_value > 0) {
            $this->cod_value = $courierTypeU->courier->cod_value;
        }

        $ueList = CountryList::getUeList(true, true);

        if(in_array($this->_sender_country_model->id, $ueList) && in_array($this->_receiver_country_model->id, $ueList)) {
            $this->service_type = self::SERVICE_STD_UE;
            $this->_isUe = true;
        } else {
            $this->service_type = self::SERVICE_STD_NON_UE;
            $this->_isUe = false;
        }

        if($courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHLEXPRESS)
            $this->setCustomer(self::CUSTOMER_PN);
        else if($courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHLEXPRESS_FB)
            $this->setCustomer(self::CUSTOMER_FB);
        else if($courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHLEXPRESS_SP)
            $this->setCustomer(self::CUSTOMER_SP);
        else if(in_array($courierTypeU->courierUOperator->uniq_id, [
            GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_STD,
            GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_EX,
            GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_STD,
            GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_EX,
        ]))
        {

            if(in_array($courierTypeU->courierUOperator->uniq_id, [
                    GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_STD,
                    GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_EX,
                ]))
                $this->setCustomer(self::CUSTOMER_ROMAD_IM);
            else if(in_array($courierTypeU->courierUOperator->uniq_id, [
                    GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_STD,
                    GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_EX,
                ] ))
                $this->setCustomer(self::CUSTOMER_ROMAD_EX);



            if (in_array($this->_sender_country_model->id, $ueList) && in_array($this->_receiver_country_model->id, $ueList))
                $this->_isUe = true;
            else
                $this->_isUe = false;


            if(in_array($courierTypeU->courierUOperator->uniq_id, [
                    GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_STD,
                    GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_STD,
                ]))
            {

                if ($this->_isUe)
                    $this->service_type = self::SERVICE_UE_ECONOMY;
                else
                    $this->service_type = self::SERVICE_NON_UE_ECONOMY;

            }
            else if(in_array($courierTypeU->courierUOperator->uniq_id, [
                    GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_EX,
                    GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_EX,
                ])) {

                if ($this->_isUe)
                    $this->service_type = self::SERVICE_STD_UE;

                else
                    $this->service_type = self::SERVICE_STD_NON_UE;
            }
        }
        else
            throw new Exception('Unknown DHL EXPRESS Operator!');

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_UE_12))
            $this->service_type = self::SERVICE_UE_12;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_UE_12))
            $this->service_type = self::SERVICE_UE_9;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_NON_UE_12))
            $this->service_type = self::SERVICE_NON_UE_12;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_NON_UE_9))
            $this->service_type = self::SERVICE_NON_UE_9;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_USA_1030))
            $this->service_type = self::SERVICE_USA_1030;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_BREAK_BULK))
            $this->service_type = self::SERVICE_BREAK_BULK;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_XPD))
            $this->service_type = self::SERVICE_XPD;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_UE_ECONOMY))
            $this->service_type = self::SERVICE_UE_ECONOMY;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_NON_UE_ECONOMY))
            $this->service_type = self::SERVICE_NON_UE_ECONOMY;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHLEXPRESS_ID8000))
            $this->_special_service_id8000 = true;
    }

    protected function _fillDataForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $operator_uniq_id, $withCollect = false)
    {

        $this->label_annotation = CourierOperator::getLabelAnnotationByUniqId($operator_uniq_id);



        // verify collection
        if ($withCollect) {
            // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
            if ($withCollect && $courierInternal->parent_courier_id != NULL)
                $withCollect = false;

            // For Type Internal do not call if without pickup option
            // PICKUP_MOD / 14.09.2016
//            if($withCollect && $courier != NULL && !$courier->with_pickup)
            if ($withCollect && $courierInternal != NULL && $courierInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR)
                $withCollect = false;

            // DO NOT CALL UPS COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
            if ($withCollect && $courierInternal->courier->user != NULL && $courierInternal->courier->user->getUpsCollectBlock())
                $withCollect = false;
        }

        $this->with_collect = $withCollect;

        $this->sender_name = $from->name;
        $this->sender_company = $from->company;
        $this->sender_zip_code = $from->zip_code;
        $this->sender_city = $from->city;
        $this->sender_address_line_1 = $from->address_line_1;
        $this->sender_address_line_2 = $from->address_line_2;
        $this->sender_tel = $from->tel;
        $this->sender_country = strtoupper($from->country0->code);
        $this->sender_email = $from->fetchEmailAddress(true);
        $this->_sender_country_model = $from->country0;

        // SPECIAL RULE FOR FIREBUSINESS ACCOUNT - 29.03.2017
        if($this->_selectedDhlCustomer == self::CUSTOMER_FB)
        {
            $this->sender_name = 'FIREBUSINESS Sp. z o.o.';
            $this->sender_company = '';
            $this->sender_zip_code = '02-699';
            $this->sender_city = 'Warszawa';
            $this->sender_address_line_1 = 'Kłobucka 23a bud. B lok. 324';
            $this->sender_address_line_2 = '';
            $this->sender_tel = '518114583';
            $this->sender_country = 'PL';
            $this->sender_email = 'info@fire-business.com.pl';
            $this->_sender_country_model = CountryList::model()->findByPk(CountryList::COUNTRY_PL);
        }

        // SPECIAL RULE FOR 4VALUES - 22.03.2017
        if($courierInternal->courier->user_id == User::USER_4VALUES && $from->country_id == CountryList::COUNTRY_PL)
        {
            $this->sender_name = 'IZETTLE/4VALUES';
            $this->sender_company = '';
            $this->sender_zip_code = '05-800';
            $this->sender_city = 'Pruszków';
            $this->sender_address_line_1 = 'ul. Parzniewska 4';
            $this->sender_address_line_2 = '';
            $this->sender_tel = '+48504660711';
            $this->sender_country = 'PL';
            $this->sender_email = 'pawel@4values.pl';
            $this->_sender_country_model = CountryList::model()->findByPk(CountryList::COUNTRY_PL);
        }


        $this->receiver_name = $to->name;
        $this->receiver_company = $to->company;
        $this->receiver_zip_code = $to->zip_code;
        $this->receiver_city = $to->city;
        $this->receiver_address_line_1 = $to->address_line_1;
        $this->receiver_address_line_2 = $to->address_line_2;
        $this->receiver_tel = $to->tel;
        $this->receiver_country = strtoupper($to->country0->code);
        $this->receiver_email = $to->fetchEmailAddress(true);
        $this->_receiver_country_model = $to->country0;

        $this->package_weight = $courierInternal->courier->getWeight(true);
        $this->packages_number = $courierInternal->courier->packages_number;


        $this->package_size_l = $courierInternal->courier->package_size_l;
        $this->package_size_d = $courierInternal->courier->package_size_d;
        $this->package_size_w = $courierInternal->courier->package_size_w;

//        $this->package_value = $courierInternal->courier->getPackageValueConverted('PLN');
        $this->package_value = $courierInternal->courier->package_value;
        $this->value_currency = $courierInternal->courier->value_currency;
        $this->reference = 'ŚP #' . $courierInternal->courier->local_id;


        // SPECIAL VALUE FOR 4VALUES - 22.03.2017
        if($courierInternal->courier->user_id == User::USER_4VALUES)
            if($courierInternal->courier->ref != '')
                $this->reference = mb_substr($courierInternal->courier->ref,2);

        $this->package_content = $courierInternal->courier->package_content;


        if ($courierInternal->courier->cod && $courierInternal->courier->cod_value > 0) {
            $this->cod_value = $courierInternal->courier->cod_value;
        }

        $ueList = CountryList::getUeList(true, true);

        if(in_array($this->_sender_country_model->id, $ueList) && in_array($this->_receiver_country_model->id, $ueList)) {
            $this->service_type = self::SERVICE_STD_UE;
            $this->_isUe = true;
        } else {
            $this->service_type = self::SERVICE_STD_NON_UE;
            $this->_isUe = false;
        }


        if($operator_uniq_id == CourierOperator::UNIQID_DHLEXPRESS)
            $this->setCustomer(self::CUSTOMER_PN);
        else if($operator_uniq_id == CourierOperator::UNIQID_DHLEXPRESS_FB)
            $this->setCustomer(self::CUSTOMER_FB);
        else if($operator_uniq_id == CourierOperator::UNIQID_DHLEXPRESS_SP)
            $this->setCustomer(self::CUSTOMER_SP);
        else if(in_array($operator_uniq_id, [
            GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_STD,
            GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_EX,
            GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_STD,
            GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_EX,
        ]))
        {

            if(in_array($operator_uniq_id, [
                GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_STD,
                GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_EX,
            ]))
                $this->setCustomer(self::CUSTOMER_ROMAD_IM);
            else if(in_array($operator_uniq_id, [
                GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_STD,
                GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_EX,
            ] ))
                $this->setCustomer(self::CUSTOMER_ROMAD_EX);

            if (in_array($this->_sender_country_model->id, $ueList) && in_array($this->_receiver_country_model->id, $ueList))
                $this->_isUe = true;
            else
                $this->_isUe = false;

            if(in_array($operator_uniq_id, [
                GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_STD,
                GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_STD,
            ]))
            {

                if ($this->_isUe)
                    $this->service_type = self::SERVICE_UE_ECONOMY;
                else
                    $this->service_type = self::SERVICE_NON_UE_ECONOMY;

            }
            else if(in_array($operator_uniq_id, [
                GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_IM_EX,
                GLOBAL_BROKERS::OPERATOR_UNIQID_DHLEXPRESS_ROMAD_EX_EX,
            ])) {

                if ($this->_isUe)
                    $this->service_type = self::SERVICE_STD_UE;

                else
                    $this->service_type = self::SERVICE_STD_NON_UE;
            }
        }
        else
            throw new Exception('Unknown DHL EXPRESS Operator!');



        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_UE_12))
            $this->service_type = self::SERVICE_UE_12;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_UE_12))
            $this->service_type = self::SERVICE_UE_9;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_NONUE_12))
            $this->service_type = self::SERVICE_NON_UE_12;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_NONUE_9))
            $this->service_type = self::SERVICE_NON_UE_9;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_USA_1030))
            $this->service_type = self::SERVICE_USA_1030;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_BREAKBULK))
            $this->service_type = self::SERVICE_BREAK_BULK;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_XPD))
            $this->service_type = self::SERVICE_XPD;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_UE_ECONOMY))
            $this->service_type = self::SERVICE_UE_ECONOMY;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_NONUE_ECONOMY))
            $this->service_type = self::SERVICE_NON_UE_ECONOMY;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLEXPRESS_ID8000))
            $this->_special_service_id8000 = true;

    }

    public static function track($number)
    {

        $data = new stdClass();
        $data->trackingRequest = new stdClass();
        $data->trackingRequest->TrackingRequest = new stdClass();
        $data->trackingRequest->TrackingRequest->Request = new stdClass();
        $data->trackingRequest->TrackingRequest->Request->ServiceHeader = new stdClass();
        $data->trackingRequest->TrackingRequest->Request->ServiceHeader->MessageTime = date('Y-m-d').'T'.date('H:i:s').'.000-01:00';
        $data->trackingRequest->TrackingRequest->Request->ServiceHeader->MessageReference = self::generateRandomString(30);
        $data->trackingRequest->TrackingRequest->AWBNumber = new stdClass();
        $data->trackingRequest->TrackingRequest->AWBNumber->ArrayOfAWBNumberItem = $number;
        $data->trackingRequest->TrackingRequest->LevelOfDetails = 'ALL_CHECK_POINTS';
        $data->trackingRequest->TrackingRequest->EstimatedDeliveryDateEnabled = false;
        $data->trackingRequest->TrackingRequest->PiecesEnabled = 'B';

        $model = new self;
        $model->setCustomer(self::CUSTOMER_PN);

        $resp = $model->_soapOperation('trackShipmentRequest', $data, self::URL_TRACKING);



        $resp = $resp->trackingResponse->TrackingResponse;



        if($resp->AWBInfo->ArrayOfAWBInfoItem->Status->ActionStatus == 'Success')
        {

//            $events = $resp->AWBInfo->ArrayOfAWBInfoItem->ShipmentInfo->ShipmentEvent->ArrayOfShipmentEventItem;

            $events = $resp->AWBInfo->ArrayOfAWBInfoItem->Pieces->PieceInfo->ArrayOfPieceInfoItem->PieceEvent->ArrayOfPieceEventItem;

            if(!is_array($events))
                $events = [$events];

            $statuses = [];
            foreach ($events AS $item) {

                $signature = '';
                if((string)$item->ServiceEvent->EventCode == 'OK')
                    $signature = ' (signed for by: '.(string) $item->Signatory.')';

                $name = (string) $item->ServiceEvent->Description;
                $name = trim($name);

                if($name == '')
                    continue;

                $temp = array(
                    'name' => $name,
                    'date' => $item->Date.' '.$item->Time,
                    'location' => $item->ServiceArea->Description.$signature,
                );

                array_push($statuses, $temp);
            }

            return $statuses;
        }

        return false;
    }

    protected static function addressLinesConverter($addressLine1, $addressLine2, $getLineNo)
    {
        $address = trim($addressLine1).' '.trim($addressLine2);
        $address = trim($address);

        $newAddress = [];
        $newAddress[0] = mb_substr($address,0,35);
        $newAddress[1] = mb_substr($address,35,35);
        $newAddress[2] = mb_substr($address,70,35);

        return trim($newAddress[$getLineNo]);
    }

    /**
     * @param string $number
     * @return bool|string Returns false or PDF string with ePod
     */
    public static function epod($number)
    {
        $xml = '<shipmentDocumentRetrieveReq>
<MSG>
<Hdr Id="'.self::generateRandomString(10).'" Ver="1.038" Dtm="'.date('Y-m-d').'T'.date('H:i:s').'">
<Sndr AppNm="DCG" AppCd="DCG"/>
</Hdr>
<Bd>
<Shp Id="'.$number.'">
<ShpTr>
<!--Zero or more repetitions:-->
<SCDtl AccNo="000000000" CRlTyCd="TC"/>
</ShpTr>
<ShpInDoc>
<SDoc DocTyCd="POD"/>
</ShpInDoc>
</Shp>
<GenrcRq>
<GenrcRqCritr TyCd="IMG_CONTENT" Val="epod-detail-esig"/>
<GenrcRqCritr TyCd="IMG_FORMAT" Val="PDF"/>
<GenrcRqCritr TyCd="DOC_RND_REQ" Val="true"/>
<GenrcRqCritr TyCd="EXT_REQ" Val="true"/>
<GenrcRqCritr TyCd="DUPL_HANDL" Val="CORE_WB_NO"/>
<GenrcRqCritr TyCd="SORT_BY" Val="$INGEST_DATE,D"/>
<GenrcRqCritr TyCd="LANGUAGE" Val="en"/>
</GenrcRq>
</Bd>
</MSG>
</shipmentDocumentRetrieveReq>';

        $data = new \SoapVar($xml, XSD_ANYXML);

        $model = new self;
        $model->setCustomer(self::CUSTOMER_PN);

        $resp = $model->_soapOperation('shipmentDocumentRetrieve', $data, self::URL_EPOD);

        if(isset($resp->MSG->Bd->Shp->ShpInDoc->SDoc->Img))
        {
            return $resp->MSG->Bd->Shp->ShpInDoc->SDoc->Img->Img;
        } else
            return false;

    }

    protected static function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

class WsseAuthHeaderDhlExpress extends SoapHeader {

    private $wss_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    function __construct($user, $pass, $ns = null) {
        if ($ns) {
            $this->wss_ns = $ns;
        }

        $auth = new stdClass();
        $auth->Username = new SoapVar($user, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
        $auth->Password = new SoapVar($pass, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);

        $username_token = new stdClass();
        $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns);

        $security_sv = new SoapVar(
            new SoapVar($username_token, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns),
            SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'Security', $this->wss_ns);
        parent::__construct($this->wss_ns, 'Security', $security_sv, true);
    }
}