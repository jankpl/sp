<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'set-cod-price',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courierUOperator/manageCouriersSetCodPriceByGV', ['operator_id' => $operator_id]),
    'htmlOptions' => [
        'class' => 'confirm-me',
    ]
));
?>

<?php
$price = new Price();
?>
    <?php $this->widget('PriceForm', array(
        'form' => $form,
        'model' => $price,
        'formFieldPrefix' => '',
    ));?>

<br/>
<?php
echo TbHtml::submitButton('Set', array(
    'onClick' => 'js:
    $(".gv-manage-countries-ids").val($("#gv-manage-countries").selGridView("getAllSelection").toString());
   ',
    'name' => 'Ids', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>
<?php echo CHtml::hiddenField('SetCodPrice[ids]','', array('class' => 'gv-manage-countries-ids')); ?>
<?php
$this->endWidget();
?>
