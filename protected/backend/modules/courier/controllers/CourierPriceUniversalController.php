<?php

class CourierPriceUniversalController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }


    /* @parm int id Courier Group Id */
    /* @parm int user_group User Group Id */
    public function actionPrice($type, $id, $user_group = NULL)
    {

        $checkCountryGroup = CourierCountryGroup::model()->findByPk($id);
        if($checkCountryGroup == NULL)
            throw new CHttpException(404);

        if($user_group != NULL)
        {
            $checkUserGroup = UserGroup::model()->findByPk($user_group);
            if($checkUserGroup == NULL)
                throw new CHttpException(404);
        }

        $groupEditURLs = [];
        foreach(UserGroup::model()->findAll() AS $item)
        {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('*');
            $cmd->from('courier_price_universal');
            $cmd->where('courier_country_group_id = :group', array(':group' => $id));
            $cmd->andWhere('user_group_id = :user_group', array(':user_group' => $item->id));
            $cmd->andWhere('type = :type', array(':type' => $type));

            $result = $cmd->queryRow();

            $temp = [];
            $temp['name'] = $item->name.' ('.($result?'yes':'no').')';
            $temp['url'] = Yii::app()->createAbsoluteUrl('/courier/courierPriceUniversal/price', array('id' => $id, 'user_group' => $item->id, 'type' => $type));
            array_push($groupEditURLs, $temp);
        }

        $groupActionDelete = true;
        if($user_group == NULL)
        {
            $groupActionDelete = false;
            $groupActionListView = $this->renderPartial('_groupActionList', array(
                'groupEditUrls' => $groupEditURLs
            ),
                true);
        }

        /* @var $model CourierPriceUniversal*/
        $model = CourierPriceUniversal::loadOrCreatePrice($type, $id, $user_group);
//        if($model->id == NULL)
//            $groupActionDelete = null;


        $courierPriceUniversalValues = $model->loadValueItems();

        $this->performAjaxValidation($model, 'courier-price-form');

        if (isset($_POST['CourierPriceUniversal']) OR isset($_POST['add_more']) OR isset($_POST['sort'])) {


            // Clicked button for just sort
            if(isset($_POST['delete']))
            {
                if($model->deleteWithRelated())
                {
                    Yii::app()->user->setFlash('success','Pricing was deleted!');
                } else {
                    Yii::app()->user->setFlash('error','Pricing was  NOT deleted!');
                }
                $this->redirect(array('courierPriceUniversal/price','id' => $id, 'type' => $type));
                return;
            }

            $courierPriceUniversalValues = Array();

            $model->setAttributes($_POST['CourierPriceUniversal']);

            $transaction = Yii::app()->db->beginTransaction();
            $error = false;

            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('courier_price_universal_value', 'courier_price_universal_id = :courier_price_universal_id',
                array(':courier_price_universal_id' => $model->id));

            function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
                $sort_col = array();
                foreach ($arr as $key=> $row) {
                    $sort_col[$key] = $row[$col];
                }

                array_multisort($sort_col, $dir, $arr);
            }

            $dataArray = [];
            foreach($_POST['CourierPriceUniversalValue'] AS $key => $item)
                $dataArray[$key] = array_merge($_POST['CourierPriceUniversalValue'][$key], $_POST['PriceValue'][$key]);


//            array_sort_by_column($_POST['CourierPriceUniversalValue'], 'weight_top');
            array_sort_by_column($dataArray, 'weight_top');


            $model->rev++;
            $model->save();


            $maxWeightFound = false;
            foreach($dataArray AS $key => $item)
            {

                $courierPriceUniversalValue = new CourierPriceUniversalValue;
                $courierPriceUniversalValue->rev = $model->rev;
                $courierPriceUniversalValue->weight_top = $item['weight_top'];
                $courierPriceUniversalValue->price = Price::generateByPost($item['price']);

                $courierPriceUniversalValue->price_id = $courierPriceUniversalValue->price->saveWithPrices();

                $courierPriceUniversalValue->courier_price_universal_id = $model->id;

                $courierPriceUniversalValues[$key] = $courierPriceUniversalValue;

                if(!$courierPriceUniversalValues[$key]->save())
                    $error = true;

                if($courierPriceUniversalValues[$key]->weight_top == CourierPriceUniversal::MAX_WEIGHT)
                    $maxWeightFound = true;
            }

            // Clicked button for more prices
            if(isset($_POST['add_more']))
            {
                array_unshift($courierPriceUniversalValues, new CourierPriceUniversalValue);
                $error = true;
            }

            // Clicked button for just sort
            if(isset($_POST['sort']))
            {
                $error = true;
            }

            if(!$maxWeightFound)
            {
                $error = true;

                $maxPriceModel = $model->generateDefaultPriceValue();

                array_push($courierPriceUniversalValues, $maxPriceModel);
            }

            if(!$error AND $model->save())
            {
                $transaction->commit();
                $this->redirect(array('courierPriceUniversal/admin', 'type' => $type));
            }
            $transaction->rollback();
        }



        $this->render('update', array(
            'model' => $model,
            'courierPriceUniversalValues' =>  $courierPriceUniversalValues,
            'groupActionListView' => $groupActionListView,
            'groupActionDelete' => $groupActionDelete,
            'userGroup' => $user_group,
            'countryGroup' => $id,
        ));

    }



    public function actionView($id, $type) {

        $this->redirect(array('price', 'id' => $id, 'type' => $type));
    }


    public function actionAdmin($type) {
        $model = new _CourierCountryGroup_CourierPriceUniversal('search');
        $model->unsetAttributes();

        if (isset($_GET['_CourierCountryGroup_CourierPriceUniversal']))
            $model->setAttributes($_GET['_CourierCountryGroup_CourierPriceUniversal']);

        $this->render('admin', array(
            'model' => $model,
            'type' => $type,
        ));
    }


    const COUNTRY = 0;
    const WEIGHT_TO = 1; 
    const PRICE_PLN = 2;
    const PRICE_EUR = 3;
    const PRICE_GBP = 4;

    public function actionImport($type, $hash = false, $ugi = NULL)
    {
        if($hash == false)
        {

            $formModel = new F_RoutingImport;

            if(isset($_POST['F_RoutingImport'])) {
                $formModel->attributes = $_POST['F_RoutingImport'];
                $formModel->file = CUploadedFile::getInstance($formModel, 'file');

                if($formModel->validate())
                {
                    $fileData = $formModel->fileToArray($formModel->file->tempName);
                    $hash = hash('sha512', uniqid(time(),true));
                    Yii::app()->cacheUserData->set('CPUI_'.$hash, $fileData, 60*60);
                    Yii::app()->cacheUserData->set('CPUI_ORG_FILE_'.$hash, file_get_contents($formModel->file->tempName), 60*60);
                    Yii::app()->cacheUserData->set('CPUI_ORG_FILE_EXT_'.$hash, $formModel->file->extensionName, 60*60);

                    $this->redirect(['/courier/courierPriceUniversal/import', 'hash' => $hash, 'ugi' => $formModel->user_group_id, 'type' => $type]);
                    exit;
                }
            }

            if(isset(Yii::app()->session['CPI']))
            {
                if(isset(Yii::app()->session['CPUI_GET'])) {

                    $path = file_get_contents(Yii::app()->session['CPI']);
                    Yii::app()->session['CPI'] = false;
                    Yii::app()->session['CPUI_GET'] = false;
                    unset(Yii::app()->session['CPI']);
                    unset(Yii::app()->session['CPUI_GET']);
                    $filename = 'backup_courier_price_'.date('YmdHis').'.csv';
                    header("Content-Type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=\"".$filename."\";");
                    header("Content-Transfer-Encoding: binary");
                    exit($path);
                } else {
                    Yii::app()->session['CPUI_GET'] = true;
                }
            }

            $this->render('import_step1', [
                'model' => $formModel,
            ]);
            exit;



        } else {

            $errors = false;

            if($ugi == -1)
                $ugi = NULL;

            $user_group_id = $ugi;

            $backup = [];
            $file = Yii::app()->cacheUserData->get('CPUI_'.$hash);


            if(!$file OR !S_Useful::sizeof($file))
            {

                Yii::app()->user->setFlash('error', 'File looks empty...');
                $errors = true;
            }

            $data = [];
            if(is_array($file))
                foreach ($file AS $item) {

                    if (!S_Useful::sizeof($item))
                        continue;

                    foreach ($item AS $key => $temp) {
                        $temp = explode('_', $temp);
                        $item[$key] = $temp[0];
                    }

                    if (!isset($data[$item[self::COUNTRY]]))
                        $data[$item[self::COUNTRY]] = [];

                    if (!isset($data[$item[self::COUNTRY]][$item[self::WEIGHT_TO]]))
                        $data[$item[self::COUNTRY]][$item[self::WEIGHT_TO]] = [];

                    $data[$item[self::COUNTRY]][$item[self::WEIGHT_TO]] = [
                        'PLN' => $item[self::PRICE_PLN],                       
                        'EUR' => $item[self::PRICE_EUR],                        
                        'GBP' => $item[self::PRICE_GBP], 
                    ];
                }

            $countries = array_keys($data);
            $countriesModels = [];
            $countriesModelsOld = [];

            foreach ($countries AS $country_id) {

                $countryData = $data[$country_id];

                /* @var $model CourierCountryGroup */

                $model = CourierCountryGroup::model()->findByPk($country_id);


                $modelsOld = [];
                $modelsOld['M'] = CourierPriceUniversal::loadOrCreatePrice($type, $country_id, $user_group_id);
                if($modelsOld['M'])
                    $modelsOld['PV'] = $modelsOld['M']->loadValueItems();

             
                if($errors)
                {
                    $this->redirect(['/courier/courierPriceUniversal/import', 'hash' => false, 'type' => $type]);
                    exit;
                }

                $models = [];
                $models['M'] = CourierPriceUniversal::loadOrCreatePrice($type, $country_id, $user_group_id);
                if($models['M'])
                    $models['PV'] = [];

                // PICKUP:
                $courierPriceUniversalValues = [];
                if(isset($countryData))
                {
                    $maxWeightFound = false;
                    foreach($countryData AS $weight => $item)
                    {

                        $courierPriceUniversalValue = new CourierPriceUniversalValue();
                        $courierPriceUniversalValue->weight_top = $weight;

                        $price = [];
                        $price[Price::PLN] = ['value' => $item['PLN']];
                        $price[Price::EUR] = ['value' => $item['EUR']];
                        $price[Price::GBP] = ['value' => $item['GBP']];

                        $courierPriceUniversalValue->price = Price::generateByPost($price);

                        $courierPriceUniversalValue->courier_price_universal_id = $model->id;

                        $courierPriceUniversalValues[$weight] = $courierPriceUniversalValue;

                        if($courierPriceUniversalValues[$weight]->weight_top == CourierPriceUniversal::MAX_WEIGHT)
                            $maxWeightFound = true;
                    }

                    if(!$maxWeightFound)
                        $models['M']->addError('_maxWeightErrorContainer', 'Max weight ('.CourierPriceUniversal::MAX_WEIGHT.') not found!');

                    $models['PV'] = $courierPriceUniversalValues;
                }



                
//                $deleteList[$country_id] = [];
//

                    if(is_array($modelsOld['PV']))
                        foreach($modelsOld['PV'] AS $weight => $item)
                        {
                            /* @var $item CourierPriceValue */

                            if(!$item->weight_top)
                                continue;

                            $temp = [];
                            $temp[] = $country_id . '_' . $model->name;
                            $temp[] = $item->weight_top;
                            $temp[] = $item->price ? $item->price->getValue(Currency::PLN_ID)->value : '';
                            $temp[] = $item->price ? $item->price->getValue(Currency::EUR_ID)->value : '';
                            $temp[] = $item->price ? $item->price->getValue(Currency::GBP_ID)->value : '';


                            $backup[] = $temp;
                        }


                $countriesModels[$country_id] = $models;
                $countriesModelsOld[$country_id] = $modelsOld;
            }

        }


        if (isset($_POST['confirm-import'])) {
            if ($_POST['confirmation'] == 1) {

                $transaction = Yii::app()->db->beginTransaction();

                Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();

//                $USER_GROUP_NULL = '';
//                $USER_GROUP_RULE = [];
//                if($user_group_id === NULL)
//                    $USER_GROUP_NULL = 'user_group_id IS NULL';
//                else
//                    $USER_GROUP_RULE = ['user_group_id' => $user_group_id];

//                $cpl = [];

                foreach($countriesModels AS $country_id => $item)
                {
                    // CLEAR OLD
//                    $cdphg = CourierDeliveryPriceHasHub::model()->findAllByAttributes(['courier_country_group' => $country_id] + $USER_GROUP_RULE, $USER_GROUP_NULL);
//                    $cpphg = CourierPickupPriceHasGroup::model()->findAllByAttributes(['courier_country_group' => $country_id] + $USER_GROUP_RULE, $USER_GROUP_NULL);
//
//                    $pricesToDelete = [];
                    /* @var %temp CourierDeliveryPriceHasHub */
//                    foreach($cdphg AS $temp)
//                    {
//                        $cpl[] = $temp->courier_price_item;
//
//                        /* @var $cpi CourierPrice */
//                        $cpi = $temp->courierPriceUniversalItem;
//                        if($cpi)
//                            $cpi->loadValueItems();
//
//                        if($cpi)
//                            foreach($cpi->courierPriceUniversalValues AS $cpv)
//                            {
//                                if($cpv->pricePerWeight)
//                                    $cpv->pricePerWeight->deleteValues();
//                                if($cpv->pricePerItem)
//                                    $cpv->pricePerItem->deleteValues();
//
//                                $pricesToDelete[] = $cpv->pricePerWeight;
//                                $pricesToDelete[] = $cpv->pricePerItem;
//                            }
//
//                        $temp->delete();
//                    }



                    /* @var %temp CourierPickupPriceHasGroup */
//                    foreach($cpphg AS $temp)
//                    {
//
//                        $cpl[] = $temp->courier_price_item;
//
//                        $cpi = $temp->courierPriceUniversalItem;
//                        if($cpi)
//                            $cpi->loadValueItems();
//
//                        if($cpi)
//                            foreach($cpi->courierPriceUniversalValues AS $cpv)
//                            {
//                                if($cpv->pricePerWeight)
//                                    $cpv->pricePerWeight->deleteValues();
//                                if($cpv->pricePerItem)
//                                    $cpv->pricePerItem->deleteValues();
//
//                                $pricesToDelete[] = $cpv->pricePerWeight;
//                                $pricesToDelete[] = $cpv->pricePerItem;
//                            }
//
//                        $temp->delete();
//                    }

//                    foreach($pricesToDelete AS $ptd) {
//                        if($ptd)
//                            $ptd->delete();
//                    }
//
//                    $cmd = Yii::app()->db->createCommand();
//                    foreach($cpl AS $temp)
//                        $cmd->delete('courier_price_value', 'courier_price_id = :cpi', [':cpi' => $temp]);

//                    $temp = $item['M']->loadValueItems();
//                    if(is_array($temp))
//                        foreach($temp AS $temp2)
//                            $temp2->delete();

                    if($item)
                    {
                        if($item['M'])
                            $item['M']->rev++;

                        if(S_Useful::sizeof($item['PV'])) {

                            if(!$item['M']->save())
                                $errors = true;

                            foreach ($item['PV'] AS $key => $courierPriceUniversalValue) {
                                $courierPriceUniversalValue->price_id = $courierPriceUniversalValue->price->saveWithPrices();

                                $courierPriceUniversalValue->courier_price_universal_id = $item['M']->id;
                                $courierPriceUniversalValue->rev = $item['M']->rev;

//                            $courierPriceUniversalValues[$key] = $courierPriceUniversalValue;

                                if (!$courierPriceUniversalValue->save())
                                    $errors = true;
                            }
                        }
                    }

                }


                Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS=1')->execute();

                if ($errors) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', 'Correct errrors!');

                } else {
//                             save backup
                    $backupText = '';
                    $backup = [[
                            'country_group_id',
                            'weight_to',
                            'PLN',
                            'EUR',
                            'GBP',
                        ]] + $backup;
                    foreach ($backup AS $line)
                        $backupText .= implode(';', $line) . PHP_EOL;

                    $ugSufix = '';

                    if ($user_group_id)
                        $ugSufix = $user_group_id . '_';


                    $backupPath = 'courier_pricing_backup_' . $ugSufix . date('YmdHis') . '.csv';
                    file_put_contents($backupPath, $backupText);
                    $transaction->commit();
//

                    $orgFileData = Yii::app()->cacheUserData->get('CPUI_ORG_FILE_'.$hash);
                    $backupFileData = $backupText;

                    $time = date('Y-m-d-H-i-s');
                    $filenameOrg = 'NEW_import_data_'.$time.'.'.Yii::app()->cacheUserData->get('CPUI_ORG_FILE_EXT_'.$hash);
                    $filenameBackup = 'BACKUP_import_data_'.$time.'.csv';

//                    $to = [
//                        'jankopec@swiatprzesylek.pl',
//                        'piotrkocon@swiatprzesylek.pl',
//                        'maciejszostak@swiatprzesylek.pl',
//                        'michalwojciechowski@swiatprzesylek.pl',
//                    ];
//
//                    S_Mailer::send($to, $to, '[Changes in PRE-ROUTING pricing][FILE]', 'Files in attachment<br/></br>User_group_id : '.$user_group_id.'<br/><br/>Admin_id : '.Yii::app()->user->id, false, NULL, NULL,true, false, [$filenameOrg, $filenameBackup], false, false, false, [$orgFileData, $backupFileData]);

                    Yii::app()->user->setFlash('success', 'Changes have been saved! Download previous setting backup.');
                    Yii::app()->session['CPI'] = $backupPath;
                    Yii::app()->cacheUserData->set('CPUI_'.$hash, false);
                    $this->redirect(['/courier/courierPriceUniversal/import', 'hash' => false, 'type' => $type]);
                    Yii::app()->end();
                }

            } else
                Yii::app()->user->setFlash('error', 'Please know what you are doing :)');

        }


        $this->render('import', [
            'countriesModels' => $countriesModels,
            'countriesModelsOld' => $countriesModelsOld,
        ]);
    }


    public function actionExport($type)
    {

        $formModel = new F_CourierRoutingExport;

        if(isset($_POST['F_CourierRoutingExport'])) {
            $formModel->attributes = $_POST['F_CourierRoutingExport'];

            if($formModel->validate())
            {

                $country_list_id = implode(',', $formModel->country_list_id);
                $user_group_id = $formModel->user_group_id;

                if($user_group_id == -1)
                    $user_group_id = NULL;



                if($user_group_id === NULL)
                    $models = CourierCountryGroup::model()->findAll('parent_courier_country_group_id IS NULL AND user_group_id IS NULL AND id IN ('.$country_list_id.')');
                else
                    $models = CourierCountryGroup::model()->findAll('parent_courier_country_group_id IS NULL AND user_group_id = :ugi AND id IN ('.$country_list_id.')', [':ugi' => $user_group_id]);

                foreach ($models AS  $model) {

                    $country_id = $model->id;

                    $modelsOld = [];
                    $modelsOld['M'] = CourierPriceUniversal::loadOrCreatePrice($type, $country_id, $user_group_id);
                    if ($modelsOld['M'])
                        $modelsOld['PV'] = $modelsOld['M']->loadValueItems();


                        if (is_array($modelsOld['PV']))
                            foreach ($modelsOld['PV'] AS $weight => $item) {
                                /* @var $item CourierPriceValue */

                                if (!$item->weight_top)
                                    continue;

                                $temp = [];
                                $temp[] = $country_id . '_' . $model->name;    
                                $temp[] = $item->weight_top;
                                $temp[] = $item->price ? $item->price->getValue(Currency::PLN_ID)->value : '';
                                $temp[] = $item->price ? $item->price->getValue(Currency::EUR_ID)->value : '';
                                $temp[] = $item->price ? $item->price->getValue(Currency::GBP_ID)->value : '';


                                $backup[] = $temp;
                            }

                }

                $arrayHeader = ['country_group_id',
                    'weight_to',
                    'PLN',
                    'EUR',
                    'GBP'
                ];


                $writer = Box\Spout\Writer\WriterFactory::create(Box\Spout\Common\Type::XLSX); // for XLSX files
                $writer->openToBrowser('export.xls');
                $writer->addRow($arrayHeader);

                if(is_array($backup))
                    foreach($backup AS $key => $line)
                        $writer->addRow($line);

                $filename = 'export_courier_pricing_' . date('YmdHis') . '.xlsx';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0'); //no cache

                $writer->close();
                Yii::app()->end();
            }
        }

        $this->render('export', [
            'model' => $formModel,
        ]);
        exit;


    }


}