<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $order_values OrderValue */
/* @var $payment PaymentType */
/* @var $product S_Product */
/* @var $addressModel AddressData_Order */
/* @var $senderAddressModel AddressData */
/* @var $form TbActiveForm */
?>

<div class="form">

    <h2><?php echo Yii::t('order','Zamówienie');?> | <?php echo Yii::t('order','Finalizuj');?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'order-form',
        'enableAjaxValidation' => false,
        'stateful' => true,
    ));
    ?>

    <div class="row">
        <div class="col-xs-12 col-lg-4 col-lg-offset-4">

            <h3 class="text-center"><?php echo Yii::t('order','Zawartość');?></h3>


            <table class="list hTop" style="width: 100%; margin: 0 auto;">
                <tr>
                    <td style="width: 400px;"><?php echo Yii::t('order','Produkt');?></td>
                    <td>
                        <?php echo Yii::t('order','Cena (netto/brutto)');?>
                    </td>
                </tr>
                <?php foreach($products AS $product): ?>
                    <tr>
                        <td style="text-align: left;"><?php echo GxHtml::link(GxHtml::encode($product->name),$product->url); ?></td>
                        <td><?php echo $product->value_netto;?> <?php echo Yii::app()->PriceManager->getCurrencyCode();?> / <?php echo $product->value_brutto;?> <?php echo Yii::app()->PriceManager->getCurrencyCode();?></td>
                    </tr>
                <?php endforeach; ?>

            </table>
            <br/>

            <h3 class="text-center"><?php echo Yii::t('order','Wybierz rodzaj płatności');?></h3>

            <div class="text-center">
                <?php
                if($model->hasErrors('payment_type_id'))
                    echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, Yii::t('order', 'Wybierz metodę płatności!'), array('closeText' => false));

                ?>
            </div>

            <div class="text-center" style="margin: 10px auto;">
                <div class="btn-group btn-group-lg" role="group">
                    <?php
                    //$buttons = array();
                    /* @var $item PaymentType */
                    foreach($payment AS $item)
                    {
                        echo '<button type="button" class="btn '.($item->id == $model->payment_type_id ? 'btn-primary active' : 'btn-default').'" data-payment-id="'.$item->id.'">'.$item->getLocalizedName().'</button>';
                    }
                    ?>
                </div>
            </div>
            <div style="display: none;">
                <?php echo $form->radioButtonList($model, 'payment_type_id', GxHtml::listDataEx($payment), array('id'=>'payment')); ?>
            </div>


            <h3 class="text-center"><?php echo Yii::t('order','Podsumowanie');?></h3>


            <table class="list hLeft" style="width: 100%; margin: 0 auto;">
                <tr>
                    <td style="width: 200px;"><?php echo Yii::t('order', 'Rabat');?></td>
                    <td style="text-align: center;"><?php echo $model->calculateDiscount();?> <?php echo Yii::app()->PriceManager->getCurrencyCode();?></td>
                </tr>
                <tr>
                    <td><?php echo Yii::t('order', 'Kwota z rabatem');?></td>
                    <td style="text-align: center; font-size: 14px;">
                        <?php echo $model->calculateValueWithDiscount();?> <?php echo Yii::app()->PriceManager->getCurrencyCode();?> netto / <?php echo Yii::app()->PriceManager->getFinalPrice($model->calculateValueWithDiscount());?> <?php echo Yii::app()->PriceManager->getCurrencyCode();?> brutto
                    </td>
                </tr>
                <tr>
                    <td><?php echo Yii::t('order','Prowizja płatności');?></td>
                    <td style="text-align: center; font-size: 14px;"><span id="commission_amount">-</span>
                    </td>
                </tr>
                <tr>
                    <td><?php echo Yii::t('order','Do zapłaty');?></td>
                    <td style="text-align: center; font-size: 16px; font-weight: bold;"><span id="total_value">-</span></td>
                </tr>
            </table>

            <?= CHtml::hiddenField('dataInvCompany', $dataInvCompany); ?>

            <script>
                $(document).ready(function(){
                    $("input[name='Order[payment_type_id]']").hide();
                    $("[data-payment-id]").on('click', function(){
                        $('[data-payment-id]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
                        $(this).addClass('active').addClass('btn-primary').removeClass('btn-default');

                        var id = $(this).attr('data-payment-id');
                        var $radio = $("input[name='Order[payment_type_id]'][value=" + id + "]");
                        $radio.prop('checked', true);
                        $radio.trigger('click');
                    });

                    $("#submit-button").attr('disabled', true).addClass("disabled");
                    $("#online-options").hide();
                })
            </script>

            <?php

            Yii::app()->clientScript->registerCoreScript('jquery');
            echo Chtml::script('

            $("input[name=\'Order[payment_type_id]\']").click(function(){

                var payment_type_id = $(this).val();

                $.post("'.Yii::app()->createUrl('paymentType/ajaxGetPaymentCommissionValue').'",
                {\'payment_type_id\' : payment_type_id,\'order_hash\' : \''.$model->hash.'\'},
                function(data){

                    $("span#commission_amount").html(data);
                });

                $.post("'.Yii::app()->createUrl('order/ajaxGetTotalValue').'",
                {\'payment_type_id\' : payment_type_id,\'order_hash\' : \''.$model->hash.'\'},
                function(data){
                    $("span#total_value").html(data);
                    $("#submit-button").attr("disabled", false).removeClass("disabled");
                   
                   
                   if(payment_type_id == '.PaymentType::PAYMENT_TYPE_ONLINE.')
                        $("#online-options").show();
                    else
                      $("#online-options").hide();
                    
                });


            });
            
            $(document).ready(function(change){
            
                    $(\'.checkboxpicker\').checkboxpicker({
                        offClass : \'btn-default btn\',
                        onClass : \'btn-primary btn\',
                        defaultClass: \'btn\',
                    });
             
                  function setInvType(change)
                    {                                       
                        var $invType = $(\'[name="dataInvCompany"]\');
                        var $invTypeSwitch = $(\'[data-inv-type-switch]\');
                        var $companyFields = $(\'[data-inv-company-field]\');
                        var $personFields = $(\'[data-inv-person-field]\');

                        if((parseInt($invType.val()) && !change) || (!parseInt($invType.val()) && change)) {                    
                            $invTypeSwitch.html($invTypeSwitch.attr(\'data-inv-person-name\'));
                            $companyFields.show();
                            $personFields.hide();
                            $invType.val(1);            
                        } else {
                            $invTypeSwitch.html($invTypeSwitch.attr(\'data-inv-company-name\'));
                            $companyFields.hide();
                            $personFields.show();
                            $invType.val(0);    
                        }
                    }
             
             $(\'[data-inv-type-switch]\').on(\'click\', function(){
                setInvType(true);
             });
             
             setInvType(false);
             
             function toggleOtherData()
             {             
                var $otherData = $("#inv-data");
                
                if($("#fv_other_data_checkbox").is(":checked"))
                    $otherData.slideDown();
                else
                    $otherData.slideUp();             
             }
             
             $("#fv_other_data_checkbox").on("change", toggleOtherData);             
             toggleOtherData();
             
             $("[data-inv-copy-sender]").on("click", function(){
             
                 var $source = $("#fv_other_data_checkbox");             
                $("#AddressData_Order_name").val($source.attr("data-name"));
                $("#AddressData_Order_company").val($source.attr("data-company"));
                $("#AddressData_Order_address_line_1").val($source.attr("data-address_line_1"));
                $("#AddressData_Order_zip_code").val($source.attr("data-zip_code"));
                $("#AddressData_Order_city").val($source.attr("data-city"));
             
            
             });
             
        });
        ');

            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);
            ?>

            <script>
                $(document).ready(function(){
                    $("input[name=\'Order[payment_type_id]\']:checked").trigger('click');
                })
            </script>

            <div id="online-options" style="display: none;">
                <h3 class="text-center"><?= Yii::t('order', 'Opcje');?>:</h3>


                <table class="errors-not-inline" style="width: 100%; margin: 0 auto;  border-spacing: 5px; border-collapse: separate;">
                    <tr>
                        <td style="text-align: right;"> <?= $form->checkBox($model, '_inv_other_data', ['id' => 'fv_other_data_checkbox', 'class' => 'checkboxpicker', 'data-off-label' => Yii::t('order','Nie'),'data-on-label' =>  Yii::t('order','Tak'),
                                'data-name' => $senderAddressModel->name, 'data-company' => $senderAddressModel->company, 'data-address_line_1' => trim($senderAddressModel->address_line_1.' '.$senderAddressModel->address_line_2), 'data-zip_code' => $senderAddressModel->zip_code, 'data-city' => $senderAddressModel->city]);?></td>
                        <td style="text-align: left; font-weight: bold; padding-left: 5px;"><?php echo Yii::t('order', 'Dane do faktury inne niż dane konta');?>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center">
                            <?= $form->error($model,'_inv_other_data'); ?>
                        </td>
                    </tr>
                </table>

                <div id="inv-data" style="display: none;">
                    <br/>
                    <div class="text-left">
                        <div style="width: 33%; display: inline-block;">&nbsp;</div>
                        <div style="display: inline-block;">
                            <a href="#"  style="font-weight: bold; text-decoration: underline;" class="inv-type" data-inv-copy-sender="true"><?= Yii::t('order', 'Kopiuj');?></a> <?= Yii::t('order', 'dane nadawcy przesyłki');?><br/>
                            <?= Yii::t('order', 'Zmień na fakturę na');?> <a href="#" style="font-weight: bold; text-decoration: underline;" class="inv-type" data-inv-type-switch="true" data-inv-person-name="<?= Yii::t('order', 'osobę fizyczną');?>" data-inv-company-name="<?= Yii::t('order', 'firmę');?>"><?= Yii::t('order', 'osobę fizyczną');?></a>
                        </div>
                    </div>

                    <div class="row" data-inv-company-field="true">
                        <?php echo $form->labelEx($addressModel,'company'); ?>
                        <?php echo $form->textField($addressModel,'company', array('maxlength' => 45)); ?>
                        <?php echo $form->error($addressModel,'company'); ?>
                    </div><!-- row -->

                    <div class="row" data-inv-person-field="true">
                        <?php echo $form->labelEx($addressModel,'name'); ?>
                        <?php echo $form->textField($addressModel,'name', array('maxlength' => 45)); ?>
                        <?php echo $form->error($addressModel,'name'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($addressModel,'address_line_1'); ?>
                        <?php echo $form->textField($addressModel,'address_line_1', array('maxlength' => 90)); ?>
                        <?php echo $form->error($addressModel,'address_line_1'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($addressModel,'zip_code'); ?>
                        <?php echo $form->textField($addressModel,'zip_code', array('maxlength' => 45)); ?>
                        <?php echo $form->error($addressModel,'zip_code'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($addressModel,'city'); ?>
                        <?php echo $form->textField($addressModel,'city', array('maxlength' => 45)); ?>
                        <?php echo $form->error($addressModel,'city'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($addressModel,'country_id'); ?>
                        <?php echo $form->dropDownList($addressModel, 'country_id', CHtml::listData(CountryList::model()->with('countryListTr')->findAll(),'id','trName')); ?>
                        <?php echo $form->error($addressModel, 'country_id'); ?>
                    </div><!-- row -->
                    <div class="row" data-inv-company-field="true">
                        <?php echo $form->labelEx($addressModel,'_nip'); ?>
                        <?php echo $form->textField($addressModel, '_nip', array('maxlength' => 13, 'placeholder' => 'XXX-XXX-XX-XX')); ?>
                        <?php echo $form->error($addressModel, '_nip'); ?>
                    </div><!-- row -->
                </div>


            </div>

            <br/>
            <h3 class="text-center"><?php echo Yii::t('order','Przejdź do płatności');?>:</h3>

            <div class="text-center">
                <?php
                echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('class' => 'btn btn-lg btn-success', 'id' => 'submit-button', 'disabled' => true));
                $this->endWidget();
                ?>
            </div>

        </div>
    </div>



</div><!-- form -->