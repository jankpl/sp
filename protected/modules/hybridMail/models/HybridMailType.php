<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailType');

class HybridMailType extends BaseHybridMailType
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}