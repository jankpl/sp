<?php
/* @var $package Courier_CourierTypeInternal */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'default-data2',
//    'htmlOptions' => ['data-edit-form-id' => $id],
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));
?>
<div id="ebay-form">
    <div style="float: left; width: 48%;">

        <h3><?= Yii::t('courier_ebay', 'Domyślne dane paczki');?></h3>

        <div class="row">
            <?php echo $form->labelEx($package, 'package_weight'); ?>
            <?php echo $form->textField($package, 'package_weight', array('maxlength' => 45)); ?>
            <?php echo $form->error($package, 'package_weight'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($package, 'package_size_l'); ?>
            <?php echo $form->textField($package, 'package_size_l', array('maxlength' => 45)); ?>
            <?php echo $form->error($package, 'package_size_l'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($package, 'package_size_w'); ?>
            <?php echo $form->textField($package, 'package_size_w', array('maxlength' => 45)); ?>
            <?php echo $form->error($package, 'package_size_w'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($package, 'package_size_d'); ?>
            <?php echo $form->textField($package, 'package_size_d', array('maxlength' => 45)); ?>
            <?php echo $form->error($package, 'package_size_d'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($package, 'package_value'); ?>
            <?php echo $form->textField($package, 'package_value', array('maxlength' => 45)); ?>
            <?php echo $form->error($package, 'package_value'); ?>
        </div><!-- row -->
    </div>

    <div style="float: right; width: 48%;">

        <h3><?= Yii::t('courier_ebay', 'Dane nadawcy paczek');?></h3>

        <?php
        $this->renderPartial('_addressData/_form',
            array(
                'model' => $senderAddressData,
                'form' => $form,
                'noEmail' => false,
                'type' => 'sender',
                'noErrorSummery' => true,
            )
        );
        ?>

    </div>
</div>
<div style="clear: both; text-align: center;">
    <input type="submit" value="<?= Yii::t('courier_ebay', 'Dalej');?>" name="default-data" />
</div>
<?php $this->endWidget(); ?>



