<h3>Pickup Date Info</h3>

<table class="list hLeft" style="width: 500px;">
    <tr>
        <td>Pickup Date</td>
        <td><?php echo $model->getDataAsObj()->PickupDateInfo->PickupDate;?> (shift: <?php echo $model->getDataAsObj()->ShiftDays;?>)</td>
    </tr>
    <tr>
        <td>Ready Time</td>
        <td><?php echo $model->getDataAsObj()->PickupDateInfo->ReadyTime;?></td>
    </tr>
    <tr>
        <td>Close Time</td>
        <td><?php echo $model->getDataAsObj()->PickupDateInfo->CloseTime;?></td>
    </tr>
</table>

<h3>Pickup Address</h3>

<table class="list hLeft" style="width: 500px;">
    <tr>
        <td>Company Name</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->CompanyName;?></td>
    </tr>
    <tr>
        <td>Contact Name</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->ContactName;?></td>
    </tr>
    <tr>
        <td>Address Line</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->AddressLine;?></td>
    </tr>
    <tr>
        <td>Room</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->Room;?></td>
    </tr>    <tr>
        <td>Floor</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->Floor;?></td>
    </tr>    <tr>
        <td>City</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->City;?></td>
    </tr>    <tr>
        <td>State Province</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->StateProvince;?></td>
    </tr>
    <tr>
        <td>Urbanization</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->Urbanization;?></td>
    </tr>    <tr>
        <td>Postal Code</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->PostalCode;?></td>
    </tr>    <tr>
        <td>Country Code</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->CountryCode;?></td>
    </tr>    <tr>
        <td>Residential Indicator</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->ResidentialIndicator;?></td>
    </tr>    <tr>
        <td>Pickup Point</td>
        <td><?php echo $model->getDataAsObj()->PickupAddress->PickupPoint;?></td>
    </tr>    <tr>
        <td>Phone</td>
        <td>(<?php echo $model->getDataAsObj()->PickupAddress->Phone->Extension;?>) <?php echo $model->getDataAsObj()->PickupAddress->Phone->Number;?></td>
    </tr>
</table>

<h3>Pickup Piece</h3>

<table class="list hLeft" style="width: 500px;">
    <tr>
        <td>Service Code</td>
        <td><?php echo $model->getDataAsObj()->PickupPiece->ServiceCode;?></td>
    </tr>
    <tr>
        <td>Quantity</td>
        <td><?php echo $model->getDataAsObj()->PickupPiece->Quantity;?></td>
    </tr>
    <tr>
        <td>Destination Country Code</td>
        <td><?php echo $model->getDataAsObj()->PickupPiece->DestinationCountryCode;?></td>
    </tr>
    <tr>
        <td>Container Code</td>
        <td><?php echo $model->getDataAsObj()->PickupPiece->ContainerCode;?></td>
    </tr>
</table>

<h3>Other Data</h3>

<table class="list hLeft" style="width: 500px;">
    <tr>
        <td>Alternate Address Indicator</td>
        <td><?php echo $model->getDataAsObj()->AlternateAddressIndicator;?></td>
    </tr>
    <tr>
        <td>Rate Pickup Indicator</td>
        <td><?php echo $model->getDataAsObj()->RatePickupIndicator;?></td>
    </tr>
    <tr>
        <td>Overweight Indicator</td>
        <td><?php echo $model->getDataAsObj()->OverweightIndicator;?></td>
    </tr>
    <tr>
        <td>Payment Method</td>
        <td><?php echo $model->getDataAsObj()->PaymentMethod;?></td>
    </tr>
    <tr>
        <td>Special Instruction</td>
        <td><?php echo $model->getDataAsObj()->SpecialInstruction;?></td>
    </tr>
    <tr>
        <td>Reference Number</td>
        <td><?php echo $model->getDataAsObj()->ReferenceNumber;?></td>
    </tr>
</table>