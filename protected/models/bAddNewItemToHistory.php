<?php

class bAddNewItemToHistory extends bBaseBehavior
{
    protected $_allowedClass = array('Courier', 'HybridMail', 'InternationalMail');

    protected function getStatClass()
    {
        $statClass = NULL;

        switch($this->getClass())
        {
            case 'Courier':
                $statClass = 'CourierStat';
                break;
            case 'HybridMail':
                $statClass = 'HybridMailStat';
                break;
            case 'InternationalMail':
                $statClass = 'InternationalMailStat';
                break;
        }

        return $statClass;
    }

    protected function getForeignKeyClassAttribute()
    {
        $attribute = NULL;
        switch($this->getClass())
        {
            case 'Courier':
                $attribute = 'courier_id';
                break;
            case 'HybridMail':
                $attribute = 'hybrid_mail_id';
                break;
            case 'InternationalMail':
                $attribute = 'international_mail_id';
                break;
        }

        return $attribute;
    }

    protected function getStatHistoryClass()
    {
        $statClass = NULL;
        switch($this->getClass())
        {
            case 'Courier':
                $statClass = 'CourierStatHistory';
                break;
            case 'HybridMail':
                $statClass = 'HybridMailStatHistory';
                break;
            case 'InternationalMail':
                $statClass = 'InternationalMailStatHistory';
                break;
        }

        return $statClass;
    }

    protected function getFirstStat()
    {
        $firstStat = NULL;
        switch($this->getClass())
        {
            case 'Courier':
                $firstStat = CourierStat::NEW_ORDER;
                break;
            case 'HybridMail':
                $firstStat = HybridMailStat::NEW_ORDER;
                break;
            case 'InternationalMail':
                $firstStat = InternationalMailStat::NEW_ORDER;
                break;
        }

        return $firstStat;
    }

    public function afterSave($event)
    {
        $statHistoryClass = $this->getStatHistoryClass();
        $foreignKeyClassAttribute = $this->getForeignKeyClassAttribute();



        if($this->owner()->isNewRecord)
        {



            $this->owner()->isNewRecord = false;

            $statHistory = new $statHistoryClass;
            $statHistory->$foreignKeyClassAttribute = $this->owner()->id;
            $statHistory->previous_stat_id = NULL;
            $statHistory->current_stat_id = $this->getFirstStat();

            if(!$statHistory->save())
               return false;
        }

    }

}