<?php

class OrderNewController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => [ 'index', 'admin'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }


    public function actionIndex() {

        $this->redirect(array('order/admin'));
    }

    public function actionAdmin() {

        $model = new _OrderNewGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['OrderNewGridView']))
            $model->setAttributes($_GET['OrderNewGridView']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}