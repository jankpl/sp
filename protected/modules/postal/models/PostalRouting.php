<?php

Yii::import('application.modules.postal.models._base.BasePostalRouting');

class PostalRouting extends BasePostalRouting
{
	const SCENARIO_JUST_VALIDATE = 1;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function onAfterConstruct($event)
	{
		parent::onAfterConstruct($event);

		$this->price = new Price();
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_entered',
				'updateAttribute' => 'date_updated',
			)
		);
	}

	public function rules() {
		return array(
			array('country_group_id, postal_type, size_class, weight_class, postal_operator_id', 'required'),
			array('price_id', 'required', 'except' => self::SCENARIO_JUST_VALIDATE),
			array('country_group_id, postal_type, size_class, weight_class, postal_operator_id, price_id', 'numerical', 'integerOnly'=>true),
			array('id, country_group_id, postal_type, size_class, weight_class, postal_operator_id, price_id, date_entered, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * Method returns PostalRouting model. If model do not exists, new one is created
	 *
	 * @param $country_group_id integer
	 * @param $postal_type integer
	 * @param $size_class integer
	 * @param $weight_class integer
	 * @param $postal_user_group_id integer
	 * @return PostalRouting
	 */
	public static function fetchOrCreateModel($country_group_id, $postal_type, $size_class, $weight_class, $postal_user_group_id = NULL)
	{
		$model = self::model()->findByAttributes([
			'country_group_id' => $country_group_id,
			'postal_type' => $postal_type,
			'size_class' => $size_class,
			'weight_class' => $weight_class,
			'postal_user_group_id' => $postal_user_group_id,
		]);

		if($model === NULL)
		{
			$model = new self;
			$model->country_group_id = $country_group_id;
			$model->postal_type = $postal_type;
			$model->size_class = $size_class;
			$model->weight_class = $weight_class;
			$model->postal_user_group_id = $postal_user_group_id;
		}

		return $model;
	}

	public static function deleteUserGroupRouting($country_group_id, $postal_user_group_id)
	{
		$transation = Yii::app()->db->beginTransaction();

		try
		{
			$cmd = Yii::app()->db->createCommand();
			$cmd->delete((new PostalRouting())->tableName(), 'country_group_id = :country_group_id AND postal_user_group_id = :postal_user_group_id', [':country_group_id' => $country_group_id, ':postal_user_group_id' => $postal_user_group_id]);
		} catch(Exception $ex)
		{
			$transation->rollBack();
			return false;
		}

		$transation->commit();
		return true;
	}

	public static function getNumberOfGroupPrices($country_group_id)
	{
		$cmd = Yii::app()->db->createCommand();
		$cmd->select('COUNT(DISTINCT(postal_user_group_id))');
		$cmd->from((new PostalRouting())->tableName());
		$cmd->where('postal_user_group_id IS NOT NULL AND country_group_id = :country_group_id', [':country_group_id' => $country_group_id]);
		$result = $cmd->queryScalar();
		return $result;
	}
}