<?php
/* @var $package Courier_CourierTypeInternal */
?>
    <div class="form">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'default-data2',
//    'htmlOptions' => ['data-edit-form-id' => $id],
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));
        ?>

        <div id="ebay-form">
            <div style="float: left; width: 48%;">
                <h4><?= Yii::t('courier', 'Options');?></h4>

                <div class="row">
                    <?php echo $form->labelEx($linkerImport, 'operator_id'); ?>
                    <?php echo $form->dropDownList($linkerImport, 'operator_id',   CHtml::listData($linkerImport->operators, 'operator_id', 'operator_name'), ['prompt' => '-']); ?>
                    <?php echo $form->error($linkerImport, 'operator_id'); ?>
                </div><!-- row -->

                <div class="row">

                    <div class="inline-form">
                        <div style="text-align: center;">
        <span class="courier-u-operators-selector-form">
            <?php
            if(!S_Useful::sizeof($linkerImport->operators)):
                ?>
                <span class="badge"><?= Yii::t('courier', 'Brak operatorów dla podanych parametrów paczki');?></span>
                <br/>
                <br/>
            <?php
            else:
                foreach($linkerImport->operators AS $operator):
                    ?>
                    <label class="smaller" data-name="<?= $operator['operatorModel']->name_customer;?>" data-broker-id="<?= $operator['operatorModel']->broker_id;?>" data-operator-id="<?= $operator['operatorModel']->id;?>" data-country-list-ids="<?= implode(',',$operator['country_list_ids']);?>">
                       <span <?= $operator['text'] ? 'rel="tooltip" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-html="true" data-title="'.$operator['text'].'"' : '';?>>
                           <?= CHtml::radioButton('CourierTypeU[courier_u_operator_id]', $operator['operatorModel']->id == $model->u_operator_id, ['data-broker' => $operator['operatorModel']->broker_id, 'value' => $operator['operatorModel']->id, 'data-operator-selector' => true]); ?>
                           <div class="operator-content-div" style="background-color: #<?= $operator['operatorModel']->getBgColor();?>">
    <div class="operator-u-name" style=" color: #<?= $operator['operatorModel']->getFontColor();?>;"><?= $operator['operatorModel']->name_customer;?></div>
    <div style="position: absolute; bottom: 10px; left: 0; right: 0;">
    </div>
</div>
                        </span>
                    </label>
                    <?php
                    $i++;
                endforeach;
            endif;
            ?>
                        </div>
                    </div>
                </div>



                <div class="row" style="padding: 5px 0;">


                    <div style="font-weight: bold; text-align: right; width: 33%; display: inline-block;"><?= Yii::t('courier', 'Dostępne kraje:');?></div>
                    <div style="display: inline-block;" data-country-list-placeholder="true">-</div>

                </div>

                <div class="row">
                    <?php echo $form->labelEx($linkerImport, 'collection'); ?>
                    <?php echo $form->checkBox($linkerImport, 'collection'); ?>
                    <?php echo $form->error($linkerImport, 'collection'); ?>
                </div><!-- row -->

                <div class="row">
                    <?php echo $form->labelEx($linkerImport, 'package_wrapping'); ?>
                    <?php echo $form->dropDownList($linkerImport, 'package_wrapping', User::getWrappingList()); ?>
                    <?php echo $form->error($linkerImport, 'package_wrapping'); ?>
                </div><!-- row -->

                <h4><?= Yii::t('courier_linker', 'Domyślne dane paczki');?></h4>
                <div class="row">
                    <?php echo $form->labelEx($package, 'package_weight'); ?>
                    <?php echo $form->textField($package, 'package_weight', array('maxlength' => 45)); ?> <?php echo Courier::getWeightUnit();?>
                    <?php echo $form->error($package, 'package_weight'); ?>
                </div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($package, 'package_size_l'); ?>
                    <?php echo $form->textField($package, 'package_size_l', array('maxlength' => 45)); ?> <?php echo Courier::getDimensionUnit();?>
                    <?php echo $form->error($package, 'package_size_l'); ?>
                </div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($package, 'package_size_w'); ?>
                    <?php echo $form->textField($package, 'package_size_w', array('maxlength' => 45)); ?> <?php echo Courier::getDimensionUnit();?>
                    <?php echo $form->error($package, 'package_size_w'); ?>
                </div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($package, 'package_size_d'); ?>
                    <?php echo $form->textField($package, 'package_size_d', array('maxlength' => 45)); ?> <?php echo Courier::getDimensionUnit();?>
                    <?php echo $form->error($package, 'package_size_d'); ?>
                </div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($package, 'package_value'); ?>
                    <?php echo $form->textField($package, 'package_value', array('maxlength' => 45)); ?> <?php echo $form->dropDownList($package, 'value_currency', Courier::getCurrencyList()); ?>
                    <?php echo $form->error($package, 'package_value'); ?>
                </div><!-- row -->

                <h4><?= Yii::t('courier_linker', 'Domyślne dane odbiorcy');?></h4>
                <div class="row">
                    <?php echo $form->label($receiverAddressData, '[receiver]tel'); ?>
                    <?php echo $form->textField($receiverAddressData, '[receiver]tel', array('maxlength' => 45)); ?>
                    <?php echo $form->error($receiverAddressData, '[receiver]tel'); ?>
                </div><!-- row -->
                <h4><?= Yii::t('courier_linker', 'Domyślne dodatki');?></h4>
                <div class="row">
                    <?php echo CHtml::dropDownList('_additions', $_additions, CHtml::listData(CourierUAdditionList::model()->with('courierUAdditionListTr')->findAllByAttributes(['stat' => 1]), 'id', 'courierUAdditionListTr.title'), array('multiple' => true, 'style' => 'width: 90%; margin: 0 15px;')); ?>
                </div><!-- row -->
            </div>
            <div style="float: right; width: 48%;">
                <h4><?= Yii::t('courier_linker', 'Dane nadawcy paczek');?></h4>
                <?php
                $this->renderPartial('../courierU/_addressData/_form',
                    array(
                        'model' => $senderAddressData,
                        'form' => $form,
                        'noEmail' => false,
                        'type' => 'sender',
                        'i' => 'sender',
                        'noErrorSummery' => true,
                    )
                );
                ?>
            </div>
        </div>

        <div class="navigate" style="clear: both;">
            <input type="submit" value="<?= Yii::t('courier_linker', 'Dalej');?>" name="save-default-data" class="btn btn-primary"/>
        </div>
        <?php $this->endWidget(); ?>
        <div style="text-align: center;">
            <a class="btn btn-xs" href="<?= Yii::app()->createUrl('/courier/courierU/linker');?>"><?= Yii::t('courier_linker','zacznij od nowa');?></a>
        </div>
    </div>

<?php
$script = "
    function getCountryList(country_list_ids) {
    
        var placeholder = $('[data-country-list-placeholder]');
        placeholder.html('?');
        
        $.ajax({
            url: '".Yii::app()->createUrl('/courier/courierU/ajaxGetCountryList')."',
            data: {country_list_ids: country_list_ids},
            method: 'POST',
            dataType: 'json',
            success: function (result) {

                

                if(result instanceof Array && result.length)
                {
                       placeholder.html(result.join(', '));
                       
                
                } else {
                    placeholder.html('-');
                }
                
            },
            error: function (e) {

                console.log(e);
            }
        });
    }

var selectedOperator = $('#LinkerImportU_operator_id').val();

if(selectedOperator)
{    
    $('[data-operator-selector][value=' + selectedOperator + ']').attr('checked', true);          
    getCountryList($('[data-operator-selector][value=\"' + selectedOperator + '\"]').closest('label').attr('data-country-list-ids'));
}

$('[rel=\"tooltip\"]').tooltip();

$('[data-operator-selector]').on('change', function(){
    $('#LinkerImportU_operator_id').val($(this).val());
    getCountryList($('[data-operator-selector][value=\"' + $(this).val() + '\"]').closest('label').attr('data-country-list-ids'));
});

$('#LinkerImportU_operator_id').on('change', function(){

    $('[data-operator-selector]:checked').prop('checked', false);
    $('[data-operator-selector][value=' + $(this).val() + ']').prop('checked', true);
    getCountryList($('[data-operator-selector][value=\"' + $(this).val() + '\"]').closest('label').attr('data-country-list-ids'));

});";

Yii::app()->clientScript->registerScript('import-js', $script, CClientScript::POS_READY);




