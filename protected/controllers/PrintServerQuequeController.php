<?php

class PrintServerQuequeController extends CController {

    const STRIP_SIZE = 100;

    // run queque
    public function actionIndex($mode = 0, $id = 0, $type = false, $async = false)
    {

        if($mode == 1)
            sleep(10);
        else if($mode == 2)
            sleep(20);
        else if($mode == 3)
            sleep(30);
        else if($mode == 4)
            sleep(40);
        else if($mode == 5)
            sleep(50);

        MyDump::dump('psql.txt', print_r($mode,1));

        Yii::app()->PrintServerQueque->run(self::STRIP_SIZE, $id, $type);

        // there is more
        if(!$id && Yii::app()->PrintServerQueque->howLong())
        {
            $path = Yii::app()->createAbsoluteUrl('/printServerQueque/index');

            header("Location: ".$path);
            exit;

        } else {

            $CACHE_NAME = 'printServerQuequeClearOld';
            $alreadyDone = Yii::app()->cache->get($CACHE_NAME);

            // prevent doing it too often
            if ($alreadyDone !== false) {
                Yii::app()->cache->set($CACHE_NAME, 10, 60 * 60 * 6);

                // clear old items if there is nothing else to do...
                Yii::app()->PrintServerQueque->clearOldAndFinished();
            }
        }

        exit;
    }


}