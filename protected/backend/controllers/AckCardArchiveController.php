<?php

class AckCardArchiveController extends Controller
{

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
//                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        /* @var $model AckCardArchive */
        $model = $this->loadModel($id, 'AckCardArchive');

        $this->render('view',array(
            'model' => $model,
        ));
    }

       /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(array('/ackCardArchive/admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new AckCardArchive('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['AckCardArchive']))
            $model->attributes = $_GET['AckCardArchive'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function actionGetPdf($id)
    {
        return self::_getFile($id, AckCardArchive::TYPE_PDF);
    }

    public function actionGetXls($id)
    {
        return self::_getFile($id, AckCardArchive::TYPE_XLS);
    }


    protected function _getFile($id, $type)
    {
        $model = AckCardArchive::model()->findByPk($id);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        if($type == AckCardArchive::TYPE_PDF)
        {
            $file_path = $model->path_pdf;
            $file_name = 'ack_'.$model->date_entered.'.pdf';
            $file_type = 'pdf';
        }
        else if($type == AckCardArchive::TYPE_XLS)
        {
            $file_path = $model->path_xls;
            $file_name = 'ack_'.$model->date_entered.'.xls';
            $file_type = 'xls';
        }

        if($file_path == NULL)
            throw new CHttpException(404);

        return Yii::app()->getRequest()->sendFile($file_name, @file_get_contents($file_path), $file_type);
    }


}

