<?php

class CourierCodSettledController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {

        $model = new _CourierCodSettledGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierCodSettledGridView']))
            $model->setAttributes($_GET['CourierCodSettledGridView']);

        $settleFormModel = new CourierCodSettledSettleByGridView();

        if(isset($_POST['CourierCodSettledSettleByGridView']))
        {

            MyDump::dump('admin_cod_settle_admin.txt', print_r($_POST,1));

            $settleFormModel->attributes = $_POST['CourierCodSettledSettleByGridView'];
            if($settleFormModel->validate()) {


                $courier_ids = $settleFormModel->package_ids;
                $courier_ids = explode(',', $courier_ids);
                MyDump::dump('admin_cod_settle_admin.txt', print_r($settleFormModel->attributes,1));
                MyDump::dump('admin_cod_settle_admin.txt', print_r($courier_ids,1));


                $return = CourierCodSettled::addListOfItems($courier_ids, $settleFormModel->type, $settleFormModel->settle_date);
                if (!$return)
                    Yii::app()->user->setFlash('error', 'Zero items has been settled!');
                else
                    Yii::app()->user->setFlash('success', 'Number of settled items: ' . $return);

                $url = Yii::app()->createUrl('/courier/courierCodSettled/admin');

                $this->redirect($url.'#');

            }

        }

        $this->render('admin', array(
            'model' => $model,
            'settleFormModel' => $settleFormModel,
        ));
    }

    public function actionStat($currency = false)
    {
        if($currency && !in_array($currency, array_keys(CourierCodAvailability::getCurrencyList())))
            throw new CHttpException(404);

        $this->render('stat', [
            'currency' => $currency
        ]);
    }


//    public function actionSettleByGridView($unsettle = false)
//    {
//        $courier_ids = [];
//        /* @var $model Courier */
//
//
//        if (isset($_POST['Courier']))
//        {
//            $courier_ids =  $_POST['Courier']['ids'];
//            $courier_ids = explode(',', $courier_ids);
//        }
//
//        if($unsettle) {
//            $return = CourierCodSettled::removeListOfItems($courier_ids);
//            if(!$return)
//                Yii::app()->user->setFlash('error', 'Zero items has been unsettled!');
//            else
//                Yii::app()->user->setFlash('success', 'Number of unsettled items: '.$return);
//        }
//
//        $this->redirect(Yii::app()->createUrl('/courier/courierCodSettled/admin'));
//
//    }

    public function actionExportToFileByGridView()
    {
        /* @var $model CourierCodSettled */

        $models = [];
        if (isset($_POST['Courier']))
        {
            $courier_ids =  $_POST['Courier']['ids'];
            $courier_ids = explode(',', $courier_ids);

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {

                $models = Courier::model()->with('courierCodSettled')->with('user')->findAllByAttributes(['id' => $courier_ids]);
            }
        }

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/courier/courierCodSettled/admin'));
        } else {
            CourierCodSettled::exportToXls($models, false);
        }
    }

    public function actionUnsettle($id)
    {
        /* @var $model CourierCodSettled */
        $model = CourierCodSettled::model()->findByAttributes(['courier_id' => $id]);

        if($model === NULL)
            throw new CHttpException(404);

        $courier_id = $model->courier_id;

        if($model->delete())
            Yii::app()->user->setFlash('success', 'COD has been unsettled!');
        else
            Yii::app()->user->setFlash('error', 'COD could not be unsettled!');

        $this->redirect(['/courier/courierCodSettled/update', 'id' => $courier_id]);
        Yii::app()->end;
    }

    public function actionUpdate($id)
    {
        /* @var $model Courier */
        $model = $this->loadModel($id, 'Courier');

        $settleFormModel = new CourierCodSettledSettleForm();
        $settleFormModel->package_id = $model->id;

        if(isset($_POST['CourierCodSettledSettleForm']))
        {
            $settleFormModel->attributes = $_POST['CourierCodSettledSettleForm'];
            if($settleFormModel->validate()) {

                $courier_ids = [$settleFormModel->package_id];

                $return = CourierCodSettled::addListOfItems($courier_ids, $settleFormModel->type, $settleFormModel->settle_date);
                if (!$return)
                    Yii::app()->user->setFlash('error', 'Zero items has been settled!');
                else
                    Yii::app()->user->setFlash('success', 'Number of settled items: ' . $return);


                $this->refresh();
            }
        }


        $this->render('update', [
            'model' => $model,
            'settleFormModel' => $settleFormModel,
        ]);

    }
}