<?php

class CourierExternalOperatorController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierExternalOperator'),
        ));
    }

    public function actionCreate() {
        $model = new CourierExternalOperator;

        $this->performAjaxValidation($model, 'courier-external-operator-form');

        if (isset($_POST['CourierExternalOperator'])) {
            $model->setAttributes($_POST['CourierExternalOperator']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id, 'CourierExternalOperator');

        if(!$model->editable)
            throw new CHttpException(400, 'You cannot edit this item.');

        $this->performAjaxValidation($model, 'courier-external-operator-form');

        if (isset($_POST['CourierExternalOperator'])) {
            $model->setAttributes($_POST['CourierExternalOperator']);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model = $this->loadModel($id, 'CourierExternalOperator');

            if(!$model->editable)
                throw new CHttpException(400, 'You cannot delete this item.');

            try
            {
                $model->delete();
            } catch(Exception $ex)
            {
                throw new CHttpException(400, Yii::t('app', 'You cannot delete it!'));
            }
            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierExternalOperator('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierExternalOperator']))
            $model->setAttributes($_GET['CourierExternalOperator']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}