<h4>Export to file</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/order/exportToFileByGridView'),
));
?>

<?php
echo TbHtml::submitButton('Export to .xls', array(
    'onClick' => 'js:
    $("#order-ids-export").val($.fn.yiiGridView.getChecked("order-grid","selected_rows").toString());
    ;',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Order[ids]','', array('id' => 'order-ids-export')); ?>
<?php
$this->endWidget();
?>