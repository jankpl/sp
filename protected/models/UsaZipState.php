<?php

class UsaZipState
{
    public static function getStateByZip($zip, $justCode = false)
    {

        $zip = preg_replace("/[^0-9]/","", $zip);
        $zip = trim($zip);
        $zip = preg_replace('/[^a-zA-Z0-9]/', '', $zip);
        $zip = substr($zip,0,3);

        $cmd = Yii::app()->db->createCommand();

        if($justCode)
            $cmd->select('code');
        else
            $cmd->select('state');

        $cmd->from('usa_zip_state');
        $cmd->where('zip_from <= :zip', array(':zip' => $zip));
        $cmd->Andwhere('zip_to >= :zip', array(':zip' => $zip));
        $state = $cmd->queryScalar();

        return $state;
    }


}