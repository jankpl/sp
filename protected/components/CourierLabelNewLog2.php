<?php

class CourierLabelNewLog2 extends CApplicationComponent
{
    const WHAT_PACKAGE_CREATED = '0:NPC';
    const WHAT_NEW = '1:NCL';
    const WHAT_ORDER_LABEL_START = '2:OLS';
    const WHAT_PROCESS_LABEL_START = '3:PLS';
    const WHAT_PROCESS_LABEL_END = '4:PLE';
    const WHAT_ORDER_LABEL_END = '5:OLE';
    const WHAT_GET_FILE_START = '6:GFS';
    const WHAT_GET_FILE_END = '7:GFE';

    protected $_lastTime = [];

    public function saveToLog(CourierLabelNew $model, $what_id, $what_desc = '')
    {

        $filename = 'CLN2_log.txt';

        $base = Yii::app()->basePath . DIRECTORY_SEPARATOR .'_dump';
        $baseWithDate = $base.'/'.date('Ymd');

        if(!is_dir($baseWithDate))
            @mkdir($baseWithDate);

        $filename = $baseWithDate . DIRECTORY_SEPARATOR . $filename;

        if($what_id == self::WHAT_NEW) {
            $this->_lastTime[$model->id] = microtime(true);
            $this->saveToLog($model, self::WHAT_PACKAGE_CREATED, 'Courier source: ' . $model->courier->source);
        }

        $lastTime = $this->_lastTime[$model->id];
        $currentTime = microtime(true);
        $diff = number_format($currentTime - $lastTime,4);

        $this->_lastTime[$model->id] = $currentTime;

        $data = date('Y-m-d H:i:s').'||'.$model->id.'||'.str_pad($model->operator,4,'0',STR_PAD_LEFT).'||'.str_pad($what_id,2,'0',STR_PAD_LEFT).'||+'.$diff.'||'.str_pad($model->stat,2,'0',STR_PAD_LEFT).'||'.$what_desc."\r\n";

        @file_put_contents($filename, $data, FILE_APPEND);


    }



}