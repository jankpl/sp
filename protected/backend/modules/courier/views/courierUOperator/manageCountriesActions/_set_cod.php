<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'set-cod',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courierUOperator/manageCouriersSetCodByGV', ['operator_id' => $operator_id]),
    'htmlOptions' => [
        'class' => 'confirm-me',
    ]
));
?>


<?php echo CHtml::dropDownList('SetCod[cod]',NULL, CourierUOperator2CountryGroup::getStatList()); ?>
<br/>
<?php
echo TbHtml::submitButton('Set', array(
    'onClick' => 'js:
    $(".gv-manage-countries-ids").val($("#gv-manage-countries").selGridView("getAllSelection").toString());
   ',
    'name' => 'Ids', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>
<?php echo CHtml::hiddenField('SetCod[ids]','', array('class' => 'gv-manage-countries-ids')); ?>
<?php
$this->endWidget();
?>
