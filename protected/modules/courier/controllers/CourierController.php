<?php

class CourierController extends Controller {

    public function beforeAction($action)
    {
//        if($action->id == 'tt')
//            return parent::beforeAction($action);
//
//        $this->render('//site/underConstruction');
//
//        Yii::app()->end();

        return parent::beforeAction($action);
    }



    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
            array(
                'application.filters.GridViewHandler' //path to GridViewHandler.php class for filtering gridwiev data
            )
        ));
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {
        return array(

            array('allow',
                //'actions'=>array('index', 'create', 'view', 'tt', 'carriageTicket'),
                'actions'=>array('tt','create', 'view', 'carriageTicket', 'ajaxGetDimensionalWeight', 'ackCard', 'ajaxCheckIfAvailable', 'ajaxCheckIfPickupAvailable', 'ajaxGetAdditions', 'ajaxGetPrice', 'ajaxIsAutomaticReturnAvailable'),
                'users'=>array('*'),
            ),
            array('allow',
//                'actions'=>array('ajaxImportModalSettings'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionAck()
    {
        $this->panelLeftMenuActive = 160;

        $model = new F_GenerateAck();

        if(isset($_POST['F_GenerateAck'])) {

            $model->setAttributes($_POST['F_GenerateAck']);

            if($model->validate())
            {
                $days = intval($model->days);
                $condition = 't.user_id = :user_id AND t.courier_stat_id != :stat AND t.courier_stat_id != :stat2 AND t.courier_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3 AND DATE(t.date_entered) > (CURDATE() - INTERVAL :days DAY)';

                if($model->onlyNew)
                    $condition .= ' AND ack_generated IS NULL';

                $params = [
                    ':user_id' => Yii::app()->user->id,
                    ':stat' => CourierStat::CANCELLED,
                    ':stat2' => CourierStat::CANCELLED_PROBLEM,
                    ':stat3' => CourierStat::CANCELLED_USER,
                    ':uc1' => Courier::USER_CANCEL_CONFIRMED,
                    ':uc2' => Courier::USER_CANCEL_REQUEST,
                    ':uc3' => Courier::USER_CANCEL_WARNED,
                    ':days' => $days
                ];


                $models = Courier::model()
                    ->with('senderAddressData')
                    ->with('receiverAddressData')
                    ->with('courierTypeInternal')
                    ->with('courierTypeInternal.order')
                    ->with('courierTypeInternal.pickupLabel')
                    ->with(array(
                        "courierTypeInternal.pickupLabel.courierLabelNew" => array(
                            'alias' => 'internalPickupLabelNew',
                        ),
                    ))
                    ->with('courierTypeInternal.commonLabel')
                    ->with(array(
                        "courierTypeInternal.commonLabel.courierLabelNew" => array(
                            'alias' => 'internalCommonLabelNew',
                        ),
                    ))
                    ->with('courierTypeExternal')
                    ->findAll([
                        'condition' => $condition,
                        'params' => $params
                    ]);

                /* @var $model Courier */

                if (!S_Useful::sizeof($models)) {
                    Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania potwierdzenia!'));
                    $this->redirect(['/courier/courier/ack']);
                }

                $temp = [];
                $temp2 = [];
                CourierAcknowlegmentCard::generateCardMulti($models, false, false, $temp, false, $temp2, false, $model->ignoreOperatorsCustoms, true);
            }

        }

        $this->render('ack',
            ['model' => $model]
        );
    }

    public function actionExport($mode)
    {

        switch($mode)
        {
            case parent::EXPORT_MODE_INTERNAL:
                $this->panelLeftMenuActive = 114;
                break;
            case parent::EXPORT_MODE_INTERNAL_SIMPLE:
                $this->panelLeftMenuActive = 152;
                break;
            case parent::EXPORT_MODE_OOE:
                $this->panelLeftMenuActive = 124;
                break;
            case parent::EXPORT_MODE_EXTERNAL:
                $this->panelLeftMenuActive = 133;
                break;
            case parent::EXPORT_MODE_DOMESTIC:
                $this->panelLeftMenuActive = 143;
                break;
        }

        $this->render('export',
            [
                'type' => $mode,
            ]
        );
    }

    /**
     * Shortcut metod used for creating package pickup for Postal.
     * @param $spPointId
     * @param $type
     * @throws CHttpException
     */
    public function actionCreatePackageBySpPoint($spPointId, $type)
    {
        if($type != Courier::TYPE_DOMESTIC && $type != Courier::TYPE_INTERNAL)
            throw new CHttpException(403);


        if($type == Courier::TYPE_DOMESTIC)
        {
            $senderAddressData = new CourierTypeDomestic_AddressData;
            $model = new Courier_CourierTypeDomestic();
            $model->courierTypeDomestic = new CourierTypeDomestic();
        }
        else if($type == Courier::TYPE_INTERNAL)
        {
            $senderAddressData = new CourierTypeInternal_AddressData;
            $model = new Courier_CourierTypeInternal();
            $model->courierTypeInternal = new CourierTypeInternal();
        }

        if(!YII::app()->user->isGuest)
            $senderAddressData->loadUserData();

        $receiverAddressData = SpPoints::model()->findByPk($spPointId)->addressData;

        if($senderAddressData === NULL OR $receiverAddressData === NULL)
            throw new CHttpException(403);

        $model->senderAddressData = $senderAddressData;
        $model->receiverAddressData = $receiverAddressData;

        if($type == Courier::TYPE_DOMESTIC)
        {
            $this->_clone_domestic($model);
        }
        else if($type == Courier::TYPE_INTERNAL)
        {
            $this->_clone_internal($model);
        }

    }


    /**
     * Fills form for creating new packages based on old ones.
     *
     * @param $urlData string Hash of package that will be base on new one
     * @throws CHttpException
     */
    public function actionClone($urlData)
    {
        $model = Courier::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->with('courierTypeInternal')
            ->with('courierTypeExternal')
            ->with('courierTypeInternalSimple')
            ->with('courierTypeOoe')
            ->with('courierTypeDomestic')
            ->with('courierTypeU')
            ->findByAttributes(['hash' => $urlData]);
        if($model === NULL)
            throw new CHttpException(404);

        Yii::app()->user->setFlash('success', Yii::t('courier', 'Dane paczki zostały sklonowane.{br}{strong}Pamiętaj, że nowa paczka nie będzie powiąznana z usługami paczki źrodłowej, takimi jak np. eBay i jej dane nie zostaną automatycznie przekazane do tych usług.{/strong}', ['{br}' => '<br/>', '{strong}' => '<strong>', '{/strong}' => '</strong>']));

        $model->source = Courier::SOURCE_CLONE;

        switch($model->getType())
        {
            case Courier::TYPE_INTERNAL:
                $this->_clone_internal($model);
                break;
            case Courier::TYPE_DOMESTIC:
                $this->_clone_domestic($model);
                break;
            case Courier::TYPE_INTERNAL_OOE:
                $this->_clone_ooe($model);
                break;
            case Courier::TYPE_EXTERNAL:
                $this->_clone_external($model);
                break;
            case Courier::TYPE_U:
                $this->_clone_u($model);
                break;
            default:
                throw new CHttpException(403);
                break;
        }

    }

    /**
     * Prepare data array for cloning package
     *
     * @param Courier $model
     */
    protected function _clone_u(Courier $model)
    {
        $data = [];

        $model->cod_value = floatval($model->cod_value);

        $data['senderAddressData'] = $model->senderAddressData->attributes;
        $data['receiverAddressData'] = $model->receiverAddressData->attributes;
        $data['model'] = $model->attributes;
        $data['modelTypeU'] = $model->courierTypeU->attributes;

        $additions = [];
        foreach($model->courierTypeU->listAdditions() AS $addition)
            array_push($additions, $addition->courier_u_addition_list_id);

        $data['additions'] = $additions;

        Yii::app()->session['cloneTypeU'] = $data;

        if($model->senderAddressData->country_id == CountryList::COUNTRY_PL && $model->receiverAddressData->country_id == CountryList::COUNTRY_PL)
            $this->redirect(['/courier/courierU/create', 'domestic' => true]);
        else
            $this->redirect(['/courier/courierU/create']);
    }

    /**
     * Prepare data array for cloning package
     *
     * @param Courier $model
     */
    protected function _clone_internal(Courier $model)
    {
        $data = [];

        $model->cod_value = floatval($model->cod_value);

        $data['senderAddressData'] = $model->senderAddressData->attributes;
        $data['receiverAddressData'] = $model->receiverAddressData->attributes;
        $data['model'] = $model->attributes;

        if($model->package_weight_client)
            $data['model']['package_weight'] = $model->package_weight_client;

        $data['modelTypeInternal'] = $model->courierTypeInternal->attributes;

        $additions = [];
        foreach($model->courierTypeInternal->listAdditions() AS $addition)
            array_push($additions, $addition->courier_addition_list_id);

        $data['additions'] = $additions;

        Yii::app()->session['cloneTypeInternal'] = $data;

        $this->redirect(['/courier/courier/create']);
    }

    /**
     * Prepare data array for cloning package
     *
     * @param Courier $model
     */
    protected function _clone_domestic(Courier $model)
    {
        $data = [];

        $data['senderAddressData'] = $model->senderAddressData->attributes;
        $data['receiverAddressData'] = $model->receiverAddressData->attributes;
        $data['model'] = $model->attributes;
        $data['modelTypeDomestic'] = $model->courierTypeDomestic->attributes;

        // clone special params
        foreach($model->courierTypeDomestic->clonableAttributes() AS $item)
            $data['modelTypeDomestic'][$item] = $model->courierTypeDomestic->$item;


        $additions = [];

        foreach($model->courierTypeDomestic->listAdditions() AS $addition)
            array_push($additions, $addition->courier_domestic_addition_list_id);

        $data['additions'] = $additions;

        Yii::app()->session['cloneTypeDomestic'] = $data;

        $this->redirect(['/courier/courierDomestic/create']);
    }

    /**
     * Prepare data array for cloning package
     *
     * @param Courier $model
     */
    protected function _clone_ooe(Courier $model)
    {
        $data = [];

        //$model->courierTypeOoe->service_code = CourierOoeService::mapServiceNameToId($model->courierTypeOoe->service_code);

        $data['senderAddressData'] = $model->senderAddressData->attributes;
        $data['receiverAddressData'] = $model->receiverAddressData->attributes;
        $data['model'] = $model->attributes;
        $data['modelTypeOoe'] = $model->courierTypeOoe->attributes;

        $additions = [];

        foreach($model->courierTypeOoe->listAdditions() AS $addition)
            array_push($additions, $addition->courier_ooe_addition_list_id);

        $data['additions'] = $additions;

        Yii::app()->session['cloneTypeOoe'] = $data;

        $this->redirect(['/courier/courierOoe/create']);
    }

    /**
     * Prepare data array for cloning package
     *
     * @param Courier $model
     */
    protected function _clone_external(Courier $model)
    {
        $data = [];

        $data['senderAddressData'] = $model->senderAddressData->attributes;
        $data['receiverAddressData'] = $model->receiverAddressData->attributes;
        $data['model'] = $model->attributes;
        $data['modelTypeExternal'] = $model->courierTypeExternal->attributes;


        Yii::app()->session['cloneTypeExternal'] = $data;

        $this->redirect(['/courier/courier/createPremium']);
    }

    /**
     * Fill form with cloned package data
     *
     * @param Courier $model
     */
    protected function _clone_internal_fill()
    {

        if(isset(Yii::app()->session['cloneTypeInternal'])) {

            $data = Yii::app()->session['cloneTypeInternal'];
            unset(Yii::app()->session['cloneTypeInternal']);
//            unset($_SESSION['cloneTypeInternal']);

            if($data['model']['cod_value'] > 0) {
                $data['model']['_client_cod'] = 1;
            }

            $this->setPageState('step1_model', $data['model']);
            $this->setPageState('step1_courierTypeInternal', $data['modelTypeInternal']);
            $this->setPageState('step1_senderAddressData', $data['senderAddressData']);
            $this->setPageState('step1_receiverAddressData', $data['receiverAddressData']);
            $this->setPageState('courier_additions', $data['additions']);

        }
    }

    /**
     * Create new courier based on data from pricing calculator on homepage. Use cloning methods
     */
    public function actionCreateFP()
    {

        $courier = new Courier_CourierTypeInternal();
        $courier->package_weight = (int) $_GET['w'];
        $courier->package_size_l = (int) $_GET['sl'];
        $courier->package_size_d = (int) $_GET['sd'];
        $courier->package_size_w = (int) $_GET['sw'];

        $courierTypeInternal = new CourierTypeInternal();
        $courierTypeInternal->with_pickup = CourierTypeInternal::WITH_PICKUP_NONE;

        $sender = new CourierTypeInternal_AddressData();
        $sender->country_id = (int) $_GET['sci'];

        $receiver = new CourierTypeInternal_AddressData();
        $receiver->country_id = (int) $_GET['rci'];

        $data = [];

        $data['senderAddressData'] = $sender->attributes;
        $data['receiverAddressData'] = $receiver->attributes;
        $data['model'] = $courier->attributes;
        $data['modelTypeInternal'] = $courierTypeInternal->attributes;

        Yii::app()->session['cloneTypeInternal'] = $data;

        $this->redirect(['/courier/courier/create']);
    }

    public function actionCreate()
    {
        $this->panelLeftMenuActive = 111;


        // flashes do not like redirects to clear page states, so keep them in session before that
        $tempFlashes = Yii::app()->session['tempFlashContainer'];
        if(is_array($tempFlashes) && S_Useful::sizeof($tempFlashes))
        {
            unset(Yii::app()->session['tempFlashContainer']);
            foreach($tempFlashes AS $key => $value)
                Yii::app()->user->setFlash($key, $value);
        }

        if(Yii::app()->session['courierFormFinished']) // prevent going back and sending the same form again
        {
            Yii::app()->session['tempFlashContainer'] = Yii::app()->user->getFlashes();

            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['courierFormFinished']);

            $this->refresh(true);
        }


        $this->_clone_internal_fill();

        if(isset($_POST['summary']))
            $this->create_step_summary(); // Validate addition list, summary
        elseif (isset($_POST['finish']) OR isset($_POST['finish_goback']))
            $this->create_finish(isset($_POST['finish_goback']));
        elseif (isset($_POST['forceReload']))
            $this->create_step_1(true);
        else
            $this->create_step_1(); // this is the default, first time (step1)
    }

    protected function create_step_1($validate = false)
    {
        if($validate)
        {
            if($this->create_validate_step_1() == 'break')
                return;
        }

        if(Yii::app()->user->isGuest && !$this->getPageState('loginProposition')) {

            $this->setPageState('loginProposition', 1);

            $model = new LoginForm();
            $model->returnURL = Yii::app()->createUrl('courier/create');

            $this->render('create/loginProposition', array('model' => $model));

        } else {

            $this->setPageState('form_started', true);
            $this->setPageState('start_user_id', Yii::app()->user->id);

            $this->setPageState('last_step',1); // save information about last step

            $model = new Courier_CourierTypeInternal(Courier_CourierTypeInternal::SCENARIO_STEP1);

            $model->user_id = Yii::app()->user->id;

            $model->courierTypeInternal = new CourierTypeInternal(CourierTypeInternal::SCENARIO_STEP1);

            $model->courierTypeInternal->courier = $model;

            $model->senderAddressData = new CourierTypeInternal_AddressData('insert', UserContactBook::TYPE_SENDER);
//        $model->senderAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_SENDER);
            $model->receiverAddressData = new CourierTypeInternal_AddressData('insert', UserContactBook::TYPE_RECEIVER);
//        $model->receiverAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_RECEIVER);
//            $model->courierTypeInternal->with_pickup = CourierTypeInternal::WITH_PICKUP_REGULAR;
            $model->senderAddressData->country_id = CountryList::COUNTRY_PL;
            $model->receiverAddressData->country_id = Yii::app()->user->model->getDefaultPackageReceiverCountryId() ? Yii::app()->user->model->getDefaultPackageReceiverCountryId() : CountryList::COUNTRY_PL;

            $model->afterFinalInit();

            // load user data as sender
            if(!S_Useful::sizeof($this->getPageState('step1_senderAddressData',array())))
                $model->senderAddressData->loadUserData();
            else
                $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());


            $model->attributes = $this->getPageState('step1_model',[]);

            $model->courierTypeInternal->attributes = $this->getPageState('step1_courierTypeInternal', []);


            $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);

            $modelCourierAddition = $this->getPageState('courier_additions', []);
            $modelCourierAdditionList = CourierAdditionList::getAdditionsByCourier($model);

            $currency = $this->getPageState('currency', false);

            if(!$currency)
                $currency = Yii::app()->PriceManager->getCurrency();
            else
                Yii::app()->PriceManager->setCurrency($currency);

            $additionsHtml = $this->renderPartial('create/_additionList', array(
                'modelCourierAdditionList'=>$modelCourierAdditionList,
                'courierAddition'=>$modelCourierAddition,
                'currency' => $currency,
            ), true, false);

            $noPrice = false;

            $currencySymbol = Yii::app()->PriceManager->getCurrencySymbol();
            if(!$model->courierTypeInternal->priceTotal($currency))
                $noPrice = true;

            $priceEach = Yii::app()->PriceManager->getFinalPrice($model->courierTypeInternal->priceEach($currency)).' '.$currencySymbol;
            $priceTotal = Yii::app()->PriceManager->getFinalPrice($model->courierTypeInternal->priceTotal($currency)).' '.$currencySymbol;



            $codCurrency = false;
            $deliveryOperator = $model->courierTypeInternal->getDeliveryOperator();

            $deliveryOperatorBrokerId = false;
            if($deliveryOperator)
            {
                $codCurrency = CourierCodAvailability::findCurrencyForOperatorAndCountry(CourierCodAvailability::OPERATOR_INTERNAL, $deliveryOperator, $model->receiverAddressData->country_id);
                if($codCurrency)
                {
                    $codCurrency = CourierCodAvailability::getCurrencyNameById($codCurrency);
                }

                $deliveryOperatorBrokerId = CourierOperator::getBrokerById($deliveryOperator);;
            }

            $this->setPageState('last_step',1); // save information about last step

            $this->render('create/step1',
                [
                    'noPrice' => $noPrice,
                    'currency' => $currency,
                    'model' => $model,
                    'noPickup' => false,
                    'noCarriage' => false,
                    'priceEach' => $priceEach,
                    'priceTotal' => $priceTotal,
                    'additionsHtml' => $additionsHtml,
                    'codCurrency' => $codCurrency,
                    'deliveryOperatorBrokerId' => $deliveryOperatorBrokerId,
                    'favList' => CHtml::listData(CourierSpPointFav::getPointsForUser(),'complexId', 'usefulName')
                ]);


        }
    }

    protected function create_validate_step_1()
    {

        $model = new Courier_CourierTypeInternal(Courier_CourierTypeInternal::SCENARIO_STEP1);
        $model->courierTypeInternal = new CourierTypeInternal(CourierTypeInternal::SCENARIO_STEP1);
        $model->courierTypeInternal->courier = $model;

        $model->user_id = Yii::app()->user->id;

        $model->senderAddressData = new CourierTypeInternal_AddressData();
//        $model->senderAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_SENDER);
        $model->receiverAddressData = new CourierTypeInternal_AddressData();
//        $model->receiverAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_RECEIVER);

        $currency = $this->getPageState('currency', false);
        if(!$currency)
            $currency = Yii::app()->PriceManager->getCurrency();
        else
            Yii::app()->PriceManager->setCurrency($currency);

        $noPrice = true;

        if(isset($_POST['Courier_CourierTypeInternal']))
        {
            $errors = false;
            $noPickup = false;
            $noCarraige = false;

            $courierAddition = [];

            $model->attributes = $_POST['Courier_CourierTypeInternal'];
            $model->courierTypeInternal->attributes = $_POST['CourierTypeInternal'];
            $model->senderAddressData->attributes = $_POST['CourierTypeInternal_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['CourierTypeInternal_AddressData']['receiver'];


            $selectedGroups = [];
            // Courier additions:
            if(is_array($_POST['CourierAddition']))
                foreach($_POST['CourierAddition'] AS $item) {
                    $temp = CourierAdditionList::model()->findByPk($item);

                    if ($temp !== null) {
                        // select only first from group
                        if ($temp->group != '') {
                            if (isset($selectedGroups[$temp->group])) {
                                continue;
                            }
                            else {
                                $selectedGroups[$temp->group] = true;
                            }
                        }
                        array_push($courierAddition, $item);
                    }
                }

            if(!is_array($courierAddition))
                $courierAddition = [];

            $model->courierTypeInternal->addAdditions($courierAddition);

            // REPLACED _client_cod_value
//            if($model->_client_cod)
//                $model->courierTypeInternal->client_cod_value = $model->_client_cod_value;
//            else {
//                $model->_client_cod_value = NULL;
//                $model->courierTypeInternal->client_cod_value = NULL;
//            }


            if($model->packages_number)
                $model->package_weight = (double) $model->_package_weight_total / $model->packages_number;



            $currencyModel = PriceCurrency::model()->findByPk($_POST['currency']);

            if($currencyModel !== NULL)
                $currency = $currencyModel->id;

            Yii::app()->PriceManager->setCurrency($currency);
            $this->setPageState('currency', $currency);

            $model->courierTypeInternal->processPickupParams();
//

            // REPLACED _client_cod_value!
            if($model->cod_value > 0)
                $model->senderAddressData->_bankAccountRequired = true;
            else
                $model->senderAddressData->_bankAccountRequired = false;

            if(!$model->validate())
                $errors = true;

            if(!$model->courierTypeInternal->validate())
                $errors = true;


            if(!$model->senderAddressData->validate())
                $errors = true;

            if(!$model->receiverAddressData->validate())
                $errors = true;

            $this->setPageState('step1_model',$model->getAttributes(array_keys(array_merge($_POST['Courier_CourierTypeInternal'])))); // save previous form into form state
            $this->setPageState('step1_courierTypeInternal',$model->courierTypeInternal->getAttributes(array_merge(array_keys($_POST['CourierTypeInternal']),array('client_cod_value', '_package_wrapping')))); // save previous form into form state
            $this->setPageState('step1_senderAddressData', $model->senderAddressData->getAttributes(array_keys($_POST['CourierTypeInternal_AddressData']['sender']))); // save previous form into form state
            $this->setPageState('step1_receiverAddressData',$model->receiverAddressData->getAttributes(array_keys($_POST['CourierTypeInternal_AddressData']['receiver']))); // save previous form into form state
            $this->setPageState('courier_additions',$model->courierTypeInternal->_courier_additions);


            if(!$errors)
            {
                $userGroup = NULL;
                if(!Yii::app()->user->isGuest)
                    $userGroup = Yii::app()->user->getUserGroupId();


                if(!$model->courierTypeInternal->isCarriagePossible($userGroup)) {
                    $noCarraige = true;
                    $errors = true;
                }

                if(!$model->courierTypeInternal->isPickupPossibleForCountry($userGroup)) {
                    $noPickup = true;
                    $errors = true;
                }
            }

            if(!$errors) {
                $currencySymbol = Yii::app()->PriceManager->getCurrencySymbol();
                if (!$model->courierTypeInternal->priceTotal($currency))
                    $noPrice = true;

                $priceEach = Yii::app()->PriceManager->getFinalPrice($model->courierTypeInternal->priceEach($currency)) . ' ' . $currencySymbol;
                $priceTotal = Yii::app()->PriceManager->getFinalPrice($model->courierTypeInternal->priceTotal($currency)) . ' ' . $currencySymbol;
            }

        }

        $modelCourierAddition = $this->getPageState('courier_additions', []);
        $modelCourierAdditionList = CourierAdditionList::getAdditionsByCourier($model);

        $additionsHtml = $this->renderPartial('create/_additionList', array(
            'modelCourierAdditionList'=>$modelCourierAdditionList,
            'courierAddition'=>$modelCourierAddition,
            'currency' => $currency,
        ), true, false);


        $codCurrency = false;
        $deliveryOperator = $model->courierTypeInternal->getDeliveryOperator();

        $deliveryOperatorBrokerId = false;
        if($deliveryOperator)
        {
            $codCurrency = CourierCodAvailability::findCurrencyForOperatorAndCountry(CourierCodAvailability::OPERATOR_INTERNAL, $deliveryOperator, $model->receiverAddressData->country_id);
            if($codCurrency)
            {
                $codCurrency = CourierCodAvailability::getCurrencyNameById($codCurrency);
            }

            $deliveryOperatorBrokerId = CourierOperator::getBrokerById($deliveryOperator);
        }

        if($model->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR && $remoteZip = CourierDhlRemoteAreas::findZip($model->senderAddressData->zip_code, $model->senderAddressData->country_id, $model->senderAddressData->city)) {
            $value = $remoteZip->getPrice($currency);

            Yii::app()->user->setFlash('notice', Yii::t('courier', 'Nadawca znajduje się w strefie utrudnionego dostępu. Kosz paczki zostanie powiększony z tego powodu o {kwota}.', array('{kwota}' => '<strong>'.$value.' '.Yii::app()->PriceManager->getCurrencySymbol().'</strong>')));
        }

        if($remoteZip = CourierDhlRemoteAreas::findZip($model->receiverAddressData->zip_code, $model->receiverAddressData->country_id, $model->receiverAddressData->city)) {
            $value = $remoteZip->getPrice($currency);


            Yii::app()->user->setFlash('notice', implode('<br/>', array_filter([
                Yii::app()->user->getFlash('notice'),
                Yii::t('courier', 'Odbiorca znajduje się w strefie utrudnionego dostępu. Kosz paczki zostanie powiększony z tego powodu o {kwota}.', array('{kwota}' => '<strong>'.$value.' '.Yii::app()->PriceManager->getCurrencySymbol().'</strong>'))
            ])));
        }

        if($model->_client_cod > 0 && !$model->findCodCurrency())
        {
            $model->_client_cod = false;
            $model->cod_value = 0;
            $model->cod = false;
            $codIgnored = true;
            $errors = true;
        }

        if($errors)
        {
            $this->render('create/step1',
                [
                    'noPrice' => $noPrice,
                    'currency' => $currency,
                    'model' => $model,
                    'noPickup' => $noPickup,
                    'noCarriage' => $noCarraige,
                    'priceEach' => $priceEach,
                    'priceTotal' => $priceTotal,
                    'additionsHtml' => $additionsHtml,
                    'codCurrency' => $codCurrency,
                    'codIgnored' => $codIgnored,
                    'deliveryOperatorBrokerId' => $deliveryOperatorBrokerId,
                    'favList' => CHtml::listData(CourierSpPointFav::getPointsForUser(),'complexId', 'usefulName'),
                    'onValidation' => true,
                ]);
            return 'break';
        }
    }

    protected function create_step_summary()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');

        switch($lastStep)
        {
            case 1 :
                if($this->create_validate_step_1() == 'break')
                    return;
                break;
        }

        $currency = $this->getPageState('currency', false);

        if(!$currency)
            $currency = Yii::app()->PriceManager->getCurrency();
        else
            Yii::app()->PriceManager->setCurrency($currency);

        $model = new Courier_CourierTypeInternal();
        $model->courierTypeInternal = new CourierTypeInternal('summary');
        $model->courierTypeInternal->courier = $model;

        $model->senderAddressData = new CourierTypeInternal_AddressData();
//        $model->senderAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_SENDER);
        $model->receiverAddressData = new CourierTypeInternal_AddressData();
//        $model->receiverAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_RECEIVER);

        $model->attributes = $this->getPageState('step1_model',[]);
        $model->courierTypeInternal->attributes = $this->getPageState('step1_courierTypeInternal', []);
        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);

        $additions = $this->getPageState('courier_additions', []);

        $model->courierTypeInternal->addAdditions($additions);

        $model->user_id = Yii::app()->user->id;

        $priceEach = $model->courierTypeInternal->priceEach($currency);
        $priceTotal = $model->courierTypeInternal->priceTotal($currency);


        $this->setPageState('last_step', 'summary'); // save information about last step

        $this->render('create/summary',array(
            'currency' => $currency,
            'model'=>$model,
            'price' => $priceTotal,
            'price_each' => $priceEach,
        ));
    }

    protected function create_validate_summary()
    {
        $errors = false;

        $currency = $this->getPageState('currency', false);
        if(!$currency)
            $currency = Yii::app()->PriceManager->getCurrency();
        else
            Yii::app()->PriceManager->setCurrency($currency);

        $model = new Courier_CourierTypeInternal();
        $model->courierTypeInternal = new CourierTypeInternal('summary');

        $model->courierTypeInternal->courier = $model;

        $model->senderAddressData = new CourierTypeInternal_AddressData();
//        $model->senderAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_SENDER);
        $model->receiverAddressData = new CourierTypeInternal_AddressData();
//        $model->receiverAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_RECEIVER);

        $model->attributes = $this->getPageState('step1_model',[]);
        $model->courierTypeInternal->attributes = $this->getPageState('step1_courierTypeInternal', []);
        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);

        $additions = $this->getPageState('courier_additions', []);

        $model->courierTypeInternal->addAdditions($additions);

        $model->user_id = Yii::app()->user->id;

        $priceEach = $model->courierTypeInternal->priceEach($currency);
        $priceTotal = $model->courierTypeInternal->priceTotal($currency);

        $model->courierTypeInternal->regulations = $_POST['CourierTypeInternal']['regulations'];
        $model->courierTypeInternal->regulations_rodo = $_POST['CourierTypeInternal']['regulations_rodo'];
        if(!$model->courierTypeInternal->validate(array('regulations', 'regulations_rodo')))
            $errors = true;

        $model->courierTypeInternal->processPickupParams();

        if($errors)
        {
            $this->render('create/summary',array(
                'currency' => $currency,
                'model'=>$model,
                'price' => $priceTotal,
                'price_each' => $priceEach,
            ));
            return 'break';
        }

    }

    protected function create_finish($goBack = false)
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 'summary')
        {
            $currency = $this->getPageState('currency', false);
            if(!$currency)
                $currency = Yii::app()->PriceManager->getCurrency();
            else
                Yii::app()->PriceManager->setCurrency($currency);

            if($this->create_validate_summary() == 'break')
                return;



            $this->setPageState('last_step',6); // save information about last step

            $model = new Courier_CourierTypeInternal('insert');
            $model->courierTypeInternal = new CourierTypeInternal();
            $model->courierTypeInternal->courier = $model;
            $model->courierTypeInternal->attributes = $this->getPageState('step1_courierTypeInternal', []);

            $model->attributes = $this->getPageState('step1_model',array()); //get the info from step 1
            $model->receiverAddressData = new CourierTypeInternal_AddressData();
            $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);
            $model->senderAddressData = new CourierTypeInternal_AddressData();
            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
            $model->courierTypeInternal->addAdditions($this->getPageState('courier_additions',array()));


            // additional params:
//            $params = $this->getPageState('step4_courierTypeInternal_params', array());
//
//            foreach($params AS $key => $item)
//            {
//                $model->courierTypeInternal->$key = $item;
//            }

            // return order
            $model->user_id = Yii::app()->user->id;
            $model->courierTypeInternal->processPickupParams();

            $return = $model->saveNewPackageInternal();

            if(is_array($return) && $return['order'] instanceof Order)
            {
                Yii::app()->session['courierFormFinished'] = true;

                $order = $return['order'];
                $order->refresh();  // to get proper hash

                // OPTIONAL FAST FINALIZE WITH PAY ON INVOICE IF USER HAS THIS OPTION AND IT'S SET
                if(!Yii::app()->user->isGuest && Yii::app()->user->model->getFastFinalizeInternal() && Yii::app()->user->model->payment_on_invoice)
                {
                    if($order->fastFinalizeOrder(true))
                    {
                        if($goBack)
                            Yii::app()->user->setFlash('success', Yii::t('order','Zamówienie {no} zostało sfinalizowane! {list}', ['{no}' => CHtml::link('#'.$order->local_id, Yii::app()->createUrl('/order/view', ['urlData' => $order->hash]) ,['target' => '_blank', 'class' => 'btn btn-sm']), '{list}' => CHtml::link(Yii::t('order', 'Lista paczek'), Yii::app()->createUrl('/courier/courier/list', ['#' => 'list']) ,['target' => '_blank', 'class' => 'btn btn-sm'])]));

                        // get id's of just created packages
                        $_justCreated = [];
                        foreach($order->orderProduct AS $item)
                            array_push($_justCreated, $item->type_item_id);

                        self::addToListOfJustCreated($_justCreated);

                        if($goBack) {
                            $this->redirect(array('/courier/courier/create'));
                        } else {

                            $temp = $return['couriers'][0];

                            // to get proper Hash
                            $temp->refresh();

                            $this->redirect(array('/courier/courier/view', 'urlData' => $temp->hash, 'new' => true));
                        }

                        Yii::app()->end();
                    }
                }
                //

                Yii::app()->session['orderHash'] = $order->hash;
                $this->redirect(array('/order/finalizeOrder', 'urlData' => $order->hash));

            }
            throw new CHttpException(403, 'Wystapił nieznany błąd');


        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }

    public function actionAjaxGetPrice()
    {

        if(Yii::app()->request->isAjaxRequest) {
            $sender_country_id = $_POST['sender_country_id'];
            $receiver_country_id = $_POST['receiver_country_id'];
            $with_pickup = $_POST['with_pickup'];
            $weight = $_POST['weight'];
            $size_l = $_POST['size_l'];
            $size_w = $_POST['size_w'];
            $size_d = $_POST['size_d'];
            $packages_number = $_POST['packages_number'];
            $currency = $_POST['currency'];

            $force_user_id = $_POST['force_user_id'];

            $sender_zip_code = $_POST['sender_zip_code'];
            $receiver_zip_code = $_POST['receiver_zip_code'];
            $sender_city = $_POST['sender_city'];
            $receiver_city = $_POST['receiver_city'];

            $noTax = $_POST['noTax'] ? true : false;

            if($currency == '')
                $currency = Yii::app()->PriceManager->getCurrency();
            else
                Yii::app()->PriceManager->setCurrency($currency);

            if($with_pickup == 'false') {
                // PICKUP_MOD / 14.09.2016
//                $with_pickup = false;
                $with_pickup = CourierTypeInternal::WITH_PICKUP_NONE;
            }

            $courier = new Courier;

            if(intval($force_user_id) > 0)
            {
                $user_id = $force_user_id;
            } else
                $user_id = false;


            if(!$user_id)
                $courier->user_id = Yii::app()->user->id;
            else
                $courier->user_id = $user_id;

            $courier->packages_number = $packages_number;

            $courier->package_weight = $weight;
            $courier->package_size_l = $size_l;
            $courier->package_size_w = $size_w;
            $courier->package_size_d = $size_d;

            $courierTypeInternal = new CourierTypeInternal();
            $courierTypeInternal->with_pickup = $with_pickup;


            $courier->senderAddressData = new AddressData();
            $courier->receiverAddressData = new AddressData();

            $courier->senderAddressData->country_id = $sender_country_id;
            $courier->receiverAddressData->country_id = $receiver_country_id;

            $courier->senderAddressData->zip_code = $sender_zip_code;
            $courier->receiverAddressData->zip_code = $receiver_zip_code;

            $courier->senderAddressData->city = $sender_city;
            $courier->receiverAddressData->city = $receiver_city;


            $courier->courierTypeInternal = $courierTypeInternal;
            $courierTypeInternal->courier = $courier;

            $currencySymbol = Yii::app()->PriceManager->getCurrencyCode();

            $priceEach = Yii::app()->PriceManager->getFinalPrice($courier->courierTypeInternal->priceEach($currency, $user_id), $noTax).' '.$currencySymbol;
            $priceTotal = Yii::app()->PriceManager->getFinalPrice($courier->courierTypeInternal->priceTotal($currency, $user_id), $noTax).' '.$currencySymbol;

            echo CJSON::encode([
                    'each' => $priceEach,
                    'total' => $priceTotal
                ]
            );
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }
    }

    public function actionAjaxCheckIfAvailable()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $sender_country_id = $_POST['sender_country_id'];
            $receiver_country_id = $_POST['receiver_country_id'];

            $weight = $_POST['weight'];
            $size_l = $_POST['size_l'];
            $size_w = $_POST['size_w'];
            $size_d = $_POST['size_d'];
            $with_pickup = $_POST['with_pickup'];

            $courierTypeInternal = new CourierTypeInternal();
            $courierTypeInternal->with_pickup = $with_pickup;
            $courier = new Courier;
            $courier->senderAddressData = new AddressData();
            $courier->receiverAddressData = new AddressData();

            $courier->package_weight = $weight;
            $courier->package_size_l = $size_l;
            $courier->package_size_d = $size_d;
            $courier->package_size_w = $size_w;

            $courier->senderAddressData->country_id = $sender_country_id;
            $courier->receiverAddressData->country_id = $receiver_country_id;

            $courier->courierTypeInternal = $courierTypeInternal;
            $courierTypeInternal->courier = $courier;

            $userGroup = NULL;
            if(!Yii::app()->user->isGuest)
                $userGroup = Yii::app()->user->getUserGroupId();

            $result = $courierTypeInternal->isCarriagePossible($userGroup);

            $deliveryBrokerId = false;
            if($result)
            {
                $deliveryOperator = $courierTypeInternal->getDeliveryOperator();
                $deliveryBrokerId = CourierOperator::getBrokerById($deliveryOperator);
            }

            echo CJSON::encode([
                'result' => $result,
                'deliveryBrokerId' => $deliveryBrokerId,
            ]);
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }
    }

    public function actionAjaxCheckIfPickupAvailable()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $sender_country_id = $_POST['sender_country_id'];

            $weight = $_POST['weight'];
            $size_l = $_POST['size_l'];
            $size_w = $_POST['size_w'];
            $size_d = $_POST['size_d'];

            $courierTypeInternal = new CourierTypeInternal();
// PICKUP_MOD / 14.09.2016
//            $courierTypeInternal->with_pickup = true;
            $courierTypeInternal->with_pickup = CourierTypeInternal::WITH_PICKUP_REGULAR;
            $courier = new Courier;
            $courier->senderAddressData = new AddressData();
            $courier->senderAddressData->country_id = $sender_country_id;

            $courier->courierTypeInternal = $courierTypeInternal;
            $courierTypeInternal->courier = $courier;

            $courier->package_weight = $weight;
            $courier->package_size_l = $size_l;
            $courier->package_size_d = $size_d;
            $courier->package_size_w = $size_w;

            $userGroup = NULL;
            if(!Yii::app()->user->isGuest)
                $userGroup = Yii::app()->user->getUserGroupId();

            $result = $courierTypeInternal->isPickupPossibleForCountry($userGroup);

            echo CJSON::encode($result);
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }
    }

    public function actionAjaxGetAdditions()
    {
        if(Yii::app()->request->isAjaxRequest) {

            $sender_country_id = $_POST['sender_country_id'];
            $sender_zip_code = $_POST['sender_zip_code'];
            $receiver_country_id = $_POST['receiver_country_id'];
            $receiver_zip_code = $_POST['receiver_zip_code'];
            $with_pickup = $_POST['with_pickup'];
            $weight = $_POST['weight'];
            $modelCourierAddition = $_POST['additions'];
            $currency = $_POST['currency'];

            if($currency == '')
                $currency = Yii::app()->PriceManager->getCurrency();
            else
                Yii::app()->PriceManager->setCurrency($currency);

            if(!is_array($modelCourierAddition))
                $modelCourierAddition = [];

            $model = new Courier_CourierTypeInternal();
            $model->package_weight = $weight;

            $modelSender = new CourierTypeInternal_AddressData();
            $modelSender->country_id = $sender_country_id;
            $modelSender->zip_code = $sender_zip_code;
            $model->senderAddressData = $modelSender;


            $modelReceiver = new CourierTypeInternal_AddressData();
            $modelReceiver->country_id = $receiver_country_id;
            $modelReceiver->zip_code = $receiver_zip_code;
            $model->receiverAddressData = $modelReceiver;

            $model->courierTypeInternal = new CourierTypeInternal();
            $model->courierTypeInternal->with_pickup = $with_pickup;
            $model->courierTypeInternal->courier = $model;

            $modelCourierAdditionList = CourierAdditionList::getAdditionsByCourier($model);


            $html = $this->renderPartial('create/_additionList',array(
                'currency' => $currency,
                'modelCourierAdditionList'=>$modelCourierAdditionList,
                'courierAddition'=>$modelCourierAddition,
            ), true, false);

            echo CJSON::encode($html);
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }
    }

    public function actionAjaxGetCodCurrency()
    {
        if(Yii::app()->request->isAjaxRequest) {

            $receiver_country_id = $_POST['receiver_country_id'];
            $sender_country_id = $_POST['sender_country_id'];
            $weight = $_POST['weight'];

            $model = new Courier_CourierTypeInternal();
            $model->package_weight = $weight;

            $modelReceiver = new CourierTypeInternal_AddressData();
            $modelReceiver->country_id = $receiver_country_id;
            $model->receiverAddressData = $modelReceiver;

            $modelSender = new CourierTypeInternal_AddressData();
            $modelSender->country_id = $sender_country_id;
            $model->senderAddressData = $modelSender;

            $model->courierTypeInternal = new CourierTypeInternal();
            $model->courierTypeInternal->courier = $model;

            $available = false;
            $currencyName = '';

            $deliveryOperator = $model->courierTypeInternal->getDeliveryOperator();
            if($deliveryOperator)
            {
                $codCurrency = CourierCodAvailability::findCurrencyForOperatorAndCountry(CourierCodAvailability::OPERATOR_INTERNAL, $deliveryOperator, $receiver_country_id);

                if($codCurrency)
                {
                    $available = true;
                    $currencyName = CourierCodAvailability::getCurrencyNameById($codCurrency);
                }
            }

            $response = [
                'available' => $available,
                'currencyName' => $currencyName,
            ];

            echo CJSON::encode($response);
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }
    }



    public function actionTt($urlData = null)
    {

        $this->redirect(Yii::app()->createUrl('/site/tt', array('urlData' => $urlData)));
        exit;
    }


    public function actionView($urlData, $new = false)
    {
        $this->panelLeftMenuActive = 100;


        /* @var $model Courier */

        $model = Courier::model()->find('hash=:hash', array(':hash' => $urlData));

        if ($model === null)
            throw new CHttpException(404);


        if ($model->user_id !== Yii::app()->user->id)
        {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Zaloguj się na konto powiązane z tą paczką, aby zobaczyć jej szczegóły!'),
                'header' => 'Courier',

            ));
            exit;
        }

//        if ($model->getType() == Courier::TYPE_INTERNAL_OOE) {
//            $this->redirect(Yii::app()->createUrl('/courier/courierOoe/view', array('urlData' => $urlData)));
//            exit;
//        }

        $actions = [];

        if($new OR !$model->isLabelArchived() && $model->isLabelGeneratingAvailableBaseOnStat() && !in_array($model->user_cancel, [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_WARNED])
            && ((($model->courierTypeInternalSimple !== NULL OR $model->courierTypeInternal != NULL OR $model->courierTypeDomestic != NULL OR $model->courierTypeOoe != NULL)
                    AND ($model->courier_stat_id != CourierStat::PACKAGE_PROCESSING AND $model->courier_stat_id != CourierStat::NEW_ORDER)
                )
                OR $model->courierTypeExternal != NULL OR $model->courierTypeU != NULL OR $model->courierTypeReturn != NULL OR $model->courierTypeExternalReturn != NULL OR $model->courierTypeReturn != NULL)
        ) {

            if(!$model->isLabelArchived()) {
//                if (!Yii::app()->user->isGuest && Yii::app()->user->model->getAllowCourierInternalExtendedLabel() && $model->courierTypeInternal)
//                    $actions[] = $this->renderPartial('actions/generate_carriage_ticket_internal_extended', array('model' => $model), true);
//                else
                $actions[] = $this->renderPartial('actions/generate_carriage_ticket', array('model' => $model), true);


                if(!in_array($model->receiverAddressData->country_id, CountryList::getUeList(true)))
                    $actions[] = $this->renderPartial('actions/generate_customs_packet', array('model' => $model), true);
            }
        }

        if($model->stat_map_id == StatMap::MAP_DELIVERED && !$model->isLabelArchived())
        {
            /* @ var $labels CourierLabelNew[] */
            $labels = $model->courierLabelNew;

            if(is_array($labels) && S_Useful::sizeof($labels) > 1)
            {
                foreach($labels AS $key => $item)
                {
                    if($item->_internal_mode == CourierLabelNew::INTERNAL_MODE_FIRST)
                    {
                        unset($labels[$key]);
                        break;
                    }
                }
            }

            $label = array_pop($labels);

            if($label && $label->isOperatorWithPod()) {
                $actions[] = $this->renderPartial('actions/epod', array('hash' => $label->hash), true);
            }
        }

        if(!in_array($model->courier_stat_id, [CourierStat::CANCELLED, CourierStat::CANCELLED_PROBLEM, CourierStat::CANCELLED_USER]) AND !in_array($model->user_cancel, [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_WARNED]))
            $actions[] = $this->renderPartial('actions/generate_ack_card', array('model' => $model), true);


        $actions[] = $this->renderPartial('actions/generate_status_conf_card', array('model' => $model), true);

        if($model->isClonable())
            $actions[] = $this->renderPartial('actions/clone', array('model' => $model), true);

//        if(Yii::app()->user->getModel()->premium && $model->courierTypeInternalSimple !== NULL)
//            if($model->courier_stat_id == CourierStat::NEW_ORDER)
//                $actions .= $this->renderPartial('actions/cancel', array('model' => $model), true);

        if($model->isUserCancelByUserPossible())
            $actions[] = $this->renderPartial('actions/cancelByUser', array('model' => $model), true);

        if($model->isUserComplaintPossible())
            $actions[] = $this->renderPartial('actions/complaint', array('model' => $model), true);

        if($model->getReturnPackageModel())
            $actions[] = $this->renderPartial('actions/return_package', array('model' => $model->getReturnPackageModel()), true);
        else if(in_array($model->getType(), [Courier::TYPE_INTERNAL, Courier::TYPE_U]))
            $actions[] = $this->renderPartial('actions/create_return_package', array('model' => $model), true);

        if($model->courierTypeReturn && $model->courierTypeReturn->base_courier_id)
            $actions[] = $this->renderPartial('actions/return_package_parent', array('model' => $model->getReturnPackageParentModel()), true);

        $this->render('view', array(
            'model' => $model,
            'actions' => $actions,
            'new' => $new,
        ));

    }

    public function actionGetEpod()
    {
        /* @var $model CourierLabelNew */
        if(isset($_POST['getEpod']))
        {
            $model = CourierLabelNew::model()->findByAttributes(['hash' => $_POST['hash']]);

            if($model && $model->isOperatorWithPod()) {
                $epod = $model->getEpod();
                if ($epod) {
                    header("Content-Disposition: attachment; filename=\"epod_".$model->courier->local_id.".pdf\"");
                    header("Content-Type: application/x-pdf;");
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    echo $epod;
                    Yii::app()->end();
                }
            }
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Nie udało się pobrać dokumentu ePOD!'));
            $this->redirect(['/courier/courier/view', 'urlData' => $model->courier->hash]);

        }

        throw new CHttpException(404);

    }

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// GRIDVIEW START

    public function actionList()
    {
        $this->panelLeftMenuActive = 101;


//        if(Yii::app()->NPS->isNewPanelSet())
//            $this->layout = '//layouts/panel-full';

        $preFilter = [];
        $preModels = false;
        if(isset($_POST['preFilter']) && $_POST['preFilter'] != '' OR isset($_POST['preFilterClear'])) {

            if(isset($_POST['preFilterClear']))
            {
                $preFilter = [];
            }
            else
            {
                $preFilter = $_POST['preFilter'];
                $preFilter = explode(PHP_EOL, $preFilter);

                foreach ($preFilter AS $key => $item)
                    $preFilter[$key] = trim($item);

                $preFilter = array_filter($preFilter);

                $preModels = CourierExternalManager::findCourierIdsByRemoteId($preFilter, true);
            }
        }


        $this->render('list', array(
            'preFilter' => implode(PHP_EOL, $preFilter),
            'preModels' => $preModels,
        ));
    }

    /**
     * Function to memorize recently added pages in order to highlight them in GridView
     *
     * @param array $packagesIds Ids of recently created packages (ids of Courier models)
     */
    protected static function addToListOfJustCreated(array $packagesIds)
    {

        $justCreated = Yii::app()->session['internalJustCreated'];
        $justCreated = CMap::mergeArray($justCreated, $packagesIds);
        Yii::app()->session['internalJustCreated'] = $justCreated;
    }

    /**
     * Function to check, if this package is memorized as recently added to highlight it in GridView
     *
     * @param int $packageId Id of package (id of Courier model)
     */
    protected static function checkIfJustCreated($packageId)
    {
        if(in_array($packageId, Yii::app()->session->get('internalJustCreated', [])))
            return true;
        else
            return false;
    }

    /**
     * Clear list of memorized recently added packages.
     */
    protected static function clearJustCreatedPackagesList()
    {
        Yii::app()->session['internalJustCreated'] = [];
    }

    public static function _cancelColumnForGridView($data)
    {

        Yii::import('application.components.yii2adaptors.FreshdeskYii2Adaptor');
        Yii::import('application.components.yii2adaptors.Yii2UrlManager');

        if($data->getType() == Courier::TYPE_EXTERNAL_RETURN)
            return '';

        if($data->courier_stat_id == CourierStat::CANCELLED_PROBLEM)
        {
            return '<span title="'.Yii::t('courier', 'Paczka została automatycznie anulowana z powodue problemu z zamówieniem').'" style="font-weight: bold; text-decoration: italic; color: darkviolet; cursor: help;">P</span>';
        }
        else if($data->isUserCancelByUserPossible())
        {
            return CHtml::link("<span class=\"glyphicon glyphicon-remove\"></span>",array("/courier/courier/cancelByUser", "urlData" => $data->hash), ["target" => "_blank", "onclick" => "return confirm(\"".Yii::t("site", "Czy jesteś pewien?")."\")", "title" => Yii::t("courier", "Anuluj paczkę")]);
        }
        else if($data->isUserComplaintPossible())
        {
            if(in_array(Yii::app()->user->id, [8,10,2538]))
                return '<a href="'.Yii2UrlManager::createUrl('support/ticket/create?type='.(FreshdeskYii2Adaptor::TYPE_COURIER).'&hash='.$data->hash).'">'.(FreshdeskYii2Adaptor::checkIfHasTicket(FreshdeskYii2Adaptor::TYPE_COURIER, $data->id) ? '[#]':'[FD]').'</a>';
            else
                return CHtml::link("<strong>[Z]</strong>",array("/user/complaint", "hash" => $data->hash, "type" => UserComplaintForm::WHAT_COURIER), ["target" => "_blank", "title" => Yii::t("courier", "Reklamuj paczkę")]) ;
        } else {
            if($data->user_cancel == Courier::USER_CANCELL_COMPLAINED)
                return "<strong style=\"text-decoration: underline; color: red;\" title=\"".Yii::t("courier", "Zgłoszono reklamację tej paczki")."\">R</strong>";
        }
    }

    public static function _createReturnColumnForGridView($data)
    {

        if(!in_array($data->getType(), [Courier::TYPE_INTERNAL, Courier::TYPE_U]) OR $data->getReturnPackageModel() !== NULL)
            return '';
        else if(Yii::app()->user->model->getCourierReturnAvailable() && CourierTypeReturn::isAutomaticReturnAvailable($data->receiver_country_id))
        {
            return CHtml::link("<span class=\"glyphicon glyphicon-transfer\"></span>",array("/courier/courierReturn/createFor", "hash" => $data->hash), ["target" => "_blank", "onclick" => "return confirm(\"".Yii::t("site", "Czy jesteś pewien?")."\")", "title" => Yii::t("courier", "Wygeneruj etykietę zwrotną dla tej paczki")]);
        }
    }


    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewCouriergrid($preModels = false){

        // for transation bot to prepare this text
        $temp = Yii::t('courier', 'Nowa paczka typu "{internal}"', ['{internal}' => Courier::getTypesNames()[Courier::TYPE_INTERNAL]]);

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new _CourierGridViewUser('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierGridViewUser']))
            $model->setAttributes($_GET['CourierGridViewUser']);

        if(is_array($preModels))
        {
            if(S_Useful::sizeof($preModels))
            {
                $preIds = CHtml::listData($preModels, 'id', 'id');
                $model->_multiIds = implode(',', $preIds);
            }
            else
                $model->_multiIds = -1;
        }

        $countries = [];
        $countries[-1] = '';


//        $ueList = CountryList::getUeList(false);
//        $ueList = implode( ',', $ueList);

        $countries += CHtml::listData(CountryList::model()->with('countryListTr')->findAll(['order' => '(CASE WHEN t.id = 1 THEN 0 ELSE 1 END) ASC, countryListTr.name ASC']),'id', 'trName');
//        $countries += CHtml::listData(CountryList::model()->with('countryListTr')->findAll(['order' => 'FIELD(t.id,'.$ueList.','.CountryList::COUNTRY_PL.') DESC, countryListTr.name ASC']),'id', 'trName');
        $countries[-1] = '<'.Yii::t('site','oprócz').' '.$countries[1].'>';

        $pageSize = Yii::app()->user->getState('pageSize', 50);


//$this->widget('zii.widgets.grid.CGridView', array(
//$this->widget('yiiwheels.widgets.grid.WhGridView', array(
//        $this->widget('ext.selgridview.BootSelGridView', array((

        $withCod = Yii::app()->user->model->hasUserCourierCodItems();

        $headers = [];


        $colSpan = 5;
        if(Yii::app()->user->getModel()->getShowCourierInternalOperatorsData() && Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_EXTERNAL))
            $colSpan++;

        if(Yii::app()->user->getModel()->getCourierRefPrefix())
            $colSpan++;

        if(Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_CONTENT))
            $colSpan++;

        if(Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_VALUE))
            $colSpan++;

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_COURIER_NOTE)) {
            $colSpan++;
            $colSpan++;
        }


        $headers[] = array(
            'text'=> '',
            'colspan'=> $colSpan,
            'options'=> []
        );
        $headers[] = array(
            'text'=>Yii::t('courier', 'Data'),
            'colspan'=>2,
            'options'=>[]
        );

        if(Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_SENDER)) {

            $headers[] = array(
                'text'=>Yii::t('courier', 'Nadawca'),
                'colspan'=>2,
                'options'=>[]
            );

        }

        $headers[] = array(
            'text' => Yii::t('courier', 'Odbiorca'),
            'colspan' => 2,
            'options' => []
        );


        if($withCod && Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_COD))
            $headers[] = array(
                'text'=>Yii::t('courier', 'COD'),
                'colspan'=>5,
                'options'=>[]
            );

        $headers[] = array(
            'text'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10 ,20=>20 ,50=>50 , 100=>100, 250 => 250, 500 => 500),array(
                'onchange'=>"$.fn.yiiGridView.update('couriergrid',{ data:{pageSize: $(this).val() }})",
            )),

            'colspan'=>7,
            'options'=>[]
        );

        $colspanListMore = 0;
        foreach($headers AS $item)
        {
            if($item['colspan'])
                $colspanListMore += $item['colspan'];
            else
                $colspanListMore++;
        }

        $columns = [];

        $columns[] = array(
            'class'=>'GridViewIndexColumn',
            'header' => '#',
            'htmlOptions' => [
                'width' => '37',
                'class' => 'text-right',
            ],
        );


        $columns[] = array(
            'name'=>'local_id',
            'header'=>Yii::t('courier','#ID'),
            'type'=>'raw',
            'value'=>'CHtml::link($data->local_id,array("/courier/courier/view/", "urlData" => $data->hash), ["target" => "_blank"])',
            'class'=>'DataColumn',
            'evaluateHtmlOptions'=>true,
            'htmlOptions' => [
                'class' => '(CourierController::checkIfJustCreated($data->id) ? "gv-courier-just-created" : "")." ".(CourierStat::getGvItemCssClassByStatMapId($data->stat_map_id)." button-column button-column-fix")',
                'title' => 'CourierController::checkIfJustCreated($data->id) ? Yii::t(\'courier\', \'Nowa paczka typu "{internal}"\', [\'{internal}\' => Courier::getTypesNames()[Courier::TYPE_INTERNAL]]) : ""',
                'width' => '130',
            ],
        );


        if(Yii::app()->user->getModel()->getShowCourierInternalOperatorsData() && Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_EXTERNAL))
            $columns[] = array(
                'name' => '__extId',
                'value' => '$data->listExternalIdsByCourierLabelNew()',
                'htmlOptions' => [
                    'width' => '170',
                    'class' => 'text-center'
                ],
                'header'=>Yii::t('courier','Zew. numery'),
            );

        if(Yii::app()->user->getModel()->getCourierRefPrefix())
            $columns[] = array(
                'name' => 'ref',
                'htmlOptions' => [
                    'width' => '170',
                    'class' => 'text-center'
                ],
            );

        $columns[] = array(
            'header'=>Yii::t('courier','No'),
            'type'=>'raw',
            'value'=> '$data->getFamilyMemberNumber()."/".$data->packages_number',
            'filter' => false,
            'htmlOptions' => [
                'width' => '29',
                'class' => 'text-center'
            ],
        );


        $columns[] = array(
            'name' => '__type',
            'header'=>Yii::t('courier','Type'),
            'type'=>'raw',
            'value'=> '$data->getTypeName(true)',
            'filter' => Courier::getTypesNames(),
            'htmlOptions' => [
                'width' => '70',
                'class' => 'text-center'
            ],
        );
        $columns[] = array(
            'name'=>'stat_map_id',
            'type'=>'raw',
            'filter'=>StatMap::mapNameList(),
            'header' => Yii::t('courier','Status'),
            'value'=>'StatMap::getMapNameById($data->stat_map_id)',
            'htmlOptions' => [
                'width' => '170',
                'class' => 'text-center'
            ],
        );


        if(Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_CONTENT)) {

            $columns[] = array(
                'name' => 'package_content',
                'value' => '($data->package_content)',
//            'header'=>Yii::t('courier','Aktualizacji'),
                'htmlOptions' => [
                    'width' => '120',
                    'class' => 'text-center',
//                'style' => 'overflow: hidden'
                ],
            );

        }

        if(Yii::app()->user->getModel()->getPostalGridViewSettings(User::GRIDVIEW_COURIER_NOTE))
        {
            $columns[] = [
                'name' => 'note1',
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            ];

            $columns[] = [
                'name' => 'note2',
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            ];
        }


        if(Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_VALUE)) {

            $columns[] = array(
                'name' => 'package_value',
                'value' => 'S_Price::formatPrice($data->package_value)." ".$data->getPackageValueCurrency()',
//            'header'=>Yii::t('courier','Aktualizacji'),
                'htmlOptions' => [
                    'width' => '120',
                    'class' => 'text-center',
//                'style' => 'overflow: hidden'
                ],
                'footer'=> _CourierGridViewUser::formatTotalValueArray($model->getTotals('package_value',$model->search()->getKeys(), 'value_currency')),
            );

        }


        $columns[] = array(
            'name'=>'date_entered',
            'value' => 'substr($data->date_entered, 0, 10)',
            'header'=>Yii::t('courier','Utworzenia'),
            'htmlOptions' => [
                'width' => '80',
                'class' => 'text-center'
            ],
        );
        $columns[] = array(
            'name'=>'date_updated',
            'value' => 'substr($data->date_updated, 0, 10)',
            'header'=>Yii::t('courier','Aktualizacji'),
            'htmlOptions' => [
                'width' => '80',
                'class' => 'text-center'
            ],
        );

        if(Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_SENDER)) {

            $columns[] = array(
                'name' => '__sender_country_id',
                'header' => Yii::t('courier', 'Country'),
                'value' => '$data->senderAddressData->country0->code',
                'filter' => $countries,
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            );
            $columns[] = array(
                'name' => '__sender_usefulName',
                'header' => Yii::t('courier', 'Name'),
                'value' => '$data->senderAddressData->getUsefulName(true)',
                'htmlOptions' => [
                    'width' => '170',
                    'class' => 'text-center'
                ],
            );

        }

        $columns[] = array(
            'name'=>'__receiver_country_id',
            'header'=>Yii::t('courier','Country'),
            'value'=>'$data->receiverCountry->code',
            'filter'=> $countries,
            'htmlOptions' => [
                'width' => '50',
                'class' => 'text-center'
            ],
        );
        $columns[] = array(
            'name' => '__receiver_usefulName',
            'header' => Yii::t('courier','Name'),
            'value' => '$data->receiverAddressData->getUsefulName(true)',
            'htmlOptions' => [
                'width' => '170',
                'class' => 'text-center'
            ],
        );

        if($withCod && Yii::app()->user->getModel()->getCourierGridViewSettings(User::GRIDVIEW_COURIER_COD)) {
            $columns[] = array(
                'name' => 'cod',
                'header' => Yii::t('courier', 'COD'),
                'filter' => [0 => 'Nie', 1 => 'Tak'],
                'value' => '$data->cod ? "tak" : "-"',
                'htmlOptions' => [
                    'width' => '60',
                    'class' => 'text-center'
                ],
            );

            $columns[] = array(
                'name' => 'cod_value',
                'header' => Yii::t('courier', 'Kwota'),
                'value' => '$data->cod ? S_Price::formatPrice($data->cod_value) : "-"',
                'htmlOptions' => [
                    'width' => '70',
                    'class' => 'text-right'
                ],
                'footer'=> _CourierGridViewUser::formatTotalValueArray($model->getTotals('cod_value',$model->search()->getKeys(), 'cod_currency')),
            );

            $columns[] = array(
                'name' => 'cod_currency',
                'header' => Yii::t('courier', 'Waluta'),
                'value' => '$data->cod ? $data->cod_currency : "-"',
                'filter' => CourierCodAvailability::getCurrencyList(),
                'htmlOptions' => [
                    'width' => '50',
                    'class' => 'text-center'
                ],
            );

            $columns[] = array(
                'name' => '_cod_settled',
                'header' => Yii::t('courier', 'Rozliczone'),
                'filter' => [-1 => Yii::t('courierCod', 'Nie'), CourierCodSettled::TYPE_OK => Yii::t('courierCod', 'Tak'), CourierCodSettled::TYPE_CANCELLED => Yii::t('courierCod', 'Anulowane')],
                'value' => '$data->courierCodSettled ? $data->courierCodSettled->getTypeNameShort() : "-"',
                'htmlOptions' => [
                    'width' => '80',
                    'class' => 'text-center'
                ],
            );

            $columns[] = array(
                'name' => '_cod_settled_date',
                'header' => Yii::t('courier', 'Kiedy'),
                'value' => '$data->courierCodSettled ? $data->courierCodSettled->settle_date : "-"',
                'htmlOptions' => [
                    'width' => '80',
                    'class' => 'text-center'
                ],
            );
        }

        $columns[] = array(
            'class' => 'MineWhRelationalColumn',
            'columns_no' => $colspanListMore,
            'name' => 'subGrid',
            'url' => $this->createUrl('/courier/courier/listMore'),
            'value' => '"<span class=\"glyphicon glyphicon-save\"></span>"',
            'type' => 'raw',
            'filter' => false,
            'header' => '',
            'cssClass' => 'gridview-show-more',
            'afterAjaxUpdate' => 'js:function(tr,rowid,data){
                    //$(tr).hide();
                    //$(\'.gridview-dont-hover\').not(tr).hide();
                    $(tr).addClass(\'gridview-dont-hover\');
                         $(tr).fadeIn();
                         $(\'[data-popup-me]\').on(\'click\', function(e){
                            e.preventDefault();
                            window.open($(this).attr(\'href\'),$(this).attr(\'data-popup-me\'), \'height=200,width=850,resizable=yes,scrollbars=yes\');
                         });
                    }',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix',
                "title" => Yii::t("courier", "Rozwiń szczegóły poniżej")
            ],
        );
        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value'=>'CHtml::link("<span class=\"glyphicon glyphicon-new-window\"></span>",array("/courier/courier/view/", "urlData" => $data->hash), ["target" => "_blank", "title" => Yii::t("courier", "Pokaż szczegóły w nowym oknie")])',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix'
            ],
        );
        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value'=>'$data->isClonable() ? CHtml::link("<span class=\"glyphicon glyphicon-duplicate\"></span>",array("/courier/courier/clone/", "urlData" => $data->hash), ["target" => "_blank", "title" => Yii::t("courier", "Klonuj paczkę")]) : ""',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix'
            ],
        );

        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value'=> 'CourierController::_createReturnColumnForGridView($data)',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix'
            ],
        );



        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value'=> 'CourierController::_cancelColumnForGridView($data)',
            'htmlOptions' => [
                'width' => '28',
                'class' => 'text-center button-column button-column-fix'
            ],
        );
        $columns[] = array(
            'type' => 'raw',
            'header' => '',
            'value'=>'$data->ack_generated ? "<span style=\"cursor: help;\" class=\"glyphicon glyphicon-print\" title=\"".substr($data->ack_generated,0, 16)." - ".Yii::t("courier", "data wygenerowania potwierdzenia:")."\"></span>" : ""',

            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center button-column button-column-fix'
            ],
        );

        $columns[] = array(
            'class'=>'MyCheckBoxColumn',
            'id'=>'selected_rows',
            'htmlOptions' => [
                'width' => '20',
                'class' => 'text-center'
            ],
            'disabled' => 'in_array($data->courier_stat_id, [CourierStat::CANCELLED, CourierStat::CANCELLED_USER, CourierStat::NEW_ORDER, CourierStat::PACKAGE_PROCESSING])',
        );


        $columns[] = [
            'name' => '_multiIds',
            'htmlOptions'=>array('style'=>'display:none;'),
            'headerHtmlOptions'=>array('style'=>'display:none;'),
            'filterHtmlOptions'=>array('style'=>'display:none;'),
        ];

        $columns[] = [
            'name' => '_hideTypeReturn',
            'htmlOptions'=>array('style'=>'display:none;'),
            'headerHtmlOptions'=>array('style'=>'display:none;'),
            'filterHtmlOptions'=>array('style'=>'display:none;'),
        ];

        $columns[] = [
            'name' => '_hideCancelled',
            'htmlOptions'=>array('style'=>'display:none;'),
            'headerHtmlOptions'=>array('style'=>'display:none;'),
            'filterHtmlOptions'=>array('style'=>'display:none;'),
        ];

        echo '<input type="hidden" class="grid-data"/>';
        $this->widget('SPGridViewNoGet', array(
            'id' => 'couriergrid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>2,
//            'enableHistory' => true,
            'selectionChanged'=>'function(id){ checkNumberOfChecks(id); }',
            'beforeAjaxUpdate' => 'function(id,options){$(\'.gridview-overlay\').show();}',
            'afterAjaxUpdate' => 'function(id,data){    

             var de = $(\'input[name="CourierGridViewUser[date_entered]"]\').val();
             de = de.split("|"); 
             
             var de_start = false;
             var de_end = false;
             if($.isArray(de) && de.length == 2)
             {
               de_start = de[0];
               de_end = de[1];
             }    
             
             var du = $(\'input[name="CourierGridViewUser[date_updated]"]\').val();
             du = du.split("|"); 
             
             var du_start = false;
             var du_end = false;
             if($.isArray(du) && du.length == 2)
             {
               du_start = du[0];
               du_end = du[1];
             }    
                  
            $(\'.gridview-overlay\').hide();
          setDataRange($(\'input[name="CourierGridViewUser[date_entered]"]\'), de_start, de_end);
          setDataRange($(\'input[name="CourierGridViewUser[date_updated]"]\'), du_start, du_end);
          }',
//    'afterAjaxUpdate' => "function(id,data){  $('[name=\"selected_rows[]\"]').on('click', function(){
//        checkNumberOfChecks($(this));
//    });
//      }",
            //'rowCssClassExpression'=>'$data->stat?"row-open":"row-closed"',
            'template'=>"{pager}\n{summary}\n{items}\n{summary}\n{pager}",
            'htmlOptions' => ['style' => 'table-layout: fixed'],
            'addingHeaders' => array(
                $headers,
            ),
            'hiddenColumns' => [
                '_multiIds',
                '_hideTypeReturn',
                '_hideCancelled',
            ],
            'rowCssClassExpression' => '
                ( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) .
                ( in_array($data->courier_stat_id, [CourierStat::CANCELLED,CourierStat::CANCELLED_USER, CourierStat::NEW_ORDER, CourierStat::PACKAGE_PROCESSING]) ? " disabled" : null )
            ',
            'columns' =>
                $columns,

        ));

        self::clearJustCreatedPackagesList();

    }

    public function actionListMore()
    {
        if(Yii::app()->request->isAjaxRequest) {

            $model = Courier::model()->with('senderAddressData')->with('receiverAddressData')->findByPk($_GET['id']);

            if(Yii::app()->user->isGuest OR $model->user_id != Yii::app()->user->id)
                throw new Exception(403);


            $html = $this->renderPartial('_listMore',[
                'model' => $model,
            ], true);

            echo $html;

        }
    }


// GRIDVIEW END
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

    public function actionListPremium()
    {
        $this->panelLeftMenuActive = 100;

        if(!Yii::app()->user->getModel()->isPremium)
            throw new CHttpException(403);

        $model = new _CourierGridViewUserPremium('search');

        $model->unsetAttributes();

        if (isset($_GET['CourierGridViewUserPremium']))
            $model->setAttributes($_GET['CourierGridViewUserPremium']);

        $this->render('listPremium', array(
            'model' => $model,
        ));
    }

    /**
     * Fill form with cloned package data
     *
     * @param Courier $model
     */
    protected function _clone_external_fill()
    {

        if(isset(Yii::app()->session['cloneTypeExternal'])) {

            $data = Yii::app()->session['cloneTypeExternal'];
            unset(Yii::app()->session['cloneTypeExternal']);


            $_POST['Courier'] = $data['model'];
            $_POST['AddressData']['sender'] = $data['senderAddressData'];
            $_POST['AddressData']['receiver'] = $data['receiverAddressData'];
        }
    }


    public function actionCreatePremium()
    {
        $this->panelLeftMenuActive = 131;


        if(!Yii::app()->user->getModel()->isPremium)
            throw new CHttpException(403);

        // 12.09.2016
        if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH OR Yii::app()->user->model->getCourierExternalDisable())
        {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier',
            ));
            exit;
        }

        $this->_clone_external_fill();


// saveAndReturn
// saveAndPDF
// validate
// save


        if(isset($_POST['validate']))
            $mode = Courier::MODE_JUST_VALIDATE;
        else
            $mode = Courier::MODE_SAVE;


        $returnToFormAfter = 0;

        if(isset($_POST['saveAndReturn']) == 1)
            $returnToFormAfter = 1;

        $model = new Courier;
        $model->courierTypeExternal = new CourierTypeExternal('summary');
        $model->senderAddressData = new AddressData();
        $model->receiverAddressData = new AddressData();

        $model->senderAddressData->loadUserData();

        if (isset($_POST['Courier'])) {
            $model->setAttributes($_POST['Courier']);

            $senderAddressData = new AddressData();
            $senderAddressData->setAttributes($_POST['AddressData']['sender']);
            $model->senderAddressData = $senderAddressData;

            $receiverAddressData = new AddressData;
            $receiverAddressData->setAttributes($_POST['AddressData']['receiver']);
            $model->receiverAddressData = $receiverAddressData;

            $courierTypeExternal = new CourierTypeExternal;
            $model->courierTypeExternal = $courierTypeExternal;


            $model->courierTypeExternal->regulations = $_POST['CourierTypeExternal']['regulations'];
            $model->courierTypeExternal->regulations_rodo = $_POST['CourierTypeExternal']['regulations_rodo'];


            if($mode == Courier::MODE_JUST_VALIDATE)
            {
                $model->validateExternalType($mode);
            } else {

                if ($model->saveExternalType(true, NULL)) {

                    $model = Courier::model()->findByPk($model->id);




                    if($returnToFormAfter)
                    {
                        Yii::app()->user->setFlash('previousModel', $model);
                        $this->redirect(Yii::app()->createUrl('courier/courier/createPremium'));
                        Yii::app()->end();

                    } else {
                        $this->redirect(array('/courier/courier/view',
                            'urlData' => $model->hash,
                        ));
                    }
                }
            }
        }

        $this->render('createPremium', array(
            'model' => $model,
            'returnToFormAfter' => $returnToFormAfter,
        ));
    }


    public function actionIndex() {


        $this->redirect(['courier/list']);

//        $this->panelLeftMenuActive = 100;

//        $this->render('index');
    }

    public function actionCarriageTicket()
    {
        $urlData = $_GET['urlData'];

        /* @var $model Courier */
        $models = Courier::model()->findAllByAttributes(['hash' => $urlData]);


        foreach($models AS $key => $model)
        {
            if($model === null OR $model->courier_stat_id == CourierStat::CANCELLED OR $model->courier_stat_id == CourierStat::CANCELLED_USER OR in_array($model->user_cancel, [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_WARNED]))
            {
                unset($models[$key]);
            }
        }

        if(!S_Useful::sizeof($models))
            throw new CHttpException('404');

        $type = $_REQUEST['type'];

        LabelPrinter::generateLabels($models, $type, false, (!Yii::app()->user->isGuest && Yii::app()->user->model->getAllowCourierInternalExtendedLabel()) ? true : false);
    }

//    public function actionCancel($urlData)
//    {
//        if(!Yii::app()->user->getModel()->isPremium)
//            throw new CHttpException(403);
//
//        /* @var $model Courier */
//        $model = Courier::model()->find('hash=:hash', array(':hash' => $urlData));
//
//        if($model === null OR $model->courier_stat_id != CourierStat::NEW_ORDER)
//            throw new CHttpException('404');
//
//        if($model->cancelPackage())
//            Yii::app()->user->setFlash('success',Yii::t('courier','Paczka została anulowana'));
//        else
//            Yii::app()->user->setFlash('error', Yii::t('courier','Paczka nie została anulowana'));
//
//        $this->redirect(array('/courier/courier/view',
//            'urlData' => $model->hash,
//        ));
//
//    }

    public function actionCarriageTicketsUniversal($type = NULL, $mode = Courier::TYPE_EXTERNAL)
    {
        if($mode == Courier::TYPE_INTERNAL_SIMPLE)
            $mode = Courier::TYPE_INTERNAL_SIMPLE;
        else
            $mode = Courier::TYPE_EXTERNAL;

        if(!Yii::app()->user->getModel()->isPremium)
            throw new CHttpException(403);

        $type = $_POST['type'];
        if(isset($_POST['all']) OR isset($_POST['24']))
        {
            if(isset($_POST['all']))
            {
                if($mode == Courier::TYPE_INTERNAL_SIMPLE)
                    $return = CourierTypeInternalSimple::generateCarriageTicketsNotGeneratedInternalSimple($type);
                else
                    $return = Courier::generateCarriageTicketsNotGenerated($type);
            }
            else
            {
                if($mode == Courier::TYPE_INTERNAL_SIMPLE)
                    $return = CourierTypeInternalSimple::generateCarriageTicketsTodaysInternalSimple($type);
                else
                    $return = Courier::generateCarriageTicketsTodays($type);

            }



        } else
            throw new CHttpException(403);

        if(!$return)
        {
            throw new CHttpException(403, Yii::t('courier','Brak paczek do utworzenia etykiet!'));
        }

    }


    public function actionMassExportByGridView()
    {
//        if(!Yii::app()->user->getModel()->isPremium)
//            throw new CHttpException(403);

        /* @var $model Courier */
        $models = [];
        if (isset($_POST['Courier']))
        {
            $courier_ids =  $_POST['Courier']['ids'];

            $courier_ids = explode(',', $courier_ids);

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {
                $models = Courier::model()->findAllByAttributes(['id' => $courier_ids, 'user_id' => Yii::app()->user->id]);

//                foreach($courier_ids AS $id)
//                {
//                    $model = Courier::model()->findByPk($id);
//                    if($model === null)
//                        continue;
//
//
//                    array_push($models, $model);
//
//                    $i++;
//                }
            }

        }

        $i = S_Useful::sizeof($models);

        if(!$i)
        {
            Yii::app()->user->setFlash('error',Yii::t('courier','Żadna przesyłka nie została eksportowana.'));
            $this->redirect(Yii::app()->createUrl('courier/courier/list'));
        } else {

            S_CourierIO::exportToXlsUser($models);
        }
    }

    public function actionExportToFileAll($mode = Controller::EXPORT_MODE_EXTERNAL)
    {
        if(!Yii::app()->user->getModel()->isPremium)
            throw new CHttpException(403);
//        echo '1:' . round(memory_get_usage() / (1024*1024)) .'MB<br/>';
        $period = $_POST['period'];

        if($period == '' or strlen($period) < 6)
            throw new CHttpException(404);

        $period = substr($period, 2,4).'-'.substr($period, 0,2).'-';

        $models = Courier::model()->with('receiverAddressData')->with('senderAddressData');

        if($mode == Controller::EXPORT_MODE_INTERNAL_SIMPLE)
        {
            $models = $models->findAll(array('condition' => 'user_id = :user_id AND t.id IN (SELECT courier_id FROM courier_type_internal_simple) AND t.date_entered LIKE :date', 'params' => array(':user_id' => Yii::app()->user->id, ':date' => $period.'%')));
        }
        else if($mode == Controller::EXPORT_MODE_OOE)
        {
            $models = $models->findAll(array('condition' => 'user_id = :user_id AND t.id IN (SELECT courier_id FROM courier_type_ooe) AND t.date_entered LIKE :date', 'params' => array(':user_id' => Yii::app()->user->id, ':date' => $period.'%')));
        }
        else if($mode == Controller::EXPORT_MODE_INTERNAL) {
            $models = $models->findAll(array('condition' => 'user_id = :user_id AND courier_type_internal_id IS NOT NULL AND t.date_entered LIKE :date', 'params' => array(':user_id' => Yii::app()->user->id, ':date' => $period.'%')));
        }
        else if($mode == Controller::EXPORT_MODE_DOMESTIC)
        {
            $models = $models->findAll(array('condition' => 'user_id = :user_id AND t.id IN (SELECT courier_id FROM '.(new CourierTypeDomestic())->tableName().') AND t.date_entered LIKE :date', 'params' => array(':user_id' => Yii::app()->user->id, ':date' => $period.'%')));
        } else
            $models = $models->findAll(array('condition' => 'user_id = :user_id AND courier_type_external_id IS NOT NULL AND t.date_entered LIKE :date', 'params' => array(':user_id' => Yii::app()->user->id, ':date' => $period.'%')));

//        $models = Courier::model()->with('receiverAddressData')->with('senderAddressData');
//
//        ->with('courierTypeInternalSimple.courierExtOperatorDetails.courierLabelNew')
//        ->with(['courierTypeDomestic.courierLabelNew' => ['alias' => 'ctd']])
//        ->with('courierTypeDomestic.courierDomesticOperator')
//        ->with(['courierTypeInternal.commonLabel.courierLabelNew' => ['alias' => 'cticl']])
//        ->with(['courierTypeInternal.pickupLabel.courierLabelNew' => ['alias' => 'ctipl']])
//        ->with(['courierTypeInternal.deliveryLabel.courierLabelNew' => ['alias' => 'ctidl']])
//        ->with(['courierTypeOoe.courierLabelNew' => ['alias' => 'cto']]);

//        $models = $models->findAll();
//
        S_CourierIO::exportToXlsUser($models);

    }


    public function actionAjaxGetDimensionalWeight()
    {
        $d = $_POST['d'];
        $l = $_POST['l'];
        $w = $_POST['w'];

        if(is_numeric($d) && is_numeric($l) && is_numeric($w) && $d > 0 && $l >0 && $w >0)
            $size = S_PackageMaxDimensions::calculateDimensionsWeightBySizes($l, $d, $w);
        else
            $size = '';

        return $size;
    }

    public function actionAckCard($hash = NULL)
    {
        $model = Courier::model()->findByAttributes(array('hash' => $hash));

        /* @var $model Courier */

        if($model == NULL OR $model->courier_stat_id == CourierStat::CANCELLED OR $model->courier_stat_id == CourierStat::CANCELLED_USER OR in_array($model->user_cancel, [Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED]))
            throw new CHttpException(403, Yii::t('courier', 'Nie można wygenerować potwierdzenia dla tej paczki'));

        CourierAcknowlegmentCard::generateCard($model);
    }

    public function actionCustomsPacket($hash = NULL)
    {
        $model = Courier::model()->findByAttributes(array('hash' => $hash));

        /* @var $model Courier */

        if($model == NULL OR $model->courier_stat_id == CourierStat::CANCELLED OR $model->courier_stat_id == CourierStat::CANCELLED_USER OR in_array($model->user_cancel, [Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED]))
            throw new CHttpException(403, Yii::t('courier', 'Nie można wygenerować dokumentów celnych dla tej paczki'));

        GenerateCustomsPacket::generate($model, GenerateCustomsPacket::RETURN_PDF);
    }

    public function actionCn22Multi()
    {
        $ids = $_POST['Courier']['ids'];

        $models = Courier::model()
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'courier_stat_id != :stat AND courier_stat_id != :stat2 AND courier_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', [':stat' => CourierStat::CANCELLED, ':stat2' => CourierStat::CANCELLED_PROBLEM, ':stat3' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_REQUEST, ':uc2' => Courier::USER_CANCEL_CONFIRMED, ':uc3' => Courier::USER_CANCEL_WARNED]);

        /* @var $model Courier */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania CD23!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        CourierCN22::generateMulti($models);
    }

    public function actionCn23Multi()
    {
        $ids = $_POST['Courier']['ids'];

        $models = Courier::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'courier_stat_id != :stat AND courier_stat_id != :stat2 AND courier_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', [':stat' => CourierStat::CANCELLED, ':stat2' => CourierStat::CANCELLED_PROBLEM, ':stat3' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_REQUEST, ':uc2' => Courier::USER_CANCEL_CONFIRMED, ':uc3' => Courier::USER_CANCEL_WARNED]);

        /* @var $model Courier */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania CD23!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        CourierCN23::generateMulti($models);
    }

    public function actionKnMulti()
    {
        $ids = $_POST['Courier']['ids'];

        $models = Courier::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->with('courierTypeInternal')
            ->with('courierTypeInternal.order')
            ->with('courierTypeInternal.pickupLabel')
            ->with(array(
                "courierTypeInternal.pickupLabel.courierLabelNew" => array(
                    'alias' => 'internalPickupLabelNew',
                ),
            ))
            ->with('courierTypeInternal.commonLabel')
            ->with(array(
                "courierTypeInternal.commonLabel.courierLabelNew" => array(
                    'alias' => 'internalCommonLabelNew',
                ),
            ))
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'courier_stat_id != :stat AND courier_stat_id != :stat2 AND courier_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', [':stat' => CourierStat::CANCELLED, ':stat2' => CourierStat::CANCELLED_PROBLEM, ':stat3' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_REQUEST, ':uc2' => Courier::USER_CANCEL_CONFIRMED, ':uc3' => Courier::USER_CANCEL_WARNED]);

        /* @var $model Courier */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania KN!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        KsiazkaNadawcza::generate($models, KsiazkaNadawcza::MODE_COURIER);
    }

    public function actionAckCardMulti()
    {
        $ids = $_POST['Courier']['ids'];

        $ignoreExternalCards = $_POST['ignoreExternalCards'];

        $models = Courier::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->with('courierTypeInternal')
            ->with('courierTypeInternal.order')
            ->with('courierTypeInternal.pickupLabel')
            ->with(array(
                "courierTypeInternal.pickupLabel.courierLabelNew" => array(
                    'alias' => 'internalPickupLabelNew',
                ),
            ))
            ->with('courierTypeInternal.commonLabel')
            ->with(array(
                "courierTypeInternal.commonLabel.courierLabelNew" => array(
                    'alias' => 'internalCommonLabelNew',
                ),
            ))
            ->with('courierTypeOoe')
            ->with('courierTypeExternal')
            ->with('courierTypeInternalSimple')
            ->with('courierTypeDomestic')
            ->with('courierTypeDomestic.courierDomesticOperator')
            ->with('courierTypeDomestic.courierLabelNew')
            ->with(array(
                "courierTypeDomestic.courierLabelNew" => array(
                    'alias' => 'domesticLabelNew',
                ),
            ))
            ->with('courierTypeDomestic.courierLabelNew')
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'courier_stat_id != :stat AND courier_stat_id != :stat2 AND courier_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', [':stat' => CourierStat::CANCELLED, ':stat2' => CourierStat::CANCELLED_PROBLEM, ':stat3' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_REQUEST, ':uc2' => Courier::USER_CANCEL_CONFIRMED, ':uc3' => Courier::USER_CANCEL_WARNED]);

        /* @var $model Courier */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania potwierdzenia!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $temp = [];
        $temp2 = [];
        CourierAcknowlegmentCard::generateCardMulti($models, false, false, $temp, false, $temp2, false, $ignoreExternalCards, true);
    }


    public function actionCustomsPacketMulti()
    {
        $ids = $_POST['Courier']['ids'];

        $models = Courier::model()
            ->with('receiverAddressData')
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'courier_stat_id != :stat AND courier_stat_id != :stat2 AND courier_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', [':stat' => CourierStat::CANCELLED, ':stat2' => CourierStat::CANCELLED_PROBLEM, ':stat3' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_REQUEST, ':uc2' => Courier::USER_CANCEL_CONFIRMED, ':uc3' => Courier::USER_CANCEL_WARNED]);

        /* @var $model Courier */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania potwierdzenia!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        GenerateCustomsPacket::generateMulti($models);
    }


//    public function actionAckCardToday($iec = false)
//    {
//
//        $models = Courier::model()
//            ->with('senderAddressData')
//            ->with('receiverAddressData')
//            ->with('courierTypeInternal')
//            ->with('courierTypeInternal.order')
//            ->with('courierTypeInternal.pickupLabel')
//            ->with(array(
//                "courierTypeInternal.pickupLabel.courierLabelNew" => array(
//                    'alias' => 'internalPickupLabelNew',
//                ),
//            ))
//            ->with('courierTypeInternal.commonLabel')
//            ->with(array(
//                "courierTypeInternal.commonLabel.courierLabelNew" => array(
//                    'alias' => 'internalCommonLabelNew',
//                ),
//            ))
//            ->with('courierTypeOoe')
//            ->with('courierTypeExternal')
//            ->with('courierTypeInternalSimple')
//            ->with('courierTypeDomestic')
//            ->with('courierTypeDomestic.courierDomesticOperator')
//            ->with('courierTypeDomestic.courierLabelNew')
//            ->with(array(
//                "courierTypeDomestic.courierLabelNew" => array(
//                    'alias' => 'domesticLabelNew',
//                ),
//            ))
//            ->with('courierTypeDomestic.courierLabelNew')
//            ->findAll(['condition' => 't.user_id = :user_id AND t.date_entered LIKE :today AND t.courier_stat_id != :stat AND t.courier_stat_id != :stat2 AND t.courier_stat_id != :stat3 AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', 'params' => [':user_id' => Yii::app()->user->id, ':today' => date('Y-m-d').'%', ':stat' => CourierStat::CANCELLED, ':stat2' => CourierStat::CANCELLED_PROBLEM, ':stat3' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_CONFIRMED, ':uc2' => Courier::USER_CANCEL_REQUEST, ':uc3' => Courier::USER_CANCEL_WARNED]]);
//
//        /* @var $model Courier */
//
//        if(!S_Useful::sizeof($models))
//        {
//            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania potwierdzenia!'));
//            $this->redirect(['/courier/courier']);
//        }
//
//        $temp = [];
//        $temp2 = [];
//        CourierAcknowlegmentCard::generateCardMulti($models, false, false, $temp, false, $temp2, false, $iec, true);
//
//        $this->render('index');
//    }

    public function actionInvoice()
    {
        $ids = $_POST['Courier']['ids'];

        $models = Courier::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'courier_stat_id != :stat AND courier_stat_id != :stat3', [':stat' => CourierStat::CANCELLED, ':stat3' => CourierStat::CANCELLED_USER]);

        /* @var $model Courier */
        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania faktury!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        CourierInvoice::generateInvoice($models);
    }

    public function actionGenerateLabelsByGridView()
    {
        /* @var $model Courier */
//        $i = 0;

        $models = [];
        if (isset($_POST['Courier']))
        {
            $type = $_POST['Courier']['type'];
            $courier_ids =  $_POST['Courier']['ids'];

            $courier_ids = explode(',', $courier_ids);

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {
                $criteriaArchive = new CDbCriteria();
                $criteriaArchive->addNotInCondition('archive', [Courier::ARCHIVED_LABEL_EXPIRED, (Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED)]);
                $criteriaArchive->addCondition('archive IS NULL', 'OR');

                $criteriaCancel = new CDbCriteria();
                $criteriaCancel->addNotInCondition('user_cancel', [Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_WARNED]);
                $criteriaCancel->addCondition('user_cancel IS NULL', 'OR');

                $criteria = new CDbCriteria();

                $criteria->addInCondition('t.id', $courier_ids);
                $criteria->addNotInCondition('courier_stat_id', Courier::_listOfStatsPreventingGeneratingLabels());
//                $criteria->addCondition('user_id = :user_id');

                $criteria->join = 'LEFT JOIN courier_type_external_return ON courier_type_external_return.courier_id = t.id';
                $criteria->addCondition('courier_type_external_return.id IS NULL');

                $criteria->mergeWith($criteriaArchive);
                $criteria->mergeWith($criteriaCancel);

                $criteria->order = 't.id DESC';

                $models = Courier::model()->findAll($criteria);

            }
        }

        $i = S_Useful::sizeof($models);

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/courier/courier/list'));
        } else {

            if(Yii::app()->user->model->getAllowCourierInternalExtendedLabel())
                $getInternalSecond = true;
            else
                $getInternalSecond = false;

            LabelPrinter::generateLabels($models, $type, true, $getInternalSecond);
        }
    }

    public function actionStatConf($hash = NULL)
    {
        $model = Courier::model()->findByAttributes(array('hash' => $hash));

        /* @var $model Courier */

        if($model == NULL)
            throw new CHttpException(403, Yii::t('courier', 'Nie można wygenerować potwierdzenia dla tej paczki'));

        CourierStatusConfirmationCard::generateCard($model);
    }

    public function actionStatConfMulti()
    {
        $ids = $_POST['Courier']['ids'];

        $models = Courier::model()
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->with('courierTypeInternal')
            ->with('courierTypeInternal.order')
            ->with('courierTypeInternal.pickupLabel')
            ->with(array(
                "courierTypeInternal.pickupLabel.courierLabelNew" => array(
                    'alias' => 'internalPickupLabelNew',
                ),
            ))
            ->with('courierTypeInternal.commonLabel')
            ->with(array(
                "courierTypeInternal.commonLabel.courierLabelNew" => array(
                    'alias' => 'internalCommonLabelNew',
                ),
            ))
            ->with('courierTypeOoe')
            ->with('courierTypeExternal')
            ->with('courierTypeInternalSimple')
            ->with('courierTypeDomestic')
            ->with('courierTypeDomestic.courierDomesticOperator')
            ->with('courierTypeDomestic.courierLabelNew')
            ->with(array(
                "courierTypeDomestic.courierLabelNew" => array(
                    'alias' => 'domesticLabelNew',
                ),
            ))
            ->with('courierTypeDomestic.courierLabelNew')
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)]);

        /* @var $model Courier */

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do wygenerowania potwierdzenia!'));
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        CourierStatusConfirmationCard::generateCardMulti($models);
    }


    /**
     * Managing custom file formats for importing packages
     *
     * @throws CException
     */
    public function actionAjaxImportModalSettings()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $id = $_POST['id'];
            $type = $_POST['type'];
            $id = intval($id);
            $type = intval($type);

            if(!$id) {
                $model = new CourierImportSettings;
                $model->type = $type;
                $model->user_id = Yii::app()->user->id;
            }
            else
                $model = CourierImportSettings::getModelForUserId($id, Yii::app()->user->id);

            if(isset($_POST['settings'])) {
                try {
                    if ($_POST['settings']['delete']) {
                        if ($model->delete()) {
                            Yii::app()->user->setFlash('success', Yii::t('courier', 'Wpis został usunięty.'));
                            echo CJSON::encode([
                                'success' => true
                            ]);
                            Yii::app()->end();
                        } else {
                            echo CJSON::encode([
                                'success' => false
                            ]);
                            Yii::app()->end();
                        }
                    }

                    $model->setNameSafe($_POST['settings']['name']);

                    $order = $_POST['settings']['order'];
                    parse_str($order, $order);
                    $order = $order['attr'];

                    if (is_array($order))
                        foreach ($order AS $key => $item)
                            $order[$key] = intval($item);

                    $separator = $_POST['settings']['separator'];
                    if (!in_array($separator, array_keys(F_OoeImport::listOfSeparators())))
                        $separator = 0;

                    $data = [
                        'order' => $order,
                        'separator' => $separator,
                    ];

                    $model->data = $data;
                    $model->validate();

                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', Yii::t('courier', 'Ustwienia zostały zapisane.'));
                        echo CJSON::encode([
                            'success' => true
                        ]);
                    } else {
                        echo CJSON::encode([
                            'success' => false
                        ]);
                    }
                } catch (Exception $ex) {
                    echo CJSON::encode([
                        'success' => false
                    ]);
                }


                //User::setOoeImportSettings($data, Yii::app()->user->id);

                Yii::app()->end();
            }

            $userSettings = $model->data;
            $order = $userSettings['order'];
            $separator = $userSettings['separator'];


            if($type == CourierImportSettings::TYPE_INTERNAL)
                $data = F_InternalImport::getAttributesList($order, false);
            else if($type == CourierImportSettings::TYPE_OOE)
                $data = F_OoeImport::getAttributesList($order, false);
            else if($type == CourierImportSettings::TYPE_U)
                $data = F_UImport::getAttributesList($order, false);
            else if($type == CourierImportSettings::POSTAL) {
                Yii::app()->getModule('postal');
                $data = F_PostalImport::getAttributesList($order, false);
            }
            else
                throw new CHttpException(403);

            $html = $this->renderPartial('_import_custom_settings', [
                'data' => $data,
                'separator' => $separator,
                'name' => $model->name,
                'id' => $model->id,
                'type' => $type,
            ], true);
            echo CJSON::encode($html);

        } else
            $this->redirect(['/']);
    }

    public function actionCancelByUser($urlData)
    {

        if(Yii::app()->user->isGuest)
            throw new CHttpException(403);

        /* @var $model Courier */
        $model = Courier::model()->find('hash=:hash AND user_id = :user_id', array(':hash' => $urlData, ':user_id' => Yii::app()->user->id));

        if($model === NULL)
            throw new CHttpException(404);

        if($model->isUserCancelByUserPossible())
        {
            if($model->userCancelRequestByUser())
            {
//                if($model->user_cancel == Courier::USER_CANCEL_REQUEST)
//                    Yii::app()->user->setFlash('notice', Yii::t('courier', 'Ze względu na złożone zamówienie, paczka nie może zostać anulowana automatycznie, ale zgłoszenie zostało wysłane do administratora.'));

//                else if($model->user_cancel == Courier::USER_CANCEL_CONFIRMED)
                Yii::app()->user->setFlash('success', Yii::t('courier', 'Paczka została anulowana.'));
//                else
//                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Anulowanie tej paczki nie było możliwe.'));

            } else
                Yii::app()->user->setFlash('error', Yii::t('courier', 'Anulowanie tej paczki nie było możliwe.'));

        } else {

            Yii::app()->user->setFlash('error', Yii::t('courier', 'Anulowanie tej paczki nie było możliwe.'));
        }



        $this->redirect(['/courier/courier/view', 'urlData' => $model->hash]);
//        Yii::app()->end();
    }


    public function actionCancelByUserMulti()
    {
        $ids = $_POST['Courier']['ids'];

        $models = Courier::model()
            ->findAllByAttributes(['user_id' => Yii::app()->user->id, 'id' => explode(',', $ids)], 'courier_stat_id != :stat AND courier_stat_id != :stat2 AND user_cancel = 0', [':stat' => CourierStat::CANCELLED, ':stat2' => CourierStat::CANCELLED_USER]);

        $return = false;
        /* @var $model Courier */
        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do anulowania!'));
        } else {
            $return = Courier::userCancelMulti($models);
        }


        if($return)
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Anulowano paczek: {no}', ['{no}' => $return]));
        else
            Yii::app()->user->setFlash('top-info', Yii::t('courier', 'Brak paczek do anulowania!'));

        $this->redirect(Yii::app()->createUrl('/courier/courier/list'));
        Yii::app()->end();
    }

    public function actionAjaxIsLabelReady()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $hash = $_POST['hash'];

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('courier_stat_id');
            $cmd->from('courier');
            $cmd->where('hash = :hash', [':hash' => $hash]);
            $result = $cmd->queryScalar();

            if($result && !in_array($result, [CourierStat::PACKAGE_PROCESSING, CourierStat::NEW_ORDER]))
                $result = true;
            else
                $result = false;

            echo CJSON::encode($result);

        } else
            throw new CHttpException(404);

    }

    public function actionLabelPrinter()
    {

        $this->panelLeftMenuActive = 103;

        $model = false;
        if (isset($_POST['local_id'])) {

            $local_id =  trim($_POST['local_id']);

            $model = Courier::model()->find('local_id=:local_id AND user_id = :user_id', array(':local_id' => $local_id, ':user_id' => Yii::app()->user->id));

            if($model === NULL) {

                $remoteId = $local_id;
                $models = CourierExternalManager::findCourierIdsByRemoteId($remoteId, true);

                foreach($models AS $key => $item)
                {
                    if(!$item->isLabelGeneratingAvailableBaseOnStat() OR $item->isLabelArchived() OR $item->user_id != Yii::app()->user->id)
                        unset($models[$key]);
                }

//                $n = S_Useful::sizeof($response);
//                if ($n > 1) {
//                    Yii::app()->user->setFlash('notice', Yii::t('courier', 'Znaleziono więcej niż 1 paczkę!'));
//                } else {
//                    $model = array_pop($response);
//                }
            }

            if(intval($_POST['print_option']) === 1 && S_Useful::sizeof($models))
            {
                $hash = [];
                foreach($models AS $item)
                    $hash[] = $item->hash;

                echo CJSON::encode(['redirect' => true, 'url' => Yii::app()->createAbsoluteUrl('/courier/courier/CarriageTicket', ['urlData' => $hash, 'type' => Courier::PDF_ROLL])]);
                exit;

            }

            if(isset($_POST['ajax'])) {
                echo CJSON::encode(array('html' => $this->renderPartial('_labelPrinter_buttons', array('models' => $models), true)));
                exit;
            }

        }

        $this->render('labelPrinter', array(
            'model' => $model,
        ));
    }

    public function actionAjaxIsAutomaticReturnAvailable()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $receiver_country_id = $_POST['country_id'];

            if(Yii::app()->user->model->getCourierReturnAvailable() && CourierTypeReturn::isAutomaticReturnAvailable($receiver_country_id))
                echo CJSON::encode(true);
            else
                echo CJSON::encode(false);
        }
    }

    public function actionRemoveRef($hash)
    {
        /* @var $model Courier */
        $model = Courier::model()->findByAttributes(['hash' => $hash, 'user_id' => Yii::app()->user->id]);

        if($model && $model->ref != '')
        {
            $ref = $model->ref;
            $model->ref = NULL;
            if($model->update(['ref']))
            {
                $model->addToLog('Customer removed REF number: '.$ref, CourierLog::CAT_INFO);
                Yii::app()->user->setFlash('success', Yii::t('courier', 'Numer REF został usunięty!'));
                $this->redirect(['/courier/courier/view', 'urlData' => $model->hash]);
            }
        }

        Yii::app()->user->setFlash('danger', Yii::t('courier', 'Numer REF nie został usunięty!'));
        $this->redirect(['/courier/courier/view', 'urlData' => $model->hash]);
        Yii::app()->end();
    }

}