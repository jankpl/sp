<?php

$this->breadcrumbs = array(
    HybridMail::label(2),
    Yii::t('app', 'Index'),
);


?>

<script>
    $(document).ready(function(){
        $("#hmMapZoomTriggerPoint").mouseenter(function(){
            $("#hmEuropeZoomed").fadeIn();
        });

        $("#hmMapZoomTriggerPoint").mouseleave(function(){
            $("#hmEuropeZoomed").fadeOut();
        });
    });
</script>

<h2><?php echo Yii::t('hybridMail','HybridMail');?></h2>

<div style="width: 680px; height: 433px; background: url(<?php echo Yii::app()->getBaseUrl();?>/images/layout/hmMap.png); position: relative; margin: 0 auto 15px auto;">
    <div style="width: 50px; height: 50px; background: url(<?php echo Yii::app()->getBaseUrl();?>/images/layout/zoom.png); position: absolute; left: 310px; top: 140px; z-index: 10;" id="hmMapZoom"></div>
    <div style="width: 90px; height: 80px; position: absolute; left: 290px; top: 110px; cursor: pointer; z-index: 30;" id="hmMapZoomTriggerPoint"></div>

    <div style="width: 680px; height: 433px; background: url(<?php echo Yii::app()->getBaseUrl();?>/images/layout/hmMapEurope.png); position: absolute; display: none; z-index: 20;" id="hmEuropeZoomed"></div>

</div>