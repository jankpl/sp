 <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'login-form-home',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>false,
        ),
        'action'=>$this->createAbsoluteUrl('site/login'),
    ));

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.clearOnClick.js');
    ?>

        <?php echo $form->textField($model,'username',array('class' => 'clearOnClick', 'id'=>'login', 'value' => Yii::t('site','login...'))); ?>
        <?php echo $form->passwordField($model,'password', array('class' => 'clearOnClick', 'id'=>'pass', 'value' => '......')); ?>

    <div class="row buttons">
        <?php echo TbHtml::submitButton('Login',array('id'=>'submit', 'value' => '', 'title' => Yii::t('site','Zaloguj się!'))); ?>
    </div>

    <?php $this->endWidget(); ?>
