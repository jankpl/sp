<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Update'),
);

$this->menu = array(
	array('label' =>'View ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
	array('label' =>'Manage ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1>Update <?php echo GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'payment-type-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'commission_percentage'); ?>
        <?php echo $form->textField($model, 'commission_percentage'); ?> %
        <?php echo $form->error($model,'commission_percentage'); ?>
    </div><!-- row -->
    <div class="row">
        <?php $this->widget('PriceForm', array(
            'form' => $form,
            'model' => $model->commissionPrice,
            'formFieldPrefix' => '',
        ));?>
    </div>


    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->