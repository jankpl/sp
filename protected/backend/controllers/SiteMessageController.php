<?php

class SiteMessageController extends Controller
{

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('@'),
				'expression'=>'0 >= Yii::app()->user->authority',
			),
			array('deny',  // block rest of actions
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{

		if(isset($_POST['SiteMessage']))
		{
			$active = $_POST['SiteMessage']['active'] == 1 ? 1 : 0;
			$text = $_POST['SiteMessage']['text'];
			$text_other = $_POST['SiteMessage']['text_other'];


			Config::setData(Config::SITE_MESSAGE_ACTIVE, $active);
			Config::setData(Config::SITE_MESSAGE_CONTENT, $text);
			Config::setData(Config::SITE_MESSAGE_CONTENT_OTHER, $text_other);

			Yii::app()->user->setFlash('success', 'Changes has been saved!');
			$this->refresh();
			exit;
		}

		$this->render('index',[]);
	}

}

