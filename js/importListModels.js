(function($) {

    var $overlay;

    $(document).ready(function(){

        $overlay = $('.overlay');

        $overlay.hide();

        var $removeButton = $('[data-remove-this-package]');
        var $editButton = $('[data-edit-this-package]');

        $removeButton.unbind('click');
        $editButton.unbind('click');

        $removeButton.on('click', function(){
            $overlay.show();
            onRemoveButton($(this));
        });

        $editButton.on('click', function(){
            $overlay.show();
            onEditButton($(this));
        });


        $(".real-wrapper").scroll(function(){
            $(".dummy-wrapper").scrollLeft($(".real-wrapper").scrollLeft());
            $(".header-wrapper").scrollLeft($(".real-wrapper").scrollLeft());
        });
        $(".dummy-wrapper").scroll(function(){
            $(".real-wrapper").scrollLeft($(".dummy-wrapper").scrollLeft());
            $(".header-wrapper").scrollLeft($(".dummy-wrapper").scrollLeft());
        });


        $('[data-autoupdate]').on('change', onAutoupdate);


        var formSubmitIgnored = false;

        $('#import-form').on('submit', function(){
            $overlay.show();

            jQuery(window).bind('beforeunload', function(){

                if(formSubmitIgnored) {
                    return yii.text.beforeLeaving;
                } else
                    formSubmitIgnored = true;
            });
        });

    });


    function onRemoveButton($that)
    {

        $.ajax({
            method: "POST",
            url: yii.urls.removeRowOnImport,
            data: { hash: yii.misc.hash, id: $that.closest('[data-row-id]').attr('data-row-id')},
            dataType: 'json',

        })
            .done(function(result){

                if(result.success)
                {
                    $that.closest('[data-row-id]').remove();
                    $overlay.hide();
                } else {
                    alert("Error...");
                }

            })
            .fail(function(ex){
                console.log(ex);
                alert("Error...");
            })
    }

    function onEditButton($that)
    {
        var $realWrapper = $('.real-wrapper');
        var height = $realWrapper.css('height');
        height = height.substring(0, height.length - 2);
        height = parseInt(height);

        if(height < 700) {
            $realWrapper.attr('data-height', $realWrapper.css('height'));
            $realWrapper.css('height', 700 + 'px');
        }


        $.ajax({
            method: "POST",
            url: yii.urls.editRowOnImport,
            data: { hash: yii.misc.hash, id: $that.closest('[data-row-id]').attr('data-row-id')},
            dataType: 'json',

        })
            .done(function(result){
                $overlay.hide();
                if(result.success)
                {
                    $('#edit-modal-content').html(result.html);
                    $('#edit-modal').modal();


                    // $('[data-edit-row]').remove();
                    //
                    // var $newTr = $('<tr>');
                    // $newTr.attr('data-edit-row', 'true');
                    // $newTr.addClass('data-edit-row');
                    // $newTd = $('<td>');
                    // $newTd.attr('colspan', $that.closest('tr').children('td').length);
                    // $newTd.html(result.html);
                    // $newTr.append($newTd);
                    // $that.closest('tr').after($newTr);




                    // $('[data-save-row-changes]').unbind('click');
                    // $('[data-save-row-changes]').on('click', function(){
                    //     onSaveRowButton($(this));
                    // });

                    // $('[data-cancel]').on('click', function(){
                    //
                    //     // $(this).closest('[data-edit-row]').remove();
                    //     // $realWrapper = $('.real-wrapper');
                    //     // var height = $realWrapper.attr('data-height');
                    //     // if(height != '')
                    //     //     $realWrapper = $realWrapper.css('height', height);
                    //
                    //     // $('#myModal').modal('hide');
                    // });


                    $("[data-autocomplete-input-field]").each(function()
                    {
                        bindAutocomplete($(this), $(this).data('content'), $(this).attr('data-url'));
                    });


                } else {
                    alert("Error...");
                }

            })
            .fail(function(ex){
                console.log(ex);
                alert("Error...");
            });
    }

    function onAutoupdate()
    {
        $overlay.show();

        var i = $(this).attr('data-i');
        var attr = $(this).attr('data-attribute');
        var val = $(this).val();

        var $that = $(this);

        $.ajax({
            method: "POST",
            url: yii.urls.updateRowOnImport,
            data: {hash: yii.misc.hash, i: i, attribute: attr, val: val},
            dataType: 'json',
        })
            .done(function(result){

                if(result.success)
                {
                    var defaultBorder = $that.css('border-bottom');
                    $that.css('border-bottom', '5px solid green');
                    $overlay.hide();
                    setTimeout(function(){
                        $that.css('border-bottom', defaultBorder);
                    },1000);
                } else {
                    alert(result.msg);
                    $that.val(result.backup);
                    $overlay.hide();
                }
            })
            .fail(function(ex){
                console.log(ex);
                alert(yii.text.unknownError);
                $overlay.hide();
            });
    }


})(jQuery);

function onItemEditSubmit(url)
{
    $('#edit-modal').modal('hide');
    $(".overlay").show();
    var $form =  $("form#courier-edit-form");

    $.ajax({
        method: "POST",
        url: url,
        data: $form.serialize(),
        dataType: "json",
    })
        .done(function(result){

            if(result.success)
                location.reload();
            else {
                console.log(result);
                alert("Error...");
            }
        })
        .fail(function(ex){
            console.log(ex);
            alert("Error...");
        });
}
