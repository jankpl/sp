<?php
/* @var $mode string */
/* @var $this UserMessageController */
/* @var $model UserMessage */
/* @var $page Int */
/* @var $pageSize Int */
?>

<h2><?php echo Yii::t('user_invoice', 'Twoje faktury');?></h2>

<?php


$waitingForPayment = UserInvoice::model()->active()->ofCurrentUser()->notPaidWithPartialyPaid()->count();
$outdated = UserInvoice::model()->active()->ofCurrentUser()->outdated()->count();
$totalNumber = UserInvoice::model()->active()->ofCurrentUser()->count();
$amoutToPayPLN = UserInvoice::sumTotalInvValue(Currency::PLN_ID, true);
$amoutToPayEUR = UserInvoice::sumTotalInvValue(Currency::EUR_ID, true);
$amoutToPayGBP = UserInvoice::sumTotalInvValue(Currency::GBP_ID, true);
$paymentDate = UserInvoice::getClosestPaymentDate();
?>

<div class="row">
    <div class="col-xs-12 col-sm-2">
        <div class="panel panel-<?= $waitingForPayment > 0 ? 'warning' : 'success';?>">
            <div class="panel-heading">
                <div class="text-right"><div class="big"><?= $waitingForPayment;?></div>
                    <div><?= Yii::t('invoice', 'Nieopłaconych faktur');?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3">
        <div class="panel panel-<?= ($amoutToPayPLN > 0 OR $amoutToPayEUR > 0 OR $amoutToPayGBP) ? 'warning' : 'success';?>">
            <div class="panel-heading">
                <div class="text-right"><div><span class="big"><?= $amoutToPayPLN ?: 0 ?></span> <?= Currency::PLN;?> | <span class="big"><?= $amoutToPayEUR ?: 0 ?></span> <?= Currency::EUR;?> | <span class="big"><?= $amoutToPayGBP ?: 0 ?></span> <?= Currency::GBP;?></div>
                    <div><?= Yii::t('invoice', 'Kwota do zapłaty');?></div>
                </div>
            </div>
        </div>
    </div>
    <?php
    if($waitingForPayment && $paymentDate):
        ?>
        <div class="col-xs-12 col-sm-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="text-right"><div class="big"><?= $paymentDate;?></div>
                        <div><?= Yii::t('invoice', 'Najbliższy termin płatności');?></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    endif;
    ?>
    <div class="col-xs-12 col-sm-2">
        <div class="panel panel-<?= $outdated > 0 ? 'danger' : 'success';?>">
            <div class="panel-heading">
                <div class="text-right"><div class="big"><?= $outdated;?></div>
                    <div><?= Yii::t('invoice', 'Faktur z przekroczonym terminem płatności');?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-2">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="text-right"><div class="big"><?= $totalNumber;?></div>
                    <div><?= Yii::t('invoice', 'Łączna liczba faktur');?></div>
                </div>
            </div>
        </div>
    </div>

</div>


<div class="gridview-container">
    <div class="gridview-overlay"></div>
    <?php
    $this->_getGridViewUserInvoicegrid();

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('jquery.ui');
    Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css');
    if(Yii::app()->getLanguage() == 'pl')
        Yii::app()->clientScript->registerScriptFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/i18n/ui.datepicker-pl.js');

    $script = "$( function() {
       
         $('[name=\"UserInvoiceGridViewUser[inv_payment_date]\"], [name=\"UserInvoiceGridViewUser[inv_date]\"]').datepicker({
                dateFormat : 'yy-mm-dd',
                minDate: \"-5Y\", 
                maxDate: \"+0M\",
                changeMonth: true,
                changeYear: true,
            });
          

        } );";
    Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);

    ?>
</div>