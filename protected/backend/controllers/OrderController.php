<?php

class OrderController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => [ 'view', 'index', 'admin', 'exportToFileByGridView'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionAssingStatusByGridView()
    {
        /* @var $model Order */
        $i = 0;
        if (isset($_POST['Order'])) {

            $order_stat_id = (int) $_POST['order_stat_id'];
            $order_ids =  $_POST['Order']['ids'];

            $order_ids = explode(',', $order_ids);

            if(is_array($order_ids) AND S_Useful::sizeof($order_ids))
            {
                $transaction = Yii::app()->db->beginTransaction();
                $errors = false;


                foreach($order_ids AS $id)
                {
                    $model = Order::model()->findByPk($id);
                    if($model === null) continue;
                    if(!$model->changeStat($order_stat_id))
                        $errors = true;

                    $i++;
                }

                if(!$errors AND $i > 0)
                {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success','Udało się zmienić statusy. Liczba pozycji: '.$i);
                }
                else
                {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error','Nie udało się zmienić statusu');
                }
            }


        }

        if(!$i) Yii::app()->user->setFlash('error','Zaznacz listę zamówień!');



        $this->redirect(Yii::app()->createUrl('order/Admin').'#mass-assign-stat');

    }


    public function actionView($id) {


        $this->render('view', array(
            'model' => $this->loadModel($id, 'Order'),
            'activeTab' => $_GET['activeTab'],
        ));
    }

    public function actionChangeStat()
    {
        /* @var $model Order */
        if (isset($_POST['Order'])) {

            $model = Order::model()->findByPk($_POST['Order']['id']);

            if($model === null)
                throw new Exception('Błąd!');

            if($model->changeStat($_POST['Order']['order_stat_id']))
                $this->redirect(array('view', 'id' => $model->id, 'activeTab' => 3));
            else
                throw new Exception("Błąd!");
        } else
            throw new CHttpException('404');

    }

    public function actionCreate() {
        $this->redirect('index');
        return;

//        $model = new Order;
//
//        $this->performAjaxValidation($model, 'order-form');
//
//        if (isset($_POST['Order'])) {
//            $model->setAttributes($_POST['Order']);
//
//            if ($model->save()) {
//                if (Yii::app()->getRequest()->getIsAjaxRequest())
//                    Yii::app()->end();
//                else
//                    $this->redirect(array('view', 'id' => $model->id));
//            }
//        }
//
//        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id) {
        $this->redirect(array('order/view', 'id' => $id));
        return;

//        $model = $this->loadModel($id, 'Order');
//
//        $this->performAjaxValidation($model, 'order-form');
//
//        if (isset($_POST['Order'])) {
//            $model->setAttributes($_POST['Order']);
//
//            if ($model->save()) {
//                $this->redirect(array('view', 'id' => $model->id));
//            }
//        }
//
//        $this->render('update', array(
//            'model' => $model,
//        ));
    }

    public function actionDelete($id) {


        throw new CHttpException(403, 'Nie można usunąć zamówienia!');


//        if (Yii::app()->getRequest()->getIsPostRequest()) {
//            $this->loadModel($id, 'Order')->delete();
//
//            if (!Yii::app()->getRequest()->getIsAjaxRequest())
//                $this->redirect(array('admin'));
//        } else
//            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('order/admin'));
    }

    public function actionAdmin() {

        $model = new _OrderGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['OrderGridView']))
            $model->setAttributes($_GET['OrderGridView']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionExportToFileByGridView()
    {
        /* @var $model Order */
        $i = 0;
        if (isset($_POST['Order']))
        {


            $order_ids =  $_POST['Order']['ids'];

            $order_ids = explode(',', $order_ids);

            if(is_array($order_ids) AND S_Useful::sizeof($order_ids))
            {

                $models = [];
                foreach($order_ids AS $id)
                {
                    $model = Order::model()->findByPk($id);
                    if($model === null)
                        continue;

                    array_push($models, $model);

                    $i++;
                }
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę zamówień!');
            $this->redirect(Yii::app()->createUrl('/order/admin'));
        } else {
            S_OrderIO::exportToXls($models);
        }
    }

}