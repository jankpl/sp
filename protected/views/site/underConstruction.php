<div style="text-align: center; margin-top: 75px;">
    <?php
    if(!Yii::app()->params['langCode'] == 'pl'):
        ?>
        <img src="<?php echo Yii::app()->baseUrl;?>/images/layout/logo_other.png" />
    <?php else: ?>
        <img src="<?php echo Yii::app()->baseUrl;?>/images/layout/logo_other_en.png" />
    <?php endif; ?>
    <p style="font-size: 18px; font-weight: bold;"><?php echo Yii::t('site', 'Przepraszamy, usługa jest jeszcze niedostępna.');?></p>
    <p style="font-size: 14px;"><?php echo Yii::t('site', 'Zapraszamy wkrótce...');?></p>
    <p>&nbsp;</p>
    <p>[ <a href="<?php echo Yii::app()->createUrl('site/contact');?>"><?php echo Yii::t('site', 'kontakt');?></a> ]</p>
</div>