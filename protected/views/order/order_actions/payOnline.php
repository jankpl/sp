<?php
/* @var $model Order */
?>

<div class="flash-notice">
<h3><?php echo Yii::t('order','Opłać zamówienie online');?></h3>
    <br/>
<?php echo CHtml::link(Yii::t('order','Zapłać online'),array('order/payment', 'urlData' => $model->hash), array('class' => 'btn')); ?>
</div>