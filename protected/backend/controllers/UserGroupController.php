<?php

class UserGroupController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => ['view', 'update','index'],
                'verbs' => ['get'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'UserGroup'),
        ));
    }

    public function actionCreate() {
        $model = new UserGroup;

        $this->performAjaxValidation($model, 'user-group-form');



        if (isset($_POST['UserGroup'])) {
            $model->setAttributes($_POST['UserGroup']);
                      $transaction = Yii::app()->db->beginTransaction();
            if ($model->save()) {



                if($model->save())
                {
                    $transaction->commit();
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }
            $transaction->rollback();
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id) {

        /* @var $model UserGroup */
        $model = $this->loadModel($id, 'UserGroup');

        if(isset($_POST['toggleBlockNotify']))
        {
            $notify = $_POST['UserGroup']['adminBlockNotifySms'];
            $model->setAdminBlockNotifySms($notify);


            Yii::app()->user->setFlash('success', Yii::t('user', 'Ustawienia blokowania powiadomień zostały zaktualizowane'));
            $this->redirect(array('update', 'id' => $id));
            exit;

        }

        $this->performAjaxValidation($model, 'user-group-form');



        if (isset($_POST['UserGroup'])) {
            $model->setAttributes($_POST['UserGroup']);


            $transaction = Yii::app()->db->beginTransaction();
            if ($model->save()) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
            $transaction->rollback();
        }


        $this->render('update', array( 'model' => $model));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'UserGroup')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('index'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $models = UserGroup::model()->findAll();

        $this->render('index', array(
            'models' => $models,
        ));

    }

    public function actionRemoveMember($id, $id2)
    {
        /* @var $model UserGroup */

        $model = UserGroup::model()->findByPk($id);

        if($model === null)
            throw new CHttpException('404','Nieprawidłowy ID!');

        if($model->removeMember($id2))
        {
            Yii::app()->user->setFlash('success','Użytkownik został usunięty z grupy');
            $this->redirect(Yii::app()->createUrl('userGroup/view', array('id' => $id)));
        }
        else
        {
            throw new CHttpException('404','Nieprawidłowy ID!');
        }

    }

    public function actionAddMember($id)
    {
        /* @var $model UserGroup */

        $model = UserGroup::model()->findByPk($id);

        if($model === null)
            throw new CHttpException('404','Nieprawidłowy ID!');

        $errors = false;

        $transaction = Yii::app()->db->beginTransaction();
        if(isset($_POST['user_id']))
        {
            $i = 0;
            foreach($_POST['user_id'] AS $item)
            {


                $i++;
                if(!$model->addMember($item))
                {
                    $errors = true;
                }
            }


            if($errors)
            {
                $transaction->rollback();
                throw new CHttpException('404','Nieprawidłowy ID!');
            } else {
                $transaction->commit();
                if($i>1) Yii::app()->user->setFlash('success','Użytkownicy zostali dodani do grupy');
                else Yii::app()->user->setFlash('success','Użytkownik został dodany do grupy');
                $this->redirect(Yii::app()->createUrl('userGroup/view', array('id' => $id)));
            }

        }

        $this->redirect(Yii::app()->createUrl('userGroup/update', array('id' => $id)));

    }

}