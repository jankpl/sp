<?php
/* @var $id_prefix string */
?>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'user-calcel',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/cancelByUserMulti'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('postal', 'Anuluj paczki'), array(
    'onClick' => 'js:

    if(!confirm("'.Yii::t("site", "Czy jesteś pewien?").'"))
         return false;

    $("#'.$id_prefix.'postal-ids-cancel-multi").val($("#postalgrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => $id_prefix.'postal-ids-cancel-multi')); ?>
<?php
$this->endWidget();
?>
<br/>
<?= Yii::t('postal', 'Przesyłki mogą być anulowanie w ciągu {n} dni.', ['{n}' => '<strong>'.Postal::PACKAGE_CANCEL_DAYS.'</strong>']);?>

