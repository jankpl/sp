<?php

Yii::import('application.modules.courier.models._base.BaseCourierPriceUniversal');

class CourierPriceUniversal extends BaseCourierPriceUniversal
{
    const MAX_WEIGHT = 9999;

    const TYPE_EXTERNAL_RETURN = 1;
    const TYPE_INTERNAL_RETURN = 2;
    const TYPE_EXTERNAL = 3;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function beforeSave()
    {
        $this->admin_id = Yii::app()->user->id;
        return parent::beforeSave(); // TODO: Change the autogenerated stub
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_EXTERNAL_RETURN => 'External Return',
            self::TYPE_INTERNAL_RETURN => 'Internal Return',
            self::TYPE_EXTERNAL => 'External',
        ];
    }

    public static function getTypeName($type)
    {
        return isset(self::getTypeList()[$type]) ? self::getTypeList()[$type] : NULL;
    }

    public function rules() {
        return array(
            array('date_entered', 'default', 'value'=>new CDbExpression('NOW()')),

            array('type, courier_country_group_id', 'required'),
            array('type, user_group_id, courier_country_group_id, admin_id, rev', 'numerical', 'integerOnly'=>true),
            array('user_group_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, type, user_group_id, courier_country_group_id, date_entered, admin_id, rev', 'safe', 'on'=>'search'),
        );
    }


    public static function loadOrCreatePrice($type, $courier_country_group_id, $user_group = null)
    {

        if($user_group == NULL)
            $model = self::model()->with('courierPriceUniversalValues')->find('courier_country_group_id = :id AND user_group_id IS NULL AND type = :type', array(':id' => $courier_country_group_id, ':type' => $type));
        else
            $model = self::model()->with('courierPriceUniversalValues')->find('courier_country_group_id = :id AND user_group_id = :group AND type = :type', array(':id' => $courier_country_group_id, ':group' => $user_group, ':type' => $type));

        if($model == NULL) {
            $model = new CourierPriceUniversal();
            $model->courier_country_group_id = $courier_country_group_id;
            $model->type = $type;
            $model->user_group_id = $user_group;
        }

        return $model;
    }

    public function generateDefaultPriceValue()
    {
        $maxPriceModel = new CourierPriceUniversalValue();
        $maxPriceModel->courier_price_universal_id = $this->id;
        $maxPriceModel->weight_top = self::MAX_WEIGHT;
        $maxPriceModel->price = new Price();

        return $maxPriceModel;
    }

    public function loadValueItems()
    {
        $courierPriceUniversalValues = [];

        if($this->id != NULL)
            $courierPriceUniversalValues = CourierPriceUniversalValue::model()->findAll(array('condition' => 'courier_price_universal_id = :courier_price_universal_id AND rev = :rev', 'params' => array(':courier_price_universal_id' => $this->id, ':rev' => $this->rev), 'order' => 'weight_top ASC'));

        if(!S_Useful::sizeof($courierPriceUniversalValues))
            $courierPriceUniversalValues[0] = new CourierPriceUniversalValue();

        return $courierPriceUniversalValues;
    }


    public static function getPrice($type, $weight, $user_id, $force_currency_id = false)
    {

        $user_group_id = false;

        if($user_id)
            $user_group_id = User::getUserGroupId($user_id);

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('price_id');
        $cmd->from('courier_price_universal');
        $cmd->join('courier_price_universal_value', 'courier_price_universal_id = courier_price_universal.id AND courier_price_universal.rev = courier_price_universal_value.rev');
        if($user_group_id)
            $cmd->where('user_group_id = :user_group_id', [':user_group_id' => $user_group_id]);
        else
            $cmd->where('user_group_id IS NULL');
        $cmd->andWhere('weight_top <= :weight', [':weight' => $weight]);
        $cmd->andWhere('type = :type', [':type' => $type]);
        $cmd->limit(1);
        $cmd->order('weight_top DESC');

        $price_id = $cmd->queryScalar();

        if($price_id === NULL)
        {
            if($user_group_id)
                return self::getPrice($type, $weight, false);
            else
                return false;
        }

        if($force_currency_id)
            $currency_id = $force_currency_id;
        else
            $currency_id = Yii::app()->PriceManager->getCurrency();

        $value = Price::getValueInCurrency($price_id, $currency_id);

        return $value;
    }

    function deleteWithRelated()
    {
        $models = self::findAllByAttributes(['user_group_id' => $this->user_group_id, 'type' => $this->type, 'courier_country_group_id' => $this->courier_country_group_id]);

        /* @var $model self */
        foreach($models AS $model)
        {
            foreach($model->courierPriceUniversalValuesAll AS $cpuv)
            {
                $cpuv->price->deleteValues(false);
                $cpuv->price->delete();
                $cpuv->delete();
            }
            $model->delete();
        }

        return true;
    }

}