<?php

class ColissimoClient extends GlobalOperatorWithReturn
{
    const URL = 'https://ws.colissimo.fr/sls-ws/SlsServiceWS?wsdl';
    const URL_POINTS = 'https://ws.colissimo.fr/pointretrait-ws-cxf/PointRetraitServiceWS/2.0?wsdl';
    const URL_TT = 'https://www.coliposte.fr/tracking-chargeur-cxf/TrackingServiceWS?wsdl';
    const ACCOUNT_NO = '825179';
    const PASSWORD = 'KAAB2018';

    public $sender_name;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_tel;
    public $sender_email;

    public $receiver_name;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;
    public $package_content;
    public $package_value;

    public $login;

    public $local_id;

    public $with_signature = false;

    public $return = false;

    public $insurance_value = 0;
    public $cod = false;

    public $user_id;

    public $delivery_point_id = false;
    public $delivery_point_type = false;


    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    protected function frZipCode2CountryCode($country_code_oryginal, $zip_code)
    {
        $country_code_oryginal = strtoupper($country_code_oryginal);

        if($country_code_oryginal != 'FR')
            return $country_code_oryginal;

        $zip_code_base = substr($zip_code, 0 ,3);

        switch($zip_code_base)
        {
            case '971':
                return 'GP';
                break;
            case '972':
                return 'MQ';
                break;
            case '973':
                return 'GF';
                break;
            case '974':
                return 'RE';
                break;
            case '976':
                return 'YT';
                break;
            default:
                return $country_code_oryginal;
                break;
        }

    }

    protected function _createShipment($returnError = false, $modifyTelNumber = false)
    {
//        if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse('Not active!');
//            } else {
//                return false;
//            }

        $delivery_point_ok = false;

        $overseas = false;
        if($this->receiver_country_id == CountryList::COUNTRY_FR) {
          if(self::frZipCode2CountryCode($this->receiver_country, $this->receiver_zip_code) != 'FR')
              $overseas = true;
        }

        if ($this->return) {

            $productCode = 'CORE';

        } else {

            if ($this->receiver_country_id == CountryList::COUNTRY_FR && !$overseas) {

                if($this->delivery_point_id) {
                    if ($this->delivery_point_type == 'BPR') {
                        $productCode = 'BPR';
                        $delivery_point_ok = true;
                    } else if ($this->delivery_point_type == 'A2P') {
                        $delivery_point_ok = true;
                        $productCode = 'A2P';
                    }
                } else {
                    if ($this->with_signature)
                        $productCode = 'COL';
                    else
                        $productCode = 'DOM';
                }

            } else if (in_array($this->receiver_country_id, CountryList::getUeList()) && !$overseas) {
                if ($this->with_signature)
                    $productCode = 'DOS';
                else
                    $productCode = 'DOM';
            } else {
                if ($this->with_signature)
                    $productCode = 'CDS';
                else
                    $productCode = 'COM';
            }
        }

        if($this->delivery_point_id && !$delivery_point_ok)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('Invalid Colissimo Service!');
            } else {
                return false;
            }
        }

        $data = new stdClass();

        $data->contractNumber = self::ACCOUNT_NO;
        $data->password = self::PASSWORD;

        $data->outputFormat = new stdClass();
        if($this->return)
            $data->outputFormat->outputPrintingType = 'PDF_A4_300dpi';
        else
            $data->outputFormat->outputPrintingType = 'PDF_10x15_300dpi';
        $data->letter = new stdClass();
        $data->letter->service = new stdClass();
        $data->letter->service->productCode = $productCode;
        $data->letter->service->depositDate = date('Y-m-d');
        $data->letter->service->orderNumber = $this->local_id;
        $data->letter->service->transportationAmount = 100;
        $data->letter->service->totalAmount = 100;
        $data->letter->service->commercialName = $this->sender_name;
        $data->letter->service->returnTypeChoice = 2;
        $data->letter->parcel = new stdClass();
        $data->letter->parcel->insuranceValue = $this->insurance_value;
        $data->letter->parcel->weight = $this->package_weight;
        $data->letter->parcel->COD = ($this->cod > 0);
        if($this->cod > 0)
            $data->letter->parcel->CODAmount = $this->cod * 100; // in cents

        if($this->delivery_point_id) {
            $data->letter->parcel->pickupLocationId = $this->delivery_point_id;
            $data->letter->service->commercialName = 'KAAB';
        }

        if(!in_array($this->receiver_country_id, CountryList::getUeList()) OR $overseas)
        {
            $data->letter->customsDeclarations = new stdClass();
            $data->letter->customsDeclarations->includeCustomsDeclarations = 1;
            $data->letter->customsDeclarations->contents = new stdClass();
            $data->letter->customsDeclarations->contents->article = new stdClass();
            $data->letter->customsDeclarations->contents->article->description = $this->package_content;
            $data->letter->customsDeclarations->contents->article->quantity = 1;
            $data->letter->customsDeclarations->contents->article->weight = $this->package_weight;
            $data->letter->customsDeclarations->contents->article->value = $this->package_value;
            $data->letter->customsDeclarations->contents->category = new stdClass();
            $data->letter->customsDeclarations->contents->category->value = 5;
        }

        $customName = 'Swiat Przesylek  / '.$this->login;

        if(in_array($this->user_id, [947, 948, 2036, 2037]))
            $customName = '';

        if($this->return)
        {
            $this->receiver_name = $customName;
            $this->receiver_country = 'FR';
            $this->receiver_city = 'ERSTEIN';
            $this->receiver_zip_code = '67150';
            $this->receiver_address1 = 'c/o Asendia Germany - 825179';
            $this->receiver_address2 = 'PFC ERSTEIN';
            $this->receiver_tel = '+48221221218';
            $this->receiver_email = 'cs@swiatprzesylek.pl';
        } else {
            $this->sender_name = $customName;
            $this->sender_country = 'FR';
            $this->sender_city = 'ERSTEIN';
            $this->sender_zip_code = '67150';
            $this->sender_address1 = 'c/o Asendia Germany - 825179';
            $this->sender_address2 = 'PFC ERSTEIN';
            $this->sender_tel = '+48221221218';
            $this->sender_email = 'cs@swiatprzesylek.pl';
        }


        $data->letter->sender = new stdClass();
        $data->letter->sender->address = new stdClass();
        $data->letter->sender->address->companyName = $this->sender_name;
        $data->letter->sender->address->line2 = $this->sender_address1;
        $data->letter->sender->address->line3 = $this->sender_address2;
        $data->letter->sender->address->countryCode = self::frZipCode2CountryCode($this->sender_country, $this->sender_zip_code);
        $data->letter->sender->address->city = $this->sender_city;
        $data->letter->sender->address->zipCode = $this->sender_zip_code;
        $data->letter->sender->address->phoneNumber = $this->sender_tel;
        $data->letter->sender->address->email = $this->sender_email;
        $data->letter->sender->address->language = 'EN';
        $data->letter->sender->senderParcelRef = $this->package_content;


        $mobileNumber = $this->receiver_tel;

        if($modifyTelNumber && in_array($this->user_id, [8,3145]))
        {
            $mobileNumber2 = ltrim($mobileNumber, '0');
            $mobileNumber2 = preg_replace("/[^0-9]/", "", $mobileNumber2);

            if (in_array($mobileNumber2[0], [6, 7]) && strlen($mobileNumber2) == 9) {
                $mobileNumber = '0' . $mobileNumber2;
            } else if (substr($mobileNumber2, 0, 2) == '33') {
                $mobileNumber = '+' . $mobileNumber2;
            } else if (strlen($mobileNumber2) <= 8) {
                $mobileNumber = str_pad('06' . $mobileNumber2, 10, "0");
            } else {
                $mobileNumber = str_pad($mobileNumber2, 10, "0");
            }
        }

        $data->letter->addressee = new stdClass();
        $data->letter->addressee->address = new stdClass();
        $data->letter->addressee->address->companyName = $this->receiver_name;
        $data->letter->addressee->address->firstName = $this->receiver_name;
        $data->letter->addressee->address->lastName = $this->receiver_name;
        $data->letter->addressee->address->line2 = $this->receiver_address1;
        $data->letter->addressee->address->line3 = $this->receiver_address2;
        $data->letter->addressee->address->countryCode = self::frZipCode2CountryCode($this->receiver_country, $this->receiver_zip_code);
        $data->letter->addressee->address->city = $this->receiver_city;
        $data->letter->addressee->address->zipCode = $this->receiver_zip_code;
        $data->letter->addressee->address->phoneNumber = $this->receiver_tel;
        $data->letter->addressee->address->mobileNumber = in_array($productCode, ['BPR', 'A2P']) ? $mobileNumber : '';
        $data->letter->addressee->address->email = $this->receiver_email;
        $data->letter->addressee->address->language = 'EN';

        $model = new self;

        $containerName = 'generateLabelRequest';
        $data2 = new stdClass;
        $data2->$containerName = $data;

        $resp = $model->_soapOperation('generateLabel', $data2, self::URL);

        if(!$modifyTelNumber && $resp->errorCode == 30221) {
            MyDump::dump('colissimo_tel_fix.txt', print_r($this->local_id,1));
            return $this->_createShipment($returnError, true);
        }

        $label = $resp->data->labelResponse->label;
        $ttNo = $resp->data->labelResponse->parcelNumber;

        if ($resp->success == true) {
            $return = [];

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImageBlob($label);

            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            // for return, split a4 in half
            if($this->return)
            {
                $imgWidth = $im->getImageWidth();
                $imgHeight = $im->getImageHeight();

                $pagesNumber = $im->getNumberImages();

                if($pagesNumber > 1)
                    $im->setIteratorIndex(1);

                $im2 = clone $im;

                if($pagesNumber > 1)
                    $im2->setIteratorIndex(1);

                $im2->cropImage($imgWidth / 2, $imgHeight , 0, 0);
                $im->cropImage($imgWidth / 2, $imgHeight , $imgWidth / 2, 0);

                $im->trimImage(0);
                $im->stripImage();
//
                $im2->trimImage(0);
                $im2->stripImage();

                $outputLabel = [
                    $im2->getImageBlob(),
                    $im->getImageBlob(),
                ];
            } else {
                $im->trimImage(0);
                $im->stripImage();

                $outputLabel = $im->getImageBlob();
            }

            $return[] = GlobalOperatorResponse::createSuccessResponse($outputLabel, $ttNo);
            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }

    }



    protected function _soapOperation($method, $data, $url)
    {
        $mode = [
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context' => stream_context_create(
                [
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false
                    ],
                ]
            )
        ];

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        try {

            $client = new \KeepItSimple\Http\Soap\MTOMSoapClient($url, $mode);
            $resp = $client->__soapCall($method, [$method => $data]);

        } catch (Exception $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('colissimo.txt', 'E:'.print_r($client->__getLastRequest(),1));
            MyDump::dump('colissimo.txt', 'E:'.print_r($client->__getLastResponse(),1));
            return $return;
        }

        MyDump::dump('colissimo.txt', print_r($client->__getLastRequest(),1));
        MyDump::dump('colissimo.txt', print_r($client->__getLastResponse(),1));


        if($resp->return->messages->type != 'ERROR')
        {
            $return->success = true;
            $return->data = $resp->return;
        } else {

            $return->error = $resp->return->messages->id .'/'. $resp->return->messages->messageContent;
            $return->errorCode = $resp->return->messages->id;
            $return->errorDesc = $resp->return->messages->messageContent;
        }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnError)
    {

        $model = new self;

        $model->user_id = $courierInternal->courier->user_id;


        if($uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_COLISSIMO_SIGN)
            $model->with_signature = true;

//        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id);

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->package_content = $courierInternal->courier->package_content;

        $model->package_value = $courierInternal->courier->getPackageValueConverted('EUR');
        $model->cod = $courierInternal->courier->cod_value;

        $model->local_id = $courierInternal->courier->local_id;

        $model->login = $courierInternal->courier->user->login;

        if($courierInternal->courier->_points_data)
        {
            $pointDetails = DeliveryPoints::getPointById($courierInternal->courier->_points_data[0], true, false, DeliveryPoints::TYPE_COLISSIMO);

            if(!$pointDetails)
                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => 'Point not found!',
                    )];
                    return $return;
                } else {
                    return false;
                }

            $model->delivery_point_id = $pointDetails[0];
            $model->delivery_point_type = $pointDetails[1];
        }

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, CourierLabelNew $cln, $returnError = false)
    {

        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model = new self;
        $model->user_id = $courierTypeU->courier->user_id;

        if($courierTypeU->courierUOperator->uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_COLISSIMO_SIGN)
            $model->with_signature = true;

//        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id);

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierTypeU->courier->getWeight(true);
        $model->package_content = $courierTypeU->courier->package_content;

        $model->package_value = $courierTypeU->courier->getPackageValueConverted('EUR');
        $model->cod = $courierTypeU->courier->cod_value;

        $model->local_id = $courierTypeU->courier->local_id;

        $model->login = $courierTypeU->courier->user->login;

        if($courierTypeU->courier->_points_data)
        {
            $pointDetails = DeliveryPoints::getPointById($courierTypeU->courier->_points_data[0], true, false, DeliveryPoints::TYPE_COLISSIMO);

            if(!$pointDetails)
                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => 'Point not found!',
                    )];
                    return $return;
                } else {
                    return false;
                }

            $model->delivery_point_id = $pointDetails[0];
            $model->delivery_point_type = $pointDetails[1];
        }

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierReturn(CourierTypeReturn $courierReturn, CourierLabelNew $cln, $returnErrors = false)
    {

        $from = $courierReturn->courier->senderAddressData;
//        $to = $courierReturn->courier->receiverAddressData;

        $model = new self;
        $model->return = true;

        $model->user_id = $courierReturn->courier->user_id;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;

//        $model->receiver_name = $to->getUsefulName(true);
//        $model->receiver_zip_code = $to->zip_code;
//        $model->receiver_city = $to->city;
//        $model->receiver_address1 = $to->address_line_1;
//        $model->receiver_address2 = $to->address_line_2;
//        $model->receiver_country = $to->country0->code;
//        $model->receiver_country_id = $to->country_id;
//        $model->receiver_tel = $to->tel;
//        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierReturn->courier->getWeight(true);
        $model->package_content = $courierReturn->courier->package_content;

        $model->package_value = $courierReturn->courier->getPackageValueConverted('EUR');

        $model->local_id = $courierReturn->courier->local_id;

        $model->login = $courierReturn->courier->user->login;


        return $model->_createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        $model = new self;

        $data = new stdClass;
        $data->accountNumber = self::ACCOUNT_NO;
        $data->password = self::PASSWORD;
        $data->skybillNumber = $no;

        $resp = $model->_soapOperation('track', $data, self::URL_TT);

        $statuses = new GlobalOperatorTtResponseCollection();

        if($resp->success)
        {
            $date = (string) $resp->data->eventDate;
            $date = substr($date,0,10).' '.substr($date,11,8);

            $statuses->addItem((string) $resp->data->eventLibelle, $date, (string) $resp->data->eventSite);
        }

        return $statuses;
    }

    /**
     * @param $address
     * @param $zipCode
     * @param $city
     * @param $country
     * @return DeliveryPoints[]
     * @throws CDbException
     * @throws CException
     */
    public static function getPoints($address, $zipCode, $city, $country)
    {

        $CACHE_NAME = 'COLISSIMO_POINTS_'.$address.'_'.$zipCode.'_'.$city.'_'.$country;

        $data = Yii::app()->cache->get($CACHE_NAME);

        if($data === false) {

            $data = new stdClass;

            $data->accountNumber = self::ACCOUNT_NO;
            $data->password = self::PASSWORD;

            $data->address = $address;
            $data->zipCode = $zipCode;
            $data->city = $city;
            $data->countryCode = $country;

            $data->shippingDate = date('d/m/Y');
            $data->lang = 'EN';

            $model = new self;
            $resp = $model->_soapOperation('findRDVPointRetraitAcheminement', $data, self::URL_POINTS);
            $data = [];

            if ($resp->success && $resp->data->errorCode == 0) {
                foreach ($resp->data->listePointRetraitAcheminement AS $point) {
                    $dp = DeliveryPoints::getUpdateDeliveryPoints(DeliveryPoints::TYPE_COLISSIMO, $point->identifiant, trim($point->adresse1 . ' ' . $point->adresse2 . ' ' . $point->adresse3), $point->codePostal, $point->localite, $point->codePays, $point->nom, $point->coordGeolocalisationLatitude, $point->coordGeolocalisationLongitude, $point->typeDePoint);
                    $data[] = $dp;
                }
            }
            Yii::app()->cache->set($CACHE_NAME, $data, 60*60*24);
        }

        return $data;
    }

}
