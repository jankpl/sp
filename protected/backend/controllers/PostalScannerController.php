<?php

class PostalScannerController extends Controller
{


    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('*'),
                'expression' => Admin::AUTHORITY_OPERATOR.' == Yii::app()->user->authority',
            ),
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_COURIER_SCANNER.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($hash = NULL, $a = false)
    {
        Yii::app()->getModule('postal');

        $step = 1;
        $selectedStatusName = '';
        $automatic = $a;

        $sessionData = Yii::app()->session['scannerData'];
        $model = new PostalScannerForm();


        $statusList = PostalStat::model()->findAllByAttributes(['id' => [405, 152,150, 261, 331, 113, 99]]);

        if($hash !== NULL && is_array($sessionData[$hash]))
        {
            $model->scenario = 'step2';
            $step = 2;
            $model->setAttributes($sessionData[$hash]);

            if(isset($_POST['PostalScannerForm']['package_ids']))
            {
                $automatic = $_POST['automatic'];

                $packagesIds = $_POST['PostalScannerForm']['package_ids'];
                $packagesIds = explode(PHP_EOL, $packagesIds);

                $model->package_ids = $packagesIds;

                if($model->validate())
                {
                    $postalModels = $model->returnPostalModels();

                    $changedCounter = 0;
                    $errorPackages = [];
                    foreach($postalModels AS $key => $postalModel) {

                        if (is_array($postalModel) && S_Useful::sizeof($postalModel) == 1)
                            $postalModel = $postalModel[0];

                        if (is_array($postalModel) && S_Useful::sizeof($postalModel) > 1)
                        {
                            $errorPackages[] = $model->package_ids[$key].' : More than one package found with this ID!';
                        }
                        else if (!($postalModel instanceof Postal)) {
                            $errorPackages[] = $model->package_ids[$key].' : Package not found!';
                        } else {
                            /* @var $postalModel Postal */
                            if ($postalModel->changeStat($model->stat_id, 1000 + Yii::app()->user->id, NULL, false, $model->location)) {
                                $changedCounter++;
//                                Yii::app()->user->setFlash('success', 'Status has been set! <a class="btn btn-mini" href="' . Yii::app()->getBaseUrl(true) . '/tt/' . $courierModel->local_id . '" target="_blank">T&T details</a>');

                            } else {
                                $errorPackages[] = $model->package_ids[$key].' : Status not changed!';
//                                Yii::app()->user->setFlash('error', 'Status has NOT been set! Please try again....');
                            }

                        }
                    }

                    if(S_Useful::sizeof($errorPackages))
                        Yii::app()->user->setFlash('error', implode('<br/>',$errorPackages));

                    if($changedCounter)
                        Yii::app()->user->setFlash('success', 'Packages with statuses changed: '.$changedCounter);

                    $this->redirect(['/postalScanner/index', 'hash' => $hash, 'a' => $automatic]);
                    Yii::app()->end();
                }
            }

        }
        else if(isset($_POST['PostalScannerForm']))
        {
            $model->setAttributes($_POST['PostalScannerForm']);

            if($model->validate()) {


                $hash = uniqid();

                $config = [
                    'stat_id' => $model->stat_id,
                    'location' => $model->location,
                ];

                if (!is_array($sessionData))
                    $sessionData = [];

                $sessionData[$hash] = $config;

                Yii::app()->session['scannerData'] = $sessionData;

                $this->redirect(['/postalScanner/index', 'hash' => $hash]);
            }
        }

        $this->render('index',
            [
                'step' => $step,
                'model' => $model,
                'automatic' => $automatic,
                'statusList' => $statusList,
            ]);
    }
}