<?php
/* @var $import CourierImportManager */
/* @var $couriers Courier[] */
?>
<div class="text-center">
    <a href="<?php echo Yii::app()->createUrl('/courier/courierDomestic/import');?>" class="btn btn-warning"><?php echo Yii::t('courier', 'Od nowa');?></a>
    <br/>
</div>


<hr/>

<?php
$this->widget('FlashPrinter');
?>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-labels',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courierDomestic/getLabels', ['hash' => $hash]),
    'htmlOptions' => ['class' => 'do-not-block',
        'target' => Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL ? '_blank' : '_self'],
));
?>

<table class="table table-striped table-condensed" style="width: 60%; margin: 0 auto;">
    <tr>
        <td><?php echo Yii::t('courier', '#ID');?></td>
        <td><?php echo Yii::t('label', 'Label ready');?></td>
        <td><?php echo Yii::t('label', 'Get label');?> <a href="#" data-toggle-labels="true"><i class="glyphicon glyphicon-check"></i></a></td>
    </tr>
    <?php
    if(is_array($couriers))
        foreach($couriers AS $item):
            ?>
            <tr data-courier-local-id="<?= $item->local_id;?>" data-label-stat="<?= $item->courierTypeDomestic->courierLabelNew->stat;?>" data-courier-temp-id="<?= $item->id;?>" data-action="<?= $item->courierTypeDomestic->courierLabelNew->stat == CourierLabelNew::STAT_NEW ? 'true' : 'false';?>">
                <td><a href="<?php echo Yii::app()->createUrl('/courier/courier/view', ['urlData' => $item->hash]);?>"><?= $item->local_id;?></a></td>
                <td class="label-stat"><?= $item->courierTypeDomestic->courierLabelNew->stat == CourierLabelNew::STAT_SUCCESS ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'?></td>
                <td class="label-checkbox"><?= in_array($item->courierTypeDomestic->courierLabelNew->stat, [CourierLabelNew::STAT_WAITING_FOR_PARENT, CourierLabelNew::STAT_NEW]) ? '<img src="'.Yii::app()->baseUrl.'/images/layout/preloader.gif" style="width: 10px; height: 10px;"/>' : '<input type="checkbox" data-get-label="true" data-label-checkbox="true" name="label['.$item->id.']"/>';?></td>
            </tr>

        <?php endforeach; ?>
</table>
<hr/>
<div class="text-center" id="generate-labels-action">
    <?php echo CHtml::dropDownList('Courier[type]','',LabelPrinter::getLabelGenerateTypes(), array('style' => 'width: 250px; display: inline-block;'));?>

    <?php
    echo TbHtml::submitButton('Generate labels', array('data-generate-labels' => 'true'));?>
</div>
<div class="text-center" id="work-in-progress">
    <img src="<?php echo Yii::app()->baseUrl;?>/images/layout/preloader.gif" />
    <h3>0/<?php echo S_Useful::sizeof($couriers);?></h3>
</div>
<?php
$this->endWidget();
?>

<script>
    LABEL_OK = '<i class="glyphicon glyphicon-ok"></i>';
    LABEL_FAIL = '<i class="glyphicon glyphicon-remove"></i>';

    var $labelsAction = $('#generate-labels-action');
    var $workInProgress = $('#work-in-progress');

    $(document).ready(function(){

        var $collection = $('[data-courier-local-id][data-action=true]');

        var total = $('[data-courier-local-id]').length;

        if($collection.length) {
            $labelsAction.hide();
            $workInProgress.show();
            getLabel($collection, 0, total, 0);
        }
        else
        {
            $labelsAction.show();
            $workInProgress.hide();
        }

    });

    function getLabel($collection, counter, total, totalCounter)
    {

        var $item = $collection.eq(counter);

        $.ajax({
            method: "POST",
            url: '<?= Yii::app()->createUrl('/courier/courierDomestic/quequeLabelByLocalId'); ?>',
            data: {localId: $item.attr('data-courier-local-id')},
            dataType: 'json'
        })
            .done(function (response) {


                if(response.request) {

                    updateItemStat($item, response.stat);

                    if(response.ids)
                        if((response.ids).length)
                        {
                            for(var i = 0; i < (response.ids).length; i++)
                            {
                                var temp = (response.ids)[i];
                                updateItemStat( $('[data-courier-temp-id=' + temp.id + ']'), temp.stat);
                                totalCounter++;
                            }
                        }

                    $workInProgress.find('h3').html(totalCounter + '/'  + total);

                    counter++;

                    if (counter < $collection.length)
                        setTimeout(function(){
                            getLabel($collection, counter, total, totalCounter);
                        },1000);
                    else
                    {
                        $workInProgress.hide();
                        $labelsAction.show();
                    }
                } else {
                    alert("Błąd!");
                }

            })
            .fail(function () {
//                alert("sie nie udało...");
            });

    }


    function updateItemStat($item, stat)
    {

        var $checkbox = $('<input>');
        $checkbox.attr('type', 'checkbox');
        $checkbox.attr('data-label-checkbox', 'true');

        if(stat == <?= CourierLabelNew::STAT_SUCCESS;?>) {

            $checkbox.attr('name', 'label[' + $item.attr('data-courier-temp-id') + ']');

            $item.find('.label-checkbox').html('');
            $item.find('.label-checkbox').append($checkbox);
            $item.find('.label-stat').html(LABEL_OK);
        }
        else
        {
            $item.find('.label-stat').html(LABEL_FAIL);
        }
    }

    var propToggleTrue = true;
    $('[data-toggle-labels]').on('click', function(){

        $('[data-label-checkbox]').each(function(i,v){
            $(v).prop("checked", propToggleTrue);
        });

        propToggleTrue = !propToggleTrue;
    })
</script>