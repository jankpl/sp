(function($) {

    var ignoreFirstUpdate = true;

    var viewTimeoutHandler;
    var formActive = true;

    $(document).ready(function(){

        $("[data-radio-nice]").on('click', function(){
            $('[data-radio-nice="' + $(this).attr('data-radio-nice') + '"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
            $(this).addClass('active').addClass('btn-primary').removeClass('btn-default');

            var $radio = $("input[name='" + $(this).attr('data-radio-nice') + "'][value=" + parseInt($(this).attr('data-value-id')) + "]");
            $radio.trigger('click');
        });

        $("[data-radio-nice]").each(function(i,v)
        {
            $radio = $("input[name='" + $(this).attr('data-radio-nice') + "'][value=" + parseInt($(this).attr('data-value-id')) + "]");
            if($radio.is(':checked'))
                $(this).addClass('active').addClass('btn-primary').removeClass('btn-default');

        });

        $('[data-sp-points-selector]').on('change', updateSpPoints);

        $('[data-show-address-more]').on('click', showAddressMore);
        $('[data-address-line-2]').on('change', function(){
            $(this).closest('[data-address-more]').slideDown();
            $(this).closest('.row').prev().find('[data-show-address-more]').hide();
        });
        $('#postal-form').on('submit', function(e)
        {
            if(!formActive) {
                e.preventDefault();
                $('.overlay').remove();
                return false;
            }
        });

        $('[name="country_suggester_trigger"]').on('change', updateView);
        $('[data-dependency]').on('change', updateView);

        $('[data-bootstrap-checkbox]').checkboxpicker({
            offClass : 'btn-default btn-lg',
            onClass : 'btn-primary btn-lg',
            defaultClass: 'btn-lg',
        });

    });



    var FormControl = (function() {
        "use strict";

        var that = {};

        that.submit = function () {
            var $form = $("#courier-ooe-form");

            var $input = $('<input>');
            $input.attr('name', 'forceReload');
            $form.append($input);

            formActive = true;
            $form.submit();
        };
        return that;
    }());

    var Overlay = (function() {
        "use strict";

        var that = {};
        var $overlay = $("<div>");
        var $textBottom = $("<div>");
        var $textTop = $("<div>");
        var $loader = $("<div>");

        that.isShow = function(){
            return $("#overlay").is(':visible');
        };

        that.show = function() {
            var $form = $("#postal-form");

            $overlay.addClass('overlay');


            $loader.addClass('loader');

            $overlay.append($loader);


            $textTop.addClass('loader-text-top');
            $textTop.html(yii.loader.textTop);


            $textBottom.addClass('loader-text-bottom');
            $textBottom.html(yii.loader.textBottom);

            $overlay.append($textTop);
            $overlay.append($textBottom);

            $form.css('position','relative');
            $form.append($overlay);

            $textTop.hide().delay(1000).fadeIn();
            $textBottom.hide().delay(3000).fadeIn();
        };

        that.hide = function() {
            $overlay.remove();
        };

        return that;
    }());



    function disableButton($button)
    {
        $button.prop('disabled', true);
        $button.addClass('disabled');
    }

    function enableButton($button)
    {
        $button.prop('disabled', false);
        $button.removeClass('disabled');
    }

    function showAddressMore()
    {
        $(this).parent().parent().parent().find('[data-address-more]').slideDown();
        $(this).remove();
    }

    function updateSpPoints()
    {

        $.ajax({
            url: yii.urls.spPointsDetails,
            data: { id: $('[data-sp-points-selector]').val()},
            method: 'POST',
            dataType: 'json',
            success: function(result){
                $("#sp-points-details").html(result);
            },
            error: function(e){
                //alert(yii.loader.error);
                FormControl.submit();
            }
        })
    }

    function updateView(force) {
        //if (ignoreFirstUpdate) {
        //    ignoreFirstUpdate = false;
        //    return true;
        //}

        if (!force || force === undefined || force.target) {

            if ($("input[data-dependency]").filter(function () {
                    return $.trim($(this).val()).length == 0
                }).length > 0
                ||
                $('input[name="Postal[size_class]"]:checked').length == 0
                ||
                $('input[name="Postal[weight]"]').val() == ''
                ||
                $('input[name="Postal[postal_type]"]:checked').length == 0
            )
                return true; // not all required fileds are filled
        }

        $('[data-continue]').hide();
        disableButton($('[data-continue-button]'));

        clearTimeout(viewTimeoutHandler);
        viewTimeoutHandler = setTimeout(_updateView, 500);
    }

    function _updateView()
    {
        Overlay.show();
        checkIfAvailableAndGetPrice();
    }



    function checkIfAvailableAndGetPrice()
    {
        var receiver_country_id = $('[data-country="receiver"]').val();
        var postal_type = $('[name="Postal[postal_type]"]:checked').val();
        var weight = $('[name="Postal[weight]"]').val();
        var size_class = $('[name="Postal[size_class]"]:checked').val();

        $.ajax({
            url: yii.urls.getPrice,
            data: { receiver_country_id: receiver_country_id, postal_type: postal_type, weight: weight, size_class: size_class },
            method: 'POST',
            dataType: 'json',
            success: function(result){


                if(result.available)
                {
                    $('[data-continue]').show();
                    enableButton($('[data-continue-button]'));
                    $('[data-not-available]').hide();
                    formActive = true;

                    $('#price').html(result.price);

                    Overlay.hide();

                } else {
                    $('[data-not-available]').show();
                    $('[data-continue]').hide();
                    disableButton($('[data-continue-button]'));
                    formActive = false;
                    Overlay.hide();
                }

                return result;
            },
            error: function(e){

                //alert(yii.loader.error);
                FormControl.submit();
            }
        });
    }

    //function updateServiceData()
    //{
    //    var $selected = $("[data-service-code]").find('option:selected');
    //
    //    var text = $selected.text();
    //    var service_id = $selected.val();
    //
    //    if(service_id == '')
    //        return;
    //
    //    $("[data-service-name]").val(text);
    //
    //    $.post(yii.urls.serviceDescription, {service_name: service_id}, function(result){
    //        $("#service-description").html(result.desc);
    //        $("#service-description").fadeIn();
    //
    //        if(result.palletways)
    //        {
    //            $("#palletways").show();
    //        } else {
    //            $("#palletways").hide();
    //        }
    //
    //
    //    } , 'json');
    //}


})(jQuery);