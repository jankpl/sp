<?php
/* @var $this PostalOperatorController */
/* @var $model PostalOperator */

$this->breadcrumbs=array(
	'Postal Operators'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PostalOperator', 'url'=>array('index')),
	array('label'=>'Manage PostalOperator', 'url'=>array('admin')),
);
?>

<h1>Create PostalOperator</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>