<?php

Yii::import('application.models._base.BaseAddressData');

class AddressData extends BaseAddressData
{
    public $addToContactBook;
    public $contactBookType;

    const PARAMS_BANK_ACCOUNT = 'bankAccount';
    const PARAMS_SMS_NOTIFICATION = 'smsNotification';

    const FAKE_EMAIL = 'empty@fake-empty123empty456empty.com';

    public $_paramsAlreadyEncoded = false;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * Function returning real email address on fake adress if option set to true and real address email is missing
     * @param bool|true $faceActive Whether to return fake email address if real is empty
     * @return string
     */
    public function fetchEmailAddress($fakeActive = true)
    {
        if($this->email == '' && $fakeActive)
            return self::FAKE_EMAIL;
        else
            return $this->email;
    }


    protected function beforeSave() {

        $country = CountryList::model()->cache(60)->findByPk($this->country_id);
        $this->country = mb_convert_case($country, MB_CASE_UPPER, "UTF-8");

        // uppercase everything
        foreach($this->attributes AS $key => $item)
        {
            // DO NOT UPPPERCASE PARAMS
            if($key == 'params')
                continue;

            if(is_string($item))
            {
                $this->$key = mb_strtoupper($item);
            }
        }

        $this->tel = str_replace(['(',')',' ','-'], '', $this->tel);

        if(!$this->_paramsAlreadyEncoded OR is_array($this->params))
            $this->params = self::serialize($this->params);

        if(mb_strlen($this->params) > 256) {
            Yii::log('Params too long for AddressData model! '.print_r($this->params,1), CLogger::LEVEL_ERROR);
            throw new Exception('Params too long for AddressData model!');
        }

        return parent::beforeSave();
    }

    public function afterFind(){

        $this->params = self::unserialize($this->params);

        parent::afterFind();

    }

    public function getCountryNameUniversal()
    {
        if($this->country == '')
        {
            $country = CountryList::model()->findByPk($this->country_id)->trName;
            $country = mb_convert_case($country, MB_CASE_UPPER, "UTF-8");
            return $country;
        } else
            return mb_convert_case($this->country, MB_CASE_UPPER, "UTF-8");

    }

    public function setAttributesWithPrefix($attributes, $prefix)
    {

        if(!is_array($attributes))
            return false;

        foreach($attributes AS $key => $value)
        {
            if(!preg_match('/^'.$prefix.'.*/', $key))
                continue;

            $key = str_replace($prefix,'', $key);

            if(!$this->hasAttribute($key))
                continue;

            $this->$key = $value;
        }

        return true;
    }

    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
        );
    }

    public function afterValidate()
    {
        if(!Yii::app()->user->isGuest && in_array(Yii::app()->user->id, [8, 2340])) // @30.05.2019 - special rule to change UK to NI for LightInTheBoxUser
        {
            if($this->country_id == CountryList::COUNTRY_UK && preg_match('/^BT/i', $this->zip_code))
                $this->country_id = CountryList::COUNTRY_ND;
        }

        parent::afterValidate();
    }

    public function beforeValidate()
    {
//        $this->address_line_1 = S_Useful::divideNumberAndLetter($this->address_line_1);
//        $this->address_line_2 = S_Useful::divideNumberAndLetter($this->address_line_2);
        if($this->country_id == CountryList::COUNTRY_PL)
        {
            if(strlen($this->zip_code) == 5)
                $this->zip_code = mb_substr($this->zip_code,0,2).'-'.mb_substr($this->zip_code,2);
        }
        else if($this->country_id == CountryList::COUNTRY_LV && strlen($this->zip_code) > 4)
        {
            $this->zip_code = preg_replace("/[^0-9]/", "", $this->zip_code);
        }
        else if($this->country_id == CountryList::COUNTRY_SE)
        {
            $this->zip_code = preg_replace("/[^0-9]/", "", $this->zip_code);
        }
        else if($this->country_id == CountryList::COUNTRY_HU)
        {
            $this->zip_code = preg_replace("/[^0-9]/", "", $this->zip_code);
        }

        return parent::beforeValidate();
    }


    public function rules() {

        Yii::app()->getModule('courier');

        $arrayValidate = array(

            array('email',
                'email',
                'except' => Courier::MODE_SAVE_NO_VALIDATE),


            array('name,
           company,
           city,
           address_line_1,
           address_line_2,
           zip_code,
            ', 'application.validators.lettersNumbersBasicValidator'
            , 'except' => Courier::MODE_SAVE_NO_VALIDATE
            ),

            array('tel,
            ', 'application.validators.telValidator'
            , 'except' => Courier::MODE_SAVE_NO_VALIDATE
            ),

            array('country_id, city, address_line_1, zip_code, tel', 'required'
            , 'except' => Courier::MODE_SAVE_NO_VALIDATE
            ),
            array('name', 'application.validators.eitherOneValidator', 'other' => 'company'
            , 'except' => Courier::MODE_SAVE_NO_VALIDATE),

            array('name,company,country,zip_code,city,address_line_1,address_line_2,tel', 'length', 'max'=>45
            , 'except' => Courier::MODE_SAVE_NO_VALIDATE),

            array('email,', 'length', 'max'=>50
            , 'except' => Courier::MODE_SAVE_NO_VALIDATE),

            array('country_id', 'safe'),

            array('id,name,company,country,zip_code,city,address_line_1,address_line_2,tel, email,', 'safe', 'on'=>'search'),

            array('params', 'safe')
        );


        $arrayFilter =
            array(
                array('country_id, date_entered, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'filter', 'filter' => array( $this, 'filterStripTags')),

                array('date_entered', 'default',
                    'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

                array('country_id, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'default', 'setOnEmpty' => true, 'value' => null),
            );


        $arrayOther = array(
            array('email', 'required', 'on' => 'with_email'),
        );

        return array_merge($arrayFilter, $arrayValidate, $arrayOther);

    }

    public function getCountry()
    {
        if($this->country === NULL AND $this->country_id !== NULL)
            $this->country = CountryList::model()->findByPk($this->country_id)->trName;

        return $this->country;
    }

    public function getUsefulName($doNotDuplicate = false, $companyFirstAndOnly = false)
    {
        if($companyFirstAndOnly)
            $doNotDuplicate = true;

        if($doNotDuplicate)
        {
            if($companyFirstAndOnly)
            {
                if($this->company == '')
                    $return = $this->name;
                else
                    $return = $this->company;

            } else {

                if($this->name == '')
                    $return = $this->company;
                else if($this->company == '')
                    $return = $this->name;
                else if($this->name == $this->company)
                    $return = $this->name;
                else
                {
                    $return = trim($this->name.' '.$this->company);
                }
            }
        }
        else
        {
            $return = trim($this->name.' '.$this->company);
        }

        return $return;
    }

    public function loadUserData()
    {
        if(Yii::app()->user->isGuest)
            return false;

        $user = User::model()->findByPk(Yii::app()->user->id);

        $this->name = trim($user->name.' '.$user->surname);
        $this->company = $user->company;
        $this->city = $user->city;
        $this->country = $user->country;
        $this->country_id = $user->country_id;
        $this->zip_code = $user->zip_code;
        $this->address_line_1 = $user->address_line_1;
        $this->address_line_2 = $user->address_line_2;
        $this->tel = $user->tel;
        $this->email = $user->email;
    }

    public static function loadDistinctData(array $ignoreAttributes = array())
    {
        $temp = new AddressData();
        $attributes = $temp->attributeNames();

        $toRemove = ["id" => 'id', 'date_entered' => 'date_entered', 'date_updated' => 'date_updated'];
        $toRemove = array_merge($toRemove, $ignoreAttributes);


        $attributes = array_diff($attributes, $toRemove);

        $models = self::model()->findAll(array(
            'select' => $attributes,
            'distinct' => true));


        foreach($models AS $key => $model)
        {
            $models[$key] = self::model()->findByAttributes(array_diff_key($model->attributes, $toRemove));
        }
        return $models;
    }

    /**
     * Unserialize data
     * @param $data
     * @return null|string
     */
    public static function serialize($data)
    {
        if($data == '')
            return NULL;

        $data = serialize($data);
        $data = base64_encode($data);
        return $data;
    }

    /**
     * Serialize data
     * @param $data
     * @return mixed|string
     */
    public static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = @unserialize($data);
        return $data;
    }

    /**
     * Make this AddressData active for sending SMS notifications
     */
    public function setSmsNotifyActive()
    {
//        if($this->params != '')
//            $params = self::unserialize($this->params);

        $params = $this->params;
        if(!is_array($params))
            $params = [];

        $params[self::PARAMS_SMS_NOTIFICATION] = 1;

        $this->params = $params;

//
//
//        $this->params = self::serialize($params);
    }

    /**
     * Check if this AddressData is active for sending SMS notifications
     * @return boolean
     */
    public function getIsSmsNotifyActive()
    {
        $params = $this->params;
//        if($params != '')
//            $params = self::unserialize($params);

        if($params[self::PARAMS_SMS_NOTIFICATION])
            return true;
        else
            return false;
    }


    /**
     * Save bank account data with this address data
     * @param $val string Bank account data
     */
    public function setBankAccount($val)
    {
//        if($this->params != '')
//            $params = self::unserialize($this->params);

        $params = $this->params;

        if(!is_array($params))
            $params = [];

        $params[self::PARAMS_BANK_ACCOUNT] = $val;

        $this->params = $params;

//
//
//        $this->params = self::serialize($params);
    }

    /**
     * Get bank account data for this address data
     * @return boolean
     */
    public function getBankAccount()
    {
        $params = $this->params;

        if(isset($params[self::PARAMS_BANK_ACCOUNT]))
            return $params[self::PARAMS_BANK_ACCOUNT];
        else
            return '';

    }

    public function getFullAddress($glue = "\r\n")
    {
        $text = [];
        $text[] = $this->address_line_1;
        if($this->address_line_2 != '')
            $text[] = $this->address_line_2;

        $text[]  = $this->zip_code.', '.$this->city;
        $text[]  = $this->country0->code;

        $text = implode($glue, $text);

        return $text;
    }

}