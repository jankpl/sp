<?php
$this->renderPartial('importEbay/_header',[
    'ebayClient' => $ebayClient,
    'step' => 1,
]);
?>
<?php
if($ebayClient->isLogged()):
    ?>

    <div class="alert alert-info">
        <form method="POST" style="margin: 0 0 -7px 0;" id="ebay-import">
            <table>
                <tr>
                    <td style="vertical-align: top; padding-top: 6px;"><?= Yii::t('courier_ebay','Od:');?></td>
                    <td><?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'name' => 'date_from',
                            'value' => $date_from,
                            'options' => array(
                                'firstDay' => 1,
                                'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                                'minDate' => $date_begining,      // minimum date
                                'maxDate' => $date_end,      // maximum date
                            ),
                            'htmlOptions' => array(
                                'size' => '5',         // textField size
                                'maxlength' => '10',    // textField maxlength
                                'id' => 'date-from',
                            ),
                        ));?></td>
                    <td style="vertical-align: top; padding-top: 6px; padding-left: 10px;"><?= Yii::t('courier_ebay','Do:');?></td>
                    <td><?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'name' => 'date_to',
                            'value' => $date_to,
                            'options' => array(
                                'firstDay' => 1,
                                'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                                'minDate' => $date_begining,      // minimum date
                                'maxDate' => $date_end,      // maximum date
                            ),
                            'htmlOptions' => array(
                                'size' => '5',         // textField size
                                'maxlength' => '10',    // textField maxlength
                                'id' => 'date-to',
                            ),
                        ));?></td>
                    <td style="vertical-align: top; padding-top: 6px; padding-left: 50px;"><?= Yii::t('courier_ebay','Pobierz:');?></td>
                    <td><?= CHtml::dropDownList('statEbay', $statEbay, EbayOrderItem::getStatShowEbayList(), ['style' => 'width: 170px;']);?></td>
                    <td style="padding-left: 10px;"><?= CHtml::dropDownList('stat', $stat, EbayOrderItem::getStatShowProcessedList(), ['style' => 'width: 130px;']);?></td>
                    <td style="vertical-align: top; width: 300px; text-align: right; padding-top: 3px;"><input type="submit" name="get-data" class="btn btn-success" value="<?= Yii::t('courier_ebay','Pobierz/odśwież dane');?>"/></td>
                </tr>
            </table>
        </form>
    </div>

    <?php if($number):?>
    <div class="alert alert-success" style="text-align: center;"><?= Yii::t('courier_ebay', 'Znaleziono zamówień: {n}', ['{n}' => $number]);?></div>
<?php endif; ?>


    <div class="form">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'import-data',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'stateful'=>false,
        ));
        ?>

        <table>

            <table class="table table-striped table-hover" id="ebay-orders-table" style="table-layout: fixed;">
                <colgroup>
                    <col width="20"></col>
                    <col width="30"></col>
                    <col width="100"></col>
                    <col width="250"></col>
                    <col width="130"></col>
                    <col width="50"></col>
                    <col width="60"></col>
                    <col width="60"></col>
                    <col width="60"></col>
                </colgroup>
                <thead>
                <tr>
                    <th>#</th>
                    <th style="text-align: center;"><?= Yii::t('courier_ebay','Id');?></th>
                    <th style="text-align: center;"><?= Yii::t('courier_ebay','Data');?></th>
                    <th><?= Yii::t('courier_ebay','Zawartość');?></th>
                    <th><?= Yii::t('courier_ebay','Kupujący');?></th>
                    <th style="text-align: center;"><?= Yii::t('courier_ebay','Kraj');?></th>
                    <th style="text-align: center;"><?= Yii::t('courier_ebay','eBay status');?></th>
                    <th style="text-align: center;"><?= Yii::t('courier_ebay','Package');?></th>
                    <th style="text-align: right;"><?= Yii::t('courier_ebay','Akcja');?></th>
                </tr>
                <tr >
                    <th></th>
                    <th style="text-align: center; font-weight: normal; font-size: 10px; cursor: help;"><span title="eBay Selling Manager Sales Record Number">[?]</span></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th><?= Chtml::dropDownList('_country_selector', '', CHtml::listData(CountryList::getActiveCountries(),'code','trName'), ['id' => 'country-selector', 'prompt' => '-', 'style' => 'width: 50px;']);?></th>
                    <th style="text-align: center; font-weight: normal; font-size: 10px;"><?= Yii::t('ebay_courier','Płatność / Wysyłka');?></th>
                    <th style="text-align: center; font-weight: normal; font-size: 10px;"><?= Yii::t('courier_ebay','jeżeli już przetworzono');?></th>
                    <th style="text-align: right;  font-weight: normal; font-size: 10px;">[<a href="#" data-inverse-selection="true"><?= Yii::t('courier_ebay','odwróć zaznaczenie');?></a>]</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if(!$number):
                    ?>
                    <tr>
                        <td colspan="9" style="text-align: center;"><?= Yii::t('courier_ebay', '-- brak zamówień --');?></td>
                    </tr>
                    <?php
                else:
                    $i = 1;
                    /* @var $order EbayOrderItem */
                    foreach($orders AS $orderKey => $order):
                        ?>
                        <tr data-country-code="<?= $order->addr_country; ?>">
                            <td><?= $i++;?></td>
                            <td style="vertical-align: middle; font-weight: bold;"><?= $order->salesRecordId;?></td>
                            <td style="vertical-align: middle; text-align: center;">
                                <span style="cursor: help; font-weight: bold;" title="<?= Yii::t('courier_ebay', 'data utworzenia');?>"><?= ebayClient::readableDate($order->createdTime);?></span><br/>
                                <span style="cursor: help;" title="<?= Yii::t('courier_ebay', 'data ostatniej modyfikacji');?>"><?= ebayClient::readableDate($order->lastModified);?></span>
                            </td>
                            <td style="font-size: 10px;">
                                <div style="text-align: right;"><strong><?= $order->orderID;?></strong></div>
                                <ul style="list-style-type: none; padding: 0; margin: 0;">

                                    <?php
                                    /* @var $transaction EbayTransactionItem */
                                    foreach($order->transactions AS $transaction):?>
                                        <li><span class="badge badge-warning"><?= $transaction->quantity;?> <?= Yii::t('courier_ebay', 'szt.');?></span> <span class="badge"><?= $transaction->transactionPrice;?> <?= $transaction->transactionCurrency;?></span> <?= ($transaction->shipped && !$order->common_track_id ? '<span class="badge badge-success">'.$transaction->shipp_track_id.' ('.$transaction->shippping_operator.')</span>' : ''); ?> <?= $transaction->title; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                                <div style="text-align: right;"><strong><?= Yii::t('courier_ebay', 'Łącznie');?>: <?= $order->quantity;?>  <?= Yii::t('courier_ebay', 'szt.');?>, <?= $order->total_price;?> <?= $order->price_currency;?></strong><?= ($order->statShipping == EbayOrderItem::STAT_SHIPPED && $order->common_track_id ? '<br/><span class="badge badge-success">'.$order->common_track_id.' ('.$order->common_operator_name.')</span>' : ''); ?> </div>
                            </td>
                            <td style="font-size: 10px;">
                                <strong><?= $order->addr_name;?></strong> (<?= $order->buyer_user;?>)<br/>
                                <?= $order->addr_street_1;?>, <?= $order->addr_street_2;?><br/>
                                <?= $order->addr_zip_code;?>, <?= $order->addr_city;?>, <?= $order->addr_country;?><br/>
                                <?= Yii::t('courier_ebay', 'tel.');?> <?= $order->addr_tel;?>, <?= $order->email;?>
                            </td>
                            <td style="vertical-align: middle; text-align: center;">
                                <?= $order->addr_country; ?>
                            </td>
                            <td style="vertical-align: middle; text-align: center;">
                                <?= $order->statPayment == EbayOrderItem::STAT_PAID ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'; ?> / <?= $order->statShipping == EbayOrderItem::STAT_SHIPPED ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'; ?>
                            </td>
                            <td style="vertical-align: middle; text-align: center;">
                                <?php if($order->local_package_id):?>
                                    <a class="btn" href="<?= Yii::app()->createUrl('/courier/courier/view', ['urlData' => $order->local_package_hash]);?>" target="_blank"><?= Yii::t('courier_ebay', 'Pokaż');?></a>
                                <?php else:?>
                                    -
                                <?php endif;?>
                            </td>
                            <td style="vertical-align: middle; text-align: right;">
                                <?php //if($order->statProcessed == EbayOrderItem::STAT_NOT_PROCESSED):?>
                                    <?php if($order->statShipping == EbayOrderItem::STAT_NOT_SHIPPED && $order->statPayment == EbayOrderItem::STAT_PAID): ?>
                                        <input data-import-ebay="true" type="checkbox" name="import[<?= $orderKey;?>]">
                                        <?php else: ?>
                                        <input type="button" class="btn btn-xs btn-warning" value="<?= Yii::t('courier_ebay', 'Wymuś');?>" data-unlock-id="<?= $orderKey;?>"/>
                                        <input data-import-ebay="true" data-import-id="<?= $orderKey;?>" type="checkbox" name="import[<?= $orderKey;?>]" disabled="disabled" title="<?= Yii::t('courier_ebay', 'To zamówienie spełnia wszyskich wymogów do wysyłki (nie zostało opłacone lub zostało już wysłane), ale możesz wymusić jego przetworzenie.');?>">
                                    <?php endif;?>
                                <?php //endif; ?>
                            </td>
                        </tr>

                    <?php endforeach;
                endif;
                ?>
                </tbody>
            </table>

            <?php if($more): ?>
                <div class="alert alert-warning"><?= Yii::t('ebay_courier', 'Nie pobrano wszyskich zamówień - doprecyzuj filtrację lub przetwórz powyższe zamówienia, aby zobaczyć kolejne.');?></div>
            <?php endif; ?>

            <?php if($number): ?>
                <div id="data-import-warning" class="alert alert-danger" style="display: none;"><?= Yii::t('ebay_courier', 'Znaznacz przynajmniej 1 zamówienie');?>. <?= Yii::t('ebay_courier', 'Maksymalna liczba importowanych zamówień za jednym razem to: {n}', ['{n}' => EbayImportOoe::MAX_IMPORT_NUMBER]);?></div>

                <div class="alert alert-warning"><?= Yii::t('ebay_courier', 'Poprawne wygenerowanie etykiety dla zamówienie spowoduje automatyczne przekazanie do eBay numeru paczki oraz zmianę statusu zamówienia na "wysłane".');?></div>

                <div class="navigate" style="clear: both;">
                    <input type="button" class="btn btn-primary" value="<?= Yii::t('ebay_courier', 'Importuj wybrane zamówienia');?>" name="import" id="importEbayData"/>
                </div>



                <script>
                    var $warning = $("#data-import-warning");

                    $('[data-import-ebay]').on('change', function(){
                        $warning.hide();
                    });

                    $("#importEbayData").on('click', function(){

                        var number = $('[data-import-ebay]:checked').length;

                        if(!number || number > <?= EbayImportOoe::MAX_IMPORT_NUMBER;?>)
                            $warning.show();
                        else
                        {
                            $(this).closest('form').submit();
                        }
                    });


                    $('[data-inverse-selection]').on('click', function(){

                        $('[data-import-ebay]').not(':disabled').each(function() {

                            $(this).is(':checked') ?  $(this).prop('checked', false) : $(this).prop('checked', true);
                        });
                    });

                    $('[data-unlock-id]').on('click', function(){
                       var id = $(this).attr('data-unlock-id');
                        $('[data-import-id=' + id + ']').attr('disabled', false);
                        $('[data-import-id=' + id + ']').prop('checked', true);
                        $(this).remove();
                    });

                    $('#country-selector').on('change', function(){

                        var selected_country = $(this).val();
                        var $rows = $('#ebay-orders-table').find('tbody').find('tr');

                        if(selected_country == '')
                        {
                            $rows.show();
                        } else {

                            $rows.hide();
                            $rows.filter(function (i, v) {
                                var $t = $(this);

                                if($t.attr('data-country-code') == selected_country)
                                    return true;
                                else
                                    return false;
                            })
                                .show();
                        }
                    })
                </script>
            <?php endif; ?>
            <?php $this->endWidget(); ?>

    </div>

    <?php
endif;
?>

