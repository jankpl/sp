<div class="form">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'address-data',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.2,
        ),
        'stateful'=>true,
    ));
    ?>

    <?php
    echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, $text, array('closeText' => false));
    ?>

    <div class="navigate">

        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'summary', 'class' => 'btn-small'));
        $this->endWidget();
        ?>
    </div>
</div><!-- form -->


