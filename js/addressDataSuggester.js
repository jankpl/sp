//Suggest just country
function bindAutocomplete_cl($field ,data)
{
    var blockTrigger = false;

    data = $.map(data, function(el) { return el });

    var real_val_id = $field.attr('data-real-val');
    var $realField = $('[data-autocomplete-id="' + real_val_id + '"]');
    var $suggesterFieldTrigger = $('[name="country_suggester_trigger"][data-suggester-type="' + real_val_id + '"]');


    $field.on('click', function()
    {
        $(this).attr('data-last', $(this).val());
        $(this).val('');
    });

    $field.on('focusout', function(){
        if($(this).val()=='')
            $(this).val($(this).attr('data-last'));
    });

    $realField.on('change', function(){

        var search = parseInt($realField.val());

        $.each(data, function(i,v)
        {
            if(parseInt(v.value) == search)
            {
                $field.val(v.label);
                return false;
            }
        });
    });

    var autoSelect = false;
    $field.autocomplete({
        minLength: 0,
        delay: 600,
        source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(data, function(value) {
                return matcher.test(value.label)
                    || matcher.test(value.desc);
            }));
        },
        response: function( event, ui) {

            if(ui.content.length == 1)
            {
                $field.autocomplete( "close" );
                $field.val( ui.content[0].label );
                $realField.val(ui.content[0].value).trigger('change');

                blockTrigger = true;

                $suggesterFieldTrigger.trigger('change');
                autoSelect = true;
            } else
                autoSelect = false;
        },
        //focus: function( event, ui ) {
        //    $field.val( ui.item.label );
        //    return false;
        //},
        select: function( event, ui ) {

            blockTrigger = true;

            $field.val( ui.item.label );
            $realField.val(ui.item.value).trigger('change');
            $suggesterFieldTrigger.trigger('change');
            return false;
        },
        change: function(event,ui)
        {
            if(blockTrigger) {
                blockTrigger = false;
                return true;
            }

            if (!autoSelect && ui.item==null)
            {
                $field.val("");
                $field.focus();
                $realField.val("");
                $suggesterFieldTrigger.trigger('change');
            }
        }
    })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
    };

    $field.on("click", function(){
        if (this.value == "")
        {
            $(this).autocomplete("search");
        }
    });



}

// suggest item from addressBook
$(document).ready(function()
{
    $("[data-autocomplete-cl]").each(function()
    {
        bindAutocomplete_cl($(this), $(this).data('content'));
    });

    $("[data-autocomplete-input-field]").each(function()
    {
        bindAutocomplete($(this), $(this).data('content'), $(this).attr('data-url'));
    });
});


function bindAutocomplete($field ,data, url)
{

    //data = $.map(data, function(el) { return el });

    var inputField = $field.attr('data-autocomplete-input-field');
    var idField = $field.attr('data-autocomplete-id-field');
    var button = $field.attr('data-autocomplete-button');
    // UPDATE 20.09.2016 - country list is checked every request
    //var countryList = $field.attr('data-countries');
    var type = $field.attr('data-type');

    var $inputField = $('#' + inputField);
    var $idField = $('#' + idField);
    var $button = $('#' + button);

    var placeholder = $inputField.attr('placeholder');

    $button.hide();
    var cache = {};
    $inputField.autocomplete({
        minLength: 3,
        delay: 600,
        //source: function (request, response) {
        //    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
        //    response($.grep(data, function(value) {
        //
        //        return matcher.test(value.label)
        //            || matcher.test(value.desc);
        //    }));
        //},
        source: function( request, response ) {
            var term = request.term;
            if ( term in cache ) {
                response( cache[ term ] );
                return;
            }
            request.countries = $field.attr('data-countries');
            request.type = type;


            jQuery.ajax({
                'url': url,
                'data': request,
                'dataType':'json',
                'type':'post',
                'success':function(result) {

                    if(!result.length) {
                        $inputField.attr('placeholder', '-- ' + $inputField.attr('data-not-found') + ' --');
                        $inputField.val('');
                        setTimeout(function(){
                            $('#' + inputField).attr('placeholder', placeholder);
                        },1000);
                    }

                    cache[ term ] = result;
                    response( result );
                },
                'error':function(result){

                    //$button.popover('hide');
                    console.log(result);
                },
                'cache':false
            });
        },
        focus: function( event, ui ) {
            $inputField.val( ui.item.label );
            return false;
        },
        select: function( event, ui ) {

            $inputField.val( ui.item.label );
            $idField.val( ui.item.value );

            $button.show();

            return false;
        },
        change: function(event,ui)
        {
            if (ui.item==null)
            {
                $inputField.val("");
                $inputField.focus();
                $idField.val( "" );
                $button.hide();
            }

        }

    })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append( "<a>" + item.label + "<br>" + item.desc + "</a>" )
            .appendTo( ul );
    };


    //$inputField.on("click", function(){
    //    if (this.value == "")
    //    {
    //        $(this).autocomplete("search");
    //    }
    //});
    $("[data-load-data-from-address-book-field]").unbind('click');
    $("[data-load-data-from-address-book-field]").on('click', function(){


        var inputField = $(this).attr('data-input-field');
        var fieldName = $(this).attr('data-load-data-from-address-book-field');
        var url = $(this).attr('data-load-url');

        var id_field = '#userAddressList-id';
        var i = $(this).attr('data-i');

        if(i !== undefined)
            id_field = id_field + '_' + i;
        else
            i = false;

        var id = $(id_field).val();

        if(id != '') {

            $(this).popover({placement: 'top'});
            $(this).popover('show');


            loadDataFromAddressBook(i, id, fieldName, url, $(this));
            $('#' + inputField).val('');
        }

    });

}

function loadDataFromAddressBook(i, vid, fieldName, url, $button)
{

    var field = '#' + fieldName + '_';
    if(i)
        field = field + i + '_';

    if(isNaN(vid))
        return;

    jQuery.ajax({
        'url':url,
        'data':{id : vid},
        'dataType':'json',
        'type':'get',
        'success':function(result) {

            $.each(result, function(i,v)
            {

                if(i == 'bankAccount')
                {
                    var curVal = $(field + i).val();

                    if(v != '' && curVal != v) {
                        $(field + i).val(v);
                        $(field + i).trigger("change");

                        if(curVal != '')
                            alert($('[data-bank-account-notify]').attr('data-bank-account-notify'));
                    }
                } else {

                    var trigger = false;

                    if(i == 'smsNotification')
                        $(field + i).prop('checked', v);
                    else {
                        if($(field + i).val() != v)
                            trigger = true;

                        $(field + i).val(v);
                        if(trigger)
                            $(field + i).trigger("change");
                    }
                }

            });


            $button.popover('hide');
            $button.popover('destroy');
            $button.hide();

        },'error':function(result){
            $button.popover('hide');
            console.log(result);
        },
        'cache':true
    });
}