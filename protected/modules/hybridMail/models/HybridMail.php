<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMail');

class HybridMail extends BaseHybridMail
{
    public $regulations;
    public $_first_status_date = NULL;
    public $document;

    public $_file_ext;

    const TMP_DIR = 'uptmp/';
    const FINAL_DIR = 'upclient/hm/';

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'bLocalIdGenerator' =>
                array('class'=>'application.models.bLocalIdGenerator'
                ),
//            'bRating'=>
//                array('class'=>'application.models.bChangeStat'
//                ),
            'bAddressDataSenderCompatilibity' =>
                array('class' => 'application.models.bAddressDataSenderCompatilibity'
                ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            )
        );
    }

    public static function getTmpDir($fileName)
    {
        $path = realpath(self::TMP_DIR);
        $path = $path.'/'.$fileName;
        return $path;
    }

    public static function getFinalDir($fileName)
    {
        $path = realpath(self::FINAL_DIR);
        $path = $path.'/'.$fileName;
        return $path;
    }

    public function changeStat($newStat, $author = 0, $status_date = NULL, $ownTransaction = false)
    {

        $status_date = HybridMailStat::convertDate($status_date);

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $newStat = HybridMailStat::model()->findByPk($newStat);
        if($newStat === null)
            return false;

//        if($this->courier_stat_id == $newStat->id)
//            return true;

        $statHistory = new HybridMailStatHistory();
        $statHistory->author = $author;
        $statHistory->hybrid_mail_id = $this->id;
        $statHistory->previous_stat_id = $this->stat;
        $statHistory->current_stat_id = $newStat->id;
        $statHistory->status_date = $status_date;


        $statHistory->validate();

        if(!$statHistory->save())
            $errors = true;

        $this->stat  = $newStat->id;

        if(!$this->save(false))
            $errors = true;

        if(!$errors) {
            $_StatNotifer = new S_StatNotifer(S_StatNotifer::SERVICE_HM, $this->id, $newStat->id, $status_date);
            $_StatNotifer->onStatusChange();
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();
            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();
            return true;
        }
    }


    public static function listCountries($user_group_id = NULL, $isSender = false)
    {

        if($isSender)
        {
            return CountryList::model()->findAll(array('order' => 'name ASC'));
        }


        $user_group_id = intval($user_group_id);


        $cmd = Yii::app()->db->createCommand();
        $cmd->select = 'country_list.name, country_list.id';
        $cmd->distinct = true;
        $cmd->from = 'country_list';
        $cmd->join('country_group_has_country','country_group_has_country.country = country_list.id');
        $cmd->join('country_group','country_group_has_country.group = country_group.id');
        $cmd->join('hybrid_mail_price_has_group','hybrid_mail_price_has_group.country_group = country_group.id');
        $cmd->where('country_list.stat = '.S_Status::ACTIVE);
        if($user_group_id === NULL OR !$user_group_id)
            $cmd->andWhere('hybrid_mail_price_has_group.user_group_id IS NULL');
        else
            $cmd->andWhere('hybrid_mail_price_has_group.user_group_id = ' . $user_group_id.' OR hybrid_mail_price_has_group.user_group_id IS NULL');

        $cmd->order('country_list.name DESC');



        $countries = CountryList::model()->findAllBySql($cmd->getText());

        return $countries;
    }

    public function getDaysFromStatusChange()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select("(SELECT TIMESTAMPDIFF(DAY , date_entered, CURRENT_TIMESTAMP) FROM hybrid_mail_stat_history
WHERE hybrid_mail_id = hybrid_mail.id ORDER BY date_entered DESC LIMIT 0,1)");
        $cmd->from("hybrid_mail");
        $cmd->andWhere("hybrid_mail.id = '".$this->id."'");
        return $cmd->queryScalar();
    }

    public function addAdditions($additions)
    {
        $this->_hybrid_mail_additions = $additions;
    }

    public function afterSave()
    {

        if($this->isNewRecord)
        {
            $this->isNewRecord = false;

            $statHistory = new HybridMailStatHistory;
            $statHistory->hybrid_mail_id = $this->id;
            $statHistory->previous_stat_id = NULL;
            $statHistory->current_stat_id = $this->stat == ''?HybridMailStat::NEW_ORDER:$this->stat;
            $statHistory->status_date = $this->_first_status_date;

            if(!$statHistory->save())
                throw new Exception(print_r($statHistory->getErrors(),1));
        }

        return parent::afterSave();
    }

    public function init()
    {
        if($this->file_hash === null)  $this->file_hash = $this->generateHash();

        return parent::init();
    }

    public function generateHash()
    {
        $hash = hash('sha256', microtime());

        return $hash;
    }

    protected function beforeSave() {

        return parent::beforeSave();
    }

    public function rules() {
        return array(

            array('stat',
                'default',
                'value'=>HybridMailStat::NEW_ORDER,
                'on'=>'insert'),

            array('user_id',
                'default',
                'value'=>Yii::app()->user->isGuest?null:Yii::app()->user->id,
                'on'=>'insert'),

            array('text,
            ', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),

            array('regulations', 'boolean', 'falseValue' => 'true', 'message' => 'Musisz zaakceptować regulamin, aby móc kontynować!', 'on' => 'step5'),

            array('document', 'file', 'types'=>'pdf, doc, docx', 'maxSize'=>1024*1024*10, 'on' => 'step1'),

            array('text', 'length', 'max'=>65000),

            array('stat, sender_address_data_id, pages', 'numerical', 'integerOnly'=>true),
            array('user_id, order_id', 'length', 'max'=>10),
            array('local_id', 'length', 'max'=>12),
            array('file_hash', 'length', 'max'=>64),
            array('date_updated', 'safe'),
            array('date_updated, user_id, text, order_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, user_id, text, order_id, stat, local_id, file_hash, sender_address_data,
                __daysFromLastStatusChanged,
                __receivers_number,
                __user_login,
                __stat,
                pages,
            ', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @param bool $forceNotSaved Ignore that model has already ID and try to get additions by array of additions ids
     * @return array|mixed|null|static
     */
    public function listAdditions($forceNotSaved = false)
    {
        if($this->id == NULL OR $forceNotSaved)
            $additions = HybridMailAdditionList::model()->findAllByAttributes(array('id' => $this->_hybrid_mail_additions));
        else
            $additions = HybridMailAddition::model()->findAllByAttributes(array('hybrid_mail_id' => $this->id));

        return $additions;
    }

    protected function calculateAdditionsPrice($currency)
    {
        $additions = $this->_hybrid_mail_additions;

        if(!is_array($additions)) return 0;

        $price = 0;
        foreach($additions AS $item)
        {
            /* @var $model HybridMailAdditionList */
            $model = HybridMailAdditionList::model()->findByPk($item);
            if($model === null) continue;

            $price += $model->calculatePriceByPages($this->getTotalPages(), $currency);
        }

        return $price;

    }

    public function totalPriceFroAdditions($currency)
    {
        return S_Useful::sizeof($this->hybridMailReceivers) * $this->calculateAdditionsPrice($currency);
    }

    public function getTotalPages()
    {
        if($this->_total_pages < 1)
            $this->_total_pages = $this->calculateTotalNumberOfPages();

        return $this->_total_pages;
    }

    public function calculatePriceForReceiver(HybridMailReceiver $receiver, $currency)
    {
        $user_group = User::getUserGroupId($this->user_id);

        $country_id = $receiver->receiverCountry->id;
        $pages = $this->getTotalPages();

        $price = 0;
        $price += self::calculatePriceByCountry($currency, $pages, $country_id, $user_group);
        $price += self::calculateAdditionsPrice($currency, $pages, $user_group);
        return $price;
    }

    protected function calculateTotalPrice($currency)
    {
        $value = 0;
        $receivers = $this->hybridMailReceivers;

        foreach($receivers AS $receiver)
        {
            $value += $this->calculatePriceForReceiver($receiver, $currency);
        }

        return $value;

    }

    protected static function calculatePriceByCountryForGroup($pages, $country_group)
    {
        if(Yii::app()->user->isGuest)
            return false;

        $price = 0;

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('first_page_price, next_page_price');
        $cmd->from('hybrid_mail_price');
        $cmd->join('user_group', 'hybrid_mail_price.group_id = user_group.id');
        $cmd->join('user_has_user_group', 'user_group_id = user_group.id');
        $cmd->where('1=1'); // stat
        $cmd->andWhere('country_group_id = :country_group', array(':country_group' => $country_group));
        $cmd->andWhere('user_has_user_group.user_id = :user_id', array(':user_id' => Yii::app()->user->id));
        $cmd->limit('1');

        $result = $cmd->queryRow();

        $price += $result['first_page_price'];
        $price += $result['next_page_price'] * ($pages - 1);

        return $price;

    }


    protected static function calculatePriceByCountry($currency, $pages, $country_to, $user_group)
    {
        $price = HybridMailPrice::calculatePriceEach($currency, $pages, $country_to, $user_group);

        return $price;
    }

    public function getTotalPrice($currency)
    {

        $this->_total_price = $this->calculateTotalPrice($currency);

        return $this->_total_price;
    }

    protected function calculatePagesForText($text)
    {
        $characters = strlen($text);

        if(!$characters) return 0;

        $pages = $characters / 4000;
        $pages = ceil($pages);

        return $pages;

    }

    protected function calculateTotalNumberOfPages()
    {

        $pages = $this->pages;

        return $pages;
    }


    public function priceTotal($currency)
    {
        return $this->calculateTotalPrice($currency);

    }

    public function saveNewHM($saveSenderToBook = false, $saveReceiverToBook = false, $ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();


        $errors = false;


        /**
         * SENDER DATA
         */
        if($this->senderAddressData === NULL)
            $this->senderAddressData = new HybridMail_AddressData('sender');

        $this->senderAddressData->validate();
        if($this->senderAddressData->hasErrors())
            $errors = true;
        else
        {
            $this->senderAddressData->save();
            $this->sender_address_data_id = $this->senderAddressData->id;

            if($saveSenderToBook)
                UserContactBook::createAndSave(false, $this->senderAddressData->attributes, 'sender');
        }


        /**
         * ORDER DATA
         */

        $this->scenario = 'insert';
        if(!$this->save())
        {
            $errors = true;
        }
        else
        {
            /**
             * RECEIVERS DATA
             */

            if(is_array($this->hybridMailReceivers) && S_Useful::sizeof($this->hybridMailReceivers))
            {
                /* @var $item HybridMailReceiver */
                foreach($this->hybridMailReceivers AS $key => $item)
                {
                    if($item->addressData == NULL)
                    {
                        $errors = true;
                        continue;
                    }
                    if(!$item->saveWithAddressData(false, $this->id))
                        $errors = true;
                    else if($saveReceiverToBook[$key])
                    {
                        UserContactBook::createAndSave(false, $item->addressData->attributes,'receiver');

                    }
                }

            }
            else
                $errors = true;


        }

        /* @var $item HybridMailAdditionList */
        foreach($this->listAdditions(true) AS $item)
        {
            $addition = new HybridMailAddition();
            $addition->hybrid_mail_id = $this->id;
            $addition->description = $item->hybridMailAdditionListTr->description;
            $addition->price_first_page = Yii::app()->PriceManager->getFinalPrice($item->getPrice('first'));
            $addition->price_next_page = Yii::app()->PriceManager->getFinalPrice($item->getPrice('next'));
            $addition->name = $item->hybridMailAdditionListTr->title;


            if(!$addition->save())
                $errors = true;
        }



        // HANDLE FILE ATTACHMENT
        {
            $finalDir = HybridMail::getFinalDir($this->file_hash.'.'.$this->_file_ext);

            if(@!copy(HybridMail::getTmpDir($this->file_hash.'.'.$this->_file_ext), $finalDir))
                $errors = true;
            else
            {
                $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                $mimeExt = finfo_file($finfo, $finalDir);

                $attachment = new HybridMailAttachment();
                $attachment->name = 'document.'.$this->_file_ext;
                $attachment->file_hash = $this->file_hash;
                $attachment->extension = $this->_file_ext;
                $attachment->hybrid_mail_id = $this->id;
                $attachment->pages = $this->pages;
                $attachment->size = filesize($finalDir);
                $attachment->type = $mimeExt;
                $attachment->status = 'enabled';
                $attachment->path = $finalDir;
                if(!$attachment->save())
                    $errors = true;

            }

        }


        $orderProducts = [];
        if(!$errors)
        {

            $product = OrderProduct::createNewProduct(
                OrderProduct::TYPE_HYBRID_MAIL,
                $this->id,
                Yii::app()->PriceManager->getCurrencyCode(),
                $this->priceTotal(Yii::app()->PriceManager->getCurrency()),
                Yii::app()->PriceManager->getFinalPrice($this->priceTotal(Yii::app()->PriceManager->getCurrency())));


            if(!$product)
                $errors = true;

            array_push($orderProducts, $product);
        }


        if(!$errors)
        {
            $order = Order::createOrderForProducts($orderProducts, true);

            if(!$order)
                $errors = true;
        }

        if(!$errors)
        {
            if($ownTransaction)
                $transaction->commit();
            return $order;
        }
        else
        {
            if($ownTransaction)
                $transaction->rollBack();
            return false;
        }
    }


    public function assingToOrder($order_id)
    {
        $this->order_id = $order_id;
        return $this->update(array('order_id'));
    }


    public function generateAddressPDF()
    {

        $margins = 10;

        /* @var $pdf TCPDF */









        $pdf = PDFGenerator::initialize();
        $pdf->SetTitle('Adresy');

        $pdf->SetFont('freesans', '', 10, '', true);
        $pdf->SetMargins($margins,$margins,$margins,true);
        $pdf->AddPage('P','A4');

        //$image = file_get_contents('images/pdf/logo.png');

        $blockWidth = 130;
        $blockHeight = 80;
        $blockVerticalSpace = 10;
        $rightBlockXPosition = $pdf->getPageWidth(0) - $blockWidth - 2 * $margins;

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => true,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 6,
            'stretchtext' => 4
        );

        $j = 0;

        /* @var $item HybridMailReceiver */

        $windowW = 87;
        $windowH = 44;

        $windowX = 210 - $windowW - 23;
        $windowY = 43;

        foreach($this->hybridMailReceivers AS $item)
        {

            if($j > 0)
                $pdf->AddPage('P','A4');

            $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

            $pdf->MultiCell($windowW, $windowH, '', 1, 'J', false, 0, $windowX, $windowY, true, 0, false, true, 0);


            $pdf->Image('@'.PDFGenerator::getLogoPl(), $windowX + 35, $windowY + 1, 22, '','','N',false);

            $text = $this->senderAddressData->name!=''?$this->senderAddressData->name."\r\n":'';
            $text .= $this->senderAddressData->company!=''? $this->senderAddressData->company."\r\n":'';
            $text .= $this->senderAddressData->address_line_1."\r\n";
            $text .= $this->senderAddressData->address_line_2!=''? $this->senderAddressData->address_line_2."\r\n":'';
            $text .= $this->senderAddressData->zip_code."\r\n";
            $text .= $this->senderAddressData->country."\r\n";



            $pdf->SetFont('freesans', '', 6);
            $pdf->MultiCell(
                30,
                $windowH / 2,
                $text,
                0,
                'L',
                false,
                1,
                $windowX + 2,
                $windowY + 2,
                true,
                0,
                false,
                true,
                15,
                'T',
                true
            );


            $pdf->SetFont('freesans', '', 16);
            $text = $item->addressData->name!=''?$item->addressData->name."\r\n":'';
            $text .= $item->addressData->company!=''? $item->addressData->company."\r\n":'';
            $text .= $item->addressData->address_line_1!=''? $item->addressData->address_line_1."\r\n":'';
            $text .= $item->addressData->address_line_2!=''? $item->addressData->address_line_2."\r\n":'';
            $text .= $item->addressData->zip_code." ".$item->addressData->city."\r\n";
            $text .= $item->addressData->country."\r\n";


            $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            $pdf->MultiCell(
                50,
                25,
                $text,
                0,
                'R',
                false,
                1,
                $windowX + $windowW/2 - 10,
                $windowY + 8,
                true,
                0,
                false,
                true,
                25,
                'B',
                true
            );

            $pdf->Line($windowX, $windowY + $windowH - 10, $windowX + $windowW, $windowY + $windowH - 10);

            $pdf->write1DBarcode($this->local_id, 'C128', $windowX, $windowY + $windowH - 12, $windowW, 15,1, $barcodeStyle);

            $j++;

        }

        $pdf->Output('www.swiatprzesylek.pl.pdf', 'D');




    }

}