<?php

class F_InternalImport extends F_BaseImport {

    //
    // SORTABLE ATTRIBUTES

    /**
     * Return list of attributes for import
     *
     * @param array $customOrder Array with custom order
     * @param bool|true $reorderKeys Whether to reorder keys. False on displaying order with widget
     * @return array
     */
    public static function getAttributesList($customOrder = [], $reorderKeys = true)
    {
        $attributes = Array(
            0 => 'with_pickup',
            1 => 'packages_number',
            2 => 'cod',

            3 => 'package_weight',
            4 => 'package_size_l',
            5 => 'package_size_w',
            6 => 'package_size_d',

            7 => 'package_value',
            8 => 'package_content',


            9 => 'sender_name',
            10 => 'sender_company',
            11 => 'sender_country',
            12 => 'sender_zip_code',
            13 => 'sender_city',
            14 => 'sender_address_line_1',
            15 => 'sender_address_line_2',
            16 => 'sender_tel',
            17 => 'sender_email',

            18 => 'receiver_name',
            19 => 'receiver_company',
            20 => 'receiver_country',
            21 => 'receiver_zip_code',
            22 => 'receiver_city',
            23 => 'receiver_address_line_1',
            24 => 'receiver_address_line_2',
            25 => 'receiver_tel',
            26 => 'receiver_email',
            27 => 'cod_currency',
            28 => 'bank_account',
            29 => 'ref',
            30 => 'options',
            31 => 'wrapping',
            32 => 'value_currency',
            33 => 'note1',
            34 => 'note2',
        );

        // apply custom order
        if(is_array($customOrder)) {
            $attributes = array_replace(array_flip($customOrder), $attributes);
            if($reorderKeys)
                $attributes = array_values($attributes);
        }

        return $attributes;
    }

    //


    public static function getAttributeLength($attribute)
    {

        $data = [

            'package_content' => 256 ,//'package_content',

            'sender_name' => 45 ,//'sender_name',
            'sender_company' => 45 ,//'sender_company',
            'sender_country' => 45 ,//'sender_country',
            'sender_zip_code' => 45 ,//'sender_zip_code',
            'sender_city' => 45 ,//'sender_city',
            'sender_address_line_1' => 45 ,//'sender_address_line_1',
            'sender_address_line_2' => 45 ,//'sender_address_line_2',
            'sender_tel' => 45 ,//'sender_tel',
            'sender_email' => 45 ,//'sender_email',

            'receiver_name' => 45 ,//'receiver_name',
            'receiver_company' => 45 ,//'receiver_company',
            'receiver_country' => 45 ,//'receiver_country',
            'receiver_zip_code' => 45 ,//'receiver_zip_code',
            'receiver_city' => 45 ,//'receiver_city',
            'receiver_address_line_1' => 45 ,//'receiver_address_line_1',
            'receiver_address_line_2' => 45 ,//'receiver_address_line_2',
            'receiver_tel' => 45 ,//'receiver_tel',
            'receiver_email' => 45 ,//'receiver_email',
            'content' => 45 ,//'content',
            'ref' => 64 ,//'ref',

            'bank_account' => 50 ,//'ref',

            'note1' => 50 ,//'ref',
            'note2' => 50 ,//'ref',
        ];

        return isset($data[$attribute]) ? $data[$attribute] : false;
    }


    public static function calculateTotalNumberOfPackages(array $data, $customOrder = [])
    {

        $number = 0;
        foreach($data AS $item)
            $number += intval($item[self::getAttributesMap($customOrder)['packages_number']]);

        return $number;

    }

    public static function mapAttributesToModels(array $data, $customOrder = [], $cut = false, $operator_id = false)
    {

        $mapOfAttributes = self::getAttributesMap($customOrder);

        $models = [];

        $i = 0;
        foreach($data AS $item)
        {
            $i++;

            $courier = new Courier_CourierTypeInternal();


            $courier->user_id = Yii::app()->user->id;
            $courier->source = Courier::SOURCE_FILE_IMPORT;

            $courier->courierTypeInternal = new CourierTypeInternal('import');
            $courier->senderAddressData = new CourierTypeInternal_AddressData();
            $courier->receiverAddressData = new CourierTypeInternal_AddressData();

            $courier->package_weight = self::_getAttrValue('package_weight', $mapOfAttributes, $item, $cut, true);
            $courier->package_size_l = self::_getAttrValue('package_size_l', $mapOfAttributes, $item, $cut, true);
            $courier->package_size_w = self::_getAttrValue('package_size_w', $mapOfAttributes, $item, $cut, true);
            $courier->package_size_d = self::_getAttrValue('package_size_d', $mapOfAttributes, $item, $cut, true);
            $courier->packages_number = self::_getAttrValue('packages_number', $mapOfAttributes, $item, $cut, false);
            $courier->package_value = self::_getAttrValue('package_value', $mapOfAttributes, $item, $cut, false);
            $courier->package_content = self::_getAttrValue('package_content', $mapOfAttributes, $item, $cut, false);


            $courier->value_currency = self::_getAttrValue('value_currency', $mapOfAttributes, $item, $cut, false);
            $courier->note1 = self::_getAttrValue('note1', $mapOfAttributes, $item, $cut, false);
            $courier->note2 = self::_getAttrValue('note2', $mapOfAttributes, $item, $cut, false);

            $wrapping = $item[$mapOfAttributes['wrapping']];
            if(in_array($wrapping, array_keys(User::getWrappingList())))
                $courier->courierTypeInternal->_package_wrapping = $wrapping;


            $senderName = $item[$mapOfAttributes['sender_name']];
            $senderCompany = $item[$mapOfAttributes['sender_company']];
            self::_distributeTooLongField($senderName, $senderCompany, 'sender_name', 'sender_company', $cut);
            self::_distributeTooLongField($senderCompany, $senderName, 'sender_company', 'sender_name', $cut);
            $courier->senderAddressData->name = $senderName;
            $courier->senderAddressData->company = $senderCompany;


            $courier->senderAddressData->country_id = CountryList::findIdByText($item[$mapOfAttributes['sender_country']]);
            $courier->senderAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->senderAddressData->country_id);
            $courier->senderAddressData->zip_code = self::_getAttrValue('sender_zip_code', $mapOfAttributes, $item, $cut, false);
            $courier->senderAddressData->city = self::_getAttrValue('sender_city', $mapOfAttributes, $item, $cut, false);


            $senderAddressLine1 = $item[$mapOfAttributes['sender_address_line_1']];
            $senderAddressLine2 = $item[$mapOfAttributes['sender_address_line_2']];
            self::_distributeTooLongField($senderAddressLine1, $senderAddressLine2, 'sender_address_line_1', 'sender_address_line_2', $cut);
            $courier->senderAddressData->address_line_1 = $senderAddressLine1;
            $courier->senderAddressData->address_line_2 = $senderAddressLine2;

            $courier->senderAddressData->tel = self::_getAttrValue('sender_tel', $mapOfAttributes, $item, $cut, false);
            $courier->senderAddressData->email = self::_getAttrValue('sender_email', $mapOfAttributes, $item, $cut, false);

            $courier->senderAddressData->bankAccount = self::_getAttrValue('bank_account', $mapOfAttributes, $item, $cut, false);


            $receiverName = $item[$mapOfAttributes['receiver_name']];
            $receiverCompany = $item[$mapOfAttributes['receiver_company']];
            self::_distributeTooLongField($receiverName, $receiverCompany, 'receiver_name', 'receiver_company', $cut);
            self::_distributeTooLongField($receiverCompany, $receiverName, 'receiver_company', 'receiver_name', $cut);
            $courier->receiverAddressData->name = $receiverName;
            $courier->receiverAddressData->company = $receiverCompany;

            $courier->receiverAddressData->country_id = CountryList::findIdByText($item[$mapOfAttributes['receiver_country']]);
            $courier->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->receiverAddressData->country_id);
            $courier->receiverAddressData->zip_code = self::_getAttrValue('receiver_zip_code', $mapOfAttributes, $item, $cut, false);
            $courier->receiverAddressData->city = self::_getAttrValue('receiver_city', $mapOfAttributes, $item, $cut, false);

            $receiverAddressLine1 = $item[$mapOfAttributes['receiver_address_line_1']];
            $receiverAddressLine2 = $item[$mapOfAttributes['receiver_address_line_2']];
            self::_distributeTooLongField($receiverAddressLine1, $receiverAddressLine2, 'receiver_address_line_1', 'receiver_address_line_2', $cut);
            $courier->receiverAddressData->address_line_1 = $receiverAddressLine1;
            $courier->receiverAddressData->address_line_2 = $receiverAddressLine2;

            $courier->receiverAddressData->tel = self::_getAttrValue('receiver_tel', $mapOfAttributes, $item, $cut, false);
            $courier->receiverAddressData->email = self::_getAttrValue('receiver_email', $mapOfAttributes, $item, $cut, false);


            $courier->afterFinalInit();

            if(in_array($item[$mapOfAttributes['with_pickup']], [1, "1", "yes", "tak"]))
                $withPickup = CourierTypeInternal::WITH_PICKUP_REGULAR;
            else if(in_array($item[$mapOfAttributes['with_pickup']], ["C", "c","U",'u']))
                $withPickup = CourierTypeInternal::WITH_PICKUP_CONTRACT;
            else
                $withPickup = CourierTypeInternal::WITH_PICKUP_NONE;

            $courier->courierTypeInternal->with_pickup = $withPickup;

            $courier->cod_value = self::_getAttrValue('cod', $mapOfAttributes, $item, false, true);
            $courier->cod_currency = strtoupper($item[$mapOfAttributes['cod_currency']]);

            $courier->ref = strtoupper($item[$mapOfAttributes['ref']]);

            $courier->courierTypeInternal->courier = $courier;

            $options = $item[$mapOfAttributes['options']];
            $options = str_replace([',','.'], ';', $options);
            $options = explode(';', $options);
            $options = array_filter($options);


            if(is_array($options) && S_Useful::sizeof($options))
                $courier->courierTypeInternal->_imported_additions = $options;

            array_push($models, $courier);
        }

        return $models;
    }

    public static function validateModels(array &$models)
    {

        $refNumbers = [];

        $errors = false;
        foreach($models AS $key => $model) {
            $models[$key]->courierTypeInternal->scenario = 'import';


            $models[$key]->validate();

            if($models[$key]->cod_value > 0 && !$models[$key]->hasErrors('cod_value'))
                $model->senderAddressData->_bankAccountRequired = true;

            $models[$key]->receiverAddressData->validate();
            $models[$key]->senderAddressData->validate();
            $models[$key]->courierTypeInternal->validate();

//            if (($models[$key]->client_cod_value > 0 && ($models[$key]->senderAddressData->country_id != CountryList::COUNTRY_PL OR $models[$key]->receiverAddressData->country_id != CountryList::COUNTRY_PL)))
//                $models[$key]->courierTypeInternal->addError('client_cod_value', Yii::t('courier', 'COD jest obsługiwane tylko na trasie wewnątrz PL.'));


//            $models[$key]->courierTypeInternal->performCODCheck(NULL, true);

            // validate if carraige is possible
            if (!self::isCarriagePossible($models[$key])) {
                $models[$key]->receiverAddressData->addError('country_id', Yii::t('courier', 'Niestety, ale ta trasa nie jest obsługiwana.'));
                $models[$key]->senderAddressData->addError('country_id', Yii::t('courier', 'Niestety, ale ta trasa nie jest obsługiwana.'));
            }




            $models[$key]->courierTypeInternal->_price_currency = Yii::app()->PriceManager->getCurrencyCode();

            if($models[$key]->ref)
            {
                if(isset($refNumbers[$models[$key]->ref]))
                {
                    $models[$key]->addError('ref', Yii::t('courier', 'Numer REF musi być unikatowy!'));
                    $errors = true;
                }
                $refNumbers[$models[$key]->ref] = true;
            }


            // ADDITIONS - START:
            $models[$key]->courierTypeInternal->_imported_additions_fail = [];
            $models[$key]->courierTypeInternal->_imported_additions_ok = [];
            $importedAdditionsList = $models[$key]->courierTypeInternal->_imported_additions;


            // @12.04.2017
            // Special rule for user YesSport (#46) - always add addition #62 - RM Signature
            if($models[$key]->user_id == 46)
                $importedAdditionsList[] = 62;

            if(is_array($importedAdditionsList) && S_Useful::sizeof($importedAdditionsList))
            {
                $allowedAdditions = CourierAdditionList::getAdditionsByCourier($model);

                $temp = [];
                foreach($allowedAdditions AS $item)
                    $temp[$item->id] = $item;

                $allowedAdditions = $temp;
                unset($temp);

                foreach($importedAdditionsList AS $additonIn)
                {
                    if(isset($allowedAdditions[intval($additonIn)]))
                        $models[$key]->courierTypeInternal->_imported_additions_ok[$additonIn] = '(#'.$allowedAdditions[$additonIn]->id.') '.($allowedAdditions[$additonIn] ? $allowedAdditions[$additonIn]->getClientTitle() : '');
                    else
                        $models[$key]->courierTypeInternal->_imported_additions_fail[$additonIn] = $additonIn;
                }


                $additonGroups = [];
                foreach($models[$key]->courierTypeInternal->_imported_additions_ok AS $addition_key => $additon_name)
                {
                    /* @var $additionModel CourierAdditionList */
                    $additionModel = $allowedAdditions[$addition_key];

                    // no group - ignore checking
                    if($additionModel->group == '')
                        continue;

                    if(!isset($additonGroups[$additionModel->group]))
                        $additonGroups[$additionModel->group] = true; // mark this additon group as used
                    else {
                        // this addition group has already been used by another addition! Remove next ones!
                        unset($models[$key]->courierTypeInternal->_imported_additions_ok[$addition_key]);
                        $models[$key]->courierTypeInternal->_imported_additions_fail[$addition_key] = $additon_name;
                    }
                }

                $models[$key]->courierTypeInternal->_courier_additions = array_keys($models[$key]->courierTypeInternal->_imported_additions_ok);
                // ADDITIONS - END

            }

            if ($models[$key]->hasErrors() OR $models[$key]->receiverAddressData->hasErrors() OR $models[$key]->senderAddressData->hasErrors() OR $models[$key]->courierTypeInternal->hasErrors())
                $errors = true;
            else
                $models[$key]->courierTypeInternal->_price_total = Yii::app()->PriceManager->getFinalPrice($models[$key]->courierTypeInternal->priceTotal(Yii::app()->PriceManager->getCurrency()));




        }

        return $errors;
    }


    public static function isCarriagePossible(Courier $courier)
    {

        $userGroup = NULL;
        if(!Yii::app()->user->isGuest)
            $userGroup = Yii::app()->user->getUserGroupId();

        return $courier->courierTypeInternal->isCarriagePossible($userGroup);

    }
}