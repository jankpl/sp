<?php

class CourierOperatorController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierOperator'),
        ));
    }

    public function actionCreate() {
        $model = new CourierOperator;

        $this->performAjaxValidation($model, 'courier-operator-form');




        if (isset($_POST['CourierOperator'])) {
            $model->setAttributes($_POST['CourierOperator']);

            if($model->broker)
            {
                $model->params = $model->wrapParams($_POST['ooe_login'], $_POST['ooe_account'], $_POST['ooe_pass'], $_POST['ooe_service'], $_POST['safepak_service']);
            }

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id, 'CourierOperator');

        if(!$model->editable)
            throw new CHttpException(400, 'You cannot edit this item.');

        $this->performAjaxValidation($model, 'courier-operator-form');

        if (isset($_POST['CourierOperator'])) {
            $model->setAttributes($_POST['CourierOperator']);

            if($model->broker)
            {
                $model->params = $model->wrapParams($_POST['ooe_login'], $_POST['ooe_account'], $_POST['ooe_pass'], $_POST['ooe_service'], $_POST['safepak_service']);
            }

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model = $this->loadModel($id, 'CourierOperator');

            if(!$model->editable)
                throw new CHttpException(400, 'You cannot delete this item.');

            try
            {
                $model->delete();
            } catch(Exception $ex)
            {
                throw new CHttpException(400, Yii::t('app', 'You cannot delete it!'));
            }
            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierOperator('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierOperator']))
            $model->setAttributes($_GET['CourierOperator']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}