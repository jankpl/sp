<?php
/* @var $model Order */
/* @var $model OrderStatHistory[] */
?>

<table class="list hTop" style="width: 700px;">
    <tr class="header">
        <td>Date</td>
        <td>Status</td>
        <td>Stat Map</td>
        <td>Location</td>
        <td>Action</td>
    </tr>
    <?php
    $i = 0;
    foreach($model AS $itemStatHistory):
        ?>
        <tr <?php echo $itemStatHistory->hidden?'style="background: #E5E5E5; font-style: italic;;"':'';?>>
            <td><?php echo $itemStatHistory->status_date;?></td>
            <td>[<?= $itemStatHistory->currentStat->id;?>] <?php echo $itemStatHistory->currentStat->name;?></td>
            <td><?php echo ($itemStatHistory->currentStat->statMapMapping ? ('['.$itemStatHistory->currentStat->statMapMapping->map_id.'] '.$itemStatHistory->currentStat->statMapMapping->getName()) : '');?></td>
            <td><?php echo $itemStatHistory->location;?></td>

            <td><?php echo CHtml::link('edit', array('/stat/edit', 'id' => $itemStatHistory->id, 'type' => $type, 'return' => base64_encode(Yii::app()->request->requestUri)));?> | <?php echo $itemStatHistory->hidden ? CHtml::link('show', array('/stat/show', 'id' => $itemStatHistory->id, 'type' => $type, 'return' => base64_encode(Yii::app()->request->requestUri)), array('onclick' => 'return confirmMe();')) : CHtml::link('hide', array('/stat/hide', 'id' => $itemStatHistory->id, 'type' => $type, 'return' => base64_encode(Yii::app()->request->requestUri)), array('onclick' => 'return confirmMe();'));?></td>
        </tr>
    <?php
    endforeach;
    ?>
</table>

<script>
    function confirmMe(){
        return confirm("Are you sure?");
    }
</script>