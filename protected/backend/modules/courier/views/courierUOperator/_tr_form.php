<div class="form">
    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']text'); ?>
        <?php echo $form->textArea($model, '['.$i.']text', ['style' => 'width: 300px; height: 100px;']); ?>
        <?php echo $form->error($model,'['.$i.']text'); ?>
    </div><!-- row -->
</div><!-- form -->