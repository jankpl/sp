<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

    <p>
        You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
    </p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-carrier-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        array(
            'name' => '__courier_id',
            'value' => 'CHtml::link("#".Courier::model()->findByPk($data->courier_id)->local_id,array("/courier/courier/view","id" => $data->courier_id),array("target" => "_blank"))',
            'type' => 'raw',
        ),
        array(
            'name' => 'stat',
            'value' => 'CourierLabelNew::getStats()[$data->stat]',
            'filter' => CourierLabelNew::getStats(),
        ),
        array(
            'header' => 'Get label',
            'value' => '$data->stat == CourierLabelNew::STAT_SUCCESS ? CHtml::link(TbHtml::icon(TbHtml::ICON_FILE), array("/courier/courierOrder/getLabel","id" => $data->id),array("target" => "_blank")):"-"',
            'type' => 'raw',
        ),
        'log',
        'date_entered',
        'date_updated',
        array(
            'name' => 'operator',
            'value' => 'CourierLabelNew::getOperators()[$data->operator]',
            'filter' => CourierLabelNew::getOperators(),
        ),
    ),
)); ?>