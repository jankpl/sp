<?php

Yii::import('application.modules.courier.models._base.BaseCourierCountryGroupHasCountry');

class CourierCountryGroupHasCountry extends BaseCourierCountryGroupHasCountry
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}