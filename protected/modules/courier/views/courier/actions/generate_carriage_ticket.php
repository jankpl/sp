<div class="alert alert-success text-center" id="label-action-block" style="position: relative;">
    <div class="overlay" style="<?= (in_array($model->courier_stat_id,[CourierStat::PACKAGE_PROCESSING, CourierStat::NEW_ORDER])) ? '' : 'display: none;';?>"></div>
    <h4><?php echo Yii::t('courier','List przewozowy');?></h4>
    <br/>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'mass-carriage-ticket',
        'action' => Yii::app()->createUrl('/courier/courier/CarriageTicket', array('/courier/courier/CarriageTicket', 'urlData' => $model->hash)),
        'htmlOptions' =>
            [
                'class' => 'do-not-block',
                'target' => '_blank',
            ],
    ));
    ?>
    <?php
    if($model->courierTypeReturn OR $model->courierTypeU OR $model->courierTypeExternal OR $model->courierTypeInternalSimple OR $model->courierTypeInternal OR $model->courierTypeDomestic OR ($model->courierTypeOoe AND $model->courierTypeOoe->courier_label_new_id != '') OR $model->courierTypeExternalReturn)
        echo CHtml::dropDownList('type','',LabelPrinter::getLabelGenerateTypes(false), array('style' => 'width: 300px; display: inline-block; height: 23px; vertical-align: top;'));
    ?>
    <br/>
    <br/>
    <?php
    if(Yii::app()->user->model->getAllowCourierInternalExtendedLabel() && $model->courierTypeInternal)
        echo CHtml::hiddenField('mode', CourierTypeInternal::GET_LABEL_MODE_LAST);
    ?>

    <?php echo TbHtml::submitButton(Yii::t('courier','Generuj list przewozowy'));?>
    <?php $this->endWidget();?>

    <?php

    if(!in_array($model->receiverAddressData->country_id, CountryList::getUeList(true))):
        ?>
        <br/>
        <div class="alert alert-warning"><?= Yii::t('courier', 'Pamiętaj o dołączeniu dokumentów celnych!');?></div>
        <?php
    endif;
    ?>
    <?php
//    if($model->courierTypeExternal)
//        $this->Widget('GenerateCarriageTicketsUniversal');
    ?>
</div>
