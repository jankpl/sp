<?php
/* @var $models _StatMapMappingGridView */
/* @var $id int */
/* @var $type int */

$this->breadcrumbs=array(
    'Stat Map Mappings',
);

$this->menu=array(
    array('label'=>'List of mappings', 'url'=>array('index', 'type' => $type)),
    array('label'=>'New mapping', 'url'=>array('new', 'type' => $type)),
    array('label'=>'Synchronize', 'url'=>array('synchronize', 'type' => $type)),
);
?>

<h1><?= StatMapMapping::getTypeName($type);?>: Manage stats for map "<?= StatMap::getMapNameById($id);?>" (#<?= $id;?>)</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'stat-map-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'ajaxUrl' => Yii::app()->createUrl('/statMapMapping/manage', ['manage_type' => $type, 'id' => $id]),
    'columns' => array(
        'id',
        'stat_id',
        [
            'name' => '_statName',
            'value' => '$data->type == StatMapMapping::TYPE_COURIER ? $data->courierStat->name : $data->postalStat->name',
        ],
        'date_entered',
        [
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'buttons'=>array
            (
                'delete' => array
                (
                    'label'=>'Delete mapping',
                    'url' => 'Yii::app()->createUrl("statMapMapping/delete", array("id"=>$data->id))',
                ),
            ),
        ],

    ),
)); ?>