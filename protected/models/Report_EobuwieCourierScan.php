<?php

class Report_EobuwieCourierScan extends Report
{
    const SEPARATOR = '; ';

    public static function checkItem(Courier $courier)
    {
        if(!in_array($courier->user_id, [947,948, 2036, 2037]))
            return false;

        $date = date('Y-m-d');
        $model = self::model()->find('type = :type AND DATE(date_entered) = :date AND stat = 0', [':type' => self::TYPE_EOBUWIE_COURIER_SCAN, ':date' => $date]);

        $eobuwie = $eobuwieZwroty = $eobuwieDe = $eobuwieNonDe = 0;
        $eobuwieId = $eobuwieZwrotyId = $eobuwieDeId = $eobuwieNonDeId = '';

        if($courier->user_id == 947) {
            $eobuwie = 1;
            $eobuwieId = $courier->listExternalIdsByCourierLabelNew(true, self::SEPARATOR).self::SEPARATOR;
        }
        else if($courier->user_id == 948) {
            $eobuwieZwroty = 1;
            $eobuwieZwrotyId = $courier->listExternalIdsByCourierLabelNew(true, self::SEPARATOR).self::SEPARATOR;
        }
        else if($courier->user_id == 2036) {
            $eobuwieNonDe = 1;
            $eobuwieNonDeId = $courier->listExternalIdsByCourierLabelNew(true, self::SEPARATOR).self::SEPARATOR;
        }
        else if($courier->user_id == 2037) {
            $eobuwieDe = 1;
            $eobuwieDeId = $courier->listExternalIdsByCourierLabelNew(true, self::SEPARATOR) . self::SEPARATOR;
        }

        if($model)
        {
            $model->content->numberEobuwie += $eobuwie;
            $model->content->numberEobuwieZwroty += $eobuwieZwroty;
            $model->content->numberEobuwieNonDe += $eobuwieNonDe;
            $model->content->numberEobuwieDe += $eobuwieDe;
            $model->content->idsEobuwie .= $eobuwieId;
            $model->content->idsEobuwieZwroty .= $eobuwieZwrotyId;
            $model->content->idsEobuwieNonDe .= $eobuwieNonDeId;
            $model->content->idsEobuwieDe .= $eobuwieDeId;
        } else {
            $model = new self;
            $model->type = self::TYPE_EOBUWIE_COURIER_SCAN;
            $model->stat = 0;
            $model->what_id = 0;

            $model->content = new stdClass();
            $model->content->numberEobuwie = $eobuwie;
            $model->content->numberEobuwieZwroty = $eobuwieZwroty;
            $model->content->numberEobuwieNonDe = $eobuwieNonDe;
            $model->content->numberEobuwieDe = $eobuwieDe;
            $model->content->idsEobuwie = $eobuwieId;
            $model->content->idsEobuwieZwroty = $eobuwieZwrotyId;
            $model->content->idsEobuwieNonDe = $eobuwieNonDeId;
            $model->content->idsEobuwieDe = $eobuwieDeId;

        }

        return $model->save();
    }

    public static function processReport()
    {

        $data = [];
        $data[] = ['EOBUWIE', 'EOBUWIE_ZWROTY', 'EOBUWIE_non_DE', 'EOBUWIE_DE'];

        $models = self::model()->findAll('type = :type AND stat = 0 AND date_entered < :date', [':type' => self::TYPE_EOBUWIE_COURIER_SCAN, ':date' => date('Y-m-d')]);

        $report = 'Courier scans report'."<br/>";
        $report .= "<br/><br/>";
        $report .= 'User: EOBUWIE, EOBUWIE_ZWROTY, EOBUWIE_non_DE, EOBUWIE_DE'."<br/>";
        $report .= '<table border="1">
<tr style="font-weight: bold;"><td rowspan="2">Date</td><td colspan="4">No</td><td colspan="4">Ids</td></tr>
<tr style="font-weight: bold;"><td>EOBUWIE</td><td>EOBUWIE_ZWROTY</td><td>EOBUWIE_non_DE</td><td>EOBUWIE_DE</td><td>EOBUWIE</td><td>EOBUWIE_ZWROTY</td><td>EOBUWIE_non_DE</td><td>EOBUWIE_DE</td></tr>';

        foreach($models AS $model)
        {
            $report .= '<tr><td style="vertical-align: top;">'.substr($model->date_entered,0,10).'</td><td style="text-align: center; vertical-align: top;">'.$model->content->numberEobuwie.'</td><td style="text-align: center; vertical-align: top;">'.$model->content->numberEobuwieZwroty.'</td><td style="text-align: center; vertical-align: top;">'.$model->content->numberEobuwieNonDe.'</td><td style="text-align: center; vertical-align: top;">'.$model->content->numberEobuwieDe.'</td><td style="vertical-align: top;">'.$model->content->idsEobuwie.'</td><td style="vertical-align: top;">'.$model->content->idsEobuwieZwroty.'</td><td style="vertical-align: top;">'.$model->content->idsEobuwieNonDe.'</td><td style="vertical-align: top;">'.$model->content->idsEobuwieDe.'</td></tr>';

            $arrayEobuwie = explode(self::SEPARATOR, $model->content->idsEobuwie);
            $arrayEobuwieZwroty = explode(self::SEPARATOR, $model->content->idsEobuwieZwroty);
            $arrayEobuwieNonDe = explode(self::SEPARATOR, $model->content->idsEobuwieNonDe);
            $arrayEobuwieDe = explode(self::SEPARATOR, $model->content->idsEobuwieDe);

            $model->stat = 1;
            $model->date_sent = date('Y-m-d H:i:s');
            $model->update(['stat', 'date_sent']);

            for($i = 0; $i < max([S_Useful::sizeof($arrayEobuwie), S_Useful::sizeof($arrayEobuwieZwroty), S_Useful::sizeof($arrayEobuwieNonDe), S_Useful::sizeof($arrayEobuwieDe)]); $i++) {
                $data[] = [isset($arrayEobuwie[$i]) ? $arrayEobuwie[$i] : '',
                    isset($arrayEobuwieZwroty[$i]) ? $arrayEobuwieZwroty[$i] : '',
                    isset($arrayEobuwieNonDe[$i]) ? $arrayEobuwieNonDe[$i] : '',
                    isset($arrayEobuwieDe[$i]) ? $arrayEobuwieDe[$i] : ''];
            }

        }

        $report .= '</table>';

        $filename = 'raport_'.date('Y-m-d').'.xlsx';
        $xls = S_XmlGenerator::generateXml($data, $filename, true);

        $to = [
            'skuleczka@eobuwie.com.pl',
            'jmusialowski@eobuwie.com.pl',
            'zwroty@swiatprzesylek.pl',
            'michalwojciechowski@swiatprzesylek.pl',
            'monika.kesik@etraf.eu',
//            'jankopec@swiatprzesylek.pl',
        ];

        S_Mailer::send($to, $to, 'Courier scans report', $report, false, NULL, NULL,true, false, $filename, false, false, false, $xls);
    }

}