<div class="alert alert-success">
    <h4><?php echo Yii::t('postal','Klonuj list');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t('postal','Klonuj list'), array('/postal/postal/clone', 'urlData' => $model->hash), array('target' => '_blank', 'class' => 'btn'));?>
</div>
