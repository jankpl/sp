<?php

class TtFileProcessingLog
{
    const GLS_NL = 1;

    public static function isProcessed($source, $file)
    {
        /* @var $cmd CDbCommand */

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from('tt_file_processing_log');
        $cmd->where('source = :source AND file = :file', [':source' => $source, ':file' => $file]);
        $result = $cmd->queryScalar();

        return $result;
    }

    public static function markAsProcessed($source, $file)
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->insert('tt_file_processing_log', [
            'source' => $source,
            'file' => $file,
        ]);
    }
}