<?php

Yii::import('application.modules.courier.models._base.BaseCourierCountryGroupOperator');

class CourierCountryGroupOperator extends BaseCourierCountryGroupOperator
{

    const SCENARIO_DELIVERY = 1;

	const TYPE_PICKUP = 1;
	const TYPE_DELIVERY = 2;

	const DEFAULT_ORDER = 'weight_from ASC, id DESC';

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('courier_country_group, weight_from, weight_to, courier_operator', 'required'),

			array('courier_operator', 'in', 'range' => CHtml::listData(CourierOperator::model()->findAll(),'id', 'id')),
            array('courier_hub', 'in', 'range' => CHtml::listData(CourierHub::model()->findAll(),'id', 'id')),

            array('courier_hub', 'required'),

			array('weight_to','compare','compareAttribute'=>'weight_from','operator'=>'>='),


			array('courier_country_group, courier_operator, type, courier_hub', 'numerical', 'integerOnly'=>true),
			array('weight_from, weight_to', 'numerical', 'integerOnly' => false, 'min'=>0, 'max' => 999),


			array('id, courier_country_group, courier_operator, date_entered, type, weight_from, weight_to, courier_hub', 'safe', 'on'=>'search'),
		);
	}
}