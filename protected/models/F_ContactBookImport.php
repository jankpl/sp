<?php

class F_ContactBookImport extends CFormModel {

    public $file;
    public $omitFirst;

    const MAX_LINES = 250;



    //
    // SORTABLE ATTRIBUTES

    /**
     * Return list of attributes for import
     *
     * @param array $customOrder Array with custom order
     * @param bool|true $reorderKeys Whether to reorder keys. False on displaying order with widget
     * @return array
     */
    public static function getAttributesList($customOrder = [], $reorderKeys = true)
    {
        $attributes = Array(
            0 => 'type',
            1 => 'name',
            2 => 'company',
            3 => 'country',
            4 => 'zip_code',
            5 => 'city',
            6 => 'address_line_1',
            7 => 'address_line_2',
            8 => 'tel',
            9 => 'email',
        );


        return $attributes;
    }

    //



    public function rules() {
        return array(
            array('file', 'file',  'allowEmpty' => false,
                'maxSize' => 512400, 'types' => 'csv, xls, xlsx'),
            array('omitFirst', 'safe'),
        );
    }

    public function attributeLabels() {
        return array(
            'file' => Yii::t('app', 'File'),
            'omitFirst' => Yii::t('app', 'Omit first line (header)'),
            'split' => Yii::t('app', 'Split after'),
            'ooe_operator_id' => Yii::t('app', 'Operator'),
            'custom_settings' => Yii::t('app', 'Custom file settings'),
        );
    }


    public static function getAttributesMap($customOrder = [])
    {

        // mapping attributes to columns in file
        $attributes = array_flip(self::getAttributesList($customOrder));

        return $attributes;
    }

    protected static function resolveType($type)
    {
        if(in_array($type, ['sender', 'nadawca', 's','n', 'nad']))
            $type = UserContactBook::TYPE_SENDER;
        else
            $type = UserContactBook::TYPE_RECEIVER;

        return $type;
    }

    public static function mapAttributesToModels(array $data)
    {

        $mapOfAttributes = self::getAttributesMap();

        $models = [];

        $i = 0;
        foreach($data AS $item)
        {
            $i++;

            $model = new UserContactBook();
            $model->type = self::resolveType($item[$mapOfAttributes['type']]);

           $model->addressData = new AddressData_ContactBook();

            $model->addressData->name = $item[$mapOfAttributes['name']];
            $model->addressData->company = $item[$mapOfAttributes['company']];
            $model->addressData->country_id = CountryList::findIdByText($item[$mapOfAttributes['country']]);
            $model->addressData->country0 = CountryList::model()->with('countryListTr')->findByPk($model->addressData->country_id);
            $model->addressData->zip_code = $item[$mapOfAttributes['zip_code']];
            $model->addressData->city = $item[$mapOfAttributes['city']];
            $model->addressData->address_line_1 = $item[$mapOfAttributes['address_line_1']];
            $model->addressData->address_line_2 = $item[$mapOfAttributes['address_line_2']];
            $model->addressData->tel = $item[$mapOfAttributes['tel']];
            $model->addressData->email = $item[$mapOfAttributes['email']];

            array_push($models, $model);
        }

        return $models;
    }

    public static function validateModels(array &$models)
    {
        $errors = false;
        foreach($models AS $key => $model)
        {
            $models[$key]->validate();
            $models[$key]->addressData->validate();

            if($models[$key]->hasErrors() OR $models[$key]->addressData->hasErrors())
                $errors = true;
        }

        return $errors;
    }

}