<?php

class Partners
{

    const PARTNER_SP = 2;
    const PARTNER_SP_ZOO = 7;
    const PARTNER_CQL = 8;

    public static function getParntersList()
    {
        return [
            1 => 'KAAB',
            self::PARTNER_SP => 'SP',
            3 => 'Orange Post',
            self::PARTNER_SP_ZOO => 'SP_ZOO',
            self::PARTNER_CQL => 'CQL',
            9 => 'PLI',
            10 => 'Romad',
        ];
    }

    public static function getPartnerNameById($id)
    {
        return isset(self::getParntersList()[$id]) ? self::getParntersList()[$id] : NULL;
    }

}