<?php

class CourierLabelNewLog
{
    const WHAT_NEW = 0;
    const WHAT_GET_FILE_START = 1;
    const WHAT_GET_FILE_END = 2;
    const WHAT_ORDER_LABEL_START = 3;
    const WHAT_ORDER_LABEL_END = 4;
    const WHAT_PROCESS_LABEL_START = 5;
    const WHAT_PROCESS_LABEL_END = 6;
    const WHAT_PACKAGE_CREATED = 99;

    public static function saveToLog(CourierLabelNew $model, $what_id, $what_desc = '')
    {
        return true;

        $filename = 'CLN_log'.date('Ymd').'.txt';
        $filename = Yii::app()->basePath . '/../'.$filename;

        if($what_id == self::WHAT_NEW) {
            self::saveToLog($model, self::WHAT_PACKAGE_CREATED, 'Courier source: ' . $model->courier->source);
        }

        $data = date('Y-m-d H:i:s').'||'.$model->id.'||'.$model->operator.'||'.$what_id.'||'.$model->stat.'||'.$what_desc."\r\n";

        file_put_contents($filename, $data, FILE_APPEND);


    }



}