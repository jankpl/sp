<?php
/* @var $model Courier */
?>
<div class="form">

    <h2><?php echo Yii::t('app','Krok {step}', array('{step}' => '4/5')); ?> | <?php echo Yii::t('courier','Podsumowanie');?></h2>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div style="overflow: auto;">
        <div style="width: 48%; float: left;">
            <h3><?php echo Yii::t('courier','Parametry przesyłki');?> </h3>
            <table class="detail-view" style="width: 550px;">
                <tr class="odd">
                    <td><?php echo Yii::t('courier', 'Operator');?></td>
                    <td><?php echo $model->courierTypeDomestic->courierDomesticOperator->courierDomesticOperatorTr->title; ?></td>
                </tr>
                <tr class="even">
                    <td><?php echo $model->getAttributeLabel('package_weight');?> [<?php echo Courier::getWeightUnit();?>]</td>
                    <td><?php echo $model->package_weight; ?></td>
                </tr>
                <tr class="odd">
                    <td><?php echo $model->getAttributeLabel('package_dimensions');?> [<?php echo Courier::getDimensionUnit();?>]</td>
                    <td><?php echo $model->package_size_l.' x '.$model->package_size_d.' x '.$model->package_size_w; ?></td>
                </tr>
                <tr class="even">
                    <td><?php echo $model->getAttributeLabel('package_value');?></td>
                    <td><?php echo $model->package_value; ?></td>
                </tr>
                <tr class="odd">
                    <td><?php echo $model->getAttributeLabel('package_content');?></td>
                    <td><?php echo $model->package_content; ?></td>
                </tr>
                <tr class="even">
                    <td><?php echo $model->getAttributeLabel('package_number');?></td>
                    <td><?php echo $model->packages_number; ?></td>
                </tr>
                <tr class="odd">
                    <td><?php echo $model->getAttributeLabel('cod_value');?></td>
                    <td><?php echo $model->cod_value;?> <?= CourierCodAvailability::CURR_PLN;?></td>
                </tr>
                <?php
                if($model->courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_RUCH):
                ?>
                    <tr class="even">
                        <td><?php echo $model->courierTypeDomestic->getAttributeLabel('_ruch_receiver_point_address');?></td>
                        <td><?php echo $model->courierTypeDomestic->_ruch_receiver_point_address;?></td>
                    </tr>
                <?php
                endif;
                ?>
            </table>
        </div>
        <div style="width: 48%; float: right;">
            <h3><?php echo Yii::t('courier','Dodatki');?></h3>
            <table class="list hTop" style="width: 500px;">
                <tr>
                    <td><?php echo Yii::t('courier','Nazwa');?></td>
                </tr>
                <?php

                /* @var $item CourierOoeAdditionList */
                if(!S_Useful::sizeof($model->courierTypeDomestic->listAdditions(true)))
                    echo '<tr><td>'.Yii::t('app','brak').'</td></tr>';
                else
                    foreach($model->courierTypeDomestic->listAdditions(true) AS $item): ?>
                        <tr>
                            <td><?php echo $item->getClientTitle(); ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        </div>
        </div>


    <div style="overflow: auto;">
        <div style="width: 48%; float: left;">
            <h3><?php echo Yii::t('courier','Nadawca');?></h3>

            <?php

            $this->renderPartial('create/_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));

            ?></div>
        <div style="width: 48%; float: right;">
            <h3><?php echo Yii::t('courier','Odbiorca');?></h3>
            <?php

            $this->renderPartial('create/_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));

            ?>
        </div>
    </div>

    <h3><?php echo Yii::t('courier','Regulamin');?></h3>
    <div class="row">
        <?php echo $form->labelEx($model->courierTypeDomestic,'regulations'); ?>
        <?php echo $form->checkbox($model->courierTypeDomestic, 'regulations'); ?>
        <?php echo $form->error($model->courierTypeDomestic,'regulations'); ?>
    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step3', 'class' => 'btn-small'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę'),array('name'=>'finish', 'class' => 'btn-primary'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'summary', 'style' => 'display: none;'));
        $this->endWidget();
        ?>

    </div>
    <div class="info">
        <p><?php echo Yii::t('courier','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>
    </div>
</div><!-- form -->