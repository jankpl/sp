<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailAdditionList');

class HybridMailAdditionList extends BaseHybridMailAdditionList
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function calculatePriceByPages($pages, $currency)
    {
        $priceFirstPage = $this->priceFirstPage->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => $currency)))[0]->value;

        $priceNextPage =  $this->priceNextPage->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => $currency)))[0]->value;

        $price = $priceFirstPage + $priceNextPage * ($pages - 1);

        return $price;

    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),

            array('price_first_page', 'numerical', 'integerOnly'=>false, 'min'=>0.01),
            array('price_next_page', 'numerical', 'integerOnly'=>false, 'min'=>0),

            array('name, price_first_page', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('price_first_page, price_next_page', 'numerical'),
            array('name', 'length', 'max'=>45),
            array('date_updated', 'safe'),
            array('date_updated, price_next_page, stat', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, price_first_page, price_next_page, stat', 'safe', 'on'=>'search'),
        );
    }

    public function getClientTitle()
    {
        return $this->hybridMailAdditionListTr->title;
    }

    public function getClientDesc()
    {
        return $this->hybridMailAdditionListTr->description;
    }

    public function toggleStat()
    {
        $this->stat = abs($this->stat - 1);

        return $this->save(false,array('stat'));
    }

    public function saveWithTrAndPrice($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;


        $price_first_page = $this->priceFirstPage->saveWithPrices();

        if(!$price_first_page)
            $errors = true;

        $this->price_first_page = $price_first_page;

        $price_next_page = $this->priceNextPage->saveWithPrices();

        if(!$price_next_page)
            $errors = true;

        $this->price_next_page = $price_next_page;

        if(!$this->save())
            $errors = true;

        foreach($this->hybridMailAdditionListTrs AS $item)
        {

            $item->hybrid_mail_addition_list_id = $this->id;
            if(!$item->save()) {
                $errors = true;
            }
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

    public function getPrice($what)
    {

        if($what == 'next')
        {
            return $this->priceNextPage->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => Yii::app()->PriceManager->getCurrency())))[0]->value;
        } else {
            return $this->priceFirstPage->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => Yii::app()->PriceManager->getCurrency())))[0]->value;
        }
    }
}