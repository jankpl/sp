<h2>Courier Buffer</h2>

<h2>Stats</h2>

<?php
$cmd = Yii::app()->db->createCommand();
$cmd->select('COUNT(*) AS no, DATEDIFF(CURDATE(), courier.date_updated) AS _days, operator')
    ->from('courier')
    ->join('courier_label_new', 'courier_id = courier.id')
    ->where('(courier_type = '.Courier::TYPE_RETURN.' OR (courier_type = '.Courier::TYPE_INTERNAL.' AND stat_map_id = '.StatMap::MAP_RETURN.')) AND courier_stat_id IN ('.implode(',', [12653, 12724, 10022, 12734, 16651, 13580]).')')
    ->having('_days > 3')
    ->group('_days, operator')
    ->order('_days ASC, no DESC')
;
$result = $cmd->queryAll();

?>

<table class="table table-bordered table-condensed table-striped">
    <tr>
        <th style="text-align: right;">Days</th>
        <th>Items</th>
        <th>Broker</th>
    </tr>
    <?php
    $totalAbove60 = [];
    foreach($result AS $item):
        if($item['_days'] > 60):
            if(!isset($totalAbove60[$item['operator']]))
                $totalAbove60[$item['operator']] = 0;

            $totalAbove60[$item['operator']] += $item['_days'];
        else:
            ?>
            <tr>
                <td style="text-align: right;"><?= $item['_days'];?></td>
                <td><?= $item['no'];?></td>
                <td><?= CourierLabelNew::getOperators()[$item['operator']];?></td>
            </tr>
        <?php
        endif;
    endforeach;
    ?>
    <?php
    foreach($totalAbove60 AS $operator => $item):
        ?>
        <tr>
            <td style="text-align: right;">60+</td>
            <td><?= $item;?></td>
            <td><?= CourierLabelNew::getOperators()[$operator];?></td>
        </tr>
    <?php
    endforeach;
    ?>
</table>

<h3>List</h3>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-buffer-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'local_id',
            'value' => 'CHtml::link("#".$data->local_id,array("/courier/courier/view","id" => $data->id),array("target" => "_blank"))',
            'type' => 'raw',
        ),
        'date_entered',
        'date_updated',
        'stat_map_id',
        'courier_stat_id',
        '_days',
        'type',
        'user_id',

    ),
)); ?>