<?php

Yii::import('application.modules.courier.models._base.BaseCourierCustomApiData');

class CourierCustomApiData extends BaseCourierCustomApiData
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function serializeData()
    {
        $this->data = json_encode($this->data);
    }

    public function unserializeData()
    {
        $this->data = json_decode($this->data);
    }
}