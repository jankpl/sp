<?php

Yii::import('application.models._base.BaseSalesman');

class Salesman extends BaseSalesman
{
	const IMG_WIDTH = 100;
	const IMG_HEIGHT = 100;

	const DEFAULT_SALESMAN_ID = 34;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function getAllSalesmen()
	{
		return self::model()->findAll();
	}

	public static function findSalesmanById($id)
	{
		return self::model()->findByAttributes(['id' => $id, 'stat' => S_Status::ACTIVE]);
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_entered',
				'updateAttribute' => 'date_updated',
			)
		);
	}

	public function rules() {
		return array(
            array('default_injection_sp_point_id', 'in', 'range' => CHtml::listData(SpPoints::model()->findAll(),'id', 'id')),

			array('name, tel, email, stat, photo', 'required'),
			array('stat', 'numerical', 'integerOnly'=>true),
			array('name, email', 'length', 'max'=>64),
			array('tel', 'length', 'max'=>32),
			array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name, date_entered, date_updated, tel, email, stat, photo', 'safe', 'on'=>'search'),
		);
	}

	public function toggleStat()
	{
		$this->stat = abs($this->stat - 1);
		return $this->save();
	}
}