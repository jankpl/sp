<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>'Manage' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'name',
        array(
            'name' => 'commission_percentage',
            'value' => $model->commission_percentage."%",
        ),
        array(
            'name' => 'commission_amount',
            'value' => S_Price::formatPrice($model->commissionPrice->pln) ." PLN / ".
                S_Price::formatPrice($model->commissionPrice->eur) ." EUR / ".
                S_Price::formatPrice($model->commissionPrice->gbp) ." GBP"
        ,
        ),
	),
)); ?>
