<div class="view">

    <h3><?php echo $model->countryGroup->name;?></h3>

    <div style="width: 500px;">
        <?php
        $modelGV = new HybridMailPriceValue();

        if($model->id !== NULL)
        {
            $this->widget('bootstrap.widgets.TbGridView', array(
                'id' => 'hybrid-mail-grid-'.$model->id,
                'dataProvider' => $modelGV->gridViewData($model->id),

                'columns' => array(
                    array(
                        'name' => 'next_page_price',
                        'value' => 'S_Price::formatPrice($data->next_page_price)." zł"',
                    ),
                    array(
                        'name' => 'first_page_price',
                        'value' => 'S_Price::formatPrice($data->first_page_price)." zł/".HybridMail::getWeightUnit()',
                    ),
                ),
            ));
        }
        ?>

    </div>


    <table class="list hLeft" style="width: 500px;">
        <tr>
            <td>Action</td>
            <td><?php
                if($model->id === NULL)
                    echo CHtml::link('create',array('/hybridMail/hybridMailPrice/create', 'id' => $model->country_group_id, 'id2' => $group->id), array('class' => 'likeButton'));
                else
                {
                    echo CHtml::link('edit',array('/hybridMail/hybridMailPrice/update', 'id' => $model->id), array('class' => 'likeButton'));
                    echo CHtml::link($model->stat?'activate':'deactivate',array('/hybridMail/hybridMailPrice/stat', 'id' => $model->id), array('class' => 'likeButton'));
                }
                ?></td>
        </tr>
        <tr>
            <td>Last update</td>
            <td><?php echo $model->date_updated; ?> by <?php echo $model->updateAdmin->login; ?> </td>
        </tr>
    </table>

</div>