<div class="modal fade " tabindex="-1" role="dialog" id="regon-widget-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id' => 'regon-widget-form',
            ));
            ?>
            <div class="form">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">CEIDG/KRS check</h4>
                </div>
                <div class="modal-body" id="edit-modal-content">
                    <div style="text-align: center;">
                        <input type="text" name="nip" value="" placeholder="NIP" style="width: 103px;"/> or
                        <input type="text" name="regon" value="" placeholder="REGON" style="width: 103px;"/> or
                        <input type="text" name="krs" value="" placeholder="KRS" style="width: 103px;"/> or
                        <input type="text" name="vies" value="" placeholder="VIES" style="width: 103px;"/>
                    </div>
                    <br/>
                    <input type="radio" name="mode" value="1" id="radio-1"/> <label for="radio-1" style="display: inline-block;">just show data</label><br/>
                    <input type="radio" name="mode" value="2" id="radio-2" checked/> <label for="radio-2" style="display: inline-block;">fill form (only empty)</label><br/>
                    <input type="radio" name="mode" value="3" id="radio-3"/> <label for="radio-3" style="display: inline-block;">fill form (overwrite)</label>
                </div>

            </div>
            <div class="modal-footer-loader" style="text-align: center; display: none;">
                ...PROCESSING...
            </div>
            <div class="modal-footer" style="text-align: center;">
                <?php echo TbHtml::submitButton('Go!', array('class' => 'btn-lg btn-primary')); ?>
            </div>
            <?php
            $this->endWidget();
            ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('#regon-widget-open-button').on('click', function(){
            $('#regon-widget-modal .modal-footer').show();
            $('#regon-widget-modal .modal-footer-loader').hide();
            $('#regon-widget-modal').modal();
        });

        $('#regon-widget-form input[type="text"]').on('change', function()
        {
            $('#regon-widget-form input[type="text"]').not($(this)).val('');
        });

        $('#regon-widget-form').on('submit', function(e){

            $('#regon-widget-modal .modal-footer').hide();
            $('#regon-widget-modal .modal-footer-loader').show();
            e.preventDefault();

            var base_attr_name = '<?= $formAttrNames;?>';
            var attrMap = JSON.parse('<?= CJSON::encode($attrMap);?>');

            $('.regon-widget-result-div').fadeOut();

            $.ajax({
                method: "POST",
                url: '<?= Yii::app()->createUrl('/site/regon.RegonActions');?>',
                data: $('#regon-widget-form').serialize(),
                dataType: "json",
            })
                .done(function(result){

                    $('#regon-widget-modal').modal('hide');

                    if(!result.success)
                        alert("Could not find data!");
                    else {

                        var data = result.data;

                        var mode = result.mode;

                        var $table = $('<table>');
                        $table.addClass('table');
                        $table.addClass('table-bordered');
                        $table.addClass('table-striped');
                        $table.addClass('table-condensed');
                        $table.css('width', '500px');
                        $table.css('margin-bottom', '0');

                        for (var property in data) {
                            if (data.hasOwnProperty(property)) {

                                var targetProperty = property;
                                if(attrMap.hasOwnProperty(property)) {
                                    targetProperty = attrMap[property];
                                }

                                $table.append('<tr><th style="text-align: right;">' + targetProperty + '</th><td>' + data[property] + '</td></tr>');

                                if(mode > 1) {

                                    if(mode == 2)
                                        if($('[name="' + base_attr_name + '[' + targetProperty + ']"]').val() != '')
                                            continue;

                                    $('[name="' + base_attr_name + '[' + targetProperty + ']"]').val(data[property]);
                                }
                            }
                        }

                        $div = $('<div>');
                        $div.addClass('well');
                        $div.addClass('regon-widget-result-div');
                        $div.css('margin', '10px auto');

                        $div.html($table);

                        $('#regon-widget-open-button').after($div);
                    }

                })
                .fail(function(ex){
                    console.log(ex);
                    alert("Error...");
                });
        });
    });
</script>