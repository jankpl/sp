<?php
/* @var $model Faq */
/* @var $lang Language */
?>

<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(2), 'url'=>array('create')),
	array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$langVersions = Array();

foreach(Language::model()->findAll() AS $lang)
{
    array_push($langVersions, array(
        'name' => 'Version: '.$lang->short_name,
        'value' => S_Useful::sizeof($model->faqTrs(array('condition' => "faqTrs.language_id = $lang->id"))) > 0?'tak':'nie',
    ));
}

?>

<?php

$attributes = array(
    'name',
    'date_entered',
    'date_updated',
);

$this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array_merge($attributes, $langVersions),
)); ?>
