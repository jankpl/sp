<?php
/* @var $this UserMessageController */
/* @var $model UserMessage */

$this->menu=array(
	array('label'=>'Create UserMessage', 'url'=>array('create')),
);

?>

<h1>Manage User Messages</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-message-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'date_entered',
		'date_updated',
		'file_name',
		'file_type',
		'text',
		'date',
		array(
			'name' => 'user_id',
			'value' => '$data->user->login',
			'filter' => CHtml::listData(User::model()->findAll(),'id', 'login'),
		),
		'no',
		array(
			'name' => 'stat',
			'value' => 'UserMessage::getStatArray()[$data->stat]',
			'filter' => UserMessage::getStatArray(),
		),
		/*
		'user_id',
		'seen',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
