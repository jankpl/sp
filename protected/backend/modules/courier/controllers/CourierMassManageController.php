<?php

class CourierMassManageController extends Controller
{
    const MAX_NO = 1000;

    const STATE_PROGRESS = 1;
    const STATE_SUCCESS = 10;
    const STATE_FAIL = 99;

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    protected static function getStatusCacheName($sid)
    {
        return 'COURIER_MASS_MANAGE_STATUS_'.Yii::app()->session->sessionID.'_'.$sid;
    }

    protected static function setStatusValue($sid, $state, $message = false, $done = [], $all = [])
    {

        $totalNo = S_Useful::sizeof($all);
        $doneIds = [];

        foreach($all AS $key => $item)
        {
            if(isset($done[$key]))
            {
                $doneIds[$key] = $item;
                unset($all[$key]);
            }
        }

        $data = [
            'state' => $state,
            'message' => $message,
            'doneNo' => S_Useful::sizeof($doneIds),
            'doneList' => $doneIds,
            'notDoneNo' => S_Useful::sizeof($all),
            'notDoneList' => $all,
            'totalNo' => $totalNo,
        ];

        Yii::app()->cache->set(self::getStatusCacheName($sid), $data, 60*60);
    }

    public function actionStatus($sid)
    {
        if(Yii::app()->request->isAjaxRequest) {
            $data = Yii::app()->cache->get(self::getStatusCacheName($sid));

            echo CJSON::encode($data);
        } else
            Yii::app()->end();
    }

    public function actionIndex($sid = false)
    {

        $done = [];


        self::setStatusValue($sid, self::STATE_PROGRESS);

        $model = new UserScanner();

        if($sid OR isset($_POST['UserScanner']['items_ids']))
        {
            $data = $_POST;

            $packagesIds = $data['UserScanner']['items_ids'];
            $packagesIds = explode(PHP_EOL, $packagesIds);

            $model->items_ids = $packagesIds;

            $model->prefilterIds();

            if($model->validate())
            {

                $courierModels = $model->returnCourierModels(true, false);

                if(!S_Useful::sizeof($courierModels)) {
                    self::setStatusValue($sid, self::STATE_FAIL, 'No packages to perfom on!', [], $model->items_ids_backup);
                    Yii::app()->end();
                }
                else if(S_Useful::sizeof($courierModels) > self::MAX_NO)
                {
                    self::setStatusValue($sid, self::STATE_FAIL, 'Max number of items at once: '.self::MAX_NO, [], $model->items_ids_backup);
                    Yii::app()->end();
                } else {

                    $bulkNo = $data['bulk'];
                    $numkat = $data['numkat'];

                    $hub = $data['hub'];
                    if($hub)
                    {
                        foreach($courierModels AS $key => $item)
                            $courierModels[$key]->senderAddressData = SpPoints::getPoint($hub)->addressData;
                    }

                    if (isset($data['mode_kn'])) {
                        $done = KsiazkaNadawcza::generate($courierModels, KsiazkaNadawcza::MODE_COURIER);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_ack'])) {
                        $done = CourierAcknowlegmentCard::generateCardMulti($courierModels, $bulkNo);
                        Courier::groupChangeStatusByIds($done,CourierStat::STAT_MANIFEST_GENERATED, Yii::app()->user->id);

                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_return_label'])) {
                        $done = ReturnLabel::generateMulti($courierModels);
                        Courier::groupChangeStatusByIds($done,CourierStat::STAT_RETURN_LABEL_GENERATED, Yii::app()->user->id);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_ack_operator'])) {
                        $done = CourierAcknowlegmentCard::generateCardMultiPerOperator($courierModels, $bulkNo);
                        Courier::groupChangeStatusByIds($done,CourierStat::STAT_MANIFEST_GENERATED, Yii::app()->user->id);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_label_10x15'])) {
                        $done = LabelPrinter::generateLabels($courierModels, LabelPrinter::PDF_ROLL, true);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_label_10x15_delivery'])) {
                        $done = LabelPrinter::generateLabels($courierModels, LabelPrinter::PDF_ROLL, true, true);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_xls'])) {
                        $done = S_CourierIO::exportToXls($courierModels);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_ack_returns'])) {
                        $temp = [];
                        $temp2 = [];
                        $done = CourierAcknowlegmentCard::generateCardMulti($courierModels, $bulkNo, false, $temp, false, $temp2, true);
                        Courier::groupChangeStatusByIds($done,CourierStat::STAT_RETURN_MANIFEST_GENERATED, Yii::app()->user->id);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_ack_dpd'])) {
                        $done = DpdAckCard::generate($courierModels, $bulkNo, $numkat);

                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }

                }

                // important!
                Yii::app()->end();
            }
        }


        $model->items_ids = '';
        $this->render('index',
            [
                'model' => $model,
            ]);

    }
}
