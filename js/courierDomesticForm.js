(function($) {

    $(document).ready(function(){

        // RUCH:
        $(function(){
            var initialized = false;
            // bind on just on click to get fresh data from form

            if(!initialized)
                $('#ruch_point_address').on('click', function()
                {
                    initialized = true;
                    $(this).unbind('click');
                    $(this).pwrgeopicker('popup', {
                        'form': {
                            'city': $('#CourierTypeDomestic_AddressData_receiver_city').val(),
                            'street': $('#CourierTypeDomestic_AddressData_receiver_address_line_1').val(),
                        },
                        'marker_icons' : {
                            'mouseover': yii.urls.mapIcoOrange,
                            'mouseout':  yii.urls.mapIco,
                            'mousedown': yii.urls.mapIcoGreen,
                            'mouseup': yii.urls.mapIco,
                        },
                        'auto_start': true,
//                        'CashOnDelivery': $('#CourierTypeDomestic_cod_value').val() > 0 ? true : false,
                        'onselect': function(data){

                            var address = data.StreetName + ', ' + data.City;

                            $('#ruch_point').val(data.DestinationCode);
                            $('#ruch_point_address').val(address);
                        }
                    });
                    $(this).trigger('click');
                });
        });
        //

        $('[data-bootstrap-checkbox], [data-courier-addition]').checkboxpicker({
            offClass : 'btn-default btn-sm',
            onClass : 'btn-primary btn-sm',
            defaultClass: 'btn-sm',
        });


        $('[data-copy-bank-account]').on('click', function(){
            $('[data-bank-account-target]').val($(this).attr('data-copy-bank-account'))
        });

        $('#domestic_operator_id_selector').on('change', function(){
            personalizeFormForOperator($(this).val());
        });

        personalizeFormForOperator($('#domestic_operator_id_selector').val());

        if($('[data-cod-info]').val() > 0)
            $('#cod-info').fadeIn();
        else
            $('#cod-info').fadeOut();

        $('[data-cod-info]').on('change', function(){
            if($(this).val() > 0)
                $('#cod-info').fadeIn();
            else
                $('#cod-info').fadeOut();
        });


        var working = false;

        var $additions = $('#additions');
        var $additionsOverlay = $('.da-overlay');
        var $domesticOperatorSelector = $('#domestic_operator_id_selector');
        var $form = $("#domestic-form").find('form');

        if($additions.find('.domestic-additions-content').is(':empty'))
        {

            $additions.hide();
        } else {
            $additionsOverlay.hide();
            binder();
        }

        $form.find('[data-additions-dependency]').on('change', getAdditions);

        function binder()
        {
            var $additionItem = $(".addition_item").children().not(".with-checkbox");
            $additionItem.css("cursor", "pointer");
            $additionItem.unbind('click');
            $additionItem.on('click', function(){
                var $checkBox = $(this).parent().find("input[type=checkbox]");
                $checkBox.prop("checked", !$checkBox.prop("checked"));
            });

            $("[data-group] input[type=checkbox]").unbind('change');
            $("[data-group] input[type=checkbox]").on("change", function(){
                $("[data-group=" + $(this).closest("[data-group]").attr("data-group") + "] input[type=checkbox]").not($(this)).prop("checked", false);
            });

            $("[data-group] label").unbind("click");
            $("[data-group] label").on("click", function(){
                $(this).closest('[data-group]').find('input[type=checkbox]').trigger('change');
            });
        }

        function getAdditions()
        {


            if($domesticOperatorSelector.val() != '' && $("input[data-additions-dependency]").filter(function () {
                    return $.trim($(this).val()).length == 0
                }).length == 0) // all required fileds are filled
            {

                if(working)
                    return false;

                working = true;

                $additionsOverlay.fadeIn();

                $.ajax({
                    method: "POST",
                    url: yii.urls.getAdditions,
                    data: $("#domestic-form").find('form').serialize(),
                    dataType: 'json',
                })
                    .done(function (data) {

                        $additions.find('.domestic-additions-content').html(data);
                        $additionsOverlay.hide();
                        $additions.fadeIn();

                        $('[data-courier-addition]').checkboxpicker({
                            offClass : 'btn-default btn-sm',
                            onClass : 'btn-primary btn-sm',
                            defaultClass: 'btn-sm',
                        });

                        binder();

                        $additionsOverlay.hide();
                        working = false;
                    })
                    .fail(function (e) {
                        console.log(e);
                    });
            } else {
                $additions.find('.domestic-additions-content').html('');
                $additions.hide();
            }
        }
    });

    function personalizeFormForOperator(operator)
    {
        //if(operator == yii.operators.dhlPaleta)
        //{
        //    $('[data-pallette-ignore]').hide();
        //}
        //else
        //{
        //    $('[data-pallette-ignore]').show();
        //}

        // RUCH:
        if(operator == yii.operators.ruch)
        {
            $('#part-paczkaWRuchu').show();
        }
        else
        {
            $('#part-paczkaWRuchu').hide();
        }
        //

//        if(operator == <?//= CourierDomesticOperator::OPERATOR_INPOST;?>// || operator == <?//= CourierDomesticOperator::OPERATOR_GLS;?>//) {
//            $('[data-packages-no]').show();
//        } else {
//            $('[data-packages-no]').show();
////            $('[data-packages-no]').hide();
//        }

        if(operator == yii.operators.gls) {
            $('#cod-row').hide();
            $('[data-cod-value]').val(0);
        }
        else
        {
            $('#cod-row').show();
        }

    }

})(jQuery);