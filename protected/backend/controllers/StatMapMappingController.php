<?php

class StatMapMappingController extends Controller
{
    public function init()
    {
        Yii::app()->getModule('courier');
        Yii::app()->getModule('postal');

        parent::init();
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionManage($id, $manage_type)
    {
        if(!isset(StatMap::mapNameList()[$id]))
            throw new Exception(404);

        if(!isset(StatMapMapping::getTypeList()[$manage_type]))
            throw new Exception(404);

        $model = new _StatMapMappingGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['StatMapMappingGridView']))
            $model->setAttributes($_GET['StatMapMappingGridView']);

        $model->type = $manage_type;
        $model->map_id = $id;

        $this->render('manage', array(
            'model' => $model,
            'type' => $manage_type,
            'id' => $id,
        ));

    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = StatMapMapping::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        SystemLog::addToLog(SystemLog::TYPE_MAPPING_REMOVED, print_r($model->attributes,1));

        $model->delete();

        $this->redirect(Yii::app()->request->urlReferrer);
        Yii::app()->end();
    }

    /**
     * Lists all models.
     */
    public function actionIndex($type)
    {
        if(!isset(StatMapMapping::getTypeList()[$type]))
            throw new Exception(404);

        $this->render('index',
            [
                'type' => $type,
            ]);
    }


    /**
     * Lists all models.
     */
    public function actionNew($type)
    {
        if(!isset(StatMapMapping::getTypeList()[$type]))
            throw new Exception(404);

        if(isset($_POST['Map']))
        {
            $stats = $_POST['Map']['stats'];
            $map = $_POST['Map']['id'];

            $result = 0;
            if(is_array($stats)) {

                $cmd = Yii::app()->db->createCommand();
                // clear old in case... rather useless?
                $cmd->delete('stat_map_mapping', 'type = :type AND stat_id IN (' . implode(',', $stats) . ')', [':type' => $type]);

                foreach ($_POST['Map']['stats'] AS $stat_id) {
                    $cmd = Yii::app()->db->createCommand();
                    $result += intval($cmd->insert('stat_map_mapping', ['stat_id' => $stat_id, 'type' => $type, 'map_id' => $map, 'admin_id' => Yii::app()->user->id]));
                }
            }

            if($result)
                Yii::app()->user->setFlash('success', 'Number of mapped statuses: '.$result);
            else
                Yii::app()->user->setFlash('danger', 'Number of mapped statuses: 0');

            $this->refresh();
        }

        $this->render('new',
            [
                'type' => $type,
            ]);
    }

    public function actionSynchronize($type)
    {

        $targetType = $type;

        if(!isset(StatMapMapping::getTypeList()[$targetType]))
            throw new Exception(404);

        if($targetType == StatMapMapping::TYPE_COURIER)
            $sourceType = StatMapMapping::TYPE_POSTAL;
        else if($targetType == StatMapMapping::TYPE_POSTAL)
            $sourceType = StatMapMapping::TYPE_COURIER;
        else
            $sourceType = false;

        if(!isset(StatMapMapping::getTypeList()[$sourceType]))
            throw new Exception(404);

        if($targetType == $sourceType)
            throw new Exception(404);


        $mode = false;
        $statList = [];

        $statTargets = [];
        if(isset($_POST['preview']))
        {
            $mode = 1;
            $statList = StatMapMapping::listStatsNotMapped($targetType);
            foreach($statList AS $stat)
            {
                $statTargets[$stat->id] = StatMapMapping::getStatByName($stat->name, $sourceType);
            }
        }

        if(isset($_POST['synchronize']))
        {
            if($targetType == StatMapMapping::TYPE_COURIER)
            {
                $models = CourierStat::model();
            }
            else if($targetType == StatMapMapping::TYPE_POSTAL)
            {
                $models = PostalStat::model();
            }
            else $models = false;

            $toSynchronize = $_POST['Synchronization']['toSynchronize'];
            if($models)
            {
                $result = 0;
                if(is_array($toSynchronize)) {
                    $toSynchronize = array_keys($toSynchronize);
                    $maps = $_POST['Synchronization']['map_id'];

                    $cmd = Yii::app()->db->createCommand();
                    $cmd->delete('stat_map_mapping', 'type = :type AND stat_id IN (' . implode(',', $toSynchronize) . ')', [':type' => $targetType]);

                    foreach ($toSynchronize AS $stat_id) {

                        $cmd = Yii::app()->db->createCommand();
                        $result += intval($cmd->insert('stat_map_mapping', ['stat_id' => $stat_id, 'type' => $targetType, 'map_id' => $maps[$stat_id], 'admin_id' => Yii::app()->user->id]));
                    }
                }
                if($result)
                    Yii::app()->user->setFlash('success', 'Number of mapped statuses: '.$result);
                else
                    Yii::app()->user->setFlash('danger', 'Number of mapped statuses: 0');

                return $this->refresh();
            }
        }

        $this->render('synchronize',
            [
                'type' => $targetType,
                'sourceType' => $sourceType,
                'statTargets' => $statTargets,
                'mode' => $mode,
                'statList' => $statList
            ]);
    }

    public function actionApplyMapping($type)
    {
        if(!isset(StatMapMapping::getTypeList()[$type]))
            throw new Exception(404);

        $counter = 0;
        if(isset($_POST['apply'])) {
            if ($type == StatMapMapping::TYPE_COURIER) {
                $table = 'courier';
                $model = Courier::model();
            }
            else if ($type == StatMapMapping::TYPE_POSTAL) {
                $table = 'postal';
                $model = Postal::model();
            }


            /* @var $mapping StatMapMapping */
            foreach(StatMapMapping::model()->findAllByAttributes(['type' => $type]) AS $mapping)
            {
                $models = $model->findAll($table.'_stat_id = :stat AND (stat_map_id != :map OR stat_map_id IS NULL) AND stat_map_id != :stat_map_return', [':stat' => $mapping->stat_id, ':map' => $mapping->map_id, ':stat_map_return' => StatMap::MAP_RETURN]);

                /* @var $model Courier */
                foreach ($models AS $model)
                {
                    $previous = $model->stat_map_id;
                    $model->stat_map_id = $mapping->map_id;
                    $model->update(['stat_map_id']);
                    $model->afterStatMapChange($previous, $mapping->map_id, true, true, NULL, NULL, false, $mapping->stat_id);
                    $counter++;
                }

//                /* @var $cmd CDbCommand */
//                $cmd = Yii::app()->db->createCommand();
//                $counter += intval($cmd->update($table, ['stat_map_id' => $mapping->map_id], $table.'_stat_id = :stat AND (stat_map_id != :map OR stat_map_id IS NULL) AND stat_map_id != :stat_map_return', [':stat' => $mapping->stat_id, ':map' => $mapping->map_id, ':stat_map_return' => StatMap::MAP_RETURN]));
            }

            StatMapMapping::clearAbandonedMappings($type);
        }


        if($counter)
            Yii::app()->user->setFlash('success', 'Number of items with map applied: '.$counter);
        else
            Yii::app()->user->setFlash('danger', 'Number of items with map applied: 0');

        $this->redirect(['index', 'type' => $type]);
    }

    public function actionSetNewMapping($type, $stat_id)
    {
        if(!in_array($type, array_keys(StatMapMapping::getTypeList())))
            throw new CHttpException(400);

        $map_id = $_POST['stat_map_id'];


        $cmd = Yii::app()->db->createCommand();
        $cmd->delete('stat_map_mapping', 'type = :type AND stat_id = :stat_id', [':type' => $type, ':stat_id' => $stat_id]);

        $result = 0;

        $cmd = Yii::app()->db->createCommand();
        $result += intval($cmd->insert('stat_map_mapping', ['stat_id' => $stat_id, 'type' => $type, 'map_id' => $map_id, 'admin_id' => Yii::app()->user->id]));

        Yii::app()->user->setFlash('success', 'Stat map changed!');

        if($type == StatMapMapping::TYPE_COURIER)
            $this->redirect(Yii::app()->createUrl('/courier/courierStat/admin'));
        else if($type == StatMapMapping::TYPE_POSTAL)
            $this->redirect(Yii::app()->createUrl('/postal/postalStat/admin'));
    }

    public function actionAssingStatusByGridView($type)
    {

        if(!in_array($type, array_keys(StatMapMapping::getTypeList())))
            throw new CHttpException(400);

        $map_id = $_POST['stat_map_id'];
        $stat_ids = $_POST['StatMapMapping']['ids'];

        $stat_ids = explode(',', $stat_ids);

        $cmd = Yii::app()->db->createCommand();
        $cmd->delete('stat_map_mapping', 'type = :type AND stat_id IN (' . implode(',', $stat_ids) . ')', [':type' => $type]);

        $result = 0;
        foreach ($stat_ids AS $stat_id) {

            $cmd = Yii::app()->db->createCommand();
            $result += intval($cmd->insert('stat_map_mapping', ['stat_id' => $stat_id, 'type' => $type, 'map_id' => $map_id, 'admin_id' => Yii::app()->user->id]));
        }

        Yii::app()->user->setFlash('success', 'Stat map changed!');

        if($type == StatMapMapping::TYPE_COURIER)
            $this->redirect(Yii::app()->createUrl('/courier/courierStat/admin'));
        else if($type == StatMapMapping::TYPE_POSTAL)
            $this->redirect(Yii::app()->createUrl('/postal/postalStat/admin'));
    }

}
