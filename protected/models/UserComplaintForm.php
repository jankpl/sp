<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class UserComplaintForm extends CFormModel
{
    protected $what;
    protected $item_hash;
    protected $what_id;
    protected $item_model;
    protected $item_admin_link;
    protected $success_customer_url;

    public $name;
    public $email;
    public $tel;
    public $body;
    public $send_copy = true;

    public $file_1;
    public $file_2;
    public $file_3;

    const WHAT_COURIER = 1;
    const WHAT_POSTAL = 2;

    public static function getRedirectUrl($what)
    {
        switch($what)
        {
            case self::WHAT_COURIER:
                return Yii::app()->urlManager->createUrl('/courier/courier/list');
                break;
            case self::WHAT_POSTAL:
                return Yii::app()->urlManager->createUrl('/postal/postal/list');
                break;
            default:
                return Yii::app()->urlManager->createUrl('/');
                break;
        }
    }

    public static function newComplaint($what, $item_hash)
    {

        if(Yii::app()->user->isGuest)
            throw new Exception('Complaint allowed only for logged users!');

        $model = new self;
        $model->item_hash = $item_hash;

        if($what == self::WHAT_COURIER)
        {
            $model->what = self::WHAT_COURIER;

            Yii::app()->getModule('courier');
            $model->item_model = Courier::model()->findByAttributes(['hash' => $model->item_hash, 'user_id' => Yii::app()->user->id]);

            if($model->item_model === NULL)
                throw new Exception('Item not found!');

            if(!$model->item_model->isUserComplaintPossible())
                throw new Exception('This package cannot be complained!',5000);

            $model->item_admin_link = 'https://swiatprzesylek.pl/nysa.php/courier/courier/'.$model->item_model->id;

            $model->success_customer_url = Yii::app()->urlManager->createUrl('/courier/courier/list');
            $model->what_id = $model->item_model->local_id;
        }
        else if($what == self::WHAT_POSTAL)
        {
            $model->what = self::WHAT_POSTAL;

            Yii::app()->getModule('postal');
            $model->item_model = Postal::model()->findByAttributes(['hash' => $model->item_hash, 'user_id' => Yii::app()->user->id]);


            if($model->item_model === NULL)
                throw new Exception('Item not found!');

            if(!$model->item_model->isUserComplaintPossible())
                throw new Exception('This package cannot be complained!',5000);

            $model->item_admin_link = 'https://swiatprzesylek.pl/nysa.php/postal/postal/'.$model->item_model->id;

            $model->success_customer_url = Yii::app()->urlManager->createUrl('/postal/postal/list');
            $model->what_id = $model->item_model->local_id;
        }
        else
            throw new Exception('Unknown WHAT complaint type!');



        $model->name = Yii::app()->user->getModel()->name != '' ? Yii::app()->user->getModel()->name : Yii::app()->user->getModel()->company;
        $model->tel = Yii::app()->user->getModel()->tel;
        $model->email = Yii::app()->user->getModel()->email;


        return $model;
    }

    public function getSuccessCustomerUrl()
    {
        return $this->success_customer_url;
    }

    public function getWhatName()
    {
        return isset(self::getWhatList()[$this->what]) ? self::getWhatList()[$this->what] : '';
    }

    public function getWhatId()
    {
        return $this->what_id;
    }

    public static function getWhatList()
    {
        return [
            self::WHAT_COURIER => Yii::t('complaintForm', 'Przesyłka kurierska'),
            self::WHAT_POSTAL => Yii::t('complaintForm', 'Przesyłka listowa'),
        ];
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('name, email, body', 'required'),
            array('email', 'email'),
            array('send_copy', 'safe'),
            array('tel', 'length', 'max' => 32),

            array('file_1', 'file', 'maxFiles' => 1, 'maxSize' => 5242880 /* 5 MB */, 'allowEmpty' => true,  'types'=> implode(',',self::allowedExtensions())),
            array('file_2', 'file', 'maxFiles' => 1, 'maxSize' => 5242880 /* 5 MB */, 'allowEmpty' => true,  'types'=> implode(',',self::allowedExtensions())),
            array('file_3', 'file', 'maxFiles' => 1, 'maxSize' => 5242880 /* 5 MB */, 'allowEmpty' => true,  'types'=> implode(',',self::allowedExtensions())),
//			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
        );
    }


    public function attributeLabels()
    {
        return array(
            'name'=>Yii::t('complaintForm','Imię i nazwisko'),
            'tel'=>Yii::t('complaintForm','Telefon'),
            'email'=>Yii::t('complaintForm','Email'),
            'body'=>Yii::t('complaintForm','Treść reklamacji'),
            'file_1'=>Yii::t('complaintForm','Załącznik'),
            'file_2'=>Yii::t('complaintForm','Załącznik'),
            'file_3'=>Yii::t('complaintForm','Załącznik'),
            'send_copy'=>Yii::t('complaintForm','Wyślij kopię do mnie'),
        );
    }

    public static function allowedExtensions($withDot = false)
    {
        $data = [
            'pdf',
            'doc',
            'docx',
            'jpg',
            'jpeg',
            'png',
            'gif',
            'txt',
            'bmp'
        ];

        if($withDot)
            foreach($data AS $key => $item)
                $data[$key] = '.'.$item;

            return $data;
    }

    public function send()
    {
        $to = [
            'cs@swiatprzesylek.pl',
//            'reklamacje@swiatprzesylek.pl',
        ];

        if($this->what == self::WHAT_COURIER) {
            /* @var $model Courier */
            $this->item_model->markUserComplaint();
        } else if($this->what == self::WHAT_POSTAL) {
            /* @var $model Postal */
            $this->item_model->markUserComplaint();
        }

        $attachmentPath = [];
        $attachmentName = [];

        $files = ['file_1', 'file_2', 'file_3'];

        foreach($files AS $item)
        {
            $file = $this->$item;
            if($file) {
                $attachmentPath[] = $file->tempName;
                $attachmentName[] = $file->name;
            }
        }

        if($this->send_copy) {

            $text = Yii::t('complaintForm', 'Imię i nazwisko') . ': <span style="font-weight: bold;">' . $this->name . '</span><br/>';
            $text .= Yii::t('complaintForm', 'Email') . ': <span style="font-weight: bold;">' . $this->email . '</span><br/>';
            $text .= Yii::t('complaintForm', 'Telefon') . ': <span style="font-weight: bold;">' . $this->tel . '</span><br/><br/>';
            $text .= Yii::t('complaintForm', 'Dotyczy') . ': <span style="font-weight: bold;">' . $this->getWhatName() . '</span><br/>';
            $text .= Yii::t('complaintForm', 'Numer') . ': <span style="font-weight: bold;">' . $this->getWhatId() . '</span><br/><br/>';
            $text .= Yii::t('complaintForm', 'Załączników') . ': <span style="font-weight: bold;">' . S_Useful::sizeof($attachmentPath) . '</span><br/><br/>';
            $text .= '---<br/><br/>' . $this->body;

            S_Mailer::sendToCustomer($this->email, $this->name, Yii::t('complaintForm', '(kopia) Reklamacja') . ' : ' . $this->getWhatName() . ' / ' . $this->getWhatId(), $text, true, NULL, true, false, false, 'reklamacje@swiatprzesylek.pl', 'Reklamacje ŚP', $this->item_model->user->source_domain);
        }

        $text = 'Nadawca reklamacji: <span style="font-weight: bold;">'.$this->name.'</span><br/>';
        $text .= 'Adres email: <span style="font-weight: bold;">'.$this->email.'</span><br/>';
        $text .= 'Nr tel: <span style="font-weight: bold;">'.$this->tel.'</span><br/><br/>';
        $text .= 'Dotczy: <span style="font-weight: bold;">'.$this->getWhatName().'</span><br/>';
        $text .= 'Id: <span style="font-weight: bold;">'.$this->getWhatId().' (<a href="'.$this->item_admin_link.'">link</a>)</span><br/><br/>';
        $text .= 'Załączników: <span style="font-weight: bold;">'.S_Useful::sizeof($attachmentPath).'</span><br/><br/>';
        $text .= '---<br/><br/>'.$this->body;

        return  S_Mailer::sendToAdministrator('Reklamacja : '.$this->getWhatName().' / '.$this->getWhatId(), $text, $attachmentPath, $attachmentName, $to, $this->email);

    }
}