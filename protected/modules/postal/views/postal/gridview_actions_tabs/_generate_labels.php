<?php
/* @var $id_prefix string */
?>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-labels',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/generateLabelsByGridView'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php echo CHtml::dropDownList('Postal[type]','', PostalLabelPrinter::getTypeNames(), array('style' => 'width: 120px; display: inline-block;'));?>

<?php
echo TbHtml::submitButton(Yii::t('postal','Generate labels'), array(
    'onClick' => 'js:
    $("#'.$id_prefix.'postal-ids-labels").val($("#postalgrid").selGridViewNoGet("getAllSelection").toString());
    ;',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => $id_prefix.'postal-ids-labels')); ?>
<?php
$this->endWidget();
?>
<br/>
<small><?= Yii::t('postal', 'Packages without labels or damaged labels will be omitted.');?></small>
