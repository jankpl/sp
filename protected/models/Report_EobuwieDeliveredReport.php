<?php

class Report_EobuwieDeliveredReport extends Report
{
    const SEPARATOR = '; ';

    public static function processReport()
    {
        if(date('d') != 1)
            return false;

        Yii::app()->getModule('courier');

        $date = date('Y-m-d');
        $model = self::model()->find('type = :type AND DATE(date_entered) = :date AND stat = 1', [':type' => self::TYPE_EOBUWIE_DELIVERED, ':date' => $date]);

        if($model)
            return false;

        $model = new self;
        $model->type = self::TYPE_EOBUWIE_DELIVERED;

        $model->date_entered = date('Y-m-d H:i:s');
        $model->date_sent = date('Y-m-d H:i:s');
        $model->what_id = 0;
        $model->stat = 1;
        $model->save();

        $date = new DateTime();
        $date->sub(new DateInterval('P1D'));
        $dateTo = $date->format('Y-m-d');

        $date->sub(new DateInterval('P6D'));
        $dateFrom = $date->format('Y-m-d');

        echo $dateFrom.' : '.$dateTo;
        echo '<br/>';

        $models = Courier::model()->findAll('user_id IN (947, 2036, 2037) AND stat_map_id = 40 AND date_delivered BETWEEN "'.$dateFrom.' 00:00:00" AND "'.$dateTo.' 23:59:59" AND courier_type != '.Courier::TYPE_RETURN);

        $filename = 'eob_delivered_rap_'.$date->format('Y-m_W');

        list($xls, $pdf) = EobuwieDeliveredRaport::generateCardMulti($models, $filename);

        $to = [
            'mkalinska@eobuwie.com.pl',
            'piotrmaksymowicz@swiatprzesylek.pl',
            'maciejszostak@swiatprzesylek.pl',
            'jankopec@swiatprzesylek.pl',
        ];

        S_Mailer::send($to, $to, 'Courier delivered report ('.$date->format('Y-m_W').')', 'Reports are attached - week: '.$date->format('Y-m_W'), false, NULL, NULL,true, false, [$filename.'.xlsx', $filename.'.pdf'], false, false, false, [$xls, $pdf]);

    }

}