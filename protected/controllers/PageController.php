<?php

class PageController extends Controller {

    public $backLink = true;
    public $layout = 'other';


    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function init()
    {
        parent::init();

        $this->layout = 'other';
    }

    public function actionIndex() {

        $this->redirect(['/']);
    }

    public function actionView($id) {


        if($id == 2) {
            $this->redirect(['/site/regulations']);
            Yii::app()->end();
        }


        $criteria = new CDbCriteria();
        $criteria->condition = 'page_id=:page_id AND language_id=:language_id AND stat = 1';
        $criteria->params = array(
            ':page_id'=>$id,
            ':language_id'=>Yii::app()->params['language'],
        );
        $model = PageTr::model()->find($criteria);

        // if not found, try default language
        if($model === NULL)
        {
            $criteria = new CDbCriteria();
            $criteria->condition = 'page_id=:page_id AND language_id=:language_id AND stat = 1';
            $criteria->params = array(
                ':page_id'=>$id,
                ':language_id'=> 1,
            );
            $model = PageTr::model()->find($criteria);
        }

        if($model === NULL)
            throw new CHttpException('404');

        $this->render('view', array(
			'model' => $model,
		));
	}





}