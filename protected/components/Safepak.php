<?php

class Safepak
{
//    const LOGIN = 'pkocon@kaabnl.nl';
//    const PASS = 'sdTv6Sza@4f&';
//    const URL = 'http://www.safepak.pl/api/';

    const COD_BANK = '26105014901000009137775475';

    const URL = 'http://www.test.safepak.pl/api/';
    const LOGIN = 'jtk@cskstudio.pl';
    const PASS = 'safepakjtk';

    private $session;

    // list of oversized couriers (labels requires orginal size on print)
    private static function oversizedCouriers()
    {
        $data = [];
        $data[] = 'DPD';
        $data[] = 'FedEx';

        return $data;
    }

    protected function getSession()
    {
        return $this->session;
    }

    protected function setSession($val)
    {
        $this->session = $val;
    }

    protected function process($method, $data)
    {

        return [
            'success' => false,
            'log' => 'Safepax is over.'
        ];


        // fix for weird safepak problem with "&"
        $data = json_decode(str_replace('&', ' ', json_encode($data)));

        //create Post request
        $options = array
        (
            'http' => array
            (
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($data),
            )
        );


        try {
            $context = stream_context_create($options);
            $result = file_get_contents(self::URL . $method . '.xml', false, $context);
            $result = simplexml_load_string($result);


            if ((string)$result->status === 'OK') {
                return [
                    'success' => true,
                    'data' => $result
                ];
            } else {
                return [
                    'success' => false,
                    'log' => $result->message
                ];
            }
        } catch (Exception $ex) {

            return [
                'success' => false,
                'log' => $ex->getMessage()
            ];
        }
    }


    protected function login()
    {
        $loginCache = Yii::app()->cache->get('safepak_login');

        if ($loginCache === false) {

            $data = [
                'email' => self::LOGIN,
                'password' => md5(self::PASS),
            ];

            $response = $this->process('login', $data);

            if ($response['success'] == true) {
                $response = $response['data'];
                Yii::app()->cache->set('safepak_login', (string)$response->session, 60 * 15); // keep login data for 15 min
                $this->setSession((string)$response->session);
                return ['success' => true];
            } else {
                $this->setSession(false);
                return $response;
            }

        } else {

            $this->setSession(Yii::app()->cache->get('safepak_login'));
            return ['success' => true];

        }
    }

    public static function getDhlHours($postCode, $date)
    {
        $model = new self;
        if (($response = $model->login()['success']) == true) {
            $data = [
                'session' => $model->getSession(),
                'type' => 'ex',
                'postCode' => $postCode,
                'date' => $date,
            ];

            $response = $model->process('dhlHours', $data);

            return $response;
        }
        return $response;
    }

    public static function getDpdHours($postCode, $date)
    {

        $model = new self;
        if (($response = $model->login()['success']) == true) {
            $data = [
                'session' => $model->getSession(),
                'postCode' => $postCode,
                'date' => $date,
            ];

            $response = $model->process('dpdHours', $data);

            return $response;
        }
        return $response;
    }

    public static function listCountries()
    {
        return [];

        $model = new self;
        if (($response = $model->login()['success']) == true) {
            $data = ['session' => $model->getSession()];

            $response = $model->process('countries', $data);

            if ($response['success'] == true) {
                return $response['data'];
            } else
                return false;
        }
        return $response;
    }

    public static function listOperators()
    {
        $model = new self;
        if (($response = $model->login()['success']) == true) {
            $data = [];
            $data['session'] = $model->getSession();


            $data['senderCountry'] = self::getCountryId(1);
            $data['receiverCountry'] = self::getCountryId(1);


            $package = [];
            $package['type'] = 702; // paczka
            $package['weight'] = 1;
            $package['length'] = 1;
            $package['width'] = 1;
            $package['height'] = 1;
            $package['unsortableShape'] = 0;
            $package['amout'] = 1;

            $data['packages'] = [$package];

            $response = $model->process('checkPrices', $data);

            if ($response['success'] == true)
                return $response['data'];
            else
                return false;


        }
        return $response;
    }

    protected static function addressDataToName(AddressData $addressData)
    {
        if($addressData->name == '')
            $name = $addressData->company;
        else
            $name = $addressData->name;

        $name = explode(' ', $name);
        $name = $name[0];
        return $name;
    }

    protected static function addressDataToLastName(AddressData $addressData)
    {
        if($addressData->name == '')
            $name = $addressData->company;
        else
            $name = $addressData->name;

        $name = explode(' ', $name);
        if (S_Useful::sizeof($name) > 1) {
            array_shift($name);
            $name = implode(' ', $name);
        } else {
            $name = $name[0];
        }
        return $name;
    }

    protected static function addressDataToStreet(AddressData $addressData)
    {
        $address = $addressData->address_line_1 . ' ' . $addressData->address_line_2;
        $address = trim($address);
        $address = explode(' ', $address);


        if (S_Useful::sizeof($address) > 1) {
            array_pop($address);
            $address = implode(' ', $address);
        } else {
            $address = $address[0];
        }
        return $address;
    }

    protected static function addressDataToHouseNumber(AddressData $addressData)
    {
        $address = $addressData->address_line_1 . ' ' . $addressData->address_line_2;
        $address = trim($address);
        $address = explode(' ', $address);


        if (S_Useful::sizeof($address) > 1) {
            $address = array_pop($address);
        } else {
            $address = $address[0];
        }
        return $address;
    }

    public static function findClosestPickupDate($serviceId, $senderZipCode, $forceOffset = 0)
    {
        $date = date('Y-m-d');
        if ($forceOffset)
            $date = S_Useful::workDaysNextDate($date, $forceOffset);

        $timeFrom = '08:00';
        $timeTo = '17:00';

        $dateCache = Yii::app()->cache->get('safepak_date_' . $serviceId . '_' . $senderZipCode);
        if ($dateCache === false OR $forceOffset) {

            if ($serviceId == 1) // DPD
            {
                for ($i = 1; $i <= 3; $i++) // three attempts to find first available date
                {
                    $data = self::getDpdHours($senderZipCode, $date);
                    if ($data['success'] == true) {

                        $response = $data['data']->timeSlots->timeSlot;

                        $item = $response[0]; // get first time slot
                        $timeFromTemp = (string)$item->timeFrom;
                        $timeToTemp = (string)$item->timeTo;

                        $fromEarliest = $timeFromTemp;
                        $toLatest = $timeToTemp;

                        $return = [
                            'date' => $date,
                            'timeFrom' => $fromEarliest,
                            'timeTo' => $toLatest,
                        ];
                        Yii::app()->cache->set('safepak_date_' . $serviceId . '_' . $senderZipCode, $return, 60 * 60);
                        return $return;


                    } else {
                        // increase date by one wroking day and check again
                        $date = S_Useful::workDaysNextDate($date, $i);
                    }
                }
                return false; // not found proper date

            } else if (in_array($serviceId, [37, 38, 8])) // DHL
            {
                for ($i = 1; $i <= 3; $i++) // three attempts to find first available date
                {
                    $data = self::getDhlHours($senderZipCode, $date);
                    if ($data['success'] == true) {

                        $fromEarliest = '23:00';
                        $toLatest = '01:00';

                        $response = $data['data']->timeSlots->timeSlot;

                        // merge time slots into one
                        foreach ($response AS $item) {
                            $timeFromTemp = (string)$item->timeFrom;
                            $timeToTemp = (string)$item->timeTo;

                            if (strtotime($timeFromTemp) < strtotime($fromEarliest))
                                $fromEarliest = $timeFromTemp;

                            if (strtotime($timeToTemp) > strtotime($toLatest))
                                $toLatest = $timeToTemp;
                        }

                        $return = [
                            'date' => $date,
                            'timeFrom' => $fromEarliest,
                            'timeTo' => $toLatest,
                        ];
                        Yii::app()->cache->set('safepak_date_' . $serviceId . '_' . $senderZipCode, $return, 60 * 60);
                        return $return;


                    } else {
                        // increase date by one wroking day and check again
                        $date = S_Useful::workDaysNextDate($date, $i);
                    }
                }
                return false; // not found proper date

            } else {
                // return default data on other operators
                $return = [
                    'date' => $date,
                    'timeFrom' => $timeFrom,
                    'timeTo' => $timeTo,
                ];
                Yii::app()->cache->set('safepak_date_' . $serviceId . '_' . $senderZipCode, $return, 60 * 60);
                return $return;
            }
        } else {
            return $dateCache;
        }
    }

    public static function orderPackageByCourierInternal(CourierTypeInternal $courierTypeInternal, AddressData $senderAddress, AddressData $receiverAddress, $operator_id, $returnError = true)
    {

        $data = [];
        $data['paymentType'] = 3; // z abonamentu

        $operator = CourierOperator::model()->findByPk($operator_id);
        $serviceId = $operator->get_Safepak_service();

        $data['serviceId'] = $serviceId;

        $data['senderName'] = self::addressDataToName($senderAddress);
        $data['senderLastName'] = self::addressDataToLastName($senderAddress);
        $data['senderCompany'] = $senderAddress->company;
        $data['senderStreet'] = self::addressDataToStreet($senderAddress);
        $data['senderHouseNumber'] = self::addressDataToHouseNumber($senderAddress);
        $data['senderFlatNumber'] = '';
        $data['senderPostCode'] = $senderAddress->zip_code;
        $data['senderCity'] = $senderAddress->city;
        $data['senderCountry'] = self::getCountryId($senderAddress->country_id);
        $data['senderPhone'] = $senderAddress->tel;
        $data['senderEmail'] = $senderAddress->fetchEmailAddress();

        $data['receiverName'] = self::addressDataToName($receiverAddress);
        $data['receiverLastName'] = self::addressDataToLastName($receiverAddress);
        $data['receiverCompany'] = $receiverAddress->company;
        $data['receiverStreet'] = self::addressDataToStreet($receiverAddress);
        $data['receiverHouseNumber'] = self::addressDataToHouseNumber($receiverAddress);
        $data['receiverFlatNumber'] = '';
        $data['receiverPostCode'] = $receiverAddress->zip_code;
        $data['receiverCity'] = $receiverAddress->city;
        $data['receiverCountry'] = self::getCountryId($receiverAddress->country_id);
        $data['receiverPhone'] = $receiverAddress->tel;
        $data['receiverEmail'] = $receiverAddress->fetchEmailAddress();


        $package = [];
        $package['type'] = 702; // paczka
        $package['weight'] = $courierTypeInternal->courier->package_weight;
        $package['length'] = $courierTypeInternal->courier->package_size_l;
        $package['width'] = $courierTypeInternal->courier->package_size_w;
        $package['height'] = $courierTypeInternal->courier->package_size_d;
        $package['unsortableShape'] = 0;
        $package['amout'] = 1;

        $data['packages'] = [$package];


        $data['content'] = $courierTypeInternal->courier->package_content;


        $data['referenceNumber1'] = $courierTypeInternal->courier->local_id;
        $data['referenceNumber2'] = '';
        $data['noLabel'] = 0;
        $data['comments'] = '';

        $pickupDate = self::findClosestPickupDate($serviceId, $senderAddress->zip_code);

        $data['pickupDate'] = $pickupDate['date'];
        $data['pickupTimeFrom'] = $pickupDate['timeFrom'];
        $data['pickupTimeTo'] = $pickupDate['timeTo'];

        $data['senderPlaceType'] = 901; // adres nadawcy
        $data['senderPoint'] = '';
        $data['receiverPlaceType'] = 1001; // adres odbiorcy
        $data['receiverPoint'] = '';

        $data['codAmount'] = $courierTypeInternal->cod_value;
        $data['codBankAccount'] = self::COD_BANK;

        $data['declaredValue'] = $courierTypeInternal->courier->package_value;


        if($courierTypeInternal->hasAdditionById(CourierAdditionList::SAFEPAK_ROD))
            $data['rod'] = 1;

        if($courierTypeInternal->hasAdditionById(CourierAdditionList::SAFEPAK_INSURANCE_5000) && $data['declaredValue'] < 5000)
            $data['declaredValue'] = 5000;

        if($courierTypeInternal->hasAdditionById(CourierAdditionList::SAFEPAK_INSURANCE_10000) && $data['declaredValue'] < 10000)
            $data['declaredValue'] = 10000;

        if($courierTypeInternal->hasAdditionById(CourierAdditionList::SAFEPAK_INSURANCE_20000) && $data['declaredValue'] < 20000)
            $data['declaredValue'] = 20000;

        $result = self::orderPackage($data);


        if ($result['success'] == true) {

            $result = $result['data'];

            $return = [];
            $temp = [];
            $temp['status'] = true;
            $temp['track_id'] = $result['track_id'];
            $temp['label'] = $result['label'];
            $temp['oversized'] = $result['oversized'];

            array_push($return, $temp);

            return $return;
        } else {
            if ($returnError) {
                $return = array(0 => array(
                    'error' => (string)$result['log']));
                return $return;
            } else {
                return false;
            }
        }
    }

    public static function orderPackageByCourierDomestic(CourierTypeDomestic $courierTypeDomestic, $returnError = false)
    {

        $data = [];
        $data['paymentType'] = 3; // z abonamentu

        $domesticOperator = CourierDomesticOperator::model()->cache(60 * 60)->findByPk($courierTypeDomestic->domestic_operator_id);
        $serviceId = $domesticOperator->get_Safepak_service();

        $data['serviceId'] = $serviceId;

        $data['senderName'] = self::addressDataToName($courierTypeDomestic->courier->senderAddressData);
        $data['senderLastName'] = self::addressDataToLastName($courierTypeDomestic->courier->senderAddressData);
        $data['senderCompany'] = $courierTypeDomestic->courier->senderAddressData->company;
        $data['senderStreet'] = self::addressDataToStreet($courierTypeDomestic->courier->senderAddressData);
        $data['senderHouseNumber'] = self::addressDataToHouseNumber($courierTypeDomestic->courier->senderAddressData);
        $data['senderFlatNumber'] = '';
        $data['senderPostCode'] = $courierTypeDomestic->courier->senderAddressData->zip_code;
        $data['senderCity'] = $courierTypeDomestic->courier->senderAddressData->city;
        $data['senderCountry'] = self::getCountryId($courierTypeDomestic->courier->senderAddressData->country_id);
        $data['senderPhone'] = $courierTypeDomestic->courier->senderAddressData->tel;
        $data['senderEmail'] = $courierTypeDomestic->courier->senderAddressData->fetchEmailAddress();

        $data['receiverName'] = self::addressDataToName($courierTypeDomestic->courier->receiverAddressData);
        $data['receiverLastName'] = self::addressDataToLastName($courierTypeDomestic->courier->receiverAddressData);
        $data['receiverCompany'] = $courierTypeDomestic->courier->receiverAddressData->company;
        $data['receiverStreet'] = self::addressDataToStreet($courierTypeDomestic->courier->receiverAddressData);
        $data['receiverHouseNumber'] = self::addressDataToHouseNumber($courierTypeDomestic->courier->receiverAddressData);
        $data['receiverFlatNumber'] = '';
        $data['receiverPostCode'] = $courierTypeDomestic->courier->receiverAddressData->zip_code;
        $data['receiverCity'] = $courierTypeDomestic->courier->receiverAddressData->city;
        $data['receiverCountry'] = self::getCountryId($courierTypeDomestic->courier->receiverAddressData->country_id);
        $data['receiverPhone'] = $courierTypeDomestic->courier->receiverAddressData->tel;
        $data['receiverEmail'] = $courierTypeDomestic->courier->receiverAddressData->fetchEmailAddress();

        $package = [];
        $package['type'] = 702; // paczka
        $package['weight'] = $courierTypeDomestic->courier->package_weight;
        $package['length'] = $courierTypeDomestic->courier->package_size_l;
        $package['width'] = $courierTypeDomestic->courier->package_size_w;
        $package['height'] = $courierTypeDomestic->courier->package_size_d;
        $package['unsortableShape'] = 0;
        $package['amout'] = 1;

        $data['packages'] = [$package];

        $data['content'] = $courierTypeDomestic->courier->package_content;

        $data['referenceNumber1'] = $courierTypeDomestic->courier->local_id;
        $data['referenceNumber2'] = '';
        $data['noLabel'] = 0;
        $data['comments'] = '';

        $pickupDate = self::findClosestPickupDate($serviceId, $courierTypeDomestic->courier->senderAddressData->zip_code);

        $data['pickupDate'] = $pickupDate['date'];
        $data['pickupTimeFrom'] = $pickupDate['timeFrom'];
        $data['pickupTimeTo'] = $pickupDate['timeTo'];

        $data['senderPlaceType'] = 901; // adres nadawcy
        $data['senderPoint'] = '';
        $data['receiverPlaceType'] = 1001; // adres odbiorcy
        $data['receiverPoint'] = '';

        $data['codAmount'] = $courierTypeDomestic->cod_value;
        $data['codBankAccount'] = self::COD_BANK;

        $data['declaredValue'] = $courierTypeDomestic->courier->package_value;

        if($courierTypeDomestic->hasAdditionById(CourierDomesticAdditionList::SAFEPAK_ROD))
            $data['rod'] = 1;

        if($courierTypeDomestic->hasAdditionById(CourierDomesticAdditionList::SAFEPAK_INSURANCE_5000) && $data['declaredValue'] < 5000)
            $data['declaredValue'] = 5000;

        if($courierTypeDomestic->hasAdditionById(CourierDomesticAdditionList::SAFEPAK_INSURANCE_10000) && $data['declaredValue'] < 10000)
            $data['declaredValue'] = 10000;

        if($courierTypeDomestic->hasAdditionById(CourierDomesticAdditionList::SAFEPAK_INSURANCE_20000) && $data['declaredValue'] < 20000)
            $data['declaredValue'] = 20000;


        $result = self::orderPackage($data);

        if ($result['success'] == true) {

            $result = $result['data'];
            $return = [];
            $temp = [];
            $temp['status'] = true;
            $temp['track_id'] = $result['track_id'];
            $temp['label'] = $result['label'];
            $temp['safepak_order_id'] = $result['safepak_order_id'];
            $temp['oversized'] = $result['oversized'];
            array_push($return, $temp);
            return $return;
        } else {
            if ($returnError) {
                $return = array(0 => array(
                    'error' => (string)$result['log']));
                return $return;
            } else {
                return false;
            }
        }
    }

    private static function orderPackage(array $data, $forceOffsetPickupDay = 0)
    {
        $model = new self;
        if (($response = $model->login()['success']) == true) {

            // trim too long data
            $data['senderName'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['senderName'], false)), 0, 19);
            $data['senderLastName'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['senderLastName'], false)), 0, 29);
            $data['senderCompany'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['senderCompany'], false)), 0, 100);
            $data['senderStreet'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['senderStreet'], false)), 0, 80);
            $data['senderHouseNumber'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['senderHouseNumber'], false)), 0, 7);
            $data['senderPostCode'] = mb_substr(trim($data['senderPostCode']), 0, 10);
            $data['senderCity'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['senderCity'], true)), 0, 50);
            $data['senderPhone'] = mb_substr(trim($data['senderPhone']), 0, 15);
            $data['senderEmail'] = mb_substr(trim($data['senderEmail']), 0, 100);

            $data['receiverName'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['receiverName'], false)), 0, 19);
            $data['receiverLastName'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['receiverLastName'], false)), 0, 29);
            $data['receiverCompany'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['receiverCompany'], false)), 0, 100);
            $data['receiverStreet'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['receiverStreet'], false)), 0, 80);
            $data['receiverHouseNumber'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['receiverHouseNumber'], false)), 0, 7);
            $data['receiverPostCode'] = mb_substr(trim($data['receiverPostCode']), 0, 10);
            $data['receiverCity'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['receiverCity'], true)), 0, 50);
            $data['receiverPhone'] = mb_substr(trim($data['receiverPhone']), 0, 15);
            $data['receiverEmail'] = mb_substr(trim($data['receiverEmail']), 0, 100);

            $data['senderPhone'] = str_pad($data['senderPhone'], 9, "0", STR_PAD_LEFT);
            $data['receiverPhone'] = str_pad($data['senderPhone'], 9, "0", STR_PAD_LEFT);

            $data['content'] = mb_substr(trim(S_Useful::removeNationalCharacters($data['content'], true)), 0, 100);

            // set proper session
            $data['session'] = $model->getSession();

            if($data['codAmount'] > 0)
            {
                $data['addServices802'] = 1;

                // if COD > declaredValue, make DeclaredValue = COD because DeclaredValue == Insurance Amount
                if($data['codAmount'] > $data['declaredValue'])
                    $data['declaredValue'] = $data['codAmount'];
            }

            // set proper pickup date
            $pickupDate = self::findClosestPickupDate($data['serviceId'], $data['senderPostCode'], $forceOffsetPickupDay);
            $data['pickupDate'] = $pickupDate['date'];
            $data['pickupTimeFrom'] = $pickupDate['timeFrom'];
            $data['pickupTimeTo'] = $pickupDate['timeTo'];

            $response = $model->process('makeOrder', $data);

            if ($response['success'] != true) {
                if ($response['log'] == 'Wystąpił błąd: Dla podanego kodu pocztowego nadawcy niemożliwe jest nadanie przesyłki w tym czasie.. ') {
                    if ($forceOffsetPickupDay >= 3)
                        return $response;
                    else {
                        return self::orderPackage($data, $forceOffsetPickupDay + 1);
                    }
                }

                return $response;
            }


            if ($response['success'] == true) {

                if(((string) $response['data']->message) == 'Zamówienie zostało zapisane w systemie, wystąpił jednak błąd z przekazaniem zamówienia do firmy kurierskiej. Skontaktujemy się z Tobą w tej sprawie.')
                {
                    return [
                        'success' => false,
                        'log' => 'Zamówienie zostało zapisane w systemie, wystąpił jednak błąd z przekazaniem zamówienia do firmy kurierskiej. Skontaktujemy się z Tobą w tej sprawie.'
                    ];
                }

                $orderId = (integer)$response['data']->orderId;
                return self::getLabelAndNumberByOrderId($orderId);
            } else
                return $response;
        } else return $response;
    }

    public static function getOrderDetails($orderId)
    {
        $model = new self;
        if (($response = $model->login()['success']) == true) {
            $data = ['session' => $model->getSession()];

            $response = $model->process('order/' . $orderId, $data);

            return $response;
        }
        return false;
    }

    public static function getDocument($labelId)
    {
        $model = new self;
        if (($response = $model->login()['success']) == true) {
            $data = ['session' => $model->getSession()];

            $response = $model->process('document/' . $labelId, $data);

            return $response;
        }
        return false;
    }

    public static function getTracking($orderId, $returnError = false)
    {

        $model = new self;
        if (($response = $model->login()['success']) == true) {
            $data = ['session' => $model->getSession()];

            $response = $model->process('order/' . $orderId, $data);
            if($response['success'])
            {

                $statuses = [];
                // sometimes Safepak does not provide status history
                if(isset($response['data']->orderDetails->statusHistory))
                {
                    foreach($response['data']->orderDetails->statusHistory->statusHistoryItem AS $statusItem)
                    {

                        $temp = array(
                            'name' => (string) $statusItem->status,
                            'date' => (string) $statusItem->time,
                            'location' => (string) $statusItem->place,
                        );
                        array_push($statuses, $temp);

                    }


                } else {

                    $temp = array(
                        'name' => (string)$response['data']->orderDetails->orderStatus,
                        'date' => '',
                        'location' => '',
                    );
                    array_push($statuses, $temp);

                }

                return $statuses;
            } else
                if($returnError)
                    return $response;
                else
                    return false;
        }
        return false;
    }

    public static function getLabelAndNumberByOrderId($orderId)
    {
        $response = self::getOrderDetails($orderId);
        if ($response['success'] != true)
            return $response;
        $response = $response['data'];

        $response = $response->orderDetails;
        $number = (string)$response->number;


        // search for "etykieta zebra"
        $label_id = $response->documents->xpath('//documents/document[type="1402"]/id');

        // if not found, get regular label
        if(!S_Useful::sizeof($label_id))
            $label_id = $response->documents->xpath('//documents/document[type="1401"]/id');

        $label_id = (integer) $label_id[0];

        $oversized = false;
        $courierOperator = (string) $response->courier;
        if(in_array($courierOperator, self::oversizedCouriers()))
            $oversized = true;

        $responseLabel = self::getDocument($label_id);

        if ($responseLabel['success'] == true) {
            $label = (string)$responseLabel['data']->documentContent;
            $label = base64_decode($label);

            return [
                'success' => true,
                'data' => [
                    'label' => $label,
                    'track_id' => $number,
                    'safepak_order_id' => $orderId,
                    'oversized' => $oversized,
                ],
            ];
        } else return $responseLabel;

    }

    /**
     * Returns ID of country in Safepak system based on polish name of country
     * @param $local_country_id
     * @return integer
     */
    protected static function getCountryId($local_country_id)
    {
        $localCountryTr = CountryListTr::model()->cache(60 * 60)->findByAttributes(['language_id' => 1, 'country_list_id' => $local_country_id]);
        $localName = $localCountryTr->name;

        $temp = [];
        $safepakCountryList = Yii::app()->cache->get('safepakCountryList');
        if ($safepakCountryList === false) {
            $safepakCountryList = self::listCountries();

            foreach ($safepakCountryList->countries->country AS $item) {
                $temp[(string)$item->name] = (int)$item->id;
            }

            Yii::app()->cache->set('safepakCountryList', $temp, 60 * 60);
        } else
            $temp = $safepakCountryList;

        $safepakCountryId = $temp[$localName];

        return $safepakCountryId;
    }

}