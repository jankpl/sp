<?php

/**
 * Class bDefaultTelNumberAddressData
 *
 * Behavior clears email address
 */
class bClearEmailAddressData extends bBaseBehavior
{

    protected $_allowedClass = [
        'CourierTypeInternalSimple_AddressData',
        'CourierTypeInternal_AddressData',
        'CourierTypeOoe_AddressData',
        'CourierTypeDomestic_AddressData',
        'CourierTypeU_AddressData',
        'CourierTypeExternalReturn_AddressData',
        'CourierTypeReturn_AddressData',
    ];

    public function afterValidate($event)
    {
        /* @var $that AddressData */
        $that = $this->owner();

        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest && Yii::app()->user->model->getClearEmails())
        {
            $that->email = '';
            $that->clearErrors('email');
        }

        return parent::afterValidate($event);
    }
}