<?php

class CourierInternalController extends Controller
{

    public function filters()
    {
        return array_merge([
            'accessControl', // perform access control for CRUD operations
        ]);
    }

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('ajaxCheckJustEbay',
                    'quequeLabelByLocalId',
                    'import',
                    'exportErroredImportData',
                    'deleteErroredImportData',
                    'listImported',
                    'ajaxRemoveRowOnImport',
                    'ajaxEditRowOnImport',
                    'ajaxUpdateRowItemOnImport',
                    'ajaxSaveRowOnImport',
                    'getLabels',
                    'ebay',
                    'ajaxListOfPoints',
                    'ajaxListOfPointsSp',
                    'ajaxLoadModalContent',
                    'linker',
                ),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function beforeAction($action)
    {

//        if(Yii::app()->user->getModel()->isPremium)
        {
            return parent::beforeAction($action);
//        } else {
//            $this->render('//site/notify', array(
//                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
//                'header' => 'Courier',
//
//            ));
//            exit;
        }

    }

    public function actionAjaxCheckJustEbay()
    {
        $local_id = $_POST['localId'];

        Yii::app()->getModule('courier');

        $rerun = false;
        $courier = Courier::model()->cache(1)->findByAttributes(array('local_id' => $local_id));
        if($courier === NULL)
            $stat = EbayImport::STAT_UNKNOWN;
        else
        {
            $courierEbayImport = $courier->courierEbayImport;

            if($courierEbayImport === NULL)
                $stat = EbayImport::STAT_UNKNOWN;
            else
            {
                $stat = $courierEbayImport->stat;
            }
        }

        if($stat == EbayImport::STAT_NEW)
            $rerun = true;

//        MyDump::dump('le_ajaxJustEbay.txt', print_r($local_id,1).' : '.print_r($courier->id,1).' '.print_r($courierEbayImport->attributes,1));

        echo CJSON::encode([
                'rerun' => $rerun,
                'stat' => $stat,
            ]
        );
    }

    public function actionQuequeLabelByLocalId()
    {

        $local_id = $_POST['localId'];

        Yii::app()->getModule('courier');

        $courier = Courier::model()->findByAttributes(array('local_id' => $local_id));

        $courier_id = $courier->id;

        /* @var $parent CourierTypeInternal */
        $parent = $courier->courierTypeInternal;

        $return = false;

        if($parent !== NULL)
        {
            $stat = $parent->getPrimaryLabelStatus();
            if($stat == CourierLabelNew::STAT_NEW) {

//                file_put_contents('dump2.txt', 'ORDER!:'.print_r($parent->id,1)."\r\n", FILE_APPEND);
                $return = $parent->orderPrimaryLabel();
//                file_put_contents('dump2.txt', 'ids:'.print_r($ids ? 'true' : 'false',1)."\r\n", FILE_APPEND);

            } else {
//                if($parent->_ebay_order_id != '')
//                    $parent->__temp_ebay_update_stat = CourierEbayImport::STAT_UNKNOWN;
//                $ids = [$courier->id];
            }



            $rerun = false;
            if($stat == CourierLabelNew::STAT_WAITING_FOR_PARENT OR ($stat == CourierLabelNew::STAT_NEW))
//            if($stat == CourierLabelNew::STAT_WAITING_FOR_PARENT OR ($stat == CourierLabelNew::STAT_NEW))
            {
                if(!$return)
                    $rerun = true;
                else
                {
                    //get current status
                    $courierRefreshed = Courier::model()->findByPk($courier_id);
                    $stat = $courierRefreshed->courierTypeInternal->getPrimaryLabelStatus();
                }
            }


            $log = '';
            if($stat == CourierLabelNew::STAT_FAILED)
            {
                $log = 'Error';
                if($courier->getSingleLabel(false, false, true) && $courier->getSingleLabel(false, false, true)->log != '')
                    $log = 'Error: '.$courier->getSingleLabel(false, false, true)->log;
            }


            $ebayStatus = EbayImport::STAT_UNKNOWN;
            if(!$rerun && $stat != CourierLabelNew::STAT_FAILED)
            {
                // get fresh data from DB
                $ebayStatus = CourierTypeInternal::model()->findByPk($parent->id)->_ebay_export_status;
            }

            $return = array(
                'request' => true,
                'stat' => $stat,
                'desc' => $log,
                'rerun' => $rerun,
            );

            if($parent->_ebay_order_id != '')
                $return['ebayImport'] = $ebayStatus;

//            MyDump::dump('le_quequeListInternal.txt', print_r($local_id,1).' : '.print_r($return,1));

            echo CJSON::encode($return);
            exit;
        } else {
            echo CJSON::encode(array('request' => false));
            exit;
        }
    }

    public function actionImport($hash = NULL)
    {

        $this->panelLeftMenuActive = 112;


        if(!Yii::app()->user->getModel()->payment_on_invoice)
        {

            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier',

            ));
            exit;
        }

        $fileModel = new F_InternalImport();

        if($hash !== NULL)
        {
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
            Yii::import('zii.behaviors.CTimestampBehavior');

            $models = Yii::app()->cacheUserData->get('internalImport_' . $hash.'_'.Yii::app()->session->sessionID);

            if(isset($_POST['refresh'])) {
                if(F_InternalImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
                }
                // save modified by validators models:
                Yii::app()->cacheUserData->set('internalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                $this->refresh();
                exit;
            }
            else if(isset($_POST['save']))
            {
//                Yii::app()->PM->m('1', 'BEFORE VALIDATE', true);
                if(F_InternalImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));

                    // save modified by validators models:               
                    Yii::app()->cacheUserData->set('internalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                    $this->refresh();
                    exit;
                }

                try
                {
//                    Yii::app()->PM->m('1', 'BEFORE SAVE WHOLE GROUP');
                    $result = CourierTypeInternal::saveWholeGroup($models);
                }
                catch (Exception $ex)
                {
                    Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
                    return;
                }
                // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING


                if($result['success'])
                {
                    $import = CourierImportManager::getByHash($hash);
                    /* @var $item Courier */
                    $ids = array();
                    if(is_array($result['data']))
                        foreach($result['data'] AS $item)
                        {
                            array_push($ids, $item->id);
                        }

                    $import->addCourierIds($ids);
                    $import->save();

//                    Yii::app()->PM->m('1', 'BEFORE REDIRECT');

                    Yii::app()->cacheUserData->delete('internalImport_rawdata_' . $hash.'_'.Yii::app()->session->sessionID);
                    Yii::app()->cacheUserData->delete('internalImport_' . $hash.'_'.Yii::app()->session->sessionID);
                    $this->redirect(Yii::app()->createUrl('/courier/courierInternal/listImported', ['hash' => $hash]));
                    exit;
                }

            } else {



                // validate only after editing one item in row
                if(Yii::app()->session['courierInternalImport_'.$hash] == 10)
                {
                    unset(Yii::app()->session['courierInternalImport_'.$hash]);
                    if (S_Useful::sizeof($models) && is_array($models))
                        F_InternalImport::validateModels($models);
                }


                if (!$models OR !S_Useful::sizeof($models)) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Import nie powiódł się!'));
                    $this->redirect(Yii::app()->createUrl('courier/courierInternal/import'));
                }

                $this->render('import/showModels', array(
                    'models' => $models,
                    'hash' => $hash,
                ));
                return;
            }
        }


        // file upload
        if(isset($_POST['F_InternalImport']) ) {
            $fileModel->setAttributes($_POST['F_InternalImport']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if (!$fileModel->validate()) {
                $this->render('import/index', array(
                    'model' => $fileModel,
                ));
                return;
            } else {
                // file upload success:

                if($fileModel->custom_settings)
                    $fileModel->applyCustomData($fileModel->custom_settings, Yii::app()->user->id);

                // generate new hash
                $hash = hash('sha256', time() . Yii::app()->user->id);

                $inputFile = $fileModel->file->tempName;

//                Yii::import('application.extensions.phpexcel.XPHPExcel');
//                XPHPExcel::init();

//                try {
//                    $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFile);
//                }
//                catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $ex)
                {
                    $inputFileType = 'Csv';
                }

                // WITH FIX:
                if(strtoupper(($inputFileType)) == 'CSV')
                {
                    // SET UTF8 ENCODING IN CSV
                    $file_content = file_get_contents( $inputFile );

                    $file_content = S_Useful::forceToUTF8($file_content);
                    file_put_contents( $inputFile, $file_content );

                    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                    // FIX ENCODING

                    if($fileModel->_customSeparator)
                        $objReader->setDelimiter($fileModel->_customSeparator);
                    else
                        $objReader->setDelimiter(',');


                } else {
                    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                }


                // WITHOUT FIX:
//
//                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//                if(strtoupper($inputFileType) == 'CSV' && $fileModel->_customSeparator)
//                {
//                    $objReader->setDelimiter($fileModel->_customSeparator);
//                }

                $objPHPExcel = $objReader->load($inputFile);

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $data = [];
                $start = 1;

                if ($fileModel->omitFirst) {
                    $headerSeen = false;
                } else
                    $headerSeen = true;

                for ($row = $start; $row <= $highestRow; $row++) {
                    if (!$headerSeen) {
                        $headerSeen = true;
                        continue;
                    }

                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                    array_push($data, $rowData[0]);
                }

                // remove empty lines:
                $data = (array_filter(array_map('array_filter', $data)));

                $numberOfLines = S_Useful::sizeof($data);

                // check just by lines number
                if($numberOfLines > F_InternalImport::MAX_LINES)
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość wierszy w pliku to: {no}! Naliczono: {number}', array('{no}' => F_InternalImport::MAX_LINES, '{number}' => $numberOfLines)));
                    $this->refresh(true);
                    exit;
                }

                $numberOfPackages = F_InternalImport::calculateTotalNumberOfPackages($data, $fileModel->_customOrder);
                // check by number of packages
                if($numberOfPackages > F_InternalImport::MAX_PACKAGES) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość paczek w pliku to: {no}! Naliczono: {number}', array('{no}' => F_InternalImport::MAX_PACKAGES, '{number}' => $numberOfPackages)));
                    $this->refresh(true);
                    exit;
                }

                $models = F_InternalImport::mapAttributesToModels($data, $fileModel->_customOrder, $fileModel->cutTooLong);
                F_InternalImport::validateModels($models);

                Yii::app()->cacheUserData->set('internalImport_rawdata_' . $hash.'_'.Yii::app()->session->sessionID, $data, 86400);
                Yii::app()->cacheUserData->set('internalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                $this->redirect(Yii::app()->createUrl('courier/courierInternal/import', ['hash' => $hash]));
                exit;
            }
        }

        $this->render('import/index', array(
            'model' => $fileModel,
        ));

    }

    protected function _exportOrDeleteErroredImportData($hash, $e, $export = false, $delete = false, $l = false)
    {
        if(!$export && !$delete)
            return false;

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');



        if(!$e && !$l) {
            // from file import
            $models = Yii::app()->cacheUserData->get('internalImport_' . $hash . '_' . Yii::app()->session->sessionID);
            $rawData = Yii::app()->cacheUserData->get('internalImport_rawdata_' . $hash . '_' . Yii::app()->session->sessionID);
        } else if($e) {
            $models = Yii::app()->cacheUserData->get('ebayImportInternal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        } else if($l)
        {
            $models = Yii::app()->cacheUserData->get('linkerImportInternal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        }


        if(!S_Useful::sizeof($models))
            return false;

        $toExport = [];

        foreach($models AS $key => $model)

            if(!$model->validate() OR !$model->senderAddressData->validate() OR !$model->receiverAddressData->validate())
            {
                if($export)
                {
                    if(!$e)
                        $toExport[] = $rawData[$key];
                    else
                        $toExport[] = $model;
                }


                if($delete)
                    unset($models[$key]);
            }

        if($delete) {
            if (!$e && !$l) {
                // from file import
                Yii::app()->cacheUserData->set('internalImport_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
            } else if($e) {
                Yii::app()->cacheUserData->set('ebayImportInternal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
            } else if($l) {
                Yii::app()->cacheUserData->set('linkerImportInternal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
            }


// used to force reloading list of imported packages with validation to, for example: recalculate prices
            Yii::app()->session['internalImport_' . ($e OR $l ? 'e_' : '') . $hash] = 10;
        }

        if($export)
        {
            if(!$e) {
                $toExport = S_XmlGenerator::fillEmptyKeys($toExport);
                S_XmlGenerator::generateXml($toExport);
            }
            else
                S_CourierIO::exportToXlsUser($toExport);
            Yii::app()->end();
        }

        if($delete)
        {
            $this->redirect(['/courier/courierInternal/import', 'hash' => $hash]);
        }

    }

    public function actionExportErroredImportData($hash, $e = false, $l=false)
    {
        self::_exportOrDeleteErroredImportData($hash, $e, true, false, $l);
    }

    public function actionDeleteErroredImportData($hash, $e = false, $l = false)
    {
        self::_exportOrDeleteErroredImportData($hash, $e, false, true, $l);
    }

    public function actionListImported($hash)
    {

        $this->panelLeftMenuActive = 110;


        $import = CourierImportManager::getByHash($hash);

        if($import->user_id != Yii::app()->user->id)
            throw new CHttpException(404);

        if(!$import)
            throw new CHttpException(404);

        // FASTER WAY:
        $couriers = [];
        if(is_array($import->courier_ids))
            $couriers = Courier::model()->with('courierTypeInternal')->with(['courierTypeInternal.commonLabel.courierLabelNew' => ['alias' => 'clcln']])->with(['courierTypeInternal.pickupLabel.courierLabelNew' => ['alias' => 'plcln']])->with(['courierTypeInternal.deliveryLabel.courierLabelNew' => ['alias' => 'dlcln']])->findAllByPk($import->courier_ids);

        $CACHE_NAME = 'COURIER_INTERNAL_IMPORT_INSTANCE_'.$hash;
        $cacheValue = Yii::app()->cache->get($CACHE_NAME);

        $mainInstance = false;
        if($cacheValue === false)
            $mainInstance = true;

        Yii::app()->cache->set($CACHE_NAME, 10, 60*60);


        $this->render('listImported',array(
            'import' => $import,
            'couriers' => $couriers,
            'hash' => $hash,
            'mainInstance' => $mainInstance
        ));

    }

    public function actionAjaxRemoveRowOnImport($e = false, $l = false)
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        if($e) {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImportInternal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);

            unset($models[$id]);
            Yii::app()->cacheUserData->set('ebayImportInternal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
        }
        else if($l)
        {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('linkerImportInternal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);

            unset($models[$id]);
            Yii::app()->cacheUserData->set('linkerImportInternal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);

        } else {

            // from file import
            $models = Yii::app()->cacheUserData->get('internalImport_' . $hash.'_'.Yii::app()->session->sessionID);
            unset($models[$id]);

            Yii::app()->cacheUserData->set('internalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        }

        Yii::app()->session['courierInternalImport_'.($e ? 'e_' : ($l ? 'l_' : '')) .$hash] = 10;

        echo CJSON::encode(array('success' => true));
    }

    public function actionAjaxEditRowOnImport($e = false, $l = false)
    {

        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        if($e) {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImportInternal_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        }
        else if($l)
        {
            // from linker import
            $models = Yii::app()->cacheUserData->get('linkerImportInternal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);

        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('internalImport_' . $hash.'_'.Yii::app()->session->sessionID);
        }

        $model = $models[$id];

        if(isset($model)) {


            $modelCourierAddition = array_keys($model->courierTypeInternal->_imported_additions_ok);
            $modelCourierAdditionList = CourierAdditionList::getAdditionsByCourier($model);

            $currency = Yii::app()->PriceManager->getCurrency();

            $additionsHtml = $this->renderPartial('import/_additionsHtml', array(
                'modelCourierAdditionList'=>$modelCourierAdditionList,
                'courierAddition'=>$modelCourierAddition,
                'currency' => $currency,
            ), true, false);

            $countries = CountryList::model()->cache(60*60)->with('countryListTr')->with('countryListTrs')->findAll();

            if($e)
                $html = $this->renderPartial('importEbay/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'e' => $e, 'additionsHtml' => $additionsHtml], true, true);
            else if($l)
                $html = $this->renderPartial('importLinker/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'l' => $e, 'additionsHtml' => $additionsHtml], true, true);
            else
                $html = $this->renderPartial('import/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'e' => $e, 'additionsHtml' => $additionsHtml], true, true);

//            file_put_contents('htmldump.txt', print_r($html,1));

            echo CJSON::encode(array('success' => true, 'html' => $html));

        } else {
            echo CJSON::encode(array('success' => false));
        }

    }

    public function actionAjaxUpdateRowItemOnImport($e = false, $l = false)
    {
        if(Yii::app()->request->isAjaxRequest) {

            $hash = $_POST['hash'];
            $i = $_POST['i'];
            $attribute = $_POST['attribute'];
            $val = $_POST['val'];

            $possibleToEdit = [
                'receiverAddressData.name',
                'receiverAddressData.company',
                'receiverAddressData.zip_code',
                'receiverAddressData.address_line_1',
                'receiverAddressData.address_line_2',
                'receiverAddressData.city',
                'receiverAddressData.tel',
                'receiverAddressData.email',
                'senderAddressData.name',
                'senderAddressData.company',
                'senderAddressData.zip_code',
                'senderAddressData.address_line_1',
                'senderAddressData.address_line_2',
                'senderAddressData.city',
                'senderAddressData.tel',
                'senderAddressData.email',
                'package_weight',
                'package_size_l',
                'package_size_d',
                'package_size_w',
            ];
            if(!in_array($attribute, $possibleToEdit))
                return false;

            try {
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
                Yii::import('zii.behaviors.CTimestampBehavior');

                if($e)
                    $models = Yii::app()->cacheUserData->get('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
                elseif($l)
                    $models = Yii::app()->cacheUserData->get('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
                else
                    $models = Yii::app()->cacheUserData->get('internalImport_' . $hash.'_'.Yii::app()->session->sessionID);

                /* @var $model Courier */
                if (is_array($models)) {
                    $model = $models[$i];

                    $submodel = false;
                    if ($model) {

                        $attribute = explode('.', $attribute);
                        if(S_Useful::sizeof($attribute) > 1)
                        {
                            $submodel = $attribute[0];

//                          $model = $model->$attribute[0];
                            // PHP7 fix
                            $temp = $attribute[0];
                            $model = $model->$temp;
//

                            $attribute = $attribute[1];
                        } else
                            $attribute = $attribute[0];


                        if ($model->hasAttribute($attribute)) {
                            $backupValue = $model->$attribute;
                            $model->$attribute = $val;

                            $model->validate([$attribute]);
                            if (!$model->hasErrors($attribute)) {

                                if($submodel)
                                    $models[$i]->$submodel = $model;
                                else
                                    $models[$i] = $model;

                                if($e)
                                    Yii::app()->cacheUserData->set('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                                elseif($l)
                                    Yii::app()->cacheUserData->set('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                                else
                                    Yii::app()->cacheUserData->set('internalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                                echo CJSON::encode(['success' => true]);
                                return;

                            } else {

                                echo CJSON::encode([
                                    'success' => false,
                                    'msg' => strip_tags($model->getError($attribute)),
                                    'backup' => $backupValue,
                                ]);
                                return;
                            }
                        }
                    }
                }

                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie. {msg}', ['{msg}' => print_r($attribute,1)])
                ]);
                return;

            } catch (Exception $ex) {
                Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);
                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie')
                ]);
                return;

            }
        }
    }

    public function actionAjaxSaveRowOnImport($e = false, $l = false)
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        if($e)
        {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImportInternal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);
        } elseif($l) {
            // from linker import
            $models = Yii::app()->cacheUserData->get('linkerImportInternal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);
        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('internalImport_' . $hash.'_'.Yii::app()->session->sessionID);
        }
        $model = $models[$id];

        if(isset($model)) {

            /* @var $model Courier_CourierTypeInternal */
            $model->courierTypeInternal->attributes = $_POST['CourierTypeInternal'];
            $model->senderAddressData->attributes = $_POST['CourierTypeInternal_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['CourierTypeInternal_AddressData']['receiver'];

            $model->senderAddressData->country0 = CountryList::model()->findByPk($model->senderAddressData->country_id);
            $model->receiverAddressData->country0 = CountryList::model()->findByPk($model->receiverAddressData->country_id);

            $model->attributes = $_POST['Courier_CourierTypeInternal'];


            if(isset($_POST['CourierAddition']))
                $model->courierTypeInternal->_imported_additions = $_POST['CourierAddition'];
            else
                $model->courierTypeInternal->_imported_additions = [];

//            $model->validate();
//            $model->courierTypeInternal->validate();
//            $model->senderAddressData->validate();
//            $model->receiverAddressData->validate();

            $temp = [];
            $temp[0] = $model;
            F_InternalImport::validateModels($temp);
            $models[$id] = $temp[0];

            if($e)
            {
                // from ebay import
                Yii::app()->cacheUserData->set('ebayImportInternal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            } elseif($l) {
                // from linker import
                Yii::app()->cacheUserData->set('linkerImportInternal_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            } else {
                // from file import
                Yii::app()->cacheUserData->set('internalImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            }

            // used to force reloading list of imported packages with validation to, for example: recalculate prices
            Yii::app()->session['courierInternalImport_'.($e ? 'e_' : '') .$hash] = 10;

            echo CJSON::encode(array('success' => true));
        } else {
            echo CJSON::encode(array('success' => false));
        }

    }


    public function actionGetLabels($hash)
    {
        if(isset($_POST['label']))
        {
            $couriers = Courier::model()->findAllByAttributes(['id' => array_keys($_POST['label']), 'user_id' => Yii::app()->user->id]);

            if(S_Useful::sizeof($couriers)) {

                if(!Yii::app()->user->isGuest && Yii::app()->user->model->getAllowCourierInternalExtendedLabel())
                {
                    $mode =  CourierTypeInternal::GET_LABEL_MODE_LAST; // make it default if option is selected by admin
                }
                else
                    $mode = false;


                LabelPrinter::generateLabels($couriers, $_POST['Courier']['type'],true, $mode);
                exit;
            }
        }

        Yii::app()->user->setFlash('error', Yii::t('courier', 'Nie znaleziono żadnych etykiet do wygenerowania!'));
        $this->redirect(Yii::app()->createUrl('/courier/courierInternal/listImported', ['hash' => $hash]));
        exit;
    }


    public function actionEbay($al = false, $hash = false)
    {
        $this->panelLeftMenuActive = 113;

        if(!Yii::app()->user->getModel()->payment_on_invoice)
        {

            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier',

            ));
            exit;
        }

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        // autoload list once on enter
        if($al)
        {
            Yii::app()->session->add('ebay_autoload', true);
            $this->redirect(['/courier/courierInternal/ebay']);
            exit;
        }


        // initialize ebayClient
        $ebayClient = ebayClient::instance();

        if($hash)
        {
            if(isset($_POST['refresh'])) {
                $this->_ebay_step3_refresh($ebayClient, $hash);
                exit;
            }
            else if(isset($_POST['save']))
            {
                $this->_ebay_step3_process($ebayClient, $hash);
                exit;
            } else
                $this->_ebay_step3($ebayClient, $hash);
        }
        else if(isset($_POST['import']))
        {
            if($this->_ebay_step1_process($ebayClient) === 'true')
                $this->_ebay_step2($ebayClient);
            else
                $this->_ebay_step1($ebayClient);

        }
        else if(isset($_POST['save-default-data']))
        {
            if($this->_ebay_step2_process($ebayClient) === 'true')
                $this->_ebay_step3($ebayClient, $hash);

        }
        else
        {
            $this->_ebay_step1($ebayClient);
        }
    }


    protected function _ebay_step1(ebayClient $ebayClient)
    {
        $orders = [];

        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $stat = (int) $_POST['stat'];
        $statEbay = (int) $_POST['statEbay'];

        if($date_from == '')
            $date_from = Yii::app()->session->get('ebay_date_from', NULL);

        if($date_to == '')
            $date_to = Yii::app()->session->get('ebay_date_to', NULL);

        if(!$stat)
            $stat = Yii::app()->session->get('stat', NULL);

        if(!$statEbay)
            $statEbay = Yii::app()->session->get('statEbay', NULL);

        $date_begining = date('Y-m-d', strtotime('-'.ebayClient::ORDERS_GOBACK_MONTHS.' months'));
        $date_end = date('Y-m-d');

        if (strtotime($date_from) > strtotime($date_to))
            $date_from = $date_to;

        if (!strtotime($date_from) OR strtotime($date_from) < strtotime($date_begining))
            $date_from = $date_end;

        if (!strtotime($date_to) OR strtotime($date_to) > strtotime($date_end))
            $date_to = $date_end;

        if (!in_array($stat, array_keys(EbayOrderItem::getStatShowProcessedList())))
            $stat = EbayOrderItem::STAT_SHOW_PROCESSED_ALL;

        if (!in_array($statEbay, array_keys(EbayOrderItem::getStatShowEbayList())))
            $statEbay = EbayOrderItem::STAT_SHOW_EBAY_READY;

        $more = false;
        // on load order
        if((isset($_POST['get-data']) OR Yii::app()->session->get('ebay_autoload', false)) && $ebayClient->isLogged()) {

            Yii::app()->session->add('ebay_date_from', $date_from);
            Yii::app()->session->add('ebay_date_to', $date_to);
            Yii::app()->session->add('stat', $stat);
            Yii::app()->session->add('statEbay', $statEbay);

            Yii::app()->session->add('ebay_autoload', false);
            list($orders, $more) = $ebayClient->getOrders($date_from, $date_to, $stat, $statEbay);
            Yii::app()->cacheUserData->set('ebayImport_'.$ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $orders, 86400);
        }

        $number = S_Useful::sizeof($orders);

        $this->render('importEbay/step_1', [
            'ebayClient' => $ebayClient,
            'orders' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'stat' => $stat,
            'statEbay' => $statEbay,
            'date_begining' => $date_begining,
            'date_end' => $date_end,
            'number' => $number,
            'more' => $more,
        ]);
    }

    protected function _ebay_step1_process(ebayClient $ebayClient)
    {
        $ordersToImport = [];
        $ordersCached = Yii::app()->cacheUserData->get('ebayImport_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);

        if (is_array($_POST['import'])) {
            foreach ($_POST['import'] AS $importKey => $temp) {
                $temp = $ordersCached[$importKey];

                /* @var $temp EbayOrderItem */
                // prevent already processed in system
//                if(CourierEbayImport::findActivePackageByEbayOrderId($temp->orderID) === false) {
                if($temp instanceof EbayOrderItem) {
                    $temp = $temp->convertToCourierTypeInternalModel();
                    array_push($ordersToImport, $temp);
//                }
                } else {
                    throw new CHttpException(403);
                }

            }

            if(S_Useful::sizeof($ordersToImport) > EbayImportInternal::MAX_IMPORT_NUMBER)
            {
                Yii::app()->user->setFlash('error', Yii::t('courier_ebay', 'Maksymalna ilość paczek do importu to: {no}!', array('{no}' => EbayImportInternal::MAX_IMPORT_NUMBER)));
                Yii::app()->session->add('ebay_autoload', true);
                $this->refresh(true);
                exit;
            }

            Yii::app()->cacheUserData->set('ebayImportInternal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            return "true";
        }
    }

    protected function _ebay_step2(ebayClient $ebayClient)
    {
        $senderAddressData = new CourierTypeInternal_AddressData;
        $receiverAddressData = new CourierTypeInternal_AddressData;
        $package = new Courier_CourierTypeInternal('default_package_data');
        $ebayImport = new EbayImportInternal();

        $senderAddressData->attributes= User::getLoggedUserAddressData();

        $receiverAddressData->tel = Yii::app()->user->model->getDefaultTelNo();

        $_additons = [];

        $this->render('importEbay/step_2', [
            'ebayClient' => $ebayClient,
            'senderAddressData' => $senderAddressData,
            'receiverAddressData' => $receiverAddressData,
            'package' => $package,
            'ebayImport' => $ebayImport,
            '_additions' => $_additons,
        ]);
    }

    protected function _ebay_step2_process(ebayClient $ebayClient)
    {

        $senderAddressData = new CourierTypeInternal_AddressData;
        $receiverAddressDataDefault = new CourierTypeInternal_AddressData;
        $package = new Courier_CourierTypeInternal(Courier_CourierTypeInternal::SCENARIO_DEFAULT_PACKAGE_DATA);
        $ebayImport = new EbayImportInternal();


        $courierTypeInternal = new CourierTypeInternal();
        $courierTypeInternal->courier = $package;
        $package->courierTypeInternal = $courierTypeInternal;

        $_additions = $_POST['_additions'];

        $senderAddressData->attributes = $_POST['CourierTypeInternal_AddressData']['sender'];
        $package->attributes = $_POST['Courier_CourierTypeInternal'];
        $ebayImport->attributes = $_POST['EbayImportInternal'];

        $receiverAddressDataDefault->tel = $_POST['CourierTypeInternal_AddressData']['receiver']['tel'];

        $senderAddressData->validate();
        $receiverAddressDataDefault->validate('tel');
        $package->validate();
        $ebayImport->validate();

        // ignore empty tel field
        if($receiverAddressDataDefault->tel == '')
            $receiverAddressDataDefault->clearErrors('tel');

        if(!$senderAddressData->hasErrors() && !$package->hasErrors() && !$ebayImport->hasErrors() && !$receiverAddressDataDefault->hasErrors('tel')) {

            $ordersToImport = Yii::app()->cacheUserData->get('ebayImportInternal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, []);

            if(is_array($ordersToImport) && S_Useful::sizeof($ordersToImport))
            {
                /* @var $order Courier_CourierTypeInternal */
                foreach($ordersToImport AS &$order)
                {
                    $order->senderAddressData = new CourierTypeInternal_AddressData();
                    $order->senderAddressData->attributes = $senderAddressData->attributes;

                    if($order->receiverAddressData === NULL)
                        $order->receiverAddressData = new CourierTypeInternal_AddressData();
                    if( $order->receiverAddressData->tel == '')
                        $order->receiverAddressData->tel = $receiverAddressDataDefault->tel;

                    $order->package_weight = $package->package_weight;
                    $order->package_size_l = $package->package_size_l;
                    $order->package_size_w = $package->package_size_w;
                    $order->package_size_d = $package->package_size_d;
                    $order->package_value = $package->package_value;

                    $order->courierTypeInternal->with_pickup = $ebayImport->with_pickup;

                    $order->courierTypeInternal->_imported_additions = $_additions;
                }

                F_InternalImport::validateModels($ordersToImport);

                Yii::app()->cacheUserData->delete('ebayImport_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);
                Yii::app()->cacheUserData->set('ebayImportInternal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            }

            return "true";
        }



        $this->render('importEbay/step_2', [
            'ebayClient' => $ebayClient,
            'senderAddressData' => $senderAddressData,
            'receiverAddressData' => $receiverAddressDataDefault,
            'package' => $package,
            'ebayImport' => $ebayImport,
            '_additions' => $_additions,
        ]);
    }

    protected function _ebay_step3($ebayClient, $hash)
    {
        if(!$hash)
        {
            // generate new hash
            $hash = hash('sha256', time() . $ebayClient->getSessionId());
            $ordersToImport = Yii::app()->cacheUserData->get('ebayImportInternal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));

            // rename data by hash and clear previous
            Yii::app()->cacheUserData->set('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $ordersToImport);
            Yii::app()->cacheUserData->delete('ebayImportInternal_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);

            $this->redirect(['/courier/courierInternal/ebay', 'hash' => $hash]);
            exit;

        } else {
            $ordersToImport = Yii::app()->cacheUserData->get('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));

            // added refresh button instead
            // validate only after editing one item in row
//            if(Yii::app()->session['courierInternalImport_e_'.$hash] == 10)
//            {
//                unset(Yii::app()->session['courierInternalImport_e_'.$hash]);
//                if (S_Useful::sizeof($ordersToImport))
//                    F_InternalImport::validateModels($ordersToImport);
//            }

//
            $this->render('importEbay/step_3', [
                'models' => $ordersToImport,
                'hash' => $hash,
            ]);
            return;
        }

    }

    protected function _ebay_step3_refresh(ebayClient $ebayClient, $hash)
    {
        $models = Yii::app()->cacheUserData->get('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));


        if(F_InternalImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
        }


        Yii::app()->cacheUserData->set('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        $this->refresh();
        exit;
    }

    protected function _ebay_step3_process(ebayClient $ebayClient, $hash)
    {
        $models = Yii::app()->cacheUserData->get('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));

        if(F_InternalImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
            Yii::app()->cacheUserData->set('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            $this->refresh();
            exit;
        }

        try
        {
            $result = CourierTypeInternal::saveWholeGroup($models, CourierTypeInternal::SOURCE_EBAY, $ebayClient->getAuthToken(), false);
        }
        catch (Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            return;
        }

        // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING
        if($result['success'])
        {
            $import = CourierImportManager::getByHash($hash);
            /* @var $item Courier */
            $ids = array();
            if(is_array($result['data']))
                foreach($result['data'] AS $item)
                {
                    array_push($ids, $item->id);
                }

            $import->source = CourierImportManager::SOURCE_EBAY;
            $import->addCourierIds($ids);
            $import->save();

            Yii::app()->cacheUserData->delete('ebayImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
            $this->redirect(Yii::app()->createUrl('/courier/courierInternal/listImported', ['hash' => $hash]));
            exit;
        }
    }

    public function actionAjaxListOfPointsSp()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $CACHE = Yii::app()->cacheFile;
            $CACHE_NAME = 'SP_POINTS_COURIER_INTERNAL_SP';

            $items = $CACHE->get($CACHE_NAME);

            if($items === false) {

                $items = [];
                $spPoints = SpPoints::getPoints();
                $i = 0;
                foreach ($spPoints AS $item) {
                    $address = $item->addressData->address_line_1 . ' ' . $item->addressData->address_line_2 . ' ' . $item->addressData->city . ' ' . $item->addressData->country0->code;

                    $addressHuman = $item->addressData->address_line_1;
                    $addressHuman .= $item->addressData->address_line_2 != '' ? "<br/>" . $item->addressData->address_line_2 : '';
                    $addressHuman .= "<br/>" . $item->addressData->zip_code . ', ' . $item->addressData->city;
                    $addressHuman .= "<br/>" . strtoupper($item->addressData->country0->countryListTr->name);

//                    $address = urlencode($address);
//
//                    $arrContextOptions = array(
//                        "ssl" => array(
//                            "verify_peer" => false,
//                            "verify_peer_name" => false,
//                        ),
//                    );
//
//                    $json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key='.GLOBAL_CONFIG::GOOGLE_MAPS_API_KEY, false, stream_context_create($arrContextOptions));
//                    $obj = json_decode($json);

                    $geoResp = GoogleApi::getCoordinatesByAddress($address);

                    $lat = $geoResp[0];
                    $lng = $geoResp[1];

                    $items[] = [
                        'id' => $i,
                        'name' => $item->spPointsTr->title,
                        'lat' => $lat,
                        'lng' => $lng,
                        'address' => $addressHuman,
                        'desc' => $item->spPointsTr->text,
                        'imgUrl' => Yii::app()->baseUrl . '/images/v2/sp_map_ico.png',
                        'type' => CourierTypeInternal::WITH_PICKUP_NONE + 0,
                        'uniqId' => $item->id,
                    ];
                    $i++;
                }

                $CACHE->set($CACHE_NAME, $items, 60*60*24);
            }

            echo CJSON::encode($items);
        }
    }


    public function actionAjaxListOfPoints()
    {
        $DISABLE_CACHE = false;
        $CACHE = Yii::app()->cacheFile;

//        MyDump::dump('listofpoints.txt', print_r($_REQUEST,1));

        if (Yii::app()->request->isAjaxRequest) {

            $geoAddress = $_POST['address'];
            $zipCode = $_POST['zip_code'];

            $countryCode = $_POST['country_code'];

//            MyDump::dump('geoaddress.txt', print_r($geoAddress,1));
//            MyDump::dump('geoaddress.txt', print_r($zipCode,1));

            $type = $_POST['type'];
            if($type == 'ALL')
                $type = -1;
            else
                $type = intval($type);

            $availableTypes = array_keys(CourierTypeInternal::getWithPickupMapTypes(true));

            $CACHE_NAME = 'SP_POINTS_COURIER_INTERNAL_'.$geoAddress.'_'.$zipCode.'_'.$type.'_'.implode('_', $availableTypes);
//            $CACHE_NAME_SUFFIX = $geoAddress.'_'.$zipCode.'_'.$type.'_'.implode('_', $availableTypes);
            $items = $CACHE->get($CACHE_NAME);

            if($DISABLE_CACHE)
                $items = false;

            if ($items === false) {

                $items = [];

                if($type === CourierTypeInternal::WITH_PICKUP_LP_EXPRESS OR ($type == -1 && in_array(CourierTypeInternal::WITH_PICKUP_LP_EXPRESS, $availableTypes))) {

                    $terminals = LpExpressClient::getTerminals();
                    $i = 0;
                    foreach ($terminals AS $item) {

                        $addressHuman = $item->address;
                        $addressHuman .= "<br/>" . $item->zip . ', ' . $item->city;
                        $addressHuman .= "<br/>" . strtoupper($item->address->country0->countryListTr->name);

                        $lat = $item->latitude;
                        $lng = $item->longitude;

                        $desc = $item->comment . '<br/>';
                        $desc .= $item->workinghours;

                        $items[] = [
                            'id' => $i,
                            'name' => $item->name,
                            'lat' => $lat,
                            'lng' => $lng,
                            'address' => $addressHuman,
                            'desc' => $desc,
                            'imgUrl' => Yii::app()->baseUrl . '/images/v2/lp_express_map_ico.png',
                            'type' => CourierTypeInternal::WITH_PICKUP_LP_EXPRESS + 0,
                            'uniqId' => $item->name,
                        ];
                        $i++;
                    }

                }

                if($type === CourierTypeInternal::WITH_PICKUP_RUCH OR ($type == -1 && in_array(CourierTypeInternal::WITH_PICKUP_RUCH, $availableTypes))) {

                    $points = PaczkaWRuchu::getListOfPlacesDetailed();
                    $i = 0;
                    foreach ($points AS $point) {

                        $addressHuman = $point['address'];

                        $lat = $point['lat'];
                        $lng = $point['lng'];

                        $desc = (string)$point['details'] . '<br/>';
                        $desc .= (string)$point['hours'];

                        $items[] = [
                            'id' => $i,
                            'name' => $point['name'],
                            'lat' => $lat,
                            'lng' => $lng,
                            'address' => $addressHuman,
                            'desc' => $desc,
                            'imgUrl' => Yii::app()->baseUrl . '/images/v2/ruch_map_ico.png',
                            'type' => CourierTypeInternal::WITH_PICKUP_RUCH + 0,
                            'uniqId' => $point['name'],
                        ];
                        $i++;
                    }

                }

                if($type === CourierTypeInternal::WITH_PICKUP_UPS OR ($type == -1 && in_array(CourierTypeInternal::WITH_PICKUP_UPS, $availableTypes))) {
                    if ($geoAddress && !is_array($geoAddress)) {
                        /* @var $hubs CourierHub[] */
                        $geoAddress = explode(',', $geoAddress);
                        $temp = explode(' ', trim($geoAddress[1]));

                        $city = trim($temp[1]);
                        $zip = trim($temp[0]);
                        $country = trim($geoAddress[2]);
                        $street = trim($geoAddress[0]);

                        if($country == '' && $city == '')
                        {
                            $country = $zip;
                            $zip = '';
                            $city = $street;
                            $street = '';
                        }


                        // to keep uniq "i" of offices
//                    $CACHE_NAME3 = 'COURIER_INTERNAL_MAP_APS_NO';
//                    $apsFoundBefore = Yii::app()->cache->get($CACHE_NAME3);
//
//                    if($DISABLE_CACHE)
//                        $apsFoundBefore = false;
//
//                    if(!is_array($apsFoundBefore))
                        $apsFoundBefore = [];

                        $aps = UpsApLocation::getPoints($street, $city, $zip, $country);
                        $i = 0;
                        if (is_array($aps))
                            foreach ($aps AS $item) {

                                $addressHuman = $item['address'];
                                $addressHuman .= "<br/>" . $item['zip'] . ', ' . $item['city'];
                                $addressHuman .= "<br/>" . strtoupper($item['country_code']);

                                if(!isset($apsFoundBefore[$item['id']]))
                                    $apsFoundBefore[$item['id']] = true;

                                $items[] = [
                                    'id' => S_Useful::sizeof($apsFoundBefore) - 1,
                                    'name' => 'UPS Access Point: '.$item['name'],
                                    'lat' => $item['lat'],
                                    'lng' => $item['lng'],
                                    'address' => $addressHuman,
                                    'desc' => $item['desc'],
                                    'imgUrl' => Yii::app()->baseUrl . '/images/v2/ups_map_ico.png',
                                    'type' => CourierTypeInternal::WITH_PICKUP_UPS + 0,
                                    'uniqId' => $item['name'],
                                ];

                                $i++;
                            }
//                    Yii::app()->cache->set($CACHE_NAME3, $apsFoundBefore, 60*60*24);
                    }

                }

                if($type === CourierTypeInternal::WITH_PICKUP_RM OR ($type == -1 && in_array(CourierTypeInternal::WITH_PICKUP_RM, $availableTypes))) {

                    if ($zipCode) {

                        // to keep uniq "i" of offices
//                        $CACHE_NAME2 = 'COURIER_INTERNAL_MAP_ROYAL_MAIL_OFFICES_NO';
//                        $officesFoundBefore = Yii::app()->cache->get($CACHE_NAME2);
//
//                        if ($DISABLE_CACHE)
//                            $officesFoundBefore = false;
//
//                        if (!is_array($officesFoundBefore))
//                        $officesFoundBefore = [];

                        $offices = RoyalMailClient::getOffices($zipCode);
                        $j = 0;
                        if (is_array($offices))
                            foreach ($offices AS $office) {
//                                if (!isset($officesFoundBefore[$office['id']]))
//                                    $officesFoundBefore[$office['id']] = true;

                                $items[] = [
                                    'id' => $j++,
                                    'name' => $office['name'],
                                    'lat' => $office['lat'],
                                    'lng' => $office['lng'],
                                    'address' => $office['address'],
                                    'desc' => '',
                                    'imgUrl' => Yii::app()->baseUrl . '/images/v2/rm_map_ico.png',
                                    'type' => CourierTypeInternal::WITH_PICKUP_RM + 0,
                                    'uniqId' => $office['name'],
                                ];
                            }
//                        Yii::app()->cache->set($CACHE_NAME2, $officesFoundBefore, 60 * 60 * 24);
                    }

                }

                if($type === CourierTypeInternal::WITH_PICKUP_INPOST OR ($type == -1 && in_array(CourierTypeInternal::WITH_PICKUP_INPOST, $availableTypes))) {

                    // to keep uniq "i" of paczkomat
//                    $CACHE_NAME2 = 'COURIER_INTERNAL_MAP_INPOST_PACZKOMAT_NO';
//                    $paczkomatFoundBefore = Yii::app()->cache->get($CACHE_NAME2);
//
//                    if ($DISABLE_CACHE)
//                        $paczkomatFoundBefore = false;
//
//                    if (!is_array($paczkomatFoundBefore))
//                    $paczkomatFoundBefore = [];

                    $paczkomats = InpostShipx::getPoints();
                    $j = 0;
                    if (is_array($paczkomats))
                        foreach ($paczkomats AS $paczkomat) {
//                            if (!isset($paczkomatFoundBefore[$paczkomat['id']]))
//                                $paczkomatFoundBefore[$paczkomat['id']] = true;

                            $items[] = [
                                'id' => $j++,
                                'name' => $paczkomat->name,
                                'lat' => $paczkomat->lat,
                                'lng' => $paczkomat->lng,
                                'address' => $paczkomat->street.', '.$paczkomat->zip_code.' '.$paczkomat->city,
                                'desc' => $paczkomat->pid,
                                'imgUrl' => Yii::app()->baseUrl . '/images/v2/inpost_map_ico.png',
                                'type' => CourierTypeInternal::WITH_PICKUP_INPOST + 0,
                                'uniqId' => $paczkomat->pid,
                            ];
                        }
//                    Yii::app()->cache->set($CACHE_NAME2, $paczkomatFoundBefore, 60 * 60 * 24);

                }

                if($type === CourierTypeInternal::WITH_PICKUP_DHLDE OR ($type == -1 && in_array(CourierTypeInternal::WITH_PICKUP_DHLDE, $availableTypes))) {
                    // to keep uniq "i"d
//                    $CACHE_NAME2 = 'COURIER_INTERNAL_MAP_INPOST_DHD_DE_'.$CACHE_NAME_SUFFIX;
//                    $paczkomatFoundBefore = Yii::app()->cache->get($CACHE_NAME2);
//
//                    if ($DISABLE_CACHE)
//                        $paczkomatFoundBefore = false;
//
//                    if (!is_array($paczkomatFoundBefore))
//                    $dhldeFoundBefore = [];

                    $dhldes = DhlDeClient::getPoints($geoAddress);
                    $j = 0;
                    if (is_array($dhldes))
                        foreach ($dhldes AS $dhlde) {
//                            if (!isset($dhldeFoundBefore[$dhlde['id']]))
//                                $dhldeFoundBefore[$dhlde['id']] = true;


                            $countryCode = strtoupper($countryCode);

                            if(is_file(Yii::app()->basePath.'/../images/v2/dhlde_icons/'.$countryCode.'.png'))
                                $img = Yii::app()->baseUrl . '/images/v2/dhlde_icons/'.$countryCode.'.png';
                            else
                                $img = Yii::app()->baseUrl . '/images/v2/dhl_map_ico.png';


//                            $temp = [
//                                'id' => $dhlde->id,
//                                'name' => $dhlde->name,
//                                'lat' => $dhlde->lat,
//                                'lng' => $dhlde->lng,
//                                'address' => $dhlde->address,
//                                'desc' => $dhlde->desc,
//                            ];

                            $items[] = [
                                'id' => $j++,
                                'name' => $dhlde->name,
                                'lat' => $dhlde->lat,
                                'lng' => $dhlde->lng,
                                'address' => $dhlde->street.'<br/>'.$dhlde->zip_code.' '.$dhlde->city.'<br/>'.$dhlde->country,
                                'desc' => $dhlde->desc ? $dhlde->desc : '',
                                'imgUrl' => $img,
                                'type' => CourierTypeInternal::WITH_PICKUP_DHLDE + 0,
                                'uniqId' => $dhlde->id,
                            ];
                        }
//                    Yii::app()->cache->set($CACHE_NAME2, $paczkomatFoundBefore, 60 * 60 * 24);

                }

                $CACHE->set($CACHE_NAME, $items, 60 * 60 * 24);
            }


            echo CJSON::encode($items);
        }
    }

    public function actionAjaxLoadModalContent()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $broker = $_POST['broker'];
            $success = false;
            $html = '';

            switch($broker)
            {
                case CourierOperator::BROKER_LP_EXPRESS:
                    $success = true;
                    $html = $this->renderPartial('create/_modal_lp_express', [], true, false);
                    break;
            }

            echo CJSON::encode([
                'success' => $success,
                'html' => $html,
            ]);

        }
    }





    ##################################################################################################################################
    ##################################################################################################################################
    ##################################################################################################################################
    ##################################################################################################################################
    ##################################################################################################################################
    ##################################################################################################################################
//
//
    public function actionLinker($al = false, $hash = false, $page = 0, $sort = 0)
    {
        $this->panelLeftMenuActive = 115;


        if(!Yii::app()->user->getModel()->payment_on_invoice)
        {

            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier',

            ));
            exit;
        }

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        // autoload list once on enter
        if($al)
        {
            Yii::app()->session->add('linker_autoload', true);
            $this->redirect(['/courier/courierInternal/linker']);
            exit;
        }

        if($hash)
        {
            if(isset($_POST['refresh'])) {
                $this->_linker_step3_refresh($hash);
                exit;
            }
            else if(isset($_POST['save']))
            {
                $this->_linker_step3_process($hash);
                exit;
            } else
                $this->_linker_step3($hash);
        }
        else if(isset($_POST['import']))
        {
            if($this->_linker_step1_process() === 'true')
                $this->_linker_step2();
            else
                $this->_linker_step1();

        }
        else if(isset($_POST['save-default-data']))
        {
            if($this->_linker_step2_process() === 'true')
                $this->_linker_step3($hash);

        }
        else
        {
            $this->_linker_step1();
        }
    }


    protected function _linker_step1()
    {
        $orders = [];

        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $stat = (int) $_POST['stat'];
        $statLinker = (int) $_POST['statLinker'];

        if($date_from == '')
            $date_from = Yii::app()->session->get('linker_date_from', NULL);

        if($date_to == '')
            $date_to = Yii::app()->session->get('linker_date_to', NULL);

        if(!$stat)
            $stat = Yii::app()->session->get('stat', NULL);

        if(!$statLinker)
            $statLinker = Yii::app()->session->get('statLinker', NULL);

        $date_begining = date('Y-m-d', strtotime('-'.LinkerClient::ORDERS_GOBACK_MONTHS.' months'));
        $date_end = date('Y-m-d');

        if (strtotime($date_from) > strtotime($date_to))
            $date_from = $date_to;

        if (!strtotime($date_from) OR strtotime($date_from) < strtotime($date_begining))
            $date_from = $date_end;

        if (!strtotime($date_to) OR strtotime($date_to) > strtotime($date_end))
            $date_to = $date_end;

        if (!in_array($stat, array_keys(LinkerOrderItem::getStatShowProcessedList())))
            $stat = LinkerOrderItem::STAT_SHOW_PROCESSED_ALL;

        if (!in_array($statLinker, array_keys(LinkerOrderItem::getStatShowLinkerList())))
            $statLinker = LinkerOrderItem::STAT_SHOW_LINKER_ALL;

        $sd = intval($_POST['sd']);

        $sa = $_POST['sa'];
        if (!in_array($sa, LinkerClient::sortAttributesList()))
            $sa = false;

        $more = false;
        // on load order
        if((isset($_POST['get-data']) OR Yii::app()->session->get('linker_autoload', false))) {

            Yii::app()->session->add('linker_date_from', $date_from);
            Yii::app()->session->add('linker_date_to', $date_to);
            Yii::app()->session->add('stat', $stat);
            Yii::app()->session->add('statLinker', $statLinker);

            Yii::app()->session->add('linker_autoload', false);

            list($orders, $more) = LinkerClient::getOrders($date_from, $date_to, $statLinker, LinkerImport::TARGET_COURIER, $sa, $sd, $stat);
//            getOrders($date_from, $date_to, $stat, $statLinker);
            Yii::app()->cacheUserData->set('linkerImport_'.Yii::app()->session->sessionID, $orders, 86400);
        }

        $number = S_Useful::sizeof($orders);

        $this->render('importLinker/step_1', [
            'orders' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'stat' => $stat,
            'statLinker' => $statLinker,
            'date_begining' => $date_begining,
            'date_end' => $date_end,
            'number' => $number,
            'more' => $more,
            'sa' => $sa,
            'sd' => $sd,
        ]);
    }

    protected function _linker_step1_process()
    {
        $ordersToImport = [];
        $ordersCached = Yii::app()->cacheUserData->get('linkerImport_'.Yii::app()->session->sessionID);

        if (is_array($_POST['import'])) {
            foreach ($_POST['import'] AS $importKey => $temp) {
                $temp = $ordersCached[$importKey];

                /* @var $temp LinkerOrderItem */
                // prevent already processed in system
//                if(CourierLinkerImport::findActivePackageByLinkerOrderId($temp->orderID) === false) {
                if($temp instanceof LinkerOrderItem) {
                    $temp = $temp->convertToCourierTypeInternalModel();
                    array_push($ordersToImport, $temp);
//                }
                } else {
                    throw new CHttpException(403);
                }

            }

            if(S_Useful::sizeof($ordersToImport) > LinkerImportInternal::MAX_IMPORT_NUMBER)
            {
                Yii::app()->user->setFlash('error', Yii::t('courier_linker', 'Maksymalna ilość paczek do importu to: {no}!', array('{no}' => LinkerImportInternal::MAX_IMPORT_NUMBER)));
                Yii::app()->session->add('linker_autoload', true);
                $this->refresh(true);
                exit;
            }

            Yii::app()->cacheUserData->set('linkerImportInternal_models_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            return "true";
        }
    }

    protected function _linker_step2()
    {
        $senderAddressData = new CourierTypeInternal_AddressData;
        $receiverAddressData = new CourierTypeInternal_AddressData;
        $package = new Courier_CourierTypeInternal('default_package_data');
        $linkerImport = new LinkerImportInternal();

        $senderAddressData->attributes= User::getLoggedUserAddressData();

        $receiverAddressData->tel = Yii::app()->user->model->getDefaultTelNo();


        $_additons = [];

        $this->render('importLinker/step_2', [

            'senderAddressData' => $senderAddressData,
            'receiverAddressData' => $receiverAddressData,
            'package' => $package,
            'linkerImport' => $linkerImport,
            '_additions' => $_additons,
        ]);
    }

    protected function _linker_step2_process()
    {

        $senderAddressData = new CourierTypeInternal_AddressData;
        $receiverAddressDataDefault = new CourierTypeInternal_AddressData;
        $package = new Courier_CourierTypeInternal(Courier_CourierTypeInternal::SCENARIO_DEFAULT_PACKAGE_DATA);
        $linkerImport = new LinkerImportInternal();


        $courierTypeInternal = new CourierTypeInternal();
        $courierTypeInternal->courier = $package;
        $package->courierTypeInternal = $courierTypeInternal;

        $_additions = $_POST['_additions'];

        $senderAddressData->attributes = $_POST['CourierTypeInternal_AddressData']['sender'];
        $package->attributes = $_POST['Courier_CourierTypeInternal'];
        $linkerImport->attributes = $_POST['LinkerImportInternal'];

        $receiverAddressDataDefault->tel = $_POST['CourierTypeInternal_AddressData']['receiver']['tel'];

        $senderAddressData->validate();
        $receiverAddressDataDefault->validate('tel');
        $package->validate();
        $linkerImport->validate();

        // ignore empty tel field
        if($receiverAddressDataDefault->tel == '')
            $receiverAddressDataDefault->clearErrors('tel');

        if(!$senderAddressData->hasErrors() && !$package->hasErrors() && !$linkerImport->hasErrors() && !$receiverAddressDataDefault->hasErrors('tel')) {

            $ordersToImport = Yii::app()->cacheUserData->get('linkerImportInternal_models_'.Yii::app()->session->sessionID, []);

            if(is_array($ordersToImport) && S_Useful::sizeof($ordersToImport))
            {
                /* @var $order Courier_CourierTypeInternal */
                foreach($ordersToImport AS &$order)
                {
                    $order->senderAddressData = new CourierTypeInternal_AddressData();
                    $order->senderAddressData->attributes = $senderAddressData->attributes;

                    if($order->receiverAddressData === NULL)
                        $order->receiverAddressData = new CourierTypeInternal_AddressData();
                    if( $order->receiverAddressData->tel == '')
                        $order->receiverAddressData->tel = $receiverAddressDataDefault->tel;

                    if($order->package_weight == '')
                        $order->package_weight = $package->package_weight;
                    $order->package_size_l = $package->package_size_l;
                    $order->package_size_w = $package->package_size_w;
                    $order->package_size_d = $package->package_size_d;

                    if($order->package_value == '')
                        $order->package_value = $package->package_value;

                    if($order->value_currency == '')
                        $order->value_currency = $package->value_currency;

                    $order->courierTypeInternal->with_pickup = $linkerImport->with_pickup;

                    $order->courierTypeInternal->_imported_additions = $_additions;
                }

                F_InternalImport::validateModels($ordersToImport);

                Yii::app()->cacheUserData->delete('linkerImport_'.Yii::app()->session->sessionID);
                Yii::app()->cacheUserData->set('linkerImportInternal_models_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            }

            return "true";
        }



        $this->render('importLinker/step_2', [

            'senderAddressData' => $senderAddressData,
            'receiverAddressData' => $receiverAddressDataDefault,
            'package' => $package,
            'linkerImport' => $linkerImport,
            '_additions' => $_additions,
        ]);
    }

    protected function _linker_step3($hash)
    {
        if(!$hash)
        {
            // generate new hash
            $hash = hash('sha256', time());
            $ordersToImport = Yii::app()->cacheUserData->get('linkerImportInternal_models_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu!'));

            // rename data by hash and clear previous
            Yii::app()->cacheUserData->set('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $ordersToImport);
            Yii::app()->cacheUserData->delete('linkerImportInternal_models_'.Yii::app()->session->sessionID);

            $this->redirect(['/courier/courierInternal/linker', 'hash' => $hash]);
            exit;

        } else {
            $ordersToImport = Yii::app()->cacheUserData->get('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !S_Useful::sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu!'));

            // added refresh button instead
            // validate only after editing one item in row
//            if(Yii::app()->session['courierInternalImport_e_'.$hash] == 10)
//            {
//                unset(Yii::app()->session['courierInternalImport_e_'.$hash]);
//                if (S_Useful::sizeof($ordersToImport))
//                    F_InternalImport::validateModels($ordersToImport);
//            }

//
            $this->render('importLinker/step_3', [
                'models' => $ordersToImport,
                'hash' => $hash,
            ]);
            return;
        }

    }

    protected function _linker_step3_refresh($hash)
    {
        $models = Yii::app()->cacheUserData->get('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));


        if(F_InternalImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
        }


        Yii::app()->cacheUserData->set('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        $this->refresh();
        exit;
    }

    protected function _linker_step3_process($hash)
    {
        $models = Yii::app()->cacheUserData->get('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));

        if(F_InternalImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
            Yii::app()->cacheUserData->set('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            $this->refresh();
            exit;
        }

        try
        {
            $result = CourierTypeInternal::saveWholeGroup($models, CourierTypeInternal::SOURCE_LINKER, false);
        }
        catch (Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            return;
        }

        // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING
        if($result['success'])
        {
            $import = CourierImportManager::getByHash($hash);
            /* @var $item Courier */
            $ids = array();
            if(is_array($result['data']))
                foreach($result['data'] AS $item)
                {
                    array_push($ids, $item->id);
                }

            $import->source = CourierImportManager::SOURCE_LINKER;
            $import->addCourierIds($ids);
            $import->save();

            Yii::app()->cacheUserData->delete('linkerImportInternal_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);

            $this->redirect(Yii::app()->createUrl('/courier/courierInternal/listImported', ['hash' => $hash]));
            exit;
        }
    }
}
