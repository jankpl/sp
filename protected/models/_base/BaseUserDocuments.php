<?php

/**
 * This is the model base class for the table "user_documents".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "UserDocuments".
 *
 * Columns in table "user_documents" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $admin_id
 * @property string $date_entered
 * @property string $hash
 * @property integer $stat
 * @property integer $customer_visible
 * @property string $desc
 * @property string $desc_customer
 * @property string $file_name
 * @property string $file_path
 * @property string $file_type
 * @property string $file_size
 * @property integer $type
 *
 * @property User $user
 * @property Admin $admin
 */
abstract class BaseUserDocuments extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'user_documents';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'UserDocuments|UserDocuments', $n);
	}

	public static function representingColumn() {
		return 'date_entered';
	}

	public function rules() {
		return array(
			array('user_id, admin_id, date_entered, stat, customer_visible, file_name, file_path, file_type, file_size, type', 'required'),
			array('user_id, admin_id, stat, customer_visible, type', 'numerical', 'integerOnly'=>true),
			array('desc, desc_customer, file_name, file_path', 'length', 'max'=>256),
			array('file_type, hash', 'length', 'max'=>64),
			array('file_size', 'length', 'max'=>32),
			array('desc, desc_customer', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, user_id, admin_id, date_entered, stat, customer_visible, desc, desc_customer, file_name, file_path, file_type, file_size, type, hash', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'admin' => array(self::BELONGS_TO, 'Admin', 'admin_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User'),
			'admin_id' => Yii::t('app', 'Admin'),
			'date_entered' => Yii::t('userDocuments', 'Date Entered'),
			'stat' => Yii::t('app', 'Stat'),
			'customer_visible' => Yii::t('app', 'Customer Visible'),
			'desc' => Yii::t('userDocuments', 'Desc'),
			'desc_customer' => Yii::t('userDocuments', 'Desc Customer'),
			'file_name' => Yii::t('app', 'File Name'),
			'file_path' => Yii::t('app', 'File Path'),
			'file_type' => Yii::t('app', 'File Type'),
			'file_size' => Yii::t('app', 'File Size'),
			'type' => Yii::t('userDocuments', 'Type'),
			'hash' => Yii::t('app', 'Hash'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('admin_id', $this->admin_id);
		$criteria->compare('date_entered', $this->date_entered, true);
		$criteria->compare('stat', $this->stat);
		$criteria->compare('customer_visible', $this->customer_visible);
		$criteria->compare('desc', $this->desc, true);
		$criteria->compare('desc_customer', $this->desc_customer, true);
		$criteria->compare('file_name', $this->file_name, true);
		$criteria->compare('file_path', $this->file_path, true);
		$criteria->compare('file_type', $this->file_type, true);
		$criteria->compare('file_size', $this->file_size, true);
		$criteria->compare('type', $this->type);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}