<?php

Yii::import('application.modules.courier.models._base.BaseCourierCodSettled');

class CourierCodSettled extends BaseCourierCodSettled
{
    const TYPE_OK = 1;
    const TYPE_CANCELLED = 9;

    const SOURCE_OWN = 0;
    const SOURCE_OWN_SCANNER = 5;
    const SOURCE_EXTERNAL = 9;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function listType()
    {
        return [
            self::TYPE_OK => Yii::t('courierCod', 'Rozliczone'),
            self::TYPE_CANCELLED => Yii::t('courierCod', 'Anulowane'),
        ];
    }

    public function getTypeNameShort()
    {
        if($this->type == self::TYPE_OK)
            return Yii::t('courierCod', 'Tak');
        else if($this->type == self::TYPE_CANCELLED)
            return Yii::t('courierCod', 'Anulowane');
        else
            return '-';
    }

    public function getTypeName()
    {
        return isset(self::listType()[$this->type]) ? self::listType()[$this->type] : '';
    }

    public static function listSource()
    {
        return [
            self::SOURCE_OWN => 'Own',
            self::SOURCE_OWN_SCANNER => 'Own - scanner',
            self::SOURCE_EXTERNAL => 'External',
            self::SOURCE_EXTERNAL => 'External',
        ];
    }

    public function getSourceName()
    {
        return isset(self::listSource()[$this->source]) ? self::listSource()[$this->source] : '';
    }


    public function rules() {
        return array(
            array('date_entered, courier_id, user_id, settle_date, type, source', 'required'),
            array('courier_id, user_id, type, source', 'numerical', 'integerOnly'=>true),
            array('settle_date', 'date', 'format' => 'YYYY-mm-dd'),
            array('type', 'in', 'range' => array_keys(self::listType())),
            array('source', 'in', 'range' => array_keys(self::listSource())),
            array('source', 'default', 'value' => self::SOURCE_OWN),
            array('id, date_entered, courier_id, user_id, type, source, settle_date', 'safe', 'on'=>'search'),
        );
    }

    public function scopes()
    {
        return array(
            'newFirst' => [
                'order'=>'stat ASC, no DESC',
            ],
            'waiting' => [
                'condition'=>'stat = '.self::SEEN.' AND inv_value > paid_value',
            ],
            'rest' => [
                'condition'=>'stat = '.self::SEEN.' AND inv_value = paid_value OR stat !='.self::INV_STAT_NONE,
            ],
            'active' => [
                'condition'=>'stat > '.self::UNPUBLISHED.' AND stat_inv = '.self::INV_STAT_NONE,
            ],
            'new' => [
                'condition'=>'stat = '.self::PUBLISHED,
            ],
            'paid' => [
                'condition'=>'inv_value = paid_value',
            ],
            'paidPartialy' => [
                'condition'=>'inv_value > paid_value && paid_value > 0',
            ],
            'notPaid' => [
                'condition' => 'paid_value = 0',
            ],
            'typeOk' => [
                'condition' => 'type = '.self::TYPE_OK,
            ],
            'typeCancelled' => [
                'condition' => 'type = '.self::TYPE_CANCELLED,
            ],
            'sourceOwn' => [
                'condition' => 'source = '.self::SOURCE_OWN,
            ],
            'sourceExternal' => [
                'condition' => 'source = '.self::SOURCE_EXTERNAL,
            ],
            'notPaidWithPartialyPaid' => [
                'condition' => 'inv_value > paid_value',
            ],
            'outdated' => [
                'condition' => 'inv_value > paid_value && inv_payment_date < :date',
                'params' => [':date' => date('Y-m-d')]
            ],
            'ofCurrentUser' => [
                'condition' => 'user_id = :user_id',
                'params' => [':user_id' => Yii::app()->user->id]
            ]
        );
    }

    public static function countNoByStatMap($statMap, $settled, $currency, $sumValue = false)
    {

        $cmd = Yii::app()->db->cache(60)->createCommand();
        if($sumValue)
            $cmd = $cmd->select('SUM(cod_value)');
        else
            $cmd = $cmd->select('COUNT(*)');

        $cmd = $cmd->from((new Courier)->tableName())
            ->leftJoin(self::getTableName(),'courier.id = courier_id');

        if($settled)
            $cmd = $cmd->where('courier_cod_settled.id IS NOT NULL');
        else
            $cmd = $cmd->where('courier_cod_settled.id IS NULL');

        $cmd = $cmd->andWhere('courier.stat_map_id = :stat_map_id', [':stat_map_id' => $statMap]);

        if($statMap)
            $cmd = $cmd->andWhere('courier.stat_map_id = :stat_map_id', [':stat_map_id' => $statMap]);
        else
            $cmd = $cmd->andWhere('courier.stat_map_id IS NULL');

        $cmd = $cmd->andWhere('courier.cod = 1');

        if($currency)
            $cmd = $cmd->andWhere('courier.cod_currency = :cod_currency', [':cod_currency' => $currency]);

        $cmd = $cmd->queryScalar();

        return $cmd;
    }

    public static function getTableName()
    {
        return 'courier_cod_settled';
    }

    public static function removeListOfItems(array $list)
    {
        if(!S_Useful::sizeof($list))
            return false;

        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();

        $condition = [];
        $params = [];
        $i = 0;
        foreach($list AS $item)
        {
            $condition[] = 'courier_id = :courier_id_'.$i;
            $params[':courier_id_'.$i] = $item;

            $i++;
        }

        $condition = implode(' OR ', $condition);

        return $cmd->delete(self::getTableName(), $condition, $params);
    }


    public static function addListOfItems(array $list, $type, $settle_date, $source = self::SOURCE_OWN)
    {

        if($settle_date == '')
            $settle_date = date('Y-m-d');

        if(!S_Useful::sizeof($list))
            return false;

        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $user_ids = $cmd
            ->select('id, user_id')
            ->from((new Courier())
                ->tableName())->where(['in', 'id', $list])->queryAll();


        $cmd = Yii::app()->db->createCommand();
        $cmd = $cmd->select('courier_id')->from(self::getTableName());

        $data = [];
        $i = 0;
        foreach ($list AS $courier_id) {
            $foundUserId = false;
            foreach ($user_ids AS $key => $user_id) {
                if ($user_id['id'] == $courier_id) {
                    $foundUserId = $user_id['user_id'];
                    unset($user_ids[$key]);
                    break;
                }
            }

            $data[$courier_id] = [
                'courier_id' => $courier_id,
                'user_id' => $foundUserId,
                'settle_date' => $settle_date,
                'type' => $type,
                'source' => $source,
            ];

            $cmd->orWhere('user_id = :user_id_' . $i . ' AND courier_id = :courier_id_' . $i, [':user_id_' . $i => $foundUserId, ':courier_id_' . $i => $courier_id]);
            $i++;
        }

        //////// to prevent duplicates
        $found = $cmd->queryColumn();
        foreach ($found As $foundItem)
            unset($data[$foundItem]);
        //////////

        if(!S_Useful::sizeof($data))
            return false;

        $builder = Yii::app()->db->schema->commandBuilder;

        $cmd = $builder->createMultipleInsertCommand(self::getTableName(), $data);
        return $cmd->execute();
    }

    public static function addItem($id, $type, $settle_date, $source = self::SOURCE_OWN)
    {
        $cmd = Yii::app()->db->createCommand();
        $user_id = $cmd
            ->select('user_id')
            ->from((new Courier())
                ->tableName())
            ->where('id = :id', [':id' => $id])
            ->queryScalar();

        if(!$user_id)
            return false;

        $cmd = Yii::app()->db->createCommand();
        return $cmd->insert(self::getTableName(), [
            'courier_id' => $id,
            'user_id' => $user_id,
            'type' => $type,
            'settle_date' => $settle_date,
            'source' => $source,
        ]);
    }

    /**
     * @param $id
     * @return string|false Returns false if not found. Otherwise - date of enter
     */
    public static function ifCodSettled($id)
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->select('date_entered')->from(self::getTableName())->where('courier_id = :id', [':id' => $id])->queryScalar();
    }



    public static function exportToXls(array &$models, $forUser = true)
    {

        /* @var $models Courier[] */

        $done = [];

        //
        $arrayHeader = [];
        if(!$forUser)
            $arrayHeader[] = 'settle_id';

        $arrayHeader[] = 'settle_date';
        $arrayHeader[] = 'type';
        $arrayHeader[] = 'local_id';
        $arrayHeader[] = 'date_package';
        $arrayHeader[] = 'cod_value';
        $arrayHeader[] = 'cod_currency';
        $arrayHeader[] = 'date_entered';
        $arrayHeader[] = 'is_settled';

        if(!$forUser)
            $arrayHeader[] = 'user_id';
        //

        $data = [];
        $data[0] = $arrayHeader;

        foreach($models AS $iter => &$model)
        {
            $temp = [];
            if(!$forUser)
                $temp[] = $model->courierCodSettled ? $model->courierCodSettled->id : '';


            $temp[] = $model->courierCodSettled ? $model->courierCodSettled->settle_date : '';
            $temp[] = $model->courierCodSettled ? $model->courierCodSettled->getTypeName() : '';
            $temp[] = $model->local_id;
            $temp[] = $model->date_entered;
            $temp[] = $model->cod_value;
            $temp[] = $model->cod_currency;
            $temp[] = $model->courierCodSettled ? $model->courierCodSettled->date_entered : '';
            $temp[] = $model->courierCodSettled ? 'yes' : 'no';

            if(!$forUser)
                $temp[] = $model->user->login;

            $data[] = $temp;

            unset($models[$iter]);
            gc_collect_cycles();

            $done[$iter] = $model->id;
        }

        require_once(Yii::getPathOfAlias('application.vendor.excel_xml').'/Excel_XML.php');
        $xls = new Excel_XML('UTF-8', true, 'Export');
        $xls->addArray($data);
        $xls->generateXML('Export');

        return $done;

    }

//    public static function exportToXlsUser(array &$models, $separateTypes = false)
//    {
//        $courierAttributes = [
//            'local_id',
//            'date_entered',
//            'courier_stat_id',
////            'stat.name',
//            'package_weight',
//            'package_size_l',
//            'package_size_w',
//            'package_size_d',
//            'packages_number',
//            'package_value',
//            'package_content',
//            'cod',
//            'ref',
//            'note',
//        ];
//
//        $addressDataAttributes = [
//            'name',
//            'company',
//            'country',
//            'city',
//            'zip_code',
//            'address_line_1',
//            'address_line_2',
//            'tel',
//        ];
//
//        $addressDataAttributesSender = array_merge($addressDataAttributes, [
//            'bankAccount',
//        ]);;
//
//        $typeInternalAttributes = [
//            'client_cod_value',
//            'cod_value',
//            'cod_currency',
//        ];
//
//        return self::_exportToXls($models, $courierAttributes, $addressDataAttributes, true, true, $typeInternalAttributes, $addressDataAttributesSender, true);
//    }

    protected static function _exportToXls(array &$models, array $courierAttributes, $forUser = false)
    {
        $done = [];

        $arrayHeader = array_merge($courierAttributes);

        $array = [];
        $array[0] = $arrayHeader;

        $i = 1;

        foreach($models AS $iter => &$model)
        {

            $dataCourier = $model->getAttributes($courierAttributes);
            $array[$i] = array_merge($dataCourier);
            $i++;

            unset($models[$iter]);
            gc_collect_cycles();

            $done[$iter] = $model->id;
        }

        var_dump($array);
        exit;


        require_once(Yii::getPathOfAlias('application.vendor.excel_xml').'/Excel_XML.php');
        $xls = new Excel_XML('UTF-8', true, 'Export');
        $xls->addArray($array);
        $xls->generateXML('Export');

        return $done;
    }
}