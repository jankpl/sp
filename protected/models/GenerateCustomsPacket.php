<?php

/**
 * Class GenerateCustomsPacket
 */
class GenerateCustomsPacket
{
    const RETURN_PDF = 1;
    const RETURN_PDF_STRING = 2;
    const RETURN_PDF_PIPELINE = 3;
    const RETURN_PNG_ARRAY = 4;


    public static function generateMulti(array $couriers, $returnType = self::RETURN_PDF)
    {
        $pdf = PDFGenerator::initialize();

        /* @var $courier Courier */
        foreach($couriers AS $courier) {

            if(in_array($courier->receiverAddressData->country_id, CountryList::getUeList(true)))
                continue;

            $pdf = self::generate($courier, false, $pdf);
        }

        if($returnType == self::RETURN_PDF) {
            return $pdf->Output('customs.pdf', 'D');
        }
        else if($returnType == self::RETURN_PDF_STRING)
        {
            return $pdf->Output('customs.pdf', 'S');
        }
        else if($returnType == self::RETURN_PDF_PIPELINE)
        {
            return $pdf;
        }
        else if($returnType == self::RETURN_PNG_ARRAY)
        {

            $pngArray = [];
            $count = $pdf->getNumPages();
            $pdfString = $pdf->Output('customs.pdf', 'S');

            $tempFile = tempnam(sys_get_temp_dir(),'spcustoms');


            @file_put_contents($tempFile, $pdfString);


            $im = ImagickMine::newInstance();

            for($i = 0; $i < $count; $i++)
            {
                $im->readImage($tempFile . '['.$i.']');
                $im->setResolution(300, 300);
                $im->setImageFormat('png');
                $im->trimImage(0);

                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }

                $im->stripImage();

                $pngArray[] = base64_encode($im->getImageBlob());
            }

            @unlink($tempFile);
            return $pngArray;
        }

        return false;
    }


    public static function generate(Courier $courier, $returnType = self::RETURN_PDF, $pipelinePdf = false)
    {
        if(!in_array($courier->receiverAddressData->country_id, CountryList::getUeList(true)))
        {

            $courierLabelNew = $courier->getSingleLabel();

            if($pipelinePdf)
                $pdf = $pipelinePdf;
            else
                $pdf = PDFGenerator::initialize();



            if($courierLabelNew->operator != CourierLabelNew::OPERATOR_DHLDE
                && !in_array($courierLabelNew->operator, [
                    CourierLabelNew::OPERATOR_UPS,
                    CourierLabelNew::OPERATOR_UPS_SAVER_4VALUES,
                    CourierLabelNew::OPERATOR_UPS_SAVER,
                    CourierLabelNew::OPERATOR_UPS_EXPRESS,
                    CourierLabelNew::OPERATOR_UPS_BIS,
                    CourierLabelNew::OPERATOR_UPS_EXPRESS_L,
                    CourierLabelNew::OPERATOR_UPS_EXPRESS_M,
                    CourierLabelNew::OPERATOR_UPS_F,
                    CourierLabelNew::OPERATOR_UPS_SAVER_F,
                    CourierLabelNew::OPERATOR_UPS_M,
                    CourierLabelNew::OPERATOR_UPS_SAVER_BIS,
                    CourierLabelNew::OPERATOR_UPS_SAVER_M,
                    CourierLabelNew::OPERATOR_UPS_UK,
                    CourierLabelNew::OPERATOR_UPS_SAVER_MCM,
                ])
                && $courierLabelNew->operator != CourierLabelNew::OPERATOR_DHLEXPRESS
            )
                $pdf = CourierCN23::generateMulti([$courier], $pdf);


            $pdf = CourierInvoice::generateInvoice([$courier],  $pdf);

            if($courierLabelNew->operator == CourierLabelNew::OPERATOR_DHLDE)
            {
                $pdf = CourierInvoice::generateInvoice([$courier],  $pdf);
                $pdf = CourierInvoice::generateInvoice([$courier],  $pdf);
            }

            $special = false;
            switch($courierLabelNew->operator)
            {
                case CourierLabelNew::OPERATOR_UPS:
                case CourierLabelNew::OPERATOR_UPS_SAVER_4VALUES:
                case CourierLabelNew::OPERATOR_UPS_SAVER:
                case CourierLabelNew::OPERATOR_UPS_EXPRESS:
                case CourierLabelNew::OPERATOR_UPS_BIS:
                case CourierLabelNew::OPERATOR_UPS_EXPRESS_L:
                case CourierLabelNew::OPERATOR_UPS_EXPRESS_M:
                case CourierLabelNew::OPERATOR_UPS_F:
                case CourierLabelNew::OPERATOR_UPS_SAVER_F:
                case CourierLabelNew::OPERATOR_UPS_M:
                case CourierLabelNew::OPERATOR_UPS_SAVER_BIS:
                case CourierLabelNew::OPERATOR_UPS_SAVER_M:
                case CourierLabelNew::OPERATOR_UPS_UK:
                case CourierLabelNew::OPERATOR_UPS_SAVER_MCM:
                    $pdf = CourierUpsClereanceDoc::generateSingle($courierLabelNew, $pdf);
                    break;
                case CourierLabelNew::OPERATOR_DHLDE:
                    $pdf = CourierExtExtAckCards::getAckCardAsPdf($courierLabelNew, $pdf, CourierExtExtAckCards::TYPE_CUSTOMS_DEC);
                    break;
//                case GLOBAL_BROKERS::BROKER_MEEST:
//                    $pdf = CourierExtExtAckCards::getAckCardAsPdf($courierLabelNew, $pdf, CourierExtExtAckCards::TYPE_CUSTOMS_DEC);
//                    break;
                case CourierLabelNew::OPERATOR_DHLEXPRESS:
                    $pdf = CourierDhlLvDoc::generateSingle($courierLabelNew, $pdf);
                    break;
                default:
                    $special = false;
                    break;
            }

            if($pipelinePdf)
                return $pipelinePdf;

            if($returnType == self::RETURN_PDF) {
                return $pdf->Output('customs.pdf', 'D');
            }
            else if($returnType == self::RETURN_PDF_STRING)
            {
                return $pdf->Output('customs.pdf', 'S');
            }
            else if($returnType == self::RETURN_PDF_PIPELINE)
            {
                return $pdf;
            }
            else if($returnType == self::RETURN_PNG_ARRAY)
            {

                $pngArray = [];
                $count = $pdf->getNumPages();
                $pdfString = $pdf->Output('customs.pdf', 'S');

                $tempFile = tempnam(sys_get_temp_dir(),'spcustoms');


                @file_put_contents($tempFile, $pdfString);


                $im = ImagickMine::newInstance();

                for($i = 0; $i < $count; $i++)
                {
                    $im->readImage($tempFile . '['.$i.']');
                    $im->setResolution(300, 300);
                    $im->setImageFormat('png');
                    $im->trimImage(0);

                    if ($im->getImageAlphaChannel()) {
                        $im->setImageBackgroundColor('white');
                        $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                    }

                    $im->stripImage();

                    $pngArray[] = base64_encode($im->getImageBlob());
                }

                @unlink($tempFile);
                return $pngArray;
            }

            return false;
        }

    }

    public static function generateReminderImage()
    {
        $im = ImagickMine::newInstance();

        $draw = new ImagickDraw();
        $pixel = new ImagickPixel( 'white' );

        $draw->setTextAlignment(2);

        $im->newImage(500, 700, $pixel);
        $draw->setFillColor('black');
        $draw->setFontSize( 20 );

        $im->annotateImage($draw, 250, 50, 0, Yii::t('courier', "Pamiętaj o dołączeniu dokumentów celnych{br}do paczek adresowanych poza UE!{br}{br}Można je wygenerować w szczegółach paczki.{br}{br}Pamiętaj o wydrukowaniu, podpisaniu{br}oraz umieszczeniu ich w osobnej przyldze{br}w widocznym miejscu na paczce, którą wysyłasz.{br}{br}Jeżeli chcesz wysłać taką paczkę,{br}skontaktuj się z opiekunem handlowym.{br}{br}Pamiętaj, że opis zawartośći (content) musi być{br}w języku angielskim.{br}{br}{br}----------------{br}{br}{br}Please remember to include customs documents{br}to items addressed outside UE!{br}{br}They can be generated in item details.{br}{br}Please remember about printing, signing{br}and placing in separate filister in visible spot{br}on package.{br}{br}If you want to send this type of package,{br}contact your salesman.", ['{br}' => "\r\n"]));

        $im->setImageFormat('png');

        return $im->getImageBlob();

    }


    public static function generateReminderPdfPage($pdf)
    {
        $pdf->AddPage('P');
        $pdf->Image('@'.self::generateReminderImage(),0,0,'','','','','TC',false, 300, '', false, false, 0, true,false,true);

        return $pdf;
    }

}