<?php

class CronJohnController extends CController {

//    const PACKAGE_DAYS_IGNORE = 31; // for how many days check statuses

//    const PACKAGE_DAYS_IGNORE_INTERNAL_RETURN = 62; // for how many days check statuses of Internal Return packages

    const PACKAGE_DAYS_FRESH = 7; // for how many days package is considered as fresh

    const TTLOG_BLOCK_MINUTES = 360; // how long to waint between checks for not-fresh packages
    const TTLOG_BLOCK_MINUTES_FRESH = 180; // how long to wait between checks for fresh packages
    const DELAY_NEW_MINUTES = 240; // how long to wait before checking TT for new packages

    const STRIP_SIZE = 100;

    const STRIP_SIZE_EXPIRE_LABELS = 500;
    const STRIP_SIZE_ARCHIVE_HISTORY = 5000;

    const INSTANCES = 16;

    const MAX_STEPS = 8;


    public function init()
    {
        Yii::app()->getModule('courier');
        parent::init();
    }

    public function filters()
    {
        return array();
    }

    function actionRun()
    {

        exit;


        $multiCurl = [];
        $result = [];

        $uniqId = uniqid();

        $mh = curl_multi_init();
        for($i = 1; $i <= self::INSTANCES; $i++) {

//            $cookieFile = Yii::app()->basePath.'/_temp/cronJohnRun_cookie_'.$i.'_'.$uniqId.'.txt';
            $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohn/index', ['mod' => $i, 'type' => 'main']);

            $multiCurl[$i] = curl_init();
            curl_setopt($multiCurl[$i], CURLOPT_URL, $fetchURL);
            curl_setopt($multiCurl[$i], CURLOPT_HEADER,0);
            curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER,1);
            curl_setopt($multiCurl[$i], CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($multiCurl[$i], CURLOPT_MAXREDIRS, 10);
            curl_setopt($multiCurl[$i], CURLOPT_TIMEOUT, 0);
            curl_setopt($multiCurl[$i], CURLOPT_TIMEOUT_MS, 0);
            curl_setopt($multiCurl[$i], CURLOPT_CONNECTTIMEOUT, 0);
            curl_multi_add_handle($mh, $multiCurl[$i]);
        }
        $index = null;
        do {
            curl_multi_exec($mh,$index);
        } while($index > 0);

        foreach($multiCurl as $k => $ch) {
            $result[$k] = curl_multi_getcontent($ch);
            curl_multi_remove_handle($mh, $ch);
        }
        curl_multi_close($mh);

//
//
//
//        for($i = 1; $i <= self::INSTANCES; $i++) {
//
//            sleep(5);
//
//            $cookieFile = Yii::app()->basePath.'/_temp/cronJohnRun_cookie_'.$i.'.txt';
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, Yii::app()->createAbsoluteUrl('/cronJohn/index', ['mod' => $i,]));
//            curl_setopt($ch, CURLOPT_HEADER, 0);
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
//            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
//            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
//            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
//            curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
////            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_exec($ch);
//            curl_close($ch);
//        }
    }

    function actionRun2()
    {
        exit;
        $cookieFile = Yii::app()->basePath.'/_temp/cronJohnRun_cookie.txt';

        var_dump($cookieFile);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Yii::app()->createAbsoluteUrl('/cronJohn/index', ['mod' => 1]));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        $resp = curl_exec($ch);
        $err = curl_error($ch);

        var_dump($resp);
        var_dump($err);


        curl_close($ch);

    }

    protected function _modelsMain($instances, $mod)
    {

        if($mod == 0)
            $mods = implode(',', range(0,self::INSTANCES));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $models = Courier::model()
            ->findAllBySql("SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
(
    (courier.date_updated > (NOW() - INTERVAL :past_date_fresh DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date_fresh DAY))
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block_fresh MINUTE))
)
OR
(
	(courier.date_updated > (NOW() - INTERVAL :past_date DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date DAY))
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
)
OR
(
	(courier.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date2 DAY)) AND courier.courier_type = :courier_type
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
)
)
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC
LIMIT ".self::STRIP_SIZE
                ,
                [
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER,
                    ':past_date2' => Courier::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS,
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,
                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }

//    protected function _modelsReturn($mod, $mods)
//    {
//        $models = Courier::model()
//            ->findAllBySql("SELECT DISTINCT courier.* FROM courier
//                    LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
//                    WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
//                    AND courier_stat_id IN ($cancelledStats)
//                    AND
//                    (
//                        (courier.date_updated > (NOW() - INTERVAL :past_date DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date DAY))
//                        AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
//                    )
//                    AND (user_cancel != :user_tt_found_flag)
//                    ORDER BY tt_log.id ASC
//                    LIMIT ".self::STRIP_SIZE
//                ,
//                [
//                    ':delay_new' => 240,
//                    ':type' => TtLog::TYPE_COURIER,
//                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER_CANCELLED,
//                    ':ttLog_block' => 1440,
//                    ':user_tt_found_flag' => Courier::USER_CANCELL_REQUESTED_FOUND_TT,
////                  ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
//                ]);
//
//    }


    protected function _modelsCancelled($mod, $mods)
    {
        $cancelledStats = implode(',', [
            CourierStat::CANCELLED,
            CourierStat::CANCELLED_PROBLEM,
            CourierStat::CANCELLED_USER
        ]);

        $models = Courier::model()
            ->findAllBySql("SELECT DISTINCT courier.* FROM courier
                    LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
                    WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
                    AND courier_stat_id IN ($cancelledStats)
                    AND
                    (
                        (courier.date_updated > (NOW() - INTERVAL :past_date DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date DAY))
                        AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
                    )
                    AND (user_cancel != :user_tt_found_flag)
                    ORDER BY tt_log.id ASC
                    LIMIT ".self::STRIP_SIZE
                ,
                [
                    ':delay_new' => 240,
                    ':type' => TtLog::TYPE_COURIER,
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER_CANCELLED,
                    ':ttLog_block' => 1440,
                    ':user_tt_found_flag' => Courier::USER_CANCELL_REQUESTED_FOUND_TT,
//                  ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                ]);
        return $models;
    }

    protected function _modelsReturned($mod, $mods)
    {
        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $models = Courier::model()
            ->findAllBySql("SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
(
    (courier.date_updated > (NOW() - INTERVAL :past_date_fresh DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date_fresh DAY))
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block_fresh MINUTE))
)
OR
(
	(courier.date_updated > (NOW() - INTERVAL :past_date DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date DAY))
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
)
OR
(
	(courier.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date2 DAY)) AND courier.courier_type = :courier_type
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
)
)
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
AND (courier.id MOD ".$mods.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC
LIMIT ".self::STRIP_SIZE
                ,
                [
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER,
                    ':past_date2' => Courier::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS,
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,
                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }


    // update courier packages statuses with external statuses
    public function actionIndex($type, $mod = 0, $set = 0, $ssid = NULL)
    {
        exit;


        sleep(2 * $mod);

        if(date('G') > 23 OR date('G') < 5)
            exit;

        if(!$mod)
            exit;

        $start = date('Y-m-d H:i:s');

        $filename = 'cjlog.txt';


        $CACHE_NAME = 'cronJohnRun_'.$mod;
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== $ssid) {

            MyDump::dump($filename, "\r\n".$start." : AIP[$mod] : ".Yii::app()->session->sessionID." : ".$sessionName." : ". $ssid);
            Yii::app()->end();
        } else
            MyDump::dump($filename, "\r\n".$start." : START[$mod][$set] : ".Yii::app()->session->sessionID." : ".$sessionName." : ". $ssid);

        if(S_SystemLoad::isHigh())
        {

            MyDump::dump($filename, "\r\n".$start." : [$mod] : HIGH LOAD");
            Yii::app()->cache->delete($CACHE_NAME);
            Yii::app()->end();
        }

        if($ssid == '')
            $ssid = Yii::app()->session->sessionID;

        Yii::app()->cache->set($CACHE_NAME, $ssid, 60 * 15);

        if($mod == 1)
            self::tt4CancelledPackages();

        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);

        if($type == 'main')
            $couriersNew = self::_modelsMain(16, $mod);
        else
            throw new Exception('Unknowny type!');

        $log = $start.' ['.print_r($mod,1).'] : '.sprintf("%02d",$set)." --- ". sprintf("%03d",sizeof($couriersNew))." --- ";

        try {
            /* $item Courier */
            if (is_array($couriersNew))
                foreach ($couriersNew AS $item) {
                    CourierExternalManager::updateStatusForPackage($item);
                    MyDump::dump('cj_full_log.txt', $item->id.' : '.print_r($mod,1).' : '.print_r($set,1));
                }
        }
        catch(Exception $ex)
        {
            MyDump::dump('cj_exception.txt', print_r($ex->getMessage(),1));
        }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(sizeof($couriersNew) == self::STRIP_SIZE)
        {
            if($set < self::MAX_STEPS)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM : ".Yii::app()->session->sessionID." : ". $ssid;
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohn/index', array('set' => $set, 'mod' => $mod, 'ssid' => $ssid, 'type' => $type));
                MyDump::dump($filename, $log);
                header("Location: ".$path);
                Yii::app()->end();
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM: ".Yii::app()->session->sessionID." : ". $ssid;
                MyDump::dump($filename, $log);

                Yii::app()->cache->delete($CACHE_NAME);
                Yii::app()->end();
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM : ".Yii::app()->session->sessionID." : ". $ssid;
            MyDump::dump($filename, $log);

            Yii::app()->cache->delete($CACHE_NAME);
            Yii::app()->end();
        }


    }

    protected function tt4CancelledPackages()
    {
        $mod = 'cancelled';

        $start = date('Y-m-d H:i:s');

        $cancelledStats = implode(',', [
            CourierStat::CANCELLED,
            CourierStat::CANCELLED_PROBLEM,
            CourierStat::CANCELLED_USER
        ]);

        $couriersNew = Courier::model()
            ->findAllBySql("SELECT DISTINCT courier.* FROM courier
                    LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
                    WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
                    AND courier_stat_id IN ($cancelledStats)
                    AND
                    (
                        (courier.date_updated > (NOW() - INTERVAL :past_date DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date DAY))
                        AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
                    )
                    AND (user_cancel != :user_tt_found_flag)
                    ORDER BY tt_log.id ASC
                    LIMIT ".self::STRIP_SIZE
                ,
                [
                    ':delay_new' => 60,
                    ':type' => TtLog::TYPE_COURIER,
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER_CANCELLED,
                    ':ttLog_block' => 720,
                    ':user_tt_found_flag' => Courier::USER_CANCELL_REQUESTED_FOUND_TT,
//                  ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                ]);

        $set = 0;
        $log = $start.' ['.print_r($mod,1).'] : '.sprintf("%02d",$set)." --- ". sprintf("%03d",sizeof($couriersNew))." --- ";

        /* $item Courier */
        if(is_array($couriersNew))
            foreach($couriersNew AS $item)
            {
                CourierExternalManager::updateStatusForPackage($item);
            }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        $log .= " --- ".sprintf("%04d",$differenceInSeconds)." ---";
        MyDump::dump('cjlog_cancell.txt', $log);

    }

    /**
     * Action expires old labels
     */

    public function actionArchiveHistoryStatus($set = 0)
    {

        $start = date('Y-m-d H:i:s');

        $CACHE_NAME = 'cronJohnArchiveHistoryStatusRun';
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== Yii::app()->session->sessionID) {
            MyDump::dump('cjahslog.txt', "\r\n".$start." : AIP");
            Yii::app()->end();
        }
        Yii::app()->cache->set($CACHE_NAME, Yii::app()->session->sessionID, 60 * 15);

        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);


        // OLD HISTORY:
        $toArchive = Courier::model()->findAll('(date_updated IS NOT NULL AND date_updated < (NOW() - INTERVAL :days DAY) OR (date_updated IS NULL AND date_entered < (NOW() - INTERVAL :days DAY)))       
        AND ((archive != :archive AND archive != :archive2) OR archive IS NULL) LIMIT :limit', [
            ':days' => Courier::TIMER_ARCHIVE_HISTORY_AFTER_DAYS,
            ':archive' => Courier::ARCHIVED_HISTORY,
            ':archive2' => (Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED),
            ':limit' => self::STRIP_SIZE_ARCHIVE_HISTORY,
        ]);

        /* @var $item Courier */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {
                CourierStatHistory::expireOldHistory($item);
            }


        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        $log = $start.' : '.sprintf("%02d",$set)." --- ". sprintf("%03d",sizeof($toArchive))." --- ";

        // there is propably more
        if(sizeof($toArchive) == self::STRIP_SIZE_ARCHIVE_HISTORY)
        {
            if($set < 10)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohn/archiveHistoryStatus', array('set' => $set ));

                MyDump::dump('cjahslog.txt', $log);

                header("Location: ".$path);
                exit;
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";
                MyDump::dump('cjahslog.txt', $log);

                Yii::app()->cache->delete($CACHE_NAME);
                exit;
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";
            // run only on first round deleting old stats
            if(!$set)
            {
                // UNUSED STATS:
                CourierStat::expireUnusedOldStats();
                $log .= "\r\nEOS";
                TtLog::clearOldData();
                $log .="\r\nEOT";
            }

            MyDump::dump('cjahslog.txt', $log);

            Yii::app()->cache->delete($CACHE_NAME);
            exit;
        }



    }



    /**
     * Action expires old labels
     */
    public function actionArchiveLabels($set = 0)
    {

        $start = date('Y-m-d H:i:s');

        $CACHE_NAME = 'cronJohnArchiveLabelsRun';
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== Yii::app()->session->sessionID) {
            MyDump::dump('cjallog.txt', "\r\n".$start." : AIP");
            Yii::app()->end();
        }
        Yii::app()->cache->set($CACHE_NAME, Yii::app()->session->sessionID, 60 * 15);

        self::_clearOldOrders();

        $set = intval($set);

        Yii::app()->getModule('postal');
        $toArchive = CourierLabelNew::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY) AND stat = :stat LIMIT :limit', [
            ':days' => Courier::TIMER_ARCHIVE_LABELS_AFTER_DAYS,
            ':stat' => CourierLabelNew::STAT_SUCCESS,
            ':limit' => self::STRIP_SIZE_EXPIRE_LABELS
        ]);

        $log = $start.' : '.sprintf("%02d",$set)." --- ". sprintf("%03d",sizeof($toArchive))." --- ";

        /* @var $item CourierLabelNew */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {
                $item->expireLabel();
            }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(sizeof($toArchive) == self::STRIP_SIZE_EXPIRE_LABELS)
        {
            if($set < 10)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                MyDump::dump('cjallog.txt', $log);
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohn/archiveLabels', array('set' => $set));

                header("Location: ".$path);
                exit;
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";

                MyDump::dump('cjallog.txt', $log);
                Yii::app()->cache->delete($CACHE_NAME);
                exit;
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";

            MyDump::dump('cjallog.txt', $log);
            Yii::app()->cache->delete($CACHE_NAME);
            exit;
        }


    }


    public function _clearOldOrders()
    {

        $models = Order::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY) AND (order_stat_id =:order_stat_1 OR order_stat_id = :order_stat_2) ', [':days' => Courier::TIMER_CANCEL_UNFINALIZED_ORDERS, ':order_stat_1' => OrderStat::NEW_ORDER, ':order_stat_2' => OrderStat::WAITING_FOR_PAYMENT]);

        /* @var $model Order */
        foreach($models AS $model) {

            MyDump::dump('coo.txt', 'ORDER CANCELLED: '.$model->id);
            $model->changeStat(OrderStat::CANCELLED);
        }


        $models = Courier::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY) AND (courier_stat_id = :stat_1 OR courier_stat_id = :stat_2) ', [':days' => Courier::TIMER_CANCEL_UNFINALIZED_ITEMS, ':stat_1' => CourierStat::NEW_ORDER, ':stat_2' => CourierStat::PACKAGE_PROCESSING]);
        /* @var $model Courier */
        foreach($models AS $model) {
            MyDump::dump('coo.txt', 'COURIER CANCELLED: '.$model->id);
            $model->changeStat(CourierStat::CANCELLED, 0, NULL, false, null, false, true, false, false, true);
        }

    }







    public function actionOldest($set = 0)
    {

        $start = date('Y-m-d H:i:s');


        $CACHE_NAME = 'cronJohnRun_OLDEST';
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== Yii::app()->session->sessionID) {

            MyDump::dump('cjlog_oldest.txt', "\r\n".$start." : AIP[]");
            Yii::app()->end();
        }

        if(S_SystemLoad::isHigh())
        {

            MyDump::dump('cjlog_oldest.txt', "\r\n".$start." : [] : HIGH LOAD");
            Yii::app()->end();
        }

        Yii::app()->cache->set($CACHE_NAME, Yii::app()->session->sessionID, 60 * 15);


        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $couriersNew = Courier::model()
            ->findAllBySql("SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
(
    (courier.date_updated > (NOW() - INTERVAL :past_date_fresh DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date_fresh DAY))
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block_fresh MINUTE))
)
OR
(
	(courier.date_updated > (NOW() - INTERVAL :past_date DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date DAY))
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
)
OR
(
	(courier.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date2 DAY)) AND courier.courier_type = :courier_type
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
)
)
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC
LIMIT ".self::STRIP_SIZE
                ,
                [
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER,
                    ':past_date2' => Courier::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS,
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,
                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);


        $log = $start.' [] : '.sprintf("%02d",$set)." --- ". sprintf("%03d",sizeof($couriersNew))." --- ";

        /* $item Courier */
        if(is_array($couriersNew))
            foreach($couriersNew AS $item)
            {
                CourierExternalManager::updateStatusForPackage($item);
            }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(sizeof($couriersNew) == self::STRIP_SIZE)
        {
            if($set < 10)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohn/oldest', array('set' => $set));
                MyDump::dump('cjlog_oldest.txt', $log);
                header("Location: ".$path);
                exit;
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";
                MyDump::dump('cjlog_oldest.txt', $log);

                Yii::app()->cache->delete($CACHE_NAME);
                exit;
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";
            MyDump::dump('cjlog_oldest.txt', $log);

            Yii::app()->cache->delete($CACHE_NAME);
            exit;
        }


    }


}