<?php

Yii::import('application.modules.courier.models._base.BaseCourierHub');

class CourierHub extends BaseCourierHub
{
    const HUB_PL = 1;
    const HUB_NL = 5;

    const HUB_NL_ADDRESS_DATA_ID = 59908;
    const HUB_DE_ADDRESS_DATA_ID = 770;

//    const HUB_2 = 2;
//    const HUB_3 = 3;
//
//
//    protected static $_addressData = array(
//        self::HUB_1_NYSA => array(
//            'company' => 'KAAB',
//            'country' => 'Poland',
//            'country_id' => '1',
//            'zip_code' => '11-111',
//            'city' => 'Nysa',
//            'address_line_1' => 'Testowa 1',
//            'tel' => '123456789',
//        ),
//        self::HUB_2 => array(
//            'company' => 'KAAB',
//            'country' => 'Czech Repunlic',
//            'country_id' => '41',
//            'zip_code' => '55-555',
//            'city' => 'Praha',
//            'address_line_1' => 'Pepicka 2',
//            'tel' => '000000000023',
//        ),
//        self::HUB_3 => array(
//            'company' => 'KAAB',
//            'country' => 'Germany',
//            'country_id' => '142',
//            'zip_code' => '33-333',
//            'city' => 'Berlin',
//            'address_line_1' => 'Posdamer Platz 2',
//            'tel' => '9999999999',
//        ),
//    );

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function getNameWithId()
    {
        return '(#'.$this->id.') '.$this->name;
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=> S_Status::ACTIVE, 'on'=>'insert'),

            array('name, date_entered, stat, address_id', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>64),
            array('description', 'length', 'max'=>256),
            array('date_updated, description', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, date_updated, description, stat', 'safe', 'on'=>'search'),
        );
    }

    public static function listHubs()
    {
        $models = CourierHub::model()->findAll();

        return $models;
    }


    public function getAddress()
    {
        return $this->address;

//
//        $addressData = new AddressData();
//        $addressData->attributes = self::$_addressData[$this->id];
//
//        return $addressData;
    }


    public function saveWithAddress()
    {
        $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        if(!$this->address->save())
            $errors = true;

        if(!$errors)
        {
            $this->address_id = $this->address->id;
            if(!$this->save())
                $errors = true;
        }

        if($errors)
        {
            $transaction->rollback();
            return false;
        } else {
            $transaction->commit();
            return true;
        }


    }
}