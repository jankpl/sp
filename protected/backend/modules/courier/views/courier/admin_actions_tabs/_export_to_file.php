<h4>Export to file</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/exportToFileByGridView'),
));
?>

<?php
echo TbHtml::submitButton('Export to .xls', array(
    'onClick' => 'js:
    $("#courier-ids-export").val($("#courier").selGridView("getAllSelection").toString());
    ;',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => 'courier-ids-export')); ?>
<?php
$this->endWidget();
?>