<?php
/* @var $form TbActiveForm */
/* @var $model Courier */
/* @var $modelCourierUAddition [] */
$this->menu = array(
    array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('index')),
    array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>




    <h2><?= $domestic ? Yii::t('courier', 'Nowa paczka U Krajowa') : Yii::t('courier', 'Nowa paczka U'); ?></h2>
    <div class="form" id="u-form">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'courier-u-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'stateful'=>true,
        ));
        ?>
        <?php if(Yii::app()->user->hasFlash('previousModel'))
        {
            echo TbHtml::alert(TbHtml::LABEL_COLOR_SUCCESS, Yii::app()->user->getFlash('previousModel'), array( 'closeText' => false));
        }
        ?>
        <div class="row">
            <?php
            $this->Widget('AddressDataSwitch', ['attrBaseName' => 'CourierTypeU_AddressData']);
            ?>
            <div class="col-xs-6">
                <fieldset><legend><?= Yii::t('courier','Nadawca');?></legend>
                    <?php
                    $this->renderPartial('create/_addressDataForm/_form',
                        array(
                            'model' => $model->senderAddressData,
                            'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_SENDER),
                            'form' => $form,
                            'noEmail' => false,
                            'i' => UserContactBook::TYPE_SENDER,
                            'type' => UserContactBook::TYPE_SENDER,
                            'domestic' => $domestic,
                        )
                    );
                    ?>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-12">
                            <?php echo $form->labelEx($model,'_sendLabelOnMail'); ?>
                            <?php echo $form->checkBox($model,'_sendLabelOnMail', ['data-bootstrap-checkbox' => true, 'data-default-class' => 'btn-sm', 'data-on-class' => 'btn-sm btn-primary', 'data-off-class' => 'btn-sm btn-default', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak')]); ?>
                            <?php echo $form->error($model, '_sendLabelOnMail'); ?>
                        </div>
                    </div><!-- row -->
                </fieldset>
            </div>
            <div class="col-xs-6">
                <fieldset><legend><?= Yii::t('courier','Odbiorca');?></legend>
                    <div class="alert alert-info" style="display: none;" data-delivery-broker-id-show="<?= CourierUOperator::BROKER_DHLDE;?>" data-delivery-target-country-id-show="<?= CountryList::COUNTRY_DE;?>">
                        <?php $this->renderPartial('/_common/_dhlde_packstation');?>
                    </div>
                    <?php
                    $this->renderPartial('create/_addressDataForm/_form',
                        array(
                            'model' => $model->receiverAddressData,
                            'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_RECEIVER),
                            'form' => $form,
                            'noEmail' => false,
                            'i' => UserContactBook::TYPE_RECEIVER,
                            'type' => UserContactBook::TYPE_RECEIVER,
                            'domestic' => $domestic,
                        ));?>
                </fieldset>

                <div id="inpost-receiver-part" style="display: none;" data-delivery-broker-id-show="<?= GLOBAL_BROKERS::BROKER_INPOST_SHIPX;?>" data-delivery-target-country-id-show="<?= CountryList::COUNTRY_PL;?>">
                    <?php
                    $this->renderPartial('/_common/_inpost_paczkomat',
                        [
                            'model' => $model,
                            'submodel' => $model,
                            'form' => $form,
                        ]);
                    ?>
                </div>

                <div id="part-paczkaWRuchu" style="display: none;" data-delivery-broker-id-show="<?= CourierUOperator::BROKER_RUCH;?>" data-delivery-target-country-id-show="<?= CountryList::COUNTRY_PL;?>">
                    <?php
                    $this->renderPartial('/_common/_ruch',
                        [
                            'model' => $model,
                            'submodel' => $model,
                            'form' => $form,
                            'addressDataModelName' => 'CourierTypeU_AddressData',
                        ]);
                    ?>
                </div>
            </div>
        </div>

        <fieldset>
            <legend><?= Yii::t('courier', 'Parametry paczki');?> <a href="#" id="show-requirements" tabindex="-1">(?)</a></legend>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="alert alert-info" id="requirements" style="display: none;">
                        <p><?php echo Yii::t('courier','Paczka musi spełniać następujące wymogi');?>:</p>
                        <ul>
                            <li><?php echo $model->getAttributeLabel('package_weight'); ?> < <?php echo CourierTypeInternal::MAX_PACKAGE_WEIGHT;?> <?php echo Courier::getWeightUnit();?></li>
                            <li><?php echo $model->getAttributeLabel('package_size_l'); ?> < <?php echo S_PackageMaxDimensions::getPackageMaxDimensions()['l'];?> <?php echo Courier::getDimensionUnit();?></li>
                            <li><?php echo $model->getAttributeLabel('package_size_d'); ?> < <?php echo S_PackageMaxDimensions::getPackageMaxDimensions()['d'];?> <?php echo Courier::getDimensionUnit();?></li>
                            <li><?php echo $model->getAttributeLabel('package_size_w'); ?> < <?php echo S_PackageMaxDimensions::getPackageMaxDimensions()['w'];?> <?php echo Courier::getDimensionUnit();?></li>
                            <li><?php echo Yii::t('courier','Suma długości i obwodu <');?> <?php echo S_PackageMaxDimensions::getPackageMaxDimensions()['combination'];?> <?php echo Courier::getDimensionUnit();?></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row text-center">
                <?php echo $form->labelEx($model,'packages_number', ['style' => 'width: auto;']); ?><br/>
                <?php echo $form->dropDownList($model, 'packages_number', S_Useful::generateArrayOfItemsNumber(CourierTypeInternal::NUMBER_OF_MULTIPLE_PACKAGES_ONCE), ['style' => 'text-align: center; width: 100px;', 'data-packages-no' => true, 'data-max-no' => CourierTypeInternal::NUMBER_OF_MULTIPLE_PACKAGES_ONCE]); ?>
                <?php echo $form->error($model,'packages_number'); ?>
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                    <div class="row">
                        <div class="col-xs-12 text-center"><strong><?= Yii::t('courier', 'Waga');?> [<?php echo Courier::getWeightUnit();?>]</strong></div>
                        <div class="col-xs-12 col-sm-4 col-sm-offset-1 text-center">
                            <?php echo $form->textField($model, '_package_weight_total', array('maxlength' => 7, 'style' => 'text-align: center;', 'data-weight-total' => true)); ?><?= Yii::t('courier', 'Łączna waga');?></div>
                        <div class="col-xs-12 col-sm-2 text-center hidden-xs" style="line-height: 40px; font-weight: bold; font-size: 1.2em; color: gray;"> = <span data-packages-no-show="true">1</span> &times; </div>
                        <div class="col-xs-12 col-sm-4 text-center"><?php echo $form->textField($model, 'package_weight', array('maxlength' => 7, 'style' => 'text-align: center;', 'data-weight' => true, 'data-package-weight' => true, 'data-dependency' => '10', 'data-max-weight' => CourierTypeU::MAX_PACKAGE_WEIGHT)); ?><?= Yii::t('courier', 'Pojedyncza paczka');?></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-right">
                    <?php echo $form->error($model,'package_weight'); ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-12 text-center"><strong><?= Yii::t('courier', 'Wymiary');?> [<?php echo Courier::getDimensionUnit();?>]</strong></div>
                <div class="col-xs-12 col-sm-2 col-sm-offset-3 text-center">
                    <?php echo $form->textField($model, 'package_size_l', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => 'l', 'data-dependency' => '10')); ?><?= Yii::t('courier', 'długość');?>
                </div>
                <div class="col-xs-12 col-sm-2 text-center"><?php echo $form->textField($model, 'package_size_w', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => 'w', 'data-dependency' => '10')); ?><?= Yii::t('courier', 'szerokość');?></div>
                <div class="col-xs-12 col-sm-2 text-center"><?php echo $form->textField($model, 'package_size_d', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => 'd', 'data-dependency' => '10')); ?><?= Yii::t('courier', 'wysokość');?></div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3 ">
                    <?php echo $form->error($model,'package_size_l'); ?>
                    <?php echo $form->error($model,'package_size_w'); ?>
                    <?php echo $form->error($model,'package_size_d'); ?>
                </div>
            </div>
            <div data-dim-weight-info="true" style="display: none;">
                <br/>
                <div class="alert alert-warning text-center"><?= Yii::t('courier', 'Waga gabarytowa pojedynczej paczki:');?> <span id="_dw_show" style="font-weight: bold;"></span> <?php echo Courier::getWeightUnit();?></div>
            </div>

        </fieldset>

        <fieldset id="operators-conainer"><legend><?php echo Yii::t('courier','Operator');?></legend>
            <div id="operators-empty" class="text-center">
                <span class="badge"><?= Yii::t('courier', 'Podaj parametry paczki, aby zobaczyć listę operatorów');?></span>
            </div>
            <div id="operators-none" class="text-center" style="display: none;">
                <span class="badge"><?= Yii::t('courier', 'Brak operatorów dla podanych parametrów');?></span>
            </div>
            <div id="operators-content">
                <div id="operators-sorting"><?= Yii::t('courier', 'Sortowanie:');?> <a href="javascript:void(0);" data-sort="name"><?= Yii::t('courier', 'Nazwa');?> <span class=""></span></a> | <a href="javascript:void(0);" data-sort="price"><?= Yii::t('courier', 'Cena');?> <span></span></a> | <a href="javascript:void(0);" data-sort="time"><?= Yii::t('courier', 'Czas');?> <span></span></a></div>
                <br/>
                <?= $form->hiddenField($model->courierTypeU, '_selected_operator_id', ['id' => '_selected_operator_id']); ?>
                <div class="text-center">
                    <?= $form->error($model->courierTypeU, 'courier_u_operator_id');?>
                </div>
                <div id="operators-list" class="text-center">
                    <?= $operators; ?>
                </div>
            </div>
        </fieldset>

        <?php
        if(Yii::app()->user->model->getCourierReturnAvailable()):
            ?>
            <div class="row" data-return="true" <?= CourierTypeReturn::isAutomaticReturnAvailable($model->receiverAddressData->country_id) ? '' : 'style="display: none;"';?>>
                <div class="col-xs-12">
                    <fieldset>
                        <legend><?= Yii::t('courier', 'Etykieta zwrotna'); ?></legend>
                        <div class="row">
                            <div class="col-xs-12">
                                <?php echo $form->labelEx($model,'_generate_return'); ?>
                                <?php echo $form->checkBox($model, '_generate_return', array('data-off-label' => Yii::t('courier','Nie'), 'data-on-label' =>  Yii::t('courier','Tak'), 'data-bootstrap-checkbox' =>true, 'data-default-class' => 'btn-md', 'data-on-class' => 'btn-md btn-primary', 'data-off-class' => 'btn-md btn-default', 'data-generate-return' => true)); ?>
                                <?php echo $form->error($model,'_generate_return'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        <?php
        endif;
        ?>

        <div class="row">
            <div class="col-xs-12">
                <fieldset>
                    <legend><?= Yii::t('courier', 'Package content');?></legend>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->labelEx($model,'package_content'); ?>
                            <?php echo $form->textField($model, 'package_content'); ?>
                            <?php echo $form->error($model,'package_content'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->labelEx($model,'package_value'); ?>
                            <?php echo $form->textField($model, 'package_value', ['style' => 'border-top-right-radius: 0; border-bottom-right-radius: 0;']); ?> <?php echo $form->dropDownList($model, 'value_currency', Courier::getCurrencyList());  ?>
                            <?php echo $form->error($model,'package_value'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->labelEx($model->courierTypeU,'_package_wrapping'); ?>
                            <?php echo $form->dropDownList($model->courierTypeU, '_package_wrapping', User::getWrappingList(), ['data-package-wrapping' => 'true']); ?>
                            <?php echo $form->error($model->courierTypeU,'_package_wrapping'); ?>
                        </div>
                    </div>
                    <?php
                    if(!Yii::app()->user->isGuest && Yii::app()->user->getModel()->getCourierRefPrefix()):
                        ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <?php echo $form->labelEx($model,'ref'); ?>
                                <?php echo $form->textField($model, 'ref'); ?>
                                <?php echo $form->error($model,'ref'); ?>
                            </div>
                        </div>
                    <?php
                    endif;
                    ?>
                </fieldset>
            </div>
        </div>

        <fieldset><legend><?php echo Yii::t('courier','Dodatki');?></legend>
            <div class="inline-form">
                <div class="text-center courier-u-additions-toggle">
                    [<a href="#additionList" data-up="<?= Yii::t('courier', 'zwiń');?> &uarr;" data-down="<?= Yii::t('courier', 'rozwiń');?> &darr;"><?= Yii::t('courier', 'zwiń');?> &uarr;</a>]
                </div>
                <div id="additionList" style="">

                    <div data-remote-area-info="<?= UserContactBook::TYPE_RECEIVER;?>" style="display: none;">
                        <div class="alert alert-warning"><?= Yii::t('courier', 'Odbiorca znajduje się w strefie utrudnionego dostępu. Koszt paczki zostanie powiększony z tego powodu o kwotę:');?> <span></span></div>
                    </div>

                    <div data-remote-area-info="<?= UserContactBook::TYPE_SENDER;?>" style="display: none;">
                        <div class="alert alert-warning"><?= Yii::t('courier', 'Nadawca znajduje się w strefie utrudnionego dostępu. Koszt paczki zostanie powiększony z tego powodu o kwotę:');?> <span></span></div>
                    </div>
                    <div class="additions-list-wrapper">
                        <div class="additions-list">
                            <?php
                            $courierAddition = [];
                            $additions = CourierUAdditionList::model()->findAllByAttributes(['stat' => 1]);
                            ?>

                            <div class="addition_item" data-addition-id="collection" data-addition-broker-id="">
                                <div class="not-available-addition-wrapper"><span class="badge"><?= Yii::t('courier', 'Ten dodatek nie jest dostępny dla podanych parametrów');?></span></div>
                                <div class="addition_item_col item_col_img"><div></div></div>

                                <?php
                                if(!Yii::app()->user->model->getHidePrices()):
                                    ?>
                                    <div class="addition_item_col item_col_price">
                                        0,00 <?= CourierTypeU::getCurrencySymbolForCurrentUser();?>/0,00 <?= CourierTypeU::getCurrencySymbolForCurrentUser();?>
                                    </div>
                                <?php
                                endif;
                                ?>
                                <div class="with-checkbox addition_item_col item_col_checkbox"><?php echo $form->checkBox($model->courierTypeU, 'collection', array('data-courier-addition' => 'true', 'data-group' => 'collection', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak'), 'id' => '_collection')); ?></div>
                                <div class="addition_item_col item_col_desc">  <label style="font-weight: normal; text-align: left; cursor: pointer; width: 100%;" for="caid_collection"><h4><?= Yii::t('courier', 'Odbiór przesyłki');?></h4>
                                        <p><?= Yii::t('courier', 'Wybierz, aby zamówić odbiór Twojej paczki przez kuriera');?></p></label>
                                </div>
                            </div>

                            <div class="addition_item cod" data-addition-id="cod" data-addition-broker-id="">
                                <div class="not-available-addition-wrapper"><span class="badge"><?= Yii::t('courier', 'Ten dodatek nie jest dostępny dla podanych parametrów');?></span></div>
                                <div class="addition_item_col item_col_img"><div></div></div>
                                <?php
                                if(!Yii::app()->user->model->getHidePrices()):
                                    ?>
                                    <div class="addition_item_col item_col_price">
                                        <span id="cod-price-n-value">-</span> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?>/<span id="cod-price-b-value">-</span> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?>
                                    </div>
                                <?php
                                endif;
                                ?>
                                <div class="with-checkbox addition_item_col item_col_checkbox"><?php echo CHtml::checkBox('_client_cod', $client_cod, array('data-courier-addition' => 'true', 'data-group' => 'cod', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak'), 'id' => '_client_cod')); ?></div>
                                <div class="addition_item_col item_col_desc">
                                    <label style="font-weight: normal; text-align: left; cursor: pointer; width: 100%;" for="_client_cod">
                                        <h4>COD</h4>
                                        <p><?= Yii::t('courier','Paczka będzie automatycznie ubezpieczona do kwoty równiej kwocie COD.');?></p>
                                    </label>
                                    <div id="cod-details" class="with-checkbox" style="<?= (!$client_cod ? 'display: none;' : '');?>">
                                        <div class="col-xs-12">
                                            <?php echo $form->labelEx($model,'cod_value'); ?>
                                            <?php echo $form->textField($model, 'cod_value', ['style' => 'border-top-right-radius: 0; border-bottom-right-radius: 0;']); ?>
                                            <div style="display: inline-block; background: #eeeeee; border: 1px solid #cccccc; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height: 28px; line-height: 26px; padding: 0 5px; margin-left: -5px;" id="cod-currency-name">-</div>
                                            <?php echo $form->error($model,'cod_value'); ?>
                                        </div>
                                        <div class="col-xs-12">
                                            <?php echo $form->labelEx($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccount'); ?>
                                            <?php echo $form->textField($model->senderAddressData, '['.UserContactBook::TYPE_SENDER.']bankAccount', array('maxlength' => 50, 'data-bank-account-target' => true)); ?> <input type="button" class="btn btn-xs btn-primary" value="<?= Yii::t('courier', 'Z ustawień');?>" data-copy-bank-account="<?= !Yii::app()->user->isGuest ? Yii::app()->user->model->getBankAccount() : '';?>"/>
                                            <?php echo $form->error($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccount'); ?>
                                            <div style="padding-left: 10px; font-style: italic; font-size: 0.8em; display: inline-block;"><?= Yii::t('courier', 'Nadawca przesyłki musi być właścicielem powyższego konta bankowego.');?></div>
                                        </div>
                                        <div class="col-xs-12">
                                            <?php echo $form->labelEx($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                                            <?php echo $form->checkBox($model->senderAddressData, '['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                                            <?php echo $form->error($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                            /* @var $additions CourierUAdditionList[] */
                            foreach($additions AS $item):
                                ?>
                                <div class="addition_item" data-addition-id="<?= $item->id;?>" data-addition-broker-id="<?= $item->broker_id;?>">
                                    <div class="not-available-addition-wrapper"><span class="badge"><?= Yii::t('courier', 'Ten dodatek nie jest dostępny dla podanych parametrów');?></span></div>
                                    <div class="addition_item_col item_col_img">
                                        <div>
                                        </div>
                                    </div>
                                    <?php
                                    if(!Yii::app()->user->model->getHidePrices()):
                                        ?>
                                        <div class="addition_item_col item_col_price">
                                            <?php
                                            echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice(), true)).' '.CourierTypeU::getCurrencySymbolForCurrentUser().'/'.S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($item->getPrice())).' '.CourierTypeU::getCurrencySymbolForCurrentUser();
                                            ?>
                                        </div>
                                    <?php
                                    endif;
                                    ?>
                                    <div class="with-checkbox addition_item_col item_col_checkbox">
                                        <?php echo CHtml::checkBox('CourierUAddition[]', in_array($item->id,$modelCourierUAddition), array('value' => $item->id, 'data-courier-addition' => 'true', 'data-group' => $item->group, 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak'), 'id' => 'caid_'.$item->id, 'data-addition-id' => $item->id)); ?></div>
                                    <div class="addition_item_col item_col_desc">
                                        <label style="font-weight: normal; text-align: left; cursor: pointer; width: 100%;" for="caid_<?= $item->id;?>"><h4><?php echo $item->courierUAdditionListTr->title;?> (#<?=$item->id;?>)</h4>
                                            <p><?php echo $item->courierUAdditionListTr->description; ?></p></label>
                                    </div>
                                </div>
                            <?php
                            endforeach;
                            ?>
                        </div>
                    </div>
                </div>
                <div style="float: right; width: 48%;">

                </div>
                <div style="clear: both;"></div>
            </div>
        </fieldset>
        <fieldset><legend><?= Yii::t('courier','Kontynuuj');?></legend>

            <div data-not-available="true">
                <div class="alert alert-warning text-center">
                    <?= Yii::t('courier', 'Niestety, ale nie możemy automatycznie obsłużyć tego kierunku.{br}{br}Skontaktuj się z nami, aby otrzymać indywidualna ofertę!', ['{br}' => '<br/>']);?>
                </div>
            </div>

            <div style="text-align: center;">
                <?php echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('name' => 'summary', 'class' => 'btn-lg btn-primary', 'disabled' => true, 'data-continue-button' => 'true')); ?>

                <?= $form->hiddenField($model, 'source'); // for cloning ?>
                <?php $this->endWidget();
                ?>
            </div>
        </fieldset>
    </div>

    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>
<?php $this->widget('BackLink');?>
<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  getOperators: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierU/ajaxGetOperatorList')).',
                  checkRemoteArea: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierU/ajaxCheckRemoteArea')).',
                  getCodCurrency: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierU/ajaxGetCodCurrency')).',
                  checkAdditions: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierU/ajaxCheckAdditions')).',
                  checkAutomaticReturn: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxIsAutomaticReturnAvailable')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).',
                    mapIco: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico.png').',
                  mapIcoOrange: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico_orange.png').',
                  mapIcoGreen: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico_green.png').'
              },
               operators: {

              },
               loader: {
                  textTop: '.CJSON::encode(Yii::t('site', 'Trwa ładowanie...')).',
                  textBottom: '.CJSON::encode(Yii::t('site', 'Proszę czekać...')).',
                  error:  '.CJSON::encode(Yii::t('site', 'Wystąpił błąd, ale system spróbuje automatycznie przetworzyć formularz. Następnie proszę spróbować kontynuować. Jeżeli błąd będzie się powtarzał, prosimy o kontakt.')).',
              },
              message: {
                customWrapping: '.CJson::encode(Yii::t('courier','Niestandardowe opakowanie wymaga opcji "Przesyłka niestandardowa"')).'
              },
              dimWeightMode: {
                    default: '.CJSON::encode(CourierUOperator::DIM_WEIGHT_MODE_DEFAULT).',
                    dhl: '.CJSON::encode(CourierUOperator::DIM_WEIGHT_MODE_DHL).',
              },             
              ids: {
               countryDe: '.CJSON::encode(CountryList::COUNTRY_DE).'
              },    
               customTypeOperatorsIds: '.CJSON::encode(CourierUOperator::getCustomTypeOperators(true)).',
               automaticReturnCountries: '.CJSON::encode(Yii::app()->user->model->getReturnPackageAutomatic()).',
               
          };
      ',CClientScript::POS_HEAD);
Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.bootstrap-touchspin.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/dimensionalWeight.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.bootstrap-touchspin.min.css');

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courierUForm.min.js', CClientScript::POS_HEAD);



Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?key='.GLOBAL_CONFIG::GOOGLE_MAPS_API_KEY);
Yii::app()->clientScript->registerCssFile('https://mapka.paczkawruchu.pl/paczkawruchu.css');
Yii::app()->clientScript->registerScriptFile('https://mapka.paczkawruchu.pl/jquery.pwrgeopicker.min.js');


?>