<?php

class GlobalOperatorTtResponseCollection
{
    protected $items = [];

    /**
     * @param $status
     * @param $date
     * @param $location
     */
    public function addItem($status, $date, $location)
    {
        $model = GlobalOperatorTtResponse::createSuccessResponse($status, $date, $location);
        $this->items[] = $model;
    }

    /**
     * @return array
     */
    public function adapterToCourierExternalManagerArray()
    {
        $data = [];
        /* @var $item GlobalOperatorTtResponse */
        foreach($this->items AS $item)
        {
            $data[] = [
                'name' => $item->geStatus(),
                'date' => $item->getDate(),
                'location' => $item->getLocation(),
            ];
        }
        return $data;
    }

    public function adapterToPostalExternalManagerArray()
    {
        return $this->adapterToCourierExternalManagerArray();
    }

    public static function mergeResponses(array $responses)
    {
        $statuses = new GlobalOperatorTtResponseCollection();

        foreach($responses AS $collection)
        {
            foreach($collection->items AS $item)
                $statuses->items[] = $item;
        }

        return $statuses;
    }

}