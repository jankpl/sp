<div class="form">
    <?php

    $form = $this->beginWidget('CActiveForm',
        array(
            'enableAjaxValidation' => false,
            'htmlOptions' =>
            array('enctype' => 'multipart/form-data'),
        )
    );

    ?>

    <h2>Update statuses from file</h2>

    <?php


    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }


    echo $form->errorSummary($model);
    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'file'); ?>
        <?php echo $form->fileField($model, 'file'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'omitFirst'); ?>
        <?php echo $form->checkBox($model, 'omitFirst'); ?>
    </div>


    <div class="navigate">

        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('size' => TbHtml::BUTTON_SIZE_LARGE));
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p>Allowed file type: .csv,.xls,.xlsx.</p>
        <p>The file can contain following columns:<br/>
            [local_id],
            [status <?php echo TbHtml::tooltip('?','#','Can be status Id, status name, short name or full name in any language');?>],
            [date of status]
        </p>
    </div>
</div>
<?php echo TbHtml::link('Download file template', Yii::app()->baseUrl.'/misc/backend/usff_template.xlsx', array('target' => '_blank', 'class' => 'btn', 'download' => 'usff_template.xlsx'));?>
<br/>
<br/>