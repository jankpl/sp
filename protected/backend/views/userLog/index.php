<h2>User Log</h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-carrier-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        [
            'name' => '_user',
            'value' => '$data->user->login',
        ],
        [
            'name' => 'admin_id',
            'value' => '$data->admin->login',
            'filter' => CHtml::listData(Admin::model()->findAll(),'id', 'login'),
        ],
        'date_entered',
        [
            'name' => 'text',
            'value' => 'substr($data->text,0, 255)',
        ],
        [
            'name' => 'cat',
            'filter' => UserLog::getCatList(),
            'value' => '$data->getCatName()',
        ]
    ),
)); ?>