<?php
/* @var $pricingOptions PricingOptions[] */
/* @var $id Integer */
?>

<div class="form">

    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'pricing-optionse-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
    ));
    ?>
<?php

foreach(PricingOptions::getOptions()[$id] AS $optionGroupKey => $optionGroup)
{


    echo '<fieldset><legend>'.$optionGroup['name'].'</legend>';
?>
    <table class="listSpecial hTop" style="width: 700px;">
        <tr class="header">
            <td style="width: 150px;">Weight Top</td>

            <?php
            foreach($optionGroup['options'] AS $optionGroupItem)
            {
                echo '<td style="width: 150px;">'.$optionGroupItem['name'].' [zł]</td>';
            }
            ?>
        </tr>


    <?php

    //foreach($optionGroup['options'] AS $key => $option)
    {


        $this->renderPartial('_option_value', array(
            'model' => $pricingOptions[$optionGroupKey],
            'form' => $form,
            'option' => $option,
            'pricingOptionsHeader' => $pricingOptionsHeader[$optionGroupKey],
        ));
    }


    echo'
    </table>
    </fieldset>';
}



?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>


</div><!-- form -->

