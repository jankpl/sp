<?php

Yii::import('application.modules.courier.models._base.BaseCourierUPriceGroup');

class CourierUPriceGroup extends BaseCourierUPriceGroup
{

    public $_openedRevision;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @param $courier_u_price_group_id
     * @param null $user_group_id
     * @return CourierUPriceGroup|static[]
     */
    public static function findOrCreate($courier_u_price_group_id, $user_group_id = NULL)
    {

        $model = self::model()->findByAttributes([
            'courier_u_2_operator_country_group_id' => $courier_u_price_group_id,
            'user_group_id' => $user_group_id,
        ],
            [
                'order' => 'revision DESC'
            ]
        );

        if($model === NULL) {
            $model = new self();
            $model->courier_u_2_operator_country_group_id = $courier_u_price_group_id;
            $model->user_group_id = $user_group_id;
        }

        return $model;
    }

    public function rules() {
        return array(
            array('courier_u_2_operator_country_group_id', 'required'),
            array('courier_u_2_operator_country_group_id, user_group_id, revision', 'numerical', 'integerOnly'=>true),
            array('user_group_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, courier_u_2_operator_country_group_id, user_group_id, revision', 'safe', 'on'=>'search'),
        );
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    /**
     * @return CourierUPriceItem[]
     */
    public function getPriceItemsCurrent()
    {
        $models = CourierUPriceItem::model()->findAllByAttributes(['revision' => $this->revision, 'courier_u_price_group_id' => $this->id]);
        return $models;
    }

    /**
     * @param $courier_2_operator_country_group_id
     * @return CourierUPriceGroup
     */
    public static function getModelForOperator2CountryGroup($courier_2_operator_country_group_id)
    {
        $priceGroup = CourierUPriceGroup::model()->findByAttributes(['courier_u_2_operator_country_group_id' => $courier_2_operator_country_group_id, 'user_group_id' => NULL]);

        if($priceGroup === NULL)
        {
            $priceGroup = new self;
            $priceGroup->courier_u_2_operator_country_group_id = $courier_2_operator_country_group_id;
            $priceGroup->user_group_id = NULL;
            $priceGroup->save();

        }

        return $priceGroup;
    }

    /**
     * @param $courier_2_operator_country_group_id
     * @param $user_group_id
     * @param bool $createNew
     * @return CourierUPriceGroup
     */
    public static function getModelForOperator2CountryGroupForUserGroup($courier_2_operator_country_group_id, $user_group_id, $createNew = false)
    {
        $priceGroup = CourierUPriceGroup::model()->findByAttributes(['courier_u_2_operator_country_group_id' => $courier_2_operator_country_group_id, 'user_group_id' => $user_group_id]);

        if($priceGroup === NULL && $createNew)
        {
            $priceGroup = new self;
            $priceGroup->courier_u_2_operator_country_group_id = $courier_2_operator_country_group_id;
            $priceGroup->user_group_id = $user_group_id;
            $priceGroup->save();
        }

        return $priceGroup;
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->_openedRevision = $this->revision;
    }

    /**
     * @return CourierUPriceItem[]
     */
    public function getPriceItems($createNew = false)
    {
        $priceItems = $this->getPriceItemsCurrent();

        if(!S_Useful::sizeof($priceItems) && $createNew)
        {
            $temp = new CourierUPriceItem();
            $temp->courier_u_price_group_id = $this->id;
            $priceItems[] = $temp;
        }

        return $priceItems;
    }

    public function afterValidate()
    {
        if($this->_openedRevision != $this->revision)
        {
            $this->addError('revision', 'Invalid revision. Probably editied by someone else in the meantime! Please refresh page and edit data again.');
            $errors = true;
        }
    }

    public function saveWithChildren($startNewTransaction = false)
    {

        $errors = false;

        if($startNewTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $nextRevision = $this->revision + 1;

        if(!$errors) {

            if (!$this->save())
                $errors = true;
        }


        if(!$errors)
        {
            if(!S_Useful::sizeof($this->courierUPriceItems))
                $errors = true;

            foreach($this->courierUPriceItems AS $item)
            {
                $item->courier_u_price_group_id = $this->id;
                $item->revision = $nextRevision;
                if(!$item->saveWithChildren(false))
                    $errors = true;

            }
        }

        if(!$errors)
        {
            $this->revision = $nextRevision;
            if(!$this->update(['revision']))
                $errors = true;
        }

        if($errors)
        {
            if($startNewTransaction)
                $transaction->rollback();
        } else {
            if($startNewTransaction)
                $transaction->commit();
        }

        return $errors ? false : true;
    }
}