<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-stat-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>


    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']source_text'); ?>
        <?php echo $form->textArea($model, '['.$i.']source_text', ['readonly' => 'true', 'style' => 'width: 100%; height: 300px; resize: vertical;']); ?>
        <?php echo $form->error($model,'['.$i.']source_text'); ?>
    </div><!-- row -->

    <?php echo $form->errorSummary($model); ?>


    <?php
    foreach($modelTrs AS $item):
        echo $form->errorSummary($item);
    endforeach;
    ?>



    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):
            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';
        endforeach;
        ?>
    </div>


    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

            $model_tr = $modelTrs[$languageItem->id];

            $this->renderPartial('_tr_form', array(
                'model' => $model_tr,
                'form' => $form,
                'i' => $model_tr->language_id,
            ));

            echo'</div>';

        endforeach;
        ?>
    </div>
    <input type="button" id="copy-to-tr" value="Copy texts from `pl` to all emtpy languages versions" />

    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>

    <div class="text-center">
        <?php
        echo GxHtml::submitButton(Yii::t('app', 'Save'));
        ?>
    </div>
    <?php
    $this->endWidget();
    ?>
</div><!-- form -->
<script>
    $('#copy-to-tr').on('click', function(){
        var data = [];
        var setData = false;

        $('.langVersion').each(function(i,v){

            if(!data.length)
                setData = true;
            else
                setData = false;

            var j = 0;
            $(v).find('textarea').each(function(ii,vv) {

                if(setData)
                    data[j] = $(vv).val();
                else if($(vv).val() == '')
                    $(vv).val( data[j]);
                j++
            });

        });
    });
</script>
