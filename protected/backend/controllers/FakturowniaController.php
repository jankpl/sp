<?php

class FakturowniaController extends Controller
{

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
//                'expression'=>'0 >= Yii::app()->user->authority && in_array(Yii::app()->user->id, [2,5,16,3])',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new FakturowniaForm;

        if(isset(Yii::app()->session['fakturowniaByCourier']))
        {
            $data = Yii::app()->session['fakturowniaByCourier'];
            $model->addressData_receiver->attributes = $data['address_attributes'];
//            $model->sellDate = $data['date'];


            $model->saveToUserInvoice = $data['user_id'];

            $item = new FakturowniaItem();
            $item->name = 'Dystrybucja paczki #'.$data['local_id'];
            $item->value_netto = $data['value_netto'];
            $item->value_brutto = $data['value_brutto'];
            if($item->value_netto)
                $item->tax = 100 * (round($item->value_brutto / $item->value_netto,2) - 1);

            $model->items = [];
            $model->items[] = $item;

            $model->lang = $data['lang'];
            $model->currency = PriceCurrency::idByName($data['currency']);

            unset(Yii::app()->session['fakturowniaByCourier']);
        }
        else if(isset(Yii::app()->session['fakturowniaByPostal']))
        {
            $data = Yii::app()->session['fakturowniaByPostal'];
            $model->addressData_receiver->attributes = $data['address_attributes'];
//            $model->sellDate = $data['date'];

            $model->saveToUserInvoice = $data['user_id'];

            $item = new FakturowniaItem();
            $item->name = 'Dystrybucja przesyłki #'.$data['local_id'];
            $item->value_netto = $data['value_netto'];
            $item->value_brutto = $data['value_brutto'];
            if($item->value_netto)
                $item->tax = 100 * (round($item->value_brutto / $item->value_netto,2) - 1);

            $model->items = [];
            $model->items[] = $item;

            $model->lang = $data['lang'];
            $model->currency = PriceCurrency::idByName($data['currency']);

            unset(Yii::app()->session['fakturowniaByPostal']);
        }
        else if(isset(Yii::app()->session['fakturowniaByUserInvoice']))
        {
            $data = Yii::app()->session['fakturowniaByUserInvoice'];
            $model->addressData_receiver->attributes = $data['address_attributes'];

            $model->saveToUserInvoice = $data['user_id'];

            $item = new FakturowniaItem();
            $item->name = $data['title'];
            $item->value_brutto = $data['value_brutto'];

            $model->items = [];
            $model->items[] = $item;

            $model->lang = $data['lang'];
            $model->currency = $data['currency'];

            unset(Yii::app()->session['fakturowniaByUserInvoice']);
        }


        if(isset($_POST['FakturowniaForm'])) {
            $errors = false;
            $model->attributes = $_POST['FakturowniaForm'];


           $model->file_sheet = CUploadedFile::getInstance($model, 'file_sheet');

            $model->addressData_receiver->attributes = $_POST['AddressData_Order'];

            if (!$model->addressData_receiver->validate())
                $errors = true;



            if($model->correction)
            {
                $model->itemsBefore = [];
                $model->itemsAfter = [];
                if (is_array($_POST['FakturowniaItem']['before']))
                    foreach ($_POST['FakturowniaItem']['before'] AS $key => $postItem) {
                        $temp = new FakturowniaItem();
                        $temp->attributes = $postItem;

                        if (!$temp->validate())
                            $errors = true;

                        $model->itemsBefore[] = $temp;
                    }

                if (is_array($_POST['FakturowniaItem']['after']))
                    foreach ($_POST['FakturowniaItem']['after'] AS $key => $postItem) {
                        $temp = new FakturowniaItem();
                        $temp->attributes = $postItem;

                        if (!$temp->validate())
                            $errors = true;

                        $model->itemsAfter[] = $temp;
                    }



            } else {
                $model->items = [];
                if (is_array($_POST['FakturowniaItem']))
                    foreach ($_POST['FakturowniaItem'] AS $postItem) {
                        $temp = new FakturowniaItem();
                        $temp->attributes = $postItem;

                        if (!$temp->validate())
                            $errors = true;

                        $model->items[] = $temp;
                    }
            }





            if (isset($_POST['switch'])) {
                $model->correction = !$model->correction;

                $model->itemsBefore = $model->items;
                $model->items = [];

                foreach($model->itemsBefore AS $key => $temp)
                    $model->itemsAfter[$key] = new FakturowniaItem();
            }
            else {



                if (!$model->validate())
                    $errors = true;

                if (isset($_POST['add'])) {


                    if($model->correction)
                    {
                        $model->itemsBefore[] = new FakturowniaItem();
                        $model->itemsAfter[] = new FakturowniaItem();

                    } else {
                        $model->items[] = new FakturowniaItem();
                    }



                } else {

                    if (!$errors) {
                        $resp = Fakturownia::createByFakturowniaForm($model);

                        if ($resp['success']) {
                            if ($resp['userInoviceModel'])
                                $text = 'Invoice created and saved: <a href="' . Yii::app()->createUrl('/userInvoice/view', ['id' => $resp['userInoviceModel']->id]) . '" target="_blank" class="btn">view invoice (#' . $resp['userInoviceModel']->id . ')</a>';
                            else
                                $text = 'Invoice created: <a href="' . $resp['pdfLink'] . '" target="_blank" class="btn">download invoice (#' . $resp['no'] . ')</a>';

                            Yii::app()->user->setFlash('success', $text);
                            $this->refresh();
                        } else
                            Yii::app()->user->setFlash('error', 'Something went wrong: ' . $resp['error']);


                    }
                }

            }

        }
        $this->render('create',[
            'model' => $model,
        ]);
    }

    public function actionCreateByCourier($id, $userData = false)
    {

        Yii::app()->getModule('courier');

        /* @var $courier Courier */
        $courier = Courier::model()->findByPk($id);
        if($courier === NULL)
            throw new CHttpException(404);


        $data = [];
        $data['date'] = substr($courier->date_entered,0,10);
        $data['local_id'] = $courier->local_id;
        $data['value_netto'] = 0;
        $data['value_brutto'] = 0;
        $data['lang'] = $courier->user->getPrefLang();
        $data['currency'] = $courier->user->price_currency_id;

        $data['user_id'] = $courier->user_id;

        if($courier->courierTypeInternal OR $courier->courierTypeU)
        {
            $orderProduct = OrderProduct::getModelForProductItem(OrderProduct::TYPE_COURIER, $courier->id);
            $data['value_netto'] = $orderProduct->value_netto;
            $data['value_brutto'] = $orderProduct->value_brutto;
            $data['currency'] = $orderProduct->currency;
        }

        if($userData) {
            $data['address_attributes'] = $courier->user->getAddressDataOrderModel()->attributes;
            $data['address_attributes']['_nip'] = $courier->user->getAddressDataOrderModel()->_nip;
        }
        else
            $data['address_attributes'] = $courier->senderAddressData->attributes;

        Yii::app()->session['fakturowniaByCourier'] = $data;

        $this->redirect(['/fakturownia/create']);
    }

    public function actionCreateByPostal($id, $userData = false)
    {

        Yii::app()->getModule('postal');

        /* @var $courier Courier */
        $postal = Postal::model()->findByPk($id);
        if($postal === NULL)
            throw new CHttpException(404);


        $data = [];
        $data['date'] = substr($postal->date_entered,0,10);
        $data['local_id'] = $postal->local_id;
        $data['value_netto'] = 0;
        $data['value_brutto'] = 0;
        $data['lang'] = $postal->user->getPrefLang();
        $data['currency'] = $postal->user->price_currency_id;

        $data['user_id'] = $postal->user_id;

        $orderProduct = OrderProduct::getModelForProductItem(OrderProduct::TYPE_POSTAL, $postal->id);
        $data['value_netto'] = $orderProduct->value_netto;
        $data['value_brutto'] = $orderProduct->value_brutto;
        $data['currency'] = $orderProduct->currency;

        if($userData) {
            $data['address_attributes'] = $postal->user->getAddressDataOrderModel()->attributes;
            $data['address_attributes']['_nip'] = $postal->user->getAddressDataOrderModel()->_nip;
        }
        else if($postal->senderAddressData)
            $data['address_attributes'] = $postal->senderAddressData->attributes;

        Yii::app()->session['fakturowniaByPostal'] = $data;

        $this->redirect(['/fakturownia/create']);
    }


    public function actionCreateByUserInvoice($id)
    {

        /* @var $userInvoice UserInvoice */
        $userInvoice = UserInvoice::model()->findByPk($id);
        if($userInvoice === NULL)
            throw new CHttpException(404);


        $data = [];

        $data['title'] = $userInvoice->title;
        $data['value_netto'] = 0;
        $data['value_brutto'] = 0;
        $data['lang'] = $userInvoice->user->getPrefLang();
        $data['currency'] = Currency::nameById($userInvoice->inv_currency);

        $data['value_brutto'] = $userInvoice->inv_value;

        $data['address_attributes'] = $userInvoice->user->getAddressDataOrderModel()->attributes;
        $data['address_attributes']['_nip'] = $userInvoice->user->getAddressDataOrderModel()->_nip;


        $data['user_id'] = $userInvoice->user_id;

        Yii::app()->session['fakturowniaByUserInvoice'] = $data;

        $this->redirect(['/fakturownia/create']);
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->render('index', []);
    }



}

