<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    const WARNING_AFTER = 2;
    const BLOCK_AFTER = 5;
    const BLOCK_MINUTES = 60;

    private $_id;
    private $_salesman_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        // BRUTEFORCE PREVENTION
        $currentValue = Yii::app()->cache->get('b_loginTry_'.$this->username);
        $currentValue = intval($currentValue);
        if($currentValue >= self::BLOCK_AFTER)
        {
            Yii::app()->user->setFlash('loginInfo', 'To konto zostało zablokowane na '.self::BLOCK_MINUTES.' minut z powodu zbyt dużej ilości nieudanych logowań!');
            return false;
        }

        // check if login details exists in database
        $model=Admin::model()->findByAttributes(array('login'=>$this->username));  // here I use Email as user name which comes from database

        if($model===null)
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        else if($model->pass!== $model->generatePass($this->password)) // let we have base64_encode password in database
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else if($model->stat == S_Status::NOT_ACTIVE)
        {
            $this->errorCode = 50;
            $this->errorMessage = 'To konto zostało dezaktywowane';
        }
        else
        {

            $this->setState('name', $model->login);

            $this->_id = $model->id;
            $this->_salesman_id = $model->salesman_id;

            $this->errorCode=self::ERROR_NONE;
        }

        if($this->errorCode)
        {
            // BRUTEFORCE PREVENTION
            $currentValue = Yii::app()->cache->get('b_loginTry_'.$this->username);
            $currentValue = intval($currentValue);
            $currentValue++;
            Yii::app()->cache->set('b_loginTry_'.$this->username, $currentValue, 60* self::BLOCK_MINUTES); // keep one hour

            if($currentValue >= self::WARNING_AFTER)
                Yii::app()->user->setFlash('loginInfo', 'To już kolejna nieudana próba logowania. Po '.self::BLOCK_AFTER.' próbie konto zostanie zablokowane na '.self::BLOCK_MINUTES.' minut!');
        } else {
            Yii::app()->cache->set('b_loginTry_'.$this->username, 0, 60* self::BLOCK_MINUTES); // clear counter after successful login
        }

        // CHECK IF PASSWORD ISN'T TOO OLD
        {
            $now = new DateTime();
            $passwordDate = new DateTime($model->password_date);
            $interval = $now->diff($passwordDate);
            $passwordDays = $interval->days;
            if($passwordDays > 30)
                Yii::app()->session['forceChangePassword'] = true;
        }

        if(!$this->errorCode)
            $model->updateLoginTime();

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getSalesmanId()
    {
        return $this->_salesman_id;
    }

}