<?php

/**
 * Class myCheckBoxColumn
 * This class changes parent behaviour by not showing disabled checkboxes
 */
class MyCheckBoxColumn extends CCheckBoxColumn
{
    /**
     * Returns the data cell content.
     * This method renders a checkbox in the data cell.
     * @param integer $row the row number (zero-based)
     * @return string the data cell content.
     * @since 1.1.16
     */
    public function getDataCellContent($row)
    {
        $data=$this->grid->dataProvider->data[$row];

        if($this->disabled!==null && $this->evaluateExpression($this->disabled,array('data'=>$data,'row'=>$row)))
            return '';

        if($this->value!==null)
            $value=$this->evaluateExpression($this->value,array('data'=>$data,'row'=>$row));
        elseif($this->name!==null)
            $value=CHtml::value($data,$this->name);
        else
            $value=$this->grid->dataProvider->keys[$row];

        $checked = false;
        if($this->checked!==null)
            $checked=$this->evaluateExpression($this->checked,array('data'=>$data,'row'=>$row));

        $options=$this->checkBoxHtmlOptions;
        if($this->disabled!==null)
            $options['disabled']=$this->evaluateExpression($this->disabled,array('data'=>$data,'row'=>$row));

        $name=$options['name'];
        unset($options['name']);
        $options['value']=$value;
        $options['id']=$this->id.'_'.$row;
        return CHtml::checkBox($name,$checked,$options);
    }
}