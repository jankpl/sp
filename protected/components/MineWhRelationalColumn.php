<?php

Yii::import('yiiwheels.widgets.grid.WhRelationalColumn');


class MineWhRelationalColumn extends WhRelationalColumn
{
    public $columns_no = false;

    /**
     * Register script that will handle its behavior
     */
    public function registerClientScript()
    {

        $path =   Yii::getPathOfAlias('yiiwheels') . DIRECTORY_SEPARATOR . 'widgets'. DIRECTORY_SEPARATOR . 'grid'. DIRECTORY_SEPARATOR . 'assets';
        $assetsUrl = $this->getAssetsUrl($path);

        /** @var $cs CClientScript */
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($assetsUrl . '/css/bootstrap-relational.css');

        if ($this->afterAjaxUpdate !== null) {
            if ((!$this->afterAjaxUpdate instanceof CJavaScriptExpression) && strpos(
                    $this->afterAjaxUpdate,
                    'js:'
                ) !== 0
            )
                $this->afterAjaxUpdate = new CJavaScriptExpression($this->afterAjaxUpdate);
        } else
            $this->afterAjaxUpdate = 'js:$.noop';

        $this->ajaxErrorMessage = CHtml::encode($this->ajaxErrorMessage);
        $afterAjaxUpdate        = CJavaScript::encode($this->afterAjaxUpdate);
        $span                   = !$this->columns_no ? count($this->grid->columns) : $this->columns_no;
        $loadingPic             = CHtml::image(Yii::app()->yiiwheels->getAssetsUrl() . '/img/loading.gif');
        $cache                  = $this->cacheData ? 'true' : 'false';
        $data                   = !empty($this->submitData) && is_array(
            $this->submitData
        ) ? $this->submitData : 'js:{}';
        $data                   = CJavascript::encode($data);

        $js = <<<EOD
$(document).on('click','.{$this->cssClass}', function(){
	var span = $span;
	var that = $(this);
	var status = that.data('status');
	var rowid = that.data('rowid');
	var tr = $('#relatedinfo'+rowid);
	var parent = that.parents('tr').eq(0);
	var afterAjaxUpdate = {$afterAjaxUpdate};

	if (status && status=='on'){return}
	that.data('status','on');

	if (tr.length && !tr.is(':visible') && {$cache})
	{
		tr.slideDown();
		that.data('status','off');
		return;
	}else if (tr.length && tr.is(':visible'))
	{
		tr.slideUp();
		that.data('status','off');
		return;
	}
	if (tr.length)
	{
		tr.find('td').html('{$loadingPic}');
		if (!tr.is(':visible')){
			tr.slideDown();
		}
	}
	else
	{
		var td = $('<td/>').html('{$loadingPic}').attr({'colspan':$span});
		tr = $('<tr/>').prop({'id':'relatedinfo'+rowid}).append(td);
		/* we need to maintain zebra styles :) */
		var fake = $('<tr class="hide"/>').append($('<td/>').attr({'colspan':$span}));
		parent.after(tr);
		tr.after(fake);
	}
	var data = $.extend({$data}, {id:rowid});
	$.ajax({
		url: '{$this->url}',
		data: data,		
		success: function(data){
			tr.find('td').html(data);
			that.data('status','off');
			if ($.isFunction(afterAjaxUpdate))
			{
				afterAjaxUpdate(tr,rowid,data);
			}
		},
		error: function()
		{
			tr.find('td').html('{$this->ajaxErrorMessage}');
			that.data('status','off');
		}
	});
});
EOD;
        $cs->registerScript(__CLASS__ . '#' . $this->id, $js);
    }
}