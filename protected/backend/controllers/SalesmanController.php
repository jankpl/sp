<?php

class SalesmanController extends Controller {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Salesman'),
		));
	}

	public function actionCreate() {
		$model = new Salesman;


		if (isset($_POST['Salesman'])) {
			$model->setAttributes($_POST['Salesman']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Salesman');


		if (isset($_POST['Salesman'])) {
			$model->setAttributes($_POST['Salesman']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Salesman')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$this->redirect(['/salesman/admin']);
	}

	public function actionAdmin() {
		$model = new Salesman('search');
		$model->unsetAttributes();

		if (isset($_GET['Salesman']))
			$model->setAttributes($_GET['Salesman']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAjaxPrepareImage()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			try {
				$img = $_POST['img'];

				$img = explode(',', $img);

				if(!isset($img[1]))
					throw new Exception;

				$img = $img[1];

				$img = base64_decode($img);

				if($img == '')
					throw new Exception;

				$im = ImagickMine::newInstance();
				if(!$im->readImageBlob($img))
					throw new Exception;

				if ($im->getImageWidth() > Salesman::IMG_WIDTH)
					$im->scaleImage(Salesman::IMG_WIDTH, 0);

				if ($im->getImageHeight() > Salesman::IMG_HEIGHT)
					$im->scaleImage(0, Salesman::IMG_HEIGHT);


				$im->setFormat('png');

				$imgString = $im->__tostring();
				$imgString = base64_encode($imgString);
				$imgString = 'data:' . $im->getImageMimeType() . ';base64,' . $imgString;

				echo CJSON::encode(['image' => $imgString]);
			}
			catch(Exception $ex)
			{
				echo CJSON::encode(['image' => false]);
			}
		} else {
			$this->redirect(['/']);
		}
	}

	public function actionStat($id)
	{
		/* @var $model Salesman */


		$model = Salesman::model()->findByPk($id);

		if($model === NULL)
			throw new CHttpException(404,'Błędny ID!');

		if($model->toggleStat())
			$this->redirect(Yii::app()->request->urlReferrer);
		else
			throw new CHttpException(403,'Operacja nie powiodła się!');
	}

}