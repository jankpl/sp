<?php
/* @var $footer_info string */
/* @var $name string */
/* @var $url string */
?>
</div>
<div style="width: 100%; background: #9aacaf; color: white; font-size: 10px;"><div id="footer" style="width: 900px; padding: 15px 0 15px 25px;">
        <?= $footer_info != '' ? '<p>[ '.$footer_info.' ]</p>' : '';?>
        <p>[<?= date('Y-m-d h:m:s'); ?>]</p>
        <p><a href="<?= $url; ?>"><?= $name; ?></a></p>
    </div></div>
</body>
</html>