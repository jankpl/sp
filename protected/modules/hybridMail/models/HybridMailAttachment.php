<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailAttachment');

class HybridMailAttachment extends BaseHybridMailAttachment
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on'=>'update'),
            array('user_id', 'default',
                'value'=>Yii::app()->user->id, 'setOnEmpty' => true),

            array('name, path, type, file_hash, extension', 'required'),
            array('pages', 'numerical', 'integerOnly'=>true),
            array('size', 'numerical'),
            array('name, file_hash', 'length', 'max'=>64),
            array('path', 'length', 'max'=>256),
            array('type', 'length', 'max'=>256),
            array('status', 'length', 'max'=>8),
            array('hybrid_mail_id, user_id', 'length', 'max'=>10),
            array('size, pages, hybrid_mail_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, name, path, type, size, pages, status, hybrid_mail_id, file_hash, user_id', 'safe', 'on'=>'search'),
        );
    }


    public function getSizeHuman()
    {
        $size = $this->size;
        $size = $size / 1024;
        $size = $size / 1024;

        return number_format($size,2,',',' ').'MB';

    }


}