<?php

class PrintServerController extends Controller
{
    public function beforeAction($action)
    {
        if(Yii::app()->user->model->getPrintServerActive())
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('printServer', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Print Server',

            ));
            exit;
        }
    }

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }


    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }


    public function actionIndex($no = NULL)
    {
        $this->panelLeftMenuActive = 161;

        if($no === NULL)
        {
            $models = PrintServer::model()->findAll('user_id = :user_id AND stat != :stat_deleted AND stat != :stat_locked', [
                ':user_id' => Yii::app()->user->id,
                ':stat_deleted' => PrintServer::STAT_DELETED,
                ':stat_locked' => PrintServer::STAT_LOCKED,
            ]);

            $this->render('list',
                [
                    'models' => $models
                ]);
        } else {

            $model = PrintServer::model()->find('user_id = :user_id AND stat != :stat_deleted AND stat != :stat_locked AND no = :no', [
                ':user_id' => Yii::app()->user->id,
                ':stat_deleted' => PrintServer::STAT_DELETED,
                ':stat_locked' => PrintServer::STAT_LOCKED,
                ':no' => $no,
            ]);

            if ($no && $model === NULL)
            {
                $this->redirect(['/printServer']);
                Yii::app()->end();
            }
            else if($model === NULL)
                $model = new PrintServer;


            if (isset($_POST['PrintServer'])) {
                $model->setAttributes($_POST['PrintServer']);
                $model->user_id = Yii::app()->user->id;

                if ($model->save()) {
                    $model->sendVerification();
                    Yii::app()->user->setFlash('success', Yii::t('printServer', 'Kod zostanie przesłany na podany adres drukarki w ciągu kilku minut.'));
                    $this->refresh();
                }
            }

            if (isset($_POST['CheckVerification'])) {
                if ($model->verify($_POST['verification_code'])) {
                    $this->refresh();
                } else {
                    Yii::app()->user->setFlash('error', Yii::t('printServer', 'Kod nie jest poprawny.'));
                    $this->refresh();
                }
            }

            if (isset($_POST['Delete'])) {

                $model->delete();
                $this->refresh();
            }

            if (isset($_POST['Resend'])) {

                if($model->sendVerification()) {
                    Yii::app()->user->setFlash('success', Yii::t('printServer', 'Kod zostanie przesłany na podany adres drukarki w ciągu kilku minut.'));
                    $this->refresh();
                }
                else
                {
                    Yii::app()->user->setFlash('error', Yii::t('printServer', 'Zbyt duża liczba prób, adres IP został zablokowany!'));
                    $this->redirect(['/printServer']);
                }
            }

            $this->render('index',
                [
                    'model' => $model
                ]);
        }
    }

    public function actionNew()
    {

        if(!PrintServer::isPossibleToAddNew())
        {
            Yii::app()->user->setFlash('error', Yii::t('printServer', 'Maksymalna liczba Print Serwerów to:').' '.PrintServer::MAX_PRINT_SERVERS);
            $this->redirect(['/printServer']);
            Yii::app()->end();
        }


        $model = new PrintServer;

        if (isset($_POST['PrintServer'])) {
            $model->setAttributes($_POST['PrintServer']);
            $model->user_id = Yii::app()->user->id;

            if ($model->save()) {
                $model->sendVerification();
                Yii::app()->user->setFlash('success', Yii::t('printServer', 'Kod zostanie przesłany na podany adres drukarki w ciągu kilku minut.'));
                $this->redirect(['/printServer/index', 'no' => $model->no]);
                Yii::app()->end();
            }
        }
        $this->render('index',
            [
                'model' => $model
            ]);
    }

    public function actionDelete($no)
    {
        $model = PrintServer::model()->find('user_id = :user_id AND stat != :stat_deleted AND no = :no', [
            ':user_id' => Yii::app()->user->id,
            ':stat_deleted' => PrintServer::STAT_DELETED,
            ':no' => $no,
        ]);

        if($model === NULL)
            throw new CHttpException(404);
        else
        {
            $model->delete();
            $this->redirect(['/printServer']);
        }

    }

    public function actionToggle($no)
    {
        $model = PrintServer::model()->find('user_id = :user_id AND (stat = :stat_active OR stat = :stat_inactive) AND no = :no', [
            ':user_id' => Yii::app()->user->id,
            ':stat_active' => PrintServer::STAT_ACTIVE,
            ':stat_inactive' => PrintServer::STAT_DISABLED,
            ':no' => $no,

        ]);

        if($model === NULL)
            throw new CHttpException(404);
        else
        {
            $model->toggleStat();
            $this->redirect(['/printServer']);
        }
    }

    public function actionSetDefault($no)
    {
        $model = PrintServer::model()->find('user_id = :user_id AND (stat = :stat_active OR stat = :stat_inactive) AND no = :no', [
            ':user_id' => Yii::app()->user->id,
            ':stat_active' => PrintServer::STAT_ACTIVE,
            ':stat_inactive' => PrintServer::STAT_DISABLED,
            ':no' => $no,
        ]);

        if($model === NULL)
            throw new CHttpException(404);

        if($model->setAsDefault())
        {
            Yii::app()->user->setFlash('success', Yii::t('printServer', 'Print Server został ustawiony jako domyślny.'));
        } else {
            Yii::app()->user->setFlash('error', Yii::t('printServer', 'Opracja się nie udała'));
        }

        $this->redirect(['/printServer']);


    }


}
