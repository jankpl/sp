<?php

class AsendiaClient extends GlobalOperatorWithPostal
{
    const URL = GLOBAL_CONFIG::ASENDIA_URL;
    const USERNAME = GLOBAL_CONFIG::ASENDIA_USER;
    const PASSWORD = GLOBAL_CONFIG::ASENDIA_PASSWORD;

    const FTP = 'ftp.asendia.uk.com';
    const FTP_USER = 'GB18050001';
    const FTP_PASSWORD = '693b45e56c5c6fcb878ff2';
    const FTP_DIR = 'OUT';

    public $sender_country;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;
    public $package_content;
    public $package_value;
    public $value_currency;

    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $user_id;
    public $login;

    public $local_id;

    public $email_notification = false;

    public $service_code = false;

    public static function getAdditions(Courier $courier)
    {
//
        $CACHE_NAME = 'ASENDIA_ADDITIONS_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_ASENDIA_EMAIL_NOTIFICATION;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    protected function operatorUniqIdToServiceName($operator_uniqid)
    {
        switch($operator_uniqid)
        {
            case GLOBAL_BROKERS::OPERATOR_UNIQID_ASENDIA_COLISSIMO_ACCESS_FR_TRACKED:
                return 'COLA';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_ASENDIA_COLISSIMO_ACCESS_FR_SIGNAUTRE:
                return 'COLEOM';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_ASENDIA_COLISSIMO_PREMIUM_GOODS:
                return 'PTLP';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_ASENDIA_TRACKED_GOODS_PERSONAL:
                return 'FTGP';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_ASENDIA_DPD_EXPRESSPAK:
                return 'DPDEP';
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_ASENDIA_DPD_CLASSIC:
                return 'DPDEU';
                break;
            default:
                throw new Exception('Unknown Asendia service code!');
                break;
        }
    }

    public static function test($returnError = true)
    {
        $model = new self;

        $model->service_code = 'COLA';

        $model->package_size_l = 10;
        $model->package_size_w = 10;
        $model->package_size_d = 10;
        $model->package_weight = 1;

        $model->package_value = 1;
        $model->value_currency = 'EUR';

        $model->local_id = 2133214412;
        $model->package_content = 'test';

        $model->sender_country = 'PL';
        $model->service_code = 'PL';
//
//        $model->receiver_name = 'test';
//        $model->receiver_company = 'test';
//        $model->receiver_address1 = 'test 1';
//        $model->receiver_zip_code = '11-111';
//        $model->receiver_city = 'test';
//        $model->receiver_country = 'PL';
//        $model->receiver_tel = 123213213;
//        $model->receiver_email = 'dsadas@dsasd.pl';

        $model->login = 'dsad';

        $data = new stdClass();
        $data->InputParameters[] = new \SoapVar('<ShippingParameter><Shipment.RefNo>123312321</Shipment.RefNo></ShippingParameter>', XSD_ANYXML);
        $data = new \SoapVar('<PrintParcelRequest xmlns="http://xlogics.eu/blackbox"><InputParameters xmlns:i="http://www.w3.org/2001/XMLSchema-instance"> <ShippingParameter> <Name>Shipment.RefNo</Name> <Value>'.self::_cdata($model->local_id).'</Value> </ShippingParameter> <ShippingParameter> <Name>AA.Service</Name> <Value>'.$model->service_code.'</Value> </ShippingParameter> <ShippingParameter> <Name>Shipment.ReturnValue</Name> <Value>ShipmentIdentcode;AA.UniqueReference</Value> </ShippingParameter> <ShippingParameter> <Name>AA.Reference2</Name> <Value>'.self::_cdata($model->login).'</Value> </ShippingParameter> <ShippingParameter> <Name>AA.EmailAlert</Name> <Value>'.($model->email_notification ? 'TRUE' : 'FALSE').'</Value> </ShippingParameter> <ShippingParameter> <Name>AA.DocumentsOnly</Name> <Value>FALSE</Value> </ShippingParameter> <ShippingParameter> <Name>AA.ExportType</Name> <Value>Other</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.CompanyName</Name> <Value>'.self::_cdata($model->receiver_company).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Name1</Name> <Value>'.self::_cdata($model->receiver_name).'</Value> </ShippingParameter> <ShippingParameter><Name>Receiver.Name2</Name> <Value>'.self::_cdata($model->receiver_name).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Street</Name> <Value>'.self::_cdata(trim($model->receiver_address1)).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.AddressDetails</Name> <Value>'.self::_cdata($model->receiver_address2).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.City</Name> <Value>'.self::_cdata($model->receiver_city).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Postcode</Name> <Value>'.self::_cdata($model->receiver_zip_code).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Country</Name> <Value>'.self::_cdata($model->receiver_country).'</Value> </ShippingParameter><ShippingParameter> <Name>Receiver.Mobile</Name> <Value>'.self::_cdata($model->receiver_tel).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Email</Name> <Value>'.self::_cdata($model->receiver_email).'</Value> </ShippingParameter> <ShippingParameter> <Name>Parcel.Weight</Name> <Value>'.$model->package_weight.'</Value> </ShippingParameter> <ShippingParameter> <Name>Parcel.Length</Name> <Value>'.$model->package_size_l.'</Value> </ShippingParameter> <ShippingParameter> <Name>Parcel.Width</Name> <Value>'.$model->package_size_w.'</Value> </ShippingParameter> <ShippingParameter> <Name>Parcel.Height</Name> <Value>'.$model->package_size_d.'</Value> </ShippingParameter> <ShippingParameter><Name>ProductParcel</Name> <Value> <![CDATA[<Table> <Row> <ASLineItemNo>1</ASLineItemNo> <ASProductDesc>'.strip_tags($model->package_content).'</ASProductDesc> <ASUnitQuantity>1</ASUnitQuantity> <ASUnitValue>'.$model->package_value.'</ASUnitValue> <ASCurrency>'.$model->value_currency.'</ASCurrency> <ASUnitWeight>'.$model->package_weight.'</ASUnitWeight> <ASCountry>'.$model->sender_country.'</ASCountry> </Row></Table>]]> </Value> </ShippingParameter></InputParameters></PrintParcelRequest>', XSD_ANYXML);


        $resp = $model->_soapOperation('PrintParcel', $data);

        if ($resp->success == true) {
            $tt = $resp->data->ShippingParameter[0]->Value;
            $label = $resp->data->ShippingParameter[2]->Value;
            $label = base64_decode($label);

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImageBlob($label);

            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $tt);
            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }


    }

    protected function _createShipment($returnError = false)
    {
//        if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse('Not active!');
//            } else {
//                return false;
//            }


//        $data = new stdClass();
//        $data->InputParameters[] = new \SoapVar('<ShippingParameter><Shipment.RefNo>123312321</Shipment.RefNo></ShippingParameter>', XSD_ANYXML);
        $data = new \SoapVar('<PrintParcelRequest xmlns="http://xlogics.eu/blackbox"><InputParameters xmlns:i="http://www.w3.org/2001/XMLSchema-instance"> <ShippingParameter> <Name>Shipment.RefNo</Name> <Value>'.self::_cdata($this->local_id).'</Value> </ShippingParameter> <ShippingParameter> <Name>AA.Service</Name> <Value>'.$this->service_code.'</Value> </ShippingParameter> <ShippingParameter> <Name>Shipment.ReturnValue</Name> <Value>ShipmentIdentcode;AA.UniqueReference</Value> </ShippingParameter> <ShippingParameter> <Name>AA.Reference2</Name> <Value>'.self::_cdata($this->login).'</Value> </ShippingParameter> <ShippingParameter> <Name>AA.EmailAlert</Name> <Value>'.($this->email_notification ? 'TRUE' : 'FALSE').'</Value> </ShippingParameter> <ShippingParameter> <Name>AA.DocumentsOnly</Name> <Value>FALSE</Value> </ShippingParameter> <ShippingParameter> <Name>AA.ExportType</Name> <Value>Other</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.CompanyName</Name> <Value>'.self::_cdata($this->receiver_company).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Name1</Name> <Value>'.self::_cdata($this->receiver_name).'</Value> </ShippingParameter> <ShippingParameter><Name>Receiver.Name2</Name> <Value>'.self::_cdata($this->receiver_name).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Street</Name> <Value>'.self::_cdata(trim($this->receiver_address1)).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.AddressDetails</Name> <Value>'.self::_cdata($this->receiver_address2).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.City</Name> <Value>'.self::_cdata($this->receiver_city).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Postcode</Name> <Value>'.self::_cdata($this->receiver_zip_code).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Country</Name> <Value>'.self::_cdata($this->receiver_country).'</Value> </ShippingParameter><ShippingParameter> <Name>Receiver.Mobile</Name> <Value>'.self::_cdata($this->receiver_tel).'</Value> </ShippingParameter> <ShippingParameter> <Name>Receiver.Email</Name> <Value>'.self::_cdata($this->receiver_email).'</Value> </ShippingParameter> <ShippingParameter> <Name>Parcel.Weight</Name> <Value>'.$this->package_weight.'</Value> </ShippingParameter> <ShippingParameter> <Name>Parcel.Length</Name> <Value>'.$this->package_size_l.'</Value> </ShippingParameter> <ShippingParameter> <Name>Parcel.Width</Name> <Value>'.$this->package_size_w.'</Value> </ShippingParameter> <ShippingParameter> <Name>Parcel.Height</Name> <Value>'.$this->package_size_d.'</Value> </ShippingParameter> <ShippingParameter><Name>ProductParcel</Name> <Value> <![CDATA[<Table> <Row> <ASLineItemNo>1</ASLineItemNo> <ASProductDesc>'.strip_tags($this->package_content).'</ASProductDesc> <ASUnitQuantity>1</ASUnitQuantity> <ASUnitValue>'.$this->package_value.'</ASUnitValue> <ASCurrency>'.$this->value_currency.'</ASCurrency> <ASUnitWeight>'.$this->package_weight.'</ASUnitWeight> <ASCountry>'.$this->sender_country.'</ASCountry> </Row></Table>]]> </Value> </ShippingParameter></InputParameters></PrintParcelRequest>', XSD_ANYXML);

        $resp = $this->_soapOperation('PrintParcel', $data);

        if ($resp->success == true) {
            $tt = $resp->data->ShippingParameter[0]->Value;
            $label = $resp->data->ShippingParameter[2]->Value;
            $label = base64_decode($label);

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImageBlob($label);

            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $tt);
            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }
    }

    protected function _soapOperation($method, $data)
    {
        $mode = [
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context' => stream_context_create(
                [
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false
                    ],
                ]
            )
        ];

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        try {

            $client = new SoapClient(self::URL.'?wsdl', $mode);

            $wsse_header = new WsseAuthHeaderAsendia(self::USERNAME, self::PASSWORD);
            $client->__setSoapHeaders(array($wsse_header));
            $resp = $client->$method($data);


        } catch (Exception $E) {
            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('asendia.txt', print_r($E->getMessage(),1));
            MyDump::dump('asendia.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('asendia.txt', print_r($client->__getLastResponse(),1));
            return $return;
        }

        MyDump::dump('asendia.txt', print_r($client->__getLastRequest(),1));
        MyDump::dump('asendia.txt', print_r($client->__getLastResponse(),1));

        if($resp->ExitStatus->Status == 'Success')
        {
            $return->success = true;
            $return->data = $resp->OutputParameters;
        } else {

            if(is_array($resp->ExitStatus->StatusDetails->StatusDetail)) {

                $error = [];
                foreach ($resp->ExitStatus->StatusDetails->StatusDetail AS $temp)
                    $error[] = (string) $temp->Message;

                $error = implode('; ', $error);
            }
            else
                $error = (string) $resp->ExitStatus->StatusDetails->StatusDetail->Message;

            $return->error = $error;
            $return->errorCode = $error;
            $return->errorDesc = $error;
        }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnError)
    {
        $model = new self;

        $model->service_code = $model->operatorUniqIdToServiceName($uniq_id);

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_ASENDIA_EMAIL_NOTIFICATION))
            $model->email_notification = true;

        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->package_content = $courierInternal->courier->package_content;
        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_value = $courierInternal->courier->getPackageValueConverted('EUR');
        $model->value_currency = 'EUR';

        $model->local_id = $courierInternal->courier->local_id;

        // @ 19.02.2019 - special rule
        if(in_array($courierInternal->courier->user_id, [2537]))
            $model->local_id = $courierInternal->courier->package_content;

        $model->user_id = $courierInternal->courier->user_id;
        $model->login = $courierInternal->courier->user->login;

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, CourierLabelNew $cln, $returnError = false)
    {
        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model = new self;

        $model->service_code = $model->operatorUniqIdToServiceName($courierTypeU->courierUOperator->uniq_id);

        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_ASENDIA_EMAIL_NOTIFICATION))
            $model->email_notification = true;

        $model->sender_country = $from->country0->code;


        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = $courierTypeU->courier->getWeight(true);
        $model->package_content = $courierTypeU->courier->package_content;
        $model->package_size_l = $courierTypeU->courier->package_size_l;
        $model->package_size_d = $courierTypeU->courier->package_size_d;
        $model->package_size_w = $courierTypeU->courier->package_size_w;

        $model->package_value = $courierTypeU->courier->getPackageValueConverted('EUR');
        $model->value_currency = 'EUR';

        $model->local_id = $courierTypeU->courier->local_id;

        $model->user_id = $courierTypeU->courier->user_id;
        $model->login = $courierTypeU->courier->user->login;

        // @ 19.02.2019 - special rule
        if(in_array($courierTypeU->courier->user_id, [2537]))
            $model->local_id = $courierTypeU->courier->package_content;

        return $model->_createShipment($returnError);
    }

    public static function orderForPostal(Postal $postal, $returnError = false)
    {
        $to = $postal->receiverAddressData;
        $from = $postal->senderAddressData;

        $model = new self;

        $model->service_code = $model->operatorUniqIdToServiceName($postal->postalOperator->uniq_id);

        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->package_weight = number_format($postal->weight / 1000, 2);
        $model->package_content = $postal->content;

        $model->package_value = $postal->getValueConverted('EUR');
        $model->value_currency = 'EUR';

        $model->local_id = $postal->local_id;

        $model->user_id = $postal->user_id;
        $model->login = $postal->user->login;

        // @ 19.02.2019 - special rule
        if(in_array($postal->user_id, [2537]))
            $model->local_id = $postal->content;


        return $model->_createShipment($returnError);
    }

    public static function trackForPostal($no)
    {
        self::updateTtData();
        return false;
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        self::updateTtData();
        return false;
    }


    protected static function updateTtData($justReturnData = false)
    {

        $CACHE_NAME = 'ASENDIA_UPDATE_TT_FTP';

        MyDump::dump('asendia_tt_log.txt', 'START');

        if (!$justReturnData) {
//            // prevent running mechanism too often
            $flag = Yii::app()->cache->get($CACHE_NAME);
            if ($flag) {
                MyDump::dump('asendia_tt_log.txt', 'LOCKED!');
                return true;
            }
//
            Yii::app()->cache->set($CACHE_NAME, true, 60 * 15);
        }

        MyDump::dump('asendia_tt_log.txt', ' FTP...');

        $conn_id = ftp_connect(self::FTP);

        if (!ftp_login($conn_id, self::FTP_USER, self::FTP_PASSWORD)) {
            Yii::log('ASENDIA TT FTP LOGIN ERROR!', CLogger::LEVEL_ERROR);
            return false;
        }

        $contents = ftp_nlist($conn_id, self::FTP_DIR);

        if (!S_Useful::sizeof($contents)) {
            Yii::log('ASENDIA TT FTP FILES NOT FOUND!', CLogger::LEVEL_ERROR);
            return false;
        }

        $ttData = [];
        if(is_array($contents))
            foreach($contents AS $file)
            {
                $array = explode('.', $file);
                $extension = end($array);

                // files with TT data starts with "cdt"
                if(strcasecmp($extension, 'csv') == 0)
                {
                    if(!TtFileProcessingLog::isProcessed(GLOBAL_BROKERS::BROKER_ASENDIA, $file))
                    {
                        // new file found - lock for 3 hours
                        Yii::app()->cache->set($CACHE_NAME, true, 60 * 60 * 3);//
                        MyDump::dump('asendia_tt_log.txt', ' FOUND NEW FILE: '.$file);

                        ob_start();
                        $result = ftp_get($conn_id, "php://output", $file, FTP_BINARY);
                        $data = ob_get_contents();
                        ob_end_clean();


                        $data = explode(PHP_EOL, $data);
                        array_shift($data);
                        array_pop($data);

                        foreach($data AS $item)
                        {
                            $item = explode('","', $item);
//
                            $ttNo = str_replace('"', '', $item[2]);
                            $ttStatus = str_replace('"', '', $item[6]);
                            $ttDate = str_replace('"', '', $item[5]);
                            $location = '';

                            if($ttNo == '')
                                continue;

                            $ttDate = substr($ttDate,6,4).'-'.substr($ttDate,3,2).'-'.substr($ttDate,0,2).' '.substr($ttDate,11,8);

                            if(preg_match('/Delivered, signed for by (.*)./i', $ttStatus, $matches)) {
                                $ttStatus = 'Delivered';
                                $location = '(signed for by: '.trim($matches[1]).')';
                            }
                            else if(preg_match('/Redelivery request made for (.*)/i', $ttStatus, $matches)) {
                                $ttStatus = 'Redelivery request';
                                $location = trim($matches[1]);
                            }


                            if(!isset($ttData[$ttNo]))
                                $ttData[$ttNo] = [];

                            $ttData[$ttNo][] = [
                                'name' => $ttStatus,
                                'date' => $ttDate,
                                'location' => $location,
                            ];
                        }
//
                        if(!$justReturnData)
                            TtFileProcessingLog::markAsProcessed(GLOBAL_BROKERS::BROKER_ASENDIA, $file);
                    }
                }
            }

//        file_put_contents('glsnl_tt_log.txt', date('Y-m-d H:i:s').' DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData)."\r\n", FILE_APPEND);
        MyDump::dump('asendia_tt_log.txt', 'DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData));

        if($justReturnData)
            return $ttData;

        Yii::app()->getModule('courier');
        Yii::app()->getModule('postal');
        MyDump::dump('asendia_tt_log.txt', print_r($ttData,1));
        foreach($ttData AS $ttNumber => $ttDataForItem)
        {
            $ttNumber = (string) $ttNumber;
            if(substr($ttNumber,0,3) != '483')
                continue;
            //searchForItem
            $couriers = [ Courier::model()->findByAttributes(['local_id' => $ttNumber]) ];
//
//
            if(S_Useful::sizeof($couriers))
                unset($ttData[$ttNumber]); // orevebt searching again in postal

            /* @var $courier Courier */
            foreach($couriers AS $courier)
            {
                MyDump::dump('asendia_tt_log.txt', 'PROCESSED COURIER #'.$courier->id);
                if($courier->isStatChangeByTtAllowed())
                {
                    $temp = [];
                    CourierExternalManager::processUpdateDate($courier, $ttDataForItem, false, GLOBAL_BROKERS::BROKER_ASENDIA, $temp);
                }
            }
        }

        foreach($ttData AS $ttNumber => $ttDataForItem)
        {
            //searchForItem
            if(substr($ttNumber,0,3) != '419')
                continue;

            $postals = [ Postal::model()->findByAttributes(['local_id' => $ttNumber]) ];

            /* @var $postal Postal */
            foreach($postals AS $postal)
            {
                MyDump::dump('asendia_tt_log.txt', 'PROCESSED POSTAL #'.$postal->id);
                if($postal->isStatChangeByTtAllowed())
                {
                    PostalExternalManager::processUpdateDate($postal, $ttDataForItem, false, GLOBAL_BROKERS::BROKER_ASENDIA);
                }
            }
        }

    }


}

class WsseAuthHeaderAsendia extends SoapHeader {

    private $wss_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    function __construct($user, $pass, $ns = null) {
        if ($ns) {
            $this->wss_ns = $ns;
        }

        $auth = new stdClass();
        $auth->Username = new SoapVar($user, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
        $auth->Password = new SoapVar($pass, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);

        $username_token = new stdClass();
        $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns);

        $security_sv = new SoapVar(
            new SoapVar($username_token, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns),
            SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'Security', $this->wss_ns);
        parent::__construct($this->wss_ns, 'Security', $security_sv, true);
    }
}



