<?php

class AdminController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('manageOwn'),
                'users'=>array('@'),
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority && AdminRestrictions::isSuperAdmin()',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Admin'),
        ));
    }

    public function actionCreate() {
        $model = new Admin;

        $this->performAjaxValidation($model, 'admin-form');

        if (isset($_POST['Admin'])) {
            $model->setAttributes($_POST['Admin']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id, 'Admin');
        $model->scenario = 'update';
        $model->pass = '';

        $this->performAjaxValidation($model, 'admin-form');

        if (isset($_POST['Admin'])) {
            $model->setAttributes($_POST['Admin']);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'Admin')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin/admin'));
    }

    public function actionAdmin() {

        $model = new Admin('search');
        $model->unsetAttributes();

        if (isset($_GET['Admin']))
            $model->setAttributes($_GET['Admin']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionManageOwn()
    {

        $model = Admin::model()->findByAttributes(array('id' => Yii::app()->user->id));

        $model->scenario = 'update-own';
        $model->pass = '';

        $this->performAjaxValidation($model, 'admin-form');

        if (isset($_POST['Admin'])) {
            $model->setAttributes($_POST['Admin']);

            if ($model->save()) {
                Yii::app()->session['forceChangePassword'] = false;
                Yii::app()->user->setFlash('success', 'Changes saved!');
                unset(Yii::app()->session['forceChangePassword']);
                $this->redirect(['/']);
            }
        }

        $this->render('manageOwn', array(
            'model' => $model,
        ));

    }

    public function actionStat($id)
    {
        /* @var $model CountryList */


        $model = Admin::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

    public function actionRemoveLock($username)
    {
        Yii::app()->cache->set('b_loginTry_'.$username, 0);
    }

}