<?php
if($model):
    echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, 'Package #' . $model->local_id, array('closeText' => false));
    ?>

    <?php
else:
  echo '<div class="alert alert-danger">Package not found!</div>';
endif;
?>
