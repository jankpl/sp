<?php

abstract class GlobalOperatorWithPostal extends GlobalOperator
{
    /**
     * @param CourierTypeReturn $courierReturn
     * @param CourierLabelNew $courierLabelNew
     * @param $returnErrors
     * @return GlobalOperatorResponse
     */
    abstract public static function orderForPostal(Postal $postal, $returnErrors = false);

    abstract public static function trackForPostal($ttNo);
}