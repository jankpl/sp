(function($) {

    var ignoreFirstUpdate = true;

    var viewTimeoutHandler;
    var formActive = true;

    var senderCountryFullCache;


    $(document).ready(function(){

        $('#Courier_CourierTypeInternal_cod_value').on('change', function(){
            var codCurName = $('#cod-currency-name').text();
            var valCurName = $('#Courier_CourierTypeInternal_value_currency').val();

            var $val = $('#Courier_CourierTypeInternal_package_value');

            if(codCurName == valCurName && $val.val() == '')
                $val.val($(this).val());

        });

        checkAutomaticReturnAvailable();
        // setAutomaticReturn();

        var data = $('#country_label_sender').attr('data-content');
        senderCountryFullCache = JSON.parse(data);

        $('[data-copy-bank-account]').on('click', function(){
            $('[data-bank-account-target]').val($(this).attr('data-copy-bank-account'))
        });

        $('#with-pickup').focus();

        $('#Courier_CourierTypeInternal_package_weight').on('change', updateView);

        $('#show-requirements').on('click', toggleRequirements);

        $('[data-package-wrapping]').on('change', wrapping2Addition);

        prepareAdditions();

        // $('[name="country_suggester_trigger"]').on('change', isCodAvailable);

        // isCodAvailable();

        $('#selected-currency').on('change', currencyChanged);


        if($('[data-address-more]').find('input[type=text]').val()!='')
            $('[data-address-more]').show();

        $('[data-show-address-more]').on('click', showAddressMore);

        //$('#selected-currency').on('change', function(){
        //    updateView(true);
        //});

        $("[data-dimensions]").TouchSpin({
            step: 1,
            decimals: 0,
            max: 330,
            min: 1,
            boostat: 5,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });

        $("[data-weight]").TouchSpin({
            step: 0.01,
            decimals: 2,
            boostat: 5,
            max: $(this).attr('data-max-weight'),
            min: 0.01,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });

        $("[data-weight-total]").TouchSpin({
            step: 0.01,
            decimals: 2,
            boostat: 5,
            max: ($(this).attr('data-max-weight') * $(this).attr('data-max-no')),
            min: 0.01,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });

        $('.bootstrap-touchspin-down').attr('tabindex', -1);
        $('.bootstrap-touchspin-up').attr('tabindex', -1);

        $('[data-address-line-2]').on('change', function(){
            $(this).closest('[data-address-more]').slideDown();
            $(this).closest('.row').prev().find('[data-show-address-more]').hide();
        });

        $('#courier-internal-form').on('submit', function(e)
        {
            if(!formActive) {
                e.preventDefault();
                $('.overlay').remove();
                return false;
            }
        });

        $('#_client_cod').checkboxpicker({
            offClass : 'btn-default btn',
            onClass : 'btn-primary btn',
            defaultClass: 'btn',
        });

        $('#_client_cod').on('change', codChanged);


        $('[data-bootstrap-checkbox]').checkboxpicker({
            offClass : 'btn-default btn-lg',
            onClass : 'btn-primary btn-lg',
            defaultClass: 'btn-lg',
        });

        $('[data-pickup-alert]').hide();
        $('[data-pickup-alert="' + $("#with-pickup").val() + '"]').show();

        if($("#with-pickup").val() == yii.pickUp.WITH_PICKUP_REGULAR || $("#with-pickup").val()  == yii.pickUp.WITH_PICKUP_CONTRACT)
        {
            $('[data-pickup-alert="-1"]').hide();
        } else {
            $('[data-pickup-alert="-1"]').show();
        }

        $('[data-sp-points-selector]').on('change', updateSpPoints);


        if($('[data-sp-points-selector]').children('option').length == 1) {
            $('[data-sp-points-selector]').trigger('change');
        }

        $('#Courier_CourierTypeInternal_packages_number, #Courier_CourierTypeInternal__package_weight_total, #Courier_CourierTypeInternal_package_weight').on('change', checkWeight);
        $('#Courier_CourierTypeInternal_package_size_l, #Courier_CourierTypeInternal_package_size_w, #Courier_CourierTypeInternal_package_size_d').on('change', calculateDimensionaWeight);



        if($("#with-pickup").val() == yii.pickUp.WITH_PICKUP_RUCH) {

            if($('#CourierTypeInternal_AddressData_receiver_country_id').val() == 1)
                $('#ruch-receiver-part').show();
            else
                $('#ruch-receiver-part').hide();

        }
        else {
            $('#userAddressList_receiver').attr('data-countries', '[]');
            $('#ruch-receiver-part').hide();
        }



        $("#with-pickup").on('change', function(){


            if($("#with-pickup").val() == yii.pickUp.WITH_PICKUP_REGULAR || $("#with-pickup").val()  == yii.pickUp.WITH_PICKUP_CONTRACT)
            {
                $('[data-pickup-alert="-1"]').hide();
            } else {
                $('[data-pickup-alert="-1"]').show();
            }

            $('[data-pickup-alert]').not('[data-pickup-alert="-1"]').hide();
            $('[data-pickup-alert=' + $("#with-pickup").val() + ']').show();

            if($(this).val() == yii.pickUp.WITH_PICKUP_RUCH) {

                var data_pl = senderCountryFullCache[1];

                data_pl = {1 : data_pl};

                $('#country_label_sender').unbind();
                bindAutocomplete_cl( $('#country_label_sender'), data_pl);


                $('#userAddressList_sender').attr('data-countries', '[1]');
                $('[data-real-val="[sender]country_id_val"]').val('Poland').autocomplete("search");


                if($('#CourierTypeInternal_AddressData_receiver_country_id').val() == 1)
                    $('#ruch-receiver-part').show();
                else
                    $('#ruch-receiver-part').hide();


            }
            else if($(this).val() == yii.pickUp.WITH_PICKUP_LP_EXPRESS) {

                var data_lt = senderCountryFullCache[115]; // 115 = LT

                data_lt = {115 : data_lt};

                console.log(data_lt);


                $('#country_label_sender').unbind();
                bindAutocomplete_cl( $('#country_label_sender'), data_lt);


                $('#userAddressList_sender').attr('data-countries', '[115]');
                $('[data-real-val="[sender]country_id_val"]').val('Lithuania').autocomplete("search");

                $('#ruch-receiver-part').hide();

            }
            else
            {

                $('#country_label_sender').unbind();
                bindAutocomplete_cl( $('#country_label_sender'), senderCountryFullCache);

                $('#userAddressList_sender').attr('data-countries', '[]');

                $('#ruch-receiver-part').hide();
            }

            updateView();

        });

        $('[name="country_suggester_trigger"]').on('change', updateView);

        $('[data-dependency]').on('change', updateView);

        calculateDimensionaWeight();

        checkWeight();

        $('[data-packages-no]').on('change', updatePackagesNo);

        updatePackagesNo();

        $('[data-load-modal]').on('click', function(){
            loadModalContent($(this).attr('data-load-modal'));
        });
    });

    var FormControl = (function() {
        "use strict";

        var that = {};

        that.submit = function () {
            var $form = $("#courier-internal-form");

            var $input = $('<input>');
            $input.attr('name', 'forceReload');
            $form.append($input);

            formActive = true;
            $form.submit();
        };
        return that;
    }());

    var Overlay = (function() {
        "use strict";

        var that = {};
        var $overlay = $("<div>");
        var $textBottom = $("<div>");
        var $textTop = $("<div>");
        var $loader = $("<div>");

        that.isShow = function(){
            return $("#overlay").is(':visible');
        };

        that.show = function() {
            var $form = $("#courier-internal-form");

            $overlay.addClass('overlay');


            $loader.addClass('loader');

            $overlay.append($loader);


            $textTop.addClass('loader-text-top');
            $textTop.html(yii.loader.textTop);


            $textBottom.addClass('loader-text-bottom');
            $textBottom.html(yii.loader.textBottom);

            $overlay.append($textTop);
            $overlay.append($textBottom);

            $form.css('position','relative');
            $form.append($overlay);

            $textTop.hide().delay(1000).fadeIn();
            $textBottom.hide().delay(3000).fadeIn();
        };

        that.hide = function() {
            $overlay.remove();
        };

        return that;
    }());


    function wrapping2Addition()
    {
        var notifyShown = false;

        if($('[data-package-wrapping]').val() != 'box')
        {
            var nonStandardAdditions = yii.ids.nonStandardAdditions;
            for (index = 0; index < nonStandardAdditions.length; ++index) {

                var item = nonStandardAdditions[index];

                if(!$('[data-addition-id=' + item + ']').parent().find('a[is=' + item + ']').hasClass('active')) {

                    if(!notifyShown)
                        $.notify({
                            icon: 'glyphicon glyphicon-flag',
                            message: yii.message.customWrapping,
                            type: 'warning',
                        },{
                            allow_dismiss: false,
                            showProgressbar: true,
                        });

                    notifyShown = true;

                    $('[data-addition-id=' + item + ']').trigger('change').attr('checked', true);
                }
            }

        }
    }

    function toggleRequirements(){

        var $requirements = $('#requirements');

        if($requirements.is(':visible'))
            $requirements.slideUp();
        else
            $requirements.slideDown();

    }

    function codChanged()
    {
        if($(this).is(':checked'))
            $('#_client_cod_value_row').show();
        else
            $('#_client_cod_value_row').hide();

    }

    // function isCodAvailable()
    // {
    //
    //     if($('[data-country="sender"]').val() == 1 && $('[data-country="receiver"]').val() == 1) {
    //         $('[data-cod]').show();
    //
    //     }
    //     else {
    //         $('[data-cod]').hide();
    //
    //     }
    // }

    function getCod()
    {
        var receiver_country_id = $('[data-country="receiver"]').val();
        var sender_country_id = $('[data-country="sender"]').val();
        var weight = $('[data-package-weight]').val();

        $.ajax({
            url: yii.urls.getCod,
            data: { receiver_country_id: receiver_country_id, weight: weight, sender_country_id : sender_country_id},
            method: 'POST',
            dataType: 'json',
            success: function(result){

                if(result.available)
                {
                    $('[data-cod="false"]').hide();
                    $('[data-cod="true"]').show();
                    $('#cod-currency-name').html(result.currencyName);
                } else {
                    $('[data-cod="true"]').hide();
                    $('[data-cod="false"]').show();
                    $('#cod-currency-name').html('-');
                }

            },
            error: function(e){
                //alert(yii.loader.error);
            }
        });
    }

    function prepareAdditions()
    {
        checkboxAddtions();

        $("[data-group]").on("change", additionsGroup);

        $(".addition_item").children().not(".with-checkbox").css("cursor", "pointer");
        $(".addition_item").children().not(".with-checkbox").on('click', additionsClickableAll);
    }

    function additionsClickableAll()
    {
        var $checkBox = $(this).parent().find("input[type=checkbox]");
        $checkBox.prop("checked", !$checkBox.prop("checked"));
    }

    function additionsGroup()
    {

        if($(this).attr("data-no-not-trigger"))
        {
            $(this).removeAttr("data-no-not-trigger");
        } else {
            var dataGroup = $(this).attr("data-group");
            if(dataGroup)
            {
                $(this).attr("data-no-not-trigger", true);
                $("[data-group=" + dataGroup + "]").not($(this)).prop("checked", false);

            }
        }
    }

    function checkboxAddtions()
    {
        $('[data-courier-addition]').checkboxpicker({
            offClass : 'btn-default btn-sm',
            onClass : 'btn-primary btn-sm',
            defaultClass: 'btn-sm',
        });
        $('[data-courier-addition][data-group]:checked').attr("data-no-not-trigger", true);
    }

    function disableButton($button)
    {
        $button.prop('disabled', true);
        $button.addClass('disabled');
    }

    function enableButton($button)
    {
        $button.prop('disabled', false);
        $button.removeClass('disabled');
    }

    function showAddressMore()
    {
        $(this).parent().parent().parent().find('[data-address-more]').slideDown();
        $(this).remove();
    }

    function updateSpPoints()
    {

        $.ajax({
            url: yii.urls.spPointsDetails,
            data: { id: $('[data-sp-points-selector]').val()},
            method: 'POST',
            dataType: 'json',
            success: function(result){
                $("#sp-points-details").html(result);
            },
            error: function(e){
                //alert(yii.loader.error);
                FormControl.submit();
            }
        })
    }

    function currencyChanged()
    {
        updateView(true);
    }

    function updateView(force) {
        if (ignoreFirstUpdate) {
            ignoreFirstUpdate = false;
            return true;
        }

        if (!force || force === undefined || force.target) {

            if ($("input[data-dependency]").filter(function () {
                return $.trim($(this).val()).length == 0
            }).length > 0)
                return true; // not all required fileds are filled
        }

        $('[data-continue]').hide();

        disableButton($('[data-continue-button]'));

        clearTimeout(viewTimeoutHandler);
        viewTimeoutHandler = setTimeout(_updateView, 500);
    }

    function _updateView()
    {
        Overlay.show();

        checkAutomaticReturnAvailable();
        setAutomaticReturn();

        var withPickup = $("#with-pickup").val();
        if(withPickup == yii.pickUp.WITH_PICKUP_REGULAR)
            checkIfPickupAvailable();
        else
            checkIfCarriageAvailabe();

    }

    function checkIfPickupAvailable()
    {
        var sender_country_id = $('[data-country="sender"]').val();
        var weight = $('[data-package-weight]').val();
        var size_l = $('#Courier_CourierTypeInternal_package_size_l').val();
        var size_w = $('#Courier_CourierTypeInternal_package_size_w').val();
        var size_d = $('#Courier_CourierTypeInternal_package_size_d').val();

        $.ajax({
            url: yii.urls.checkIfPickupAvailable,
            data: { sender_country_id: sender_country_id, weight: weight, size_l : size_l, size_w : size_w, size_d : size_d},
            method: 'POST',
            dataType: 'json',
            success: function(result){

                if(result)
                {
                    $('[data-pickup-not-available]').hide();
                    checkIfCarriageAvailabe();
                } else {
                    $('[data-pickup-not-available]').show();
                    $('[data-continue]').hide();
                    disableButton($('[data-continue-button]'));
                    formActive = false;
                    Overlay.hide();
                }


            },
            error: function(e){
                //alert(yii.loader.error);
                FormControl.submit();
            }
        });
    }

    var modalLoaded = false;
    function loadModalContent(broker) {

        if(modalLoaded == broker)
            $('#modal').modal();
        else {
            Overlay.show();
            $.ajax({
                method: "POST",
                url: yii.urls.loadModalContent,
                data: {broker: broker},
                dataType: 'json',

            })
                .done(function (result) {
                    modalLoaded = broker;

                    if (result.success) {
                        $('#modal-content').html(result.html);
                        $('#modal').modal();
                    }
                    Overlay.hide();

                })
                .fail(function (ex) {
                    console.log(ex);
                    alert("Error...");
                });
        }
    }

    function checkIfCarriageAvailabe()
    {

        var sender_country_id = $('[data-country="sender"]').val();
        var receiver_country_id = $('[data-country="receiver"]').val();
        var weight = $('[data-package-weight]').val();
        var size_l = $('#Courier_CourierTypeInternal_package_size_l').val();
        var size_w = $('#Courier_CourierTypeInternal_package_size_w').val();
        var size_d = $('#Courier_CourierTypeInternal_package_size_d').val();
        var with_pickup = $("#with-pickup").val();

        $.ajax({
            url: yii.urls.checkIfCarriageAvailable,
            data: { sender_country_id: sender_country_id, receiver_country_id: receiver_country_id, weight: weight, size_l : size_l, size_w : size_w, size_d : size_d, with_pickup : with_pickup},
            method: 'POST',
            dataType: 'json',
            success: function(result){

                if(result.result)
                {
                    var deliveryBrokerId = result.deliveryBrokerId;

                    if(receiver_country_id && receiver_country_id == yii.ids.countryDe && deliveryBrokerId) {
                        $('[data-delivery-broker-id-show]').not('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideUp();
                        $('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideDown();
                    }
                    else if(receiver_country_id && receiver_country_id == yii.ids.countryLt && deliveryBrokerId) {
                        $('[data-delivery-broker-id-show]').not('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideUp();
                        $('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideDown();
                    }
                    else if(receiver_country_id && receiver_country_id == yii.ids.countryPl && deliveryBrokerId) {

                        $('[data-delivery-broker-id-show]').not('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideUp();
                        $('[data-delivery-broker-id-show="' + deliveryBrokerId + '"]').slideDown();
                    }
                    else
                        $('[data-delivery-broker-id-show]').hide();

                    getCod();
                    getAdditions();

                } else {
                    $('[data-not-available]').show();
                    $('[data-continue]').hide();
                    disableButton($('[data-continue-button]'));
                    formActive = false;
                    Overlay.hide();
                }

                return result.result;
            },
            error: function(e){

                //alert(yii.loader.error);
                FormControl.submit();
            }
        });
    }

    function getAdditions()
    {
        var selectedAdditions = [];
        $('input[name="' + 'CourierAddition[]"]').each(function() {
            if($(this).is(':checked'))
                selectedAdditions.push($(this).val());
        });

        var sender_country_id = $('[data-country="sender"]').val();
        var receiver_country_id = $('[data-country="receiver"]').val();
        var sender_zip_code = $('[data-zip-code="sender"]').val();
        var receiver_zip_code = $('[data-zip-code="receiver"]').val();
        var withPickup = $("#with-pickup").val();
        var weight = $('[data-package-weight]').val();
        var currency = $('#selected-currency').val();

        $.ajax({
            url: yii.urls.getAdditions,
            data: { sender_country_id: sender_country_id, receiver_country_id: receiver_country_id, with_pickup: withPickup, weight: weight, sender_zip_code : sender_zip_code, receiver_zip_code : receiver_zip_code, additions: selectedAdditions, currency: currency},
            method: 'POST',
            dataType: 'json',
            success: function(result){
                $('#additions-placeholder').html(result);
                $('[data-continue]').show();
                enableButton($('[data-continue-button]'));
                $('[data-not-available]').hide();
                formActive = true;

                checkboxAddtions();
                prepareAdditions();

                Overlay.hide();

                getPrice();

            },
            error: function(e){
                //alert(yii.loader.error);
                FormControl.submit();
            }
        });
    }

    function getPrice()
    {
        var selectedAdditions = [];
        $('input[name="' + 'CourierAddition[]"]').each(function() {
            if($(this).is(':checked'))
                selectedAdditions.push($(this).val());
        });

        var packages_number = $('[data-packages-no]').val();
        var sender_country_id = $('[data-country="sender"]').val();
        var receiver_country_id = $('[data-country="receiver"]').val();
        var sender_zip_code = $('[data-zip-code="sender"]').val();
        var receiver_zip_code = $('[data-zip-code="receiver"]').val();
        var sender_city = $('[data-city="sender"]').val();
        var receiver_city = $('[data-city="receiver"]').val();
        var withPickup = $("#with-pickup").val();
        var weight = $('[data-package-weight]').val();
        var size_l = $('#Courier_CourierTypeInternal_package_size_l').val();
        var size_w = $('#Courier_CourierTypeInternal_package_size_w').val();
        var size_d = $('#Courier_CourierTypeInternal_package_size_d').val();
        var currency = $('#selected-currency').val();



        $.ajax({
            url: yii.urls.getPrice,
            data: { sender_country_id: sender_country_id, receiver_country_id: receiver_country_id, with_pickup: withPickup, weight: weight, sender_zip_code : sender_zip_code, receiver_zip_code : receiver_zip_code, additions: selectedAdditions, size_l : size_l, size_w : size_w, size_d : size_d, packages_number : packages_number, sender_city : sender_city, receiver_city : receiver_city, currency: currency },
            method: 'POST',
            dataType: 'json',
            success: function(result){

                var price_each = result.each;
                var price_total = result.total;

                price_each = price_each.split(' ');
                price_total = price_total.split(' ');

                var currency = price_each[1];

                price_each = price_each[0];
                price_total = price_total[0];

                price_each = parseFloat(price_each);
                price_total = parseFloat(price_total);

                price_each = price_each.toFixed(2);
                price_each = price_each.toString();
                price_each = price_each.replace('.', ',');
                price_each += ' ' + currency;

                price_total = price_total.toFixed(2);
                price_total = price_total.toString();
                price_total = price_total.replace('.', ',');
                price_total += ' ' + currency;


                $('#price-each').html(price_each);
                $('#price-total').html(price_total);
            },
            error: function(e){
                //alert(yii.loader.error);
                FormControl.submit();
            },
        });
    }

    function checkWeight()
    {
        var newVal;
        var $no = $('#Courier_CourierTypeInternal_packages_number');
        var $weightOne = $('#Courier_CourierTypeInternal_package_weight');
        var $weightTotal = $('#Courier_CourierTypeInternal__package_weight_total');

        var triggerId = $(this).attr('id');

        if(triggerId == undefined)
            triggerId = $weightOne.attr('id');

        var no = parseInt($no.val());
        var weightOne = parseFloat($weightOne.val());
        var weightTotal = parseFloat($weightTotal.val());

        if(triggerId == $no.attr('id'))
        {
            if(!isNaN(weightTotal)) {

                newVal = parseFloat(weightTotal / no).toFixed(2);

                if(newVal != $weightOne.val()) {
                    $weightOne.val(newVal);
                    $weightOne.trigger('change');
                }

            }
        }
        else if(triggerId == $weightOne.attr('id'))
        {
            if(!isNaN(weightOne))
                $weightTotal.val(parseFloat(weightOne * no).toFixed(2));
        }
        else if(triggerId == $weightTotal.attr('id'))
        {
            if(!isNaN(weightTotal)) {

                newVal = parseFloat(weightTotal / no).toFixed(2);

                if(newVal != $weightOne.val()) {
                    $weightOne.val(newVal);
                    $weightOne.trigger('change');
                }
            }
        }

    }

    function calculateDimensionaWeight()
    {
        var l = $("#Courier_CourierTypeInternal_package_size_l").val();
        var w = $("#Courier_CourierTypeInternal_package_size_w").val();
        var d = $("#Courier_CourierTypeInternal_package_size_d").val();

        var dw = DimensionalWeight.calculate(l,w,d);

        $("#_dw_show").html(dw);
    }



    function updatePackagesNo()
    {
        $('[data-packages-no-show]').html($('[data-packages-no]').val());
    }



    ///////////////////////////////////////////////////////
    // UPDATED "WITH_PICKUP":

    $(document).ready(function(){
        // $("input[name='Order[payment_type_id]']").hide();
        $("[data-with-pickup-id]").on('click', function(){

            var id = $(this).attr('data-with-pickup-id');
            if(id > -1) {
                $("#with-pickup").val(id).trigger('change');
                $('[data-pickup-alert="-1"]').hide();
            }
            else
            {
                $('[data-pickup-alert]').not('[data-pickup-alert="-1"]').hide();
                $('[data-pickup-alert="-1"]').show();
            }

            pickupButtonsActivator(id);
        });

        $(document).ready(function(){
            pickupButtonsActivator($("#with-pickup").val());


            $('#sp-point-list').on('change', function(){

                var val = $(this).val();
                if(val)
                    CourierInternalMap.selectSpPoint(val);
            });

        });

        function pickupButtonsActivator(id) {

            if (id != yii.pickUp.WITH_PICKUP_REGULAR && id != yii.pickUp.WITH_PICKUP_CONTRACT) {
                $('[data-with-pickup-id="-1"]').addClass('active');
                $('[data-with-pickup-container]').slideDown('normal', function(){
                    id = parseInt(id);
                    CourierInternalMap.mapStart();


                });
            }


            if (id == yii.pickUp.WITH_PICKUP_REGULAR || id == yii.pickUp.WITH_PICKUP_CONTRACT) {
                $('[data-with-pickup-container]').slideUp();
                $('[data-with-pickup-id="-1"]').removeClass('active');
            }

            if (id != -1)
            {
                $('[data-with-pickup-id]').not('[data-with-pickup-id="-1"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
                $('[data-with-pickup-id="' + id + '"]').addClass('active').addClass('btn-primary').removeClass('btn-default');
            } else {
                $('[data-with-pickup-id]').not('[data-with-pickup-id="-1"]').removeClass('active').removeClass('btn-primary');
                $("#with-pickup").val('');
            }

        }



        // RUCH - send point:
        $(function(){
            var initialized = false;
            // bind on just on click to get fresh data from form

            if(!initialized)
                $('#ruch_point_suggester').on('click', function()
                {
                    initialized = true;
                    $(this).unbind('click');
                    $(this).pwrgeopicker('popup', {
                        'form': {
                            'city': $('#CourierTypeInternal_AddressData_sender_city').val(),
                            'street': $('#CourierTypeInternal_AddressData_sender_address_line_1').val(),
                        },
                        'marker_icons' : {
                            'mouseover': yii.urls.mapIcoOrange,
                            'mouseout':  yii.urls.mapIco,
                            'mousedown': yii.urls.mapIcoGreen,
                            'mouseup': yii.urls.mapIco,
                        },
                        'auto_start': true,//
                        'onselect': function(data){

                            $('#ruch-suggester-address').show().find('.well').html(data.StreetName + ',' + data.City + '<br/>' + data.Location + '<br/><br/>' + data.OpeningHours);
                        }
                    });
                    $(this).trigger('click');
                });
        });
        //

        $('#CourierTypeInternal_AddressData_receiver_country_id').on('change', function(){

            if($(this).val() == 1) {
                if($("#with-pickup").val() == yii.pickUp.WITH_PICKUP_RUCH)
                    $('#ruch-receiver-part').show();
            }
            else
                $('#ruch-receiver-part').hide();
        });

    });

    function checkAutomaticReturnAvailable()
    {

        var $returnBlock = $('[data-return]');
        var receiver_country_id =   $('#CourierTypeInternal_AddressData_receiver_country_id').val();

        $.ajax({
            url: yii.urls.checkAutomaticReturn,
            data: { country_id: receiver_country_id},
            method: 'POST',
            dataType: 'json',
            success: function(result){

                if(result)
                {
                    $returnBlock.show();
                } else {

                    $returnBlock.hide();
                }

            },
            error: function(e){

                console.log(e);
            }
        });
    }

    function setAutomaticReturn()
    {

        var currentCountry = parseInt($('#CourierTypeInternal_AddressData_receiver_country_id').val());
        var defaultReturnCountries = yii.automaticReturnCountries;
        var $generateReturnCheckbox = $('[data-generate-return]');

        if(defaultReturnCountries.indexOf(currentCountry) >= 0) {
            // console.log('AR: t');

            if($generateReturnCheckbox.is(':checked'))
            {

            } else {
                // console.log('AR: tr');
                // $generateReturnCheckbox.prop('checked', true);
                $generateReturnCheckbox.trigger('click');
            }


        }
        else {
            // console.log('AR: n');
            if($generateReturnCheckbox.is(':checked'))
            {
                // console.log('AR: tr');
                $generateReturnCheckbox.trigger('click');
                // $generateReturnCheckbox.prop('checked', false);
            } else {
                // $generateReturnCheckbox.prop('checked', true);

            }
        }
    }

})(jQuery);