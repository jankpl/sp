<?php

class PostalController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => [ 'view', 'index', 'generateReturnLabel', 'searchByRemote','exportToFileByGridView', 'generateAckCardsByGridView', 'labelPrinter', 'generateLabelsByGridView'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $preFilter = [];
        $preModels = false;
        if(isset($_POST['preFilter']) && $_POST['preFilter'] != '' OR isset($_POST['preFilterClear'])) {

            if(isset($_POST['preFilterClear']))
            {
                $preFilter = [];
            }
            else
            {
                $preFilter = $_POST['preFilter'];
                $preFilter = explode(PHP_EOL, $preFilter);

                foreach ($preFilter AS $key => $item)
                    $preFilter[$key] = trim($item);

                $preFilter = array_filter($preFilter);

                $preModels = PostalExternalManager::findPostalIdsByRemoteId($preFilter);
            }
        }


        $this->render('index', array(
            'preFilter' => implode(PHP_EOL, $preFilter),
            'preModels' => $preModels,
        ));
    }

    public function actionView($id = NULL, $urlData = NULL)
    {
        if($urlData!= NULL)
            $model = Postal::model()->findByAttributes(array('local_id' => $urlData));
        else
            $model = Postal::model()->findByPk($id);

        if($model === null)
            throw new CHttpException('404');

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR && $model->user->salesman_id != Yii::app()->user->model->salesman_id)
            throw new CHttpException(403);

        $this->render('view', array(
            'model' => $model,
        ));
    }

    public function actionGenerateReturnLabel($urlData)
    {
        /* @var $model Postal */
        $model = Postal::model()->findByAttributes(['hash' => $urlData]);

        if($model == NULL)
            throw new CHttpException(404);

        PostalReturnLabel::generate($model);
    }



    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewPostal($preModels = false, $multiselectAttributes = false, $dataRangeScript = false){
        $model = new _PostalGridView('search');

        $model->unsetAttributes();

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        if (isset($_GET['PostalGridView']))
            $model->setAttributes($_GET['PostalGridView']);

        $pageSize = Yii::app()->user->getState('pageSize', 50);

        if(is_array($preModels))
        {
            if(S_Useful::sizeof($preModels))
            {
                $preIds = CHtml::listData($preModels, 'id', 'id');
                $model->_multiIds = implode(',', $preIds);
            }
            else
                $model->_multiIds = -1;
        }


        $headers = [];
        $headers[] = array(
            'text'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10 ,20=>20 ,50=>50 , 100=>100, 250 => 250, 500 => 500),array(
                'onchange'=>"$.fn.yiiGridView.update('postal',{ data:{pageSize: $(this).val() }})",
            )),

            'colspan'=>21,
            'options'=>['style' => 'text-align: right;']
        );

        $this->widget('BackendGridView', array(
            'id' => 'postal',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>2,
            'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => $multiselectAttributes, 'afterAjaxUpdate' => true, 'additionScript' => $dataRangeScript), true),
            'selectionChanged'=>'function(id){  
                var number = $("#courier").selGridView("getAllSelection").length;
                $(".selected-no").html(number); 
         }',
            'template'=>"<div class=\"text-center\">{pager}</div>\n{summary}\n{items}\n{summary}\n<div class=\"text-center\">{pager}</div>",
            'htmlOptions' => ['style' => 'table-layout: fixed'],
            'addingHeaders' => array(
                $headers,
            ),
            'columns' => array(
                [
                    'name' => '_multiIds',
                    'htmlOptions'=>array('style'=>'display:none;'),
                    'headerHtmlOptions'=>array('style'=>'display:none;'),
                    'filterHtmlOptions'=>array('style'=>'display:none;'),
                ],
                array(
                    'name'=>'local_id',
                    'header'=>'# local ID',
                    'type'=>'raw',
                    'value'=>'CHtml::link($data->local_id,array("/postal/postal/view/", "id" => $data->id), ["target" => "_blank"])',
                ),
                array(
                    'name' => 'bulk',
                    'value'=> '$data->bulkName',
                    'filter' => Postal::getBulkList(),
                ),
                array(
                    'name' => 'bulk_number',
                    'value'=> '$data->bulk_number',
                ),
                array(
                    'name' => 'bulk_id',
                    'value'=> '$data->bulk_id',
                ),
                [
                    'name' => 'ref',
                ],
                array(
                    'name' => 'postal_type',
                    'value'=> '$data->postalTypeName',
                    'filter' => Postal::getPostalTypeList(),
                ),
                array(
                    'name' => 'size_class',
                    'value'=> '$data->sizeClassName',
                    'filter' => Postal::getSizeClassList(),
                ),
                array(
                    'name' => 'weight_class',
                    'value'=> '$data->weightClassName',
                    'filter' => Postal::getWeightClassList(),
                ),

                array(
                    'name'=>'receiver_country_id',
                    'header'=>'Receiver country',
                    'value'=>'$data->receiverCountryName',
                    'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
                ),
                array(
                    'name' => '__sender_usefulName',
                    'header' => 'Sender',
                    'value' => '$data->senderAddressData->usefulName',
                ),
                array(
                    'name' => '__receiver_name',
                    'header' => 'Receiver',
                    'value' => '$data->receiverAddressData->usefulName',
                ),
                array(
                    'name'=>'date_entered',
                    'header'=>'Created',
                ),
                array(
                    'name'=>'date_updated',
                    'header'=>'Updated',
                ),
                array(
                    'name'=>'__user_login',
                    'header'=>'User',
                    'value' => '$data->user->login',
                ),
                array(
                    'name'=>'target_sp_point',
                    'value' => 'SpPoints::getPoint($data->target_sp_point)->name',
                    'filter' => CHtml::listData(SpPoints::getPoints(), 'id', 'name'),
                ),
                array(
                    'name'=>'__operator',
                    'value' => '$data->postalOperator ? $data->postalOperator->getUsefulName() : ""',
                    'filter' => CHtml::listData(PostalOperator::getOperators(false, false), 'id', 'usefulName'),
                ),
                array(
                    'name'=>'__stat',
                    'value'=>'$data->stat->name',
//                    'filter'=>GxHtml::listDataEx(PostalStat::model()->findAllAttributes(null, true)),
                    'filter'=> CHtml::activeDropDownList($model, 'postal_stat_id', CHtml::listData(PostalStat::getStats(true),'id','adminStatNameShort'), array('multiple' => 'true', 'size' => 1, 'id' => 'stat-selector')),
                ),
                array(
                    'name'=>'stat_map_id',
                    'value'=>'StatMap::getMapNameById($data->stat_map_id)',//
                    'filter'=> StatMap::mapNameList(),
                ),
                [
                    'name' => 'user_cancel',
                    'filter' => Postal::getUserCancelList(),
                    'value' => 'Postal::getUserCancelListShort()[$data->user_cancel]',
                ],
                [
                    'name' => 'source',
                    'filter' => Postal::getSourceList(),
                    'value' => '$data->getSourceName()',
                ],
//                array(
//                    'class' => 'CButtonColumn',
//                ),
                array(
                    'class'=>'CCheckBoxColumn',
                    'id'=>'selected_rows'
                ),

            ),
        ));
    }






    public function actionAssingStatusByGridView()
    {
        if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER]))
            throw new Exception(403);

        /* @var $model Postal */
        $i = 0;
        if (isset($_POST['Postal'])) {

            $postal_stat_id = (int) $_POST['postal_stat_id'];
            $postal_ids =  $_POST['Postal']['ids'];

            $postal_ids = explode(',', $postal_ids);

            $date = $_POST['stat_date'];
            $location = $_POST['location'];

            if(is_array($postal_ids) AND S_Useful::sizeof($postal_ids))
            {

                $models = [];
                foreach($postal_ids AS $id)
                {
                    $model = Postal::model()->findByPk($id);

                    if($model !== null)
                        array_push($models, $model);

                }

                $result = Postal::changeStatForGroup($models, $postal_stat_id, $date, $location);

                if($result)
                {

                    Yii::app()->user->setFlash('success','Udało się zmienić statusy. Liczba pozycji: '.$result);
                }
                else
                {
                    Yii::app()->user->setFlash('error','Nie udało się zmienić statusu');
                }
            }
        }

        if(!S_Useful::sizeof($postal_ids))
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');

        $this->redirect(Yii::app()->createUrl('/postal/postal/index'));

    }

    public function actionSearchByRemote()
    {
        $remoteId = $_POST['remoteId'];
        $remoteId = trim($remoteId);

        if($remoteId == '')
        {
            Yii::app()->user->setFlash('notice', 'Please fill remote Id number');
            $this->redirect(['/postal/postal/index']);
            Yii::app()->end();
        }

        $response = PostalExternalManager::findPostalIdsByRemoteId($remoteId);

        $n = S_Useful::sizeof($response);
        if(!$n) {
            Yii::app()->user->setFlash('notice', '0 packages found!');
            $this->redirect(['/postal/postal/index']);
            Yii::app()->end();
        }
        else
        {

            $html = '<ul>';
            foreach($response AS $postal)
            {
                $html .= '<li>'.CHtml::link($postal->local_id, ['/postal/postal/view', 'id' => $postal->id]).'</li>';
            }
            $html .= '</ul>';
            Yii::app()->user->setFlash('success', $n.' package(s) found for ID "'.$remoteId.'":<br/> '.$html);
            $this->redirect(['/postal/postal/index']);
            Yii::app()->end();

        }

    }

    public function actionExportToFileAll()
    {
        $period = $_POST['period'];

        if($period == '' or strlen($period) < 6)
            throw new CHttpException(404);

        $period = substr($period, 2,4).'-'.substr($period, 0,2).'-';

        // with() are partly omited due to memory limits
//        $models = Postal::model();

//        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
//        {
//            $models = $models->with('user');
//        }

//        $models = $models->findAll('t.date_entered LIKE :date', [':date' => $period.'%']);

//        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
//        {
//            foreach($models AS $key => $item)
//                if($item->user->salesman_id != Yii::app()->user->model->salesman_id)
//                    unset($models[$key]);
//        }

        $cmd = Yii::app()->db->createCommand();
        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
        {
            $models = $cmd->select('postal.id')->from('postal')->join('user', 'postal.user_id = user.id')->where('postal.date_entered LIKE :date AND user.salesman_id = :salesman_id', [':date' => $period . '%', ':salesman_id' => Yii::app()->user->model->salesman_id])->queryColumn();
        } else if(AdminRestrictions::isUltraAdmin() && Yii::app()->user->id != 3)  {
            $models = $cmd->select('id')->from('postal')->where('date_entered LIKE :date', [':date' => $period . '%'])->queryColumn();
        } else {
            $models = $cmd->select('postal.id')->from('postal')->join('user', 'postal.user_id = user.id')->where('postal.date_entered LIKE :date AND user.partner_id = :partner_id', [':date' => $period . '%', ':partner_id' => Yii::app()->user->model->partner_id])->queryColumn();
        }



        if(!S_Useful::sizeof($models)) {
            Yii::app()->user->setFlash('notice', '0 packages found!');
            $this->redirect(['/postal/postal/index']);
            Yii::app()->end();
        } else {
            S_PostalIO::exportToXls($models, true);
        }
    }

    public function actionDeleteLastStatusByGridView()
    {
        if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER]))
            throw new Exception(403);

        /* @var $model Postal */
        $i = 0;
        if (isset($_POST['Postal']))
        {

            $postal_ids =  $_POST['Postal']['ids'];

            $postal_ids = explode(',', $postal_ids);

            if(is_array($postal_ids) AND S_Useful::sizeof($postal_ids))
            {

                $models = [];
                foreach($postal_ids AS $id)
                {
                    $model = Postal::model()->findByPk($id);
                    if($model === null)
                        continue;

                    array_push($models, $model);

                    $i++;
                }
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/postal/postal/index'));
        } else {
            $result = Postal::deleteLastStatForGroup($models);

            if($result)
            {

                Yii::app()->user->setFlash('success','Udało się zmienić statusy. Liczba pozycji: '.$result);
            }
            else
            {
                Yii::app()->user->setFlash('error','Nie udało się zmienić statusu');
            }

            $this->redirect(Yii::app()->createUrl('/postal/postal/index'));
        }
    }

    public function actionExportToFileByGridView()
    {
        /* @var $model Postal */
        $i = 0;
        if (isset($_POST['Postal']))
        {


            $postal_ids =  $_POST['Postal']['ids'];

            $postal_ids = explode(',', $postal_ids);

            if(is_array($postal_ids) AND S_Useful::sizeof($postal_ids))
            {

                $models = [];
                foreach($postal_ids AS $id)
                {
                    $model = Postal::model()->findByPk($id);
                    if($model === null)
                        continue;

                    array_push($models, $model);

                    $i++;
                }
            }
        }

        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/postal/postal/index'));
        } else {
            S_PostalIO::exportToXls($models);
        }
    }




    public function actionDeleteLastStatus($id)
    {
        /* @var $model Postal */

        $model = Postal::model()->findByPk($id);
        if($model == NULL)
            throw new CHttpException(404);

        $return = $model->deleteLastStatus();

        if($return)
            Yii::app()->user->setFlash('success', 'Last stat has been deleted');
        else
        {
            Yii::app()->user->setFlash('error', 'Last stat has not been deleted');
        }

        $this->redirect(Yii::app()->createUrl('/postal/postal/view', array('id' => $model->id)));


    }



    public function actionChangeStat()
    {

        if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER]))
            throw new Exception(403);

        /* @var $model Postal */
        if (isset($_POST['Postal'])) {

            $model = Postal::model()->findByPk($_POST['Postal']['id']);

            if($model === null)
                throw new Exception('Błąd!');

            $date = $_POST['status_date'];
            $location = $_POST['location'];

            if($model->changeStat($_POST['Postal']['postal_stat_id'], '', $date, false, $location, false, false, false, true))
                $this->redirect(array('/postal/postal/view', 'id' => $model->id));
            else
                throw new Exception("Błąd!");
        } else
            throw new CHttpException('404');

    }


    public function actionUserCancelConfirm($id)
    {
        $model = Postal::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        if($model->userCancelConfirmByAdmin())
            Yii::app()->user->setFlash('success', 'Operation has succeded');
        else
            Yii::app()->user->setFlash('error', 'Operation has not succeded');

        $this->redirect(['/postal/postal/view', 'id' => $model->id]);
    }

    public function actionUserCancelDismiss($id)
    {
        $model = Postal::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        if($model->userCancelDismiss())
            Yii::app()->user->setFlash('success', 'Operation has succeded');
        else
            Yii::app()->user->setFlash('error', 'Operation has not succeded');

        $this->redirect(['/postal/postal/view', 'id' => $model->id]);
    }



    public function actionCarriageTicket($id, $type = NULL)
    {
        /* @var Postal $model */

        $model = Postal::model()->findByPk($id);

        if($model === null)
            throw new CHttpException('404');

        PostalLabelPrinter::generateLabels($model, $type, false);
    }


    public function actionGenerateLabelsByGridView()
    {
        /* @var $model Postal */
        $i = 0;
        if (isset($_POST['Postal']))
        {
            $type = $_POST['Postal']['type'];
            $postal_ids =  $_POST['Postal']['ids'];

            $postal_ids = explode(',', $postal_ids);

            if(is_array($postal_ids) AND S_Useful::sizeof($postal_ids))
            {

                $models = [];
                foreach($postal_ids AS $id)
                {
                    $model = Postal::model()->findByPk($id);
                    if($model === null)
                        continue;

                    if($model->bulk)
                        continue;

                    array_push($models, $model);

                    $i++;
                }
            }
        }


        if(!$i)
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/postal/postal/index'));
        } else {

            PostalLabelPrinter::generateLabels($models, $type, true);
        }
    }

    public function actionGenerateAckCardsByGridView()
    {
        /* @var $model Postal */

        if (isset($_POST['Postal']))
        {
            $postal_ids =  $_POST['Postal']['ids'];
            $models = Postal::model()
                ->findAllByAttributes(['id' => explode(',', $postal_ids)], 'postal_stat_id != :stat AND postal_stat_id != :stat2 AND postal_stat_id != :stat3', [':stat' => PostalStat::CANCELLED, ':stat2' => PostalStat::CANCELLED_PROBLEM, ':stat3' => PostalStat::CANCELLED_USER]);

        }

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/postal/postal/index'));
        } else {
            PostalAcknowlegmentCard::generateCardMulti($models);
        }
    }

    public function actionGenerateInvoiceProFormaByGridView()
    {
        /* @var $model Postal */

        if (isset($_POST['Postal']))
        {
            $postal_ids =  $_POST['Postal']['ids'];
            $models = Postal::model()
                ->findAllByAttributes(['id' => explode(',', $postal_ids)], 'postal_stat_id != :stat AND postal_stat_id != :stat2 AND postal_stat_id != :stat3', [':stat' => PostalStat::CANCELLED, ':stat2' => PostalStat::CANCELLED_PROBLEM, ':stat3' => PostalStat::CANCELLED_USER]);

        }

        if(!S_Useful::sizeof($models))
        {
            Yii::app()->user->setFlash('error','Zaznacz listę przesyłek!');
            $this->redirect(Yii::app()->createUrl('/postal/postal/index'));
        } else {
            PostalInvoiceProforma::generateCardMulti($models);
        }
    }


    public function actionEditTrackingId($id)
    {
        /* @var $model PostalLabel */
        $model = PostalLabel::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        if (isset($_POST['PostalLabel'])) {

            $model->source_postal_operator_id = $_POST['PostalLabel']['source_postal_operator_id'];
            $model->track_id = $_POST['PostalLabel']['track_id'];

            if ($model->update(['track_id', 'source_postal_operator_id'])) {

                $model->expireLabel();
                Yii::app()->user->setFlash('success', 'Tracking ID has benn updated!');
                $this->redirect(array('/postal/postal/view', 'id' => $model->postal->id));
            }
        }

        $this->render('updateTrackingId', array( 'model' => $model));



    }


    public function actionUpdateTtOne($id)
    {
        Yii::import('application.components.*');

        $postal = Postal::model()->with('postalLabel')->findByPk($id);

        /* $item Courier */
        if($postal !== NULL)
            PostalExternalManager::updateStatusForPackage($postal);
        else
            throw new CHttpException(404);

        $this->redirect(['/postal/postal/view', 'id' => $postal->id]);
    }



    public function actionLabelPrinter($withClone = false)
    {
        $model = false;
        if (isset($_POST['local_id'])) {

            $model = Postal::model()->find('local_id=:local_id', array(':local_id' => $_POST['local_id']));

            if($model === NULL) {
                $remoteId = $_POST['local_id'];

                if($withClone)
                {
                    $response = PostalLabel::model()->findAllByAttributes(['track_id' => $remoteId]);

                } else {
                    $response = PostalExternalManager::findPostalIdsByRemoteId($remoteId);
                }

                $n = S_Useful::sizeof($response);
                if (!$n) {
                    Yii::app()->user->setFlash('notice', '0 packages found!');
                } else if ($n > 1) {
                    Yii::app()->user->setFlash('notice', 'More than 1 package found!');
                } else {
                    $model = array_pop($response);

                    if($withClone)
                    {
                        $newModel = Postal::cloneItem($model->postal);
                        $model = $newModel;
                    }
                }
            }

            if($_POST['automatic_roll_label'] == 'true' && $model)
            {
                echo CJSON::encode(['redirect' => true, 'url' => Yii::app()->createAbsoluteUrl('/postal/postal/CarriageTicket', ['id' => $model->id, 'type' => PostalLabelPrinter::PDF_ROLL])]);
                exit;
            }

            if(isset($_POST['ajax'])) {
                echo CJSON::encode(array('html' => $this->renderPartial('label_printer_buttons', array('model' => $model), true)));
                exit;

            }
        }

        $this->render('labelPrinter', array(
            'model' => $model,
            'withClone' => $withClone
        ));
    }

    public function actionAddRef($urlData)
    {
        /* @var $model Courier */
        $model = Postal::model()->findByAttributes(['hash' => $urlData]);

        if($model === NULL)
            throw new CHttpException(404);

        if($model->ref != '')
            throw new CHttpException(400, 'This package has ref number!');

        if (isset($_POST['Postal'])) {
            $model->ref = $_POST['Postal']['ref'];

            if ($model->validate(['ref']) && $model->update(['ref'])) {

                $model->addToLog('Added REF number: '.$model->ref.' by: '.Yii::app()->user->model->login, PostalLog::CAT_INFO);

                Yii::app()->user->setFlash('success', 'REF number has been updated!');
                $this->redirect(array('/postal/postal/view', 'id' => $model->id));
            }
        }

        $this->render('addRef', array('model' => $model));
    }

    public function actionRemoveRef($urlData)
    {
        /* @var $model Courier */
        $model = Postal::model()->findByAttributes(['hash' => $urlData]);

        if($model === NULL)
            throw new CHttpException(404);

        if($model->ref == '')
            throw new CHttpException(400, 'This package has no ref number!');

        $ref = $model->ref;
        $model->ref = NULL;

        if ($model->update(['ref'])) {

            $model->addToLog('Removed REF number: ' . $ref . ' by: ' . Yii::app()->user->model->login, PostalLog::CAT_INFO);
            Yii::app()->user->setFlash('success', 'REF number has been removed!');
        }

        $this->redirect(array('/postal/postal/view', 'id' => $model->id));
    }



}