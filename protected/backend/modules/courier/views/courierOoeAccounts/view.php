<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
    array('label'=>'Delete ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage' . $model->label(2), 'url'=>array('admin')),
);
?>

    <h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'date_entered',
        'date_updated',
        array(
            'name'=>'stat',
            'value'=>S_Status::stat($model->stat),
        ),
        'login',
        'account_code',
        array(
            'name'=>'stat',
            'value'=>S_Status::stat($model->stat),
        ),
    ),
)); ?>

    <h2>List of services from OOE:</h2>
<?php
list($status, $data) = Yii::app()->OneWorldExpress->getServicesForUser($model->login, $model->pass, true);

if($status)
    echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, implode('<br/>', $data), array('closeText' => false));
else
    echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, $data, array('closeText' => false));
