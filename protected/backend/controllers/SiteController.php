<?php

class SiteController extends Controller
{


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
//            array('deny',
//                'actions'=>array('login'),
//                'users'=>array('@'),
//            ),
            array('allow',
                'actions'=>array('index','login','ajaxGetStatisticGeneral', 'cSC', 'switchLayout'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('logout', 'stat', 'summary', 'yesterday'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('regon.RegonActions'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('googleTranslate.GoogleTranslateActions'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }


    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
            'regon.'=>'application.backend.components.regon.RegonWidget',
            'googleTranslate.'=>'application.backend.components.googleTranslate.GoogleTranslateWidget',
        );
    }


    public function actionIndex()
    {

        if(Yii::app()->user->isGuest)
            $this->redirect(array('site/login'));

        $this->render('index');
    }

    public function actionSummary()
    {
        $this->render('summary');
    }

    public function actionYesterday()
    {
        $this->render('yesterday');
    }

    public function actionStat()
    {
        $this->render('stat');
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {

        if(!Yii::app()->user->isGuest)
            $this->redirect(['/']);

        $this->layout = 'guest';

        $model=new LoginForm();

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {

            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
            {
                $this->redirect(Yii::app()->user->returnUrl);
            }

        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout(false);
        $this->redirect(Yii::app()->homeUrl);
    }


    public function actionAjaxGetStatisticGeneral()
    {

        $to = $_POST['to'];
        $from = $_POST['from'];

        $this->renderPartial('_statistic_general',
            array(
                'from' => $from,
                'to' => $to,
            ));
    }

    public function actionCSC($pass)
    {
        if($pass == 'qwerty')
            Yii::app()->cache4DbSchema->flush();
    }

    public function actionSwitchLayout()
    {
        Yii::app()->session['fullWidthPanel'] = !Yii::app()->session['fullWidthPanel'];

        $this->redirect(Yii::app()->request->urlReferrer);
    }


}