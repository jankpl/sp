<?php

Yii::import('application.modules.courier.models._base.BaseCourierOoeService');

class CourierOoeService extends BaseCourierOoeService
{

	public $_countryLists;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('date_entered', 'default',
				'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
			array('date_updated', 'default',
				'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
			array('stat', 'default',
				'value'=>S_Status::ACTIVE, 'on'=>'insert'),

			array('service_name', 'unique'),
			array('service_name, title', 'required'),
			array('stat', 'numerical', 'integerOnly'=>true),
			array('service_name', 'length', 'max'=>32),
			array('title', 'length', 'max'=>128),
			array('date_updated', 'safe'),
			array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, service_name, date_entered, date_updated, title, stat', 'safe', 'on'=>'search'),
		);
	}

	public function toggleStat()
	{
		$this->stat = abs($this->stat - 1);

		return $this->save(false,array('stat'));
	}

	public function saveWithTrAndCountries($ownTransaction = true)
	{
		if($ownTransaction)
			$transaction = Yii::app()->db->beginTransaction();

		$errors = false;

		if(!$this->save())
			$errors = true;


		if(!$errors)
			foreach($this->courierOoeServiceTrs AS $item)
			{
				$item->courier_ooe_service_id = $this->id;
				if(!$item->save()) {
					$errors = true;
				}
			}

		if(!$errors) {
			$cmd = Yii::app()->db->createCommand();
			$cmd->delete('courier_ooe_service_to_country_list', 'courier_ooe_service_id = :id', array(':id' => $this->id));
			if (isset($this->_countryLists))
				foreach ($this->_countryLists AS $country) {
					$cmd->insert('courier_ooe_service_to_country_list', array('courier_ooe_service_id' => $this->id, 'country_list_id' => $country));
				}
		}

		if($errors)
		{
			if($ownTransaction)
				$transaction->rollback();

			return false;
		} else {
			if($ownTransaction)
				$transaction->commit();

			return true;
		}
	}

	/**
	 * @param array $services Array of active OOE services returned form OWE API in format [ SERVICE_NAME => SERVICE_TITLE ]
	 * @return array
	 */
	public static function getEnabledServices(array $services, $returnModels = false)
	{
		$servicesKeys = array_keys($services);
		$enabledServices = self::model()->findAllByAttributes(array('stat' => S_Status::ACTIVE, 'service_name' => $servicesKeys));

		$senabledServicesShort = [];

		/* @var $item CourierOoeService */
		foreach($enabledServices AS $item)
		{
			if($returnModels) {
				// override service name for API - get name directly from OWE
				$item->title = $services[$item->service_name];
				$senabledServicesShort[$item->id] = $item;
			}
			else
			{
				$senabledServicesShort[$item->id] = $services[$item->service_name];
			}
		}


		return $senabledServicesShort;
	}

	public static function isCountryAvailableForService($service_id, $country_list_id)
	{
		$cmd = Yii::app()->db->createCommand();
		$cmd->select('COUNT(s.id)');
		$cmd->from('courier_ooe_service s');
		$cmd->join('courier_ooe_service_to_country_list l', 's.id = l.courier_ooe_service_id');
		$cmd->where('s.id = :service_id AND s.stat = :stat AND l.country_list_id = :country_list_id', [':service_id' => $service_id, ':stat' => S_Status::ACTIVE, ':country_list_id' => $country_list_id]);
		$cmd->limit(1);
		$result = $cmd->queryScalar();

		if($result)
			return true;
		else
			return false;
	}

	protected static $_mapIdToServiceName = [];
	public static function mapIdToServiceName($id)
	{
		if(!isset(self::$_mapIdToServiceName[$id]))
		{
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand();
			$cmd->select('service_name');
			$cmd->from((new self)->tableName());

			$cmd->where('id = :id AND stat = :stat', [':id' => $id, ':stat' => S_Status::ACTIVE]);
			self::$_mapIdToServiceName[$id] = $cmd->queryScalar();
		}

		return self::$_mapIdToServiceName[$id];
	}

	protected static $_mapServiceNameToId = [];
	public static function mapServiceNameToId($serviceName)
	{
		if(!isset(self::$_mapServiceNameToId[$serviceName]))
		{
			$cmd = Yii::app()->db->createCommand();
			$cmd->select('id');
			$cmd->from((new self)->tableName());
			$cmd->where('service_name = :service_name AND stat = :stat', [':service_name' => $serviceName, ':stat' => S_Status::ACTIVE]);
			self::$_mapServiceNameToId[$serviceName] = $cmd->queryScalar();
		}

		return self::$_mapServiceNameToId[$serviceName];
	}

	public static function getReceiverCountriesForService($service_id)
	{
		$CACHE_NAME = 'getReceiverCountriesForService_'.$service_id;

		$countryList = Yii::app()->cache->get($CACHE_NAME);
//		$countryList = false;
		if($countryList === false) {
			/* @var $model CourierOoeService */
			$model = self::model()->findByAttributes(array('id' => $service_id));

			$countryListIds = array();
			if($model !== NULL && is_array($model->countryLists))
				foreach ($model->countryLists AS $item) {
					array_push($countryListIds, $item->id);
				}


			$countryList = CountryList::model()->with('countryListTr')->findAllByAttributes(array('stat' => S_Status::ACTIVE, 'id' => $countryListIds));
			Yii::app()->cache->set($CACHE_NAME, $countryList, 60);
		}

		return $countryList;
	}

}