var data = Array();

$(document).ready(function(){

    $(".clearOnClick").focus(clearOnClick);
    $(".clearOnClick").blur(clearOnClickRestore);
	
    $(".clearOnClick").each(function(x,y){
		
		data[$(this).attr('id')] = $(this).val();
	});
	
	
});

function clearOnClick()
{
	if($(this).val() == data[$(this).attr('id')]) $(this).val('');
}

function clearOnClickRestore()
{

    if($(this).val() == data[$(this).attr('id')] || $(this).val() == '') $(this).val(data[$(this).attr('id')]);
	
}