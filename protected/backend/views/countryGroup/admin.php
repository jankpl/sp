<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
		array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
	);

?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'country-group-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'name',
		'date_entered',
        array(
            'header' => 'Countries',
            'name' => 'countryListsCOUNT',
            'filter' => false,
        ),
		array(
			'class' => 'CButtonColumn',

		),
	),
)); ?>


<a href="<?= Yii::app()->createUrl('/countryGroup/listAllGroups');?>" class="btn">List all groups</a>
