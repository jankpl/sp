<?php

/**
 * This is the model class for table "autenti".
 *
 * The followings are the available columns in table 'autenti':
 * @property integer $id
 * @property string $date_entered
 * @property integer $user_id
 * @property integer $admin_id
 * @property integer $autenti_document_id
 * @property integer $status
 *
 * @property Admin $admin
 * @property User $user
 */
class Autenti extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'autenti';
    }

    public static function createNew($user_id, $autenti_id, $admin_id)
    {
        $model = new self;
        $model->user_id = $user_id;
        $model->autenti_document_id = $autenti_id;
        $model->admin_id = $admin_id;
        $model->status = 'NEW';

        if(!$model->save())
            throw new Exception('Local save problem: '.print_r($model->getErrors(),1));
        else
            return $model->id;
    }

    public function updateStatus($status)
    {
        $this->status = $status;

        if(!$this->update(['status']))
            throw new Exception('Local save problem: '.print_r($this->getErrors(),1));
        else
            return true;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, admin_id, autenti_document_id, status', 'required'),
            array('user_id, admin_id, autenti_document_id', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, date_entered, user_id, admin_id, autenti_document_id, status', 'safe', 'on'=>'search'),
        );
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),

        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'admin' => array(self::BELONGS_TO, 'Admin', 'admin_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Id',
            'date_entered' => 'Date Entered',
            'user_id' => 'User',
            'admin_id' => 'Admin',
            'autenti_document_id' => 'Autenti Document',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('date_entered',$this->date_entered,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('admin_id',$this->admin_id);
        $criteria->compare('autenti_document_id',$this->autenti_document_id);
        $criteria->compare('status',$this->status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Autenti the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
