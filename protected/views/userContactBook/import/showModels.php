<?php
// LAYOUT CONFIGURATION

$colWidth[0] = 20;
$colWidth[1] = 120;
$colWidth[2] = 90;
$colWidth[3] = 100;
$colWidth[4] = 230;
$colWidth[5] = 230;
$colWidth[6] = 230;
$colWidth[7] = 230;
$colWidth[8] = 230;
$colWidth[9] = 230;
$colWidth[10] = 230;
$colWidth[11] = 230;
$colWidth[12] = 230;



$totalWidth = 0;
foreach($colWidth As $item)
    $totalWidth += $item;

$height = S_Useful::sizeof($models) * 31 + 60;
if($height > 850)
    $height = 850;



?>

<?php $modelTemp = new UserContactBook();?>
<?php $modelTemp2 = new AddressData();?>

<?php AddressDataSuggester::registerAssets(); ?>
<?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>

<?php
// WE DO NOT NEED FORM HERE - ALL DATA IS IN CACHE
//$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
//    array(
//        'enableAjaxValidation' => false,
//    )
//);
?>

<div class="text-center">
    <a href="<?php echo Yii::app()->createUrl('/userContactBook/import');?>" class="btn btn-warning"><?php echo Yii::t('courier', 'Od nowa');?></a>
    <br/>
</div>
<hr/>
<div class="text-center">
    <a href="#bottom">[<?php echo Yii::t('courier', 'skocz na dół');?>]</a>
</div>
<br/>
<?php
$this->widget('FlashPrinter');
?>

<div style="position: relative;">
    <div class="overlay"><div class="loader"></div></div>
    <?php
    // for upper scroll:
    ?>
    <div style="width: 100%; position: relative; height: 20px; overflow-x: scroll;" class="dummy-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; height: 20px;">
        </div>
    </div>
    <?php
    // end for upper scroll
    ?>
    <div style="width: 100%; position: relative; height: 30px; overflow: hidden;" class="header-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; overflow: hidden;">
            <table class="list hTop" style="font-size: 10px; text-align: left !important; table-layout: fixed; height: 30px;">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <tr>
                    <td>#</td>
                    <td><?php echo Yii::t('courier', 'Is valid');?></td>
                    <td>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('type'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="width: 100%; position: relative; height: <?= $height;?>px; overflow-x: scroll;" class="real-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute;">

            <table class="list list-special" style="font-size: 10px; text-align: left !important; table-layout: fixed;">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <?php

                foreach($models AS $key => $item):
                    $this->renderPartial('import/_item',
                        array(
                            'model' => $item,
                            'i' => $key,
                        ));

                endforeach;
                ?>
            </table>
        </div>
    </div>

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
        array(
            'enableAjaxValidation' => false,
        )
    );
    ?>

    <div class="navigate" style="text-align: center;" id="bottom">
        <?php echo TbHtml::submitButton(Yii::t('courier', 'Save this part of data'), array('name' => 'save', 'class' => 'save-packages btn-primary')); ?>
    </div>

    <?php
    $this->endWidget();
    ?>
</div>



<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  removeRowOnImport: '.CJSON::encode(Yii::app()->createUrl('/userContactBook/ajaxRemoveRowOnImport')).',
                  updateRowOnImport: '.CJSON::encode(Yii::app()->createUrl('/userContactBook/ajaxUpdateRowItemOnImport')).',
              },
               text: {
                  unknownError: '.CJSON::encode(Yii::t('site', 'Wystąpił nieznany błąd!')).',
              },
              misc: {
                  hash: '.CJSON::encode($hash).',
              },
          };
      ',CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/importListModels.js', CClientScript::POS_HEAD);
?>