<?php
class GenerateCarriageTicketsUniversal extends CWidget
{


    public $mode = Courier::TYPE_EXTERNAL;

    public function run()
    {
        $this->render('application.components.views.generateCarriageTicketsUniversal', array(
            'mode' => $this->mode,
        ));
    }
}
?>