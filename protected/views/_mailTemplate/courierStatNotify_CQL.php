<?php
/* @var $text string */
/* @var $model Courier */
/* @var $status string */
/* @var $addEnAnnouncement boolean */
/* @var $addEnIntro boolean */
/* @var $toWho string RECEIVER/SENDER/OWNER */
/* @var $packageNo string */
?>
<?= $text;?><br/><br/>
<?php
list($operator, $tt_id) = $model->getExternalOperatorsAndIds(true);
?>

<strong><?= Yii::t('mail_courier', 'Operator:');?></strong> <?= is_array($operator) ? $operator[0] : '-';?><br/>
<strong><?= Yii::t('mail_courier', 'Zew. numer TT:');?></strong> <?= is_array($tt_id) ? $tt_id[0] : '-';?> <br/><br/>

<?php
if($model->note1):
    ?>
    <strong><?= Yii::t('mail_courier', 'Note1:');?></strong> <?= $model->note1; ?><br/><br/>
<?php
endif;
?>

<?php
$baseUrl = PageVersion::lang2domain(Yii::app()->language);
echo Yii::t('mail_courier', 'Paczkę możesz śledzić {a}tutaj{/a}.', array('{a}' => '<a href="'.$baseUrl.'/tt/'.$packageNo.'">', '{/a}' => '</a>')); ?><br/><br/>

<strong><?= Yii::t('mail_courier', 'Dane paczki:');?></strong><br/>
<?= Yii::t('mail_courier', 'Wymiary {unit}: {w}x{s}x{d}', [
    '{unit}' => '['.Courier::getDimensionUnit().']',
    '{w}' => $model->package_size_d,
    '{s}' => $model->package_size_l,
    '{d}' => $model->package_size_w,
]);?><br/>
<?= Yii::t('mail_courier', 'Waga {unit}: {n}', [
    '{unit}' =>  '['.Courier::getWeightUnit().']',
    '{n}' =>$model->package_weight,
]);?><br/>
<?= Yii::t('mail_courier', 'Opis:');?> <?= $model->package_content; ?>
<br/><br/>

<strong><?= Yii::t('mail_courier', 'Nadawca paczki:');?></strong><br/>
<?= $model->senderAddressData->getUsefulName(); ?><br/>
<?php
if(!$model->user->getHideSenderAddressOnNotificationAndTt()):
    ?>
    <?= $model->senderAddressData->address_line_1; ?><br/>
    <?= $model->senderAddressData->address_line_2 != '' ? $model->senderAddressData->address_line_2.'<br/>' : '';?>
    <?= $model->senderAddressData->city;?>, <?= $model->senderAddressData->zip_code;?><br/>

    <?= $model->senderAddressData->getCountryNameUniversal();?><br/>
<?php
endif;
?>
<br/>
<strong><?= Yii::t('mail_courier', 'Odbiorca paczki:');?></strong><br/>
<?= $model->receiverAddressData->getUsefulName(); ?><br/>
<?= $model->receiverAddressData->address_line_1; ?><br/>
<?= $model->receiverAddressData->address_line_2 != '' ? $model->receiverAddressData->address_line_2.'<br/>' : '';?>
<?= $model->receiverAddressData->city;?>, <?= $model->receiverAddressData->zip_code;?><br/>
<?= $model->receiverAddressData->getCountryNameUniversal();?>
<?php if($model->isCod()): ?>
    <br/><br/><strong><?= Yii::t('mail_courier', 'Paczka została wysłana za pobraniem: ');?></strong> <?= $model->getCodValue();?> <?= $model->getCodCurrency();?>
<?php endif; ?>
