<?php

class CourierStatusConfirmationCard
{


    protected static function printTableHeader(&$pdf)
    {
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(50,3,Yii::t('courier_pdf','Paczka'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('courier_pdf','Nadawca'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('courier_pdf','Odbiorca'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(40,3,Yii::t('courier_pdf','Inne'),1,'L',false,0,'','',true,0,false,true,0,'T',true);

        $pdf->ln(4);

        $pdf->SetFont('freesans', '', 8);
    }

    public static function generateCardMulti(array $couriers)
    {

        foreach($couriers AS $key => $item)
        {
            if($item->stat === NULL OR $item->getType() == Courier::TYPE_EXTERNAL_RETURN)
                unset($couriers[$key]);
        }


        $totalNumber = S_Useful::sizeof($couriers);

        if($totalNumber > 2500)
            throw new Exception(Yii::t('courier','Za dużo paczek do wygenerowania potwierdzenia statusu.'));


        $pageSize = 17;
        $firstPageSizeOffset = 1;
        $lastPageSizeOffset = 0;

        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();
        $pdf->setMargins(10,10,10, true);

        $pdf->AddPage('P','A4');

//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);

        $firstModel = reset($couriers);
//        $pdf->Image('@'.PDFGenerator::getLogoPlBig($firstModel->user), $pdf->getPageWidth() /2 - 15 - $pdf->getMargins()['right'], 5 , 40,'','','N',false);

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->SetY($pdf->GetY() + 8);
        $pdf->SetTextColor(0,0,0);

        $pdf->ln();

        self::printTableHeader($pdf);

        $pw = $pdf->getPageWidth();
        $ph = $pdf->getPageHeight();


        if($totalNumber <= ($pageSize - $firstPageSizeOffset - $lastPageSizeOffset))
            $totalPages = 1;
        else {
            $totalPages = ceil((($totalNumber + $firstPageSizeOffset + $lastPageSizeOffset)) / $pageSize);
        }


        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->MultiCell(20,3,'1/'.$totalPages,0,'R', false, 0, $pw-30, $ph - 10);
        $pdf->MultiCell(80,3,Yii::t('courier_pdf','Data wygenerowania:').' '.date('Y-m-d H:i'),0,'L', false, 0, 9, 13);
        $pdf->SetAbsX($x);
        $pdf->SetAbsY($y);


        $i = 0;
        $pageI = 0;
        $page = 1;
        /* @var $item Courier */
        foreach($couriers AS $item)
        {
            if($item->stat === NULL)
                continue;

            if($item->getType() == Courier::TYPE_EXTERNAL_RETURN)
                continue;

            $cellContent = Yii::t('courier_pdf','Nr paczki:').' '.$item->local_id."\r\n";

            $cellContent .= Yii::t('courier_pdf','Status:').' '.StatMap::getMapNameById($item->stat_map_id);
            $cellContent .= "\r\n".Yii::t('courier_pdf','Data nadania:').' '.substr($item->date_entered,0,10);
            $pdf->MultiCell(50,15,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);


            $cellContent = $item->senderAddressData->company != '' ? $item->senderAddressData->company."\r\n" : '';
            $cellContent .= $item->senderAddressData->name != '' ? $item->senderAddressData->name."\r\n" : '';
            $cellContent .= $item->senderAddressData->address_line_1."\r\n";
            $cellContent .= $item->senderAddressData->address_line_2 != '' ? $item->senderAddressData->address_line_2."\r\n" : '';
            $cellContent .= $item->senderAddressData->zip_code.', ';
            $cellContent .= $item->senderAddressData->city."\r\n";
            $cellContent .= $item->senderAddressData->country;

            $pdf->MultiCell(50,15,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $cellContent = $item->receiverAddressData->company != '' ? $item->receiverAddressData->company."\r\n" : '';
            $cellContent .= $item->receiverAddressData->name != '' ? $item->receiverAddressData->name."\r\n" : '';
            $cellContent .= $item->receiverAddressData->address_line_1."\r\n";
            $cellContent .= $item->receiverAddressData->address_line_2 != '' ? $item->receiverAddressData->address_line_2."\r\n" : '';
            $cellContent .= $item->receiverAddressData->zip_code.', ';
            $cellContent .= $item->receiverAddressData->city."\r\n";
            $cellContent .= $item->receiverAddressData->country;

            $pdf->MultiCell(50,15,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);

            $cellContent = Yii::t('courier_pdf','Waga').' ['.Courier::getWeightUnit().']: '.$item->package_weight;
            $cellContent .= "\r\n".Yii::t('courier_pdf','Wymiary').' ['.Courier::getDimensionUnit().']: '.$item->package_size_d . ' x ' . $item->package_size_l . ' x ' . $item->package_size_w;
            $cellContent .= "\r\n".Yii::t('courier_pdf','Wartość').': '.$item->package_value.' '.$item->getPackageValueCurrency();
            $cellContent .= "\r\n".Yii::t('courier_pdf','Zawartość:').' '.$item->package_content;

            $cellContent .= "\r\n".Yii::t('courier_pdf','Kwota pobrania:').' '.($item->cod_value ? $item->cod_value.' '.$item->cod_currency : '-');

            $pdf->MultiCell(40,15,$cellContent,1,'L',false,0,'','',true,0,false,true,0,'T',true);
            $pdf->ln(15);

            // break first page earlier to make space for header
            if($page == 1 && $pageI > 0  && ($pageI%($pageSize-$firstPageSizeOffset) == 0))
            {

                $pdf->SetFont('freesans', '', 8);
                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

                $pdf->AddPage('P','A4');

                if($i < ($totalNumber-1)) {
                    self::printTableHeader($pdf);
                }

                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }
            // break last page earlier to make space for footer
            else if($page > 1 && $page == $totalPages && ($pageI%($pageSize-$lastPageSizeOffset) == 0))
            {

                $pdf->SetFont('freesans', '', 8);
                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

                $pdf->AddPage('P','A4');
                if($i < ($totalNumber-1)) {
                    self::printTableHeader($pdf);
                }

                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }
            else if(($pageI%$pageSize) == 0 && $pageI > 0)
            {

                $pdf->SetFont('freesans', '', 8);
                $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
                $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

                $pdf->AddPage('P','A4');
                if($i < ($totalNumber-1)) {
                    self::printTableHeader($pdf);
                }

                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPages,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }

            $i++;
            $pageI++;
        }

        $pdf->SetAbsY($pdf->GetY() + 20);

//        $pdf->SetFont('freesans', '', 8);
//        $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//        $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

        /* @var $pdf TCPDF */
        $pdf->Output('stat-card.pdf', 'D');
    }

    public static function generateCard(Courier $item)
    {

        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();
        $pdf->setMargins(10,10,10, true);

        $pdf->AddPage('P','A4');

//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);

//        $pdf->Image('@'.PDFGenerator::getLogoPlBig($item->user), $pdf->getPageWidth() /2 - 15 - $pdf->getMargins()['right'], 5 , 50,'','','N',false);

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->SetY($pdf->GetY() + 15);
        $pdf->SetTextColor(0,0,0);

        $pdf->SetFont('freesans', '', 20);
        $y = $pdf->GetY();
        $pdf->MultiCell(0, 0, Yii::t('courier_pdf','Potwierdzenie statusu'), 1, 'L', false);

        $pdf->MultiCell(0, 0, "#".$item->local_id, 0, 'R', false, 1, 15, $y, true, 0, false, true, 0);

        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'B', 15);
        $pdf->MultiCell(0,0, Yii::t('courier_pdf','Data wygenerowania potwierdzenia:'), 0, 'L', false, 1);
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() +2);
        $pdf->MultiCell(0,0, date('Y-m-d H:i'), 0, 'L', false, 1);

        $pdf->SetAbsY($pdf->GetY() + 5);

        if($item->stat !== NULL)
        {
            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0,0, Yii::t('courier_pdf','Status:'), 0, 'L', false, 1);
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() +2);
            $pdf->MultiCell(0,0, $item->stat->getStatText(true), 0, 'L', false, 1);

            $pdf->SetAbsY($pdf->GetY() + 5);
        }


        $pdf->SetFont('freesans', 'B', 15);
        $pdf->MultiCell(0,0, Yii::t('courier_pdf','Nadawca:'), 0, 'L');
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() -5);
        $data = array(
            array(Yii::t('courier_pdf','Firma:'), $item->senderAddressData->company),
            array(Yii::t('courier_pdf','Imię i nazwisko:'), $item->senderAddressData->name),
            array(Yii::t('courier_pdf','Adres:'), $item->senderAddressData->address_line_1),
            array(Yii::t('courier_pdf','Adres c.d.:'), $item->senderAddressData->address_line_2),
            array(Yii::t('courier_pdf','Kod pocztowy:'), $item->senderAddressData->zip_code),
            array(Yii::t('courier_pdf','Miasto:'), $item->senderAddressData->city),
            array(Yii::t('courier_pdf','Kraj:'), $item->senderAddressData->country),
        );
        self::ColoredTable($pdf, $data);

        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'B', 15);
        $pdf->MultiCell(0,0, Yii::t('courier_pdf','Odbiorca:'), 0, 'L', false, 1);
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() -5);
        $data = array(
            array(Yii::t('courier_pdf','Firma:'), $item->receiverAddressData->company),
            array(Yii::t('courier_pdf','Imię i nazwisko:'), $item->receiverAddressData->name),
            array(Yii::t('courier_pdf','Adres:'), $item->receiverAddressData->address_line_1),
            array(Yii::t('courier_pdf','Adres c.d.:'), $item->receiverAddressData->address_line_2),
            array(Yii::t('courier_pdf','Kod pocztowy:'), $item->receiverAddressData->zip_code),
            array(Yii::t('courier_pdf','Miasto:'), $item->receiverAddressData->city),
            array(Yii::t('courier_pdf','Kraj:'), $item->receiverAddressData->country),
        );
        self::ColoredTable($pdf, $data);

        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'B', 15);
        $pdf->MultiCell(0,0, Yii::t('courier_pdf','Szczegóły:'), 0, 'L', false, 1);
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() -5);
        $data = array(
            array(Yii::t('courier_pdf','Data nadania:'), substr($item->date_entered, 0, 10)),
            array(Yii::t('courier_pdf','Waga przesyłki').' ['.Courier::getWeightUnit().']:', $item->package_weight),
            array(Yii::t('courier_pdf','Wymiary przesyłki').' ['.Courier::getDimensionUnit().']:', $item->package_size_d.' x '.$item->package_size_l.' x '.$item->package_size_w),
            array(Yii::t('courier_pdf','Zawartość:'), $item->package_content),
            array(Yii::t('courier_pdf','Wartość').':', $item->package_value.' '.$item->getPackageValueCurrency()),
        );

        array_push($data, array(Yii::t('courier_pdf','Kwota pobrania:'), $item->cod_value ? $item->cod_value.' '.$item->cod_currency : '-'));

        if($item->courierTypeInternal) {

            array_push($data, array(Yii::t('courier_pdf','Cena usługi:'), $item->courierTypeInternal->order->value_brutto.' '.$item->courierTypeInternal->order->currency));
        }

        self::ColoredTable($pdf, $data);


//        $pdf->SetFont('freesans', '', 8);
//        $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//        $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);

        /* @var $pdf TCPDF */
        $pdf->Output('stat-card.pdf', 'D');
    }

    protected static function ColoredTable(TCPDF $pdf,$data, $header = array()) {
        // Colors, line width and B font
        $pdf->SetFillColor(255, 0, 0);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(128, 0, 0);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFont('', 'B', 10);
        // Header
        $w = array(40, 100);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $pdf->Ln();
        // Color and font restoration
        $pdf->SetFillColor(224, 235, 255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('');
        // Data
        $fill = 0;
        $i = 0;
        $size = S_Useful::sizeof($data);
        foreach($data as $row) {
            $border = 'LR';
            if(!$i) {
                if($size - 1)
                    $border = 'LRT';
                else
                    $border = 'LRTB';
            }
            else if($i == $size - 1)
                $border = 'LRB';


            $pdf->Cell($w[0], 6, $row[0].'  ', $border, 0, 'R', $fill);
            $pdf->Cell($w[1], 6, '  '.$row[1], $border, 0, 'L', $fill);

            $pdf->Ln();
            // $fill=!$fill;
            $i++;
        }

    }
}
