<?php

class S_CourierIO
{

    public static function exportToXls(&$models, $modelsAsDataReaderObj = false)
    {
        $courierAttributes = [
            'local_id',
            'date_entered',
            'courier_stat_id',
            'package_weight',
            'package_size_l',
            'package_size_w',
            'package_size_d',
            'packages_number',
            'package_value',
            'package_content',
            'cod',
            'user_id',
            'cod_currency',
            'cod_value',
            'ref',
            'date_delivered',
            'date_attempted_delivery',
            'package_weight_client'
        ];

        $addressDataAttributes = [
            'name',
            'company',
            'country',
            'city',
            'zip_code',
            'address_line_1',
            'address_line_2',
            'tel',
            'email'
        ];

        $addressDataAttributesSender = array_merge($addressDataAttributes, [
            'bankAccount',
        ]);;




//        file_put_contents('xls_dump.txt', $i++."\r\n", FILE_APPEND);
//        file_put_contents('xls_dump2.txt', sizeof($models)."\r\n", FILE_APPEND);

        if($modelsAsDataReaderObj)
            return self::_exportToXls2($models, $courierAttributes, $addressDataAttributes, true, true, $addressDataAttributesSender, false, false, $modelsAsDataReaderObj);
        else
            return self::_exportToXls($models, $courierAttributes, $addressDataAttributes, true, true, $addressDataAttributesSender, false, false, $modelsAsDataReaderObj);


    }

    public static function exportToXlsUser(array &$models, $separateTypes = false, $returnString = false)
    {
        $courierAttributes = [
            'local_id',
            'date_entered',
            'courier_stat_id',
//            'stat.name',
            'package_weight',
            'package_size_l',
            'package_size_w',
            'package_size_d',
            'packages_number',
            'package_value',
            'package_content',
            'cod',
            'cod_currency',
            'cod_value',
            'ref',
            'date_delivered',
        ];

        $addressDataAttributes = [
            'name',
            'company',
            'country',
            'city',
            'zip_code',
            'address_line_1',
            'address_line_2',
            'tel',
            'email'
        ];

        $addressDataAttributesSender = array_merge($addressDataAttributes, [
            'bankAccount',
        ]);;


        return self::_exportToXls($models, $courierAttributes, $addressDataAttributes, true, true, $addressDataAttributesSender, true, $returnString);
    }

    protected static function _exportToXls($models, array $courierAttributes, array $addressDataAttributes, $addPackageType = false, $addExternalIds = true, $senderAddressDataAttributes = false, $forUser = false, $returnString = false, $modelsAsDataReaderObj = false)
    {
        $done = [];

        if(!$senderAddressDataAttributes)
            $senderAddressDataAttributes = $addressDataAttributes;

        // specific fileds for particular courier types
        $specificHeader = [];

//        array_push($specificHeader, 'domestic COD');

        $packageTypeHeader = [];
        if($addPackageType)
        {
            array_push($packageTypeHeader, 'type');
        }

        $packageExternalIdsHeader = [];
        if($addExternalIds)
        {
            array_push($packageExternalIdsHeader, 'external ids');
        }

        $senderHeader = [];
        foreach($senderAddressDataAttributes AS $key => $item)
        {
            $senderAttributes[$key] = 'sender '.$item;
        }

        $receiverHeader = [];
        foreach($addressDataAttributes AS $key => $item)
            $receiverAttributes[$key] = 'receiver '.$item;

        // add status name:
        $courierAttributesWithStat = $courierAttributes;
        $courierAttributesWithStat[] = 'stat name';

        // add status map name && last date:
        $statMapHead = [];
        $statMapHead[] = 'stat map name';
        $statMapHead[] = 'stat map last date';

        $arrayHeader = array_merge($courierAttributesWithStat, $packageTypeHeader, $senderAttributes, $receiverAttributes, $packageExternalIdsHeader, $specificHeader, $statMapHead);


        if(!$forUser)
            $arrayHeader[] = 'salesman';

        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Order ID' : 'note1';
        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Optional Reference' : 'note2';
        $arrayHeader[] = 'date_activated';
        $arrayHeader[] = 'date_delivered';

        $array = [];
        $array[0] = $arrayHeader;

        $i = 1;


        foreach($models AS $iter => $model)
        {
            // Create AR model with raw array of data
            if($modelsAsDataReaderObj) {
                $model = Courier::model()->populateRecord($model);
            }



            //            $model = $models[$iter];

            $packageTypeData = [];
            if($addPackageType)
            {
                array_push($packageTypeData, $model->getTypeName($forUser ? true : false));
            }

            $packageExternalIds = [];
            if($addExternalIds)
            {
                $externals = CourierExternalManager::listExternalServices($model);

                foreach($externals AS $external)
                {
                    array_push($packageExternalIds, CourierExternalManager::getNameForType($external['type']).';'.$external['id']);
                }
            }

            // merge all external ids into one cell
            $packageExternalIds = [implode('||', $packageExternalIds)];

            if($model->senderAddressData == NULL)
                $model->senderAddressData = new AddressData;

            if($model->receiverAddressData == NULL)
                $model->receiverAddressData = new AddressData;


            $dataCourier = $model->getAttributes($courierAttributes);
            // add status name:
            $dataCourier[] = $model->stat->name;

//            $dataTypeInternal =  array_combine($typeInternalAttributes, array_fill(0,S_Useful::sizeof($typeInternalAttributes),''));
//            if($model->courierTypeInternal != NULL)
//                $dataTypeInternal = $model->courierTypeInternal->getAttributes($typeInternalAttributes);

//            $dataSender = $model->senderAddressData->getAttributes($senderAddressDataAttributes);
            $dataReceiver = $model->receiverAddressData->getAttributes($addressDataAttributes);


            $temp = [];
            foreach($senderAddressDataAttributes AS $key => $item)
                $temp['sender_'.$key] = $model->senderAddressData->$item;
            $dataSender = $temp;

            $temp = [];
            foreach($dataReceiver AS $key => $item)
                $temp['receiver_'.$key] = $item;
            $dataReceiver = $temp;

            $specificContent = [];
//            if($model->courierTypeDomestic)
//            {
//                array_push($specificContent, $model->courierTypeDomestic->cod_value.' '.$model->courierTypeDomestic->cod_currency);
//            } else {
//                array_push($specificContent, '');
//            }

            // add status map name && last date:
            $statMap = [];
            $statMap[] = $model->statMapName;
            $statMap[] = $model->getLastStatMapUpdate();




            $array[$i] = array_merge($dataCourier, $packageTypeData, $dataSender, $dataReceiver, $packageExternalIds, $specificContent, $statMap);


            if(!$forUser)
                $array[$i][] = $model->user->salesman->name;

            $array[$i][] = $model->note1;
            $array[$i][] = $model->note2;
            $array[$i][] = $model->date_activated;
            $array[$i][] = $model->date_delivered;


            $i++;

            $done[$iter] = $model->id;

            if($modelsAsDataReaderObj)
                unset($model);
            else
                unset($models[$iter]);

            gc_collect_cycles();



        }

        $xls = S_XmlGenerator::generateXml($array, false, $returnString);

        if($returnString)
            return $xls;
        else
            return $done;

    }


    protected static function _exportToXls2(&$modelsBase, array $courierAttributes, array $addressDataAttributes, $addPackageType = false, $addExternalIds = true, $senderAddressDataAttributes = false, $forUser = false, $returnString = false, $modelsAsDataReaderObj = false)
    {

        $name = 'export_'.date('Y-m-d');

        $done = [];

        if(!$senderAddressDataAttributes)
            $senderAddressDataAttributes = $addressDataAttributes;

        // specific fileds for particular courier types
        $specificHeader = [];

//        array_push($specificHeader, 'domestic COD');

        $packageTypeHeader = [];
        if($addPackageType)
        {
            array_push($packageTypeHeader, 'type');
        }

        $packageExternalIdsHeader = [];
        if($addExternalIds)
        {
            array_push($packageExternalIdsHeader, 'external ids');
        }

        $senderHeader = [];
        foreach($senderAddressDataAttributes AS $key => $item)
        {
            $senderAttributes[$key] = 'sender '.$item;
        }

        $receiverHeader = [];
        foreach($addressDataAttributes AS $key => $item)
            $receiverAttributes[$key] = 'receiver '.$item;

        // add status name:
        $courierAttributesWithStat = $courierAttributes;
        $courierAttributesWithStat[] = 'stat name';

        // add status map name && last date:
        $statMapHead = [];
        $statMapHead[] = 'stat map name';
        $statMapHead[] = 'stat map last date';

        if(!$forUser)
            $statMapHead[] = 'salesman';

        $arrayHeader = array_merge($courierAttributesWithStat, $packageTypeHeader, $senderAttributes, $receiverAttributes, $packageExternalIdsHeader, $specificHeader, $statMapHead);

        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Order ID' : 'note1';
        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Optional Reference' : 'note2';
        $arrayHeader[] = 'date_activated';
        $arrayHeader[] = 'date_delivered';



//        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
//        $cacheSettings = array( 'memoryCacheSize' => '512MB');
//        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

//        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

//        $doc->setActiveSheetIndex(0);
//        $doc->getActiveSheet()->fromArray($arrayHeader, '', 'A1');



//        $spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();
//        $sheet = $spreadsheet->getActiveSheet();


//        $sheet->fromArray($arrayHeader, '', 'A1');
        $writer = Box\Spout\Writer\WriterFactory::create(Box\Spout\Common\Type::XLSX); // for XLSX files
        $writer->openToBrowser('export.xlsx');
        $writer->addRow($arrayHeader);

//        $array = [];
//        $array[0] = $arrayHeader;

        $i = 1;

        $CHUNK_SIZE = 15000;

        if($modelsAsDataReaderObj)
        {
            $modelsContainer = array_chunk($modelsBase, $CHUNK_SIZE);
        } else
            $modelsContainer = [ $modelsBase ];

        unset($modelsBase);

$i = 0;
//        MyDump::dump('exportOpt2.txt', $CHUNK_SIZE.': START: '.print_r(self::convert(memory_get_usage(true)),1));
        foreach($modelsContainer AS $keyContainer => &$models) {

//            MyDump::dump('exportOpt2.txt', 'CHUNK ('.$keyContainer.'): '.print_r(self::convert(memory_get_usage(true)),1));

            if($modelsAsDataReaderObj)
                $modelsAr = Courier::model()->with('senderAddressData')->with('receiverAddressData')->with('courierTypeInternal')->with('user')->findAllByAttributes(['id' => $models]);

            unset($modelsContainer[$keyContainer]);

            foreach ($modelsAr AS $iter => &$model) {

                if($modelsAsDataReaderObj)
                    $iter = $iter + $CHUNK_SIZE * $keyContainer;

//                // Create AR model with raw array of data
//                if ($modelsAsDataReaderObj) {
//                    $model = Courier::model()->populateRecord($model);
//                }


                //            $model = $models[$iter];

                $packageTypeData = [];
                if ($addPackageType) {
                    array_push($packageTypeData, $model->getTypeName($forUser ? true : false));
                }

                $packageExternalIds = [];
                if ($addExternalIds) {
                    $externals = CourierExternalManager::listExternalServices($model);

                    foreach ($externals AS $external) {
                        array_push($packageExternalIds, CourierExternalManager::getNameForType($external['type']) . ';' . $external['id']);
                    }
                }

                // merge all external ids into one cell
                $packageExternalIds = [implode('||', $packageExternalIds)];

                if ($model->senderAddressData == NULL)
                    $model->senderAddressData = new AddressData;

                if ($model->receiverAddressData == NULL)
                    $model->receiverAddressData = new AddressData;


                $dataCourier = $model->getAttributes($courierAttributes);
                // add status name:
                $dataCourier[] = $model->stat->name;

//            $dataTypeInternal =  array_combine($typeInternalAttributes, array_fill(0,S_Useful::sizeof($typeInternalAttributes),''));
//            if($model->courierTypeInternal != NULL)
//                $dataTypeInternal = $model->courierTypeInternal->getAttributes($typeInternalAttributes);

//            $dataSender = $model->senderAddressData->getAttributes($senderAddressDataAttributes);
                $dataReceiver = $model->receiverAddressData->getAttributes($addressDataAttributes);


                $temp = [];
                foreach ($senderAddressDataAttributes AS $key => $item)
                    $temp['sender_' . $key] = $model->senderAddressData->$item;
                $dataSender = $temp;

                $temp = [];
                foreach ($dataReceiver AS $key => $item)
                    $temp['receiver_' . $key] = $item;
                $dataReceiver = $temp;

                $specificContent = [];
//            if($model->courierTypeDomestic)
//            {
//                array_push($specificContent, $model->courierTypeDomestic->cod_value.' '.$model->courierTypeDomestic->cod_currency);
//            } else {
//                array_push($specificContent, '');
//            }

                // add status map name && last date:
                $statMap = [];
                $statMap[] = $model->statMapName;
                $statMap[] = $model->getLastStatMapUpdate();

//                $array[$i] = array_merge($dataCourier, $packageTypeData, $dataSender, $dataReceiver, $packageExternalIds, $specificContent, $statMap);

                $i++;

                $temp = array_merge($dataCourier, $packageTypeData, $dataSender, $dataReceiver, $packageExternalIds, $specificContent, $statMap);

                if(!$forUser)
                    $temp[] = $model->user ? $model->user->getSalesmanName() : '';

                $temp[] = $model->note1;
                $temp[] = $model->note2;
                $temp[] = $model->date_activated;
                $temp[] = $model->date_delivered;

                $writer->addRow($temp);

//                $doc->getActiveSheet()->fromArray(array_merge($dataCourier, $packageTypeData, $dataSender, $dataReceiver, $packageExternalIds, $specificContent, $statMap), '', 'A'.$i);
//                unset($line);


                $done[$iter] = $model->id;

//                if ($modelsAsDataReaderObj)
//                    unset($model);
//                else
//                    unset($models[$iter]);
                $modelsAr[$iter] = NULL;
                unset($modelsAr[$iter]);
                $model = NULL;
                unset($model);
            }

//            MyDump::dump('exportOpt2.txt', 'CHUNK BEFORE END: '.print_r(self::convert(memory_get_usage(true)),1));
            $modelsAr = NULL;
            unset($modelsAr);
            gc_collect_cycles();
//            MyDump::dump('exportOpt2.txt', 'CHUNK END: '.print_r(self::convert(memory_get_usage(true)),1));
        }

        $modelsContainer = NULL;
        unset($modelsContainer);
//        gc_collect_cycles();
//        MyDump::dump('exportOpt2.txt', 'BEFORE TO XLS: '.print_r(self::convert(memory_get_usage(true)),1));

//        $array = array_chunk($array, 5000);
//
//        $i = 0;
//        foreach($array AS &$chunk)
//        {
//            MyDump::dump('exportOpt2.txt', 'XLS CHUNK START ('.$i.'): '.print_r(self::convert(memory_get_usage(true)),1));
//
//            $sheet->fromArray($chunk, '', 'A'.(1+($i*5000)));
//
////            $doc->getActiveSheet()->fromArray($chunk, '', 'A'.(1+($i*5000)));
//
//            $chunk = NULL;
//            unset($chunk);
            gc_collect_cycles();
//
//            $i++;
//            MyDump::dump('exportOpt2.txt', 'XLS CHUNK END ('.$i.'): '.print_r(self::convert(memory_get_usage(true)),1));
//        }

        $array = NULL;
        unset($array);
//        gc_collect_cycles();
//
//        MyDump::dump('exportOpt2.txt', 'END: '.print_r(self::convert(memory_get_usage(true)),1));

//        $doc->getActiveSheet()->fromArray($array, '');



        $filename = $name . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0'); //no cache

//        $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
//        $writer->save('php://output');
        $writer->close();
//        $objWriter->save('php://output');



    }

    protected static function convert($size)
    {
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }

}