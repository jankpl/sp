<?php

class Fakturownia
{
    private $host = GLOBAL_CONFIG::FAKTUROWNIA_HOST;
    private $token = GLOBAL_CONFIG::FAKTUROWNIA_TOKEN;
    private $sellerName;
    private $sellerTaxNo;
    private $sellerBankAccount;
    private $sellerPostCode;
    private $sellerCity;
    private $sellerStreet;


    const ACCOUNT_SP = 1;
    const ACCOUNT_KEDZIERZYN = 2;
    const ACCOUNT_SP_SPZOO = 3;

    public static function listAccounts()
    {
        return [
            self::ACCOUNT_SP => 'SwiatPrzesylek',
            self::ACCOUNT_SP_SPZOO => 'SwiatPrzesylek SP. Z O.O.',
            self::ACCOUNT_KEDZIERZYN => 'Kędzierzyn',
        ];
    }

    public function setAccount($id = self::ACCOUNT_SP)
    {
        switch($id)
        {
            case self::ACCOUNT_SP:
            default:
                $this->host = GLOBAL_CONFIG::FAKTUROWNIA_HOST;
                $this->token = GLOBAL_CONFIG::FAKTUROWNIA_TOKEN;
                $this->sellerName = 'Świat Przesyłek Piotr Kocoń';
                $this->sellerTaxNo = '749-203-54-49';
                $this->sellerBankAccount = '26105014901000009137775475';
                $this->sellerPostCode = '48-300';
                $this->sellerCity = 'Nysa';
                $this->sellerStreet = 'Grodkowska 40';
                break;
            case self::ACCOUNT_SP_SPZOO:
                $this->host = GLOBAL_CONFIG::FAKTUROWNIA_HOST;
                $this->token = GLOBAL_CONFIG::FAKTUROWNIA_TOKEN;
                $this->sellerName = 'Świat Przesyłek SP. Z O.O.';
                $this->sellerTaxNo = '753-244-94-60';
                $this->sellerBankAccount = 'EUR 05 1050 1490 1000 0090 3153 6700<br/><br/>ING, SWIFT: INGBPLPW<br/>PLN 27 1050 1490 1000 0090 3153 6692';
                $this->sellerPostCode = '48-300';
                $this->sellerCity = 'Nysa';
                $this->sellerStreet = 'Grodkowska 40';
                break;
            case self::ACCOUNT_KEDZIERZYN:
                $this->host = 'pawelwazny.fakturownia.pl';
                $this->token = '1pVCOe6OFQHgwI5JSEHw/pawelwazny';
                $this->sellerName = 'PWC Paweł Ważny';
                $this->sellerTaxNo = '7492019396';
                $this->sellerBankAccount = '86109021830000000132211015';
                $this->sellerPostCode = '47-232';
                $this->sellerCity = 'Kędzierzyn-Koźle';
                $this->sellerStreet = 'Gajdzika 6';
                break;
        }
    }



    protected static function makeCall(stdClass $extendedData, $ignoreNip = false, $account = false, $source = '', $sellDate = false, $issueDate = false, $paymentTo = false)
    {
        $id = uniqid();

        $model = new self;
        $model->setAccount($account);

        $return = [
            'success' => false,
        ];

        $data = $extendedData;

        $data->api_token = $model->token;
        $data->invoice->sell_date = $sellDate ? $sellDate : date('Y-m-d');
        $data->invoice->issue_date = $issueDate ? $issueDate : date('Y-m-d');
        $data->invoice->payment_to = $paymentTo ? $paymentTo : date('Y-m-d');
        $data->invoice->seller_name = $model->sellerName;
        $data->invoice->seller_tax_no = $model->sellerTaxNo;
        $data->invoice->seller_street = $model->sellerStreet;
        $data->invoice->seller_country = 'PL';
        $data->invoice->seller_post_code = $model->sellerPostCode;
        $data->invoice->seller_bank_account = $model->sellerBankAccount;

        MyDump::dump('fakturownia.txt', 'REQ DATA ('.$id.') ('.$source.'): '.print_r($data,1));
        $data = CJSON::encode($data);

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, 'https://'.$model->host.'/invoices.json');
        $head[] ='Accept: application/json';
        $head[] ='Content-Type: application/json';
        curl_setopt($c, CURLOPT_HTTPHEADER, $head);
        curl_setopt($c, CURLOPT_POSTFIELDS, $data);

        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($c);

        $result = CJSON::decode($result);
        MyDump::dump('fakturownia.txt','RESP DATA ('.$id.'): '.print_r($result,1));

        if($result['code'] == 'error')
        {
            if(isset($result['message']['buyer_tax_no']) && !$ignoreNip)
            {
                MyDump::dump('fakturownia.txt', 'Try again without NIP...');

                return self::makeCall($extendedData, true, $account);
            }
            $return['error'] = print_r($result['message'],1);
        } else {

            $pdfPath = $result['view_url'] . '.pdf';
            $pdf = @file_get_contents($pdfPath);

            if ($pdf) {
                $return = [
                    'success' => true,
                    'pdf' => base64_encode($pdf),
                    'no' => $result['number'],
                    'pdfLink' => $result['view_url'] . '.pdf',
                ];
            } else {
                $return['error'] = print_r($result,1);
            }
        }

        return $return;

    }


    public static function createByOrder(Order $order, $ignoreNip = false)
    {
        if($order->inv_address_data_id)
            $addressData = $order->addressDataOrder;
        else
        {
            $addressData = $order->user->getAddressDataOrderModel();
        }


        $data = new stdClass();
        $data->invoice = new stdClass();
        $data->invoice->buyer_name = trim($addressData->name.' '.$addressData->company);
        $data->invoice->buyer_post_code = $addressData->zip_code;
        $data->invoice->buyer_city = $addressData->city;
        $data->invoice->buyer_street = trim($addressData->address_line_1.' '.$addressData->address_line_2);
        $data->invoice->buyer_note = $order->user->login;
        $data->invoice->buyer_country = $addressData->country0->code;
        if($addressData->_nip && !$ignoreNip)
            $data->invoice->buyer_tax_no = $addressData->_nip;

        $data->invoice->positions = [];
        $data->invoice->paid = $order->value_brutto;
        $data->invoice->paid_date = date('Y-m-d');
        $data->invoice->currency = $order->currency;
        $data->invoice->lang = $order->user->getPrefLang();

        $lang = substr($order->user->getPrefLang(),0,2);

        $namePrefix = 'Zamówienie';
        if($lang == 'en')
            $namePrefix = 'Order';
        else if($lang == 'de')
            $namePrefix = 'Bestellen';

        $nameSuffix = [];
        /* @var $item OrderProduct */
        foreach($order->orderProduct AS $item)
        {
            $temp = $item->getProductModel();
            if($temp)
                $nameSuffix[] = '#'.$temp->local_id;
        }


        $nameSuffix = implode(', ', $nameSuffix);
        $nameSuffix = ' ('.$nameSuffix.')';

        $item = new stdClass();
        $item->name = $namePrefix.' #'.$order->local_id.$nameSuffix;
        $item->total_price_net = $order->value_netto;
        $item->total_price_gross = $order->value_brutto;
        $item->tax = round((100 * ($item->total_price_gross / $item->total_price_net)) - 100);
        $item->quantity = 1;

        $data->invoice->positions[] = $item;
        $data->invoice->kind = 'vat';

        if($lang == 'en')
            $data->invoice->payment_type = 'Payment online (Przelewy 24)';
        else if($lang == 'de')
            $data->invoice->payment_type = 'Zahlung online (Przelewy 24)';
        else
            $data->invoice->payment_type = 'Płatność online (Przelewy 24)';

        return self::makeCall($data, $ignoreNip, false, 'byOrder');

    }

    public static function createByFakturowniaForm(FakturowniaForm $form)
    {

        $addressData = $form->addressData_receiver;

        $data = new stdClass();
        $data->invoice = new stdClass();

        if($form->number != '')
            $data->invoice->number = $form->number;
        $data->invoice->buyer_name = trim($addressData->name.' '.$addressData->company);
        $data->invoice->buyer_post_code = $addressData->zip_code;
        $data->invoice->buyer_city = $addressData->city;
        $data->invoice->buyer_street = trim($addressData->address_line_1.' '.$addressData->address_line_2);


        $data->invoice->description = $form->description;

        if($form->saveToUserInvoice)
            $data->invoice->buyer_note = $form->getUserLogin();

        $data->invoice->buyer_country = $addressData->country0->code;

        if($addressData->_nip)
            $data->invoice->buyer_tax_no = $addressData->_nip;


        $force23vat = false;
        if($addressData->country_id != CountryList::COUNTRY_PL && $addressData->_nip)
        {
            if(!ViesCheck::check($addressData->_nip))
                $force23vat = true;
        }

        $data->invoice->positions = [];

        $data->invoice->paid = ($form->isPaid && !$form->correction) ? $form->calculateTotalPaymentValue() : 0;

        if($form->isPaid && !$form->correction)
            $data->invoice->paid_date = $form->paidDate;

        if($form->correction)
        {
            /* @var $formItem FakturowniaItem */
            foreach ($form->itemsBefore AS $key => $formItem) {

                $formAfterItem = $form->itemsAfter[$key];

                $item = new stdClass();

                $item->name = $formItem->name;
                $item->quantity = $formAfterItem->no - $formItem->no;
                $item->total_price_gross = ($formAfterItem->value_brutto * $formAfterItem->no) - ($formItem->value_brutto * $formItem->no);


                $taxBefore = doubleval($formItem->value_netto) ? round((100 * ($formItem->value_brutto / $formItem->value_netto)) - 100) : NULL;

                $item->correction_before_attributes = new stdClass();
                $item->correction_before_attributes->name = $formItem->name;
                $item->correction_before_attributes->quantity = $formItem->no;
                $item->correction_before_attributes->total_price_gross = $formItem->value_brutto * $formItem->no;
                $item->correction_before_attributes->tax = $taxBefore;
                $item->correction_before_attributes->kind = 'correction_before';



                $taxAfter = doubleval($formAfterItem->value_netto) ? round((100 * ($formAfterItem->value_brutto / $formAfterItem->value_netto)) - 100) : NULL;

                $item->correction_after_attributes = new stdClass();
                $item->correction_after_attributes->name = $formAfterItem->name;
                $item->correction_after_attributes->quantity = $formAfterItem->no;
                $item->correction_after_attributes->total_price_gross = $formAfterItem->value_brutto * $formAfterItem->no;
                $item->correction_after_attributes->tax = $taxAfter;
                $item->correction_after_attributes->kind = 'correction_after';

                $item->kind = 'correction';

                $data->invoice->positions[] = $item;
            }
        } else {

            /* @var $formItem FakturowniaItem */
            foreach ($form->items AS $formItem) {

                $netto = $formItem->value_netto;
                $brutto = $formItem->value_brutto;

                if ($netto == $brutto && $force23vat)
                    $brutto = round($netto * 1.23, 2);


                $item = new stdClass();
                $item->name = $formItem->name;
                $item->total_price_net = $netto * $formItem->no;
                $item->total_price_gross = $brutto * $formItem->no;
                $item->tax = doubleval($item->total_price_net) ? round((100 * ($item->total_price_gross / $item->total_price_net)) - 100) : 0;
                $item->quantity = $formItem->no;
                $data->invoice->positions[] = $item;
            }
        }

        $data->invoice->currency = Currency::nameById($form->currency);
        $data->invoice->lang = $form->lang;
        $data->invoice->kind = $form->correction ? 'correction' : 'vat';

        $data->invoice->payment_type = $form->getPaymentTypeDesc();

        $return = self::makeCall($data, false, $form->fakturowniaAccountId, 'byForm', $form->sellDate, $form->issueDate, $form->paymentToDate);

        $sheet_string = false;
        if($return['success'])
        {

            $return['userInoviceModel'] = false;


            if($form->saveToUserInvoice)
            {
                if($form->file_sheet)
                    $sheet_string = @file_get_contents($form->file_sheet->getTempName());

                $return['userInoviceModel'] = UserInvoice::createAndPublishWithFileString($form->saveToUserInvoice, $return['no'], $form->issueDate, $form->paymentToDate, $form->calculateTotalPaymentValue(),$form->currency,$form->userInvoiceDescription,$form->getUserInvoiceSource(),$return['pdf'], $form->isPaid, $sheet_string, NULL, NULL, $form->userInvoicePublish);
            }
        }

        return $return;
    }



}