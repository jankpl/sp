<?php

class CorrectionNote extends CFormModel
{

    public $place;
    public $date;
    public $receiverAddressModel;
    public $creatorAddressModel;
    public $textBefore;
    public $amount;
    public $currency;
    public $textAfter;
    public $payment;
    public $paymentDate;
    public $type;

    public $creator_name;
    public $receiver_name;

    public $saveToUserInvoice = false;
    public $userInvoiceDescription;
    public $file_sheet;

    protected $_no;

    const TYPE_CHARGE = 0;
    const TYPE_CREDIT = 1;

    public function init()
    {
        $this->date = date('Y-m-d');
        $this->receiverAddressModel = new AddressData_Order();
        $this->creatorAddressModel = new AddressData_Order();

        $this->receiverAddressModel->scenario = AddressData_Order::SCENARIO_AT_LEAST_ONE;
        $this->creatorAddressModel->scenario = AddressData_Order::SCENARIO_AT_LEAST_ONE;

        $this->receiverAddressModel->name = 'Świat Przesyłek Piotr Kocoń';
        $this->receiverAddressModel->address_line_1 = 'Grodkowska 40';
        $this->receiverAddressModel->zip_code = '48-300';
        $this->receiverAddressModel->city = 'Nysa';
        $this->receiverAddressModel->_nip = '749-203-54-49';

        $this->place = 'Nysa';

        parent::init();
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_CHARGE => 'Charge',
            self::TYPE_CREDIT => 'Credit',
        ];
    }

    public static function getPaymentList()
    {
        return [
            1 => 'Przelewem',
        ];
    }

    public function attributeLabels()
    {
        return [
            'creator_name' => 'Signature - name',
            'receiver_name' => 'Signature - name',
        ];
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('place, date, textBefore, textAfter, amount, currency, type, creator_name, receiver_name, paymentDate', 'required'),
            array('payment', 'safe'),
            array('amount', 'numerical'),

            ['saveToUserInvoice', 'numerical'],
            ['userInvoiceDescription', 'length', 'max' => 128],
            ['file_sheet', 'file', 'allowEmpty'=>true, 'types'=>'xls, xlsx, pdf, doc, docx', 'maxSize' => 10448896, 'maxFiles' => 1,],
        );
    }

    public function create()
    {
        $this->_no = NumbersStorehouse::_getNumber(NumbersStorehouse::CORRECTION_NOTE, true);

        $sheet_string = false;
        if($this->saveToUserInvoice)
        {
            if($this->file_sheet)
                $sheet_string = @file_get_contents($this->file_sheet->getTempName());

            $pdf = $this->generateCorrectionNote(false, true);

            $return = UserInvoice::createAndPublishWithFileString($this->saveToUserInvoice, $this->_no, date('Y-m-d'), $this->paymentDate, $this->type == self::TYPE_CHARGE ? $this->amount : -$this->amount, $this->currency,$this->userInvoiceDescription,UserInvoice::SOURCE_CORRECTION_NOTE, $pdf, false, $sheet_string);

            return $return;
        } else
            $this->generateCorrectionNote();
    }





    public function generateCorrectionNote($pipelinePdf = false, $returnAsString = false)
    {
        /* @var $items Courier[] */

        /* @var $pdf TCPDF */
        if($pipelinePdf)
            $pdf = $pipelinePdf;
        else {
            $pdf = PDFGenerator::initialize();
            $pdf->setFontSubsetting(true);
        }




        $pdf->setMargins(5,5,5, true);


        $pdf->AddPage('P', 'A4');

//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);

//            $firstModel = reset($items);
            $pdf->Image('@' . self::_getLogo(), 80, 15, 50, '', '', 'N', false);




//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
//        $pdf->SetY($pdf->GetY() + 5);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->SetFont('freesans', 'B', 22);
        $pdf->MultiCell(0, 0, Yii::t('correction_note', 'Nota ksiegowa'), 0, 'L', false, 1, 5, 75);
        $pdf->SetFont('freesans', '', 14);
        $pdf->MultiCell(0, 0, 'Numer (No.) '.$this->_no, 0, 'L', false, 1,5,93);

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(0, 0, $this->place.', '.$this->date, 0, 'R', false, 1, 105, 75);


        $pdf->SetFont('freesans', 'B', 10);

        if($this->paymentDate)
            $pdf->MultiCell(0, 0, Yii::t('correction_note', 'Termin płatności').' (payment date): '.$this->paymentDate, 0, 'L', false, 1, 4, 113);

        if($this->payment)
            $pdf->MultiCell(0, 0, Yii::t('correction_note', 'Płatność').' (payment type): '.self::getPaymentList()[$this->payment], 0, 'L', false, 1, 4, 118);


        $pdf->MultiCell(0, 0, ($this->type == self::TYPE_CHARGE ? Yii::t('correction_note', 'Odbiorca').' (receipent)' : Yii::t('correction_note', 'Wystawca').' (exhibitor)').':', 0, 'L', false, 1, 4, 128);
        $pdf->MultiCell(0, 0, ($this->type == self::TYPE_CHARGE ? Yii::t('correction_note', 'Wystawca').' (exhibitor)' : Yii::t('correction_note', 'Odbiorca').' (receipent)').':', 0, 'L', false, 1, 109, 128);
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() - 5);

        $text = '';
        $text .= $this->creatorAddressModel->name ? $this->creatorAddressModel->name."\r\n" : '';
        $text .= $this->creatorAddressModel->company ? $this->creatorAddressModel->company."\r\n" : '';
        $text .= $this->creatorAddressModel->address_line_1 ? $this->creatorAddressModel->address_line_1."\r\n" : '';
        $text .= $this->creatorAddressModel->address_line_2 ? $this->creatorAddressModel->address_line_2."\r\n" : '';
        $text .= $this->creatorAddressModel->zip_code ? $this->creatorAddressModel->zip_code."\r\n" : '';
        $text .= $this->creatorAddressModel->city ? $this->creatorAddressModel->city."\r\n" : '';
        $text .= $this->creatorAddressModel->country0->code ? $this->creatorAddressModel->country0->code."\r\n" : '';
        $text .= $this->creatorAddressModel->_nip ? $this->creatorAddressModel->_nip."\r\n" : '';

        $pdf->MultiCell(95, 40, $text, 0, 'L', false, 1, 5, 133,
            true,
            0,
            false,
            true,
            40,
            'T',
            true
        );

        $text = '';
        $text .= $this->receiverAddressModel->name ? $this->receiverAddressModel->name."\r\n" : '';
        $text .= $this->receiverAddressModel->company ? $this->receiverAddressModel->company."\r\n" : '';
        $text .= $this->receiverAddressModel->address_line_1 ? $this->receiverAddressModel->address_line_1."\r\n" : '';
        $text .= $this->receiverAddressModel->address_line_2 ? $this->receiverAddressModel->address_line_2."\r\n" : '';
        $text .= $this->receiverAddressModel->zip_code ? $this->receiverAddressModel->zip_code."\r\n" : '';
        $text .= $this->receiverAddressModel->city ? $this->receiverAddressModel->city."\r\n" : '';
        $text .= $this->receiverAddressModel->country0->code ? $this->receiverAddressModel->country0->code."\r\n" : '';
        $text .= $this->receiverAddressModel->_nip ? $this->receiverAddressModel->_nip."\r\n" : '';

        $pdf->MultiCell(95, 40, $text, 0, 'L', false, 1, 110, 133,
            true,
            0,
            false,
            true,
            40,
            'T',
            true
        );



        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line(5,175,208, 175);



        $pdf->SetFont('freesans', '', 12);


        $text = $this->textBefore.' '.number_format($this->amount,2,","," ").' '.Currency::nameById($this->currency).' '.$this->textAfter;
        $pdf->MultiCell('', '', $text, 0, 'L', false, 1, 5, 188,
            true,
            0,
            false,
            true,
            60,
            'T',
            true
        );


        $pdf->SetFont('freesans', '0', 12);
        $pdf->MultiCell(95, 0, Yii::t('correction_note', 'Imię i nazwisko wystawcy:')."\r\n".'(exhibitor name and surname)', 0, 'C', false, 1, 4, 260);
        $pdf->MultiCell(95, 0, Yii::t('correction_note', 'Imię i nazwisko odbiorcy:')."\r\n".'(receipent name and surname)', 0, 'C', false, 1, 109, 260);
        $pdf->SetFont('freesans', '', 10);


        $pdf->MultiCell(95, 0, $this->creator_name, 0, 'C', false, 1, 4, 280);
        $pdf->MultiCell(95, 0, $this->receiver_name, 0, 'C', false, 1, 109, 280);



        /* @var $pdf TCPDF */
        if($pipelinePdf)
            return $pdf;
        else
        {
            if($returnAsString)
                return base64_encode($pdf->Output('correction_note.pdf', 'S'));
            else
                $pdf->Output('correction_note.pdf', 'D');
        }

    }

    protected static function _getLogo()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAALUAAAC0CAMAAADl0Z0+AAABXFBMVEX///8XEhQZEhT+/f0XEBMZEBT//f8aFBYYFBUVERL9//8VDxEbFhj7/PwZFhccGBn8+vv6+fkgHB0eGhv39fYjICEmIiMrJigwLC03NDUyLjD9/PwiHR/h3+Cqpqc5NjdRTU/e3d6QjY68ubpEQEIoJCawra5HQ0Sxr7DV09SopqZKRkc7NzlCPT8tKSr69/jy8fFZVlfo5ud/fH1APD3Pzc5pZmc+OTsSDhCuq6yrqKlTUFE1MTL4+PjZ19f//P308vPS0NC4tbZNSku0srJ5dndsaGpWU1S5t7h0cnJnZGX28/TJx8iLiImFgoNbWVr19PS2s7Shnp+Cf4B8eXrEwsOjoaKVkpNfXF3s6uuSkJFvbG1iXmAuKyzw7/Dc2dqYlZaIhYbl5OS+u7ybmJnw7e7q6Omem5xlYWPLycrj4eLNy8ympKXCwMGOi4xxb3DHxcXu7O3Avb6zr7G4g5sqAAArE0lEQVR42sSZiVfTQBDGM7uTnV2TJmmlFCgVj6LWiiBeIFoPRKWKiiIVERBFsV54/P/vuROs2pqW93wav7b7QjtNfnw7e2Tq/LGkVKp47eP86uqZxbdXlFQJMUqpn/GqGE0ct+E7jY1IXZGOs4/lpCcpHXm19n6+nAt9EOSPnr15e0IqKWUXaulkSzfOVAt+ABAEw4Nvntdk2tSZjJOdeF7NG02ABgC0MZfuHVJOpgu1fbt5v0DGMmMcPrX/dFNKJ1Vqxrg+CJ4hgyAEukYAFOZXos4s2YXmHui7Gdpw8AQIBEPCyz17l7GfpEgtHVk6PRWEGtEyABICCcgv1ZRMoOb4yYf94BMgIKInjLbHYf1DxknTa+WsPx0WgGhIYMyMnjsVFu6VOs2Oh5x0Xh04ZsjzPCKPBKBA1vD5jYyTpuSRakAAvkAUGoQnBLkhUPloNol6X/SuHmqLTC6ij74Rgo9hqDHhpCjZnPHBAMYpinFqE/8RjJUSqUs3c4AW2XVd/j9tI/hYjyxLJxVJbsZXc8blCwuwEkJYBCRjQjOdgKHUNiHZ2Da5ntBh9bXiiH+eKJKflVkfUVAbtUsA4G3yjPEb9RNPey61U6PwweuvMPW/NzymXiwI7mmrX6hRkNFvkqjvnCRtP2+nFogGgsfKkf+eOkaSy/s1MnYnNYYHk2acQwOI0JkhAlxN5iIzp+K1c3mB0BNil7rFzVQIx7Ic0MHRzMUzZGeGIIE+XLRGyH+OzUSXN0FrQPErNR/ZVzWJ+m5OT4FwO6m5/ShTS+tsAwA6qYUB4QXVjPM79bk8BL+NRle4GJiG5ARJR8v7wQCQYLWwySMyMJMUXlozwe95jb4BM51xMmlhH18DIINt1OgjmKCeFH75LBiBHdToGQj0J8dJzezaQQ2wC/2Tha0OFhLTqm69JuiQL0D315z0UiTaWjNgoI0aEQj2f0oKV18G48/bZQTkVvskI6eEvTLHGwvR5jaRF56fSKRebwwY0bnK+Ghm3jFveuPxy4w2Gn51W3vkz97OJlKrys4wIHQIL21HlpjdTkdRtF3wTRs1kB5prKhk6r4bM9Q5Gs3oUqnI8WlZvU/KF/cDgxpdJEGuRffA5DcPJftmDb38ftQAIfL86IK2DwhvNlPgbaeWGzuDOfJAIDE66tH64p1uvc03iMvlPCEgIVpqBH9seYXDU5TkZ9/je7P9BB6SsE15a10p6XTzWkaZa09P9yMbTmDo5OpdR0ZOqmbL+FGcPLIwltMeuoLC9xMq0xVCxq/PjxvPhglRQHn+xEa8YUnXbO5yLnaM127f9wkBaEsph2eEbtR25bbhiwNktDdw/GqkuL/SvdmN4bLxQfaJ4dU9eBZFjLavW4bYRl2Z2O4H1Lh2nI1OYXlJzBHJ9yljGiwJnHzLhadMIrUTxb1TVKWdELTx+5fHY+A08iOZvXnKACBAfjEei8nUu3wqmr4Q73DDzfW0eBNLBo7cylkM1GCWsnstGGrlPK9LKKB+6D9TP/IDBNQBPHmV6TEYWfLdoAEwBDBz7n9QKxVT86S9ajTfW2ko1+y7mZ5ebwfAm3KCCx+clJRMvfLEAPc54KlFpXqPrwd1MALJCzH/1UlfLWouGlRBCFeA0Ps3J1SvtJbyy4ieEohIaJ5Hf20+YK/6apVKZZ2vwQTcdKOOa6Xy2hBCXNEBLJ+TVsnAfK7XS3ltuALhCbNzVfYuuvz4IUGqzxuV2ousUopx9rXd00tGKdYa98sjgxdm5o884OW555IbUxdfjmJcUrAKN6/Y+K4Y8noVOENc8gCqL1RvB5k6Y4uxzcb5W2cHT1afzVW4csLXbI+8enTuVAAGPNAQzmxd210anJ7U8mJOwHeZ6ose1M6DhYJHGBdTAQbe9aJudXQ02ThlBKE2UxoGtw6NK9WWABwyXTZgFWhugny9IiNH9qLmvN4KCHalobDYDZqfk/UQfEBC9AEKx6/suamU2bsLa57xALS2XurR8zc+t+3QmP/uLGmBLBLkIQ3PfXacPbyWagEAW9i5RxOO7EpdGwMAPrlLYPobas/tgvpUXwMiHjWgiTDIzz683Nq9sGWOKjXGAuQ5zMW4Jm5jh+ufoj28luOr4EJLunokkt0w1HSoAZmb85p2+vaqga43TgYAzIKWCWJ3+rf7OGtb1K/mhgHIAgskf8pF4mg91uxVEOcvvjoN+ONuF/JLk129VqsaDGjkKwiAW5W98mO6MOUjM/O6JAygbzQNPc7K7xeXqjg9JDR21rSMPze5xw54sqoNAYvPGtz/pLpN8GoWflSpeCX9IntmiBrfAYgrzG1Fl/1LJSXjDLHtyrPA80QHtAa4cELKnp7UzoJpeW2MHjmRTYyzvrwe+pXajBxtnSJB1iq1UU6gtkTTxdaX1PSAJhTYWYgLcvPrvc1uDulWDwkPIJx70GUBk9dz9P3yfB1T2Mr0rDqpA8PfqduYRDi/wmsNh/Tt5DQimt9Lcd9INxOuto0gAGt2R5rdSlgyxjZg0oYESjnCFUhIgBRCgSYBAgRSKLkI4QikbWj//3vdWVvGh6w+pwNPPB7S+tPsXDu7rDxxUuUkT7EiUJCGoenk+0rv7ocqVprL8WZrO2VZIGV2DZKogR5esX3w9PWvkEFGaFQ26uJJR5zCkyS7CwKr1KAh2E+ecrM/KW5asAgqeH4oW25uSG4eQ7Jkzkue1bU3PK41hyRwa0UgYeaxk0Qd/zr5DIBiC3GBQB3LpGAjL/cKSK5rqdkUUQzdam0hUsqRjEpiRq1eLlWoN7qVbVOA24Dtqi8yjfp6CjRhrGtUqA6kbKaW0ef5EPGGmlCMLaflMLkZQiI1qXuLltokoW4BLgjAhjs0wG5aDvP6HxqGmNqG+4HpchehrhPiTbxeqMZVS+2K7g3JppOw+89mG21mkBVXDjo1TIE/1BUZ6g7Huyq6JFAHTe8l1Jc06tLXXsR4l4iTqit6/5ANPiaNnK32IeHNnh0hFu7aV2pB7RznRBK1ADX0wrPeKIfHUGjzkboe2wfI/ZOq63MTntCNb+cNpdyracaWVWR7vdsLN/1rwWpHem2rcZlILZ39PFrfbRDDuRpVLGR6KgjQF0j1dwBRz+c06uyzDAejmIa9jEwtUmuutgzKrmlAEDcDu4LwuOXWBqvydEgB3FALFnY1lXsqpeS1nzexl0Nqpg5Qzd9Koz47UFC1VzTDGu7ib9tejYlIXgAfjoFPLlYRXETCZ3EdnWghL/YgiRr8oWWPLcQuM36YI+Bx64T8wv3UAxzLK5mbmUc79xhc9Hv1JbDxmz7ShFgdF0EL9025Smq1KtgZrEVhZhadf3VYzbfXFxmFSI0Wou498tKo794D1lr1fuET6B/2axvCbLvRY2U0UJ1JlzjyidtdZuzWhe2toSRqMfBPzUyeLITA8VQLoDKJIMwEr5a8lNWJM/KWgLBxhu5EXt2NnvcqKOfOOhngoyEtc6OX3Qs0kNEDmwCyAXOh3flquoao66eVnAJEAS7DG2hXi7evDj0pW0FL53ozjygaaDQNLZdkrdbk9Lrm4qOhouhlS2qpa+kNz4damKc0CI45NpvgwUevJrKWsrNHREoL33AQIga+GP9y6klPtq7c36/lrC1DveTumIFrtBbd7VYCsHFO8idees9leE0pNjpbbZHiqnzmStb7gnwyH/iB9UnkKhXD3p3I60gbdnZdaWg8GUSuPjiUtbdt3+m0EyIaXm7XS6meHOldroZzrggYR6EPWhWm2M1qkaQn/749Tj5bdYDCvACGB/tpLTApTwaSqH0YG64b+PJCgCbROCXhlte6cyGlnPytaHjnNPLTpOZyM/c3XkSOE1mm6knS6O/j9XHAEH2R80nMqfyD/pQ9CDkx0sNTWE/NU1p46ph2S5zlvJMxhRg0WUjmqCtm9rwEXe/PzEGog5CEIK0zfVOPbtVYh+fF7hVdX+3+NTZeXLi4Pyi4diqY9Cxbqnr7w1vdTO2SDj7F9QX/iD4EPCV+U3p+edZYQtaGy+1VlSGfwoOti3vdC+t7Hz6zjye28Txn8ezs7HD7bI2UDwj5g2Eeg6WxYnbk9LOQXaS5UsA3S9bRpdXF8gDPSEJRcW/fM7nZSP3haE+yXfbv5W17Y+jX7NL12dlkNrUPIe219GgchAhA67XIWWw2PtsEnp2yk97AzXQL9rxbmTo6DgmTqHHwe87NRhpinuyQ0dcHgbYetmdmuxo3Usm54p7ndYOxqvEd6SRSO97+ANiavOmIDeZ/LFVnp/8gA8nUnb9NSIeV3ay25aOC4gcwdz5R+UMaMYvD+XSvk7ufqGHlaRQlUHOlUFRKQaMILvpXT51KoJePflA+JFGLzN5ZqUnXVkeHz8a15iJRFze8eupU+InzXhIBL9OC9aeL/FATdek8Y6CxSYWkBY4OV5PuVidAsl2ri/eNFiLLzZAf84rrX6HVw0kvnoD/0jUre/ZlhotQBSo38GFRStlkRV2rFrLRRmxCKWxmK9uR71cy3OZJKvBpyLpj/bgGuotNzx611OqBrCsM0+FLzsTvg4josupU/he7TW+/brpDp7c1JIjwSWNm9Mqzn+bt5HylExTN1AuvbyKDVando373ZUgJQ21r3/M2TpWzKk9mql0XPd/ftG8g5ccxTKTWXD4P7pZ7pj//rvj3RGos3lm6oZYVE9k20AostVK5P9qhNqNMH0C5IgnmVPH3S8+uq6KaAPVknAvmJiDbeVN49NHjdF16YJcuSdRCd65eV6GZmg3U278dIrCuXV9lep/I9qizx8p2YV2FAd3jACSduobB56JKpkahCVY27PJqcjRg90ikBnp+WZvTJUN/fqOUKlMHvp6abkvXRn7t4/rSPqxpbCdba9f886QvMaLZHg1C8ZP1heXuDKJItmtltnhrqK39TR8VBAmwFhL4nVuT38k27Jpr/hnkgtxFO+fdx09qdmkY/zWoRGpCn2uj+WmTQbq2QkQQiRbiAiz8JMvMRuywp6s54ZNrqQWIoZ2OdiyEvTn7PES2UleQIC3yDy7rd5+2MphEzWsmXnL2bZgo9nkUCFro2tUUfspWqTnoLd7pU37gVqgx9+yyrKc2wDv+1OzM1U8sfPHi8OTx5ShIinxxpxzCXackd7kEQoQkUVxin3KeqZSdP784X1BCY8DKQhex+GlCdrSha/v9KI9KYEztBwO3ag0k+wYhhRqF6Xc8eRCiYM0nC9HFrZjaBJzJ84c5HSC7heEmVw18lbItb2SdbizYRWqFmjDYvLQuyeyR826+2VrrDmOve6W7A8gZ302GRqLRYUvtMHR2ZDT0uc6l8qob1PNTntS2qPmcYQgIMZsWavDiaxdDW12/nxFM1EoEjs8e7nW2tA+mRuj5Na5EZOluDyF/jOBQyZ5RGLEm2QY2B9us2Q5WSLGuzVWNjSwatVjyje44USeKFrT5+KEBYN0nK9slLGxWqa9mNHHILDcRNJEJ1vaAYLtnsU54izqmRpd7UwPDlRAT/TSIadQo9MpRn06xEN7UDI8qFpJd/ivMGFaBGoGE9glpM9vecWHZUe6pvwy00hW0cvII/lwsD/XiUx/pFGryqa8n0IKL5BZuy373JlumvtwrxqsiEr5xaPILP0Ws63bFy27mdDVpV3ajZjYi+/6n93OInApaS+upiJOooJVru4y/3uo2zIQiTvao4IdliyzbpZYnBdVIHV7MOhFTHymg/0VtE0lPv4ltHV0feudAIeNacTli3/7ofQu1lEsHMXWcO0CFo9+/k1JuzNsXSePmZ1KgeU8MO2elNPt/RVAGuuoorqshs5cth6u2JXo0WFNsCBZfhd3HSx3Oj28xwCRqfrl06lgIfQEm9GV3FgDBnnipDMeFT/7xtx4J9a5WlK6nBszonn3+TzDC/0nNsc8/95yTdeLaxUVBVWrhd3/9Nmbr2qFfpbYYgvwMrL1b3FRGO8nUN6zpdo0kCP6c2J8KzKDoGubYvfk6yvVD9C3Ycvs8r+qpkddX3SPv1zJA+L+o7YF9dX/2eY40CsWN9aBKrcOLSbuT3a66OZD+29t1cCWSBGF6ZujgDEMGSYqSRBDFiIpiQDHnnNec3dX//95VNbLuet7t7cldwyID091fF9VV1VXVvSPPQfHRNEtdZaRD+jMFrTvBDvr9BjD1nywUY4/+yzxUMGlOhz/skPhdMhT3mWKViTbesP7RgEj2mH7iJIWpj6c/aDb0VH+uYPxKhtb/tKWXs6US/ZeYEfUV/1iv6TLy9LmiWKXxLf7M/+LwJaT++zT80LP4K1prOvscpYmMH8hgg/U9h1T66Cc2DzhavB8tU3Vh4Gz6HGpUuOi7wMd7d2vPLVU/sXfAsSyb+aHdaiKBhgr403z9GlT/kzRKJR3/FnU1w2fLJbiugJT+H4v9qYX+a9SS2ncZwrim/K+ow9se2f2/p/bprKEruFX4fyy5M2r73I75lfND4OD/l9YdMu78mT1qtliGy7Dz/1iGR+hntvBAscikNu1/5ZBFVdL6MyVU7gBVyP5+qfUX8hhluhWz1wSue4XCBaoVYWUylqhJU/dH65AJA10ZD5/fy0vTy6gKfxM16DtUIgbBV2lvQCOMc65xvZqKgh9qys+oFTQV9ES6Dqg9PSYqxN8uuPApME0oDAmL+aM64XouOlvh8JGVoLNE5gC+odak9y/a/fk96pSW3dpvSz7kCygGAEOFLQC4yxsciLaOd8+3zLnhU4AHX1l/Qg3jBHW+MYbS67OoPUVOfin53hbyEoGVyGUDoMKvfMV4Zep84uZ2TAacT9cOGjlauoaO9X42qXjvC0Z0PsshltIOE/yXqBHzG2rO8YVxYRBjYOp+c7TbWfUeAiJ4uWzbsetcEI29Oe7hDyNEd++PqvU4MSL0RTfIb6JG3Myw+7I7i7HjXWfolXg2RC7/jU7bOdL2J9RwyQ+r6vyzsG108C9IXYUnk7sIygONyXgGIhbMnuvN77c+rKi1VagNLMjL0ZkWsH+d4AAfnS6ahBOu1LSBhC/M1CqtywYxuuknf8chGgKWeWBMlz5zQO3vndsbXGhxqjVveIPtW3pm7XFuuQyA71bgo/RjJlzgmGKlvdFasR+00PrsahuNf0xsqyS3XEMaCkZx8CkID0enrtrSXU7pf656B8bSV7M7mUCj/bDdopaXhzppyHK5OhcmQooSiRoCowox1h11ITUG1LziQyLDE8urpOVuX2PqeuN8ZvTbCnAyYHauXI7exO57luNFv8uQq8RgOySy232ZxfQIdVwsHPhQi9YcKUzw4qiq1gd2qGngI9A18QZcKQTzZ+aW1iZunVVbMUQdp3er918OOvwGjonL/TqgWIJDNDQohDAnk7vA3bvbUW9B1FDripiidTv28C5v/hVq6eli4YG5k9jo7sirG9Tm3L3Z3l+OB/3VE9esrCpfYLYWm2ipiYB2NDJf+qDpy5l+P3ujtRv2w9jqhHtlyv0xanQOGP7JxZe7iKf0mq3m6V7YfprszblNkxOZY6pUYSsYYg82legQUwTumals7UKYMbLdiLDlsEj8jtYJNMC+T2BgWkdyKApHnUc0UyOGqzH61Jq8BKXgoJQ6xr6ND55M5f2K8Rd6XgG+tliGECMmF+b60yUIypwHTME5KxjChDQGR91OJ2jpBSXCGJBMR1MIsBODJHqHn9Y2I86qQhiJ3JTP15ebE3iX+DVqnTFN9+7fOSGP8Dz6etpFeLvTotaJ1FDWq1EwuaYRdl8wtbyU/Dp+2jWC+4iObsdiICcCOa+bW+FXAGb/K9RasOkVNUGnk3D3zpZPbSOjW9c+jRMSuKnTkUSyDdt4UJfOGxbOdewsztyOjWDrNmcXqI6lymRsA5OypD9JJkLpH9uBP6JGdpcHhIajMaeTdq4FTZ3vXNB6HRmBxL695pwYZrCyvhZLexCxOtKdfrjfyPcmOO+d6LFXs92FLk1T9mvUUhUyxongky8XlB6dV3z6VKh+BxLJvcLecLx/fW1hV+6xHunsjp1MHWSKXk64vdAx02PnHFU6UBBz0/8JrQXyko4RA3tqMT0fujjbaD6pI6nRVpuYPdk8HuukUDwtD+c9B3GvG5AKAk8TaO0SvJqCKJPD/gGt8S4rZt1yXVESO/djpdDp1gKt7xlyjhVIc71Nx87X+ytZn8sk/Lt9SgigdsMFXr3X+VZU+/BS2w/cOKSqQwAWCY3i6NVS5b65ze6Qw1b3M0VWe+aiRS/hXBdVuLVCSIdELT96jxoDrNVoujxkQdK6XRe4BHg7aVD+YIHrM4tab9Tqst3k1c3qHDpEJv41agVpTAAgorYaMFOD7TbLEK7ZhdyU8T0SQ0TB/eisL2RciEU5KRC55BZCaD+sFv+GQ+AaZQrcL/ey6Do5HGpoaBdc1+BKCj9ZrIZVMckXRF1XvqbqJEEaC/ilucRq1X6FGouU30hrXPsajAeaGhoGDa4Y1U1Fr/cbus6F+FJ/WtMoWjw6g4ITTGjf05jfOOTjzE6rJLTblwjGJ3e+3DQ03Ox0+KEV5c2bbwXuUYylkTqLEEuJRhUuO5IhK46YtSoqidpe9W1obzmGHF9ch8WBTH56Y/Fk6OwmfRy56HLCCvLi+GFtv9ePZzpzaABRoyz60omLn/oe0xzVURYggVGQAYsyBVmTwWW8vO6u2tCAgJs6cfv8gcnrqcW1iYV0t8fhOdrtPu6buG9dmp6G5PmH2fu+bs8lIg8LzWQaRyNc54DaUl/UqhpVWDXSwRV4cEPRleogOAdaG0IooOW8icNsZnm2Z3tr8/i2u/vb7nHf19jg+dLs9WSgeOgNG2Dz2Ybs/o6585vub6c3S9GgC9kNhYqJqJFB6ooaGbSWdSJXVa/8IcRArMeVAFYYvt7YXmt7Ht3djRz3nSW3ThanriuZgM+QzoaCaRZ4cEi1DJowP7xgmb+kTzevNob9hhXnK1mqN2rkEE2pWUUmx0xOuShnnOiJ5eet2S/ngw/jx5Hj9MJE+/aXqel8aiBw6AMRz7gmcGgyHZQ3toOWESA0oKq/N/8YO458fYz6DSE4yJD/gtYoqEzTsId9uWCgdzJ/MD21eL7WPpiMRb5u7a3Pzu0Mx5uLh/6w3TCY5J3qZHsrQis2gUZH5SrPzeD2RPPk3PZQcu3LctG/OPIf0NrlDzan8nOz6ydD5bPNvr7N1aaTL7PAAo0dM1MuUYVVk4cM3gn+qrPlh5j4RpTikIUOcVxfIG8x+aUwcrAqKl+dOS11R/3Y03reFOsb79ucGVxrXd/PT3Zkgz6Df5fXH6TeKD+Wn9cyP8l2YfLizmz9Udsst6fHX19OeqYPKqlgzhd2m0hdVO51QE244IZ3qf60Vr/EG3OJMMe8O2Damt+U4YHciNoqyz9H/f78BIwiOWndUU8qhq5JpYiqpApaOp/qgJpxJnhhqf4yBCVflci82qP2Hdp31G8/+q89Cz8PDFS7MEFe1x81xn6k5FU0pDCaztjnD9bTO+r+eRCIuqHhR9TVxQWTRw8g6jrbITSKm+QlZ9RoqlSdwXVAbZW2wu/aIb8+ZJjSKGqNj/NTelcRtYT8cxbiG3D8SoOrwyGVAup340GHDqAesbzfhEfprzyntr+PGExyzNL8sXzPWgbPgkwftv44JX98Ly/RQiLFdhpqh5H+3JIiFc7Sb6JG2L+0rzkYpsoHxco6ZjbsqOZqEaUff/ofOEFnhhbconQQhOfPLcnDKWo2n/o7+3p+wSFgh3CFvBNvVWCa5GtZ/o7WcpNBsImGhjQOoZyfijz4GFBTi/q7eTd/z9eTf5W6oJGOWI+BQfIa6bh8XyvfLxiwQXCoBHz9LuyK/hFNiEVE/RuFjo2fPW92/+3ADg4bc8HGD8phMBpb97l+WcBZ5XVl7yENOhh811AQHocB1I1v5df/GxSNRU2zcva3I5tpahq6b/qwDJ7GWv9J2Wt9PBkHb3j7+4ba2/FfXwj5+h+htoF4oJ6lBE9sHckqWLEaN359K/mnVh+v3u20lV9Jx3D1RfIaBkLxe2jFUYvn4kMezNggP6h2j/fIp7xZAq1tAa2hUWvsa6MUIn70dSI6B4OEb3RhS7Ueau72Wg1sTJa38SME+azBwE/wDllHvn0dAj7gUlaBIrFhhWotrILNyBFhgXq1vlUq/8rLKiJKq78IpV87hJm/dYRk86+3VQPwtWE4JNZqr84a6oa3gj3h3TVaOfFOdCJ8JwPirN1gk6hHcBeL43WEEi9+BW9qe7rgOwSNWCQU5Ap40iqxPXt+Ep7ug8t0W7ItNk8fkm3JVQedb0smnd2bZ2dpnCZdbeWZiViLxbbZ9vLShiU5uLqL7a20TUzEYg8L8jjL0fIqXjx3Y1eecjnZ9pLEm1tOY9AqPpKr36DTlvLWVvJhhXZPDLaVT+GD3YfBtljXM9w8KiHSU6j10oaVn53d5WSyD6jcstpWbrtEHy+dqYQL9sODs3m6lg0059Oe60CxOdNNR1PN2cvVnVRm6ZRClkF2IN6RufJ0PQVeSzaQ6sNhRwK9van4ZHR/+2vIchUfiMdTmehs7AjmXSZQDASam+HfUCxaqzYJW2H6ZrPBYGByK3RTCQQG2gH15nJzcScylw0E1iQtaTlbhJrN2WJg7ttDKtD7CPNvLxUIdGziqFqGTc0gzByecDa5TJ7dvMgYnPtGadlN7KftzaZ9LkJp6bmgGwZs6/ccSBMJQ3jCt0ChRHTGGXxn9+WP6aOXc8VQDHcGHOkRDJPqmNXA114GMJ5hKIQH72h6P8wIprBsthwYuv3JQx1rfmHuzA9zzlsli9JBRuT/HSZ4ZSx2KMwNSm+H3cKMp3FQJyZXdDy1dDldThR4YLU7RXSRWKAnbu7dvR/gpB9RDzL02fQfXxyIV1WtG4kFB7WUIugpgCYI1xdpq1er7pYSvWUa8XO0MlA9ba0OCI3j/nat2De2ZCc6Wkxs7njRzXFLj2fJBIlQykPlPUnr0otArSYIJ/ndWI7Y12noISyEbwp+RDreS4i3chA3rL7FzUau5bZamgUn9ueGHs4C39qbrcp0SwMMTm6H7XhYWYpGUy6m5aLR4XwaVL0agbaN+GRRaCQV2QvDKmE4lWPEvR/p3hmORqMDUI/clwdApcfhengu/VzUSCJ/0GsnjednHRp4tellnBB3O63A0mhPCiCaxF1+WD/aMz9zqNl7Suos0Yz8AjL9uZeR5QvnQsJg+dEAM3zb6RyuCCec+9waHXtFbVFndQzqJgYhKaWzD8Y153DaPDYLyKEWQB1+drwAX/ln9nyKfXDscsmua5k7OgLFsQi/g3kDqDmb6fQ4nZ0loC/Z93SWs4T3387aFXfPRZ+dk0S6VOFWBVE32GhSI+xkXu10Oj005udsiaYbGW+coIj6MUyMaap2B5k22ZLiwr4+jpEtljy6Ztqyp4bacqChDzi8vQKx7/GAya+prbp9VY3A7e4HOm4yw5t89AmjjdItl6akFpzYQ9cB/Eodu20DCtHPKM4zOgV+nvUSJp6Ig90mNzF37somJ83f6DC4/IBDGiD8muScX1EZywvN+DlZ72p1Cz49Jt3Fe2HNmHPQi7lofioSFdw19dVOgFOvWvKKdtBZ4xDbMMGtG/apI2qhfc1E66fVY02R1hpzPdC0ENw3CKjZIHVsuQVL3aBotQELsvCSsw0Iy2NVybthCvusg0ae8pUvXQ85ARHLE53zA5tlWDBtT6W3ZeAQTth5CDeTNKixRpiNCxUuDtuopHV7AigxTkORSOTWk9eI2b+KIU/jcWFS0fo9NdRHGcEnGwnfuQRi9WUFmaNSfcNrCyck/FAa5ybxtQFfI62v3MJIjVNUvEN+6KCslgfgtljVQtgLE234OERvj1tOaUsHuP/WZgnhJxa1grORdu89gQzhwCFUlRYDzEZjqtVP3HMtUsnQCDhL7QfP8zgFHNPQ8sGVKZhgU21ZrvXUUNOFZi23lCEkPg5IxptBmgCkqiaPCOCQTTouiO5fbfVaWVJV19yE5dOogdPLnOceL2nbACFiZsU54rSpdx3C6ppeGJGK0HnNufsgXm26wjXy+HU60Q+0thJlu3PEM+JsUGdgqnX0Ej757Hg9LKTNCxwbP/dgCz0gmipfTEwS69/KQa2R9mYFUFscq34znuxnHKa7Squoa0cNtRi65gLUnJi52KOPsMHuvilDCa8fAdc7H+3CfT1uAVrDpEwt5/PLC9Sz5te4khrqkjp/G3oLu4Q8HTlfYKQ/bpJ+kHyKxnor+eX8uoXONGp4apt3a6WWug8bkOCDgQc0IBYZF6kNIThRrk/8hK05X1E7r7wkurBkCu+eavkZtdoimOJKjp7bCyRw1+ojxtxs5ZCx1ASS+rhicv/evFMtZxn6sQnnM5SOPoV1xR7f8gDD0KSXoNIKbIHRWAHd0WiY9g3gEJ0IYud6oWKhZ36GYeDG2NuRLlcu1HYHqN7PgQTZJ4Hr2vwS9D8IHEIIoL5YdJkHAAwmRQj4+ifUEQFe82glCIBSR3thoZsFLrTi3iUYerTJR0gKSA201nVciRf4jEpLZ70chhC4QQw3OYUJzYhuwvthAYq2YF/ug8EwphRgohvD1AHyGiNO/iEnRdBSpfe7gFr+c7Cv23UIZF6j41mbfPJaXWWU14gaThULz0YmQM5OX/wJtamD7rULLsScbc/LGOEF0HcxFLvd+4yQdQw1rw5oAENXQBNQYJxFuJ+Ysx7sP45Z1679FrgrjyEr+/KDE/hagwGgNz5KaawR4k3QR0+E1ox8W990WNeM1MtIwyoxRGIYPXjKwLJbS8RGXlGP7hD/l8vxsCEqafqGWsJu4WiXFIQp3G2WPZ/Q7Rzms7FzZ2kIrQ4YzLvQYENawz0DqUwq9RWMUDWyA9rZHjwLUfVomgDUwz0PWMQVE8ySua9oyYJuJMVUKhOfclrOcgp4JhifjJVqWwcabJdXGZfGMpu2ry4ivAFiL4ZZImBYAwuAmgBfq88pcBt1RgJED8yo71BHiM5Yrpid3N/qsrSC0lqa2Y4Cq0/dqpf9YY3nPWh/o7wm5a6jriMnWtjq6V6vW2f5PgCwlRCKMvwV7OmVYRjA8imY86jRNb6N94812GKHwCp+lxae7UbRBQwHTVBPbBJ+o42udBDmqpv48kVm2mEKp5Gv2XSLba0oDnvWToqEh+9DlveojYLrPj3avUKp2uplRpmGmnwai/c1LAR1Zj8BTkFaC9AyDirVBqJyDnZoVv9jJ8zYDkUwVF+etSKxgm5UJWoOEhx1CtweQ8n3WDEL/hl5Mqltb3n54Jh2PoJaqHQf9wKPKtbcbAojnSLT4rkf0Fl/xPnoZszlDZuk4Hrs/JmvaQQ0Q/i5etwe2HyEJSl9zpmk44ZemVw0Pqg2AAry2iQxgABx56Xl/MEpHduAPq9B3B2lrMRYh9/96wDGPvZAlsB9SUH4uaUkj3GZyYFWPm3ljD+tIGrLEyeJvhJdTRB9siWS4Qp4toJLwwZm/g9HOtuzGqC+mHWhu0gQk9v7L/4CteUH1AtBIjruQk8Fbh++xXUboNYJi0nbRb0WJtjv9L7RLOx0Wxq6XlFbtkxM59+Tsw1QC/OqugaTqHscDwlNNI9TXG7Pgh5+oJj4y1LHlxWhgNWV3b62A63tO92epmbOp1siy9AlntkvCkZ+9wPUrrM31GaSltIBQjruLjKCeJc8tFSy2Mq9ilZ4DlEKqmfO1Hx9tNRWJGR590fUBczO2eukJYeK1hO5KpVwdWs7Q9T0qFch7u1OnI3gWHSf0NK9jynR265r9Juyjq0nPNHG3b/raQoovL8lnWFWjPhjyZx+hDr2E2oayXIzfrPQSMzeGbWE3FnOKpoSo4AaDAfoc4uWTvyaudNlU7+jXjt0G8DXFCpQtVwgxgnFYqvSmlp2NO7eOVbRgQMD9ucrRab71lc8G2HBCYvGllzcSsIbXSP3zZz0t8Tc3FgcHR3d40QUx+n4O9ScuGZUvFBrqHd7BaRvrhtMuJuzWXgOvgyAqy+YhRJND/kJacwPHxoit+1BviZVvl6YmBRM8w9kswPZl0FCrLmBLNTev0DraZ2qWwbRg48r4CwNrbtNA2MAruVxi3M7gTGMndETH9FE4tGJMgRQLxoFd5nSUBnmY/jZ8hOtLRHBuWviA9QL+4pCFB3pINZWB2SmNdxbvLuYCnPc0M3s+8dwcNxYDTWlwybuUTG0ArtPKlyByoyLyu7ZIeGAerRZ53zyGN2O45kCIdDwZNKj2rb8JsCeO70/hD+5ExU4hABf7wuSuKHA/TnodvXPqIn7HerLAS4GZvYJ5rIQDDEAal3RuCCCwGr3K8wbAbTKznSCSKjRGqrnAZbCdGHXhoaA7Aq85yR/MdOItLZcXhsA5AFRO9oyPpfXFT2bBxkz0+v1eROznnI8kUjE29WRoVQi8RTJe72BbnSYxN2JRLtlPO71PVVRA59GfF5/bgY1A3R7cuhNvFDaFfX6m09mvT4oXm/C571fjfvDCXiX8PaOWzx31+DPDAfWUKOrXVHoc1G62JZ9WAXLYBJuhcp+r/fg6KzZ5/8SUkf2mhMJ/yA6pmmoq69t8OFbiUIDoaPOsfn5eRryeOY9Y+DyomPwzuJc8XjQV6XOr4x0OW2WMc/8ynfUFK7mnXCB6eLO+bEu6qTUM7bSRR3znnmof+QZm+90OMZWPGMjXV2eFfDB0LGF5ODXUweVvqn5Ts9KdWtK57yzax7LCtw/74HqUH+FOjzzF04K1TrHPCsjlj8AGdA2hxNpKRMAAAAASUVORK5CYII=';

        return base64_decode($data);
    }


}
