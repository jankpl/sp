<?php
/* @var $this InternationalMailPriceController */
/* @var $model InternationalMailPrice */
/* @var $internationalMailPriceValues InternationalMailPriceValue[] */
/* @var $groupActionListView string */
/* @var $groupActionDelete boolean */
/* @var $mode string */
?>


<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);


?>

<h1>Manage <?php echo $mode;?> pricing for: <?php echo CountryGroup::model()->findByPk($countryGroup)->name;?>


</h1>
<h2>For user group: <?php echo (UserGroup::model()->findByPk($userGroup)!=NULL)?UserGroup::model()->findByPk($userGroup)->name:'-';?></h2>


<div class="form">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'international-mail-price-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
    ));
    ?>

    <?php
    if($groupActionListView!=''):
        ?>
        <div class="flash-notice">
            <p>Edit price for user group:</p>
            <?php echo $groupActionListView; ?>
        </div>
    <?php
    endif;
    ?>

    <?php echo $form->hiddenField($model, 'id'); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
    if($groupActionDelete):
        ?>
        <div class="flash-notice">
            <p>Delete whole pricing for this group.</p>
            <?php


            echo CHtml::submitButton("Delete",array(
                'name'=>'delete',
                'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
            ));

            ?>
        </div>

    <?php
    endif;
    ?>
    <?php

    echo CHtml::submitButton("More",array(
        'name'=>'add_more',
        'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
    ));

    ?>

    <?php

    foreach($internationalMailPriceValues AS $item)
    {
        $firstItem = $item;
        break;
    }


    ?>
    <table class="list hTop" style="width: 700px;">
        <tr>
            <td style="width: 40%;"><?php echo $form->labelEx($firstItem,'weight_top_limit'); ?></td>
            <td style="width: 40%;"><?php echo $form->labelEx($firstItem,'price_per_item'); ?></td>
            <td style="width: 40%;"><?php echo $form->labelEx($firstItem,'price_per_weight'); ?></td>
            <td><span style="font-weight: bold;">action</span></td>
        </tr>
        <tr><td></td><td></td><td></td></tr>
        <?php
        foreach($internationalMailPriceValues AS $key => $item)
        {
            $this->renderPartial('_international_mail_price_value', array(
                'model' => $item,
                'form' => $form,
                'i' => $key,
            ));
        }
        ?>

    </table>


    <?php

    echo CHtml::submitButton("Sort",array(
        'name'=>'sort',
        'onClick'=>'$(this).closest(\'form\').attr(\'action\',
        $(this).closest(\'form\').attr(\'action\'));',
    ));

    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>

<div class="center">
    <?php echo CHtml::link('Back', array('/internationalMail/internationalMailPrice/view', 'id' => Yii::app()->request->getQuery('id','')),array('class' => 'likeButton')); ?>
</div>

  <?php echo CHtml::script('$("[data-delete-button]").on("click", function(){


        if($("._price_item").length > 1)
        {
            $("[data-price-id=" + $(this).attr("data-delete-button") +"]").remove();
        }
        else
        {
            alert("You cannot delete all prices!");
        }

  });
  ');?>

</div><!-- form -->