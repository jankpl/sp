<h4>Change statuses</h4>
<?php
if(!in_array(Yii::app()->user->authority, [Admin::AUTHORITY_ADMIN, Admin::AUTHORITY_COURIER_SCANNER])):
    ?>
    Option available only for administrators
<?php
else:
?>
    <?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'mass-assign-stat',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/assingStatusByGridView'),
));
?>
    <table>
        <thead>
        <tr>
            <td>Status</td>
            <td>Date (yyyy-mm-dd hh:mm:ss) *</td>
            <td>Location</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>  <?php echo CHtml::dropDownList('postal_stat_id','', CHtml::listData(PostalStat::getStats(true),'id','name')); ?></td>
            <td>         <?php
                $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'name'=>'stat_date',
                    'options'=>array(
                        'hourGrid' => 4,
                        'hourMin' => 0,
                        'hourMax' => 23,
                        'showSecond'=>true,
                        'timeFormat' => 'hh:mm:ss',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'dateFormat' => 'yy-mm-dd',
                    ),
                ));
                ?>
            </td>
            <td>
                <?php echo CHtml::textField('location'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">(*) leave empty for current date</td>
        </tr>
        </tbody>
    </table>
    <?php
echo TbHtml::submitButton('Change', array(
    'onClick' => 'js:
    $("#postal-ids").val($("#postal").selGridView("getAllSelection").toString());
    ;',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => 'postal-ids')); ?>
<?php
$this->endWidget();
?>
<?php
endif;
