<?php

class ArchiveTtNumbers
{
    const DAYS = 1300;
    const REGEXP_DETECT_PATTERN = '/^A\d\d_/';

    const TYPE_COURIER = 1;
    const TYPE_POSTAL = 2;

    static function addPrefix($number, $date_entered)
    {
        if(preg_match(self::REGEXP_DETECT_PATTERN, $number))
            return $number;

        $number = 'A'.substr($date_entered,2,2).'_'.$number;
        return $number;
    }

    static function removePrefix($number)
    {
        return preg_replace(self::REGEXP_DETECT_PATTERN,'', $number);
    }


    static function archiveOldNumbers()
    {
        Yii::app()->getModule('courier');
        Yii::app()->getModule('postal');

        $cmd = Yii::app()->db->createCommand();
//        $ids = $cmd->select('id')->from('courier')->where('date_entered < (NOW() - INTERVAL '.self::DAYS.' DAY) AND id NOT IN (SELECT item_id FROM archive_tt_numbers WHERE type = '.self::TYPE_COURIER.')')->limit(1000)->queryColumn();
        $ids = $cmd->select('id')->from('courier')->where('date_entered LIKE "2018-01-%" AND id NOT IN (SELECT item_id FROM archive_tt_numbers WHERE type = '.self::TYPE_COURIER.')')->queryColumn();



        foreach($ids AS $id)
        {

            echo $id;
            $cmd = Yii::app()->db->createCommand();
            $cmd->insert('archive_tt_numbers', ['type' => self::TYPE_COURIER, 'item_id' => $id]);

            $temp = CourierLabelNew::model()->findAllByAttributes(['courier_id' => $id]);

            foreach($temp AS $tempItem)
            {
                if($tempItem) {

                    if($tempItem->track_id != '') {
                        $newTemp = self::addPrefix($tempItem->track_id, $tempItem->courier->date_entered);
                        if($newTemp != $tempItem->track_id) {
                            echo $tempItem->courier->id.' : '.$tempItem->track_id.' => '.$newTemp." (CLN)\r\n";

                            $orgTemp = $tempItem->track_id;
                            $tempItem->track_id = $newTemp;
                            $tempItem->update(['track_id']);
                            MyDump::dump('archive_tt_numbers18-01.txt', $tempItem->courier->id.' : '.$orgTemp.' => '.$tempItem->track_id." (CLN)");
                        }
                    }
                }
            }

            $temp = CourierAdditionalExtId::model()->findAllByAttributes(['courier_id' => $id]);

            foreach($temp AS $tempItem)
            {
                if($tempItem) {

                    if($tempItem->external_id != '') {
                        $newTemp = self::addPrefix($tempItem->external_id, $tempItem->courier->date_entered);
                        if($newTemp != $tempItem->external_id) {
                            echo $tempItem->courier->id.' : '.$orgTemp.' => '.$newTemp." (CAE)\r\n";

                            $orgTemp = $tempItem->external_id;
                            $tempItem->external_id = $newTemp;
                            $tempItem->update(['external_id']);

                            MyDump::dump('archive_tt_numbers18-01.txt',  $tempItem->courier->id.' : '.$orgTemp.' => '.$tempItem->external_id." (CAE)");

                        }
                    }
                }
            }
            echo "\r\n";
        }

        $cmd = Yii::app()->db->createCommand();
//        $ids = $cmd->select('id')->from('postal')->where('date_entered < (NOW() - INTERVAL '.self::DAYS.' DAY) AND id NOT IN (SELECT item_id FROM archive_tt_numbers WHERE type = '.self::TYPE_POSTAL.')')->limit(1000)->queryColumn();
        $ids = $cmd->select('id')->from('postal')->where('date_entered LIKE "2018-01-%" AND id NOT IN (SELECT item_id FROM archive_tt_numbers WHERE type = '.self::TYPE_POSTAL.')')->queryColumn();
        foreach($ids AS $id)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->insert('archive_tt_numbers', ['type' => self::TYPE_POSTAL, 'item_id' => $id]);

            $tempItem = Postal::model()->findByPk($id);

            if($tempItem) {
                if ($tempItem->external_id != '') {

                    $newTemp = self::addPrefix($tempItem->external_id, $tempItem->date_entered);
                    if ($newTemp != $tempItem->external_id) {

                        echo $tempItem->id.' : '.$tempItem->external_id.' => '.$newTemp." (PEI)\r\n";

                        $orgTemp = $tempItem->track_id;
                        $tempItem->external_id = $newTemp;
                        $tempItem->update(['external_id']);
                        MyDump::dump('archive_tt_numbers18-01.txt', $tempItem->id.' : '.$orgTemp.' => '.$tempItem->external_id." (PEI)");
                    }
                }

                if ($tempItem->postalLabel && $tempItem->postalLabel->track_id != '')
                {
                    $newTemp = self::addPrefix($tempItem->postalLabel->track_id, $tempItem->date_entered);
                    if ($newTemp != $tempItem->postalLabel->track_id) {

                        echo $tempItem->postalLabel->id.' : '.$tempItem->postalLabel->track_id.' => '.$newTemp." (PLT)\r\n";

                        $orgTemp =  $tempItem->postalLabel->track_id;
                        $tempItem->postalLabel->track_id = $newTemp;
                        $tempItem->postalLabel->update(['track_id']);
                        MyDump::dump('archive_tt_numbers18-01.txt', $tempItem->postalLabel->id.' : '.$orgTemp.' => '.$tempItem->postalLabel->track_id." (PLT)");
                    }
                }
            }

            ///

            $temp = PostalAdditionalExtId::model()->findAllByAttributes(['postal_id' => $id]);

            foreach($temp AS $tempItem)
            {
                if($tempItem) {

                    if($tempItem->external_id != '') {
                        $newTemp = self::addPrefix($tempItem->external_id, $tempItem->postal->date_entered);
                        if($newTemp != $tempItem->external_id) {
                            $tempItem->track_id = self::addPrefix($tempItem->external_id, $tempItem->postal->date_entered);

                            echo $tempItem->postalLabel->id.' : '.$tempItem->external_id.' => '.$newTemp." (PAE)\r\n";

                            $orgTemp = $tempItem->external_id;
                            $tempItem->track_id = $newTemp;
                            $tempItem->update(['track_id']);
                            MyDump::dump('archive_tt_numbers18-01.txt', $tempItem->postalLabel->id.' : '.$orgTemp.' => '.$tempItem->external_id." (PAE)");
                        }
                    }
                }
            }


        }
    }



}