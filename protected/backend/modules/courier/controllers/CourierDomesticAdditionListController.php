<?php

class CourierDomesticAdditionListController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierDomesticAdditionList'),
        ));
    }

    public function actionCreate() {

        $model = new CourierDomesticAdditionList;
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $additionTr = new CourierDomesticAdditionListTr();
            $additionTr->language_id = $item->id;
            $modelTrs[$item->id] = $additionTr;
        }

        if (isset($_POST['CourierDomesticAdditionList'])) {
            $model->setAttributes($_POST['CourierDomesticAdditionList']);

            foreach($_POST['CourierDomesticAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierDomesticAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierDomesticAdditionListTrs = $modelTrs;

            if ($model->saveWithTr()) {
                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,

        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'CourierDomesticAdditionList');

        $modelTrs = Array();


        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = CourierDomesticAdditionListTr::model()->find('courier_domestic_addition_list_id=:courier_domestic_addition_list_id AND language_id=:language_id', array(':courier_domestic_addition_list_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $additionTr = new CourierDomesticAdditionListTr();
                $additionTr->language_id = $item->id;
                $additionTr->courier_domestic_addition_list_id = $id;
                $modelTrs[$item->id] = $additionTr;
            }
        }


        if (isset($_POST['CourierDomesticAdditionList'])) {
            $model->setAttributes($_POST['CourierDomesticAdditionList']);


            foreach($_POST['CourierDomesticAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierDomesticAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierDomesticAdditionListTrs = $modelTrs;


            if ($model->saveWithTr()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model = $this->loadModel($id, 'CourierDomesticAdditionList');
            if($model->broker_id != NULL)
                throw new CHttpException(403,'You cannot delete this item!');

            try
            {
                $model->delete();
            }
            catch(Exception $ex)
            {
                throw new CHttpException(403,'Nie udało się usunąć tego wpisu!');
            }


            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierDomesticAdditionList('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierDomesticAdditionList']))
            $model->setAttributes($_GET['CourierDomesticAdditionList']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionStat($id)
    {
        /* @var $model CourierDomesticAdditionList */


        $model = CourierDomesticAdditionList::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}