<?php

Yii::import('application.modules.courier.models._base.BaseCourierTypeDomestic');

class CourierTypeDomestic extends BaseCourierTypeDomestic
{

    public $_additions;
    public $regulations;

    public $_ruch_receiver_point;
    public $_ruch_receiver_point_address;

    const MAX_COD_VALUE = 5000;
    const MAX_COD_VALUE_DHL = 11000;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bAcceptRegulationsByDefault' =>
                    [
                        'class'=>'application.models.bAcceptRegulationsByDefault'
                    ],
            ]
        );
    }

    public static function getCodAvailableOperators()
    {
        return [
            CourierDomesticOperator::OPERATOR_INPOST,
            CourierDomesticOperator::OPERATOR_UPS,
            CourierDomesticOperator::OPERATOR_UPS_SAVER,
            CourierDomesticOperator::OPERATOR_UPS_EXPRESS,
            CourierDomesticOperator::OPERATOR_RUCH,
            CourierDomesticOperator::OPERATOR_DHL,
            CourierDomesticOperator::OPERATOR_DHL_F,
            CourierDomesticOperator::OPERATOR_DHL_F_PALETA,
            CourierDomesticOperator::OPERATOR_UPS_M_EXPRESS,
            CourierDomesticOperator::OPERATOR_DHL_PALETA,
            CourierDomesticOperator::OPERATOR_DHL_F_DLUZYCE
        ];
    }

    public static function idCodAllowedForThisOperator($operator_id)
    {
        return in_array($operator_id, self::getCodAvailableOperators());
    }

//	public static function getOperators()
//	{
//		$operators[CourierDomesticOperator::OPERATOR_INPOST] = Yii::t('courierDomestic', 'Inpost');
//		$operators[CourierDomesticOperator::OPERATOR_SAFEPAK] = Yii::t('courierDomestic', 'Safepak');
//
//		return $operators;
//	}

    public static function getBrokers()
    {
        $brokers[CourierOperator::BROKER_SAFEPAK] = 'Safepak';
        $brokers[CourierOperator::BROKER_INPOST] = 'Inpost';
        $brokers[CourierOperator::BROKER_GLS] = 'GLS';
        $brokers[CourierOperator::BROKER_DHL] = 'DHL';
        return $brokers;
    }

    public function getBrokerId()
    {
        return CourierDomesticOperator::model()->cache(60)->findByPk($this->domestic_operator_id)->broker;
    }

    public function getOperator()
    {
        // operator has been changed by administrator
        if(CourierExternalManager::courierDomesticOperatorToCourierExternalManagerOperator($this->getBrokerId()) != CourierExternalManager::courierLabelNewOperatorToCourierExternalManagerOperator($this->courierLabelNew->operator))
        {
            return CourierLabelNew::getOperators()[$this->courierLabelNew->operator];
        }

        return CourierDomesticOperator::model()->cache(60)->findByPk($this->domestic_operator_id);
    }

    public function init()
    {
//        $this->cod_value = 0;

        return parent::init();
    }

    public function isValidMultipackage()
    {
//		if($this->domestic_operator_id != CourierDomesticOperator::OPERATOR_INPOST && $this->courier->packages_number > 1)
//			return false;
//		else
        return true;
    }

    public function beforeValidate()
    {

        // RUCH:
        if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_RUCH)
        {

            if($this->_ruch_receiver_point == '')
                $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybierz punkt odbioru paczki RUCH!'));

            if(!$this->hasErrors('_ruch_receiver_point')) {
                if ($this->courier->cod_value > 0) {
                    if (!PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, true)) {
                        if (PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, false)) {
                            $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybrany punkt RUCH nie obsługuje paczek COD!'));
                        } else
                            $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybranych punk RUCH nie jest poprawny!'));
                    }
                } else {
                    if (!PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, false))
                        $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybranych punk RUCH nie jest poprawny!'));
                }
            }
        }

        if($this->courier->cod_value > 0)
        {
            if(!self::idCodAllowedForThisOperator($this->domestic_operator_id))
                $this->courier->addError('cod_value', Yii::t('courier', 'Ten operator nie obsługuje COD!'));
            else if(CourierDomesticOperator::isItDhlOperator($this->domestic_operator_id) && $this->courier->cod_value > self::MAX_COD_VALUE_DHL)
                $this->courier->addError('cod_value', Yii::t('courier', 'Maksymalna kwota COD dla tego operatora to {kwota} PLN', ['{kwota}' => self::MAX_COD_VALUE_DHL]));
            else if(!CourierDomesticOperator::isItDhlOperator($this->domestic_operator_id) && $this->courier->cod_value > self::MAX_COD_VALUE)
                $this->courier->addError('cod_value', Yii::t('courier', 'Maksymalna kwota COD dla tego operatora to {kwota} PLN', ['{kwota}' => self::MAX_COD_VALUE]));
        }

        return parent::beforeValidate();
    }

    public function rules() {

        $criteria = new CDbCriteria();
        $criteria->condition = 'stat = 1';

        return array(

            array('_ruch_receiver_point, _ruch_receiver_point_address', 'application.validators.lettersNumbersBasicValidator'),

            array('courier_id, domestic_operator_id', 'required', 'except' => ['step_1', 'api']),

            array('cod_value', 'default', 'value' => 0),

            array('domestic_operator_id', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999),
            array('cod_value', 'numerical', 'integerOnly' => false, 'min' => 0),

            array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('courier', 'Musisz zaakceptować regulamin.'), 'on' => 'summary'),

            array('domestic_operator_id', 'required'),
            array('domestic_operator_id', 'numerical', 'integerOnly'=>true),
            array('courier_id', 'length', 'max'=>10),
            array('params', 'safe'),
            array('params', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, courier_id, domestic_operator_id, params', 'safe', 'on'=>'search'),

            array('domestic_operator_id', 'exist', 'allowEmpty' => false, 'className' => 'CourierDomesticOperator', 'attributeName' => 'id', 'criteria' => $criteria),

            array(implode(',', array_keys(self::getAttributes())),'safe', 'on' => 'clone'),
        );
    }

    protected static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    protected static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    /**
     * Returns list of non-db attributes that should be copied on model clone
     * @return array
     */
    public function clonableAttributes()
    {
        return [
            '_ruch_receiver_point',
            '_ruch_receiver_point_address'
        ];
    }

    /**
     * Serialize additional params (ex. Palletway size)
     * @return bool
     */
    public function beforeSave()
    {
        $this->params = array(
            '_ruch_receiver_point' => $this->_ruch_receiver_point,
            '_ruch_receiver_point_address' => $this->_ruch_receiver_point_address
        );

        // serialize only if not all values are empty
        if(array_filter($this->params))
            $this->params = self::serialize($this->params);
        else
            $this->params = NULL;

        return parent::beforeSave();
    }

    public function afterSave()
    {
        // temporary solution before moving all COD data to Courier model
//        if($this->cod_value > 0)
//        {
//            $this->courier->cod_value = $this->cod_value;
//            $this->courier->cod_currency = $this->cod_currency;
//            $this->courier->cod = true;
//
//            // if not already saved, do not try to update it, bacause it'll be saved with new attributes after all
//            if(!$this->courier->isNewRecord)
//                $this->courier->update(['cod_value', 'cod_currency', 'cod']);
//        }

        return parent::afterSave();
    }

    /**
     * Unserialize additional params (ex. Palletway size)
     * @return bool
     */
    public function afterFind()
    {
        if($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if(is_array($this->params))
            foreach($this->params AS $key => $item)
            {
                try {
                    $this->$key = $item;
                }
                catch (Exception $ex)
                {}
            }

        parent::afterFind();
    }

    /*
 * Get children courier packages (from multipackage)
 */
    public function getChildrenCourierDomestic()
    {
        return CourierTypeDomestic::model()->findAll('parent_courier_id = :courier_id', [':courier_id' => $this->courier->id]);
    }


    /**
     * Add additions to package
     * @param $additions Array of additions ID's
     */
    public function addAdditions($additions)
    {
        if($additions == '' OR !S_Useful::sizeof($additions))
            return false;

        // find if additons IDs are poroper and remove multiple additions from one group
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, group');
        $cmd->from((new CourierDomesticAdditionList)->tableName());
        $cmd->where('stat = :stat', array(':stat' => S_Status::ACTIVE));
        $cmd->andWhere('broker_id IS NULL or broker_id = :broker', array(':broker' => $this->getBrokerId()));
        $cmd->andWhere(array('in', 'id', $additions));
        $result = $cmd->queryAll();

        $temp = [];
        $temp2 = []; // for non grouped - not to by filtered by array_unique
        foreach($result AS $key => $item)
        {
            if($item['group'] === NULL)
                $temp2[$item['id']] = $item['group'];
            else
                $temp[$item['id']] = $item['group'];
        }
        $temp = array_unique($temp);

        // filter grouped additions - only one in group is allowed
        $temp = array_unique($temp);

        $additions = [];
        if(is_array($temp))
            foreach($temp AS $key => $item)
            {
                array_push($additions, $key);
            }

        if(is_array($temp2))
            foreach($temp2 AS $key => $item)
            {
                array_push($additions, $key);
            }


        $this->_additions = $additions;

    }

    /**
     * @param bool $forceNotSaved Ignore that model has already ID and try to get additions by array of additions ids
     * @return array|mixed|null|static
     */
    public function listAdditions($forceNotSaved = false)
    {
        if($this->id == NULL OR $forceNotSaved)
            $additions = CourierDomesticAdditionList::model()->findAllByAttributes(array('id' => $this->_additions));
        else
            $additions = CourierDomesticAddition::model()->findAllByAttributes(array('courier_id' => $this->courier->id));

        return $additions;
    }


    public function saveNewPackage()
    {

        if($this->courier->packages_number < 1)
            $this->courier->packages_number = 1;



        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $courierPackagesFamily = [];
        $courierParentId = NULL;
        $parentLabelId = NULL;
        for ($i = 0; $i < $this->courier->packages_number; $i++) {

            if ($errors)
                continue;

            /* @var $courierTypeDomestic CourierTypeDomestic */

            $courierTypeDomestic = S_Useful::cloneOrmModels((new CourierTypeDomestic()), $this);
            $courierTypeDomestic->_additions = $this->_additions;
            $courierTypeDomestic->courier = S_Useful::cloneOrmModels((new Courier_CourierTypeDomestic()), $this->courier);

            $this->courier->courierTypeDomestic = $courierTypeDomestic;

            $courierTypeDomestic->courier->senderAddressData = S_Useful::cloneOrmModels((new CourierTypeDomestic_AddressData()), $this->courier->senderAddressData);
            $courierTypeDomestic->courier->receiverAddressData = S_Useful::cloneOrmModels((new CourierTypeDomestic_AddressData()), $this->courier->receiverAddressData);

            $courierTypeDomestic->parent_courier_id = $courierParentId;
            $courierTypeDomestic->family_member_number = $i + 1;
            $courierTypeDomestic->courier->courierTypeDomestic = $courierTypeDomestic;

            if(!$courierTypeDomestic->_saveNewPackage())
                $errors = true;

            $courierTypeDomestic->courier->changeStat(CourierStat::PACKAGE_PROCESSING);


            $label = $courierTypeDomestic->finalizeDetails($parentLabelId);

            if(!$i) {
                $courierParentId = $courierTypeDomestic->courier->id;
                $parentLabelId = $label->id;
            }

            array_push($courierPackagesFamily, $courierTypeDomestic);
        }



        if($errors)
        {
            $transaction->rollback();
            return false;
        } else {
            $transaction->commit();
            return $courierPackagesFamily;
        }

    }

    protected function _saveNewPackage()
    {
        $errors = false;

        if (!$this->courier->saveWithAddressData(false))
            $errors = true;

        $this->courier_id = $this->courier->id;

//        if($this->cod_value > 0)
//        {
//            $this->cod = 1;
//            $this->cod_currency = Currency::PLN;
//        }

        if (!$this->save())
            $errors = true;

        /* @var $item CourierDomesticAdditionList */
        foreach($this->listAdditions(true) AS $item)
        {
            $addition = new CourierDomesticAddition();
            $addition->courier_id = $this->courier->id;
            $addition->description = $item->courierDomesticAdditionListTr->description;
            $addition->name = $item->courierDomesticAdditionListTr->title;
            $addition->courier_domestic_addition_list_id = $item->id;

            if(!$addition->save())
                $errors = true;
        }

        if ($errors)
            return false;
        else
            return true;

    }




    public function finalizeDetails($parentLabelId = NULL)
    {
        $courierLabelNew = new CourierLabelNew();
        $courierLabelNew->triggers_pickup = true;


        if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_INPOST)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_INPOST;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_RUCH)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_RUCH;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_GLS)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_GLS;
        else if(CourierDomesticOperator::getOperatorById($this->domestic_operator_id) == CourierDomesticOperator::OPERATOR_SAFEPAK)
        {
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_SAFEPAK;
        }
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_UPS;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_SAVER)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_UPS_SAVER;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_EXPRESS)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_UPS_EXPRESS;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_M_EXPRESS)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_UPS_EXPRESS_M;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_DHL;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL_PALETA)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_DHL;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL_F)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_DHL_F;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL_F_PALETA)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_DHL_F;
        else if($this->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL_F_DLUZYCE)
            $courierLabelNew->operator = CourierLabelNew::OPERATOR_DHL_F;

        $courierLabelNew->courier_id = $this->courier->id;
        $courierLabelNew->address_data_from_id = $this->courier->sender_address_data_id;
        $courierLabelNew->address_data_to_id = $this->courier->receiver_address_data_id;


        if($parentLabelId !== NULL && in_array($this->domestic_operator_id, CourierDomesticOperator::getMultiOperators())) {
            $courierLabelNew->parent_id = $parentLabelId;
            $courierLabelNew->stat = CourierLabelNew::STAT_WAITING_FOR_PARENT;
        } else
            $courierLabelNew->stat = CourierLabelNew::STAT_NEW;

        $courierLabelNew->save();

        $this->courier_label_new_id = $courierLabelNew->id;
        $this->save(array('courier_label_new_id', 'date_updated'));

        return $courierLabelNew;

    }

    /**
     * Checks if package has particular addition
     * @param $id Integer Id of CourierAdditionList
     * @return boolean
     */
    public function hasAdditionById($id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from = (new CourierDomesticAddition())->tableName();
        $cmd->where('courier_id = :courier_id', [':courier_id' => $this->courier->id]);
        $cmd->Andwhere('courier_domestic_addition_list_id = :addition_id', [':addition_id' => $id]);
        return $cmd->queryScalar();
    }




    public static function validateWholeGroup(array $couriers, $model = NULL)
    {
        $errors = false;

        /* @var $courier Courier */
        foreach($couriers AS $key => $courier)
        {


            if(!$courier->validate())
                $errors = true;

            if(!$courier->courierTypeDomestic->validate())
                $errors = true;

            if(!$courier->senderAddressData->validate())
                $errors = true;

            if(!$courier->receiverAddressData->validate())
                $errors = true;

            $couriers[$key] = $courier;
        }

        if($errors)
        {
            return ['success' => false, 'data' => $couriers];
        } else {
            return ['success' => true, 'data' => $couriers];
        }

    }

    public static function saveWholeGroup(array $couriers)
    {
        $errors = false;

        $packages = [];

        /* @var $courier Courier */
        foreach($couriers AS $key => $courier)
        {
            if($courier->courierTypeDomestic->courier === NULL)
                $courier->courierTypeDomestic->courier = $courier;


            $return = $courier->courierTypeDomestic->saveNewPackage();
            if(is_array($return))
                $packages = array_merge($packages, $return);
        }

        if($errors)
        {
            return ['success' => false, 'data' => $couriers];
        } else {
            return ['success' => true, 'data' => $packages];
        }

    }



}
