<?php

class PerformanceMonitor extends CApplicationComponent
{

    protected $_lastTime = [];

    public function m($id, $what, $start = false)
    {
        return false;
        $filename = 'performance_monitor.txt';

        $base = Yii::app()->basePath . DIRECTORY_SEPARATOR .'_dump';
        $baseWithDate = $base.'/'.date('Ymd');

        if(!is_dir($baseWithDate))
            @mkdir($baseWithDate);

        $filename = $baseWithDate . DIRECTORY_SEPARATOR . $filename;

        if($start) {
            $this->_lastTime[$id] = microtime(true);
        }

        $lastTime = $this->_lastTime[$id];
        $currentTime = microtime(true);
        $diff = number_format($currentTime - $lastTime,4);

        $this->_lastTime[$id] = $currentTime;

        $data = date('Y-m-d H:i:s').'||'.$id.'||+'.$diff.'||'.$currentTime.'||'.$what."\r\n";

        @file_put_contents($filename, $data, FILE_APPEND);
    }



}