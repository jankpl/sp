<?php

Yii::import('application.modules.postal.models._base.BasePostalImportManager');

class PostalImportManager extends BasePostalImportManager
{
    const SOURCE_FILE = 0;
    const SOURCE_EBAY = 1;
    const SOURCE_LINKER = 2;


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('hash, postal_ids, user_id', 'required'),
            array('source', 'default', 'value' => self::SOURCE_FILE),
            array('hash', 'length', 'max'=>64),
            array('id, hash, postal_ids, date_entered, user_id, source', 'safe', 'on'=>'search'),
        );
    }


    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),

        );
    }

    public function beforeSave()
    {
        $this->postal_ids = serialize($this->postal_ids);
        $this->postal_ids = base64_encode($this->postal_ids);

        return parent::beforeSave();
    }

    public function afterFind()
    {
        if($this->postal_ids == '')
            return array();

        $this->postal_ids = base64_decode($this->postal_ids);
        $this->postal_ids = unserialize($this->postal_ids);

        parent::afterFind();
    }

    public function addPostalIds(array $postal_ids)
    {
//        if(!is_array($this->postal_ids))
//            $this->postal_ids = array();

        $this->postal_ids = $postal_ids;
    }

    public static function getByHash($hash, $dontGenerate = false, $ignoreUserId = false)
    {

        if($ignoreUserId)
            $model = self::model()->findByAttributes(['hash' => $hash]);
        else
            $model = self::model()->findByAttributes(['hash' => $hash, 'user_id' => Yii::app()->user->id]);

        if($model)
            return $model;
        else
        {
            if($dontGenerate)
                return false;

            $model = new self;
            $model->user_id = Yii::app()->user->id;
            $model->hash = $hash;
            $model->save();
        }

        return $model;
    }


}