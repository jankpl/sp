<?php

class CourierTypeDomestic_AddressData extends AddressData
{

    public $smsNotification = true;

    public $addToContactBook;
    public $contactBookType;

    public $_bankAccountRequired = false;

    public $bankAccountSaveInContactBook;

    public function rules() {

        $arrayValidate = array(

//            array('address_line_1',
//                'match',
//                'pattern' => '/^(.+)\s(.+)$/',
//                'message' => Yii::t('site', 'Adres musi zawierać ulicę oraz oddzielony spacją budynek i lokal'),
////                'except' => Courier::MODE_SAVE_NO_VALIDATE,
//                'enableClientValidation' => true
//            ),

            array('addToContactBook, contactBookType', 'safe'),

            array('email', 'email'),

            array('name, company, city, address_line_1, address_line_2, zip_code', 'application.validators.lettersNumbersBasicValidator'),
            array('tel', 'application.validators.telValidator'),

            array('country_id, city, address_line_1, tel, zip_code', 'required'),

            array('name', 'application.validators.eitherOneValidator', 'other' => 'company'),

            array('bankAccountSaveInContactBook', 'boolean', 'allowEmpty' => true),

            array('smsNotification', 'boolean', 'allowEmpty' => true),

            array('bankAccount', 'application.validators.lettersNumbersBasicValidator'),
            array('bankAccount', 'length', 'max'=> 50, 'min' => 15),


            array('name,company,country,zip_code,city,address_line_1,address_line_2,tel', 'length', 'max'=>45),

            array('email', 'length', 'max'=>45),

            array('country_id', 'safe'),

            array('id,name,company,country,zip_code,city,address_line_1,address_line_2,tel, email,', 'safe', 'on'=>'search'),

            array('params', 'safe'),

            array(implode(',', array_keys(self::getAttributes())),'safe', 'on' => 'clone'),

            array('country_id', 'compare', 'compareValue' => CountryList::COUNTRY_PL, 'message' => 'Only PL is allowed!'),
        );


        $arrayFilter =
            array(
                array('country_id, date_entered, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'filter', 'filter' => array( $this, 'filterStripTags')),

                array('bankAccount', 'filter', 'filter' => array( $this, 'filterStripTags')),

                array('date_entered', 'default',
                    'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

                array('country_id, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'default', 'setOnEmpty' => true, 'value' => null),
            );

        return array_merge($arrayFilter, $arrayValidate);
    }

    public function afterValidate()
    {
        if($this->_bankAccountRequired)
        {
            if($this->bankAccount == '')
                $this->addError('bankAccount', Yii::t('courier', 'Podanie rachunku bankowego jest wymagane przy COD.'));
        }

        parent::afterValidate();
    }

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bDefaultTelNumberAddressData' =>
                    [
                        'class'=>'application.modules.courier.models.bDefaultTelNumberAddressData'
                    ],
                'bClearEmailAddressData' =>
                    [
                        'class'=>'application.models.bClearEmailAddressData'
                    ],
                'bValidateZipCodeWithBaseAddressData' =>
                    [
                        'class'=>'application.models.bValidateZipCodeWithBaseAddressData'
                    ],
                'bFilterTelNumberAddressData' =>
                    [
                        'class'=>'application.models.bFilterTelNumberAddressData'
                    ],
            ]
        );
    }

    public function beforeSave()
    {

        if($this->bankAccount != '')
            $this->setBankAccount($this->bankAccount);

        if(!Yii::app()->user->isGuest && !Yii::app()->user->getModel()->hasUserAvailableSmsOptions())
            $this->smsNotification = false;

        if($this->smsNotification)
        {
            $this->setSmsNotifyActive();
        }


        return parent::beforeSave();
    }


    public function attributeLabels()
    {
        return CMap::mergeArray(
            [
                'addToContactBook' => Yii::t('_addressData','Dodaj do swojej książki adresowej'),
                'bankAccountSaveInContactBook' => Yii::t('courier','Przypisz konto do nadawcy w książce adresowej'),
                'bankAccount' => Yii::t('courier','Konto bankowe'),
                'smsNotification' => Yii::t('sms_courier', 'SMS Notifications'),
            ],
          parent::attributeLabels());
    }

    /**
     * List attributes including non-db attributes by default
     * @param bool|true $names
     * @param bool|false $omitNonDb Do not include special attributes
     * @return array
     */
    public function getAttributes($names = true, $omitNonDb = false)
    {
        $attributes = parent::getAttributes($names);
        if(!$omitNonDb) {
            $attributes['addToContactBook'] = $this->addToContactBook;
            $attributes['contactBookType'] = $this->contactBookType;
            $attributes['bankAccount'] = $this->bankAccount;
            $attributes['bankAccountSaveInContactBook'] = $this->bankAccountSaveInContactBook;
            $attributes['smsNotification'] = $this->smsNotification;
        }

        return $attributes;
    }

    /**
     * Save to DB with optional automatic save to contact book
     *
     * @return bool
     */
    public function afterSave()
    {

        if($this->addToContactBook)
        {
            UserContactBook::createAndSave(false, $this->getAttributes(true, true), $this->contactBookType, false, true, $this->bankAccountSaveInContactBook);
        }

        return parent::afterSave();
    }

    /**
     * Constructor allowing setting userContactBook type for future saving (whether it's sender or receiver)
     *
     * @param string $scenario
     * @param string|false $contactBookType
     */
    public function __construct($scenario='insert', $contactBookType = false)
    {
        if($contactBookType)
            $this->contactBookType = $contactBookType;
        return parent::__construct($scenario);
    }
}
