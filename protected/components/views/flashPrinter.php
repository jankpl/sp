<?php
if(Yii::app()->user->hasFlash('weight-info')):?>
    <div class="alert alert-warning">
        <img src="<?= Yii::app()->getBaseUrl();?>/images/layout/new/weight.png"/>&nbsp;&nbsp;&nbsp; <?= Yii::app()->user->getFlash('weight-info'); ?>
    </div>
<?php endif; ?>

<?php
foreach (Yii::app()->user->getFlashes() as $key => $message):
    ?>
    <div class="flash-<?= $key;?>"><?= $message;?></div>
    <?php
endforeach;
?>
