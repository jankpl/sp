<?php

/**
 * S_Payment class.
 */
class S_Payment extends CModel
{
    public $payment_type_id;
    public $payment_values;
    public $order_id;

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeNames()
    {
        return array(
            'name'=>'Name',
            'id'=>'ID',
            'type'=>'Type',
            'url'=>'URL',
        );
    }
}