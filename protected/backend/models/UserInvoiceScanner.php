<?php

class UserInvoiceScanner extends UserScanner
{
    public function returnUserInvoiceModels()
    {
        if(!is_array($this->items_ids))
            return [];

        $models = [];

        $this->items_ids = array_unique($this->items_ids);
        $this->items_ids_backup = $this->items_ids;

        foreach($this->items_ids AS $key => $item_id)
        {
            $item_id = trim($item_id);

            if($item_id == '')
                continue;

            $model = UserInvoice::model()->findByAttributes(['title' => $item_id, 'stat_inv' => UserInvoice::INV_STAT_NONE]);

            if($model)
            {
                $models[$key] = $model;
            }
        }

        return $models;
    }

}