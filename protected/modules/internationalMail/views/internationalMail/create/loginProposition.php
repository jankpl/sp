<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Janek
 * Date: 16.09.13
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */
?>

<div class="form">

    <h2><?php echo Yii::t('internationalMail','International Mail'); ?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '0/5')); ?> | <?php echo Yii::t('app','Zaloguj się'); ?></h2>

    <div>

        <?php $this->renderPartial('//site/_login', array(
            'model' => $model,
        )); ?>


    </div>


</div>
