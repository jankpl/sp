<?php

class _UserGridView extends User
{
    public $__packages;
    public $_contract_type;
    public $_debt_block;
    public $_debt_block_days;
    public $_id;
    public $_spzoo;


    public function rules() {

        $array = array(
            array('__packages, _contract_type, _debt_block, _debt_block_days, _id, _spzoo, injection_sp_point_id
                ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }


    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.id', $this->_id);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('pass', $this->pass, true);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('last_logged', $this->last_logged, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('company', $this->company, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('zip_code', $this->zip_code, true);
        $criteria->compare('address_line_1', $this->address_line_1, true);
        $criteria->compare('address_line_2', $this->address_line_2, true);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('nip', $this->nip, true);
        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('activated', $this->activated);
        $criteria->compare('type', $this->type);
        $criteria->compare('without_vat', $this->without_vat);
        $criteria->compare('price_currency_id', $this->price_currency_id);
        $criteria->compare('payment_on_invoice', $this->payment_on_invoice);
        $criteria->compare('premium', $this->premium);
        $criteria->compare('source_domain', $this->source_domain);
        $criteria->compare('injection_sp_point_id', $this->injection_sp_point_id);
        $criteria->compare('partner_id', $this->partner_id);


        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
            $criteria->compare('salesman_id', Yii::app()->user->model->salesman_id);
        else
            $criteria->compare('salesman_id', $this->salesman_id);
//
//        if($this->__packages)
//        {
//            $criteria->together  =  true;
//
//            $criteria->compare('courierCount',$this->__packages);
//        }

        if($this->_contract_type)
        {
            $criteria->join .= 'JOIN user_options user_options_s2 ON (user_options_s2.name = "getContractType" AND user_options_s2.user_id = t.id AND user_options_s2.value = '.intval($this->_contract_type).')';
        }

        if($this->_debt_block)
        {
            $criteria->join .= 'JOIN user_options user_options_s3 ON (user_options_s3.name = "debtBlock" AND user_options_s3.user_id = t.id AND user_options_s3.value = '.intval($this->_debt_block).')';
        }

        if($this->_debt_block_days)
        {
            $criteria->join .= 'JOIN user_options user_options_s4 ON (user_options_s4.name = "debtBlockDays" AND user_options_s4.user_id = t.id AND user_options_s4.value = '.intval($this->_debt_block_days).')';
        }

        if(intval($this->_spzoo) === -1)
        {
            $criteria->addCondition('t.id NOT IN (SELECT user_id FROM user_options WHERE name = "spZooStatus")');
        }
        else if(intval($this->_spzoo) === 1)
        {
            $criteria->join .= 'JOIN user_options user_options_s5 ON (user_options_s5.name = "spZooStatus" AND user_options_s5.user_id = t.id AND user_options_s5.value = 1)';
        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes = array(
//            '__packages' => array(
//                'asc' => 'couriersSTAT ASC',
//                'desc' => 'couriersSTAT DESC',
//            ),
            '*', // add all of the other columns as sortable
        );


        return new CActiveDataProvider($this->with('salesman'), array(
            'criteria' => $this->searchCriteria(),
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

    }

}