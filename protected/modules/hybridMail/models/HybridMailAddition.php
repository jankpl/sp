<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailAddition');

class HybridMailAddition extends BaseHybridMailAddition
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(

            array('date_entered',
                'default',
                'value'=>new CDbExpression('NOW()'),
                'on'=>'insert'),


            array('name, description, price_first_page, hybrid_mail_id', 'required'),
            array('price_first_page, price_next_page', 'numerical'),
            array('name', 'length', 'max'=>45),
            array('hybrid_mail_id', 'length', 'max'=>10),
            array('date_updated', 'safe'),
            array('date_updated, price_next_page', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, description, price_first_page, price_next_page, hybrid_mail_id', 'safe', 'on'=>'search'),
        );
    }

    public function getClientTitle()
    {
        return $this->name;
    }

    public function getClientDesc()
    {
        return $this->description;
    }

}