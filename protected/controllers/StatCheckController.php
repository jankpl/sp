<?php

class StatCheckController extends CController
{
    const HOURS_OLD = 1;
    const MINUTES_FRESH = 10;

    public function actionIndex()
    {
        Yii::app()->getModule('courier');

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*) AS no, stat')->from('courier_label_new')->where('date_entered >= (NOW() - INTERVAL '.self::HOURS_OLD.' HOUR) AND date_entered < (NOW() - INTERVAL '.self::MINUTES_FRESH.' MINUTE)')->group('stat');
        $result = $cmd->queryAll();

        $waiting = 0;
        $failed = 0;
        $success = 0;

        foreach($result AS $item)
        {
            if($item['stat'] == CourierLabelNew::STAT_SUCCESS)
                $success += $item['no'];
            else if(in_array($item['stat'], [CourierLabelNew::STAT_FAILED_WITH_PROBLEM_LABEL, CourierLabelNew::STAT_FAILED]))
                $failed += $item['no'];
            else
                $waiting += $item['no'];
        }

        $total = $waiting + $failed + $success;

        $data = [
            'w' => $waiting,
            's' => $success,
            'f' => $failed
        ];

        $dataP = [];
        if($total > 0)
        {
            $dataP = [
                'w' => round(100*$waiting/$total),
                's' => round(100*$success/$total),
                'f' => round(100*$failed/$total)
            ];
        }

        Yii::app()->getModule('postal');

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*) AS no, stat')->from('postal_label')->where('date_updated >= (NOW() - INTERVAL '.self::HOURS_OLD.' HOUR) AND date_updated < (NOW() - INTERVAL '.self::MINUTES_FRESH.' MINUTE)')->group('stat');
        $result = $cmd->queryAll();

        $waiting = 0;
        $failed = 0;
        $success = 0;

        foreach($result AS $item)
        {
            if($item['stat'] == PostalLabel::STAT_SUCCESS)
                $success += $item['no'];
            else if(in_array($item['stat'], [PostalLabel::STAT_FAILED]))
                $failed += $item['no'];
            else
                $waiting += $item['no'];
        }

        $total = $waiting + $failed + $success;

        $pData = [
            'w' => $waiting,
            's' => $success,
            'f' => $failed
        ];

        $pDataP = [];
        if($total > 0)
        {
            $pDataP = [
                'w' => round(100*$waiting/$total),
                's' => round(100*$success/$total),
                'f' => round(100*$failed/$total)
            ];
        }

        $response =
            [
                'c' => [
                    'd' => $data,
                    'p' => $dataP
                ],
                'p' => [
                    'd' => $pData,
                    'p' => $pDataP
                ],
            ];


        $response = CJSON::encode($response);
        echo $response;
    }

}