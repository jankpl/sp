<?php

class Semaphore
{
    const COURIER_LABEL_NEW_PREFIX = 9990;
    const POSTAL_LABEL_PREFIX = 9980;
    const FTP_PROCESSOR_PREFIX = 9970;
    const MAILBOT_PREFIX = 9960;
    const MANIFEST_PREFIX = 9950;

    const FORCE_USE_FILE = false;

    private $id;
    private $semId;
    private $isAcquired = false;
    private $isWindows;
    private $filename;
    private $filepointer;

    function __construct($id, $lockPath = '', $forceFile = self::FORCE_USE_FILE)
    {
        if(!function_exists('sem_get'))
            $forceFile = true;

        $this->id = $id;

        if (!is_int($this->id)) {
            throw new Exception('Semaphore key must be an integer');
        }

        if ($forceFile OR strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $this->isWindows = true;
            $this->filename = $lockPath . $id . '.lock';
        }

        if (!$this->isWindows && !($this->semId = sem_get($this->id, 1))) {
            throw new Exception("Error getting semaphore ID");
        }

//        MyDump::dump('sem.txt', $this->id.' : SEM CONTRUCTED');
    }

    public function acquire()
    {
//        MyDump::dump('sem.txt', $this->id.' : SEM AQUIRED');

        if ($this->isAcquired)
            return;

        if ($this->isWindows) {
            if (($this->filepointer = @fopen($this->filename, "w+")) === false) {
                throw new Exception("Error opening semaphore file");
            }

            if (flock($this->filepointer, LOCK_EX) === false) {
                throw new Exception("Error locking semaphore file");
            }

        } else {

            if (!sem_acquire($this->semId, true)){
//                MyDump::dump('sem.txt', $this->id.' : SEM NOT AQUIRED');
                throw new Exception("Error acquiring semaphore");
            }

        }
        $this->isAcquired = true;
    }

    public function release($remove = false)
    {
        if (!$this->isAcquired)
            return;

        if ($this->isWindows) {

            if (flock($this->filepointer, LOCK_UN) === false) {
                throw new Exception("Error unlocking semaphore file");
            }

            fclose($this->filepointer);
            if($remove)
            {
                @unlink($this->filename);
            }


        } else {

            if (!sem_release($this->semId)) {
//                MyDump::dump('sem.txt', $this->id.' : SEM NOT RELEASED');
                throw new Exception("Error releasing semaphore");
            }

            if($remove)
            {
                if (!sem_remove($this->semId)) {
//                    MyDump::dump('sem.txt', $this->id.' : SEM NOT REMOVED');
                    throw new Exception("Error removind semaphore");
                }
            }

        }
//        MyDump::dump('sem.txt', $this->id.' : SEM RELEASED');
        $this->isAcquired = false;
    }


    public static function removeSemaphore($id)
    {
       if(!($semId = @sem_get($id, 1))) {
//           MyDump::dump('semClean.txt', print_r($id, 1) . ' : SEM NOT INITED');
       }
//       else {
//           MyDump::dump('semClean.txt', print_r($id, 1) . ' : SEM GOT');
//       }

        if(!@sem_remove($semId)) {
//            MyDump::dump('semClean.txt', print_r($id, 1) . ' : SEM NOT REMOVED');
        }
//        else {
//            MyDump::dump('semClean.txt', print_r($id, 1) . ' : SEM CLEANED');
//        }
    }
}

