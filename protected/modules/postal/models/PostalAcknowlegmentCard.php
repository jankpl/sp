<?php

class PostalAcknowlegmentCard
{


    protected static function printTableHeader(&$pdf)
    {
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(10,3,Yii::t('postal_pdf','L.p.'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('postal_pdf','Paczka'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('postal_pdf','Nadawca'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('postal_pdf','Odbiorca'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(30,3,Yii::t('postal_pdf','Parametry'),1,'C',false,0,'','',true,0,false,true,0,'T',true);

        $pdf->ln(4);

        $pdf->SetFont('freesans', '', 8);
    }

    public static function generateCardMultiPerOperator(array $postals, $bulkNo = false)
    {
        $done = [];

        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();
        $pdf->setMargins(10,10,10, true);
        $pdf->SetAutoPageBreak(false);


        $postalsGrouped = [];
        /* @var $postals Postal[] */
        foreach($postals AS $key => $item) {

            $operator = $item->postalOperator ? $item->postalOperator->name : '';

            if(!isset($postalsGrouped[$operator]))
                $postalsGrouped[$operator] = [];

            $postalsGrouped[$operator][] = $item;
        }


        foreach($postalsGrouped AS $group => $items) {
            $pdf->AddPage('P', 'A4');
            $pdf = self::generateCardMulti($items, $bulkNo, $pdf, $done, $group);
        }

        $pdf->Output('manifest.pdf', 'D');

        Postal::onAfterAckGenerationGroup($done);

        return $done;
    }

    public static function generateCardMulti(array $postals, $bulkNo = false, $perOperatorOwnPdf = false, &$done = false, $operatorHeader = false, $setManifestStatus = false)
    {
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );


        $done = [];

        /* @var $postals Postal[] */
        foreach($postals AS $key => $item)
        {
            if(in_array($item->postal_stat_id, [PostalStat::CANCELLED_PROBLEM, PostalStat::CANCELLED, PostalStat::CANCELLED_USER]))
                unset($postals[$key]);
        }


        $totalNumber = S_Useful::sizeof($postals);

        $numberOfBulkGroups = [];
        $numberOfBulk = 0;
        /* @var $item Postal */
        foreach($postals AS $item)
        {
            if($item->bulk) {
                $numberOfBulkGroups[$item->bulk_id] = true;
                $numberOfBulk++;
            }
        }
        $numberOfBulkGroups = S_Useful::sizeof($numberOfBulkGroups);
        $totalNumber -= $numberOfBulk;

        $numberOfBulkPages = ceil(($numberOfBulkGroups * 5 + $numberOfBulk) / 35);

        if($totalNumber > 2500)
            throw new Exception(Yii::t('postal','Za dużo paczek do wygenerowania potwierdzenia nadania.'));


        $pageSize = 17;
        $firstPageSizeOffset = 1;
        $lastPageSizeOffset = 0;

        if(!$perOperatorOwnPdf) {
            /* @var $pdf TCPDF */
            $pdf = PDFGenerator::initialize();
            $pdf->setMargins(10,10,10, true);

            $pdf->AddPage('P','A4');
        } else {
            $pdf = $perOperatorOwnPdf;
        }


        $txt = '';
        if($operatorHeader)
            $txt .= $operatorHeader."\r\n";

        if($bulkNo)
            $txt .= $bulkNo."\r\n";

        $pdf->MultiCell(60,10,$txt,0,'L', false, 0, 10,5,false,false,false,true,10,'T', true);

//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);


        $title = Yii::t('courier_pdf','Potwierdzenie nadania');



        $pdf->MultiCell(140,8,$prefix.$title.$suffix,1,'C',false,0,10, 5,true,0,false,true,0,'M',true);

        $pdf->SetY($pdf->GetY() + 8);
        $pdf->SetTextColor(0,0,0);

        $pw = $pdf->getPageWidth();
        $ph = $pdf->getPageHeight();

        $stat_packages = 0;
        $stat_weight = 0;
        // for stats
        foreach($postals AS $item) {

//            if(in_array($item->courier_stat_id, [CourierStat::CANCELLED_PROBLEM, CourierStat::CANCELLED, CourierStat::CANCELLED_USER]))
//                continue;
//
//            if(in_array($item->user_cancel, [Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED]))
//                continue;
//
////            if($forReturns && $item->getType() != Courier::TYPE_EXTERNAL_RETURN)
////                continue;
//
//            if(!$forReturns && $item->getType() == Courier::TYPE_EXTERNAL_RETURN)
//                continue;

            $stat_packages++;
            $stat_weight += $item->weight;
        }

        $pdf->SetFont('freesans', '', 8);
        $y = $pdf->GetY();
        $pdf->MultiCell(50,8,Yii::t('courier_pdf','Łącznie paczek:').' '.$stat_packages."\r\n".Yii::t('courier_pdf','Łączna waga:').' '.$stat_weight.' [kg]',1,'R',false,0,$pw-60, 5,true,0,false,true,0,'M',true);
        $pdf->SetAbsY($y - 8);


        $firstModel = reset($postals);
//        $pdf->Image('@'.PDFGenerator::getLogoPlBig($firstModel->user), $pdf->getPageWidth() /2 - 15 - $pdf->getMargins()['right'], 5 , 40,'','','N',false);

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->SetY($pdf->GetY() + 8);
        $pdf->SetTextColor(0,0,0);

        $pdf->ln();

        self::printTableHeader($pdf);

        $pw = $pdf->getPageWidth();
        $ph = $pdf->getPageHeight();

        if($totalNumber <= ($pageSize))
            $totalPages = 1;
        else {
            $totalPages = ceil((($totalNumber)) / $pageSize);
        }

        $totalPagesWithBulk = $totalPages + $numberOfBulkPages;

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->MultiCell(20,3,'1/'.$totalPagesWithBulk,0,'R', false, 0, $pw-30, $ph - 10);
        $pdf->SetAbsX($x);
        $pdf->SetAbsY($y);

        $alreadyPrintedFromBulkId = [];

        $i = 0;
        $pageI = 0;
        $page = 1;

//        $withBarcode = false;
//        $lastBulkId = NULL;

        $isFirstItem = true;

        $bulkItems = [];

        /* @var $item Postal */
        $tc = 1;
        foreach($postals AS $key => $item) {

            if($item->bulk && $item->bulk_id && !isset($alreadyPrintedFromBulkId[$item->bulk_id]))
            {
                $bulkModels = Postal::getPostalsForBulkId($item->bulk_id, true);

                if(S_Useful::sizeof($bulkModels)) {
                    $bulkItems[] = $bulkModels;
                    $alreadyPrintedFromBulkId[$item->bulk_id] = true;
                    continue;
                }

            } else if(isset($alreadyPrintedFromBulkId[$item->bulk_id]))
                continue;

            if($setManifestStatus && $item->stat_map_id == StatMap::MAP_NEW && $item->postal_stat_id != PostalStat::STAT_MANIFEST_GENERATED_CUSTOMER)
                $item->changeStat(PostalStat::STAT_MANIFEST_GENERATED_CUSTOMER);


            $pdf->MultiCell(10, 15, $tc, 1, 'C', false, 0, '', '', true, 0, false, true, 0, 'M', true);
            $y = $pdf->GetY();
            $x = $pdf->GetX();
            $pdf->MultiCell(50,4,Yii::t('postal_pdf','Nr paczki:').' '.$item->local_id,1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->SetAbsY($y + 4);
            $pdf->SetAbsx($x);
            $cellContent = Yii::t('postal_pdf','Data nadania:').' '.substr($item->date_entered,0,10);
            $cellContent .= "\r\n" . Yii::t('postal_pdf', 'Content') . ': ' . $item->content;
            $pdf->MultiCell(50,6,$cellContent,1,'L',false,0,'','',true,0,false,true,6,'M',true);

            $currentY = $pdf->GetY();
            $currentX = $pdf->GetX();
            $pdf->MultiCell(50,5,'',1,'L',false,0,20,$currentY + 6,true,0,false,true,5,'M',true);

            $pdf->SetAbsY($currentY);
            $pdf->SetAbsX($currentX);
            if(1)
                $pdf->write1DBarcode($item->local_id, 'C128', 20, $currentY + 5.5, 50, 6, 1, $barcodeStyle);

            $pdf->SetAbsY($y);

            if($item->senderAddressData !== NULL) {
                $cellContent = $item->senderAddressData->company != '' ? $item->senderAddressData->company . "\r\n" : '';
                $cellContent .= $item->senderAddressData->name != '' ? $item->senderAddressData->name . "\r\n" : '';
                $cellContent .= $item->senderAddressData->address_line_1 . "\r\n";
                $cellContent .= $item->senderAddressData->address_line_2 != '' ? $item->senderAddressData->address_line_2 . "\r\n" : '';
                $cellContent .= $item->senderAddressData->zip_code . ', ';
                $cellContent .= $item->senderAddressData->city;

                $y = $pdf->GetY();
                $x = $pdf->GetX();
                $pdf->MultiCell(50, 11, $cellContent, 1, 'L', false, 0, '', '', true, 0, false, true, 0, 'T', true);
                $pdf->SetAbsY($y + 11);
                $pdf->SetAbsx($x);
                $pdf->MultiCell(50, 4, $item->senderAddressData->country, 1, 'L', false, 0, '', '', true, 0, false, true, 0, 'T', true);
                $pdf->SetAbsY($y);


            } else
                $pdf->MultiCell(50, 15, Yii::t('postal_pdf','Dane umieszczone na przesyłce zgodnie z regulaminem'), 1, 'C', false, 0, '', '', true, 0, false, true, 0, 'M', true);

            if($item->receiverAddressData !== NULL) {
                $cellContent = $item->receiverAddressData->company != '' ? $item->receiverAddressData->company . "\r\n" : '';
                $cellContent .= $item->receiverAddressData->name != '' ? $item->receiverAddressData->name . "\r\n" : '';
                $cellContent .= $item->receiverAddressData->address_line_1 . "\r\n";
                $cellContent .= $item->receiverAddressData->address_line_2 != '' ? $item->receiverAddressData->address_line_2 . "\r\n" : '';
                $cellContent .= $item->receiverAddressData->zip_code . ', ';
                $cellContent .= $item->receiverAddressData->city;

                $y = $pdf->GetY();
                $x = $pdf->GetX();
                $pdf->MultiCell(50, 11, $cellContent, 1, 'L', false, 0, '', '', true, 0, false, true, 0, 'T', true);
                $pdf->SetAbsY($y + 11);
                $pdf->SetAbsx($x);
                $pdf->MultiCell(50, 4, $item->receiverAddressData->country, 1, 'L', false, 0, '', '', true, 0, false, true, 0, 'T', true);
                $pdf->SetAbsY($y);
            } else
                $pdf->MultiCell(50,15,Yii::t('postal_pdf','Dane umieszczone na przesyłce zgodnie z regulaminem'),1,'C',false,0,'','',true,0,false,true,0,'M',true);

            $y = $pdf->GetY();
            $x = $pdf->GetX();
            $pdf->MultiCell(30,5,$item->getWeightClassName(),1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->SetAbsY($y + 5);
            $pdf->SetAbsx($x);
            $pdf->MultiCell(30,5,$item->getSizeClassName(),1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->SetAbsY($y + 10);
            $pdf->SetAbsx($x);
            $pdf->MultiCell(30,5,$item->getPostalTypeName(),1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->SetAbsx($x);
            $pdf->SetAbsY($y);
            $pdf->ln(15.2);

            // break first page earlier to make space for header
            if($page == 1 && $pageI > 0  && ($pageI%($pageSize-$firstPageSizeOffset) == 0)) {

                $pdf->SetFont('freesans', '', 8);
//
//                if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH) {
//                    $pdf->MultiCell(0, 10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                    $pdf->MultiCell(0, 6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);
//                }

                $pdf->AddPage('P', 'A4');

                if ($i < ($totalNumber - 1)) {
                    self::printTableHeader($pdf);
                }


                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPagesWithBulk,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }
            // break last page earlier to make space for footer
            else if($page > 1 && $page == $totalPages && ($pageI%($pageSize-$lastPageSizeOffset) == 0))
            {

                $pdf->SetFont('freesans', '', 8);
//                if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH) {
//                    $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                    $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);
//                }

                if(($page+1) < $totalPages) {

                    $pdf->AddPage('P', 'A4');
                    if ($i < ($totalNumber - 1)) {
                        self::printTableHeader($pdf);
                    }


                    $x = $pdf->GetX();
                    $y = $pdf->GetY();
                    $pdf->MultiCell(20,3,($page+1).'/'.$totalPagesWithBulk,0,'R', false, 0, $pw - 30, $ph - 10);
                    $pdf->SetAbsX($x);
                    $pdf->SetAbsY($y);


                    $page++;
                    $pageI = 0;
                }
            }
            else if(($pageI%$pageSize) == 0 && $pageI > 0)
            {

                $pdf->SetFont('freesans', '', 8);

//                if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH) {
//                    $pdf->MultiCell(0,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                    $pdf->MultiCell(0,6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '' , $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);
//                }

                $pdf->AddPage('P', 'A4');

                if ($i < ($totalNumber - 1)) {
                    self::printTableHeader($pdf);
                }


                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20,3,($page+1).'/'.$totalPagesWithBulk,0,'R', false, 0, $pw - 30, $ph - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $page++;
                $pageI = 0;
            }

            $i++;
            $pageI++;


            $isFirstItem = false;
            $tc++;


            $done[$key] = $item->id;
        }




        if(S_Useful::sizeof($bulkItems)) {
            $pdf->SetFont('freesans', '', 8);
//            if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH) {
//                $pdf->MultiCell(0, 10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                $pdf->MultiCell(0, 6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);
//            }
            self::_generateBulkPart($pdf, $bulkItems, $isFirstItem, $page, $totalPagesWithBulk);

        }


        $pdf->SetAbsY($pdf->GetY() + 20);

        $pdf->SetFont('freesans', '', 8);
//        if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH) {
//            $pdf->MultiCell(0, 10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//            $pdf->MultiCell(0, 6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);
//        }


        if($perOperatorOwnPdf)
            return $pdf;

        /* @var $pdf TCPDF */
        $pdf->Output('manifest.pdf', 'D');

        Postal::onAfterAckGenerationGroup($done);

        return $done;
    }


    protected static function _generateBulkPart(TcPDF &$pdf, array $bulkItems, $isFirstPage, $countFrom, $totalPages)
    {

        $i = 1;
        $pageUsage = false;
        $lastY = 0;
        $pageAdded = false;
        foreach($bulkItems AS $item) {
            if (!$isFirstPage) {

                if(($pageUsage + 5 + S_Useful::sizeof($item)) > 35 OR $pageUsage === false) {
                    $pdf->AddPage('P', 'A4');
                    $pageUsage = 0;
                    $lastY = 0;
                    $pageAdded = true;
                } else
                    $pageAdded = false;

                $pageUsage += 4 + S_Useful::sizeof($item);
            }
            else
            {
                $lastPage = $pdf->getPage();
                $pdf->deletePage($lastPage);
                $pdf->AddPage('P', 'A4');
                $pageAdded = true;
                $i--;
                $totalPages--;
            }


            $lastY = self::_generateBulkPartItem($pdf, $item, $lastY);
            if($pageAdded) {
                $pdf->SetFont('freesans', '', 8);
//                if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH) {
//                    $pdf->MultiCell(0, 10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//                    $pdf->MultiCell(0, 6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);
//                }

                $x = $pdf->GetX();
                $y = $pdf->GetY();
                $pdf->MultiCell(20, 3, ($countFrom + $i) . '/' . $totalPages, 0, 'R', false, 0, $pdf->getPageWidth() - 30, $pdf->getPageHeight() - 10);
                $pdf->SetAbsX($x);
                $pdf->SetAbsY($y);

                $i++;
            }
            $isFirstPage = false;


        }

    }

    protected static function _generateBulkPartItem(TcPDF &$pdf, array $items, $baseY = 0)
    {
        /* @var $firstItem Postal */
        $firstItem = reset($items);

        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        /* @var $item Postal */
        $packagesTotal = 0;
        foreach($items AS $item)
            $packagesTotal += $item->bulk_number;

        $pdf->SetFont('freesans', '', 10);

        $cellContent = Yii::t('postal_pdf', 'Masowa - ID importu').":\r\n";
        $cellContent .= Yii::t('postal_pdf','Data nadania:')."\r\n";
        $cellContent .= Yii::t('postal_pdf','Paczek:')."\r\n";
        $cellContent .= Yii::t('postal_pdf','Klient:');

        $pdf->MultiCell(50,18,$cellContent,0,'R',false,0,10,$baseY+6,true,0,false,true,0,'T',true);

        $pdf->SetFont('freesans', 'B', 10);

        $cellContent = $firstItem->bulk_id."\r\n";
        $cellContent .= substr($firstItem->date_entered,0,10)."\r\n";
        $cellContent .= $packagesTotal."\r\n";
        $cellContent .= $firstItem->user->login;

        $pdf->MultiCell(50,18,$cellContent,0,'L',false,0,60,$baseY+6,true,0,false,true,0,'T',true);

        $pdf->ln(20);

        $pdf->SetFont('freesans', 'B', 10);
        $pdf->MultiCell(10,5,Yii::t('postal_pdf','L.p.'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(30,5,Yii::t('postal_pdf','Nr paczki'),1,'C',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(20,5,Yii::t('postal_pdf','Liczba'),1,'C',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,5,Yii::t('postal_pdf','Państwo odbiorcy'),1,'C',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(20,5,Yii::t('postal_pdf','Typ'),1,'C',false,0,'','',true,0,false,true,0,'T',true);

        $pdf->MultiCell(30,5,Yii::t('postal_pdf','Klasa wagowa'),1,'C',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(30,5,Yii::t('postal_pdf','Klawa wymiarowa'),1,'C',false,0,'','',true,0,false,true,0,'T',true);

        $pdf->ln(6);

        $pdf->SetFont('freesans', '', 10);

        $i = 1;
        /* @var $item Postal */
        foreach($items AS $item)
        {
            $pdf->MultiCell(10,7,$i,1,'L',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,7,$item->local_id,1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,7,$item->bulk_number,1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(50,7,$item->getReceiverCountryName(),1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,7,$item->getPostalTypeName(),1,'C',false,0,'','',true,0,false,true,0,'M',true);

            $pdf->MultiCell(30,7,$item->getWeightClassName(),1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,7,$item->getSizeClassName(),1,'C',false,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln(7);

            $i++;
        }


        $y = $pdf->GetY();
        $pdf->write1DBarcode($firstItem->bulk_id, 'C128', 125, $baseY+4, 60, 18, 1, $barcodeStyle);
        $pdf->SetAbsY($y);

        return $y;
    }

    public static function generateCard(Postal $item)
    {

        if ($item->bulk && $item->bulk_id) {
            $bulkModels = Postal::getPostalsForBulkId($item->bulk_id, true);

            // if this is bulk import, print all ack at once
            if (S_Useful::sizeof($bulkModels)) {
                self::generateCardMulti([$item]);
                Yii::app()->end();
            }
        }


        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();
        $pdf->setMargins(10, 10, 10, true);

        if (in_array($item->postal_stat_id, [PostalStat::CANCELLED_PROBLEM, PostalStat::CANCELLED, PostalStat::CANCELLED_USER])) {
            // just throw empty page if package is Cancelled or Problem
            $pdf->Output('manifest.pdf', 'D');
            Yii::app()->end();
        }

        $pdf->AddPage('P', 'A4');

//        $pdf->setMargins(10,10,10,true);
//        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false);


        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        if ($item->bulk) {
            $y = $pdf->GetY();
            $pdf->write1DBarcode($item->bulk_id, 'C128', 155, 5, 40, 14, 1, $barcodeStyle);
            $pdf->SetFont('freesans', '', 6);
            $pdf->MultiCell(0, 0, Yii::t('postal_pdf', 'Bulk ID'), 0, 'C', false, 1, 151, 18, true, 0, false, true, 0);
            $pdf->SetAbsY($y);

        }

//        $pdf->Image('@' . PDFGenerator::getLogoPlBig($item->user), $pdf->getPageWidth() / 2 - 15 - $pdf->getMargins()['right'], 5, 50, '', '', 'N', false);

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->SetY($pdf->GetY() + 15);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->SetFont('freesans', '', 20);
        $y = $pdf->GetY();
        $pdf->MultiCell(0, 0, Yii::t('postal_pdf', 'Potwierdzenie nadania'), 1, 'L', false);

        $pdf->MultiCell(0, 0, "#" . $item->local_id, 0, 'R', false, 1, 15, $y, true, 0, false, true, 0);


        $pdf->SetAbsY($pdf->GetY() + 5);

        if ($item->senderAddressData !== NULL) {
            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0, 0, Yii::t('postal_pdf','Nadawca:'), 0, 'L');
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() - 5);
            $data = array(
                array(Yii::t('postal_pdf', 'Firma:'), $item->senderAddressData->company),
                array(Yii::t('postal_pdf', 'Imię i nazwisko:'), $item->senderAddressData->name),
                array(Yii::t('postal_pdf', 'Adres:'), $item->senderAddressData->address_line_1),
                array(Yii::t('postal_pdf', 'Adres c.d.:'), $item->senderAddressData->address_line_2),
                array(Yii::t('postal_pdf', 'Kod pocztowy:'), $item->senderAddressData->zip_code),
                array(Yii::t('postal_pdf', 'Miasto:'), $item->senderAddressData->city),
                array(Yii::t('postal_pdf', 'Kraj:'), $item->senderAddressData->country),
            );
            self::ColoredTable($pdf, $data);
        }
        $pdf->SetAbsY($pdf->GetY() + 5);

        if ($item->receiverAddressData !== NULL) {

            $pdf->SetFont('freesans', 'B', 15);
            $pdf->MultiCell(0, 0, Yii::t('postal_pdf','Odbiorca:'), 0, 'L', false, 1);
            $pdf->SetFont('freesans', '', 12);
            $pdf->SetAbsY($pdf->GetY() - 5);
            $data = array(
                array(Yii::t('postal_pdf', 'Firma:'), $item->receiverAddressData->company),
                array(Yii::t('postal_pdf', 'Imię i nazwisko:'), $item->receiverAddressData->name),
                array(Yii::t('postal_pdf', 'Adres:'), $item->receiverAddressData->address_line_1),
                array(Yii::t('postal_pdf', 'Adres c.d.:'), $item->receiverAddressData->address_line_2),
                array(Yii::t('postal_pdf', 'Kod pocztowy:'), $item->receiverAddressData->zip_code),
                array(Yii::t('postal_pdf', 'Miasto:'), $item->receiverAddressData->city),
                array(Yii::t('postal_pdf', 'Kraj:'), $item->receiverAddressData->country),
            );
            self::ColoredTable($pdf, $data);
        }
        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'B', 15);
        $pdf->MultiCell(0, 0, Yii::t('postal_pdf','Szczegóły:'), 0, 'L', false, 1);
        $pdf->SetFont('freesans', '', 12);
        $pdf->SetAbsY($pdf->GetY() - 5);
        $data = array(
            array(Yii::t('postal_pdf', 'Data nadania:'), substr($item->date_entered, 0, 10)),
            array(Yii::t('postal_pdf', 'Klasa wagowa') . ' :', $item->getWeightClassName()),
            array(Yii::t('postal_pdf', 'Klasa wymiarowa') . ' :', $item->getSizeClassName()),
            array(Yii::t('postal_pdf', 'Typ:'), $item->getPostalTypeName()),
            array(Yii::t('postal_pdf', 'Content:'), $item->content),
        );

        if ($item->bulk) {
            array_push($data, array(Yii::t('postal_pdf', 'Masowa:'), $item->getBulkName()));
            array_push($data, array(Yii::t('postal_pdf', 'Masowa - ID importu:'), $item->bulk_id));
            array_push($data, array(Yii::t('postal_pdf', 'Masowa - liczba:'), $item->bulk_number));
            array_push($data, array(Yii::t('postal_pdf', 'Państwo odbiorcy:'), $item->getReceiverCountryName()));
        }

        array_push($data, [Yii::t('postal_pdf', 'ID klienta'), $item->user_id]);

        self::ColoredTable($pdf, $data);

        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', '', 9);

        $pdf->MultiCell(0, 0, Yii::t('postal_pdf', "Data wygenerowania potwierdzenia: {data}", ["{data}" => date('Y-m-d h:i')]), 0, 'L', false);

        $pdf->SetAbsY($pdf->GetY() + 5);


        $pdf->MultiCell(0, 0, Yii::t('postal_pdf', "Dziękujemy za skorzystanie z usług Świata Przesyłek.{nl}Twoja przesyłka zostanie obsłużona przez najlepszych operatorów w zależności od wybranego kierunku i rodzaju przesyłki.{nl}Korzystając z usług Świata Przesyłek akceptujesz regulamin świadczenia usług.", ["{nl}" => "\r\n"]), 0, 'L', false);


        $pdf->SetAbsY($pdf->GetY() + 20);

        $y = $pdf->GetY();
        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(0, 0, Yii::t('postal_pdf', 'Podpis Nadawcy:') . ' ....................................................', 0, 'L', false);

        $pdf->SetAbsY($y);
        $pdf->MultiCell(0, 0, Yii::t('postal_pdf', 'Podpis Kuriera:') . ' ....................................................', 0, 'R', false);

        $pdf->SetFont('freesans', '', 8);
//        if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_RUCH) {
//            $pdf->MultiCell(0, 10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 15);
//            $pdf->MultiCell(0, 6, 'headquarters : KAAB international mail and parcel distribution', 0, 'C', false, 1, '', $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - 10);
//        }


        $item->onAfterAckGeneration();

        /* @var $pdf TCPDF */
        $pdf->Output('manifest.pdf', 'D');
    }

    protected static function ColoredTable(TCPDF $pdf,$data, $header = array()) {
        // Colors, line width and B font
        $pdf->SetFillColor(255, 0, 0);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(128, 0, 0);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFont('', 'B', 10);
        // Header
        $w = array(40, 100);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $pdf->Ln();
        // Color and font restoration
        $pdf->SetFillColor(224, 235, 255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('');
        // Data
        $fill = 0;
        $i = 0;
        $size = S_Useful::sizeof($data);
        foreach($data as $row) {
            $border = 'LR';
            if(!$i) {
                if($size - 1)
                    $border = 'LRT';
                else
                    $border = 'LRTB';
            }
            else if($i == $size - 1)
                $border = 'LRB';


            $pdf->Cell($w[0], 6, $row[0].'  ', $border, 0, 'R', $fill);
            $pdf->Cell($w[1], 6, '  '.$row[1], $border, 0, 'L', $fill);

            $pdf->Ln();
            // $fill=!$fill;
            $i++;
        }

    }
}
