<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);


?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'hybrid-mail-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'date_entered',
		'date_updated',
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
				),
		'sender_name',
		/*
		'sender_country',
		'sender_zip',
		'sender_city',
		'sender_address_line_1',
		'sender_address_line_2',
		'sender_tel',
		'text',
		array(
				'name'=>'order_id',
				'value'=>'GxHtml::valueEx($data->order)',
				'filter'=>GxHtml::listDataEx(Order::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'stat',
				'value'=>'GxHtml::valueEx($data->stat0)',
				'filter'=>GxHtml::listDataEx(HybridMailStat::model()->findAllAttributes(null, true)),
				),
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>