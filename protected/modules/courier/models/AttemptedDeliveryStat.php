<?php

class AttemptedDeliveryStat
{
    public static function listOfStats()
    {
        $data = [
            9289,
            9339,
            9365,
            9406,
            9410,
            9556,
            9560,
            9681,
            9925,
            10083,
            10213,
            10221,
            10224,
            10232,
            10241,
            10300,
            10362,
            10459,
            10672,
            10743,
            11088,
            11108,
            11260,
            11350,
            11409,
            11422,
            11632,
            11766,
            11767,
            11768,
            11769,
            11770,
            11773,
            11776,
            11858,
            11882,
            12172,
            13033,
            13346,
            13567,
            13573,
            13576,
            13578,
            13583,
            13587,
            13601,
            13918,
            14079,
            14138,
            13652
        ];

        return $data;
    }

    public static function isStatAttemptedDelivery($courier_stat_id)
    {
        $result = in_array($courier_stat_id, self::listOfStats());

        return $result;
    }
}