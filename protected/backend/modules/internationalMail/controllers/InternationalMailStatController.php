<?php

class InternationalMailStatController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'InternationalMailStat'),
        ));
    }

    public function actionCreate() {
        $model = new InternationalMailStat;
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $statTr = new InternationalMailStatTr('create');
            $statTr->language_id = $item->id;
            $modelTrs[$item->id] = $statTr;
        }

        if (isset($_POST['InternationalMailStat'])) {
            $model->setAttributes($_POST['InternationalMailStat']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            $model->validate();

            if($model->hasErrors())
                $errors = true;

            $model->save();

            foreach(Language::model()->findAll('stat = 1') AS $item)
            {
                if($modelTrs[$item->id] === null)
                    $modelTrs[$item->id] = new InternationalMailStatTr;

                $modelTrs[$item->id]->setAttributes($_POST['InternationalMailStatTr'][$item->id]);
                $modelTrs[$item->id]->international_mail_stat_id = $model->id;
                $modelTrs[$item->id]->language_id = $item->id;

                if(! $modelTrs[$item->id]->save())
                    $errors = true;
            }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
            else
            {
                $transaction->rollback();
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionUpdate($id) {


        $model = $this->loadModel($id, 'InternationalMailStat');

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = InternationalMailStatTr::model()->find('international_mail_stat_id=:international_mail_stat_id AND language_id=:language_id', array(':international_mail_stat_id' => $model->id, ':language_id' => $item->id));

            if($modelTrs[$item->id] == NULL)
            {
                $statTr = new InternationalMailStatTr('create');
                $statTr->language_id = $item->id;
                $modelTrs[$item->id] = $statTr;
            }
        }

        if (isset($_POST['InternationalMailStat'])) {
            $model->setAttributes($_POST['InternationalMailStat']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            $model->validate();

            if($model->hasErrors())
                $errors = true;

            $model->save();

            foreach(Language::model()->findAll('stat = 1') AS $item)
            {
                if($modelTrs[$item->id] === null)
                    $modelTrs[$item->id] = new InternationalMailStatTr;
                $modelTrs[$item->id]->setAttributes($_POST['InternationalMailStatTr'][$item->id]);
                $modelTrs[$item->id]->international_mail_stat_id = $model->id;
                $modelTrs[$item->id]->language_id = $item->id;

                if(! $modelTrs[$item->id]->save())
                    $errors = true;
            }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
            else
            {
                $transaction->rollback();
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {

        $model = InternationalMailStat::model()->findByPk($id);
        if($model == NULL)
            throw new CHttpException(404);
        if($model->editable != S_Status::ACTIVE)
            throw new CHttpException(403, 'This status cannot be edited or deleted');

        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $model =  $this->loadModel($id, 'InternationalMailStat');

                foreach($model->internationalMailStatTrs AS $item)
                {
                    $item->delete();


                }

                $model->delete();
            }
            catch (Exception $ex)
            {
                throw new CHttpException(400, Yii::t('app', 'You cannot delete it!'));
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {

        $model = new InternationalMailStat('search');
        $model->unsetAttributes();

        if (isset($_GET['InternationalMailStat']))
            $model->setAttributes($_GET['InternationalMailStat']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}