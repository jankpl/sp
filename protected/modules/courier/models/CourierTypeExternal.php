<?php

Yii::import('application.modules.courier.models._base.BaseCourierTypeExternal');

class CourierTypeExternal extends BaseCourierTypeExternal
{
    public $regulations;
    public $regulations_rodo;


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function beforeSave()
    {
        $this->courier_external_operator_name = CourierExternalOperator::getNameById($this->courier_external_operator_id);

        return parent::beforeSave();
    }

    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
        );
    }

    public function attributeLabels()
    {
        return CMap::mergeArray(parent::attributeLabels(), RodoRegulations::attributeLabels());
    }

    public function rules() {


        $arrayValidate = array(

            array('courier_external_operator_id', 'numerical', 'integerOnly'=>true),

            array('external_id', 'length', 'max'=>64),

            array('courier_external_operator_name', 'length', 'max'=>128),

            array('external_id', 'default', 'setOnEmpty' => true, 'value' => null),

            array('id, courier_external_operator_id, courier_external_operator_name, external_id', 'safe', 'on'=>'search',
                'except' => Courier::MODE_SAVE_NO_VALIDATE),
        );


        $arrayFilter = array(
            array('courier_external_operator_id, courier_external_operator_name, external_id', 'default', 'setOnEmpty' => true, 'value' => null),

            array('courier_external_operator_id, courier_external_operator_name, external_id', 'filter', 'filter' => array( $this, 'filterStripTags')),
        );

        return array_merge($arrayFilter, $arrayValidate, RodoRegulations::regulationsRules(''));
    }
}