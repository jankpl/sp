<?php


$json = [];
foreach(LpExpressClient::getTerminals() AS $item)
{

    $addressHuman = $item->address;
    $addressHuman .= "<br/>" . $item->zip . ', ' . $item->city;
    $addressHuman .= "<br/>" . strtoupper($item->address->country0->countryListTr->name);

    $lat = $item->latitude;
    $lng = $item->longitude;

    $desc = $item->comment.'<br/>';
    $desc .= $item->workinghours;




    $json[] = [
        'id' => $item->machineid,
        'name' => $item->name,
        'address' => $addressHuman,
        'desc' => $desc,
        'lat' => $lat,
        'lng' => $lng,
        'imgUrl' => Yii::app()->baseUrl . '/images/v2/lp_express_map_ico.png',
    ];
}

echo '<script>$(document).ready(function(){
    CourierInternalModal_Lp_Express.mapStart('.CJSON::encode($json).');
});</script>';

Yii::app()->clientScript->registerScript('_modal_lp_express', '
$(document).ready(function(){
    CourierInternalModal_Lp_Express.mapStart('.CJSON::encode($json).');
});
      ');

?>
<div id="lp-express-map" style="width: 100%; height: 400px; margin: 10px auto;"></div>
<div class="text-center">
    <br/>
<?= CHtml::dropDownList('_modal_lp_list', '', CHtml::listData(LpExpressClient::getTerminals(),'machineid', 'name'), ['prompt' => '-- wybierz punkt --']); ?>
    <br/>
    <br/>
    <input type="button" class="btn btn-lg btn-primary" value="Wybierz ten punkt" id="lp_express_point_confirm"/>
    <br/>    <br/>
</div>
