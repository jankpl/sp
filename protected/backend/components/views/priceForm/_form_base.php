<?php
/* @var $model Price */
/* @var $vertical boolean */
/* @var $showValidation */
/* @var $readonly */
?>
<?php
$currencyList = PriceCurrency::model()->findAll();
$z = $i;
?>

<table class="table table-bordered table-striped table-condensed">
    <?php if($vertical):?>
        <?php foreach($currencyList AS $currency):?>
            <tr>
                <th style="width: 50px;">
                    <?php echo CHtml::activeTextField($model->getValue($currency->id), ($formFieldPrefix!==NULL?$formFieldPrefix:'').'['.$currency->id.']'.'value', array('style' => 'width: 50px;', 'data-price-value' => $currency->id, 'data-price-key' => 1, 'readonly' => $readonly)); ?>
                </th>
                <td>
                    <?php echo $currency->symbol;?>
                </td>
            </tr>
        <?php endforeach;?>
    <?php
    else:
        ?>
        <tr>
            <?php foreach($currencyList AS $currency):?>
                <th class="text-center" style="text-align: right;">
                    <?php echo $currency->symbol;?>
                </th>
            <?php endforeach;?>
        </tr>
        <tr>
            <?php foreach($currencyList AS $currency):?>
                <td style="text-align: right;">
                    <?php
                    if($readonly):
                        $value = $model->getValue($currency->id)->value;
                        $value = (double) $value;
                        ?>

                        <span style="<?= !$value ? 'color: red;' : '';?>"><?= S_Price::formatPrice($value); ?> </span>

                    <?php
                    else:
                        ?>
                        <?php echo CHtml::activeTextField($model->getValue($currency->id), ($formFieldPrefix!==NULL?$formFieldPrefix:'').'['.$currency->id.']'.'value', array('style' => 'width: 50px;', 'data-price-value' => $currency->id, 'data-price-key' => 1,)); ?>
                    <?php
                    endif;
                    ?>
                </td>
            <?php endforeach;?>
        </tr>
    <?php
    endif;
    ?>
</table>
