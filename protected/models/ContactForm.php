<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
    const GC_PUBLIC_KEY = '6Ldbi48UAAAAAO-kkd6CfMRQ6yGtbHT7uXgFwKIr';
    const GC_SECRET_KEY = '6Ldbi48UAAAAAOUjNHnjeWOYYDpSjbkxwv4nA9wM';

    public $name;
    public $email;
    public $body;
    public $verifyCode;
    public $accept;

    protected function verifyGC()
    {
        $data = [
            'secret' => self::GC_SECRET_KEY,
            'response' => $this->verifyCode,
        ];

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,  'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);

        $result = json_decode($result);

        MyDump::dump('gc3.txt', print_r($result,1).' : '.$this->name.' : '.$this->email.' : '.$this->body);

        if($result->success && $result->score > 0.5)
            return true;
        else
            return false;

    }

    public function afterValidate()
    {
        if(!$this->hasErrors() && ($this->verifyCode == '' OR !$this->verifyGC())) {
            $this->addError('body', Yii::t('site', 'Twoja wiadomość nie mogła zostać wysłana'));
            MyDump::dump('gc3.txt', $this->name.' : '.$this->email.' : '.$this->body);
            $this->verifyCode = '';
        }

        parent::afterValidate(); // TODO: Change the autogenerated stub
    }


    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('name, body', 'required'),
            // email has to be a valid email address
            array('email', 'email'),
            // verifyCode needs to be entered correctly
//			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
			array('verifyCode', 'safe'),
            array('accept', 'compare', 'compareValue' => true, 'message' => Yii::t('rodo', 'Potwierdzenie jest wymagane')),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'name'=>Yii::t('m_ContactForm','Imię i nazwisko'),
            'email'=>Yii::t('m_ContactForm','Email'),
//			'subject'=>Yii::t('m_ContactForm','Temat'),
            'body'=>Yii::t('m_ContactForm','Treść'),
//			'verifyCode'=>Yii::t('m_ContactForm','Kod sprawdzający'),
            'accept'=> Yii::t('rodo', 'Zapoznałem się i akcetpuję {tooltip}informację o administratorze i warunki przetwarzaniu danych{/tooltip} ', ['{tooltip}' => '<a rel="tooltip" title="" data-original-title="'.self::getRodoFullText().'" style="text-decoration: underline;">', '{/tooltip}' => '</a>'])
        );
    }

    public static function getRodoFullText()
    {
        return Yii::t('rodo', 'Wyrażam zgodę na przetwarzanie danych osobowych zgodnie z ustawą o ochronie danych osobowych w związku z zapytaniem przez formularz kontaktowy. Podanie danych jest dobrowolne, ale niezbędne do przetworzenia zapytania. Zostałem poinformowany, że przysługuje mi prawo dostępu do swoich danych, możliwości ich przetwarzania i żądania zaprzestania ich przetwarzania. Administratorem danych osobowych jest firma Świat Przesyłek.');

    }
}