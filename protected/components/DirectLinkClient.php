<?php

class DirectLinkClient extends GlobalOperatorWithReturn
{
    const URL = GLOBAL_CONFIG::DIRECTLINK_URL;

    protected static $INSTALLATION_ID = GLOBAL_CONFIG::DIRECTLINK_INSTALLATION_ID;
    protected static $ACTOR_ID = GLOBAL_CONFIG::DIRECTLINK_ACTOR_ID;
    protected static $KEY = GLOBAL_CONFIG::DIRECTLINK_KEY;

    const URL_TT = 'https://customer-api.consignorportal.com/PortalData/PortalData.svc';
    const LOGIN_TT = 'jankopec@swiatprzesylek.pl';
    const PASSWORD_TT = 'Adgyb7ju';

    const CONCEPT_ID_POST_NORD_MYPACK_COLLECT = 607;
    const CONCEPT_ID_POST_NORD_RETURN_DROPOFF = 547;
    const CONCEPT_ID_POST_NORD_MYPACK_HOME = 3564;

    protected $concept_id;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $weight;
    public $value;
    public $content;

    public $cod_value = 0;
    public $cod_currency;

//    public $insurance = false;
    public $smsNotification = false;
    public $changeDeliveryAddress = false;

    public $local_id;

    public $user_id;

    public $delivery_point_id = false;
    public $delivery_point_name = false;
    public $delivery_point_address = false;
    public $delivery_point_zip_code = false;
    public $delivery_point_city = false;
    public $delivery_point_country = false;

    public static function test()
    {
        $model = new self;

        $model->concept_id = self::CONCEPT_ID_POST_NORD_MYPACK_COLLECT;

//        $model->sender_name = 'Sandra Warnerbring';
//        $model->sender_company = '';
//        $model->sender_address1 = 'Herrestadsgatan 2D';
//        $model->sender_city = 'Malmö';
//        $model->sender_zip_code = '00-011';
//        $model->sender_country = 'SE';
//        $model->sender_tel = '0046705388488';
//        $model->sender_mail = '';

        $model->sender_name = 'Maria Öhrling';
        $model->sender_company = '';
        $model->sender_address1 = 'Kungsklippevägen 13';
        $model->sender_city = 'Huddinge';
        $model->sender_zip_code = '14140';
        $model->sender_country = 'SE';
        $model->sender_tel = '00460738908483';
        $model->sender_mail = '';

//        $model->sender_name = 'Staff Operations Fabian Kisch';
//        $model->sender_company = 'Direct Link Worldwide GmbH';
//        $model->sender_address1 = 'Flughafenstraße 9';
//        $model->sender_city = 'Griesheim';
//        $model->sender_zip_code = '64347';
//        $model->sender_country = 'SE';
//        $model->sender_tel = '+49 6155 89790-28';
//        $model->sender_mail = '';
//
        $model->receiver_name = 'eskor.se / Swiat';
        $model->receiver_company = 'c/o PostNord/DirectLink';

        $model->receiver_zip_code = '20229';
        $model->receiver_city = 'Malmo';
        $model->receiver_address1 = 'U-Box 343';
        $model->receiver_address2 = '';
        $model->receiver_country = 'SE';
        $model->receiver_tel = '+46852500510';
        $model->receiver_mail = 'info@eskor.se';


        $model->package_weight = 1;
        $model->content = 'qwertyuiopqwertyuiopq1qwertyuiopqwertyuiopq2qwertyuiopqwertyuiopq3';
        $model->reference = 'Test';



//        $model->saturday_delivery = true;

        $resp = $model->_createShipment(true);

        var_dump($resp);

        file_put_contents('lab.png', $resp[0]->getLabel());
    }

    public static function getAdditions(Courier $courier)
    {

        $uniq_id = false;
        if($courier->courierTypeInternal) {
            $operator = $courier->courierTypeInternal->getDeliveryOperator(true);
            $uniq_id = $operator->uniq_id;
        } else if($courier->courierTypeU)
        {
            $uniq_id = $courier->courierTypeU->courierUOperator->uniq_id;
        }

        $receiverCountryIsSweeden = $courier->receiverAddressData->country_id == CountryList::COUNTRY_SE;

        $CACHE_NAME = 'DIRECT_LINK_ADDITIONS_'.$uniq_id.'_'.$receiverCountryIsSweeden.'_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_INSURANCE;
            if($uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_MYPACK_COLLECT)
            {
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_SMS_NOTIFICATION;

                if(!$receiverCountryIsSweeden)
                    $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_CHANGE_DELIVERY_ADDRESS;
            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function currencyCodeToId($currency)
    {
        $currency = strtoupper($currency);

        $data = ['NOK' => 1,
            'DKK' => 2,
            'SEK' => 3,
            'GBP' => 4,
            'EUR' => 5,
            'USD' => 6,
            'AUD' => 7,
            'HKD' => 8,
            'ISK' => 9,
            'JPY' => 10,
            'CAD' => 11,
            'CYP' => 12,
            'MTL' => 13,
            'SGD' => 14,
            'ZAR' => 15,
            'CHF' => 16,
            'THB' => 17,
            'CZK' => 18,
            'ZWD' => 19,
            'BHD' => 20,
            'AED' => 21,
            'PHP' => 22,
            'INR' => 23,
            'IDR' => 24,
            'ILS' => 25,
            'KES' => 26,
            'CNY' => 27,
            'KRW' => 28,
            'KWD' => 29,
            'MXN' => 30,
            'MAD' => 31,
            'NZD' => 32,
            'PKR' => 33,
            'PLN' => 34,
            'QAR' => 35,
            'RUB' => 36,
            'SAR' => 37,
            'SKK' => 38,
            'LKR' => 39,
            'TWD' => 40,
            'TZS' => 41,
            'TND' => 42,
            'TRY' => 43,
            'HUF' => 44,
            'EEK' => 45,
            'RON' => 46,
            'BGN' => 47,
            'EGP' => 48,
            'HRK' => 49,
            'LTL' => 50,
            'LVL' => 51,
            'SIT' => 52,
            'AFN' => 53,
            'ALL' => 54,
            'AMD' => 55,
            'ANG' => 56,
            'AOA' => 57,
            'ARS' => 58,
            'AWG' => 59,
            'AZN' => 60,
            'BAM' => 61,
            'BBD' => 62,
            'BDT' => 63,
            'BIF' => 64,
            'BMD' => 65,
            'BND' => 66,
            'BOB' => 67,
            'BOV' => 68,
            'BRL' => 69,
            'BSD' => 70,
            'BTN' => 71,
            'BWP' => 72,
            'BYR' => 73,
            'BZD' => 74,
            'CDF' => 75,
            'CLP' => 76,
            'CLF' => 77,
            'COP' => 78,
            'COU' => 79,
            'CRC' => 80,
            'CUP' => 81,
            'CVE' => 82,
            'DJF' => 83,
            'DOP' => 84,
            'DZD' => 85,
            'ERN' => 86,
            'ETB' => 87,
            'FJD' => 88,
            'FKP' => 89,
            'GEL' => 90,
            'GHS' => 91,
            'GIP' => 92,
            'GMD' => 93,
            'GNF' => 94,
            'GTQ' => 95,
            'GWP' => 96,
            'GYD' => 97,
            'HNL' => 98,
            'HTG' => 99,
            'IQD' => 100,
            'IRR' => 101,
            'JMD' => 102,
            'JOD' => 103,
            'KGS' => 104,
            'KHR' => 105,
            'KMF' => 106,
            'KPW' => 107,
            'KYD' => 108,
            'KZT' => 109,
            'LAK' => 110,
            'LBP' => 111,
            'LRD' => 112,
            'LSL' => 113,
            'LYD' => 114,
            'MDL' => 115,
            'MGA' => 116,
            'MKD' => 117,
            'MMK' => 118,
            'MNT' => 119,
            'MOP' => 120,
            'MRO' => 121,
            'MUR' => 122,
            'MVR' => 123,
            'MWK' => 124,
            'MYR' => 125,
            'MZN' => 126,
            'NAD' => 127,
            'NGN' => 128,
            'NIO' => 129,
            'NPR' => 130,
            'OMR' => 131,
            'PAB' => 132,
            'PEN' => 133,
            'PGK' => 134,
            'PYG' => 135,
            'RWF' => 136,
            'SBD' => 137,
            'SCR' => 138,
            'SDG' => 139,
            'SHP' => 140,
            'SLL' => 141,
            'SOS' => 142,
            'SRD' => 143,
            'STD' => 144,
            'SVC' => 145,
            'SYP' => 146,
            'SZL' => 147,
            'TJS' => 148,
            'TMT' => 149,
            'TOP' => 150,
            'TTD' => 151,
            'UAH' => 152,
            'UGX' => 153,
            'UYU' => 154,
            'UYI' => 155,
            'UZS' => 156,
            'VEF' => 157,
            'VND' => 158,
            'VUV' => 159,
            'WST' => 160,
            'YER' => 161,
            'ZMK' => 162,
            'RSD' => 163,
            'XAF' => 164,
            'XOF' => 165,
            'XCD' => 166,
            'XPF' => 167,
        ];

        return (isset($data[$currency])) ? $data[$currency] : false;
    }

    public function _createShipment($returnError = false)
    {

        if(self::$INSTALLATION_ID == '30000143724' && !in_array($this->user_id,[User::INTEGRATIONS_TEST_USER_ID, 2216])) // Only for customer ILEM (#2216)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('This operator is not available for this user!');
            } else {
                return false;
            }
        }

        if(self::$INSTALLATION_ID == '30000143724')
        {
            $this->sender_name = 'IELM / DIRECT LINK';
            $this->sender_company = '';

            $this->sender_zip_code = '20226';
            $this->sender_city = 'Malmo';
            $this->sender_address1 = 'PO-BOX 9196';
            $this->sender_address2 = '';
            $this->sender_country = 'SE';

            $this->sender_tel = '221221218';
            $this->sender_mail = '';
        }

        $receiverTel = $this->receiver_tel;
        if(strtoupper($this->receiver_country) == 'SE') {
            $receiverTel = ltrim($receiverTel, '0');
            if (mb_strlen($receiverTel) == 9)
                $receiverTel = '+46' . $receiverTel;

            if(mb_strlen($receiverTel) < 8)
                $receiverTel = str_pad($receiverTel,8, '0', STR_PAD_LEFT);
        }

        $senderTel = $this->sender_tel;
        if(mb_strlen($senderTel) < 8)
            $senderTel = str_pad($senderTel,8, '0', STR_PAD_LEFT);


        $options = [];
        $options['Labels'] = 'PNG';

        $senderCompany = $receiverCompany = '';
        if($this->sender_name)
        {
            $senderName = $this->sender_name;
            $senderCompany = $this->sender_company;
        } else {
            $senderName = $this->sender_company;
        }

        if($this->receiver_name)
        {
            $receiverName = $this->receiver_name;
            $receiverCompany = $this->receiver_company;
        } else {
            $receiverName = $this->receiver_company;
        }

        $addresses = [];

        $addresses[] =  array(
            'Kind' => 2,
            'Name1' => $senderName,
            'Name2' => $senderCompany,
            'Street1' => $this->sender_address1,
            'Street2' => $this->sender_address2,
            'PostCode' => $this->sender_zip_code,
            'City' => $this->sender_city,
            'CountryCode' => $this->sender_country,
            'Phone' => $senderTel,
            'Email' => $this->sender_mail,
        );

        $addresses[] = array(
            'Kind' => 1,
            'Name1' => $receiverName,
            'Name2' => $receiverCompany,
            'Street1' => $this->receiver_address1,
            'Street2' => $this->receiver_address2,
            'PostCode' => $this->receiver_zip_code,
            'City' => $this->receiver_city,
            'CountryCode' => $this->receiver_country,
            'Phone' => $receiverTel,
            'Email' => $this->receiver_mail,
        );

        if($this->delivery_point_id)
        {

            $addresses[] =  array(
                'Kind' => 10,
                'Name1' => $this->delivery_point_name,
                'CustNo' => $this->delivery_point_id,
                'CountryCode' => $this->delivery_point_country,
                'PostCode' => $this->delivery_point_zip_code,
                'Street1' => $this->delivery_point_address,
                'City' => $this->delivery_point_city,
                'ERPRef' => $this->delivery_point_id,
            );
        }

        $data = array(
            'Kind' => 1,
            'ActorCSID' => self::$ACTOR_ID,
            'ProdConceptID' => $this->concept_id,
            'OrderNo' => $this->content,
            'Addresses' => $addresses,
            'Lines' => array(
                array(
                    'PkgWeight' => $this->weight * 1000,
                    'Number' => 1
                )),

            'References' => [
                [
                    'Kind' => 7,
                    'Value' => $this->content,
                ],
            ],

            'Amounts' => [],
            'Services' => [],
        );

        if($this->concept_id != self::CONCEPT_ID_POST_NORD_MYPACK_HOME) {

            if($this->smsNotification)
                $data['Services'][] = 37008;

            $data['Services'][] = 37021; // email notification - required

            if ($this->changeDeliveryAddress OR (in_array($this->receiver_country_id, [CountryList::COUNTRY_SE, CountryList::COUNTRY_FI]) && $this->concept_id == self::CONCEPT_ID_POST_NORD_MYPACK_COLLECT))
                $data['Services'][] = 37073; // it is required for sweden or as addition

        } else {
            $data['Services'][] = 37093; // flex change - required
        }

        if($this->cod_value)
        {
            $data['Amounts'][] = [
                'Kind' => 5,
                'CurrencyCode' => self::currencyCodeToId($this->cod_currency),
                'Value' => $this->cod_value,
            ];
        }

//        if($this->insurance)
//        {
//            $data['Amounts'][] = [
//                'Kind' => 12,
//                'CurrencyCode' => 5, // EUR
//                'Value' =>  Yii::app()->NBPCurrency->PLN2EUR($this->value),
//            ];
//
//            $data['References'][] = [
//                'Kind' => 30,
//                'Value' => $this->content,
//            ];
//        }

        $resp = $this->_curlCall('SubmitShipment', $data, $options);

        if ($resp->success == true) {
            $return = [];
            $trackingId = (string) $resp->data->Lines[0]->Pkgs[0]->PkgNo;
            $label = (string) $resp->data->Labels[0]->Content;
            $label = base64_decode($label);

            $return[] = GlobalOperatorResponse::createSuccessResponse($label, $trackingId);

            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }
    }

    protected function _curlCall($operation, $data, $options)
    {
        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $Post_data = array();
        $Post_data['actor'] = self::$ACTOR_ID;
        $Post_data['key'] = self::$KEY;
        $Post_data['command'] = $operation;
        $Post_data['data'] = json_encode($data);
        $Post_data['Options'] = json_encode($options);


        $curl = curl_init(self::URL);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $Post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLINFO_HEADER_OUT, TRUE);

        $resp = curl_exec($curl);

        MyDump::dump('directlink.txt', print_r($Post_data,1));
        MyDump::dump('directlink.txt', print_r($resp,1));

        $respOrg = $resp;
        $resp = json_decode($resp);

        if($resp === NULL)
        {
            $error = curl_error($curl);
            if($error == '')
                $error = $respOrg;

            $return->error = $error;
            $return->errorCode = '';
            $return->errorDesc = $error;
        }
        else if($resp->ErrorMessages) {

            if(is_array($resp->ErrorMessages))
                $error = implode('||', $resp->ErrorMessages);
            else
                $error = $resp->ErrorMessages;

            $return->error = $error;
            $return->errorCode = '';
            $return->errorDesc = $error;
        }
        else if($resp->WarningMessages && $resp->Lines[0]->Pkgs[0]->PkgNo == '') {

            if(is_array($resp->WarningMessages))
                $error = implode('||', $resp->WarningMessages);
            else
                $error = $resp->WarningMessages;

            $return->error = $error;
            $return->errorCode = '';
            $return->errorDesc = $error;
        }
        else
        {
            $return->success = true;
            $return->data = $resp;
        }

        return $return;
    }

    protected function setConteptId($operator_uniq_id)
    {
        if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_MYPACK_COLLECT)
            $this->concept_id = self::CONCEPT_ID_POST_NORD_MYPACK_COLLECT;
        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_RETURN_DROPOFF)
            $this->concept_id = self::CONCEPT_ID_POST_NORD_RETURN_DROPOFF;
        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_MYPACK_HOME)
            $this->concept_id = self::CONCEPT_ID_POST_NORD_MYPACK_HOME;

        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_MYPACK_COLLECT_LELM)
            $this->concept_id = self::CONCEPT_ID_POST_NORD_MYPACK_COLLECT;
        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_RETURN_DROPOFF_LELM)
            $this->concept_id = self::CONCEPT_ID_POST_NORD_RETURN_DROPOFF;
        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_MYPACK_HOME_LELM)
            $this->concept_id = self::CONCEPT_ID_POST_NORD_MYPACK_HOME;
        else
            $this->concept_id = false;

        if(in_array($operator_uniq_id,[
            GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_MYPACK_COLLECT_LELM,
            GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_RETURN_DROPOFF_LELM,
            GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_MYPACK_HOME_LELM
        ]))
        {
            self::$INSTALLATION_ID = '30000143724';
            self::$ACTOR_ID = '1660';
            self::$KEY = '9xwYAhJjrU';
        }
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;
        $model->user_id = $courierInternal->courier->user_id;

        $model->setConteptId($uniq_id);

        // SENDER IS ALWAYS ORYGINAL SENDER FOR RETURNS
        if($uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_RETURN_DROPOFF)
            $from = $courierInternal->courier->senderAddressData;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_tel = $from->tel;
        $model->sender_mail = $from->fetchEmailAddress(true);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);
        $model->receiver_country_id = $to->country_id;

        $model->weight = $courierInternal->courier->getWeight(true);
        $model->content = $courierInternal->courier->package_content;
        $model->ref1 = $courierInternal->courier->local_id;
        $model->ref2 = $courierInternal->courier->user_id;
        $model->value = $courierInternal->courier->getPackageValueConverted('EUR');

//        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_INSURANCE))
//            $model->insurance = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_SMS_NOTIFICATION))
            $model->smsNotification = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_CHANGE_DELIVERY_ADDRESS))
            $model->changeDeliveryAddress = true;

        // FOR EOBUWIE, EOBUWIE_ZWROTY
        if($model->concept_id == self::CONCEPT_ID_POST_NORD_RETURN_DROPOFF)
        {
            if(in_array($courierInternal->courier->user_id, [947, 948, 2036, 2037])) {
                $model->receiver_name = 'eskor.se / Swiat';
                $model->receiver_company = 'c/o PostNord/DirectLink';
            }
            $model->receiver_zip_code = '20229';
            $model->receiver_city = 'Malmo';
            $model->receiver_address1 = 'U-Box 343';
            $model->receiver_address2 = '';
            $model->receiver_country = 'SE';
            $model->receiver_country_id = CountryList::COUNTRY_SE;
            if(in_array($courierInternal->courier->user_id, [947, 948, 2036, 2037])) {
                $model->receiver_tel = '+46852500510';
                $model->receiver_mail = 'info@eskor.se';
            }
        }
        else if($model->concept_id == self::CONCEPT_ID_POST_NORD_MYPACK_HOME)
        {
            if(in_array($courierInternal->courier->user_id, [947, 948, 2036, 2037])) {
                $model->sender_name = 'eskor.se / Swiat';
                $model->sender_company = 'c/o PostNord/DirectLink';
            }
            $model->sender_zip_code = '20229';
            $model->sender_city = 'Malmo';
            $model->sender_address1 = 'U-Box 343';
            $model->sender_address2 = '';
            $model->sender_country = 'SE';
            if(in_array($courierInternal->courier->user_id, [947, 948, 2036, 2037])) {
                $model->sender_tel = '+46852500510';
                $model->sender_mail = 'info@eskor.se';
            }
        }

        if($courierInternal->courier->_points_data) {
            $pointDetails = DeliveryPoints::getPointById($courierInternal->courier->_points_data[0], true, false, DeliveryPoints::TYPE_DIRECTLINK);

            if (!$pointDetails)
                if ($returnErrors == true) {
                    $return = [0 => array(
                        'error' => 'Point not found!',
                    )];
                    return $return;
                } else {
                    return false;
                }

            $model->delivery_point_id = $pointDetails[0];
            $model->delivery_point_name = $pointDetails[1];
            $model->delivery_point_address = $pointDetails[2];
            $model->delivery_point_zip_code = $pointDetails[3];
            $model->delivery_point_city = $pointDetails[4];
            $model->delivery_point_country = $pointDetails[5];
        }

        return $model->_createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;
        $model->user_id = $courierU->courier->user_id;


        $model->setConteptId($courierU->courierUOperator->uniq_id);

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_tel = $from->tel;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(false);
        $model->receiver_country_id = $to->country_id;

        $model->weight = $courierU->courier->getWeight(true);
        $model->content = $courierU->courier->package_content;
        $model->ref1 = $courierU->courier->local_id;
        $model->ref2 = $courierU->courier->user_id;
        $model->value = $courierU->courier->getPackageValueConverted('EUR');

//        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_INSURANCE))
//            $model->insurance = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_SMS_NOTIFICATION))
            $model->smsNotification = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_CHANGE_DELIVERY_ADDRESS))
            $model->changeDeliveryAddress = true;

        if($model->concept_id == self::CONCEPT_ID_POST_NORD_RETURN_DROPOFF)
        {

            if(in_array($courierU->courier->user_id, [947, 948, 2036, 2037])) {
                $model->receiver_name = 'eskor.se / Swiat';
                $model->receiver_company = 'c/o PostNord/DirectLink';
            }
            $model->receiver_zip_code = '20229';
            $model->receiver_city = 'Malmo';
            $model->receiver_address1 = 'U-Box 343';
            $model->receiver_address2 = '';
            $model->receiver_country = 'SE';
            $model->receiver_country_id = CountryList::COUNTRY_SE;
            if(in_array($courierU->courier->user_id, [947, 948, 2036, 2037])) {
                $model->receiver_tel = '+46852500510';
                $model->receiver_mail = 'info@eskor.se';
            }
        }
        else if($model->concept_id == self::CONCEPT_ID_POST_NORD_MYPACK_HOME)
        {
            if(in_array($courierU->courier->user_id, [947, 948, 2036, 2037])) {
                $model->sender_name = 'eskor.se / Swiat';
                $model->sender_company = 'c/o PostNord/DirectLink';
            }
            $model->sender_zip_code = '20229';
            $model->sender_city = 'Malmo';
            $model->sender_address1 = 'U-Box 343';
            $model->sender_address2 = '';
            $model->sender_country = 'SE';
            if(in_array($courierU->courier->user_id, [947, 948, 2036, 2037])) {
                $model->sender_tel = '+46852500510';
                $model->sender_mail = 'info@eskor.se';
            }
        }

        if($courierU->courier->_points_data) {
            $pointDetails = DeliveryPoints::getPointById($courierU->courier->_points_data[0], true, false, DeliveryPoints::TYPE_DIRECTLINK);

            if (!$pointDetails)
                if ($returnErrors == true) {
                    $return = [0 => array(
                        'error' => 'Point not found!',
                    )];
                    return $return;
                } else {
                    return false;
                }

            $model->delivery_point_id = $pointDetails[0];
            $model->delivery_point_name = $pointDetails[1];
            $model->delivery_point_address = $pointDetails[2];
            $model->delivery_point_zip_code = $pointDetails[3];
            $model->delivery_point_city = $pointDetails[4];
            $model->delivery_point_country = $pointDetails[5];
        }

        return $model->_createShipment($returnErrors);
    }

    public static function orderForCourierReturn(CourierTypeReturn $courierReturn, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;
        $model->user_id = $courierReturn->courier->user_id;

        $operator_uniq_id = GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_RETURN_DROPOFF;

        if($courierReturn->courier->user_id == 2216) // @ 31.20.2018 - special rule for user ILEM (#2216)
            $operator_uniq_id = GLOBAL_BROKERS::OPERATOR_UNIQID_DIRECT_LINK_POSTNORD_RETURN_DROPOFF_LELM;

        $model->setConteptId($operator_uniq_id);

        $from = $courierReturn->courier->senderAddressData;

        $to = $courierReturn->courier->receiverAddressData;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_tel = $from->tel;
        $model->sender_mail = $from->fetchEmailAddress(true);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);
        $model->receiver_country_id = $to->country_id;

        $model->weight = $courierReturn->courier->getWeight(true);
        $model->content = $courierReturn->courier->package_content;
        $model->ref1 = $courierReturn->courier->local_id;
        $model->ref2 = $courierReturn->courier->user_id;
        $model->value = $courierReturn->courier->getPackageValueConverted('EUR');

        // FOR EOBUWIE_DE, EOBUWIE_NON_DE
        if(in_array($courierReturn->courier->user_id, [2036, 2037])) {
            $model->receiver_name = 'eskor.se / Swiat';
            $model->receiver_company = 'c/o PostNord/DirectLink';
        }
        $model->receiver_zip_code = '20229';
        $model->receiver_city = 'Malmo';
        $model->receiver_address1 = 'U-Box 343';
        $model->receiver_address2 = '';
        $model->receiver_country = 'SE';
        $model->receiver_country_id = CountryList::COUNTRY_SE;
        if(in_array($courierReturn->courier->user_id, [2036, 2037])) {
            $model->receiver_tel = '+46852500510';
            $model->receiver_mail = 'info@eskor.se';
        }

        if($courierReturn->courier->user == 2216 && $courierReturn->courier->senderAddressData->country_id == CountryList::COUNTRY_DK) // User IELM
        {
            $model->receiver_name = 'Filip and Family';
            $model->receiver_company = 'IELM / DIRECT LINK';

            $model->receiver_zip_code = '20226';
            $model->receiver_city = 'Malmo';
            $model->receiver_address1 = 'PO-BOX 9196';
            $model->receiver_address2 = '';
            $model->receiver_country = 'SE';
            $model->receiver_country_id = CountryList::COUNTRY_SE;
        }


        return $model->_createShipment($returnErrors);
    }



    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        if($courierLabelNew->courier->user_id == 2216) // For user IELM use Track17
            return Client17Track::getTtGlobalBrokers($no, false, false, 19241, 'DirectLink'); // 19241 = Sweden Posten

        $data = new stdClass();
        $data->packageNumber = $no;

        if($courierLabelNew->courier->date_entered <= '2018-08-10 23:59:59')
        {
            $data->userName = 'jankopec2@swiatprzesylek.pl'; // kaab account
            $data->password = 'sGtV35ncaq';
            $data->installationValue = '20002089223';
        } else {
            $data->userName = self::LOGIN_TT;
            $data->password = self::PASSWORD_TT;
            $data->installationValue = self::$INSTALLATION_ID;
        }

        $method = 'GetShipmentsByPackageNumber';

        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
        );

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $client = new SoapClient(self::URL_TT . '?wsdl', $mode);

        $statuses = new GlobalOperatorTtResponseCollection();

        try {
            $resp = $client->__call($method, [$method => $data]);
        }
        catch (Exception $E)
        {
            return false;
        }

        $result = $resp->GetShipmentsByPackageNumberResult->Shipment->Events->Event;

        if(!$result)
        {
            if(round((time() - strtotime($courierLabelNew->courier->date_entered)) / (60 * 60 * 24)) >= 7)
                return Client17Track::getTtGlobalBrokers($no, false, false, false, 'DirectLink2');
//                return TrackingmoreClient::getTtGlobalBrokers($no, 'sweden-posten');
            else
                return false;
        }

        if(!$result)
            $result = [ $result ];


        foreach($result AS $item)
        {
            $date = substr($item->Date,0, 19);
            $date = str_replace('T', ' ', $date);

            $statuses->addItem($item->StatusText, $date, NULL);
        }

        $result2 = $resp->GetShipmentsByPackageNumberResult->Shipment->Lines->Line->Packages->Package->Events->Event;


        if(!$result2)
            $result2 = [ $result2 ];

        foreach($result2  AS $item2)
        {
            $date = substr($item2->ServerDate,0, 19);
            $date = str_replace('T', ' ', $date);

            if($item2->StatusText == 'SMS skickat till mottagaren')
                continue;

            $text = trim($item2->StatusState.' : '.$item2->StatusText);

            if($text == ':')
                continue;

            $statuses->addItem($text, $date, $item2->CityName);
        }

        return $statuses;
    }



    /**
     * @param $address
     * @return DeliveryPoints[]
     * @throws CDbException
     * @throws CException
     */
    public static function getPoints($zip_code, $countryCode)
    {
        $countryCode = strtoupper($countryCode);

        $CACHE_NAME = 'DIRECTLINK_POINTS_' . $zip_code . '_' . $countryCode;

        $data = Yii::app()->cache->get($CACHE_NAME);
//        $data = false;
        if ($data === false) {
            $data = [];


            $Soap_client = new SoapClient('http://www.consignorsupport.no/ShipAdvisor/Main.asmx?WSDL');
            $Soap_header = new SoapHeader('SoapAuthenticator', 'ServiceAuthenticationHeader',
                array('Username' => 'Flexerman21073', 'Password' => 'Gm15/46exRF'));

            $Soap_client->__setSoapHeaders($Soap_header);

            $Params = array(
                'productConceptID' => self::CONCEPT_ID_POST_NORD_MYPACK_COLLECT,
                'installationID' => self::$INSTALLATION_ID,
                'country' => $countryCode,
                'postCode' => $zip_code,
                'limit' => 100
            );

            $dropPoints = $Soap_client->SearchForDropPoints($Params);

            if($dropPoints->SearchForDropPointsResult->DropPointData)
                foreach ($dropPoints->SearchForDropPointsResult->DropPointData AS $item) {

                    if($item->City) {
                        $dp = DeliveryPoints::getUpdateDeliveryPoints(DeliveryPoints::TYPE_DIRECTLINK, $item->OriginalId, $item->Address1, $item->PostalCode, $item->City, strtoupper($item->CountryCode), $item->Name1, $item->MapRefY, $item->MapRefX, $item->shopType, NULL);
                        $data[] = $dp;
                    }
                }

            Yii::app()->cache->set($CACHE_NAME, $data, 60*60*24);
        }
        return $data;

    }


}

