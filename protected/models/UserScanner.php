<?php

class UserScanner extends CModel
{
    public $items_ids;
    public $items_ids_backup;

    public function attributeNames()
    {
        return [];
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(

            array('items_ids', 'required'),
        );
    }


    /**
     * Function return model or array of models of found packages
     *
     * @return Postal[]|NULL|boolean
     */
    public function returnPostalModels($adminMode = false)
    {
        if(!is_array($this->items_ids))
            return [];

        $models = [];

        $this->items_ids = array_unique($this->items_ids);
        $this->items_ids_backup = $this->items_ids;

        /* @var $model Postal */
        foreach($this->items_ids AS $key => $item_id)
        {
            $item_id = trim($item_id);

            if($item_id == '')
                continue;

            $model = Postal::model()->findByAttributes(['local_id' => $item_id]);

            // maybe it's external id?
            if($model === NULL) {
                $model = PostalExternalManager::findPostalIdsByRemoteId($item_id);

                // method above returns array, but for simplicity get just first element - should work in most of examplest
                if(S_Useful::sizeof($model))
                    $model = array_shift($model);
            }

            // maybe it's bulk_id
//            if($model === NULL OR !S_Useful::sizeof($model))
//            {
//                $modelsBulk = Postal::model()->findAllByAttributes(['bulk_id' => $item_id]);
//                if(S_Useful::sizeof($modelsBulk)) {
//                    foreach ($modelsBulk AS $bulkItem)
//                        $models[$bulkItem->local_id] = $bulkItem;
//
//                    unset($models[$key]);
//                    continue;
//                }
//            }

            $models[$key] = $model;


            if(!$adminMode && $model->user_id != Yii::app()->user->id)
                unset($models[$key]);
        }

        return $models;
    }


//    public function returnCourierModels($adminMode = false, $justFirstOfMany = true)
//    {
//        if(!is_array($this->items_ids))
//            return [];
//
//        $models = [];
//
//        $this->items_ids = array_unique($this->items_ids);
//        $this->items_ids_backup = $this->items_ids;
//
//        foreach($this->items_ids AS $key => $item_id)
//        {
//            $item_id = trim($item_id);
//
//            if($item_id == '')
//                continue;
//
//            $model = Courier::model()->findByAttributes(['local_id' => $item_id]);
//
//            if($model === NULL) {
//                $model = CourierExternalManager::findCourierIdsByRemoteId($item_id, true);
//
//                // method above returns array, but for simplicity get just first element - should work in most of examples
//                if(S_Useful::sizeof($model))
//                    $model = array_shift($model);
//            }
//
//
//            if($model)
//                $models[$key] = $model;
//
//            if(!$adminMode && $model->user_id != Yii::app()->user->id)
//                unset($models[$key]);
//        }
//
//
//
//        return $models;
//    }

    public function returnCourierModels($adminMode = false, $justFirstOfMany = true)
    {
        if(!is_array($this->items_ids))
            return [];

        $models = [];

        $this->items_ids = array_unique($this->items_ids);
        $this->items_ids_backup = $this->items_ids;

        foreach($this->items_ids AS $key => $item_id)
        {
            $item_id = trim($item_id);

            if($item_id == '')
                continue;

            $model = Courier::model()->findByAttributes(['local_id' => $item_id]);

            if($model === NULL) {
                $model = CourierExternalManager::findCourierIdsByRemoteId($item_id, true);

                // method above returns array, but for simplicity get just first element - should work in most of examples
                if(1 OR $justFirstOfMany && S_Useful::sizeof($model))
                    $model = array_shift($model);
            }

            if(!$adminMode && $model->user_id != Yii::app()->user->id)
                $model = NULL;

            if($model)
            {
                $i = 0;
                if(is_array($model))
                {
                    foreach($model AS $item)
                    {
                        if(!$i)
                            $models[$key] = $item;
                        else
                            $models[$key.'_'.$i] = $item;
                        $i++;
                    }
                }
                else
                    $models[$key] = $model;
            }
        }

        return $models;
    }

    public function prefilterIds()
    {
        if(!is_array($this->items_ids))
            return false;

        foreach($this->items_ids AS $key => $item)
        {
            $item = explode(',', $item);
            $item = end($item);

            if($item != '')
            {
               $this->items_ids[$key] = $item;
            }
        }
    }


}