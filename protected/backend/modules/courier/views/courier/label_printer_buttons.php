<table style="margin: 0 auto; width: 100%;">
    <tr>
        <?php
        if($model):
        echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, 'Package #' . $model->local_id, array('closeText' => false));
        ?>

        <?php
        if($model->courierTypeInternal):
        ?>
        <td colspan="4" style="text-align: center;">First</td>
        <td><?php echo TbHtml::link('Generate Both', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_BOTH), array('class' => 'btn'));?></td>
        <td colspan="4" style="text-align: center;">Last</td>
    </tr>
    <tr>
        <td><?php echo TbHtml::link('4 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_FIRST, 'type' => Courier::PDF_4_ON_1), array('class' => 'btn'));?></td>
        <td><?php echo TbHtml::link('2 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_FIRST, 'type' => Courier::PDF_2_ON_1), array('class' => 'btn'));?></td>
        <td><?php echo TbHtml::link('2 on 1 (1R)', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_FIRST, 'type' => Courier::PDF_2_ON_1_ONE_RIGHT), array('class' => 'btn'));?></td>
        <td><?php echo TbHtml::link('Roll', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_FIRST, 'type' => Courier::PDF_ROLL), array('class' => 'btn'));?></td>
        <td></td>
        <td><?php echo TbHtml::link('4 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_4_ON_1), array('class' => 'btn'));?></td>
        <td><?php echo TbHtml::link('2 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_2_ON_1), array('class' => 'btn'));?></td>
        <td><?php echo TbHtml::link('2 on 1 (1R)', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_2_ON_1_ONE_RIGHT), array('class' => 'btn'));?></td>
        <td><?php echo TbHtml::link('Roll', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'mode' => CourierTypeInternal::GET_LABEL_MODE_LAST, 'type' => Courier::PDF_ROLL), array('class' => 'btn'));?></td>
        <?php else: ?>
            <td><?php echo TbHtml::link('4 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'type' => Courier::PDF_4_ON_1), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('2 on 1', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'type' => Courier::PDF_2_ON_1), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('2 on 1 (1R)', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'type' => Courier::PDF_2_ON_1_ONE_RIGHT), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('Roll', array('/courier/courier/CarriageTicket', 'id' => $model->id, 'type' => Courier::PDF_ROLL), array('class' => 'btn'));?></td>
        <?php endif; ?>
        <?php
        else:
            echo TbHtml::alert(TbHtml::ALERT_COLOR_ERROR, 'Package not found!', array('closeText' => false));
        endif;
        ?>
    </tr>
</table>


