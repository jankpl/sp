var activeClass = "langVersionMenuActive";

$(document).ready(function(){


    var $langVersionMenu = $("#langVersionMenuContainer");
    var $langVersionMenuItem = $(".langVersionMenu");

    hideMenuIfOnlyOneLangauge($langVersionMenu);
    loadFirstLanguage($langVersionMenu);

    $langVersionMenuItem.click(langVersionMenu);
	
});

function hideMenuIfOnlyOneLangauge($langVersionMenu)
{

    if($langVersionMenu.children().length < 2)
        $langVersionMenu.hide();

}

function loadFirstLanguage($langVersionMenu)
{
    var langId = extractId($langVersionMenu.children(0).attr('id'));

    langVersionMenu(langId);

}

function langVersionMenu(id)
{


    id = parseInt(id);

    if(isNaN(id))
    {
        if($(this).hasClass(activeClass)) return;

        id = extractId($(this).attr('id'));
    }

    $(".langVersionMenu").removeClass(activeClass);
    $(".langVersion").hide();
    $("#langVersionMenu_" + id).addClass(activeClass);
    $("#langVersion_" + id).fadeIn();

}


function extractId(string)
{
    var langId = string;
    langId = langId.split('_');

    var length = langId.length;

    return langId[length - 1];


}



