<h1>Update <?php echo GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?> (# <?= $model->id;?>)</h1>


<fieldset>
    <legend>Custom settings for country group</legend>
    <?= CHtml::link('Custom settings for country group', ['/courier/courierUOperator/manageCountries', 'operator_id' => $model->id], ['class' => 'btn btn-lg btn-primary']); ?>
</fieldset>

<fieldset>
    <legend>Custom settings for user group</legend>
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'manage-countrier-user-group',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('/courier/courierUOperator/manageCountriesUserGroup', ['operator_id' => $model->id]),
    ));
    ?>
    <?php echo CHtml::dropDownList('user_group_id','', CHtml::listData(UserGroup::getListForThisAdmin(),'id', 'nameWithAdmin')); ?>
    <br/>
    <?php
    echo CHtml::submitButton('Custom settings for user group', ['class' => 'btn btn-lg btn-primary']);
    ?>
    <?php
    $this->endWidget();
    ?>
</fieldset>


<fieldset>
    <legend>Base operator settings</legend>

    <?php
    if($userGroup === NULL && !AdminRestrictions::isUltraAdmin()):
        ?>
        <div class="alert alert-warning text-center">You have no authority to modify main routing.</div>
    <?php
    else:
    ?>
    <?php
    $this->renderPartial('_form', array(
        'model' => $model,
        'modelTrs' => $modelTrs,

    ));
    ?>
</fieldset>

<?php
endif;
