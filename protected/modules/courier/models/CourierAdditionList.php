<?php

Yii::import('application.modules.courier.models._base.BaseCourierAdditionList');

class CourierAdditionList extends BaseCourierAdditionList
{

    //ids from DB
    const OVERSIZED_PACKAGE = 1;

    const INPOST_EXPRESS_17 = 3;
    const INPOST_EXPRESS_12 = 4;

    const INPOST_INSURANCE_5000 = 5;
    const INPOST_INSURANCE_10000 = 6;
    const INPOST_INSURANCE_20000 = 7;
    const INPOST_INSURANCE_1000 = 115;

    const INPOST_ROD = 8;


    const SAFEPAK_ROD = 9;

    const SAFEPAK_INSURANCE_5000 = 10;
    const SAFEPAK_INSURANCE_10000 = 11;
    const SAFEPAK_INSURANCE_20000 = 12;

    const GLS_ROD = 15;
    const GLS_DELIVERY_TO_10 = 16;
    const GLS_DELIVERY_TO_12 = 17;
    const GLS_DELIVERY_SATURDAY = 18;

    const DHL_DOR_1822 = 21;
    const DHL_DOR_SB = 22;
    const DHL_NAD_SB = 23;
    const DHL_ROD = 24;
    const DHL_UBEZ = 27;

    const DHL_12 = 25;
    const DHL_09 = 26;

    const PP_POTW_ODBIORU = 29;
    const PP_PRIO = 30;
    const PP_OKRESLONA_WARTOSC = 31;

    const PACKAGE_NON_STANDARD_DOMESTIC = 1;
    const PACKAGE_NON_STANDARD_INT = 29;

    ///////////////////////////////

    const UNIQID_UPS_ROD = 10;
    const UNIQID_UPS_SATURDAY_DELIVERY = 11;
    const UNIQID_UPS_NOTIFICATION = 12;

    const UNIQID_DHLDE_PAEKET_PRIO = 40;
    const UNIQID_DHLDE_INSURANCE = 41;
    const UNIQID_DHLDE_NOTIFICATION = 42;

    const UNIQID_DHLEXPRESS_UE_12 = 100;
    const UNIQID_DHLEXPRESS_UE_9 = 101;
    const UNIQID_DHLEXPRESS_NONUE_12 = 102;
    const UNIQID_DHLEXPRESS_NONUE_9 = 103;
    const UNIQID_DHLEXPRESS_USA_1030 = 104;
    const UNIQID_DHLEXPRESS_BREAKBULK = 105;
    const UNIQID_DHLEXPRESS_XPD = 106;
    const UNIQID_DHLEXPRESS_UE_ECONOMY = 107;
    const UNIQID_DHLEXPRESS_NONUE_ECONOMY = 108;
    const UNIQID_DHLEXPRESS_ID8000 = 109;

    const UNIQID_UKMAIL_AIR = 110;
    const UNIQID_UKMAIL_SPECIFIED_ADDRESS_ONLY = 111;
    const UNIQID_UKMAIL_LEAVE_SAFE_OR_SIG_NEIGH = 112;
    const UNIQID_UKMAIL_NEXT_DAY_9 = 113;
    const UNIQID_UKMAIL_NEXT_DAY_1030 = 114;
    const UNIQID_UKMAIL_NEXT_DAY_12 = 115;
    const UNIQID_UKMAIL_SAT = 116;
    const UNIQID_UKMAIL_SAT_9 = 117;
    const UNIQID_UKMAIL_SAT_1030 = 118;
    const UNIQID_UKMAIL_48_HOUR = 119;
    const UNIQID_UKMAIL_48_HOUR_PLUS = 120;

    const UNIQID_ROYAL_MAIL_RECORDED = 142;
    const UNIQID_ROYAL_MAIL_SIGNATURE = 143;

    const UNIQID_DPDPL_GUARANTEED_930 = 1100;
    const UNIQID_DPDPL_GUARANTEED_1200 = 1101;
    const UNIQID_DPDPL_GUARANTEED_SAT = 1102;
    const UNIQID_DPDPL_GUARANTEED_SUN = 1103;
    const UNIQID_DPDPL_CARRYIN = 1104;
    const UNIQID_DPDPL_ROD = 1105;
    const UNIQID_DPDPL_DECLARED_VALUE = 1106;

    const UNIQID_DPDNL_TYRES = 1200;
    const UNIQID_DPDNL_SATURDAY = 1201;
    const UNIQID_DPDNL_E10 = 1202;
    const UNIQID_DPDNL_E12 = 1203;
    const UNIQID_DPDNL_G18 = 1204;
    const UNIQID_DPDNL_INTERNATIONAL_EXPRESS = 1205;

    const UNIQID_LPEXPRESS_SATURDAY_DELIVERY = 1700;
    const UNIQID_LPEXPRESS_HAS_LOAD = 1701;
    const UNIQID_LPEXPRESS_DELIVERY_NOTIFICATION = 1702;
    const UNIQID_LPEXPRESS_INSURANCE = 1703;
    const UNIQID_LPEXPRESS_SAME_DAY_DELIVERY = 1704;
    const UNIQID_LPEXPRESS_HAS_COVER_LETTER = 1705;

    //    const DHL_CONNECT = 27;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),

            array('price_id', 'numerical', 'integerOnly'=>true),

            array('name, price', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>45),
            array('date_updated', 'safe'),
            array('date_updated, stat, uniq_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, description, price, stat, broker_id, group', 'safe', 'on'=>'search'),
        );
    }

    public static function getIdByUniqId($uniq_id)
    {
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        return $cmd->select('id')->from('courier_addition_list')->where('uniq_id = :uniq_id', [':uniq_id' => $uniq_id])->queryScalar();
    }

    public function getClientTitle()
    {
        return $this->courierAdditionListTr->title;
    }

    public function getClientDesc()
    {
        return $this->courierAdditionListTr->description;
    }

    public function toggleStat()
    {
        $this->stat = abs($this->stat - 1);

        return $this->save(false,array('stat'));
    }

    public function saveWithTrAndPrice($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;


        $price_id = $this->price->saveWithPrices();

        if(!$price_id)
            $errors = true;

        $this->price_id = $price_id;

        if(!$this->save())
            $errors = true;

        foreach($this->courierAdditionListTrs AS $item)
        {

            $item->courier_addition_list_id = $this->id;
            if(!$item->save()) {
                $errors = true;
            }
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

    public function getPrice()
    {
        $currency = Yii::app()->PriceManager->getCurrency();

        $CACHE_NAME = 'COURIER_ADDITION_PRICE_'.$currency.'_'.$this->id;
        $price = Yii::app()->cache->get($CACHE_NAME);

        if($price === false) {

            $price = $this->price->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => $currency)))[0]->value;
            Yii::app()->cache->set($CACHE_NAME, $price, 60);
        }

        return $price;
    }


    public static function getAdditionsByCourier(Courier $courier)
    {
        $excludeIds = [];
        if($courier->senderAddressData->country_id == CountryList::COUNTRY_PL && $courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
        {
            $excludeIds[] = self::PACKAGE_NON_STANDARD_INT;

        } else if($courier->senderAddressData->country_id != CountryList::COUNTRY_PL AND $courier->receiverAddressData->country_id != CountryList::COUNTRY_PL) {
            $excludeIds[] = self::PACKAGE_NON_STANDARD_DOMESTIC;
        }

        $excludeRule = '';

        if(S_Useful::sizeof($excludeIds)) {
            $excludeIds = implode(',', $excludeIds);
            $excludeRule = ' AND id NOT IN ('.$excludeIds.')';
        }

        // find universal additions
        $models = self::model()->findAll('stat = '.S_Status::ACTIVE.' AND broker_id IS NULL'.$excludeRule);

        // find additions for specific operator
        // only if pickup operator == delivery operator or without pickup option
        // PICKUP_MOD / 14.09.2016
//        if(!$courier->courierTypeInternal->with_pickup !-= OR ($courier->courierTypeInternal->getPickupOperator() == $courier->courierTypeInternal->getDeliveryOperator()
        if(
            in_array($courier->courierTypeInternal->with_pickup, [CourierTypeInternal::WITH_PICKUP_REGULAR, CourierTypeInternal::WITH_PICKUP_RUCH, CourierTypeInternal::WITH_PICKUP_RM]) AND ($courier->courierTypeInternal->getPickupOperator() == $courier->courierTypeInternal->getDeliveryOperator())
            OR
            (in_array($courier->courierTypeInternal->with_pickup, [CourierTypeInternal::WITH_PICKUP_NONE, CourierTypeInternal::WITH_PICKUP_CONTRACT]))

//                AND $courier->courierTypeInternal->getPickupHub() == $courier->courierTypeInternal->getDeliveryHub()

        )
        {
            $temp = self::findOperatorAdditions($courier->courierTypeInternal->getDeliveryOperator(), $courier);

            if(is_array($temp))
                $models = array_merge($models, $temp);
        }

        return $models;
    }

    protected static function findOperatorAdditions($operator, Courier $courier)
    {

//        switch(CourierOperator::model()->cache(60*60)->findByPk($operator)->ooe)
        $broker_id = CourierOperator::model()->findByPk($operator)->broker;

        if(in_array($broker_id, array_keys(GLOBAL_BROKERS::getBrokersList())))
        {
            $operatorClient = GLOBAL_BROKERS::globalOperatorClientFactory($broker_id);
            return $operatorClient::getAdditions($courier);
        }
        else
            switch($broker_id)
            {
                case CourierOperator::BROKER_INPOST:
                    return self::_findInpostAdditions($courier);
                    break;
//            case CourierOperator::BROKER_SAFEPAK:
//                return self::_findSafepakAdditions($courier);
//                break;
                case CourierOperator::BROKER_GLS:
                    return self::_findGlsAdditions($courier);
                    break;
                case CourierOperator::BROKER_DHL:
                    return self::_findDhlAdditions($courier);
                    break;
                case CourierOperator::BROKER_POCZTA_POLSKA:
                    return self::_findPocztaPolskaAdditions($courier);
                    break;
                case CourierOperator::BROKER_DHLDE:
                    return self::_findDhlDeAdditions($courier);
                    break;
                case CourierOperator::BROKER_UK_MAIL:
                    return self::_findUkMailAdditions($courier);
                    break;
                case CourierOperator::BROKER_DHLEXPRESS:
                    return self::_findDhlExpressAdditions($courier);
                    break;
                case CourierOperator::BROKER_UPS:
                    return self::_findUpsAdditions($courier);
                    break;
                case CourierOperator::BROKER_DPDPL:
                    return self::_findDpdPlAdditions($courier);
                    break;
                case CourierOperator::BROKER_ROYAL_MAIL:
                    return self::_findRoyalMAilAdditions($courier);
                    break;
                case CourierOperator::BROKER_DPDNL:
                    return self::_findDpdNlAdditions($courier);
                    break;
                case CourierOperator::BROKER_LP_EXPRESS:
                    return self::_findLpExpressAdditions($courier);
                    break;
//                case GLOBAL_BROKERS::BROKER_SWEDEN_POST:
//                    return self::_findSwedenPostAdditions($courier);
//                    break;
                case CourierOperator::BROKER_DPD_DE:
                    return self::_findDpdDeAdditions($courier);
                    break;
//                case GLOBAL_BROKERS::BROKER_DPDIR:
//                    return self::_findDpdIrAdditions($courier);
//                    break;
//                case GLOBAL_BROKERS::BROKER_ANPOST:
//                    return self::_findAnpostAdditions($courier);
//                    break;
//                case GLOBAL_BROKERS::BROKER_WHISTL:
//                    return self::_findWhistlAdditions($courier);
//                    break;
                default:
                    return false;
                    break;
            }
    }

    protected static function _findInpostAdditions(Courier $courier)
    {
        $additions = [];

        $services = Inpost::getServicesForCourierTypeInternal($courier->courierTypeInternal, $courier->receiverAddressData, $courier->senderAddressData);

        if(isset($services[Inpost::SERVICE_EX_17]))
            $additions[] = self::INPOST_EXPRESS_17;

        if(isset($services[Inpost::SERVICE_EX_12]))
            $additions[] = self::INPOST_EXPRESS_12;

        if($courier->cod_value < 5000) {
            $additions[] = self::INPOST_INSURANCE_1000;
            $additions[] = self::INPOST_INSURANCE_5000;
        }

        $additions[] = self::INPOST_INSURANCE_10000;

        $additions[] = self::INPOST_INSURANCE_20000;

        $additions[] = self::INPOST_ROD;


        $array = implode(',', $additions);

        if(!S_Useful::sizeof($additions))
            $models = [];
        else
            $models = self::model()->findAll('stat = '.S_Status::ACTIVE.' AND id IN ('.$array.')');

        return $models;
    }

//    protected static function _findSafepakAdditions(Courier $courier)
//    {
//        $additions = [];
//
//        $additions[] = self::SAFEPAK_ROD;
//
//        if($courier->courierTypeInternal->client_cod_value < 5000 && $courier->package_value < 5000)
//            $additions[] = self::SAFEPAK_INSURANCE_5000;
//
//        if($courier->courierTypeInternal->client_cod_value < 10000 && $courier->package_value < 10000)
//            $additions[] = self::SAFEPAK_INSURANCE_10000;
//
//        if($courier->courierTypeInternal->client_cod_value < 20000 && $courier->package_value < 20000)
//            $additions[] = self::SAFEPAK_INSURANCE_20000;
//
//        $array = implode(',', $additions);
//
//        if(!S_Useful::sizeof($additions))
//            $models = [];
//        else
//            $models = self::model()->findAll('stat = '.S_Status::ACTIVE.' AND id IN ('.$array.')');
//
//        return $models;
//    }

    protected static function _findGlsAdditions(Courier $courier)
    {
        $zip_code = $courier->receiverAddressData->zip_code;

        $CACHE_NAME = 'GLS_ADDITIONS_INTERNAL_' . $zip_code;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {
            $additions = [];

            $services = GlsClient::getPremiumServices($zip_code);

            if ($services) {

                if ($services->{(GlsClient::OPTION_ROD)})
                    $additions[] = self::GLS_ROD;

                if ($services->{(GlsClient::OPTION_DEVLIVERY_TO_12)})
                    $additions[] = self::GLS_DELIVERY_TO_12;

                if ($services->{(GlsClient::OPTION_DEVLIVERY_TO_10)})
                    $additions[] = self::GLS_DELIVERY_TO_10;

                if ($services->{(GlsClient::OPTION_SATURDAY)})
                    $additions[] = self::GLS_DELIVERY_SATURDAY;

            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDhlAdditions(Courier $courier)
    {
        $CACHE_NAME = 'DHL_ADDITIONS_INTERNAL_'.$courier->senderAddressData->zip_code.'_'.$courier->receiverAddressData->zip_code;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $servicesSender = DhlClient::getServicesForZipCode($courier->senderAddressData->zip_code, date('Y-m-d'));
            $servicesReceiver = DhlClient::getServicesForZipCode($courier->receiverAddressData->zip_code, date('Y-m-d'));

            $additions = [];

            $additions[] = self::DHL_ROD;
            $additions[] = self::DHL_UBEZ;

            if($servicesReceiver[DhlClient::OPT_DOR_SB])
                $additions[] = self::DHL_DOR_SB;

            if($servicesReceiver[DhlClient::OPT_DOR_1822])
                $additions[] = self::DHL_DOR_1822;

            if($servicesSender[DhlClient::OPT_DOM09] && $servicesReceiver[DhlClient::OPT_DOM09])
                $additions[] = self::DHL_09;

            if($servicesSender[DhlClient::OPT_DOM12] && $servicesReceiver[DhlClient::OPT_DOM12])
                $additions[] = self::DHL_12;

//            $additions[] = self::DHL_CONNECT;

            if($courier->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR && $servicesSender[DhlClient::OPT_NAD_SB])
                $additions[] = self::DHL_NAD_SB;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }


    protected static function _findPocztaPolskaAdditions(Courier $courier)
    {

        $CACHE_NAME = 'POCZTA_POLSKA_ADDITIONS_INTERNAL';

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = self::PP_OKRESLONA_WARTOSC;
            $additions[] = self::PP_POTW_ODBIORU;
//            $additions[] = self::PP_PRIO;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDhlDeAdditions(Courier $courier)
    {

        $CACHE_NAME = 'DHL_DE_ADDITIONS_INTERNAL_'.$courier->senderAddressData->country_id.'_'.$courier->receiverAddressData->country_id;
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $deliveryOperator = $courier->courierTypeInternal->getDeliveryOperator(true);


            $additions = [];

            $additions[] = self::UNIQID_DHLDE_INSURANCE;
            $additions[] = self::UNIQID_DHLDE_NOTIFICATION;

            /* @var $deliveryOperator CourierOperator */
            if($deliveryOperator->uniq_id == CourierOperator::UNIQID_DHLDE_PAKET)
                $additions[] = self::UNIQID_DHLDE_PAEKET_PRIO;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findUkMailAdditions(Courier $courier)
    {
        $CACHE_NAME = 'UK_MAIL_ADDITIONS_INTERNAL_'.$courier->senderAddressData->country_id.'_'.$courier->receiverAddressData->country_id;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            if (!in_array($courier->senderAddressData->country_id, [CountryList::COUNTRY_UK, CountryList::COUNTRY_IE]) OR !in_array($courier->receiverAddressData->country_id, [CountryList::COUNTRY_UK, CountryList::COUNTRY_IE]))
            {
                $additions[] = self::UNIQID_UKMAIL_AIR;
            } else {

                $deliveryOperator = $courier->courierTypeInternal->getDeliveryOperator(true);
                /* @var $deliveryOperator CourierOperator */
                if($deliveryOperator->uniq_id == CourierOperator::UNIQID_UK_MAIL)
                {
                    $additions[] = self::UNIQID_UKMAIL_SPECIFIED_ADDRESS_ONLY;
                    $additions[] = self::UNIQID_UKMAIL_LEAVE_SAFE_OR_SIG_NEIGH;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_9;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_1030;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_12;
                    $additions[] = self::UNIQID_UKMAIL_SAT;
                    $additions[] = self::UNIQID_UKMAIL_SAT_9;
                    $additions[] = self::UNIQID_UKMAIL_SAT_1030;
                    $additions[] = self::UNIQID_UKMAIL_48_HOUR ;
                    $additions[] = self::UNIQID_UKMAIL_48_HOUR_PLUS;
                }
                else if($deliveryOperator->uniq_id == CourierOperator::UNIQID_UK_MAIL_RETURN OR $deliveryOperator->uniq_id == CourierOperator::UNIQID_UK_MAIL_3RD)
                {
                    $additions[] = self::UNIQID_UKMAIL_SPECIFIED_ADDRESS_ONLY;
                    $additions[] = self::UNIQID_UKMAIL_LEAVE_SAFE_OR_SIG_NEIGH;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_9;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_1030;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_12;
                    $additions[] = self::UNIQID_UKMAIL_SAT;
                }
            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDhlExpressAdditions(Courier $courier)
    {
        $CACHE_NAME = 'DHL_EXPRESS_ADDITIONS_INTERNAL_'.$courier->senderAddressData->country_id.'_'.$courier->receiverAddressData->country_id.'_'.Yii::app()->user->id;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

//            if( in_array($courier->receiverAddressData->country_id, CountryList::getUeList(true)))
//            {
            $additions[] = self::UNIQID_DHLEXPRESS_UE_12;
            $additions[] = self::UNIQID_DHLEXPRESS_UE_9;
            $additions[] = self::UNIQID_DHLEXPRESS_UE_ECONOMY;

//            }
//            else if($courier->receiverAddressData->country_id == CountryList::COUNTRY_US)
//            {
            $additions[] = self::UNIQID_DHLEXPRESS_USA_1030;
//            } else {
            $additions[] = self::UNIQID_DHLEXPRESS_NONUE_12;
            $additions[] = self::UNIQID_DHLEXPRESS_NONUE_9;
            $additions[] = self::UNIQID_DHLEXPRESS_NONUE_ECONOMY;
//            }

            $additions[] = self::UNIQID_DHLEXPRESS_XPD;
            $additions[] = self::UNIQID_DHLEXPRESS_BREAKBULK;

            /* @var $deliveryOperator CourierOperator */
            $deliveryOperator = $courier->courierTypeInternal->getDeliveryOperator(true);

            $servicesChecker = DhlExpressClient::getRateRequest($courier->courierTypeInternal, $courier->senderAddressData, $courier->receiverAddressData, $deliveryOperator->uniq_id, $courier->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR ? true : false);
            $additions = array_intersect($additions, $servicesChecker);

            // REST OF ADDITONS:



            if($deliveryOperator->uniq_id == CourierOperator::UNIQID_DHLEXPRESS_FB)
                $additions[] = self::UNIQID_DHLEXPRESS_ID8000;


            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findUpsAdditions(Courier $courier)
    {

        /* @var $deliveryOperator CourierOperator */
        $deliveryOperator = $courier->courierTypeInternal->getDeliveryOperator(true);
        $deliveryOperator_id = $deliveryOperator->id;
        $CACHE_NAME = 'UPS_ADDITIONS_INTERNAL_'.$deliveryOperator_id;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];
            $additions[] = self::UNIQID_UPS_ROD;
            $additions[] = self::UNIQID_UPS_NOTIFICATION;

//            if(UpsSoapShipClient::isCourierOperatorUpsSaver($deliveryOperator_id))
//                $additions[] = self::UNIQID_UPS_SATURDAY_DELIVERY;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findRoyalMailAdditions(Courier $courier)
    {


        $CACHE_NAME = 'ROYAL_MAIL_ADDITIONS_INTERNAL';

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND broker_id = '.CourierOperator::BROKER_ROYAL_MAIL);

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDpdPlAdditions(Courier $courier)
    {

        $CACHE_NAME = 'DPDPL_ADDITIONS_INTERNAL';

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];
            $additions[] = self::UNIQID_DPDPL_ROD;
            $additions[] = self::UNIQID_DPDPL_GUARANTEED_SUN;
            $additions[] = self::UNIQID_DPDPL_GUARANTEED_SAT;
            $additions[] = self::UNIQID_DPDPL_GUARANTEED_1200;
            $additions[] = self::UNIQID_DPDPL_GUARANTEED_930;
            $additions[] = self::UNIQID_DPDPL_CARRYIN;
            $additions[] = self::UNIQID_DPDPL_DECLARED_VALUE;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }


    protected static function _findDpdNlAdditions(Courier $courier)
    {

        $CACHE_NAME = 'DPDNL_ADDITIONS_INTERNAL_'.$courier->senderAddressData->country_id.'_'.$courier->receiverAddressData->country_id;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];


            if($courier->senderAddressData->country_id == $courier->receiverAddressData->country_id) {
                $additions[] = self::UNIQID_DPDNL_TYRES;
                $additions[] = self::UNIQID_DPDNL_SATURDAY;
                $additions[] = self::UNIQID_DPDNL_E10;
                $additions[] = self::UNIQID_DPDNL_E12;
                $additions[] = self::UNIQID_DPDNL_G18;
            } else
                $additions[] = self::UNIQID_DPDNL_INTERNATIONAL_EXPRESS;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findLpExpressAdditions(Courier $courier)
    {

        $isBefore1pm = (date('H') < 13);
        $toTerminal = false;

        $CACHE_NAME = 'LPEXPRESS_ADDITIONS_'.$courier->receiverAddressData->country_id.'_'.$courier->getWeight(true).'_'.$isBefore1pm.'_'.$toTerminal;
        $models = Yii::app()->cache->get($CACHE_NAME);



        if ($models === false) {

            $additions = [];

            $additions[] = self::UNIQID_LPEXPRESS_INSURANCE;

            if($courier->receiverAddressData->country_id == CountryList::COUNTRY_LT) {

                if(!$toTerminal)
                    $additions[] = self::UNIQID_LPEXPRESS_HAS_COVER_LETTER;

                if(!$toTerminal)
                    $additions[] = self::UNIQID_LPEXPRESS_SATURDAY_DELIVERY;

                $additions[] = self::UNIQID_LPEXPRESS_DELIVERY_NOTIFICATION;

                if($courier->getWeight(true) > 30 && !$toTerminal)
                    $additions[] = self::UNIQID_LPEXPRESS_HAS_LOAD;

                if($isBefore1pm && !$toTerminal)
                    $additions[] = self::UNIQID_LPEXPRESS_SAME_DAY_DELIVERY;

            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

//    protected static function _findSwedenPostAdditions(Courier $courier)
//    {
//        $CACHE_NAME = 'SWEDENPOST_ADDITIONS';
//        $models = Yii::app()->cache->get($CACHE_NAME);
//
//        if ($models === false) {
//
//            $additions = [];
//
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SWEDEN_POST_LITHIUM;
//
//            $array = implode(',', $additions);
//
//            if(!S_Useful::sizeof($additions))
//                $models = [];
//            else
//                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');
//
//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }
//
//        return $models;
//    }

    protected static function _findDpdDeAdditions(Courier $courier)
    {

        $receiver_country_id = $courier->receiverAddressData->country_id;

        $CACHE_NAME = 'DPDDE_ADDITIONS_'.$receiver_country_id;
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            if($receiver_country_id == CountryList::COUNTRY_DE) {
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_830;
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_10;
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_12;
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_18;
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_12_SAT;
            } else {
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_INT;
            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

//    protected static function _findDpdIrAdditions(Courier $courier)
//    {
//
//        $is_receiver_ie = $courier->receiverAddressData->country_id == CountryList::COUNTRY_IE ? 1 : 0;
//
//        $CACHE_NAME = 'DPDIR_ADDITIONS_'.$is_receiver_ie;
//        $models = Yii::app()->cache->get($CACHE_NAME);
//
//        if ($models === false) {
//
//            $additions = [];
//
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_EMAIL_NOTIFY;
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SMS_NOTIFY;
//
//            if($is_receiver_ie)
//                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SATURDAY_DELIVERY;
//            else
//                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_OVERNIGHT_INT_SERVICE;
//
//            $array = implode(',', $additions);
//
//            if(!S_Useful::sizeof($additions))
//                $models = [];
//            else
//                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');
//
//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }
//
//        return $models;
//    }

//    protected static function _findAnpostAdditions(Courier $courier)
//    {
//
//        $is_receiver_ie = $courier->receiverAddressData->country_id == CountryList::COUNTRY_IE ? 1 : 0;
//
//        $CACHE_NAME = 'DPDIR_ANPOST_'.$is_receiver_ie;
//        $models = Yii::app()->cache->get($CACHE_NAME);
//
//        if ($models === false) {
//
//            $additions = [];
//
//            $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_NOTIFY_SMS;
//            $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_NOTIFY_EMAIL;
//
//            if($is_receiver_ie)
//                $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_SATURDAY_PREMIUM;
//
//            $array = implode(',', $additions);
//
//            if(!S_Useful::sizeof($additions))
//                $models = [];
//            else
//                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');
//
//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }
//
//        return $models;
//    }

//    protected static function _findWhistlAdditions(Courier $courier)
//    {
//
//        $CACHE_NAME = 'DPDIR_WHISTL';
//        $models = Yii::app()->cache->get($CACHE_NAME);
//
//        if ($models === false) {
//
//            $additions = [];
//
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_POD;
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_AEROSOL;
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_FRAGILE;
//
//            $array = implode(',', $additions);
//
//            if(!S_Useful::sizeof($additions))
//                $models = [];
//            else
//                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');
//
//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }
//
//        return $models;
//    }
}