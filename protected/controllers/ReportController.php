<?php

class ReportController extends CController {

    public function actionRunAdminCourierScan()
    {
        Report_AdminCourierScan::processReport(19);
    }

    public function actionRunEobuwieCourierScan()
    {
        Report_EobuwieCourierScan::processReport();
    }

    public function actionRunCustomersCourierReturnScan()
    {
        Report_CustomersCourierReturnScan::processAllReports();
    }

    public function actionRunPocztapolskaFirbusinessSent()
    {
        Report_PocztaPolskaFirebusinessSent::processReport();
    }

    public function actionRunCodSettled()
    {
        Report_CodSettled::processReport();
    }

    public function actionRunCourierSentNumbers()
    {
        Report_CourierSentNumbers::processReport();
    }

    public function actionRunEobuwieDelivered()
    {
        Report_EobuwieDeliveredReport::processReport();
    }


}