<?php
/* @var $ebayClient ebayClient */
/* @var $orders EbayOrderItem[] */
/* @var $senderAddressData AddressData */
/* @var $package Courier_CourierTypeOoe */
?>

<h2><?= Yii::t('courier_ebay', 'Import paczek z systemu eBay.com');?> | <?= Yii::t('courier_ebay', 'Krok {n}/{m}', ['{n}' => $step, '{m}' => 4]);?></h2>

<?php
$this->widget('FlashPrinter');
?>

<?php if($ebayClient): ?>
    <?php
    if(!$ebayClient->isLogged() && $step == 1):
        ?>
        <div class="alert alert-success" style="text-align: center;">
            <p><?= Yii::t('courier_ebay', 'Aby kontynuować, zaloguj się na swoje konto eBay klikając w poniższy przycisk. Pozwoli nam to wykonać import Twoich zamówień do systemu Świat Przesyłek.');?></p>
            <br/>
            <a href="<?= Yii::app()->createUrl('/courier/ebay/login', ['return' => ebayClient::RETURN_POSTAL]);?>" class="btn btn-primary"><?= Yii::t('courier_ebay','Zaloguj się na konto eBay.com');?></a>
        </div>
        <?php
    elseif($step == 1):
        ?>
        <div class="alert alert-success" style="text-align: center;">
            <?= Yii::t('courier_ebay', 'Jesteś zalogowany na konto eBay: {strong}{login}{/strong}', ['{strong}' => '<strong>', '{/strong}' => '</strong>', '{login}' => $ebayClient->getLogin()] );?> (<a href="<?= Yii::app()->createUrl('/courier/ebay/logout', ['return' => ebayClient::RETURN_POSTAL]);?>"><?= Yii::t('courier_ebay', 'wyloguj się');?></a>)<br/>
        </div>
    <?php endif;?>

<?php endif; ?>



