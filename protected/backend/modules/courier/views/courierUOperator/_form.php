<?php
/* @var $model CourierUOperator */
?>

    <div class="form">


        <?php $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'courier-operator-form',
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">
            <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
        </p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
            <?php echo $form->error($model,'name'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model,'name_customer'); ?>
            <?php echo $form->textField($model, 'name_customer'); ?>
            <?php echo $form->error($model,'name_customer'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model,'label_symbol'); ?>
            <?php echo $form->textField($model, 'label_symbol'); ?>
            <?php echo $form->error($model,'label_symbol'); ?>
        </div><!-- row -->

        <div id="langVersionMenuContainer">
            <?php
            foreach(Language::model()->findAll('stat = 1') AS $languageItem)
                echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';
            ?>
        </div>


        <div id="langVersionContainer">
            <?php
            foreach(Language::model()->findAll('stat = 1') AS $languageItem):

                echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

                $model_tr = $modelTrs[$languageItem->id];
                if($model_tr === null)
                {
                    $model_tr = new CourierUOperatorTr();
                    $model_tr->language_id = $languageItem->id;
                }

                $this->renderPartial('_tr_form', array(
                    'model' => $model_tr,
                    'form' => $form,
                    'i' => $languageItem->id,
                ));

                echo'</div>';
            endforeach;
            ?>
        </div>

        <?php
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/colorpicker/jquery.wheelcolorpicker.js');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/colorpicker/css/wheelcolorpicker.css');
        ?>


        <div class="row">
            <?php echo $form->labelEx($model,'has_dim_weight'); ?>
            <?php echo $form->dropDownList($model, 'has_dim_weight', [0 => 'No', 1 => 'Yes']); ?>
            <?php echo $form->error($model,'has_dim_weight'); ?>
        </div><!-- row -->

        <div class="row">
            <?php echo $form->labelEx($model,'dim_weight_factor'); ?>
            <?php echo $form->textField($model, 'dim_weight_factor'); ?>
            <?php echo $form->error($model,'dim_weight_factor'); ?>
        </div><!-- row -->
        <fieldset><legend>Max dimensions</legend>
        <div class="row">
            <table>
                <tr>
                    <td>
                        <?php echo $form->labelEx($model,'_max_dim_l'); ?>
                        <?php echo $form->textField($model, '_max_dim_l'); ?>
                        <?php echo $form->error($model,'_max_dim_l'); ?>
                    </td>
                    <td>
                        <?php echo $form->labelEx($model,'_max_dim_d'); ?>
                        <?php echo $form->textField($model, '_max_dim_d'); ?>
                        <?php echo $form->error($model,'_max_dim_d'); ?>
                    </td>
                    <td>
                        <?php echo $form->labelEx($model,'_max_dim_w'); ?>
                        <?php echo $form->textField($model, '_max_dim_w'); ?>
                        <?php echo $form->error($model,'_max_dim_w'); ?>
                    </td>
                </tr>
            </table>
        </div><!-- row -->
            <i>Leave emtpy to keep default values.</i>
        </fieldset>

        <fieldset><legend>Max NST dimensions</legend>
        <div class="row">
            <table>
                <tr>
                    <td>
                        <?php echo $form->labelEx($model,'_max_dim_nst_l'); ?>
                        <?php echo $form->textField($model, '_max_dim_nst_l'); ?>
                        <?php echo $form->error($model,'_max_dim_nst_l'); ?>
                    </td>
                    <td>
                        <?php echo $form->labelEx($model,'_max_dim_nst_d'); ?>
                        <?php echo $form->textField($model, '_max_dim_nst_d'); ?>
                        <?php echo $form->error($model,'_max_dim_nst_d'); ?>
                    </td>
                    <td>
                        <?php echo $form->labelEx($model,'_max_dim_nst_w'); ?>
                        <?php echo $form->textField($model, '_max_dim_nst_w'); ?>
                        <?php echo $form->error($model,'_max_dim_nst_w'); ?>
                    </td>
                </tr>
            </table>
        </div><!-- row -->
            <i>Leave emtpy to keep default values.</i>
        </fieldset>

        <?php
        if($model->isNewRecord):
            ?>
            <div class="row">
                <?php echo $form->labelEx($model,'broker_id'); ?>
                <?php echo $form->dropDownList($model, 'broker_id', CourierUOperator::getBrokerList(true), array('id' => 'broker-option')); ?>
                <?php echo $form->error($model,'broker_id'); ?>
            </div><!-- row -->
            <?php
        else:
            ?>
            <?php echo $form->hiddenField($model, 'broker_id',  array('id' => 'broker-option')); ?>
        <?php endif; ?>

        <div class="ooe-details">
            <div class="row">
                <?php echo CHtml::label('OOE Login', '_ooe_login') ?>
                <?php echo CHtml::textField('CourierUOperator[_ooe_login]', $model->_ooe_login, array('id' => '_ooe_login')) ?>
            </div><!-- row -->

            <div class="row">
                <?php echo CHtml::label('OOE Account', '_ooe_account') ?>
                <?php echo CHtml::textField('CourierUOperator[_ooe_account]', $model->_ooe_account, array('id' => '_ooe_account')) ?>
            </div><!-- row -->

            <div class="row">
                <?php echo CHtml::label('OOE Pass', '_ooe_pass') ?>
                <?php echo CHtml::passwordField('CourierUOperator[_ooe_pass]', $model->_ooe_pass, array('id' => '_ooe_pass')) ?>
            </div><!-- row -->

            <div class="row">
                <?php echo CHtml::label('OOE Service', '_ooe_service') ?>
                <?php echo CHtml::textField('CourierUOperator[_ooe_service]', $model->_ooe_service, array('id' => '_ooe_service')) ?>
            </div><!-- row -->
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'stat'); ?>
            <?php echo $form->dropDownList($model, 'stat', CourierUOperator::getStatList()); ?>
            <?php echo $form->error($model,'stat'); ?>
        </div><!-- row -->

        <div class="row">
            <?php echo CHtml::label('Available countries', 'countries'); ?>
            <?php echo CHtml::dropDownList('CourierUOperator[__countries]', CHtml::listData($model->getActiveCountriesGroup(), 'id', 'id'), CHtml::listData($model->getAllCountiesGroup(), 'id', 'name'), ['multiple' => true, 'size' => 15, 'id' => 'countries', 'data-drag-select' => 'true']);?>
            <br/><input type="button" value="select all" onclick="$(this).parent().find('[data-drag-select]').multiSelect('select_all');"/>
        </div>

        <div id="visualization" style="width: 150px; height: 120px; border-radius: 25px; padding-top: 15px; position: relative; text-align: center">
        <div style="font-family: Oswald, sans-serif; text-transform: uppercase; font-weight: bold; font-size: 1.1em;"><?= $model->name_customer ? $model->name_customer : 'Operator XXX';?></div>

        <div style="position: absolute; bottom: 10px; left: 0; right: 0; color: white; font-family: Tahoma; font-weight: bold; font-size: 0.9em;">10,00 zł</div>
    </div>


    <div class="row">
            <?php echo CHtml::label('Background color', '_bg_color') ?>
            <?php echo CHtml::textField('CourierUOperator[_bg_color]', $model->_bg_color, array('id' => '_bg_color', 'data-wheelcolorpicker' => true)) ?>
        </div><!-- row -->

        <div class="row">
            <?php echo CHtml::label('Font color', '_font_color') ?>
            <?php echo CHtml::textField('CourierUOperator[_font_color]', $model->_font_color, array('id' => '_font_color', 'data-wheelcolorpicker' => true)) ?>
        </div><!-- row -->



        <div class="text-center">
            <?php
            echo GxHtml::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-lg']);
            $this->endWidget();
            ?>
        </div>
    </div><!-- form -->



    <script>
        var $brokerOption;
        var $ooeDetails;

        $(document).ready(function(){


            var $bgColor = $('#_bg_color');
            var $fontColor = $('#_font_color');
            var $visualization = $('#visualization');

            $bgColor.on('change', setBgColor);
            $fontColor.on('change', setFontColor);

            setBgColor();
            setFontColor();

            function setBgColor()
            {
                $visualization.css('background-color', '#' + $bgColor.val());
            }

            function setFontColor()
            {
                $visualization.css('color', '#' + $fontColor.val());
            }

            $brokerOption = $("#broker-option");
            $ooeDetails = $(".ooe-details");


            if($brokerOption.val() == <?= CourierUOperator::BROKER_OOE;?>) {
                $ooeDetails.show();
            }
            else {
                $ooeDetails.hide();
            }


            $brokerOption.on('change', function(){
                if($(this).val() == <?= CourierUOperator::BROKER_OOE;?>) {
                    $ooeDetails.show();
                }
                else
                {
                    $ooeDetails.hide();
                }
            })
        });

    </script>

<?php
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/dropImage.js', CClientScript::POS_END);
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/preloader.js', CClientScript::POS_END);
//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/backend/dropImage.css');
//Yii::app()->clientScript->registerScript('jsHelpers', '
//          jsHelpers = {
//              url: {
//                  resizeImageUrl: '.CJSON::encode(Yii::app()->createAbsoluteUrl('/user/ajaxPrepareImage', ['maxWidth' => CourierUOperator::IMG_WIDTH, 'maxHeight' => CourierUOperator::IMG_HEIGHT])).',
//              },
//               text: {
//                  imageSaved: '.CJSON::encode('Twój obrazek został zapisany!').',
//                  imageSaveError: '.CJSON::encode('Ups, coś się nie udało...').',
//                  onlyImagesError: '.CJSON::encode('Dozwolone są jedynie obrazki').',
//                  imageTooBigError: '.CJSON::encode('Maksymanla dozwolona waga pliku to: ').',
//              },
//          };
//      ', CClientScript::POS_HEAD);
?>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/dragSelect/js/jquery.multi-select.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/dragSelect/css/multi-select.css');
$script = "
$('[data-drag-select]').multiSelect({
selectableHeader: '<div class=\"drag-select-header drag-select-header-available\">Available:</div>',
  selectionHeader: '<div class=\"drag-select-header drag-select-header-selected\">Selected:</div>',
  })
";
Yii::app()->clientScript->registerScript('drag-select', $script, CClientScript::POS_READY);
$css = '
.ms-container{ 
  width: 100%;
}
.ms-container .ms-list{
height: 350px;
}

.drag-select-header
{
background: lightgray;
font-weight: bold;
padding: 2px;
text-indent: 2px;
}

.drag-select-header-available
{
}

.drag-select-header-selected
{
background: lightblue;
}

.ms-selection ul
{
font-weight: bold;
}
';
Yii::app()->clientScript->registerCss('drag-select', $css);

