<?php
//
//class bChangeStat extends bBaseBehavior
//{
//    protected $_allowedClass = array('Courier', 'HybridMail', 'InternationalMail');
//
//    protected function getStatClass()
//    {
//        $statClass = NULL;
//
//        switch($this->getClass())
//        {
//            case 'Courier':
//                $statClass = 'CourierStat';
//                break;
//            case 'HybridMail':
//                $statClass = 'HybridMailStat';
//                break;
//            case 'InternationalMail':
//                $statClass = 'InternationalMailStat';
//                break;
//        }
//
//        return $statClass;
//    }
//
//    protected function getStatHistoryClass()
//    {
//        $statClass = NULL;
//        switch($this->getClass())
//        {
//            case 'Courier':
//                $statClass = 'CourierStatHistory';
//                break;
//            case 'HybridMail':
//                $statClass = 'HybridMailStatHistory';
//                break;
//            case 'InternationalMail':
//                $statClass = 'InternationalMailStatHistory';
//                break;
//        }
//
//        return $statClass;
//    }
//
//    protected function getForeignKeyClassAttribute()
//    {
//        $attribute = NULL;
//        switch($this->getClass())
//        {
//            case 'Courier':
//                $attribute = 'courier_id';
//                break;
//            case 'HybridMail':
//                $attribute = 'hybrid_mail_id';
//                break;
//            case 'InternationalMail':
//                $attribute = 'international_mail_id';
//                break;
//        }
//
//        return $attribute;
//    }
//
//    protected function getForeignKeyClassStatAttribute()
//    {
//        $attribute = NULL;
//        switch($this->getClass())
//        {
//            case 'Courier':
//                $attribute = 'courier_stat_id';
//                break;
//            case 'HybridMail':
//                $attribute = 'stat';
//                break;
//            case 'InternationalMail':
//                $attribute = 'international_mail_stat_id';
//                break;
//        }
//
//        return $attribute;
//    }
//
//    public function changeStat($newStat, $author = 0, $status_date = NULL)
//    {
//        $statClass = $this->getStatClass();
//
//        $newStat = $statClass::model()->findByPk($newStat);
//        if($newStat === null)
//            return false;
//
//        if($this->owner()->stat == $newStat->id)
//            return true;
//
//        $statHistoryClass = $this->getStatHistoryClass();
//        $foreignKeyClassAttribute = $this->getForeignKeyClassAttribute();
//        $foreignKeyClassStatAttribute = $this->getForeignKeyClassStatAttribute();
//
//        $statHistory = new $statHistoryClass;
//        $statHistory->author = $author;
//        $statHistory->$foreignKeyClassAttribute = $this->owner()->id;
//        $statHistory->previous_stat_id = $this->owner()->$foreignKeyClassStatAttribute;
//        $statHistory->current_stat_id = $newStat->id;
//        $statHistory->status_date = $status_date;
//
//        $statHistory->validate();
//
//        if(!$statHistory->save())
//            return false;
//
//        $this->owner()->$foreignKeyClassStatAttribute  = $newStat->id;
//
//
//
//        if(!$this->owner()->save(false))
//            return false;
//
//
//        S_StatNotifer::onStatusChange(S_StatNotifer::SERVICE_COURIER, $this->id, $newStat->id, $status_date);
//
//        return true;
//    }
//
//
//}