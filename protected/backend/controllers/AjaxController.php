<?php

class AjaxController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
//                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

   public function actionAjaxPrepareImage($maxWidth = false, $maxHeight = false)
   {
       if(Yii::app()->request->isAjaxRequest)
       {
           try {
               $img = $_POST['img'];

               $img = explode(',', $img);

               if(!isset($img[1]))
                   throw new Exception;

               $img = $img[1];

               $img = base64_decode($img);

               if($img == '')
                   throw new Exception;

               $im = ImagickMine::newInstance();
               if(!$im->readImageBlob($img))
                   throw new Exception;

               if ($maxWidth && $im->getImageWidth() > $maxWidth)
                   $im->scaleImage($maxWidth, 0);

               if ($maxHeight && $im->getImageHeight() > $maxHeight)
                   $im->scaleImage(0, $maxHeight);


               $im->setFormat('png8');
               $im->stripImage();

               $imgString = $im->getImageBlob();
               $imgString = base64_encode($imgString);
               $imgString = 'data:' . $im->getImageMimeType() . ';base64,' . $imgString;

               echo CJSON::encode(['image' => $imgString]);
           }
           catch(Exception $ex)
           {
               echo CJSON::encode(['image' => false]);
           }
       } else {
           $this->redirect(['/']);
       }
   }
}