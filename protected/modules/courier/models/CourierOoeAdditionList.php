<?php

Yii::import('application.modules.courier.models._base.BaseCourierOoeAdditionList');

class CourierOoeAdditionList extends BaseCourierOoeAdditionList
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('date_entered', 'default',
				'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
			array('date_updated', 'default',
				'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
			array('stat', 'default',
				'value'=>S_Status::ACTIVE, 'on'=>'insert'),


			array('name', 'required'),
			array('stat', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('date_updated', 'safe'),
			array('date_updated, stat', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, date_entered, date_updated, name, description, stat', 'safe', 'on'=>'search'),
		);
	}

    public function getClientTitle()
    {
        return $this->courierOoeAdditionListTr->title;
    }

    public function getClientDesc()
    {
        return $this->courierOoeAdditionListTr->description;
    }

	public function toggleStat()
	{
		$this->stat = abs($this->stat - 1);

		return $this->save(false,array('stat'));
	}

	public function saveWithTr($ownTransaction = true)
	{
		if($ownTransaction)
			$transaction = Yii::app()->db->beginTransaction();

		$errors = false;

		if(!$this->save())
			$errors = true;

		foreach($this->courierOoeAdditionListTrs AS $item)
		{

			$item->courier_ooe_addition_list_id = $this->id;
			if(!$item->save()) {
				$errors = true;
			}
		}

		if($errors)
		{
			if($ownTransaction)
				$transaction->rollback();

			return false;
		} else {
			if($ownTransaction)
				$transaction->commit();

			return true;
		}
	}

	public static function getAdditions()
	{
		// find universal additions
		$models = self::model()->findAll('stat = '.S_Status::ACTIVE);
		return $models;
	}

}