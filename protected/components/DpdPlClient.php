<?php

class DpdPlClient
{
    const URL = GLOBAL_CONFIG::DPDPL_URL;

    protected $__username = GLOBAL_CONFIG::DPDPL_USERNAME;
    protected $__password = GLOBAL_CONFIG::DPDPL_PASSWORD;
    protected $__masterfid = GLOBAL_CONFIG::DPDPL_MASTERFID;
    protected $__fid = GLOBAL_CONFIG::DPDPL_FID;

    public $withCollect = true;

    public $reference;

    public $package_value;

    public $sender_name;
    public $sender_company;
    public $sender_address;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_tel;
    public $sender_mail;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_tel;
    public $receiver_mail;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $packages_number = 1;
    public $package_content;

    public $user_id;

    public $cod_value = 0;
    public $cod_currency = false;
    public $declared_value = 0;
    public $rod = false;
    public $guaranteed_9300 = false;
    public $guaranteed_1200 = false;
    public $guaranteed_sat = false;
//    public $guaranteed_sun = false; // no longer available
    public $carryIn = false;

    public $cud = false;
    public $inPers = false;
    public $privPers = false;
    public $dox = false;

    public function forceMainAccount($overwriteData = true)
    {
        // just in case
        $this->__username = GLOBAL_CONFIG::DPDPL_USERNAME;
        $this->__password = GLOBAL_CONFIG::DPDPL_PASSWORD;
        $this->__masterfid = GLOBAL_CONFIG::DPDPL_MASTERFID;
        $this->__fid = GLOBAL_CONFIG::DPDPL_FID;

//        $this->sender_name = 'BTL Logistic sp. z o.o.';
//        $this->sender_company = '';
        if($overwriteData) {
            $this->sender_zip_code = '48300';
            $this->sender_city = 'Nysa';
            $this->sender_address = 'ul. Grodkowska 40';
            $this->sender_tel = '221221219';
            $this->sender_mail = '';
            $this->sender_country = 'PL';
        }
    }

    public function setSpAccount(User $user, $overwriteData = true)
    {

        // if subaccount not provided, get froum user data:
        if($user->getCustomDpdPlSPSubaccount())
            $subaccount = $user->getCustomDpdPlSPSubaccount(); // get custom subaccount
        else
            $subaccount = false; // set default subaccount

        $this->__username = '25254901';
        $this->__password = 'mq1NeAVbBXyf4B2E';
        $this->__masterfid = '252549';

        switch($subaccount) {
            case User::CUSTOM_SUBACCOUNT_SP_WRO:
                $this->__fid = '258136';
                if ($overwriteData)
                {
                    $this->sender_zip_code = '52407';
                    $this->sender_city = 'Wrocław';
                    $this->sender_address = 'Ul. Tyniecka 5a';
                    $this->sender_tel = '510291995';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case false:
            default:
                $this->__fid = '252549';
                break;
        }
    }


    public function setCustomAccount(User $user, $overwriteData = true)
    {
        throw new Exception('Inactive DPD PL accout!');

        // CUSTOM DATA FOR DPD_V:
        $this->__username = '20941801';
        $this->__password = 'ZtwqAOCFbZuoeK7j';
        $this->__masterfid = '209418';

        // if subaccount not provided, get froum user data:
        if($user->getCustomDpdPlVSubaccount())
            $subaccount = $user->getCustomDpdPlVSubaccount(); // get custom subaccount
        else
            $subaccount = false; // set default subaccount

        switch($subaccount) {
            case User::CUSTOM_SUBACCOUNT_V_MCM:
                $this->__fid = '215200';
//                $this->sender_name = 'MCM';
//                $this->sender_company = '';
                if ($overwriteData)
                {
                    $this->sender_zip_code = '53608';
                    $this->sender_city = 'Wrocław';
                    $this->sender_address = 'ul. Robotnicza 72';
                    $this->sender_tel = '510291995';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_PHUKK:
                $this->__fid = '215202';
//                $this->sender_name = 'PHUKK';
//                $this->sender_company = '';
                if ($overwriteData) {
                    $this->sender_zip_code = '66530';
                    $this->sender_city = 'Drezdenko';
                    $this->sender_address = 'Osów 15a';
                    $this->sender_tel = '504071133';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_RUCH:
                $this->__fid = '215203';
//                $this->sender_name = 'RUCH';
//                $this->sender_company = '';
                if ($overwriteData) {
                    $this->sender_zip_code = '03236';
                    $this->sender_city = 'Warszawa';
                    $this->sender_address = 'ul. Annopol 17A';
                    $this->sender_tel = '504075261';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_MS:
                $this->__fid = '215205';
//                $this->sender_name = 'MAIL SOLUTION';
//                $this->sender_company = '';
                if ($overwriteData) {
                    $this->sender_zip_code = '02669';
                    $this->sender_city = 'Warszawa';
                    $this->sender_address = 'ul. Kłobucka 23A lok. 324';
                    $this->sender_tel = '501407557';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_BIGKALIBER:
                $this->__fid = '216038';
//                $this->sender_name = 'BIG KALIBER';
//                $this->sender_company = '';
                if ($overwriteData) {
                    $this->sender_zip_code = '43340';
                    $this->sender_city = 'Kozy';
                    $this->sender_address = 'Krakowska 42';
                    $this->sender_tel = '123123123';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_JAGLAND:
                $this->__fid = '218907';
//                $this->sender_name = 'JAG-LAND';
//                $this->sender_company = '';
                if ($overwriteData) {
                    $this->sender_zip_code = '63820';
                    $this->sender_city = 'Piaski';
                    $this->sender_address = 'ul. Poznańska 52';
                    $this->sender_tel = '601254406';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_ALLIOONE:
                $this->__fid = '220936';
//                $this->sender_name = 'JAG-LAND';
//                $this->sender_company = '';
                if ($overwriteData) {
                    $this->sender_zip_code = '47206';
                    $this->sender_city = 'Kędzierzyn Koźle';
                    $this->sender_address = 'ul. Litewska 15';
                    $this->sender_tel = '511222555';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_SIECHNICE:
                $this->__fid = '223239';
                if ($overwriteData) {
                    $this->sender_zip_code = '55011';
                    $this->sender_city = 'Siechnice';
                    $this->sender_address = 'Kwiatkowskiego 24';
                    $this->sender_tel = '726003814';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_PWC:
                $this->__fid = '217351';
//                $this->sender_name = 'PWC Paweł Ważny';
//                $this->sender_company = '';
                if ($overwriteData) {
                    $this->sender_zip_code = '47220';
                    $this->sender_city = 'Kędzierzyn Koźle';
                    $this->sender_address = 'ul. Dworcowa 1B';
                    $this->sender_tel = '576567227';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
            case User::CUSTOM_SUBACCOUNT_V_KAAB:
            case false:
            default:
                $this->__fid = '215207';
//                $this->sender_name = 'KAAB';
//                $this->sender_company = '';
                if ($overwriteData) {
                    $this->sender_zip_code = '48300';
                    $this->sender_city = 'Nysa';
                    $this->sender_address = 'ul. Grodkowska 40';
                    $this->sender_tel = '221221219';
                    $this->sender_mail = '';
                    $this->sender_country = 'PL';
                }
                break;
        }
    }


    protected function _soapOperation($functionName, stdClass $data, $customUrl = false)
    {
        if($customUrl)
            $url = $customUrl;
        else
            $url = self::URL;


        $mode = array
        (
//            'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false)))
        );

        $return = new stdClass();
        $return->success = false;

        MyDump::dump('dpdppl.txt', 'DATA: '.print_r($data,1));
        MyDump::dump('dpdppl.txt', 'URL: '.print_r($url,1));

        try {

            $client = new SoapClient($url , $mode);
            $resp = $client->__soapCall($functionName, [$functionName => $data]);
            MyDump::dump('dpdppl.txt', 'REQUEST: '.print_r($client->__getLastRequest(),1));
            MyDump::dump('dpdppl.txt', 'RESPONSE: '.print_r($resp,1));
        } catch (SoapFault $E) {

            MyDump::dump('dpdppl.txt', 'REQUEST: '.print_r($client->__getLastRequest(),1));
            MyDump::dump('dpdppl.txt', 'ERROR: '.print_r($E,1));

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;
            return $return;

        } catch (Exception $E) {

            MyDump::dump('dpdppl.txt', 'REQUEST: '.print_r($client->__getLastRequest(),1));
            MyDump::dump('dpdppl.txt', 'ERROR: '.print_r($E,1));

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;
            return $return;
        }

        $return->success = true;
        $return->data = $resp;

        return $return;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniqid, $returnError = false, $withCollect = false)
    {

        $overwriteSenderData = false;

        $model = new self;

        $model->user_id = $courierInternal->courier->user_id;

        if($uniqid == GLOBAL_BROKERS::OPERATOR_UNIQID_DPDPL_SP) // @13.03.2019 - block pickups for DPD PL SP
        {
            $withCollect = false;

            if(!$courierInternal->courier->user->getDpdPlSpDisableSenderOverwrite())
                $overwriteSenderData = true;
        }



        // verify collection
        if($withCollect)
        {
            // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
            if($withCollect && $courierInternal->parent_courier_id != NULL)
                $withCollect = false;

            // For Type Internal do not call if without pickup option
            // PICKUP_MOD / 14.09.2016
//            if($withCollect && $courier != NULL && !$courier->with_pickup)
            if($withCollect && $courierInternal != NULL && $courierInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR)
                $withCollect = false;

            // DO NOT CALL UPS COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
            if($withCollect && $courierInternal->courier->user != NULL && $courierInternal->courier->user->getUpsCollectBlock())
                $withCollect = false;
        }

        $model->withCollect = $withCollect;

        $senderName = $from->name;
        $senderCompany = $from->company;

        if($uniqid == GLOBAL_BROKERS::OPERATOR_UNIQID_DPDPL_SP)
        {
            if($courierInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_NONE)
                $from = $courierInternal->courier->senderAddressData;

            $senderName = $from->name;
            $senderCompany = $from->company;
        }
        else if($courierInternal->courier->senderAddressData->country_id == CountryList::COUNTRY_PL && !$withCollect)
        {
            $senderName = $courierInternal->courier->senderAddressData->name;
            $senderCompany = $courierInternal->courier->senderAddressData->company;
        }

        // sender data will be overwritten anyway except for name...
        $model->sender_name = $senderName;
        $model->sender_company = $senderCompany;
        $model->sender_zip_code = preg_replace('/\D/', '', $from->zip_code);
        $model->sender_city = $from->city;
        $model->sender_address = $from->address_line_1.' '.$from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_mail = $from->email;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = preg_replace('/\D/', '', $to->zip_code);
        $model->receiver_city = $to->city;
        $model->receiver_address = $to->address_line_1.' '.$to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_mail = $to->email;

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->packages_number = $courierInternal->courier->packages_number;

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->reference = 'ŚP #'.$courierInternal->courier->local_id;

        $model->cod_value = 0;
        if($courierInternal->courier->cod && $courierInternal->courier->cod_value > 0) {
            $model->cod_value = $courierInternal->courier->cod_value;
            $model->cod_currency = $courierInternal->courier->cod_currency;
        }

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDPL_ROD))
            $model->rod = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDPL_DECLARED_VALUE))
            $model->declared_value = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDPL_CARRYIN))
            $model->carryIn = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDPL_GUARANTEED_930))
            $model->guaranteed_9300 = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDPL_GUARANTEED_1200))
            $model->guaranteed_1200 = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_CUD))
            $model->cud = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_IN_PERS))
            $model->inPers = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_PRIV_PERS))
            $model->privPers = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_DOX))
            $model->dox = true;

//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDPL_GUARANTEED_SUN))
//            $model->guaranteed_sun = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DPDPL_GUARANTEED_SAT))
            $model->guaranteed_sat = $courierInternal->courier->getPackageValueConverted('PLN');

        try {

            if($uniqid == GLOBAL_BROKERS::OPERATOR_UNIQID_DPDPL_SP)
                $model->setSpAccount($courierInternal->courier->user, !$withCollect);
            else {
                // SPECIAL RULE - IF NONSTANDARD DIMENSIONS, FORCE DEFAULT DPD PL ACCOUNT
                if ($courierInternal->hasAdditionById(CourierAdditionList::PACKAGE_NON_STANDARD_DOMESTIC) && $courierInternal->courier->user->getDpdplNstRedirectActive())
                    $model->forceMainAccount(!$withCollect);
                else if ($uniqid == CourierOperator::UNIQID_DPDPL_V)
                    $model->setCustomAccount($courierInternal->courier->user, !$withCollect);
                else
                    $model->forceMainAccount(!$withCollect);
            }
        }
        catch (Exception $E)
        {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => $E->getMessage(),
                ]];
                return $return;
            } else {
                return false;
            }

        }

        return $model->createShipment($returnError, $overwriteSenderData);
    }

    public static function orderForCourierU(CourierTypeU $courierU, $returnError = false)
    {
        $overwriteSenderData = false;
        $model = new self;

        $model->user_id = $courierU->courier->user_id;

        $withCollect = false;
        if($courierU->collection)
            $withCollect = true;


        if($courierU->courierUOperator->uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DPDPL_SP && !$courierU->courier->user->getDpdPlSpDisableSenderOverwrite())
            $overwriteSenderData = true;

        if($courierU->courierUOperator->uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DPDPL_SP) // @13.03.2019 - block pickups for DPD PL SP
            $withCollect = false;

        // verify collection
        if($withCollect)
        {
            // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
            if($withCollect && $courierU->parent_courier_id != NULL)
                $withCollect = false;
        }

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->withCollect = $withCollect;

        // sender data will be overwritten if without collect anyway except for name...
        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = preg_replace('/\D/', '', $from->zip_code);
        $model->sender_city = $from->city;
        $model->sender_address = $from->address_line_1.' '.$from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_mail = $from->email;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = preg_replace('/\D/', '', $to->zip_code);
        $model->receiver_city = $to->city;
        $model->receiver_address = $to->address_line_1.' '.$to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_mail = $to->email;

        $model->package_weight = $courierU->courier->package_weight;
        $model->packages_number = $courierU->courier->packages_number;

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->reference = 'ŚP #'.$courierU->courier->local_id;

        $model->cod_value = 0;
        if($courierU->courier->cod && $courierU->courier->cod_value > 0) {
            $model->cod_value = $courierU->courier->cod_value;
            $model->cod_currency = $courierU->courier->cod_currency;
        }

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDPL_CARRYIN))
            $model->carryIn = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDPL_DECLARED_VALUE))
            $model->declared_value = $courierU->courier->getPackageValueConverted('PLN');

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDPL_GUARANTEED_930))
            $model->guaranteed_9300 = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDPL_GUARANTEED_1200))
            $model->guaranteed_1200 = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDPL_GUARANTEED_SAT))
            $model->guaranteed_sat = true;

//        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDPL_GUARANTEED_SUN))
//            $model->guaranteed_sun = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DPDPL_ROD))
            $model->rod = true;


        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_CUD))
            $model->cud = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_IN_PERS))
            $model->inPers = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_PRIV_PERS))
            $model->privPers = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_DOX))
            $model->dox = true;

        try {

            if($courierU->courierUOperator->uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DPDPL_SP)
                $model->setSpAccount($courierU->courier->user, !$withCollect);
            else {

                if ($courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DPDPL_V)
                    $model->setCustomAccount($courierU->courier->user, !$withCollect);
                else
                    $model->forceMainAccount(!$withCollect);
            }
        } catch (Exception $E) {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => $E->getMessage(),
                ]];
                return $return;
            } else {
                return false;
            }

        }

        return $model->createShipment($returnError, $overwriteSenderData);
    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }


    public function createShipment($returnError = false, $overwriteSenderData = false)
    {
        $data = new stdClass();
        $data->authDataV1 = new stdClass();
        $data->authDataV1->login = $this->__username;
        $data->authDataV1->password = $this->__password;
        $data->authDataV1->masterFid = $this->__masterfid;
        $data->authDataV1->channel = 'client';

        $data->langCode = 'PL';

        $data->openUMLV1 = new stdClass();
        $data->openUMLV1->packages = new stdClass();


        $parcels = '';
        for($i = 0; $i < $this->packages_number; $i++) {
            $parcels .= '<parcels>
<content>' . self::_cdata($this->package_content) . '</content>
<sizeX>' . ($this->package_size_d) . '</sizeX>
<sizeY>' . ($this->package_size_l) . '</sizeY>
<sizeZ>' . ($this->package_size_w) . '</sizeZ>
<weight>' . ($this->package_weight) . '</weight>
</parcels>';
        }

        $data->openUMLV1->packages->parcels = new \SoapVar($parcels, XSD_ANYXML);

        $data->openUMLV1->packages->receiver = new stdClass();

        $data->openUMLV1->packages->receiver->address = $this->receiver_address;
        $data->openUMLV1->packages->receiver->city = $this->receiver_city;
        $data->openUMLV1->packages->receiver->company = $this->receiver_company;
        $data->openUMLV1->packages->receiver->countryCode = strtoupper($this->receiver_country);
        $data->openUMLV1->packages->receiver->email = $this->receiver_mail;
//        $data->openUMLV1->packages->receiver->fid = $this->__fid;
        $data->openUMLV1->packages->receiver->name = $this->receiver_name;
        $data->openUMLV1->packages->receiver->phone = $this->receiver_tel;
        $data->openUMLV1->packages->receiver->postalCode = $this->receiver_zip_code;

        $data->openUMLV1->packages->ref1 = $this->reference;
        $data->openUMLV1->packages->payerType = 'SENDER';

        $data->openUMLV1->packages->sender = new stdClass();

        $data->openUMLV1->packages->sender->fid = $this->__fid;

        if($overwriteSenderData)
        {
            $data->openUMLV1->packages->sender->address = 'Grodkowska 40';
            $data->openUMLV1->packages->sender->city = 'Nysa';
            $data->openUMLV1->packages->sender->company = 'Świat Przesyłek';
            $data->openUMLV1->packages->sender->countryCode = 'PL';
            $data->openUMLV1->packages->sender->email = 'INFO@SWIATPRZESYLEK.PL';
            $data->openUMLV1->packages->sender->name = '';
            $data->openUMLV1->packages->sender->phone = '221221218';
            $data->openUMLV1->packages->sender->postalCode = '48300';

        } else {

            $data->openUMLV1->packages->sender->address = $this->sender_address;
            $data->openUMLV1->packages->sender->city = $this->sender_city;
            $data->openUMLV1->packages->sender->company = $this->sender_company;
            $data->openUMLV1->packages->sender->countryCode = strtoupper($this->sender_country);
            $data->openUMLV1->packages->sender->email = $this->sender_mail;
            $data->openUMLV1->packages->sender->name = $this->sender_name;
            $data->openUMLV1->packages->sender->phone = $this->sender_tel;
            $data->openUMLV1->packages->sender->postalCode = $this->sender_zip_code;

        }

        $sessionType = (strtoupper($data->openUMLV1->packages->sender->countryCode) == strtoupper($data->openUMLV1->packages->receiver->countryCode) && strtoupper($data->openUMLV1->packages->sender->countryCode) == 'PL') ? 'DOMESTIC' : 'INTERNATIONAL';


        $services = '';

        if($this->guaranteed_9300) // Guaranted 09:30
        {
            $services .= '<guarantee><type>TIME0930</type></guarantee>';
        }

        if($this->guaranteed_1200) // Guaranted 12:00
        {
            $services .= '<guarantee><type>TIME1200</type></guarantee>';
        }

        if($this->guaranteed_sat) // Guaranted SATURDAY
        {
            $services .= '<guarantee><type>SATURDAY</type></guarantee>';
        }

//        if($this->guaranteed_sun) // Guaranted SUNDAY/HOLIDAYS
//        {
//            $services .= '<guarantee><type>SUNHOL</type></guarantee>';
//        }

        if($this->carryIn) // Carry In
        {
            $services .= '<carryIn/>';
        }


        if($this->rod) // ROD
        {
            $services .= '<rod/>';
        }

        if($this->cod_value) // COD
        {
            $services .= '<cod><amount>'.$this->cod_value.'</amount><currency>'.$this->cod_currency.'</currency></cod>';
        }

        if($this->declared_value) // DECLARED VALUE
        {
            $services .= '<declaredValue><amount>'.$this->declared_value.'</amount><currency>PLN</currency></declaredValue>';
        }

        if($this->cud)
        {
            $services .= '<cud/>';
        }

        if($this->inPers)
        {
            $services .= '<inPers/>';
        }

        if($this->privPers)
        {
            $services .= '<privPers/>';
        }

        if($this->dox)
        {
            $services .= '<dox/>';
        }

        if($services != '')
            $data->openUMLV1->packages->services = new \SoapVar('<services>'.$services.'</services>', XSD_ANYXML);

        $data->pkgNumsGenerationPolicyV1 = 'ALL_OR_NOTHING';

        $resp = $this->_soapOperation('generatePackagesNumbersV2', $data);

        if(!$resp->success)
        {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => $resp->error,
                ]];
                return $return;
            } else {
                return false;
            }
        }

        if($resp->data->return->Status != 'OK')
        {
            if(is_array($resp->data->return->Packages->Package->ValidationDetails->ValidationInfo) && $resp->data->return->Packages->Package->ValidationDetails->ValidationInfo[0])
                $error = $resp->data->return->Packages->Package->ValidationDetails->ValidationInfo[0];
            else if($resp->data->return->Packages->Package->ValidationDetails->ValidationInfo)
                $error = $resp->data->return->Packages->Package->ValidationDetails->ValidationInfo;
            else
                $error = $resp->data->return->Status;


            if ($returnError == true) {
                $return = [0 => [
                    'error' => print_r($error,1),
                ]];
                return $return;
            } else {
                return false;
            }
        }

        $sessionId = $resp->data->return->SessionId;
//        $packageNo = $resp->data->return->Packages->Package->Parcels->Parcel->Waybill;

        $temp = $resp->data->return->Packages->Package->Parcels->Parcel;
        if(!is_array($temp))
            $temp = [ $temp ];

        $packagesNo = [];

        foreach($temp AS $temp2)
            $packagesNo[] = $temp2->Waybill;

        $data = new stdClass();
        $data->authDataV1 = new stdClass();
        $data->authDataV1->login = $this->__username;
        $data->authDataV1->password = $this->__password;
        $data->authDataV1->masterFid = $this->__masterfid;
        $data->authDataV1->channel = 'client';

        $data->langCode = 'PL';

        $data->dpdServicesParamsV1 = new stdClass();
        $data->dpdServicesParamsV1->policy = 'IGNORE_ERRORS';
        $data->dpdServicesParamsV1->session = new stdClass();
        $data->dpdServicesParamsV1->session->sessionId = $sessionId;
        $data->dpdServicesParamsV1->session->sessionType = $sessionType;
//        $data->dpdServicesParamsV1->DocumentId = new stdClass();
//        $data->dpdServicesParamsV1->PickupAddress = new stdClass();



//        $data->dpdServicesParamsV1->PickupAddress = '';
//        $data->dpdServicesParamsV1->DocumentId = '';
//        $data->dpdServicesParamsV1 = $packageNo;

        $data->outputDocFormatV1 = 'PDF';
        $data->outputDocPageFormatV1 = 'LBL_PRINTER';
        $data->outputLabelTypeV2 = 'BIC3_EXTENDED1';

        $resp = $this->_soapOperation('generateSpedLabelsV2', $data);

        if(!$resp->success)
        {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => $resp->error,
                ]];
                return $return;
            } else {
                return false;
            }
        }

        $label = $resp->data->return->documentData;
        if($label == '')
        {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => $resp->data->return->session->statusInfo->description,
                ]];
                return $return;
            } else {
                return false;
            }
        }

        $data = new stdClass();
        $data->authDataV1 = new stdClass();
        $data->authDataV1->login = $this->__username;
        $data->authDataV1->password = $this->__password;
        $data->authDataV1->masterFid = $this->__masterfid;
        $data->authDataV1->channel = 'client';

        $data->langCode = 'PL';

        $data->dpdServicesParamsV1 = new stdClass();
        $data->dpdServicesParamsV1->pickupAddress = new stdClass();
        $data->dpdServicesParamsV1->pickupAddress->fid = $this->__fid;
        $data->dpdServicesParamsV1->policy = 'IGNORE_ERRORS';
        $data->dpdServicesParamsV1->session = new stdClass();
        $data->dpdServicesParamsV1->session->sessionId = $sessionId;
        $data->dpdServicesParamsV1->session->sessionType = $sessionType;
        $data->outputDocFormatV1 = 'PDF';
        $data->outputDocPageFormatV1 = 'A4';
        $data->outputLabelTypeV2 = 'BIC';

        $resp = $this->_soapOperation('generateProtocolV1', $data);

        if(!$resp->success)
        {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => $resp->error,
                ]];
                return $return;
            } else {
                return false;
            }
        }

        $doc = $resp->data->return->documentData;

        $collectId = false;
        if($this->withCollect) {

            $data = new stdClass();
            $data->authDataV1 = new stdClass();
            $data->authDataV1->login = $this->__username;
            $data->authDataV1->password = $this->__password;
            $data->authDataV1->masterFid = $this->__masterfid;
            $data->authDataV1->channel = 'client';

            $data->langCode = 'PL';

            $data->dpdPickupParamsV3 = new stdClass();
            $data->dpdPickupParamsV3->operationType = 'INSERT';
            $data->dpdPickupParamsV3->orderType = $sessionType;
            $data->dpdPickupParamsV3->pickupDate = date('Y-m-d');
            $data->dpdPickupParamsV3->pickupTimeFrom = '01:00';
            $data->dpdPickupParamsV3->pickupTimeTo = '19:00';
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails = new stdClass();

            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupCustomer = new stdClass();
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupCustomer->customerFullName = $this->sender_name ? $this->sender_name : $this->sender_company;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupCustomer->customerName = $this->sender_company ? $this->sender_company : $this->sender_name;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupCustomer->customerPhone = $this->sender_tel;

            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams = new stdClass();
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->dox = false;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->doxCount = 0;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->pallet = false;

            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->palletsCount = false;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->palletsWeight = false;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->parcelMaxDepth = $this->package_size_d;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->parcelMaxHeight = $this->package_size_l;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->parcelMaxWeight = $this->package_weight;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->parcelMaxWidth = $this->package_size_w;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->parcelsCount = $this->packages_number;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->parcelsWeight = $this->package_weight * $this->packages_number;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->packagesParams->standardParcel = true;


            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupPayer = new stdClass();
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupPayer->payerCostCenter = '';
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupPayer->payerName = '';
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupPayer->payerNumber = $this->__fid;

            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupSender = new stdClass();
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupSender->senderAddress = $this->sender_address;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupSender->senderCity = $this->sender_city;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupSender->senderFullName = $this->sender_name ? $this->sender_name : $this->sender_company;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupSender->senderName = $this->sender_company ? $this->sender_company : $this->sender_name;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupSender->senderPhone = $this->sender_tel;
            $data->dpdPickupParamsV3->pickupCallSimplifiedDetails->pickupSender->senderPostalCode = $this->sender_zip_code;

            $data->dpdPickupParamsV3->waybillsReady = true;

            $resp = $this->_soapOperation('packagesPickupCallV3', $data);

            if($resp->data->return->statusInfo->status == 'OK')
            {
                $collectId = $resp->data->return->orderNumber;
            }
            else
            {
                if($resp->data->return->statusInfo->errorDetails->code == 'BOK_WS_INCORRECT_PICKUP_TIME')
                {
                    $range = $resp->data->return->statusInfo->errorDetails->description;
                    preg_match('/"(.*)"/', $range, $matches);

                    $matches = $matches[1];
                    $matches = str_replace(['[',']'],'', $matches);
                    $matches = explode('|', $matches);

                    $from = $matches[0];
                    $to = $matches[1];

                    $data->dpdPickupParamsV3->pickupTimeFrom = $from;
                    $data->dpdPickupParamsV3->pickupTimeTo = $to;

                    $resp = $this->_soapOperation('packagesPickupCallV3', $data);

                    if($resp->data->return->statusInfo->status == 'OK')
                    {
                        $collectId = $resp->data->return->orderNumber;
                    } else {
                        if($resp->data->return->statusInfo->errorDetails->code == 'BOK_WS_INCORRECT_PICKUP_TIME')
                        {
                            $nextDay = S_Useful::workDaysNextDate(date('Y-m-d'),1, $withSaturday = false);
                            $data->dpdPickupParamsV3->pickupDate = $nextDay;
                            $data->dpdPickupParamsV3->pickupTimeFrom = '01:00';
                            $data->dpdPickupParamsV3->pickupTimeTo = '19:00';

                            $resp = $this->_soapOperation('packagesPickupCallV3', $data);

                            if($resp->data->return->statusInfo->status == 'OK')
                            {
                                $collectId = $resp->data->return->orderNumber;
                            }
                        }
                    }
                }
            }

        }


        $return = [];

        $basePath = Yii::app()->basePath . '/../';
        $dir = 'uplabels/';
        $temp = $dir . 'temp/';

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        @file_put_contents($basePath . $temp . $sessionId . '.pdf', $label);
        for ($i = 0; $i < $this->packages_number; $i++) {
            $im->readImage($basePath . $temp . $sessionId . '.pdf['.$i.']');
            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            if($overwriteSenderData)
            {
                $draw = new \ImagickDraw();
                $draw->setStrokeColor('transparent');
                $draw->setStrokeOpacity(0);
                $draw->setFillColor('white');
                $draw->rectangle(735, 122, 935, 630);
                $draw->setFillColor('black');
                $draw->rotate(90);
                $draw->setFontSize(6);
                $draw->annotation(132,-902, $this->sender_company);
                $draw->annotation(132,-862, $this->sender_name);
                $draw->annotation(132,-822, $this->sender_address);
                $draw->annotation(132,-782,$this->sender_country.'-'.$this->sender_zip_code.' '.$this->sender_city);
                $draw->annotation(132,-742,'Tel.: '.$this->sender_tel);

                $im->drawImage($draw);
            }

            $im->trimImage(0);
            $im->stripImage();

            $return[] = [
                'status' => true,
                'label' => $im->getImageBlob(),
                'ack' => $doc,
                'ack_source' => CourierExtExtAckCards::SOURCE_DPDPL,
                'ack_type' => CourierExtExtAckCards::TYPE_ACK,
                'track_id' => $packagesNo[$i],
                'collect_id' => $collectId,
                '_dpdpl_numkat' => $this->__fid,
            ];
        }
        @unlink($basePath . $temp . $sessionId . '.pdf');

        return $return;
    }

    public static function track($number)
    {

        $model = new self;

        $data = new stdClass();
        $data->authDataV1 = new stdClass();
        $data->authDataV1->login = $model->__username;
        $data->authDataV1->password = $model->__password;
        $data->authDataV1->channel = 'client';

        $data->langCode = 'PL';

//        $data->getEventsForWaybillV1  = new stdClass();
        $data->waybill = $number;
        $data->eventsSelectType = 'ALL';

        $resp = $model->_soapOperation('getEventsForWaybillV1', $data, 'https://dpdinfoservices.dpd.com.pl/DPDInfoServicesObjEventsService/DPDInfoServicesObjEvents?wsdl');

        if($resp->success)
        {
            $statuses = [];
            if($resp->data->return->eventsList)
                foreach($resp->data->return->eventsList AS $item)
                {
                    $date = $item->eventTime;
                    $date = substr($date,0,19);
                    $date = str_replace('T', ' ', $date);

                    $statuses[] = [
                        'name' => $item->description,
                        'date' => $date,
                        'location' => $item->depotName ? $item->depotName.', '.$item->country : NULL,
                    ];
                }

            return $statuses;
        }

        return false;
    }

    public static function getProtocol()
    {

    }


}