<?php

class HttpFilter extends CFilter
{
    public $bypass = true;

    protected function preFilter($filterChain)
    {
        // after 17.01.2017 always HTTPS
//        if ( Yii::app()->getRequest()->isSecureConnection && (!$this->bypass))
//        {
//            $url = 'http://' .
//                Yii::app()->getRequest()->serverName .
//                Yii::app()->getRequest()->requestUri;
//            Yii::app()->request->redirect($url);
//            return false;
//        }
        return true;
    }
}