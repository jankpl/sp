<?php
/* @var $model Courier */
/* @var $this CourierController */
/* @var $modelCourierOoeAdditionList CourierOoeAdditionList[] */
/* @var $courierAddition CourierOoeAddition[] */
/* @var $form CActiveForm */
/* @var $specialOption string */
?>
<div class="form">
    <table class="detail-view" style="width: 100%;">
        <tr class="odd">
            <th style="width: 50px; text-align: center;"><?php echo Yii::t('courier','Wybierz');?></th>
            <th style="width: 60%; text-align: left;"><?php echo Yii::t('courier','Dodatek');?></th>
        </tr>
        <?php
        $bg = '';
        if(is_array($modelCourierOoeAdditionList))
            foreach($modelCourierOoeAdditionList AS $item):
                if($bg == 'even')
                    $bg = 'odd';
                else
                    $bg = 'even';

                ?>
                <tr class="addition_item <?= $bg;?>">
                    <td class="with-checkbox text-center">
                        <?php echo CHtml::checkBox('CourierAddition[]', in_array($item->id,$courierAddition), array('value' => $item->id, 'data-courier-addition' => 'true', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak'))); ?>
                    </td>
                    <td>
                        <label style="font-weight: normal; text-align: left; cursor: pointer; width: 100%;"><h4><?php echo $item->courierOoeAdditionListTr->title;?></h4>
                            <p><?php echo $item->courierOoeAdditionListTr->description; ?></p></label>
                    </td>
                </tr>
                <?php
            endforeach;
        ?>
    </table>
    <?php //echo $specialOption; ?>
</div><!-- form -->