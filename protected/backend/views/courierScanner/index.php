<?php
/* @var $step integer */
/* @var $model CourierScannerForm */
/* @var $form CActiveForm */
/* @var $automatic boolean */
/* @var $statusList CourierStat[] */
?>

<?php
if($step == 1):
    ?>
    <h2>Preconfiguration</h2>
    <div class="alert alert-info">Select status and optionally location for assigning statuses.</div>
    <div class="form" style="position: relative;">
        <div class="ajax-preloader"><div></div></div>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'=>'scanner-form',
            'enableClientValidation' => true,
        )); ?>

        <?= $form->errorSummary($model); ?>

        <table class="table table-striped" style="margin: 0 auto; width: 500px;">
            <thead>
            <tr style="font-weight: bold; text-align: center;">
                <th>Status</th>
                <th>Location <span style="font-style: italic;">(optional)</span></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?= $form->dropDownList($model, 'stat_id', CHtml::listData($statusList, 'id', 'statTrEn.short_text'), ['prompt' => '-']);?></td>
                <td><?= $form->textField($model, 'location'); ?></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;"><?= CHtml::submitButton('Next', ['class' => 'btn btn-primary btn-large']);?></td>
            </tr>
            </tbody>
        </table>
        <?php $this->endWidget(); ?>
    </div>
    <?php
elseif($step == 2):
    ?>

    <h2>Status assignment</h2>

    <div class="form" style="position: relative;">
        <div class="ajax-preloader"><div></div></div>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'=>'scanner-form',
            'enableClientValidation' => true,
        )); ?>

        <h3>Configuration</h3>
        <div class="alert alert-info">Ready for assigning the following status:</div>

        <table class="table table-striped" style="margin: 0 auto; width: 700px;">
            <tr>
                <th style="width: 100px;">Status ID</th>
                <td><?= $model->stat_id; ?></td>
            </tr>
            <tr>
                <th>Status</th>
                <td><?= $model->getStatusName(); ?></td>
            </tr>
            <tr>
                <th>Location</th>
                <td><?= $model->location; ?></td>
            </tr>
        </table>
        <br/>

        <h3>Provide Package Id</h3>

        <?= $form->errorSummary($model); ?>

        <?php
        foreach(Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
        }
        ?>

        <table class="table table-striped" style="margin: 0 auto; width: 500px;">
            <tr>
                <td>Automatic on paste:</td>
                <td style="text-align: center;"><?= CHtml::checkBox('automatic', $automatic, ['id' => 'automatic-checkbox']);?></td>
            </tr>
        </table>
        <br/>
        <br/>
        <table class="table table-striped" style="margin: 0 auto; width: 500px;">
            <tr>
                <td style="text-align: center;"><?= $form->textArea($model, 'package_ids', ['id' => 'package-id', 'style' => 'padding: 15px 5px; text-align: center; width: 400px; font-size: 1.1em; font-weight: bold; height: 300px;', 'placeholder' => 'Package Id(s) (local or remote). Every ID must be in new line.']);?></td>
            </tr>
            <tr>
                <td style="text-align: center;"><?= CHtml::submitButton('Set status', ['class' => 'btn btn-large btn-primary']);?></td>
            </tr>
        </table>
        <?php $this->endWidget(); ?>
    </div>
    <div class="alert alert-warning" id="automatic-warning" style="display: none;">Status will be automatically assigned after pasting data (ctrl + v) into field below!</div>
    <?php
endif;
?>

<br/><br/>
<div style="text-align: center;">
    <a href="<?= Yii::app()->createUrl('/courierScanner/index');?>" class="btn btn-mini">Restart</a>
</div>
<?php
Yii::app()->clientScript->registerCoreScript('jquery');
?>

<script>
    var $automaticWarning = $('#automatic-warning');
    var $automaticCheckbox = $('#automatic-checkbox');
    var $packageId = $("#package-id");
    var $ajaxPreloader = $(".ajax-preloader");
    var $form = $("#scanner-form");

    function warningNotificator()
    {
        if($automaticCheckbox.is(':checked'))
            $automaticWarning.show();
        else
            $automaticWarning.hide();
    }

    function automaticSubmit()
    {

        $("#local_id").bind("input paste", function (e) {

            $(this).val(e.originalEvent.clipboardData.getData("text/plain"));
            e.preventDefault();
            $("#outside_id").focus();

        });

        $packageId.on("input paste", function (e) {

            $(this).val(e.originalEvent.clipboardData.getData("text/plain"));
            e.preventDefault();

            if($automaticCheckbox.is(':checked'))
                $(this).closest('form').submit();

        });
    }

    $(document).ready(function() {

        $form.on('submit', function(){
            $ajaxPreloader.show();
        });

        if($packageId.length)
        {
            $("#package-id").focus();
            automaticSubmit();
        }

        warningNotificator();

        $automaticCheckbox.on('change', warningNotificator);
    });
</script>