<?php
/* @var $model PageTr */
?>
<div id="modal-rodo" role="dialog" tabindex="-1" class="modal fade in" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="text-center">
                <?php
                if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH)
                    $logo_src = Yii::app()->baseUrl.'/theme/ruch/images/logo.png';
                else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS)
                    $logo_src = Yii::app()->baseUrl.'/theme/quriers/images/logo.png';
                else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                    $logo_src = Yii::app()->baseUrl.'/theme/cql/images/logo.png';
                else
                    switch(Yii::app()->language)
                    {
                        case 'pl':
                            $logo_src = Yii::app()->baseUrl.'/images/v2/logo.png';
                            break;
                        case 'en_gb':
                            $logo_src = Yii::app()->baseUrl.'/theme/pli/images/logo.png';
                            break;
                        default:
                            $logo_src = Yii::app()->baseUrl.'/images/layout/new/logo_en.png';
                            break;
                    }
                ?>
                <img src="<?= $logo_src;?>" id="logo"/>
                <br/>
                <hr/>
                <?= $model->text; ?>
                <hr/>
                <div class="text-center">
                    <input type="button" class="btn btn-success btn-lg" value="<?= Yii::t('rodo', 'Akceptuję');?>" id="rodoAccept"/>
                </div>
                <br/>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function($) {
        $(document).ready(function(){
            $('#modal-rodo').modal('show');
            $('#rodoAccept').on('click', function(){
                document.cookie="<?= RodoInfo::COOKIE_NAME;?>=10; expires=<?php echo (new DateTime())->add(new DateInterval('P1Y'))->format('D, j M Y,h:i:s');?> UTC; path=/";
                $('#modal-rodo').modal('hide');
            });
        });
    })(jQuery);
</script>
