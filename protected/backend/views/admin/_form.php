<?php
/* @var $model Admin */
?>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'admin-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'login'); ?>
        <?php echo $form->textField($model, 'login', array('maxlength' => 20, 'disabled' => $model->getScenario()=='update'?'disabled':'')); ?>
        <?php echo $form->error($model,'login'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'pass'); ?>
        <?php echo $form->passwordField($model, 'pass', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'pass'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'passCompare'); ?>
        <?php echo $form->passwordField($model, 'passCompare', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'passCompare'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model, 'email', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'authority'); ?>
        <?php echo $form->dropDownList($model,'authority', Admin::getAuthoritiesNames()); ?>
        <?php echo $form->error($model,'authority'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'salesman_id'); ?>
        <?php echo $form->dropDownList($model,'salesman_id', CHtml::listData(Salesman::getAllSalesmen(),'id','name'), ['prompt' => '-']); ?>
        <?php echo $form->error($model,'salesman_id'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'scanning_sp_point_id'); ?>
        <?php echo $form->dropDownList($model,'scanning_sp_point_id', CHtml::listData(SpPoints::model()->findAll(),'id','name'), ['prompt' => '-']); ?>
        <?php echo $form->error($model,'scanning_sp_point_id'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'partner_id'); ?>
        <?php echo $form->dropDownList($model,'partner_id', Partners::getParntersList(), ['prompt' => '-']); ?>
        <?php echo $form->error($model,'partner_id'); ?>
    </div><!-- row -->

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->