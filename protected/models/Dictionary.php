<?php

/**
 * This is the model class for table "dictionary".
 *
 * The followings are the available columns in table 'dictionary':
 * @property integer $id
 * @property integer $type
 * @property integer $source_id
 * @property string $source_text
 * @property integer $admin_id
 * @property string $date_entered
 * @property string $date_updated
 * @property integer $hits
 *
 * @property DictionaryTr $dictionaryTr
 * @property DictionaryTr[] $dictionaryTrs
 * @property Admin $admin
 */
class Dictionary extends CActiveRecord
{
    const TYPE_FRESHDESK = 1;
    const TYPE_OPERATOR_COURIER = 2;
    const TYPE_OPERATOR_POSTAL = 3;


    public static function label() {
        return 'Dictionary';
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'dictionary';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, source_text', 'required'),
            array('type, source_id, admin_id, hits', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type, source_id, source_text, admin_id, date_entered, date_updated, hits', 'safe', 'on'=>'search'),
        );
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),

        );
    }

    public function beforeSave()
    {
        if(Yii::app()->params['backend'] && !Yii::app()->user->isGuest)
            $this->admin_id = Yii::app()->user->id;

        return parent::beforeSave();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'dictionaryTr' => array(self::HAS_ONE, 'DictionaryTr', 'dictionary_id', 'on'=>'dictionaryTr.language_id='.Yii::app()->params['language'].' OR dictionaryTr.language_id=1', 'order' => 'FIELD(language_id,'.Yii::app()->params['language'].',1)'),
            'dictionaryTrs' => array(self::HAS_MANY, 'DictionaryTr', 'dictionary_id'),
            'admin' => array(self::BELONGS_TO, 'Admin', 'admin_id'),
        );

    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'source_id' => 'Source',
            'source_text' => 'Source Text',
            'admin_id' => 'Admin',
            'date_entered' => 'Date Entered',
            'date_updated' => 'Date Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('type',$this->type);
        $criteria->compare('hits',$this->hits);
        $criteria->compare('source_id',$this->source_id);
        $criteria->compare('source_text',$this->source_text,true);
        $criteria->compare('admin_id',$this->admin_id);
        $criteria->compare('date_entered',$this->date_entered,true);
        $criteria->compare('date_updated',$this->date_updated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Dictionary the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////


    public static function getTypeList()
    {
        return [
            self::TYPE_FRESHDESK => 'FreshDesk',
            self::TYPE_OPERATOR_COURIER => 'Operator (C)',
            self::TYPE_OPERATOR_POSTAL => 'Operator (P)'
        ];
    }

    public function getTypeName()
    {
        return isset(self::getTypeList()[$this->type]) ? self::getTypeList()[$this->type] : NULL;
    }

    public function getSourceIdName()
    {
        switch($this->type)
        {
            default:
                return NULL;
                break;
            case self::TYPE_OPERATOR_COURIER:
                Yii::app()->getModule('courier');
                return CourierLabelNew::getOperators()[$this->source_id];
                break;
            case self::TYPE_OPERATOR_POSTAL:
                Yii::app()->getModule('postal');
                return Postal::getBrokers()[$this->source_id];
                break;
        }
    }

    public static function getTranslatedText($text, $type, $source_id = NULL)
    {
        $text = trim($text);

        if($source_id !== NULL)
            $model = self::model()->cache(60)->findByAttributes(['source_id' => $source_id, 'source_text' => $text, 'type' => $type]);
        else
            $model = self::model()->cache(60)->findByAttributes(['source_text' => $text, 'type' => $type]);

        if($model === NULL)
        {
            $model = new self;
            $model->type = $type;
            $model->source_text = $text;
            $model->source_id = $source_id;
            $model->save();

        } else {

            $model->hits++;
            $model->update(['hits']);

            if($model->dictionaryTr !== NULL)
                $text = $model->dictionaryTr->target_text;
        }

        return $text;
    }
}
