function prepareSettingsModal(params)
{
    $('#custom-settings').on('change', function(){
        if($(this).val())
            $('[data-settings-modal=edit]').show();
        else
            $('[data-settings-modal=edit]').hide();
    });

    $('[data-settings-modal]').on('click', function () {

        var $modal = $('#main-modal');

        var id = '';
        if($(this).attr('data-settings-modal') == 'edit')
            id = $('#custom-settings').val();

        $.ajax({
            url: params.ajaxUrl,
            method: 'post',
            data: { id : id, type: params.type},
            dataType: 'json',
            success: function (result) {

                $modal.find('.modal-content').html(result);
                $modal.modal('show');

                $modal.on('shown.bs.modal', function () {

                    $modal.find("#sortable").sortable({
                        placeholder: "ui-state-highlight"
                    });

                    $modal.find("#sortable").disableSelection();

                    $modal.find('[data-custom-settings]').unbind('click');
                    $modal.find('[data-custom-settings]').on('click', function () {


                        if($(this).attr('data-custom-settings') == 'delete')
                        {
                            var r = confirm(params.textConfirmDelete);
                            if (r != true) {
                                return true;
                            }
                        }

                        $('#custom-order-data').val($('#sortable').sortable('serialize'));
                        var $form = $modal.find('form');

                        if($(this).attr('data-custom-settings') == 'delete')
                            $form.append($('<input>').attr('type','hidden').attr('name', 'settings[delete]').val(1));

                        $.ajax({
                            url: params.ajaxUrl,
                            dataType: 'json',
                            method: 'post',
                            data: $form.serialize(),
                            success: function (result) {
                                if(result.success) {
                                    location.reload();
                                } else {
                                    alert(params.textError);
                                    $modal.modal('hide');
                                }

                            },
                            error: function (e) {
                                alert(params.textError);
                                $modal.modal('hide');
                                console.log(e);
                            }
                        });
                    });
                });
            },
            error: function (e) {
                console.log(e);
            }
        });
    });
}