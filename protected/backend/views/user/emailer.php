<?php
/* @var $modelEmail UserSendMailGroup */
/* @var $form TbActiveForm */
?>

<h2>Send Mesages</h2>


<div class="form">

    <h3>Send Mail</h3>
    <p class="listOfReceivers">Receivers number: <strong id="number-of-receipients"><?= $modelEmail->getUserNumber();?></strong></p>
    <p class="listOfReceivers">Receivers: <?= $modelEmail->getUserList();?></p>

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'emailer-form',
        'enableClientValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'clientOptions'=>array(
            'validateOnSubmit'=>false,
            //'validateOnType' => true,
        ),
    )); ?>
    <?= $form->dropDownList($modelEmail, 'toAll', [UserSendMailGroup::TO_ALL => 'Send to ALL customers (~'.sizeof(UserSendMailGroup::getAllUsersIds()).')', UserSendMailGroup::TO_ALL_ACTIVE => 'Send to ACTIVE (>100 orders last month) customers  (~'.sizeof(UserSendMailGroup::getActiveUserIds()).')'], ['id' => 'toAll', 'style' => 'display: inline-block', 'prompt' => '(only listed above)']);?>

    <?php echo $form->errorSummary($modelEmail); ?>
    <div>
        <?php echo $form->textField($modelEmail,'title', ['placeholder' => $modelEmail->getAttributeLabel('title'), 'style' => 'width: 95%;']); ?>
    </div>

    <div>
        Text:<br/>
        <?php
        $this->widget('ext.editMe.widgets.ExtEditMe', array(
            'model'=>$modelEmail,
            'attribute'=>'text',
            'resizeMode' => 'vertical',
//                    'width' => '580',
            'baseHref' => Yii::app()->baseUrl.'/',
            'toolbar' => array(
                array(
                    'Source', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo', 'Blockquote', '-', 'Table', 'HorizontalRule','SpecialChar',
                ),
                array(
                    'Bold', 'Italic', 'Underline', 'FontSize', 'TextColor', 'Link','NumberedList', 'BulletedList', 'Outdent', 'Indent',  'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
                ),
            ),
            'advancedTabs' => false,
            'ckeConfig' => array(
                'forcePasteAsPlainText' => true,
                'allowedContent' => true,
            )
        ));
        ?>



    </div>
    <br/>
    Attachment:
    <div class="row">
        <?php  echo $form->fileField($modelEmail, 'attachment', ['id' => 'file-field']); ?>
        <?php echo $form->error($modelEmail,'attachment'); ?>
    </div>
    <div class="row">
        Include all salesmen:<br/>
        <?php  echo $form->checkbox($modelEmail, 'include_salesman', ['id' => 'include_salesman']); ?>
        <?php echo $form->error($modelEmail,'include_salesman'); ?>
    </div>
    <br/>


    <div class="text-center">

        <?php echo TbHtml::submitButton(Yii::t('app','Wyślij'), ['class' => 'btn btn-primary btn-lg']); ?>
    </div>
    <?php echo $form->hiddenField($modelEmail,'user_ids'); ?>
    <?php $this->endWidget(); ?>
</div><!-- form -->

<hr/>
<div class="text-center">
    Preview:<br/>
    <img src="<?= Yii::app()->baseUrl;?>/images/backend/mail_form.jpg"/>
</div>
<script>
    $(document).ready(function(){

        checkAll();
        $('#emailer-form').on('submit', function(){

            if ($('#file-field').get(0).files.length === 0) {
                var c = confirm("No file is selected. Are you sure you want to continue?");

                if(!c)
                    return false;
            }

            var number = $('#number-of-receipients').html();

            if($('#toAll').val() == 1)
                number = 'ALL';
            else if($('#toAll').val() == 2)
                number = 'ACTIVE';

                var text = "Confirm sending email to number of receipients: " + number;

            if($('#include_salesman').is(':checked'))
                text = text + ' + salesman';

            var c = confirm(text);
            return c;
        });

        $('#toAll').on('change', checkAll);

        function checkAll()
        {
            if($('#toAll').val())
                $('.listOfReceivers').hide();
            else
                $('.listOfReceivers').show();
        }
    })
</script>