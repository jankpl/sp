<?php
class CookiesInfo extends CWidget
{
    public function run()
    {
        $cookiesInfo = isset(Yii::app()->request->cookies['cookiesInfo']) ? Yii::app()->request->cookies['cookiesInfo']->value : '';

        if($cookiesInfo != 10)
            $this->render('cookiesInfo');
    }
}
?>