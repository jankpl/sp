    <?php //echo CHtml::hiddenField('fileTempName', $fileModel->file->tempName);?>
    <div style="width: 100%; position: relative; height: 500px; overflow-x: scroll;">
        <div style="width: 950px; position: absolute;">

            <table class="list hTop" style="width: 100%;">
                <tr>
                    <td>
                        Local Id
                    </td><td>
                        Duplicate
                    </td><td>
                        Package found
                    </td><td>
                        Current Status
                    </td><td>
                        New Status
                    </td><td>
                        Data of status
                    </td>
                </tr>

                <?php

                foreach($models AS $key => $item):
                    $this->renderPartial('importStatFromFile/_item',
                        array(
                            'split' => $split,
                            'model' => $item,
                            'i' => $key,
                        ));

                endforeach;
                ?>
            </table>

        </div>
    </div>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <div class="navigate" style="text-align: center;">

        <?php echo TbHtml::button(Yii::t('app', 'Validate this part of data'), array('name' => 'validate', 'class' => 'save-packages', 'data-mode' => F_MassStatusChangePostal::MODE_JUST_VALIDATE)); ?>

        <?php echo TbHtml::button(Yii::t('app', 'Save this part of data'), array('name' => 'save', 'size' => TbHtml::BUTTON_SIZE_LARGE, 'class' => 'save-packages', 'data-mode' => F_MassStatusChangePostal::MODE_SAVE)); ?>


        <?php echo CHtml::hiddenField('mode','', array('id' => 'mode'));?>
    </div>
</div><!-- form -->