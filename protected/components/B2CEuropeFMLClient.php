<?php

class B2CEuropeFMLClient extends GlobalOperator
{
    const URL = GLOBAL_CONFIG::B2C_CORREOS_FML_URL;
    const USER = GLOBAL_CONFIG::B2C_CORREOS_FML_USER;
    const KEY = GLOBAL_CONFIG::B2C_CORREOS_FML_KEY;

    const SHIPPING_METHOD = 'PPLUSFML';
    const SHIPPING_METHOD_COD = 'CODFML';
    const TYP = 'QI0B';

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $weight;
    public $value;
    public $content;

    public $cod = 0;
    public $cod_currency;
    public $size_l;
    public $size_d;
    public $size_w;

    public $local_id;

    public $sender_prefix = false;

    public $user_id;

//
//    public static function test()
//    {
//        $model = new self;
//
//        $model->sender_name = 'KAAB GMBH ZAPATOS.ES';
//        $model->sender_address1 = 'MALTITZ 35A';
//        $model->sender_zip_code = '02627';
//        $model->sender_city = 'WEISSENBERG';
//        $model->sender_country = 'ESP';
//        $model->sender_tel = '033523386886';
//
//
//        $model->receiver_name = 'MARTA MACIAS CAMA';
//        $model->receiver_address1 = 'Test str 1';
//        $model->receiver_zip_code = '02627';
//        $model->receiver_city = 'OLOT';
//        $model->receiver_country = 'ESP';
//        $model->receiver_mail = 'test@testtesttest.com';
//        $model->receiver_tel = 111222333;
//
//        $model->weight = 1;
//        $model->cod_currency = 'EUR';
//        $model->size_l = '10';
//        $model->size_d = '10';
//        $model->size_w = '10';
//
//        $model->cod = 10;
//
//        $model->local_id = '123123401'.rand(999,999999);
//        $model->content = 'Test item';
//
//        $model->value = 10;
//
//        return $model->_createShipment();
//    }

    protected static function _cdata($text)
    {
        return htmlspecialchars($text);
    }

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    protected static function _addressLinesConverter($addressLine1, $addressLine2, $getLineNo)
    {
        if(mb_strlen($addressLine1) <= 40 && mb_strlen($addressLine2) <= 40)
        {
            if($getLineNo == 0)
                return $addressLine1;
            else if($getLineNo == 1)
                return $addressLine2;
            else if($getLineNo == 2)
                return '';

        } else {
            $address = trim($addressLine1) . ' ' . trim($addressLine2);
            $address = trim($address);

            $newAddress = [];
            $newAddress[0] = mb_substr($address, 0, 40);
            $newAddress[1] = mb_substr($address, 40);

            return trim($newAddress[$getLineNo]);
        }
    }

//    protected function _createShipment(Courier $courier, AddressData $from, AddressData $to, $returnError = false)
    protected function _createShipment( $returnError = false)
    {


        $this->cod = 100 * doubleval($this->cod);

        $tel = $this->receiver_tel;
        $tel = ltrim($tel, '0');
        $tel = mb_substr($tel, -9);

        $item = \FluidXml\fluidxml('FinalMileLabelCreateRequest');

        $authentication = \FluidXml\fluidxml('Authentication');

        $senderAddress1 = S_Useful::removeNationalCharacters($this->sender_address1);
        $senderAddress2 = S_Useful::removeNationalCharacters($this->sender_address2);

        $receiverAddress1 = S_Useful::removeNationalCharacters($this->receiver_address1);
        $receiverAddress2 = S_Useful::removeNationalCharacters($this->receiver_address2);

        if(in_array($this->user_id, [3145])) // J-RUN
        {
            $senderAddress1 = S_Useful::removeNationalCharacters(self::_addressLinesConverter($this->sender_address1, $this->sender_address2, 0));
            $senderAddress2 = S_Useful::removeNationalCharacters(self::_addressLinesConverter($this->sender_address1, $this->sender_address2, 1));

            $receiverAddress1 = S_Useful::removeNationalCharacters(self::_addressLinesConverter($this->receiver_address1, $this->receiver_address2, 0));
            $receiverAddress2 = S_Useful::removeNationalCharacters(self::_addressLinesConverter($this->receiver_address1, $this->receiver_address2, 1));
        }

        $authentication
            ->add('AuthenticationKey', self::KEY)
            ->add('LayoutType', 'L01')
            ->add('LayoutVersion', '1.0')
            ->add('CustomerReference', 'Test1234');

        $shipperAddress = \FluidXml\fluidxml('ShipperAddress');
        $shipperAddress
            ->add('ShipperAddressType', 'SHA')
            ->add('CompanyName', $this->sender_name ? self::_cdata(S_Useful::removeNationalCharacters($this->sender_name)) : self::_cdata(S_Useful::removeNationalCharacters($this->sender_company)))
            ->add('AddressLine1', self::_cdata($senderAddress1))
            ->add('AddressLine2', self::_cdata($senderAddress2))
            ->add('HouseNumber', '')
            ->add('HouseNumberExtension', '')
            ->add('CityOrTown', self::_cdata(S_Useful::removeNationalCharacters($this->sender_city)))
            ->add('StateOrProvince', '')
            ->add('PostalCode', $this->sender_zip_code)
            ->add('CountryCode', ($this->sender_country));

        $shipperAddressReturn = \FluidXml\fluidxml('ShipperAddress');
        $shipperAddressReturn
            ->add('ShipperAddressType', 'SHR')
            ->add('CompanyName', $this->sender_name ? self::_cdata(S_Useful::removeNationalCharacters($this->sender_name)) : self::_cdata(S_Useful::removeNationalCharacters($this->sender_company)))
            ->add('AddressLine1', self::_cdata($senderAddress1))
            ->add('AddressLine2', self::_cdata($senderAddress2))
            ->add('HouseNumber', '')
            ->add('HouseNumberExtension', '')
            ->add('CityOrTown', self::_cdata(S_Useful::removeNationalCharacters($this->sender_city)))
            ->add('StateOrProvince', '')
            ->add('PostalCode', $this->sender_zip_code)
            ->add('CountryCode', ($this->sender_country));

        $delivery = \FluidXml\fluidxml('Delivery');
        $delivery
            ->add('OrderNumber', $this->local_id)
            ->add('OrderReference', $this->local_id)
            ->add('OrderContent', self::_cdata(mb_substr($this->content,0, 40)))
            ->add('ShippingMethod', $this->cod > 0 ? self::SHIPPING_METHOD_COD : self::SHIPPING_METHOD)
            ->add('Currency', $this->cod_currency ? $this->cod_currency : 'EUR')
            ->add('LabelType', 1)
            ->add('MeasurementSystem', 1)
            ->add('CustomsClearance', 1)
            ->add('CustomsInvoice', 0)
            ->add('PurchaseDate', date('Y-m-d'));

        $receiverCompanyName = '';
        if($this->receiver_name != '' && $this->receiver_name != $this->receiver_company)
            $receiverCompanyName = $this->receiver_company;

        $deliveryAddress = \FluidXml\fluidxml('DeliveryAddress');
        $deliveryAddress
            ->add('DeliveryAddressType', 'DL1')
            ->add('ConsigneeName', $this->receiver_name ? self::_cdata(S_Useful::removeNationalCharacters($this->receiver_name)) : self::_cdata(S_Useful::removeNationalCharacters($this->receiver_company)))
            ->add('CompanyName',self::_cdata(S_Useful::removeNationalCharacters($receiverCompanyName)))
            ->add('AddressLine1', self::_cdata($receiverAddress1))
            ->add('AddressLine2', self::_cdata($receiverAddress2))
            ->add('HouseNumber', '')
            ->add('HouseNumberExtension', '')
            ->add('CityOrTown', self::_cdata(S_Useful::removeNationalCharacters($this->receiver_city)))
            ->add('StateOrProvince', '')
            ->add('PostalCode', $this->receiver_zip_code)
            ->add('CountryCode', ($this->receiver_country));


        $deliveryContact = \FluidXml\fluidxml('DeliveryContact');
        $deliveryContact
            ->add('PhoneNumber', $tel)
            ->add('SMSNumber', $tel)
            ->add('EmailAddress', $this->receiver_mail)
            ->add('PersonalNumber', $tel);

        $deliveryPackage = \FluidXml\fluidxml('DeliveryPackage');

        if($this->cod)
            $deliveryPackage
                ->add('PackageNumber', 1)
                ->add('PackageReference', $this->local_id)
                ->add('PackageWeight', $this->weight * 1000)
                ->add('DimensionHeight', $this->size_d)
                ->add('DimensionWidth', $this->size_w)
                ->add('DimensionLength', $this->size_l)
                ->add('CODAmount', $this->cod);
        else
            $deliveryPackage
                ->add('PackageNumber', 1)
                ->add('PackageReference', $this->local_id)
                ->add('PackageWeight', $this->weight * 1000)
                ->add('DimensionHeight', $this->size_d)
                ->add('DimensionWidth', $this->size_w)
                ->add('DimensionLength', $this->size_l);

        $deliveryContent = \FluidXml\fluidxml('DeliveryContent');
        $deliveryContent
            ->add('PackageNumber', 1)
            ->add('SKUCode', $this->local_id)
            ->add('SKUDescription',  self::_cdata(mb_substr($this->content,0, 40)))
            ->add('Quantity', 1)
            ->add('Price', ceil($this->value))
            ->add('CountryCodeOrigin', ($this->sender_country));

        $validation = \FluidXml\fluidxml('Validation');
        $validation
            ->add('TotalDeliveryPackages', 1)
//            ->add('MailAddressConfirmation', 'jankopec@swiatprzesylek.pl')
//            ->add('MailAddressError', 'jankopec@swiatprzesylek.pl')
            ->add('TimeZone', 1)
        ;

        $item->add($authentication);
        $item->add($shipperAddress);
        $item->add($shipperAddressReturn);
        $item->add($delivery);
        $item->add($deliveryAddress);
        $item->add($deliveryContact);
        $item->add($deliveryPackage);
        $item->add($deliveryContent);
        $item->add($validation);

        $xml = $item->xml();

        $resp = $this->_curlCall($xml);

        if ($resp->success == true) {
            $return = [];

            $ttNumber = (string) $resp->data->DeliveryPackage->TrackingNumber;
            $label = (string) $resp->data->DeliveryPackage->DeliveryPackageData->DataBlob;
            $label = base64_decode($label);

            $im = ImagickMine::newInstance();
            $im->setResolution(300,300);

            $im->readImageBlob($label);
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            if($this->sender_prefix)
                LabelAnnotation::createAnnotation($this->sender_prefix, $im, 95, 90, false, false, 6);

            $im->rotateImage(new ImagickPixel(), 90);
//            $im->trimImage(0);

            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);

            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }

    }

    protected function _curlCall($data)
    {

        $throttle = new Stiphle\Throttle\LeakyBucket;


        if(Yii::app()->params['console'])
            $storage = New Stiphle\Storage\Process();
        else
            $storage = New Stiphle\Storage\Apcu();

        $throttle->setStorage($storage);

        $throttled = $throttle->throttle('B2C_FML_ORDER', 15, 1000 * 60);
        if($throttled)
            MyDump::dump('b2c_fml_throttled.txt', print_r($throttled,1));

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

//        $dataOryginal = $data;

        $headers = [];
        $headers[] = 'Content-Type: text/xml';

        $curl = curl_init(self::URL);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt_array($curl, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        );
        $resp = curl_exec($curl);

        MyDump::dump('b2ceurope_fml.txt', print_r($data,1));
        MyDump::dump('b2ceurope_fml.txt', print_r($resp,1));

        $xml = @simplexml_load_string($resp);

        if($xml->Result->ResultDescription == 'SUCCESS')
        {
            $return->success = true;
            $return->data = $xml;
        }
        else
        {
            if ($xml == '')
            {
                $error = curl_error($curl);
                if ($error == '')
                    $error = 'Unknown error';

                $return->error = true;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
            else
            {
                $error = $xml->ErrorData->ErrorDescription;
                if ($error == '')
                    $error = 'Unknown error';


                $return->error = true;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
        }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;

        $model->user_id = $courierInternal->courier->user_id;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->getCodeIso3();

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->getCodeIso3();
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->weight = $courierInternal->courier->getWeight(true);
        $model->content = $courierInternal->courier->package_content;

        $model->size_l = $courierInternal->courier->package_size_l;
        $model->size_d = $courierInternal->courier->package_size_d;
        $model->size_w = $courierInternal->courier->package_size_w;
        $model->cod = $courierInternal->courier->cod_value;
        $model->cod_currency = $courierInternal->courier->cod_currency;
        $model->local_id = $courierInternal->courier->local_id;

        $model->value = $courierInternal->courier->getPackageValueConverted($model->cod_currency ? $model->cod_currency : 'EUR');

        if(in_array($courierInternal->courier->user_id, [947, 948, 2036, 2037])) {
            $model->sender_prefix = 'ZAPATOS.ES';

            // special rule:
            if($model->weight <= 5 && $model->weight > 1)
                $model->weight = 1;
        }

        return $model->_createShipment($courierInternal->courier, $from, $to, $returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;

        $model->user_id = $courierU->courier->user_id;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->getCodeIso3();

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->getCodeIso3();
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->weight = $courierU->courier->getWeight(true);
        $model->content = $courierU->courier->package_content;

        $model->size_l = $courierU->courier->package_size_l;
        $model->size_d = $courierU->courier->package_size_d;
        $model->size_w = $courierU->courier->package_size_w;
        $model->cod = $courierU->courier->cod_value;
        $model->cod_currency = $courierU->courier->cod_currency;
        $model->local_id = $courierU->courier->local_id;

        $model->value = $courierU->courier->getPackageValueConverted($model->cod_currency ? $model->cod_currency : 'EUR');

        if(in_array($courierU->courier->user_id, [947, 948, 2036, 2037])) {
            $model->sender_prefix = 'ZAPATOS.ES';

            // special rule:
            if($model->weight <= 5 && $model->weight > 1)
                $model->weight = 1;
        }

        return $model->_createShipment($courierU->courier, $from, $to, $returnErrors);
    }


    public static function track($no, CourierLabelNew $courierLabelNew)
    {
//        B2CEuropeClient::updateTtData(false, GLOBAL_BROKERS::BROKER_B2C_EUROPE_FML);
        return Client17Track::getTtGlobalBrokers($no, false, false, false, 'B2C_FML');

//        return TrackingmoreClient::getTtGlobalBrokers($no, 'correos-spain');
    }

}

