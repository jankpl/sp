<h4>Generate labels</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-labels',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/generateLabelsByGridView'),
));
?>

<?php echo CHtml::dropDownList('Postal[type]','',PostalLabelPrinter::getTypeNames(), array('style' => 'width: 120px; display: inline-block;'));?>

<?php
echo TbHtml::submitButton('Generate labels', array(
    'onClick' => 'js:
    $("#postal-ids-labels").val($("#postal").selGridView("getAllSelection").toString());
    ;',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => 'postal-ids-labels')); ?>
<?php
$this->endWidget();
?>
