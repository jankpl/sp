<?php

class _CourierBufferGridView extends _CourierGridView
{
    public $_days;



    public function rules() {

        $array = array(
            array('location,
           _days','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = parent::searchCriteria();


        $criteria->select = ['DATEDIFF(CURDATE(), t.date_updated) AS _days', '*'];

        if($this->_days) {
            $criteria->having = '_days > ' . (intval($this->_days));
        }
        else
        {
            $criteria->having = '_days > 3';
            $this->_days = 3;
        }

        $criteria->addInCondition('courier_stat_id', [12653, 12724, 10022, 12734]);
        $criteria->compare('t.courier_type', Courier::TYPE_RETURN);

        return $criteria;
    }

    public function search()
    {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';

        $sort->attributes = array(
            '__user_login' => array(
                'asc' => 'user.login ASC',
                'desc' => 'user.login DESC',
            ),
            '_days' => array(
                'asc' => '_days ASC',
                'desc' => '_days DESC',
            ),
//            'daysFromStatusChange' => array(
//                'asc' => '(
//                        SELECT TIMESTAMPDIFF(
//                        DAY , date_entered,
//                        CURRENT_TIMESTAMP )
//                        FROM courier_stat_history
//                        WHERE courier_id = t.id
//                        ORDER BY date_entered DESC
//                        LIMIT 0 , 1
//                        ) ASC',
//                'desc' => '(
//                        SELECT TIMESTAMPDIFF(
//                        DAY , date_entered,
//                        CURRENT_TIMESTAMP )
//                        FROM courier_stat_history
//                        WHERE courier_id = t.id
//                        ORDER BY date_entered DESC
//                        LIMIT 0, 1
//                        ) DESC',
//            ),

            '__stat' => array(
                'asc' => 'courier_stat_id ASC',
                'desc' => 'courier_stat_id DESC',
            ),
            '*', // add all of the other columns as sortable
        );

        $criteria = $this->searchCriteria();

        // if there are no conditions, use simple count of packages to improve performance
        $totalCount = NULL;
        if ($criteria->condition == '') {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(id)')->from((new self)->tableName());
            $totalCount = $cmd->queryScalar();
        }


        return new CActiveDataProvider($this
            ->with('courierTypeInternal')
            ->with('courierTypeU')

            ->with('courierLabelNew')

            ->with(['user' => ['select' => 'login']])
            ->with('stat')
            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', 50),
                ),
            ));



    }

}