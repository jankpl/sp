<?php
/* @var $model Dictionary */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label' =>'List ' . $model->label(2), 'url'=>array('index')),
    array('label' =>'Create ' . $model->label(), 'url'=>array('create')),
    array('label' =>'View ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
);
?>

<h1>Update <?php echo GxHtml::encode($model->label());?> #<?= $model->id;?> (<?= $model->getTypeName();?>)</h1>


<?php
//$this->widget('backend.components.googleTranslate.GoogleTranslateWidget', [
//    'button' => true,
//]);
?>

<?php
//$this->widget('backend.components.googleTranslate.GoogleTranslateWidget', [
//    'formAttrNames' => 'AddressData_Order',
//    'button' => false,
//    'attrMap' => ['nip' => '_nip']
//]);
?>


<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'modelTrs' => $modelTrs,
));
?>
