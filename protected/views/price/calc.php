<?php
$this->blockBootstrap = true;


Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/custom-theme/jquery-ui-1.10.3.custom.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/ui.slider.extras.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/pricing.css');
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-ui-1.10.3.custom.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/selectToUISlider.jQuery.js');
$productType = Pricing::getProductType();
?>

<h2><?php echo $productType[$id];?></h2>
<?php
$this->Widget('ShowPageContent', array('id' => $page_id));
?>
<form id="pricing">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">

            <div id="price-calc">
                <input type="hidden" name="product" value="<?php echo $id; ?>"/>
                <input type="hidden" id="falseChange"/>
                <h3><?php echo Yii::t('price','Waga');?></h3>
                <div id="slider">
                    <select name="weight_id" class="slideMe" id="weight_id">
                        <?php $models =  Pricing::model()->findAll('product=:product',array(':product' => $id));
                        $previous = 1;
                        $firstSelected = true;
                        foreach($models AS $item)
                        {
                            echo '<option value="'.$item->id.'" '.($firstSelected?'selected="selected"':'').' label="od&nbsp;'.$previous.'g<br/>do&nbsp;'.$item->top_weight.'g">od&nbsp;'.$previous.'g do&nbsp;'.$item->top_weight.'g</option>';
                            $previous = $item->top_weight + 1;
                            $firstSelected = false;
                        }

                        ?>
                    </select>
                </div>
                <?php
                $priceOptions = PricingOptions::getOptions();

                foreach($priceOptions[$id] AS $optionGroupKey => $optionGroup)
                {
                    echo '<h3>'.$optionGroup['name'].'</h3>';

                    if(S_Useful::sizeof($optionGroup['options']) > 1)
                        echo '<div class="buttonInsideMe">';

                    $firstSelected = true;
                    foreach($optionGroup['options'] AS $key => $option)
                    {
                        if($option['hidden'])
                            continue;


                        if(!$optionGroup['forceCheckbox'] AND S_Useful::sizeof($optionGroup['options']) > 1)
                        {
                            echo '<label title="'.$option['tooltip'].'" for="radio_'.$optionGroupKey.$key.'">'.$option['name'].'</label> <input class="buttonMe" type="radio" name="'.PricingOptions::encodeName($id,$optionGroupKey).'" value="'.PricingOptions::encodeValue($id,$optionGroupKey, $key).'" id="radio_'.$optionGroupKey.$key.'" '.($firstSelected?'checked="checked"':'').'>';
                            $firstSelected = false;
                        }
                        else
                            echo '<label title="'.$option['tooltip'].'" for="checkbox_'.$optionGroupKey.$key.'">'.$option['name'].'</label> <input type="checkbox" name="'.PricingOptions::encodeName($id,$optionGroupKey).'" id="checkbox_'.$optionGroupKey.$key.'" value="'.PricingOptions::encodeValue($id,$optionGroupKey, $key).'" class="buttonMe"/>';
                    }

                    echo'<br/><br/>';
                    if(S_Useful::sizeof($optionGroup['options']) > 1)
                        echo '</div>';
                }

                ?>
                <div id="price" id="price">
                    <span></span>
                    <div><?php
                        if(Yii::app()->params['language'] == 1)
                            echo Yii::t('m_pricingOptions', '+23% VAT');?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>
<br/>
<br/>
<?php $this->widget('BackLink'); ?>
<script>
    function loadPrice()
    {
        jQuery.ajax({
            'dataType':'text',
            'url':'<?php echo Yii::app()->createUrl('price/ajaxCalculatePrice');?>',
            'type':'post',
            'success':function(result) { $("#price").find('span').html(result); },
            'cache':false,
            'data': jQuery("form#pricing").serialize()
        });
    }

    $("form#pricing input").change(loadPrice);

    $(document).ready(function(){
        loadPrice();

        $(function() {
            $(".buttonInsideMe").buttonset();
        });

        $(function() {
            $(".buttonMe").button();
        });

        $(function() {
            var select = $( ".slideMe" );

            var number = select.children('option').length;
            select.hide();

            select.selectToUISlider({
                labels: number,
                labelSrc: 'text',
                sliderOptions: {
                    change:function(e, ui) {
                        $("#falseChange").trigger('change');
                    }
                }
            });
        });
    });
</script>