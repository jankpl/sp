<?php
/* @var $model Postal */
/* @var $form TbActiveForm */
/* @var $domestic boolean */
/* @var $senderCountryList CountryList[] */
/* @var $receiverCountryList CountryList[] */
/* @var $available boolean */
/* @var $price string */
?>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  spPointsDetails: '.CJSON::encode(Yii::app()->urlManager->createUrl('/spPoints/ajaxGetSpPointDetails')).',
                  getPrice: '.CJSON::encode(Yii::app()->urlManager->createUrl('/postal/postal/ajaxGetPrice')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).'
              },
               loader: {
                  textTop: '.CJSON::encode(Yii::t('site', 'Trwa ładowanie...')).',
                  textBottom: '.CJSON::encode(Yii::t('site', 'Proszę czekać...')).',
                  error:  '.CJSON::encode(Yii::t('site', 'Wystąpił błąd, ale system spróbuje automatycznie przetworzyć formularz. Następnie proszę spróbować kontynuować. Jeżeli błąd będzie się powtarzał, prosimy o kontakt.')).',
              },
          };
      ',CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.bootstrap-touchspin.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/postalForm.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.bootstrap-touchspin.min.css');
?>

<h2><?php echo Yii::t('postal', 'Create Postal'); ?> | <?= Yii::t('app','Krok {step}', array('{step}' => '1/2')); ?></h2>
<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'postal-form',
        'enableClientValidation' => true,
        'stateful'=>true,
//        'htmlOptions' => [
//            'class' => 'form-horizontal',
//        ],
    ));
    ?>
    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('postal', 'Typ');?></legend>
                <div class="text-center" style="margin: 10px auto;">
                    <div class="btn-group btn-group-lg" role="group">
                        <?php
                        foreach(Postal::getPostalTypeList() AS $key => $item)
                            echo '<button type="button" class="btn btn-default" data-value-id="'.$key.'" data-radio-nice="Postal[postal_type]">'.$item.'</button>';
                        ?>
                    </div>
                    <?php echo $form->error($model,'postal_type'); ?>
                </div>
                <div class="row text-center" style="display: none;">
                    <?php echo $form->radioButtonList($model, 'postal_type', Postal::getPostalTypeList(), ['data-dependency' => 'true', 'id' => 'postal_type']); ?>
                </div>
                <div class="alert alert-info">
                    <ul>
                        <?php
                        foreach(Postal::getPostalTypeList() AS $key => $item):
                            ?>
                            <li><strong><?= $item;?></strong>: <?= Postal::getPostalTypeListDesc()[$key];?></li>
                        <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('postal', 'Waga');?></legend>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <strong><?= Yii::t('postal', 'Waga');?> [g] :</strong><br/>
                        <?php echo $form->numberField($model, 'weight', ['maxlength' => 32, 'data-weight-value' => true, 'min' => 1, 'max' =>  Postal::getMaxWeightValue(), 'style' => 'text-align: center;', 'data-dependency' => 'true']);  ?><br/>
                        <?php echo $form->error($model, 'weight');  ?>                        <br/>

                        <?= Yii::t('postal', 'Klasa wagowa');?>: <span id="weight-class-info" style="font-weight: bold;">-</span>
                    </div>
                </div>
                <?php
                if(0):
                    ?>
                    <div class="text-center" style="margin: 10px auto;">
                        <div class="btn-group btn-group-lg" role="group">
                            <?php
                            foreach(Postal::getWeightClassList() AS $key => $item)
                                echo '<button type="button" class="btn btn-default" data-value-id="'.$key.'" data-radio-nice="Postal[weight_class]">'.$item.'</button>';
                            ?>
                        </div>
                        <?php echo $form->error($model,'weight_class'); ?>
                    </div>
                    <div class="row text-center" style="display: none;">
                        <?php echo $form->radioButtonList($model, 'weight_class', Postal::getWeightClassList(), ['data-dependency' => 'true', 'id' => 'weight_class']); ?>
                    </div>
                <?php
                endif;
                ?>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('postal', 'Gabaryt');?></legend>
                <div class="text-center" style="margin: 10px auto;">
                    <div class="btn-group btn-group-lg" role="group">
                        <?php
                        foreach(Postal::getSizeClassList() AS $key => $item)
                            echo '<button type="button" class="btn btn-default" data-value-id="'.$key.'" data-radio-nice="Postal[size_class]" data-size-class-max-weight="'.Postal::getMaxWeightForSizeClass($key).'">'.$item.'</button>';
                        ?>
                    </div>
                    <?php echo $form->error($model,'size_class'); ?>
                </div>
                <div class="row text-center" style="display: none;">
                    <?php echo $form->radioButtonList($model, 'size_class', Postal::getSizeClassList(), ['data-dependency' => 'true', 'id' => 'size_class']); ?>
                </div>

                <div class="alert alert-info">
                    <ul>
                        <?php
                        foreach(Postal::getSizeClassList() AS $key => $item):
                            ?>
                            <li><strong><?= $item;?></strong>: <?= Postal::getSizeClassListDesc()[$key];?></li>
                        <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
            </fieldset>
        </div>
    </div>
    <?php
    // SPECIAL RULE FOR RUCH // 09.09.2016
    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH):
        ?>
        <?php
        $model->target_sp_point = SpPoints::CENTRAL_SP_POINT_ID;
        echo $form->hiddenField($model,'target_sp_point');
        ?>
    <?php
    else:
        ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <fieldset>
                    <legend><?= Yii::t('postal', 'Punkt dostarczenia listów');?></legend>
                    <div class="row text-center">
                        <div class="col-xs-12 col-sm-12 text-center">
                            <?php
                            $points = SpPoints::getPoints();
                            echo $form->dropDownList($model, 'target_sp_point', CHtml::listData($points, 'id' ,'spPointsTr.title'), ['prompt' => '-- '.Yii::t('site', 'wybierz punkt z listy').' --', 'data-sp-points-selector' => true]);
                            echo $form->error($model, 'target_sp_point');
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-12" id="sp-points-details">
                            <?php
                            if($model->target_sp_point):

                                $spModel = SpPoints::getPoint($model->target_sp_point);
                                if($spModel != NULL)
                                    $this->renderPartial('//spPoints/_ajaxGetSpPointDetails', ['model' => $spModel]);
                            endif;
                            ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    <?php
    endif;
    ?>
    <div class="row">
        <?php
        $this->Widget('AddressDataSwitch', ['attrBaseName' => 'Postal_AddressData']);
        ?>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('postal', 'Nadawca');?></legend>
                <?php
                $this->renderPartial('_addressData_create/_form',
                    array(
                        'model' => $model->senderAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_SENDER),
                        'form' => $form,
//                        'noEmail' => true,
                        'i' => UserContactBook::TYPE_SENDER,
                        'type' => UserContactBook::TYPE_SENDER,
                        'countryList' => $senderCountryList,
//                    'focusFirst' => true,
                    ));
                ?>
            </fieldset>
        </div>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('postal', 'Odbiorca');?></legend>
                <?php
                $this->renderPartial('_addressData_create/_form',
                    array(
                        'model' => $model->receiverAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_RECEIVER),
                        'form' => $form,
//                        'noEmail' => true,
                        'i' => UserContactBook::TYPE_RECEIVER,
                        'type' => UserContactBook::TYPE_RECEIVER,
                        'countryList' => $receiverCountryList,
//                    'focusFirst' => true,
                    ));
                ?>
            </fieldset>
        </div>
    </div>
    <?php
    if(!Yii::app()->user->model->getHidePrices()):
        ?>
        <div class="row" data-price="true" data-continue="true" style="<?= (!$available) ? 'display: none;' : '';?>">
            <div class="col-xs-12 col-sm-12">
                <fieldset>
                    <legend><?= Yii::t('postal', 'Cena');?></legend>

                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4">
                            <table class="detail-view text-center" style="width: 100%;">
                                <tr class="odd">
                                    <th style="text-align: center;"><?php echo Yii::t('postal','Cena');?></th>
                                </tr>
                                <tr class="even">
                                    <td>
                                        <div class="postal-form postal-form-price" id="price"><?= $price;?></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <p style="text-align: center; font-size: 0.9em; padding-top: 10px"><?= Yii::t('courier', 'Cena brutto. Cena nie uwzględnia indywidualnych rabatów użytkownika oraz dodatków do paczki.');?></p>
                </fieldset>
            </div>
        </div>
    <?php
    endif;
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('postal', 'Dodatkowe');?></legend>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->label($model, 'value'); ?>
                        <?php echo $form->textField($model, 'value', ['maxlength' => 32]);  ?> <?php echo $form->dropDownList($model, 'value_currency', Postal::getCurrencyList());  ?>
                        <?php echo $form->error($model, 'value');  ?>
                    </div>
                    <div class="col-xs-12">
                        <?php echo $form->label($model, 'content'); ?>
                        <?php echo $form->textField($model, 'content', ['maxlength' => 32]);  ?>
                        <?php echo $form->error($model, 'content');  ?>
                    </div>
                    <div class="col-xs-12">
                        <?php echo $form->label($model, 'note1'); ?>
                        <?php echo $form->textField($model, 'note1', ['maxlength' => 50]);  ?>
                        <?php echo $form->error($model, 'note1');  ?>
                    </div>
                    <div class="col-xs-12">
                        <?php echo $form->label($model, 'note2'); ?>
                        <?php echo $form->textField($model, 'note2', ['maxlength' => 50]);  ?>
                        <?php echo $form->error($model, 'note2');  ?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>
    <div class="row" style="padding: 10px 0;">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('postal', 'Kontynuuj');?></legend>
                <div data-not-available="true" style="display: none;">
                    <div class="alert alert-warning text-center">
                        <?= Yii::t('postal', 'Niestety, ale wybrane trasa nie jest przez nas obsługiwana.');?>
                    </div>
                </div>
                <div class="text-center">
                    <?php echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('name' => 'summary', 'class' => 'btn-lg btn-primary', 'disabled' => '', 'data-continue-button' => 'true')); ?>
                </div>
            </fieldset>
        </div>
    </div>
    <?= $form->hiddenField($model, 'source'); // for cloning ?>
    <?php $this->endWidget(); ?>
</div>
<script>
    var classes = '<?= json_encode(array_combine((Postal::getWeightClassValueList()), Postal::getWeightClassList()));?>';
    classes = JSON.parse(classes);

    function showWeightClass() {
        var weight = parseFloat($('[data-weight-value]').val());
        var max = parseFloat($('[data-weight-value]').attr('max'));

        if (weight > max) {
            $('[data-weight-value]').val(max);
            weight = max;
        }

        var found = false;

        if(!weight)
            found = '-';
        else
            $.each(classes, function(i,v)
            {
                found = v;
                if(parseFloat(i) >= weight)
                    return false;

            });
        $('#weight-class-info').html(found);

        $('[data-size-class-max-weight]').each(function(i,v){
            var val = parseFloat($(v).attr('data-size-class-max-weight'));


            if(val < weight)
                $(v).attr('disabled', true);
            else
                $(v).attr('disabled', false);
        })
    }

    $('[data-weight-value]').on('change', showWeightClass);
    $(document).ready(function(){
        showWeightClass();
    });
</script>