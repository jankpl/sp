<div id="upload-form">
    <div class="form">
        <?php

        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
            array(
                'enableAjaxValidation' => false,
                'htmlOptions' =>
                    array('enctype' => 'multipart/form-data'),
            )
        );
        ?>

        <h2><?php echo Yii::t('courier','Import contact book items from file');?></h2>

        <?php $this->widget('FlashPrinter'); ?>

        <?php
        echo $form->errorSummary($model);
        ?>

        <table class="table table-striped" style="margin: 10px auto;">
            <tr>
                <td style="width: 300px;"><?php echo $form->labelEx($model,'file', ['style' => 'width: 100%;']); ?></td>
                <td><?php echo $form->fileField($model, 'file'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'omitFirst', ['style' => 'width: 100%;']); ?></td>
                <td><?php echo $form->checkBox($model, 'omitFirst'); ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><?php echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('class' => 'btn btn-lg btn-primary'));?></td>
            </tr>
        </table>
        <?php
        $this->endWidget();
        ?>

        <div class="info">
            <div class="alert alert-info">
                <?php echo Yii::t('courier', 'Maksymalna liczba wierszy w pliku to: {number1}.', array('{number1}' => '<strong>'.F_ContactBookImport::MAX_LINES.'</strong>')); ?>
            </div>
            <p><?php echo Yii::t('courier','Allowed file type:');?> .csv,.xls,.xlsx.</p>
            <br/>
            <p><?php echo Yii::t('courier','The file must contain following columns:');?><br/>
                [type],
                [name],
                [company],
                [zip_code],
                [city],
                [address_line_1],
                [address_line_2],
                [tel],
                [email],              
            </p>
            <br/>
            <?php echo CHtml::link(Yii::t('courier','Download file template'), Yii::app()->baseUrl.'/misc/ipff_template_contactBook.xlsx', array('target' => '_blank', 'class' => 'btn btn-success', 'download' => 'ipff_template_contactBook.xlsx'));?>

        </div>
    </div>
</div>
<?php
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/custom-theme/jquery-ui-1.10.3.custom.min.css');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
?>