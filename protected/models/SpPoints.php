<?php

Yii::import('application.models._base.BaseSpPoints');

class SpPoints extends BaseSpPoints
{

	const CENTRAL_SP_POINT_ID = 1;
	const QURIERS_SP_POINT_ID = 17;
	const DE_SP_POINT_ID = 17;
	const CQL_SP_POINT_ID = 19;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('date_entered', 'default',
				'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
			array('date_updated', 'default',
				'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),

			array('name', 'required'),
			array('address_data_id, stat', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>123),
			array('date_updated', 'safe'),
			array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, date_entered, date_updated, name, address_data_id, stat', 'safe', 'on'=>'search'),
		);
	}

	public function getNameWithId()
    {
        return $this->id.') '.$this->name;
    }

	/**
	 * Returns fake SpPoint based on Central SP Point
	 * @return SpPoints
	 */
	public static function getFakeModel()
	{
		$model = self::model()->findByPk(self::CENTRAL_SP_POINT_ID);
		$model->name = 'HUB';

		$model->spPointsTr->title = 'HUB';
		$model->spPointsTr->text = '-';
		$model->addressData->name = 'HUB';
		$model->addressData->address_line_1 = '-';
		$model->addressData->address_line_2 = '';
		$model->addressData->city = '-';
		$model->addressData->zip_code = '-';
		$model->addressData->company = '-';
		$model->addressData->tel = '-';

		return $model;
	}


	public function saveWithRelatedModels($ownTransaction = true)
	{
		$errors = false;

		if($this->addressData === null)
			return false;

		$transaction = null;
		if($ownTransaction)
			$transaction = Yii::app()->db->beginTransaction();


		if(!$this->addressData->save())
			$errors = true;

		if(!$errors)
		{
			$this->address_data_id = $this->addressData->id;

			if(!$this->save())
				$errors = true;
		} else
			$this->validate();


		if(is_array($this->spPointsTrs))
			foreach($this->spPointsTrs AS $item)
			{
				if(!$errors)
				{
					$item->sp_points_id = $this->id;
					if(!$item->save())
						$errors = true;
				} else
					$item->validate();
			}

		if($errors)
		{
			if($ownTransaction)
				$transaction->rollback();
			return false;
		}

		if($ownTransaction)
			$transaction->commit();
		return true;
	}

	public function deleteWithRelatedModels()
	{
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			AddressData::model()->findByPk($this->address_data_id)->delete();

			if(is_array($this->spPointsTrs))
				foreach($this->spPointsTrs AS $item)
					$item->delete();

			parent::delete();
			$transaction->commit();
			return true;
		}
		catch(Exception $ex)
		{
			$transaction->rollBack();
			return false;
		}
	}

	public function toggleStat()
	{
		$this->stat = abs($this->stat - 1);
		return $this->save();
	}

	/**
	 * Function returning active SP points
	 *
	 * @return SpPoints[]
	 */
	public static function getPoints()
	{
		if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH)
			return [self::getFakeModel()];
		else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS)
		    return [self::model()->with('spPointsTr')->with('addressData')->cache(60*15)->find(['condition' => 'stat = :stat AND t.id = :id', 'params' => [':stat' => S_Status::ACTIVE, ':id' => self::QURIERS_SP_POINT_ID], 'order' => 't.id ASC'])];

		$models = self::model()->with('spPointsTr')->with('addressData')->cache(60*15)->findAll(['condition' => 'stat = :stat AND t.id != :id', 'params' => [':stat' => S_Status::ACTIVE, ':id' => self::QURIERS_SP_POINT_ID], 'order' => 't.id ASC']);

		return $models;
	}

	/**
	 * Function returning active SP point by id
	 *
	 * @param $id int Id of SpPoint
	 * @return SpPoints
	 */
	public static function getPoint($id)
	{
		if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH)
			return self::getFakeModel();

		$model = self::model()->with('spPointsTr')->with('addressData')->cache(60*15)->find(['condition' => 'stat = :stat AND t.id = :id', 'params' => [':stat' => S_Status::ACTIVE, ':id' => $id]]);

		return $model;
	}

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('address_data_id', $this->address_data_id);
        $criteria->compare('stat', $this->stat);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }
}