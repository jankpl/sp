<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('international-mail-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<p>
    You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'international-mail-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'selectableRows'=>2,
    'columns' => array(
        array(
            'name'=>'local_id',
            'header'=>'# local ID',
            'type'=>'raw',
            'value'=>'CHtml::link($data->local_id,array("/internationalMail/internationalMail/view/", "id" => $data->id))',
        ),

        array(
            'name'=>'__sender_country_id',
            'header'=>'Sender country',
            'value'=>'$data->senderAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name'=>'__receiver_country_id',
            'header'=>'Receiver country',
            'value'=>'$data->receiverAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name'=>'date_entered',
            'header'=>'Creation date',
        ),
        array(
            'name'=>'order_id',
            'type'=>'raw',
            'value'=>'$data->order_id !== null? CHtml::link($data->order_id,array("/order/view/", "id" => $data->order_id)) : "-"',
        ),
        array(

            'name'=>'__user_login',
            'type'=>'raw',
            'value'=>'$data->user !== null? CHtml::link($data->user->login,array("/user/view/", "id" => $data->user_id)) : "-"',
        ),
        array(
            'name' => 'daysFromStatusChange',
            'header' => 'Status age (days)',
            'filter' => false,
        ),
        array(
            'name'=>'__stat',
            'value'=>'$data->stat->name',
            'filter'=>GxHtml::listDataEx(InternationalMailStat::model()->findAllAttributes(null, true)),
        ),
        'mails_number',
        array(
            'header'=>'Dimensions ['.InternationalMail::getDimensionUnit().']',
            'value'=>'$data->mail_size_l ." x ". $data->mail_size_w ." x ". $data->mail_size_d',
        ),
        array(
            'header'=>'Weight ['.InternationalMail::getWeightUnit().']',
            'value'=>'$data->mail_weight',
        ),
        'outside_id',
        /*array(
            'class' => 'CButtonColumn',
        ),*/
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'selected_rows'
        ),

    ),
)); ?>

<h3>Change statuses of selected items</h3>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div style="text-align: center;">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'mass-assign-stat',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('/internationalMail/internationalMail/assingStatusByGridView'),
    ));
    ?>

    <?php echo CHtml::dropDownList('international_mail_stat_id','', CHtml::listData(InternationalMailStat::model()->findAll(),'id','name')); ?>

    <?php
    echo Chtml::submitButton('Change', array(
        'onClick' => 'js:
    $("#international-mail-ids").val($.fn.yiiGridView.getChecked("international-mail-grid","selected_rows").toString());
    ;',
        'name' => 'InternationalMailIds'));
    ?>

    <?php echo CHtml::hiddenField('InternationalMail[ids]','', array('id' => 'international-mail-ids')); ?>
    <?php
    $this->endWidget();
    ?>
</div>