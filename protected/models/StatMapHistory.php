<?php

Yii::import('application.models._base.BaseStatMapHistory');

class StatMapHistory extends BaseStatMapHistory
{
    const TABLE_NAME = 'stat_map_history';

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    /**
     * Adds stat map to history
     * @param $type
     * @param $type_item_id
     * @param $new_stat_map_id
     * @param $previous_stat_map_id
     * @return boolean
     */
    public static function addItem($type, $type_item_id, $new_stat_map_id, $previous_stat_map_id, $stat_date = false, $location = NULL)
    {

        $date_entered = date('Y-m-d H:i:s');

        if(!$stat_date)
            $stat_date = $date_entered;

        $data = [
            'type' => $type,
            'type_item_id' => $type_item_id,
            'stat_map_id' => $new_stat_map_id,
            'previous_stat_map_id' => $previous_stat_map_id,
            'stat_date' => $stat_date,
            'date_entered' => $date_entered,
            'location' => $location == '' ? NULL : $location,
        ];


        $cmd = Yii::app()->db->createCommand();
        return $cmd->insert(self::TABLE_NAME, $data) ? true : false;
    }

    /**
     * Method returns array containing all visited stat maps. Returns empty array if not found.
     * @param $type
     * @param $type_item_id
     * @return array
     */
    public static function getReceivedMaps($type, $type_item_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('stat_map_id');
        $cmd->from(self::TABLE_NAME);
        $cmd->where('type = :type AND type_item_id = :type_item_id', [':type' => $type, ':type_item_id' => $type_item_id]);
        return $cmd->queryColumn();
    }

    public function getDate()
    {
        return $this->stat_date == NULL ? $this->date_entered : $this->stat_date;
    }
}