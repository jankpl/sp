<?php

class PostalScannerController extends Controller
{
    const MAX_NO = 1000;

    const STATE_PROGRESS = 1;
    const STATE_SUCCESS = 10;
    const STATE_FAIL = 99;

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function beforeAction($action)
    {
        if(Postal::isAvailableForUser() && Yii::app()->user->getModel()->isPremium)
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('postal', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Postal',

            ));
            exit;
        }
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    protected static function getStatusCacheName($sid)
    {
        return 'POSTAL_SCANNER_STATUS_'.Yii::app()->session->sessionID.'_'.$sid;
    }

    protected static function setStatusValue($sid, $state, $message = false, $done = [], $all = [])
    {
        $totalNo = S_Useful::sizeof($all);

        $doneIds = [];

        foreach($all AS $key => $item)
        {
            if(isset($done[$key]))
            {
                $doneIds[$key] = $item;
                unset($all[$key]);
            }
        }

        $data = [
            'state' => $state,
            'message' => $message,
            'doneNo' => S_Useful::sizeof($doneIds),
            'doneList' => $doneIds,
            'notDoneNo' => S_Useful::sizeof($all),
            'notDoneList' => $all,
            'totalNo' => $totalNo,
        ];

        Yii::app()->cache->set(self::getStatusCacheName($sid), $data, 60*60);
    }

    public function actionStatus($sid)
    {
        if(Yii::app()->request->isAjaxRequest) {
            $data = Yii::app()->cache->get(self::getStatusCacheName($sid));

            echo CJSON::encode($data);
        } else
            Yii::app()->end();
    }

    public function actionIndex($sid = false)
    {
        $this->panelLeftMenuActive = 202;


        $done = [];

        self::setStatusValue($sid, self::STATE_PROGRESS);
        Yii::app()->cache->set(self::getStatusCacheName($sid), 'abc', 60*60);
        $model = new UserScanner();

        if($sid OR isset($_POST['UserScanner']['items_ids']))
        {
            $data = $_POST;

            $packagesIds = $data['UserScanner']['items_ids'];
            $packagesIds = explode(PHP_EOL, $packagesIds);

            $model->items_ids = $packagesIds;

            if($model->validate())
            {
                $postalModels = $model->returnPostalModels();

                if(!S_Useful::sizeof($postalModels)) {
                    self::setStatusValue($sid, self::STATE_FAIL, Yii::t('userScanner', 'Nie przetworzono żadnej przesyłki!'), [], $model->items_ids_backup);
                    Yii::app()->end();
                }
                else if(S_Useful::sizeof($postalModels) > self::MAX_NO)
                {
                    self::setStatusValue($sid, self::STATE_FAIL, Yii::t('userScanner', 'Maksymalna liczba przesyłek na raz: {no}', ['{no}' => self::MAX_NO]), [], $model->items_ids_backup);
                    Yii::app()->end();
                }
                else
                {

                    if (isset($data['mode_kn'])) {

                        $postingDate = false;
                        if(Yii::app()->user->getModel()->getPPDaysDelay())
                            $postingDate = S_Useful::workDaysNextDate(date('Y-m-d'), Yii::app()->user->getModel()->getPPDaysDelay());

                        $done = KsiazkaNadawcza::generate($postalModels, KsiazkaNadawcza::MODE_POSTAL, $postingDate);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_ack'])) {
                        $done = PostalAcknowlegmentCard::generateCardMulti($postalModels);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_ack_stat']) && Yii::app()->user->model->getManifestGenerationSetsStatus()) {
                        $temp = [];
                        $done = PostalAcknowlegmentCard::generateCardMulti($postalModels, false, false, $temp, false, true);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_label_10x15'])) {
                        $done = PostalLabelPrinter::generateLabels($postalModels, PostalLabelPrinter::PDF_ROLL, true);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_xls'])) {
                        $done = S_PostalIO::exportToXlsUser($postalModels);
                        self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);
                    }
                    else if (isset($data['mode_cancel']))
                    {
                        $return = Postal::userCancelMulti($postalModels, $done);

                        if($return)
                            self::setStatusValue($sid, self::STATE_SUCCESS, Yii::t('userScanner', 'Anulowano przesyłek: {b}{no}{/b}', ['{b}' => '<strong>', '{/b}' => '</strong>', '{no}' => $return]), $done, $model->items_ids_backup);
                        else
                            self::setStatusValue($sid, self::STATE_FAIL, Yii::t('userScanner', 'Nie anulowano żadnej przesyłki!'), $done, $model->items_ids_backup);
                    }

                }
                // important!
                Yii::app()->end();
            }
        }

        $model->items_ids = '';
        $this->render('index',
            [
                'model' => $model,
            ]);

    }
}
