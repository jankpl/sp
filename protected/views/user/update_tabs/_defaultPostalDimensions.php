<div class="form">
    <?php
    Yii::app()->getModule('postal');
    $postal = new Postal;
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'default-postal-sizes',
        'enableClientValidation'=>true,
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'dimensionsPostal']),
    ));
    ?>

    <h3><?php echo Yii::t('user', 'Domyślne parametry listów');?></h3>

    <div class="alert alert-info"><?= Yii::t('postal', 'Ta opcja powoduje automatyczne uzupełnienie wymiarów oraz wagi Twoich listów.');?></div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <table class="table">
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $postal->getAttributeLabel('postal_type');?></th>
                    <td><?= $form->dropDownList($model, 'defaultPostalType', Postal::getPostalTypeList());?><?= $form->error($model, 'defaultPostalType');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $postal->getAttributeLabel('postal_size');?></th>
                    <td><?= $form->dropDownList($model, 'defaultPostalSize', Postal::getSizeClassList());?><?= $form->error($model, 'defaultPostalSize');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $postal->getAttributeLabel('weight');?> [<?= Postal::getWeightUnit();?>]</th>
                    <td><?= $form->textField($model, 'defaultPostalWeight', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPackageWeight');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $postal->getAttributeLabel('value');?></th>
                    <td><?= $form->textField($model, 'defaultPostalValue', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPostalValue');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $postal->getAttributeLabel('value_currency');?></th>
                    <td><?= $form->dropDownList($model, 'defaultPostalValueCurrency', Postal::getCurrencyList());?><?= $form->error($model, 'defaultPostalValueCurrency');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $postal->getAttributeLabel('content');?></th>
                    <td><?= $form->textField($model, 'defaultPostalContent', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPostalContent');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $model->getAttributeLabel('defaultPostalReceiverCountryId');?></th>
                    <td><?= $form->dropDownList($model, 'defaultPostalReceiverCountryId', CountryList::getActiveCountries(true), ['prompt' => '-']);?><?= $form->error($model, 'defaultPostalReceiverCountryId');?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'savePackagePostalDefault'));?></td>
                </tr>
            </table>
        </div>
    </div>

    <?php
    $this->endWidget();
    ?>

</div>