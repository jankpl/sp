<?php
/* @var $this SiteController */
/* @var $cc string */
/* @var $form CActiveForm */

?>

<h2><?php echo Yii::t('zip_code','Reguły kodu pocztowego');?></h2>


<div class="form alert alert-info text-center">

    <div>
        <strong><?= Yii::t('zip_code', 'Wybierz kraj:');?></strong>
        <?php echo CHtml::dropDownList('cc', $cc, CHtml::listData(CountryList::model()->findAll(), 'code', 'name'), ['prompt' => '-' ,'id' => 'country_code']); ?>
    </div>
    <br/>
    <br/>
    <div class="well text-center">
        <p><strong><?= Yii::t('zip_code', 'Lista dozwolonych reguł:');?></strong></p>
        <div data-rules="true">-</div>
        <div data-rules="false"><?= Yii::t('zip_code', '- nie znaleziono reguł dla tego kraju -');?></div>
    </div>

    <div class="alert text-left" data-desc-nice="true">
        <?= Yii::t('zip_code', 'Gdzie:');?>
        <ul>
            <li><strong>\N</strong> - <?= Yii::t('zip_code', 'Cyfra');?></li>
            <li><strong>\A</strong> - <?= Yii::t('zip_code', 'Litera');?></li>
        </ul>
    </div>

    <div class="alert  text-left" data-desc-nice="false">
        <?= Yii::t('zip_code', 'Reguła opisana jest za pomocą wyrażeń regularnych (regular expression), gdzie ważniejsze symbole to:');?>
        <ul>
            <li><strong>.</strong> - <?= Yii::t('zip_code', 'Dowolny znak');?></li>
            <li><strong>*</strong> - <?= Yii::t('zip_code', 'Zero lub więcej wystąpień poprzedzającego symbolu');?></li>
            <li><strong>+</strong> - <?= Yii::t('zip_code', 'Jeden lub więcej wystąpień poprzedzającego symbolu');?></li>
            <li><strong>{3}</strong> - <?= Yii::t('zip_code', '3 wystąpienia poprzedzającego symbolu');?></li>
            <li><strong>?</strong> - <?= Yii::t('zip_code', 'Poprzedzający symbol jest opcjonalny');?></li>
            <li><strong>\d</strong> - <?= Yii::t('zip_code', 'Cyfra');?></li>
            <li><strong>\w</strong> - <?= Yii::t('zip_code', 'Litera');?></li>
            <li><strong>[XYZ]</strong> - <?= Yii::t('zip_code', 'Dozwolone są litery X,Y lub Z');?></li>
            <li><strong>[A-Z]</strong> - <?= Yii::t('zip_code', 'Dozwolone są litery od A do Z');?></li>
            <li><strong>[0-6]</strong> - <?= Yii::t('zip_code', 'Dozwolone są cyfry od 0 do 6');?></li>
            <li><strong>[XYZ]+</strong> - <?= Yii::t('zip_code', 'Dozwolony jest jeden lub więcej znaków z tej grupy');?></li>
        </ul>
        <br/>
        <?= Yii::t('zip_code', 'Szczegółowy opis wszystkich symboli i reguł można znaleźć w internecie.');?>
    </div>

</div><!-- form -->


<script>
    (function($) {

        var rules = <?= CJSON::encode(ZipValidatorBase::$RULES);?>;
        var rulesNice = <?= CJSON::encode(ZipValidatorBase::getDescriptionList());?>;


        var $input = $('#country_code');
        var $rulesDescAll = $('[data-desc-nice]');
        var $rulesDescNice = $('[data-desc-nice="true"]');
        var $rulesDescNotNice = $('[data-desc-nice="false"]');
        var $rulesFound = $('[data-rules="true"]');
        var $rulesNotFound = $('[data-rules="false"]');

        $(document).ready(function(){
            showRules();
        });

        function showRules()
        {
            $rulesDescAll.hide();
            $rulesNotFound.hide();

            var cc = $input.val();

            if(cc == '')
                return;

            var nice = true;



            var rule = rulesNice[cc];

            if(rule === undefined) {
                rule = rules[cc];
                nice = false;
            }

            if(rule === undefined) {

                $rulesFound.hide();
                $rulesNotFound.show();
                $rulesDescAll.hide();

            } else {
                $rulesFound.html(rule).show();

                if(nice)
                    $rulesDescNice.show();
                else
                    $rulesDescNotNice.show();
            }


        }

        $input.on('change',showRules)

    })(jQuery);
</script>
