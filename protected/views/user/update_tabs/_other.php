<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'other-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'other']),
    ));
    ?>

    <?php
    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL):
        ?>


        <h3><?php echo Yii::t('user', 'Pozostałe ustawienia');?></h3>

        <table class="table">
            <?php
            if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL):
                ?>
                <tr>
                    <td><?php echo $form->checkBox($model, 'acceptRegulationsByDefault'); ?></td>
                    <td><?php echo Yii::t('user', 'Zaznacz, jeżeli chcesz, aby akceptacje regulaminu były domyślnie zaznaczone.');?></td>
                </tr>
            <?php
            endif;
            ?>
            <?php
            if(0):
                ?>
                <tr>
                    <td><?php echo $form->checkBox($model, 'fastFinalizeInternal'); ?></td>
                    <td><?php echo Yii::t('user', 'Dla paczek typu "{type}" domyślnie wybieraj formę płatności "{on_invoice}" i automatycznie finalizuj transakcje.', ['{type}' => Courier::getTypesNames()[Courier::TYPE_INTERNAL], '{on_invoice}' => PaymentType::getPaymentTypeName(PaymentType::PAYMENT_TYPE_INVOICE)]);?></td>
                </tr>
            <?php
            endif;
            ?>
            <?php
            if(0):
                ?>
                <tr>
                    <td><?php echo $form->checkBox($model, 'activateNewPanel'); ?></td>
                    <td><?php echo Yii::t('user', 'Aktywuj nowy panel');?></td>
                </tr>
            <?php
            endif;
            ?>
            <tr>
                <td colspan="2"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'saveOther'));?></td>
            </tr>
        </table>

    <?php
    endif;
    ?>

    <h3><?php echo Yii::t('user', 'Ustawienia listy przesyłek kurierskich');?></h3>

    <table class="table table-striped">
        <thead>
        <tr>
            <th><?= Yii::t('user', 'Kolumna');?></th>
            <th><?= Yii::t('user', 'Widoczność');?></th>
        </tr>
        </thead>
        <?php
        foreach(User::listGridViewCourierCustomColumns() AS $key => $item):
            ?>
            <tr>
                <td><?= $item;?></td>
                <td><?= CHtml::checkBox('courierGridViewSettings['.$key.']', $model->getCourierGridViewSettings($key));?></td>
            </tr>
        <?php
        endforeach;
        ?>

        <tr>
            <td colspan="3"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'saveCourierGridViewSettings'));?> <?php echo TbHtml::submitButton(Yii::t('app', 'Restore default'), array('name' => 'restoreCourierGridViewSettings', 'class' => 'btn-xs'));?>
        </tr>
    </table>

    <h3><?php echo Yii::t('user', 'Ustawienia listy przesyłek listowych');?></h3>

    <table class="table table-striped">
        <thead>
        <tr>
            <th><?= Yii::t('user', 'Kolumna');?></th>
            <th><?= Yii::t('user', 'Widoczność');?></th>
        </tr>
        </thead>
        <?php
        foreach(User::listGridViewPostalCustomColumns() AS $key => $item):
            ?>
            <tr>
                <td><?= $item;?></td>
                <td><?= CHtml::checkBox('postalGridViewSettings['.$key.']', $model->getPostalGridViewSettings($key));?></td>
            </tr>
        <?php
        endforeach;
        ?>

        <tr>
            <td colspan="3"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'savePostalGridViewSettings'));?> <?php echo TbHtml::submitButton(Yii::t('app', 'Restore default'), array('name' => 'restorePostalGridViewSettings', 'class' => 'btn-xs'));?>
        </tr>
    </table>

    <?php
    $this->endWidget();
    ?>

</div>