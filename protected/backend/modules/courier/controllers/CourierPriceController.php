<?php

class CourierPriceController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    /* @parm int $id Courier Group Id */
    /* @parm int $id2 Courier Pickup Hub Id */
    /* @parm int $user_group User Group Id */
    public function actionDeliveryPrice($id, $id2, $user_group = NULL)
    {

        $checkCountryGroup = CourierCountryGroup::model()->findByPk($id);
        if($checkCountryGroup == NULL)
            throw new CHttpException(404);

        if($user_group != NULL)
        {
            $checkUserGroup = UserGroup::model()->findByPk($user_group);
            if($checkUserGroup == NULL)
                throw new CHttpException(404);
        }


        $groupEditURLs = [];
        foreach(UserGroup::getListForThisAdmin() AS $item)
        {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('*');
            $cmd->from('courier_delivery_price_has_hub');
            $cmd->where('courier_country_group = :group', array(':group' => $id));
            $cmd->andWhere('courier_pickup_hub = :hub', array(':hub' => $id2));
            $cmd->andWhere('user_group_id = :user_group', array(':user_group' => $item->id));
            $result = $cmd->queryRow();

            $temp = [];
            $temp['name'] = $item->getNameWithAdmin().' ('.($result?'yes':'no').')';
            $temp['url'] = Yii::app()->createAbsoluteUrl('/courier/courierPrice/deliveryPrice', array('id' => $id, 'id2' => $id2, 'user_group' => $item->id));
            array_push($groupEditURLs, $temp);
        }

        if($user_group == NULL)
        {
            $groupActionDelete = true;
            $groupActionListView = $this->renderPartial('_groupActionList', array(
                'groupEditUrls' => $groupEditURLs
            ),
                true);
        } else {
            $groupActionDelete = true;
        }

        $this->updatePrice('delivery', $_POST,  $id, $id2, $user_group,  $groupActionListView, $groupActionDelete);

    }

    /* @parm int id Courier Group Id */
    /* @parm int user_group User Group Id */
    public function actionPickupPrice($id, $user_group = NULL)
    {

        $checkCountryGroup = CourierCountryGroup::model()->findByPk($id);
        if($checkCountryGroup == NULL)
            throw new CHttpException(404);

        if($user_group != NULL)
        {
            $checkUserGroup = UserGroup::model()->findByPk($user_group);
            if($checkUserGroup == NULL)
                throw new CHttpException(404);
        }

        $groupEditURLs = [];
        foreach(UserGroup::getListForThisAdmin() AS $item)
        {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('*');
            $cmd->from('courier_pickup_price_has_group');
            $cmd->where('courier_country_group = :group', array(':group' => $id));
            $cmd->andWhere('user_group_id = :user_group', array(':user_group' => $item->id));
            $result = $cmd->queryRow();

            $temp = [];
            $temp['name'] = $item->getNameWithAdmin().' ('.($result?'yes':'no').')';
            $temp['url'] = Yii::app()->createAbsoluteUrl('/courier/courierPrice/pickupPrice', array('id' => $id, 'user_group' => $item->id));
            array_push($groupEditURLs, $temp);
        }

        if($user_group == NULL)
        {
            $groupActionDelete = true;
            $groupActionListView = $this->renderPartial('_groupActionList', array(
                'groupEditUrls' => $groupEditURLs
            ),
                true);
        } else {
            $groupActionDelete = true;

        }


        $this->updatePrice('pickup', $_POST, $id, null, $user_group, $groupActionListView,$groupActionDelete);



    }

    protected function updatePrice($mode, $post, $country_group, $hub = NULL, $user_group, $groupActionListView = NULL, $groupActionDelete = false)
    {


        /* @var $model CourierPrice*/
        $model = CourierPrice::loadOrCreateDeliveryPrice($mode, $country_group, $hub, $user_group);
        if($model->id == NULL)
            $groupActionDelete = null;


        $courierPriceValues = $model->loadValueItems();

        $this->performAjaxValidation($model, 'courier-price-form');

        if (isset($post['CourierPrice']) OR isset($post['add_more']) OR isset($post['sort'])) {


            // Clicked button for just sort
            if(isset($post['delete']))
            {
                if($model->deleteWithRelated())
                {
                    Yii::app()->user->setFlash('success','Pricing was deleted!');
                } else {
                    Yii::app()->user->setFlash('error','Pricing was  NOT deleted!');
                }
                $this->redirect(array('courierPrice/view','id' => $country_group));
                return;
            }

            $courierPriceValues = Array();

            $model->setAttributes($post['CourierPrice']);

            $transaction = Yii::app()->db->beginTransaction();
            $error = false;

            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('courier_price_value', 'courier_price_id = :courier_price_id',
                array(':courier_price_id' => $model->id));

            function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
                $sort_col = array();
                foreach ($arr as $key=> $row) {
                    $sort_col[$key] = $row[$col];
                }

                array_multisort($sort_col, $dir, $arr);
            }

            $dataArray = [];
            foreach($post['CourierPriceValue'] AS $key => $item)
                $dataArray[$key] = array_merge($post['CourierPriceValue'][$key], $post['PriceValue'][$key]);


//            array_sort_by_column($post['CourierPriceValue'], 'weight_top_limit');
            array_sort_by_column($dataArray, 'weight_top_limit');


            if($model->id == NULL)
            {
                $model->save(true,'id');

                if($mode == 'pickup')
                {
                    $model2 = new CourierPickupPriceHasGroup();
                    $model2->courier_country_group = $country_group;
                    $model2->courier_price_item = $model->id;
                    $model2->user_group_id = $user_group;
                    if(!$model2->save())
                        $errors = true;
                }
                else if($mode == 'delivery')
                {
                    $model2 = new CourierDeliveryPriceHasHub();
                    $model2->courier_country_group = $country_group;
                    $model2->courier_price_item = $model->id;
                    $model2->user_group_id = $user_group;
                    $model2->courier_pickup_hub = $hub;

                    if(!$model2->save())
                        $errors = true;
                } else
                    $errors = true;
            }

            $maxWeightFound = false;
            foreach($dataArray AS $key => $item)
            {

                $courierPriceValue = new CourierPriceValue;
                $courierPriceValue->weight_top_limit = $item['weight_top_limit'];
                $courierPriceValue->pricePerItem = Price::generateByPost($item['pricePerItem']);
                $courierPriceValue->pricePerWeight = Price::generateByPost($item['pricePerWeight']);

                $courierPriceValue->price_per_item = $courierPriceValue->pricePerItem->saveWithPrices();
                $courierPriceValue->price_per_weight = $courierPriceValue->pricePerWeight->saveWithPrices();

                $courierPriceValue->courier_price_id = $model->id;

                $courierPriceValues[$key] = $courierPriceValue;

                if(!$courierPriceValues[$key]->save())
                    $error = true;

                if($courierPriceValues[$key]->weight_top_limit == CourierPrice::getMaxWeight())
                    $maxWeightFound = true;
            }

            // Clicked button for more prices
            if(isset($post['add_more']))
            {
                array_unshift($courierPriceValues, new CourierPriceValue);
                $error = true;
            }

            // Clicked button for just sort
            if(isset($post['sort']))
            {
                $error = true;
            }

            if(!$maxWeightFound)
            {
                $error = true;

                $maxPriceModel = $model->generateDefaultPriceValue();

                array_push($courierPriceValues, $maxPriceModel);
            }

            if(!$error AND $model->save())
            {
                $transaction->commit();
                $this->redirect(array('courierPrice/view','id' => $country_group));
            }
            $transaction->rollback();
        }

        $this->render('update', array(
            'model' => $model,
            'courierPriceValues' =>  $courierPriceValues,
            'groupActionListView' => $groupActionListView,
            'groupActionDelete' => $groupActionDelete,
            'userGroup' => $user_group,
            'countryGroup' => $country_group,
            'mode' => $mode,
        ));

    }

    public function actionView($id) {

        $model = CourierCountryGroup::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $this->render('view',array(
            'model' => $model,
        ));

    }

    public function actionAdmin() {
        $model = new CourierCountryGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierCountryGroup']))
            $model->setAttributes($_GET['CourierCountryGroup']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionSimulate($from = NULL)
    {

        $package_weight = 1;
        $package_size_l = 10;
        $package_size_d = 10;
        $package_size_w = 10;
        $user_group = NULL;
        $with_pickup = CourierTypeInternal::WITH_PICKUP_REGULAR;

        if($_POST)
        {
            $package_weight = (float) abs($_POST['package_weight']);
            $user_group = $_POST['user_group'] ==''?'':(int) abs($_POST['user_group']);
            $currency = $_POST['currency'] ==''?'':(int) abs($_POST['currency']);
            $with_pickup = $_POST['with_pickup'];
            $from = (int) $_POST['from'];
            $package_size_l = (int) $_POST['package_size_l'];
            $package_size_d = (int) $_POST['package_size_d'];
            $package_size_w = (int) $_POST['package_size_w'];

        }
        if(!$package_weight)
            $package_weight = 1;

        $currency = PriceCurrency::model()->findByPk($currency);
        if($currency == NULL)
            $currency = PriceCurrency::model()->find();

        $maxWeight = S_PackageMaxDimensions::getPackageMaxDimensions()['weight'];
        if($package_weight>$maxWeight)
            $package_weight = $maxWeight;

        $countryGroupList = CourierCountryGroup::model()->with('courierCountryGroupHasCountries')->findAll();


        $this->render('simulate',array(
            'countryGroupList' => $countryGroupList,
            'packageWeight' => $package_weight,
            'package_size_l' => $package_size_l,
            'package_size_d' => $package_size_d,
            'package_size_w' => $package_size_w,
            'userGroup' => $user_group,
            'currency' => $currency,
            'withPickup' => $with_pickup,
            'currencyList' => PriceCurrency::model()->findAll(),
            'from' => $from,
        ));


    }

    const COUNTRY = 0;
    const MODE = 1;
    const WEIGHT_TO = 2;
    const PPI_PLN = 3;
    const PPW_PLN = 4;
    const PPI_EUR = 5;
    const PPW_EUR = 6;
    const PPI_GBP = 7;
    const PPW_GBP = 8;

    public function actionImport($hash = false, $ugi = NULL)
    {
        if($hash == false)
        {

            $formModel = new F_RoutingImport;

            if(isset($_POST['F_RoutingImport'])) {
                $formModel->attributes = $_POST['F_RoutingImport'];
                $formModel->file = CUploadedFile::getInstance($formModel, 'file');

                if($formModel->validate())
                {
                    $fileData = $formModel->fileToArray($formModel->file->tempName);

                    $conversionSuccess = true;
                    if($formModel->deliveryPricesFromMatrixMode)
                        $conversionSuccess = $formModel->arrayFromMatrixConverter($fileData);

                    if($conversionSuccess)
                    {
                        $hash = hash('sha512', uniqid(time(),true));
                        Yii::app()->cacheUserData->set('CPI_'.$hash, $fileData, 60*60);
                        Yii::app()->cacheUserData->set('CPI_ORG_FILE_'.$hash, file_get_contents($formModel->file->tempName), 60*60);
                        Yii::app()->cacheUserData->set('CPI_ORG_FILE_EXT_'.$hash, $formModel->file->extensionName, 60*60);

                        $this->redirect(['/courier/courierPrice/import', 'hash' => $hash, 'ugi' => $formModel->user_group_id]);
                        exit;
                    }
                }
            }

            if(isset(Yii::app()->session['CPI']))
            {
                if(isset(Yii::app()->session['CPI_GET'])) {

                    $path = file_get_contents(Yii::app()->session['CPI']);
                    Yii::app()->session['CPI'] = false;
                    Yii::app()->session['CPI_GET'] = false;
                    unset(Yii::app()->session['CPI']);
                    unset(Yii::app()->session['CPI_GET']);
                    $filename = 'backup_courier_price_'.date('YmdHis').'.csv';
                    header("Content-Type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=\"".$filename."\";");
                    header("Content-Transfer-Encoding: binary");
                    exit($path);
                } else {
                    Yii::app()->session['CPI_GET'] = true;
                }
            }

            $this->render('import_step1', [
                'model' => $formModel,
            ]);
            exit;



        } else {

            $errors = false;

            if($ugi == -1)
                $ugi = NULL;

            $user_group_id = $ugi;

            $backup = [];
            $file = Yii::app()->cacheUserData->get('CPI_'.$hash);


            if(!$file OR !S_Useful::sizeof($file))
            {

                Yii::app()->user->setFlash('error', 'File looks empty...');
                $errors = true;
            }

            $data = [];
            if(is_array($file))
                foreach ($file AS $item) {

                    if (!S_Useful::sizeof($item))
                        continue;

                    foreach ($item AS $key => $temp) {
                        $temp = explode('_', $temp);
                        $item[$key] = $temp[0];
                    }

                    if (!isset($data[$item[self::COUNTRY]]))
                        $data[$item[self::COUNTRY]] = [];

                    if (!isset($data[$item[self::COUNTRY]][$item[self::MODE]]))
                        $data[$item[self::COUNTRY]][$item[self::MODE]] = [];

                    if (!isset($data[$item[self::COUNTRY]][$item[self::MODE]][$item[self::WEIGHT_TO]]))
                        $data[$item[self::COUNTRY]][$item[self::MODE]][$item[self::WEIGHT_TO]] = [];

                    $data[$item[self::COUNTRY]][$item[self::MODE]][$item[self::WEIGHT_TO]] = [
                        'PPI_PLN' => $item[self::PPI_PLN],
                        'PPW_PLN' => $item[self::PPW_PLN],
                        'PPI_EUR' => $item[self::PPI_EUR],
                        'PPW_EUR' => $item[self::PPW_EUR],
                        'PPI_GBP' => $item[self::PPI_GBP],
                        'PPW_GBP' => $item[self::PPW_GBP],
                    ];
                }

            $countries = array_keys($data);
            $countriesModels = [];
            $countriesModelsOld = [];

            foreach ($countries AS $country_id) {

                $countryData = $data[$country_id];

                /* @var $model CourierCountryGroup */

                $model = CourierCountryGroup::model()->findByPk($country_id);

                $hubs = CHtml::listData(CourierHub::model()->findAll(),'id', 'name');

                $modelsOld = [];
                $modelsOld['P'] = [];
                $modelsOld['P']['M'] = CourierPrice::loadOrCreateDeliveryPrice(CourierPrice::MODE_PICKUP, $country_id, NULL, $user_group_id);
                if($modelsOld['P']['M'])
                    $modelsOld['P']['PV'] = $modelsOld['P']['M']->loadValueItems();

                foreach($hubs AS $hub_id => $hub_name) {
                    $modelsOld[$hub_id] = [];
                    $modelsOld[$hub_id]['M'] = CourierPrice::loadOrCreateDeliveryPrice(CourierPrice::MODE_DELIVERY, $country_id, $hub_id, $user_group_id);
                    if($modelsOld[$hub_id]['M'])
                        $modelsOld[$hub_id]['PV'] = $modelsOld[$hub_id]['M']->loadValueItems();
                }

                if($errors)
                {
                    $this->redirect(['/courier/courierCountryGroup/import', 'hash' => false]);
                    exit;
                }

                $models['P'] = [];
                $models['P']['M'] = CourierPrice::loadOrCreateDeliveryPrice(CourierPrice::MODE_PICKUP, $country_id, NULL, $user_group_id);
                if($models['P']['M'])
                    $models['P']['PV'] = [];
//                    $models['P']['PV'] = $models['P']['M']->loadValueItems();

                foreach($hubs AS $hub_id => $hub_name) {
                    $models[$hub_id] = [];
                    $models[$hub_id]['M'] = CourierPrice::loadOrCreateDeliveryPrice(CourierPrice::MODE_DELIVERY, $country_id, $hub_id, $user_group_id);
                    if($models[$hub_id]['M'])
                        $models[$hub_id]['PV'] = [];
//                        $models[$hub_id]['PV'] = $models[$hub_id]['M']->loadValueItems();
                }

                // PICKUP:
                $courierPriceValues = [];
                if(isset($countryData['P']))
                {
                    $maxWeightFound = false;
                    foreach($countryData['P'] AS $weight => $item)
                    {

                        $courierPriceValue = new CourierPriceValue;
                        $courierPriceValue->weight_top_limit = $weight;

                        $ppi = [];
                        $ppi[Price::PLN] = ['value' => $item['PPI_PLN']];
                        $ppi[Price::EUR] = ['value' => $item['PPI_EUR']];
                        $ppi[Price::GBP] = ['value' => $item['PPI_GBP']];

                        $ppw = [];
                        $ppw[Price::PLN] = ['value' => $item['PPW_PLN']];
                        $ppw[Price::EUR] = ['value' => $item['PPW_EUR']];
                        $ppw[Price::GBP] = ['value' => $item['PPW_GBP']];

                        $courierPriceValue->pricePerItem = Price::generateByPost($ppi);
                        $courierPriceValue->pricePerWeight = Price::generateByPost($ppw);

                        $courierPriceValue->courier_price_id = $model->id;

                        $courierPriceValues[$weight] = $courierPriceValue;

                        if($courierPriceValues[$weight]->weight_top_limit == CourierPrice::getMaxWeight())
                            $maxWeightFound = true;
                    }

                    if(!$maxWeightFound)
                        $models['P']['M']->addError('_maxWeightErrorContainer', 'Max weight ('.CourierPrice::MAX_WEIGHT.') not found!');

                    $models['P']['PV'] = $courierPriceValues;
                }



                // DELIVERY:

                foreach($countryData AS $hub_id => $item) {

                    if($hub_id == 'P')
                        continue;

                    $courierPriceValues = [];
                    if (isset($countryData[$hub_id])) {
                        $maxWeightFound = false;

                        foreach ($countryData[$hub_id] AS $weight => $item) {

                            $courierPriceValue = new CourierPriceValue;
                            $courierPriceValue->weight_top_limit = $weight;

                            $ppi = [];
                            $ppi[Price::PLN] = ['value' => $item['PPI_PLN']];
                            $ppi[Price::EUR] = ['value' => $item['PPI_EUR']];
                            $ppi[Price::GBP] = ['value' => $item['PPI_GBP']];

                            $ppw = [];
                            $ppw[Price::PLN] = ['value' => $item['PPW_PLN']];
                            $ppw[Price::EUR] = ['value' => $item['PPW_EUR']];
                            $ppw[Price::GBP] = ['value' => $item['PPW_GBP']];

                            $courierPriceValue->pricePerItem = Price::generateByPost($ppi);
                            $courierPriceValue->pricePerWeight = Price::generateByPost($ppw);

//                            $courierPriceValue->price_per_item = $courierPriceValue->pricePerItem->saveWithPrices();
//                            $courierPriceValue->price_per_weight = $courierPriceValue->pricePerWeight->saveWithPrices();

//                            $courierPriceValue->courier_price_id = $model->id;

                            $courierPriceValues[$weight] = $courierPriceValue;

//                            if (!$courierPriceValues[$weight]->save())
//                                $error = true;

                            if ($courierPriceValues[$weight]->weight_top_limit == CourierPrice::getMaxWeight())
                                $maxWeightFound = true;
                        }

                        if(!$maxWeightFound && $models[$hub_id]['M'])
                            $models[$hub_id]['M']->addError('_maxWeightErrorContainer', 'Max weight ('.CourierPrice::MAX_WEIGHT.') not found!');

                        $models[$hub_id]['PV'] = $courierPriceValues;
                    }


                }

//                $deleteList[$country_id] = [];
//
                if($model && $model->isAvailableForPickup())
                    if(is_array($modelsOld['P']['PV']))
                        foreach($modelsOld['P']['PV'] AS $weight => $item)
                        {
                            /* @var $item CourierPriceValue */

                            if(!$item->weight_top_limit)
                                continue;

                            $temp = [];
                            $temp[] = $country_id . '_' . $model->name;
                            $temp[] = 'P';
                            $temp[] = $item->weight_top_limit;
                            $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::PLN_ID)->value : '';
                            $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::PLN_ID)->value : '';
                            $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::EUR_ID)->value : '';
                            $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::EUR_ID)->value : '';
                            $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::GBP_ID)->value : '';
                            $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::GBP_ID)->value : '';

                            $backup[] = $temp;
                        }


                if(is_array($modelsOld))
                    foreach($modelsOld AS $hub_id => $item)
                    {
                        if($hub_id == 'P')
                            continue;

                        if(is_array($modelsOld[$hub_id]['PV']))
                            foreach($modelsOld[$hub_id]['PV'] AS $weight => $item) {

                                if(!$item->weight_top_limit)
                                    continue;

                                $temp = [];
                                $temp[] = $country_id . '_' . $model->name;
                                $temp[] = $hub_id . '_HUB';
                                $temp[] = $item->weight_top_limit;
                                $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::PLN_ID)->value : '';
                                $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::PLN_ID)->value : '';
                                $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::EUR_ID)->value : '';
                                $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::EUR_ID)->value : '';
                                $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::GBP_ID)->value : '';
                                $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::GBP_ID)->value : '';

                                $backup[] = $temp;

                            }
                    }

                $countriesModels[$country_id] = $models;
                $countriesModelsOld[$country_id] = $modelsOld;
            }

        }




        if (isset($_POST['confirm-import'])) {
            if ($_POST['confirmation'] == 1) {

                $transaction = Yii::app()->db->beginTransaction();

                Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();

                $USER_GROUP_NULL = '';
                $USER_GROUP_RULE = [];
                if($user_group_id === NULL)
                    $USER_GROUP_NULL = 'user_group_id IS NULL';
                else
                    $USER_GROUP_RULE = ['user_group_id' => $user_group_id];

                $cpl = [];

                foreach($countriesModels AS $country_id => $item)
                {
                    // CLEAR OLD
                    $cdphg = CourierDeliveryPriceHasHub::model()->findAllByAttributes(['courier_country_group' => $country_id] + $USER_GROUP_RULE, $USER_GROUP_NULL);
                    $cpphg = CourierPickupPriceHasGroup::model()->findAllByAttributes(['courier_country_group' => $country_id] + $USER_GROUP_RULE, $USER_GROUP_NULL);

                    $pricesToDelete = [];
                    /* @var %temp CourierDeliveryPriceHasHub */
                    foreach($cdphg AS $temp)
                    {
                        $cpl[] = $temp->courier_price_item;

                        /* @var $cpi CourierPrice */
                        $cpi = $temp->courierPriceItem;
                        if($cpi)
                            $cpi->loadValueItems();

                        if($cpi)
                            foreach($cpi->courierPriceValues AS $cpv)
                            {
                                if($cpv->pricePerWeight)
                                    $cpv->pricePerWeight->deleteValues();
                                if($cpv->pricePerItem)
                                    $cpv->pricePerItem->deleteValues();

                                $pricesToDelete[] = $cpv->pricePerWeight;
                                $pricesToDelete[] = $cpv->pricePerItem;
                            }

                        $temp->delete();
                    }



                    /* @var %temp CourierPickupPriceHasGroup */
                    foreach($cpphg AS $temp)
                    {

                        $cpl[] = $temp->courier_price_item;

                        $cpi = $temp->courierPriceItem;
                        if($cpi)
                            $cpi->loadValueItems();

                        if($cpi)
                            foreach($cpi->courierPriceValues AS $cpv)
                            {
                                if($cpv->pricePerWeight)
                                    $cpv->pricePerWeight->deleteValues();
                                if($cpv->pricePerItem)
                                    $cpv->pricePerItem->deleteValues();

                                $pricesToDelete[] = $cpv->pricePerWeight;
                                $pricesToDelete[] = $cpv->pricePerItem;
                            }

                        $temp->delete();
                    }

                    foreach($pricesToDelete AS $ptd) {
                        if($ptd)
                            $ptd->delete();
                    }
//
                    $cmd = Yii::app()->db->createCommand();
                    foreach($cpl AS $temp)
                        $cmd->delete('courier_price_value', 'courier_price_id = :cpi', [':cpi' => $temp]);

//                    $temp = $item['P']['M']->loadValueItems();
//                    if(is_array($temp))
//                        foreach($temp AS $temp2)
//                            $temp2->delete();

                    if($item['P'])
                    {
                        if($item['P']['M'])
                            $item['P']['M']->deleteWithRelated();

                        if(S_Useful::sizeof($item['P']['PV'])) {

                            $cp = new CourierPrice();
                            $cp->save();

                            $model2 = new CourierPickupPriceHasGroup();
                            $model2->courier_country_group = $country_id;
                            $model2->courier_price_item = $cp->id;
                            $model2->user_group_id = $user_group_id;
                            if (!$model2->save())
                                $errors = true;

                            foreach ($item['P']['PV'] AS $key => $courierPriceValue) {
                                $courierPriceValue->price_per_item = $courierPriceValue->pricePerItem->saveWithPrices();
                                $courierPriceValue->price_per_weight = $courierPriceValue->pricePerWeight->saveWithPrices();

                                $courierPriceValue->courier_price_id = $cp->id;

//                            $courierPriceValues[$key] = $courierPriceValue;

                                if (!$courierPriceValue->save())
                                    $errors = true;
                            }
                        }
                    }



                    if(is_array($item))
                        foreach($item AS $hub_id => $item2)
                        {
                            if($hub_id == 'P')
                                continue;

                            if($item[$hub_id]['M'])
                                $item[$hub_id]['M']->deleteWithRelated();

                            if(S_Useful::sizeof($item[$hub_id]['PV'])) {

                                $cp = new CourierPrice();
                                $cp->save();

                                $model2 = new CourierDeliveryPriceHasHub();
                                $model2->courier_country_group = $country_id;
                                $model2->courier_price_item = $cp->id;
                                $model2->user_group_id = $user_group_id;
                                $model2->courier_pickup_hub = $hub_id;

                                if (!$model2->save())
                                    $errors = true;

                                foreach ($item[$hub_id]['PV'] AS $key => $courierPriceValue) {
                                    $courierPriceValue->price_per_item = $courierPriceValue->pricePerItem->saveWithPrices();
                                    $courierPriceValue->price_per_weight = $courierPriceValue->pricePerWeight->saveWithPrices();

                                    $courierPriceValue->courier_price_id = $cp->id;

//                                $courierPriceValues[$key] = $courierPriceValue;

                                    if (!$courierPriceValue->save())
                                        $errors = true;
                                }
                            }
                        }
                }


                Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS=1')->execute();

                if ($errors) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', 'Correct errrors!');

                } else {
//                             save backup
                    $backupText = '';
                    $backup = [[
                            'country_group_id',
                            'pickup/hub_id',
                            'weight_to',
                            'ppi_PLN',
                            'ppw_PLN',
                            'ppi_EUR',
                            'ppw_EUR',
                            'ppi_GBP',
                            'ppw_GBP',
                        ]] + $backup;
                    foreach ($backup AS $line)
                        $backupText .= implode(';', $line) . PHP_EOL;

                    $ugSufix = '';

                    if ($user_group_id)
                        $ugSufix = $user_group_id . '_';


                    $backupPath = 'courier_pricing_backup_' . $ugSufix . date('YmdHis') . '.csv';
                    file_put_contents($backupPath, $backupText);
                    $transaction->commit();
//

                    $orgFileData = Yii::app()->cacheUserData->get('CPI_ORG_FILE_'.$hash);
                    $backupFileData = $backupText;

                    $time = date('Y-m-d-H-i-s');
                    $filenameOrg = 'NEW_import_data_'.$time.'.'.Yii::app()->cacheUserData->get('CPI_ORG_FILE_EXT_'.$hash);
                    $filenameBackup = 'BACKUP_import_data_'.$time.'.csv';

                    $to = [
                        'jankopec@swiatprzesylek.pl',
                        'piotrkocon@swiatprzesylek.pl',
                        'maciejszostak@swiatprzesylek.pl',
                        'michalwojciechowski@swiatprzesylek.pl',
                    ];

                    S_Mailer::send($to, $to, '[Changes in PRE-ROUTING pricing][FILE]', 'Files in attachment<br/></br>User_group_id : '.$user_group_id.'<br/><br/>Admin_id : '.Yii::app()->user->id, false, NULL, NULL,true, false, [$filenameOrg, $filenameBackup], false, false, false, [$orgFileData, $backupFileData]);

                    Yii::app()->user->setFlash('success', 'Changes have been saved! Download previous setting backup.');
                    Yii::app()->session['CPI'] = $backupPath;
                    Yii::app()->cacheUserData->set('CPI_'.$hash, false);
                    $this->redirect(['/courier/courierPrice/import', 'hash' => false]);
                    Yii::app()->end();
                }

            } else
                Yii::app()->user->setFlash('error', 'Please know what you are doing :)');

        }


        $this->render('import', [
            'countriesModels' => $countriesModels,
            'countriesModelsOld' => $countriesModelsOld,
        ]);
    }


    public function actionExport()
    {

        $formModel = new F_CourierRoutingExport;

        if(isset($_POST['F_CourierRoutingExport'])) {
            $formModel->attributes = $_POST['F_CourierRoutingExport'];

            if($formModel->validate())
            {

                $country_list_id = implode(',', $formModel->country_list_id);
                $user_group_id = $formModel->user_group_id;

                if($user_group_id == -1)
                    $user_group_id = NULL;



                if($user_group_id === NULL)
                    $models = CourierCountryGroup::model()->findAll('parent_courier_country_group_id IS NULL AND user_group_id IS NULL AND id IN ('.$country_list_id.')');
                else
                    $models = CourierCountryGroup::model()->findAll('parent_courier_country_group_id IS NULL AND user_group_id = :ugi AND id IN ('.$country_list_id.')', [':ugi' => $user_group_id]);



                foreach ($models AS  $model) {

                    $country_id = $model->id;

                    $hubs = CHtml::listData(CourierHub::model()->findAll(), 'id', 'name');

                    $modelsOld = [];
                    $modelsOld['P'] = [];
                    $modelsOld['P']['M'] = CourierPrice::loadOrCreateDeliveryPrice(CourierPrice::MODE_PICKUP, $country_id, NULL, $user_group_id);
                    if ($modelsOld['P']['M'])
                        $modelsOld['P']['PV'] = $modelsOld['P']['M']->loadValueItems();

                    foreach ($hubs AS $hub_id => $hub_name) {
                        $modelsOld[$hub_id] = [];
                        $modelsOld[$hub_id]['M'] = CourierPrice::loadOrCreateDeliveryPrice(CourierPrice::MODE_DELIVERY, $country_id, $hub_id, $user_group_id);
                        if ($modelsOld[$hub_id]['M'])
                            $modelsOld[$hub_id]['PV'] = $modelsOld[$hub_id]['M']->loadValueItems();
                    }

                    if($model->isAvailableForPickup())
                        if (is_array($modelsOld['P']['PV']))
                            foreach ($modelsOld['P']['PV'] AS $weight => $item) {
                                /* @var $item CourierPriceValue */

                                if (!$item->weight_top_limit)
                                    continue;

                                $temp = [];
                                $temp[] = $country_id . '_' . $model->name;
                                $temp[] = 'P';
                                $temp[] = $item->weight_top_limit;
                                $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::PLN_ID)->value : '';
                                $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::PLN_ID)->value : '';
                                $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::EUR_ID)->value : '';
                                $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::EUR_ID)->value : '';
                                $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::GBP_ID)->value : '';
                                $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::GBP_ID)->value : '';

                                $backup[] = $temp;
                            }


                    if (is_array($modelsOld))
                        foreach ($modelsOld AS $hub_id => $item) {
                            if ($hub_id == 'P')
                                continue;

                            if (is_array($modelsOld[$hub_id]['PV']))
                                foreach ($modelsOld[$hub_id]['PV'] AS $weight => $item) {

                                    if (!$item->weight_top_limit)
                                        continue;

                                    $temp = [];
                                    $temp[] = $country_id . '_' . $model->name;
                                    $temp[] = $hub_id . '_HUB';
                                    $temp[] = $item->weight_top_limit;
                                    $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::PLN_ID)->value : '';
                                    $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::PLN_ID)->value : '';
                                    $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::EUR_ID)->value : '';
                                    $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::EUR_ID)->value : '';
                                    $temp[] = $item->pricePerItem ? $item->pricePerItem->getValue(Currency::GBP_ID)->value : '';
                                    $temp[] = $item->pricePerWeight ? $item->pricePerWeight->getValue(Currency::GBP_ID)->value : '';

                                    $backup[] = $temp;

                                }
                        }
                }

                $arrayHeader = ['country_group_id',
                    'pickup/hub_id',
                    'weight_to',
                    'ppi_PLN',
                    'ppw_PLN',
                    'ppi_EUR',
                    'ppw_EUR',
                    'ppi_GBP',
                    'ppw_GBP',];


                $writer = Box\Spout\Writer\WriterFactory::create(Box\Spout\Common\Type::XLSX); // for XLSX files
                $writer->openToBrowser('export.xls');
                $writer->addRow($arrayHeader);

                if(is_array($backup))
                    foreach($backup AS $key => $line)
                        $writer->addRow($line);

                $filename = 'export_courier_pricing_' . date('YmdHis') . '.xlsx';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0'); //no cache

                $writer->close();
                Yii::app()->end();
            }
        }

        $this->render('export', [
            'model' => $formModel,
        ]);
        exit;


    }
}