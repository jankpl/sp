<div id="tel-widget" class="fluid row">
    <div class="col-sm-7">
        <a href="mailto:<?php echo Yii::t('site', 'info@swiatprzesylek.pl');?>"><?php echo Yii::t('site', 'info@swiatprzesylek.pl');?></a>
    </div>
    <div class="col-sm-5">
        <?php echo Yii::t('site', '22 122 12 18');?>
    </div>
</div>