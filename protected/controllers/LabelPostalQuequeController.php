<?php

class LabelPostalQuequeController extends CController {

    const LIMIT = 50;

    public function filters()
    {
        return array();
    }

    // run queque
    public function actionIndex($id = NULL, $async = false, $counter = 0, $reverse = false)
    {
        $uniqid = uniqid().' ('.$_SERVER['SERVER_NAME'].')';
        $log = $uniqid.' : ID='.print_r($id,1).' ; ASYNC='.print_r($async,1).' ; COUNTER='.print_r($counter,1);

        if($async && $counter)
        {
            // slow down async process to let server rest...
            sleep(1 * $counter);
        }

        $counter++;

        if($counter > 60)
            exit;

        Yii::app()->getModule('postal');

        if($id === NULL) {

            $offset = 0;
            if($reverse == 1)
                $order = 'id DESC';
            else if($reverse == 2) {
                $order = 'id DESC';
                $offset = self::LIMIT;
            }
            else
                $order = 'id ASC';

            $parents = PostalLabel::model()->findAll(array('condition' => 'stat = :stat', 'params' => array(':stat' => PostalLabel::STAT_NEW), 'order' => $order, 'limit' => self::LIMIT, 'offset' => $offset));
        }
        else {
            $parents = PostalLabel::model()->findAll(array('condition' => 'stat = :stat AND id = :id ', 'params' => array(':stat' => PostalLabel::STAT_NEW, ':id' => $id), 'order' => 'id ASC', 'limit' => 1));
        }


        $log .= ' ; PARENTS SIZE: '.print_r(S_Useful::sizeof($parents,1));
        MyDump::dump('postal-label-queque.txt', $log);



        // no more found - finish the job...
        if(sizeof($parents) < 1)
            exit;


        $temp = '';
        foreach($parents AS $item)
            $temp .= $item->id.',';

        foreach($parents AS $parent)
            $parent->runLabelOrdering();

        sleep(1);


        MyDump::dump('postal-label-queque.txt', $uniqid.' : AFTER RUN LABEL ORDER');


        if($async)
            PostalLabel::asyncCallForId($id, $counter);
        else if(sizeof($parents) > self::LIMIT) {
            $path = Yii::app()->createAbsoluteUrl('/labelPostalQueque/index', array('id' => $id, 'counter' => $counter, 'reverse' => $reverse));
            header("Location: " . $path);
        }
        exit;
    }

}