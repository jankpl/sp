<?php

Yii::import('application.models._base.BaseUserLog');

class UserLog extends BaseUserLog
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public $_user;

    const CAT_USER_CHANGED = 1;
    const CAT_ADMIN_CHANGED = 2;

    public static function getCatList()
    {
        $data[self::CAT_USER_CHANGED] = 'by User';
        $data[self::CAT_ADMIN_CHANGED] = 'by Admin';

        return $data;
    }

    public function getCatName()
    {
        return self::getCatList()[$this->cat];
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

            array('user_id, text', 'required'),
            array('user_id, cat, admin_id', 'numerical', 'integerOnly'=>true),
            array('cat, admin_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, user_id, date_entered, text, cat, admin_id, _user', 'safe', 'on'=>'search'),
        );
    }

    public function beforeSave()
    {
        $this->text = strip_tags($this->text);
        $this->text = trim($this->text);

        return parent::beforeSave();
    }

    public static function addToLog($text, $user_id = false, $ignoreAdminAsAuthor = false)
    {
        $model = new self;
        $model->text = $text;
        if(Yii::app()->params['frontend'] == true)
        {
            $model->cat = self::CAT_USER_CHANGED;
            $model->user_id = $user_id ? $user_id : Yii::app()->user->id;

        } else {

            $model->cat = self::CAT_ADMIN_CHANGED;
            $model->user_id = $user_id;
            if(!$ignoreAdminAsAuthor)
                $model->admin_id = Yii::app()->user->id;
            $model->text = $text;
        }

        $result = $model->save();

        return $result;
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('cat', $this->cat);
        $criteria->compare('admin_id', $this->admin_id );

        if($this->_user)
        {
            $criteria->together  =  true;
            $criteria->with = array('user');
            $criteria->compare('user.login',$this->_user);
        }


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes = array(
            '_user' => array
            (
                'asc' => 'user.login asc',
                'desc' => 'user.login desc',
            ),
            '*', // add all of the other columns as sortable
        );

        return new CActiveDataProvider($this->with('admin')->with('user'), array(
            'criteria' => $criteria,
            'sort'=> $sort,
            'Pagination' => array (
                'PageSize' => 50,
            ),
        ));
    }
}