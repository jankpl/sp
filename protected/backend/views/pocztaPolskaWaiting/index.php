<h1>Poczta Polska - waiting</h1>


<table class="table">
    <tr>
        <th style="text-align: center;">Waiting - today</th>
        <th style="text-align: center;">Waiting - this week</th>
        <th style="text-align: center;">Waiting - total</th>
    </tr>
    <tr>
        <td style="text-align: center;"><?= PocztaPolskaWaiting::countFromToday(true);?></td>
        <td style="text-align: center;"><?= PocztaPolskaWaiting::countFromThisWeek(true);?></td>
        <td style="text-align: center;"><?= PocztaPolskaWaiting::countTotal(true);?></td>
    </tr>
    <tr>
        <th style="text-align: center;">Sent - today</th>
        <th style="text-align: center;">Sent - this week</th>
        <th style="text-align: center;">Sent - total</th>
    </tr>
    <tr>
        <td style="text-align: center;"><?= PocztaPolskaWaiting::countFromToday();?></td>
        <td style="text-align: center;"><?= PocztaPolskaWaiting::countFromThisWeek();?></td>
        <td style="text-align: center;"><?= PocztaPolskaWaiting::countTotal();?></td>
    </tr>
</table>

<br/>
<br/>


<div class="text-center">
    <a href="<?= Yii::app()->createUrl('/pocztaPolskaWaiting/admin');?>" class="btn btn-large">Manage</a> |
    <a href="<?= Yii::app()->createUrl('/pocztaPolskaWaiting/scanner');?>" class="btn btn-large">Scanner</a>
</div>