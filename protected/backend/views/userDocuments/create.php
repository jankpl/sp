<?php
/* @var $this UserDocumentsController */
/* @var $model UserDocuments */

$this->breadcrumbs=array(
	'User Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserDocuments', 'url'=>array('index')),
	array('label'=>'Manage UserDocuments', 'url'=>array('admin')),
);
?>

<h1>Create UserDocuments</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>