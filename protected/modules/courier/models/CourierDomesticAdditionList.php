<?php

Yii::import('application.modules.courier.models._base.BaseCourierDomesticAdditionList');

class CourierDomesticAdditionList extends BaseCourierDomesticAdditionList
{
    //ids from DB
    const INPOST_EXPRESS_17 = 1;
    const INPOST_EXPRESS_12 = 2;

    const INPOST_INSURANCE_5000 = 4;
    const INPOST_INSURANCE_10000 = 5;
    const INPOST_INSURANCE_20000 = 6;


    const INPOST_ROD = 7;

    const SAFEPAK_ROD = 8;

    const SAFEPAK_INSURANCE_5000 = 9;
    const SAFEPAK_INSURANCE_10000 = 10;
    const SAFEPAK_INSURANCE_20000 = 11;


    const GLS_ROD = 20;
    const GLS_DELIVERY_TO_10 = 21;
    const GLS_DELIVERY_TO_12 = 22;
    const GLS_DELIVERY_SATURDAY = 23;

    const UPS_DELIVERY_SATURDAY = 30;
    const UPS_ROD = 31;
    const UPS_NOTIFICATION = 32;

    const RUCH_INSURANCE = 40;

    const DHL_DOR_1822 = 41;
    const DHL_DOR_SB = 42;
    const DHL_NAD_SB = 43;
    const DHL_ROD = 44;

    const DHL_UBEZ = 47;


    const DHL_12 = 45;
    const DHL_09 = 46;



    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),


            array('name', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>45),
            array('date_updated', 'safe'),
            array('date_updated, stat', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, stat, broker_id, group', 'safe', 'on'=>'search'),
        );
    }

    public function getClientTitle()
    {
        return $this->courierDomesticAdditionListTr->title;
    }

    public function getClientDesc()
    {
        return $this->courierDomesticAdditionListTr->description;
    }

    public function toggleStat()
    {
        $this->stat = abs($this->stat - 1);

        return $this->save(false,array('stat'));
    }

    public function saveWithTr($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        if(!$this->save())
            $errors = true;

        foreach($this->courierDomesticAdditionListTrs AS $item)
        {

            $item->courier_domestic_addition_list_id = $this->id;
            if(!$item->save()) {
                $errors = true;
            }
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

    public static function getAdditionsByCourierDomestic(CourierTypeDomestic $courier)
    {

        // find universal additions
        $models = CourierDomesticAdditionList::model()->findAll('stat = '.S_Status::ACTIVE.' AND broker_id IS NULL');

        $temp = self::findOperatorAdditions($courier);
        $models = array_merge($models, $temp);

        return $models;
    }

    protected static function findOperatorAdditions(CourierTypeDomestic $courier)
    {

        switch($courier->getBrokerId())
        {
            case CourierDomesticOperator::BROKER_INPOST:
                return self::_findInpostAdditions($courier);
                break;
            case CourierDomesticOperator::BROKER_SAFEPAK:
                return self::_findSafepakAdditions($courier);
                break;
            case CourierDomesticOperator::BROKER_GLS:
                return self::_findGlsAdditions($courier);
                break;
            case CourierDomesticOperator::BROKER_UPS:
                return self::_findUpsAdditions($courier);
                break;
            case CourierDomesticOperator::BROKER_RUCH:
                return self::_findRuchAdditions($courier);
                break;
            case CourierDomesticOperator::BROKER_DHL:
                return self::_findDhlAdditions($courier);
                break;
            default:
                return array();
                break;
        }
    }

    protected static function _findInpostAdditions(CourierTypeDomestic $courier)
    {
        $additions = [];

        $services = Inpost::getServicesForCourierTypeDomestic($courier);


        if(isset($services[Inpost::SERVICE_EX_17]))
            $additions[] = self::INPOST_EXPRESS_17;

        if(isset($services[Inpost::SERVICE_EX_12]))
            $additions[] = self::INPOST_EXPRESS_12;

        if($courier->cod_value < 5000)
            $additions[] = self::INPOST_INSURANCE_5000;

        $additions[] = self::INPOST_INSURANCE_10000;

        $additions[] = self::INPOST_INSURANCE_20000;

        $additions[] = self::INPOST_ROD;

        $array = implode(',', $additions);

        if(!S_Useful::sizeof($additions))
            $models = [];
        else
            $models = CourierDomesticAdditionList::model()->findAll('stat = '.S_Status::ACTIVE.' AND id IN ('.$array.')');

        return $models;
    }

    protected static function _findSafepakAdditions(CourierTypeDomestic $courier)
    {
        $additions = [];


        $additions[] = self::SAFEPAK_ROD;

        if($courier->cod_value < 5000 && $courier->courier->package_value < 5000)
            $additions[] = self::SAFEPAK_INSURANCE_5000;

        if($courier->cod_value < 10000 && $courier->courier->package_value < 10000)
            $additions[] = self::SAFEPAK_INSURANCE_10000;

        if($courier->cod_value < 20000 && $courier->courier->package_value < 20000)
            $additions[] = self::SAFEPAK_INSURANCE_20000;

        $array = implode(',', $additions);

        if(!S_Useful::sizeof($additions))
            $models = [];
        else
            $models = CourierDomesticAdditionList::model()->findAll('stat = '.S_Status::ACTIVE.' AND id IN ('.$array.')');

        return $models;
    }

    protected static function _findGlsAdditions(CourierTypeDomestic $courier)
    {
        $zip_code = $courier->courier->receiverAddressData->zip_code;

        $CACHE_NAME = 'GLS_ADDITIONS_DOMESTIC_'.$zip_code;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if($models === false) {
            $additions = [];

            $services = GlsClient::getPremiumServices($zip_code);

            if ($services) {

                if ($services->{(GlsClient::OPTION_ROD)})
                    $additions[] = self::GLS_ROD;

                if ($services->{(GlsClient::OPTION_DEVLIVERY_TO_12)})
                    $additions[] = self::GLS_DELIVERY_TO_12;

                if ($services->{(GlsClient::OPTION_DEVLIVERY_TO_10)})
                    $additions[] = self::GLS_DELIVERY_TO_10;

                if ($services->{(GlsClient::OPTION_SATURDAY)})
                    $additions[] = self::GLS_DELIVERY_SATURDAY;

            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = CourierDomesticAdditionList::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND id IN (' . $array . ')');


            Yii::app()->cache->set($CACHE_NAME, $models, 60*15);
        }

        return $models;
    }

    protected static function _findUpsAdditions(CourierTypeDomestic $courier)
    {
        $additions = [];

        if(in_array($courier->domestic_operator_id, [CourierDomesticOperator::OPERATOR_UPS_EXPRESS, CourierDomesticOperator::OPERATOR_UPS_M_EXPRESS, CourierDomesticOperator::OPERATOR_UPS_SAVER]))
            $additions[] = self::UPS_DELIVERY_SATURDAY;

        $additions[] = self::UPS_ROD;
        $additions[] = self::UPS_NOTIFICATION;

        $array = implode(',', $additions);

        if(!S_Useful::sizeof($additions))
            $models = [];
        else
            $models = CourierDomesticAdditionList::model()->findAll('stat = '.S_Status::ACTIVE.' AND id IN ('.$array.')');

        return $models;
    }

    protected static function _findRuchAdditions(CourierTypeDomestic $courier)
    {
        $additions = [];


        $additions[] = self::RUCH_INSURANCE;

        $array = implode(',', $additions);

        if(!S_Useful::sizeof($additions))
            $models = [];
        else
            $models = CourierDomesticAdditionList::model()->findAll('stat = '.S_Status::ACTIVE.' AND id IN ('.$array.')');

        return $models;
    }

    protected static function _findDhlAdditions(CourierTypeDomestic $courier)
    {

        $courier = $courier->courier;

        $CACHE_NAME = 'DHL_ADDITIONS_DOMESTIC_'.$courier->senderAddressData->zip_code.'_'.$courier->receiverAddressData->zip_code.'_'.$courier->courierTypeDomestic->domestic_operator_id;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $servicesSender = DhlClient::getServicesForZipCode($courier->senderAddressData->zip_code, date('Y-m-d'));
            $servicesReceiver = DhlClient::getServicesForZipCode($courier->receiverAddressData->zip_code, date('Y-m-d'));

            $additions = [];

            $additions[] = self::DHL_ROD;
            $additions[] = self::DHL_UBEZ;

            if($courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_PALETA && $courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_F_PALETA)
                if($servicesReceiver[DhlClient::OPT_DOR_SB])
                    $additions[] = self::DHL_DOR_SB;

            if($courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_PALETA && $courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_F_PALETA)
                if($servicesReceiver[DhlClient::OPT_DOR_1822])
                    $additions[] = self::DHL_DOR_1822;

            if($courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_PALETA && $courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_F_PALETA)
                if($servicesSender[DhlClient::OPT_DOM09] && $servicesReceiver[DhlClient::OPT_DOM09])
                    $additions[] = self::DHL_09;

            if($courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_PALETA && $courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_F_PALETA)
                if($servicesSender[DhlClient::OPT_DOM12] && $servicesReceiver[DhlClient::OPT_DOM12])
                    $additions[] = self::DHL_12;

//            $additions[] = self::DHL_CONNECT;

            if($courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_PALETA && $courier->courierTypeDomestic->domestic_operator_id != CourierDomesticOperator::OPERATOR_DHL_F_PALETA)
                if($servicesSender[DhlClient::OPT_NAD_SB])
                    $additions[] = self::DHL_NAD_SB;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = CourierDomesticAdditionList::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

}