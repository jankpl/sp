<?php
/* @var $text string */
/* @var $model Courier */
/* @var $status string */
/* @var $addEnAnnouncement boolean */
/* @var $addEnIntro boolean */
?>
<?php if($addEnAnnouncement):?>
    <div style="font-style: italic; font-size: 0.8em;">
        ---------------------------------<br/>
        <?= Yii::t('mail_postal', 'English version is on the bottom of this email [{a}go to english version{/a}].', ['{a}' => '<a href="#en">', '{/a}' => '</a>']); ?>
        <br/>---------------------------------
    </div><br/>
<?php endif; ?>
<?php if($addEnIntro):?>
<br/>
<div style="font-size: 0.8em;" id="en">
    <br/>---------------------------------<br/>
    <?php endif; ?>
    <?= $text;?><br/><br/>
    <strong style="padding-left: 15px; font-size: 1.2em;"><?= $status; ?></strong><br/><br/>

    <?php
    $baseUrl = PageVersion::lang2domain(Yii::app()->language);
    echo Yii::t('mail_postal', 'Paczkę możesz śledzić {a}tutaj{/a}.', array('{a}' => '<a href="'.$baseUrl.'/tt/'.$model->local_id.'">', '{/a}' => '</a>')); ?><br/><br/>

    <br/><br/>
    <?php
    if($model->senderAddressData !== NULL):
        ?>
        <strong><?= Yii::t('mail_postal', 'Nadawca paczki:');?></strong><br/>
        <?= $model->senderAddressData->getUsefulName(); ?><br/>
        <?= $model->senderAddressData->address_line_1; ?><br/>
        <?= $model->senderAddressData->address_line_2 != '' ? $model->senderAddressData->address_line_2.'<br/>' : '';?>
        <?= $model->senderAddressData->city;?>, <?= $model->senderAddressData->zip_code;?><br/>
        <?= $model->senderAddressData->getCountryNameUniversal();?><br/><br/>
        <?php
    endif;
    ?>
    <?php
    if($model->receiverAddressData !== NULL):
        ?>
        <strong><?= Yii::t('mail_postal', 'Odbiorca paczki:');?></strong><br/>
        <?= $model->receiverAddressData->getUsefulName(); ?><br/>
        <?= $model->receiverAddressData->address_line_1; ?><br/>
        <?= $model->receiverAddressData->address_line_2 != '' ? $model->receiverAddressData->address_line_2.'<br/>' : '';?>
        <?= $model->receiverAddressData->city;?>, <?= $model->receiverAddressData->zip_code;?><br/>
        <?= $model->receiverAddressData->getCountryNameUniversal();?>
        <?php
    endif;
    ?>
    <?php if($addEnIntro):?>
    <br/>---------------------------------<br/>
</div>
<?php endif; ?>



