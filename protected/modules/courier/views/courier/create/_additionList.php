<?php
/* @var $model Courier */
/* @var $this CourierController */
/* @var $modelCourierAdditionList CourierAdditionList[] */
/* @var $courierAddition CourierAddition[] */
/* @var $form CActiveForm */
/* @var $specialOption string */
?>
<div class="form">
    <table class="detail-view" style="width: 100%;">
        <tr class="odd">
            <th style="width: 50px; text-align: center;"><?php echo Yii::t('courier','Wybierz');?></th>
            <th style="width: 60%; text-align: left;"><?php echo Yii::t('courier','Dodatek');?></th>
            <?php
            if(!Yii::app()->user->model->getHidePrices()):
                ?>
                <th style="text-align: right; width: 150px; padding-right: 10px;"><?php echo Yii::t('courier','Cena netto/brutto');?> <?php echo Yii::t('courier','za pojedyczną paczkę');?></th>
            <?php
            endif;
            ?>
        </tr>
        <?php
        $bg = '';
        if(is_array($modelCourierAdditionList))
            foreach($modelCourierAdditionList AS $item):
                if($bg == 'even')
                    $bg = 'odd';
                else
                    $bg = 'even';

                ?>
                <tr class="addition_item <?= $bg;?>">
                    <td class="with-checkbox text-center">
                        <?php echo CHtml::checkBox('CourierAddition[]', in_array($item->id,$courierAddition), array('value' => $item->id, 'data-courier-addition' => 'true', 'data-group' => $item->group, 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak'), 'data-addition-id' => $item->id)); ?>
                    </td>
                    <td>
                        <label style="font-weight: normal; text-align: left; cursor: pointer; width: 100%;"><h4><?php echo $item->courierAdditionListTr->title;?> (#<?=$item->id;?>)</h4>
                            <p><?php echo $item->courierAdditionListTr->description; ?></p></label>
                    </td>
                    <?php
                    if(!Yii::app()->user->model->getHidePrices()):
                        ?>
                        <td style="text-align: right;  padding-right: 10px;">
                            <?php
                            echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice(), true)).' '.Yii::app()->PriceManager->getCurrencySymbol().'/'.S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($item->getPrice())).' '.Yii::app()->PriceManager->getCurrencySymbol();
                            ?>
                        </td>
                    <?php
                    endif;
                    ?>
                </tr>
            <?php
            endforeach;
        ?>
    </table>
    <?php //echo $specialOption; ?>
</div><!-- form -->