<?php

class SwiatSkanera extends CFormModel
{
    public $status_id;
    public $tracking_id;
    public $location;
    public $date;

    const KEY = 'SfBy7Nk7A1234vSa6hvsAfE721AfrasQ';

    const TYPE_COURIER = 1;
    const TYPE_POSTAL = 2;

    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
        );
    }


    public static function getStatusesList()
    {
        $data = [
//            StatMap::MAP_NEW => '1: NEW ORDER',
            StatMap::MAP_ACCEPTED => '2: ACCEPTED',
            StatMap::MAP_DELIVERED_TO_HUB => '10: IN HUB',
            StatMap::MAP_IN_TRANSPORTATION => '20: IN TRANSPORTATION',
            StatMap::MAP_IN_DELIVERY => '30: IN DELIVERY',
            StatMap::MAP_DELIVERED => '40: DELIVERED',
            StatMap::MAP_PROBLEM => '50: PROBLEM',
            StatMap::MAP_RETURN => '60: RETURN',
            StatMap::MAP_CANCELLED => '90: CANCELED',
        ];

        return $data;
    }

    public function rules() {
        return array(
            array('tracking_id, status_id', 'required'),
            array('location, tracking_id', 'length', 'max' => 512),
            array('status_id', 'numerical', 'integerOnly' => true),
            array('status_id', 'in', 'range' => array_keys(self::getStatusesList())),
            array('date', 'date', 'format' => 'yyyy-M-d H:m:s'),

            array('tracking_id, location', 'filter', 'filter' => array( $this, 'filterStripTags')),
        );
    }

    public function beforeValidate()
    {
        $this->tracking_id = trim($this->tracking_id);
        $this->location = trim($this->location);

        if($this->date == '')
            $this->date = NULL;

        if(mb_substr($this->tracking_id, 0, 3) == 'JGB' && strlen($this->tracking_id) > 100 && preg_match('/(483([0-9]{12}))$/', $this->tracking_id, $match)) // decode Royal Mail QR CODE
        {
            $this->tracking_id = $match[0];
        }

        return parent::beforeValidate();
    }

    public function attributeLabels() {
        return array(
        );
    }

    public static function returnCourierPostalModels($no)
    {
        Yii::app()->getModule('courier');
        Yii::app()->getModule('postal');

//        if($no == 'CA061287695IE')
//            return Courier::model()->findAllByPk('2404918');
//        else
//            return [];

        if(preg_match('/483[0-9]{12}/i', $no))
        {
            $models = Courier::model()->findAllByAttributes(['local_id' => $no]);
            return $models;
        }
        else if(preg_match('/4[0-9]{11}[A-Z]+[0-9]+[A-Z]+/i', $no))
        {
            $models = Postal::model()->findAllByAttributes(['local_id' => $no]);
            return $models;
        }
        else {
            $models = CourierExternalManager::findCourierIdsByRemoteId($no, true);
            if($models)
                return $models;

            $models = PostalExternalManager::findPostalIdsByRemoteId($no);

            if($models)
                return $models;
        }

        return [];
    }

    public function convertMapIdToStatId($mapId, $type)
    {
        switch($mapId)
        {
            case StatMap::MAP_NEW:
                if($type == self::TYPE_COURIER)
                    return 1;
                else if($type == self::TYPE_POSTAL)
                    return 1;
                break;

            case StatMap::MAP_ACCEPTED:
                if($type == self::TYPE_COURIER)
                    return 640;
                else if($type == self::TYPE_POSTAL)
                    return 3488;
                break;

            case StatMap::MAP_DELIVERED_TO_HUB:
                if($type == self::TYPE_COURIER)
                    return 70;
                else if($type == self::TYPE_POSTAL)
                    return 405;
                break;

            case StatMap::MAP_IN_TRANSPORTATION:
                if($type == self::TYPE_COURIER)
                    return 514;
                else if($type == self::TYPE_POSTAL)
                    return 152;
                break;

            case StatMap::MAP_IN_DELIVERY:
                if($type == self::TYPE_COURIER)
                    return 334;
                else if($type == self::TYPE_POSTAL)
                    return 71426;
                break;

            case StatMap::MAP_DELIVERED:
                if($type == self::TYPE_COURIER)
                    return 3;
                else if($type == self::TYPE_POSTAL)
                    return 3;
                break;

            case StatMap::MAP_PROBLEM:
                if($type == self::TYPE_COURIER)
                    return 147;
                else if($type == self::TYPE_POSTAL)
                    return 156;
                break;

            case StatMap::MAP_RETURN:
                if($type == self::TYPE_COURIER)
                    return 150;
                else if($type == self::TYPE_POSTAL)
                    return 261;
                break;

            case StatMap::MAP_CANCELLED:
                if($type == self::TYPE_COURIER)
                    return 99;
                else if($type == self::TYPE_POSTAL)
                    return 99;
                break;
        }

        return NULL;
    }

    public function setStatusForItem($justFirst = false)
    {
        $models = self::returnCourierPostalModels($this->tracking_id);

        if(!is_array($models) OR !S_Useful::sizeof($models))
            throw new Exception('ITEM NOT FOUND!');

        if(!$this->validate())
            throw new Exception('GOT BAD DATA!');

        if($justFirst)
            $models = [ array_shift($models) ];
        else if(S_Useful::sizeof($models) > 1)
            throw new Exception('MORE THEN 1 ITEM FOUND!');


        $success = true;
        foreach($models AS $model)
        {
            $type = self::detectModelType($model);
            $stat_id = self::convertMapIdToStatId($this->status_id, $type);

            if($type == self::TYPE_COURIER)
            {
                /* @var $model Courier */
                if(!$model->changeStat($stat_id, 7000, $this->date, false, $this->location))
                    $success = false;
            }
            else if($type == self::TYPE_POSTAL)
            {
                /* @var $model Postal */
                if(!$model->changeStat($stat_id, 7000, $this->date, false, $this->location))
                    $success = false;
            }
        }

        return $success;
    }

    public static function detectModelType($model)
    {
        if($model instanceof Courier)
            return self::TYPE_COURIER;
        else if($model instanceof Postal)
            return self::TYPE_POSTAL;
        else
            throw new Exception('UNKNOWN PACKAGE TYPE!');
    }

    public static function getCurrentPassword()
    {
        return md5(date('Ymd'));

        $pass = hash('sha256',(date('Ymd').self::KEY));
        return $pass;
    }

}