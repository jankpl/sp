<?php

Yii::import('application.models._base.BaseLanguage');

/**
 * Class Language
 * Stat = 0 - inactive, = 1 - active, = 2 - active, just partial inline translations
 */
class Language extends BaseLanguage
{



    const LANG_PL = 'pl';
    const LANG_EN = 'en';
    const LANG_DE = 'de';
    const LANG_UA = 'uk';

    const LANG_ID_EN = 2;

    const DEFAULT_LANG = 'pl';

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function languagesArray()
    {
        return array(self::LANG_PL, self::LANG_EN);
    }

    public static function isLanguageCodeValid($code)
    {
        return true;
//        return in_array($code, self::languagesArray());
    }


    private static $_model = [];
    protected static function autoReturnModel($lang)
    {
        $lang = explode('-', $lang);
        $lang = $lang[0];

        if(!isset(self::$_model[$lang]) OR !(self::$_model[$lang] instanceof Language)) {

//        $model = Language::model()->cache(1000)->find('code LIKE :lang AND stat = 1', array(':lang' => $lang.'%'));
//            $model = Language::model()->find('code LIKE :lang AND stat > 0', array(':lang' => $lang . '%'));
            $model = Language::model()->find('code = :lang AND stat > 0', array(':lang' => $lang));

//        if($model === null) $model = Language::model()->cache(1000)->find('stat = 1', array('order' => 'id ASC'));
            if ($model === null)
                $model = Language::model()->find('stat > 0', array('order' => 'id ASC'));

            self::$_model[$lang] = $model;
        }

        return self::$_model[$lang];
    }

    public static function autoLanguageIdReturn($lang)
    {
        $model = Language::autoReturnModel($lang);

        return $model->id;
    }

    public static function autoLanguageCodeReturn($lang)
    {
        $model = Language::autoReturnModel($lang);

        return $model->code;
    }
}