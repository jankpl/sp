<?php
/* @var $model CourierPrice */
/* @var $models CourierPrice[] */
/* @var $this CourierPriceController */
?>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>



<fieldset>
    <legend><?php echo Pricing::getProductType()[Pricing::PRODUCT_PP_LOCAL];?></legend>
    <?php echo CHtml::link('weight prices',array('weight', 'id' => Pricing::PRODUCT_PP_LOCAL), array('class' => 'likeButton')); ?>
    <?php echo CHtml::link('options prices',array('options', 'id' => Pricing::PRODUCT_PP_LOCAL), array('class' => 'likeButton')); ?>
</fieldset>


<fieldset>
    <legend><?php echo Pricing::getProductType()[Pricing::PRODUCT_PP_ALTERNATIVE];?></legend>
    <?php echo CHtml::link('weight prices',array('weight', 'id' => Pricing::PRODUCT_PP_ALTERNATIVE), array('class' => 'likeButton')); ?>
    <?php echo CHtml::link('options prices',array('options', 'id' => Pricing::PRODUCT_PP_ALTERNATIVE), array('class' => 'likeButton')); ?>
</fieldset>


<fieldset>
    <legend><?php echo Pricing::getProductType()[Pricing::PRODUCT_COURIER];?></legend>
    <?php echo CHtml::link('weight prices',array('weight', 'id' => Pricing::PRODUCT_COURIER), array('class' => 'likeButton')); ?>
    <?php echo CHtml::link('options prices',array('options', 'id' => Pricing::PRODUCT_COURIER), array('class' => 'likeButton')); ?>
</fieldset>