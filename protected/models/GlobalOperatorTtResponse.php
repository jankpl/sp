<?php

class GlobalOperatorTtResponse
{
    protected $status;
    protected $date;
    protected $location;

    public function geStatus()
    {
        return $this->status;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public static function createSuccessResponse($status, $date, $location)
    {
        $model = new self;
        $model->status = $status;
        $model->date = $date;
        $model->location = $location;

        return $model;
    }

    public function adapterToCourierExternalManagerArray()
    {

    }
}