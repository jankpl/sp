<?php

class PostalOperatorController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules() {
		return array(
			array('allow',
				'users'=>array('@'),
				'expression'=>'0 >= Yii::app()->user->authority',
			),
			array('deny',  // block rest of actions
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id)
	{
		$this->redirect(['admin']);
	}

	public function actionCreate()
	{
		$model=new PostalOperator;
		$model->returnAddressData = new AddressData();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PostalOperator']))
		{
			$model->attributes=$_POST['PostalOperator'];
			$model->returnAddressData->setAttributes($_POST['AddressData']);
			if($model->saveWithRelatedModels())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		/* @var $model PostalOperator */
		$model = $this->loadModel($id, 'PostalOperator');

		if($model->returnAddressData === NULL)
			$model->returnAddressData = new AddressData();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PostalOperator']))
		{
			$model->attributes=$_POST['PostalOperator'];
			$model->returnAddressData->setAttributes($_POST['AddressData']);
			if($model->saveWithRelatedModels())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionIndex()
	{
		$this->redirect(['admin']);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PostalOperator('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PostalOperator']))
			$model->attributes=$_GET['PostalOperator'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionAjaxPrepareImage()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			try {
				$img = $_POST['img'];

				$img = explode(',', $img);

				if(!isset($img[1]))
					throw new Exception;

				$img = $img[1];

				$img = base64_decode($img);

				if($img == '')
					throw new Exception;

				$im = ImagickMine::newInstance();
				if(!$im->readImageBlob($img))
					throw new Exception;

				if ($im->getImageWidth() > PostalOperator::STAMP_MAX_WIDTH)
					$im->scaleImage(PostalOperator::STAMP_MAX_WIDTH, 0);

				if ($im->getImageHeight() > PostalOperator::STAMP_MAX_HEIGHT)
					$im->scaleImage(0, PostalOperator::STAMP_MAX_HEIGHT);


				$im->setFormat('png');

				$imgString = $im->__tostring();
				$imgString = base64_encode($imgString);
				$imgString = 'data:' . $im->getImageMimeType() . ';base64,' . $imgString;

				echo CJSON::encode(['image' => $imgString]);
			}
			catch(Exception $ex)
			{
				echo CJSON::encode(['image' => false]);
			}
		} else {
			$this->redirect(['/']);
		}
	}
}
