<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>


<h1>Welcome</h1>
<h3>Please log in!</h3>

<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>false,
		'clientOptions'=>array(
			'validateOnSubmit'=>false,
		),
	)); ?>

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username', ['autofocus' => true, 'style' => 'text-align: center;']); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'password'); ?>
		<?php echo $form->passwordField($model,'password', ['style' => 'text-align: center;']); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	<br/>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Login', ['style' => 'background: white; color: black']); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
