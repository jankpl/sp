<?php
/* @var $form CActiveForm */
/* @var $model AddressData */
/* @var $i Integer */
/* @var $type String */
/* @var $countries CountryList[] */

if(!isset($type))
    $type = key(UserContactBook::getTypes_enum());
?>



<?php echo $form->errorSummary($model); ?>

<?php

$this->widget('AddressDataSuggester',
    array(
        'type' => $type,
        'fieldName' => 'HybridMail_AddressData',
        'i' => $i,
    ))
?>

<?php $this->renderPartial('_addressData/_form_base',
    array('i' => $i, 'model' => $model, 'form' => $form, 'countries' => $countries));
?>

<?php
if(!Yii::app()->user->isGuest)
{
    ?>

    <div class="row">
        <?php echo CHtml::label(Yii::t('_addressData','Dodaj do swojej książki adresowej'),'Other[saveToContactBook]'.(isset($i)?'['.$i.']':'')); ?>
        <?php echo CHtml::checkBox('Other[saveToContactBook]'.(isset($i)?'['.$i.']':''), $saveToContactBook?true:false , array("id" => 'CB_saveToContactBook') ); ?>
    </div><!-- row -->


<?php

}
?>

<p class="note">
    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
</p>