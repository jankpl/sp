<?php

class B2CEuropeCorreosClient extends GlobalOperator
{
    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $weight;
    public $value;
    public $content;

    public $cod = 0;
    public $cod_currency;
    public $size_l;
    public $size_d;
    public $size_w;

    public $local_id;
//
    public static function test()
    {
        $model = new self;

        $model->sender_name = 'Test';
        $model->sender_address1 = 'Test str 1';
        $model->sender_zip_code = '02627';
        $model->sender_city = 'Madrit';
        $model->sender_country = 'ES';

        $model->receiver_name = 'Test';
        $model->receiver_address1 = 'Test str 1';
        $model->receiver_zip_code = '02627';
        $model->receiver_city = 'Madrit';
        $model->receiver_country = 'ES';
        $model->receiver_mail = 'test@testtesttest.com';
        $model->receiver_tel = 123123123;

        $model->weight = 2;
        $model->cod_currency = 'EUR';
        $model->size_l = '10';
        $model->size_d = '10';
        $model->size_w = '10';

        $model->cod = 0;

        $model->value = 10;

       return $model->_createShipment();
    }

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

//    protected function _createShipment(Courier $courier, AddressData $from, AddressData $to, $returnError = false)
    protected function _createShipment( $returnError = false)
    {
        $label = self::_generateLabel(123, 456);

        $im = ImagickMine::newInstance();
        $im->setResolution(300,300);

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $im->stripImage();

        file_put_contents('cl.png', $im->getImageBlob());
exit;
        $item = \FluidXml\fluidxml('FinalMileLabelCreateRequest');

        var_dump($xml);
        exit;

        $resp = $this->_curlCall($xml);



        if ($resp->success == true) {
            $return = [];

            $TYPNumber = $resp->data->Shipment->TYPNumber;
            $orderNumber = $resp->data->Shipment->OrderNumber;

            $label = self::_generateLabel($TYPNumber, $orderNumber);


            $im = ImagickMine::newInstance();
            $im->setResolution(300,300);

            $im->readImageBlob($label);
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $this->local_id);

            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }

    }

    protected function _curlCall($data)
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

//        $dataOryginal = $data;

        $headers = [];
        $headers[] = 'Content-Type: text/xml';

        $curl = curl_init(self::URL);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt_array($curl, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        );
        $resp = curl_exec($curl);

        MyDump::dump('b2ceurope_fml.txt', print_r($data,1));
        MyDump::dump('b2ceurope_fml.txt', print_r($resp,1));

        var_dump($resp);
        exit;

        $xml = @simplexml_load_string($resp);

        if($xml->Result == 'OK')
        {
            $return->success = true;
            $return->data = $xml;
        }
        else
        {
            if ($xml == '')
            {
                $error = curl_error($curl);
                if ($error == '')
                    $error = 'Unknown error';

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
            else
            {
                $error = $xml->Message . ' : ' . $xml->Details;;

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
        }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->getCodeIso3();

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->getCodeIso3();
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->weight = $courierInternal->courier->getWeight(true);
        $model->content = $courierInternal->courier->package_content;
        $model->value = $courierInternal->courier->package_value;
        $model->size_l = $courierInternal->courier->package_size_l;
        $model->size_d = $courierInternal->courier->package_size_d;
        $model->size_w = $courierInternal->courier->package_size_w;
        $model->cod = $courierInternal->courier->cod_value;
        $model->cod_currency = $courierInternal->courier->cod_currency;
        $model->local_id = $courierInternal->courier->local_id;



        return $model->_createShipment($courierInternal->courier, $from, $to, $returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->getCodeIso3();

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->getCodeIso3();
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->weight = $courierU->courier->getWeight(true);
        $model->content = $courierU->courier->package_content;
        $model->value = $courierU->courier->package_value;
        $model->size_l = $courierU->courier->package_size_l;
        $model->size_d = $courierU->courier->package_size_d;
        $model->size_d = $courierU->courier->package_size_w;
        $model->cod = $courierU->courier->cod_value;
        $model->cod_currency = $courierU->courier->cod_currency;
        $model->local_id = $courierU->courier->local_id;

        return $model->_createShipment($courierU->courier, $from, $to, $returnErrors);
    }


    protected function _generateLabel($TYPNumber, $orderNumber)
    {


        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pageW = 150;
        $pageH = 100;

        $border = 0;

        /* @var $pdf TCPDF */

        $pdf->AddPage('H', array($pageW, $pageH), true);

//        $pdf->SetLineWidth(0.5);
//        $pdf->MultiCell($pageW - 1, $pageH - 1, '', 1, 'L', false);
        $pdf->SetLineWidth(0.1);

        $pdf->Image('@' . self::_getLogo(), 2, 2, 30, '', '', 'N', false);

        $pdf->Image('@' . self::_getStdLogo(), 50, 1, 42, '', '', 'N', false);

        $pdf->Image('@' . self::_getToHomeLogo(), 100, 30, 25, '', '', 'N', false);

        $pdf->Image('@' . self::_getFromToLogo(), 2, 15, 3, '', '', 'N', false);




        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(60, 5, 'EXPEDITION: PQ43BG041097956T', $border, 'L', false, 1, 80, 10,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->MultiCell(60, 5, 'B2C Ref: ABC 132465', $border, 'L', false, 1, 80, 15,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->MultiCell(20, 10, "Peso:\r\n320 grs", $border, 'L', false, 1, 120, 65,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );

        $pdf->MultiCell(20, 15, "Fetcha\r\nEtiquetado\r\n".date('d/m/Y'), $border, 'L', false, 1, 120, 80,
            true,
            0,
            false,
            true,
            15,
            'T',
            true
        );

        if($this->cod > 0) {

            $pdf->Image('@' . self::_getCodLogo(), 90, 23, 8, '', '', 'N', false);


            $pdf->SetFont('freesans', '', 12);
            $pdf->MultiCell(25, 5, 'Importe a cobrar 1 €', $border, 'L', false, 1, 100, 25,
                true,
                0,
                false,
                true,
                5,
                'T',
                true
            );
        }

              $text = '';
        $text .= $this->sender_name ? $this->sender_name."\r\n" : '';
        $text .= $this->sender_company ? $this->sender_company."\r\n" : '';
        $text .= $this->sender_address1."\r\n";
        $text .= $this->sender_address2 ? $this->sender_address2."\r\n" : '';
        $text .= $this->sender_zip_code."\r\n";
        $text .= $this->sender_city."\r\n";
        $text .= $this->sender_country."\r\n";

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(70, 20, $text, $border, 'L', false, 1, 7, 14,
            true,
            0,
            false,
            true,
            20,
            'T',
            true
        );




        $text = '';
        $text .= $this->receiver_name ? $this->receiver_name."\r\n" : '';
        $text .= $this->receiver_company ? $this->receiver_company."\r\n" : '';
        $text .= $this->receiver_address1."\r\n";
        $text .= $this->receiver_address2 ? $this->receiver_address2."\r\n" : '';
        $text .= $this->receiver_zip_code."\r\n";
        $text .= $this->receiver_city."\r\n";
        $text .= $this->receiver_country."\r\n";

        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(70, 20, $text, $border, 'L', false, 1, 7, 36,
            true,
            0,
            false,
            true,
            20,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(80, 5, 'Código Bulto: PQ43BG0410979560128030M', $border, 'L', false, 1, 9, 59,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );



        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $pdf->write1DBarcode('PQ43BG0410979560128030M', 'C128', 5, 62, '', 35, 0.4, $barcodeStyle, 'N');



        return $pdf->Output('SP.pdf', 'S');
    }

    protected static function _getFromToLogo()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAACgAAAI5CAMAAADkNYhIAAAC/VBMVEX///8AAACLi4v/+8yLIwDMzMytra0AACPn5udereKLzPMAAED///T/5q1eAADMi0D9///n//8BXq3///gkAAAAAAwMAABeXl4AABrj9v3//e/+9+n//d7//uXu/f/z/f/u9/398dMzCwDf8PzM5Par0+7pzp8ADEcADDv89OJlNRn3//+43/f//un88d1nl8LcyLHRvqkAB1gADDPy4c8jToCj3PbiwpiLi5fHrY9BZ4OtflIAI0VwQB1LIwxBIwye2ffR5fSftsysrrfMsK24j4vmrmkAJWcMM1lWBAC43PD/++T//NLr1cD/773926AlV4v0xIK2nH0ADGOQdleIZ0YAADHM+v++7v1yuup8vee+0OL369izxthVnczgpmG4i1vRkk7AgEkAAEh2WTo8AgDf/v/Z7Pi92fDT5ebm5t2sxttVotr35tBwosuOqL7j0b375bp8mbUOarTkxqD81ZdVeJUMQHWni2mafFkCJlG6dDt8VzKaYDG2ay1ySyh9QR12KAxPCAAdAADK7fqs5Pqb1PHv5+bM1ubezcwji8zr3MubqbeIn7eyrKPRuqIDTaHPuJ1AdZrAqJBLcJAAI4sMQYXuu3gzVXbDlmwjQWidhGesZjiYPhKPNg/1///r8/u/5Pa4z+KBt9tGnNapvdCVsMdLi7Xu1rJzkagAVZ22opYMTpb6zItdcoAALH/Bi1u4fTqQZzaHWjSnViBVMxm37PzV7Pjj7vat2/N3q9BOltAwhsj86cQaeL8oeb49frtnhKF4jZ5nhJ01Z5o1XoPMpXkAGXkjR3iuknGokXEAD3GHe20MNGmEVCxoQSV2FwA1DABFAADX/f/U9/6t1fWkzOiYy+jI1+SXwdh+rdM2lNLAytFgkLgmbLJoi5sAO5XQqZKonZKLi5JBb5LXvXxRanxZX3DQmGIjQF6ScE6td0heWEVBI0HCfTqsXyeYUiIzLxFXJQzQ2eHm4tnZ0MPMx73CuLhOfq1Ld6WLl6L31JmKi5dLXF4jI0BsOBXzAAAHCElEQVR42u2bc/TcQBDHO7WbtrnrVb/atm3btm3btm3btm3btvu62+xlLneb7rXvtc21+/3r8MlmmZ3ZmfjzfYUMGjRodPwaNXfgwPW5YKjQECI4fg0QEIIEY5/Dpw4UqH8iL8CQYQDChZWgBH8LWDWopoYrABY2CMoUt4A7aKrfDEbJ499c6RP7s4LC9wsadLqAwZGR4B8FHSMDmyhXbEvMcFrHeYHMFMsq/fgfgn9RkQYE+oFiWWLiUrvHU0NX2ilYC0G+klZXCWZLE0f58bM1bhYgip8sohWLC9nau+JiZPKuuHQJf6a44gW9Ki5eUz9fLi5KJzvljlwIylN0SywFX7BSxJIekgQlKD0kJukh/R+g9JAk6HsekkBuborFZ7jI3fvPRbYPTRZ2Ab6DqZaKt+HCU4GSbRthvU0UOXmHmJTNvGNmNBGbYFgZlbL5W55PJGIrj6ptJ6htYrMciqjnB5U8BkSOPTnridhIzQ9mpWyQ0XMiet20XVWie920BZW8btr+DeJJlbpkNjqEwtunKB0TGCiiqI6kFVGsMdFMO4dRtHtmmlPJJ5H6YycKKDLjvKNw8PiU+RDjDGdURrNJg2sGKSHYMQJf97Jb4gHg25IeuwT/FVB67BZRkhtxvDLv5q/SuweNiaH+bw027pEJJpwABuIBfDcAPFrHM343MNJ9FZjiVVTwnNo9UhE+L+OoSqXFc2piUpxVXMCiPQCg3dwGDVMcBW06Flqu8g7vx9kBFvemnwrVtEPK7P5iPLIbjsdxVpN/9cIds6MSHKDjRT93f+YZwPC67KIaAK3uqACO3TMUj214CsBA568lekGqF6TxG/0E7l6bOkBbtgGLcwdx4QGMnUE+iUHWlWLQtnmad2DZCgLfFR8OPg5WdX2AzbKe4/PfPM2kpKT+G1VOcfMw8S6Xfs7VP5o5FXnYKjvoyt8yh2KaKIAyC/IzIwLFbJvExkKZLUFd58Hn1yukEq9TaJ55PvdtNlINwjlG1ncpIMHerHTj7m0ES8QEiDfLraEtugPYtvsZjIrnaEG5kdR6QY1TIcgmTsd96gm2bS6/R+0KsKw87zipJsChtS4W13FiFHEPgNqc1MwBtGBq9TUSaBHZLilYxV54A6OSkEptjYZGIcCYcp4U+iLeg0x57bCvPP/WnQBaYZcvUbFfTRqD3dNY4XrsBQzdE2U1wHVjJdGYLNXHxRauZidmisI9AoBWYY3GME49VIwuaF1jkTAWpzNytqu0QFSUydT4neXnsYZwruDVRPmHVFqvaIcQI76vyHx9FU59NKX6EiHCY7bQ2vFycpLsrQNGBXm/zh9XTQ5kBVSqtrjUPBS5+aLDj2MCZL63kxyfScn4zA8lHXHpiEtH3DqgdMR/sxLE8uohVaT9k+BiLPKIznbSPcL4prZ9C8DIKWgIUgg2KbmGIKKQbaTkD1U9QBbxB6FMGnkVxWlDNieBVE2OTebh0aQZujlv2RmnPKdrszkdmdghwyDo2bUsDEiD2GZg1AlZWf2H0PrzQZzhHbWIqAAMcptFMUUg2B7srBJRBEaZbAcWjvdD0CRcvEbvGwVB/giTAC4Lm0dEkD/MzpHJfG0lgoLhgSCNEwmmPwuag2O86ezBliUExs4VhGIjkbi9PndxuZrMudp2wZrBlh3MKgZxLQpAXJCLzsgkRJ8AZRKiBDmgtCmkTSFtCmlTWNWm+AXJV0TlK6LyFVH5iqivnc5wJaMAFosCSCfXwqB0ciX4b7xpR14XiGb216B3sfV4IvVZUo1uxGETZEjonHqY9eLY3duda3EUwBlkSkIjcUxjyxu5Ld2BiHXmOFpe5qX+a6s0s6CuYaPppCXO5Pje/VMBHNvr0ZqSy0MY8lqW1AHId479UoIUtPi0VqEeGPajSvIUIGVwBbM3SDF6skNK47spNKqGS3JZOYw8BXHJ3Sh2ktrGGAakqSiYuzEQwRKYwELupncTUaSnnLwUzG1JmVaQwMIySuD6KSHIqkgahuW38nOtY6k+HlXEsCi2uifpOWwmuQj7AxeJNmi2bX7OayBcXb3RGLllQVkWZKbD4prKUqwAGULWMr2ceBtjhU9dXaUZJpjVhJdhPpVTZck/bCq/BDY9PE8B6ATTui1ubZWbo1VowgmWERZW71tMYDGoyajDEZYOiY2xZFK/D739CbXk6116lQ9IvockwX8FlO8hWVsJ3gy9SVr6Mdfl9coPjxxVoGInn+v4bBKSVmtU/Cs8l3BLFwPkmYSIew9RqruDL78ik2bQ20WlY+KuYNhnvt+K1gpr/JKSuIEya4TZM2610TdT3F4hJSd58jmaDbpNhEfGxs3UMVsxbMP8ZNeoXZklhd8xr9RYdywBzSDuRqcywwFXIRpWxkp6rmuTfUaC+tMMJZ9mv0vfAIwJjcpdYo6SAAAAAElFTkSuQmCC';

        return base64_decode($data);
    }

    protected static function _getCodLogo()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAAG0AAAGqCAMAAAA2vDrcAAAC/VBMVEX///8AAADm5uf///ytra5eXl78//////j///MAACMAAAwNAAD/+8wAAByLIwAjAAAAAD5drOHMi0H3///z/f+LzPOLi4v//un//uFbAADm///u/v/N+/////D/5q4AI4tABwCt5vsii8wBXazmrGb//NT/9sf/77+U0/T/++4zDAAAAGYAACnG9f7//NwAClsAABEaAADf/v/V7vno0rwCT6L5z5DvvHoABknY+//l9v287/0RbLbAqI3rtnFXMxmCLw1HIwzb8fvS5fSCweltteX98dc9k9H/7LYAEG0jQWbTllMAI1AAADGoWCFrEADq/f/k/P/u9/2z6Pum3/j+9+bNzM3l2MyOqcEaeb8AGnsqTnAMM1UADDsADDSyaTNySyiZPxNgJQw1AAApAACe2PWFx+7C2ezv5+b78uK+0N6uxdpSotpGnddSlc+iuMzy4MgnbbLNuKn+4qgCQ5lUeZbpwIfywoC1nH4ALH6qkXQAJW7bn1m/ej16VjS6cjGsXyWONg99IQNOCQDM5vf76syFobqirK7536vz1KL925/QuJ2Hj5v81ZdGbpA1YYwAN4xDZHyjiGsAJWGcfluBcViVc1CHZ0acUCBrJgx1GQC75vjn7vb37+my0OibyeX369n15dSStNJik73z3bvdy7jOwrd/m7LZxa1EdancvqCvpJfHsJXgvZD3x4YMQoTVnmUMNWAADE9sOhvN9fzi8/yv2fL+9+x7vOS1y+Dp49gxi8u5w8osd7pzka0QYa2wraMoYaNcg6KhlIwJHoMmT4EAAFEAI0XHhEGJXThnQSV9Qx35//+63/Om0u8wgcONoqpoi6FcZncMQHYMNWuJdGmiZV67ilmrgFSXYzODTihYQCVbDAD49PbX4Oxtq9l0n8lChcT65buXoqz+4ajsz6Z1h5lvgo6Vi4vWrXrhrnDFlWegaz9LMxlXJQzMz9yus8G7uaawl5XMoG9eXlprWUWFQR1OMxnQ2ebZ37eWg6lldouLhHU5M1lBI0HSomnSAAALwUlEQVR42uzNAQEAAAQDMPqXJsAlsBVYwUu96mKz2Ww2m81ms9kym81ms9lsNpvNltlsNpvNZrPZbLbMZrPZbDabzWYbdq0q1okoiO50yxYopcDiUNzd3YO7E9wlBHd3d3fXAMEJBHd3d3d3CyFhprtwaXcLXMjwQTg/u+3L2/Puzp2Zc+a+/2x/DvWqQvAtnZiqzth5uVjZiuY9VD0qXmP2dAMi/N7pfGzq0LUAWTMpiiufFwysTMvGlrwAAOzB5yfri+uqGb2XGzJs1pnYwvZwA9TaVkhRVjSCDFsrKL5VXihRiYktQl/wLEEuxXcAIH1lvInRE8oUYWIb1h/GpDVpIXEmuuucERJpHGz06PBGlLI1BE9LP0n7WJA4NgubmgTCRaYbVw+AHCkUQjx+tnRlAWqkVZjXls+doRi9v+LlADDH6avsGfGOJ24jE0Ce8n5WSGPsxBi9wdOCaU8mKwBrmuBlCphZFvZRIwwgE5svCkDtk8vqYR3ZGge5ql3wYiHLyVW54k0FA+lTIFkpQKyJxleVOxUEQpkWuhE+yHFd5+s46oIJqcCzd7bmz3GvZ/QsjbebbnDMMO8GOpvq/5ZS4Gezvk2Ho+JfYUu5cNxlINT61LoQM5vr7iUQKH2jPCeb72EC+B6enZn52FydiCzc2BGDnTt2T6RMD9+2gsJZuWrP/7qcKsNR70UqojBW5fFGHTHz+yLAmLhsHQeXogUtNlITha2bdgjoL9QHsJ0zKgVrO4/DqoIE+DWXwG9rrqu60b3GvffU3DZACYEV3mCBRZorkS7FlnLp5D25zdwllC6shUi3K1AiYcA3MfvQcmXYij5xk4UQqoOElQ1ItQbVDtcjL/6qBBvZFMCkodyldd2fmAozdp194LI3QpOTWfzq6wSkviTYyLhAnY7r/bkLXaej/2xA2tR+cXXpD9rW3KEr6sxqeevhp+4JZSoX6bTTyOV3gOFaaMYKMIdskfwdBCJHYakekG4K5GhiFkEzBsn6Qfy4ij0GYpBB4MxsGc1Fj8a6ajpAsy7FLAupEyohN3Cvb3yrBxWS692iGAxLgCXviLmtBZsV6txFyx5Ej37ixQeRe7JsriRuiFRYEWtj0FzkxYz3F7MuOsDcxlaYhl6Qhy1pfXRHBiuaM6PUZ8QJDAsbDSM8N+MovlJfHaA6ZKoRQIGk58KExqnGEmz++cCowZO8mNq5aQvkfYaZEPiECBEhNCJFk2CjgBkI10r3d0fAxUblYlOSG3o0/M6cZvg8S/DO/k2+eUoi+d7gLVWrLlpGFXX8CecAOV1StEt+fMSNCuYyVs+L8yO3eObbj9WBF7zhUb7KqiBXM4dulvV2G39gJ4Y0gJXTxUeaqmU4HodFc1GI0XAHVtk+uKOY2O5cwZQMVgo48JJlo5dZrerzODg1tivq4tEtLCpIXuGFzTLuPQD1mRi9R8/SWRWeuuAgNRFio0KWZkmhkGw4MQweIkqujaQBmGzY7szMsy87qFiCRY1k3IYgmafmiZfTiC3d+VT4qZgewnVgwYn6fbCzN5Sac5FsI5ukU16TPlBvHQShGq2Oqsz8XKKPk1PdHluCTe3cCMZTyphspHTo7YSe0K9e3HyGJjRXZYlaQhPGNJE1wWZoLuxvoUMsQLparpv2pW4q2AxhtCdtqO17CQTwYGW9rFKghQi2n2suM13osIMOjeRVkGD7Fc01Z9EO1Fy3t1zTf0NzBaxNSBUGXUIklrhh8aU7ecjvSXIGbqxGLGxk3SlnBJuarT+pLxY24yjt9DHtK1vY5Q2AzoYEqKE7nS8KGVcL5HSJSv43TcfWH9HZrNvU7qCbpkmW2OIupeufa66weGQXgDJYebnY6OU9CzCA15GMi40wEDWeidq7jtpJMocjqnG1oqJU3DR/Qar2cnf06COczXMp0pDqOJ8XN9XYZ+ZCI5Ld4GczraGQNvKQr8r2syMW/0bKRnhDASZHRefz3a39hc0tnvfC6dlBCc3n33zL1wLsPzXC+R1maTz+LWwUsEEiTcK/MbDJ+Dd5Nh7/ps502GDGr/u3f2ZCL+XfJMDk30TLtISLwb8RXG26vPUCos7YQaLZMfg3QtELQgeh7TZKEYt/oxHhYQjAmWP4LYd/I9wisvAl7992Ol+dvQwI6gkc/s2UCp7RTfWvk5OLbjw+Ka+w+Dfa1hj5754e47GX9hmPf/MdoJOgoMWGP66zTbH3pbA5HmWc0FvH6L+u+VRN2ncL2Pru0PVnU7uz0STZRNh+NlNQ23R7m8qigqROjWrktgha23mJOOuUZxOvLU1g06ITHSp+9ieDQaD/fJVgw2cHnQ8OmWqvnqm/UNH5wr75hEQVRWGcIXmTiYwLi2jVKhVCgiRCELGxWTmbaCYhJaFFTEKWtEgXQgW6SIKCymUbU3JKsMxJKsRAIzEoCHFTu1pIBC0jovebO81t5s4w774/QXHvanBm/N699/z5zjnfjFdduhqaHavKijBm1/UUVg1XRN2dl6s8UssQecQHnrdmJ4RDHHVsI0IG0kFjrhvaf33gg131LSwOMmKJf7TKhB3umB6H8FGmP3ONGmicz6uC4SSDFQJZ6WYH03VecNa45oRmfmOQXGDUvYh9SvMS5ItiDgmswF3RnQeE0bvl6+4zVgXOJRt/MDw3+snU5JKsuyugUfTgkeJVYNrQQxFIdC62Hc+hBcUnIcZEcNIUE13hFEGhkRzQBRESSIFCAODoJMMtVcqqVL+F7ToWgRUpUFQ7sQln7FXWNhr1G3EnVJchmthl6Wgy3Sdk0oGg0e8j6NPTOR1iIVbbExQaUVTkPrT6LGpVB2iybnNSv8mVmP4u+sH3Ycrc4t/RhoYXk19b/z8lqs9oVuBo0nwmh89ZgaGl2m59ceYBKDQpg1xzLiqqiv7W/A35rvj1jyeGR7JXVlfGKm7iURZCx7yhMckMrX4eW1oLzU1Vzfywz2kuc0xpGcLDYHi08N7t+nONr+tNjUJD68R3oSltsocDFx8rV7tRE8+QzBjourcSJpnxo6KOQviZnRoxgVbStk0SHkZQmnmsqOCIvKD8EBOx4gk0O19ptHmJMr51N1skMaOrErhKhtx32f4UaIpruJqbcoHI8ERFVbQFRFd1nfTwfEIj4WEm4hW7LDQlKDEljpeTlCNFyRZBI2MWfoi3EBnQaXWPxrFRQUgNLTspRONOd1xoxTL30n9wj4a18R8EW6ROolVXXK4yuO3pzPKDWrUNpqdEFa0gjJJqjDJJaTPENu1jpqjfigjBvsu5qQhHaGVhi/HlZ/duR0rMWhLp952+zBYb1kTQpx/IKttW8wUNLSsmJ/uBhzNWYGiw9JdvRDeEOrjcz5U8z03V/JNKJqOW/ywofIJs7MNKzTti5r3L6MS8LO7g5s+djtBonnYfdI8EJRqpwUocVh0ozJ/Wu9zW+eEOxzYZfvCaP9FK0t8gxcediJYHNE3St9DfIFLcvNJg9e3ZeqceEEuP1GhvsGkar2RV3xiY14nKnH67zga33W2TNxB1wcwXeFbx/VH5/TI6y3zTqBa9sQaa+sD08vrL649iFPXiBEdIQBpoqjq0Iyf6L73BcP4DPVNR2328oLFS1zBquUHFj3Pv2VHYBzRyAJat1ji7tzoKLtYfNMy7BRsFTclrXS9wLV/RENG3l0Cr5gRZ/qFhnNyPisYpfuqf9xMtNYiZlPDb5lM1uQCA+fiBRtRUYopiPpi/5Q2NRC7iFxnvSNnoyNNAWwZa3aNhF32/AzostvylpvOf29RBU32aZ1YDevmAYv/kSjNOEpC5LOFMDiVkifzTneyOWs7RtuPIrCJnchxxACQrajAFwrA2g8SAZULVYEG4kP6Sad8554IfeOGSkC7QnLFXoqynlbCjD2hBM3PprDNP/rHeq0EzaMoyaAbNoBk0g2bQDJpBM2gGzaCVXgbNoBk0g/arvTkkAAAAYBjUv/ULzNxDAWw2m81mszWbzWaz2Ww2m83WbDabzWaz2Ww2W7PZbDabzWaz2WzNZrNxGj/aCNuqUf+cAAAAAElFTkSuQmCC';

        return base64_decode($data);
    }

    protected static function _getToHomeLogo()
{

    $data = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgFBgcGBQgHBgcJCAgJDBMMDAsLDBgREg4THBgdHRsYGxofIywlHyEqIRobJjQnKi4vMTIxHiU2OjYwOiwwMTD/2wBDAQgJCQwKDBcMDBcwIBsgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDD/wgARCAD1APUDAREAAhEBAxEB/8QAHAABAAMBAQEBAQAAAAAAAAAAAAUGBwEIBAMC/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAEDBAUCBv/aAAwDAQACEAMQAAAA38AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEf7rrltXZgAAAAAAAAAAc8z+qbdRoAApmjN2Il/FgAAAAAAAAAARNB1ZNZxdAACm6csvVZN13AAADnl2XEVW+iy03/r59d9QAAACMz2YtMx7SQBTdOWXqsm67gAAAhHe68K6vMi7af7j1vXI6s7XcAAACMz2YtMx7SQBTdOWXqsm67gAAgmKNpy4n1OZsHN6Oi49tK05cP63L1rm9HSsWzqeQ76AAjM9mLTMe0kAU3Tll6rJuu4AAfN68Yt0+ZXrqt54/WmvFg5Ex/uvD+ry/yN15XVkfHtIAEZnsxaZj2kgCm6csvVZN13ADnlV9OfCetytDx69c53SAACGcbcWQ9Hn7Zy+leMuvokCMz2YtMx7SQBTdOWXqsm67gBBWU+W/oODZaLvTfC7vUgAAgYf1OXlPS53pTg9y45tXZAjM9mLTMe0kAU3Tll6rJuu4ADiPNPZ4vpfjdokciOy4dSBgXV5Gxc7ozddwAIzPZi0zHtJAFN05ZeqybruAAI80dni+l+N2iR576/HqujPaqL/QnG7KQwLq8jY+b0prxaACMz2YtMx7SQBTdOWXqsm67gACPNHZ4vpfjdokeae7wqfsyXDFr9LcPuEjAuryNj5vSmvFoAIzPZi0zHtJAFN05ZeqybruAAI80dni+l+N2iR5p7vCp+zHcMWz0tw+4SMC6vI2Pm9Ka8WgAjM9mLTMe0kAU3Tll6rJuu4AAjy/3OHqmHaShlPQwWqi+saKNT528JjMt2PfuN15vxaACMz2YtMx7SQBTdOWXqsm67gABnWvF8lnjLN2CwV27RyetN+LI714xvp8yk68mqc7ofX593/Hs+2PQAIzPZi0zHtJAFN05ZeqybruABHCiasub7MepYN1xz6eyAFYuoyXo867ZNelY9nQkAjM9mLTMe0kAU3Tll6rJuu4AfNPnz12eNX7qtIw7dl5nT7IADkRlPR5+abcX6xPobi9mW8+uvQIzPZi0zHtJAFN05ZeqybruAHEQ/vx/KMp6PP3Hl9QkADkRiHT5eq4d4la7f2SARmezFpmPaSAKbpyy9Vk3XcAOI7Lnmcc6XN2Pn9DsSABwxrfz9mwdAEgAjM9mLTMe0kAUzRlyvoc/6o9AABCtaaK1pzgABEzlVluyagAAmPhR6L4/ZJAFM0ZvNff4KYAABITAAAeZTAAAHZTGe71d899EABTdOXzV3uEJmq3SsO2G9+M26XPQ4E9RyJ7MTdN/xzEfbSibVl03HLqpmvLW9FH8evHEkCZov9XfPfQEgCm6cvmrvcIXHLqmaLvh9+ft8+vvr9fh781y+i05NX5eo+7zNTvpjba7tk1V2/PP03WbNppWzHT9NGj49cR78Zz0MCYmaL/V3z30BIApunL5q73CFxy6pmm2u3Vajz99Y0Z5mqyJ9+a9fRpOLbCXUfn49/jZX+Uss6PP3vi9hPmGtr+SYgr6r5j14T2OS9eJmi/1d899ASAKbpy+au9wu+osmXTp/O6EZZVlvQxaxzt/x+vFb0Uxtld2yao6zxzygdFN+xa6bpz2Sm0mO9+Kvoo0jDshLPGYdLnp8zNF/q7576AkAUzRl819/hJgAAAAAAAAAAJTOe71d899CSAKjdn8/9rjBIAAAAAAAAAAiUpu9NcTuAAAgAAAAAAAAAAAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//EADUQAAEDAwEFBwQBAwUBAAAAAAUDBAYAAgcBFhcgNlYREhMVN1RVCBAUMEcxNEUhIiRAYGb/2gAIAQEAAQwA/wDAGizIINVIE1vBab1YX8zW9WF/M1vVhfzNb1YX8zW9WF/M1vVhfzNb1YX8zW9WF/M1vVhfzNb1YX8zW9WF/M1vVhfzNb1YX8zW9WF/M1vVhfzNb1YX8zW9WF/M1vVhfzNb1YX8zW9WF/M1vVhfzNb1YX8zW9WF/M1vVhfzFMslxJ88QatCvfX4cy+mxaofF4+vEg664MYstshGenhVbIRnp4VWyEZ6eFVshGenhVbIRnp4VWyEZ6eFVshGenhVbIRnp4VWyEZ6eFVshGenhVbIRnp4VWyEZ6eFVshGenhVbIRnp4VWyEZ6eFVshGenhVbIRnp4VWyEZ6eFVshGenhVbIRnp4VWyEZ6eFVshGenhVbIRrp4VWRRA0VM4RoLYNWXFmb02LVBuSgX7FsjRJF/oy1OIaqoLJOkLFkL7Fkv1Za5zgfFmb02LVBuSgX6jJceFZXOyjtJqjO8pkpFYqyGd8cMYY4kb6N6m0GtngwydGIU9vbK2KuWkXlIqUNNXIhzqp+rLXOcD4szemxaoNyUC/TP8iDosks1QvsdGHz2Rz45/Rcg4guLBsdvSfFb7CBKpzjkVK++5/siRYPIIGcQVX0vZuoBlRqeWSGmrLWRP9GWuc4HxZm9Ni1QbkoFxvnjUe2vcvnCTZCf5bXff8KKXqtEIBjwlKlknS9l7QPG42LjTCxoJa2I6fcyIYGmVzQo0SdIz/FbwAgsSDX3vRkDykSjtiTInpeRGBi480ytdi3aTpHiy1znA+LM3psWqDclAuKazgTEm3Y8V8Z7JpMcnRnudxypZAsQ2IX2P5X3FluPIOLGh/vvwuiTEoxeyOAnP6Lj3GPsiD5Ski1XvsamOHLXOcD4szemxaoNyUC4ZmuWZxh+vH0vHJPHjl85vcvXCrlfGJx4Gl7Cxl4XZ+nP5ZfQy0E9iHgVid8afw5Bc/3/ABeDLXOcD4szemxaoNyUC4uzSv5k4e3h+oPnNpUH5KBcWWuc4HxZm9Ni1QbkoFx/zJwZXkJpjPibZkYftUNrZH1CVrFEhNPp8MQemH7pDT7/AFB85tKg3JQLiy1znA+LM3psWqDclAuP+ZODMvqSX+2GfUkTwfUHzm0qDclAuLLXOcD4szemxaoNyUC4/wCZODMvqSX+2GfUkTwfUHzm0qDclAuLLXOcD4szemxaoNyUC4ybxMblF0/X0vuS32xz2JWt9sb9iVrfdHPYlamT/bGau3oVo6VpphM0qK8Zd+1QfCryWPJk0dFxt/j77o57ErW+6OexK1vujnsStZQlDKXH0SI1JdFKD8lAuLLXOcD4szemxaoNyUC4yeHgJIi6frvCVqq+GIu2QvXXJk0kpVZD23ajGryr2+FYnLm79FzeiopjGo4LjTGxoJa2I6UZEMDTK5oUZpOkZjh14wRWeR1e9+jHUAdhJVpLdCTWwXiSIlWCT5gUJrobkY574rQlimLFtWCF196XDlrnOB8WZvTYtUG5KBcXbUzyeHjN9zVDTzN9e1m+U3Wi99ngsIZjsLFFLHKNt7sjwy6EhZZbbqUQv0XKxaXY6dqvQDpddlC8tizF9jQ3baKd9vFlrnOB8WZvTYtUG5KBcL92iPYrvHV/cRmOUjciSWaIdwawi5EULeauSwXzimOb2yHgIbNeA004z+ZrBJx6wRB3L2yo4ANdqzCN+TuoXPjEUvtTRv8AymEWNIyECzKt/wDSzgy1znA+LM3psWqDclAuJeLR9dW9ZcGNVV2QjXTwqs9iRoryXytg0ZVp/TjgQ5iUy9J0CLNB4jshGenxVbIRnp4VSCKTVCxBCyxJLgy1znA+LM3psWqDclAv0fUf/gK0107K7dK7dK7dK7dK7dK7axh6zyj9GWuc4HxZl9Ny1CsvHxg1qwbNBuqW+2S+yF1vtkvshdb7ZL7IXW+2S+yF1vtkvshdb7ZL7IXW+2S+yFVNJsSmOrTzRBql+iISd7EiSpAYk3VW32yX2Qut9sl9kLrfbJfZC632yX2Qut9sl9kLrfbJfZC632yX2QqttiUymMY80RapVpw5l9Ni3/Y0qE85AuLM3psW+8NA7SyRqH8f8atxH/0lSTDZkY2VXFu0iifHC49dKD6ArRzY1qQDNQxt6MuVsW1+0vhd8aDiCF79JzUdhYB5iNU6uw75LHsPvmRJdpo9sZWrJXILXo362a38EJ5yBcWZvTYt98NepAmvqD5zaViSYPw8iZDFV1VhmdwKAyRtiLVJJJMbiVgwYIvpmdsH6SbFCPk3m0NI3lUMXxdlLT648iquikhjKOsF3lkrP2MLpFh5doYS8se2aBnmJQREKu6iRtZ8viECmflN6F716wvyGJTCTIkwRXcubAmIEWrDV9NC1jBLIUcioRm1vjZ7zBfLcMQj0aYuUShR7UR9Al6wvGUz6xVbUkSGqkxGqcrdhRul6t6WJwIdCzWXSaxBWdYwvDjNTUedXkhn2hPOQLizN6bFvvhr1JEV9QfOjSsZB3hmajrWfc7Ji/YPsuxcU78LWz6hlXep8alf3/w/p2Ud6jTCV/b+Fhf8PeYa8r/sczLKKZEJW33332zNW9fBViy996qv04/5+sVpJIZfkiCFliSRJJqr9Q9tr/WzRL6hVXdx8ajf3/w6+oPkxnUS9Al6+nD/AD9QBJrfm01c57ni5uWd3z91a70v8LEitl+K31p/v+V/aE85AuLM3psW++NCjMLNWBEmv4DQ+fxXI3tjsy6/JXVyRFIo0WZwcR4t6xkiub0MrOr7yNk8hUyFIWThr4Do9kEAAAXg4Ah/sw8fGRyTOXhl1+MhksozNTUgRFr+O0kU0APMRpgkH/fJYVlQaM+canHn41QWUhhWTDxh+88JjPT6DzIbk4Cdd+xGfwyWh7EZq0sRc5D2D/Da7GeN+XmGaR+SRps0DPvHWjs0j7PEaoJd/wBwlhWUhoz5x528/GpeRKDp67PhVLLtXcuxzMEUHcnQvbPJxkIboAvi8NQ8Id9oTzkC4szemxb/ALMJ5xBcWWWjh9ACbZmgq4X2TkfTxStkpJ08VrZKSdPFa2SknTxWtkpJ08VrZKSdPFa2SknTxWtkpJ08VrZKSdPFa2SknTxWtkpJ08VrZKSdPFa2SknTxWtkpJ08VrZKSdPFa2SknTxWtkpJ08VrZKSdPFa2SknTxWtkpJ08VrZKSdPFa2SknTxWtkpJ08VrZKR9PFaiMZPoSsMuuDJpJf8AoP/EAD0QAAECAwUGBAQEBAYDAAAAAAIBAwAREgQhdLPSExQgIzFBIjAycxBRUnEFFiRhgbLDxEBCVGCCg2Ny4//aAAgBAQANPwD/AGAzKtykilMqUuSa9VjCvaIwr2iMK9ojCvaIwr2iMK9ojCvaIwr2iMK9ojCvaIwr2iMK9ojCvaIwr2iMK9ojCvaIwr2iMK9ojCvaIwr2iMK9ojCvaIwr2iMM9oh8xabHYOpMiWSdR4uTnBDthYMzOyNqRkraRg29MYNvTGDb0xg29MYNvTGDb0xg29MYNvTGDb0xg29MYNvTGDb0xg29MYNvTGDb0xg29MYNvTGDb0xg29MYNvTGDb0xg29MYJvTDtv8ewZFuqTjPFyc4I3Cz5Y+ZUI1DUTd/wD5ESiHBQgMVQhIVS5UXunl7+uYxxcnOCNws+WPlDPxOFKayVZJ8yki3JfBiQGPVx8f3Xtd2GKK22L9u6H1AMoCbTlgfNQ2RTX0znQU5zgKUdaJKTaVb5Enlb+uYxxcnOCNws+WPk0pRZuwT6EemDJSBhqatsCqoNydAH0zJYAhMC6NsFL/AC6l+C0ytYDOcuxBNEKAJTs9paJCBxEuu0lDpKIGFzDv0pesxLyd/XMY4uTnBG4WfLHjCVTjpoAp91W6LxdthXG528Gq4or8dp7nT2DVFKIbkk2jyp3Mu68BT8LgzkslSafIpKt6Xw0MzEr32vqK5EQhgBEAG5HGB/Ze93YoKXibKcllOS/IpKly38e/rmMcXJzgjcLPljxEFbNkD1HpSCKdn/DrNUYigzvpT1FKcygCEwsQXt/9nkG5W5VNGnp9Z9ZLAEhGw7NG3xRVG9Ohj6pEkUrXZuxy7hp4t/XMY4uTnBG4WfLHhBubQSq73qidyQZySDlU68amSySSTVb+iRbrQ1Znq2xJVAjl16j5W6o/NWRJyZOL0Nbx9Hb4VSYcP1vMXUmXDv65jHFyc4I3Cz5Y8f5g/ufK3AMx2Nws+WPFv65jHFyc4I3Cz5Y8f5g/ueANlS0zaTAb2RjGuaoPa1NPWkzG5o+DcAzHY3Cz5Y8W/rmMcXJzgjcLPljx/mD+54OTkh8OdknwbgGY7G4WfLHi39cxji5OcEbhZ8seP8wf3PByckPhzsk+DcAzHY3Cz5Y8W/rmMcXJzgjcLPljx2T8aN40Dqog/VHtN649pvXHtN64tlGyY2U3VpaFF8Iz+mP9NqNO/wDAosokWwV1BRwTAhuNJpHtN649pvXHtN64bsosKj6CJVIZr2VY3Cz5Y8W/rmMcXJzgjcLPljx2p43zodCmZFNeoQ2MzMnmkERT/hH+pfMAaTp0GhCLul9Mlj5GHOdvvSlbwilEN2SbR5U7mXdfgU7nBnJZKk0+RSVb0vgAmtmdTn/8ZXHCEgVWalCZOqRVgQqsPhMDF1vRHut6IsjIMgpXkoiMknxb+uYxxcnOCNws+WPHfNll1KQkUpGd9KxdRXNqyBKpJil9ZTrS6qEG+1Pfv1oHoKcQDSFpZKkwGc/sv8UWJTO02YM1rxXDffEr3TNNgcuviX0ce/rmMcXJzgjcLPljw2dsnXClOQik1WHQUDZavMxuuJzTTCUq02b+yAFRe6SWuAkEmbV6A/YaE8iyPEwrh2miohuW6hY+bFrm0vTq3QiduyjF07I8S0glU1o+hYtLdShetBdCGcknJZpPh39cxji5OcEbhZ8seJwlIzOyNkRkvVVWUYNvTDu3r2DIt1So6y8hDtZUPtC4M9uMYJvTGCb0w0KCACiCIiiSRETsnDv65jHFyc4I3Cz5Y+R+o/p+R+rzw8jf1zGOLk5wRZGQZCbbilIRl9ce05rj2nNce05rj2nNce05rj2nNce05rix10bACGdVPWZL9PkOMqxJ9CIZKSF2VL/DHtOa49pzXHtOa49pzXHtOa49pzXHtOa4sdvao2AEPrcDrMl4uTnB/id/s+YPFyc4Pjaa+bRXKkCLpNIwX/0hoKyBAUHi/wDUL5+Q6JltFkUqRncM0qiyPE3WKpI0RbluVZL80nd0X4/iTdZAFHguErpGtY3+oYCx2p1Hts51BXKbpyhhnaqVxkq1IlwVIsAaiqgaGM0XsQ3Kn7ovDv8AZ8weLk5wfHnZJxuAZjsWtxGNh1Rsi9Khfd4lvj8SAqxDqTw+sv4oQQ6MlZE226HOqJtCmhXQAz2UxcJyU6lbMJIUN2Un0VhREqkME7osE8a2JhbWy25sKiQTOpOpQYzftVrMZ2QR6qXScMzu2zTwGSDPZzGmlYYspvA9YnUbcQpgPWS9jhswJXrSaG4ZEAmqkt07yij0MuCKNTlKtwrodNRca2zb0h+qYIlMA+Nmabtj6OA0CgVwJJJegY3C3fzOxZQaEXLC+jREJqeiBtx2NhDVEI1RxRGa3JD4pQIGDAzT13nOuKUcLoRg3L11DcY/Hf7PmDxcnOD487JONwDMdixvBbHlLs2BjOLDW8Rq90eK9sfvMA+9cBZZteC6tT8d/wBkCAebJrwXbRRWu/7IEbB/Ye1two6w0LIh8gHZCUO2KxGZleRkpNR+n/qw0FqAAG4RFHwhH2iGspeNLOKh/GumBss2vBdWp+O/7I38N/DLdjcLd/M7H6f+rDT9sJjtM9poU4aZaGzzCU25a1OGifG8JTs1EzlK8kmrnx3+z5g8XJzg+LO0rORF1aMe04AEaQqLUH8qJHTbnMAPqs1Il2hyWEeF9HyRFWsVuu6SSSSSUost/ocpUu9BN+KXzEofA52mZt7Aiun4vGRwdjJoSoI5lWC9BRYf2dByIejQD3lA2OytKzsXOoKFV8pQ/sdl4DOdNc/SkWzb7F7ZGVdb4kNyDOEcZeYdo7g2HYk+YwyHdkySffZGEzH0xWu1ltaKP+3v9obtYukOxcC6g06kMHY7U0jOxc6mrlN8pQ/sdn4DOdNc/Skb88+yRhcYGRdUX5isMpRSYu1fatrqMUC0tpvbmPUgEet/ci+O/wBnzB4uTnB/id/s+YPEeypaZBTIpOhGCc0xgnNMYJzTGCc0xgnNMYJzTGCc0xgnNMYJzTGCc0xgnNMYJzTGCc0xgnNMYJzTGCc0xgnNMYJzTGCc0xgnNMYJzTGCc0xgnNMYJzTDVuYIzOyOCIijif7h/8QALxEAAQMDAwQBBAIBBQEAAAAAAQACAwQRURQgMxITITIQIjAxQUSBYCMkNEBhYv/aAAgBAgEBPwD/AABjHPdYLSy4WllwtLLhaWXC0suFpZcLSy4WllwtLLhaWXC0suFpZcLSy4WllwtLLhaWXC0suFpZcLSy4WllwtLLhaWXC0suEKWTCNPKBe26lsZQpZXh5AK7r8ruvyu6/K7r8ruvyu6/K7r8ruvyu6/K7r8ruvyu6/K7r8ruvyu6/K7r8ruvyu6/K7r8ruvyu6/K7r8ruvyoXudG+53UvKFL7u+yD8eUIJC29kWke326fjfupeUKX3d9psZd4ChpGs8uK1UbXdKkgbMLtUkToj9X2qfjfupeUKX3d9mGndL5/SDY6cXU1UZPiGpdEfCa9lQ1TUrmeR+Pm+6n437qXlCl93b2tLvAUFFby9TVTYhZv5T5HPNyh8seWeWqGrDvpepaQSfUxOjLfB30/G/dS8oUvu7b+1FA6Q+EyOOnbcqesLvDPsQVJjREdQ3wpqcxr8bafjfupeUKX3dtiDS/6/wm9DB9Kqmh0dz9qhYLXTum3lVAYH/Sv3sp+N+6l5Qpfd2/+P8A1u87KL0Kk9zup+N+6l5Qpfd2/wDj/wBbKaFjogSEaePCqYWMjuAh80XoVJ7ndT8b91LyhS+7t/8AH/rZScI+KziOyi9CpPc7qfjfupeUKX3dv/j/ANbKThHxWcR2UXoVJ7ndT8b91LyhS+7t7GF8FhhaKVaGVaKRRMEEf1la4XT7VEf0laKRaKVaKRU0Do2G6k9zup+N+6l5Qpfd29lU9gsEKyU+AonSnzIpaxrPDPKfK57vJ+GOLfIKhrf09SvkIvGn1crDYrWSWRPUb7qfjfupeUKX3duChpXyK8NN/wClS1DpF52xzPj/AAhNFO20g8qWlczyPO+n437qXlCl93bWtJNlBSsaOpykjc8Wa6ydQk+er7EdGXN6rqFkjPZ1wpqdsvlSM6DbbT8b91LyhS+7tvld12V3X5VG5zg7zvClcRA2y7j8ruPzup+N+6l5Qpfd28qgHt9ibhb9in437qSxmCdRtc4m60DcrQNytA3K0DcrQNytA3K0DcqCARKwVgrBWCsFYLpF1PAJAtA3K0DcrQNytA3K0DcrQNytA3K7AijdupeYfZO/8/ZqOM7qXlCHxNJ2oy5Cv/8AlMrWk/ULL9XX4+B4XgrqRwFNKIW3Ubw9gKJR8eVDP3HkEKSeQTdN1PP2gDZAghEgDZUcR3UvKEEQqzzCVQi7SqqBpb1BUUl2kO/SfWOc76Aoqs36ZFUSmIAtTqp7vUJlabeR5Qq3g2equS0YVM7qi6in1hJswKCaV9+oKllLnkWUw/3KrJOiwso5QI+pyNY93oFDV3NnIefio4jupeUIL8qt4iqC5aVVPDWEFRhwhc4KgsWuyq0AOFlVA9pt1SAdnyoedV4ALVObwtTSRSWCoGg3uiLAqj5FL/yP7CrrXapSdOFRBojuqkWm+hMJtY/FRxHdS8oQ+KmMvjNlHHUs8NCFNLKbyLtN6ej9LTzRn/STKZ7z1SKric9gDFTRuZH0lR08jZ+ojwq2J8liFLA8wtFlBFaHoejTyxOvGoO/ch6pYZGPu4KWCR0xcB4VXE9/T0hCLqhDHIQzxGzFDTPLut6/PxUcR3Utu6ED/wBio4jupSBKCVqI8rURZWoiytRFlaiLK1EWVqIsrURZWoiytRFlaiLK1EWVqIsrURZWoiytRFlaiLK1EWVqIsrURZWoiytRFlaiLK1EWVPPGYyAf8h//8QALhEAAQMCBgICAQMEAwAAAAAAAQACAwQRExQgITNREjIQMTAiQUMjJDRgQGJx/9oACAEDAQE/AP8AQHvaxtys1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1F2s1H2hUxk2vqqr4RUUTHMBIWBH0sCPpYEfSwI+lgR9LAj6WBH0sCPpYEfSwI+lgR9LAj6WBH0sCPpYEfSwI+lgR9LAj6WBH0sCPpYEfSwI+lhMP7KZjWPahpquIqDjH49kaiMG10HA+v46n3ahpquIqDjH4nSBqlqnPFmhCnkI8lHO6I2KimEo2/FU+7UNNVxFQcY/DLUtj2H2i585UNKGfE1MJAnMfAVDVB2x/DU+7UNNVxFQcY1ueG7lT1flsxQ0zpTc/SZE1gsPgfD4w/2UtKW7tUVUY9nJsgdrqfdqGmq4ioOMapagRhOkfObBQ0ljd34JqUSbhAyQHdQ1Af8Aeqp92oaariKg4xpmJDD4/aPk87qncQ/b8IVa7eyb5fsqbzLN9NT7tQ01XEVBxjVYL+dDRcLbRWe4UQHgNVT7tQ01XEVBxjX/ADofNRK9shATZ39qnlc6Sx0VnuFD6DVU+7UNNVxFQcY1/wA6HzVcp+KTlCHzWe4UPoNVT7tQ01XEVBxjX/Oh81XKfik5Qh81nuFD6DVU+7UNNVxFQcY1ueGS3QrWLPRrOsUjjM/9IWSd4ppMD9ws61Z1izrFUTNkfcKHdg1VPu1DTVcRUHGNb6Vrzco0cY+1I1g2YoqR0m7lHE1jdh8OYHfYU1GRu1Rtbez0yljkGyFGy6aPEW1VPu1DTVcRUHGNRNlLVNZ9K0tR/wCKKmazVJAyRGKSB12FRVQf9q/Wqp92oaariKg4xpc4NF1NUPebBRvDPYXTK223igb65KuzrWUr43+osoZ3RKJ/mNNT7tQ01XEVBxjTsVgsKwWdKra1pah9aiom3mKwWdLCZ1qqfdqGmq4ioOMadvgKuO7UCLK4VwrhXCuEbKHmP4Kn3ahpq7iIplU5oss65Z1yzrlnXLOuWdchXOU0xlVyrlXKuVcq5VyopjGVnXLOuWdcs65Z1yzrlnnIzGV4Q01XCfwjX9fhg5BqquIo/ELMR4CNB/2T6RzRtvo+1uFZBRRmV1gns8HEIBDfZSQ+DbpkLDD5KGHEuiLFAXOiDkCGmq4iigqTaUKtNnKmnIdZVbAHbfumUoDf1qSlsLsUEWKSCm0zRs5OpOkaVpF2qljJeQp2+MniFHSAC7ypY2NtYqpi8WAqI/26pI/O+6dES/xCFI1o/UVJTeIuF9fEHIENNVxFH4o+UKtt5KmYXO2UhaZmtKrrhwVGSWm6pSMQ2VUTiqXgVCSQ5QC0pTrGpuVXONxZA3IVX6KL/HVFcByhAxyqsuL7KnN4f1JwF7j4g5AhpquIo/FPIGSBPkpn7uRqI4+NYjvLyWPFIP6qfOxgtGqWRrHEvVQ9rpLhPnY6EtCo5WRg3UUzGykqaS8pexCeORoEimwtixVMzJG2aopmNh8SqWRrPIlYvjJ5hYsMgu9S1DfHwb9fMHIENNVfCKI/5EHIENNSCYiAsB5WBJ0sCTpYEnSwJOlgSdLAk6WBJ0sCTpYEnSwJOlgSdLAk6WBJ0sCTpYEnSwJOlgSdLAk6WBJ0sCTpYEnSwJOlgSdKGF/mNv8AYf/Z';

    return base64_decode($data);
}

    protected static function _getStdLogo()
{

    $data = 'iVBORw0KGgoAAAANSUhEUgAAAUcAAAA6CAMAAAAUXGvyAAAA9lBMVEX///8AAAAHBwcEBAQVFRUJCQn09PS9vb38/Pz39/fl5eUnJycjIyP+/v4NDQ3u7u4RERHz8/OUlJTw8PAdHR1TU1MZGRnr6+suLi75+fnp6emOjo59fX3Hx8elpaVYWFhpaWni4uJmZmZOTk5dXV329vbb29u6urpubm7t7e3Y2NjT09M7Ozve3t6Dg4MxMTHW1tbExMSIiIjKysqYmJhycnLOzs6RkZGGhoZjY2NgYGBCQkKqqqqLi4tJSUkrKyuioqLR0dE/Pz/7+/vBwcGAgIA1NTVFRUV1dXW/v7+3t7dLS0udnZ14eHinp6eysrKvr69ra2vr33A4AAAKUklEQVR42u2baVvqOBSA04VSSmnZyiZSkP1aNhFUBJVdEPTe//9npu1pabC1yHJnfObh/XQxJE3enCQnZQadOXPmzJn/Lx88RZIkIYSZw+rHrwi1PpVAxxIX45xFVWSS6OSEcpwjIXQstWdC5+7QplI9vb4fHUtjPqM8JkHlKXu/FIsn9jggPA6Q9BQdSUnRLVBhH/qvPRKfoTx8p3naoLwnHPGsd1dlUhpcwLFQfDxWI4o/nNojTrCe+jc8pnfWDFwIGpTj7ucrkIQKvfKhH+qRIJ+qJ/R4c1A84lMQ+lpjtIzQ0R7503sEKPlf8Jj+dtWQk+IgRZKaxh/kkQwCgocyB5k5uceolMd5kzLHeGyG729ubtJt9gd5JLOBqoqYavfXPAHMTu7xATHbJI/xyAbQCYhfndKjp7P5nKxJRkA2zQ6zxWIoolH0jZEdNunzaeUiUywySZZ1ynt0rtwOlDHDwDOKSbyFC6ga+fxM1ucrMhpFX/ILoYGkLyRGGB/r1GUmpD8smcLj0T7er4JdfXxE9NnjsYIsuBH8LaxrDbVuB5LE0yrBt0K9zXxukVu9yhIfpGlBkfJPr4NVn9vPIzQyfVLUNuiYJHUuvJzWfyakUoeqXEiDAWNJ3/DlriAlaBVhJK+9kY1JloloXxyr/0r9rsgNITrqrXKfBdfClbk/SAd5qbCebXv0RRa3N/k8T2vkCzdDLJdOQidYxOZWVzJP3Ll79IXhpElr/frDBz0kdo4Lj8ut1djKRkkKP59ID/Gyr8f4lCdJLIEVom2ExnndE/yN1gg+itBcI+jx4I9svJiz2x4Jah1/GbWzvLnVk/zF1uTnXrEuk9SWR6YXFbbGG3xabGKyz2udULhhNqq3XXf3iK6he5I2BQ655YsVz2w4Sti53dMjJ9lO8pb6aMXW8HMcaTzbs4u6sYFO9DJ62qEJHDzBKids1S2PcXtR7Le5ma0EvRNpPwHc7/CYgq+9aaIIO0KL3Qiiie94vDDPGUfEP4STx/HI9ucEePQTdlbG8khARFOfXPSRyWWMcCCKgKpDGd0NQOFLUHdFEnt5pMCjA5KZoy+jxAk8XptBZXCQR6WJebQjbxa1TOzrkegxlkeL3eu6BMPKax5J/vGxktaZvpmbUg0Bc3O6Z5VperqWEp4DPPpujaU16Otk0um1NFQf3dFeaFGYY6IBHhOx0egB+lSXzRXxYfPIS51p3thfG20E3JqLar6epteyEt32KBL++bySBuZG5JFxm8fvxGNyBV2/0eePi8cZHyAORzCs3yyEo7FE0iX9K8XxB7/DY2AbPIOjWwGjWHvSWH/blsv1C8ay7b+/v2eWsDNzap9CPiBUK5AwKHbLYyPdqopqrxYKtP5ihKMEY5tfhopqjyPVprzlkc2lrPFW2zNozMtue6SEmfy6vrl29xh/NvMeGxmY3gEkAx0Iv1dxc3g/u3pUXsIYd6smPK7nlgkPYOZEBOIdWMDkZauYR+UjldwKQMEYzARUJGqbzTlrebRzS8GAkrhHPvteSolMMRlw88jWZCOaS8hGElZRBdJiBZodWkPyu3qkBBzSv8RvZlGINVtVGInbWxN4eZbX1VwmsBWH7+HGAF8g5bjYFFev3DyKEOvrIu6xDAKd74VorMIy5VWFJ4A5ciAGc697jIC1h9S3PNqJwpYWmkIXeuyBHt8gu8A9DrBTDGa7p38opuEtWXe3R8BYcQzusevynkLgAVogCYBcunjUV3IJjNetJHcCHsN7eURlI1qV9+KpPN5Ypc1HzGMVUqyGaEXcST3aobYO10CA1dny+AEfluhIjznFzPBng5S7R8c+PX3LI37MjJCLR7ztoz1SEmetjFW6J/kBCvPYB4/e3R7vXD2O+1b+zWcXjh7jaJtapn4lN3itS8+e73gsQLW8/uHRzSPbvbXGS+AeM3t7JKebyO/Po0HsMot5/L2vx3zkF0azFEJAEfNMxfKLnR4nMh/Erizf95iTdnkcv4yigjXeozzSlYm5U4Ue4IT+psdnV48F9AVMOIZf4rIpd4912JhdPN67eYR17Zz3pGRwdaTHoKJcvX6UIpt9okIRf9sjrKUC9iBqVnLzeBck/pbHIuSyB3sk5dy1xq9cTgxhhZeksSLvP4ZaeS1P2TyWd3sMu3gEAnFvD9MzS208Qg/gM7gwNi0l3e9qfeIq+jJ8KukdbhzmMQbTWSYAabAEH4+U3WPbxeMf59FNjQs5ZybJPfL0HoGAOEw/byJy6ugRPzUfmj4jHb4RTuXR9wqPv9jchqQ9PWadxwapWQLWueXxyu4Rvxfe7e0RKJb6c3ODYcxFbPMIt1HaWl33tnjE38T82sMjM4JIB4uWx86xHo30Keno8RK2+8yJPMJrglvoK3X5pUd4dyBzh3jkCrBtfOExAs+uo4M9Xrn9KFt39ljywxryfdejjHaTvIOx3H3pEZ7RS+3nEZ4dWsO98JebR+LiC4/v9HEe084eGTgbpdypPOJvUm92eCwc5BGtjLdorh4Hf8nj1PL4AB7hmgaXAwHyZizruDjGY3yXx8RnjzeCq8c5/mwvtD7arKFI5TvxGDrSI+xFShH7jQjzeC9A8bWxJC8fBFePyjKzzcRhXRvf/b3tsWZ9AzJNvml+Hs6J73tswohIYxrG3U4QP2f80BQy+e0nTuLxwQhIvR3uoxelcI9dPxT7s6v+S12a8aRtPrde5pMxeovoGu4QD+t+pn89VOl+1GFcAmfOnAdGImprsJS0Zq+X03cBb8dPYR7bOzwydaMrs0q4/9J54j3wmYa8R4bCgR6utb4cIxw8Xu7lEX9R42n0Kq+PiSh8Ao8aMgFQdIyGDrl5xMGz1pIqOBbz60QFKJI2Wbdg9EBRlMRbSosRGir75cr6rRGFufueR/znQioYo6Gu5TFpWBZmD52OkoCvnsJjddMUhV8QH+LGkvITFgd4rMBI7SW09dOHQljwegyONn0CDw4ep196HIcJJ2gorQmO460c4xGqeogteAr3iFb0X/FIYzlpGevBs77aWzyxRVTYwyOK/3HxOP7UU4qn9vP4ZYr8gpnySB+wOfVMj2z5icTKeWE/j1ljXVvApC4CWOWBgMejhjeBjXT0fqt7ffyFnTNT271QsjKdOo3NQaWCe0TJW8EqFORlWh/eH/CYgYotR1FLr8qiib6i23lKqJPin8mDBYeqE/XbZew/H+f6HYX3ECTfyE8nGb+zR27pdWJRgr0/My3k/VCVSjx1ynGE41tkFTUsYv6RHA4Zy6+en3lUCY18fVljmXZZ7VOX0TMX7d/eJXa8M92y1uVr7Pp5eZePetRYm2XDQ1HUir0bN2zrz/xZC/1Z4a7FodRCq1xi4UBsebXGI+ggxNq12tSwybHO5ZFf3Ul50m3WfLCqwOM+BIpcbjhcaJ2cXNdEe0pZ6nrL7WGJ81n/I0lzUi63m7kkOoRxrtsqL7rN6thxvEO1I+p4A+i/omX+Wnfm7PEHcHn2ePb4g9j8Wnfm7PEHcPZ4GibPZ49nzpw5c+bMafkHnBla2U1pYL0AAAAASUVORK5CYII=';

    return base64_decode($data);
}

    protected static function _getPremiumLogo()
    {

        $data = 'iVBORw0KGgoAAAANSUhEUgAAAUYAAAA8BAMAAADoNw7QAAAAMFBMVEX///8AAACIiIhERES7u7sRERHu7u7MzMxmZmZVVVUiIiLd3d13d3czMzOZmZmqqqrVqWnbAAADyElEQVRo3u2XvW/TQBjGXVqlLSmBh9TQhgaoEd9INESAQK1IICAmRAQSQkhVgJUhLWJg64fKwkLDhgQilD+grWBEgiJYmEIXulEGBtQBmJBY8N3jNCSRY1+UWBn8LO+d7573frkPX6z58uXLV0MEYLfW4lJnjMcgtDyVUHd4xYii9IfqDi8ZqY/qDs8Zoxl1h9eMGKrDoa5UKjVfP+POOhxeyJqMV4fMQqRBjqYwCk2apWxjHE1j7OZe7vwWTwOnDhfbgwfiQDh+7GjCxkEtPoboEFg7jXDu/+22+BRhkexPDCvfnWCCBws4Zro3TyP6LFPNuNksvdW60qBOanwag6UtNg5t0KyfAwRj4AOEwgKXDfdFXf/KhkiieK7ZypQsbDBtnbMwNardkRAvqxkDstQGS3pCPpyELSMdcoxhkPE8qIHi4J9Z793LuL0mY3SQvc6mGbNVI2rljLjB5bRjZImMVEJOLDXBwSsVqcVYpeeOjH30qzC+RlFbaa7STxXGXXZrPTdsGMYesdjimZj1OcN4n6+91mSUBz2X+p03zZki43VjBkJfjFlu31qMeu6AXOfo1EHumMoTQC/1RmwHGq8W89g4iHJl6mgmaMZrGl0WBc6YbwqxyIcZh2oyhs1pHjPji3kZw1UjbqKXCnFd2sX02DDSQcboflHvkT+MvS8z9IrqrLW1Yox2jByq29q17YxlIwbEUpj8VICJ2oD+9Tx2DraRuU8WbvIEs6Eq2jNqWnmsGPHSDEqTu55ojDcy89g52Ea0XZo1B1sby1hSbyVjks+Yx8ZRakvK6eOi9zeLcaKcsSKPjaPU9osF3hlNYuyXN+T4dIFVJ0Y6yjv1poQuAnpzGHmqf4ByZmSX8k4lNYVR/ySqt+GSkQ4vGfX4Em/wWZeMdHjEWP5R0iEu6r9839owVji8Z+zh/VInIwtU8xjbxVujxRnbeEOQUb7xdrhnTALbvGbkPeOecYy9PWZcAAZcMtI84J5xFdjeCMaNQCTjmrEb0LOuGZPW/5WOmPqZicoeaZmoC8AJs7qYd2Zk2lGNyjgyLvAXLRagxEioB+adnWeiDvn//chxwAUjvyzCS4YxPYIhR8aNZuw3TgOqjCHhWI6tX8YFUM6MLK3LmbEHlAojlS//wzCmxNilwhhM18t4jya9wEQhJUbtnQKjtsqefauqjJ1yIiP7Bq1EdyHrBTeMPKPUI2fGUBryGzBpz2ij0DTCuWypPj6CuVyW6Z0VXBsBIvGlC24633qClf1awyQZW1w+o8/YSvIZfUZfvny1mv4BeS80dkvXnIwAAAAASUVORK5CYII=';

        return base64_decode($data);
    }



    protected static function _getLogo()
    {
        $data = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAQDAwQDAwQEBAQFBQQFBwsHBwYGBw4KCggLEA4RERAOEA8SFBoWEhMYEw8QFh8XGBsbHR0dERYgIh8cIhocHRz/2wBDAQUFBQcGBw0HBw0cEhASHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBz/wgARCABpAWIDAREAAhEBAxEB/8QAHQABAAMAAwEBAQAAAAAAAAAAAAcICQQFBgMCAf/EABYBAQEBAAAAAAAAAAAAAAAAAAACAf/aAAwDAQACEAMQAAAAv8ACprbZMAAAAAAAAAAAAAAAAAAAA4xnw3wiuMcwsQm4DAAAAAAAAAAAAAAAAAAAOmKqq4R+j5n8ZdNgAAAAAAAAAAAAAAAAAAH8KutgE/hyC1ZN7AAAAAAAAAAAAAAAAAABnOrumfY86eaVOSbkMztVK7LgMAAAAAAAAAAAAAAAAAGfbdBGZNLtumPm8lnanxDbrp4RETeoJnZ3QAAAAAAAAAAAAAABUtvuCvRwiz5Vg8OqSU3VZBLarqtYnmElMkwAAAAAAAAAAAAAAA/BRZVfG3ATGrfSs8c2wbPOFLFauo7sHEKgN6Y7E8K26ye6MxF2aTBSrTplRmda7GJgYuETUyoivOs7E8Y29aa3N8CejbHDLeslYp834EblrCZGCFW1yaPRFpk9+Y5r0ETZhgGaqpUZdZgqw2qar6oziXr2ilSoyLTso+rVtFCldYeJbYVNuWDwJUlXSGhqf0Qs3NhWk6KdK0uT0B34AAAIQblgraBHfAhszJXsejlghgzBXfxNYlXiRRJd8UQwqGi37KKqsUmnatQUdcS+eRMe17Go9ODzJjCvStOdKresuGn14AAAK6tzNVtcjnAps2B1aVo8ESmVSbVVUvM6gq63WFEomc6ucyqitfEVKVXJs0s+5oWmuraEK2BQBBjc4VbHIr22kKvoawoAAAHljHtejKbKMFdm1+IdVeNM+syjXeFNNVXBTX1vRmlyckl37TUFvoCQmQMrUdAEANzfVssjkn5MyVzymzrPREItoqrVhAAAAFfG0PVNjO+PBs9A3nkmMhAn0suzHdeoaPsZTL0mTmwrXVHizMFemiaMKllkoMi8s4yujfFNmtkSkjlt2ZrtsQQiXAZLAAAAAPkUVV7ktskQM2A23uT9wAAAAADN1XnzTlPJB4Uq+q5qeaDqSqareJ+wP//EADcQAAEEAwABAQQHBwMFAAAAAAQDBQYHAQIIABEQEhMVCRQWITA0NhcYIjE1QVAgQEIyM0NSVv/aAAgBAQABDAD/AEcxSFZ1uK3EN8Sj/FGmINwZJhSmqQ286t/qyaOYkMdF2KMV2fdFoO5MGi81eShq6abcsJ/fmuMzB23eIWtcd2y5yahZ06IyPlm+pU6y4+sbCysq/f4eUsuJJGHplyp8PHHEc0hrlN4piWRd9zyBEfsNP5k2gS2Kv6PKcMFjNs2EYHMI0+lc4wtJboOZStaYxZ0c6khukq6mf5zpMIqcr/h84xnHpnzFYwbkfSSWZj506aVVcVNVFLnaStDHNCiqquCmqilzpJGhjmhRVM2rTlaz4l2ZGaZLm1NzLFagl7vJWgs8gn/DNfVN9Pzsc1swIjod+3bqH/403yt7/k9hz/eG28KxYjE5vGuV3bdqramY45b7ymbsGmT3qiYx8soyxaNs10Daja8jselSy2g6SqqmfRNk6sveXmHaRhtQd8ct3/P7Wsd2Y5RkHIX+EihOaQ7SdAC8fBavLRjZ8x6Plsfak/fcXqR19xbEAGpsa/m0wae85KGeMrJIKPhm6KqSLTyvUrnrXTCOf3ofrnKZTieT78z5pExS/N8wsdxx8AngGJL/AFSYTMv+fjk7AMgm5bkaMEI99YVExK7oqy9ApUXtCoV18JbvpiHkXk7RM2QR8YT0j2r/AH/bFOryuOCzplQ33d+YL3Gt6H6BHr6YlsFFSA7zd9DcYxm6staHaICs2/TXT50T/YdIsPqgm6VG3fGK6oGXMMhL3LO5z51drkeRiz01xIX0jYe9tSxgpyuUdFmmsYEDWUFZYuB6ZSv/AK+bq6XJjkQTQd5NFIPaXVUiVLNciTBInwrXbMNj56S6Pxcq4grN6A3SZtD2I6hIO5VvVjLF3fKWTv8Af7aaqaZ12xjOl283SWr5R+0eo8kpIzS5DpTYTPYYYmGuXGBVt2nEgF8OPyea6fR+SPcnGis5bcBIc2UnTPq6WNMNHVew+kpFbO6Vc04wFgNHNvNoNNNeXR0ykbMOvb/Wrtn0iEcK9yUUlVB9yz4NgQ33RBi8XaIWwhMbGEkE2ew88VrCINOISGDtHuwBqKVbYA16O6rAydT20hhyXkn2UbT6J6VaUMlNltZcl2brm16skKrBYjTo55rKzo5a8bTfY4b8ZCYOuWKIvzrrv7uzT1Hc624oaE0JVXzWXVK6Od97NZkt5rbXQlOSXDZJZKYmZzJ0hm6UDmd8FQDlNwSReIVbMHwVbKJcd6Vut7dW1lBmS6pq9Y9UKoZ3zZrP8WSXff1SyndpkUiMTP5s6BSu5kOSOERBkzi5Bs4BB7gUgIFZndzS0k7gQJny8rNR/WNppZOFWxHW5eq+rQMfHHn4xm+eq7qqB7y0T9mGN3iUgVkMTan01vVaVbd7dj8RJXaoWKlIHOMrdP3olh1Ge/suwr859BoaZXEuhZcoq/73oSRYaJvjR00pW+I1dbMou1ZyG7+XR2GU2yLeG1gAk7vYFIdFywbB8ittZkJn7n0JzgYGe4zEh6Z2F+uhzYmw4iPRNBf2Wdy9XtorqnFtuWt5euBJMAdheLzQBTH7p99GY+rFWIlgaJcAiYXwVMZcuZvAqyilZtv1CLsozek8uorC0Hupu/uBzyZHWFMnqTH75yRxNXukVqjEgWR9HL29XdBlWRIi4qxF5xD+NufBMADWRJw8LFezuOuw32tdJbojjDryxY5Ne28zafHzo0dPvmWChpsTpv7u9DMGZNc0GbcJ+/p59IMUJhpgov8ABk/hNrKLuUw5L7hex3vDNQciS/58rMXz++4cl/w8+kEJEzI4ONj8/wACgL4nktec/wADb030MbbL+uztJW6MJ5K5wBjbMDO5SFovIfZZdSx21RmdB+QzvjsjoIkxzLraNFZRB47oAWcm7zeSh4XYNdca4xjGPTHnUcAEntOyHG6OMn1dPjqznTJKAN84z01YK0HpB8eGpfOhnDza2m3ZlU33MkeSuIM80bU258DwYH+B1u/5YKElWdM5wrtjOdNsaf8AXCmbSPQ9gaUsY109nUE9Wr2mX88NT4TlXkS3nE5jcYS9cYbgBmoAUANLVET2dXLaI8+zfO/kOTVVmEdTR/73eT/gCqGtpxn0W5Fc2KPW5h/kboK2N0p6jqqLNqhe8uBcVZa5TrrGy1j2RhJWR58pEKk4hkHC2hb79IE+ZQikNZcZ84weY1FJ+9SCTvALWNMurKtiDYsViTCPBRoFhdWWQa9AM6qnlrsAfMPM60YZifffavAYz7CjSElMGCj+Oh6nS0+6esGNF+o6hHzjGZyArnGfXXGfLQmKdfV5JJPv9+cfXXx0xjbfYhzr6HiwGEsUZCxj4HssQpIKASshb0yj6e6J6edFQd3l3MTcKAgqQ4sb24x9zFdmY9cJxrbvRxCwiHPmTByUAsuLWez/ADSLO6J6H4HbAKxlCuW6Wmc41U+FnCmf5NRibi1hGI59UfZ9IKeslFoUBj7kOMw9C7/Y8748f5G1RRoKd3o9ABtpq5QLoQkbi0hrINPnd8w0aKzbI5j81ytDd5nd8a0yn74n0gT98eVQ1i/tynUgtnVpa4hWmMbmgGsboSCajsO5UBPm2x6tZnkAYYRbzvJ/yfa7Sz439UufKUxZnO1h6e5p8xEIIaXFFfVPXUuops12FXbFIGhBIUX6QZx3y8wNu/8ADT9PPV0SExkZDARCd+BZ/wD2kMZz5EuJ5+yzaOmumWIpm87ZPVDoZxS0/lSAWh9xwIVXHql7et5elEqNkWMqe4VWEOXsGwY3GkMZz5prqnpjXXHprafH8FsRclyBxvHXy4ueZfTGdCXbRA5k5mmp8JuiLqCLZ0G/AsaHIT+DP8ZIzjRN9Yz4y9OLK6oZHcuPbiEncADjBpWMST2ddVE92xCmXSNh/XHqKAQTk1zy+v8AIvtNZFi2zO7+kowxfxyfOQauktXQF0HlAmgRji4iNAJJ5xKQwfQdsqXLZRbsLhX5PyZSC1TwxZxekPhSnrd/xIb7k+cb+qXELDlno0Yz+/cNXfZmcCTQBD0buN7a+wNiYjziv6MXnRslxK7vmx+P4kuRGXDNQMV/9+yqw+wtoZfQ0fRo4itzEWly8Gc1/Rr7+iS5bFE5Ujp6o8hTYaFXY2YMVwkJ7b80brkqOfsUZKw5OsDkmsSmsakGfvTCNHcQ0DBVtFhfM5xrjOc59Mdb3WhaE2Ta2YnCsZ4toteLAKz+QC5RdWKZM0keZC1NxXxj/OsiwBaCmHzHGmccswkma3bG8JaZyJ+D0LzE1XNph5bF9GuXv9bWbST6g5FNLs0Gw3vyRtQmBpSwAvW7n9IUsvpnRmgqWF3Kf9FX1hURsAd0GmJ8OOgYCr3PnbccSCdOVlTbwUztNXujQ2OffFbCiZyAA/GlTSe3H1Vv8ojkWMBilBcjNVYlDSGTLpPMpXX0GQVWUz6aSZnlsmfXqRrxx92xQDHvHaVg7erpnRW7a4QtWtXuN74x9bTgktztvhOMPvxaOuE6TVCY5yMA5CSnxeWl/WXQmNvvrWrHiNV5FmfGnuZ6Pq/Fq1W7NaCeMu7bEpfoogcBHX/VWuH8PoOndwZY1LpF3DzBNKoOJKFDJeY1U3dSrI1INE+bCnPdy7zrMUbORA38teSXtct/pKstcxA9nZudKuJqKsAGA/KGXfoTjc4t0Nk9cIoqYqbpyX0GLiITiNOJLSt35XGA/fRaZEqVN7wtnpBJaOwWJHtzBRfFocVLFf5/uM5uRe66Qi+wqWFiGCcWdzjarxJpbGzvTPfNa/UPjYbn/Js0e7U6/egxGSMrtsQoqj2ek4xkATfBjz+ER+XX8tj9Qr+UR/UEvb0X/T0fKu/UiHgf5VD8bqn9Umec+/18fxu/Ije21v0ur5FP1Jp41f0wT2yX+gn+R39XY8F/LI+3/8QARRAAAgIBAQMHCAcGBQMFAAAAAQIDBAAFERIhEyIxQVFxchAUQlJhYoGRBjIzQ4KhoiAwUHOSsTRUY7PBJdLTU3aDw/T/2gAIAQEADT8A/YFsPt1V0Kc0iPngINkvM5vufwqtG0skh6FRRtJPcBlHiAlt6sMMZJCGaROe7tleRnnsvqMggiRSV3zNxk3D1DNAied0fV54zPuybhCHt8WaXWafzW5qM1UvuOEZFCc0OD2gZSWTze1OAJw8X2kMvr9of+EalSmqGT1OURl2/nkgR7A0izKZq7ptjP141DJ7QTsOPEI7UVCeU2a5jchG2NEFYc4htjY8EyGlpk8hkjJsbwJLxgEdRKls3bbz6dok8rmOSaYE8XjUFF4jaMpTWZp9L0qzLJNCGQwL0xKrgekwJG9/CZytRKYMZMCTSjgv1cvI8EUNs1ilSNiGZF2Pl1HgihtmsUqRsQzIux+dmsHzCCK4axiqJLINoG6+05dR4IYbLgpUiZwxRf4PSLiaGloxmdArbpJCnP8A23LleGzPqdW/pwi5B4ELjfDngVYZtIjuXtKDmbwV0G98yM9Ll/og8KbPGMLg1ozWieGw4/8ASl2Dnew5GCzHsAG05XfiKOjPOUQkhC27lHTpJ2SCnyLpKssafwXXrUsCeC2RJEfhLsTyap9IZq0IJ2Daz9fsA45fi35DwSa1s6ZZZPQj28AMn6HqvLHIU7UMnNfAguXEqJuCeIHnS7voTRn62Sb/ANHvflnKfb/CJt85qamep78cYKQf1yscuypQhfwc+U/Nk8ifWnsyrGg72JAxOqjBLY/Uilc9ebTpwPyU5cBaGzFt3XAJB/MEfwDQEItiL671OLb3fGc0mJY70XXOvQLC+Lr7Dh1G+8PieAumcrR+3+y5HkuaT7gl+tk1b/pgBUu9n7kxfHs6s1SzZhpaTAC8hSSuikn0UQtlJwbd0gjzojphi7+sjguadOlbZB9lLYXmAD/SiXKEIWSXrmlPGRz4mJOJzJ5zxq0T2H1393IH2T6pqJIqVvciQDZt91M63eY1ovgkfH9Rz0J4LLzj8SSEg5pj2Y2kiPMkBsSMrjvUj+AEbCDxBGQOZ5aFLjNSPpGNPvIT1plQQtbMXGCWeHgsig8RvJzWTKCbCiFTZq+spQ/awnEJCEUpC4HhLZBxFO3KIUfuroTI+TKIB5rEI7MsXQQAvNghy7FsnnH1KidcMWarFvzWUPPo1j1j32xdtnUbg4mCAHifGxO6MpRiOGGMbAB2ntJPEk8SfLXQvLPM4RI1HEsSeAGAlDqd3atcn3Ixz3yfjGkwFRtnsjRDJ/XifcPfmQv/AFqVysQJ4bUIrW0XtR05jZt3JoHAWau/qOvUcoUJ7IbsKRs2TukSB6lckuxA6486ok//ADYU5WLlYoJ69hO1TuZp0QmbkNohsxEgb6gkkEEgEZR0yeWCT1JNwhD8yMvzx1Id+rX4u5Cj7vOqJCF/MV8h5/I3IIZoZ09dSF4rmlbvncEJ5kyN9WVAeIHaMroXmsTuEjjUdJLHgBgJTz+7tjrnwIOe+S8U5WGKl8g6tLg+5F7/AL4QuDiYr9YV5HTtjmi5jd+xsvVUtSU7DgvXDDbusRkJIkuu5FKM+wjjLk/GGUoKaOPcAVpWGeo960gzpRNSjBSwnbFYjyp/jNMnI5WD3h66djDycr5q18oZk5b1IIx9ocmH+CqyMSnsIi3Eyd+SS07m3A7+o6yDfQnLVaKaSGSzOjRsyglSNh2EeV+Jv6WRFJIe112FH+IxOMT3IpK0yHxx72e3W7rj5buenBpkW585n3mxgOUkQbZZj2vIec2UYHszP2IilmPyGanZaYKTt5OPbsRB7FUBc+k0psl+sV0JWIf3f8f7GlymJynRqMyni59wHguWeOjVZ+iFP8x4j6Pl+j00YM3W9aRwjKe5ipzXZU067F4+ET96vk9PzRe+ZxHh1OKeQe5EeVb8k8hs2ZR2iMIoOUdJm5c+N4wozUpa9JPxShj+lGypNJef/wCKMsP1bvkSpZeT+UXQLlPSRBNL6Ad5Qw/KJs02UiFEJAvOPv37R6oy8gnoVpkBFCI8Vf8Amny6VejvQuoG07pBaPwOODDKvM1ieLpnc/cA+oPTyhLyVCpKOZbnXpdu1EweTR676lRl60eIFmA8SBhlOYcvGDsEsDHZIh9hXL4jp1Jk9AzEAuO5N45S0yeeknbISiEj8DN5I7MNtYnJ2cpFIHQ/Mfub6xUE7pZAG/TvYeA78oUIK4A9yMDy3gunVX7HmOwt3hN85ql2Ks5HSkZba7fBAxyrEsMMS9CIoAAHcB5TXiQd5nTG1KqE2dO8Zk2Zq2qR/FIlZz+e5mmUZiLNtwkfKybEUbe4tgHNq6W4syyH8OALWqwp9hQgB+8mPNB62y+wm1G6BsDv1InuL1Zcvy2m7oo93/7sr0PNa012URgyyOCQCfdjwDmU9KInkk+I5o+JGWnEIl4inp8I+qpl6OHzY59JpxTuX+h52dCZmHsEabgzz1HvTWDuxiJecVJ97ZuYOy2uE7AIUllP6VPk0ynJNGO2TojX4uVGajY2F34tJLI/Se9jml1Ug8bgc5+9mJbyw6Xadx2gRMc5P/jNIgo33gjBLyCOPY4A7mJyo2/BZrvuurewj5EdedepaWAkve0JO6fwkYuwSoObLCx47siHip/c1L1SeTwcqFyMhvkduWIUmQjoKsAR5bF+ed++OMAf7mVatuYd/JFcqLvy2LDhUQZpl8U68s3CSwAgYyFfRBLeTXbyk/yYeef1lM0lzqdjwxDmfOQplSlNcbvkcIP9rL4r0ak5+6mjBlU/BzFmnzNDLE4BMcqNsIIPA7CMC8hdq1ohGkVlAA4CjoB4MPJpOlAnxyuzH9Kpl++h01+yatHvL8zKyZSmD8nOgYCRG27rKeBG0bCDlyEb1WEALXlU7JI9g7GBGJBbn+JKLlSqbbyXS4TcDqvog9bDPHP/AOPKmo157fIW3JMSyBm4Mg8lu9Ugfw8rvY+tVSR2hXDfsayF0yDvkPP/AEB81G4iSEDbuRA70rHuQMcUbAB1DJ+Js0AORkfteHo/pK5PJyUWqU9pi3+pHB4oc1a1HptyLqljlIUbfCxDfudVqSQB/UcjmP8ABgDlCZq9iI9Tqdh+B6QesZ9HIhXeI9M9YcI5B3DYh8um6iHSEyiMGKQFH4t2c05DE8MWh6I+yvU3xzhLIf8AnZ4MeTZQ0LTkdo4+5Bxd/fOane87jrCQO8acmi8/ZwByrG0s08pCpGijaWJPQAMqgUdLh6zED9fZ2u3HNd3ZbMfXWiH1IfzJbNO5Ggn4IwW/WzZq9+zb+AIiH+1n0h5lr3Lif96Z9JisHsit9Eb/AIvqeSK8aUfhgAi/umXhNdf8crkfpC59JwbSdiWV4TL8eD5r779Mv6Fwf+RcoTy0bHsEoBQ/OPNbgfSy/vuVaP8AWgH7H0ekjeRIOgWIiJTEG6C25mlajXtv7UV1J/LLEayxSodqujDaGB7CPIOJJz6P78cMvVYnPCSXu9Bc1SHc02CVOfBWPTJ4pM+j1hK15NhHJu6BwAevgfJNFHFAO2cyryeaNKNUtP2JEQV+cm6P3UCBBaIJitoOhJR/ZspvvV9UpbZIe8SptXvU4nTaqz+aSHxLsZc7bepb/wClEyY7DFpdY0q/c0zkEj8eV0M0mlaDCbdyTtAPb4Q2DmPfmIOpTfzEk2H4b+dUJgSL8y+FgTBFzIH7DPZcKG8IyLjAE/w1E+4Dxd/fORKXY+wDac1K5NbeU0JtnPct6uJpkMjo3Ah5Bvn82yWLlqUh+7spxjPz4HIXKkxUJjuOp2HYQvSDn0aquNQgngeOSxyaEpKAw9MDLLvO8poTbCzEk8d3KOmVoCvYwjUH88pDz3Tv5yA80eJdqZA6zRTxUJtsbqQQwIXpBGTwmhrFKxCYSJgBz1BAIB4OpwPth1SkhLxj/VReKEdo5uVgIxqtAqZnA9eNiAT7Qc6ovNUj/NnyfmTW4jz3B7bTBUjHh52PLLbvGAkoZXPUevYoUZZczWdD4IUc9LwE8PwZT5kEVlDXtUx6oL8HXD9wYYR+rlcn5k/m3F5weqWyQqIuQkSQaTDxrQP2ufvTixsY4y26HYDgpPVtPXmuyu+pwS/YW95ywMU4BTapPDP8pyEfT4uUypLvwiclKyP1yyzEAOwHUuXCJb+olNhnfsHYi9Q/d7p/tm+c2jy7ub4zcX+377fOb4zk1/t+xyn/ADnJjy8mc5XN0eX/xAAUEQEAAAAAAAAAAAAAAAAAAACQ/9oACAECAQE/AB2//8QAHBEAAwEBAQEBAQAAAAAAAAAAAAERECAwUAJA/9oACAEDAQE/AOH89/JYxj+dfjzuD+TBEGPYT4KHqxsWTKX4V2lINi6mzZrWTXk2ZMvE6pSl4fCG9QxahiHiGfkb4Q9WIfmuVysQyahk4hPFZPJcofCGIfC4WUuLlbSj83q16z8jEPEMQxD8fyPUPyTyEykJl8VtITGflkJl2e71fwIft//Z';

        $data = base64_decode($data);
        return $data;

    }



    public static function track($no, CourierLabelNew $courierLabelNew)
    {
//         method only calls updating method which goes another way round - makes updates for all new stats found
        self::updateTtData();

        return false;
    }

    public static function updateTtData($justReturnData = false)
    {
        $CACHE_NAME = 'B2C_UPDATE_TT_FTP';

        MyDump::dump('b2c_tt_log.txt', 'START');

        if(!$justReturnData) {
//            // prevent running mechanism too often
            $flag = Yii::app()->cache->get($CACHE_NAME);
            if ($flag) {
                MyDump::dump('b2c_tt_log.txt', 'LOCKED!');
                return true;
            }
//
            Yii::app()->cache->set($CACHE_NAME, true, 60 * 15);
        }

        MyDump::dump('b2c_tt_log.txt', ' FTP...');

        $conn_id = ftp_connect(self::FTP_HOST);

        if(!ftp_login($conn_id, self::FTP_USER, self::FTP_PASS))
        {
            Yii::log('B2C TT FTP LOGIN ERROR!', CLogger::LEVEL_ERROR);
            return false;
        }
        ftp_pasv($conn_id, true);

        $contents = ftp_nlist($conn_id, self::FTP_DIR);

        if(!S_Useful::sizeof($contents))
        {
            Yii::log('B2C TT FTP FILES NOT FOUND!', CLogger::LEVEL_ERROR);
            return false;
        }

        $ttData = [];
        if(is_array($contents))
            foreach($contents AS $file)
            {


                $array = explode('.', $file);
                $extension = end($array);

                // files with TT data starts with "cdt"
                if(strcasecmp($extension, 'xml') == 0)
                {

                    if(!TtFileProcessingLog::isProcessed(GLOBAL_BROKERS::BROKER_B2C_EUROPE, $file))
                    {
                        // new file found - lock for 3 hours
                        Yii::app()->cache->set($CACHE_NAME, true, 60 * 60 * 3);//
                        MyDump::dump('b2c_tt_log.txt', ' FOUND NEW FILE: '.$file);

                        ob_start();
                        $result = ftp_get($conn_id, "php://output", self::FTP_DIR.$file, FTP_BINARY);
                        $data = ob_get_contents();
                        ob_end_clean();

                        $data = simplexml_load_string($data);

                        foreach($data->children() AS $item)
                        {
                            $ttNo = (string) $item->OrderNumber;
                            $ttStatus = (string) $item->TrackEventStatus;
                            $ttDate = (string) $item->TrackEventDate;
                            $ttLocation = NULL;

                            if(!isset($ttData[$ttNo]))
                                $ttData[$ttNo] = [];

                            $ttData[$ttNo][] = [
                                'name' => $ttStatus,
                                'date' => $ttDate,
                                'location' => $ttLocation,
                            ];

                        }

                        if(!$justReturnData)
                            TtFileProcessingLog::markAsProcessed(GLOBAL_BROKERS::BROKER_B2C_EUROPE, $file);
                    }
                }
            }

//        file_put_contents('glsnl_tt_log.txt', date('Y-m-d H:i:s').' DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData)."\r\n", FILE_APPEND);
        MyDump::dump('b2c_tt_log.txt', 'DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData));

        if($justReturnData)
            return $ttData;

        foreach($ttData AS $ttNumber => $ttDataForItem)
        {
//                        searchForItem
            $couriers = CourierExternalManager::findCourierIdsByRemoteId($ttNumber, false, false, GLOBAL_BROKERS::BROKER_B2C_EUROPE);

            /* @var $courier Courier */
            foreach($couriers AS $courier)
            {
                if($courier->isStatChangeByTtAllowed())
                {
                    $temp = [];
                    CourierExternalManager::processUpdateDate($courier, $ttDataForItem, false, GLOBAL_BROKERS::BROKER_B2C_EUROPE, $temp);
                }
            }
        }

    }

}

