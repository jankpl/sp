(function($) {

    var ignoreFirstUpdate = true;

    var viewTimeoutHandler;
    var formActive = true;

    $(document).ready(function(){

        $('#operator').focus();

        $("[data-service-code]").on('change', function(){
            $("#service-description").hide();
            updateServiceData();

        });

        $("#service-description").hide();
        $("#palletways").hide();

        updateServiceData();

        prepareAdditions();

        if($('[data-address-more]').find('input[type=text]').val()!='')
            $('[data-address-more]').show();

        $('[data-show-address-more]').on('click', showAddressMore);

        //$('#selected-currency').on('change', function(){
        //    updateView(true);
        //});

        $("[data-dimensions]").TouchSpin({
            step: 1,
            decimals: 0,
            max: 140,
            min: 1,
            boostat: 5,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });

        $("[data-weight]").TouchSpin({
            step: 0.1,
            decimals: 1,
            boostat: 5,
            max: $(this).attr('data-max'),
            min: 0.1,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });


        $('.bootstrap-touchspin-down').attr('tabindex', -1);
        $('.bootstrap-touchspin-up').attr('tabindex', -1);

        $('[data-address-line-2]').on('change', function(){
            $(this).closest('[data-address-more]').slideDown();
            $(this).closest('.row').prev().find('[data-show-address-more]').hide();
        });

        $('#courier-ooe-form').on('submit', function(e)
        {
            if(!formActive) {
                e.preventDefault();
                $('.overlay').remove();
                return false;
            }
        });
        $('[data-bootstrap-checkbox]').checkboxpicker({
            offClass : 'btn-default btn-lg',
            onClass : 'btn-primary btn-lg',
            defaultClass: 'btn-lg',
        });


        $('[data-sp-points-selector]').on('change', updateSpPoints);


        $('[name="country_suggester_trigger"]').on('change', updateView);

        $('[data-dependency]').on('change', updateView);

    });



    var FormControl = (function() {
        "use strict";

        var that = {};

        that.submit = function () {
            var $form = $("#courier-ooe-form");

            var $input = $('<input>');
            $input.attr('name', 'forceReload');
            $form.append($input);

            formActive = true;
            $form.submit();
        };
        return that;
    }());

    var Overlay = (function() {
        "use strict";

        var that = {};
        var $overlay = $("<div>");
        var $textBottom = $("<div>");
        var $textTop = $("<div>");
        var $loader = $("<div>");

        that.isShow = function(){
            return $("#overlay").is(':visible');
        };

        that.show = function() {
            var $form = $("#courier-ooe-form");

            $overlay.addClass('overlay');


            $loader.addClass('loader');

            $overlay.append($loader);


            $textTop.addClass('loader-text-top');
            $textTop.html(yii.loader.textTop);


            $textBottom.addClass('loader-text-bottom');
            $textBottom.html(yii.loader.textBottom);

            $overlay.append($textTop);
            $overlay.append($textBottom);

            $form.css('position','relative');
            $form.append($overlay);

            $textTop.hide().delay(1000).fadeIn();
            $textBottom.hide().delay(3000).fadeIn();
        };

        that.hide = function() {
            $overlay.remove();
        };

        return that;
    }());


    function prepareAdditions()
    {
        checkboxAddtions();

        $(".addition_item").children().not(".with-checkbox").css("cursor", "pointer");
        $(".addition_item").children().not(".with-checkbox").on('click', additionsClickableAll);
    }

    function additionsClickableAll()
    {
        var $checkBox = $(this).parent().find("input[type=checkbox]");
        $checkBox.prop("checked", !$checkBox.prop("checked"));
    }

    function checkboxAddtions()
    {
        $('[data-courier-addition]').checkboxpicker({
            offClass : 'btn-default btn-sm',
            onClass : 'btn-primary btn-sm',
            defaultClass: 'btn-sm',
        });
    }

    function disableButton($button)
    {
        $button.prop('disabled', true);
        $button.addClass('disabled');
    }

    function enableButton($button)
    {
        $button.prop('disabled', false);
        $button.removeClass('disabled');
    }

    function showAddressMore()
    {
        $(this).parent().parent().parent().find('[data-address-more]').slideDown();
        $(this).remove();
    }

    function updateSpPoints()
    {

        $.ajax({
            url: yii.urls.spPointsDetails,
            data: { id: $('[data-sp-points-selector]').val()},
            method: 'POST',
            dataType: 'json',
            success: function(result){
                $("#sp-points-details").html(result);
            },
            error: function(e){
                //alert(yii.loader.error);
                FormControl.submit();
            }
        })
    }

    function updateView(force) {
        //if (ignoreFirstUpdate) {
        //    ignoreFirstUpdate = false;
        //    return true;
        //}

        if (!force || force === undefined || force.target) {

            if ($("input[data-dependency]").filter(function () {
                    return $.trim($(this).val()).length == 0
                }).length > 0)
                return true; // not all required fileds are filled
        }

        $('[data-continue]').hide();

        disableButton($('[data-continue-button]'));

        clearTimeout(viewTimeoutHandler);
        viewTimeoutHandler = setTimeout(_updateView, 500);
    }

    function _updateView()
    {
        Overlay.show();
        checkIfAvailable();
    }


    function checkIfAvailable()
    {

        var operator = $('#operator').val();
        var receiver_country_id = $('[data-country="receiver"]').val();

        if(operator == '')
        {
            $('[data-continue]').show();
            enableButton($('[data-continue-button]'));
            $('[data-not-available]').hide();
            formActive = true;

            Overlay.hide();
            return true;
        }

        $.ajax({
            url: yii.urls.checkIfAvailable,
            data: { receiver_country_id: receiver_country_id, operator: operator},
            method: 'POST',
            dataType: 'json',
            success: function(result){

                if(result)
                {
                    $('[data-continue]').show();
                    enableButton($('[data-continue-button]'));
                    $('[data-not-available]').hide();
                    formActive = true;

                    Overlay.hide();

                } else {
                    $('[data-not-available]').show();
                    $('[data-continue]').hide();
                    disableButton($('[data-continue-button]'));
                    formActive = false;
                    Overlay.hide();
                }

                return result;
            },
            error: function(e){

                //alert(yii.loader.error);
                FormControl.submit();
            }
        });
    }


    function updateServiceData()
    {
        var $selected = $("[data-service-code]").find('option:selected');

        var text = $selected.text();
        var service_id = $selected.val();

        if(service_id == '')
            return;

        $("[data-service-name]").val(text);

        $.post(yii.urls.serviceDescription, {service_name: service_id}, function(result){
            $("#service-description").html(result.desc);
            $("#service-description").fadeIn();

            if(result.palletways)
            {
                $("#palletways").show();
            } else {
                $("#palletways").hide();
            }


        } , 'json');
    }


})(jQuery);