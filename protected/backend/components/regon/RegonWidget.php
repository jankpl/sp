<?php

class RegonWidget extends CWidget{

    public $formAttrNames;
    public $button = false;
    public $attrMap = [];

    public static function actions(){
        return array(

            'RegonActions'=>'application.backend.components.regon.RegonActions',
        );
    }

    public function run()
    {
        if($this->button)
            $this->render('button',[
            ]);
        else
            $this->render('widget',[
                'formAttrNames' => $this->formAttrNames,
                'attrMap' => $this->attrMap,
            ]);
    }

}