<?php

class CourierLabelNew_CourierExternalReturn extends CourierLabelNew
{
    public function attributeLabels()
    {
        return [
            'track_id' => Yii::t('courier', 'Zew. nr śledzenia'),
            '_ext_operator_name' => Yii::t('courier', 'Nazwa zew. operatora'),
        ];
    }

    public function rules()
    {
        $rules = [
            array('track_id, _ext_operator_name', 'length', 'max' => 15),
            array('track_id, _ext_operator_name', 'filter', 'filter' => array( $this, 'filterStripTags')),
            array('track_id, _ext_operator_name', 'application.validators.lettersNumbersBasicValidator'),
        ];

        return $rules;

        return array_merge(parent::rules(), $rules);
    }

    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
        );
    }

}