<?php ///* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/logged'); ?>
    <div id="content">

        <?php if($this->contentTitle)
            echo '<h2>'.$this->contentTitle.'</h2>';
        ?>

        <?php if($this->menu):?>
            <div class="navbar">
                <div class="navbar-inner">
                    <a class="brand" href="#">Actions:</a>
                    <div class="btn-group">
                        <?php foreach($this->menu AS $item):?>
                            <?php
                            if(!isset($item['visible']) OR $item['visible'] == true):
                                echo CHtml::link($item['label'], $item['url'], CMap::mergeArray($item['linkOptions'], array('class' => 'btn')));
                            endif;
                            ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>


        <?php if(0):?>
            <div id="column1SideMenu">
                <?php
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title'=>'Inne',
                ));

                $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'operations'),
                ));
                $this->endWidget();
                ?>
            </div>
        <?php endif; ?>
        <?php echo $content; ?>
    </div><!-- content -->
<?php $this->endContent(); ?>