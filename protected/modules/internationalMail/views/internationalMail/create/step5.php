<?php
/* @var $this InternationalMailController */
/* @var $model InternationalMail */
/* @var $form CActiveForm */
?>

<div class="form" style="">

    <h2><?php echo Yii::t('internationalMail','International Mail'); ?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '5/6')); ?> | <?php echo Yii::t('internationalMail','Podsumowanie'); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'international-mail-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <h3><?php echo Yii::t('internationalMail','International Mail'); ?></h3>
    <h4><?php echo Yii::t('internationalMail','Podsumowanie'); ?></h4>
    <table class="list hLeft" style="width: 550px;">
        <tr>
            <td><?php echo Yii::t('internationalMail','Przesyłek'); ?></td>
            <td><?php echo $model->mails_number; ?></td>

        </tr>
        <tr>
            <td><?php echo Yii::t('internationalMail','Cena za jedną przesyłkę'); ?></td>
            <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price_each,true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price_each));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('internationalMail','netto/brutto');?></td>
        </tr>
        <tr>
            <td><?php echo Yii::t('internationalMail','Łączna cena'); ?></td>
            <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price, true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?>  <?php echo Yii::t('internationalMail','netto/brutto');?></td>
        </tr>


    </table>

    <h3><?php echo Yii::t('internationalMail','Parametry przesyłki'); ?> <?php echo TbHtml::submitButton(Yii::t('app','edytuj'),array('name'=>'step1', 'style' => 'display: inline')); ?> </h3>
    <table class="list hLeft" style="width: 550px;">
        <tr>
            <td><?php echo $model->getAttributeLabel('mail_weight'); ?> [<?php echo InternationalMail::getWeightUnit();?>]</td>
            <td><?php echo $model->mail_weight; ?></td>
        </tr>
        <tr>
            <td><?php echo Yii::t('internationalMail','Wymiary'); ?> [<?php echo InternationalMail::getDimensionUnit();?>]</td>
            <td><?php echo $model->mail_size_l.' x '.$model->mail_size_d.' x '.$model->mail_size_w; ?></td>
        </tr>
    </table>

    <h4><?php echo $model->getAttributeLabel('additions');?> <?php echo TbHtml::submitButton(Yii::t('app','Edytuj'),array('name'=>'step4', 'style' => 'display: inline')); ?></h4>
    <table class="list hTop" style="width: 500px;">
        <tr>
            <td><?php echo Yii::t('internationalMail','Nazwa');?></td>
            <td><?php echo Yii::t('internationalMail','Cena za 1 przesyłkę');?></td>
        </tr>
        <?php

        /* @var $item InternationalMailAdditionList */
        if(!S_Useful::sizeof($model->listAdditions(true)))
            echo '<tr><td colspan="2">'.Yii::t('app','Brak').'</td></tr>';
        else
            foreach($model->listAdditions(true) AS $item): ?>
                <tr>
                    <td><?php echo $item->getClientTitle();; ?></td>
                    <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice(),true));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice()));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('internationalMail','brutto/netto');?></td>
                </tr>

            <?php endforeach; ?>
    </table>

    <h3><?php echo Yii::t('internationalMail','Nadawca'); ?> <?php echo TbHtml::submitButton(Yii::t('app','edytuj'),array('name'=>'step2', 'style' => 'display: inline')); ?> </h3>

    <?php

    $this->renderPartial('_addressData/_listInTable',
        array('model' => $model->senderAddressData,
        ));

    ?>

    <h3><?php echo Yii::t('internationalMail','Odbiorca'); ?>  <?php echo TbHtml::submitButton(Yii::t('app','edytuj'),array('name'=>'step3', 'style' => 'display: inline')); ?> </h3>
    <?php

    $this->renderPartial('_addressData/_listInTable',
        array('model' => $model->receiverAddressData,
        ));

    ?>
    <br/>
    <br/>
    <div class="row">
        <?php echo $form->labelEx($model,'regulations'); ?>
        <?php echo $form->checkbox($model, 'regulations'); ?>
        <?php echo $form->error($model,'regulations'); ?>
    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step4', 'class' => 'btn-small'));
        echo ' ';
        echo TbHtml::submitButton("Zapisz i idź do kasy",array('name'=>'finish', 'class' => 'btn-primary'));
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p><?php echo Yii::t('internationalMail','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie'); ?>.</p>
        <p><?php echo Yii::t('internationalMail','Będzie można je opłacić od razu lub też zrobić to w późniejszym terminie'); ?>.</p>
    </div>
</div><!-- form -->