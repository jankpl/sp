<?php

$this->breadcrumbs = array(
    SpecialTransport::label(2),
    Yii::t('app', 'Index'),
);

?>

    <h2><?php echo Yii::t('specialTransport','Transport specjalny');?></h2>

<?php
if(Yii::app()->user->isGuest OR !Yii::app()->user->getModel()->isPremium):
    echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, Yii::t('site', 'Dostęp do tej usługi jest możliwy tylko dla wybranych klientów. Skontaktuj się z znami w sprawie uzyskania dostępu.'), array('closeText' => false));
else:
    ?>

    <div style="text-align: center;">
        <?php echo CHtml::link(Yii::t('specialTransport','Utwórz nową przesyłkę'),array('/specialTransport/specialTransport/create'), array('class' => 'btn')); ?>

        <?php echo CHtml::link(Yii::t('specialTransport','Lista Twoich przesyłek'),array('/specialTransport/specialTransport/list'), array('class' => 'btn')); ?>
    </div>

<?php endif;?>