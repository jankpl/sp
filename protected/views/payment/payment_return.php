<?php
/* @var $order Order */
?>
    <h2><?php echo Yii::t('payment', 'Płatność online');?></h2>

<?php
if($paid):
    ?>

    <?php echo $htmlPaid;?>

    <?php
else:
    ?>
    <div id="waiting">
        <h4><?php echo Yii::t('payment', 'Twoja płatność jest w trakcie weryfikacji.');?></h4>
    </div>
    <br/>
    <div style="height: 100px; margin: 0 auto; text-align: center; position: relative;" id="working-wrapper">
        <div id="working" style="position: absolute; left: 0; right: 0;"><?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, Yii::t('payment','sprawdzam...'), array('closeText' => false));?></div>
    </div>
    <div id="success"></div>
    <script>
        $(document).ready(function(){
            var looper = setTimeout(checkStatus, 1500);
            animate();
        });


        function animate()
        {
            $("#working").fadeOut('slow', function(){
                $("#working").fadeIn('slow', function(){
                    setTimeout(animate, 1000);
                });
            });
        }

        function checkStatus()
        {
            <?php echo CHtml::ajax(array(
            'url' => Yii::app()->createUrl('payment/ajaxCheckIsPaid'),
            'dataType' => 'json',
            'type' => 'post',
            'data' => 'js:{hash: "'.$order->hash.'"}',
            'async' => true,
            'success' => 'js:function(result){
                              
                              if(result.result == "OK")
                              {
                                $("#working-wrapper").remove();
                                $("#waiting").hide();
                                $("#success").html(result.html);
                              
                                }
                              else
                                  setTimeout(checkStatus, 3000);
                            }',
            'error' => 'js:function(result){console.log(result)}'
        ));
            ?>
        }
    </script>

    <?php
endif;
?>