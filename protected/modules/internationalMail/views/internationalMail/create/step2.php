<?php

/* @var $this InternationalMailController */
/* @var $model AdressData */
/* @var $model2 UserAddress */
/* @var $form CActiveForm */
/* @var $saveToContactBook bool */
/* @var $countries CountryList[] */

?>


<div class="form">

    <h2><?php echo Yii::t('internationalMail','International Mail'); ?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '2/6')); ?> | <?php echo Yii::t('internationalMail','Dane nadawcy'); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'address-data',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));
    ?>

    <?php

    $this->renderPartial('_addressData/_form',
        array('form' => $form,
            'model' => $model,
            'model2' => $model2,
            'saveToContactBook' => $saveToContactBook,
            'type' => $type,
            'countries' => $countries,
        ));

    ?>

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step1', 'class' => 'btn-small'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Dalej'),array('name'=>'step3', 'class' => 'btn-primary'));
        echo ' ';
        echo $jumpToEnd? TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'step5', 'class' => 'btn-small')):'';
        $this->endWidget();
        ?>
    </div>

</div><!-- form -->