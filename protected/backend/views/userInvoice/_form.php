<?php
/* @var $this UserInvoiceController */
/* @var $model UserInvoice */
/* @var $form CActiveForm */

$readyOnly = $model->stat != UserInvoice::UNPUBLISHED ? true : false;
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(

        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div style="overflow: auto;">
        <div style="float: left; width: 48%;">
            <div class="row">
                <?php echo $form->labelEx($model,'user_id'); ?>
                <?php echo $form->dropDownList($model,'user_id', CHtml::listData(User::model()->findAll(),'id','login'), ['prompt' => '-', 'disabled' => $readyOnly, 'data-user-selector' => 'true']); ?>
                <?php echo $form->error($model,'user_id'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'title'); ?>
                <?php echo $form->textField($model,'title', ['disabled' => $readyOnly]); ?>
                <?php echo $form->error($model,'title'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'note'); ?>
                <?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'note'); ?>
            </div>


            <?php
            if($model->isNewRecord):
                ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'_file_invoice'); ?>
                    <?php echo $form->fileField($model, '_file_invoice'); ?>
                    <?php echo $form->error($model,'_file_invoice'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'_file_sheet'); ?>
                    <?php  echo $form->fileField($model, '_file_sheet'); ?>
                    <?php echo $form->error($model,'_file_sheet'); ?>
                </div>
                <?php
            else:
                ?>
                <?php if($model->inv_file_name != ''): ?>
                <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file - Invoice', array('/userInvoice/getFile', 'id' => $model->id, 'type' => UserInvoice::FILE_TYPE_INV), array('class' => 'btn btn-primary')), array('closeText' => false));?>
            <?php endif; ?>
                <?php if($model->sheet_file_name != ''): ?>
                <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file - Sheet', array('/userInvoice/getFile', 'id' => $model->id, 'type' => UserInvoice::FILE_TYPE_SHEET), array('class' => 'btn btn-primary')), array('closeText' => false));?>
            <?php endif; ?>
                <?php
            endif;
            ?>
        </div>
        <div style="float: right; width: 48%;">

            <div class="row">
                <?php echo $form->labelEx($model,'inv_date'); ?>
                <?php echo $form->textField($model,'inv_date', ['date-picker' => 'true', 'disabled' => $readyOnly]); ?> (*)
                <?php echo $form->error($model,'inv_date'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'inv_payment_date'); ?>
                <?php echo $form->textField($model,'inv_payment_date', ['date-picker' => 'true']); ?> (*)
                <?php echo $form->error($model,'inv_payment_date'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'inv_currency'); ?>
                <?php echo $form->dropDownList($model,'inv_currency', CHtml::listData(Currency::model()->findAll(),'id', 'short_name'), ['prompt' => '-', 'disabled' => $readyOnly]); ?>
                <?php echo $form->error($model,'inv_currency'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'inv_value'); ?>
                <?php echo $form->numberField($model,'inv_value', ['step' => 0.01, 'data-inv-value' => 'true', 'disabled' => $readyOnly]); ?>
                <?php echo $form->error($model,'inv_value'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'paid_value'); ?>
                <?php echo $form->numberField($model,'paid_value', ['step' => 0.01, 'data-paid-value' => 'true']); ?> <a href="#" class="btn btn-xs btn-primary" data-paid-value-fill="true">100%</a>
                <?php echo $form->error($model,'paid_value'); ?>
            </div>
            <br/>
            <br/>
            <div class="row">
                <?php echo CHtml::label('Specjalny status', 'UserInvoice_stat_inv'); ?>
                <?php echo $form->dropDownList($model,'stat_inv', UserInvoice::getStatInvArray()); ?>
                <?php echo $form->error($model,'stat_inv'); ?>
            </div>
        </div>
    </div>

    <hr/>

    <?php

    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.jquery.min.js');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.min.css');

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('jquery.ui');
    Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    $script = "$( function() {
            $('[date-picker]').datepicker({
            dateFormat : 'yy-mm-dd'
            });
            
            $('[data-paid-value-fill]').on('click', function(){
                 $('[data-paid-value]').val($('[data-inv-value]').val());
            });
                           
                    } );";


    $script .= "
 $( function() {
       
     $('[name=\'UserInvoice[user_id]\']').chosen({allow_single_deselect:true});
        $('.chosen-drop').css('width', '300px');
        $('[name=\'UserInvoice[user_id]\']').hide();
    });
";

    Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);
    ?>

    <div class="row buttons text-center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn-primary btn-large']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->