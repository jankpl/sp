<?php
/* @var $model CountryGroup */
?>

<?php
$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
);
?>

    <h1>Pricing for country group: <?php echo GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
foreach (Yii::app()->user->getFlashes() as
         $key => $message) {
    echo '<div class="flash-' . $key . '">' .
        $message . '</div>';
}
?>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'name',
        'date_entered',
        'date_updated',
    ),
)); ?>
<br/>

    <h2>International Mail price</h2>

    <table class="list hLeft" style="width: 500px;">
        <tr>
            <td>Regular price</td>
            <td><?php echo $model->numberOfInternationalmailPrices()>0?'yes':'no';?></td>
            </tr>
        <tr>
            <td>Group prices</td>
            <td><?php echo $model->numberOfInternationalmailPrices(true);?></td>
            </tr>
        <tr>
            <td>Action</td>
            <td><?php echo CHtml::link('edit', array('/internationalMail/internationalMailPrice/internationalMailPrice', 'id' => $model->id), array('class' => 'likeButton')); ?></td>
        </tr>
    </table>


    <h2><?php echo GxHtml::encode($model->getRelationLabel('countryLists')); ?></h2>
<?php
echo GxHtml::openTag('ul');
foreach($model->countryLists as $relatedModel) {
    echo GxHtml::openTag('li');
    echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('/countryList/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
    echo GxHtml::closeTag('li');
}
echo GxHtml::closeTag('ul');
?>