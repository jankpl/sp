<?php

Yii::import('application.modules.courier.models._base.BaseCourierTypeReturn');

class CourierTypeReturn extends BaseCourierTypeReturn
{
    CONST DHL_DE_RETOURE_TARGET_ADDRESS_SP_POINT_ID = 4;
    CONST ROYAL_MAIL_TARGET_ADDRESS_SP_POINT_ID = 7;
    CONST CORREOS_TARGET_ADDRESS_SP_POINT_ID = 4;


    CONST DIRECT_LINK_TARGET_ADDRESS_HUB_ID = 13;

//    CONST OPERATOR_ROYAL_MAIL = 1;


    public $regulations;
    public $regulations_rodo;

    const SCENARIO_STEP_1 = 'step_1';


    const MODE_PREGENERATED = 0;
//    const MODE_ON_DEMAND = 1;

    const TYPE_PICKUP_UPS = 1;
    const TYPE_HUB = 100;
    const TYPE_DHL_DE_RETURN = 101;
    const TYPE_RUCH = 102;
    const TYPE_ROYAL_MAIL = 103;
    const TYPE_LP_POST = 104;
    const TYPE_DHL_DE = 105;
    const TYPE_COLISSIMO = 106;
    const TYPE_B2C_RYP = 107;
    const TYPE_PACCOREVERSE = 108;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array_merge(RodoRegulations::regulationsRules(), parent::rules());
    }

    public function attributeLabels()
    {
        return CMap::mergeArray(parent::attributeLabels(), RodoRegulations::attributeLabels());
    }

    public function beforeValidate()
    {
        if($this->base_courier_id) {

            $cmd = Yii::app()->db->createCommand();
            $found = $cmd->select('COUNT(*)')->from('courier_type_return')->where('base_courier_id = :courier_id', [':courier_id' => $this->base_courier_id])->queryScalar();


            if ($found)
                $this->addError('base_courier_id', Yii::t('courier', 'Return order already created for this package'));
        }

        return parent::beforeValidate();
    }

    public function beforeSave()
    {
        if($this->base_courier_id) {
            $cmd = Yii::app()->db->createCommand();
            $found = $cmd->select('COUNT(*)')->from('courier_type_return')->where('base_courier_id = :courier_id', [':courier_id' => $this->base_courier_id])->queryScalar();

            if ($found)
                throw new Exception(Yii::t('courier', 'Return order already created for this package'));
        }

        return parent::beforeSave();
    }

    /**
     * @param $receiver_country_id
     * @return bool
     */
    public static function isAutomaticReturnAvailable($receiver_country_id)
    {
        return self::countryToCourierLabelNewOperator($receiver_country_id, 0, 0) ? true : false;
    }

    public static function getAvailableCountryList($models = false)
    {
        $codes = array_keys(DhlDeRetoureClient::getCountryListDeliveryNames());

        if($models)
        {
            return CountryList::model()->findAllByAttributes(['code' => $codes]);
        } else {
            return $codes;
        }
    }

    public static function getAvailableAutomaticCoutryList()
    {
        return [
//            CountryList::COUNTRY_PL,
            CountryList::COUNTRY_DE,
            CountryList::COUNTRY_UK,
            CountryList::COUNTRY_AT,
            CountryList::COUNTRY_BE,
            CountryList::COUNTRY_HR,
            CountryList::COUNTRY_CY,
            CountryList::COUNTRY_CZ,
            CountryList::COUNTRY_BG,
            CountryList::COUNTRY_EE,
            CountryList::COUNTRY_FI,
            CountryList::COUNTRY_FR,
            CountryList::COUNTRY_GR,
            CountryList::COUNTRY_IE,
            CountryList::COUNTRY_IT,
            CountryList::COUNTRY_LV,
            CountryList::COUNTRY_LU,
            CountryList::COUNTRY_LT,
            CountryList::COUNTRY_MT,
            CountryList::COUNTRY_NL,
            CountryList::COUNTRY_PT,
            CountryList::COUNTRY_RO,
            CountryList::COUNTRY_HU,
            CountryList::COUNTRY_SK,
            CountryList::COUNTRY_SI,
            CountryList::COUNTRY_ES,
            CountryList::COUNTRY_SE,
            CountryList::COUNTRY_CH,
            CountryList::COUNTRY_DK,
            CountryList::COUNTRY_NO,

        ];
    }


    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bAcceptRegulationsByDefault' =>
                    [
                        'class'=>'application.models.bAcceptRegulationsByDefault'
                    ],
            ]
        );
    }

    public static function countryToCourierLabelNewOperator($country_id, $weight, $user_id = false)
    {
        //
        switch($country_id)
        {
            case CountryList::COUNTRY_FR:
//                return GLOBAL_BROKERS::BROKER_COLISSIMO;
//                break;

            case CountryList::COUNTRY_DE:
//                return CourierLabelNew::OPERATOR_DHLDE;
//                break;
            case CountryList::COUNTRY_AT:
            case CountryList::COUNTRY_BE:
            case CountryList::COUNTRY_HR:
            case CountryList::COUNTRY_CY:
            case CountryList::COUNTRY_CZ:
            case CountryList::COUNTRY_BG:
            case CountryList::COUNTRY_EE:
            case CountryList::COUNTRY_GR:
            case CountryList::COUNTRY_IE:

            case CountryList::COUNTRY_LV:
            case CountryList::COUNTRY_LU:
            case CountryList::COUNTRY_LT:
            case CountryList::COUNTRY_MT:
            case CountryList::COUNTRY_NL:
            case CountryList::COUNTRY_PT:
            case CountryList::COUNTRY_RO:
            case CountryList::COUNTRY_HU:
            case CountryList::COUNTRY_SK:
            case CountryList::COUNTRY_SI:
            case CountryList::COUNTRY_CH:
                return GLOBAL_BROKERS::BROKER_DHLDE_RETOURE;
                break;
            case CountryList::COUNTRY_DK:
                if($user_id == 2216) // Sepcial rule for user IELM (#2216)
                    return GLOBAL_BROKERS::BROKER_DIRECT_LINK;
                else
                    return GLOBAL_BROKERS::BROKER_DHLDE_RETOURE;
                break;
            case CountryList::COUNTRY_ES:
                if($user_id == 2621) // MOLEO2 @12.03.2019
                    return GLOBAL_BROKERS::BROKER_DHLDE_RETOURE;
                else
                    return GLOBAL_BROKERS::BROKER_B2C_EUROPE_RYP;
                break;
            case CountryList::COUNTRY_UK:
                if($weight < 3)
                    return CourierLabelNew::OPERATOR_ROYALMAIL;
                else
                    return GLOBAL_BROKERS::BROKER_DHLDE_RETOURE;
                break;
            case CountryList::COUNTRY_SE:
            case CountryList::COUNTRY_FI:
            case CountryList::COUNTRY_NO:
                return GLOBAL_BROKERS::BROKER_DIRECT_LINK;
                break;
            case CountryList::COUNTRY_IT:
                if($user_id == 2036)
                    return GLOBAL_BROKERS::BROKER_PACCO_REVERSE;
                else
                    return GLOBAL_BROKERS::BROKER_DHLDE_RETOURE;
                break;
            default:
                return false;
        }
    }

    /**
     * @param $operator_id
     * @return AddressData|bool
     */
    public static function operatorToTargetAddress($operator_id)
    {
        switch($operator_id)
        {
            case GLOBAL_BROKERS::BROKER_PACCO_REVERSE:
            case GLOBAL_BROKERS::BROKER_COLISSIMO:
            case GLOBAL_BROKERS::BROKER_DHLDE_RETOURE:
            case CourierLabelNew::OPERATOR_DHLDE:
                $model = SpPoints::model()->findByPk(self::DHL_DE_RETOURE_TARGET_ADDRESS_SP_POINT_ID);
                $model = $model->addressData;
                break;
            case CourierLabelNew::OPERATOR_ROYALMAIL:
                $model = SpPoints::model()->findByPk(self::ROYAL_MAIL_TARGET_ADDRESS_SP_POINT_ID);
                $model = $model->addressData;
                break;
            case GLOBAL_BROKERS::BROKER_DIRECT_LINK:
                $model = CourierHub::model()->findByPk(self::DIRECT_LINK_TARGET_ADDRESS_HUB_ID);
                $model = $model->address;
                break;
            case GLOBAL_BROKERS::BROKER_B2C_EUROPE_RYP:
                $model = SpPoints::model()->findByPk(self::CORREOS_TARGET_ADDRESS_SP_POINT_ID);
                $model = $model->addressData;
                break;

            default:
                return false;
        }

        return $model;
    }

    /**
     * @param $operator_id
     * @return bool|int
     */
    public static function operatorToType($operator_id)
    {
        switch($operator_id)
        {
            case GLOBAL_BROKERS::BROKER_DHLDE_RETOURE:
                return self::TYPE_DHL_DE_RETURN;
                break;
            case GLOBAL_BROKERS::BROKER_COLISSIMO:
                return self::TYPE_COLISSIMO;
                break;
            case CourierLabelNew::OPERATOR_ROYALMAIL:
                return self::TYPE_ROYAL_MAIL;
                break;
            case CourierLabelNew::OPERATOR_DHLDE:
                return self::TYPE_DHL_DE;
                break;
            case GLOBAL_BROKERS::BROKER_B2C_EUROPE_RYP:
                return self::TYPE_B2C_RYP;
                break;
            case GLOBAL_BROKERS::BROKER_PACCO_REVERSE:
                return self::TYPE_PACCOREVERSE;
                break;
            default:
                return false;
        }

    }

    /**
     * Methods where do details are requires
     * @param bool $withoutTimed // withous types where couriers are ordered, etc.
     */
//    public static function getAutoTypes($withoutTimed = false)
//    {
//        $data = [
//            self::TYPE_DHL_DE_RETURN,
////            self::TYPE_HUB,
////            self::TYPE_RUCH,
//        ];
//
////        if(!$withoutTimed)
////            $data[] = self::TYPE_PICKUP_UPS;
//
//        return $data;
//    }

//    public static function getTypesPickup()
//    {
//        return [
////            self::TYPE_PICKUP_UPS,
//        ];
//    }

//    public static function getTypesNoPickup()
//    {
//        return [
////            self::TYPE_HUB,
////            self::TYPE_DHL_DE_RETURN,
////            self::TYPE_RUCH,
////            self::TYPE_ROYAL_MAIL,
////            self::TYPE_LP_POST,
//        ];
//    }

//    protected static function typeToCourierLabelNew($type)
//    {
//        switch($type)
//        {
//            case self::TYPE_DHL_DE_RETURN:
//                return GLOBAL_BROKERS::BROKER_DHLDE_RETOURE;
//                break;
//            case self::TYPE_ROYAL_MAIL:
//                return CourierLabelNew::OPERATOR_ROYALMAIL;
//                break;
//            default:
//                throw new Exception('Cannot handla this TYPE!');
//                break;
//        }
//    }

//    public static function countryToHub($country_id)
//    {
//        $data = [
//            CountryList::COUNTRY_DE => CourierHub::HUB_NL,
//        ];
//
//        if(isset($data[$country_id]))
//            return $data[$country_id];
//        else
//            return CourierHub::HUB_PL; // default HUB
//
//    }


    public static function createPackageOnCourier(Courier $courier, $isAutomatic = false, $viaApi = false)
    {
        $cmd = Yii::app()->db->createCommand();
        $found = $cmd->select('COUNT(*)')->from('courier_type_return')->where('base_courier_id = :courier_id', [':courier_id' => $courier->id])->queryScalar();

        if($found)
            throw new Exception(Yii::t('courier','Return order already created for this package'));

        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $model = new Courier_CourierTypeReturn();

        if($courier->ref)
            $model->ref = $courier->ref.'_R';

        if($isAutomatic)
        {
            if($viaApi)
                $model->source = Courier::SOURCE_AUTOMATIC_API;
            else
                $model->source = Courier::SOURCE_AUTOMATIC;
        }
        else if($viaApi)
            $model->source = Courier::SOURCE_API;

        $model->senderAddressData = new CourierTypeReturn_AddressData();
        $model->senderAddressData->attributes = $courier->receiverAddressData->attributes;
        $model->receiverAddressData = new CourierTypeReturn_AddressData();
        $model->receiverAddressData->attributes = $courier->senderAddressData->attributes;
        $model->package_content = $courier->package_content;
        $model->package_value = $courier->package_value;
        $model->value_currency = $courier->value_currency;
        $model->packages_number = 1;
        $model->package_size_l = $courier->package_size_l;
        $model->package_size_d = $courier->package_size_d;
        $model->package_size_w = $courier->package_size_w;
        $model->user_id = $courier->user_id;

        $model->package_weight = $courier->package_weight;
        $model->package_weight_client = $courier->package_weight_client;

        if(!$model->saveWithAddressData(false))
            $errors = true;


        $operator_id = self::countryToCourierLabelNewOperator($model->senderAddressData->country_id, $model->package_weight, $model->user_id);
        $targetAddress = self::operatorToTargetAddress($operator_id);

        $courierLabelNew = new CourierLabelNew();
        $courierLabelNew->courier_id = $model->id;
        $courierLabelNew->address_data_from_id = $model->sender_address_data_id;
        $courierLabelNew->address_data_to_id = $targetAddress->id;

        $courierLabelNew->operator = $operator_id;

        if(!$courierLabelNew->save())
            $errors = true;

        $model->courierTypeReturn = new self;
        $model->courierTypeReturn->courier_label_new_id = $courierLabelNew->id;
        $model->courierTypeReturn->base_courier_id = $courier->id;
        $model->courierTypeReturn->type = self::operatorToType($operator_id);
        $model->courierTypeReturn->courier_id = $model->id;

        if(!$model->courierTypeReturn->save())
            $errors = true;



        $courierLabelNew->courier = $model;
        $courierLabelNew->courier->courierTypeReturn = $model->courierTypeReturn;
        $model->courierLabelNew = $courierLabelNew;

//        var_dump($errors);
//        var_dump($model->getErrors());
//        var_dump($courierLabelNew->getErrors());
//        var_dump($model->courierTypeReturn->getErrors());
//        var_dump($model->senderAddressData->getErrors());
//        var_dump($model->receiverAddressData->getErrors());


        if($errors)
        {
            $log = print_r($model->getErrors(),1)."\r\n";
            $log .= print_r($courierLabelNew->getErrors(),1)."\r\n";
            $log .= print_r($model->courierTypeReturn->getErrors(),1)."\r\n";
            $log .= print_r($model->senderAddressData->getErrors(),1)."\r\n";
            $log .= print_r($model->receiverAddressData->getErrors(),1);


            MyDump::dump('auto_return.txt', print_r($log,1));
            $transaction->rollBack();
            return false;
        } else {
            $transaction->commit();
            $model->changeStat(CourierStat::PACKAGE_PROCESSING);

            if(!$viaApi)
                CourierLabelNew::asyncCallForId($model->courierTypeReturn->courier_label_new_id);

            MyDump::dump('clnlog2.txt', print_r($viaApi,1));

            return $model;
        }

    }


    public function createPackageFromScratch()
    {
        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        if(!$this->courier->saveWithAddressData(false))
            $errors = true;

        $operator_id = self::countryToCourierLabelNewOperator($this->courier->senderAddressData->country_id, $this->courier->package_weight, $this->courier->user_id);
        $targetAddress = self::operatorToTargetAddress($operator_id);

        $courierLabelNew = new CourierLabelNew();
        $courierLabelNew->courier_id = $this->courier->id;
        $courierLabelNew->address_data_from_id = $this->courier->sender_address_data_id;
        $courierLabelNew->address_data_to_id = $targetAddress->id;

        $courierLabelNew->operator = $operator_id;

        if(!$courierLabelNew->save())
            $errors = true;

        $this->courier_label_new_id = $courierLabelNew->id;
        $this->base_courier_id = NULL;
        $this->type = self::operatorToType($operator_id);

        $this->courier_id = $this->courier->id;


        if(!$this->save())
            $errors = true;

//        var_dump($errors);
//        var_dump($model->getErrors());
//        var_dump($courierLabelNew->getErrors());
//        var_dump($model->courierTypeReturn->getErrors());
//        var_dump($model->senderAddressData->getErrors());
//        var_dump($model->receiverAddressData->getErrors());


        if($errors)
        {
            $log = print_r($this->getErrors(),1)."\r\n";
            $log .= print_r($courierLabelNew->getErrors(),1)."\r\n";
            $log .= print_r($this->courier->getErrors(),1)."\r\n";
            $log .= print_r($this->courier->senderAddressData->getErrors(),1)."\r\n";
            $log .= print_r($this->courier->receiverAddressData->getErrors(),1);


            MyDump::dump('return_log.txt', print_r($log,1));
            $transaction->rollBack();
            return false;
        } else {
            $transaction->commit();
            $this->courier->changeStat(CourierStat::PACKAGE_PROCESSING);

            CourierLabelNew::asyncCallForId($this->courier_label_new_id);
            return true;
        }



    }

}