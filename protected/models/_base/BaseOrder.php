<?php

/**
 * This is the model base class for the table "order".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Order".
 *
 * Columns in table "order" available as properties of the model,
 * followed by relations of table "order" available as properties of the model.
 *
 * @property integer $id
 * @property string $local_id
 * @property double $value_netto
 * @property double $value_brutto
 * @property string $currency
 * @property string $date_entered
 * @property string $date_updated
 * @property string $hash
 * @property integer $finalized
 * @property integer $paid
 * @property double $discount_value
 * @property double $payment_commision_value
 * @property string $user_id
 * @property integer $owner_data_id
 * @property string $payment_type_id
 * @property integer $order_stat_id
 * @property integer $product_type
 * @property integer $params
 * @property integer $inv_address_data_id
 * @property integer $invoiced
 *
 * @property OrderProduct[] $orderProduct
 * @property User $user
 * @property AddressData_Order $ownerData
 * @property OrderStat $orderStat
 * @property AddressData_Order $addressDataOrder
 */

abstract class BaseOrder extends GxActiveRecord {

    public $__product;
    public $__payment_type;
    public $__total_value;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'order';
    }

    public static function label($n = 1) {
        return Yii::t('model_label', 'Order|Orders', $n);
    }

    public static function representingColumn() {
        return 'id';
    }


    public function relations() {
        return array(
            'orderProduct' => array(self::HAS_MANY, 'OrderProduct', 'order_id'),
            'orderProductSTAT' => array(self::STAT, 'OrderProduct', 'order_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'ownerData' => array(self::BELONGS_TO, 'AddressData_Order', 'owner_data_id'),
            'orderStat' => array(self::BELONGS_TO, 'OrderStat', 'order_stat_id'),
            'orderStatHistories' => array(self::HAS_MANY, 'OrderStatHistory', 'order_id'),
            'paymentType' => array(self::BELONGS_TO, 'PaymentType', 'payment_type_id'),
            'addressDataOrder' => array(self::BELONGS_TO, 'AddressData_Order', 'inv_address_data_id'),
        );
    }

    public function pivotModels() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('app', 'ID'),
            'local_id' => Yii::t('app', 'Order no.'),
            'value_netto' => Yii::t('app', 'Value Netto'),
            'value_brutto' => Yii::t('app', 'Value Brutto'),
            'currency' => Yii::t('app', 'Currency'),
            'date_entered' => Yii::t('app', 'Date Entered'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'hash' => Yii::t('app', 'Hash'),
            'finalized' => Yii::t('app', 'Finalized'),
            'paid' => Yii::t('app', 'Paid'),
            'discount_value' => Yii::t('app', 'Discount Value'),
            'payment_commision_value' => Yii::t('app', 'Payment Commision Value'),
            'user_id' => null,
            'owner_data_id' => null,
            'payment_type_id' => Yii::t('app', 'Payment Type'),
            '__payment_type' => Yii::t('app', 'Payment Type'),
            'order_stat_id' => null,
            'product_type' => Yii::t('app', 'Product type'),
            'orderProduct' => null,
            'user' => null,
            'ownerData' => null,
            'orderStat' => null,
        );
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.local_id', $this->local_id, true);
        $criteria->compare('t.value_netto', $this->value_netto);
        $criteria->compare('t.value_brutto', $this->value_brutto);
        $criteria->compare('t.currency', $this->currency, true);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('t.hash', $this->hash, true);
        $criteria->compare('t.finalized', $this->finalized);
        $criteria->compare('t.paid', $this->paid);
        $criteria->compare('t.discount_value', $this->discount_value);
        $criteria->compare('t.payment_commision_value', $this->payment_commision_value);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.owner_data_id', $this->owner_data_id);
        $criteria->compare('t.payment_type_id', $this->payment_type_id, true);
        $criteria->compare('t.order_stat_id', $this->order_stat_id);
        $criteria->compare('t.product_type', $this->product_type);


        if($this->__payment_type)
        {
            $criteria->together  =  true;
            $criteria->with = array('paymentType');
            $criteria->compare('paymentType.id',$this->__payment_type);
        }

        // INSECURE - SQL INJECTION:
        /*if($this->__total_value)
        {
            $criteria->together  =  true;
            $criteria->with = array('orderValues');
            $criteria->group = 'order_id';
            $criteria->having ='SUM(value_brutto) '.$this->__total_value;

        }*/

           return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.local_id DESC';
        $sort->attributes = array(
            '__payment_type' => array
            (
                'asc' => 'paymentType.name asc',
                'desc' => 'paymentType.name desc',
            ),
            '*', // add all of the other columns as sortable
        );

        return new CActiveDataProvider($this->with('paymentType'), array(
            'criteria' => $this->searchCriteria(),
            'sort'=> $sort,
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
    }

    public function searchForUser() {


        $sort = new CSort();
        $sort->defaultOrder = 't.local_id DESC';
        $sort->attributes = array(
            '__payment_type' => array
            (
                'asc' => 'paymentType.name asc',
                'desc' => 'paymentType.name desc',
            ),
            '__total_value' => array(
                'asc' => '(SELECT SUM(value_netto) FROM order_value WHERE order_id = t.id) asc',
                'desc' => '(SELECT SUM(value_netto) FROM order_value WHERE order_id = t.id) DESC',
            ),
            '*', // add all of the other columns as sortable
        );

        $criteria = $this->searchCriteria();
        $criteria->addCondition('user_id='. Yii::app()->user->id);

        return new CActiveDataProvider($this->with('paymentType'), array(
            'criteria' => $criteria,
            'sort'=> $sort,
            'pagination'=>array(
                'pageSize'=>30,
            ),
        ));
    }
}