<div class="alert alert-danger">
    <h4><?php echo Yii::t('postal','Anuluj paczkę');?></h4>
    <?= Yii::t('postal', 'Przesyłki mogą być anulowanie w ciągu {n} dni.', ['{n}' => '<strong>'.Postal::PACKAGE_CANCEL_DAYS.'</strong>']);?>
    <br/>
    <?= Yii::t('postal', 'Pozostało: {n}', ['{n}' => '<strong>'.$model->countTimeLeftForCancel().'</strong>']);?>
    <br/>
    <br/>
    <?php echo CHtml::link(Yii::t('postal','Anuluj'), array('/postal/postal/cancelByUser', 'urlData' => $model->hash), array('class' => 'btn', 'onclick' => 'return confirm("'.Yii::t('site', "Czy jesteś pewien?").'")'));?>
</div>
