<?php

class AlertSmsLogRoute extends CLogRoute
{
    const ALERT_EVERY_N_ERRORS = 25;
    const NOTIFICATION_TEL_NO = '515540406';

    protected function processLogs($logs)
    {
        foreach($logs as $log)
        {
            $errorName = $log[1] .' : '.$log[2].'|'.trim(preg_replace('/\s+/', ' ',substr($log[0],0,250)));
            MyDump::dump('error_log.txt', $errorName, true, true, true, false);

            $no_of_lines = count(file(MyDump::getTodaysDir().'/error_log.txt'));

            if(($no_of_lines % self::ALERT_EVERY_N_ERRORS) == 0)
            {
                $currentGroupNo = floor($no_of_lines / self::ALERT_EVERY_N_ERRORS);

                $sentAfter = @file_get_contents(MyDump::getTodaysDir().'/error_log_sms_sent.txt');
                if($sentAfter < $currentGroupNo)
                {
                    file_put_contents(MyDump::getTodaysDir().'/error_log_sms_sent.txt', $currentGroupNo);
                    $smsApi = Yii::createComponent('application.components.SmsApi');
                    $smsApi->realSendSms(self::NOTIFICATION_TEL_NO, 'SP ERRORS NO: '.$no_of_lines.'. @ '.date('Y-m-d H-i-s').'. LAST: '.$errorName);
                }
            }
        }
    }

}