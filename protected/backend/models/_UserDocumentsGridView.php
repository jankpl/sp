<?php

class _UserDocumentsGridView extends UserDocuments
{

    public function rules() {

        $array = array(


        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        if(Yii::app()->user->id == Admin::MAIN_ADMIN_ID)

            $criteria->compare('admin_id', $this->admin_id);
        else {
            $this->admin_id = Yii::app()->user->id;
            $criteria->compare('admin_id', Yii::app()->user->model->salesman_id);
        }


        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('stat', $this->stat);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('type', $this->type);
        $criteria->compare('customer_visible', $this->customer_visible);

        $criteria->compare('desc', $this->desc, true);
        $criteria->compare('file_name', $this->file_name, true);

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR) {
            $criteria->together = true;
            $criteria->with = array('user');
            $criteria->compare('user.salesman_id', Yii::app()->user->model->salesman_id);

        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';

        $criteria = $this->searchCriteria();

        // if there are no conditions (except for User ID), use simple count of packages to improve performance
        $totalCount = NULL;



        return new CActiveDataProvider($this

            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize'=> Yii::app()->user->getState('pageSize', 50),
                ),
            ));

    }


}