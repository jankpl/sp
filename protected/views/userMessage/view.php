<?php
/* @var $this UserMessageController */
/* @var $model UserMessage */

?>

<h1><?php echo Yii::t('user_message', 'View message #{no}', array('{no}' => $model->no)); ?></h1>

<h5><?php echo $model->date; ?></h5>

<?php echo TbHtml::well($model->text);?>



<?php if($model->file_name !== NULL): ?>

	<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link(Yii::t('user_message','Pobierz plik'), array('/userMessage/getFile', 'hash' => $model->hash), array('class' => 'btn btn-primary')), array('closeText' => false));?>
<?php endif; ?>


<?php $this->widget('BackLink'); ?>
