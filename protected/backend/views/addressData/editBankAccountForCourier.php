<?php
/* @var $model AddressData */
?>
<div class="form">

    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'update-bank-account-id',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'bankAccount'); ?>
        <?php echo $form->textField($model, 'bankAccount'); ?>
        <?php echo $form->error($model,'bankAccount'); ?>
    </div><!-- row -->


    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->