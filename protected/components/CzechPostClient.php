<?php

class CzechPostClient extends GlobalOperatorWithPostal
{
    const URL = 'https://b2b.postaonline.cz/services';
    const CUSTOMER_ID = 'M98882';
    const CONTRACT_ID = '256397001';

    const POST_CODE_OFFICE_T1_DDU = '15006';
    const POST_CODE_SENDER_T1_DDU = '22000';
    const LOCATION_NUMBER_T1_DDU = 3;
    const CITY_T1_DDU = 'Praha 120';
    const STREET_T1_DDU = 'P.O.Box 5550';

    const POST_CODE_OFFICE_INT = '22500';
    const POST_CODE_SENDER_INT = '22537';
    const LOCATION_NUMBER_INT = 1;
    const CITY_INT = 'Praha 025';
    const STREET_INT = 'P.O.Box 37';

    const POST_CODE_OFFICE_DOM = '79007';
    const POST_CODE_SENDER_DOM = '79007';
    const LOCATION_NUMBER_DOM = 2;
    const CITY_DOM = 'Depo Jeseník 70';
    const STREET_DOM = 'P.O.Box 08';

    protected $receiverAddressData;
    protected $senderAddressData;

    protected $weight;
    protected $size;
    protected $user_id;
    protected $type_name;
    protected $local_id;

    public $content;
    public $value;
    public $value_currency;

    public $note;

    public $force_tt_no = false;

    public $rbPrefixMode = false;

    public $dduMode = false;


    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    public static function checkGroup($models)
    {
        $temp = [];
        foreach($models AS $model)
        {

            $uniq_id = $model->postalOperator->uniq_id;
            $isToCzech = $model->receiverAddressData->country_id != CountryList::COUNTRY_CZ ? 1 : 2;

            if(!isset($temp[$uniq_id.$isToCzech]))
                $temp[$uniq_id.$isToCzech] = [];

            $temp[$uniq_id.$isToCzech][] = $model->postalLabel->track_id;
        }

        return $temp;
    }

    protected static function _ownExtractNumber($text, $defaultValue = '0')
    {
        $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-]+)((\s)+|$))*/i';
        $building = '';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[0][0];

        $building = trim($building);
//
//        if(mb_strlen($building) > 8)
//        {
//            // max length is 8
//            // if it's longer, use older, more choosy rexexp
//            $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';
//            if(preg_match_all($REG_PATTERN, $text, $result2)) $building = $result2[2][0];
//
//            $building = trim($building);
//        }

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }

    public static function orderForPostal(Postal $postal, $returnErrors = false)
    {
        $model = new self;


        if($postal->postalOperator->uniq_id == 3)
            $model->rbPrefixMode = true;

        if($postal->postalOperator->uniq_id == 4) {
            $model->dduMode = true;
            $model->rbPrefixMode = true;
        }

        $model->receiverAddressData = $postal->receiverAddressData;
        $model->senderAddressData = $postal->senderAddressData;

        $weight = round($postal->weight / 1000,3);

        $model->weight = $weight < 0.001 ? 0.001 : $weight;
        $model->size = $postal->getSizeClassName();
        $model->content = $postal->content;
        $model->user_id = $postal->user_id;
        $model->type_name = $postal->getPostalTypeName();
        $model->local_id = $postal->local_id;

        $model->value = $postal->value;
        $model->value_currency = $postal->value_currency;


        $model->note = $postal->note1;

        if($postal->postalLabel->_pp_forceTTNumber)
            $model->force_tt_no = $postal->postalLabel->_pp_forceTTNumber;

        return $model->_createShipmentLocal($returnErrors);
    }

    public static function submitData(array $models, $toFile = true, $fileNo = 1, $forceDDUmode = false)
    {
        $model = new self;
        return $model->_sendDataToCP($models, $toFile, $fileNo, $forceDDUmode);

    }

    protected function _sendDataToCP(array $models, $toFile, $fileNo = 1, $forceDDUmode = false)
    {

        $groups = self::checkGroup($models);

        if(S_Useful::sizeof($groups) > 1)
        {
            throw new Exception('There are more than one group of items! Please send each group separately!<br/>'.print_r($groups,1));
        }

        if($forceDDUmode === 1 OR ($forceDDUmode !== 2 && $models[0]->postalOperator->uniq_id == 4))
        {
            $postCode = self::POST_CODE_OFFICE_T1_DDU;
            $city = self::CITY_T1_DDU;
            $street = self::STREET_T1_DDU;
            $locationNumber = self::LOCATION_NUMBER_T1_DDU;
            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_T1_DDU;
        }
        else if($models[0]->receiverAddressData->country_id != CountryList::COUNTRY_CZ)
        {
            $postCode = self::POST_CODE_OFFICE_INT;
            $city = self::CITY_INT;
            $street = self::STREET_INT;
            $locationNumber = self::LOCATION_NUMBER_INT;
            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_INT;
        }
        else
        {
            $postCode = self::POST_CODE_OFFICE_DOM;
            $city = self::CITY_DOM;
            $street = self::STREET_DOM;
            $locationNumber = self::LOCATION_NUMBER_DOM;
            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_DOM;
        }

        if($toFile) {

            $output = '';
            /* @var $model Postal */
            foreach ($models AS $model) {

                $countryOfOrgin = strtoupper($model->senderAddressData->country0->code);
                $senderName = 'KAAB Joanna Bokhorst';

                // S.F_Express
                if($model->user_id == 812)
                {
                    $countryOfOrgin = 'SG';
                }

                $trackId = $model->postalLabel->track_id;

                $serviceName = '53+9';
                if(strtoupper(mb_substr($trackId,0,2)) == 'RB')
                    $serviceName = '9';

                $weight = round($model->weight / 1000, 3);
                $weight < 0.001 ? 0.001 : $weight;

                $receiverTel = mb_substr($model->receiverAddressData->tel,0,20);

                $receiverTel = str_replace('+','', $receiverTel);

                if(strlen($receiverTel) == 12)
                    $receiverTel = '+'.$receiverTel;

                if(strlen($receiverTel) != 13)
                    $receiverTel = '';

//                $senderTel = mb_substr($model->senderAddressData->tel,0,20);
//                if(strlen($senderTel) == 12)
//                    $senderTel = '+'.$senderTel;

                $senderTel = '';

                $temp = [
                    $trackId,
                    date('Ymd'),
                    date('H:i:s'),
                    mb_substr(str_replace(';', ',', trim($model->receiverAddressData->getUsefulName(true))),0,30),
                    str_replace(';', ',', $model->receiverAddressData->zip_code),
                    str_replace(';', ',', strtoupper($model->receiverAddressData->country0->code)),
                    str_replace(';', ',', ($model->receiverAddressData->city)),
                    '',
                    mb_substr(str_replace(';', ',', trim($model->receiverAddressData->address_line_1 . ' ' . $model->receiverAddressData->address_line_2)),0,40),
                    '',
                    '',
                    $receiverTel,
                    mb_substr(str_replace(';', ',', $model->receiverAddressData->fetchEmailAddress(true)),0,50),
                    '',
                    $weight,
                    '',
                    '',
                    $serviceName,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    'P',
                    '',
                    '',
                    $senderTel,
                    'sender@kaab.com',
                    '',
                    '',
                    '',
                    '',
                    $senderName,
                    '',
                    $senderPostCode,
                    $senderCountry,
                    $city,
                    $street,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ($model->value > 0 ? $model->getValueConverted('USD') : 1),
                    'USD',
                    1,
                    '',
                    mb_substr(str_replace(';', ',', $model->content),0,30),
                    1,
                    ($weight - 0.001),
                    ($model->value > 0 ? $model->getValueConverted('USD') : 1),
                    '392690',
                    $countryOfOrgin,
                ];

                $output .= implode(';', $temp) . "\r\n";
            }
            $output = S_Useful::removeNationalCharacters($output);

            header("Content-type: text/csv; charset=OEM-582");
            header("Content-Disposition: attachment; filename=PM".str_pad($fileNo,3,'0', STR_PAD_LEFT)."882.C98");
            echo $output;

            Yii::app()->end();

        } else {

            $data = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<b2bRequest xmlns="https://b2b.postaonline.cz/schema/B2BCommon-v1" xmlns:ns2="https://b2b.postaonline.cz/schema/POLServices-v1">
<header>
	<idExtTransaction>1</idExtTransaction>
	<timeStamp>' . date('c') . '</timeStamp>
	<idContract>' . self::CONTRACT_ID . '</idContract>
</header>
<serviceData>
<ns2:sendParcels>
	<ns2:doParcelHeader>
		<ns2:transmissionDate>' . date('d.m.Y') . '</ns2:transmissionDate>
		<ns2:customerID>' . self::CUSTOMER_ID . '</ns2:customerID>
		<ns2:postCode>'.$postCode.'</ns2:postCode>
		<ns2:senderAddress>
			<ns2:companyName>KAAB Joanna Bokhorst</ns2:companyName>
			<ns2:street>' . $street . '</ns2:street>
			<ns2:city>' . $city . '</ns2:city>
			<ns2:zipCode>' . $senderPostCode . '</ns2:zipCode>
			<ns2:isoCountry>CZ</ns2:isoCountry>
		</ns2:senderAddress>';

            $data .= '<ns2:locationNumber>' . $locationNumber . '</ns2:locationNumber>
        </ns2:doParcelHeader>';

            MyDump::dump('cpost2.txt', print_r($data,1));

            $log = [];
            /* @var $model Postal */
            foreach ($models AS $model) {

                $itemData = '';

                $street = trim($model->receiverAddressData->address_line_1 . ' ' . $model->receiverAddressData->address_line_2);
//                $houseNumber =  self::_cdata(self::_ownExtractNumber($street, ''));
                $houseNumber = '';

                if(!preg_match('/\d/', $street) OR str_word_count($street) <= 1)
                    $street .= ' 0';

                $street = mb_substr($street, 0, 40);
                $street = self::_cdata($street);

                $trackId = $model->postalLabel->track_id;
                $prefix = strtoupper(mb_substr($trackId,0,2));

//                if($prefix == 'RB')
                $countryOfOrgin = strtoupper($model->senderAddressData->country0->code);

                // S.F_Express
                if($model->user_id == 812)
                {
                    $countryOfOrgin = 'SG';
                }

//                $trackId = $model->postalLabel->track_id;
//                $prefix = strtoupper(mb_substr($trackId,0,2));


                $weight = round($model->weight / 1000, 3);
                $weight < 0.001 ? 0.001 : $weight;

                $receiverTel = mb_substr($model->receiverAddressData->tel,0,20);
                $receiverTel = str_replace('+','', $receiverTel);

                if(strlen($receiverTel) == 12)
                    $receiverTel = '+'.$receiverTel;

                if(strlen($receiverTel) != 13)
                    $receiverTel = '';


                $value = $model->value > 0 ? $model->getValueConverted('USD') : 1;




                $itemData .= '<ns2:doParcelData>
		<ns2:doParcelParams>
			<ns2:recordID>' . $model->postalLabel->track_id . '</ns2:recordID>
			<ns2:parcelCode>' . $model->postalLabel->track_id . '</ns2:parcelCode>
			<ns2:prefixParcelCode>'.$prefix.'</ns2:prefixParcelCode>
			<ns2:weight>' . $weight . '</ns2:weight>	
		</ns2:doParcelParams>';

                if($prefix != 'RB')
                    $itemData .= '<ns2:doParcelServices>
			<ns2:service>53</ns2:service>
		</ns2:doParcelServices>';

                $itemData .= '<ns2:doParcelServices>
			<ns2:service>9</ns2:service>
		</ns2:doParcelServices>';

                $itemData .= '<ns2:doParcelAddress>
			<ns2:recordID>2</ns2:recordID>
			<ns2:companyName>' . self::_cdata(mb_substr(trim($model->receiverAddressData->getUsefulName(true)),0,50)) . '</ns2:companyName>
			<ns2:street>'.$street.'</ns2:street>
			<ns2:houseNumber>'.$houseNumber.'</ns2:houseNumber>
			<ns2:city>' . self::_cdata(mb_substr($model->receiverAddressData->city,0,40)) . '</ns2:city>
			<ns2:zipCode>' . self::_cdata($model->receiverAddressData->zip_code) . '</ns2:zipCode>
			<ns2:isoCountry>' . strtoupper($model->receiverAddressData->country0->code) . '</ns2:isoCountry>
			<ns2:mobileNumber>' . self::_cdata($receiverTel) . '</ns2:mobileNumber>';

                if ($model->receiverAddressData->email != '')
                    $itemData .= '<ns2:emailAddress>' . self::_cdata($model->receiverAddressData->email) . '</ns2:emailAddress>';


                $content = $model->content;

                if($content == '')
                    $content = 'gift';

                $itemData .= '</ns2:doParcelAddress>
            <ns2:doParcelCustomsDeclaration>
            	<ns2:category>11</ns2:category>
            	<ns2:note>' . self::_cdata(mb_substr($content, 0, 30)) . '</ns2:note>
            	<ns2:customValCur>USD</ns2:customValCur>
            	<ns2:doParcelCustomsGoods>
                    <ns2:sequence>1</ns2:sequence>
                    <ns2:customCont>' . self::_cdata(mb_substr($content, 0, 50)) . '</ns2:customCont>
                    <ns2:quantity>1</ns2:quantity>
                    <ns2:weight>' . ($weight - 0.001) . '</ns2:weight>
                    <ns2:customVal>' . (!$value ? 0.01 : $value) . '</ns2:customVal>
                    <ns2:hsCode>392690</ns2:hsCode>
                    <ns2:iso>' . strtoupper($countryOfOrgin) . '</ns2:iso>
            	</ns2:doParcelCustomsGoods>
            </ns2:doParcelCustomsDeclaration>
	</ns2:doParcelData>';


                MyDump::dump('cpost2.txt', print_r($itemData,1));
                $data .= $itemData;
            }


            $end = '</ns2:sendParcels>    
</serviceData>
</b2bRequest>';


            MyDump::dump('cpost2.txt', print_r($end,1));
            $data .= $end;



            $resp = $this->_call('POLService/v1/sendParcels', $data);

            if ($resp->errorDetail) {
                throw new Exception(CHtml::encode($resp->errorDetail->asXML()));
            }

            $transactionId = (string)$resp->header->idTransaction;

            $data = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<b2bRequest xmlns="https://b2b.postaonline.cz/schema/B2BCommon-v1" xmlns:ns2="https://b2b.postaonline.cz/schema/POLServices-v1">
	<header>
		<idExtTransaction>1</idExtTransaction>
	<timeStamp>' . date('c') . '</timeStamp>
	<idContract>' . self::CONTRACT_ID . '</idContract>
	</header>
	<idTransaction>' . $transactionId . '</idTransaction>
</b2bRequest>';

            $retries = 250;
            do {
                $retries--;
                sleep(5);
                $resp = $this->_call('POLService/v1/getResultParcels', $data);
                echo 'TRIES: ' . $retries;
            } while ($resp->errorDetail == 'UNFINISHED_PROCESS' && $retries);


            if ($resp->serviceData->getResultParcelsResponse->doParcelHeaderResult->doParcelStateResponse->responseText != 'OK') {
                if ($resp->serviceData->getResultParcelsResponse->doParcelHeaderResult->doParcelStateResponse->responseText == 'BATCH_INVALID') {
                    foreach ($resp->serviceData->getResultParcelsResponse->doParcelParamResult AS $item) {
                        $erroredNo = (string)$item->recordNumber;
                        if (!isset($log[$erroredNo]) OR $log[$erroredNo] == '')
                            $log[$erroredNo] = $item->doParcelStateResponse->responseText != 'OK' ? (string)$item->doParcelStateResponse->responseText : false;
                    }

                    MyDump::dump('czech_post_errors.txt', print_r($log, 1));

                    $errorText = '';
                    foreach ($log AS $key => $item) {
                        if ($item != '') {
                            $errorText .= $key . " : " . $item . "<br/>";
                        }
                    }


                    throw new Exception('ERRORED:<br/><br/>' . $errorText);

                } else
                    throw new Exception($resp->serviceData->getResultParcelsResponse ? CHtml::encode($resp->serviceData->getResultParcelsResponse->asXML()) : 'Unknown error from CPOST system');
            }


            return CHtml::encode('Looks like everything went fine... :)');
        }
    }



    protected function _call($method, $data)
    {
        $pemfile = Yii::getPathOfAlias('application.components.czechPostCert').'/cert.pem';
        $keyfile = Yii::getPathOfAlias('application.components.czechPostCert').'/key.pem';


        $url = self::URL.'/'.$method;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');
        curl_setopt($ch, CURLOPT_SSLKEY, $keyfile);
        curl_setopt($ch, CURLOPT_SSLCERT, $pemfile);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, 'Nysa2018');

        $response = curl_exec($ch);

        MyDump::dump('czech_post.txt', print_r($url.' : '.$data, 1));
        MyDump::dump('czech_post.txt', print_r($response, 1));

        $response = str_replace(['v1:', 'v1_1:'], '', $response);
        $response = simplexml_load_string($response);

        return $response;
    }




    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        return false;
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        return false;
    }

    public static function trackForPostal($ttNo)
    {

        return Client17Track::getTtGlobalBrokers($ttNo, false, false, false, 'CPOST2');


//        TrackingmoreClient::updateTtCarrier($ttNo, 'posten-norge', 'czech-post'); // to prevent "duplicated" error
//        $trackCz = TrackingmoreClient::getTtGlobalBrokers($ttNo, 'czech-post');



//        TrackingmoreClient::updateTtCarrier($ttNo, 'czech-post', 'posten-norge'); // to prevent "duplicated" error
//        $trackNo = TrackingmoreClient::getTtGlobalBrokers($ttNo, 'posten-norge');
//        return $trackNo;

        return $trackCz;

//        return GlobalOperatorTtResponseCollection::mergeResponses([$trackNo, $trackCz]);
//        return self::_track($ttNo);
    }


    protected static function _track($tt)
    {
        $model = new self;

        $dateFrom = substr(date('c', strtotime(date('c'))-(60*60*24*7)),0,-6).'.000';
        $dateTo = substr(date('c'),0,-6).'.000';



        $data = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<b2bRequest xmlns="https://b2b.postaonline.cz/schema/B2BCommon-v1" xmlns:ns2="http://www.cpost.cz/schema/b2b/ConsignmentServices-v1">
	<header>
		<idExtTransaction>1</idExtTransaction>
		<timeStamp>'.date('c').'</timeStamp>
	    <idContract>'.self::CONTRACT_ID.'</idContract>  
	</header>
	<serviceData>
		<ns2:consignmentRequest>
			<ns2:timeStampFrom>'. $dateFrom.'</ns2:timeStampFrom>
			<ns2:timeStampTo>'. $dateTo .'</ns2:timeStampTo>
			<ns2:customerID>'.self::CUSTOMER_ID.'</ns2:customerID>
		</ns2:consignmentRequest>
	</serviceData>
</b2bRequest>';



        $resp = $model->_call('ConsignmentServices/v1/ConsignmentStatuses', $data);

        $statuses = new GlobalOperatorTtResponseCollection();


        if($resp->errorCode == 8)
            return false;

        foreach ($resp->serviceData->getParcelStateResponse->parcel->states->state AS $item) {

            if($item->id == -3)
                continue;

            $statuses->addItem((string) $item->text, (string) $item->date, (string) $item->name);
        }

        return $statuses;

    }

    protected static function _track_old($tt)
    {
        $model = new self;

        $data = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<b2bRequest xmlns="https://b2b.postaonline.cz/schema/B2BCommon-v1" xmlns:ns2="https://b2b.postaonline.cz/schema/POLServices-v1">
	<header>
		<idExtTransaction>1</idExtTransaction>
		<timeStamp>'.date('c').'</timeStamp>
	    <idContract>'.self::CONTRACT_ID.'</idContract>  
	</header>
	<serviceData>
		<ns2:getParcelState>
			<ns2:idParcel>'.$tt.'</ns2:idParcel>			
			<ns2:language>en</ns2:language>
		</ns2:getParcelState>
	</serviceData>
</b2bRequest>';

        $resp = $model->_call('getParcelState', $data);
        $statuses = new GlobalOperatorTtResponseCollection();


        if($resp->errorCode == 8)
            return false;

        foreach ($resp->serviceData->getParcelStateResponse->parcel->states->state AS $item) {

            if($item->id == -3)
                continue;

            $statuses->addItem((string) $item->text, (string) $item->date, (string) $item->name);
        }

        return $statuses;

    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return Client17Track::getTtGlobalBrokers($no, false, false, false, 'CPOST');
//        return TrackingmoreClient::getTtGlobalBrokers($no, 'czech-post');
//        return self::_track($no);
    }

    protected function _createShipmentLocal($returnError = true)
    {
        $senderName = 'KAAB Joanna Bokhorst';
        $fromCountry = 'CZ';
        if($this->dduMode)
        {
            $postCode = self::POST_CODE_OFFICE_T1_DDU;
            $city = self::CITY_T1_DDU;
            $street = self::STREET_T1_DDU;
//            $locationNumber = self::LOCATION_NUMBER_T1_DDU;
//            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_T1_DDU;
        }
        else if($this->receiverAddressData->country_id != CountryList::COUNTRY_CZ)
        {
            $postCode = self::POST_CODE_OFFICE_INT;
            $city = self::CITY_INT;
            $street = self::STREET_INT;
//            $locationNumber = self::LOCATION_NUMBER_INT;
//            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_INT;
        }
        else
        {
            $postCode = self::POST_CODE_OFFICE_DOM;
            $city = self::CITY_DOM;
            $street = self::STREET_DOM;
//            $locationNumber = self::LOCATION_NUMBER_DOM;
//            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_DOM;
        }

        if(!$this->force_tt_no)
        {
            try {

                if($this->rbPrefixMode)
                {
                    $prefix = 'RB';
                    $numberStorehouseId = NumbersStorehouse::POSTAL_CZECH_POST_RB;
                } else {
                    $prefix = 'RB'; // @ 14.11.2018 we've switched from RR to RB range as well
                    $numberStorehouseId = GLOBAL_BROKERS::BROKER_CZECH_POST;
                }

                $trackId =  NumbersStorehouse::_getNumber($numberStorehouseId, true);
                $trackId .= NumbersStorehouse::calculateControlDigitS10($trackId);
                $trackId = $prefix.$trackId.'CZ';
            }
            catch (Exception $ex)
            {
                if ($returnError == true) {
                    return GlobalOperatorResponse::createErrorResponse($ex->getMessage());
                } else {
                    return false;
                }
            }

        } else
            $trackId = $this->force_tt_no;

        $return = [];

        $label = self::generateLabel($trackId, $street, $senderPostCode, $city, $postCode, NULL, false, false, $this->user_id, $this->note);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
//        $im->trimImage(0);
        $im->stripImage();
        $im->rotateImage(new ImagickPixel(), 90);


        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $trackId);

        return $return;
    }



    protected function generateLabel($parcelNo, $fromStreet, $fromPostCode, $fromCity, $officePostCode, TcPdf $ownPdf = NULL, $asImg = false, $rescaleForZebra = false, $user_id = false, $note = false, $pdfRotate = false)
    {

        if($ownPdf === NULL OR $asImg) {
            $pdf = PDFGenerator::initialize();
            $pdf->setFontSubsetting(true);
        } else {
            $pdf = $ownPdf;
        }

        $pageW = 150;
        $pageH = 100;

        $xBase = 1;
        $yBase = 1;
        $margins = 0;

        $border = 0;

        /* @var $pdf TCPDF */
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );


        $pdf->AddPage('H', array(0 => $pageW, 1 => $pageH, 'Rotate' => $pdfRotate ? 90 : 0), true);

        $pdf->Image('@' . self::_getLogo(), $xBase + $margins, 5, '', 7, '', 'N', false);


        $pdf->SetFont('freesans', 'B', 13);
        $pdf->SetLineStyle(array('width' => 0.7, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->MultiCell(70, 13, "TAXE PERCUE\r\n2013/0960", 1, 'C', false, 1, 77.5, 33,
            true,
            0,
            false,
            true,
            13,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(70, 5, "Adresát/Addressee:", 0, 'L', false, 1, 77.5, 50,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );



        $text = $this->receiverAddressData->name . "\r\n";
        $text .= $this->receiverAddressData->company ? $this->receiverAddressData->company . "\r\n" : '';


        $text .= $this->receiverAddressData->address_line_1 . "\r\n";
        $text .= $this->receiverAddressData->address_line_2 != ''  ? $this->receiverAddressData->address_line_2 . "\r\n" : '';
        $text .= $this->receiverAddressData->zip_code . " " . $this->receiverAddressData->city . "\r\n";
        $text .= $this->receiverAddressData->country0->name.' ('.$this->receiverAddressData->country0->code.')';
        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(70, 45, $text, 0, 'L', false, 1, 77.5, 55,
            true,
            0,
            false,
            true,
            45,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(85, 5, 'Odsílatel/Sender:', $border, 'L', false, 1, 1, 18,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(85, 10, 'KAAB Joanna Bokhorst', $border, 'L', false, 1, 1, 23,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );


        $text = $fromStreet . "\r\n";
        $text .= $fromPostCode.' '.$fromCity . "\r\n";
        $text .= $fromCity. "\r\n";
        $text .= 'Czech Republic (CZ)'. "\r\n";
//        $text .= '+31645330433';

        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(70, 30, $text, 0, 'L', false, 1, 1, 35,
            true,
            0,
            false,
            true,
            30,
            'T',
            true
        );



        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(30, 5, 'PSČ podací pošty:', $border, 'L', false, 1, 1, 67,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );
        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(30, 7, $officePostCode, $border, 'L', false, 1, 30, 66.5,
            true,
            0,
            false,
            true,
            7,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell(20, 5, 'Hmotnost:', $border, 'L', false, 1, 1, 73,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );
        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(30, 7, $this->weight.' kg', $border, 'L', false, 1, 20, 72.5,
            true,
            0,
            false,
            true,
            7,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 14);
        $pdf->MultiCell(30, 10, 'Prioritaire', $border, 'L', false, 1, 1, 90,
            true,
            0,
            false,
            true,
            10,
            'B',
            true
        );

        if($user_id == 2404 && $note) // special rule for SFX_DDU customer @ 19.12.2018
        {
            $pdf->SetFont('freesans', '', 10);
            $pdf->MultiCell(30, 10, $note, $border, 'L', false, 1, 80, 90,
                true,
                0,
                false,
                true,
                10,
                'B',
                true
            );
        }

        $pdf->write1DBarcode($parcelNo, 'C128', 95, 1, '', 30, 0.4, $barcodeStyle, 'N');

        $pdf->SetFont('freesans', 'B', 50);
        $pdf->MultiCell(15, 15, 'R', $border, 'L', false, 1, 73, 1,
            true,
            0,
            false,
            true,
            15,
            'T',
            true
        );
        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(35, 20, "DOPORUČENĚ\r\nRECOMMANDÉ", $border, 'C', false, 1, 62, 15,
            true,
            0,
            false,
            true,
            20,
            'T',
            true
        );

        if($asImg)
        {
            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            $im->readImageBlob($pdf->Output('CzPost.pdf', 'S'));
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
//        $im->trimImage(0);

            if($rescaleForZebra)
            {
//                $im->borderImage('white', 20, 20);
                $im->scaleImage(1155,1733, true);
            }

            $im->stripImage();
            $im->rotateImage(new ImagickPixel(), 90);
            return $im->getImageBlob();

        }
        else if($ownPdf === NULL) {
            return $pdf->Output('CzPost.pdf', 'S');
        } else {
            return $pdf;
        }
    }

    protected static function _getLogo()
    {
        $img = 'iVBORw0KGgoAAAANSUhEUgAAAYYAAAA6CAMAAACDIQ5tAAAAM1BMVEX///8AAADAwMCAgIBAQEAQEBDw8PCgoKDQ0NBgYGAgICDg4OAwMDCQkJCwsLBwcHBQUFCEre1oAAAHuElEQVR42u2c13rbMAyFEXBvvf/TtpJrgZuK23x1Wp2rRtQg8XMAIGsAcGwiBx1JiciYUqihJ8WeUjDUWS7ZLoT/W859zGQbBMqIrJwHV5MKWbGBkX49LtEe/2Dwf+vG8BZCnGLwFTNiQOIRcqmsSMBI9nHD83UR/m8tMAQgyTS6i2N+W17iYCBd8DTwn0vrKQadAbOT+1KiG3l2PY4/fFMoNKMg8rlmLi9PJ4guzuZ8dVO4MbydPibiz5uMoYsrDvHidBPC4zkPt2gq7ynBQ4kurTnoguNUEndJuAWzfs7q+eOSd3sJg06c05MJ4VVp/pAxU5wxa9QfF2PHixmAtkezbwzfFEOYYyAffy12GYOyTbxuFbwkzIKcicwXYzjEAzX7k9JLu/J26WZBMWYaW2p9BYPj/fUIfxPDBhOJdSzzunTVU1VVNX6Bi1hgUJUTq6Q8TcArPHyOgQbXV2DgVzqbUvAVwpLCjeG7YtgWGMqpp1oIoxC1KWWBYU3h6zFQTCkQflfraNTAdQyX4uOtKVaLaZED4BSDFB9/D4PQ8GUyFLG+hEFOXNB6sLDlJOMgjO+vXmeDBnAumq/HgGyXhi9UYIfkjeE7Y5gtDlISpOHbYrlDYfK/JkuZMUTSlxicUyxtjDHlmt3CyB5CN8Lg8CFHe+eMMWP293VB0CNwKLJkDH27pGl+FgVsi7Rih8LzLRrD2Z19kbRxZ32ic1cWh1hgcG6dluIFBmyRr7185du9PSoh+RBkB4MU+VDEZKswkUErdg5/yZjouyPOZHuPIiVHRVKy4iubIqtUxojGVhVStCM5kqkNtx4OBVUolUEVEirRyCh1Zikkb125FoP0VNgf6V4OMXyU4b3Vur6FJMJZZdtYEHsYhvW5MbwRBjbEIKQkG6tLEaDDOtlHUuu9Hi2G2x7EZ4bBFK3jq4MOcwtYK4Hg1jJU5WsYyovtbDp05pWisSIvLfIox45SSovMjnPdmjAy1QIDfcHK8W5KWGEgpdmujKGyFzCQKEM47A405cyDI/pYfaKAxPnHnCndIBIzppjBLPVmIQYYFI1jPTGfX2IgyVl5jNU2129hkJPYNjwf9Z/GYKFSNodOAnrqy4oq4VzZldF0MOimB/CdiDAKfypR6bgBdnckfZk3cI6K414tccIW8oyTxOGpSnV0JWzjBuoWNinEGM2N4S0xzGIH4Zx4cVIK4+MHfLHGxHJV5tm5tjzkcgUGJ5o8auI8UCOHERCrlgJT5NVSqvw7nVNiTZM0oqwxUH2UanbcrpwM4BBXGEwPgxDykxgkdbL6LEKGgSvXj8u9r/kPv484wMCacWvzP1jTYE7PGhOpxVeSGbLF4MQk3a0WOR+bY9CUrPgkBsyD/0Nnc111vNBuTNNDlQz8NoYQaMZwecas+Wi1fHOjlLuIAW4Mb4phdjRAPeKuBAO5wvmKZ+Q3wUBfvuY2YmeXkGvdx8CKka+2zT/g8usYkF6bfUF0WqJ7/UCOMcgQftWHbz1jbDMOoAQZdrY0eGDjnDjnv4HB9Pz2CkN7kJzl9K5j0LrX3XkHA/ZyQVaOMLBx3EDx+kgh7LmelNZnOxJwqkitjVDrT2PoOdVCdjHwpodcx/DUdQyxV4MBBv9xY/gWGEALMdmWlsAE7agP8UU52UpjjMB+GgNo3wsusZ9k6C54r60NYYEBYsdwzhUY5i1st/eHshGkVKvTl/aXV5VW/6nFzzB4j5Uk7FJb3eANcJYZFed9uOsVDGK1RB9ZiOCbjlBgqJMVpqhPu3MwEUdshwLnlZPij1vXrm1sNnKzpgsYSiMzBIPnGETKKlLYcrvssDbjlq8c1qcwRrbR91mLAbFyqW8Mb42BslZ9eSVntwshkfaT5j6pyHa23MGOyatnlqS0PQwIvLBYjMT85WQGmycztqqTdDF4X7HFBYb18W2flEJ3nJFIonWo/A5Djw0oMmZMPiAwkY+kytFCzh/VPkCRx9Fi4IXTxvNQOFzFsB1fkKaY+8l8QrepPdybop7tKzGckiWGOMFAG6svygME6uZXzvF7zoUokiY0weAu5s/ec1w1JiAqcnxTjqHe30MsqYY1hg+xbcyIjGa5tArlnGOCXIHd2GfyGoOlLxQYjC4yJP6B5cbw7hhASv4aBeH2rQnvYCozO7js/ThsGBTgyGJWOkd1MynZddzQCsfbMWTsVqJzHhK1zneXDNXnQpe9rgjeH5PrixwYRS5XMfD2ZIbK39fvUNcxpDSttAIYYGCdmBmHR5FgeVjoutRe1wBrpcFgQnIRrmEQwjUYctM7p38PgxlWmigAjh9UdSPwGgZSEOJzM5IC47WGK0LkzePnGVy5te8+yprrXEroYNCZsUuP2oe1p2SLvEEmtE2eHQ452w6i7sjHGoxXN4bvgQGkZNdBWKuBKbgunXxm5q2ICl2yNlvLYnxeDxk9a3B0ojvlxnZGPEOoAKDsMnyLkXHOt609hR/jJs4VHzFvTNYan5LLn/FlzOHMGdIpAPZoKUx1GUSSEl4QPuQmZW3noOtXpfX4pwhaDHO5YY017tIdK1Irs1vhM5JKrRdr4+C7a8fwxj9zdmN4G7kwIeEZ+/4QvgWGn5L7uiXqVZmz+C8g+D4YaNlUbBfiP/bDqTeGt9BfxvAD3LxcnzuCj3kAAAAASUVORK5CYII=';

        return base64_decode($img);
    }



    protected static function _bgCn22()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAAAw4AAARMCAMAAAAHqzSWAAAC91BMVEX///8iHyD///z9//8AAAC/ez7//+vxv357KSDy////8b////giH1ug2vdPn9hNHyAiTp+/8f3a/f8iKX57v+sse78sHyoiHz4sHyCgTir//djan1v92p8iHyrn/v8iH0wiHzj//ef///GPzfF7SCqw5voiKWX+5q/mr29kKSAiSHtKjsqwaDjN5/n//eFpr+GPMCD/+82wr6/N8/0iSJIiH1IiKXAsaK/958n/89oiMI7N+/9pKSDa8/2ge07/88rNjknzzZL///Pa2tqQYziwYzH//+TnzaV+v+UiY69KfryPSCoiH23NjlKyzecyjs3NonsyHyCje077zY56enrnzbYsY45kHyDnv4MiSIK/5/q/fk79581kn9Sg2fB+SCr957x7r9iyn3vz2r4sY6NOHyosHz7Y8/3Yn2jz2q9Ke6Kwe07KkmPbr36gYzhKfJzz2KO/2vFkr+P29/Zkjq9Ke6//89Sw2vGSyudkkcagv9roype/n3s/Pz/av58sY5ePr80sY5KPvNrGxsa82vHt7e3l5ORkjrpNHz3Nsp1kjqmjzed7n8B7osp7n7VqaWlNH1agss2XlpdXVlawr73z2sHZ58R5elvmza+JiYmtjmSko6Pav63Nr46PSDfOsq17n62wjmRKd3iPrbBPn9Gwssywv9hKY2AiSF+g2d+7u7tkSC0sKX5kY0mwr50iSHB7YzjQ0NC/saDM2ri/rY7bvJKyr469jGJxKVitv9qPrcGgr7mw2vfNopphY1jZ/OnXvYDn779kYDjL6OQsHzi/5M5KRzfa89m8ez5Pn9azzdJ7KTygr6+Pf0+jkbIySJNkfHEsY3ciKUR4SCotYGSSrtpKKW+/2t2dwq6AjnZ3srcsKXAsH1ubdz7a8+idnXRKRFJpKT2/8Oixr6jZ1qCgTk4se65PKX5rMHTr+9Tl2KPm/vSY0dF+SFhKSHzz8b9onpurhJdKKX7Qz+dPnrl4a6bNkYKw5uYiY63H2acyY44veYtXIyC+eNTPAADIO0lEQVR42uzTsQmAAADEQN1/aWvhKwtRcrdCyAEAAAA8cELafYeDb1LmFXb4B2UmOzQpM9mhSZnJDk3KTHZoUmayQ5Mykx2alJns0KTMZIcmZSY7NCkz2aFJmckOTcpMdmi62DOv3xuCKI7v7I2yFj/dVe69er9670TvJXrvvUYnWjwIISR4EIkgUUJCRHgkkSiJJ888CU+exB/gnK/hmDUmbEKw8324d+7uzJnZme9np1w/MlZ5HLIpPzJWeRyyKT8yVnkcsik/MlZ5HLIpPzJWeRyyKT8yVnkcsiklyaYVqnqNAKpcSTWsF7hVrWqtOsHvE1pBjfg/5HH4N2TiAPvB6h4Hj0MGZeBwvaJ+xKmwysonHgePQ/Zk4DDndd3an30457XHweOQPRk41Hq0st5np398BifGMwYr1WxbOQiHraiZw7zxqobg8PRBnm+yWuxvpQqbJuUkHMp22BNxoQbPl41QbQ4WcaPfvgpVOln+HOLcpbyaiWISQuc58Njj4PVnZeJwrmr9CJ6/ASe2WJ1XrJ49gsaNMBdUrsTzh8bh9h1FmjOXfjSfh4ylMV95iNuhbGFckXG43B5p5mHxIqWLUYjrNzndq2UixI6NnLx9x+Pg9Udl4vCQ3M6epwQ7se3ADjuDoO8ISsdN+NSJqGBeNA6K7vZbp+hK3K7B2nIQj27Vu7VEa3M4oivNuuTCKnl1oByuacUxO3cqnSgGzadU9Cn+KARVRvmpXuVx8PqjMnGoQ2sh9nzNypWAAH1grVMzR2h0jCjP0Xqyd+jVEoAQQTx3IASWVCQqSxzooIQD5eF0/UhnAV4UghlDHiMEx0QNqXAIz3Y/j5mp2bHuW4K/Qx6Hf0MJHKpVJbM2PVUDOLCaX+y+tILe3lgmERm3aptbaTBDaaVFOSE4Gt/VaxAOfBnXKK1zohhnQiQjBLhJuZXud4/WZaIOp4uBqFtXBaHa76+XiG+b4hlvRijwdabsvOlx+MeVwIHf0XGTWnXgRF66fHEoH77WI4PWjwQHpDF30F8WWkCEBKfD0oQDZ9FpWgdpFWrmNFH4+jYE8NGk/ZpabEAYA4jjue9wKHSMfhoHbPFFh444b6bEAVjdZ5BnfoWq7SylJuu2Dx+q1ITWGvi7jB8ySttFha0BNB+UFjadGRRoTZ+qZITCdsg9tvj5MS5gQm1zRQ5DpBFa0rJRI601ok6JotvWf0CQFN6JeHPK0yWqEZfh7SXtF1GHcAXoF/3qxkN0eEFrAmkoVim6OPrzF3DgNQytlXLAIRxWQY/W/epsuHPI+PpR47fwpwUHeTY3DobFDRwkRJgWB95ufK9mu4tJHPD3uwMHR0yEc91Mh0O47Gsk3lvZcUgAX1oSWXEAM+uMNomd8OgYDMGBtnVfS28fZMFBKnTh0O9B/suVtWUHDqgbJrfgwDjtypngwAZuHMw34YGy4IB6UuLAS3daK2GdQh+9WsKVAJknjWEfKLRlsYSiIlks4Rs46HTcpIC0BQcJkW6xhEMrm3CiBYl1+hR/Dodwc3tLOOfNFDhIJIlhxyGeLYOOF58FB+kJcYfYifsfnS44ILe9Z4CDVLgg+jEOOA6EcHDowKHtQL1atuOg5ECmcydZgrtxwMGkSRQaiqVAWhzIsu/W0xecCEujSWhO01Pv+VxJvIxWDxmPrTTAge+tW+mePbTL6YPTmCkMHCQEb6V1aAOHtDSgQ5I4ILIDB0dMmMJ1MwUOMIhhcxsO4mLxjQUH7jllaPkqsRMPJgZD48BjkazcjgOOxO04iHOFKjsO8IZkERws44VWCh8uHFpMVIbaLJSGonQqHKipL5+QpfXswGPbd5pSeid8TZ8ryUEr3aW2858MxGO8t/23B62lJXLQSnMwzcjUmdRr/LJqvj7PB62CgxGC0oeO/PJBq+kByyga1gGUbhxArT2c8+av4wArNTv2ib0zCdk5CAN4SFkjsu9r9oOt7CmJRE7KkoO4cLBccUa5igOlbDlwkbKWCxe5KAe5WXJQolxw8f1/Rj9vDw9jX745fN/3fvN/Z+Y/8/xmnnnmmZmdbZ3csYmM75/BAeUB8wAPlp6ehleJLrIx/soCLAsf5At5uHurSRyRHHYLHN4jxrpo9yNLnFyIw3uF6tCmXkSaGaF1nWnN5a4lFZpOHFo1hw4d3q5QYxV2lEZyEZzZd3jPUCJCwQG9nhpspkFLSvsWHBxcqnFofjalBAfyML1iLyW4DEe+DFX2LHEZ7kyv0ih0qQXZFhxakkA8KpbhlAHCpKtTmXKObRV+cbCoCQ72Tga7viyyFgeEnPVHBgpk6TM4TKAzLl8iPuBAEowIRYeBryIPsw83b4lIzjpccFDAIKNFfo1DT6NkEQe+SWMXupGZiIO+osuGKOTiUGLbviM4/Xb0go8MB3IsIwKrxxbUHupbcCj2fy1LbbJ1oUyLWybMOGls6zDi4AItLGs+66Rxsi0l/i4OGPh2wIG/TIK+r9JJA3ad8Dm7UvjFwSE0xcH+n/KOOjDWmXgWWYGD2VC39CNvpq/qlODQDMqGgAN9qq8nX8jD3E2NFCK9vfeIA5VmiDiUYlKiiANwAl351qUTp1SWwts2Tw5FyDMcSLSt5IMHUacJDnzJDk4cwYEeqhqHPGjoqQxOKX5eUHW1j7BD0wAsDiqoCQ4IjZ4jjGxqy0lkFQ5kH/L+NA60u6bMgEOky48Fh9e92uoCQes3ARzog9E0Tg1PceBLfIg4UMAobBEH6q0pzrQpCrk4oGmRieC06RkgnOAgjB+zSUFRthDcH4SDxo+ZM/5YHMrgoHCqstlli4NDaIIDwic55oJ0ZZE1ONDCNmaGgyqhCwoBB94GAVA46A6Qhza3tLbn2tJro7bgYME12Qccygeku2Uq7YBBnjkOdvn8oqbiVJqEBIdfVk/EgdfhSYmjxSnV3AvzUZarcciHBkajPxYHukBlMcyv6V7EwSE0w6HjBFlSmS0GmSyyEgf1gxwHzZmEkfsEQhwcbFx+I3l+L3/WuQNGw7Zyl85eV01XEj+NQ5HLiAPvTp4JDg7jwKmQ83bRWE1f9n4iWwhJcVDUrQFwmDdhIKL7Q3CQ6Ua9/GNxQHNBOAPG9Hqnuyr2dkIpDjaTXROCgMQnkXU4IK9fj0Pj/dhi3a/FYch7N5i2cjefKavuBw6vifBFHMwzwUH26ZuKkEcc2lw/7V4acBxIvg2HwYOaVio4/BdHB1BjwaxD7T8HhWBo1d4UcdAaMH369EdLaBPFskh8EvmTcNAZYaEDXC0Ow9t+9u4zoZEvcWhedVMvbQzJ6PDtONg/0Wcp5OKgCis4jkrfikObMtiotP8RDt2HqBMlQRx8OsHBYCesSpZF1s8dnJpGSRQHvXbPj3UwzOYOqHz0sgWHNtnqcbZz8+7i8MEZaqG1ks8dzMz8sSHkONjlI+Sg59uNvjjREU9wGEjU1NO5gwV17jCPCdfsceJQHShaj578PNu5VQNhQTkqQ5hLf09QEi1ZjoMdYQUOPChHSWQlDsqv45ntrrRHLyck0Af8O1qWPuDQJo/Dtndp3rUVB92O+F/EgQQQ5VgaBZIw+MlxJvriEIZxOyRhxyYMDwUXAxl/DgeI/7RlifxJt9/igEONoaZJkJ8P/wIcgud2Ls0vVjjxrsGBaoG6isiadQc+ICVNoZSBov4y6S4PuiSXrjvwHZ5Dmvt1bWRvbzN1KDiQdlRF4mc62S+vO2jLEodo8lDIxYHJMxpgAMdqzdcdrC2eKTiQ7qz734SDLkflp8Hwp+FQXGus+xyHXVQ1mmoVDjQmjVgRWbcqzVJJ2XlIExfSeDd6TMqMdH4SBztZHasQ1DKXJDNKWQQL/iZ3FemIg8l+eVWaLCE64sBjQcjFAbMTLx3AIcF8VZoaLC4SFpT8ASSsjFQuufHzr8BhaK8wi01xoPqciwYcssGBlkkja3GgtfC4YcmeUiEPrHizRs/wwf/wL2p0pYVIU43PEjhQV5SyCBZ8cGYDuRMVcCClzGeJIQmfJbwLGJ3EQVFxSc1xUBxADhMH4JCbw02dz5I4EF+Ng24V1NiA+72g7L1yxFvSOHz2lI2PcRi98sPc51eiQUvX4VD4oSf6KhxsK/raPLIWB/27HbNsQs3Ccp9YlqI3I7IhDrDHVngECxjDhKrCo5VEWv27AdB61fNVhwsVrBYcUJfwCtWpCTYocsAherRaUHEgvhYHne4iDmUEYgjm88enbIgDO53dGvTZ8PtxaKpHSclxUMhEqCIywcG63w0PJmETKvk2UpBccYiu5zSaONAp82wRLEk296r9DiTiVihLG3HQU0khF4eiLvFegtMyu4o4xP0O6k3iwDtW4NBy9kWLsoT4t5HctstyzrY2cPn88Skb4lAcADlhIA2/HQeqp3RjX4MD3Z8EVUTW7oZzM1jjBh+2FWGNJBRfvoiDO9NcvBYHDa9FsMxcB8ia3XAkYp5uwIs4tHX5mtAUcnFAbqFJo8FHA0mCgxXj0ro40I9U4NC6YSfiMHhQ8yd7Pvn88Skb4tCW6awxzfe+YOH5A3CgekoPKw4pDfoI5pH1OLgQNmx7iyPdhvNjSfXEztblAf6HOTbi4L7lsvXYSuLNi3mKztm90m2Zc/zBgmSvNHGfw8Hi4l//SRwWrVCZVMjFochtMwytFBwHkgQH90rr3ygOzM8qcGg9+yLgAAethlZP2RCHoiZxwkAafj8O6tf9uopDRoPKRBrZfpLGPxHc7P8pHIb2asVBN5dWHDh/ptvtX6sriYN6aY6DM1VmXeKQ0KD6nEW2Hyzzb4QqHFpO2RCHRsjapg1Dn1boSr9l3cEdEjAkDt9LQ/s5S/9I+Pjsiy8rS56y0YIDR/iNXvmLdSVXd1SfcxyctKHhiMN30tB+7Ni/Er5mKs3Eonz2lA1xIJnej3+1XUnTeRTopS+vLog4tBoy+94Uh++iof0Uvn8ncPZFbmhts/z17sNnT9lowQEj67lfbVdyRd+FG8cNVlwjDs6mcWlLcBi1Xmt2Gtl+Rus/FD5ahos40OwYEp07xKl0kcxfrivp7+Vc2giMb1MjDryxARwSGlgQyCLbjyz+p4JOGhGHchvJ1p3l88enbIiD7k6/OGBLj+6kzpdBVBzUsXIc3DlJT5BFtp/g3R6SKfmvC1Hs44kCICoORuc44GGqwCeR7Qfat4dPdqb9J/9SXUmlSBU+UgKi4uCcI8NB8xMCn0X+Wfc7NFogSu9vCeauP9fMGaNXskhZ3cvRon9pGDwIr4dfGaxyRVPNXgfLiIMCLQ4xWT0kk8g/7LqTRSt+Cw0xdwfhzTd60Qi1u9D+Zhqa3ha/sl8cHB50NPNUd6cU4qB5OcEBW6w4JZF/Fg5t3fNvoSHmbk3h3VivAv/VNPyuoF7kGa0dD3F9iAIbcLCHDzjEwSM4rWWRvwkH3+k30WDuYXGGMbR+cGinIQ/159nbL4lDHFXyM1qjxBP5h+LQHtoDW8ry8+zFIYwq2RmtMZRz4NtxaA9/bgg8uF6c4KCxVRzCxCJKfBr5o+6GM+COxcaAUsjs9jMfi/sORhxf1Sm9j42C8CD7KdwpYVrNG5qboWW3s8Hb7BZ89m453rYYYUr53WvdHn6gvuR6sTiEeV7EATtZIvFZ5PfeDVeHg7ef5Ti4VS25j4174vxnFQ52Ia7EhtvsPosDi0btOPyw4HbKcDFaggP9mTjkupISn0Z+991wNThgKajAgdHkk/exubAoOZU40Eso2KZo1gkOs8a04/BjwwZ2OZbgDSwJDiwbRRxo8ETis8jvvxuuAgdkuQIHT68wFKTUHPXSrcMBbxmy0NAUqPsMDuyRbsfhB4fRR5tdwvjtoazmONhiipCHm35e4tPIH3Q3XEgq4uDtZwkOijA+yJ5eEe5joyY4LanjITYe01+nOERb3Ow7zmc+eZtd3NjN21Kydhz+g1B9N1wVDkptjkMhj4c+eQFV2bbfVQUO0FIcoj/9ZG9z+8RtdgkOzMH/FBzY91Px9Np11O6sMYtWaEgwuIHO0Dw5dH+p7qj4X7/2mczMp6nT8Rvb/Gnv/VXLM9V3w9Xh4O1nOQ6IZ4LD4EEkpRLJQJXiEMFuLurXtTjeZpfgwKjyN+IweuX4jUXCN69+4KslOKCabnk1heo2hKOQYzCfpsXGb+Q8m38Ih3fsnT2vDVEUholQiIJEkIjvkCCi85EoFSLRKJFQCImIRkh0VFqFaIVOIZQ68QMkSg21xA9QaKx57LmvbY3F2ULmzqy3cK5z1pmZM7OfmbX3nlnvgDnCAjh497P46kDGMuTHpvItSP+NcfDWPqqg59zsQhy+UndmOeKw9hXXdjpfEB3gUPda6UwthoPWYy8PTq2aGg6/9oaTaB4eB+d+FuJAZVf1HSQ1e22IiqjyvuRwcNY+9sIGVePYKiHl7VKKuYtdjqxLvwxxCBThgAIcRmSv9Tdq8oZrxMG7nykscD3yfmzyCXJ2oCEO3tqHiwR8eTe7EAf7no09jQ6HfsYV1KnrTNVo1JtO9yUF1j+6SLqpKM5BO++8MRy6oGc2orDvrkXoiTqkgVHMqRnHt28QxOPNGFnT+K9s7NdDhRtybduG5aMFzLBacJD7WYyDZumH/djacQjc3HCzE4kRDqu+FwgcLQ7W5pjqVd1bh8OLl/xyRZUhiW1P94PDjuf9sDaPUmqyRQPufK3HgWcUOGblwTpKmfXrSRwcDt79LMaBqWHvx/bXOECBc3PzbnYRDpZZ2daMDYc+0d90ertd+OyugY5wusAeB6Z9f4yy92w2s6tJDQ7d7rd5evvbOtLXDnUPWWsgykLt+nnsrO0llsdKvwcd2ctj16UeeL8ecJhasiRvuJautNzPAhzc7Unej019By2c/CDsSrtJh73Ozc252Q12pcGBiY+jn8eIg51yLl9dQY16Lhhk9h6H7u8qihyHUwU4dPvTwihG03FA7qN1AhkfszxqgC8tkCURz3qmikPkDRfj4N3P4q605P3YWGXjyFLg5oZ0O5QtJ8CBwd3Np0aIg6U8tpO4ACpxcjjQkOsozul9V7qO/dmxk1Be7S1itEstMyJbIldiPZPFYcgbbgEc5H7WgoP82DTvoJSZmD/BgXhvdOXd7GIc6IUfeDs+HC5dZGaB36hG7HAgva+iysd8RFCJrVp6tSSad8FB9jYUA+RrBYzJ4jDkDbcIDnI/a8BBfmygISsJtmSRWWk2AanBeDe7GIfSAEaHw+0bHJ+6bFKAg6ICHLSCAIeelT5l6nKlaeMw4A23IA6ktk041E8otN+zVF8IIJxleje7CAeYGSEOu85z60rJY3wjLgm/cFCUkiWHA7GoCuWVmHor6Em8tlxp6jg4bzg/skR7rEZHaZWV+1kTDvJjK1BJvBvhINmyduNQ4vodzs1u2C6l4ACSo8NB1ZLsbU4cmiTTgE+PQx1F/5g9W+Ng/9gludv7cVeaxkCDp47yO7tcTB0H5w3XgAMn+QYc5Mfmnk4gb/pjHDS4WvWHnJtdjANfGBkOtkU007KvbIhz13V7R6mudbLNGa/HoY6yv230lIHWGgcDxKZ7bGjvx4HWbbfKQCuUsWp7CMu+bTuOjeIcU+NQUqplpAZvuAVxYM814SA/Nm2IJuf+HAem3pybmzdti3GgLY0Dh6XBIQMdaYINruvnA+59+AEHRbF3NQ0nHLie/3Iajh2oaThOkWBg7zkc7Mo0pWk45w3XgAP7qBUHSGKPl2el6cqwJX+Mw5fVzs2NRXo3uxgHjvGIcei6dxhISxeebNl5/6O60p0Uxee6SUM4hDdpUCXMorogPcWGn4rHweb1ti2ndCkraaRSiUMqlTikUolDKpU4pFKJQyqVOKRSiUMqlTikUolDKrWkxCGVWlLikEotKXFIpaT/isPKVGraShxSqSVlsrQslUdmWNl3mKXyyAwqcZin8sgMKnGYp/LIDCpxmKeme2QuXaRUtVQXYz5480z4tHXiMEdN9sisOX947acNv7bWWvf+hHdjKeW7qD+YOMxQK38ob0ihkVYdf7j9twFUI3EGSq3ylreUU6xx4IepOIkuDzgRUL6l1EJ8fK4vdQMNlF7ZsSdxmJVacfBV5Ki9Ewb8YxyQVTFTsmTNWjg4K2m7OAiHYyf7Co1bu68lDvNUKw7eoSDCgYD/ggNVRSWHgxM4WNOvfnziMFNNBgdruZQ+o3JZjINX4vCNvTN7dZ+I4rjVBxX3XXBfEH8XFRkQUdCXCPooGMUHEdx3UPLgAhUlpiIuxdSFWkVBRa0oVPRF3FAREX0RfVQE/SN89Juvk3w7dzLTe00tLjkPt20ymZn0nk/nnDOTM7204gAduPALKNXW49htmOnxqBb3fbCnydh5wft77FaGlZL//tsebjsDgb7jotqCpzbhI3eLvMIWwBtmN/x7cKBPAJcA/SIBfGlwOOWx05k0XkkRq+McHbhZFIE45aU9vOsakR6H/5W04HDg5wPISc8fa00PHDr5E/tByViR75769ubb8Elrba+zNSM3py5DLlzhcPwhfyMOzAyKVlpxYO5XJoiN4MBsuVXKWHoRPQ7/N2nBAcqALT6rZObQfHsI7iU+QJWqvLiI519wB1UeaoRiF9S2EJQRH5GoGzv34DImo757D80XFqCpLgNk/Tiwe9DxFhxAKgjGTdCDZgSJfDu+AwjAzaHL3NzuTFzX4/C/Eh8HZCxm+mKoDe0d/GVMEgm2oSX4CyWqFB8qD32Drnu+A7Wwvox1qAA3hZCsFwfeRQAHcIIOo/u4vTprOhLruziwEKDgXaHsoYf3OPyvxMcBqkAlgfZaHPjXEsDzTMmNV6tvwoHp0H+F+yCwXBzwKgNkkzgAcMDMG8MLnQwOdg4OKGS3Ubb7ZaLLm8BhlAZkmnk1JXMzxRljEv/czF5W6pR/foE3JoWM9uplNQ74244DlYulaFjjoI8DMtnXRjkvo6YKByWmh2zUWNJuBOiHTCUXBxWig8M9VTaAw2g4CMnYKZjlU+fkvHBOm/bLJClPTkdjviZ79bJ2HFBMOGByC/t4P3duAAe2Int8Q660j4NMpRAOOFadwt8N4GAGQZkuw2B8bMplIOY8FFH12fKlw2KvXv4CDrR6AsaSiwOsbYY5QzjwOtnjmwy0WjuIIlOp3VjS/jUHbwSHeRiHXKUIgy9LhlGho6a9G+USDb2t9JdxgL3d7koLB5XHuTAOkvXjwFkDu1Or1e4GB3QAHbbAylRycbB3ZZGnbAKHJIxD1qj6OFRkqJFAZaaBfqhET0MHHBB+ZKCV9pACrcKBygatxCQDziksJRxQYEOLNDh2IaqKzRyFA6y7k+8/kUuVaNPdeMu2RRp28zpcd/ce3MUGZ6WT1a5DMhyEZS5jSBe2SzZuhp1e/hIOmk/jzNvet52uaTjiUG+bCn2HnkFO+Aq65eDQFPjbcYCaa3vkH/YIB+wbjyOcQXE8ZuLAoQ+dtoW44ePmcMgGISltiZyfdlMs1JEipfQ0dHOlLz19sPXA9kUawgF74XL2GS/Y5ZAWiYuDLfD34oBu2b0pD0B/T3riOW+RBrdg9HHAycsPAQS8OW74uBEc4pll5AIsBitk5g0zkb4USZL0ptJfevxHatHL34dDGvSkvUBsvKAOFKrdjdROapcjnatMXPwZkmkEp7FQXpMYGzLIypZ6exz+hRLHYTYISEIdngziojhRPNJalANX0l1PPSRh30ThrfXjwCbzser926THISAbxCGP4ODDMi6NmaXbD4ZwiE9v7BqHlRfm6w/kjhzwM/dmVl/e4/CPkzgORQyHkTsMmKy2e9xBIw/hoJjSOnHIQiWm66ZBiKle4dAv//o3ShgHyiQSQk0dn3lZDefDZU5cVzqL0tAdh6CZtX4aqPyaLulx+PdLEIe482C2jRx5ZLFTHo4sEarN4JCIhnVK2dxkj8N/QFbgMAovWZp5NAR4GDveQdhvmMzyJDHT9eOg0NP6F8tmUztd0uPwX5AgDnFrabLXXkN9MhH1ZGy1VMzILVaoFuOEmZzpCEjR0gRktBMcVIvWnoelCBRjL0IVZ6twyJKkn1X550sYh7i1VGQOG9FFefMli6gMVZ/r4MKYBrCktMAMpwt81OFJ3fg0z3wcsuESY0U+FdUsvk1sTTmKziZ/vp+7QwsPsxe5g8I8bTg3o6YmF/FsMdP9j2f9et1/sKzCoQj60mNHk2MXTqH1us6RoTBpk8Txs8ejxvDx5jaEg+OfG5yxhVXcciVpnH7jNwYpVAOpCUWIyzYcitKfqe8oB1x58cAuvwiJ1j6H1mlzhcSPqiJWCEuFjl7XGuprrscypa2n17Rq9pSPP9oYDopP+jLW6u9hKLCp0SMPrFddxCer8/Y5PTkmQsDFodQnR2cD/DYd9RtTLyTTrD0KkPo4qGMONp3EppqokgF0wWGljquQinYm+WrAYHNgdBWuSDzwoM3ikAStpaH+uyt0WZUMA7bSdCUNejKIDUdxMNJ7B4cQfNFVu3nLmUyd3w0OGiI7CBZ1bj2+T7X4rVq6vRPpjkNnEZ4nPXki1wmef+K/EodAJBQQmNg/151rKHRVe90mWoEklX6HcchVawCHMoKDJA+uUpzSGvzLOEy6qwAfIz708H8ZDk1t55x14EH/ThxWDg9JqGqpa/N+4bkO4ToaZxkX5XXJkY5PjTHToY/DyB22TL0yMA3qY+ShDg1Fw+lMngy6NG/iw8ZUZ4hDmjYGl42/Js1H1tT5ESf7UL/enPrg6Y0jsfcNF9Mwd40lKM1bLyE730MnVsVfvIRF+GQDnibde9+bL7v12PNOs5rFF1bK9dN1IaoxDX/mvmPFzyH9ANdhq86o8MntY47UG/WXqQQoeJgBjb2GrIBoXs4G2mGbn15+CLIFVsvCMULiwKB6gknfgbh79oVjByc9hRrUSFcc4t5Dutdi1zgMvSJhHHLHspl77qqxxSbbcGgUOJW/O11kLNtoZMB3MElWVBWqZSOHgb0QTzP7LmvCw2lboDVhnKpQYKDTM07UJDzR43kSTGdNy1xOhXA46Z36ATTm3+PbRtO//ORYKOEyDqyUBr6DA8Yje7iqGGn9bLI81RnvOZNe8LEfK+qvxYHajcbeqI4zZ8DeeOTNtoM2t75jO58dwiM1Ds53QGFPbQ1qpDsOmhuQ6BeytK+tkrXgMAvhEFmPXTpFJ8KhfQxbjGXgUxJjGthm7fi5Jht4ktbyvbR+pptPdULi41AYk3udNN22yxmc9MozFzX749x+duVI4JkYKAF+M2GYW6dCOFTPiZ561yFHHAa1O+Pa6kk5vK295GNxEhwIB+ueXHAHFHzJlca1+GW2dj80cevevfB8KZ7OVp07CAPgV9zcw/d+f2FDQaXROtLdoz4MIDhS9e6UO49FO2jz4Udw+9//ct3+PGI77XwH+slA71CD00h3HHzrW35tRtUzq32H+n0RwiFC08J1M3TR2CwSv7Wx9DfYpzgOGgOMLphv61XZdGhYzpMijEOgoS4ChYSc8erZtJig75Djbzp4nwOOo0bzmIsDHnqrDuP08YfU54QDjjg4oKRNyYGfauGAa8GBrcdm6WN1qnOl8Gm2ATISV4aN219u6AMttj3CWfSaOZTq+2CbOA5KbcvsrfMd4KXuIsVrpCsO+qH2AyzkIQ2kEvMjS2XQnirCmlsaysTzQyiTMm/3cEb+3HGxQxwKecxGHXSpHPOUJJ0XMRwyTkl3xEFywXuVQX3GtcwpYeUomj+1xeHiQO22mj7Y+pZGtnCAwjk4kAM/0Fob/tRTFrH1q86VQkfj3dP/fITb7S/O3Ho0LBrbI9syp1kMrkARtonmLasWB/c7qBnAAPp6xa7XSGcc/EC/1AU8UFXCnrBURwXjvkM8qU3ihbomizYc8iUUTDNk7AQHfU41geFB7oecZlk7DsrJNlRDa5nPwj8YOVmsHHo41LLxRx0cpOnWFoeT6Wi6iwMqbcOBVNWHWLGtX3XuTPg4NOpy+ksjn44F629eOBRSmdlYCw7Od8Dqbf4BOtdOI51xiD8GVGZZGvjnJsthzVKegxdZCvgfJoiD35lFCw7DLFZTHIe0xkGDgVcZO+9K2orDYuI3b7rHKq1eKCGjm0gygANHltMHHFnWhoPq3PGsIPvt9hdQcW7OwQG5ZWBZmWee2y+MA+vyx6CLmUbEaWRdOECfQ/ZS9XBDERwcKDk/yJqPzjusxkGhLqm+hwPgU9e745D6OLTMSOQ+DoK3Ow5SJGmq7HZZzDEcKKe8fAnsijAOcWOJrw4OqnOnQWJe6PQXLjJxcnHAH8arAGgMB7xtW8vywrFHHKZGuuMgScO5xYrpOM5PNpIKBvyL8c5x0GWSXDhIRlLH3eGgmY3Y6AAehv5EhQrEwnKm07xDFXRkGAZ6wU9Wd+hkSkeDOFgXeDsOVssaV5p4BVxpHwdpZUTqHiIYBP9gub8wb3gjwkEIsngYB+c7UFu2p04j3XCIP7Qmk3k0illXJdVXplJ0zZLvO+SJIxhitPBUTbTgkLqTeXlRJPPd+Q5G4HmY80vJiYvEwcEBfmgSuNPdcWCqLpjpnFxCWB2Kgr1KsN/nn4FWJLDD5ppeoHXZDkKsEiHIOmpjlYgOLGwVJMRrAq2IZeJiFYJ2IQBaB1qFg1NnXGD70KDH3nDQ4OX+nnMW/vg4oCc33sJwVCsOfO9+B5aDakcwhJZRZqmRNeEQ52FosnBUiao3DKa4GLanQDaGqhOd1khyM5bmJ0Jjsmy7FOJtx4HWkSrItweqxmLNFk4wNa5SwsEtn68v0Fpt+UPZulfTcPhna4Ec1DWEA9I5UkASTmqGjVksIQ//pGk4G7pqn4YTDk6dcaHXTeFch/rL7HoUNub4DhFXGpdVJ/Qd6CejmbxTI52NpTgPAmI2Ci/LTqlTk2yFT5IrEDSmwuxk/efCx2Gqt8NMAKS7mHcohVDheiL6PHe+miYK7OGgT91x0AoEhlk/5EwcFygMrtIiDS7GCOHAoE59LX498YNucWAM5+RHf9YijRNuv0eF/EUawkF1rhZ63dwJ0elvCAf2CkHTb74GAz4OGJIYMNJ3oDXwbMVppDsOq3lQ/D9hqdFcxbRqgqul4zPes8JGRXnIyGHOm8LTNB1xb5SZhSsTDm2u9kwAjHeCQ+kaehP5TPUNpCIlT9M0EdXeqg43wVRWf/pHpGTq5Q/2zuX1piiK4wh5lLzzFlHkMTgDU4kiUgZnRkqIIgY3JpLBzcmMGEmUR3kWRgyYiCRTI5liYuQv8N1fy2/tbXmc43bZ51jfxL3nnHvvOe763LPX3mt/9+A40GyumRiShdJgVcSZ6MWi1Hk7fW3+0Mamf1HiuORQMEakq4d6tAZ6zFil2ysNW4ODQo1mWpnUYT/USUOBxwicghxiRLq3Mb4j3NEiJwPAQ8chczXAgcHbVGd7FaJMaGjQZVX8ZA9xsFvtfAe5a/S0Qacj2xYHK7bv9PzsvIvix3XfG6Ojyr4O6J8PoDkOmasBDta/vo4nJdrVZa95E6zgWHhNHM5+3wzSbNocXR+HjT8/v4fCnH2F8Ccq0sF1xyF3NcKBRirNaEBDvvjNO9qAZRyThzo42LnScTa98U9x6Ce82l0Wh0tmxKVAeazj0CI1w4GhdqEJDecvSOQZWdM+lbyoMpFYnraD0hcqmyTzGFvTcbb8JQ4XzBlIymQ/T5IES4MCYO9xJy86DnmrOQ5cDK6WzoKGolfHuyvtjYosZPrxjpPIqbkxBuICPsHicFqfRkVDl3rVyV92tFbnLxj7GNrYRBc1sqsSzxvN983trkhsP9AtVjoOWasxDnap0F8MV/dqmwqd7hf0s7vEuTp2x/miX8VjcEVxHgdzo2wRCX5V/HQjDsYb8LFsbjgNofcwfF5Z9NMLqvoFun0vci3txH4JB+vR1Z1LOObOaT0v91rKVA1x0N/z3zWZyhatDu1NetcAOJi2gtHFFsHgOLgGxYGq0JVuRZvIVqlVOESlmdZkpTNijflgb4DJDjSwmTRh66Ih46DaeKcYsWxh079lKDgOWYo4DOo3wlkUHx5JaWFzHP5TF3bHITsNisP6K1sXSW0rylodB8eh1WqAQza5Q1dUli1a4x04vHw0RzzqwkQaTtoXHKS0GSut4zkDaswJQrLgzMxRGSj162MV96u9m6T8Ws9dcMBsh8VbNuyX6UnvYn8+7MKvAg/Ebio2G+SEvtjLTM34HIdOadKE+w9kEg4tyOQxcVBHvK2L4DY5k0EyhZZGiIh/L/XrUxyurRSPvOjcBQe6kIlBEuhO/PnEP4lXScVmgykOdNhQMz7HoUuK/e8QLzDEXrIDk5hHptFjWuRXd+/123AE21YhOvD438v49YVpP6fW0HEvOXfiMHkFaJC45xUk/nwL9qecJ2aDxGHEZAYfGpvxOQ4d0qQJITekKYWEBCOGOOARv25O4w+Rgnh78zhQci6LvELdBRSHELyckR2fO3HYcA40hEBeNZGtvdSfb+1q3CMizmOzQcFBfDABVWLG5zh0SJJKhzmS8ZRK4kBIvnlIkIHxO9/ODYYUeE0GMn59dNwTuJNzBw4vwt2NYU6s701L/PmkmTTpNf6i1GxQceDM8ufocU3M+ByHDmnShBASjAl4L4rwu5g4JAkbsAd7vBsohEejMpDx64tdnqJzpwfss9F0xmDcY6O6fsg/4T+ClERdawYHpAzihqNmfI5Dh5TgEL5zkcEBh0ydjj+4SSz4mEMiDalf329xWHjssLhfhCRo3ntcaYrD0u1IL+ZrTmRxIH9oUsntxztaO6iosfS1ES1KG0v8d9Lj2+OmoGvmxvwsEmmR+PUpDqaxJPbE67chS4JCU2/DZ+xLcGBCtP6K/h4YHNjzFu4w0gJzHDqoSRP4Da/fRsNi/vYxAkwqHbYffRx23LqbxaCDyaiZO+AKfpJKY0QZOxn3n5AxGxwC5yTopzgQKErN+DyV7pTY0YoeQ3y96F1BN2VYDCTqaF28RzorWaEwgwloHon0KOvXN9J1irBNzl3GHfCQcX8T/UopDgzxW3dByc9xUGs/dfzzjtZuicNw4nknw3DIOe0w3CjprA+vyCORHmX9+jiwJkNndhgOMYwrk+5YiwMO0tzJ4qAdb0if1YzPh+E6pVCkcYheeFKkwaIFW6QRNPkgux4fZTHoABm/vlB2sXkZz9cWaUgTR9pSFgdmGTVxGDHj8yINV7ZiZNdacPSX3v5ewufqgurgIL2lRuKB7zi4uqIaOISxRpv/cgksJgKOg6srqoGDrPRrFAbqQg7iOLhcX+U4uFyqv4nDaJer23IcXK5mONinVu1pZXXkMlxDkOPQ1stwWf1lHMbWk77Qruh3HRUEuuT9ozmYx73k6ZNRQ9bgl6HmD7rqHjTMXkbWWw74jrmUJ7VTQ8OBq5tSWPP3Gw4s4ZrFx79URjhgpWWKfd+OQ8c1JBxIA01tdl2ds265zpUNj9uEw/EjXBd8yeWVKAgYhhyHnDQcHFhqv++AeOvMZlXitxLEVuGAksoRc8MfRqrj0Cl9Ye/cXm8Koji+EZEHhfKjXBJCLvVLLg9IeRB5FZFLKXKXRIlS5B8Q8iAeSCkPbpGihORBXv0P8jf4rq/lrFmGY2OrmdNaD7/fPnPOnjmzz3z2nlm3aYFD01/8ODJvWzwHjAEi8C3SfdLDsfiLov8o3XSD45POY+wQjuihzP3HCfVO+CafvyA7gSPdUV7yY5a8SW+OwNOfnsbcE3ykbV7OAocDll7Xh3iqej7Tj5vnoHYkJEJ2pekHFrtEdlqjtRpSCA4a4M7f8uKoplYcfnQhRviZCDL3SNK3TxJRsuzhrhGMX8lLfsyS90rywTEOhVVw8DK7hAgmY4YDw7dYLBDO3j+kx0xbN0c+/Xm7lqSJ7BQHazWkCByyp7bg0H6yVAoOdLR3r+Xej/vy8rnoBe/R26e/Pbl6/JkF6Fhe4rPkIaiTNwqUzT6MMGBeJHwGq3QJ8AR4ioNlZF91EF8AZOBMLOqnMPPWrEurgdTZL6dY4hLZsUbXakgJOHAcDR4OVAVoGjhN+ibR+5qoIS+xLHn6LonCuGeOLA5efQCRBMNBz3ffgh9n4LO8Kw8Tfo80kR0/Yq0OUkrvPlIPDohzh0yZPCA4cOhqsgcZ0FqkXcpKfJY8vst/7hHKpcHw/TmYPxoOxGbmzdszLBvLMFYYTFOK2vVCKiCWyI4FSatlJdCoQP7nZKl+HJgALk+hyGHLXjgcspI8Sx6DeD0Osu6mKA6m4kWZaHmBBhboFL3/OxzSzF08TloNNVMZOOiY0PtctTi4pfTIE6P+EIc8S16OA3IBweY9fPnKWMXBm8NHYAUBJS+M+8N3tg61xCGeCqXhsGTR8rkDgAO/sBpSkOpH6eD/FjjkWfLyyRL+aLKIH3HgLOo6FgZjRmOhwOlTHxxYpDjEU6E0HMwMB8+MenEwM9wxaIrcUroFDnmWPB3UWoXdyyXHUI6DrqiVySWLfoGDJbJjAVsduN2xfi014AAImE8HOWrXI82Nw+H3T/NicMBUhgayVY8WIHwd3xzKUlW0tsEhz5KnilboYTm14WjGfQNV2tpBOYDStpl9mIt2zJi4xPg5DpbIjgXWaihai8FBfo4RFFHVOxzwA9ZihuuZtGQO78xwrXDIs+QxxbrW11s7uKW0PV7VOsfDfktpS2THgrTVkFJwgPPBkwUYOTdxc/U4yE5OeNVPysGBCafNwducNFrhkGfJYzo45pozzZJcpffvpkw2HNQxg+2qZmnh84dYbec4WCI7Lei1Gk4aReFQigxIN8Jt72cSOAQOgUNP6sDhm/1NHvGWGooaj3a7aheEA9X/04/eGddnL3NqTzPx5rusCFvg/L2wycChaerCwfwqoWvCAKgOB6xzvsnefV3iwC1lA4fSpGMcfCALl40cRyx6umFUdThAlbTl2Tjulo9+dIbDSG4S+284hO1ZpCocJEg0/dlqwwGhfBtnaD9gN+sMB0rgUJz8ZxwYxoIjaiipXrSwMAvaUhyggheFPqUQHNZu0uWObsYmAW+PF9AyB+nFtWFsboCykxFpfseR2edQfJo44JJcuTWk59Inw/yi1F2wt90roLNq5K0N+4dggWB1dOsjDjIBnb4xbG014SC7ZKt/pgaNaVhYGrSlW4F9p6EYHBg64FyvZl4dUjtcY3FtsBt//NmeThrHhnOIw917alhTHMwvSq9TDweUWDXA4fWLISDC7CTaOnHAwytoqA0H7mgp2cc1aEzDwpKgLeIwfr7RUAgOgoBTaOK7I3Zt9mEOZotrQzEOWSwjHe4VuuMfjk+PEvs8cUC/sTTHseFgjVg4LW8LaTWa011GP6KjUbZ8LnHAkzdoaJqqcOCvzTugDxpLg7aIQ7q6LBAHdWPlA4DlSVybdinby1bPBy3EAe9+K3c4sGILrcAECA4crhqGwPHy8R+vnDS79kbQAKkPB5Edw8NrRljQmAvaAg5vNqXLzWJxQAf02BazFulm5Jsvqi6liTyvjOHgUo2wQOZXMgFy1fB8p71m6xs24yMhTVU4cATRZdMFxjUuaAt3zlepy1mBOHCyxEGr3WMgT44DR732m8dKD8/JcbDnIz5CDz96xrtq2DSrGaFCN9cXQ7OWhsWhaarCQW5zzEa5ZXj4zdQUBxynOxsdSrb6KgSHbCndNQ7WCudKjKug3TLHwcXmcQK27cGEcFptmrpw0KFAheU0hwPn22Z3WLvJ8j6WgsO0qV7R2n6yxP82WeqDA9cEL5Wz3bvUWp1WYzjIqdYk1KxYyYRUhAPNcBw+tEEYDmnQFnEYuY4FlFJwgLZzhTfDcX7CAL8kri3FwS+l9eMj+uEgp3zkBbK9YV01Fv/Jd/mSbfEOElINDmJKwnjCy3k76bZvOLigLbU7zFpamN0B3wmWMHPSoKIV+SAJrsW1GQ5Ef+Ye1ZCCf2hL0cW+OHBRwArsCrhqFAeBE3pbtKiKVhRExFtTBw4jVDDkNZO9X0q7oC3igMGDY0oxOGQufDCp0ZTm4toSHJwZTo9xTn8cYH7DS6dsS6shDskmASCEbQk/sZpuKsjg/R2HLcycRc2SeElv5WNecbCgLeLAeYfNhYvI4K3ZLFIH752HJyAczse1ORyck8b4a+vppNEXB513eRysGsVBfT7oFqJt6YwzpHQcSpIB6UbITyRwCBxCehI4BA4hXmLn0Ng5NKQr6X6b9aLH0YB0I+SnEk+HeDqEfJPAIXAI6UktOMB8S5l583jTvbTYp73Tbvx8n/WVHyY2uXQeGG6nhT/3z6QGzRLHEaXrELc2uak77wYN0bkfSuAwYPKVvTN7uW+Kw7iXzHMZMwuZL5DhAilFScmFkMiUMt+4cCGRolCUKYXMZMycechMMmYmifI3uPCs5zzOs77vOntzbNs++7W+xW+9Z6+191rrrM/Za/gOveCgEauD56SiOV4cpLV60LMwSKs4rHTpFwcq9kBXc/w4QDtovfUrDitdesbB48gaOFJWghcWKePoHwyvV47ZMIWJTQ6pr8bnUgq6flsk7ZYFSfqaoJopr/aMg1OukfSLZC6q9v7pMgc+tW85XNpYdp+j0ttf/JrHtTzPXH4hfjNiHzkdiyHSRHItU/X2cll4jVbjAJv4oOZJVVall+Gw+4d0zvLqhpyqS2VUhTK3LMJBt2zQ+uveDEPgwLau0Uwc5DKHERcUIt3uc2jqlgrftdsUB3meoblD6COnYzEFmKhmDlMZHQ4Y68HbCvxOnL0Xw9UswwGBUTB6Pvnx1LUZdRwK33hJIP4BCuVuWXhzuTnFbTBIescBlaDdRqgRJ0sRB7rMmcbjQdncfQ5qvP3FyUDCwV5wQ3ueyfooT4diW8OSOsUE2rRawUlGhgMt4XLbLio3y2Iy4oAiUvJmRprUy59K7paFN1eS0icOSxSZZcQaRRzsMod+YNiSGPOcPcHL0ccSHxP7KE/nxWoYxFJGiEPpJoWy7O3gwHEcax6LuJC5ZZm+HZZ2/4CT6P5x0OQ/1GgWDqy7GljEPGfT4prYxtSxj/K0i/HtsM0FN9SwPpmMDAeOitwvBG1dShw4vAIOGO92WcqrAQdMqTVOe54sYapz5tl8SqhRiYPSblWMeS5axAAld7WR91GeDsW41sBavpqETmVcOHB8/0McPMeegUOKSpqi8WMF0S8OXPByWVzWqB2HGPP8X8ABd73xtA0npqpVJKPCgd9idJOyxd/FAfdowsGBDAFXvzjQSJkrGtZoThw82f87kyV9lqdjMcWgrN7sMxkRDpNjuGIprTTJ0BK0wIGraqUbcNBtesaBPHDnt6gRc/CTAocy5vnW2F5iiNywlKZnhbaldFmsrqgzGRMOJ50GT3F2k+KN1lUOPJzOKKHShH3GmThgFxI7jMiIQgEH5sEtsS+LrVeU7R8HOU7LauQg2SeciE+WChyKmOdIp0PGcqMV27DAIe+jkHYxvCmQTLvLeFAVykhwWJLg616lOIaTqxY5nNn/h5mTpT8Pvbjjn+GgOO0sq8Ou/nGgx7tQIzQRg1hh0i/7rgEHu8/h4A/HcOoXSuMxXCymoOzV+55lTDjscrc2fgolje2uOVgOZy67YvbaQSoRjKkTcFCcdgYU2uV53KZnHChb8zXnGqXfaJxA84Ru+yu/L9YOM2Oen3TnFqWSBrTHT8PpRLOShovxdbuEcHWVhqmMAYdFksVvRo3+uSBScRikGX77YC2S3jPVFfdCSMVhwGZwu2hp4tiyzn4WQXrBIRqH9qC4L+PQ3nFwTWPg2rm8QepO/v2PPPy+VChsO+Tqmth4Y+nKS4OMCwcbh3bGIe7i9o6DGfBGK0UbTfMY+WGDwCrZf8OISCc2V319+x5rs3SloUnGgkNmHDpaHDCX4ZF3udhd57m5TMAPvd3juR2HgJw2kWtIxGYZFw6TU+mx4sCjNj+yDyEOVRZE+sahNA4VDjhQwjSqg3HoT0fjvnxST0oaVqLQqHXwf5x4sDE6OEjHCkpyHcDmP3jbbjyg0LmBsuuueZDIjd5+dgvdgDeGu3yFRdT5HKCsKkqNMjIcSuNQ4sBY4p2MQ2lYky70qMIn0xvZLij4P0JW+Wg4RSCR7aeSaFcKi/KIskgTXemIA3V9H3tcN2CsId2BOIiYikOLjA+H5cahwAGGzuk772gcytfC1ldt0stkybMlNgcbQ9Pg/1vB9nMVKBHhEj5DHQ86B2N93V03gyoW2oisk6BZMOJEFtxj0gyQnAvyg/OsXbgxegAH9QoISpCgyVJxaJGx4VAYhxKHw647MnHQwTgUw2miZNqnrbR1RjBaafQpRGSpppHKJglPmoNSR1dZ+HrUPeIyHUMdn5ISqcGTA+mrUrkLL42KQ6uMEYfc2DGNnTePwtDoZByq8YU0/tcLDq69RrQMNSgn7bPPIUsKeVqEw1OQXfWCDeK8yFEAaTVI2GlmJuYZRxS7chWHdhkZDoVxaPqhfx0anl2NQ/FPwuR9ZO8LB07n8RA+SdWU3qGm/o7xhqSHfW4UFKimOIC0mNa9hZvairUIXqkVh7+SceFg41DjsM1F50JHupNxKKcbV2E8/rpxnzjwxaAxq9pSK/24ffZ5c8vlOPg9VdjIBeEPP9ZOrTjAInUzTBsrDu0yNhwK41CywQDhnYxDebcNdjwaI6g/HDRe13R4c34iw2lz7ilOiQNbZJFxHc+ZGydLm9K0Ar8YFYe/knHhUBqHcghhIYkxtWon41Dk3ehnvCD6xIFTsp9wtGEcOFqpU4SaTkaqnMEYmgwHfABLOOYXuf7htzLToUctW0pPOqji8JcyKhxmGYfq3IHJDsah3GR9BLfqEQcO72+/XIsY+O2AaT/agrWDNlrRAtC76xaw/MP+Kxqc44ChjrPFVU8/nLTIBA6AULTReuCxIB/tQlobrQw5X3H4SxkLDs3GocSB33cn49A/vdf1igMfecm3RNtrB5+i8exN3m2UBLzGwQ6h2PZyD4rHcDRxDcdw7D+tyysOfyUj8ODdaBwqHDgV6mQcqsy9ObR3U/josLMEZYz7jsef1MyQXgWTVCOJOMA30q1BSSPgACWNc2gpG5Q0Kg5/V0aBQ9/iZXqNK12l4jBdkVYcqlQc0quB7loqDlUWPnJo/zKJplAD6VbpD4cVGa5/hTSjykypb4caZr3KRCoOFYcqU6k4VByqTGU8ONCeWLF55rb73/Hp3XjQNqfQzODfxQHhenEUiEM37uhCN+Pz1aRq1SZW1dNOcP+OAso+hl4IbUutq17d0wyy0Sov3VI4mAMHW4rROqAjDt2bwQNwCP2N62D67+LA7A+jEQPhQG/foT+2vq66wR8EB8VwkDra3DhIAXYBcIAWEdxlM6I7eJgDB7lXQjMGcyPDMCnRcdMB1V/TEDjwe5d9PMbP/DhgWC8CDjTv17QH8455cGBpvByGxIG1DK+HGp59ABz0VTihsATWbrMLJYnyQJdNZpYe2MiKCApvUz/JmWKaURQufYM44Kect+7eDMw3JkYHskhYY3VWLOFw4LH0rqRHc5FEU6HfdqORhoqg7anBJ2MpxMYLB5dwz6SPn9qNfprK7vFFIvkuVgXy4eSMdvnEPp6Y1v7yrL1RpYP8VCFa8FX5DxW80fH2RSkVZzpJEg65CyW76JXHIuOQF/740/QlOlNIy/XRY4+jlHTJMSq7N4O+nPzGMg4ffyqtbK6SVA2M9s928+pVLjTgc+nmLbSKIg4uEXqG6t1Klt3ji8Dhrt10MWS0yyfjsOmFmTcqBnuvOAyAA74mBEHHdoy+y+SgiM6ThEPmQmkKEIxk8EmybvBkiYVhBKSIanmmWAAR2GAolCAiiQiZgOwdm+F62POYJktYGeERuJradu22WCRhok4r8IOz0tLyXkID6HOJOOQlQs9snTzNIB+SZff4Ip6OxurpWcbM5RP7mC/m6I0KgFZN8SFwoLUY5Lh7t/WMyZadwYWSF378JJoXq3AaghhOWaY8TU1vPgDlbLP8b+HgekxxSI/gH3qBsEVyBuU3pG2UdC/2QFYi7xm7V5rRPdlF9oTqk2fMXD4Zh+CNih/VvdYBcKDRyz6HcINS5j/7PLoTfuAbnK1o5Ol71x9FNOY8UyiQvmaOPL4d8F7CHLtXHEQof6vVkuhI4E/6VSmVTx/lJdwzZH2bC27ggqLsnuwiHyva8ozZw41D5o2qOkgePPoPbOD404gpDeVPHIILpbAp1BqrP8/ktF2ysByn41yp9jdZMg52KgYqNdoiDmyw/kz/hRJZz8B8A5xwWVx2T3aRddB984xaEkQc7I2q+gsfDofoVwZbLIiOuc8NN61lHJCS/Ns4YBPmiZ1ovvyvL6VXKXEIL5C/iYNLhJ7B+/S0Dfk+LbsnuxhxcMaZONgbVcVhCBw8dvzN4AvENo++MOEgXIoS/Ld5suRMoUA+WaLscMfhfC113iALG60zcPCkvh0HT5byEqFnKCc9s9vmm5Tdk10UDvx9yDPOnCzZG1XFYRAcHDpN26caxfSm8ScOdqH095bS1NtoXUrLW5HLcUXdtRl2AeNjuIiDWspmuTlxKY22hqW0S4Se8aK57B5fTK/CVJ79kmfMXD4Zh8wbVV1KD4KD9AE4z8XBEL4vjJszzqL/iT9xsAulbKMVm5TNG61HcLWZZQppjCe4gudGK75yJKFrhIHTqRl6OFxgWEmD4ynggBzY9sRWKbjx8A0brXJuDwiYIS+R9ww0U7BxjA1UlCq6J7uIMmg4ktgyyjNmLp+MQ+aNqm60DoYDz8Uo+OY4Q45L6eBCqTiGMw6+AGk+huOw1TFcIpGC0dexGaUKH8YziAs46FCNZtvGIR7DPaITNGVwibxnmNb5Ydk9vog31P0bKl/IaJdPAQd7o+J+az2GGwAHKUZT/w2C33DE1H3vHYxWTaXtQqlQ0og4KOtxj+6EkdSspLEDYk9RSUOq5bs8f3C3Znhz7NYtrOCNH3lOV4wDVS6ksBFwkE4EA2MdsyG8JymDS8SeYXvUY2X3+CJwuAfvSvltcka7fAo4uDerksYqKyfMehpJIwuzThW+fz3KItcv83qjqip8KwQH/AZjpo1JC6bHI8MBCt4brDY4DvJGxVlTVfAeBAefvGKq7R/2OSPre9asyfsQONgazmLDuCg7vPTistfDR0+utf1bxMHWcXMIO6UTDvJGxUJbX3f5s1ss7Xcnpnveki5fwW6G24HO/z/IwDi0R9a3arN0mOeUHpbSraPS7wFvTN2//cXv6FNZx80h6pQOOMgbFQvtveceWyWd73PbcWAzCqjwyP+B9ISDhzu2F3uLrN8/DtEabn4c3Cxbx/1N6aNTGIKixVDKzSisdb/YeZX/gfSDg+0cEOxg8LPQeZrRbg03Pw7l0now0RHdfDi45B7/h/O7fnDwtJ/rghhZX9uj7ZH1FwSHZdZwmaWdLIHkqZ9tU/j3tLGKXV9ufqpZDdZx1K9783y8H+3vX+7voYGo0syPjVWcLWJTNllKSFwXdXC0znMaNSG8E93B9U7eSpOl9G6G3RBHf3asrmZw35b2d/+jE4tecPBMg0dFjqwfD8/yyPoLikNQ4QuWdsIhaZrq0M84bPeIMhmHBus4jMHXUwgHR4NhB+mGLJ3yf7Vh+uDJU4KRn+sSOpiHfE7rtrNw0LuvAQee6qma6omKQxccMFK4sRci62c2bHlk/cXEoVDwzizt+HcavGgatCRSRo4oK2UgE5vVYh2nhS7G2S6XK1YYl8/orFROOCzhIiZrb110MIwCpwPSdUEHpyBL7OBga0d/JlTnmNRWCmHCAXUHDQUO+gCswBcK6kRtMFz5P2g79YYDle74CxUi6wfFO0fWHwEOPOS1XqBwYA79McUBw00tZLOareOUN0SSTOVcYppHin/ZVdeFVoB6QLS1A3I6PZ+BAxfqjTjIEk/3w58r/MXQIw4a54QgBosNNmzTyPqjwSGztCMBUibfB8shmYFrlZr+MQ6N1nFKO85wUnKF0gZMao0D8ih7WOa6LtTtVnVzWztyIHhLHI48GoWacIgRuyaX/wc+y/rDgaFBHZp5ltHOn5H1R4EDa2xLO7WANuEU44D/t+NgcyClQxR6Kj9yCcvSyl/i4LrwVqpibmuHdDMO27+8Bbq+EQc8z9HpKg5dceCmBVMtOEiBYGFx8FK6sLRTCzAjRDjIfe47fot/DQcpP2IF0Y6D6xJx8ANbcVg64Ql4SGzDwYuFikNnxzKc6jZOlvivIusvMA7LrOFyS7s/lwuYEXLgzYeDxrfSmizpX6nRItmOg+sSOtjnI+2TpU3wi4XfogYcuDSpOPw748hRwmNk/biUnkTWX2QcllvDZatYDjAOYM4MW3BosY5jpriUzk/M2nFwXeJehWzttJRWlhk44AtJxVKSX1LEgSfY5KgupTuNIy6QuV1dRtYPNmxkZqFxWGYNl1vaIU0CMKlJzUk4cISXOLRYx/lNgY+40Zr2ci/HA89LZLTgEOrCDk7JPaJ1HjdasQuMLDNwEOx4h2BvD5aEwkHNwEVs3mJnt260dhxH1uArI+v7GC6EEl9UHKIKX25pJ8O4w3yyzmbzVDri0Ggd59GdHcPxjrrcioPrgqo88JCSwdaOabnfKHHQdodUhi/7TjiwGT6GSzkmbvzqqXRnHBxZv7BhGwcOwRout7STYRx3lnZ/4UkshskO7fYjDs3WcR7dVtJgEt5mcLkFh1gXdPBNxwbvzUFhA5/PxkEzq+R++crv7fuAzaCSBuzvqpLGCgvIvDDNWGYdtwiWgk03qip8FYeGZiy4ddy/h4PX8qWNxTdVwXtxxtEKwQHWcfsuMA5YDWEe9T9+OfzB3nWF/jvFYZI9M7L33mUrI3EhSnIhIkn2lqwi5Ua5kAshGVkhcWVfWNl7ZZeRne3Knec8Hp7z8fEejq+XF9+Tfv+vd30+5znned8zPuc5czqMm408ULXMQtOlA2SRrcefaPx/SG06zNM8/bfTnA7zNE+DdPgfbDP+L9lmfQLgd7rwn7P/v6DDv6Q4JgB+pwv/OftzOkwn/xMAv9OF/5z9OR2mk/8JgN/pwn/O/pwO08n/BMDvdOE/Z39Oh+nkfwLgd7rwn7M/p8N08j8B8Dtd+M/Zn9NhOvmfAPidLvzn7M/pMJ38TwD8Bb2mNO2/OHs0SH5itu9l4Apeb6Yopz42Hbx/64FQOmmmrOvOCHv+ngW09qGdnl8+y4sOFZrKuYmGHoIlL4pca5scgw7KplY4aP0DlTV+v/CF+c6QrV9u+Z/+zuKCdjb9++mANXgLMlGTrWPPi7HpgEWGXY65TlqJaxQ6qHDGoQOXkokOTZPj00GL0iRY2Sh8n+AyuZVO5188oNsFPABrg4zn30oHSeZQL+fQ67BH6pToAGQheMsXlGpIMyUM/046OM1OB6zknggduB8o1P64jvqPFT53Febffhds2dn9u+kA18tKe1Y/BJxPhw7UlbTe6v+GDis/vv36E6EDAdRGSbLcLnx5zb/9LvgJNmSNcmaffwyFdb6VfD13dbxiL/7Wro7rnPNwkw5uoQmM8ssrZ+mQ/qmU0qUfjcXoex+7yvZv7AvHeHNAaWY6SEVIkfoologEF+vSHefbuu50Wb/3oHoXrnrH3PDNbdDiIcOvBdmoMdpUE4dUeVJB8TdUff8oHZZ6Yxd8pVUtaJ/K8TbpNDYd6EKg5m9p6asirP+LDN8Ozy1I5PtdsAqW4Kw0yjMdgs43U3099/zVb6kGrnn9pi062LqTdRUiHayU/gsdHn1glSJRDq+9ifbMdMjvQnkQkYB6otwJ+f4NOhT5W6JrJMLNTdDiIcP/c93kaSQrU6SCUhFBSP4P0uGwTcoLilWQ4mgqjr+fDmk/LNMh1jtWhKVnp4NN0xc0CaxRnukQdb6Z6ut/2REevwEmhMtLJ7lBh9xji/LmkQ5WSueBYg5HpJ1ItYNZ6ZAbF3YqI4G1U5RYD/lOXWkpDElJginc3AYtHIrw0w18t4AB+vyWhUwFBeehGVSGBf4gHZbeaguwiwXD8kXrfacDUBx/f2MJ2jCQML40jDhmLX1VBJ6YvbGkm/l+DRrlmQ5R5xspXA+3ymlqdyg7ONRFh6jJFuhgpXR5J3OoEXhDuLbNTofcRs1I0GtV/pDvTAeUHF621OpRCje3QQuHAvx0Q6crAblcUKWIaO0P0wEeA1J6KS7zgePTwcrZ+g6Bh/gFIdgtf1Uuud79ZXTQs/U1t0Z5rgT21sj6er5P5ZbcxR1ddIjy5pEOKtSKDjRHGsPQM/jz99GBmknbbLNb+W7X+c50ELBh38xwcxO0cMjwq3DUedvmjg3w8ZC5VFBCFIb+KB3wcXm0KOLxmXoODPztdNCcFCoXpIlTuaR6NzMd3OIhvvgTNMpTJYg630jh+lx77VcLf18R9JwjHVhMkQ7ChY6zto3ZWBIc7kXtTyxYo+1mpoP4it9+Wri5DVo4ZPhVLnp9IpkOqaCEKI79YTpwoJImsY+CEg7//Y0lJIm16biQGqx3s9NBVZKFFjTKEx0SouH6fjrkrvSSp/8JOqyHrSuWXD14NnZXmrM9B26zzesL/y4dtEvgah85l765kw6C3+7h0dCs2+bSyxYLdAhX9tOBxbrjZzBJu//YQKuwCZUkaemPQIdChUUWRnsxaJRnOmRhJl8vtzobSy4m1eta3vyP0IFPuGTp3a+huREGWte7ar+F0kAr/mgw8nfpQHSWW36P74xDuvmPNZYi/HRDo9Q4E+gQruxvLLEo1nkL97HN+o/RwVVDKPmn690IdCjtja85bm+NctMBR9TjCzrfTL4+uiXtdpC4YxruWBRY7ErjAEXQG3TgE27EuNI403DI/5qnXEk/KiRYUGxl/h4dxNcvqp5+vLkNWjgU4LfeNAenAx3ClUSUn6QOOuCz8/Lj7EpjNFhM+NvpoGywfsWvg+vdGHQACMs9xFmioFGu8Yo19+FIX0E/6nyH66NbcBSbZP7uQKuqHWZTuKMwHlvLm8MBMAVDhJEOwqr+lN5yGw6MFKSBfzgpYCRYM9C5g5ep78AKGn6TrzdzXEkp3NwGLRwK8LPEwc0jj2FXJNAhXAnaYei6Z6BVpYJnlpsx0IsBTRRHqmSj0wFOIH6HM4GO34la+iPQga2lx4EiUYiDFzqyzjd8M0Sd73h9dAs4evqobV8zPRw+CNNwki3f4d3YWJKcOu1488gxQ/i4wXeNhDzLvWEJote/F0jjzenmFmg+FOAHEgVz6rGnrnS4Eoh2TMMJZzrpaTjGMeHc3zsN50leB1EmLf0x6AAbiheqNcqJLI9sdMHX/G2db6Z4fXCLIXmON2jbP/TuTR3g7SANDsFgx/lf9x0kpx6bbKMFeKPLdscGfDsZCXqGIIibHvpVV06C6PXv1I1NNzdB86EIP2bu8JkqL0jMVD39JJhV0yEKsiMKBEEaPXRgc8lBGvhK2qTS+HRQCcTY/6ylP0CHf9uSi7/CvrEbgw6uv1ef02kgb8I59TRf/tOVpmv/zJO3X3/Kq+EQWLTd9FVv53ToSlO1jykntGynSwcMBrINP/U0p0NXmqp9dHvO2HXCa6UZYfYv2CBgToeu9J+zP5cOmE7+JwB+pwv/OftzOkwn/xMAf1wXMPSN+UxNJXHYa+NPD/s9++vdtSmGJptrQlubwTNu1NY5UK45FBjvp0MrzKTtoKUnepaIclxzL27KNLOiSJTxMCbapo3zEdwf1/nvyp6xsT1NI72wrYbLsW0nix8zZn9tXfx9UB0yefiX6/80XK3Re7szPh1yzVQIgUJ3WvY10WWEe+mw5J5rweihW/5iHfGYooONj06HGAXYSQfuc6xR2X57v/+uEC1IOMCj22emg+0x1N6zdwhzYUzZ30yHdb8vdNhzLfFCk5kS5IjujE8HvYE0mlJcAWo61LSv8Jo/kxT99OYll6OoZUox6/ZnJjqMv22ZY5TGoIMQ4Iw1MOLb57UNe6tDmw6czUYGShzKBQyiQAHw0Lh0yChu9txLr+AS5rF8ERBXgFlEDMjLnfHpkLcR1qzAS68ANdZ2DL9n+7NPV1m+bLd1LoJImKx7/bSM/yvoQAaPQAdiwtbYOg/wPu2J+RfTYeP3V5Rnmtnm+3j9cemQwwnu2GCjGzAnDyIAT6Eqr+ROdsFiDZzQjwE0eLV+vumam2MtxGMnI0ZBm7hfLImNy9AWO9BCDQjCQ96tacEYf8W+7PCYpsy1QNL2LSvBfei5aEWE8A79QSphIelCXHAqMkhM7AvL4na2h2kdDDBmNN5Bh6hhYT0JwHDnptSocM6Dg2XtPXpA8Edr7emlcxO1M2JW1boTckRc8RHCKNytVfRVwVRyI9ItKIGRPCdMfnpvH0QgtcDc+fcLZAn0SbTg1jlntRAsFz6BMi10+GD/ZYm97DFIv/5eh3XibTqEzIW6WP7gS4MPDsJb1jweZ42ycFImVWkXX3KTW25DFwEfB5e6Hil3miF00meIdHgV5b4MDD/C5VdYxq3WIE7dcpv0G8q3V8dZ+wSpcKU/W1f6IHhOtM8gXZk3HdTGkr1KKmEhRXWt86LoUPuCR+34MMRpZB2Y7fCGwJXxDjoEDQvrSRR3Zc45t4OFDgiW+yk/pkPMjbUzfp3VQAcibqMqIt8tOlQFU8mNqHriyTJFTAornt9SxexF784/m5e4tkDFF4hzrmoh7Q5+8UGtywWG7OkBca2F30ttOgRofkWH6zcodr44RGcrlBeINZOVdqGN3996UXgV1loizImQ253sAiFAwwqBUdttG+ig6S2AjnBCNb9K8NmOfPXgGALUflFK2/kEFIE1LWhRXcMd1xIoci6thkOQXnkuYpHVWBKSiGxG6xMzzrVUAqqOdCFEh9qX9fZff8m7zl7I4hULCqdkvEmHrGFhPQlwFt8foIXoO+e8chB44T5KfrgrHXJj7YycVX3WhRZehGzwVhjVd9NeXTC13AjNUpkF4OBi1wHd59rv/Ovl/rMESMz5z+YMC7DHWWLPe0Sw1PRzq7VNh5Q5iwMAITDzvK/O+kk9o8pprJmakz3z2223+nZLvwNZj/ldCe5kF7TyjcsQf0UH5k8SFsonUeUpLdHhmi41I61pwdeCmkob8n/cN7B9qwhKSkLn3aeUhIClEqwLYTrYl/hSwikwpwze4knBeJsOWcOi1pMofsqKc145SM9kzXTIuWGGU1ZNBy1x4yq2zyqMqrtpry6YWm6EZvnLmAQ6+KDy7yblEvc+wuDQArdzLnOGRdg7mwFkvkLpt7D8Q3SImQtaGVwNpLPI6eZCOdXMVIgWaDp/y3g0u0AeGKeaDoSKp+mpg/R1yqvlkIKmBU+xHXYJvmYCXQ+2fbum+mwwhasyX0kleKmj6FD5MtCF5WXBeJsOeVlmrSeB3elPuRSoRq1nO0i8lK9f6slQbnJWCUf10IRRdbf+uGBquRF9HRD9TQHyvPTb0MS3U3nkxyfi4jIgWec8LWA19voa6YGe59BAoY236WAcEx1wXPVIKDtyPtXMNNjg0PAdjcXg4swhOvAeGQawSiizuH7aDljTgqckMdSigxDQw4S1SapbKre8EF50oKUmHfjUDjrkRfu1ngT7LuxbOudpUbd8+IUOQ7lJWRUd/NVOGFV3809dMLJtOlDvjx3/P0QHTQ4tsvAyoEL5FZQ0krxBgw5iA1pqfXRw5vLvSAf81INTzRygg72bmQ7xKzhEB2ta8JQ5xL74P0cH4DQjHWA0rgla8MhjpkIHF0ymA5QPdoOzqJd/kA5oKC2N//CRoPZHnfMeOpgN49Mh1MwmHYRVkw7MYJsOrlM2mhtL1rTooIMbAvj3NxtL6tKkxtJoX4fcWHIT3Mv3Vl4RZxp0CI2lodykrAoOkyBhlOjggsl00PLCK9HQb9EhVoenMK6ET8OHnyB3dc47GkscFiMbRqODUTYAM9PBbyHCaumKSAca5ihVqoL8uAg3a1qE9+msXeksvkJdiCE6BNw8pNTdlbaGRa0n4S9OlfNAB1zb7kpHOrS70uwwPFFhlGpJVTCZDh7+/wNdaZ08+Lny6LVvL44457ErTVgyHQQym8nSRzGfO+nguvibdJA8CMylmukU5t8UPKej7YFWDJ3haZauiHTgo0q/HOdQdFUV1EArjuO3NS3C0O7vDLRioPQ3B1oxZKahSaHkgVZc3qIDrWtkCY8+CiXaM9CaNSysJ7Hk6jhaVg2jXjvnv6WtLJlTAjiUm5TVNNAKo2suU2EU7ubFVcFUdCCWeHKJlYAzeN//gYFWDY2XQtAQjXMeB1qxUjnSgfY0AMmHiA0eY+mkg+tioENAGYgA5VwzlTQi5ppwIlWp5E5zGo7tS0tXZDposoMb4WSRe8pgWNPCFk0HTQc2puEEaZqGCyhRFwKpRQdaV8bYzE/Gm3TIGhbWkygxdprpcs5/rbxvifkwDZdzk7JqOhhxuG+Mwt0UlagKpqKDZDwQEylnXCI2zWHVvL/BT1MPxLPOudU+mCIdZI8Ei+OQemQvHVwXMx002SmUc81U8jvQNcE5bARpSLvC0hWRDg4Z4BRv2gKFAQRDNd8H20Eabs/kIA2jpMiIi47AW2iIDrJuFYjeII2sYeEnaX+R+xZ3ztO+LIgkwFFJTzwIL4dzE7NqOghxHjZG8W6JSrhgKjpIxqPEWQOt04RJoIPqRd7fYB9WaF7inNs0Q3ru2AB4V3SQvV/ItKDpINO9dFBdHOw7HHaSUM41U8mlThUvlZ/dGTuEz6XpED6nHMKX0+zRa7aejY+//Ed1cYopYZJC+GaPTlQIX0gdIXwjJIbwZXdGdCFrWniJSYTwmRzg3V/b0HQtr93GXniyno3/r+lgTPxOebszwJvNIDTU8SkIofhBu5A8iZD0BnjPnryEBpOJ2Z3xXbCmhV5GL2yb388z2rfsn0bxcrL1bPz/TQdhEj8Ovfln38nNcCd/Hu5dv+PjMBod/HlwJu3O2C5Y08J++GVknGa0r1Yg51mbydZt/H9OB2NienTnnwHunJUfZp0/D7lCjk+HTM+lkztTXK79p+3PpQOY/kvFMQHwO134z9mf02E6+Z8A+J0u/Ofsz+kwnfxPAPxOF/5z9ud0mE7+JwB+pwv/OftzOkwn/xMAv9OF/5z9OR2mk/8JgE8X0h4T/eIqlnvrt+/5fKRsJM/xbL/+aqt6yLs39a2VphyR5p6a295gk38svUHIUQsX4himS/pVXHom6DWqiDinDjQY7u/Qmk4VwPZ1kgFkRArCKySI19zuhGJ+cVidm14T7AFAW9PmDIQdiw4eaB+dDp4E2u5cT6WOTQdxQLtANQU4V7gbkzttXLSQ/2+lwxqM5h2HDtly+zrJACrgDjTl+pgWHRQkKfk8U1xgC9AOOhCOSdPBNWrNVsk5zNT8HpsOK2PvTOX/PATft2kq79syeC7V0engeITJ0KHIAMIAIuRLeB6eTkG8YTpo8cAC2jgzgZ0BbdMB5tCw+FfQAUuQO/csGZ8OS715tKLV33mzRYd+XMang9Nk6MAYQlVTLtxRwMQgHQihXoK4aSwFxDYdHE0clegsdxdCkqU3xSPU8uuwnzZxruXusvSaQ9H/Jjq8geB9bTMMOgwg4CWwKGBr9AkXhqwr7t6r0pGMlyvNU5DvVhSyN4yMhSCVOZ8P91nGrvhvUcQ/TQdk+cH9l8X7HG/ooqlUeybZM0v/xVD1XwQAdZ2FGb2AnbHWg3QwI/XDYe/ETYCud9VeqjLW4xMdYomtCzfhWycdvNYkKNFZ7i4sWFGx80qe7a2Noj9vDKJvSXqN7mjpzd9Ch1sXVjz/U2Wl4gACpgNeZcLOsnSEVMvOajokvJjfTZX3sJ1wKASpzPl8fV+QsatFEWegw2bPEfGHlpWztqxqXkn/1ajop9UC7YjXCdPJYTqwb5CjpoWb6ECHZN56fKKDS4w3CpoOOgRZvEqJLsjdVbpx/KIVDhUdOqzQw9nO2khP1Ti06FuWXiuNxp/Oqg6NToelUYJsKy1NOgwhoNbeoyg6a/QRF1Uq1cmaDhkv5BeLHClBV0oCX0W1mUMhMEoynA/Sdf6t0uR6nFnogMqAWvbye1iKSWeDZTSCgiBehUolAMjrvF7YTxcwg3SAYehEXbprHMVDhDlqBXAnoPiDhZjIctTjEx1cYrCPk1oq3KYDWaMVl2Etf5Bcs9xdWOzOYpf7dKKrNqqLo6GDWvQtSa8BfvzUq/bvoUNpaMC2xL4GENAK26OQhSxrwbeg1iiaDhkvoawzUvaXqVwIPk+ctttW9wWFPxplc6R3oE9J9iQg8JO1aJl0sPRfjYo1OKodn8No6ULGwXSwdR4Hs5A2usHyzqyeFaDWXwxKkaKDS0y1hnd20CFIvmUVEZoJunGl2CVgss0dXFHeWxvBdX3Bguhbkl6zTo0AGJ0OZcUwfkhiZAiBn9o+HLSQRp/pQDhZFJEOGS/l18JO8uG3pFzq89alFmZCqxJFnIUO+ENOK+PZciX9V6NiAcAoVMSkt7ueOUQHbVLAjdYPy11kAYoB8c2eRecgrtC26pEFwdym/eONpVrj6sGgMeVFyULAkjfiMVIPHdxx0NK2LPoW6FCT5e+hA98zGBwnKIMIcPX6T1mwRp/owA+dmgSmQ8LLj+J9KhJLlqVC8Hk+XL/jYmOLIs7SlV4+0EGWbU35zOpqFgA0HcwGjXjy6HBjyaMO5bhORDqoa4X+c1aKrEsscGVsOqBZjxXq21x62WL9dLAISfigDmlNOY1Ph/L5LW2lNh20KDZq9KmaSEoYeQh0MF7j0QGmJYr4j9DBAoCmQ2JDkw5RmW2ADvyAbEBBk6A8kemAh/UPtAbJN3+nAx2CbpxkTCBhw5u66OD5N1fBFh1g7m+lA/5A80saWEMIWJvXGn3CRQ1aNVtNh4yXspgaS6VC50Koz9f3BTpQRoKiiH8hHWrLiQ4BFQsAmg4qcK8sbdKBf4zNQGOJad1r91ow6PElOvBhvXRIXWkp0UU65K40AWC3v48OvMXzb1n0zXQolRKXsnH1N3WlqZPyAwIJAh0SAhgJ8GhF6lNh5ONygBPp8Bt4uUucutKxEHJXWuf1Pq2ZdWOUz5idDrXlRIeAilEMXWkM/2jcxFVugA4aRuE9cMBdaXlkQOVQ0uOL6qm4UUp8HXSoJN+sRBfpEHTj6BnKBIzHTX19B/ZBHfoWRN8yHdDCgDvY3OxvG2gt7/bHkV/SYQiBitCVRl8lvcoOQqRDxsuDpMgdSgKDlxpozYWg8/VAK37jvkgHsI2iiH8lHWrL9dRxEhusBAB5HVuNHkVUag+0guLoE5RpSM0WME9ADrWmGlnCUDAGU/EGinp8kQ4wXMaMd74Hn+U/OQ1nJbpIh3q6RfJrFFjo7krHkQzkLYi+JToAnr9rGs6qs4A50yFORLoDZ40+4UJUOUoSX2YJL02h8d4wDZcLASlPw8mm6WBRxL+SDsEzqgCKDkls0AKAuM7TcB488qzIEB04MsGEF7SSqifYKEBRzTW9FvT4Eh3QwZNvfz5Iw0p08dGejJf8WnklYGTt6ScD/r10CKJviQ4pSGN8OuidZjpkBAIdrNEnXMyCSIeEFwMsEAmh3DkIIxdCSTFI4zjdF+kg8P5SOgTPqAJY0SHUCwsASi2QMyBAsqJDO0hDIxOoD/fjgSFIA9OxApTVVZdYjy/TQRog8G2iSy7+pP3/6vKfjlC8LlHEyRQHQ/iyBNMkwJ+AC3/K/pwOHWndQ7Zff0LFQRnAFIA+CfAn4MKP7F1LyE1RGA0ZYGAiKY+8RR5FYSAGDIwNpAjJYyBDMmXCgBkyJmSCkgyUISMi5ZFXxjIQMrUsi/Wtdi6bvJ3Bf85/7757f3udb52zH99e+1vK/0+HXlHE3+h2SOcsXw6/B/i/gQnfUv5/OnSKIv5Wt8MygBbE+z3A/w1MaMp3KNyA46+lQ//xF7nDbwB+pwk/p/yRb57v+vzY1H861OMvcoffAPweE7hof9GEg19a+r75xbSP7cTtY3jauqOjfO6ywe02Bh4O//TB2E1b4SFcpLMV3XToXwDav0gxgk9aQN0PbQD9azyyt/DU1Iij1S4BwHACwznq+CpOJCSaQzqV5Le9nvulnpnLnbWMt5jR0T/g7dDSYcM80GHKq2GywtGxuKYVfyQdBCgju1CFBPQ38MhfW7g1NfJI7ZIpr96juHKS4OREoqaZjWa3CciFQR8DKXMXKTR1iFusj3ogmLoLfYceOjhM6MT0W4x2VJGKm7Nhv6Cx1E+HFlCxgZN8WZXfwSN/beGtgEmrXYIRtjk3bt/Bc4vYacUl97LUJz0m9O88xLD/Rwxb054a3RD00wHhxFtGrz0yT1bEGmFZ8QfSQYAiTGvyVY47ZVV+B4/8/QsfevTc9Jknse6CcGqK3ctR6uCWJ/w5te02wEbECyDiCaEE71eYxoaIKZ3AKXJeg2rcl44XC1hobsfXQqCyLs2m2IFNxE84oSqvdmCADJm5F3TI+E3U9czdjbbCS3NtRQcd6MfXL48TLA4+aA22ZoQPJ49sLPpg4EiH1YBb1w55UFW4D+z+daxkVuWP8civLdw3mrMmWvloPYxwVmtqWEIjxEQUxcFIRvQgBGe0c4WmTXCclCUGII7wYPT7Ty5uUmwYEykgK6QTEBelay9DxTtsyXPQwQtYB9GBW6KyKRd04B/lWcLGZAh+k3RAmodvh6MKsgKnxfeJiq3opcP5Cwr5ipDF1mB9afycPLNhPVrgoMR1S8F7lQ4GdMXNeaxkU5W/jQ6+0aSD+n7Ww0hntaaGJTRCTER0mPVsAVbZ5zJpSQgITZmge4d4QWpVmA6UN1o/8dru95HFyAQdaSgdIa4XhVTpBKiTYqqHaaTswxjexTP0SlIbfvDiXMR0M7dKB4eNpXoDrrFjNmJ4hyQdECl+eMTmQ5NkBXvSQshWdNJhCEpqdDNag4tmhI6SPOU3JPqQwBHQwwEBTwY0JumiKn8ZHeqNlm/i/lUI7axFU6NKaFQxEdFhz8tF819aeYoQqtEjNG0C/S7x/iiOwNUUWuODB6+aLiGdoGe41TTUVBpmOjDJQDrgV7Qi6MDcWGAsdmE6/SobSxm2po3mp16aDbSUZy8dUJJ+WJa1tAbz0zhKcmSDB5Oy8cLGAI6A6pp/ElDToQX076JDvdFqZuD5UfUwqrNaU6NKaFQxEdEhPUN0wDPNjmETtLZu4RZF5btvp5/rljuIuUgnMPWGhQuXYxypLKPHu052O93AiGIVVunAv6xs6EboGpVu6ZB+49J97qCDgo1ZkdDNaAy2ZoSOSP5p/VqIPgRwypGnQoeoiq+jKn8XHeqN/iC/xdHQoodRnfXj/ymhYTGRpEN6hrYCNpquP15OVcrDSUyHWLQRDzC8oGSHu7R4kfTQAV+rskEHqeVY0MWL6mXbl+kgO/rpIIOUX0olNAZbM+LDEclZA11b9OETcP/p4MLjRnN0mSJ8sWqhOKvftSGhQaC+SAdll3SQPCp0nRaeXjduAB3MxBj84FKktQsX3hsuOrCJZXGYb6eDOjkcV/o+OgChH0wHa0Z8gQ4WfTBw/+nwGTqg1zwWzYykg5016VDUPr+KDvKypIPQReOWDBtABxqa3+pz9ilQ7HfRoW0s8QRlG1yFboQbSz/l7ZCNJZ5bg60ZEWXy7GxS9OETcP/p0DaWeJb8TNKhOqs+9jPvu+ngj9HGGUQHbpvCSKS4Y0yikIxce9XRlf6wOHi8/IK5qdU++RZyzB6WrvFwlSQUc2i70lYtMsK9dCDybVfaBhtLfdl2pSngwE6hylc9KnBJBw2o/etdaTQs+cBIOlRnLZoaRULjc3RIf5Yei9DMtwPe8ly6O4AOeElhCAzjNCg23w4zN7I9h2I9kIWjY6BVw7bIGf4N0V6os39sEDHb0I1ALWDIh4FWZI2uFq6DDrRCI0saXv62gVYN78H5QznEBguKqhnBI5IzG1wjmyr6YOCSDoAAQ4tLVw35RwdaP91oSzNWOlRnLZoaRULj83QQnEAYaQFxCP5G3+FLXWk+jj0Nl30HHhbmTTpQ+2oQBJzUY87Sl8Dc9+OPT3TyImZneK1pOJW++Gm8BGWFNDmocCUrOumgyR94fcyr2WA/KoTNgGk4ZmPRhwJc0kESG/seexrOdGgB/cvo4BtdBUgLHcJZralRJDQG0EFwaoct8CrQjJGlOVcuunvQ0iGDNHJkCYMqp7gdiLNGph1BGmexnYmEOxgU8pxmuzuTQRoIz1WQBktHKIk9xe5iTY7vCNI4tkaRFo66sMGGwpoRnwnS2Klriz4EcJUOhGDygSfI2FUhHf6tII137J3RCcMwEEPpAJmj63acjtcfw1HcOghy4VnIC0h6WAmBs/OnDl+bte7UqCs0FnUYOMeozBjuKJptI3y11iN8Vb17XOif0upQdzPQjPBdiLNoChbEMxGvYxo9rR99CnXocLFhHX5HyYD3BTiLZoeFOgs+t0+oQ6OLHetwEmXDHamJN+Ismo0Wnu9jvktEqEOjiy3rsI6y447UxNtwFk1EfoB+rg7g5AfAFy3Y6acOnPwA+KIFO/3UgZMfAF+0YKefOnDyA+CLFuz0UwdOfgB80YKdfurAyQ+AL1qw008dOPkB8EULdvqpAyc/AL5owU5/XYdHVpb3ytuB+nQCwBct2OmnDpz8APiiBTv91IGTHwBftGCnnzpw8gPgixbs9FMHTn4AfNGCnX7qwMkPgC9asNP/sE8HJQAAAAgD+7c2g79DTKAb7Dk4/ID88sLc/nNw+AH55YW5/efg8APyywtz+8/B4Qfklxfm9p+Dww/ILy/M7T8Hhx+QX16Y238ODj8gv7wwt/8cHH5Afnlhbv85OPyA/PLC3P5zcPgB+eWFuf3n4PAD8ssLc/vPweEH5JcX5vafg8MPyC8vzO0/B4cfkF9emNt/Dg4/IL+8MLf/HBx+QH55YW7/OTj8gPzywtz+c3D4Afnlhbn95+DwA/LLC3P7z8HhB+SXF+b2n4PDD8gvL8ztPweHH5BfXpjbfw4OPyC/vDC3/xwcfkB+eWFu/zk4/ID88sLc/nNw+AH55YW5/efg8APyywtz+8/B4Qfklxfm9p+Dww/ILy/M7T8Hhx+QX16Y238ODj8gv7wwt/8cHH5Afnlhbv85OPyA/PLC3P5zcPgB+eWFuf3n4PAD8ssLc/vPweEH5JcX5vafg8MPyC8vzO0/B4cfkF9emNt/Dg4/ID/skzENAAAAgvq3NoMfYyQQ3DgRdPvlwPEHnH8i6PbLgeMPOP9E0O2XA8cfcP6JoNsvB44/4PwTQbdfDhx/wPkngm6/HDj+gPNPBN1+OXD8AeefCLr9cuD4A84/EXT75cDxB5x/Iuj2y4HjDzj/RNDtlwPHH3D+iaDbLweOP+D8E0G3Xw4cf8D5J4Juvxw4/oDzTwTdfjlw/AHnnwi6/XLg+APOPxF0++XA8QecfyLo9suB4w84/0TQ7ZcDxx9w/omg2y8Hjj/g/BNBt18OHH/A+SeCbr8cOP6A808E3X45cPwB558Iuv1y4PgDzj8RdPvlwPEHnH8i6PbLgeMPOP9E0O2XA8cfcP6JoNsvB44/4PwTQbdfDhx/wPkngm6/HDj+gPNPBN1+OXD8AeefCLr9cuD4A84/EXT75cDxB5x/Iuj2y4HjDzj/RNDtlwPHH3D+iaDbLweOP+D8E0G3Xw4cf8D5J4Juvxw4/oDzTwTdfjlw/AHnnwi6/XLg+APOPxF0++XA8QecfyLo9suB4w84/0TQ7ZcDxx9w/omg2y8Hjj/g/BNBt18OHH/A+SeCbr8cOP6A808E3X45cPwB558Iuv1y4PgDzj8RdPvlwPEHnH8i6PbLgeMPOP9E0O2XA8cfcP6JoNsvB44/4PwTQbdfDhx/wPkngm6/HDj+gPNPBN1+OXD8AeefCLr9cuD4A84/EXT75cDxB5x/Iuj2y4HjDzj/RNDtlwPHH3D+iaDbLweOP+D8E0G3Xw4cf8D5J4Juvxw4/oDzTwTdfjlw/AHnnwi6/XLg+APOPxF0++XA8QecfyLo9suB4w84/0TQ7ZcDxx9w/omg2y8Hjj/g/BNBt18OHH/A+SeCbr8cOP6A808E3X45cPwB558Iuv1y4PgDzj8RdPvlwPEHnH8i6PbLgeMPOP9E0O2XA8cfcP6JoNsvB44/4PwTQbdfDhx/wPkngm6/HDj+Y5+MaQAAABDUv7UZ/BgjgcgGQP6JoNsvB85/gPwTQbdfDpz/APkngm6/HDj/AfJPBN1+OXD+A+SfCLr9cuD8B8g/EXT75cD5D5B/Iuj2y4HzHyD/RNDtlwPnP0D+iaDbLwfOf4D8E0G3Xw6c/wD5J4Juvxw4/wHyTwTdfjlw/gPknwi6/XLg/AfIPxF0++XA+Q+QfyLo9suB8x8g/0TQ7ZcD5z9A/omg2y8Hzn+A/BNBt18OnP8A+SeCbr8cOP8B8k8E3X45cP4D5J8Iuv1y4PwHyD8RdPvlwPkPkH8i6PbLgfMfIP9E0O2XA+c/QP6JoNsvB85/gPwTQbdfDpz/APkngm6/HDj/AfJPBN1+OXD+A+SfCLr9cuD8B8g/EXT75cD5D5B/Iuj2y4HzHyD/RNDtlwPnP0D+iaDbLwfOf4D8E0G3Xw6c/wD5J4Juvxw4/wHyTwTdfjlw/gPknwi6/XLg/AfIPxF0++XA+Q+QfyLo9suB8x8g/0TQ7ZcD5z9A/omg2y8Hzn+A/BNBt18OnP8A+SeCbr8cOP8B8k8E3X45cP4D5J8Iuv1y4PwHyD8RdPvlwPkPkH8i6PbLgfMfIP9E0O2XA+c/QP6JoNsvB85/gPwTQbdfDpz/APkngm6/HDj/AfJPBN1+OXD+A+SfCLr9cuD8B8g/EXT75cD5D5B/Iuj2y4HzHyD/RNDtlwPnP0D+iaDbLwfOf4D8E0G3Xw6c/wD5J4Juvxw4/wHyTwTdfjlw/gPknwi6/XLg/AfIPxF0++XA+Q+QfyLo9suB8x8g/0TQ7ZcD5z9A/omg2y8Hzn+A/BNBt18OnP8A+SeCbr8cOP8B8k8E3X45cP4D5J8Iuv1y4PwHyD8RdPvlwPkPkH8i6PbLgfMfIP9E0O2XA+c/QP6JoNsvB85/gPwTQbdfDpz/APkngm6/HDj/AfJPBN1+OXD+A+SfCLr9cuD8B8g/EXT75TD2yZgGAAAAQf1bm8GPMRIobHD4AfLPC7r9cuDwA+SfF3T75cDhB8g/L+j2y4HDD5B/XtDtlwOHHyD/vKDbLwcOP0D+eUG3Xw4cfoD884Juvxw4/AD55wXdfjlw+AHyzwu6/XLg8APknxd0++XA4QfIPy/o9suBww+Qf17Q7ZcDhx8g/7yg2y8HDj9A/nlBt18OHH6A/POCbr8cOPwA+ecF3X45cPgB8s8Luv1y4PAD5J8XdPvlwOEHyD8v6PbLgcMPkH9e0O2XA4cfIP+8oNsvBw4/QP55QbdfDhx+gPzzgm6/HDj8APnnBd1+OXD4AfLPC7r9cuDwA+SfF3T75cDhB8g/L+j2y4HDD5B/XtDtlwOHHyD/vKDbLwcOP0D+eUG3Xw4cfoD884Juvxw4/AD55wXdfjlw+AHyzwu6/XLg8APknxd0++XA4QfIPy/o9suBww+Qf17Q7ZcDhx8g/7yg2y8HDj9A/nlBt18OHH6A/POCbr8cOPwA+ecF3X45cPgB8s8Luv1y4PAD5J8XdPvlwOEHyD8v6PbLgcMPkH9e0O2XA4cfIP+8oNsvBw4/QP55QbdfDhx+gPzzgm6/HDj8APnnBd1+OXD4AfLPC7r9cuDwA+SfF3T75cDhB8g/L+j2y4HDD5B/XtDtlwOHHyD/vKDbLwcOP0D+eUG3Xw4cfoD884Juvxw4/AD55wXdfjlw+AHyzwu6/XLg8APknxd0++XA4QfIPy/o9suBww+Qf17Q7ZcDhx8g/7yg2y8HDj9A/nlBt18OHH6A/POCbr8cOPwA+ecF3X45cPgB8s8Luv1y4PAD5J8XdPvlwOEHyD8v6PbLgcMPkH9e0O2XA4cfIP+8oNsvBw4/QP55QbdfDhx+gPzzgm6/HDj8APnnBd1+OXD4AfLPC7r9cuDwA+SfF3T75cDhB8g/L+j2y4HDD5B/XtDtlwOHHyD/vKDbLwcOP0D+eUG3Xw5jt4xNAIBhGEb/f7pb1+JNCOUB2wJBOPsB8McKuvx04OwHwB8r6PLTgbMfAH+soMtPB85+APyxgi4/HTj7AfDHCrr8dODsB8AfK+jy04GzHwB/rKDLTwfOfgD8sYIuPx04+wHwxwq6/HTg7AfAHyvo8tOBsx8Af6ygy08Hzn4A/LGCLj8dOPsB8McKuvx04OwHwB8r6PLTgbMfAH+soMtPB85+APyxgi4/HTj7AfDHCrr8dODsB8AfK+jy04GzHwB/rKDLTwfOfgD8sYIuPx04+wHwxwq6/HTg7AfAHyvo8tOBsx8Af6ygy08Hzn4A/LGCLj8dOPsB8McKuvx04OwHwB8r6PLTgbMfAH+soMtPB85+APyxgi4/HTj7AfDHCrr8dODsB8AfK+jy04GzHwB/rKDLTwfOfgD8sYIuPx04+wHwxwq6/HTg7AfAHyvo8tOBsx8Af6ygy08Hzn4A/LGCLj8dOPsB8McKuvyPDqfr3JcOXfeuZ+myc6WvM0Vh+JwryxiMfR2MbeyUXVlSioikyL7vu7KTLWWLKEtJlkhEPvDBnhIlCh9QFMUXvvgfvOe5L++9LsZ+7x3vE/O7c8/cOb95nvc57znvPb9JKBJAvjU/hbLrX+2QHCj58UPtkBgo+fFD7ZAYKPnxQ+2QGCj58UPtkBgo+fFD7ZAYKPnxQ+2QGCj58UPtkBgo+fFD7ZAYKPnxQ+2QGCj58UPtkBgo+fFD7ZAYgPxsE/sZDepKY4tbN00QlSpWq26+A69y/RoVvt2cHfe4e5NAP23fTzGKknZo3EjUqVPr19UBqlapWTv8PPCWfR4F2lpM7JhBawSi27QPreix74acLW5uZkyXTpMzJtX4jh3A75+0Q9WHdaWnatWpW5JG8Wt2gBh/1g5oEy0v1JLWIKAb91iPwn/gcEtoOoz8kN9X3aQaNvAh2Ql/yw4tR/QmxoBsu/odHZHHulYwihJ2YE8gMH9HHUFpO7A2aI2CdfMGPW/jZC2sbWb6DHbRA4nTjH9oh7Zv6n4+7NejO7qkH4ok2iHbjpLDV+0gunXp5JIDX5XP0RNKD61MmhG1Q3bMAGs7rMvQ5+TsPGlGztqxIysI4fjwiP85daXdtwO/yP/hzXRvtr8ZkoNPLdp6+qzld9Q1ip+xQ4udrW3TBcvNZ3UgV2FFZ7EDHeC13nr3FBy33EhXLSWF/ODF8+LWZSQiBfzbUTmnnle5obUWsho3A6Ij2MEdNx9aASuELdtckLBu3iA8Vr5LqUJ+zXqpXj1E7JBt15Cngky4t6y9daAIZsJZHSZe2qN2yI7L8Zs5ah1TbCaXU/E2unr4cTtgQAbXFJ6sTst5kMv26M7q8CAFBShVk6x1arWY+Okq3w7+RYUnsEPxsK+e2EHewtmB+nQXrlpiCcXDCBLoBtdxrxJE1VK9eojYIZ8rbs1kh7QudOXAJjamZ0yLRQ3rZXBCXuyHeKA9Yoe2/TrsrmD6jKY3M/nZpI4kBxnH/nv8sB2IdhqgTYtRuV5tOA6J4dUGM3c+IUM7Ih/jdbv6Czob0pSucifpouKaCqSYdXawJFHfhXSItlBYww7ZduQGN0pu6kz9oNQC3eAJHL6lXIPKEmyU6nwftoN7pNCFK2rWBr88LwJXQjjO4sMH27+0A5MK4thAkhy4z1Sz94/tQI+92oC2pswzEyjEOyAj0LmnV6uj2MM5GAMSyQQpoBDsgIvgBDxId2yHQfvIDXKCLoVuLDw1FF8gQUFTOpnq2VLYDvyxwUe16uAXTWO6XWxNmRknhC8e3KX9SzsQ4Qy5VjzAMmpt6UftIMEmgxWhxfFutHgL5m4UPCtNfNmoRgVXFKUMwKDwp5BnjWl25J5/jvugHXCI0/eQadAzVBX7uf8oAzff08z0XUjDKK5I9fQ3bAfmhaOZ+e0zOhrSyKaYK0l71A5SMRdv+cqqHX7FDghgE0rVNAWygNgBr6Y8cHUKWcEd5XOWQdagK6ExQry0HardsS4hoWfIHrUDZSq+4n+wA91lKezqduBQlZAdkHkxUZT2qB0k8L+oSqkd/owdvEE5WzzV7cw4Wdn5vNasTf8oSeTf1RLCCT9rh6YrF9E06Dt2QK/8C5afHWSyxNFPn7yjny3FDrweuE1zpVB71A7uUOwg/agdfnOyxNGPqgSW2HyCUfXqOaorNW50ugldkM9JiP70ZKnWwOHUxTcnS9R1OdshspSW+mvIDnQehbpQO9uBi05uKd0UVvm8fhNf6FL6Tyylpf4qdmCSl151Zjl7HkMcPAN1RAq6EWSjdogupSkB9eiOE6hZhZfSmCawxvUy5beUps/TfDoXWnGGxpBZc11tL7R2wAtRdJN2tgOdGD/BleRQaPVv4AcKrUysFlp/sdBKdVUUWqEXEViYnMHqLWwHimF/clWzNoYqKqxm17cPF1pJspAd0Ee00EpeI/m40Do6UmjlQnzfhQ3qlmGhVW7DYcJoXbIMV4dEIAv2pN23A5/Y8kpuw9nechsOgpTLTcwYb8OxOrR2AMQOwqzjGkMQ34ajqAb70BgI2YGC/Ku34cg95CPuGXbAu38ey6ZN5YBBEKV7fPvOJg1DlqchHmNP8xMP7jeoC8Ll9cydtMMOOFHY/lo2aRQ3B5nimaxu0vi9TRqsDipLHW5cprgNqzMPo/gVnMGmDDs2vEmjsGsG5eugHbz5rS20ETuhFXNefxv3qQGuD+gm6k062dAFTDmMb9b8M2ALn0C38MX35z+l6xjYwhcFLBfawhdEOW3h+7vA0u3LokPVh7rB+1/bATFNKzqXC7AuZ0Q3cYtXkD5ouWFaLsaGAtbNG7Sv+pfvXDYbvP8+2l5tpckhAXbweMHhVuGCSHp4LOKgDAUUp4hulB46ZsoqOfw7O4BhpIeAPVJOXkrtYLyDRxryhu9vALEeSg8tjw6mJeLezqIbjiQ9+PZI9cpB/1A9CdCvDkgMlPz4oXZIDJT8+KF2SAyU/PihdkgMlPz4oXZIDJT8+KF2SAyE/L6XWltbvN4/3C6bVEsB+yYZvD0+itKt2SHPpL4nX/aWz8lfdKX/y95+2g7eQXxbybVMRB3eZ1EKedqWWpp/gXwtXynI9wHKdy1+JOfMeW+KoihOFCIKlTmmqEg0EqFBo1CKQhBjIiEKiRjiG2jUiITKEIVGoUQjKgVKjda3sM46+561/rbEu6ac/3034T13ON7de68z7vPzgPGJ9gsXRfiLVJHru+JCF8dgfOQ7MtiQMfk7ciBmBEH6p3LImQMBe2tyQGrz4oe9jZRDATPUl7+xf5wctIqG9bNxcphx84JS9sVaXJICRpv0BsJfW9lD+jMudLLtJYwPOgyWWJiNhVpkjBxkE6Q0/gs5EPbmvLIJwN5GygEgmMKqWnn/EHLpRslB24EOL/svcqBvJIfsyUbxw9o4UsZJYKkXejiWDrpFznB9G1S/4+Sg41/IQZli/Lpn+ySSAUbIgT2dEyeDGIPAGycHHf9aDkrUlBxS8wA5Mz0wntK+pj7Ws2n82OEZJsA3YdskBygFOe3K/+Z7xDaoenbLnf1m8H1H2TH0pOTAv90q7oA1nj/cUZO/VSjZG2ud7Bewt/YzJ5FHPEIO4lLRAHsGZDYbc3rHUHkytXuHZ2ne7B1E/Jsjq7be3o9M8UJcMncNHLLlax7fP1Tvpl/o58j0wK8QrRFHsBY9YET8q0n+wkANTXxH+yRo/LRJU9g2yQGYYbycdgfxmdgoIhZcM/jHz3HCtqzEfVvuUQ5bXrAkBLgKDTmI7Oc75rg3chKwtzFyyHzWc2ej0x1yECpPpnbvEKBHSyfvICh3fuC1t6vC3iqjyeHZqrgbndX4r/m1npUc2h5RBYwT/wI4wd+0oBvAC12MHpaawaVVYduaHPD6eDmH9HFbLKMVZwcWXDP4psu7kEKME0aOw30N/4bAx6ZCZAzjcRVa5efkP4Ero3GYAuxtnBxoaO88AmwEgyLXmtYSKs9NLe+s3IgReJkrCR+7dxC+eAQC+/T1/Ara28tgZwl343neDV+gHSnbu/aiNtx6mntChf9TWCtgnPgXamFdRhdibxgbHV7oor3/qRwc2zbI4eCj8nJOFuBD8YKxoTzGHcMmXXrLtrsb/g1ywM0Vb+aF8hEn+1kwREkTgL39iRzolWghaS2h8pwsIO+Ec4U1ce+Qwhf73egAL4P2591xgvzEWpSKczmEhxUw7vIItPKnoifYBcB/xwtd9JZ+KgfHtoUcDlf+tkP6yptS5GLBSQ6oZ+LDyHHvjWhSLBamVKHh4ET2WxgWi59f8idyYJeeR+zGTag8hvVC75zavfvAUslB3ilaqkEbVvUyyr9195panbWqDTuCn2x2OeirB4xcHter7/A8mqi6oYgXuuj+wviZaOHYtiqHra/XsQZ3KhmrKT7pLLhkcENl3TXeleGYVGjIIZH9VPPMoxy8I8E40hJMMU5C5dE65h30fEI+WQ681eXgZfBidQA/ogqrpzgi4XRLloMFjFye5ICPeqY3OVik4R/XfuTkoak78RKdyYVyYJ+KrhpYcBv/khwS2a81YXMphwVt97VrP2LbhMrzUG7e4XLXsd27363/u3LAhqADdUtdlkMKmCwHlhGh15sc+Gs1UybYhSZaMWuG7qVD+th7ZWtsLLgsByPHfVPL6XJQoRH9iezXfuNcyoERI1Q9R2jmnYTK42fzDmzFCb8Ns8nBy3A5eGeJn7VH+0A0OpNDCpjcWULr1a8ctAx3Cb/bsW1tZokW91Fv+fvjZ7yts+CyHGxsFk+junI55KG0k/3oF1VJ8ziU1jLczSswDNOPrC0VKs+Hwc07tBUX9GeSQx5KSw4+lMZHIr0ruBUwIv75UJpfy2uwn97bULp0dzDlxaUVWMOwbb7usA62MEgfyWN8S2PBZTkYOQ4uwYwqZ91cDl4onedkPzb6Zuk5nGhFMJXlSm5XhgtQ69TshphoFSrPTS3vAIJxhqO7meTgZTBGXQ7Add+IidYN6+HKMhIWb1TTXgoYJ/75RCtn81EWCli9oreJ1iWFlydUgmHbJIcGJ3TmGkxsaIafysGX4fC0luEkBysUdkGZRvbDHRpZ9LaI+Z/kQH/wuHBRy3CIV3pHqDw3dXjHIBizycHKCCyfyUHLcM3naAFQUF6G84BRaIBUxrpML4WyOgLQyfinXu2IBG/Htpkc2EobpI9nqhmCBXcX5sxy8CQNZKFFkobLwQtFdYFmWmQ/WmpolqcJs/y1HNhJV4I3kjS4fhVyECpPpnbvoG7GFNDT47h3BjmojMDyuRyUpEGfYcLjaqM1WkXlASPiX70q721Dkg7K6ql+WwSbTSYIe5vw9p/MWtTAz1P4/Ogtha/rY4KwtwnLQazFRPyDKL7g70T444WuEry7PqYHe5uwHAQly8Q/tgFB+OuycVgcxp8c7G3KcnDWohP/pIJE+MOFTuiki934Uzi+s1/HJgDDQBAE++/amTG4gD2JmUzpwYL+rhyOZvyeHGYYvyeHGcbvyWGG8XtymGH8nhxmGL8nhxnG78lhhvF7cphh/J4cZhi/J4cZxu/9c/g+4W5ygJfPErgdQA4gB5ADyAHkwMPelYTgFEVhQ2RIpsg8hVA2SCyQslA2srAxFFJC2JiyEClFlDKXKYSMSaZMhZJ5jKykxI6Vte99Dt85jpk/Q/dL/uu9e88595z7vf/e+67/FBQ6FBS8R6FDQcFHFDoUFHxEoUNBwUcUOhQUfEShQ0HBRxQ6FBR8RKFDQcFHFDoUFGTUng7K1RPR/fw5S7Hzk/h665/Nz5pNzRL/jiw/32WyvKRyraGUoL8ODZXcyxxnpPB6BL2/WbMl+Rp6qh3ySNWGDtRR6FDo8FVoqHwXHaT3d2rm35aesE3jf5wOFQodXPmvSOf4O+kg1IYOBsvOXOhQ6PCj+D/pgLDXhg7MNVylGEZ4RiNjKzJOs9q0KvnqOpb1DWgfMO3Atr6saolHl16ugssUsczNw1zdo2e0Q17coSf62qWqrdVftgLJRSUnqYQcSwkbXUI31FvSzJLlgg7PxzZnTlpAOUqZM9tkySYFas/mUcyVCjB5aSWB9l0c27zbymFV5tp+q6sGNLbr2qomYVpochSUTZbo4CVflpecInnMF7P63FagMcvnIck2hyXHj+UlVnX2eOOudjTRoLS7yVEaKtUYGjKON740VJA0mamW/d3sX4OMNpl5HD2xQQpSIKNzVb46BqOJUkG7WtCh293mKCGBruUFZ2ruz9Oh6+G6H6oumFMVjx6r+s7czkwiDT1XLrRDQlzLK11FjW2tfu9NqG9yssoeMyxhuExVOnw0+5BFFynfN32QnhKOm1zZJDrso6pBA/GPeX2dff1usdWl5ryinNtMVk3QfBPtBGWTg+joJZU/eskrksdU/Kz63NZgxiCeRgdmNjb/uero8f6DUYFLyu77l+hw74Hd8HHLdAh3s3+FmBg8j6NMh5ZMr88+1WayVLfrmi7Iw/0+y3rF3iHjkNg804FVpzR+XxXGLe1fZ8go9r1P25n9q6zgTNneDnWqZNDI5Y6qUMq2cPfH+pITVaJR70X1u89u9+l6acSY6gLlMJ00woNGQ2ehHgzBd06V9HpwL9xDen/kF4VcZ5M80Hn+MNx9n7u9shKqaB8kYLTcfzalMa/gLlxSyfyYWhbi5g+r03RJX4j2gpLJQbT3Uiibl4IiecwVs3q1TUbCl2aM0QGCIILfrqF65b+Fdd77j2A/ljeGuxAG1780Weo8sz9vhLilpXS8m/0rUChjCabncRSW0jZZ6qTc4jWhA/LR2z+aknmVXnIg0YFVmamepvE6XGPVaSj0vM+4zqoS4etLTlTJhxkJGk1FJQ6Dh8h1iXyvzSCFjUwwnGcKOzXHBSLYZB6QZTZOTAKlWYL5OhRhjWibpq3msiAomyzRsdehbCK8IucxV/yy+s8YKWOMDmxncyVf3fwnd+EyhzUvpRCKDrxB0T5umQ7hbvZv7FplNJeDeRx9hg7gNb6c+FSs4VKaH3CkAXoTHVQV7Rh0awb6G6owUA/ud567vr9EuPpeTlBpQc+mkgMNJzxtX4WBDmZFflCwBbJD+7r9bnJCHGzyHrB4cbI84EhPaDVp1VgyY1HT2uKSMHHAgOG4EgRlk71o32uVJSIoksdUFLL6bKSMMTqwU5xXhOryH3wZmvIRFR2Vl9L88HHLdPB3s3/ztogV8zjKdGB3UPcG/qoxHTCcDLiT6GAN+GHi0EkGsq4BQ4nusGks11Js6+t7OUGlecYGjkcHEKHJ6cmgQlXydDDBpoXTdi4XvU1hz8M+hoxTuNAyhIuzVELPR8wybDAFQdlkL9r3WmWJCIqcx1zxi+o/Y6QzxujABy3nSqG6D2poypiw6bfo4OOW6eDvZv/GJx2nzEQeR5kOFFF171DLmtOB+oTvpwPLGnQsbJha9Q6T4G/QQSrz2JLUFq3wB18SnV6iypfogN2T4z2rBfvkICPTAauRut1WDVi/ERJyuFgM4Bug8QMGPG7wTTpI9LfpIEXRYyom9bmtkOnA2QrnSqH6b6GD4pbp4O9+Px3yOMp0oDNat2zaEVVrTwdc+RYd8mRJ80F1g5h4si9u58mS5ASVeeYhM04faFBNTfbQDWmy5H3ZfccofNl6mzId8BcmshaLTAfNmuWGQQNtohsEZZMl+kuTJYmQougxFZP63FbIkyVO4jm5UPVMBzXl53fSQXHLdLC7uv5dk6U8jjIdbOo8YjuE1ZoOXO7kxwZbRzpwBctHIVnE3TP2wDvRgqBFpdV3coJKruf4KIymsu7c09VCY/9B+lR0CEtp51ZvU6YDY8PdxUwHcpejz+wU2bBh8gkdssletPeSyhKRFakfKmb1ua1UmzGqde8BDAnVAx3yUvq76ODjlukQ7mb/5qU0CeRj9jU6VE9FlH4vHWhgpAP83W1xF2wG0jfmCOwdY4evbqRDFe5lK2zbEGVs8KHS4F56duG1S7XniapuoxXT6kiHoBJl7PZp19KbPxIPW2NloAM0VZuG3GhFMGETNhAxjLxNmQ4QhIkIGn127VDt+8NYGGJ7mNVlTMCwBfjp2iGb7EV7L6ksEV6R85h3Xlaf28aNVqgWHTiLh2mheqIDedB1im20ZjrQOZEOIW6igzE33M3+zRutsAuPwzyO2CzQgdf2H4SSX6aDZmp2CSELdODbEwAu0zis3uy8+XSyBPem13Dou3WDM2h7b0QPsD4R6RBUYsTonRafGgI31vgqMtLBv4bjFNteEzmbvrB2+PJS+uMrq49UMsl5KR1MJrxo7yWVJSIoksd8MavPbeO7NEB0QLeqNrF6okN6Daf++aES6JCHCsXhbyj0d7N/Y2wtUOB7GkcapJ4Otln9u+mAxygW/p4OfLduZwEMfCb1Xv76UzrA9m19/SENHkswJ+qkwpnG4ZBG793D0dbLiSqbbh7FEw+ZDk1n0MpTaBHooEMaOspwdlidYNOXdpa67rx+DUpiuGSsHRzQ1k7nuXsvwTQvKJhskGh5KZblJSmSx2Ixq89t43mHVVOna7KhiKt6okM6pKH++aES6KC4RTrg7SG+nHX3K3SQ0Ra/PI5Mc6QDC79CB6HTHZj258B+fR86vGhWp+CPn+3+QdQ+tlpK/TodeswIgmoPPTowLcSjQ0cKvhXUea/+rlOrfz9w/AwTdqw5MCH6q/GLsV0wB0vu30KHEVv61/kj4OyXU8TvbfB20V/+iPvrwHUe0G1S4zp/NX4utnpZhyXJv/5/pauz13xhXFAz1NuwtV11QPq/fo40aYQDhuWnAwoKyi9pFBQUOhQUFDoUFBQ6FBQUOhQUCLWlw4/95N6Q23pbzR9S05m/X/z1tN/28lRHAX6pujqs08XfFExv/IpJcshf8a6dIf4eaMz86ihSwPNBBTWtMR3yae6vRFRm/wIdpO9vpgOu15oO2SF/DR3o+x+gw6+Nom/TgU3/fzr8vUdrPB2EmtHhryHCH6HDN1Ho8I69c3ndaAjjOEt3idyvibBQSBYuKUWkZKWIJKXEzsKSDQsLK8pCkVu5lmSBBYVka0MuG1JS5C/wnK/H+32+fec47/EeHL93nsXvct6ZZ2aeme/5zZwz8/l1OcyyLocF5eDIPSG7RaWJnEhy1BhyjYG5850LCMJLTw16WuZGUjmCg58Vh3fCTxfEJn/sNeB+jnRw1mOfxr5eHmZOPF9FtxHvJ2E26tymwcT5ZUpy/GCK2dMwYMM0ikJONgT1+vjB4dQFdk0jhIczILIn3ZF3TU8s2el1meHMJ646hBkS1cfo1BzZxV7eGGhxkVGUHU7CICXGrEQFGrxvUTk4co9kt3ZDxpFrSBVHXvLUTfHUoKdlbiR1OSgO74sLBtDD+VcemgUr2A7HHHnyOgFCRLcp3o9yMOoc5UCc3+bQnjgQzJ6GAcdp8DNysiHo4Y8GHATyoiVVDnJiyZF3TU9SstHrhoDhR2ZIVF+NDnNQDlZeG7S4yChChwth0ORAVKDD+5aUgyP3hOzmf+amkGvhKXYTuyenpw3OUWgc1nU5KA4Pm7OOODW2ZMaxj00vRKIoK/xvzq6RuCO4PcH7UQ5GneNkiTi/dEyOH0wwexKGODWL8EQ/IWdtSNQjykPY4tsV10VLSkAEHOjIu7Ynluz0ulNOjntofDhs9MwM2Q0SnZIj+8HKGwMtLjKKcMUJg1xKK1nQ4X3Ly4HIPSG7eUOmkGvJncO0pcWI096PpJhxuhwch+c8j0wUrlMOKDbnShXdVvB+KgdS5+S64PzwpSRxsl0NA06Z5ZqCf7AEV5h5M54MiNAOHHnX9sSSnV532jB6stmZId1KdEoOkYOV56DFRUYRvpSGuhwcFejwvuWX0vgmZDdvyARyDS4YS3pyehpLM7hLA4eHYMTZyhfPsCGJz8sBdcyVBN22aZ7JgdQ5W0qLY3L8hLjiYcAF8cWGZIsTc3NJTMwjKQMi4EBH3rU8eQdk1RmelFRmSLcSHeaQ4rw8ZwctMopwhYRBl4OgAg3e98/KgWS3iYY4co3+MqMz4vRmOC4Hx+ElrpqrLcYdQxOJcNfDXEnQbSjO5YAv28mBHD9DeWkYQrMiB2kIKpDnzGEiByGl+cHklqdasuC6GJ70gQz0V6PDHFKcl+dkuUVGEa6QMOhycFTgvycHkt0mG6LINZcDPc2Xg+PwcFTo6nqoy+WAP6eYK8m52d3lQI6fyUHDYHIoDdlAEa4dYAmXvHTbSXPk0PJUS54pB0bH5GDljcphkVGEK04YpBwUFfjvykHIbpNyQJDMX8bbGHFbT5Ych8c/38/ESqE9WUp+NeZKmHEsKAdy/HyyxDDggsihNoT1iHU2lCNyEHCgy6HlyTrAJ0v53eRQm2xy8PLaoMVFRhGuOGGQcuAK6j+QA8luEw1x5Bpc4BJqa4y45lIabR285VpsDIfHWbsw8HCzZaJ4BPQ0slZ02zJykFFEzJ6GAaMnUw6/syF6T8eTG8rBltIuh5YnKdnodbKUFjlIdEoOFmflNUGLi4wiFpptNzkIKvCflIMj94Tslj1XRt8Eco1P5KL+xohjeXzQGs/i0MZ4Ah6PzSIYhsPjSA+w3fAsDr/xQWsUwEShMSwrBQE4Qw5osMmhcvwyITF7GgY8aI2fc5ZbG8J6xHwPM/PIy4AIONDl0PJUSzZ6XQA4T3+ID1opB41OzVEnUV6egxZ3HUWUgxMGyVASVOA/KgdH7lWyG0Z3nQvHtQnkGt7X5OuR4qlJT0NurAUy6eXfRaI2Di+n3OmatEVNhGhnhYlumyGHbLDJoXD8YCQDahhItUtEORuiawdYlMSA6Gs4l0PLk5Ts9Dq+hlM5aHSYI7vYyxsDLe46iigHJwyirMzqqMDl5dBG7gnZbZDr5h8hJEjtL5FreJt/yzHYaCCeWvQ05AbhDUnj7QqX0obDSyBcrO7gGpa7PN44l0ObC3Si22bIIRtsciDHD0YyoIUB+wmwMyJzsiH6ZOnC995GKQiIbdJwObQ8SclGr+MmDZMDoyM5soutvDHQ4s6jiHIgYVDkkFmJCnQ5rPn4z3+xIw0x6dZtv+UQ95aYUcad6ayLDzRBqFuXw5Rxdot5erdu+y4HbArGm8xu3dYoh27dVm1dDt26dTl069bl0K1bl0O3bl0O3botLgclqezG2JubfrkHtU58+U+MXD1jtqzXt4HWWMYUEM936s/fPuBZ3RUan9f/H3JA3GamP3ByQBPaQ3a9viflgMv/lRxYjT2Tw2AHQg7N3l6vbxsAXtLycnCblMP/arLU5dDl8P+WA0B2D38Y0ZANwYJpEwSdIeL4j7gTP5WpCNljGsnL9KT1ocni/y+RcOfcHMkRKQTa8HjE3Slyj/g+ttQweaiv7gJXVNx80KBT87irm8W77/zk7a19s6JOR2Qx7G/jDjLDGBCPPD6Rw2f3XPcHfO/ae/OEy9dHqRwYbziIjf2nbk7EkLRoNMYcLbjOESU9u5MciFo7/fnAQ8lxkYJpMwSdIOLwaYLgVA6E7JU0kjfTF2IbIi9pJpBwIPAkd8/xeAV3V5B7gu+rLSXcjvUVORgqbi5o0Kl54dOKd9/5yVNb+mZFnY7IYmp/G3dQMrSBeOTxVTk8f0FWAiynpF5RDhLvdHDe0xs5kLRoNEaRA3tIenZHOUSdCbIr1DbBtBmCjog4EFzilGQk5LGzNEL2ahrJi/RCbMOVkmYKCZcxP+1xpDQ8HnF3Fbkn+D7F9Q1ppE0iB0PFzQQNOjWP6D0W777zk619W0VJR6Qz6W/jDpYgjwHxhMdnIwNUE5QiLAWJdwzqdJByKKRFozHKUlp6iD27gxwMZFeOmgumzVBnFRGHM/RwpI8jKmSvpql5M30ltuHLVgg61DMCGveESAkPjscj7k6Qe2x1E9cnbSpyMFTcXNCgU/OEF4Dim77xyba+taJOR0xntb+NOygMiDYQz3l8COlll8I1bkbhodyp4EHiXR2gEYULYTTGKgftIfbsTnJQNotQ2yqmzRB0RMRli4TX4AusmqbmzfSV2IYrWyLospNw34gvTnwR3F1B7tVWO65P61vl4Ki4maBBp+YJei+Tu+90tLVvqajREfNn6W87DlsJQWNAPEfMkNSBfsLfzNfkCa7EuzpA1kJadBojquGjjj27gxyc3CWYqoppI4LOsEgZR7m3C+EEVtMgk8iBxDbKYRpBx6ExdODnx7fweIK7K8O6ttpxfVrfms9RcTNBg07NE9ZYDV8jLDN8t5l2Woz0t8kBcZUMDsRzABkIiFkcfuGdKn1JvOkgw1dIi0ZjrHKwHvrH5cBWCIJueTmQ2DYmh3Ek3ICleDxuUz+Gf5eD4O7G5cCWTsjBUXEzQYNCWJmQg4dlhm9WdHE5FH7GhByi02Jq8YMA9JRd43IgadFojP+8HHyylNdsIkYEHeUgvYDvo5MlpnE5kNg2JodxJByyHX3OzaiZTZaIu1M51FY7rk/rW/M5Km4maNCpeYLeEzl4WGb6dqadFiP9PT1ZciDe1GQpEh973LW/UaQGQ7fJUiEt4nfvdx91C8oByymQf2wpTUybIOhUDpNLacSvpnE5CLHN5DCBhEPMv8dqzfF4xN2pHATfZ7g+W0onoR7ZFRU3CzQYnwo1z5fSIgcPy2zfrKivjG0pbXLIOGYGB+I5jw9hyvGEup/25M98rpQeEG8Z1XDA3q5EQO93H3WLyQFDJR625SPHQm0TTFtB0Lkc4tMz785HXsqWJWSvppG8SC/ENpPDBBIOD1lfR1Adj0fcncpB8H0NXJ+0KQbSjUMzYqprqLgZoEF86tQ8Qe+JHDws2/uWihodMZ1Jf7sckCHqhQxtIB55fPqgNSqNIR9ZXi53KjhmvPmg9dZN+App0WmMaLyPuiXlEEFsvIZTTJsi6CgHeyGSNDakEcge00jeTA//Y0vpCSQcnyE28HiCu5OgEd/XwvVpm+DkrF8jn6HiZoAG8alQ8/w1nMrBwjLDNyvqdEQWw/5uySFCKBkciIe6+2s4FoxH2SoHiXc6YPgKadFojNl4G3VLyIF2x3Mn2SYNxbQRQadysNflSWNDGoXsMY3mRfpKbHM5TCDhOAd2PJ7g7iRoxPe1cH3SJjg579FfkE9QcbNAg/hUqHm+SUPlYGGZ4ZsVdToii2F/N9cOdz4gGRyIBx6fbdKIZBk5rowoB4k3nMXTjqs3nU7SotEYs/E26mrP9uM/+CPe8X2rAv7UJzUrq9xBl8M598XDh47vW9GIqxvicoI9ej+KZUQsFbCmH7cuh+3/NAzT2I7vW6Echjd2zfuNLY/4RqttXQ7bGrajdXzfGuWQWxrHLY8OcL/DiHU5dOsG63Lo1m1jXQ7dum2sy6Fbt411OXTrtrEuh27dNrYaOez+ypbUNne5rGfa8o8lWeRevcY+qPYfyIEO/Mr/4n8jNiFDXQ4Hwboc/p4c9nZ708G2Locuh25byME3+Z7w0wXYqZ4D4NN3T8JO3mtuirGAS9jBLFA33Q5NihqpbcTncUxpJi3Y6XL45p51a/Sd158bxKJhQ/AAAnIOHdlzxMiRxqenKPLnZPixyGwAyyUQsLa923ptXA5+BOSLGEE54uO3N9/KTwDV4blwhbrJYRlS1P4cQcSoqRyYSQtWuhzl4J714MyZX0auwDbelYdPnEOX7Dli5JTG53Igw0/lwHIFCFgIct1WayYHmpHYhsOCacI2A2sKcC+HugmzjhS1HPyCUaMcJJMULHQ5ysE8G79uOBJ6++mfPHQVuHVNDl20RzByQuNzOZDhJ0tpLZesulLDbqu1v5aDk9g4Koc7Kw7XxLCJWx7wgg51k4P28FOm24LPq3JwjFqD3lflYJ6dX5fn2tEY59BlEsHIFRqfy0EYfpSDl5vhw5duK7dxObRJbDrmkCKnSYB7OdStEtGSokYHglErcnCMmrp2Yod5Nn5dXsJgHuPQCUZOADMmB2X4UQ5ebsoHNdyfPeP/TxuXg6GnOFoJxcYn+IV4QaVYCQKKFDV8pvi8UoBzowRX5nIwzw7sohzGOXSCkRP8mMnBGX746uVmpk0N9+WE3f/SFpHDOTcPWLK4MCUHUtTwmeDzdpKDep6WQ5tDp8d3/1oOyvCblkOtYbe12rgcjMSmcuBkCQCpo655FkkN6iZENFLU4EDxeSzAQXdTkyXzLPw6l0ObQ6dY8L+eLDnDD1+tXPpmDbut1eYspVUOiViL3h0SvMDnSgJ101UxjO4En1cLcIya0fuSictFsHp2fp3LgYOfVwtGTmh8KUMs6/GhM/zw1cuVEvuKet02LgcjsYkc8KA1Polxj85/5VXcFA3qJsy6QlHDcBJ8nhTgGDWn9w0/3jD865IYeOZZ+HUmB+PQZRLFyJHGh5rGq4hAyvGvAxl+KDLlYOWm700N+/vrFduc13DsSL6Gw72U/9PCoW7C2CNFLaltxKiJHFoYNaP3JWnh8u/IZ6NneQ1nchAOncihYOSExpc5HvmGawcy/LJIfw1HOdS2hzT624d12tabNHzt8On9+Qkn+w510/0WpKglta1g1LQAx6gZvQ8gunjnhbWDe3Z+HeVgHDrKgRg5pfFFTWPDxrd8slQYflmkb9KgHErbuxzWa8sc/8FIq3I4SBPkvj9vf2wROcQz9ZgDHDQ57CGNb+9tFznwT8MAhj5wcthDGt/e2xJywB67/xz5OWmdxtetowO6dety6Naty6Fbty6Hbt26HLp1+xflkP8vNV874yFS7I6IzQvxL+FxEHqGcef2bhalx54RN9/z2n6XcN6d/l7t7A/e91dvRs/YshiNjruyfZC7G/ezzzDuBV7W2MnLG/7PLs0OZP3TcojNO5RDHviNfUOPf/XsRYf9J5GKOjAis+UQG+nubI/15eTA6Lgrb0mXw5waX/t4jL//Ug7xgo1yuObZG/HbOTeTCPFvR+rwz/mSeEoOHs4n4649Qw6zi5kRnYgm+nYpOazmWNHycmDQMf7M/jU5nPgJ7mCQw6ojNTVOaUvJYU3b8PZEDrD/Ug5H/s7emYXeN0VxPGSex2Qm81jIUIaUoqTkQUSGkNmLxKtSFMoTHvAgQnghlAcklAyZp8whPHrwbJ3vf/FdX9+773U4rvtn7wf/a9+911577f09v33O2ftzPz7uyCNSDrkVcxfubMavACu+i8Qx2dyJ3aH5C9VEewm+jCywwvTCn/QH7zkZn5NzEVmfoffSRJrN3zGmY/iJMbxclmN3BJ7F/2MTN+u0FMMSkMOpp+ydSDF6IdGhsuI/L5yxZRyyGPbBHnjbevmT3diZKz2UvbyZGAvPgUv5Q9mBcDj18h3j8sX4MqZmx4Btzc24kTQHp7bwWaI2b5Dvis5GrI1R552ni5h2Gc3f9gRjEzWHNN3i6KAyG59cDufvj/ObLgeQwPAb8ckXc+IYt/4nwmvXBwbIEdFegi8jC6wyvYZ4Pbzlms/RPoKJYUCqTeBwfv7KPR3jT9BHN4ocCvAs5VDqtOTAEpGxx9tbptniRY2OyOHAN1H4xS2zh2gf1WsP5aQHksTCc/JzBH2Qw8vP7xgTh/H1mNKOAdukG3PlQB5Q/IOYLBrkRx5LL4xRJ50XF0UOYZFf1iGlHOBJtsjGJ5bD5oceHL2ockCKnl9yWbi1w5WHDDv/j9rHiWOVwBU1ifCqaK+YivHjwYkvIwuMTK9hHRBbouJEQXzOyxKRTqWJ+BwH2aJOMGHEMdw+R/1omXIQ4BmiKXVmyoElEil2h3e0RkfkECVikN796qKN0WZ4FX8k4hBFBLf0UIFrSDUWnhPlI35hMgKYP8Yp8WVMzY4B20o3FsgBswFzWmLSHOQ2o046ry4yhS1caH7/kkNKOQyWIqTHXhUiKY1PLYfwMfSochjkNygvr9booxHH5NhwQXhVtFceo4N5nlwWplfUGRrBtwnw2OXW9EKaWOMiLIpjPKVd5CDAM3RT6syUQy2RSDHkixeMjsoBgtsZf0sGh/LyiA7VHsopcYuF5+Bzhj3t1PjSJbMjwDYL5nw5wBb+SEtMmoOM3qNPxqjTztNFeTQxKBZfZkFGh3LA5Mw7l9L41HII/b982Hoqh1iXhDxxYporZSOOVQJXoVJUtBcycqzIAhOmF+4SM0ZrRqAgnWoT+JweVMfiD+6Bb8SyVeQgwDN0U+rMlIOUQDPZWvWC0VE5RDPoZeYQ6BTSKD0U4FomxsJzsjxMpB3GV2LqdhTYpsGcLwcUhZQlJvMHGf02Rl3tvLrIpeEOsXKk/yEHDinlwDt4icDkcsBVQ+QQq7TfrmEcVEcsFeTQC4VZVNBeeRnOaJAFVplelR+DYEYbsMtYsjkqi45hRYv7KpEDgWdwWjszUw5SoshBvGB0kAS4STlEddhB+7WHiqWKJLHwHJTPqZx2anwZU7NjwDbpRlMO/EuKsSgxWTjI+IeMOscDqYs8HI+FZ/2SQ1rkgLFFksYnlgPQKkf/SDlAr3jNJGdCR8ihaLbKgSywwvRSOeCv7E7fZf22HNSxY58cAhlXbMpBgGdwWuq05MASLTkwOgvkwKv/fDnUWFjOLDlIfBlTs2PAthFyGJYh+EtdY7JBe5ApBzLqvPPuIm7DYuFZeg2yEId0lhyk8anlgMXbp5RD6DVvUbjClRq+WJK/o7medXwZWWBkeokc8h7spJ+z2woXK4slOsZlwslhi3IQ4BmcZp2mHFDC5SBeZHQWySE/2owwSpvyzTTHFkuwI/FlTM2OAdtqN+bLAbcKWCvVmGzqg+xyIKPOO28u4jYs5rzFgUPqiyWLwNRyiEvpu6/+JoeQKfhJ3ELCuJI4ZrfSmCNAeFW0l5DA2RcyvVQOCOajBzNYeiudJDQ0rY6ldCkHAZ7BE6kzUw5SgnLQjjI6bTlAjkFywkMi7aHfStdYeA7KDxcsRkriOzOmlIMA22o3iFmjHJiD3S534/8Zk+YgixzIqHM5qIs8he9x4JDqrXT2pDY+vRzwh+o3OcQIImqJQ4rHfPFoLXKMOFYJXNGLeAeVz+CI9uIzuBBRYYFVppfKISo/jOdKSNJEOBPPCfNBKx2LoEV2NBGDTDkI8AzTVOrMlIOUKHIQLxCdxXII1UZv17345Bjt2kMBriHVWHhOtBcPZiOWjFSN7+yYUg4CbCvdEMwakuQg8JgRjEl7kEUOZNS5HMzFsDIzDmVI//igNVrcapvS+PRywHLpdznwJjA6l28+wmcjjuk7shg2fUMDtBff0IR6CguMTC+VA8JHyqk0gc98DUfHQCfLJuTeASkRe/FPrYMi+LsskWAJkUPxQqLTlANfA4LGVnror+FqLDwH5fM1XNqR+HpMRQ4KbGM3BLOWSXIikrAjMWkPMuVARp3Lobpoj4jqlxxSfw2He0Q2zlGcUA6Asboc8F4cOx2cOKYv/bEnoby/B9or39+fmW/eyQKrTK8aL719tyb2vO8A2aQBx1AkgvRcNFHkUIFncZ3BNY91GnJgCZUDvfjTcsDmCGzGUDnYJo0aC8+J/7nn5NykkQYY32ZMORMJbJNgCmYNSXKITpOYtAeZcuCyx+UgLtoT0/zyrrDCIbVNGnCfjU8ghwk3U03P9MKUWkZCWzFE7Nd/jVpG+Sw34YK2jMRR/M/K4cILlvRjg7iP2+7vk7d3WVlIMR/ALCMpo24piaP435TDcAO3JCYe1ui378ZzH/RzXP/iVdBKQvyGhciycVhk1C0jcRT/o3LITWpLT8OmgBv+4ugPSLZVTHFpWWYsOeCrGpCODuippy6Hnnrqcuippy6Hnnrqcuippy6HnnpauhyOeWubid5+4oxy/MI/T1P6U06SCQyhQutTk2icRchkDL8JA+aIFn0knUdME/d30q3imrseJx3GPtL28TnoutP91dlYtCC3mW22+SYbEY1gcbKcv4fn4wBPLweGdiI5YBPYts/cKZ1x8tq/Jgfs7TfioMV9yoC15ZDbxzDDE/cn08oSyAFjXXCM2lEff03VjYYk+saC75/1rQXqpOcsxvOtJXKYnzDAeb1rk9dcDsvhlGE2xJRaHTmsuyH+kC7G/aFAlJ1ifPJF8hRoQezIhWSmkkMbz7d2ymFhWrIcPK2QHP7TabwcmP5hOSgjj78xndtut8fx6WSjrOG/3bgzT2yXfbv4DxBt/ktrPOYdLksRxyyQN6eUPrXuG4cBPEnWFaFxgu1rQeNy13j0motU9hoZCS0ru6ONuZcB25gh9ADD4k8HxJ9JMbTtN3FCCZ6QHYgvE7CHndnsPal2trkZ9Q3KJzvMhTnz2sUnD4xBnIt+9FwcHZgB99P2iBZ0YKCDxIzh+FucWElyquFNy1QreD4yBW2xtNfpcYaE7MXxclBGHs524HP4SI4IJlny387n1F7X5BBniMu5FpdD+C1FTA6FN6eUPrXePHQYYRHUnmD7GtA4lMmGFFcEaVU58OyMMfd+CxhDaAFGs+8fMIyuGNr37vTE2IHZIOYpey9UO5MDoXyC+5sphwcOWPNtGImYRw9nwv1qe4IWzNTm6oU3HAuVAyv9lkNn/SoqbZApKHIAHi0ql/iNl0Nl5EU/4nI3HAHJ0365bx2TTLbWoWGTQzi6BjsnT2kYtZdjySlFTA7kzSmlz61TDmRz5QWd0DjB9jWhcTEXkM+7afZaFksVYefMPQRMQugBxuY2MxTdwtcwERbIDsRtcmA7tPdCtTM5EMpXcX8uh7ASsfkNfTj42IL7SXsFLdhECJLTVceiOCmVMsfZhJxqFc9XmIIqh7AZJYW9OFoOlZEXjWIQsfEfPiK7cOR4oxShMjkkES2pbfaTCJeGs1qEcnDeHD63rLscMIVxQS/QOMH2taBx6J1SYdnrIgeFGBhzLwNWQ+gBzmbdEGqTHZiucfJK4CrVzuQgUD4yClwOeQQX7cFIE+5X26toQUf/ybPE+IsgY1Gc1ErIcaACp5rg+UpoVQ75UwyFvTheDpWRl43mMPGsI0KQ05ZcHJdD5WyxAUGaehHj02WWUPrMussBdTGlCY0TEk0LGkeqVoa59rrEXRF2xtxb8x8JoQU4P5shlDJ2IFxLwJ4EjiEzOUidivtzOaQVwkKbcL/aXkULthCCXAtwLNRJrYQcYxNyqimej0xBlcOpp8OAsBdHyMGhYHl7Tu5KyhqTDJnCxTE5YM3IicWEOrggShGTQwVsCZHNrLsc8qIfFQmNE05ZCxrneEP2WuQg6DEYcDlICBlgDaoaohycHUjAXu09TDXlwDrkm7kc+JH4+hbcr9atsmwiBMnpqmNRnNRKyHEYG6eaFCdTUOSwx/M75jKL8ZtcDvhrh0mGzFwU4sbI5QCBzz8taEWachBKn1l3OWAKJ0OR/LcihxY0zt/xsddLlwPZgd8OBAQA9nB7WXs/Xw6E8o2UQxPuV+v6I+cGVw831mUsNPSs1JYDp5ri+cgUrHJY55wnQVcne/FvyIETJj8jP5eRmGQEzp4bjhgvhKRoSxVpiiIuBwV0I8sofbOqKssyuJhRX1afZbHUhMb5kLHXvlhKow05SAjZlARVDVEOzg7MWfnMAcELY+8XyIFQvor7a8qBfMkm3K/WFcU4Ok/XAjoWxUmthBxnE3KqaXEyBfXeIR6zRuQsfmPlEC21bqVh9p33BsMIBuGtnMjAvmeowh+OChMcxScpwhmbVipvTih9Zt0cwI3j2zBFaJxg+1rQOOzAQUsZv9rr5q10Qw4SQgtwllFDlIOzA/kSsfZ+oRzSttxKS6wyekM8EH802oT76QsmogVb6LwIN7psDEc6KZVat9Kcat4GIqxyWNOsxm+8HMjIiybigVo+JaRahwVb9gjNlEeI8acsnmjtyget8VsY8TjMnpWxTiniVipvzih9C6pinYpIEBon2L4mNC7UF2WOfZYUYva63FsrD9DlgIBJCD3AKKiGKAdnB8YUiR7jNz9q7+fKoUD5BPcnsZIfIeEcasD9pD2iBbPRFlfPGY6Uj1ZCjjjLaeNtFKagymFNhRK/8XJQRh7fIQ0ewNUh3HwmxkcFBKUd9eUfX8NhKpBZxToxFKVIpmJFeHNG6ZtbFXFGBBQaR2xfExqHezN5c8Zec0Wnr+FcDhmwGkIPcDYrhigHYwfiM3oivZ8rhwLlE9yfxoo/UYWC6EcT7iftFbRg2mlz9WKeGsMx41QqZU51VqeNFi9MQZVD6pDxGy8HZeTVHQZxXcDTACzCZssBGrzp5j9s0sBr/6YcWAQpt0iklcqbc0qfVy0OcGWp0Dhi+5rQOGxHkH0VtvRMhh/3VrgcMmAMoQWY7YkhykHYgR/FKNf9Buz9AjkQyie4P8aKcngwQoxv0Y8W3E/aq2hBBwaaHJzhmHEqlTKnOOtyKHg+MgVNDljsCXvxr95KT0972+m7zdd2ltwf0soj+oam/h9BHZGWKQenvfFZ2w9br+UsubVODrts2eWwAnKYRXvb65cb11urWXJrnRzW0AC7HP51Ocykva3tLLm1Tg6gAf5PVqAdHdBTT10OPfXU5dBTT0hdDj319Hvqcuipp99Tl0NPPf2eVk8OfCQ3ktzSfsoIvNyGeaCc0DllrRj5bQJP55HjjtrHyXGO6PPW2hQ6+I1NyiOpNZ7rocMbGZyOseShdJDd4pS7V9tpsU0NgE8M79uRR5zkCDjOMA3Qf0YOwMuh24TO2Rga+W1SOfhQkBxniL5RrZFClwfdp5MDQ4eZ6GpwOTRBdosnuobD02KbGgCfGG7t04NDg/9RObQT8XKEzvkYOvltejk4Oc4RfSNboy2cZ5+OdMbQoZE7SbZty0FBdmPi8eECsS22uTgA3jdZBcgMW+sXSyMTx3Bt2Os3cWsuh4lC+Z9Iy5GD8ugIYVOCG3ltZKDlNmVkg2Rwyt74nAPJU2c0y+UHt68TOpebmNf8hvd88hu/NT4dPWU+D97jBNX5Tx+Q/SUjDqVhsInoczYcWyvxEgod9rkbrk46wInf9JMbwjV03IXObG7xjpqQw7AY3f1UguykGR3EzR6Nzv7Wn/APTWaiQ4DwvHJt/IkqNgsGsdpBAFA3pkecsord1wPKCRPDDce69MGzt4w7tQZIUX3A/vF7T8YYRx9hMAzzEjVeDsqjI4RNCW7ktZGBhmMzeUAkfIuTmfnZ5ECzKgdMqQpNi89D5gMHQA5zyG/yLfl06inzOf7D+doBOJdcODLi0AYMthB9zoaT1hgvodBRDoKrKx1wObifPC5UQ0c5iEpKRCGHqB0VS+u1GR3E3Z/Iz3BWWXbVoXD5paF71SYxiGIn5bD7J1sO5Z66IPMph2o45BARjQoNkGItirAh6rAIkgrRiX9dDuTRKYSNBDdS2MhAAwMwtozFwT8c9htEHyf28FnlIGYNvVSgc/E5DkCGwRim+eQ3+bbBp3NqH6ZFeBqNwNPKiIsvIxuIgxaiz9hw0lqJV6HQiRyEK8iqRQ7qZwQm/SS1r4aOcmALcckpGDrIYdP9MasIsqvN6CCyyZiYa0Lz+3WAgUPJMCY2CwZR7KQc1hlO/Z6766vXH4+2KAcxHAbjY4S3AVIsRRE2PEzI07Mgm0VpCcl4OZBHJxA2OXbOQ+Y8E8/rQeTgSHp+VjmoWX36EmNXoXP4nHi4+eQ3fit0PvG05uddWigZiL88/yWMOJz5RxMtRJ+x4bQ1xosUOpGDcgXZAZUD/cTFFZUqaoChoxxIpY/JkVEkrgU33AKyYzM+iIk9yA7zYiuBQ0m1SQyi2In/sjwOq5FM4YazRy2QYi0KlzPcqe41xg2LMp7C5wAuI7jl/CcDDem8ww8/IUrnjQ7+MTkY16tePSt0Dr1Ml+aT3/itgrCqpzU/uQyxkIGLhLrw+KGwHtqIPsUQsrUaL1LoRA7CFZQO0Db9RG76Wal9DJ3JAVQixdBFV145LSoryK4044OIlrmeyuIaOHxWm8Qg0g7lgBxUlVW0Gc4eNkCK6kO6eeAbuMNI5W7y+nYTnHfAPxXCJsgqNJT9IAMNp28jLZKDsN0cvURoGmobF2gRjpJ8OvW05uNp/XBBT7eyw5URVw7cNRB9xoarrdV4kUIncjCuoMtB/IRBeFExZQwd5aBUIkYUUNiX8ORSWmczHEQdOKNJauDwWW0Sg0g7lAM+mhzc8JoSLZCi+ZB3fHhKkyThx7eeTg4cupYcyEADSOOsww//aP1FclBSlTJ0CJ2bJ4cGcMzofCIH5vOC7sHX86dtRJ9dhdtyIIWOchBcncvB/GzLgaGjHHi5VwwdkOFXgz0iIDs2UwbRV7mSXA5ikxjECeTQACnO8uHYJwdFxuUpujIsXlB4IjlwSjjBLfPIQNtkIww5+tuUg5o1hqdA02Sx5OQ3l4PR+XT54rBPBp9/mrVmG9GnV2FvjfEihY5yEJScy2GRn6T2ZehUDrzca0QRStC4pPXSDAdRBk7poeYQPqtNYhBHysF72gQpug+5Xjt5jR7v3PzE+xHtSeRACJvdSuf1RRho6ed+x6kc8J+8RzezvFDm1bNC06JgtkM5GPnN5EA+nXpa85MRG6neogojDm1jFrQQfcaG09YYL1LoRA7GFXQ5iJ9JJPzDrTRDRzmQiioRTTmghrRemuEgihyy+bDCW2kGLkuKTWIQx8uBhlGiCVI0H4SZELUexHOlieQgnLqEovFBa3wOHwsDLX635nysHlUOwyyJP8TxZAy9FrNk8UUXkSo0LQoG4S0ftDbIby4H8unU05pPLhyfJsYUqYy4KBLrv8sTodtA9CkbTlpjvNiaysG4gi4H8zMCE35Wah9DRzm0I4o1HKzW1kszHESVQ2goBnqvp1NkGjiWpE1iEEfLgYZRog1SNB+ifHwXz8TzQdcjj6H6RHIghM1fw+XrEzLQkuvmt9LJb7vp83DXzGYAeKdUoWkxqHwN1ya/UQ5K51NPS369IcY7InRAeH1oewGiz2BYbK3GS7Bbeu+A3LYcqp944wQ/hdpXQ0c5yM8/MKIpB2iltl6aKYOo0zjfsPH3vhm4LKI9IgZxtBxoGCXaIEX3Ae7nW9J8GDuZHAhh800aVySvjQw0PJSIZ0wPvQjeNuWARxt73PIFe02zLocKncNmjNykMYf8xmyl86mnzK/TDDsIztgSuw+E14fSu165ENGncmBrjJfLwXF1i+UQuedfAz9lk8afkEPB0KUcsG4sILvSjA4ipzG2P8gmDQaO695ik/cQ4+VAwyjRBimqD9xi8hyGB4u3lT7+s3o/DbvB+msxs2lEWnsCB8NTFIWYVloOu2zZ5fAfSasvh+EBwSofDo2FxcrRJLscVi1wk8gBN/Jxm7jKcljDhVut1OWwaoGbSA74keKODuipp07S6KmnLoeeeupy6KmnLoeeeloJOYylRPDQ5BZ7nb5mw9kpH/wZAwbP8jd4RIJJ7mLH+RKcaBgr6D046LrT/QF2bjXuv5CwtqelyIG7n7d9Jg4jjjBg8CyTA5Fg4+TA572uhraDoeaPvz5svS6H/2hajhx4Xjf2x4wwQHjWeCRY23Gd30o1W9wDeZ0jclh7f4Sop78th5Vlcv3z7XY5/FfTX8KOrdtAaiE1vk0oGTYYxnodGyb5ZR5LSRIPwVTYlzkfTEUkGHPTW9ig4ykHwZJho+VQ8YUzthzOU0QzQZyhbmp3wPt49Fzs806Tub8yf+6aRbMpctj2Oj1emaKDq7ZPq6df2TtjnLiCIIhaOLITO7AlB84t+QzOfRfLB+EGSMSk3IEckXACAiSuQW2pVjVNMx9tg1Yj1BWg1Xb/oUea2j87O/P+a7FjM6QWlaOGkumIgewwBA08+f4pgKlkhzmYykiwaAdUq6ZduOxgqpft8PuGLV99YW9th9AdZO/OGaAiN6m9/3gfdnCqWjCHjQCdXYl9E1lYJezYNlILURxwy1FCybCzEB+TGm0O6oy/SDwGU9EOm2CqgAQLhwr/oQacRQuF0w6u3nbArQdj/PYOZ8z+6/R+7g6ycR/A+xHBhrdwEdoZU9nCnsNmbNPP028fWsuqhB3bQmqJDJajpBuEObyDImqIxGMwleywCaYyEiy8i+s0ox8KT1Qv2YH9xGvdkAzJSN3RSUc3ycKFxxpTA7mAa7RwJq54L5Tg96kSdmyO1PK3yhwVlMx2CJdytsRBbTCV7TAHU2XKhWugUuGmetkO7KdGtpkxqTvifiQEm7LGVLYwdHnft54rrawSduzjlCHkAepohpIpJQSJjcwkHtlhfnwwM5AgjXIqFk474E+wAy/MdsjdUUZCsKm4MZWv3WXd+T5f91xpZZWwYycVOwhKxgPn2Q6ceHDEvLUdQuFvZQc3uW0Hc9h2z64BG+Wh50orq4AdmyO1Xo5ieJz/2I+2GMR8nPOJiR3KkyUXXrWDu8OM2GScLDmVr81hI64aW1R6rrS0StixKVKL2viiHTF6MYjtGGcYbIfYgcpIMIjXqelQeMUO7o4yEoKN/EbYwamyhjlsu+a/3ve60toqYcdmSK34fIUQ5VoqViQBJcNY0ch2kFwogk8OtoORYGmhFTwqfIyHwit2cHeUEVhpv/5gEVgLrU4VJM4cNi6yXva60toqYcdOnkdqhafvpCg5VIZ3PfkZjqMdI+pgO2QkGKVquVvVhdfs4O4oIyPY9DOcU/WvzGHzswoAF+p7xKIqYccM8Jps0rj4m6Lh6Wf4wCT2fwhqmBTskJBgkKolEcyFF+3g7ijDTeo5adqk4VQ1P3LYdHXbYWU9scPa++SOq9ydeqr83nOlxdV2OIIdTPfudaW11XY4hh14a+BXjdbSajscxw56nmZrbTU6oNVqO7RabYdWq+3QarUdWo/snc2rz0EUxl0rr5GFUMhbXkpeFvJSklCKJIWFlRWFIlFkJysbJQsbC4lSFhbKSigLS9kpGzb+AGXpM4+HM3N/DV+iDHMWrjt3Zs787n2emXPm5ZwunQ5duvwJOuzaOSbxNYNCJh6/8Av61q0d27zlN7Zafmhs94Lvbn6uGFu4ZKiexbcGB13yZQtd0RhbdW2BszB3aVoG0QHh6mn5o0UnZp/51+iwcceeoQdlfkOtOBq+hcgl2n7q3LgMpYMvZYYc2D//X6PD9m1j0GH44sDlP67TEl3m3Ckneu3Hzo3Lj+gADJ10duasv5EOzMn7BpwZ/346cHcbWwmycTOXX8bYvvTY9a9Ldtflt9PB712mTbehPHbw8WSQg0AI5e4N18KJflUiED/hu/QERm2JoPfBwC5r+vI3HX+3Veii0qf3K5memaSBIBmnI4Fw0bVXB+W5Jr7eCsZcaoi2FCN7lvlD8wVywPo1x+YA/K8DDBryWEl0OKOnGtCBoh5ir20ZSAcMZf7svHRxvvdvdCADfJhSJo5fxiR8TXvm/PBf275e6R6jZgrRPfXwDAfeq7fKdVHp3n0mZuJ9vVmmB0B+6zO+a9PBJfNfmA6hgbaODVijw90ZTAUxwFiwditHO4P48m9S1TMtti3D6OD5b8okXEasBCZBG0vAlaeZvA2LuAFziJTBy2HwAWJ4Q4mzSX1glwLY8Wgo9eiagCuFlJQJDpaBO9NwtVWui0oMZRO+w8fLzMqJIrSg9UjXooNoRacnZ0CHQgNtiSxJXG6AbGNpHB1S5I8rC7IBSrwg0FK+A+yXAdmtpbZlKB1wWfnrhyFvOuBPAlrgZ5hYZExDB0Dm+rw6AyqpJt/Yx40HpfyUmhjf4LfaKtelSjFKIBswLLsWHaii+oBfdAgNX70GKlXogNJygBNiudRu1Biy6rx/ST1SRtPyk3TAhH63I1kUpoPfN5c4WHTrEZb7bqwPJnLDDiACOAEqVocUkev6au3SUDOp40u1Va4rg7JAPhbR7oquTQertamfaVAfR9bfnEN5hQ587nKAX1kmOhD4IPah0YIB1aVd+SljSW4nEqsDaLEIB/Z9k1BdeHMnApeXlsJ30DNnSiWJaNVWua6CDrb/Fep+pGvRwfux3g/LNcjpRlKlOh2KARb7tzaicENUq9OhbfkpVxpDeezg9RtrK3Tw/iM7Oncu7v8xHRISHe795+lQAs80DUc3uv4BHRbvxeG+/bJqLKlBlQ4sTDaiMKk6HdqXn9lo9QZK7jvYoQwxPg6MpwNQK40lRHbK1jEazJsbpdVWoWsUeBhxD5b6bKTsetRYKjQIyKXvYA4ypqBDPsAwllzXY+7GUvsyjA7MvqBGoDE2jRNwAJrMkwAzBePoAHxyVzpEnfLPt0m22irXNUIHqx7tOnelYfV4Ooi3kHiUDgwm6JAPMDuGLFcHOVh9Z6ll+ZlLGiCL7XmSn+BPAgenRyDZCEYUJW6BGa2dUfu7hh2IKTdandWEnRnassqkrc+92mittcp1iQ5lUhbyoKBzpGvRIZEIG5+91fF0AMEblrEBi2Zjni/sS+ljBh3yASYx9N3vohOJoqZIl4ZlEB0cgxrfwWdZCvylCPFsuSOx0em4c69ejJvnfdDmA7WIZOe22U24eqvQVdJBlZybaqTr4hiOLFujvkOSSzO+nKbJvNJH4Jgvp0MMMK4sMQIdDfoEsB/DtS+D6MAu5ml9T6ZC8ibKXGGHMa0DujihXIoWlZ99noyIHHa+bnH1RmbRbHq4tLykoRyD9Vahq6QDCQmPzvAljeh69JIGCa9GfIdUjt6nsnb4P2tF+gjofBuudD7AsMRYehSSfIfL06LU44q1Lf/X8x+48bsCQmI+ZUuBF4x+w7tx+U/owMSd0qP7NsVvEcCPv1ASpNtKjctn9s5Yt4kgDMKIlsJtJAJJCw0lChJFOkAUPABSIkqooEIRz0CFhJQH4AlAdDR5Mcb/jZkbzj5pLUvxyjOFo9jevbX1j+/fvX+/OxA7YD6gOdBuhDIoreuy0uRplln71oHYAan/dRVu/NhZNoOFharj9f+jrnUodoii2CGKYocoih2iKHaIothhBxfScJ/PO1G0JYXPdfatfZm9HSfTTk3yLaozl+ZQgaQe52h8G4pVdQtrVv2efK37MeamV71qawrfsoL0Zdd2OF6w7s57dBrfvB10e1xwClhFuKywzQWITtVI4fNg7dsO3qPvk2iwwzHwY1Uxe/ICZa+oWsrl6X7VQuHbAzuU9soOqBBHroTX8QVp81OKl/pUE4VPdDvukoYhbGcxAunZzZcl7m4ozH4+hfPZXrXPv8jCg85+PkIRBaq3nb1XIXbz+wjV2QreMXZPVd7XQ/LOo2EQtIMAejYMxi571Asr/JiofyM7vH238OMWa4asWO4hDZ2yW7WgAwyxt8EO5NudfD8ab+W3lrLD6R+96/JCJXbG3itmHtM1Bq9j97QHiOA9Hq26RwwLoGd4PreDxic7iPpHOyxHSSKBs2q5PZSP+DJCp+xTTWCZMWJvlSz9bwdMJmsP5quHlxfKOaxlqd6L5y4vKt7wu4rZ+tkHgiK5JZR2QJ+IaQQhg3eC3asBYofoXbRBQIu5h0GPCH+G5zM72PgqWTLCIO0A08BIGAqaq4wVzStnqrnDcvjoINvi+lQbdkwp+SY7IOgqNBFCJA1Qaik7EAuA4BrilD+yYu/RDgTooWuCAIjdk+gQggXgLTbB4cYAPeH5PFmy8dEORhjk5z9eoHfteqvj0vPYL80FuNUuo6g/NdpBiL0NdkDoMZE2O1jLEl+mA4aGJFMYe49Ry0Cv4J1i94j0WPU2NCTPYwzQE57P7WDjox2MMIjHclaFuaKdn5v7ZqHhuh4OE4ZAl2pKloTY23h2wOM6O1hLt0NF5dCcuYcjyuqRv9y0wxS7x0MSiDT8wyZGDBOez+xg4+OncKRa2QEvUIh2+4aURmGJNUiNbtUylRZir9UOarkLOxh2r8kOYzyf2UHja7eDzl3DyS126FctC61C7MkOYtbN2cFbljjPWJcsGXuPgGFLlhy7N02W8I+SJQH0HM9ndtD4ZAcjDDJZ0ifyZKmgfLR4kqV+1ULhM8Se2wFhOGcHtTQ7IClfN5U29h6awjY+lbZO10ylMSZNpR2gpxOT2UHjkx2MMFh2gF10DijxvX52qD5TptGjGoo0HLHHaBGzbs4O3lJ2wEIMF1pBNNNCqxH7cBwsodpCq2P3mI1pobXCWgutAugZns/sYOMbDGSEQdoBZxoM7FwLrWxebsPcASPid/I6N03sUQ0UPkfs1W8v/opZN2MHa+mX4dg1AlnHQexPL8Mh3Bh9jt2jKvj5Ll5A42U4AfQMz2d2sPGRxjei/tEOVqinKvFPfhUwl+H6VQOFzxF7tbSIdRQx6+bs4C2VseDZx1eTIg1j79WNDd8sHrz/qB9jYffmijSuzlmkQYCe4/nMDjY+0vhE/fMiDbwPB1HGhlMFD89XcKrIXYD61K1t/5Fb5sWo3Vvdu68zgc4YqfDuU7HD7uCUckhypU4VO2wrFSdWHZbhKk+fpKC1S8UOO+BdqsBVT0Q96pDRAVEUO0RR7BBFsUMUxQ5RFDtEUewQRbFDFMUOUTQodoiif4odouhv+3RIAAAMAzDs/lWflQ4NLfGQ6ADRAaIDRAeIDpDtDnDaAwBg8gGyIvCMzv3Z9QAAAABJRU5ErkJggg==';

        $data = base64_decode($data);
        return $data;
    }

    public static function generateLabelByPostal(Postal $postal, $withCn22 = false, $countyOfOrgin = false, $forceValueCurrency = false, $rescaleForZebra = false, $ownPdf = NULL, $forceDDUMode = false, $pdfRotate = false)
    {
        $model = new self;

        $model->receiverAddressData = $postal->receiverAddressData;
        $model->senderAddressData = $postal->senderAddressData;

        $weight = round($postal->weight / 1000,3);

        $model->weight = $weight < 0.001 ? 0.001 : $weight;
        $model->size = $postal->getSizeClassName();
        $model->content = $postal->content;
        $model->user_id = $postal->user_id;
        $model->type_name = $postal->getPostalTypeName();
        $model->local_id = $postal->local_id;

        $model->value = $postal->value;
        $model->value_currency = $postal->value_currency;

        $model->note = $postal->note1;

        $postal->postalLabel->track_id;


        $senderName = 'KAAB Joanna Bokhorst';
        $fromCountry = 'CZ';

        if($postal->postalOperator->uniq_id == 4)
            $model->dduMode = true;

        if($forceDDUMode === 1 OR ($forceDDUMode !== 2 && $model->dduMode))
        {
            $postCode = self::POST_CODE_OFFICE_T1_DDU;
            $city = self::CITY_T1_DDU;
            $street = self::STREET_T1_DDU;
//            $locationNumber = self::LOCATION_NUMBER_T1_DDU;
//            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_T1_DDU;
        }
        else if($model->receiverAddressData->country_id != CountryList::COUNTRY_CZ)
        {
            $postCode = self::POST_CODE_OFFICE_INT;
            $city = self::CITY_INT;
            $street = self::STREET_INT;
//            $locationNumber = self::LOCATION_NUMBER_INT;
//            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_INT;
        }
        else
        {
            $postCode = self::POST_CODE_OFFICE_DOM;
            $city = self::CITY_DOM;
            $street = self::STREET_DOM;
//            $locationNumber = self::LOCATION_NUMBER_DOM;
//            $senderCountry = 'CZ';
            $senderPostCode = self::POST_CODE_SENDER_DOM;
        }

        $response = [];

        $temp = $model->generateLabel($postal->postalLabel->track_id, $street, $senderPostCode, $city, $postCode, $ownPdf, $ownPdf ? false : true, $rescaleForZebra, $postal->user_id, $postal->note1, $pdfRotate);

        if($ownPdf)
            $ownPdf = $temp;
        else
            $response[] = $temp;

        if($withCn22) {
            $temp = self::generateCn22($postal, $countyOfOrgin, $ownPdf, true, $forceValueCurrency);
            if($ownPdf)
                $ownPdf = $temp;
            else
                $response[] = $temp;
        }

        if($ownPdf)
            return $ownPdf;
        else
            return $response;
    }


    public static function generateCn22(Postal $item, $countyOfOrgin = false, TcPdf $ownPdf = NULL, $asImg = false, $forceValueCurrency = false, $rescaleForZebra = false)
    {
        if($ownPdf === NULL OR $asImg) {
            $pdf = PDFGenerator::initialize();
            $pdf->setFontSubsetting(true);
        } else {
            $pdf = $ownPdf;
        }

        $pdf->AddPage('H',array(100,150), true);

        $pageW = 100;
        $pageH = 150;

        $xBase = 1;
        $yBase = 1;

        $blockWidth = $pageW - (2 * $xBase);


        $margins = 0;

        $weightKg = round($item->weight / 1000,3);

        $weightKg = $weightKg < 0.001 ? 0.001 : $weightKg;

        $pdf->Image('@' . self::_bgCn22(), $xBase + $margins, $yBase + $margins, $blockWidth, 130, '', 'N', false);

        $border = 0;

        $pdf->SetFont('freesans', 'B', 12);
        $pdf->MultiCell(10, 10, 'X', $border, 'L', false, 1, 29, 33,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );


        $pdf->SetFont('freesans', '', 10);

        // Quantity:
        $pdf->MultiCell(25, 5, 1, $border, 'L', false, 1, 4, 53,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        // Description
        $pdf->MultiCell(25, 20, $item->content ? $item->content : 'gift', $border, 'L', false, 1, 4, 57,
            true,
            0,
            false,
            true,
            20,
            'T',
            true
        );

        // Weight
        $pdf->MultiCell(10, 6, $weightKg, $border, 'C', false, 1, 35, 53,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        // Value
        $pdf->MultiCell(17, 6, $item->value ? $item->value.' '.($forceValueCurrency ? $forceValueCurrency : $item->value_currency) : '20 EUR', $border, 'C', false, 1, 65, 53,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        if($countyOfOrgin)
            $pdf->MultiCell(10, 6, $countyOfOrgin, $border, 'C', false, 1, 82, 53,
                true,
                0,
                false,
                true,
                6,
                'T',
                true
            );


        // Total weight
        $pdf->MultiCell(10, 6, $weightKg, $border, 'C', false, 1, 35, 90,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        // Total value
        $pdf->MultiCell(17, 6,  $item->value ? $item->value.' '.($forceValueCurrency ? $forceValueCurrency : $item->value_currency) : '20 EUR', $border, 'C', false, 1, 80, 90,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );


        // Date
        $pdf->MultiCell(17, 6,  substr($item->date_entered,0,10), $border, 'C', false, 1, 50, 125,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        if($asImg){
            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            $im->readImageBlob($pdf->Output('CN22.pdf', 'S'));
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            if($rescaleForZebra)
            {
//                $im->borderImage('white', 20, 20);
                $im->scaleImage(1155,1733, true);
            }

//        $im->trimImage(0);
            $im->stripImage();
            return $im->getImageBlob();

        } else if($ownPdf === NULL) {
            return $pdf->Output('CN22.pdf', 'S');
        } else {
            return $pdf;
        }
    }

    public static function generateNonRegLabel(Postal $item, $ttNo, $countyOfOrgin = false, TcPdf $ownPdf = NULL, $asImg = false, $forceValueCurrency = false)
    {

        if(in_array($item->user_id,  [812, 1289]))
        {
            $countyOfOrgin = 'CN';
            $forceValueCurrency = 'USD';
        }


        if($ownPdf === NULL OR $asImg) {
            $pdf = PDFGenerator::initialize();
            $pdf->setFontSubsetting(true);
        } else {
            $pdf = $ownPdf;
        }

        $pdf->AddPage('H',array(100,150), true);

        $pageW = 100;
        $pageH = 150;

        $xBase = 1;
        $yBase = 1;

        $blockWidth = $pageW - (2 * $xBase);


        $margins = 0;

        $weightKg = round($item->weight / 1000,3);

        $weightKg = $weightKg < 0.001 ? 0.001 : $weightKg;

        $pdf->Image('@' . self::_getNonRegBg(), $xBase + $margins, $yBase + $margins, $blockWidth, 130, '', 'N', false);

        $border = 0;

        $pdf->SetFont('freesans', 'B', 12);
        $pdf->MultiCell(10, 10, 'X', $border, 'L', false, 1, 64, 77.5,
            true,
            0,
            false,
            true,
            10,
            'T',
            true
        );


        $pdf->SetFont('freesans', '', 10);


        // Description
        $pdf->MultiCell(27, 5, $item->content ? '1: '.$item->content : '1: gift', $border, 'L', false, 1, 4, 92,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );




        if($countyOfOrgin)
            $pdf->MultiCell(10, 6, $countyOfOrgin, $border, 'C', false, 1, 82, 92,
                true,
                0,
                false,
                true,
                6,
                'T',
                true
            );

        // weight
        $pdf->MultiCell(15, 6, $weightKg.' kg', $border, 'C', false, 1, 80, 13.5,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        // weight
        $pdf->MultiCell(10, 6, $weightKg, $border, 'C', false, 1, 35, 92,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        // Total weight
        $pdf->MultiCell(10, 6, $weightKg, $border, 'C', false, 1, 35, 98,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        // value
        $pdf->MultiCell(17, 6,  $item->value ? $item->value.' '.($forceValueCurrency ? $forceValueCurrency : $item->value_currency) : '20 EUR', $border, 'C', false, 1, 65, 92,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        // Total value
        $pdf->MultiCell(17, 6,  $item->value ? $item->value.' '.($forceValueCurrency ? $forceValueCurrency : $item->value_currency) : '20 EUR', $border, 'C', false, 1, 75, 98,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );

        // Date
        $pdf->MultiCell(17, 6,  substr($item->date_entered,0,10), $border, 'C', false, 1, 33, 111,
            true,
            0,
            false,
            true,
            6,
            'T',
            true
        );


        /* @var $pdf TCPDF */
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );



        $text = $item->senderAddressData->name . "\r\n";
        $text .= $item->senderAddressData->company ? $item->senderAddressData->company . "\r\n" : '';
        $text .= $item->senderAddressData->address_line_1 . "\r\n";
        $text .= $item->senderAddressData->address_line_2 != ''  ? $item->senderAddressData->address_line_2 . "\r\n" : '';
        $text .= $item->senderAddressData->zip_code . " " . $item->senderAddressData->city . "\r\n";
        $text .= $item->senderAddressData->country0->name.' ('.$item->senderAddressData->country0->code.')';
        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(40, 14, $text, $border, 'L', false, 1, 8, 23,
            true,
            0,
            false,
            true,
            15,
            'T',
            true
        );




        $text = $item->receiverAddressData->name . "\r\n";
        $text .= $item->receiverAddressData->company ? $item->receiverAddressData->company . "\r\n" : '';


        $text .= $item->receiverAddressData->address_line_1 . "\r\n";
        $text .= $item->receiverAddressData->address_line_2 != ''  ? $item->receiverAddressData->address_line_2 . "\r\n" : '';
        $text .= $item->receiverAddressData->zip_code . " " . $item->receiverAddressData->city . "\r\n";
        $text .= $item->receiverAddressData->country0->name.' ('.$item->receiverAddressData->country0->code.')';
        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(90, 25, $text, $border, 'L', false, 1, 8, 40,
            true,
            0,
            false,
            true,
            25,
            'B',
            true
        );

        $pdf->write1DBarcode($ttNo, 'C128', 53, 21, 44, 15, 0.4, $barcodeStyle, 'N');

        if($asImg){
            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            $im->readImageBlob($pdf->Output('CN22.pdf', 'S'));
            $im->setImageFormat('png8');

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
//        $im->trimImage(0);
            $im->stripImage();
            return $im->getImageBlob();

        } else if($ownPdf === NULL) {
            return $pdf->Output('CN22.pdf', 'S');
        } else {
            return $pdf;
        }
    }

    protected static function _getNonRegBg()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAABNoAAAToCAMAAAD6513eAAADAFBMVEX///8AAAAiHiD///v///XZ2dkAABgAAFX//dcYAACd2fdEAAD//+Pm//+dRhnZ/f/ZnVb//+wAAEIARp0AACP92Z1GndcAADPx//88AQBereIAACuLzPEAAGet5vvMi0Aji8wAXq2tXiPwvXrmrWj29/ZeAACLIwD/+8z/5q12GQAZdr0AGXlfGAD/8b0iAAC98f3z//8AGF+9djMAFmn7zIvt7e12veusra3k4+MAI4vM+/8BAEv/89iMXisYXo3a8/1rMBEQTX12QBnxzI9rk6cAQHfZ2b+LQBlmFQL/9Mr95smvnHWhzteKrLEAMm1OfZyTazq82fFramrMi02t5PlAi8nN9P1ireCedkWsk2sAQI7Oz811na6szeYtKSc1MzMYABnCsIyAUCPZzqLM5vmtYix2TCQZYq1APz+IZTuZiGh0dHTz2KTDxMT95K1AdptOZnJTFABQKxBbPx+52Nkwa5IAFFWsi13mzbJ2n8KWeVFdi6dkTTEOPV1Adqf19vC5t7Xz2b7Dn3aDgYItT2mXtL8/WWoPQ24xBADZvayNjo14bFh6Z0cYADDUwZ2aj3kmRl6kgFIEK1AjPErMrp95QBloXUkAE0R3kJZhiJlNS0tejL3Yuopee4osWntzWTtFABwvIBLN5uilsMzp9/Y/aIPJ2dqtqot+WjKsvdksY4cZXqD95rp0QBj39uWHe2G7jF5AJBCtrZ/X8/1entMAQIBNBwC95vrj4cW9eUZLb4MBLV7z2ribmpmLudrf6tWjv8FebnQ0TFogLDSdXitdMhRAebqEnaqkmokjU3NUVVa+u6BJNx97NxOJrcOjoKXf8esKHC1tQhyt2fLZrXnAwLybrbBbKA/t790APIbmzaIANnnDxKpaXV8MKDw9ABZ4vePZ4uLPyrG8r5xlZGSxtLWJnJ9yMxOyyc1wf4utdkZkORjB4uPu68jJ5/mrrr5+fXidvdp2rddpRiUaFhIAOX9ORDQNDQ2bxM2vo4E/A1GNM2c7IVtaHmppT4oqWfrNAADILUlEQVR42uzUQQrCQBSD4biYo/QKhbn/xURnFOmibyW8hP/baSu8BIwAAAAAAAAAAABuPAAgBdMGIBDTBiAQ0wYgENMGINB12uSrQ4A/3tAgXc2oT+Ri2uR0Q4N0NaM+kYtpk9MNDdLVjPpELqZNTjc0SFcz6hO5mDY53dAgXc2oT+Ri2uR0Q4N0NaM+kYtpk9MNDdLVjPpELqZNTjc0SFcz6hO5mDY53dAgXc2oT+Ri2uR0Q4N0NaM+kYtpk9MNDdLVjPpELqZNTjc0SFcz6hO5mDY53dAgXc2oT+Ri2uR0Q4N0NaM+ketu2sZy/n6YejvH1DbHOHR5ZX1/vt77mvvb/fhYP5tjOfbTj9P4b8C02fT5ZO9cQpsIwji+Y5to8CCGwBpyzkUUIUhIIAcf+AI1epWITxCtCKnRgNKgB9uIVoxZaFWoL3whqNSDiD2olWDVVUtBW1GUVvCBih406MlvZr5dsztR8YHuyPxB3Zn978TOOj/m2+/bqPT/6jtoGzPIW53ZY5o2foA3/KUbcKph34ozHFB7O3YSUrh2UnNbxq9JzQJfgKD87ILG0/6bGtPq5vQmGGgrGspdx+HsOoKKLJJ4GSi0STOfSv+vvoe26QTVM00bv4qgYksZ2jZTy7n2MO8sXtWcFmiNroO2kTsk1srRNu8hRZttiBwCtC1WaFNo8+Q99YxYSONsaj8hjLL+uZL1A7O/hraHrT7Q+croWUCqqQtp41lLoDjBQlvD4UC+94Ev05QjxZNOC0MbD0jn8P4kvWDrnuV7FjnQtueED5TpmBxbAGjTT6iAVKHNg/fUK2o0CQlm7eaV14SUtZ9RQwspn9T+uQ5PSS3VRP09tG1ik7EOok/g1gLWe3tu+piFtpXz8gdY56U1kR1OC6INhP1M5wbS76fP6K9FG5IuvhjsgLZF/8EyUGiTZj6lU2OOEL8Rt5p7JxNS+Fm0zfYE2kZ7Dm2P537chGhr2AonuD7Pe/TGaRHRxlh4pvF0alYdtG1bp9Cm0Obde+oVUbSR/EKLU2ZAoe3XA9LM+UpqqR1t9rUHZmBACpmC0Qes7Ricdljqoq2hPXVAW/L0jBiQbu9ontpaG5DKvAwU2qSZT+kEaCtv1HuxNTIY+aDQ9htphEjpeG2OYBamEcCQ2sKtDGQOS120vV0z45g2tCr9qV4a4US0Jo3QM07iZaDQJs18SidAW7GbFPp5a2u4YH5FW9yXsWqsorg7wLSBgDYfGlFx8em2ffV25sRBk2Dd4BgPK7dwGDiM40eLDhQMkPQG2vyla8OajbZQ2w2tBm13BbShpS7amo60wY/ZvmeRiLZCV1RTaFNo8+o99YoAbbOr4dhCbOi9OQttyY7qTrpaDw5DY6iblF9y3MA275gLbc/NMPEb2aTFmoM5uPB6VgyVMmvpGWNthrXmvNbb+uBDdOOVhaoLy7sJ0Udhe/7Y/KbzECNHrgMBBAe2q+yzW7wQkDLiYkC6N9eZhWa9gDS11GER0VZTtjaj3xmQPm7ZswOmXwWkCm1evadeEUXbrgpGpI8rsf0W2nw53CT4jePQbB+b6uKPt5t1wxWQFrqZMVQa5qOY1oUvNaf6ctaZBwxtZ4OEX9rZy0nVxJtEb2Pt+Rfzo3ZyQxaaosNud5IpXkgj1PLp9prgZUQbyy8cQhjPb8Y0Alrqom3OHbvuzZVGGDq84ma/ptIICm1evadeEUXbyRxGpLvDxWFE2zYzEBx1LZE4nyPBLsqr6f4nUUayQLrVhTYSMp4n+sxAqEQdQyYJQnt9R3Ok5AxKRyqkkzl3kp6XDG0UaonEbpPwyohTG0nZSNzraAnksxxtsGu8ldjV0ow7RcEBQ/YYbATiNbRpTc3pVkQbUOyOVfxxOrLDZRHR1rD1CN+T7T5yyJ0hnTMzdVmhTaHNs/fUK2Jo2xqO7Wc403sbKdoQZcOsVmGy3kbP5Uj6Kit88xtRF9oiBnVeMbmjqTliJGnk2hLOH3A4gZZsdxZtCeu9HG28o2/Qb/QzQ76LrV6TFE5ytBV3cc7GICR1O3BIhrxB76FtG91f2SW77WG7ZHeTyyKi7dyaqa0chHcevXSgDXnIA1KUxMtAoU2a+ZRODG0jg7pBcfYith/RBtsj/TIvKj0b2sEzDKksK3yDPYMLbbEbuOTAQQfsuYrlvyEDTdgGHOFDNlIcpmjDDtgK0qU8UkFswqfAYqdoAwTyD6fgEh1DMJA1gufQps1Zk7pc70Urt0VE28p5xeP89qzRT4h1bcBDlkbAASfIuwwU2qSZT+nE0AabH4qa3WML/QxtmAjdcKFj7cGNhKNtzgDdWYGz56UbbZhfHZkeKgHABvwlH5dJClF0WefxmhyJLaBo05F9Sy7Sxf15XqqNX3n7bLCNoY0/4IMDCi7RMYcOifD7p2gbTNegbSDPudWwdWe59evr8X34erxgoa/HOy5tOIwUA8hN7plmvR5vda4e1Nsa1ym0KbR58p56RQxtWtNkiEjjkB+l7YKd5mQCtOG+CPKoI4PgEdDGDoBU/hL9ndhyVrydmkhphFVokxZSK3YAsuCIxp+2QiWbaPaB6Hg2T7dH+Jdo0zA56mpEoQRG/FIj0ZKEX7X92IHno1HW63D56FgqIFVo8+A99Yo42q4MALGeTQd2IdowQVo+eHA9DUjxoZuh4VO5+mgDpwtt6b+BNhz8O2hTX0Xp1WWg0CbNfEonhjbgE0SkLYFCP6INOsIR43kmScNIRNsQxJfD8KtfRFvUCjgNGriGDB/X9sSGbwak6U0UbRaYmgBdDGBd/MpMIpER0eZ2YECKDoU2CZeBQps08ymdONrgqXys6zVNNiLaGjEhSs9Y9Ng9NnarApkCAW2QsMNEw2U+ACYLukPX2RHqyoB9Bo5YGoEmBbCi5CrNylrP3vq6O6+LaBMdQxVvpBGklBd+AIU2aeZTOiHaICIdNRnwUoO2hyzyzJj0WRsW9OokDB4BbZEdSTi4j6nR3WEs8ag2671OZ03xh8GLP3gpx95BxrhtMAL7UN/RQGy/iDbR0XA4nDKS9BH9AFFok3AZKLRJM5/SCdEGmU8CeEG0sba/BBW7HSZPI6CVekS0kaDxKrG3m8Cuj9e36defJ3ZVK6TwwFWyy85cqF78WrJbpiW7AKyFWJ1WNqBdhWi4X0Cb4OD1bfneXfeWmUShTcZloNAmzXxKJ0QbxJ2EhpN2GmHEJEy6YZJiXMPCNfbWgPiszURr3PGiFSm/cnn77FewnmPewag4nE0bHe9oCWgTHNpt3g6OUs/aZFwGCm3SzKd0ajR5GnNoIylfpe1u8oQDajlAo3P5q3j7zs5W+2FZcbgO2p48roZr33H3dVQDgKvl71xWrCgJ9WZ9dkp1L1g71z74+vK7WWOYPzaFaNsZXCo4uDIHoV3ONo1NKbTJtwwU2qSZT/lEv6/C9afzy4PitPLK+qYOo+7/SgDVV2hCxb9ZbhVHp4U21uEaDg340daB6EBBM4kOUQptnl4GCm3SzOd/rfYwvFL1x4Rok0cKbZpMf4e/9tPFfajoj/tFSTOf/62ivu27c5FSVKHtv/lX9IW9s42Jowjj+A7bPb30g+HSBC983i9GYiRkc5sQtTUGa1Q0/WA0p4e1CaGQS6CtRCuEixRKgPYQEqBEQJqWEhJL6gdDimgBCViK1BpqVcD40mpTjSZqfYnGZ15ub+dm2b3qgdyx/0S9eW5mGY/bH/O8zOxG+B/IBLQ940dUXZXfOdlFpc3nmbGCHe0IZzxdtGXMt2gj/A9kBNq2opjGGhzsotLm88xYnT4DO5uupRqXUA2cPnLRJqXTHNYRbfQZPuGRG3Aosr1dVNp8npkrJdVxevbAhfSRizYpneawnmh7jB0/s7XwQwe7qHT5PF1lrly0Sek0h/8Bbbt35jzhYBeVLp+nq8yVizYpnebwP6BtD0WYaHfR5mojy0WblE5zWH+0yQf9BQ32dlFp83m6yly5aJPSaQ7rnkZYbKnyfHDC3i4qbT5PV5krF21SOs3h/yj+OHzOwS4qbT5PV5krF21SOs1h3dHmOTQx6GQXlTafp6vMVUajTdab620VlCVRMCy8MF5f3zweCGuyZKXFekNX5QxFG3U8FRu7nVy0ubp1uWhL9jZQS7LsFVLFzZMLu8yjyiquinjTzddYkDMUbY/Z2B3los3VrctFW7K3gZLloJIEtMmLyxb82xVMxJ/57fMu2izkos3VrctFW7K3ge6EtnoebJGeVfqdD/P84gCoumgT5aJtYyt2iJqqgNRVtlDxZtGu4WtsWGU02uqc0BY2Dw2et+n5jW7uusBdZFOgDe4FG7sgF20bWXX4qNvcRkkavelHqOvcKhvfObNgZ6fqblhlNNrUUPKhNnncoW9AXiXYNr4Z0JZdSh/ob20X5aJtA0s+2IQQqp7B54OAar9c5bgi3iza4REILtrWRWKGtMweV8uSobpdWU6qJ2wTL1yRkWjzV4sIo3YXbemuLfvR5xMRRYNHHns+uBZWJJtVm4u2jSJEJMTErNVsDFOAVY46rxn9d3F5hAxEm6YoquB4MrvrkKa5AG07dAn09U76wD6bWJuLto0i/n+g3p5VV40wW0lWMirTJaZmszUTV20bSWn/pdwIUoJBRY0Baz+qVRRdU6J3+SYVRSMMEx/0wpl1JQh2EW1kgL4RD3TLbLQF7FEVZN2+ZGRzZptmceFQJq7aNpJMnyefl9P5F/QuYwZDGn/XsU5UhlHncn0sccj1Y1IT56AoCeP4IbpkCAaLc9NID2ZWuZ6glJHi1M19CD+fT4udLY6zCI/SnSXVj5s8z2AZfjxfR6XGO6RLPWCGC1RaoG13k6eD9doHrzaMMhttQXtSxf7Y9GQlq/PsOxfmkhEu2tZK4uc517k19mCGLXsLHyI29uK9Fnz/Vs6QCGBM9J2RGrhhG7tVGkb09ElEP0zTQOHXnU0ItcM1Db0OBlBO4wxu3f4JYqp+jJ8DvDV2hxRXub/4AI1BXkJEnraZmBe3vfCYBOLmJr/2/DA2qlE8hdqJQfrjfGclKXbxFEjt3IeIfB1fwkT2CWgz8gVz5o5xc3QaUeWctVi17acPl5fkGm/+jLRhlNloc0iRajQnUJ+VvJplkZl1LtrWRuLnKZdX0dZYNxDtydsI2tiL0VKGn95EtM39jIhy276k22A/pxSDe3sHoO2PaWG//+vG0216MWK2x9HGzwHeKqb0YYxF7BC77BcQU37sOerww05YoO1FsJ1u9cemQH9c8e+SxC6eAo1UodrKQCB6AdZjqoVDarCq6ALqmrwc6PwZ+RqhaZin0dhUcyBQsw+1HxBjbS1bCwnxrtzwtF1MMpQbDID4sK0ClnCiQeEMEWOQaN1kaJN7kkDbosXirD6wGI4s1IvDQ4qLtvUWIqIwqsqdnFHC0X7v4cEEtMkHn+/rjihLrXmff0+cvt07dwwSp2/0Bjp8PQzvVOX2qYC2N6b+PEaGDX3UBGjLvuSrhDRhtP/5D0xoI2NnW73F32HEwMZZKpWfQwJ9juZ1VAG/CNrokK9rvDSDfLq06wI54I6bG0Wb3O+tnrysBMv3I7gkRpun7UTq0PbeNFCYkYssq7YA2qifehdBmMGq1q2+ScyI2e1QERI3t4CZ3E4XYFkmom1p2tOBR0XzMOKcpQZuZg0g0AqaMLHs0Sbk+WhQ4gxdj8dHvRUKeRHyTKFmVbK0biq0SfXOaFMTk6MlzbqBRqU+lEg9mXdIQa5DurZCRIQPl3JPquRuHWqf4dGG/9WLG9peH7Xu2QncIlVcXrYeK5/OfxajrWN+mPij307lQZei+V/J26fmx47H0UbHvlfq6cOIYcV8whx4+sgHC88OFTxO0caGzD1cfJyet9538I2T8IKfG6ANv1dNWAsLz9xD+Me1X/C9kjq0/ZEXe4p8FF7ZoK2OoBWkXjgy9Z1hlkP+LvIJaS3AMhFtWy6g4nPEHx3D3ZwUDg2gmDwrE6pBMj92hFmTGfINtOmdZxDTypRub3VGm6LwhQBqzMA3WFhU4sRiu+srESsRW7QpNNnJq55bhMlahVVaNeLG2tZPiIjy4fMD1KapiQ4pBNzOqvR7p3L4OF0KzCOS9z4/jNE2XAoLO7h1/uzBXW6f/vQa931naKNY2AlkMtAmzIGnz2m4bvkbfRzalh7+9QCh3m3HRueLv7dAG7yHp8V+HMwMrhmt2jGYMrS1bGWfk1R0CdWujjZ4G6cB2GfIzOxeDwZmz5fVNCEBbZSYvkk22lnNA8isHEokhjbUdc0aberNJvMYzcZqjzYuiDlauq3tAS5GyjW27I3FS7lbnMZ211liNs022LYo9igLCqXbiyV8DxWM4+ZVnpshXVshIgqCPsMqxNpe2pc7WXEZ8MTjA/4LcGFYAhOg7UXgDPZHPz8Fbbiqv2viehjfFyLaiuYBUXGHVOfnwNGHrMxgKQjkYw4pdWnJla68UPz9liHMWBFt2aUwHQZHGEau2X+kT00V2vrvuZsSAphmj7bt5D/iboS3Slh6wRpt791AtRelg/78bslR0TOYQlnnFwKBn0L4dVejGkcbdklFtLFhOeh6INCS1QRjJuyszmhjHywskvtOcDFSrgHfq1iI9WVocV+8/x9t0jd2aBsXFm09dZIonXdZA5Ik/2Qe49a1CVojtB0EEKyKNukUKVCo7FY5fACmGCAY5QBtR/OGsT86TLvUteA8QvvydwLamC9qpBE8w/wceLTJe8HflVsLj/FpBDLHPz45SdB3r4g2uEYhu3VgGPyf4GuOPpj/0IZB2yz+fLZNlixbOqQsNVq3H8fnnDR3BuPrusacKoSJdM0gGSkwUS3QphEG6uRuDPnhCpq19ZbQVlTqO3mCi5HyDRLjwIfld+YdvnPjoe2qHdoqEo90K9ElKykhLtoGlvPmnfMu2kStP9pA2tcVUEOx7dBxJ7RlX/r8e/BHj8W66NGKGpwN/W9oG/12R1hRojsPn4ijbVvjDM1YFHSDV1da0GCNtg8T0QbJisPHU4S2VpND6mlzdkhZDMowX9kPedOrikZcWxFtoK+350wuTedMOmcQBrxAoLDR1pEX5SItRrKuAUI6EW1zn6BcAJcxZqzB2noraFv62XdW5WOkfMP4Xr13X/5zAtq4vTj0pRbz5TXFiHDoOre1IzVocz6NsiwxPRqWrLXA9dL5opJmF21rK0REN+0z4kiqZkbbXswGRaeUGsp9JcEhBY7FPMbiOzDacOQL/NHvSReVnu4T7Hyz+mMBbaMJDik/Bx5tUN5BBPcFc0ij+7sqVXoZFg866eSQFj5Lr1nXf+Rk6tIIk2yG8MoGbWA/PEhp2JRzzDDvyfO0qWx1ZoE2ln+o8Rc3OJcOQ4Vgu9ltfWsfGBoMkqG3sUsqog3KV6qNYS1vM7tovQW0EbJxMVKuYUbYlfuKn7JC21etb4OXcFkySiN7MRl93RCTrO1WIfU+Nl4Df2kbJDDjykoPvEwd2uSf7Gt2x/lV3CqSz/N+rM7lFVy0CVojtB01Qvitf/XCt+jIE5IRs3/miz52/94F7gOXRihMSCO8CIz7APxR0mXLUCEFi3rwyCsWaQTwTYw0gjgHRh+6MkNUuSeNNMJcqe8sox7VjuMi2mBafbF4UxVgmLlLD+Z3p7j44z2n4o8WPy3+UC6gsXNmtPUpJLT1iVWsjQ6s/sWzokoOguoRlDulcsu4laysywbJJga8OEsqoK3lpjdeB1LuR9Uz1tbk0Xb159w2VYiRcg3DIW3B0QQRbV9N0/JG+Kmf+REW5LXl144MsHrK7BdWSJYDElHy017a90Bq0OZ80u4CB6mQpTsq1rF9w5/8oacj2vjVsca1WH4s7pqI72g2wxVF5xLpWgqLP4ZY4cXSJRyRf/SL4S9p2QZ8e4rm2aqhHCo6+OIPf6z44wYt/ngRcFcc/fsY7XLwjT5yFe1pQkq++GMI1lkc2rg5cGg7+i3UqoGW5gFPsQxpeRWeVfZQwYyCNQTrM7H4Y24+VvwxBMUfsWuW53XsS1nJbnsjLtn14pJdEW18yW4gUDHgz21jZopG32RzYBZv1oJ7WkAbu8sLe50hewYczm7+zgpr5kXaLF7FXRNjbZCi1SWmGoDY49bWpNHWfhPBClWIkXINI42Qe+iEBdq2DHkOdUdm+707Lm7Zn9N4VVnsx3+nXvNWN4bf2gsvIShxuDscLYXOP5wZ6w0qs5fgZUrQ5nz6xzIXNdslCbKs/S2R5W9MrfQ7QFyN1tAoENVo5z7Wii3J6X4itnfJ0xYLzOMl9vfCAGiZ8uPwqyabnYjqyuEH1fZqKS3ZhaAVzH/HRUgEbM899CO0+j19+DvpHesNK4GW0kIeH9KVUijZDZKS3UMnKNogoFICACJdikrh/1AJztbkFZ9LKNmFstzi37mSXY2fA8bQAeqqynuBjNQ9Bn7F0FYHhcAXpd15LEhd/sawBdrkVr9RsnvAQNvpUoSKU7vRKgf2T9mhTZq7wEKE3EarkWlqbQ994psU0caWY7XfOScRsD86KNrjJINyDoifKTzaeOkDCK0Eba3OaEO+xv4jhFhcjJRrGGiDjKkqoA2WdsWYwcH3IYCqKrjDM/AHEWfQ4XU5hC2yXyCJ+L2Qb6XRtzp4mUq0he090ki8EbG57jhf6ttsAmLaPdHq9BBdHRt1orEW41Grn96WxltGUAIyi8KAop+NPvFNSNXwqye3LPujl7KNVp1Vph1JcvnbtFV7gE6Lbae6yOFD2Gj1BqmS9QyzLsYOyepjwkYrsHEbrXKf4OcQf2vHFZIjYBHBEzG0ATgLzxqeM9wO+Q0C2sSNVpRoR7enAm2segOvl8pYZhJqbNsp2nbmNPL74IM9dHu8bjar0RCOJPVczr7g6bhoecpuFEfxHHUqD21Dqh3apCJAVPWkaoO2zq3gs1pap5Iv/qiuVIse9J0VYqRcgzmkwZHSwmdFtMHv8Evc2Isb2uw3OIWF0UZ+10d3YrQVwN9FZgiWV+yqQalFm2y3bGs2743X7Bw4/sSQABdqSxHa2N46qPgJBLU1RFv51ure8XC0BteQs9WOMjvkZXWKRTVexG5LeKu4NxLpzDP2Qb7xBDeARpiqewOBkSofhsDct7mTy8FZuqiC9Y2vMRgZqSo8lrrU0Czdyj5IW2xj+3G2xMSh2rbYInFPFY25GdvjJ9n2+C+G8Z4CmFSsy9w3dF+9sD2+q5LfHg9o4+cQf2vH7rfhDz1bJVZ/mH2p+rHYibbt16arf4/5PkdOmudmbI+f5bbHj93BhsKLFEkBqaaGcQiKcHqRcKgRe6XTN1XLZyOUVzkVtbFQfw6SbNEmzZ6hLikzWBbGrZxztDpnSOHrWXyOj5HyDSNdgGMWxsejW6Ht6H5EZKBtD0XbYwxtcmsVAqUObc7Hf5TUSWHx3DVRfE40YkqshuqSmEMkYK0Iz7VAD0Je4g8gtBxR1wht2aU55K/i0n3kgx6dJ+QpeoG0NHCKEKCNrTHIpkDtSc8wc7UKGhIHHM0j6zW53w8+F4QfPghjVmyvho7Q6lPxMhyqVlOANj7Lbt0y7l9x84zR0thNqrIu8Xuc7w7ShEONtMSfqjDp9LLMpsE/qnEpU4kA9OLmtsqhRmzoRjr+zFbaBSjaTQptPie0MZc0bhAZ1tVoY00ebTIEC47zMVK+EUdbLCtOk1VbhkwO6VewNoM/ZMu7rl9u3boa2t6pOlyxqzswlGK0SRW2yza5wqhXs1MJlxON47JCTmIOBFiiPOZO6gIaML/30VRkTdAGIU1fLwWV50XiPt1/kWUPSQscjzyGttEaIBT1mhjqAF/CALpHcm6+oAE6sPWdopPutDxoN1STuUdRZq40ZaGzKr9XSg3aqEvaaBhEhsGOKltr8rsRHoS1GRcj5RrUIQVFSymSiKNZfD0cvZH/mJFGGII0wiPbi68pkejQqqs2SHhdVd5quZQ6tDknSUOKpJXEcgp2KuP2IwRNzmkyaLsHWYhbnOs3f0G8tqEKdW3SCHR5UId/YXQDIw2c4OKdZ9quD+7ZaSQEddzzyhB0ZHnxxAHya3RJBiH92x6CDsA5PbYQOcqQ9sj2wmddtGWu8LlvuR0XkwqFoG1OaGMu6WUrtKkthGG65Gx1Rhvo62/zHzptjpG+xwVMjTQCqj1upNiIt9pH0sI32NlWsYOqOqpWQ9sPn5BhJUdSjDap2f5wyaCwYUqUHOJWbXEvVk4N2oIQqBX0UY++hnVtcw9i4mx50kOXZBRomsZexaTBuTw4Tci2eCcOALTdTwJES98C/+D7EMRljI3n6LeDfoGywXV10Za5OlWFujqCSfXMQx6HNILEXFKki2jTOgWGCdakt8ezPFftiDlGuosLmLLt8ah9gpKN8Ky/CUKwX9LX5Lt+mfg3TRB8Dd8Y+x2njFgwlcZbiUGG8LOn7fqpvLZ7U3sbqGX2Lum4UMUhSOO31UeM8t3k0OZ1QJuOmijMUE8FKIQGaHNKXTO0QVlCH0lwU+4woPGvSEIbiqhnWJDu1wPCgN07Cx6iXxLPi8C5FRpPHWtgewfoOBdtmSyVRTidtQTpmtpBfnBA4dHGXNKuRgFtesgPdsIwe2vyhxqp3CHvCQ1N0vmQq1i3yTwU1h2aGvmHhUnpx2IYVGxJ9W0QtD//Q66wrL1dtYREMXZehbSk5rBQkaAegrJtE8YBJU2UZAuxMHUzopblNUIbkM1ffI4sqWzRJh8EKnto7qloHlDFDyAr9DHIkNZMI4I2KAK5Fu6E+qzj1Fd1V22u4tIGhJLduYEpVMmhjbmkK5cT0LaImnBlnk4agnWzHSBuqNk+3KZW0L0JgqzPtKwzms3/7lbUCco+mlRZu2WAJBUqzJm/zl9IMiGQerSx4rWx65LkvGrTlEjnfs8H97LSD3HAHI041NbkPAEwo7tXTn1720Puqs2VoBo/t9EKdBNbEtGmwv2xDY1waFsc8ALvlulgwbrpno2QXHFbiS7L9fYpUrWE31W/HKsdufVbkS3SzOGBIPJisvGcVDsHiM+qrQXaRmu8ZFuhEGsT0EY3MvneJadXNAgDWMSh6+xgKUFbAbmqBg031uZK0Nw+/rRJadHYesX5n8EBMCNmMBjmWekWyEatm++xL3FpPXYngCiSHClhB384H/1RIcmMdIvJzsESWsH4Mah4DXc9sdtNsrTrTjHaWHl+LfExhQwpjzZVoWkhQqrTLxQftx6gKyo5swJgVvA74WUpxt9ROHKDZUgfctHmCqSSY9V+NG2Pwm1N3Fc1Asj7rSluUJAXbfvossTEWzfjE63ikvUSO580Ap/yN/QAXWsHMsuqrO2nf3crRomriQLmQ6tgDSf8cIXYkZpytM1N5x6iwVxa1zZolKnxaHumie4gvgLFvaz0QxzAQsi7gWN4ZC+p/ocCHlLXNuPWtbkya5E/inKgiRxyJKJNJY6N2UPlznnjrJvyYX18IsBO9aokL5ZRVjm4s6E6Fror0/7VrRhBXn6RNjuA29fEnlCvAwqnGm1FN8hmSqbR+dt6Sfki0IhHGzApv5JM8OGCx2nphzhgT1X+DFnYefrw9iX6uLbynTAA1wQPa7joG9IJLtpcEY0IB4gLZxgxl9SMtlPgyK70BOIKWls3J9ocHiXfo8BNeFWRrcgGJOP90TKCuOC/uhUVRPxMU+Sz821EjhoVtIhA7d0pRpv8JDzhSSHSYxtFw0s1sCWURxvLfkYC5dOeD04AqPLB2RQGZA9BbYiy1O8ngbhyP2RIIyOlMIDuIT0bCLbiPaQu2lyxaMwZ8bEvItoYA5lBhpAaJ1+ltXWTog0CZnYKNWuroGCc7xhk/ujCv7oVtZA/4ek7Ml7F+SYlUSp5pyPFaIPKaCa6NXT0hnFYB48247A9z6HvoBsp/RAH/DFNW8dY4pW0DsMAsnuU1m6fcNHmikmdHYgXsG+bmuAfhcB7mzFD0T7E67ZKS+umRRuwzVYllnCrq+B7nWdlcBWy/RzsUwiKeRmHaJZIVIsfr+dSjLZXm3i0wYFrW7nz2vZUGee1FXXiIssJehYQcVjFAXD6RrylwhFnUK39Xey8tv14uCq5aHNlSGsOleDvoKcDTSjCE5U5l5QZ5uCluD4TrUmjzXBaeKnUxOpsN5jsb4OrWfYKVQRljliyHEjcyRCkz/c7r/6rOfz0C9kdGpY4tNE9c6Kib+POSmrRpimG9LiJ+wVbnbKrxX7Z4gDd6ZRdF22uOOnBAIjfw6BgCxeUNgx6IFFBa2vSG60QiGwI5PT6F8OxB/blJluvpK0KgnVFm7z4D3tnGxpHEcbxmRu3EhSkUqgln+eLEMRSlluIYiviC2iU+yDIxT1fIFx7HKjRghrJ4TWNnK2JFaxdaKKhUfFDAn4qLYdvBPVEI8L5Vk9RL2rx7cBg9JPPvNzeTmaTvUticpfdv9jOzs5ddy+3v8wzzzzPY8cClMo0PnFiFbUQrQzh5RYcc03X8KZI6lFQ+jRngbcoBsiIaiOE8K5bECEkW3PsxaVF26llKUGRArPsCo1OLUPbYwgpG9mDRMacAO//pj0GyVQsWLZTzeTztWrah3wmX2krmWu6hqR0IfjxK0JbhLY1isRt3JA9H7EtAG0y7ftrB48/uT60wTt91S5oQ2Yith7l0DAAL0ObvgY9vurb92iEtghtGyZipbGqtEEiujWT1Ojfq5+6yl2CcdFmGQZHm2FoSy6WQU3Lk6eY3rz/qxELbYowV8CvuLWTrUAIoLFImr4G3YXQDd7uCG0R2jZMyUUMSuXLlmWV8ykMWkxGaAtCm9i4ydbWhp8dZBmHqETbR3MnH7r2rt3vvsiqiSKl3uglB698Z3DXkTM/cM/ZCPO4cWdcgDbvMTCctZItQ1Axliq3eg26C0GNg2NTuYUV3QgR2kJ5183KijEjNEeIu5wMx7Fo3taMQTp5uB+hmckuUbeIow3IdviUW8uqt99bbxS6u1/Bu6YvF7ufTvzRdmgjtGCvlWzZWNVq4Rp0FwIuaqTFWIkZ1jZ/RCWWw3jXLZBtKU+Vb/cSsO37CG3BboTD53jgIOxLf3P24X0jDG2cbMAwOAfJwQFb3nqj0H3gJcN8+ruJP42PBnbd2WYGKZeVWJM1iox0nKzxGrIyvsp/Y+7Joytv2Y3QFsa7bk40DWQrE6J+1YBtKYpCrmY8pCfuAIr1su3y9Inj04C28bm9p0V6mw+RqCbqrTcqvQtf94xmPjdNq83cCHUlW7VK7QVEjLy11muw7Hp8laa/n1kt0OrkxQhtYbzr5lTEGM8Tsnw5GXqLYZ+2BRuk5Rd7+qZ2Dux7XxSifIyXn+2elh5SWU3UW5TvkrtYHLUovXfynffbFG2I5DS4BcaYWmu+Bvr3K1jP4N7IKYr9UrM8y09kI7SF8a6bEgVz1PFhmAMmadinbU24Ecy7evtVtJ2v3HhhxEXbgzraRHThN6ye8pVPtinagFTZatNTtiJdZ8HARSVFm5Ca1MhcKalRhLYw3nVTWsAY53S0kTL0x1G41QzagGDSICV3Hb+TRyPc892Hnypo89QbraPNMuH/2Z4DV7Ur2kBWsRQLVrporvMaiqL6nnSD+s/OKiuloozQFsa7bkak6s7O9NlcJeQWaVMG6d7bhRshXncjoJm/es8paPPUGxVog0yFE58buWM9oxxtn7eXG8Eja75irx55lSXrvQZwIajxVaoMv9P0RTHRMyO0hfKumxEBgmWQn2oYL0ZoC3Qj7DqC0MyQ3PxBOdqggHzfPx60eeuNylnb74+IaPx+xCqQgsMhQFv3GJDhcr7ki7d0opik678Gy74Gy+IU/qqJEi/rKftiNeLQTaMhSx9GlSh3MyiUnQaNsQwp9021f1v0Up9wehjlHRGhrSVZGK9gCeT4Vu9QKzg8fsf4S0hu2YUmleHx5Nh3L3x6sLteTdRbb/SSgyIVPmzZhVe8z9hxzyMYIhpW1tb/hifUWChkKumUzZRKlxL5eNIkZP3XIF0Iek1RpI7gQ+JrK9ZnHoLPmvlsKBywknlC2nbCH9mwp06bEivPPoK9CYiGxDkpvdd/DNShlTpx8XtZuRRrpWkveR3vHWm8Ar4x8jsmr1COiNDWkpIMYMTXEOAZF0Kt4KRGxvJcN9Sw5JFbPFStN8q71SkCFV3+age0eRhHmDbyGmqMWydxPq6I1ya13HndshLLi4ElltUqLiA5qVbR9pj31g49zDu7x8948kzi0bdlIW1+7gicE9J6fce4G7d5Aq4b+BZI7OqpKTdNufCqy1ccqKNtv7xCOSJCW0vKYowt5CcrQluoUlFukjCXElPgJ2XHh2W/4nPasVAw2mAtAOZefxrG2FBX7zmPRZiEHN93eGsi/Nx99KfsB8f29J4Ss6TRi7nc7Jyb7PtoMjerJPtWerUjD6heNUC5Y3v4AgWUtbrDYPpmUnQI1/oLPRd+80WbMqJdf6btqdVnbUkUakVoY9oqtJ34XFlbU7ULZygKRhu4dPB5QTBzsKvvF++M60pGOk8h+ANZZlXexcuwPNgjqk8d6tn3D48mmaA85Teck1J79TEqqNgJfP4KjjaZpNdkp6QNunt64NJT/mjzjmjXn2l7ylxxj0ecz+dCrTCgjcB/Adp6tCEaxy7cuAPhvVzgj0vWneo9587fjoy4/Yce7p6mSOHUuDjBSx/f88z5R/lc7mPw+rDCenfohfXUXvVIRxuwc++9LtpEgRgxDwOv074RIOsZX7QpIyK0tSBiY1z1PZHAOIbCre2ONlJzakWngjZROtq68ArapYQgmPG/Me7i/RhXcsHF/utAgdLHUoZnAW7/jg+nkCLLQkxPXMbJYorB33BgPQe4kuWQ+5ErpVcfo6MNoCbRJqdxfXcLxgG07u/ZN+KLNmVEhLZAqQRbpEgXXWTMC7e2O9qQkYJYgjLaPOnXkI2vKAupMpOi391yEoy2nawCqK6zA6KslC5z8D7vC56FODrGI7E/e+cD3vSiaq96pKMNavsdONNYa0u+Obmne0Ke6T0FZf26p5XVOdDNgDZ1RIS2AOlRB4T42qM5FG5te7SRZCZT3qjkVYRs7XqfjjY58dEEC2J7b9J6paF64W2Pb+GzXpHZBfjEQeliS+vVxqigeo15MVQPKZs3ytLyv3BD+DfFpyp9uOqICG3Ni5AUximfb2UaYzuKId3eaKMLiXQqXfGNlFpIpzPEkwrEcazVXe1OJZlwsghRSkkboU2CRrVS9wBmfHX/nNe5cBaqIX/KN4wwbKkzMrXXd4wKqu7xT71o2zHON7rxyJQ33LLzOtrUERHaAqRNzwpa5o8i9MZDHoyw3dFmOTGhPPEtS2ojVwWltKifaEkU6SM21FduI7T5zNr+nWtsPrOU0IAxIBttzNkG2MRqvbM2od0PXaTI3fyRnf315EO07o249JwBAtP5jJ9Bqo6I0NZiFCnOU6L0FaCvGnayNbNl10S+wTp6qE3zNTAt/6YcYm0Y2igjWzpTtWN5FIS2mpMnQVdfciomajO07RzwhBxYVMzFPoPNsu55zCSoNDz78JUet+lrg4Js611re9ULT+lGAMNXQBRarom6b8TPjaCOiNDWiqiDMXYs0rBRLdaTHkZhV2CgFQvF8Tra6BgP1nnnVS3UBoKqdPNe8+aJ8J+VmuhLCP7ZfXRqY9BGiiI3LrIKZYKsYqKayRHoztWq1VqOCLRZ+XwBmXHoStTN1nI+X7YK1QQbjGg8U83MD8tWopCkeTZ5y39Ps4VEtZpPkq1Cm+4hveSv8YfETre+t5EP2maO7Tl8ijaiE2Bu933dF3rhN+H9FFFyeq/vGAVUCtrkcp8aioV3T/uhTR0Roa0lmazQy1KBykNaXIRj2wr9pK2p8Hh8/gbRIx4ONzxHM0kOP478pEPwDf8m3wMgVp83Bm0OzNmIWl05QVAmxmVxtA07zBAtia6UVbdNU7Y0Y420OJNFpsNb9rAYm4WXcxW3Fm2wAu9GB3z0MZtPEQAY69EM0i+Hukbf98ZdHT5KGzvY3lf2rOm96lEw2tDZG0UJ2x8+7j1lcB0ECmto00ZEaGtF5HsbgxarxXI2V6wuYpBtRGRbHW1fjRig2TnY0ylFJrsOv/u5kTw0iEfdeRuk+jgtsh+JqGd/CVPmAn9Ly7/Jnpq+07mxv3pPbQjaGIRq7i83IFUcODVvAKjyeUegjVYggS4FtDkJRq6CRBsM4SAz2MtStTSMtKA3XailbDlrM+ZjqWrVZu+xtWiDiCkZjfDWAL7wKLcEpxHy9Zp6fiHBMA8ALzm44w2T/3wFdfRe9SgIbdIQ3T3NXRoHpiSFey49paFNGxGhrRURYlaxqqoZ1bMKQtujrvfKnSHIx+HsQPcRD9o4+966HjajM8n1OcuNo3dXrT8+cJVprNwkUHuBimoLG4E2S8JK4sohCGhWKkOrbFCTG6QJUQk+aRA+x8u4szaCcjHgYIFBjXOxAEOrWYua9bU2KwmvYVM3a2vRhp7bj8+/84uRHBuC+Hh2uGM8a9Tlnd49vPe07KUIXfdZ97g8MkV86Ll48pgeQyp79aNgtM0MgGEMf7pG5k4I1bpWQ5s2IkJbgIxCXpGDvXLUk4WQTuFaQhugx23+yxZeVLR9eT3few5lrfCJ0hQYPJc9xf58+IUp90n5bnyya9fRt1dqQp7yEb4HoG9qI9BmMptStivAqHQ6xYxOYVfW6LxrUBKzUHEqKS/ahLegUAIOQg9kGq8UhWVaJBJtJJ5wSk4boA39OIiFwMBUEn9gmT1IUsPt7XsUkafd8K/em8SUjrW7VQ+l2qseBaMNfhNC1On9t/L4LBkIsW9ER9vyERHaVhWxFnErWgpnMGmwQZqcnXPjanY2AphnHoCvo2qQPvvFtMicI5fLhg8en4Y5RN/bihcBBD2+TTBN+VMh/1r/WlsKiCSbJY42kEOKtkCaQJuclcHZmIo2xNEm3oFN7mhCLrJJtNWgnU61A9qQMTvE06PBJ70y2m57xIs2MGMVtEEutkHmHqLIldKrH7lG63K07Tl8e2P94uRNk12jLgrP7t81oeVrI8tHRGhbVaSIW1MBhVHBbgSZ30t29T7uLpvBVE1xI4g8Xmf3X3nup+zYX7unof1Z3+d/gY3UWBXadeTt5AcDYHP4N9lEcQPRxh0GSWYUz5ehWSIyoMDMzjtgXTK01WxokCKwbBiVFLRxczaeAXzVbVVilYvMMhVoM+H0Aom3BdpAjV06puGVkiDXFRXeBSlznVl2LaTn03XblnIejvUsu8tHRGhbVcTBOJVpVimM06G0SIPRtmP8ne9RA22frIw2vPtOiu55+UM2fOzqA1exxZnUyxPU+2Cwg49u2Xu7f3Oj0WbYAKnCQtGJJXJsnmZZ8dJ8Ml00jBqwiq218f0hZcYySr1rbXY8V2LUyrLlOnOeDSo6ccOAziJDW41arA8V2wVt21gR2lSZS61UGZ0Hi9REIVSwQcq542eQ9varBmlydqC3nzx//Cji7ny2FEcmhetL1cyAIJfe3GCDFMGkSipBKrJVSMpGlqENMaA5ZThMpbwGqRyLSF42M0i2bItw52k5zY3YCG0+itD2/4nw4CqjpfoJ82GctgW5ERTxMlW07jvT3AjoiZcnXLRxT8CXA1gJ0jYM4W4Acvk2N9aNACLJqs2WxPImooUUr09lmAnW5SyguG2nCEratp1khmalmLIzpG6Q5tPpCt/fu+Cw0XGCsiUbYFZJIpRMMzImHeZUcGw7QttyRWj7H0VKGNstDE9h7ERoC0Ib7Amtb/54Xdv8IRKBSYP0EKs/SiaPv3tjY/8b+v2LC4xcPwK5/Jts8wfaqM0fUsQt1UBMyySyQdlfIPkXdA3zlmetzRW1hmXDNKl8J4N1mSbhr4nQtkwR2v5HDYM9mm8BbYWQWqRNo80wTJGU2t2y+6jolQYpaIyFTZ/92XUjwMxudOqJ78bPePNRw7nJ7mn/ptyyewjM3q17DCTa2vNRDOVDHqFNUbnFsgcGjC+j8KlZtO38rPtOLdBK9ALaGgWM3M0fR6Yg0GZvP5rxukif2y83hvg33UCriU+3Fm22nW7PRzGUD3mENkV5jGMEtaAYxjUUPq0aHt+now3RD9zweBdtB5Wyk3zLLnhVLxk8PsFxduIfxCVrk8pwe//mWzI8PkJbhLYIbb5KY5xoCW0ZjNMhjLxaPakRVQ1SPakR9PIOLlMdS2WP5wQfa63eNA2rwx+DCG0d83l2oOgSxgukRQt2yYzQ1unfona4gQhtHfN5dqDirNJLAKj0GjBxFDpFaEOddA1tcHfB6qDPswNVwrjaet2rUjRr6/RvUTvcQIS2jvk8O09sCrbQItpyYJGGL+tuhDbUSdew+XdneRZcTT2c1F8d83l2nnKt71Ijw0thLN0XoQ110jVs+t3NDLhpPAxwXoMj+5d6EpAhcJSfNpGvOuXz7DiRROuxBYTFLyRQ2BShDXXSNWz23Q0f21MP5/1hzt3QKPZuu8lefNQpn2fHicZkRGjrroewrbZFaEOddA2bfHc3D3VhQJtMUz56MZebndvx4RmRcPdoMjcrEu7q6pTPs+NUXlPUFLdII7R19reoHW5gu6DNZIkn62h7sGffacSDg/f9I0LmKE+7u0K0b4d8np0mwrbfruFlbJtvhLbO/ha1ww1sF7RBGfgTTo9E2z3PnH9UFLK49CZe3OoOT3ErXR3yeXaaiL2moCnCgrMitIGUhKl+TUu29FAFKoe5Q4Uo8pdyxk+WpSVfhabZ1o/BtkHb+MVXH7xVog3JGj3fcKg9B0iTJUn7kY865fPsNBl4Tb5OIkLqw6XV0PblszITvtrUojx/GHoGn3iInSNjQ6zSsjdPvoybv6ghTISpgv9tNX009wYS79slokzdf62NH4P/2Duz0KiBMI5PjPFARRTBln3Om32wSGigoq6ICtZVfBBlPaqCoGXBelSUisWjFV2tB9RVbFeLVemDog8iVvEAqVoF2QcPvKi3eD1U0QfBbyazMWnaRLuyZpLvD9Jp3N1kvnV+5svMfP+goE3TCGFos2pXUazZYSTvkCjxFE19LVCksUJI4ZIL2k4DTkAjW2xNpwny1A8SiNX3MKp+tM205jSGRrX0iLZJ7uV0T65hDrzyfNNKxjybj4dBUNAGcqBtaqaUhn4+r6g3YjmiLWflo6zkbbBTCFlG6oK2JdOj7xM1DUXf3lmaThNkMPBbl4K5stg7WAQ1clZJR7rcdJIEp4RWBRRPF8aa/xptL6orJYa2h0uLL5Yk6qGMm+Vs/h0GAUbbheUDbt1jrlnRArxry7N4MXDSB/Gi46FS72iTk6zirQYldC1NpwkyeBpABrr51OhZlICdtHZ4cdlvtK0jRnVwsMLSFVXTTXOlLNpURbfYH9kNLtsWs/cvq4zeY/XJ11nO5t9hEFy0TQWyNRO8a/tPkvtu4aJ9D51DgmtRI43Z6gKozKbTvQD+8UeP01z06DrA3ixmTDlwig1t3CSm/4qR19aMgjWe8plqWqtyO0Pb7Wpp1Db2kQ2rJdtDvP7JVe/4+3WdsFO0WM7m32EQWLSdXGOQDZ+1/R/Jr/puvHeY7qoPlTwXf5y8Gy2wNJ2eU3umRwv4j30MbYxw9oS0AxLSj/1nR47RZ27yPMMDuG2taXXa9pEMvSEZz9PGkqx0E438zq94ofVsvh0GAUWbvPJGZFtn9kt//JHPkE4hPUiUeIoleZGLXbK3Q8IivGuTrFOUXcULrE2nUyinzzKAjWH/IgPhHNMIVYdoghlNKRr5dLatMa7czMCLAW11qUT9DchWZ+y49UaJw0O0ucSUDW1Xkkebfp8N0ZYfmWijleGralXWZOva2nFdW76lQxgUuW9o64T3dpIwyQNtLzMjL1qbf4S2jQ60jV6VIoA2nrxoCstu4bdB7D/9/tDkSecFPq/gRNuVdPneTkTb/0MbkM2cOoKvbECrRn1mcTdCHrUzh2lOuiFhZ6hu21zRBilI1SHV1uwpIa0r4D96TUh17unHx0Z8ZcWcaomijZKM3+apHRUVc1ZLvaCtoysCZLOeDdGWD5loG5+J7FUMacYe0osl8STuIc2fZHkx0KnvXAzb8g83tEEK0tbYrdnrNMJ8l2kEu12pnKyUqOxo40vnekFbR2Zky3Hr2XAaIS8y0SZvKZS44Eab7R6l7UgTVv7Im/TvuSzgUMKWkbqgDXAWvdq9yUyQXRZ/nCDkxdviMje0Xaqsq5iTKjllQxv8qa2oaK+/2yPaXmSqjP0HuPgj32irrCrjhT9saIN6bXx/So8SJZ4iSb4MMwG5zUFcDtNtmwvalk2KpXgCYmk6TJDlFeVNb0rOFD1mS3ZbXt+cXd5K3NA243nra+XcrowNbf1XlNYm4icbCh1oY7eKpQfiRmJrng2X7OZF5p5gXTGlYZXdvIuv36iQcyoachjRZtyfDZaYIrMszR5MkKdOlkClvzdaxba7ou3TA/ZRi3db0cYTnoN3xv1wom1ZkWQoWpA9G260Co/88J36QDQfTeTAprgkfddJeNQ72kZ8kLI8szR7MkF+0WBuj7/ZfXv881bLrBqkMuaL3p8s2juWbo/n8w796ceuat9Ip2Ht7+eQ5WiDs+H2+HDJD9+pDwT56Fc5l4S2H2SkJDxySUgtCYil+bdFjXSrg4hmfZGi0D+q+Reaojg+G361JkM6FjX6773zlkDxFEm3wXKP5MK2p+HKSLEUJRHpGnzQO28JFE+BtPl7rtMAl9kO1NAI0UZEugYf9M5bAsVTGMny65yrSSpSqFzkEW1EpGvwQe+8JVA8BdKrnGuAq/1CtUUe0UZEugYf9M5bAsVTGKnq15ydW+QK6tlHwiJEGxHpGnzQO28JFE9hlEhACBI5JrX0M16TsAjRRkS6Bh/0zlsCxVMY7dwJ+agq56ZwZaSINiLSNfigd94SKJ7C6PBhmN5cnKu+Q0ZKwiJEGxHpGnzQO28JFE9h9PWrlLNCFkcvtGl/vFFQdSzk1V1MSF09SeGj2K+CDgNEmzDxFEaLFkn/Rt9JWOSOto6GIRL4f74jf6AjOyxVCW2bUPc3HifeGvQgttYsNwGVyjNVZYIOA0SbMPEURoi2f12vjVsXpP4EbYXd0baPow2qehFP8fq9vEgYog3R5qvvFCWc3NA2ozJyvV1J1KcH153oE9p2tyigeENRcdlfow0TUkSbj75TlHByc4/PRFoYXa6c2t9OFC7d8WBNV9Qs2jQTR6b9C/cv5U/TzKdqKm+b++MZ2jRF7442eKFowwDRJkw8UcGVq3v8Y34bpalkREZiGtBKd+ruWgNliWYSUP+VtNnO0Fa3azU8mJvoQNtmo6L4zfRgaf+14/SpWlWqevDo2nesWUbLHUVmUbRt7YJySSesCemFhkIodfROrGGAaBMmnqjgyqUU5b7yJmKKoo0bGvBqlNLBDUCtNGuO3EDRZrCvqXtCWtNQOW47TW8l0Ki9xwFiVXdoe1Mzr0VJva6g2UaPgokvoI0bnl74YMxDfBZqGCDahIknKrhyq7JbbrVWUUDx9NG994hRQzxx5kPsI20eqkk0VD4rALRFmt4kkoWx4c5pBEhsT4PX35t4fbqUuY9uep+o7wKe2dAm1b1PrOwa3WKiTU5CtfB4x6ndLUINA0SbMPFEBVceaLNpc/Lo3k5WFLdWASUHTuEeVqqiEkBbrJlaXRWXdUfbpkaV8jD6Gt60a3orQGxMis5SFEULbGgbN4UdrSvIom3EUvZCTRFrGCDahIknKrhyedY233StUjWDbM9vdTKPAymbnFJrUfsMKSWSLSF9Ur17W7PVR57ybAL9nBd3Y8NtaHtEn6k9mRYbnkXb1LuPPwo4DBBtwsQTFVy5oG2POY2Q/NlokK2ZWNE2oNUVbXwa4WG6/NY9C9rahnO0PXGgbS2iDdHmz+8UJZzcFn+c4os/nmQGNsKEAZCNZ6oDVymgRIkur2D0IoreO9rAg6r0Ik1IWxP0cV1JnELsDRw/U8QA9oOaixoJaTt3OO2WkKqKJtQwQLQJE09UcOW1ZBfsR+urpeg9INu2LwoVe2zWWBOvrx59KDujcOPgx97RBhZ+YN/3cGnxqvtKR/r5s7GDJg1oelOzsqv0EDiMRpoSNelCPo2QSqxcDkezaGOmo/Ga5PMDQg0DRJsw8UQFV64brRr4Rqu6q2AFauaTZLOx+GPAtmHZ5shal7s2eBFNSfmurf0LAWIH2TIPOCivL4RWpB/zJI2t4UfNxR9T+eKPdqGGAaJNmHiigiv37fE3wfMTTD/pKtpKE23AqjNr4PDtZkKbXdRUVM1ujze3fnJ7UaZlD0bXGkt2R6/aTmjq+b5LOljbyfap0mW+SYq2B7GtXdDutG6Pf5E8D6t474s1DBBtwsQTFVx5FTXSeVEjVcmK8F81s+yRxg/pfOeUw3cUDiqWH3TuwOI3Ci34DL79Su9e1EhXVNGGAaJNmHiigqu8l6LkaCOmgjcMEG3CxBMVXCHaiEjX4IPeeUugeKKCq/+CtgdViDZEmx/iiQqu/gfa7KXYgjcMEG3CxBMVXKHtCxHpGnzQO28JFE9UcIVoIyJdgw965y2B4okKrhBtRKRr8EHvvCVQPFHBFaKNiHQNPuidtwSKJyq4QrQRka7BB73zlkDxRAVXiDYi0jX4oHfeEiieqOAK0UZEuoZf7NqxTUMxFIZRu6BgkNRIFBFMQEdDG+kxAKKhYIgsQM0IWQMxAx09GyAhpYlk3ovS2H/OmcD3Fl/j28F08wbaJ7mkrYz0hg6mmzfQPsl1mDboUwFpI08BaSNPAWkjTwFpI0+Bc/4hBZA2IJO0AYGkDQgkbUAgaQMCSRsQSNqAQNIGBJI2IJC0AYGkDQgkbUAgaQMCSRsQSNqAQNIGBJI2IJC0AYGkDQgkbUAgaQMCSRsQSNqAQNIGBJI2IJC0AYGkDQgkbUAgaQMCSRsQaGHa1g9NmwLQmYVpu9vVhtf7AtCZpWl7qQ3X0gZ05+S0vUkb0J1j0vZV6+P25tBFAejMwrTdTtNV/avbx/e6APRt8fHHZjtNl+oGDOGIu7bV8099VzdgAK20qRswsFba/q3bp7oBXWulba5uu7qv26oAdKaVttm6Pe3r5q6NX/bun7WpKAzg8HmVFsStFIp07tyttJ8jayCuWQrSwc1VSiMIEap0kSyuEge3DBoEwSmDfwabKS066TewtUpje4cTs5xcnmfMB/hxb+457wvFyUtbdd2Oou82AlCi/07bzqDd6rloBRSpMm3ZXYsYD38kgMJUpi2/a+3BTgIoTWXasro20jWgVJVp0zVgseWnbbprwwNdAwqWmbaNQfvGZdfMDAfKlj1AXNeAxTHDKMr1YcRk2yhKoHwGiAM1ZO0LUEOW9QE1ZMUyUEPVaQNYaNIG1JC0ATUkbUANSRtQQ7Omrbn9c3J8fNzYblr3AhRrprRtHbRavfhtdRjfXbECCjVL2l5FL6aMbpx6cgOKlJ+2jU4/rhg/1DagRPlpOzwv2/Iw2pNGY9K+2CD/9K22AQXKTtv7fsTyuL279XcLaae1GdF9kwCKk5u2pV7E+ugkTWn29iPG3xJAaXLT1rl9Vrbd9I+ds7atPEkApcmd/NGL6H5KVzTPfzX5AyhOZto+fK58Puu8jvvPEkBhMtP28qxhp+mar3uxci8BFCYzbY/2o/siXfOlF+tunALFyUzb8814sJSuuRmxKm1AcaQNqKHcF9I70W1WvpCuSRtQnDk/I7zzGQEo0AyHP4YOfwALYu4ju2NHdoHiuGgF1NAs1+PX4iRN+eh6PFCo3LSlx/2I5dH0UKOeoUZAobLTtnG4dzGKsjVpNBpH0boVEV1jdoESGSAO1NA8a1/iNAGUaNZlfXcvl/UdbCWAIs26Ynnpz4rlwa53UaBYVWlzKxRYcNIG1JC0ATU0d9o2/OcGv9i7Y9emoiiAw+8gVYqbFDo4dy44BArd3bpkFepakELp0MmiY6mD0KEdJYurxFXEIRQkuHTJlkwiurtrYklEEG6S5eTyfVPyD/y477177yGdZdP2cn/QACSzXNparzpXew1AMnOkrfWxHxHPDv9esnXjkbQB6RSmbXYWYaM33Jou2SKkDUioOG2fOzE2OxP/PrqTf961AenMcV/b1MVgelj+4c03X0iBdErT9u5+xKh3fv50/yw2e1svLidlGx2sNQDpFKbtTqzfPogefjmLi++XZxFxEwNLNiCjwrR96sZGtJqx19dxEscRd0f9nQYgo8K0Pb6O3aNmohWx+TPiKnw/ALIqTdtxnOzNnk0jLnqWbEBaxWmbbWB7vj3eAOIeSiCvBdL2ZjsehLIBiS2Wtg+mIgCZLZa207cNQF4Lps1OXSAzaQMqJG1AhaQNqNAcW3YP2n901uN02J560gAkU562/zhxFSWQznJpc8sukFJp2rpWbcDqKExbq/0P79qAxEyPByokbUCFpA2okLQBFZI2oEKFadv5OhyahQCsivJ9bbtHDcBqKD+NcO82bWsR8aMBSGyetM1+WsABqUkbUCFpAyokbUCFpA2okLQBFZI2oELSBlRI2oAKSRtQIWkDKlSett1ee6Lf/f3TbAQgM3NIgQqZQwpUyBxSoELmkAIVMhsB+MVOHcgAAAAADPK3vsdXEA2pDRhSGzCkNmBIbcCQ2oAhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkNmBIbcCQ2oAhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRiKnTqQAQAAABjkb32PryBSGzCkNmBIbcCQ2oAhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkNmBIbcCQ2oAhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkttipAxkAAACAQf7W9/gKImBIbcCQ2oAhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkNiD27ZAnoSgM4/h5dLA5G6ORyTSGn4N6NzKFzRlsVse8JtzU5k6xOgx2pbhpoajBe9OVafMbqCTkEs5tZ+/+v/h8gP/e8hpE2gAYRNoAGETaABhE2gAYRNoAGETaABhE2gAYRNoAGETaABhE2gAYRNoAGETaABhUNW1J77vIsqzfS9oOACJVKW3dk8HAa6k501fNAUCUqqTtVl4r5lsLLjcAUQpPWzudak1+TNsAxCg8bedTSfWZhkW/Xwz1vCPp4oG2AYhQcNoep1I9H466bqk9SgcdaXLvACA6oWmreak1/1idEj+W8k8HALEJTVu6q9Z89H/b82M1zhwAxCYwbV0vTV7W1+RvTRwARCYwbU+vG++z9E6Hlw4AIhOYtpvfhi3K8/uBGvsOACITmLbTsSbX5fnNq8XHKYDoBKbtqqOjWnnelpqk7Ye9O8RpKIjCMPpGFMIKGhbABhAEgcdhniUpFoPpIhAYkpIgcCyhC0A1LAABEgVsAwEJFSWZl5p5f85ZxJdMZuZeoDnSBgSqPZDul8X5xgPpVNqA5mx5jfDsGgFo0IDHHyuPP4CR2PrJ7rsnu0BzfLQCAg35Hj8tn92aV9/jgUZVDzW6XZay87I+1OjRUCOgUdVpO7if/4yinH30ff9QZnullIUxu0CLDBAHAm2z9qV8dQAtGrqs7+JvWd/NUQfQpKErlie/K5afrpxFgWZtSptfocDISRsQSNqAQLV/SPu+P+4AxqEybafLcmJ6ETAWtWmbl11pA8ZC2oBA0gYEkjYgkLQBgaQNCDQgbdeTjfwlBZpTn7bp3eUmq7cOoDH1afvH4VkH0BhpAwI5kAKBXCMAgTz+AAJJGxBI2oBA0gYEkjYgkLQBgQwQBwJZ+wIEsqwPCCRtQCBpAwJJGxBI2oBA0gYEkjYgkLQBgaQNCCRtQCBp45udOpABAAAAGORvfY+vIIIhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkNmBIbcCQ2oAhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkNmBIbcCQ2oAhtUHs1IEMAAAAwCB/63t8BRFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkNmBIbcCQ2oAhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkNmBIbcCQ2oAhtQFDagNiz45pAAAAGIT5d71/EkgrgoegTxtAhbQBQdIGBEkbECRtQJBDCrRIGxAkbUCQtAFB0gYESRsQJG1AkLQxds3mtakgCOA7vNLPtGq0tjFt/aTaj7RNUArW2EBKEFN6CH60iDkI9iZ6Ea3UgwY8iFc9iaceivRmEf8DDx686MmT4F3Roxcz7G4mcUYJgTRpmN+lr7s7b2c27K/Z16coLYiqTVGUFuS/ams7DHC019SLZKrWkNAxgLNzhhGsbcYAoP3Oj5SpASr7wKH6VsKD+aT1roAGKkrr0Ti1hQq/R2oNkdUWfEmDJ/oyVV8x1F4JD1a1KYppFbV19sHwSI0hstra9nZBGW/f1VMMtVfCg1VtimJMq6jt1EkmhGpDZLUFCxGAzJMU5v30fhfA1raRaXwl/wpWtSmKqm2Off9pf++TDY5HILpsRJqgElWbojBUbbLa+ntgcqbsdAqwf4+RaIJKVG2KwlC1odq4uM5MV+Q6NQ4d3Uak8ZWo2hSFsfNqw5/hIRN6ngbIbKA+kt+Kl4mPKYO47mTBPesi7DOv6PrruB83PILDouuL04BgHJJ8tIK/JZZyxjh3YVd2M4bheacuH4LdzLnY721Hd7AEa5iHvxFrlPc7T8KTfZPGPK7nK9OqKDDnb0I3pDXB4FxlMI1hC8cy4YnIFWQXIwDzG71yzTRQmnRqnL4D9/fgGHeJi6wozU/1aruVtp1b2wFe0j8hXXfMtiWGhHcx5nNebXdX8ffJG+VqCxUi9M5GL+3lz7YZH6Exh3C1jY1C+KuRyJ4v5fFdbBTFwJNwvdjiI1lavsAZWW24dHQ/rjZ54XgmPBFegf9EMp/kmikzPike6Adm6Ww/MWgQ26oou4Cq1bb0wu/JK7fdnvGHQupGzg3Rk/wSiW47ruMZIBMLZULYdwmI9oN+L0c/dEFpyjhziNttRGgVv8DcZBUGuMU9py/LjVwMYhIYeQJbHNeuVqZFBQ4ytdG0ZCimNnnheCY8EVbBqxh9IlLNlJk4aX+PX+HOPlS1u8QrRdkFVKs2fMU/Z//u33uIl8HjFbBPs2w3ZH7G8VBqt4HbSOEHKYPnItxKblz4V9xkc/SQCV/asPfGEy9Gu71cZD5fvKJb2hA/4xHzF9a4iSV3AvNcvODOyXh77wDeyNUmJzE17rLFWqPLLC1fIFObmxb1axdvYJaCaQxfODETngjBPhFbH6+ZZuWTksXGRq24jfed8oe9M+fBKYjCcDS22Im1sEZiizUhxFJRUBEJjUJCKESvkVD4BTolf0CikYhKqRSlUiHCT3BmHrziPZeJiOST8xaWcWfmzB3ncWbumVGaAQ2ijVABh2rxBq7SYiweI4sMF6Fw0XUWrBS26v25bz4on15+kLZpPZwItNEN6ItCVSF0MrRpsdfoAd5YQ2Fbb/U5/Xuhoy01gnXxjSgibGqOLrN+HKCjjW5bCMXrIeZ1tPmLSy1xQySfkXzM6tU7jcejmx7PLXj3dO7qWo+WZkuDaCNSAET8El/ZxGNa1IWbhcvyDzxeTGFUUjPiVMcZlWk9fAe08SiroHA/Q1s4m2nR1e9LLgIa7Yd/+w3GeaGhLTOCniGKJLM0wARtNALFeT/zFhra8hfnlsgQUz4j6ZjVq3caRXNXMydzH22Jp2s9WpopjaKNsEC/VOikMiVd8IcAg8KGLJozIBhUoApNwjtD29YFFPn5+P7RUF8ktIaiWVrwQrMiM4L2AptXJtCmATrahDPJ0Ja/OLdEhkygDYZqRvIx02vaafTz7Z+BeQs39qehXak0ExpFW/s7rV0uQ1u4gpym/UZVvi06V6tujrZFD49cniO09epyelVhWUjRBN5OfNtt70ZK7NjnhY42N4LIpenkkfM2Eg0wQ1sHsiUaG9qyF+eWmCFSPiP5mGks7bRZG0RsPx09tnVBn9NljZGl0kxoDG1y0xxtYgPPdn+iimrhQA6EllEVMELUUvVJtMl1XeyUxxKruyNS+3lhjrbpj5zkphnajEBNio0UteZoy1+cW+KGSPmM5GOenq0WrsVgWr1da3vEFzCtpLbSzOjvoE2UMmfRo2pGPq2MqnG0sSyU6+Y6/qqt4aKye7QVDqOt62xktqK5d0/JLD3019BGlQm0uSGSz0g25l/PFivS4Nmaw/FDhJu1Hi3Nkv4S2uYtHIvaHG3KqDr99sjHjYNo27FdaMsOI7D6iwIFUlJWOIw2osxz2Bwb+Ya2fxW1uSGSR2025t/OFivS4NnufSxF44f4NFEqzYb+7oJ0eq8N73K0kU5y8sg1Wh9Hm2Bqp+O/PRKRRjPSU7Eo/HO0cTasxUxhxQDaaOjP9tp6qVvihmRo01GGbMzTe23fdvPmrg6erVzKZ5v5SyqprTQ7+gtok2NAquar+RdSR1v3d0IOvHAAbTTIr6RwPKWn6bhjRHOWsJAXjqNNycC9zNGml/LNNvtC2rNoNo1/IXVL3BDJZ2RizNNfSMnu2fzkYDTUi3bd3l5JbaXZ0d9BG8ntA3ltjjYVUmkEbS4+DehiXY5v7VrbnRLkwQDY4oVjaFPmHV3+Em30r2E1xEEc8WM0r80tcUMkzYiud8rHPJ3Xhim7z+yM59h3e7OgktpKs6O/gzblyt/aEx7nSfXdzXO0LWsbRTpOMIw2P2fVT4LrNvFwXhDHsXmS8OnLC4fQRpAJNBii6KSH9HYOXfz2RUOnEQ4s/Ebezg9Hm7+4zBI35Ge06fQCSPMxq7Gs047iNRf2BhY5h/C61qOlGdJfQhsnHDmxeOObS/10KtHQtuHZ/u6iHB3gwp0Ia3K0qYpJx/Gjpa+XLp0grw3ktUuG2p57FBKbeOEo2jiJGZ2EvSdaaCSzLALlFGfkoay6qcrtnXCGNICjyqprLy63xA2RNCO0wYzYmNVY1ilAg3KMJqqUSrOiv7XX9mCd/ruViZs/1AzwIm+0BwxSNJmjjcLp+9p0yY9u1rALL8BJUjiGNhtWc3aZxUPajJcl9/ObP6g8cPOHWeKGWF7b+z2akckxT9/8oVXzt+3CSmorzZL+EtpWXfrmGic/Td7Xpma+xQn997pFbMWd++zU5VRRFUMbTb6gJdzz5X47Nq/L5KxwDG1YEYRQTZmlh9DxD98Ztq2Vy0SdA1Pl6fvackvcEGQ36GlGbMzT97UJzzo7XEltpVnS30Lb+pZA+u3+Vp0JiDJugTW0sY4Kd2mPxfIs6j5ezPZ27suqYmhTm/faWa3WZbsqFin96+TnU3nhONp0J/C3ta/M0kOIJXac82xvKr9lV5VV115cZokb4mhra8v4Q96Ejdkas07ZWWyvmV/VIavSLClHm1QX7JdKpRlUoa1UKv2HKrSVSqX/UIW2Uqn0H6rQViqV/kMV2kql0n+oQlupVPoPlaOtVCqVZlqFtlKp9B/qC3tnEjNDEMXxrhDrjGXs+9j3nRB7QkQQB7FGTEJCOAhHRCIhcXBxskS4OYi4WCMSwUGcBAlxEEHi5mC7Ovi/espDdWOY0VXT73/4vq+7p7qrXtX79avtG0WbSqVqQSnaVCpVC0rRplKpWlANRpv5Ayk+1bCxSqshHina4pAaNghpNcQjRVscUsMGIa2GeKRoi0Nq2CCk1RCPFG1xSA0bhLQa4pGiLQ6pYYOQVkM8ajDa/ii5Vn39yq4YXblTt7R9F0GKtjikaAtC2r7jkaItDinagpC273ikaItDirYgpO07Hina4pCiLQhp+45HirY4pGgLQtq+45GiLQ4p2oKQtu94FATa7Legy5eatxnBXw1vv5DeVJM2zz8mRdc/oG3+mwU4PW3V6uQ/aeyoQYMzzjZG0kj+RPmjrTTMfNWAA7Xkf6l0/EZSZAWAttIlAIxV+TT1u1a7fBlO9ZnVfY1+88Lfo63t037Orc4sSrKlaGsq2kQL3yXNl/Ue2LvIyh9t8++ZdnsuTKW3zAIzYMN3rbZX58pr2zAUbX+NttJNa12OgHdks0DR1ly0VfmvFSv7mU3rk2ZK7K1oyxdtpe2m8lr8cMZa+dzMyXPmKtr+CW3d13yzbpvh/dr1TupSyK4WJ9qSNrsmtls3NT1d0PaOTzmjDQ7XEZGaBNGod0Vbw9CGwBcGFcxNmJLUo5BdLVK0Id8d8fpOVdD2jk95o61DF9OjqwwMddtzrsytFj+sqvwbkCu06kebT7NJn3kqoe3RjYa7qdCY0YMGU2e1sn9RUto6EedX82tlaHkfph+WfpyauMtWbfZtlNFwm/jKo/eL+KY4j8TiVCuuLDa4xZFFnqvh47i5WXiu7O66sh/nKTNLAERl4PzD/Uxl8zaHNi9D6QoJbcmkcQieeXpnsUFhUDY5nPZwkZiKagF/pNvDK7o1j71ak+G9Kj50CEkxi7QtKZhyRhvCij6zfrqsaGsY2vDiGN/XmzF9a6zaHVzEHHkx0ZAu3rlnSPMGMtoOdzTQgGvP+TIPEZVufk18v8yJ757AZzZgxFQmgsgzue9ljCTlszIMwbp4lV9peBYJMWZmluC7q/AwaFonQdv3GcpSUGjrPh0ZJ+ssFouJsczSJx7a0uzh1YWYB1U2VdCGKNE4oxVLOaMN/dH2nVLRph3Sf0cbfERMJ1RBbFBOSoh/dpTJbxA8raaJ1J47p92eCo8DDcn2NKNKGDz1gC/jFURhoIsdgDNKXDm5cPaVD1NwHjfFx+F1zjN3T6Twgh5EYQqflV4ZhYNtz07EJXtoo8IVGw1aQ1aWyFvp8NYCjMhyI/EylKbA0Na2G97TZB2zsGaLjIzTUAwOiXd4D3hoS7OHV3Q8hGNsa55EaoE+BKNR76hQyhdtqGf0mBRtzUIb5pgHypF7mYA+9McS0Ir8Bp7AwQTO2+uwOmwP8NnuE3yNL1etozBBkBipkNhW38ipvTojsQsT2alQtUhpk+KcoI1PEdLoM4g4KBVFa/Zp2Vki38WhG5G1jcTLUKrCQhsyDkOgbLCOGzMQY9lejI823x5e0fEQ+kXncQtnb3wWSLM3vl6wZW55o20IdzVl7U9loKKtUWiDBT20WdDIX/Ab/OSagA84d8JP6x+4/BVCQ1An8ED6ECdGpVDiKiMKN2Fwzttfdmjb0lveVD+jjcIQJ/RmKVhzVZ2RJVyk0861cVc/QxkKEG0McYdzwB0/uBhLax7a0uzhFd2ZB/eiQ0EbAr2kiMofbROmKNr+V9QmDZ99DPYdM5qOHbsEbfQT+uEyR1sucVWulobhfNrcXJtDs2dvNB7aEGjQyHYtgWRiYTZmHXA/P0sObY4GdCtkwM9QuoJD24QpqBkqkruIEgHuTh7a0uzhFd3VAf4QtFGISzMNmK4pnHJGG2rF1qlUqqKtcWhDROCZDsNjA2VxTZ1ogxs6Ea9wlWgjvBTPdOtTST7aaJRN9h3xVJ/Vr9FWGeiKVRlIjcTPULqCQpvtqLDt3XFVSPfHaPOKTm7io82OspEWFo5ugc2QKtqaOEPa4fPmWoPRhptloY3m/Cg0m73a65DyDseNX2fz3ATpwtnvHw9rfbTZjuN/QpssrOHJ6ELJ5L+ubUdZ0dYUtMksjbxGfuyQ9uhaL9royA8B/Q4pz/mt5kz4aPvqc+cnImin7ilmYwWRf94h/bO2ERba7Lo2YRnbrv4OqVf0TLTxCsMFBo8plExQuxEUbY1FG82iXZsqqz7AOW8aoR60gVIu8Rrc2KFNzsNl23eCU9k7wFv5OR7a5r/FNTeGLsPjOMxEm/CTZ/2okfgZSlVQaEOOMZGZNY3AkwMOdChjJtq8oqejre3TV72/q+0iyeS+h/QmL1ok0ZqcNLQV7X3TjD2kcAW8RLzFH/WhDeRCYllh4NBmz4tnCtr4OR7aZLUHupaCNuT2l2gjKPDij97USPwMpSsktGEnLxU9c/EHLNPbcQ8H2Wjziu6jDfaXG9vVO4VS7mjDG5z/Uxv6J3s70hL5n9GGStlRtCHQRqENoRrvvMFAvVu1vh0rPcu00hNmrQttskK27bHFGLoRtOE8raal5bnAJ0GMqIWV9TyXgKTekl3qgrY5tBGOR7RFtdss/gZtZmmNxsVtohEpGcpQMGijTU/IJ/OICkOb2GAx133HCl7gm1r8tIGwx4KXlzPR9lPRPbTB/rgxPcfeGDvUAMJCKX+0udkyEmrbW7KLF49utKofbf5/wxtwpuxttKoTbbK5x2ABqKBNbkpdI7eiqt/XxxzrTLUnaJPb0PYfOYKnXgIDstHW8/QJ92w0Ej9DWQoAbVZidW+jFQ6dQWTX1bS7ozPR9lPRfbQh5DOmStXgaqZgy9sCQBvNluFlBq49q3m7EZx3ti/aDriGoE02qcv+aG+Hez1o49jaLSUQtPnb4+UxiEgQMLizkgfZOY8GsNjmEJ1XBGTZaOuPIIe3kKOReBnKUkBoW/rMrVSWQvvb4znWxb8BGJONtp+K7qONRnfQ0bf34m34BVMQaFPpdyP8RoFMJ2n7jkeKtjikaFO0qRRtLShFm6JNpWhrQSnaFG2q3NH2G7W4B/5KathWQFuxqyEaKdrikBo2CGk1xCNFWxxSwwYhrYZ4pGiLQ2rYIKTVEI8UbXFIDRuEtBrikaItDqlhg5BWQzwypMKsH1CpVAWRok2lUrWgFG0qlaoFpWhTqVQtKEXbF3buoAYAAIRhoH/X2GDNnYSR9AkQJG1AkLQBQdIGBEkbECRtQJC0AUHSBgRJGxDk88cGw77gDDukbYNhX3CGHdK2wbAvOMMOadtg2GPvjlkSCOM4jj9/TiTDqy4kzNTKIhSLDJIgswJForawxoaGaGxUCILeQFO+gsacihD36A0EDU3Obq1J5XEghT3ieYj/R36fwUFu8XcPXw6XYwG3QR1ImxowLAu4DerAK5bVgFcssyBf2KgGcL5ZQdrUgLSxIF/YWw/ifLOCtKkBaWNBvnC0Ecb5ZgVpUwPSxoJ84dhXCuebFaRNDUgbC/KFk+c6zjcrSJsakDYWpAtrC3E/zjcrSJsakDYWpAu7JqZwvnlB2tSAtLEgXdiobuB88zLotHkj1LJzp4v/aO8fwrnkaigsHHLN0rwuusMkbVqx4CFyX0i2jS3ZWMV7ukK0mVJzwf6mbaQx2vki7fohQ0S5t8PWmrcV8zPiC4guadnPWtt3iwMetPULtOI20WRQ/FIuHa8JO4Y1bUT3j6KdkfcFkLYe05Z+JrLkXp2mzchTU9yv5oL9TVu0keh4UbpOZHFf7QrT/l4obDNt5VLczzZtWnaaiJYTf/+B3KoJG4YtbXPCclDwuM3It0+HtPWaNu8Z5cznNddNhtaPHKZtZMz9oiu7YH/TFvse73RRuUQzlyfms1vBQ08Va3m7aTPyzVvINm0/7J1L6I5BFMaJ3O/3W5Hc79fIvURCFnIXsiAWYolcFopiwcaCWJNsFBslK2sUJStWikS2Es/xMw6HwYsv837+p+ibeWfm/d7nnHneM2fOfP+2PXiwYH/iugrSpNQm1Yn0W6jtL1Ebb82OZm44Cfhbf0BtY0b3GlJfBBtDbStOrY+5H7ER4E/r9Ekleya23ry+KrWxHH2ybmqB1OaaHZb5zr8sTUttrfp27j+rhdr+GrVhbs5yE6a0UNvfmy7M3MPbZkEww3KNcGjmgRwLt3b9qlIbHpCctppRmzx9vvSvSfNS29hRUjMRVoVcp63ageokCl6jwzQ79H+fQV3O3pu9WvXD2p5c0JqVV+idgt/t9q0O1EarhVu7DKe67cmNFm2/MjVFfVcO1JDHF/lquXWvg/c/TUzdz7c98lICtclVS+C278SjEM/m0ZhgETNJREEtPykiA2HEzItlINiA6dLl2oX703t3/0heg3KNJJPGoQak53QtTogxy/aN2uZ/tFCtV6P5upV/pEQbRB16DZl/bGDrXlt2OLVZEF/2a4teEysKfLdnu4Nf/VbbsY1NKgUx/FvF8VHuQg0POX96nl63p7c2QODzXV31v3i8gjQrtSWvTZEJgHr6Q2q7cVMXdnUd0Xr8zbQLEXt7cdoZp7YvqgcvuWPVhHk9yNv24cAUfMflobhwo93aIlh+w6wUQG1y1QZfSOzhCyLEl0UBMySgEKgtQhhaf10sAsFGUNsl2ezbIR9zP4ZkGqEF7Prz+qH/rC+p7fwnHG9M/cZ83crFiBoEaltFk2mdnNq63KSXoqHwSkfKRK/3SueZWyCxTRsmVSpmxme6fEVtQ/p21hsUPtf3tTktO/t1aVJqU6zNYLHIxNI3U+01L2fWd2AitbU78W7V7E2zVC9vQAhf6zh4Q+xNIH1rqza35CR8pjaqp92e+qma4t2urbrobSgr0r06Dj60Q1knBN+l9nZHd7Rqe1Ha1611VXew4o/fSP+e2gSGvWpfL/rSguXBtrIn9WVRxAyJKLAgzUAYWodiCQg2YLqAghlMBw7Hf9sIHvAYMpNeT+0L0taCUdgvEE7RfN3KrZfa0wHc6cC00NTRHLAFyuANaE5FecobNaPsO37U+fxrHb+dX0hsY5Nq8IVFVrQB4/jyIKVce3uJOpmfLEhtBYpKxefiNCtXiWI0J7XZ5pGAMliBXHiKZ/LU1rr/LBVGql5wM9XksYfeQlhhDvTp1GbVxHXn37FqWqEzjSoz4m1H8F0vTLk4XNWtVUR7crZ/vGP476nNjFui1YuWDnxnHkUPYU/GBIuIIxEFqC0DYWwdigUg2IjpQq5ZJ8v9mJtphMHi6yLCUTbv1AalifxleUEVbuXm62kQOiSLNoSZFnuNcsBX3cUnuoQKMWejK6uWn5bRdmyDv6c2prjM+JD2V9SmKlakqNiqq729morakOTeOu/zospTG/aiesHLPFF97M0MZiY6tYVqin7BQ+58wkC4qlvo/4WrfwWNAqiNlF2J3rpCTyBtBx7gZIJFxJGIAtSWgTC2DsUCEGwQtQk8wTD2aNdMI2w8UJswcGoTjIwjMw+qwMpdX4zmuM+ZSz18Ar4aRW8beIq5o6HMA0O+uQUS2jCpqJfSw/haZX6+enTHl9QmYUUKVTJSpUzIpqQ2YpQOChHTPLVRVD2w0yD2xmCAXHp069JUc/XSikFkS5CkiQxQg9A1sSfJidNWbf0pGiVQG4nwCwiTfK6YPXtja6e2iDgSUYDaMhDG1qFYAIKNmC4wjPyYmUzfbxtBBz/02iAazDyowq0c5qAe3BkBq8dFSvH/pL+Ts7VhpEHJtV44m12D72o7tIGQ/GuF8ZlySQK1qa08OvEuTfjw69JU1DaMhdPHiIOEeDUixeSpDfCp9w+hd5iACO+iZGeq/jxp8fvRkoTprDr3QHQLQkbOxTkphdrSnhhrihUrCeg7tX2DOBJR4P8MhLF1KBaAYEOojYXhuqkjmPm/FWsDcT4EVWDlkdrAHQ1g9TgIrlb2M12fCpnpI3ui39V2bANDwlT6FMbn/ZShNvz1Dt2Yd76Q/jVpQmqzPRc2Y36N2kCyArUxAT3NpDq1cdXuZUyx0bePcvLPqY0li4SQmOBhg1T2O3v1iD+gNiCsQG0FINgQasNyJ0zpgZFld0iB6osd0l+jNlCtTG1sYMoHe32f0cn4YVP1u9oObf6E2gjf2JK1hdoECNvLuBUhi7QitcXeLDXDsPkFKb42qvNsMJZTfi8+H7g8kR3drPxbavO4h393C+oT5eIZQQVoogQUfnVBSutQLADBxlAbz7/ump6iUl5bltqoDa8XtJFfkNIXIdivwFkyf3fc93eUcdMtSGgTF6Rh/PyClKWt+vbQv0Rt//mC1E+juC+rGlUAXcKTLnlqC71/vI2AFVTeRpj/4qvUnbz8c2pjE+0LppKh8d15FKZHwKziNgIQxtahWACCjaE25vKR91OyjcjF+OlpBD4EVWDlcRtB3T2KlbYRkv2ukR+sXroCvhq0zZ7FFFHK97Qd2jDZPDIXxscnS1sSkdrs6stu6vVbpyWaktqkc9toJj8BVIQyukyYg9wPqC321qBYVZedIfljxlqqf578wQ44G/SwJ1c93JSRf05twkBb+SnB1pxiqA2wE7UFzH6e/BEh/HnyRwEINojaeChfc2WS3+IZUpzZSG3R+LHyRBi6ScoWAVjNC6xeF7FfUkigNhwoDcrVFKD4rrZjGzKqGEFDhfHRX1oWRGoz7nze2Xr998kfAAKMhlhKKdRpEr0h7EWv9Bvc+oWvlDX4aHfHPLV923uNJeP+PGXXkiMsJTGl7CrDVCsmz6NcuPXLhFP5+9pn3IiN5OTfU5s9F2dl5vNkBuLSB+wlOLVFzJCIws9Tdr11KJaAYOOoTRZqUP70lz9W+y9/kG2x4RtqC6pwK8d9SjkFJKELu5Cya2ejrl43elIeFXsJNiiasqvfmV9IaEMefFfbWzBOC+OnFa/lY+9iNRzWzq3hM18//6I0K7UZ5gLSDMFPdYCU7Z6nwx1XL07OU1vo7b9YNu+Zhxi+qO51btRPD1r56aTHm/xUS4q4ZqUEagu/E8aBJ0qnzQuA2iJmSEQBastDmD9oVQSCjaE2pO+6TKOMHvBtVBgWqC2qAiv3jDKJn8wyiowHraikZOx0Tf0ZMxxFDNoObTTqwo1+GCuOj3JpHGJtuHa8C2HkKj9r1LTUZqskVHN2gWcG2BEqizbzFtIh+Jk/orbYm5PYip3bJI7pEPJo9lIdj//i16Tj23g9elH5WWQ/YZ6VEqiNX3e14wg8mT/n8mX2GgGVgBkSUYDashDG1l4sA8GGURuHSZEqv7Kro1Va7AVqC6rAytOSUW437XRGSmuRRW71pGYntP38u2aUuXaUOSyf1ba3YdT7Upi5moiPH47HR2rzMCzr2+2VftWoiajtHwg+s1xy35suX8r72wi1g9ClnvbdZacH5xosEOYffFMtQtPH//ZHjf6J+J4EO9WtypfyqK12ENad2j7+rGMtqM02UFt+ivJfCXsSH8OktdiiK4/aagdh3antowdUB2pbsUAbJOkrV9wmaqG2PxXC6TVKhy+Q2uoGYe2pTeH58QNKpzYFz9mJYJjKb72yqe0nUsify1SQ26LrNfkBiiKBrReE9VeD/bG+0qmt7VDbLGrWP9ZX2gxsBmkBtghpUUN9pIXa6iEtwBYhLWqoj7RQWz2kBdgipEUN9ZEWaquHfGDvjlEABIIgCP7/1+aCwSKC11v1BJudxOB82F+Q4Ry3DLIABaYNCDJtQJBpA4JMGxBk2oAg0wYEmTYgyLQBQfdpA6gwbUCQaQOCTBsQZNqAIH9Iv3PCQxHv/erRhifKrapk2uYcyPKjaZarVTJtcw5k+dE0y9UqmbY5B7L8aJrlapVM25wDWX40zXK1SqZtzoEsP5pmuVol0zbnQJYfTbNcrZJpm3Mgy4+mWa5WybTNOZDlR9MsV6tk2uYcyPKjaZarVbrYO5PQn6IojnuReZ7nOfM8Zy5jpiJjYqFMC7E0lVCUDQsUsaMkQoakxEYoJUUWVsrORtlSvtfX9eVct2f6/959j7Pg/d7v9+49955zPu/ec+97/z9G27Q303FFt73nWvy+Egsf98S/gwZmWe/Pn5tvuPfnLWOBfVr85Pl4veNHZm27e/XyaksrQBr0gmlWS7/e0e63kkLQ+B6VEZzU37WoC0qb+3Kj+Tm+eRTtf3UBDT08y9rubqGDukRbm7FZ1mmi/9ShWZa1axVxMshfd4AePWuAtgZHdjZBT65bX++PMJAQ2pofhJtRxvyuZaYd7NJDaEMvHZ3Vvmvt0cZ6q4e2bO2qCNrU/SVC27S33uHavh/9HdgOrOGv89HWfFMGAWC+HtQp2ur3Q9QP7fzFJK0/c65aaKu/bVZGabhv5h9gICG0Nb+TSbrdHP1b3dI/y75D2+ABWSFoQ72VRBuHbdI/7P4SoU0ex4ZJmvfNfgptHDhBQJuvB3U7IR0xJMuGjeJx45buuFpoq9+vi6xy6Ua938VAQmhrszz7VqZ2//M2JYi2z1JitCGSqoM2wogiROSjzXKk4crROqjjXJtmpGwAUFottG0fnknQoeVHW/3ZDtZjnsKfFiL/4aY+/9GWINrgbFVBG6ZzmPNshNqvZhEXv4c2WFoHdYw2zUg1H60U2jCtdxiYiWSny7uPW1F+tLmbUXbrNjXbNpw2AyAYQTxgfvCCw/rcQzN5HkZsfgKeOePiaGdT1tkbnuz/dTJ5Q5OsUdMvY3imQ5QudinLhjtwOT26d/1da9i5Pqc5HaWvt7Cx50lkqqF6p5gKwgmpUz6sLV200dmkP3rLLfwsw6G631yZKtoYql/uqw072paSD/I3XtljxYsu2dz17IKvTe5xwR/0rDu02Rmp5qPSUu2DN9MDdSBvk+TEFDOPO5vgqvM1QBsGonQxUo5W4foivGy9Bxc01EEcA7LX4rEMe9aAUmuBNhmM9KHyvDNZtJF6EGKQbVpy0o9d42ibNwTNsQ3jja9JxssftqAPDL2TQThqVD6m22noJ9iE55EiUHpAaDMVBGi7TOWzMU1NqcmiDf0s/akwc6PlQ5sftUks2qy/4cpGV5u4dElRaNOMVPNRo2UEbcbbKPkxJdeesabu0QabKO3Z4fDL9VKQ6woRtEUwIHv14+CGNeCoZmjTMrZAB2OEaFswP6PQqDjvBfrG0bbKQxsN401B02AKvIU+4AUAtBlAwcae/7agLS2+QZupwKBNzaFSKjVLE23j9tAvqL8Udv5YOrR9ybWNWXKuRQxt1t/YQudLBaBNM1I6p+ajRssI2oy3UfJi6rsYqWO0UVXWYdGr4ArRFseA7DWCgxsOdVFKDdAmeykkWD9aaNDGn7V9Xa8BxpjOvdimGctwOyKLYiuk9T20cctT4pUfcavCJgB3OdE25t7o5nfovvR+nGhwFr0r2ITnobCbpTS/1oTexnpNBQZtdE5shKo/ezi1UqmJom3y/flkt5YH3bhn4XQ3M0o+1yaBETRkh8wg3Uyuzfob0cbhN7uggFwb7/tQRvNRo2UEbdbbKHkxxWq6nZlZ/+70OkSbzESdJVQAekMB3kEt2uIYkL0QiGwdeo8RWiO0URk6huBt0SYyobXsAZzHHUhkjG7+ILSDhvEjmdKb1aLz/KhR42NOkQUbex71Y9ArBVmvrcCiDd9yzI/L4aTflZoo2qZAMz8bY2vXrvLNLB3aGM6ao1m0WX9jqMBQxaINSuE+rfmo0TKGNuttlPyYQjUc5mwfXgDaqAeigsNL4ClAWxQDspcGN2x8gmjT9zIDXJAHUbRx5oQfRhvGy321Kh9mx+8JQcEmPA8o0SJQAF+oXlVg0UavwSd+DTR8W2qiubbJU5DbhXtRf0QDW8UkZefSoY0JdLEtQJs+C21oZ4Fo04wU/8FpLJDjaLPeRsmNKVZDpy8EbYwKpckCtEUxIHuRizCPB3l6E1LG2JHr0zOZAYrmow12Rf+4hoVbZabth397tOFXMrNc3GzHsOdRrgQKqF5VEKCNjfaCn7PUlDd/MHnYaaJGnV6cviVE22frLOrCfEyINvmb0IZ2F4g2zUj9kNloGUWb9TZKbkzRpnSAQtAmW7CLA7TFMSB7kYv4DfoOs46aoY1a5y4jcB2at1mZAefz0EZYIiJRLrAfPK5G6S1nMGizzh6e5/qDwkb1qgKLNnqNpPd3paaKNox/EVH32b1oJYX2SB1tZhlBgo1tUlz9b/2N23I7Fo42zkh9jsVoGUWb9TZKXkzJpjiqyTKCInTEhyUXR8sWjKoQbXEMyF5+cAO+EzO13fxBnHY4fGimzzVRU6WquA49Zsm7Z31/BW2CNpqKhpktHDMmXdw+II42XEA0Cjb2vEZtFm2qIGfURrSp1GTR5lIu3eb4UVuZ0Yb/cLdTKr27RVvgb8RX4WjjjNSvjFktQ7Qxi2a9DWKujqFN2ybqEG1yMgcjNVNo8xYj0ahQHtpkEw5uUD6KLGbLrsvltD01nA2kpl8Hlsq7N/8ltAnarb9vGLP/upzF/k6ujWEvYb2mAoM2O3woRa6NaxwMDjajtE8jwCac0TEV+wO0yd+SQhvUQqws5X3QRoVFm2Z5PG0kFlMF5dqQ7+CKAHfhIXjgd5FcG775SbRpRgq+oYSCHrTii418Mt6Zj54nP6I5fg1thPbTltrRo+yBMn0GbfwiXLMMzxNhFm2mgvgKqUpNfoWUCVmIVkjLijY3mOH29YU7m/x4Qip/Swpt6HdtvQy1FNq0MU0rpEbyYgrna7hCCqMQA+7dOUyA/mCFlE3H559Em2akT5qhLQU/Ho/G0H3wuhm87Uhow9COW2wiaANi6k/4Fm0w55UW5MbxLpiUmmjFthfuIwvRRnu7nWbcJSDYBOfRy9jXht2Fm/e+m6l6z6uCEG1sM3bsYNvbjEnLvis11X1tU7TPuTePsK8Nmyg+uqyIuv9bSRZtziMk9AyhDcxuM0H+lhTaqLkffwRRoQfJ3P59JBJ53ngbJTemwFHua8O7hmqANjqXXpLhCaZ9bdR47vN6C9dkRFscA7SJbgjtt3IEWhO0SaY9yCRYi6cyEk5Iv3tVg20TW+18mI5HB6RBeK0aJheRa1u0afYFUWiE55mw0I5pX+8KU4FBG5980QZxlJr20wiuT2gW6q/XAsnpQs0TRZu0h9jhPJcRv/e3ZNBGX+Z8NIyKEN2ElvU2Sn5M6V0ctX6pEQeZoh2330pjoS2CAdpE0c4mFvQqSoXK3o3eGEhbP3AqK/bnXmiGe5ZtEwkvtHEsSIMwIG3DkNnjs2k7naMEaIO45wv8A3QK2fA8Rlp6x5TqNRVYtPFWqG1VKjVltNHZe/uXOeohUnR/mdDG7qZgFGDHDoiK7/0tHbQ5X1ba2Gpp0X3pGi1nvI2SH1N4fIEXHRtZt2jTcjWFbyymAiKb6Dfm+gChLcCA0CaIc4BRyAvE96/5DLV3R4dTCfe6THxeD9RQd/9aDkzJw+fn+XpU1yUebSwAcyZCO8w1+NcFYB6OAZ1FG2ucDrOfO9/LhKw5j5oXdXG6u4mZ6jUVWLThd29mOZS9m2lKTRptyMhCfx4eQYdjgQ0zHHV/WdDGfQ+0WvCWZJdhMP6WDNqcLysfHmrJ9rEVeIisPy1nvI2SH1PMRbZdt3F8DdDGoKRPbfzmjH/zh9wM3w8m2uIYoE10Q+AUtwi0SRZOp+X+luhRiypLxf7Wxj/xB0WqabkkrcTnSItCmzi94Uznv9UkvT8ADauypOhP9f6FoPlvuRJYqUFrt0+ucn+sDyNTprorLQl1+DdS/aD5b7nkrfT1RaqVQxtSXVy7rLQk1OHfSMWD5r/lymClBq05tqnen1hGfhHZ+YqTrXIB8om9cwm9b4riOBHJIyTyKkbk/SZ55ZWQJM+JgWIiYWbAQIoyUlJMzJRQXiUxkIFiICNJkpSZgQwpj+/2tX2OvWzHuef+r3P33Xvw+93Hvvustddan7PPPvusvRVB0y23DVb6Y53vQ3f33eO3tOxCh7fpVa0p1ZqVOtqmlx4gOx40bVquNSt1tE0vPUB2PGjatFxrVupom156gOx40LRpudas1NE2vfQA2fGgadNyrVmpo2166QGy40HTpuVas1JH2/TSA2THg6ZNy7VmpQloYw+JuQ9rx3LDJySjXLXwrLqecZ/Twsdja+mQORSnZTjtLFYdSiM/Dcuq6pkBMr/DSSXAR/d8MBDfD9E77WOWfqlBM2G/HSeOSTkcpJF/k5MkXPvkFX5mUYvEtxptD55+1IW89rPoZ5zCwtc1Wsn5QJWRYKyE5NRjn2MgXFLBRFoku+Ty0aaUG8fPQxtYIeHftEKCgtE4QeaKbyl4eG2N5F2KmcWiTfkmUroFxE/JDkj4ZOlbQJscJEV+iikbidRW77zriDr2zm1Gm9AtDOTXiQIGNhyYbyU8ncTBa0YbBsIl5bWgzS65dLSFFOKUVXIZr5y7bsq+VJa5kqtAwUOqTmnkj5WcZbFoc/J0xFdUDHMLWvoW0KaA0B5JV9/8ygMHnnDQc7+dRdLEv3ae1v/tRZvQLAxwnhcF/KnMuG4reRSFt68PbUMDEVEyNGizSy4FbRMwMcPN8czNo43dU/ciaaXR5i3HFhIgdbRZfCcXu/wHjd08BLb02482b5LmrJAPn573yD7nOOXndIzqvYYh24s2b3BJGkeDgx0v12IleABd1loGBiKi9D/rRkDtFtpw882ijf0ifNq8VPlgQZs3il1IgFTQhvgytS5h7E4p0i399qPt+usUHd6dWIkatd1AyqEoBdlbUhGjk+O2ok1jUGPApsxoY5/yGVba8zygYCAiiuSNIaCmo40JKE0XnvPZS1YlJ7pVks888a6vNZ34xb3kwU0PturR9uR4ChoNX5Q/M18f/ZFo8wbN3SrhuMekRoAvSEmlmpN2+gc592055/UdyTz1+7jPdjqQeunuUN+AkkQW5RYbi+ztKBlEQOagvjo8BY/xAdrY3n9+gDgAbQpvEegP5NNOv2pluCCV1vecnnKu/mGMv9Bmqx5Mv3kLJcf+ZT4l2iRIv2fQFk0R07Dak6RIchvnZJUpMtpeevoyvbdVi15IiWZv04SaprvdSzmfNVM1MBwDrgVtRELcoChZ58Qvv083vQYvRxyvaNR7NrMrHmZKlANtVnyWldh8XDmAiRVOK2WFoYJceMqz0jPv+i790J8HLTEQBpGGnI8JqDlo82Gc/d9o0/YHpGt3ujje+zqMGejkkC8KUzppDNB21Aun+xdSK6KNJrU31p/mvcnHVDMhWaVK2gYooI1JYm8EUNZ3PJ32cqrhoTVoK5QsREDmUn0zJ6Ptna9TWBK7eNcstGFvRaRbtZ2VWh9tQVvW5ZwPQZv1tpFAG+JzHCmA9HsUbZiigrb93rdrnXebXNJq2upyTRLxF70gW+3/xgGpVfWSA0pucMlP97m2j66tl9Sq7UbEzEBbiIR/RNuVOqwGiYOXI45XNCqF2OFbr2QuGTgPUC/50eBwMwB7spWYOmZXigJtscJQwWLvB1mmQFuhJQYiolK3XXv/AYY+Lrkq2sA/2+Oo09hkx4dmItb3M9goymHjm5agTe9pIaANWjhbEz9wO4FsHAu0MZNKS2X91DsUuQhoK5QsREDmQn18TZYQPh2WeVygdteFNqNMPWV5pLpcV2N3tAVt6PLNW6CNmgO0IT79p18g/R5CWzDFP6MtF23hiG38OWoXvSBb5UY9yLFtbtfWF1bIhzBKGBzMRluIhH9Cm+8wDl+OOF7RqC/SbB9py1t1giBxtcFh8qkTV7QS4EK0gLZQYaBVRhimOerTAdqilhiIiGJTrHfeJaBmoM29ok0NdWIz2mT7dHmpPWtSdOnQacI5TS1dftG9DjpVP1gnlySlHc+y/A1tl9/irZXUQrhDKpnT5YO2Ak19xA90PlM/FaKx3WGcd/Euocd57Kb+KevryNZNgwAJy1xboWQQAZkL9b0Pxx9o++RuwtLxY+HnoQ0SSCG2SVKXSX791ehfk/8+n7nDE5/ob9Cm3VO1PZTV9Vwb4hOctgDS71G0YYoK2tTVOj15OK9AEs79uYzs3dWkdtkLCW0eoDERfekzt+112BPHcTWqFr9FEB1/LtpCJPwj2nxUXo45XmjUsM5XnPpxUtmW0yuDwydaA2JlK1ky9avnkIVq5toqFdAqI0xSsWsvaItaYiBcUkZiDIFLroY27D7cito7WufeVKf5LTTJs37aFdaOJ3cq0KZ4sdRqoUCbrafvPV6U8PkHbOjuEjepBm1U8H2v97SNcqyvI1s3HVJdDNoKJUsRQBvqoz0SEp/WDmzMRZtZ5jOZWpVXSR/vDM/lqjtcIlp2regAbZwKVMNoC+Lb06Qd0u9ZtGGKCtqkhipYKO9H5c91sekhgdQuekHVHO82XjzP+3PPUTgK9bO5aIuR8I9oc5fycszxikYZ03gUr7+pZZtaBgZtOoKam2EltqRXPxv+Rlu1QtYKtMnI9jufbUFbLcKJKDtjmhm+wcMSXHJltIEHdV9ShXGg5LEnpWn6vJkYnkFAq08LtEkkY94KgzasxwSCfpD6oSCFBVLbTKIGtNmg1fo6smNdLwq0FUqWIiAz6sehGQJbGWSbhTbu8ktMXWT6QlKH9VUH80XucPpTpsxoA8ygDfG5wDAlkH7PoQ1T1NEmZ7BR2e8NPOe7nGUvyCH1rzogY05WSHErEmQ22mIkRLRx0ufliOPRKLNFsqQxzthM6BBlQJscHgxNtxIc4JYLaKtVyFqBNgJQ8oK2qCUFl4R1VhKXXA1tBHFWhcklX97kgaLi+5boPjief08csQ9kiTZ/zxFhYaG4OURLJdowdajPkdX5EW1ByUIEZB6qjxwRbbj2fLSxQuvQvfd79BD1t8wt8fWXIi3c4aG/ESWgzRXYfdHjIaTfs2jDFBFt2NZOwRfY3Awve0E6uaH6agVGGjQ7G21EQh1tBg8vRx3PjYZTnJes0BW6wNIgCLTh4CtaCY/HEu7OegW0ypI4uqgB2ioRjkuGAQ0dOhNtvEp/KSd4o2juSFFdhXPtdLRxRPStok3fBLRRoVYfgwe0FUqWIiBzof4G0IbJL77jXEl9qI7oVY06GsXBA9rQegxtrAHzLeyNoA1T1NHm/3W0+VXZC9LJp/062hjRrhVtHK2GNlOKl2OOR6PlImTpSFd4GdueQZsVAG31CmgV0aZXoA0tx9CGFdeFNgnoXmTURuentSo35rv2oQ9xPNDmIeYI2rigHEUbg/+INpSP9etoQ8kRtJXqizF1tBnya0GbF5h/eIrnik/9w7/1UQVt9PcI2hBfV6PcNUT6zaNNMv0XtDEtU/SClR69KqMa3TUbbUQCfe0LKb2ADLwcdTw3Gp5+OfkkxSiXaHpL0SdE8Wy0McmF1JUKfA/a7EQYrY429KugjdbWNtdmacpy6eOPHBAmA9BhylybO4a5tqA4Sk6Za4v162gLSlbQFtTf1Fybz8xPHa0WFcrPPpbBPuye2lxbHW2Ir3lo7rIj/abR5qgYRZusyhSP60Vmhfj22vC3L9O39M2en2uTgFW0jTqeGw2TrpI5BbmDPaJt7lwb6sS5tmoFvh+fa0PL8bk23yuZPdeWpYh3SIt2cD2q/3rTK2cDmzl3SFF88h3SfEdG95Ni/TrarOQY2oL6tTukVJiPNnTzwo8ksoXxvcESbapAf4+gzeJ7HtoLTQvrbhJtNqkmwUbRJquGO6QRbdwhjff/9StjD7jMRFuIBPW1jmCvrqFtzPHKRvMV6aHqAaI+oI3Y2/QdUtDGHVIL/d/QhkuqUWPxQS+LwSFWQxvLc67wKmiJqmOklTdaJiVcpPPeRXdpMYuWFZlD+lrVfZNW7ytoS+uVWdeWruX2ucCy2vZhXVtQ3ISqrWujgqaLPB+mzi7q/zPaFBeHXVAoWYqAzIX6GY0BbeBjPtpoW0LbTRIFfCqQMPt+/ovcPnc4z7pryX4dbQqo1w+2+CxooyD9RtBmU2r1mvp1HG2pphdLiWllL4C2qIRxL3fVKnHHJSetmWgLkSC50wKTfb5SHNXRNuJ4ZaOO/ec/VdRH2aE0fjndSqhTWddWqxDRJllZgvhf0FasNHXGhhR5WHMG2uzmPI3g1U4sOmZqwyfL9DXvq2ijgaSfTy52XtYse8VfTXEelOAZG47GM2Ik5wn1I9oko+MfLf6JrshcqO8FZgFtrERbB9oIStakHpmfMGIhfnwaoYY240HGzeLrP+UEpN8g2nRGpYyhDa1jL4A2dZk7KrgQvogFZ6ItRALTaP+GthHHKxv10MNDBKwU0IbmM6yEG2mmAqmrFSLaUmRTxtGGPdw6oYyyM9DGPcBznjlTovIUm2eZvTqcJ8fS17yvoO2Iaz7KDWShQZucU9yHbFXFLRqk4mjxGVKdL2P9iDZ5uuN/qGQUAZkL9YcPYVKb57/XhzYG9YmutrHO9dytjc+QvvNz5TaCXU6CW3xjE64g/cbQhtDHylVG0GZ3staxF0Bbfoa09G7M5y5Rq7PRFiIB5L725r+gbcTxQqN2uTzJxTOkAIWn7GdaSQMmwIXUtQoRbfTB4Tf9x9sIRBRQV+wSUKujjUTBShxxfkKb02ek7r3c61RTXpi981tSE/j5/xrajjnIKRxyats0CgdtOiKZP+qK+1hk8uBoZeYPVYj1Yzw536xzIqNkFGEgc6k+iSOovc7MHzFZBaDKab/VcbcMOtw5gSXsFXakiDZX0MWExbfcoA3pN4g2yyRLScZRtB0jl0sJJfaKvQDaYgIMfM0JxPdA5g8iwb6V3kjDKtpGHC80aok9gMNMoG1dmT/I8YM6SB0rBLQRXzLULTbmKNowSI5dWxldl7Xti6+RGy3pseQjK9ePDJ//j6xfZLyaJj7SLzNf24SisfaoGn/cbNyqfG1kNkoXHZY+jPBVZUlWQmbKKi65sB2tmkYboVH5fPMBsu+tj2pG2pl7dO6eJD4fLzbL7pRCiNdrbFuWXS1AJMZjvPPxAqx0/qsXvXK207SGsfFKLtnRtqHicbK9KIQMttxUgMTMTgrbqeIj/UL3Rpg4WqjEAxAQ/bcHbbqwc3ojBuY2cYkBXbstwErDO5LYYXWXXNo+pG2jTebzHZxwKbTJHa0ozN06feEk8ZF+uTtaTcV8Peu0r+i2akcrkdi3UtnRSvNu8ZpOVZZgJVYs8KALZbpLdrRtpuBHnPexKSG12QDhUTDNyetqYKL4SL/UfUinzrZ5/FIdIYgMW4Q23WNJN/j+xmYGM4xVxYZFWElPf1zFTTfKai7Zd49fctmFDm/Tq1pTqjUrdbRNLz1Adjxo2rRca1bqaJteeoDseNC0abnWrFSirZdeeumlldLR1ksvvTRYOtp66aWXBktHWy+99NJg6WjrpZdeGiwdbb300kuDpd0bwMsvu9DhbXpVa0q1ZqWOtumlB8iOB02blmvNSh1t00sPkB0PmjYt15qVOtqmlx4gOx40bVquNSt1tE0vPUB2PGjatFxrVupom156gOx40LRpudas1NE2vfQA2fGgadNyrVmpo2166QGy40HzOzt1TAQADAQh0L/r9Om+5RYLDDTN1SxZ2x2BjEfTNFezZG13BDIeTdNczZK13RHIeDRNczVL1nZHIOPRNM3VLFnbHYGMR9M0V7NkbXcEMh5N01zNkrXdEch4NE1zNUvWdkcg49E0zdUsWdsdgYxH0zRXs2RtdwQyHk3TXM2Std0RyHg0TXM1S9Z2RyDj0TTN1SxZ2x2BjEfTNFezZG13BDIeTdNczZK13RHIeDRNczVL1nZHIOPRNM3VLFnbHYGMR9M0V7NkbXcEMh5N01zNkrXdEch4NE1zNUvWdkcg49E0zdUsWdsdgYxH0zRXs2RtdwQyHk3TXM2Std0RyHg0TXM1S9Z2RyDj0TTN1SxZ2x2BjEfTNFezZG13BDIeTdNczZK13RHIeDRNczVL1nZHIOPRNM3VLFnbHYGMR9M0V7NkbXcEMh5N01zNkrXdEch4NE1zNUvWdkcg49E0zdUsWdsdgYxH0zRXs2RtdwQyHk3TXM2Std0RyHg0TXM1S9Z2RyDj0TTN1SxZ2x2BjEfTNFezZG13BDIeTdNczZK13RHIeDRNczVL1nZHIOPRNM3VLFnbHYGMR9M0V7NkbXcEMh5N01zNkrXdEch4NE1zNUvWdkcg49E0zdUsWdsdgYxH0zRXs/SvDQAqWBuAINYGIIi1AXjsnVuoDVEYx2fau73Z9nE57o47uR+Xg8i9XJIjRa6JIuRFeCSlKA8STxTxoDxIlHtSkgd5lKJIQnlTyKsX3/rPrDXmOzPMKZc94/8vMWuvNWvX/P3W+r611uwCimijKKqAItooiiqgirsATDWC6Coqk7ivjcqX6Coqg4g2Km+iq6gMItqovImuojKIaKPyJrqKyiCijcqb6Coqg4g2Km+iq6gMItqovImuojKIaKPypsZ1VX2k/6P6DOpYpf1LU8fC8WOGDM3WQ2m0P6IpS70l39573swpvpP0cO3M+mkeRbRRDarGdVUC2jRxBo74G2gTik0cEEfbpKleadS89x5FtFENqoZ31cwpilSKTH8ebb3WzlgXJ+q8wQa9uwRwFNFGNaYa3lX/HG2lJe9ioeeK5RW5RjlD0lyiTZ57TAkmqJ++11m/ZK1fH4kARPUVDqJ37/3Uv6VR3wb/6MQnuP38D4t9f9kl8+/6/c0bPIpoy2TV+q5o0gb7+aF7unSXD6giom3Fcnjob6ANfVlwXZz1C//2mi7wMwLGEDaU9iz2oYWf5WryBMEdlT+0lY9v8v3KvivTgDBf1GxGsfZb5ukuO7aoA9r2tvYXt3h45iaMLB1YOdDdAdZD/XhP7abOQoyCiD8nDtDhKFTuWennUflDW6hxY2OMUcPj30Ib+rKQ292U1ndkaLgRTry4MWjlL9wqV62+fCJ9npzlUblD2/yPPlQ5vChCmwxbrT6E6VRoDTfKmQeOEbFr7x5Co64+hJgyAW2oOdCHZHzEHSwcw6kamkJ9u3H2T7RlUAa0IR7AoJmKNlfJghDuQxQRjLnVGoppyvyhrb7Lb3va5NWPDvTN8AanYSCr7FvjmeJKP20NIZrM2tG20k9adG0+KFRs3+RXa4log0XM7UoHFkgnmO1JqZPcAVFoiDkBK0W0/Qa0wccwXBra3HhqamF6tjGKRuBG3Lfcs2WjR+ULbYIp8xRBn/6zrNPkWWLYCmdozhoOP1IVjqjWwikX/CClSWgDP8U7GP7EI6Z0ztzESZv0PIwRaVHQVjqyciByGtG2o+FeYgLDRYZiKDjCl3AARoENo2xGVL9+erGUba2PFIMBbfO3t8pNt8b66rUWToUfkUnzmw82qcboT/qFPTFmIxpR7JMCKldoA7qif0Vo2wbABNcKbfIpGgV/zX8MWwT2SkYbGGhJKp86e9qy4FPbodyUKgDayi/CLMSy5z/gJjGB4VIdMq5hlPQNY5B5VdkM1Me+SBS1LH0cou1cWHBnmuvL3Aj3gR+Re2kb7OnG6Ee+C5wo3jOXQHAsZTKPsUTO0IYZt2WKTKXgHDfozp69yU9Am6BKHIPJm1uKmC2rDiloc6MgPCadlIcF1lF0taDjAFkItEmeoeXQTq/0erE/Y53DS2ICQz18+cs31sJsSmUzUD/IozycVnqwAKdYADO59toXxPvCSBpeyyDst0mPqnFoaLGlOBFfXvzZZ1D5+AK7+cPCjsoV2saNbR7sCh3abAwAabTBchJXAnCILzb5UCrapMxJzKTQhkAWir4GlX+0TZ6AqRCWG6OkRnICA7JJWPHHvBOVfmEcqLIZqG+YBUoJrizagDSs3ju0OaPhWqpiEUs1Dr+2MBT/GcL44859u/nDupIDbjHQFiyQtq2avUYHpNHo6sLSrsYCs788G9lptLlFCaKtcGjDc43+ZT2UnMCA7NK50G/S9ilSinhUZTNQP7ZIH6AN1zYKQV+ui+D60RnLKd0YFaVf2wpzQDPhLJ9aDKriBjwmkzO0xQJSGZjgnGBfz5pgJq7QZkfXas38wfJDy/lFwa1S0Kbs66b3Oh7l+FgktAXOCblgxk94IjmBEc/iSs2JnyRJhnhUZzNQX7qJtlYGaBMIRrxEX27gxrXdxyTSjV2SD6MqbmJSdrEtnDA7lSe0JS8jSIVqLSzVaHNWMJbDnN/8haf/I9pMe1xJQxu6AoRXbwd1kuNRfMat30VAG6bmYaFDW3ICI57FLY3a1q8+UpogHlVTftQX04BZuLddIf0F2ir7WwP8qcbuy8bQBr/C/sPDmtyTlDO0ddz8AVIBbUGpQptrdnc1nOLQ1mttZFXwKpjJAW0uv1LfBZIiawu5eIObP/4TtCUmMCC7rbHX9GrNxKKIRxPRJvfqLNpazjeF2ydVY402XNnvhN6INi+PaBPYYKtPfXtrsGV3lHn+gias2GMtIQFtiFjNAhcayDkGDMVAm8uFtA02i1pvb/64ZffIJqxRYXqfmGpD1oOHWgqBtiggxRweHkpLYDgZnEmqbapYdMQrE4/qCh0D0nS0uf3fuJYrWFw3jgZUjLj4EjJca7QxIM0b2mIHrTCJMgetsJUIhafwuBXa8PQx/8I2SCMB5A1/eGgGd06r7dHY+EGr5vdyZVfZ4zaybmO+thBoS1xGSElgxDfH7ugmzhIfvBkjNXU2Qy0jSFk62lwxrkFVWdhXjV0z2wpdyvjucnDuBlS+0IYJlywIbcUFjgTL43SFK5bPUylga0EMfXZnd9uqnWKUzRusVTHha96y00352y+3olb8ZAusDp+FkjwL32pUCLQlbP6o1lISGPHNsde3DcJa6QlsG1fZDNS3wQXKEtGGvrBe5ciELoWSurGdlLlJHrp80mTPkHJxK09o++eq76rWUj7gS40KgrbSaElV7MRW2xnrgLqWjSkJDBUOAIHh3yqbkbJlV6MNfUX7v4E2LHiCkqqx29uBOZrtUr56+YKpzzMyRFtWpb1ykq+iLBTaYgetMBXC4aekBEY8HDCnYvA3sKSyGaiP7XFB0dkxHdHm+nLJtOgkhMQgurE7YYWNcy52ga7eDgNTZoCJtmxyszNdzLfHFAZt6iV95hSU0ColgQGFBBTMGCphChXLZujj8XIafm8C2lxfeGWHQxv8JScSVGOQC8SzL0J1XS7ECX58GfqSaMsovP5PSXzLU8j5RtvfkSBr2ddp6vBKis0EcD9vjFr2pUZI7yX48iRfalR8V/0m4cf6lPhjfURbNrnl115rMcfL8JbJ9Mb6VZRKfBUl0UZ1VkRb52UjRJwInX8DybN0YUL2q8Z7X2JEtdMzBhNEG9VI+q/QZpcWwsOe6dK/a6AbqwRvx1+LBPoYTPwXrqIaU/8V2sIliuYta9JrRD/Wl9o49cf6nPhjfUQb1WkRbVTORFdRGUS0UXkTXUVlENFG5U10FfX3pdFGURRVFBFtFEUVUEQbRVEFFNFGUVQBRbRRFFVAcS2LoqhiiWijKKqAItooiiqgvrN3ZaHWjWH47I5w+M1z3HGFI1PZFxzKkEwhY0oZIwlFhmQsynwjQyjlQi5EIpLx0pAhkqQouZFM93i/Z3/ver/n2d/nLJx9nHOsr/7z773X987PetZe31prv21qi9YpOtDE+J8PNKBagUk60IF0GZ1rs6cjJbQVmE6e/YDZ2SudfYk0d/Yb76PdDmtPWe0MqRr1Z8bFUzttdfq6vyKS7e/RKlLbpoc/G6htMgZq+9vjxI+360dtwNlKDUUu+iSPPIk6p53e+a/e4lj+dYagc61TG1ycPbVpIVaZ2mBthtSmY6C2KrWt1+amCKMHtmcRIzQGDM46qDGHTSt4JBb8nc2esGbKnV3USGdEbapioLaB2tbDWCvUhnY8A7UN1DZQW20M1DZQ20BtG4barD3AKC1SJMsPpuZOp0+Cv2fJWuvddZS6j6TYm/2+vHMP67KXts/fccPC6MjTsSEEUyz7X/fD6OC95u9Aj+7TI6VHXLho7T+33NY02qQDw2wId7OuhF3u3oJf/k6JgrjpTBa8gcsR5pi1qMov0YvK1dnn+U0WpYDPHZmSZ7YxJ3f71jqmWvtRNDz9CN6wuEgkkcM6HxDFnk+87aWE9HdHj+AVB0aTk93P99jziW3I1mbmI5yQcNSFkJbSRYV2eXpX1BGeRRNzn88pwZCERtmDUBK0drrYlMYGzRXK7jjL3uDHPyuZ5oF+Yx6yIuv6+0cdclOv2sk77tuerZ7M4PZqGIpsKyAZsWy3CwWjsaDWJ54LXdUKvLsHdGKHiQC6XRuf47ViDtWLcteqB1mqXiSlUj3YZHBieNgRqe/fYp7wKdnAj7hit4YsYdx3ffPdJqUG+yXNtEA1O2rb8RHMw4//e4vQV15tUNuRS7YVzTnnr0SjzoPvB0RC0OI72MB36Jm2vosJW2Xp3Ptz81OPB7Ud/MLCZDMJ+yxDilObA9Byk7y2gzTysvv2m98NC1hrgTc2DCWpVz1evnwQ+giNMMl/OZcso+/LKCuxtn6A5+i8Dxbx39kVcZKYM5GnFrMtewO17z0f1AZPkFsJrJw8sbvnOWQLVpBoCqfigktr6ZATGL83U9t2e1+wa6Qy5nNKMCihVHahtluuHdVzpdSWf7MdrWQrmeaBVEFTDVkPLUxRG5r3YGeeI2oLcFM16tRWBqOxQPljizmAWgUe3zeorQxAqY2EvXpFuSvVg+x8UT0qdqV6sEPgFGqLSOGumid8cmXNLS8jZAnjvuunazvZAFFbA1QzOyE1Q0dZ105DR2pVZ9yfqBVHvwq1pWOAdR/e7TALKTloxzHbQIKmMXVUNs787aC5TS8umCpIW/6OPH1ukzE1qA18DbOlMGb95LOIih84DDA0AWDYUG36kgELzLzJzd73PMeMWXN6czJ1FN10aTJjXw5s58cgy+n62nHn4waC/Q80hSaYymiqbLu5PC1OEuYHDpbwwTakw9VXR1shOwoyTWguY6YosHKyKbFMsS3LtOXQnKBw6i5AulI6VOioXCHU8YB9kdP0H82nlGBQQqnsckKKJvzYoLly0ABn+NptOtLXg0PPrGUaI6jboofaCrJoKQUnpBZRwgC+v5ToJnBTNSonpBSMxoJgdr7c8VapAJ3tlQEotSnmIBvlhi2tHhwsqkfFpupF8AROKghFCtVqnvBJlQUujpoohSxhHAWC7zveZCz4xpLHj79NUM2M2vCtxxnY/iAFtqlKbcbNfjS2WBCFbSBB02ifBNzs0ABpNClG/3ZQW5glqxADupnabANyd+TxkAYqoA9hmDdWZJRkp08M7NBmvs4f882uOVhXRwH7Ss3kGwBMZr2Ib1qcJOZKH7ABHnjuPBaY4sBisttlV2E9ST591m1FOFUXIK2li/NOA6ZTm3/zvUAqRinBoIRuV5RdqC02sP9KbSnu7JedYFUyLaUGjaMn7BSyKtSGQx187RRhK9WaqlGltgimEot7mxhy10oFiNrKAJTaapiLcsNdrZ5j1avHxZbqVcEp1BaRQmrKPOGTK5twAbdsQ5IljKNAEQ4MRLGaoJoZtSEIHAwSk+aBD4XaXAnSi+B9AwkmvciOnWvbcoJPQk3cEpkthZGVsEtnpOk48fS3VmDQnFUOM1FxeONB7b2w+dXfX+J49JHDJ8t5VWH80hK+PSHJWS8cmRYnibnSB6+xLJsecfv42UXDLwcWk8Mu2bJzh+O+OB8YL8KpugBpLZ0nJldrYhYYxec8n1KCwQmNsiu1xQbxX6nN/vpWSxllWqhNPlBkVakNCxT20tnf51BgUY0GtUUwtbp3IrClFWBqKwNQaqthzsvtQ6sHB6N6XGwNEjYFnBQDIiXVap7wSZX9EemmvEVWMb1bKRyPb1ggamuCarZXSBGMYcpjQAqE2vwNsAc5d40EuxXDH9L7I43dIA0Nzv1kNoR9lhdKzkiN+Iy47D/71yEK+YI6f7fZ5+lM/+BTLgG/+sgTyPIcvnNjVKltWpwkyAfkhpGE0880rOwcWEx2u+Lq/JVmxVZrDW9FOG0XpHTEsfkyQv62i7V2ns8pCWEOTSnHw6jnSqkN6zyejJ7UpnE1qQ2BMWSwlQPTaii1RTC3aSy+kOezKxXoS201zBXlxtDqwbWoHhe7Wj0BJ1EblQ2v1Tzhk5B5slJbl1WiNjuPxiBqa4NqNahNzfSnNq0liNu4HMuEy1FbCBMDqCcGuZ22w38WS5va0kWbJSyr6q0BSm3p7NhIY/zz29s2qE3FSWI5att0qVH7ePzclVv3oDayhQP7DQtYAo5wWi4oQlrUht0z/ZP5K0NtmiultsDfZj2pTW22qc2WzM8BEtvURtVYjto0FqW2SgV6U5tiTsrdpjavHidlJahNzRM+Q0Eoi1llVjtXrlo0nhyPT99+LVEbzDWpTavvGyDIeuMqvm3XE1KlthCm8zb1125f2G93++8100CVwx/h1RvPHaXVJUxqUptd0vGdpU5tKk4S5IOekCIGo3X83+OE1G1Js1QKp+kCV6B1QorDA479WjHdOTih/ahN/Vdqg40e1AYv8YHE1aY2gOoTxNuktqhGD2qTWPSEVCug1GZT2tSmmJNyN09Io3pIykpSm5onfAYyoUGorcwqpmMSOBLmmiekq0tt9gerFXaAgw8RJBYIOSdYEswbSLC7IobtCE8uIwi1kdW8Aov1VcXWFs8+nXQd/oJVmyrHlxEuhQYnFDMKbxCGIgEBQaxKbRXxkBD07HCIXfbBBDof8ysHHFhM7uySLet4f0529uQinKYLXIHWZQQsWH6A3Y7mU0oqlxF6URv5r9QG5jZvUP7l1tqQKr+XkfxsURuY4J194XGL2qgay1Db7hqLFw7yux2mFVBqKwOAg1kc/lQw5+XG9tZlhKgeFW8FqG3KPOGTkem42CczLmEcClwPLjG2LyPMltosZEqMhZ6uCtuVXMSSZ9EdBOFOeXGaBKHRBe0s3qzIzR9CbWSVZvHY5enLJ/nBAghVjm/+MA1Y6sMlVXuTLkNjx1Jq8ztU7IbECrXVxEmCfLB6WRR080e+pn7DgsXCgXWTwy7ZMlHbnpbZ7NbkCKfpAlegdfMHdpjLAXSaTymp3PxRpzY/MtRyxdSWb0U0b5LCw/eqZToGNljikqZda35CI1EbVmLff+AwQTfVmqoxMU2xUDAciztlGXFHtAJdOFDDAUTd4U8Fc2W5mzd/ePWkeL2pLVxUapsyT/hkZNpXmVtRxkzShPHsSr5f6cYl852KRaBaWWqzMBGbxzniI7bfHxnrOjjK4oNnnxZqk1t2QxAakR2/zw9Z5ntWyWwhzLPMUFcuZNjemQhYjyont+z6/Yy421DuRtS1Nvh4td2BU93hVJwk2Ae9ZRdrbbi78TEc+9u37EIJ2zIi9DsjKZyWC7XSUYVQOiQP1CcV052DEsoojGKM9v+ANmiu4JvjLN+yi/WcWqaj1MBAaBI/A7lEbSlLIe9zPioC42rYNUYYj1gOpCg1FgRzzWJ2pF4B6ARXUABXLSLsY4+PtTbBHJW7Vj3o9OpJ8fpTW7io1KbmCZ+MTHPLywi3KKux1oYPj7vCDESxCFQzpbbJEYESg+cr8IBEaAH5Xo3nPcSdE/lBKxeERn9QBA+beJY3xZNGbDaEaRbj3XcFnAIztcWzIOf7S3pSCg+1VKgNT5jAx/T1v0ZtKs4SQq836oNWR9yJZ1JwVwIFFpOD2tgWUmKyFE7bhVrpqEJ+TnMGpmvFdOeghFZRiCP6m7RBc4USOc4yHqCwSW38oBU0qZ+KXKQ+lGF0c24uAqNq0GkBYtHFH40FTr+xZAouaVQg64QaCsCqYHen2hoxqK2COS63Vi+oDdXT4vWntnBRqU3NEz4FmXjQKnBBWY1d/1wgFmtQUSwC1YpRWxSIFa3+QEz9J/9+4Nww5tbiDxj0HrMvta5vY2yMNK6X6s1q9Ke2Ey6aAQD6UlosQPYaOEz950y8bke6IWJyWrwufl3k35QaJ6Q4VZOxfjlmnVWvOlaV2uY/t7PcVR5xJpgeLMM6cm+Zq/74aW4Y//hYYs8q5rXmtT9Q6n/BBNUD5vqltnVWvcb4n7R9wXWFvAA5jJmPWJs+eMMf9tOjDhUGWNfU9v+p3vqntlhHHsash6w1b/QxuTthaqxravv/VG8DUNswhjGMYQzUNoxhDGMYA7UNYxjD2KhjoLZhDGMYG3AM1DaMYQxjA46B2oYxjGFswDFQ2zCGMYwNOAZqG8YwhrEBx0BtwxjGMDbgWF/URrd384+C9JfWNzrln97dvoaeSa4lZR2HBSekSrPKFYytpFIo7IM9daHtTS9FKqxu6edrDSNJ63qnthM/7ptWlFIE21XZ9PBn9Lo3tbno6tdXE9JfYKNSG0KbEbVB+39FbQDZGqA2S8GaoDbLxoajtt5prXXsFXFNDb2G9KxqBZlVTUhbYE0dO2eQCxRyBtpnQG3istqZJbW13dJP1gJGko6B2gZqG6htoLaB2gZqG6htbqC2gdoGaqP3R3x39Ag/KYQ21Nf9MDp4L/+pcmtfgJEbXvoP8W/2YP59d/w887ef77HnE19WJU88CT+PnttMd+nd7B5TBQVoofil/Yj6jjclxfN3xG/2Y7ig1ebtYpapTb/qkluIIznd6wSEO8419b967wX8Sjt6MRRY6URh1LSdPgnmxqXUn2jeNHjTAfc5fhI+bYV9jAhYDaGt94NH21ZTTnnzuCS3PCmkQyBiL5oje7g+YhIpwe+yxs8aS1g96oI2GBEwJt149OjIn7qM5OQ6aPE5EIDBeHO4fQo0en+ASAUU1ZDYWQv4IRyK3J1773lovxg7d2iHMdJer95XRyMF3Z6BWfDeRoFKKPQ2CQgYbyog06R7rp5chGOVKmki4CvaaXRNycl54KJLc2AHnwe+FJ4YFJ+Xd3kfAcMTz4Uq0RKe+S4XvhG1qWvI97+iNu+LdPhe6NF7/2h06JnWqcabTlEnG7RP8q48FjN+zvhdo69zapJoU4N2OJ5c/i29zc/C3nHkkjTFQa8epbZbrsWs887GL1fm1w1qu3sPVw9UIML4pUuhNjRUgpeTYKD5g8VsrNLIx1/nxEfAagj5fGFBWnIhbx6X5racRNIuELF31LbnU4tktUwQuYBGGhmGlbCWrcvh94LaImCblJjjsm0iI0xt8MTDn8JbhtujHwa1FamAIrLmI6x18KtEnp078rSgtkI7jJH2WvUOvWIhO+P4dkyDTUtUOrVdM0lVTkQFZJx0t7TjfQtZ0XSVNBGAEl6fenymNnIeuIg0M7UFvhSeGBSfl7eXj7s9tugRkpbwzHc5gkBQm7qGfP9jagPP2JcU/B4xOkamNomJle1IlA6OJWhSk8QTl0b73XbG6LjzU/NBC9pCGqXPa5IWQeo6uOnFhdyYMlSZfPrB8EmTx/SFY/6Yxd0OM4GUCyP/ygmpHaO6WfZ748m+va6ekFpyTp+YBSomzQ9NFokKrHQdn81+OmAceqaJ2tQEIsuJxZpmRfvFvXKDI3ttDqSMmcIyYDWErNhWZG13y3TkzePS3O5eTiJpCFDsTm12+PNwMWgSudB1xcZ/Ela7LmXnSwrYJqWI58qMELWhs2byLUU8jTcHDQKBQJkKKJpCIltz+NUih3PU87nQjuKT9lr1Hr38CaTOXaWGmYxKKJzCXgVklPTACTKMSk9VaSoR0517yfkUa5nmclWHQEgAw5D4kMGePu58uUdOWsgzZEMg0CVBXUO+/wW1uZOIG+3uAJ5vds3h2IfUNdraXr+GDtjJ/0kxMaUmiZbQCT/RGTt6hOU4bYNxt3+RslihWKitNguv69SGY3acIXRduqHCsRJ5zgrsbAvBdBpgiH1GY2+kIjdIo4A/EUPIylndKcIBRd48Ls3tduUkksZfih2fcLgYNIlcSAdZBIHtEla7LtSvnCqcG+KXGSFq6/YAuDiFN4CGqK1MBUwrEsWaw68WOZwjaiu042yQtO9Qr17UYlftnV/mqKO2FJ0ZyomogIySHrSRO/dDiVRpOhHodYe+s5nayPkUa5nm+Mv4YnhiaHzwsJ+PgCGKQlpop0A2BAKdCXVt15W4jHDE7eNnF6nJ88gHnOgQGfjBLAaySv5oQRFD0crV+KWl0WQXgmLbfcBB1csINGuypDIe37DQojYrAeCFEoALN7/6+7wwJtSGiuVhYq3m5Qif9ld3jVLlhrhTOcSdloiXRLkmN6QdmBG7q+BwMWgSuWASNjc3P9Ww2nWB6vyasZF94owgPGcqW1+Klp2KN5jNYpxbT0EFiWxNVtg1PUxt2pCTtAtMIivYj/NrQNS3ESrpvjaUEW8qIKOksyWojyq1EgGDXlQIk/OINdJM1EYgJGxgSHwQ6+sjXMIG0uKeUcgKAYU+Zv5basPXWoygNkOkD7wPPLllrp+NiuTJdWpD41pXTg1VocOhQoLcdtXOjDCq1CaN20G/n6cIDz7lkgq1WWC2zRPQoja3DwLBa88JpYoMaddmzluD2mhSKZ1doNjxCYWLwZNICU4gHMgS1vJ1wXcOCjhPoowQIo74AYsnYDfBWy9qqyCRrWVjGnk/amPtWj1kyx3B/Czm6SZUBrURx1dARklnnEAkqtRKRGfGLyOw8zDgaRZqIxBKD+ZGfD19tPkeOWkJz1yHQqDGKytBbZsuNeiNx891rds9Lh89qU0l/bXiEV+jLdLxzzgn+AfUdtWiXYIaj0/fvie14aLcki+WKrXRzvT3qY2CDEOzoraIvU1tNIlcQA9Lz66GJXVpU5tWtU1tuDJoB2dci5hTvPWhNk5vm9o08n7U5tq1eitObTDsY1lq8yo1E1GhtsJ5GIg0z4ba1Mc2tYVnriN8myG1wRXADi4FQQVgqMzNE1KS1GhdfacK4IEI7UK3NU9ICUTb4zIQZvWitri/wl1gagsFm/39E1JKFRlSaqO8SUJaJ6RKbZtF7G1qo0nkAk49PvE9SsL6q7rEIYexsdwJqT9dc3TIBN5q1Oap8BSQtWVPSDU9Sm2hHcYiFq1e7xNSpTY9IRWQYSx7QupVaiZi+oQ0nM+xFmlun5AKtTXi6+Vj+4Q0PHNXw7dZnpDGnnFGZxpWbE0QR8GIuetZ/Q5fRoB8VXLvhegKH1nCS19GZdJKAtD1V9QGSzDfk9o2XQp/oUOpLU1LS6n4Lllfa4vwoYMvI1DAP6ohws6ylxFwvaWcNE1tFHuL2ngSKbFZVj5LciWsv6pLXEbgCudJlBFIZJ1XLcKW+8h4q1FbmYo0tYJEsubqNPI6tRXaT7apHIvAxHEIdfsfCNW6zK7U5ovpgb0ayCjpdMEiF0mrNJWI6csI5Hz6E2luX0ZQamvE19NHMwSDFjldRig8g0GCQPMywsp8a0sXWU+8YcGpLdo32nVYRJPn2XXzdMF2p0/45g/4WZW0ZNx6VLoE3K24YuQ7AuyeQ92F6CaDPCBIsyZXk+3WWl8l50V7pTaDf3IpLfvE5RwXhYFkFL3rW9/aUCS7zF+5+YMCVkOEHbn5A3FpbvniPElDYLOIvU1tPImUWEwPvP9Anidh9asLB5wnUUZM0nUaOSfB+SuPhm+KN+A6aox3RSoQmSJRrDG1VdLj2vEmtOMKKceiMMFWM+RYtvlyc0SN2nBXFjq7O7UpyCLpcvPHNh6lVmkqEbWbP8J5uFWmOXYixpdSWyO+nj7ihilETlrIM2SDINC6+WOl1tps7PhYOhpAYdyHijUSvU9yK7llF9giSb7TE3fr4RoR8odjDVZ6r7a7jxgefMsuRhaUtbY0jrvCXgNM3T6N10Jtccsu7ogNrLio3U3pXjaorX3LrgSshgQ7kbcuLs0tTWJpCHxdxt5ea4tJpATQdasaVr+6UMA+iTLiiDryBhwF6IZbwRt863KBd5EKREbWarfs6lqbpse1f4A3oR3GWHtUj2+kxVbMl5tRqyekrx+d3YdvFZA1btn9ZQ/3Q6s0nQhHid2unKmNnE9ulWn2BOstu4qNRnx9fbxm0SMnLeGZ73IEgfotuytAbXgOAg9D4UIuFPqDHHjmgVch/Fmae5Z8I1ObSp54kj9uEQcpPE+BJy/SeQ7BwwTkQSsXpB3tjsnDMvhWTt/x8HqK2uASJBgrLhq965vUhvDj4RhIIBUUsBpS7ETeIiGa25jE0i5QxN6kNkoQK/HYKmH1qwsF7JM4I/jiZ7eCbm868Xk8vCR4g29dLvAuUuGRMRLZmlLbvKan0/4m3oR2GGPtUT19/GkbwXfGdH2t7cRzLbx44kxApkkPS+YlLM1JleqJ2IQHrbprf+Q83CrSzDtR4Eux0Yivt49vLCFy1RKeeTYUAj7EtXXyePww1sbAtZ1/8xOh/6OxAg+Ez7hK4J5VHOrjGsDGQG1/snemITcFYRw/03UvN/uSJT66X2whXW7d7Ht2QrbshMi+C2Ur+5KlbNlTiCL7UmRLyb6VrSyhyBK+eOa5Z8wM59gd17n/X+I97znPzJz7en/NmZkzD1BjKV07/fx/YjX6ngV78X6HcKhN/ZT8laYn6IJHtxFqA9lCniI//8vA4749HHeIOIcISm3+PyX/ltH8hjsrETi6jdnQo4faACEXxf+8nfSWINVzqtP2w2oL/qcUaahnJQJHtxFqA1lCZgHBz8Pjuzz6nlP8qNqC/ynpWYng0W3MinFYqA0AEEKgNgBACIHaAAAhBGoDAIQQqA0AEEKgNgBACIHaAAAh5D9Qm1wYgxcXAQABqi1y66jjwd9WW4Fll6E2AMBPqi27dnyQlWTFYnAAwH8D1AYACCFQGwAghPipTSUcymyEambh4D1XeXNMfsvf3SlUbYDKiWtejIvTZuZyy0zaMdMxAhR0zfgGtB2w2hW1nXrVWmbYL6LTKepkELKOFmW4Etq61Mq5SNXw+8Bc7A06kCn9zVoBADnHNzcQP6m2r7fUVrSD2opdqU1vSc/XnN0hiPRMDk+XMwIcRl0zsKDaGL7YPb2x/d7FHmrTu7XQjimG2qzN1qnYOrVVTWatAIBcw19tnEfmMimlcqmaptpU2pnaZTtr61hpi2RYgQNCdtwKyAREOkCrjRIMcTKOxq+qye4VBep0Pp5qa95U1hE5TJlHjNx7dqIdqprK4xY7ZjMBALmGj9p09j/ei9hU2+dskus7cqInO/2jSqJI/6TLuXkjjACH4Ws4wEo+TgGsOy+1qVr5QKvNTo/IdeoTulYAQI7hqzblBsZUG9lKNL7WQ4/w20m7+S+V4zFzTgdI1IVcrFBULM35rjkBk5faJJHptWqNi1tqs5Nac50qhbZbKwAgF/kFtVFeXDm2NcnNYadE5XrMQ206wFIb20dRvLDOXGmrzUigx9hqUy1jx1lp5FStsBsAOcjPq42nMsfFdebh76rNCLDVxicV31HbsMqkqVq12hX5UbXpWvFACkDO4ac2do2f2twcyaSgH3ogNQK+VBt9R/HNB1L6ixXFB74PpLbaVK1YAwdA7vH9aQTqLxW/SAJhrwiaRig/v7Nyicc0gofadICtNgqM8Rh/pDy5yJxGYDmp6QbDZjwh6j+NYKpNNhPLewHIVfzUxiLjxR+Ha8dK0peUpqYALYflbhkt0nDyXClTqSqnwLcWf3ipzQgw1cb/0hJdN2uiufiD6+Cvda9NLheJjK8tWG1kWo/FH1avzawVAJBjfH/JLi96pZEuomyjpsaS3WL3eBEbyc1YsuulNiPAVBtLyMia6JZydgfprEB/IakzzhxrkzQeHJf1CVGqpseSXVNtulbqHUoDAwByh++/aNV4Y0E5ZiXfnXqVr5B0B+ee5Feo3I6cel+qn+OpNjPAVpudNZH6ZGSoY/wEWaB3ZfnCVBFWm/suF782VaVC105ytpS6kupFK7qUi7DVpmuF2gDIObJvK0oMjgEAQqS2fIXUrAV1vQAAIBxqkxMF/XjWAm9GAQBCozaa68RWHQCA0KmN5wN41gIAAMKjNgAAgNoAAABqAwDkEFAbACCEQG0AgBACtQEAQsi31JaKZkg5f4doNPlXornlzg+QjEYTzt8nEU192Sz6DgDgSwLa1GiQyFBq1mlvAySSzm+Q73q60d+I5pbPLu18ny5lWo92/PhjdxwZ/JFb+mDePlFi1kpu4OTZox0AgElwamsrFCUWbfKIvTO30W+prV7eRn8jmtVW//fV9qfuuG89/vQeXBKSsascYkG9s5scAIBJgGqrvyVKLJwcL7HZQy6n8map2pzUAFLb7z6Q/qE7jqyrMVN+mttE/adDz6yL35Y2jQyq3t0BAGgCVZsriNTkePWWHnLJWrU9fNbG8SHwFvdutt0h9l/Iu0b2+2qX4qYteFwf3TYALIJVG9OkT1nVbYuq4ftkk3p519DXjFf3J0mXOsbpVMo6x6L4MkBfklDBFBhNyZOe0Xw6qb/PtUQGzdpgFmmT+tzYpPkVF2RV5nvHfA3BJziM/vh8EHm2pbmdfZu5nbeM2vLIbhvfJmYUAGCCV5t0RZw7GTd2TabTS86ROCKL4/Ql/9om6IlVlBreyjF5sHykELFFKzND/mOPzx1JR6cd5sbc16LEjD222o7RFWLF8CfuwSiq58TzjAXKtvkwWR5u8oh+uHwfFfyUT3UpM3vwSDFlppNM2EUanJ88R4gpJ+6bY21c4qzjg9KN7Mq87piviW3XMxkUcXDenBIzNnh/EE0utN5g9Nq6ncy7ypEMXr+WjTeibBsHAOAErzaWQP4aLen3fIBgYmc3GWqbOq+MkJQ6aBqknmCKTeIHubE94zyG3oh1MZkPlo401VbULXv2E+kB92DJe25H2Z77BFF2RrWvoheeyrRoRl1u5VI6nL3Bo0jFi+uCi1q0ia+/OVqXeOgdlWhV5n3HdE1sjH4+pYg3Zci1Ph8EK4yH3OKzVw49sy0+e4urvNvPoTYANP9ObZGJZVqvmRa9OndE9ZbG41mLEa1P1Iru2XXJGBnfuS22duXQ6NbJcSqCHCCKzbrWbeFrLpCKGTu8W7ddp4SpttVzWq8ZOnThgLKbpQZ6zVg5beiZeWWmbOB2UGHSCtVnfhndpEGx4Y+GTlu4jcJkK2PbV0ZTHkUqqMO19lG3absqc1tZbVzimiO1Bj+jEu3KfO74S7WJYm1qeX4QXGEpV13Hlo9gBx93vdsn3R0PpAB8QfBqSx91iu4usTkh+1T10hP0oHrRbXmHR4lu86h/ouhbpvpK7rPkb11YXhk7e//z0c4BpTY7rA2tNtklqiO7MxPvLqpGY+xvV0aJWm3TGXe0Ps79HLreiqa/766dFiVeNm29gVupS7SLdCn6rJS8heQg6lkptd3okz6YkK2rTCValXnfsa02jnjueH4QbDB1k+NHCcmSNQnlPHTXAPAg8F4bj5hfXdi+/WvuMPFvNktAKNTl3BvptnXIkLn7REZt7rRgs7ejeUD9vTv6ZIjo8Ahx6MTTbskU1yZcKE61gy1hR5MfhILaR3FUvmeRisi4eGxR+2upVNJx1catesI9TSrfrsy6Y3+1xT6xd8YgbQVhAM4jiTZkKAahDZnf6CISFCxFiqSDrS0OhWIHu0irCMZYh6DoYpUqtTjEdqhCaRwyGHASpdBkiVTSwUErFUtrSyLi5OLW//5318v/enZKM/3fIObFu/fuwft4d///n2gy8404K8Hlouz3I/mToQ/ZeAikqjphGIZQc7UNoDQmsyMWoh904RpLsXVVRxFWZvGQVFu9ihKCgLqCuMok7aHwPoRlMpGtb+OilqQugxLIOH8PoiOt4YelCHUKVcHZjF3+YW0lbInQwp5Wm2wFnoQeycnIiM1q05/MNyJ2G4YMeOf7nq06b5sYVoBB9rHaGIZQW7Xp9H5vImAt5XoG1zuo2lru+iUexQYYYaJnMJcKmtQGmqRqQ/zZ8QAu33uTsNau61fRHco2tDUcSvslUao2xD8nu9SH7OOfCQxomNVGT2YesVlt9Ea41dYwAvNowSEcYLUxjInaq+2wKB7M2H7z26jwVomorQkXuGgy2EB4Zs/2oDYeULU5CnDmakRtUejav50Khz7DWxu82Qhw3khsQ1s39MNkEIF0Mqo22iV+VH9o92bHxYuVnpCO6gkpOZkesUltsSJRm/FGnHXghBSutEJtPCFlGAO1V1v0fuDrR4g4hg/KagW/Ioyw68QIJlcCi2r+6oMAqS1s0W+BNqja4GAEv0wGtdpEUDRto0TAD4+bnIX9H7ca01Rtrtbwftc9jSqNL11ItZm7lMRGnBrOgeDWaGUYwQN0xV1qM44YPzpZfnNBojZyI2gYAdvclBNSnFBDMw4jMIyJmtWQ9i6sBCLL+JYSSrf6D+fjauUJZoNRoYlIPje0nmpqR2sgyaBImhAZrAcuteHT3bz5a4omf0AvzcOQOTEfXpoW9ZZj+Re92/OB7jJVm7t1rGTNfGqdyp6LxDOqNtqlxNdvzRSmILfDOrpOkz9kj/Rk5hFDO3kJRG3uG0EDoTtFGUaILMuku/pOTv5gGE0t1aYIpd87NQm4tP8qAY8rasKK3MNMVZ0GK4mdOu2exFsuqNow6Q37OSIpuyo5NpTXKbu4TQa1DW2NKbvIRFlNSM1dulOJFws6ZXdtPIBdjLjUdumIbzijm0kQtdEboVN2M3Jro7hMLnbm2hucssswZmq2X9tirmDLEicRHTj5nuzDh/dlwsKlLhte60Sxk03qrGYhNllY2w1tyookfI7H7qADBhKQvj8ljiK6tqox/9pWH6BkqeBchwxa7qIFXK13UkG4wuFVp3BKhSZpl/TCgjJCqgutotmELN0iJ7t8xF9AYteel6EyS1+e6UYohSHbqVn4EtbukK5vGS60Yhgjtdpl13ZvA2SrY23qu8qgoG6NP9tUHbluh7+poxpb9ULqxkk1uql1VP+G7c1d0ip8Uh7vt7HlWan9ETnZv0fcpgrzVQvzjfD1178hhflqNZLL4xnmEvh/I1SFK/tLnR6sRoCQR/Wgmxq5iRWXeVMjhqmE1VZlfE6968J5HRRkVR/vU9jNiMBbUTKMAVZbtTnukKGI6r+0yc3C7b+OnUJeCsMwlbDaqg2GFhZz7zz/BW8S/u0LxTcOWxsxzG/27tAIYBAAgmA16b/FzKBA4MAcu0WcefEspO28MUXc8k1LgbM+2JA2AGkD3iNtQJC0AUHSBgRJGxAkbUCQtAFB0gYESRsQJG1A0M9OHcgAAAAADPK3vsdXEKkNGFIbMKQ2YEhtwJDagCG1AUNqA4bUBgypDRhSGzCkNmBIbcCQ2oAhtQFDagOG1AYMqQ0YUhswpDZgSG3AkNqAIbUBQ2oDhtQGDKmN2LuTVx2jAI7j7+mReZ7nOfM85MlcJCELGRM7smOHIsXOnpWllSgyJCVZyEbZkIWVki1/gjL0c/CTc166x/H9ruR67/tcdT+d6XleogqDNiKqMGgjogqDNiKqMGgjogqDNiKqMGgjogqDNiKqMGgjogqDNiKqMGgjogqDNiKqMGgjogqDNiKqMGgjogqDNiKqMGgjogqDNiJKqDndtgfC5yacad8d6ZQZtBH9iRbNC58bPrHz63pd/ELDsp0fNnZ+bM7sYPpkSdvu6iQ2cLq+w7FBnZ+2cnH42tSObd3b9T+5pmtLO+UFbUR/oGZGv4g2P+Y51S8EteGwpc03/NCubNom7E+mTdd+wF5QeWM3aCP6A/UdHESbrXn446Bnw3tLm6/3uSNptKkVe1Jp07X7JlzZ2CkraCPqvmZmP9HmibmlEZtaNjGFNr0qk7bee5cm06Zr9y17VNa0FNqIuu/4wuBoU+uemOncG0+bb8LdpSm0qbUTM2jbvv43RpJFDdygjSg9j5an7cblYBI2CbSZVTNLmzq4L4U2XbtPc+tygjaibhv4IChH27atwXbvfgptEjGDNpnoaXODUuV/jmKCNqI/J5unbeDRYNL6l6HNtmBJCm1+J8HTpjHbP2cbtBF1LVtMm91n8PXpn0SbREymTYfbPG1+uu3TZLeQoI0oPy2wx7QlT+nmj02hTSIm0uansp62YbvDd215dXXQ58PHt9d/r+3TQZ0ygjaiLmo2R2Y52oYt/+GEa6+L66M54k9pmzboW6EufS9J79E5tGlw5Wnz483eJ3ZFsp/q9/1CXhlBG1Fa/rSXp23UgJ9sJPYa2i9CytLmz82OGJJDm6aynrb4DjK15bA7yiukiwjaiHJrXm8KKqbND9qu39HWQoSYoc2v7PkZqadNAHna7HRUFx9fUqxmGUd3oY0or+b8gaA8bRr5aBNRK3Da7fS0+a3WkeNzaNNOgqdNl+hfJdsKHLZBG1FOvS4KNkNbfO+81qLi0Zxe5mnzk9upubSNWeVpiwZtbswWc1vesA3aiDJqZgblaJNg5ixar6ERNYY2893MP7C0+Z0ET5vGmxqP+fPI+hELCNqI/i5twkqnPATKlldte1U+GdrMW2fTpsGVp03jTb8fG2+jSukCgjairmlb9ni2p01TSP9rn0BbpFBYvSaTNm1BeNo0QtRozD/UKeK754M2om5p2/JijqctHvpMmlwMbdoTcLRpPqpBmx+2FTYjhTaiXNp0Tu2XtPWa4g6i5dOm75dHm3Y0PG36W3/QJEZQ/wU9H7QRZdKmx5Q52iSL5mr5tGnvIX+t7eTZaIbpadNbxYr694jGdz0ftBHl0aan3VraZJXYyKPNL2ul0bb62bhoJ8HSpqU271UMbmGLbdBGlEubPhLA06a5mo7YDry0Y9ynuzHbw+m0NZvHRd4k0rZvd3ybvKdNivoNED9N7vGgjSibtg27xJGnbe6s6IvrLsimsOz5oATadIRM3iTStkavl0GONqHsl9r0U5a1jwBtRFm06YP2LG1SQ1/Uo0Lcx6V42vTwNBmSTFu0lzlhv6EtiStdtvaBezxoI8qoeanPAk2gbdKel+Pcx6V42vwN+ccGJdMW3xW1Yo+hLfmYSd/BZW2RQhtRZpY2f6ytz0392dyaaWlrTre3BZvmo4m06Y53rfh72pyySm8CbUS1ZWnz92TFthnaunhct6dtWLyTYGgTygm0abekx4M2oh6nTY/dTqBNLCXSpp0I8RjTpiuHNqL/uXzatJzvafMg5tH26fyI3hraiOhv0OY/GyF/Oupp+3QUN3praCP6yN4dq0YRBHAY33AiXEhlF1L6BF51TWIh2FywsAtCCuFAC8EygVS+Q3wCK8HCwjfRV7CxEXwBIRg+1vMfuQ2zO47frz/dJh8zOzM7KpA2DgaEtKU7P4ekjW/nssxq2iTdOm0HF6dXRxKO+sO2lLZctmFp40MdvLLrpy2cMHCFVPrfbJE2jjCwe5cjUzFt4IQXqBCoVD9t7EJjZnuLfW08tmmTWvOXLbtho8fsFW1jVBTSBvI4MG2sJDAb7qWNb2iGEwZIh7ImZ9qk8dLGxJN5IQeZYtrAhHZ42rjPhYdKB61SrkAGPUMqtSamjT96dv/3hzr8LqeNa+eXy6/rDsPSxjF7VhL6aeNR8tIA2N7L/zI50yYVThtjH45G8UE00pEPWmXD0/bbMfmNtPFDohzwKWEee3KmTSqctvxNDJYYWEconzYuD8Wz5/Eru/z05tPxfmVXak5IG3/1vIXKNxuPm7b+5ra7L+f8aHOWmY91Meuu7LY+0yYVSxuzTtJST9pYSUC40SrOSPm3artj2bRJxdLGtI4JaT1pYyUhpI1fxsvjaWBlVyOYNqlA2vIH23YrShub20LaWGsgWWnQVtl81LRJpdPGEil/9lRv+AopspA2VhJy2hiN5bdtNLK2+ahpk0qnjXUERj7MVYneyGljJSGnjfeEeUrKzJazDRUwbVLhtBEw6kD0mKqOmTYmnDFtrHzmM/lcRVPXKSvTJpVPG33g+5FM5IjOmGmjrTltDNvw+HO3YXW0U9+gzbRJJdKWA3Lw6UFvuMNMddy0EVeQtjhnvfejXy6+X8KwtAqmTSqcNuZ9XM7HcIdXbSOnjbXNnDaeHQ/fE7dudn6y8Sn0Opg2qWTaWGjE4sm662Zvjvd797iPl7b8aKQNLKPy/N+v6rx3eX17IF5UcHzUtElF05aHPmCwM2La8qORNnx4u7PN9YGVMG1SobTl1/Vg0DZ62hiVxbTxxcysyhdtpk0qnjbexmeL3W6qtPFopG1o2xZ17PswbdJoaZvd379xOjph2lhJIG3bzUnZ9VYP0yYVTlt+3cZ+kEnSxmGCkDYcftvJWPmtiGmTCqeNcVsu2zRpY3NbSBvufAlPz4a3OrbqmjZppLTxyuoRJcCCH4ydNlYSYtqwOpvfNGRbd3UxbVLhtOHw4/xPF4pOnjZWEkjbNnE7uHja1ca0ScXThtXxfv+KqnXXVZA2XgSSNmDvcrNud1+/q2WbrmmTJjM7X578GuosT7t/0Gq5PJtfV225rG+8ZtokNcu0SWqQaZPUINMmqUGmTVKDTJukBpk2SQ0ybZIaZNokNci0SWqQaZPUINMmqUGmTVKDTJukBpk2SQ0ybZIaZNokNci0SWqQaZPUINMmqUGmTfrZTh0TAQCAABCyf2p3BwP8QQgIUhsQpDYgSG1AkNqAILUBQWoDgtQGBKkNCFIbEKQ2IEhtQJDagCC1AUFqA4LUBgSpDQi6tQFUqA0IUhsQpDYgSG1AkNqAoAEAAAAAAHgsRwQUx9jE2pYAAAAASUVORK5CYII=';

        return base64_decode($data);

    }
}