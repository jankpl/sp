<?php
/* @var $this UserDocumentsController */
/* @var $model UserDocuments */

$this->breadcrumbs=array(
    'User Messages'=>array('index'),
    $model->id,
);

$this->menu=array(
    array('label'=>'List UserDocuments', 'url'=>array('index')),
    array('label'=>'Create UserDocuments', 'url'=>array('create')),
    array('label'=>'Update UserDocuments', 'url'=>array('update', 'id'=>$model->id)),
);
?>

<h1>View UserDocuments #<?php echo $model->id; ?></h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'date_entered',
        'desc',
        'desc_customer',

    ),
)); ?>

<?php if($model->file_name != ''): ?>
    <br/>
    <br/>
    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, CHtml::link('Get file', array('/userDocuments/getFile', 'id' => $model->id), array('class' => 'btn btn-primary')), array('closeText' => false));?>
<?php endif; ?>

