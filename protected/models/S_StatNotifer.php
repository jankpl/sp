<?php

/**
 * Class S_StatNotifer
 * Sends notifications about status changes of products items
 */
class S_StatNotifer
{
    const SERVICE_COURIER = 1;
    const SERVICE_HM = 2;
    const SERVICE_IM = 3;
    const SERVICE_POSTAL = 4;

    protected $_service;
    protected $_serviceModel;
    protected $_statusModel;
//    protected $_userModel;
    protected $_statusDate;
//    protected $_location;

    protected static function countryPrefixTelVerifier($country, $tel)
    {
        $country = strtoupper($country);

        switch($country) {
            case 'DE':
                $tel = ltrim($tel, '0');
                if(in_array(mb_strlen($tel), [9,10]))
                    $tel = '+49'.$tel;
                break;
            case 'ES':
                $tel = ltrim($tel, '0');
                if(mb_strlen($tel) == 9)
                    $tel = '+34'.$tel;
                break;
            case 'SE':
                $tel = ltrim($tel, '0');
                if(mb_strlen($tel) == 9)
                    $tel = '+46'.$tel;
                break;
        }

        return $tel;
    }

    public function __construct($service, $service_id, $status_id, $status_date, $location = NULL)
    {

        switch($service) {
            case self::SERVICE_COURIER:
                $this->_serviceModel = Courier::model()->with('user')->findByPk($service_id);
                $this->_statusModel = CourierStat::model()->findByPk($status_id);
//                $this->_userModel = User::model()->findByPk($this->_serviceModel->user_id);
//                $this->_location = $location;
                break;
            case self::SERVICE_HM:
                $this->_serviceModel = HybridMail::model()->findByPk($service_id);
                $this->_statusModel = HybridMailStat::model()->findByPk($status_id);
//                $this->_userModel = User::model()->findByPk($this->_serviceModel->user->id);
                break;
            case self::SERVICE_IM:
                $this->_serviceModel = InternationalMail::model()->findByPk($service_id);
                $this->_statusModel = InternationalMailStat::model()->findByPk($status_id);
//                $this->_userModel = User::model()->findByPk($this->_serviceModel->user->id);
                break;
            case self::SERVICE_POSTAL:
                $this->_serviceModel = Postal::model()->findByPk($service_id);
//                $this->_statusModel = PostalStat::model()->findByPk($status_id);
//                $this->_userModel = User::model()->findByPk($this->_serviceModel->user->id);
//                $this->_location = $location;
                break;
            default:
                return false;
                break;
        }

        $this->_statusDate = $status_date;
        $this->_service = $service;
    }

    public function onStatusChange()
    {

        if(!$this->_service)
            return false;

        // COURIER and POSTAL uses stat map
        if(in_array($this->_service, [self::SERVICE_COURIER, self::SERVICE_POSTAL]))
        {

            switch($this->_service)
            {
                case self::SERVICE_COURIER:
                    $this->notify_courier();
                    $this->courier_package_waiting_receiver_notification();
                    $this->courier_package_waiting_receiver_notification_eobuwie();
                    $this->courier_package_waiting_receiver_notification_eobuwie_es();
                    break;
                case self::SERVICE_POSTAL:
                    $this->notify_postal();
                    break;
                default:
                    return false;
                    break;
            }

        }
//        else
//            if($this->_statusModel->notify)
//            {
//                switch($this->_service)
//                {
//                    case self::SERVICE_HM:
//                        $this->notify_hm();
//                        break;
//                    case self::SERVICE_IM:
//                        $this->notify_im();
//                        break;
//                    default:
//                        return false;
//                        break;
//                }
//            }

    }


    protected function courier_package_waiting_receiver_notification()
    {
        if(isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $courier = $this->_serviceModel;
        $statusText = $this->_statusModel->name;

        $needles = [
            'Delivered to UPS Access Point',
            'has been delivered at the GLS ParcelShop',
            'delivered by driver to DPD ParcelShop',
        ];

        if(preg_match('/'.implode('|', $needles).'/i', $statusText))
        {

            $lang = Language::DEFAULT_LANG;

            $pv = NULL;

            if($courier->user)
            {
                $lang = $courier->user->getPrefLang();
                $pv = $courier->user->source_domain;
            }

            // SPECIAL RULE FOR PACKAGES TO DE
            if ($pv != PageVersion::PAGEVERSION_CQL) {
                if ($courier->receiverAddressData->country_id == CountryList::COUNTRY_DE)
                    $lang_receiver = 'de';
                else if ($courier->receiverAddressData->country_id != CountryList::COUNTRY_PL)
                    $lang_receiver = 'en';
            }

            // SET LANGUAGE
            $tempLangChange = new TempLangChange;
            $tempLangChange->temporarySetLanguage($lang);

            // IF LANGAUGE IS OTHER THAT EN, ADD ENGLISH VERSION ON BOTTOM OF MAIL ANYWAY
            $addEnVersion = false;
            if(substr($lang, 0, 2) != Language::LANG_EN)
                $addEnVersion = true;

            $statusFull = $statusText;

            $packageNo = $courier->local_id;
            if($courier->user && $courier->user->getExternalIdsInNotifications())
            {
                list($operators, $remoteIds) = $courier->getExternalOperatorsAndIds(true);
                if(isset($remoteIds[0]) && $remoteIds[0] != '')
                    $packageNo = $remoteIds[0];
            }



            // SEND EMAIL TO PACKAGE RECEIVER
            if($courier->receiverAddressData->email != '') {
                // EMAIL TEXT:

                $title_receiver = Yii::t('mail_courier', 'Twoja paczka oczekuje na odbiór');
                $text_receiver_content = Yii::t('mail_courier', 'Nadana do Ciebie paczka #{no} oczekuje na odbiór! Sprawdź szczegóły na stronie śledzenia paczki!', array('{no}' => $packageNo));
                $text_receiver = $controller->renderInternal(Yii::getPathOfAlias('application.views._mailTemplate.courierStatNotify').'.php', ['text' => $text_receiver_content, 'model' => $courier, 'status' => $statusFull,  'addEnAnnouncement' => ($addEnVersion && $lang != Language::LANG_EN), 'toWho' => 'RECEIVER', 'packageNo' => $packageNo], true);

                //
                // CREATE EMAIL CONTENT IN ENGLISH VERSION ANYWAY:
                if($addEnVersion)
                {
                    $tempLangChange->temporarySetLanguage(Language::LANG_EN);


                    // get again for proper language
                    $statusFull = StatMap::getMapNameById($courier->stat_map_id);

                    $text_receiver_content = Yii::t('mail_courier', 'Nadana do Ciebie paczka #{no} oczekuje na odbiór! Sprawdź szczegóły na stronie śledzenia paczki!', array('{no}' => $packageNo));

                    if($lang != Language::LANG_EN)
                        $text_receiver .=  $controller->renderInternal(Yii::getPathOfAlias('application.views._mailTemplate.courierStatNotify').'.php', ['text' => $text_receiver_content, 'model' => $courier, 'status' => $statusFull,  'addEnIntro' => $addEnVersion, 'toWho' => 'RECEIVER', 'packageNo' => $packageNo], true);

                    $tempLangChange->temporarySetLanguage($lang);
                }


                $receiver_email = $courier->receiverAddressData->email;
                $recever_name = $courier->receiverAddressData->getUsefulName();

                $text = $text_receiver;
                $title = $title_receiver;

                S_Mailer::sendToCustomer($receiver_email, $recever_name, $title, $text, false, $lang, true, false, false, false, false, $pv);
//                MyDump::dump('pickupPackageNotificationEmail.txt', print_r($receiver_email,1).'||'.print_r($recever_name,1).'||'.print_r($title,1).'||'.print_r($lang,'1').'||'.print_r($text,1));

            }

            // SEND SMS TO PACKAGE RECEIVER
            if($courier->receiverAddressData->tel != '' && $courier->user->source_domain != PageVersion::PAGEVERSION_PLI) {
                $smsApi = Yii::createComponent('application.components.SmsApi');

                $sms_receiver = Yii::t('sms_courier', 'Nadana do Ciebie paczka #{no} oczekuje na odbiór! Sprawdź szczegóły na stronie śledzenia!', array('{no}' => $packageNo));
                $sms_receiver .= ' ' . Yii::t('sms_courier', 'Nadawca') . ': ' . $courier->senderAddressData->getUsefulName(true) . ';';
                if ($courier->isCod())
                    $sms_receiver .= ' ' . Yii::t('sms_courier', 'COD') . ': ' . $courier->getCodValue() . ' ' . $courier->getCodCurrency() . ';';


                $receiver_tel = $courier->receiverAddressData->tel;

//                MyDump::dump('pickupPackageNotificationSms.txt', print_r($receiver_tel,1).'||'.print_r($sms_receiver,1));
                $smsApi->sendSmsWithArchiveCourier($receiver_tel, $sms_receiver, $courier->id, $pv);
            }


            $tempLangChange->revertToBaseLanguage();
        }


    }

    protected function courier_package_waiting_receiver_notification_eobuwie_es()
    {
        $courier = $this->_serviceModel;

        if(!in_array($courier->user_id, [947, 2036, 2037]) OR !in_array($this->_statusModel->id, [
                13079,
                13084,
                13096,
                13098,
                13099,
                13111,
                13114,
                13116,
                13117,
                13121,
                13129,
                13181
            ]) OR $courier->receiver_country_id != CountryList::COUNTRY_ES)
            return false;


        switch($this->_statusModel->id)
        {
            case 13079:
                $text = 'Su envío está disponible para ser retirado de la Oficina Postal más cercana a su domicilio.';
                break;
            case 13084:
                $text = 'En reparto, envío en proceso de entrega.';
                break;
            case 13096:
                $text = 'A la espera de respuesta del destinatario, el envío se encuentra en la Oficina Postal más cercana para ser retirado';
                break;
            case 13098:
                $text = 'Se ha realizado un intento de entrega, destinatario ausente. Su envío estará disponible para ser retirado a partir del día laborable siguiente a la recepción del aviso en la Oficina Postal más cercana a su domicilio.';
                break;
            case 13099:
                $text = 'Se ha realizado un intento de entrega, destinatario ausente. Se realizará un segundo intento de entrega.';
                break;
            case 13111:
                $text = 'Envío retenido (estacionado) por dirección incorrecta/incompleta, indique dirección correcta para poder gestionarlo con Correos';
                break;
            case 13114:
                $text = 'Envío retenido (estacionado) en espera de recibir instrucciones del destinatario.';
                break;
            case 13116:
                $text = 'Envío retenido (estacionado) por Desconocido, indique dirección correcta para poder gestionarlo con Correos';
                break;
            case 13117:
                $text = 'Envío corregido, en proceso de entrega';
                break;
            case 13121:
                $text = 'Se ha realizado un intento de entrega, destinatario ausente';
                break;
            case 13129:
                $text = 'A la espera de respuesta del destinatario, el envío se encuentra en la Oficina Postal más cercana para ser retirado.';
                break;
            case 13181:
                $text = 'Envío retenido (estacionado) porque ha sido rehusado o nadie quiere aceptarlo, esperando instrucciones del remitente.';
                break;
            default:
                return false;
        }

        $tel = $courier->receiverAddressData->tel;
        $tel = self::countryPrefixTelVerifier('ES', $tel);

        list($operators, $remoteIds) = $courier->getExternalOperatorsAndIds(true);
        if(isset($remoteIds[0]) && $remoteIds[0] != '')
            $packageNo = $remoteIds[0];


        $smsApi = Yii::createComponent('application.components.SmsApi');
        $sms_receiver = $text.' www.correos.es ID:'.$packageNo;
        $receiver_tel = $tel;

        MyDump::dump('eobuwie_pickupPackageNotificationSms_ES.txt', print_r($receiver_tel,1).'||'.print_r($sms_receiver,1));

        $smsApi->sendSmsWithArchiveCourier($receiver_tel, $sms_receiver, $courier->id, PageVersion::PAGEVERSION_RS);

        $email_text = $text.'<br/><br/>https://www.correos.es/ss/Satellite/site/aplicacion-4000003383089-herramientas_y_apps/detalle_app-sidioma=es_ES?numero='.$packageNo;

        S_Mailer::sendToCustomer($courier->receiverAddressData->email, $courier->receiverAddressData->email, 'Estado de tu paquete', $email_text, false, Language::LANG_EN, true, false, false, false, false, PageVersion::PAGEVERSION_RS);

        MyDump::dump('eobuwie_pickupPackageNotificationEmail_es.txt', print_r($courier->receiverAddressData->email,1).'||'.print_r($email_text,1));
    }

    protected function courier_package_waiting_receiver_notification_eobuwie()
    {
        $courier = $this->_serviceModel;

        if(!in_array($courier->user_id, [947, 2036, 2037]) OR $this->_statusModel->id != 11769 OR $courier->receiver_country_id != CountryList::COUNTRY_SE)
            return false;

        $tel = $courier->receiverAddressData->tel;
        $tel = self::countryPrefixTelVerifier('SE', $tel);

        list($operators, $remoteIds) = $courier->getExternalOperatorsAndIds(true);
        if(isset($remoteIds[0]) && $remoteIds[0] != '')
            $packageNo = $remoteIds[0];


        $smsApi = Yii::createComponent('application.components.SmsApi');

//        $sms_receiver = 'Du har fått ett paket från eskor.se. För mer info gå till: www.royalshipments.com/tt/'.$packageNo;
        $sms_receiver = 'Ditt paket kommer att levereras idag. Mer info hos www.postnord.se och har www.royalshipments.com/tt/'.$packageNo.' ; Sok med ditt kolli-ID: '.$packageNo;

        $receiver_tel = $tel;

        MyDump::dump('eobuwie_pickupPackageNotificationSms.txt', print_r($receiver_tel,1).'||'.print_r($sms_receiver,1));

        $smsApi->sendSmsWithArchiveCourier($receiver_tel, $sms_receiver, $courier->id, PageVersion::PAGEVERSION_RS);
    }

    protected function notify_courier()
    {
        if(isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        /* @var $courier Courier */
        $courier = $this->_serviceModel;

        $pv = NULL;

        if($courier->user !== NULL) {

            $emailNotifications = $courier->user->getNotificationCustomSettings('email');
            $smsNotifications = $courier->user->getNotificationCustomSettings('sms');

            $pv = $courier->user->source_domain;
        } else {
            // if no user, get default
            $emailNotifications = $smsNotifications = [];
            $emailNotifications['o'] =
            $emailNotifications['s'] =
            $emailNotifications['r'] =
            $smsNotifications['o'] =
            $smsNotifications['s'] =
            $smsNotifications['r'] =
                StatMap::getNotifyListJustActiveKeys();
        }



//        $data = 'courier_id: '.print_r($courier->id,1 ).';';
//        $data .= 'frontend: '.print_r(Yii::app()->params['frontend'],1 ).';';
//        $data .= 'backend: '.print_r(Yii::app()->params['backend'],1 ).';';
//        $data .= 'Yii::app()->user->id: '.print_r(Yii::app()->params['console'] ? false : Yii::app()->user->id,1 ).';';
//        $data .= 'courier->user_id: '.print_r($courier->user_id,1 ).';';
//        $data .= 'emailNotifications: '.print_r($emailNotifications,1 ).';';
//        $data .= 'courier->courier_stat_id: '.print_r($courier->stat_map_id,1 ).';';
//        MyDump::dump('notification-debug.txt', $data);


        if(!is_array($emailNotifications) && !is_array($smsNotifications))
            return false;

        if(!S_Useful::sizeof($emailNotifications) && !S_Useful::sizeof($smsNotifications) OR array_map('count', $emailNotifications) <= 3 OR array_map('count', $smsNotifications) <= 3)
            return false;

        if (!in_array($courier->stat_map_id, $emailNotifications['o'])
            AND !in_array($courier->stat_map_id, $emailNotifications['s'])
            AND !in_array($courier->stat_map_id, $emailNotifications['r'])
            AND !in_array($courier->stat_map_id, $smsNotifications['o'])
            AND !in_array($courier->stat_map_id, $smsNotifications['s'])
            AND !in_array($courier->stat_map_id, $smsNotifications['r']))
            return false;


        $smsApi = Yii::createComponent('application.components.SmsApi');


        // GET OWNER DATA AND LANGUAGE
        $owner_email = NULL;
        $owner_tel = NULL;
        $lang = Language::DEFAULT_LANG;

        if($courier->user)
        {
            $owner_email = $courier->user->email;
            $owner_tel = $courier->user->tel;
            $lang = $courier->user->getPrefLang();
        }
        else {
            if($courier->getType() == Courier::TYPE_INTERNAL && isset($courier->courierTypeInternal->order) && $courier->courierTypeInternal->order->ownerData)
            {
//                $lang = $courier->courierTypeInternal->order->ownerData->getPrefLang();
                $owner_email = $courier->courierTypeInternal->order->ownerData->email;
                $owner_tel = $courier->courierTypeInternal->order->ownerData->tel;
            }
        }

        $lang_receiver = $lang;
        // SPECIAL RULE FOR PACKAGES TO DE
        if ($pv != PageVersion::PAGEVERSION_CQL) {
            if($courier->receiverAddressData->country_id == CountryList::COUNTRY_DE)
                $lang_receiver = 'de';
            else if($courier->receiverAddressData->country_id != CountryList::COUNTRY_PL)
                $lang_receiver = 'en';
        }

//        $lang = Language::DEFAULT_LANG;
//
        // SET LANGUAGE
        $tempLangChange = new TempLangChange;
        $tempLangChange->temporarySetLanguage($lang);

        // IF LANGAUGE IS OTHER THAT EN, ADD ENGLISH VERSION ON BOTTOM OF MAIL ANYWAY
        $addEnVersion = false;
        if(substr($lang, 0, 2) != Language::LANG_EN)
            $addEnVersion = true;

        /* @var $statusModel CourierStat */
//        $statusModel = $this->_statusModel;
//
//        $statusFull = $statusModel->getStatText(true);
//        $statusShort = $statusModel->getStatText(false);

//        if($this->_location != '') {
//            $statusFull = $statusFull . ' (' . $this->_location . ')';
//            $statusShort = $statusShort . ' (' . $this->_location . ')';
//        }

        $statusFull = StatMap::getMapNameById($courier->stat_map_id);
        $statusShort = $statusFull;

        $statusSms = $statusShort;

        $packageNo = $courier->local_id;

        if($courier->user && $courier->user->getExternalIdsInNotifications() OR $courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
        {
            list($operators, $remoteIds) = $courier->getExternalOperatorsAndIds(true);

            if(isset($remoteIds[0]) && $remoteIds[0] != '')
                $packageNo = $remoteIds[0];
        }


        if($courier->stat_map_id && (
                (is_array($emailNotifications['o']) && in_array($courier->stat_map_id, $emailNotifications['o']))
                OR
                (is_array($emailNotifications['s']) && in_array($courier->stat_map_id, $emailNotifications['s']))
                OR
                (is_array($emailNotifications['r']) && in_array($courier->stat_map_id, $emailNotifications['r']))

            )
        ) {
            // EMAIL TEXT:

            $template = Yii::getPathOfAlias('application.views._mailTemplate.courierStatNotify').'.php';
            if($courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $template = Yii::getPathOfAlias('application.views._mailTemplate.courierStatNotify_CQL').'.php';

            $title_owner = Yii::t('mail_courier', 'Zmiana statusu paczki');

            if($courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $title_owner = 'The status of your '.$courier->senderAddressData->getUsefulName(true,  true).' delivery #'.$packageNo.' is: '.$statusSms;

            $text_owner_content = Yii::t('mail_courier', 'Nowy status nadanej przez Ciebie paczki #{no} to:', array('{no}' => $packageNo));

            if($courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $text_owner_content = 'The status of your '.$courier->senderAddressData->getUsefulName(true,  true).' delivery is: '.$statusSms;

            $text_owner = $controller->renderInternal($template, ['text' => $text_owner_content, 'model' => $courier, 'status' => $statusFull, 'addEnAnnouncement' => $addEnVersion, 'packageNo' => $packageNo], true);

            $title_sender = Yii::t('mail_courier', 'Zmiana statusu paczki');
            if($courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $title_sender = 'The status of your '.$courier->senderAddressData->getUsefulName(true,  true).' delivery #'.$packageNo.' is: '.$statusSms;

            $text_sender_content = Yii::t('mail_courier', 'Nowy status nadanej od Ciebie paczki #{no} to:', array('{no}' => $packageNo));

            if($courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $text_sender_content = 'The status of your '.$courier->senderAddressData->getUsefulName(true,  true).' delivery is: '.$statusSms;


            $text_sender = $controller->renderInternal($template, ['text' => $text_sender_content, 'model' => $courier, 'status' => $statusFull, 'addEnAnnouncement' => $addEnVersion, 'packageNo' => $packageNo], true);


            if($lang_receiver != $lang) {
                $tempLangChange->temporarySetLanguage($lang_receiver);
            }

            $statusFull_receiver = StatMap::getMapNameById($courier->stat_map_id);

            $title_receiver = Yii::t('mail_courier', 'Zmiana statusu paczki');
            if($courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $title_receiver = 'The status of your '.$courier->senderAddressData->getUsefulName(true,  true).' delivery #'.$packageNo.' is: '.$statusSms;

            $text_receiver_content = Yii::t('mail_courier', 'Nowy status nadanej do Ciebie paczki #{no} to:', array('{no}' => $packageNo));


            if($courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $text_receiver_content = 'The status of your '.$courier->senderAddressData->getUsefulName(true,  true).' delivery is: '.$statusSms;

            $text_receiver = $controller->renderInternal($template, ['text' => $text_receiver_content, 'model' => $courier, 'status' => $statusFull_receiver, 'addEnAnnouncement' => ($addEnVersion && $lang_receiver != Language::LANG_EN), 'toWho' => 'RECEIVER', 'packageNo' => $packageNo], true);

            if($lang_receiver != $lang)
                $tempLangChange->temporarySetLanguage($lang);
            //
            // CREATE EMAIL CONTENT IN ENGLISH VERSION ANYWAY:
            if($addEnVersion)
            {
                $tempLangChange->temporarySetLanguage(Language::LANG_EN);

                // get again for proper language
//            $statusFull = $statusModel->getStatText(true);
                $statusFull = StatMap::getMapNameById($courier->stat_map_id);

                $text_owner_content = Yii::t('mail_courier', 'Nowy status nadanej przez Ciebie paczki #{no} to:', array('{no}' => $packageNo));
                $text_sender_content = Yii::t('mail_courier', 'Nowy status nadanej od Ciebie paczki #{no} to:', array('{no}' => $packageNo));
                $text_receiver_content = Yii::t('mail_courier', 'Nowy status nadanej do Ciebie paczki #{no} to:', array('{no}' => $packageNo));

                $text_owner .= $controller->renderInternal($template, ['text' => $text_owner_content, 'model' => $courier, 'status' => $statusFull, 'addEnIntro' => $addEnVersion, 'packageNo' => $packageNo], true);
                $text_sender .= $controller->renderInternal($template, ['text' => $text_sender_content, 'model' => $courier, 'status' => $statusFull, 'addEnIntro' => $addEnVersion, 'packageNo' => $packageNo], true);

                if($lang_receiver != Language::LANG_EN)
                    $text_receiver .= $controller->renderInternal($template, ['text' => $text_receiver_content, 'model' => $courier, 'status' => $statusFull, 'addEnIntro' => $addEnVersion, 'toWho' => 'RECEIVER', 'packageNo' => $packageNo], true);

                $tempLangChange->temporarySetLanguage($lang);
            }


            $toOwnerSentEmail = false;
            // SEND EMAIL TO PACKAGE OWNER
            if ($owner_email != '' && (is_array($emailNotifications['o']) && in_array($courier->stat_map_id, $emailNotifications['o']))) {
                $owner_name = trim($courier->user->name . ' ' . $courier->user->company);

                $text = $text_owner;
                $title = $title_owner;
                $toOwnerSentEmail = true;
                S_Mailer::sendToCustomer($owner_email, $owner_name, $title, $text, false, $lang, true, false, false, false, false, $pv);
            }


            // SEND EMAIL TO PACKAGE SENDER
            $sender_email = $courier->senderAddressData->email;
            if ($sender_email != '' AND  (is_array($emailNotifications['s']) && in_array($courier->stat_map_id, $emailNotifications['s']))) {
                $sender_name = $courier->senderAddressData->getUsefulName();

                $text = $text_sender;
                $title = $title_sender;

                if(!$toOwnerSentEmail OR (strcasecmp($owner_email, $sender_email) != 0))
                    S_Mailer::sendToCustomer($sender_email, $sender_name, $title, $text, false, $lang, true, false, false, false, false, $pv);
            }

            // SEND EMAIL TO PACKAGE RECEIVER
            $receiver_email = $courier->receiverAddressData->email;
            if ($receiver_email != '' AND (is_array($emailNotifications['r']) && in_array($courier->stat_map_id, $emailNotifications['r']))) {
                $recever_name = $courier->receiverAddressData->getUsefulName();

                $text = $text_receiver;
                $title = $title_receiver;

                if(!$toOwnerSentEmail OR (strcasecmp($owner_email, $receiver_email) != 0))
                    S_Mailer::sendToCustomer($receiver_email, $recever_name, $title, $text, false, $lang_receiver, true, false, false, false, false, $pv);
            }
        }

        if($courier->user->source_domain != PageVersion::PAGEVERSION_PLI &&
            $courier->stat_map_id && (
                (is_array($smsNotifications['o']) && in_array($courier->stat_map_id, $smsNotifications['o']))
                OR
                (is_array($smsNotifications['s']) && in_array($courier->stat_map_id, $smsNotifications['s']))
                OR
                (is_array($smsNotifications['r']) && in_array($courier->stat_map_id, $smsNotifications['r']))

            )
        ) {

            if($courier->user->source_domain == PageVersion::PAGEVERSION_CQL)
            {
                list($operators, $remoteIds) = $courier->getExternalOperatorsAndIds(true);
                if(isset($remoteIds[0]) && $remoteIds[0] != '')
                    $packageNo = $remoteIds[0];
            }

            // SMS TEXT:
            $sms_owner = Yii::t('sms_courier', 'Nowy status nadanej przez Ciebie paczki #{no} to:', array('{no}' => $packageNo));
            $sms_owner .= ' '.$statusSms.';';
            $sms_owner .= ' '. Yii::t('sms_courier','Nadawca').': '.$courier->senderAddressData->getUsefulName(true, $courier->user->source_domain == PageVersion::PAGEVERSION_CQL ? true : false).';';
            if($courier->isCod())
                $sms_owner .= ' '. Yii::t('sms_courier','COD').': '.$courier->getCodValue().' '.$courier->getCodCurrency().';';

            $sms_sender = Yii::t('sms_courier', 'Nowy status nadanej od Ciebie paczki #{no} to:', array('{no}' => $packageNo));
            $sms_sender .= ' '.$statusSms.';';
            $sms_sender .= ' '. Yii::t('sms_courier','Nadawca').': '.$courier->senderAddressData->getUsefulName(true, $courier->user->source_domain == PageVersion::PAGEVERSION_CQL ? true : false).';';
            if($courier->isCod())
                $sms_sender .= ' '. Yii::t('sms_courier','COD').': '.$courier->getCodValue().' '.$courier->getCodCurrency().';';


            if($lang_receiver != $lang)
                $tempLangChange->temporarySetLanguage($lang_receiver);

            $statusSms_receiver = StatMap::getMapNameById($courier->stat_map_id);

            $sms_receiver = Yii::t('sms_courier', 'Nowy status nadanej do Ciebie paczki #{no} to:', array('{no}' => $packageNo));





            $sms_receiver .= ' '.$statusSms_receiver.';';
            $sms_receiver .= ' '. Yii::t('sms_courier','Nadawca').': '.$courier->senderAddressData->getUsefulName(true, $courier->user->source_domain == PageVersion::PAGEVERSION_CQL ? true : false).';';
            if($courier->isCod())
                $sms_receiver .= ' '. Yii::t('sms_courier','COD').': '.$courier->getCodValue().' '.$courier->getCodCurrency().';';

            if($lang_receiver != $lang)
                $tempLangChange->temporarySetLanguage($lang);

            //


            $toOwnerSent = false;
            $owner_tel = preg_replace("/[^0-9]/","", $owner_tel);
            // SEND SMS TO PACKAGE OWNER
            if ($courier->user && $courier->user->hasUserAvailableSmsOptions()) {
                if ($owner_tel != '' AND is_array($smsNotifications['o']) && in_array($courier->stat_map_id, $smsNotifications['o'])) {
                    $smsApi->sendSmsWithArchiveCourier($owner_tel, $sms_owner, $courier->id, $pv);
                    $toOwnerSent = true;
                }

            }


            // SEND SMS TO PACKAGE SENDER
            if ($courier->user && $courier->user->hasUserAvailableSmsOptions()) {

                $sender_tel = $courier->senderAddressData->tel;
                $sender_tel = preg_replace("/[^0-9]/","", $sender_tel);
                if(!$toOwnerSent OR ($owner_tel != $sender_tel))
                    if ($sender_tel != '' AND $courier->senderAddressData->getIsSmsNotifyActive() AND is_array($smsNotifications['s']) && in_array($courier->stat_map_id, $smsNotifications['s']))
                        $smsApi->sendSmsWithArchiveCourier($sender_tel, $sms_sender, $courier->id, $pv);
            }

            // SEND SMS TO PACKAGE RECEIVER
            if ($courier->user && $courier->user->hasUserAvailableSmsOptions()) {
                $receiver_tel = $courier->receiverAddressData->tel;
                $receiver_tel = preg_replace("/[^0-9]/","", $receiver_tel);

                if(!$toOwnerSent OR ($owner_tel != $receiver_tel))
                    if ($receiver_tel != '' AND $courier->receiverAddressData->getIsSmsNotifyActive() AND is_array($smsNotifications['r']) && in_array($courier->stat_map_id, $smsNotifications['r'])) {
                        $receiver_tel = self::countryPrefixTelVerifier($courier->receiverAddressData->country0->code, $receiver_tel);
                        $smsApi->sendSmsWithArchiveCourier($receiver_tel, $sms_receiver, $courier->id, $pv);
                    }
            }
        }

        $tempLangChange->revertToBaseLanguage();

    }

    protected function notify_postal()
    {
        if(isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        /* @var $postal Postal */
        $postal = $this->_serviceModel;

        $pv = NULL;

        if($this->_serviceModel->user !== NULL) {
            $emailNotifications = $this->_serviceModel->user->getNotificationCustomSettings('email');
            $smsNotifications = $this->_serviceModel->user->getNotificationCustomSettings('sms');

            $pv = $this->_serviceModel->user->source_domain;
        } else {
            // if no user, get default
            $emailNotifications = $smsNotifications = [];
            $emailNotifications['o'] =
            $emailNotifications['s'] =
            $emailNotifications['r'] =
            $smsNotifications['o'] =
            $smsNotifications['s'] =
            $smsNotifications['r'] =
                StatMap::getNotifyListJustActiveKeys();
        }

        if(!is_array($emailNotifications) && !is_array($smsNotifications))
            return false;

        if(!S_Useful::sizeof($emailNotifications) && !S_Useful::sizeof($smsNotifications) OR array_map('count', $emailNotifications) <= 3 OR array_map('count', $smsNotifications) <= 3)
            return false;

        if (!in_array($postal->stat_map_id, $emailNotifications['o'])
            AND !in_array($postal->stat_map_id, $emailNotifications['s'])
            AND !in_array($postal->stat_map_id, $emailNotifications['r'])
            AND !in_array($postal->stat_map_id, $smsNotifications['o'])
            AND !in_array($postal->stat_map_id, $smsNotifications['s'])
            AND !in_array($postal->stat_map_id, $smsNotifications['r']))
            return false;


        $smsApi = Yii::createComponent('application.components.SmsApi');

        // GET OWNER DATA AND LANGUAGE
        $owner_email = NULL;
        $owner_tel = NULL;
        $lang = Language::DEFAULT_LANG;

        if($postal->user)
        {
            $owner_email = $postal->user->email;
            $owner_tel = $postal->user->tel;
            $lang = $postal->user->getPrefLang();
        } else
            return true;


        $senderUsefulName = $postal->senderAddressData === NULL ? $postal->user->getUsefulName() : $postal->senderAddressData->getUsefulName(true, $postal->user->source_domain == PageVersion::PAGEVERSION_CQL ? true : false);

        // SET LANGUAGE
        $tempLangChange = new TempLangChange;
        $tempLangChange->temporarySetLanguage($lang);

        // IF LANGAUGE IS OTHER THAT EN, ADD ENGLISH VERSION ON BOTTOM OF MAIL ANYWAY
        $addEnVersion = false;
        if(substr($lang, 0, 2) != Language::LANG_EN)
            $addEnVersion = true;

        /* @var $statusModel CourierStat */
//        $statusModel = $this->_statusModel;
//        $statusFull = $statusModel->getStatText(true);
//        $statusShort = $statusModel->getStatText(false);
//
//        if($this->_location != '') {
//            $statusFull = $statusFull . ' (' . $this->_location . ')';
//            $statusShort = $statusShort . ' (' . $this->_location . ')';
//        }

        $statusFull = StatMap::getMapNameById($postal->stat_map_id);
        $statusShort = $statusFull;
        $statusSms = $statusShort;

        if($postal->stat_map_id && (
                (is_array($emailNotifications['o']) && in_array($postal->stat_map_id, $emailNotifications['o']))
                OR
                (is_array($emailNotifications['s']) && in_array($postal->stat_map_id, $emailNotifications['s']))
                OR
                (is_array($emailNotifications['r']) && in_array($postal->stat_map_id, $emailNotifications['r']))

            )
        ) {

            $packageID = $postal->local_id;
            $operator = false;

            if($postal->user->source_domain == PageVersion::PAGEVERSION_CQL)
            {
                if($postal->postalLabel && $postal->postalLabel->track_id)
                    $packageID = $postal->postalLabel->track_id;
                else if($postal->external_id)
                    $packageID = $postal->external_id;

                if($postal->postalOperator)
                    $operator = $postal->postalOperator->name;
            }

            if($packageID == '')
                $packageID = $postal->local_id;

            $template = Yii::getPathOfAlias('application.views._mailTemplate.postalStatNotify').'.php';
            if($postal->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $template = Yii::getPathOfAlias('application.views._mailTemplate.postalStatNotify_CQL').'.php';

            // EMAIL TEXT:
            $title = Yii::t('mail_postal', 'Zmiana statusu paczki');

            if($postal->user->source_domain == PageVersion::PAGEVERSION_CQL)
                $title = 'The status of your '.$postal->senderAddressData->getUsefulName(true,  true).' delivery #'.$packageID.' is: '.$statusSms;

            $text_owner_content = Yii::t('mail_postal', 'Nowy status nadanej przez Ciebie paczki #{no} to:', array('{no}' => $packageID));
            $text_sender_content = Yii::t('mail_postal', 'Nowy status nadanej od Ciebie paczki #{no} to:', array('{no}' => $packageID));
            $text_receiver_content = Yii::t('mail_postal', 'Nowy status nadanej do Ciebie paczki #{no} to:', array('{no}' => $packageID));


            if($postal->user->source_domain == PageVersion::PAGEVERSION_CQL)
            {
                $text_owner_content = $text_owner_content = $text_receiver_content = 'The status of your '.$postal->senderAddressData->getUsefulName(true,  true).' delivery is: '.$statusSms;
            }


            $text_owner = $controller->renderInternal($template, ['text' => $text_owner_content, 'model' => $postal, 'status' => $statusFull, 'addEnAnnouncement' => $addEnVersion, 'packageID' => $packageID, 'operator' => $operator], true);
            $text_sender = $controller->renderInternal($template, ['text' => $text_sender_content, 'model' => $postal, 'status' => $statusFull, 'addEnAnnouncement' => $addEnVersion, 'packageID' => $packageID, 'operator' => $operator], true);
            $text_receiver = $controller->renderInternal($template, ['text' => $text_receiver_content, 'model' => $postal, 'status' => $statusFull, 'addEnAnnouncement' => $addEnVersion, 'packageID' => $packageID, 'operator' => $operator], true);
            //

            // CREATE EMAIL CONTENT IN ENGLISH VERSION ANYWAY:
            if($addEnVersion)
            {
                $tempLangChange->temporarySetLanguage(Language::LANG_EN);

                $statusFull = StatMap::getMapNameById($postal->stat_map_id);

                $text_owner_content = Yii::t('mail_postal', 'Nowy status nadanej przez Ciebie paczki #{no} to:', array('{no}' => $packageID));
                $text_sender_content = Yii::t('mail_postal', 'Nowy status nadanej od Ciebie paczki #{no} to:', array('{no}' => $packageID));
                $text_receiver_content = Yii::t('mail_postal', 'Nowy status nadanej do Ciebie paczki #{no} to:', array('{no}' => $packageID));

                $text_owner .= $controller->renderInternal($template, ['text' => $text_owner_content, 'model' => $postal, 'status' => $statusFull, 'addEnIntro' => $addEnVersion, 'packageID' => $packageID, 'operator' => $operator], true);
                $text_sender .= $controller->renderInternal($template, ['text' => $text_sender_content, 'model' => $postal, 'status' => $statusFull, 'addEnIntro' => $addEnVersion, 'packageID' => $packageID, 'operator' => $operator], true);
                $text_receiver .= $controller->renderInternal($template, ['text' => $text_receiver_content, 'model' => $postal, 'status' => $statusFull, 'addEnIntro' => $addEnVersion, 'packageID' => $packageID, 'operator' => $operator], true);


                $tempLangChange->temporarySetLanguage($lang);
            }

            $toOwnerSentEmail = false;
            // SEND EMAIL TO PACKAGE OWNER
            if ($owner_email != '' && (is_array($emailNotifications['o']) && in_array($postal->stat_map_id, $emailNotifications['o']))) {
                $owner_name = trim($postal->user->name . ' ' . $postal->user->company);
                $text = $text_owner;
                S_Mailer::sendToCustomer($owner_email, $owner_name, $title, $text, false, $lang, true, false, false, false, false, $pv);

                $toOwnerSentEmail = true;
            }

            if ($postal->senderAddressData !== NULL) {

                // SEND EMAIL TO PACKAGE SENDER
                $sender_email = $postal->senderAddressData->email;
                if ($sender_email != '' AND  (is_array($emailNotifications['s']) && in_array($postal->stat_map_id, $emailNotifications['s']))) {
                    $sender_name = $postal->senderAddressData->getUsefulName();

                    $text = $text_sender;

                    if(!$toOwnerSentEmail OR (strcasecmp($owner_email, $sender_email) != 0))
                        S_Mailer::sendToCustomer($sender_email, $sender_name, $title, $text, false, $lang, true, false, false, false, false, $pv);
                }
            }

            if ($postal->receiverAddressData !== NULL) {

                // SEND EMAIL TO PACKAGE RECEIVER
                $receiver_email = $postal->receiverAddressData->email;
                if ($receiver_email != '' AND (is_array($emailNotifications['r']) && in_array($postal->stat_map_id, $emailNotifications['r']))) {
                    $recever_name = $postal->receiverAddressData->getUsefulName();

                    $text = $text_receiver;

                    if(!$toOwnerSentEmail OR (strcasecmp($owner_email, $receiver_email) != 0))
                    S_Mailer::sendToCustomer($receiver_email, $recever_name, $title, $text, false, $lang, true, false, false, false, false, $pv);
                }


            }
        }

        if($postal->stat_map_id && (
                (is_array($smsNotifications['o']) && in_array($postal->stat_map_id, $smsNotifications['o']))
                OR
                (is_array($smsNotifications['s']) && in_array($postal->stat_map_id, $smsNotifications['s']))
                OR
                (is_array($smsNotifications['r']) && in_array($postal->stat_map_id, $smsNotifications['r']))

            )
        ) {

            // SMS TEXT:
            $sms_owner = Yii::t('sms_postal', 'Nowy status nadanej przez Ciebie paczki #{no} to:', array('{no}' => $packageID));
            $sms_sender = Yii::t('sms_postal', 'Nowy status nadanej od Ciebie paczki #{no} to:', array('{no}' => $packageID));
            $sms_receiver = Yii::t('sms_postal', 'Nowy status nadanej do Ciebie paczki #{no} to:', array('{no}' => $packageID));

            $sms_owner .= ' '.$statusSms.';';
            $sms_sender .= ' '.$statusSms.';';
            $sms_receiver .= ' '.$statusSms.';';

            $sms_owner .= ' '. Yii::t('sms_postal','Nadawca').': '.$senderUsefulName.';';
            $sms_sender .= ' '. Yii::t('sms_postal','Nadawca').': '.$senderUsefulName.';';
            $sms_receiver .= ' '. Yii::t('sms_postal','Nadawca').': '.$senderUsefulName.';';


            $owner_tel = preg_replace("/[^0-9]/","", $owner_tel);
            // SEND SMS TO PACKAGE OWNER
            if ($postal->user && $postal->user->getPostalAvailableSms()) {
                if ($owner_tel != '' AND is_array($smsNotifications['o']) && in_array($postal->stat_map_id, $smsNotifications['o']))
                    $smsApi->sendSmsWithArchivePostal($owner_tel, $sms_owner, $postal->id, $pv);
            }

            if ($postal->senderAddressData !== NULL) {

                // SEND SMS TO PACKAGE SENDER
                if ($postal->user && $postal->user->getPostalAvailableSms()) {
                    $sender_tel = $postal->senderAddressData->tel;
                    $sender_tel = preg_replace("/[^0-9]/","", $sender_tel);
                    if ($sender_tel != '' AND $postal->senderAddressData->getIsSmsNotifyActive() AND is_array($smsNotifications['s']) && in_array($postal->stat_map_id, $smsNotifications['s']))
                        $smsApi->sendSmsWithArchivePostal($sender_tel, $sms_sender, $postal->id, $pv);
                }

            }

            if ($postal->receiverAddressData !== NULL) {

                // SEND SMS TO PACKAGE RECEIVER
                if ($postal->user && $postal->user->getPostalAvailableSms()) {
                    $receiver_tel = $postal->receiverAddressData->tel;
                    $receiver_tel = preg_replace("/[^0-9]/","", $receiver_tel);
                    if ($receiver_tel != '' AND $postal->receiverAddressData->getIsSmsNotifyActive() AND is_array($smsNotifications['r']) && in_array($postal->stat_map_id, $smsNotifications['r'])) {
                        $receiver_tel = self::countryPrefixTelVerifier($postal->receiverAddressData->country0->code, $receiver_tel);
                        $smsApi->sendSmsWithArchivePostal($receiver_tel, $sms_receiver, $postal->id, $pv);
                    }
                }

            }
        }

        $tempLangChange->revertToBaseLanguage();

    }

}