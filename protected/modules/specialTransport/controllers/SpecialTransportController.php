<?php

class SpecialTransportController extends Controller {

    public function beforeAction($action)
    {

        if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI)
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'HybridMail',
            ));
            exit;
        }

    }

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('index',),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('create', 'view', 'list'),
                'expression' => '0 < Yii::app()->user->getModel()->isPremium',
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }


    public function actionView($urlData) {

        $this->panelLeftMenuActive = 600;

        /* @var $model SpecialTransport */

        $model = SpecialTransport::model()->find('hash=:hash', array(':hash'=>$urlData));

        if($model === null)
            throw new CHttpException(404);

        $this->render('view', array(
            'model' => $model,
        ));
    }


    public function actionCreate() {

        $this->panelLeftMenuActive = 601;

        $saveToContactBookSender = NULL;
        $saveToContactBookReceiver = NULL;

        $model = new SpecialTransport;
        $model->senderAddressData = new AddressData();
        $model->receiverAddressData = new AddressData();
        //$this->performAjaxValidation($model, 'special-transport-form');
        //$this->performAjaxValidation($model->senderAddressData, 'special-transport-form');
        //$this->performAjaxValidation($model, 'special-transport-form->receiverAddressData');

        if(!isset($_POST['SpecialTransport']))
        {
            $model->senderAddressData->attributes = User::getLoggedUserAddressData();
        }

        if (isset($_POST['SpecialTransport'])) {

            $saveToContactBookSender = $_POST['Other']['saveToContactBook']['sender-data'];
            $saveToContactBookReceiver = $_POST['Other']['saveToContactBook']['receiver-data'];

            $model->setAttributes($_POST['SpecialTransport']);

            $model->senderAddressData->setAttributes($_POST['AddressData']['sender-data']);
            $model->senderAddressData->validate();

            $model->validate();


            $model->receiverAddressData->setAttributes($_POST['AddressData']['receiver-data']);
            $model->receiverAddressData->validate();


            if((!$model->hasErrors() AND !$model->senderAddressData->hasErrors() AND !$model->receiverAddressData->hasErrors()))            {
                $error = false;
                $transaction = Yii::app()->db->beginTransaction();

                try
                {

                    if(!$model->senderAddressData->save())
                        $error = true;
                    $model->sender_address_data_id = $model->senderAddressData->id;

                    if($saveToContactBookSender)
                    {
                        $modelContactBookSenderAddressData = new AddressData();
                        $modelContactBookSenderAddressData->attributes = $model->senderAddressData->attributes;

                        $modelContactBookSender = new UserContactBook();
                        $modelContactBookSender->user_id = Yii::app()->user->id;
                        $modelContactBookSender->addressData = $modelContactBookSenderAddressData;

                        if(!$modelContactBookSender->saveWithAddressData(false))
                            $error = true;
                    }

                    if(!$model->receiverAddressData->save())
                        $error = true;
                    $model->receiver_address_data_id = $model->receiverAddressData->id;

                    if($saveToContactBookReceiver)
                    {
                        $modelContactBookReceiverAddressData = new AddressData();
                        $modelContactBookReceiverAddressData->attributes = $model->receiverAddressData->attributes;

                        $modelContactBookReceiver = new UserContactBook();
                        $modelContactBookReceiver->user_id = Yii::app()->user->id;
                        $modelContactBookReceiver->addressData = $modelContactBookReceiverAddressData;

                        if(!$modelContactBookReceiver->saveWithAddressData(false))
                            $error = true;
                    }


                    if(!$model->save())
                        $error = true;


                    if(!$error)
                    {
                        $transaction->commit();

                        Yii::app()->user->setFlash('success', 'Twoje zlecenie zostało zapisane w systemie!');
                        $this->redirect(array('specialTransport/list',
                        ));
                        return;
                    }
                    else
                    {
                        $transaction->rollback();
                    }

                }
                catch (Exception $ex)
                {
                    $transaction->rollback();
                }
            }
            //Yii::app()->user->setFlash('special-transport-form-errors', 'Nie wszystko jest poprawnie wypełnione!');

        }

        $this->render('create', array(
            'model' => $model,
            'saveToContactBooksender' => $saveToContactBookSender,
            'saveToContactBookreceiver' => $saveToContactBookReceiver
        ));
    }



    public function actionList()
    {

        $this->panelLeftMenuActive = 602;

        $model = new SpecialTransport('search');
        $model->unsetAttributes();

        if (isset($_GET['SpecialTransport']))
            $model->setAttributes($_GET['SpecialTransport']);

        $this->render('list', array(
            'model' => $model,
        ));


    }

    public function actionIndex() {

        $this->panelLeftMenuActive = 600;

        $this->render('index');
    }

}