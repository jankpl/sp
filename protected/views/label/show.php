<?php
/* @var $model LabelsUrl */
?>
<h1><?= Yii::t('label_url', 'Twój link do etykiet');?></h1>
<br/>
<br/>
<br/>
<div class="alert alert-info text-center" id="link-waiting">
    <br/>
    <strong><?= Yii::t('label_url', 'Trwa generowanie pliku...');?></strong>
    <br/>
    <br/>
    <i class="fa fa-clock-o" aria-hidden="true" style="font-size: 4em;" id="clock-animation"></i>
    <br/><br/>
    <?= Yii::t('label_url', 'Link do pobrania pliku będzie automatycznie dostępny po ukończeniu operacji.');?>
</div>
<div class="alert alert-info text-center" id="link-ready" style="display: none;">
    <input type="text" style="text-align: center; width: 80%; margin: 0 auto; font-size: 1.2em; padding: 15px;" value="<?= $model->getUrl();?>" id="url"/><br/><br/>
    <input type="button" class="btn btn-xl btn-success" value="<?= Yii::t('label_url', 'Kopiuj do schowka');?>" id="copy">
    <br/><br/>
    <?= Yii::t('label_url', 'Link będzie ważny przez 7 dni.');?>
</div>
<script>
    $('#copy').on('click', function(){
            var copyText = document.getElementById("url");
            copyText.select();
            document.execCommand("Copy");
    });

    $(document).ready(function(){

        var $clock = $('#clock-animation');

        function runIt() {
            $clock.animate({opacity:'1'}, 1000);
            $clock.animate({opacity:'0.1'}, 1000, runIt);
        }
        runIt();

        function checkData()
        {
            $.ajax({
                url: '<?= Yii::app()->createUrl('/label/ajaxCheckIfReady');?>',
                data: { hash: '<?= $model->hash;?>'},
                method: 'POST',
                dataType: 'json',
                success: function(result){
                    console.log(result);
                    if(result)
                    {
                        $('#link-waiting').fadeOut('fast', function(){
                            $('#link-ready').fadeIn('fast', function(){
                            });
                        });

                    } else
                        setTimeout(checkData, 1000);

                },
                error: function(e){
                    console.log(e);
                }
            });
        }

        checkData();
    });
</script>
