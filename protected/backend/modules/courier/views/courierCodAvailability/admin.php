<?php
/* @var $model CourierCountryGroup */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-country-group-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'countryList.name',
        'currency',
        [
                'header' => 'Operators no',
                'value' => 'S_Useful::sizeof($data->courierCodAvailabilityOperators)',
        ],
		array(
            'class' => 'CButtonColumn',
            'template' => '{view} {update}'
        ),
	),
)); ?>
