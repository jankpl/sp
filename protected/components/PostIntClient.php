<?php

class PostIntClient extends GlobalOperator
{
    const URL = GLOBAL_CONFIG::POSTINT_URL;
    const USER = GLOBAL_CONFIG::POSTINT_USER;
    const KEY = GLOBAL_CONFIG::POSTINT_KEY;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $weight;
    public $value;
    public $value_currency;
    public $content;

    public $cod = 0;
    public $cod_currency;
    public $size_l;
    public $size_d;
    public $size_w;

    public $local_id;

    public $ref;


    public $detailedItems = false;

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    protected function _createShipment($returnError = false)
    {

        $data = [];
        $data['ParcelNumber'] = $this->local_id;
        $data['Receiver'] = trim($this->receiver_name.' '.$this->receiver_company);
        $data['ReceiverStreet'] = trim($this->receiver_address1.' '.$this->receiver_address2);
        $data['ReceiverZip'] = $this->receiver_zip_code;
        $data['ReceiverCity'] = $this->receiver_city;
        $data['ReceiverCountry'] = $this->receiver_country;

        $data['ReceiverPhone'] = $this->receiver_tel;
        $data['ReceiveType'] = 0;
        $data['Weight'] = $this->weight;
        $data['ValueCurrency'] = $this->value_currency;
        $data['Value'] = $this->value;
        $data['Contents'] = [];

        if($this->detailedItems && isset($this->detailedItems['UA']) && is_array($this->detailedItems['UA']))
        {
            foreach($this->detailedItems['UA'] AS $temp)
            {
                $data['Contents'][] = [
                    'Item' => $temp['n'],
                    'Quantity' => $temp['q'],
                    'Weight' => '',
                    'ValueCurrency' => 'EUR',
                    'Value' => $temp['p'],
                ];
            }
        } else {

            $data['Contents'][] = [
                'Item' => $this->content,
                'Quantity' => 1,
                'Weight' => $this->weight,
                'ValueCurrency' => $this->value_currency,
                'Value' => $this->value,
            ];
        }

//        $data['Notes'] = $this->ref;

        if($this->cod > 0)
        {
            $data['COD'] = $this->cod;
            $data['CODCurrency'] = $this->cod_currency;
        }

        $resp = $this->_curlCall('CreateParcel', $data);

        if(!$resp->success)
        {
            return GlobalOperatorResponse::createErrorResponse($resp->error_code);
        }

        $tt = $resp->data->TrackingCode;

        $data = [
            'ParcelNumber' => $this->local_id,
        ];

        $label = $this->_curlCall('PrintCN23', $data, false);

        file_put_contents('postint.pdf', $label);

        // probably no label, so error
        if(strlen($label) < 10000)
        {
            $label = json_decode($resp);
            return GlobalOperatorResponse::createErrorResponse($label->error_code);
        }

        $data = [
            'DispatchNumber' => $this->local_id,
            'Parcels' => [ $this->local_id],
        ];

        $resp = $this->_curlCall('RegisterDispatch', $data, false);

        $im = ImagickMine::newInstance();
        $im->setResolution(300,300);

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->stripImage();
        $im->trimImage(0);
        $im->rotateImage(new ImagickPixel(), 90);


        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $tt);
        return $return;
    }

    protected function _curlCall($method, $data = [], $jsonDecode = true)
    {
        $dataContainer = [
            'method' => $method,
            'login' => self::USER,
            'key' => self::KEY,
            'rest_data' => json_encode($data),
        ];

        MyDump::dump('postint.txt', 'REQ: '.print_r($dataContainer,1));

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

//        $dataOryginal = $data;

        $headers = [];
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=utf-8';

        $curl = curl_init(self::URL);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($dataContainer));
        curl_setopt_array($curl, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false
            )
        );
        $resp = curl_exec($curl);

        MyDump::dump('postint.txt', 'RESP: '.print_r($resp,1));

        if($jsonDecode)
            $resp = json_decode($resp);

        return $resp;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->getCodeIso3();
        $model->receiver_country_id = $to->country_id;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->weight = $courierInternal->courier->getWeight(true);
        $model->content = $courierInternal->courier->package_content;
        $model->value = $courierInternal->courier->getPackageValueConverted('EUR');
        $model->value_currency = $courierInternal->courier->value_currency;
        $model->size_l = $courierInternal->courier->package_size_l;
        $model->size_d = $courierInternal->courier->package_size_d;
        $model->size_w = $courierInternal->courier->package_size_w;
        $model->cod = $courierInternal->courier->cod_value;
        $model->cod_currency = $courierInternal->courier->cod_currency;
        $model->local_id = $courierInternal->courier->local_id;
        $model->ref = $courierInternal->courier->ref;

        $model->detailedItems = $courierInternal->courier->courierCustomApiData;

        return $model->_createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;

        $to = $courierU->courier->receiverAddressData;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->getCodeIso3();
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(true);

        $model->weight = $courierU->courier->getWeight(true);
        $model->content = $courierU->courier->package_content;
        $model->value = $courierU->courier->getPackageValueConverted('EUR');
        $model->value_currency = $courierU->courier->value_currency;
        $model->size_l = $courierU->courier->package_size_l;
        $model->size_d = $courierU->courier->package_size_d;
        $model->size_d = $courierU->courier->package_size_w;
        $model->cod = $courierU->courier->cod_value;
        $model->cod_currency = $courierU->courier->cod_currency;
        $model->local_id = $courierU->courier->local_id;
        $model->ref = $courierU->courier->ref;

        $model->detailedItems = $courierU->courier->courierCustomApiData;

        return $model->_createShipment( $returnErrors);
    }



    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        $data = [
            'ParcelNumber' => $no,
        ];

        $model = new self;
        $resp = $model->_curlCall('GetOrderStatus', $data);

        $statuses = new GlobalOperatorTtResponseCollection();
        if($resp->success)
            foreach($resp->data AS $item)
            {

                if((string) $item->OrderStatus == 'Нет информации')
                    continue;

                $statuses->addItem((string) $item->OrderStatus, (string) $item->ChangeDate, NULL);
            }

        return $statuses;
    }

}

