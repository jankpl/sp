<?php
/* @var $model InvoiceCustomDataForm */
?>

<?php
/* @var $this SiteController */
/* @var $form CActiveForm */
?>


<div class="row">
    <div class="col-xs-12">


            <div class="form alert alert-info">

                <h3 style="margin-top: 0;">Custom invoice data</h3>

                <?php
                $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                )); ?>
                <?php echo $form->errorSummary($model); ?>
                <div><?= $form->textField($model,'operator_reference', ['placeholder' => $model->getAttributeLabel('operator_reference')]); ?></div>
                <div><?= $form->textField($model,'shipment_date', ['placeholder' => $model->getAttributeLabel('shipment_date')]); ?></div>
                <div><?= $form->textField($model,'unit_value', ['placeholder' => $model->getAttributeLabel('unit_value')]); ?></div>
                <div><?= $form->textField($model,'total_value', ['placeholder' => $model->getAttributeLabel('total_value')]); ?></div>
                <div><?= $form->textField($model,'qty_of_items', ['placeholder' => $model->getAttributeLabel('qty_of_items')]); ?></div>
                <div><?= $form->textField($model,'comodities', ['placeholder' => $model->getAttributeLabel('comodities')]); ?> <?= $form->textField($model,'comodities', ['placeholder' => $model->getAttributeLabel('comodities')]); ?> <?= $form->textField($model,'comodities', ['placeholder' => $model->getAttributeLabel('comodities')]); ?> +</div>
                <div><?= $form->textField($model,'total_pieces', ['placeholder' => $model->getAttributeLabel('total_pieces')]); ?></div>
                <div><?= $form->dropDownList($model,'content_type', ['documents', 'goods'] ); ?></div>
                <div><?= $form->textField($model,'country_of_orgin', ['placeholder' => $model->getAttributeLabel('country_of_orgin')]); ?></div>
                <div><?= $form->textField($model,'hs_tariff_code', ['placeholder' => $model->getAttributeLabel('hs_tariff_code')]); ?></div>
                <div><?= $form->dropDownList($model,'terms_of_trade', ['DAP - Delivered At Place ',
                        'DDP - Delivered Duty Paid',
                        'DAT – Delivered at Terminal',
                        'CIP - Carriage and Insurance Paid',
                        'CPT - Carriage Paid To',
                        'EXW – ExWorks',
                        'FCA - Free Carrier',] ); ?></div>
                <div><?= $form->textField($model,'sender_tax_id', ['placeholder' => $model->getAttributeLabel('sender_tax_id')]); ?></div>
                <div><?= $form->textField($model,'consinee_tax_id', ['placeholder' => $model->getAttributeLabel('consinee_tax_id')]); ?></div>

                <div class="text-center">
                    <?php echo TbHtml::submitButton(Yii::t('app','Generate'), ); ?>
                </div>
                <?php $this->endWidget(); ?>
            </div><!-- form -->
    </div>
</div>
