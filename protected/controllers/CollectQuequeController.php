<?php

class CollectQuequeController extends CController
{

    public function filters()
    {
        return array();
    }

    // run queque
    public function actionIndex($id = NULL, $async = false, $counter = 0)
    {

        $counter++;

        if ($counter > 60)
            exit;

        Yii::app()->getModule('courier');

        if ($id === NULL)
            $model = CourierCollect::model()->find(array('condition' => 'stat = :stat', 'params' => array(':stat' => CourierCollect::STAT_WAITING), 'order' => 'id ASC'));
        else {
            $model = CourierCollect::model()->find(array('condition' => 'stat = :stat AND id = :id', 'params' => array(':stat' => CourierCollect::STAT_WAITING, ':id' => $id), 'order' => 'id ASC'));
        }

        if (!$model)
            exit;

        $model->runOrdering();
        sleep(1);


        $path = Yii::app()->createAbsoluteUrl('/collectQueque/index', array('id' => $id, 'counter' => $counter));
        header("Location: " . $path);

        exit;
    }

}