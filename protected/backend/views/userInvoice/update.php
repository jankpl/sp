<?php
/* @var $this UserInvoiceController */
/* @var $model UserInvoice */

$this->breadcrumbs=array(
    'User Messages'=>array('index'),
    $model->id=>array('view','id'=>$model->id),
    'Update',
);

$this->menu=array(
    array('label'=>'List UserInvoice', 'url'=>array('index')),
    array('label'=>'Create UserInvoice', 'url'=>array('create')),
    array('label'=>'View UserInvoice', 'url'=>array('view', 'id'=>$model->id)),
);
?>

    <h1>Update UserInvoice <?php echo $model->id; ?></h1>

<?php
if($model->user->getDebtBlockActive()):
    ?>
    <div class="alert alert-error">User account is blocked!</div>
    <?php
endif;
?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>


<h3>History</h3>
<table class="table table-striped table-bordered table-condensed">
    <tr>
        <th>#</th>
        <th>Date</th>
        <th>Action</th>
        <th>Author</th>
    </tr>
    <?php
    $i = 1;
    foreach($model->userInvoiceHistory AS $item):
        ?>
        <tr>
            <td><?= $i;?></td>
            <td><?= $item->date_entered;?></td>
            <td><?= $item->text;?></td>
            <td><?= $item->admin_id ? $item->admin->login : '-';?></td>
        </tr>
        <?php
        $i++;
    endforeach;
    ?>
</table>
