<?php
/* @var $model CourierCountryGroup */
/* @var $form GxActiveForm */

/* @var $modelsDelivery CourierCountryGroupOperator[] */
/* @var $modelsPickup CourierCountryGroupOperator[] */
/* @var $modelDeliveryNew CourierCountryGroupOperator */
/* @var $modelPickupNew CourierCountryGroupOperator */
/* @var $allowExtendedOperators boolean */
/* @var $userGroup int */
/* @var $userGroupModel UserGroup */

?>

<div class="form">

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-country-group-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>



    <?php
    $modelsForSummary = [];

    $modelsForSummary[] = $model;
    if($allowExtendedOperators)
    {
        $modelsForSummary[] = $modelDeliveryNew;
        $modelsForSummary[] = $modelPickupNew;
    }

    echo $form->errorSummary($modelsForSummary); ?>

    <div id="delivery">

        <h2>Delivery</h2>

        <div class="alert alert-success">
            <h5 class="text-center">Default operator:</h5>

            <div class="row text-center">
                <?php echo $form->labelEx($model,'delivery_hub'); ?>
                <?php echo $form->dropDownList($model,'delivery_hub', CHtml::listData(CourierHub::listHubs(),'id','name'), ['prompt' => '-']); ?>
                <?php echo $form->error($model,'delivery_hub'); ?>
            </div><!-- row -->
            <div class="row text-center">
                <?php echo $form->labelEx($model,'delivery_operator'); ?>
                <?php echo $form->dropDownList($model, 'delivery_operator',  CHtml::listData(CourierOperator::listOperators(),'id','nameWithIdAndBroker'), ['prompt' => '-']); ?>
                <?php echo $form->error($model,'delivery_operator'); ?>
            </div><!-- row -->

        </div>

        <?php
        if($allowExtendedOperators)
            echo $this->renderPartial('_customOperatorForm', [
                'form' => $form,
                'models' => $modelsDelivery,
                'modelNew' => $modelDeliveryNew,
                'type' => CourierCountryGroupOperator::TYPE_DELIVERY
            ], true, true
            ); ?>


    </div>


    <h2>Pickup</h2>
    <?php
    if($model->user_group_id === NULL):
        ?>
        <div class="row text-center">
            <?php echo CHtml::label('Pickup available', '__pickup_available');?>
            <?php echo $form->checkBox($model,'__pickup_available', array('id' => 'pickup_available')); ?>
        </div>
    <?php
    endif;
    ?>
    <?php
    if($model->user_group_id === NULL OR $model->courierCountryGroupCustomParent->isPickupAvailable()):
    ?>
    <div id="pickup">


        <div class="alert alert-success">
            <h5 class="text-center">Default operator:</h5>

            <div class="row text-center"">
            <?php echo $form->labelEx($model,'pickup_hub'); ?>
            <?php echo $form->dropDownList($model,'pickup_hub', CHtml::listData(CourierHub::listHubs(),'id','name'), ['prompt' => '-']); ?>
            <?php echo $form->error($model,'pickup_hub'); ?>
        </div><!-- row -->

        <div class="row text-center"">
        <?php echo $form->labelEx($model,'pickup_operator'); ?>
        <?php echo $form->dropDownList($model, 'pickup_operator',  CHtml::listData(CourierOperator::listOperators(),'id','nameWithIdAndBroker'), ['prompt' => '-']); ?>
        <?php echo $form->error($model,'pickup_operator'); ?>
    </div><!-- row -->
</div>

<?php
if($allowExtendedOperators)
    echo $this->renderPartial('_customOperatorForm', [
        'form' => $form,
        'models' => $modelsPickup,
        'modelNew' => $modelPickupNew,
        'type' => CourierCountryGroupOperator::TYPE_PICKUP
    ], true, true
    ); ?>

    </fieldset>

<?php
else:
    ?>
    <div class="alert alert-warning">Pickup must be enabled in main settings for this country group.</div>
<?php
endif;
?>

<i> (*) If more than one operator is available for particular weight range, operator with lower No will be used.</i>
<br/>
<br/>

</div>
<hr/>
<?php
if($userGroup === NULL):
    ?>
    <h2>Other group settings</h2>
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'countryList'); ?>
        <?php echo CHtml::dropDownList('countryList',$model->listCountriesForGroup(true),CHtml::listData($model->listUngroupedCountriesAndMine(),'id','name'), array('multiple' => true, 'size' => 15));?>
        <?php echo $form->error($model,'countryList'); ?>
    </div>

<?php
endif;
?>


<br/>
<br/>
<div class="text-center">
    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-large']);
    ?>
    <?php
    if($userGroup !== NULL && !$model->isNewRecord):
        ?>
        <br/>
        <br/>
        <?php
        echo GxHtml::submitButton('Delete custom setting for this group', ['class' => 'btn btn-xs btn-danger', 'name' => 'deleteGroup', 'confirm' => 'Are you sure?']);
        ?>
    <?php
    endif;
    ?>
</div>
<?php
$this->endWidget();
?>


</div><!-- form -->



<?php

if($model->user_group_id === NULL):

    Yii::app()->clientScript->registerCoreScript('jquery');
    echo CHtml::script('
$(document).ready(toggle_pickup);
$("#pickup_available").click(toggle_pickup);

function toggle_pickup()
{
if($("#pickup_available").is(":checked"))
    $("#pickup").show();
else
    $("#pickup").hide();

}
');
endif;
?>

<div class="alert alert-info">
    Routing consists of 2 parts: <strong>PICKUP</strong> setting and <strong>DELIVERY</strong> setting.<br/><br/>
    <strong>DELIVERY</strong> is <strong>required</strong><br/><br/>
    When <strong>PICKUP</strong> is not set for the country group, sending item <strong>from</strong> this country group will be impossible, but sending <strong>to</strong> will still be possible.<br/><br/>
    <strong>Example:</strong><br/>Carraige from <strong>PL</strong> to <strong>DE</strong>:<br/>
    System gets <strong>PICKUP operator</strong> from <strong>PL</strong> setting and <strong>DELIVERY operator</strong> from <strong>DE</strong> settings.<br/><br/>
    If they are <strong>THE SAME</strong>, only <strong>ONE</strong> label is created - from <strong>SENDER</strong> to <strong>RECEIVER</strong><br/><br/>
    If they are <strong>DIFFERENT</strong>, <strong>TWO</strong> labels are created:<br/>
    <strong>1:</strong> from <strong>SENDER</strong> to <strong>PL PICKUP HUB</strong><br/>
    <strong>2:</strong> from <strong>DE DELIVERY HUB</strong> to <strong>RECEIVER</strong><br/>
</div>
