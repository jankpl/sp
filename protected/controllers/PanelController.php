<?php

class PanelController extends Controller
{
    public $layout = '//layouts/panel-full';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array_merge([
            'accessControl', // perform access control for CRUD operations
        ]);
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        Yii::app()->getModule('courier');

        $this->render('index',
//        $this->render('index_no_react',
            []);
    }

    public function actionJsonCourierPackagesOrders()
    {
        Yii::app()->getModule('courier');

        $data = [];
//        $data[] = [Yii::t('panel', 'Okres'), Yii::t('panel', 'Kurier'), Yii::t('panel', 'Postal')];
        $data[] = [Yii::t('panel', 'Miesiąc'), Yii::t('panel', 'Paczek')];


        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        if($from && $to)
        {
            $from = new DateTime($from);
            $to = new DateTime($to);
        } else {
            $to = new DateTime();
            $from = new DateTime();
            $from->sub(new DateInterval("P1Y"));
        }

        $months = 0;
        $temp = clone $from;

        while ($temp < $to){
            $months ++;
            $temp->add(new \DateInterval('P1M'));
        }
        unset($temp);

        $periods = [];
        for($i = 0; $i <= $months; $i++)
        {
            $temp2 = date("Y-m", strtotime("+$i month", strtotime($from->format('Y-m-d'))));
            $temp2 = str_replace('.','', $temp2);
            $periods[$temp2] = $temp2;
        }

        foreach($periods AS $key => $period)
//            $data[] = [$period, PanelStats::getTotalPackagesForDate($period, Yii::app()->user->id), PanelStats::getTotalPostalForDate($period, Yii::app()->user->id)];
            $data[] = [$period, PanelStats::getTotalPackagesForDate($period, Yii::app()->user->id)];

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierStatMap()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = [];
        $data['MAP_NEW'] = StatMap::countCouriersForStatMap(StatMap::MAP_NEW, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_ACCEPTED'] = StatMap::countCouriersForStatMap(StatMap::MAP_ACCEPTED, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_DELIVERED_TO_HUB'] = StatMap::countCouriersForStatMap(StatMap::MAP_DELIVERED_TO_HUB, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_IN_TRANSPORTATION'] = StatMap::countCouriersForStatMap(StatMap::MAP_IN_TRANSPORTATION, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_IN_DELIVERY'] = StatMap::countCouriersForStatMap(StatMap::MAP_IN_DELIVERY, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_DELIVERED'] = StatMap::countCouriersForStatMap(StatMap::MAP_DELIVERED, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_CANCELLED'] = StatMap::countCouriersForStatMap(StatMap::MAP_CANCELLED, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_PROBLEM'] = StatMap::countCouriersForStatMap(StatMap::MAP_PROBLEM, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_RETURN'] = StatMap::countCouriersForStatMap(StatMap::MAP_RETURN, Yii::app()->user->id, 0, $from, $to);

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierPackagesStat()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = [];
        $data[] = [Yii::t('panel', 'Status'), Yii::t('panel', 'Paczek')];


        foreach(StatMap::mapNameList() AS $key => $stat)
            $data[] = [$stat, intval(StatMap::countCouriersForStatMap($key, Yii::app()->user->id, 0, $from, $to))];

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierPackagesStatPercent()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $totalTotal = 0;
//        $total7 = 0;
        foreach(StatMap::mapNameList() AS $key => $stat)
        {
            $totalTotal += StatMap::countCouriersForStatMap($key, Yii::app()->user->id, 0, $from, $to);
//            $total7 += StatMap::countCouriersForStatMap($key, Yii::app()->user->id, 7);
        }

        $data = [];
//        $data[] = [Yii::t('panel', 'Status'), Yii::t('panel', 'Paczek - łącznie'), Yii::t('panel', 'Paczek - 7 dni')];
        $data[] = [Yii::t('panel', 'Status'), Yii::t('panel', 'Paczek')];


        foreach(StatMap::mapNameList() AS $key => $stat)
//            $data[] = [$stat, !$totalTotal ? 0 : (round(StatMap::countCouriersForStatMap($key, Yii::app()->user->id)/$totalTotal,2)*100), !$total7 ? 0 : (round(StatMap::countCouriersForStatMap($key, Yii::app()->user->id,7)/$total7,2)*100)];
            $data[] = [$stat, !$totalTotal ? 0 : (round(StatMap::countCouriersForStatMap($key, Yii::app()->user->id, 0, $from, $to)/$totalTotal,2)*100)];


        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierCountriesDelivered()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $list = PanelStats::getCountriesStatForCourier(Yii::app()->user->id, $from, $to);

        $data = [];
        $data[] = [Yii::t('panel', 'Kraj'), Yii::t('panel', 'Liczba paczek')];

        foreach($list AS $item)
            $data[] = [$item['code'], intval($item['no'])];

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonPostalCountriesDelivered()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $list = PanelStats::getCountriesStatForPostal(Yii::app()->user->id, $from, $to);

        $data = [];
        $data[] = [Yii::t('panel', 'Kraj'), Yii::t('panel', 'Liczba przesyłek')];

        foreach($list AS $item)
            $data[] = [$item['code'], intval($item['no'])];

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierHeaviestWeight()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = PanelStats::getHeaviestDeliveredWeightForCourier(Yii::app()->user->id, $from, $to).' kg';

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierAverageWeight()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = PanelStats::getAverageDeliveredWeightForCourier(Yii::app()->user->id, $from, $to).' kg';

        echo CJSON::encode($data);
        Yii::app()->end();
    }



    public function actionJsonCourierTotalDeliveredWeight()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = PanelStats::getTotalDeliveredWeightForCourier(Yii::app()->user->id, $from, $to).' kg';

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierAverageDeliveryTime()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = PanelStats::getAverageDeliveryTimeForCourier(Yii::app()->user->id, $from, $to);

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierTotalPackagesDelivered()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = PanelStats::getTotalDeliveredForCourier(Yii::app()->user->id, $from, $to);

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonPostalTotalPackagesDelivered()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = PanelStats::getTotalDeliveredForPostal(Yii::app()->user->id, $from, $to);

        echo CJSON::encode($data);
        Yii::app()->end();
    }


    // POSTAL:
    public function actionJsonPostalPackagesOrders()
    {
        Yii::app()->getModule('postal');

        $data = [];
//        $data[] = [Yii::t('panel', 'Okres'), Yii::t('panel', 'Kurier'), Yii::t('panel', 'Postal')];
        $data[] = [Yii::t('panel', 'Okres'),  Yii::t('panel', 'Przesyłek')];



        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        if($from && $to)
        {
            $from = new DateTime($from);
            $to = new DateTime($to);
        } else {
            $to = new DateTime();
            $from = new DateTime();
            $from->sub(new DateInterval("P1Y"));
        }

        $months = 0;
        $temp = clone $from;

        while ($temp < $to){
            $months ++;
            $temp->add(new \DateInterval('P1M'));
        }
        unset($temp);

        $periods = [];
        for($i = 0; $i <= $months; $i++)
        {
            $temp2 = date("Y-m", strtotime("+$i month", strtotime($from->format('Y-m-d'))));
            $temp2 = str_replace('.','', $temp2);
            $periods[$temp2] = $temp2;
        }

        foreach($periods AS $key => $period)
//            $data[] = [$period, PanelStats::getTotalPackagesForDate($period, Yii::app()->user->id), PanelStats::getTotalPostalForDate($period, Yii::app()->user->id)];
            $data[] = [$period, PanelStats::getTotalPostalForDate($period, Yii::app()->user->id)];

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonPostalStatMap()
    {
        Yii::app()->getModule('postal');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = [];
        $data['MAP_NEW'] = StatMap::countPostalsForStatMap(StatMap::MAP_NEW, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_ACCEPTED'] = StatMap::countPostalsForStatMap(StatMap::MAP_ACCEPTED, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_DELIVERED_TO_HUB'] = StatMap::countPostalsForStatMap(StatMap::MAP_DELIVERED_TO_HUB, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_IN_TRANSPORTATION'] = StatMap::countPostalsForStatMap(StatMap::MAP_IN_TRANSPORTATION, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_IN_DELIVERY'] = StatMap::countPostalsForStatMap(StatMap::MAP_IN_DELIVERY, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_DELIVERED'] = StatMap::countPostalsForStatMap(StatMap::MAP_DELIVERED, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_CANCELLED'] = StatMap::countPostalsForStatMap(StatMap::MAP_CANCELLED, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_PROBLEM'] = StatMap::countPostalsForStatMap(StatMap::MAP_PROBLEM, Yii::app()->user->id, 0, $from, $to);
        $data['MAP_RETURN'] = StatMap::countPostalsForStatMap(StatMap::MAP_RETURN, Yii::app()->user->id, 0, $from, $to);

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonPostalWeightTypeStat()
    {
        Yii::app()->getModule('postal');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $result = PanelStats::countPostalWeightTypeStat(Yii::app()->user->id, $from, $to);


        $data = [];
        $data[] = [Yii::t('panel', 'Status'), Yii::t('panel', 'Paczek')];

        foreach(Postal::getWeightClassList() AS $weightClass => $weightClassName) {

            $value = 0;
            foreach($result AS $key => $item)
            {
                if($item['weight_class'] == $weightClass)
                {
                    $value = $item['count'];
                    unset($result[$key]);
                }
            }

            $data[] = [$weightClassName, intval($value)];
        }

        echo CJSON::encode($data);
        Yii::app()->end();
    }


    public function actionJsonPostalPackagesStat()
    {
        Yii::app()->getModule('postal');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = [];
        $data[] = [Yii::t('panel', 'Status'), Yii::t('panel', 'Paczek')];

        foreach(StatMap::mapNameList() AS $key => $stat)
            $data[] = [$stat, intval(StatMap::countPostalsForStatMap($key, Yii::app()->user->id, 0, $from, $to))];

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonPostalPackagesStatPercent()
    {
        Yii::app()->getModule('postal');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $totalTotal = 0;
//        $total7 = 0;
        foreach(StatMap::mapNameList() AS $key => $stat)
        {
            $totalTotal += StatMap::countPostalsForStatMap($key, Yii::app()->user->id, 0, $from, $to);
//            $total7 += StatMap::countPostalsForStatMap($key, Yii::app()->user->id, 7);
        }

        $data = [];
//        $data[] = [Yii::t('panel', 'Status'), Yii::t('panel', 'Paczek - łącznie'), Yii::t('panel', 'Paczek - 7 dni')];
        $data[] = [Yii::t('panel', 'Status'), Yii::t('panel', 'Przesyłek')];


        foreach(StatMap::mapNameList() AS $key => $stat)
            $data[] = [$stat, !$totalTotal ? 0 : (round(StatMap::countPostalsForStatMap($key, Yii::app()->user->id, 0, $from, $to)/$totalTotal,2)*100)];
//            $data[] = [$stat, !$totalTotal ? 0 : (round(StatMap::countPostalsForStatMap($key, Yii::app()->user->id)/$totalTotal,2)*100), !$total7 ? 0 : (round(StatMap::countPostalsForStatMap($key, Yii::app()->user->id,7)/$total7,2)*100)];


        echo CJSON::encode($data);
        Yii::app()->end();
    }



    public function actionJsonCourierLastItems()
    {
        $lastId = $_POST['lastId'];

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = [];
        $lastStats = PanelStats::getLastStatsForCourier(Yii::app()->user->id, $lastId, $from, $to);
        foreach($lastStats AS $item) {
            $map_id = StatMapMapping::findMapForStat($item['current_stat_id'], StatMapMapping::TYPE_COURIER);

            $data[] = [
                'id' => $item['id'],
                'icoClass' => StatMap::getMapCssClassById($map_id),
                'url' => Yii::app()->createUrl('/courier/courier/view', ['urlData' => $item['hash']]),
                'local_id' => $item['local_id'],
                'statName' => StatMap::getMapNameById($map_id),
                'date' => $item['status_date'],
                'receiver' => [
                    'name' => trim($item['name'].' '.$item['company']),
                    'city' => $item['city']
                ],
                'statFullText' => $item['full_text'] ? $item['full_text'] : $item['default_full_text'],
                'location' => $item['location'],
            ];
        }

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonPostalLastItems()
    {
        $lastId = $_POST['lastId'];

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $data = [];
        $lastStats = PanelStats::getLastStatsForPostal(Yii::app()->user->id, $lastId, $from, $to);
        foreach($lastStats AS $item) {
            $map_id = StatMapMapping::findMapForStat($item['current_stat_id'], StatMapMapping::TYPE_POSTAL);

            $data[] = [
                'id' => $item['id'],
                'icoClass' => StatMap::getMapCssClassById($map_id),
                'url' => Yii::app()->createUrl('/postal/postal/view', ['urlData' => $item['hash']]),
                'local_id' => $item['local_id'],
                'statName' => StatMap::getMapNameById($map_id),
                'date' => $item['status_date'],
                'receiver' => [
                    'name' => trim($item['name'].' '.$item['company']),
                    'city' => $item['city']
                ],
                'statFullText' => $item['full_text'],
                'location' => $item['location'],
            ];
        }

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionJsonCourierWeightStat()
    {
        Yii::app()->getModule('courier');

        $from = $_POST['dateFrom'];
        $to = $_POST['dateTo'];

        $result = PanelStats::countCourierWeightStat(Yii::app()->user->id, $from, $to);


        $data = [];
        $data[] = [Yii::t('panel', 'Waga [kg] (zaokrąglona)'), Yii::t('panel', 'Paczek')];

        $heavyCount = 0;
        foreach($result AS $key => $item)
        {
            $weight = $item['package_weight'];
            $number = intval($item['count']);

            if(intval($weight) > 50)
            {
                $heavyCount += $number;
            }
            else
            {
                $data[] = [$weight, $number];
            }
        }

        if(!S_Useful::sizeof($result)) {
            $data[] = [0, 0];
            $data[] = [1, 0];
        }

        if($heavyCount)
            $data[] = ['50+', $heavyCount];

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetClientImg($hash)
    {
        $img = Yii::app()->user->model->clientImg;
        if($img)
        {
            $img = explode(';base64,', $img);
            $type = str_replace('data:','', $img[0]);
            $img = $img[1];

            header("Content-type: ".$type);
            echo base64_decode($img);
        } else
            throw new CHttpException(404);
    }

    public function actionGetSalesmanImg($id)
    {
        $cmd = Yii::app()->db->cache(300)->createCommand();
        $cmd->select('photo');
        $cmd->from('salesman');
        $cmd->where('id = :id', [':id' => $id]);
        $img = $cmd->queryScalar();

        if($img)
        {
            $img = explode(';base64,', $img);
            $type = str_replace('data:','', $img[0]);
            $img = $img[1];

            header("Content-type: ".$type);
            echo base64_decode($img);
        } else
            throw new CHttpException(404);
    }


}