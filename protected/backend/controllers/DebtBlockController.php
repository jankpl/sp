<?php

class DebtBlockController extends Controller
{

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        if(isset($_POST['DebtBlockMessage']))
        {

            $text = $_POST['DebtBlockMessage']['text'];


            foreach($text AS $lang => $item)
            {
                Config::setData(Config::DEBT_BLOCK_CONTENT . '[' . $lang . ']', $item);
            }

            Yii::app()->user->setFlash('success', 'Changes has been saved!');
            $this->refresh();
            exit;
        }

        $this->render('index',[]);
    }

}

