(function() {

    $(document).ready(function(){
        var video = document.getElementById("bg_video");
        var req = new XMLHttpRequest();
        req.open('GET', $('[data-current-vid]').attr('data-current-vid'), true);
        req.responseType = 'blob';
        req.onload = function() {
            if (this.status === 200) {
                var videoBlob = this.response;
                var vid = URL.createObjectURL(videoBlob);
                video.src = vid;
            }
        }
        req.onerror = function() {
        }
        req.send();


        //////////////////////

        AOS.init({
            offset: 200,
            duration: 600,
            delay: 500,
            once: true
        });


        /////////////////////

        $('.home-info-block').on('click', function(){


            var $that = $( this );

            $that.addClass('full');
            $that.find('.info-intro').css('cursor', 'auto');
            $that.find('.show-more-link').remove();

            $( this ).stop(true, true).animate({
                width: '100%',
            }, 500, function() {

            });
            $that.find('.info-content').slideDown(500);
        });

        /////////////////////

        $('#returns-more-button').on('click', function(){
            $(this).slideUp('normal', function(){

                $('#returns-more').slideDown('normal', function(){

                });

            });
        })
    })

})();


(function($) {

    $(document).ready(function(){
        var markers = [];

        var lat = 52.887719;
        var lon = 21.390474;

        var icon = L.icon({
            iconUrl: yii.map.img,
            iconSize: [32, 32]
        });

        map = L.map('sp-points-map').setView([lat, lon], 5);

        map.scrollWheelZoom.disable();
        map.on('focus',  function () {
            map.scrollWheelZoom.enable();
        });
        map.on('blur', function () {
            map.scrollWheelZoom.disable();
        });

        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
            maxZoom: 18,
        }).addTo(map);

        $.each(yii.map.branches, function(i,v){
            marker = L.marker([v.lat, v.lng],{icon: icon}).addTo(map);
            marker.bindPopup('<strong>' + v.name + '</strong><br />' + v.desc + '<br/><br/>' + v.address);
            markers[v.id] = marker;
        });

        $('#map-sp-point-list').on('change', function(){
            markers[$(this).val()].openPopup();
        });

    })
})(jQuery);


// (function($) {

    var Utils = {

        /** @author http://stackoverflow.com/a/7557433/1684970 */
        IsElementInViewport: function(el){
            if (typeof jQuery === "function" && el instanceof jQuery) el = el[0];
            var rect = el.getBoundingClientRect();
            var offsetHeight = $('.tv').height() / 4;

            return (
                (rect.top + offsetHeight) <= (window.innerHeight || document.documentElement.clientHeight) &&
                (rect.bottom + offsetHeight) >= (window.innerHeight || document.documentElement.clientHeight)
            );
        }
    };

    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/player_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var tv,
        playerDefaults = {autoplay: 0, autohide: 1, modestbranding: 0, rel: 0, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3};

    function onYouTubePlayerAPIReady(){
        tv = new YT.Player('tv', {events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange}, playerVars: playerDefaults});
    }

    function onPlayerReady(){

        $(window).on('scroll', function(){
            if( Utils.IsElementInViewport($('#tv')) )
            {
                tv.playVideo();
            }
            else
            {
                tv.pauseVideo();
            }
        });

        tv.loadVideoById({'videoId': 'vPC5ehXDe1U', 'startSeconds': 0, 'suggestedQuality': 'hd720'});
        tv.mute();
        $('#tv-muter').addClass('mute');
    }



    function onPlayerStateChange(e) {
        $('#tv').addClass('active');
    }

    function vidRescale(){

        var w = $('.tv').width();
        var h = (w * 315) / 560;
        $('.tv').height(h);
        var vidW = (h *560)/315;

        tv.setSize(vidW, h);
        $('.tv .screen').css({'left': '0px'});
    }

    $(window).on('load resize', function(){
        vidRescale();
    });

    $('#tv-muter').on('click', function(){
        $(this).toggleClass('mute');
        if($(this).hasClass('mute')){
            tv.mute();
        } else {
            tv.unMute();
        }
    });

// })(jQuery);