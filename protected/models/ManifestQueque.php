<?php

Yii::import('application.models._base.BaseManifestQueque');

class ManifestQueque extends BaseManifestQueque
{
    public $_is_sent;

    const TYPE_COMMON = 5;
    const TYPE_COURIER_LABEL = 1;
    const TYPE_POSTAL_LABEL = 2;


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),
        );
    }

    public function rules() {
        return array(
            array('type, type_id, broker, local_id', 'required'),
            array('type, type_id, broker', 'numerical', 'integerOnly'=>true),
            array('date_sent', 'safe'),
            array('date_sent', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, type, type_id, broker, date_sent, local_id, _is_sent', 'safe', 'on'=>'search'),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('type_id', $this->type_id);
        $criteria->compare('broker', $this->broker);
        $criteria->compare('date_sent', $this->date_sent, true);
        $criteria->compare('local_id', $this->local_id, true);

        if($this->_is_sent == 1)
            $criteria->addCondition('date_sent IS NOT NULL');
        else if($this->_is_sent == -1)
            $criteria->addCondition('date_sent IS NULL');

        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
            'sort' => $sort,
        ));
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_COURIER_LABEL => 'Courier',
            self::TYPE_POSTAL_LABEL => 'Postal',
        ];
    }

    public function getTypeName()
    {
        return isset(self::getTypeList()[$this->type]) ? self::getTypeList()[$this->type] : NULL;
    }

    public static function getBrokerList()
    {
        return GLOBAL_BROKERS::getBrokersList();
    }

    public function getBrokerName()
    {
        return isset(self::getBrokerList()[$this->broker]) ? self::getBrokerList()[$this->broker] : NULL;
    }

    public static function addToQueque($type, $type_id, $broker, $local_id)
    {
        $model = new self;
        $model->type = $type;
        $model->type_id = $type_id;
        $model->broker = $broker;
        $model->local_id = $local_id;

        $model->validate();

        return $model->save();
    }

    public static function runQuequeOnceADay($broker, $forceDate = false)
    {
        MyDump::dump('manifestQueque.txt', 'RUN DAILY: ('.$broker.') ('.$forceDate.')');

        if(!$forceDate)
            $date = date('Y-m-d');
        else
            $date = $forceDate;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(id)');
        $cmd->from('manifest_queque');
        $cmd->where('broker = :broker', [':broker' => $broker]);
        $cmd->andWhere('DATE(date_entered) = :date', [':date' => $date]);
        $result = $cmd->queryScalar();

        MyDump::dump('manifestQueque.txt', 'RESULT: ('.$broker.') ('.$forceDate.')'.' : '.print_r($result,1));

        if(!$result)
        {
            $success = true;
            try
            {
                switch($broker)
                {
                    case GLOBAL_BROKERS::BROKER_GLS_IT:
                        GlsItClient::manifestYesterdaysItems();
                        break;
                    default:
                        throw new Exception('Unknown broker!');
                        break;
                }
            }
            catch(Exception $ex)
            {
                MyDump::dump('manifestQueque.txt', 'ERROR: '.print_r($ex->getMessage(),1));
                $success = false;
            }

            MyDump::dump('manifestQueque.txt', 'SENT ONCE A DAY: ('.$broker.') :'."\r\n".print_r($result,1)."\r\n".print_r($success,1));

            if($success) {

                $model = new ManifestQueque();
                $model->broker = $broker;
                $model->type = 0;
                $model->type_id = 0;
                $model->local_id = 0;
                $model->date_sent = date('Y-m-d H:i:s');
                $model->save();

                return true;
            }
        }
        return false;
    }

    public static function runQuequeSingle($broker, $forceDate = false)
    {

        MyDump::dump('manifestQueque.txt', 'RUN SINGLE: ('.$broker.') ('.$forceDate.')');

        if(!$forceDate)
            $date = date('Y-m-d');
        else
            $date = $forceDate;

        $cmd2 = Yii::app()->db->createCommand();
        $cmd2->select('COUNT(id)');
        $cmd2->from('manifest_queque');
        $cmd2->where('broker = :broker', [':broker' => $broker]);
        $cmd2->andWhere('DATE(date_entered) = :date', [':date' => $date]);
        $cmd2->andWhere('date_sent IS NULL');

//        $result = $cmd2->queryScalar();

        while($cmd2->queryScalar())
        {

            MyDump::dump('manifestQueque.txt', 'RESULT: ('.$broker.') ('.$forceDate.')'.' : '.print_r($result,1));

            $success = true;
            try {
                switch ($broker) {
                    case GLOBAL_BROKERS::BROKER_DEUTCHE_POST:
                        $cmd = Yii::app()->db->createCommand();
                        $cmd->select('local_id');
                        $cmd->from('manifest_queque');
                        $cmd->where('broker = :broker', [':broker' => $broker]);
                        $cmd->andWhere('DATE(date_entered) = :date', [':date' => $date]);
                        $cmd->andWhere('date_sent IS NULL');
                        $local_id = $cmd->queryScalar();
                        DeutchePostClient::closeShipment($local_id);
                        break;
                    case GLOBAL_BROKERS::BROKER_POST11:
                        $cmd = Yii::app()->db->createCommand();
                        $cmd->select('local_id');
                        $cmd->from('manifest_queque');
                        $cmd->where('broker = :broker', [':broker' => $broker]);
                        $cmd->andWhere('DATE(date_entered) = :date', [':date' => $date]);
                        $cmd->andWhere('date_sent IS NULL');
                        $local_id = $cmd->queryScalar();
                        Post11Client::manifestUpdateAndSend($local_id, $date);
                        break;
                    default:
                        throw new Exception('Unknown broker!');
                        break;
                }
            } catch (Exception $ex) {
                MyDump::dump('manifestQueque.txt', 'ERROR: ' . print_r($ex->getMessage(), 1));
                $success = false;
            }

            MyDump::dump('manifestQueque.txt', 'SENT SINGLE: (' . $broker . ') :' . "\r\n" . print_r($result, 1) . "\r\n" . print_r($success, 1));

            if ($success) {

                if ($local_id) {
                    $model = ManifestQueque::model()->find('local_id = :local_id AND date_sent IS NULL', [':local_id' => $local_id]);
                } else {
                    $model = new ManifestQueque();
                    $model->broker = $broker;
                    $model->type = 0;
                    $model->type_id = 0;
                    $model->local_id = 0;
                }

                $model->date_sent = date('Y-m-d H:i:s');
                $model->save();

                return true;
            }

        }
        return false;
    }

    public static function runQueuqe($broker, $fromYesterday = true)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, type, type_id');
        $cmd->from('manifest_queque');
        $cmd->where('date_sent IS NULL AND broker = :broker', [':broker' => $broker]);

        if($fromYesterday)
            $cmd->andWhere('DATE(date_entered) <= DATE(NOW() - INTERVAL 1 DAY)');

        $result = $cmd->queryAll();

        $products = [];

        $ids = [];
        foreach($result AS $item) {
            $products[] = UniversalProduct::createByTypeAndItemId($item['type'], $item['type_id']);
            $ids[] = $item['id'];
        }

        $success = false;
        if(S_Useful::sizeof($ids))
            switch($broker)
            {
                case GLOBAL_BROKERS::BROKER_SWEDEN_POST:
                    Yii::app()->getModule('courier');
                    $success = SwedenPostClient::runManifestQueque($products);
                    break;
                case GLOBAL_BROKERS::BROKER_ANPOST:
                    $success = AnPostClient::runManifestQueque($products);
                    break;
                case GLOBAL_BROKERS::BROKER_YUNEXPRESS_SYNCH:
                    $success = YunExpressSynchronousClient::runManifestQueque($products);
                    break;
                default:
                    throw new Exception('Unknown broker!');
                    break;
            }

        MyDump::dump('manifestQueque.txt', 'SENT: ('.$broker.') :'."\r\n".print_r($result,1));

        if($success) {
            $cmd = Yii::app()->db->createCommand();
            $cmd->update('manifest_queque', ['date_sent' => date('Y-m-d H:i:s')], 'id IN (' . implode(',', $ids) . ')');
        }

        return $success;

    }


    public static function getManifestId($broker_id, $date, $type)
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->select('local_id')->from('manifest_queque')->where('broker = :broker AND date_entered LIKE :date AND type = :type AND date_sent IS NULL', [
            ':broker' => $broker_id,
            ':date' => substr($date, 0,10).' %',
            ':type' => $type,
        ])->queryScalar();
    }

    public static function setManifestId($broker_id, $type, $manifestId)
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->insert('manifest_queque', ['broker' => $broker_id, 'type' => $type, 'local_id' => $manifestId]);
    }
}