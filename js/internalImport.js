function runOrdering(params) {
    LABEL_OK = '<i class="glyphicon glyphicon-ok"></i>';
    LABEL_FAIL = '<i class="glyphicon glyphicon-remove"></i>';
    LABEL_UNKNOWN = '<i class="glyphicon glyphicon glyphicon-question-sign"></i>';

    var $labelsAction = $('#generate-labels-action');
    var $workInProgress = $('#work-in-progress');

    $(document).ready(function(){

        var $collection = $('[data-courier-local-id][data-action=true]');

        var total = $('[data-courier-local-id]').length;

        var counterAtStart = $('[data-label-checkbox=true]').length;

        $workInProgress.find('h3').html(counterAtStart + '/'  + total);

        if($collection.length) {
            $labelsAction.hide();
            $workInProgress.show();
            getLabel($collection, 0, total, counterAtStart);
        }
        else
        {
            $labelsAction.show();
            $workInProgress.hide();
        }

        var $justEbay = $('[data-courier-local-id][data-action=5]');
        if($justEbay.length)
            getEbayStat($justEbay, 0);

    });

    function getEbayStat($collection, counter) {

        var $item = $collection.eq(counter);

        $.ajax({
            method: "POST",
            url: params.urlEbay,
            data: {localId: $item.attr('data-courier-local-id')},
            dataType: 'json'
        })
            .done(function (response) {

                if(response.rerun) {
                    console.log('wait...');
                    setTimeout(function(){
                        console.log('going again...');
                        getEbayStat($collection, counter);
                    }, 5000);
                    return;
                }

                updateItemEbayStat($item, response.stat);

                if (counter < $collection.length) {
                    setTimeout(function(){
                        getEbayStat($collection, counter);
                    }, 500);
                }

            })
            .fail(function (e) {
                console.log(e);
                setTimeout(function(){
                    alert(params.errorUnknown);
                },5000);
            });

    }

    function updateItemEbayStat($item, stat)
    {
        if(stat == params.statEBaySuccess)
            $item.find('.ebay-stat').html(LABEL_OK);
        else if(stat == params.statEBayUnknown)
            $item.find('.ebay-stat').html(LABEL_UNKNOWN);
        else
            $item.find('.ebay-stat').html(LABEL_FAIL);
    }

    function getLabel($collection, counter, total, totalCounter)
    {
        console.log('next...');

        var $item = $collection.eq(counter);

        $.ajax({
            method: "POST",
            url: params.url,
            data: {localId: $item.attr('data-courier-local-id')},
            dataType: 'json'
        })
            .done(function (response) {

                if(response.request) {

                    if(response.rerun) {
                        console.log('wait...');
                        setTimeout(function(){
                            console.log('going again...');
                            getLabel($collection, counter, total, totalCounter);
                        }, 5000);
                        return;
                    }

                    $.notify({
                        icon: 'glyphicon glyphicon-flag',
                        message: params.messageDone,
                        type: 'info',
                    },{
                        allow_dismiss: false,
                        showProgressbar: true,
                    });

                    updateItemStat($item, response.stat, response.desc, response.ebayImport);

                    counter++;
                    $workInProgress.find('h3').html((totalCounter + counter) + '/'  + total);

                    if (counter < $collection.length) {
                        setTimeout(function(){
                            getLabel($collection, counter, total, totalCounter);
                        },250);
                    }
                    else
                    {
                        $workInProgress.hide();
                        $labelsAction.show();
                    }
                } else {
                    $.notify({
                        icon: 'glyphicon glyphicon-flag',
                        message: params.errorUnknown,
                        type: 'info',
                    },{
                        allow_dismiss: false,
                        showProgressbar: true,
                    });
                }

            })
            .fail(function (e) {
                console.log(e);
                setTimeout(function(){
                    alert(params.errorUnknown);
                },5000);

            });

    }


    function updateItemStat($item, stat, desc, ebay)
    {

        var $checkbox = $('<input>');
        $checkbox.attr('type', 'checkbox');
        $checkbox.attr('data-label-checkbox', 'true');

        if(stat == params.statSuccess) {

            $checkbox.attr('name', 'label[' + $item.attr('data-courier-temp-id') + ']');

            $item.find('.label-checkbox').html('');
            $item.find('.label-checkbox').append($checkbox);
            $item.find('.label-stat').html(LABEL_OK);
        }
        else
        {
            $item.find('.label-stat').html(LABEL_FAIL);
            var descShort = '<span title="' + desc + '" style="cursor: help;">' + desc.substring(0,15) + '... (?)</span>';
            $item.find('.label-checkbox').html(descShort);
        }

        if(params.sourceEBay)
        {
            if(ebay == params.statEBaySuccess)
                $item.find('.ebay-stat').html(LABEL_OK);
            else if(ebay == params.statEBayUnknown)
                $item.find('.ebay-stat').html(LABEL_UNKNOWN);
            else
                $item.find('.ebay-stat').html(LABEL_FAIL);

        }
    }

}