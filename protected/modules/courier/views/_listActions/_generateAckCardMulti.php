<?php
/* @var $id_prefix string */
/* @var $form GxActiveForm */
?>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-ack-card',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/ackCardMulti'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('courier', 'Generate ACK card'), array(
    'onClick' => 'js:
    $("#'.$id_prefix.'courier-ids-ack").val($("#couriergrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>
<br/>
<br/>
<?= CHtml::label(Yii::t('courier', 'Ignoruj karty operatorów'), 'ignoreExternalCards', ['style' => 'font-weight: normal; display: inline-block; width: auto; text-align: left; padding-left: 5px;']);?>
<?php echo CHtml::checkBox('ignoreExternalCards','', ['style' => 'float: left;']); ?>
<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => $id_prefix.'courier-ids-ack')); ?>
<?php
$this->endWidget();
?>

