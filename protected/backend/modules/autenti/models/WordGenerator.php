<?php

class WordGenerator
{

    public static function getTemplatePath()
    {
        return Yii::app()->getModulePath().DIRECTORY_SEPARATOR.Yii::app()->controller->module->id.DIRECTORY_SEPARATOR.'misc'.DIRECTORY_SEPARATOR.'umowa.docx';
    }

    public static function generate(CreateForm $model)
    {
        $params = [
            'NR_UMOWY' => $model->no,
            'DATA_UMOWY' => $model->date,
            'NAZWA_FIRMY' => $model->company,
            'FORMA_PRAWNA' => $model->form,
            'ADRES_1' => $model->address_line_1,
            'ADRES_2' => $model->address2,
            'SAD' => $model->sad,
            'KRS' => $model->krs,
            'NIP' => $model->nip,
            'REGON' => $model->regon,
            'KAPITAL' => $model->kapital,
            'EMAIL' => $model->email,
            'OSOBA_1' => $model->person1,
            'OSOBA_2' => $model->person2,
            'OSOBA_KONTAKT' => $model->person_contact,
            'OSOBA_KONTAKT_EMAIL' => $model->person_contact_email,
            'OSOBA_ROZLICZENIA' => $model->person_rozliczenia,
            'OSOBA_ROZLICZENIA_EMAIL' => $model->person_rozliczenia_email,
            'OSOBA_REKLAMACJE' => $model->person_reklamacje,
            'OSOBA_REKLAMACJE_EMAIL' => $model->person_reklamacje_email,
            'OPIEKUN' => $model->salesman,
            'OPIEKUN_TEL' => $model->salesman_tel,
            'NR_KONTA' => $model->account_no,
        ];

        $tempFile = tempnam(sys_get_temp_dir(), date('YmdHis'));
        copy(self::getTemplatePath(), $tempFile);
        $docx = new IRebega\DocxReplacer\Docx($tempFile);

        foreach($params AS $key => $value) {
            $docx->replaceText('**' . $key . '**', $value);
        }

//        $tempDir = Yii::app()->basePath.DIRECTORY_SEPARATOR.'_temp';
        $tempDir = sys_get_temp_dir();

        $tempDocx = $tempDir.DIRECTORY_SEPARATOR.date('YmdHis').uniqid().'.docx';

        copy($tempFile, $tempDocx);
        $resp = exec('libreoffice --headless --convert-to pdf '.$tempDocx.' --outdir '.$tempDir);

        return str_replace('.docx', '.pdf', $tempDocx);
    }
}