<?php

class QuequeController extends CController {


    const STRIP_SIZE = 500;

    // run queque
    public function actionIndex($more = 0)
    {
        $CACHE_NAME = 'QUEQUE_LOCK_'.$more;

        if (Yii::app()->cache->get($CACHE_NAME) !== false) {
            MyDump::dump('queque-lock.txt','LOCK : '.$more);
            return false;
        }
        Yii::app()->cache->set($CACHE_NAME, 10, 15);

        $load = S_SystemLoad::get();
        $isHigh = S_SystemLoad::isHigh();

        MyDump::dump('queque_load.txt', ($isHigh ? 'BLOCK' : 'GO').' : '.print_r($load,1));

        if($isHigh)
            return false;

        $id = uniqid();

        MyDump::dump('queque.txt',$id.':START');
        Yii::app()->Queque->run(self::STRIP_SIZE);
        MyDump::dump('queque.txt',$id.':END');
        // there is more
        if(Yii::app()->Queque->howLong())
        {
            MyDump::dump('queque.txt',$id.':MORE');
            $path = Yii::app()->createAbsoluteUrl('/queque/index', ['more' => ++$more]);

            header("Location: ".$path);
            exit;

        } else {
            // clear old items if there is nothing else to do...
            Yii::app()->Queque->clearOldAndFinished();

            PhpErrorToYiiLog::run('rror.txt', 'PHP FRONT ERROR');
            PhpErrorToYiiLog::run('rror2.txt', 'PHP BACK ERROR');
        }

        exit;
    }

    public function actionClearOldOrders()
    {

        $models = Order::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY) AND (order_stat_id =:order_stat_1 OR order_stat_id = :order_stat_2) ', [':days' => 7, ':order_stat_1' => OrderStat::NEW_ORDER, ':order_stat_2' => OrderStat::WAITING_FOR_PAYMENT]);

        /* @var $model Order */
        foreach($models AS $model)
            $model->changeStat(OrderStat::CANCELLED);

    }

    public function actionClearAckCardArchive()
    {
        AckCardArchive::deleteOldItems();
    }

}