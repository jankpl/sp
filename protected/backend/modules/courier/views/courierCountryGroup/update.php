<?php
/* @var $model CourierCountryGroup */
?>

<?php
$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label' =>'Create ' . $model->label(), 'url'=>array('create')),
    array('label' =>'Manage ' . $model->label(2), 'url'=>array('admin')),
);
?>

    <h1>Update Country Group: <?= GxHtml::encode(GxHtml::valueEx($userGroup != NULL ? $model->courierCountryGroupCustomParent : $model)); ?></h1>
<?php
if($userGroup !== NULL):
    ?>
    <div class="alert alert-warning">
        <h2>For user group: <?= $userGroupModel->name;?></h2>
    </div>

<?php
elseif(!$model->isNewRecord):
    ?>
    <div class="alert alert-info">
        <p>Edit custom settings for user group:</p>
        <?= CHtml::dropDownList('user_group','', $listUserGroups, array('id' => 'group_edit')); ?>
        <?= CHtml::button('Edit',array('onClick' => 'window.location.assign("'.Yii::app()->urlManager->createUrl('/courier/courierCountryGroup/update', ['id' => $model->id]).'?user_group=" + $("#group_edit").val())')); ?>
    </div>
<?php
endif;
?>



<?php
if($userGroup === NULL && !AdminRestrictions::isRoutingAdmin()):
    ?>
    <div class="alert alert-warning text-center">You have no authority to modify main routing.</div>
    <br/>
    <h2>Settings:</h2>


    <div style="overflow: auto;">
        <div style="float: left; width: 49%;">
            <h3>Pickup:</h3>

            <?php
            if($model->isPickupAvailable()):
                ?>

                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Operator</th>
                        <td><?= $model->pickupOperator->getNameWithIdAndBroker();?></td>
                    </tr>
                    <tr>
                        <th>HUB</th>
                        <td><?= $model->pickupHub->getNameWithId();?></td>
                    </tr>
                </table>

                <h4>Custom:</h4>

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>No (*)</th>
                        <th>Weight from [<?= Courier::getWeightUnit();?>]</th>
                        <th>Weight to [<?= Courier::getWeightUnit();?>]</th>
                        <th>Operator</th>
                        <th>Hub</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    if(is_array($modelsPickup)):
                        foreach($modelsPickup AS $model2):
                            ?>
                            <tr>
                                <td><?= $i;?></td>
                                <td><?= $model2->weight_from;?></td>
                                <td><?= $model2->weight_to;?></td>
                                <td><?= $model2->operator->nameWithIdAndBroker;?></td>
                                <td><?= $model2->hub->nameWithId;?></td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            <?php
            else:
                ?>
                -
            <?php
            endif;
            ?>
        </div>

        <div style="float: right; width: 49%;">
            <h3>Delivery:</h3>

            <table class="table table-bordered table-striped">
                <tr>
                    <th>Operator</th>
                    <td><?= $model->deliveryOperator->getNameWithIdAndBroker();?></td>
                </tr>
                <tr>
                    <th>HUB</th>
                    <td><?= $model->deliveryHub->getNameWithId();?></td>
                </tr>
            </table>

            <h4>Custom:</h4>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>No (*)</th>
                    <th>Weight from [<?= Courier::getWeightUnit();?>]</th>
                    <th>Weight to [<?= Courier::getWeightUnit();?>]</th>
                    <th>Operator</th>
                    <th>Hub</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if(is_array($modelsDelivery)):
                    foreach($modelsDelivery AS $model2):
                        ?>
                        <tr>
                            <td><?= $i;?></td>
                            <td><?= $model2->weight_from;?></td>
                            <td><?= $model2->weight_to;?></td>
                            <td><?= $model2->operator->nameWithIdAndBroker;?></td>
                            <td><?= $model2->hub->nameWithId;?></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                endif;
                ?>
                </tbody>
            </table>
        </div>
    </div>


<?php
else:
    ?>

    <?php
    $this->renderPartial('_form', array(
        'model' => $model,
        'modelsDelivery' => $modelsDelivery,
        'modelsPickup' => $modelsPickup,
        'modelDeliveryNew' => $modelDeliveryNew,
        'modelPickupNew' => $modelPickupNew,
        'allowExtendedOperators' => true,
        'userGroup' => $userGroup,
        'userGroupModel' => $userGroupModel
    ));
    ?>

<?php
endif;


