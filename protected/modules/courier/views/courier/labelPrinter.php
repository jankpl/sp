<?php

/* @var $this CourierController */
/* @var $model Courier */
?>


<h2><?= Yii::t('courierLabelPrinter', 'Drukowanie etykiet');?></h2>

<?php
/* @var $form CActiveForm */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'label-printer-form',
    'enableClientValidation' => false,
    'htmlOptions' => [
        'class' => 'do-not-block',
    ]
)); ?>

<div class="form">

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <p><?= Yii::t('courierLabelPrinter', 'Podaj numer przesyłki, dla której chcesz wygenerować etykietę.');?></p>


    <table class="table table-striped table-bordered" style="max-width: 500px; margin: 0 auto;">
        <tr>
            <td class="text-center"><?php echo CHtml::textField('local_id', '', array('id' => 'local_id', 'placeholder' => Yii::t('courierLabelPrinter', 'number paczki'), 'style' => 'text-align: center; padding: 20px; width: 100%;')); ?> </td>
        </tr>
        <tr>
            <td><?php echo CHtml::radioButton('print_option', true, array('id' => 'print_option_0', 'value' => 0)); ?> <label for="print_option_0" style="display: inline;"><?= Yii::t('courierLabelPrinter', 'Pokaż opcje');?></label></td>
        </tr>
        <tr>
            <td><?php echo CHtml::radioButton('print_option', '', array('id' => 'print_option_1', 'value' => 1)); ?> <label for="print_option_1" style="display: inline;"><?= Yii::t('courierLabelPrinter', 'Automatycznie generuj etykietę w formacie: {format}', ['{format}' => LabelPrinter::getLabelGenerateTypes()[LabelPrinter::PDF_ROLL]]);?></label></td>
        </tr>
    </table>
</div><!-- form -->

<?php
$this->endWidget();
?>
<br/>
<div class="text-center">
<img src="<?php echo Yii::app()->baseUrl;?>/images/layout/preloader.gif" id="ajax-preloader" style="display: none;"/>
</div>
<br/>
<table class="" style="width: 500px; margin: 0 auto;">
    <tr>
        <td id="labels-placeholder" class="text-center" style="vertical-align: top;">

            <?php
            if(isset($_POST['local_id']))
            {
                $this->renderPartial('_labelPrinter_buttons', array('model' => $model));
            }
            ?>
        </td>
    </tr>
</table>

<?php
Yii::app()->clientScript->registerCoreScript('jquery');
?>

<script>
    $(document).ready(function() {

        $("#local_id").focus();

        $("#local_id").on("keypress", function (e) {
            if (e.which == 13) {
                e.preventDefault();
                submit();
            }
        });

        $("#local_id").on("paste", submit);

        function submit()
        {
            if($("#local_id").val() != '') {

                $("#ajax-preloader").show();
                $("#labels-placeholder").html('');

                $.ajax({
                    method: "POST",
                    dataType: 'json',
                    url: "<?php echo Yii::app()->createUrl('/courier/courier/labelPrinter'); ?>",
                    data: {
                        local_id: $("#local_id").val(),
                        ajax: 1,
                        print_option: $("[name='print_option']:checked").val(),
                        ip: $('#ip').val(),
                        port: $('#port').val()
                    },
                    success: function (val) {

                        if (val.redirect) {
                            window.location.href = val.url;
                        } else {
                            $("#ajax-preloader").show();
                            $("#labels-placeholder").html(val.html);
                            $("#ajax-preloader").fadeOut('');
                        }
                    }
                });
            }

        }
    });
</script>