<?php
/* @var $model Courier */
/* @var $i Integer */
/* @var $ebay boolean */
/* @var $bringSenderNameForward boolean */


$rowWithErrors = true;
if(!$model->hasErrors() && !$model->courierTypeInternal->hasErrors() && !$model->senderAddressData->hasErrors() && !$model->receiverAddressData->hasErrors())
    $rowWithErrors = false;

if(!$rowWithErrors && S_Useful::sizeof($model->courierTypeInternal->_imported_additions_fail))
    $rowWithErrors = -1;
?>
<tr class="condensed <?= $rowWithErrors === true ? 'row-errored' : ($rowWithErrors === -1 ? 'row-warned' : '');?>" data-row-id="<?= $i;?>">
    <td>
        <?= ($i+1);?>
    </td>
    <td>
        <?php if($rowWithErrors === true)
            echo '<i class="glyphicon glyphicon-remove" style="cursor: help;" title="'.Yii::t('courier', 'Błąd wymagający korekty').'"></i>';
        else if($rowWithErrors == -1)
            echo '<i class="glyphicon glyphicon-exclamation-sign" style="cursor: help;" title="'.Yii::t('courier', 'Ostrzeżenie nie wymagające korekty').'"></i>';
        else
            echo '<i class="glyphicon glyphicon-ok" style="cursor: help;" title="'.Yii::t('courier', 'Dane są poprawne').'"></i>';
        ?>
    </td>
    <?php if($ebay):?>
        <td>
            <?= $model->courierTypeInternal->_ebay_order_id; ?>
        </td>
    <?php endif;?>
    <td>
        <input type="button" value="<?php echo Yii::t('courier', 'Remove');?>" data-remove-this-package="true"/>
    </td>
    <td>
        <input type="button" value="<?php echo Yii::t('courier', 'Edit');?>" data-edit-this-package="true"/>
    </td>
    <td style="text-align: center;">
        <?php
        if(!Yii::app()->user->model->getHidePrices()):
            ?>
            <?= $model->courierTypeInternal->_price_total != '' ? $model->courierTypeInternal->_price_total.' '.$model->courierTypeInternal->_price_currency : '-' ; ?>
        <?php
        endif;
        ?>
    </td>
    <?php
    if($bringSenderNameForward):
        ?>
        <td style="border-left: 2px dashed gray; <?php echo ($model->receiverAddressData->hasErrors('name')?'background: red;':'');?>" title="<?= $model->receiverAddressData->getError('name');?>">
            <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]name', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.name')); ?>
        </td>
        <td <?php echo ($model->receiverAddressData->hasErrors('company')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('company');?>">
            <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]company', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.company')); ?>
        </td>
    <?php
    endif;
    ?>
    <td <?= $bringSenderNameForward ? 'style="border-left: 2px dashed gray;"' : '';?> <?php echo ($model->courierTypeInternal->hasErrors('with_pickup')?'class="td-with-error"':'');?> title="<?= $model->courierTypeInternal->getError('with_pickup');?>">
        <?php echo CHtml::activeDropDownList($model->courierTypeInternal, '['.$i.']with_pickup', CourierTypeInternal::getWithPickupList(true,true), ['disabled' => 'disabled', 'style' => 'width: 95px;']); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_weight')?'class="td-with-error"':'');?> title="<?= $model->getError('package_weight');?>">
        <?php echo CHtml::activeNumberField($model,'['.$i.']package_weight', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'package_weight', 'step' => 0.1)); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_l')?'class="td-with-error"':'');?> title="<?= $model->getError('package_size_l');?>">
        <?php echo CHtml::activeNumberField($model,'['.$i.']package_size_l', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'package_size_l')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_w')?'class="td-with-error"':'');?> title="<?= $model->getError('package_size_w');?>">
        <?php echo CHtml::activeNumberField($model,'['.$i.']package_size_w', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'package_size_w')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_d')?'class="td-with-error"':'');?> title="<?= $model->getError('package_size_d');?>">
        <?php echo CHtml::activeNumberField($model,'['.$i.']package_size_d', array('style' => 'width: 75px;', 'data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'package_size_d')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_value')?'class="td-with-error"':'');?> title="<?= $model->getError('package_value');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']package_value', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('value_currency')?'class="td-with-error"':'');?> title="<?= $model->getError('value_currency');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']value_currency', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_content')?'class="td-with-error"':'');?> title="<?= $model->getError('package_content');?>">
        <?php echo CHtml::activeTextField($model,'['.$i.']package_content', array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->courierTypeInternal->hasErrors('_package_wrapping')?'class="td-with-error"':'');?> title="<?= $model->courierTypeInternal->getError('_package_wrapping');?>">
        <?php echo CHtml::activeDropDownList($model->courierTypeInternal,'['.$i.']_package_wrapping', User::getWrappingList(), array('style' => 'width: 75px;', 'disabled' => 'disabled')); ?>
    </td>
    <td style="border-left: 2px dashed gray; <?php echo ($model->senderAddressData->hasErrors('name')?'background: red;':'');?>" title="<?= $model->senderAddressData->getError('name');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]name', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.name')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('company')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('company');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]company', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.company')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('country_id')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('country_id');?>">
        <?php echo CHtml::activeHiddenField($model->senderAddressData,'['.$i.'][sender]country_id', array('disabled' => 'disabled')); ?>
        <?php echo CHtml::textField('['.$i.'][sender]country_temp', $model->senderAddressData->country0 !== NULL ? $model->senderAddressData->country0->getTrName() : '', array('disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('zip_code')?'class="td-with-error"':'');?>>
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]zip_code', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.zip_code')); ?>
        <?= $model->senderAddressData->getError('zip_code');?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('city')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('city');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]city', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.city')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('address_line_1')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('address_line_1');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]address_line_1', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.address_line_1')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('address_line_2')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('address_line_2');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]address_line_2', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.address_line_2')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('tel')?'class="td-with-error"':'');?>  title="<?= $model->senderAddressData->getError('tel');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]tel', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.tel')); ?>
    </td>
    <td style="border-right: 2px dashed gray;" <?php echo ($model->senderAddressData->hasErrors('email')?'class="td-with-error"':'');?> title="<?= $model->senderAddressData->getError('email');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]email', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'senderAddressData.email')); ?>
    </td>
    <?php
    if(!$bringSenderNameForward):
        ?>
        <td style="border-left: 2px dashed gray; <?php echo ($model->receiverAddressData->hasErrors('name')?'background: red;':'');?>" title="<?= $model->receiverAddressData->getError('name');?>">
            <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]name', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.name')); ?>
        </td>
        <td <?php echo ($model->receiverAddressData->hasErrors('company')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('company');?>">
            <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]company', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.company')); ?>
        </td>
    <?php
    endif;
    ?>
    <td <?= $bringSenderNameForward ? 'style="border-left: 2px dashed gray;"' : '';?> <?php echo ($model->receiverAddressData->hasErrors('country_id')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('country_id');?>">
        <?php echo CHtml::activeHiddenField($model->receiverAddressData,'['.$i.'][receiver]country_id', array('disabled' => 'disabled')); ?>
        <?php echo CHtml::textField('['.$i.'][sender]country_temp', $model->receiverAddressData->country0 !== NULL ? $model->receiverAddressData->country0->getTrName() : '', array('disabled' => 'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('zip_code')?'class="td-with-error"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]zip_code', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.zip_code')); ?>
        <?= $model->receiverAddressData->getError('zip_code');?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('city')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('city'); ;?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]city', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.city')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('address_line_1')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('address_line_1');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]address_line_1', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.address_line_1')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('address_line_2')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('address_line_2');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]address_line_2', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.address_line_2')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('tel')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('tel');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]tel', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.tel')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('email')?'class="td-with-error"':'');?> title="<?= $model->receiverAddressData->getError('email');?>">
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]email', array('data-autoupdate' => true, 'data-i' => $i, 'data-attribute' => 'receiverAddressData.email')); ?>
    </td>
    <td style="border-left: 2px dashed gray;" title="<?= $model->getError('_imported_additions_ok');?>">
        <?php
        if(is_array($model->courierTypeInternal->_imported_additions_ok) && S_Useful::sizeof($model->courierTypeInternal->_imported_additions_ok))
            echo '(</trong>'.S_Useful::sizeof($model->courierTypeInternal->_imported_additions_ok).'</strong>) '.CHtml::dropDownList('additions_ok', false, $model->courierTypeInternal->_imported_additions_ok);
        else
            echo '-';
        ?>
    </td>
    <td <?php echo (S_Useful::sizeof($model->courierTypeInternal->_imported_additions_fail) ?'class="td-with-warning"':'');?> title="<?= $model->getError('_imported_additions_fail');?>">
        <?php
        if(is_array($model->courierTypeInternal->_imported_additions_fail) && S_Useful::sizeof($model->courierTypeInternal->_imported_additions_fail))
            echo '(</trong>'.S_Useful::sizeof($model->courierTypeInternal->_imported_additions_fail).'</strong>) '.CHtml::dropDownList('additions_fail', false, $model->courierTypeInternal->_imported_additions_fail);
        else
            echo '-';
        ?>
    </td>
</tr>