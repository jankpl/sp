<?php
/* @var $this SiteController */
/* @var $model LinkerFormBuilder */
/* @var $form CActiveForm */
?>

<div class="form">
    <h2><?= Yii::t('linkerManager', 'Linker');?></h2>
    <h3><?= Yii::t('linkerManager', 'Nowa integracja');?> : <?= $model->adapter;?> - <?= Yii::t('linkerManager', 'krok');?> <?= $model->step;?>/2</h3>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="col-xs-12 col-sm-8">
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'linker-form',
        )); ?>
        <?php echo $form->errorSummary($model); ?>

        <?php echo $model->build($form); ?>


        <p class="note">
            <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
        </p>

        <div class="navigate text-center">
            <?php echo TbHtml::submitButton($model->step == 2 ? Yii::t('linkerManager', 'Zakończ') : Yii::t('linkerManager', 'Dalej'), ['class' => 'btn '.($model->step == 2 ? 'btn-success' : 'btn-primary').' btn-lg']); ?>
        </div>

        <?php echo CHtml::hiddenField('step', $model->step); ?>
        <?php echo CHtml::hiddenField('step2Id', $model->step2Id); ?>
        <?php echo CHtml::hiddenField('internalName', $model->internalName); ?>
        <?php $this->endWidget(); ?>
    </div>
</div>