<?php

class SmsArchiveController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {

        $model = new SmsArchive('search');
        $model->setMode(SmsArchive::TYPE_COURIER);
        $model->unsetAttributes();

        if (isset($_GET['SmsArchive']))
            $model->setAttributes($_GET['SmsArchive']);

        $this->render('index', array(
            'model' => $model,
        ));
    }


}