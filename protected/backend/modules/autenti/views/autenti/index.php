<h2>Autenti</h2>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'autenti',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        [
            'name' => 'user',
            'value' => '$data->user->login',
        ],
        [
            'name' => 'admin_id',
            'value' => '$data->admin->login',
            'filter' => CHtml::listData(Admin::model()->findAll(),'id', 'login'),
        ],
        'date_entered',
        'status',
        'autenti_document_id'
    ),
)); ?>


<a href="<?= Yii::app()->createUrl('/autenti/autenti/checkWaitingAgreements');?>" class="btn btn-primaty">Check agreements statuses</a>
