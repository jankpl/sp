<?php

Yii::import('application.modules.courier.models._base.BaseCourierLog');

class CourierLog extends BaseCourierLog
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public $_local_id;

	const CAT_INFO = 1;
	const CAT_ERROR = 9;

	public static function getCatList()
	{
		$data[self::CAT_INFO] = 'Info';
		$data[self::CAT_ERROR] = 'Error';

		return $data;
	}

	public function getCatName()
	{
		return self::getCatList()[$this->cat];
	}

	public function rules() {
		return array(
			array('date_entered', 'default',
				'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

			array('cat', 'default', 'value' => self::CAT_INFO),

			array('courier_id, text', 'required'),
			array('courier_id, cat', 'numerical', 'integerOnly'=>true),
			array('source', 'length', 'max'=>128),
			array('cat, source', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, courier_id, date_entered, text, cat, source, _local_id', 'safe', 'on'=>'search'),
		);
	}

	public function beforeSave()
	{
		$this->text = strip_tags($this->text);
		$this->text = trim($this->text);

		return parent::beforeSave();
	}

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('courier_id', $this->courier_id);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('cat', $this->cat);
        $criteria->compare('source', $this->source, true);

        $criteria->compare('courier.local_id', $this->_local_id, true);

        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';

        return new CActiveDataProvider($this->with('courier'), array(
            'criteria' => $criteria,
            'sort'=> $sort,
            'Pagination' => array (
                'PageSize' => 50,
            ),
        ));
    }


}