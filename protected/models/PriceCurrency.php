<?php

/*
 * This class used to be using data from DB, but now it's hardcoded
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property string $symbol
 */

class PriceCurrency
{

    const PLN_ID = 1;
    const EUR_ID = 2;
    const GBP_ID = 3;

    const PLN = 'PLN';
    const EUR = 'EUR';
    const GBP = 'GBP';

//    public static function model($className=__CLASS__) {
//		return parent::model($className);
//	}

    protected static function _data()
    {
        $data = [];
        $data[self::PLN_ID] = new stdClass();
        $data[self::PLN_ID]->id = self::PLN_ID;
        $data[self::PLN_ID]->name = 'złotówki';
        $data[self::PLN_ID]->short_name = self::PLN;
        $data[self::PLN_ID]->symbol = 'zł';

        $data[self::EUR_ID] = new stdClass();
        $data[self::EUR_ID]->id = self::EUR_ID;
        $data[self::EUR_ID]->name = 'euro';
        $data[self::EUR_ID]->short_name = self::EUR;
        $data[self::EUR_ID]->symbol = '€';

        $data[self::GBP_ID] = new stdClass();
        $data[self::GBP_ID]->id = self::GBP_ID;
        $data[self::GBP_ID]->name = 'funty';
        $data[self::GBP_ID]->short_name = self::GBP;
        $data[self::GBP_ID]->symbol = '£';

        return $data;
    }

    /**
     * Fake cache...
     * @param $value Dummy value...
     * @return PriceCurrency
     */
    public function cache($value = 0)
    {
        return new self;
    }

    public static function model()
    {
        return new self;
    }

    public function findAll()
    {
        return self::_data();
    }

    public function findByPk($pk)
    {
        $data = self::_data();

        if(isset($data[$pk]))
            return $data[$pk];
        else
            return null;
    }

    public function find()
    {
        return self::_data()[self::PLN_ID];
    }

    public function findByAttributes($attributes)
    {
        foreach(self::_data() AS $currency)
        {
            $found = true;
            foreach($attributes AS $attrName => $attrValue)
            {
                // unknown attr
                if(!$currency->$attrName)
                    return NULL;

                if($currency->$attrName != $attrValue)
                    $found = false;
            }

            if($found)
                return $currency;

        }
    }

    public static function nameById($id)
    {
        $data = self::_data();

        if(isset($data[$id]))
            return $data[$id]->short_name;
        else
            return null;
    }

    public static function idByName($name)
    {
        $data = self::_data();

        $name = strtoupper($name);

        $id = false;

        foreach($data AS $key => $item)
        {
            if($item->short_name == $name) {
                $id = $key;
                break;
            }
        }

        return $id;
    }

    public static function listIds()
    {
        return [
            self::PLN_ID,
            self::EUR_ID,
            self::GBP_ID,
        ];
    }

    public static function listIdsWithNames()
    {
        return [
            self::PLN_ID => self::nameById(self::PLN_ID),
            self::EUR_ID => self::nameById(self::EUR_ID),
            self::GBP_ID => self::nameById(self::GBP_ID),
        ];
    }

}