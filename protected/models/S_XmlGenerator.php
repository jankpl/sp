<?php

class S_XmlGenerator
{

    public static function fillEmptyKeys($data)
    {

        foreach($data AS $key => $item) {

            $max = max(array_keys($item));

            for ($i = 0; $i <= $max; $i++) {
                if (!isset($data[$key][$i]))
                    $data[$key][$i] = NULL;
            }

            ksort($data[$key]);
        }

        return $data;
    }


    public static function generateXml(array $data, $name = false, $returnString = false)
    {
        if($name) {
            $name = explode('.', $name);

            // remove externsion in case there is
            if (S_Useful::sizeof($name) > 1)
                array_pop($name);

            $name = implode('.', $name);
        }


//        Yii::import('application.extensions.phpexcel.XPHPExcel');
//        XPHPExcel::init();

        if(!$name)
            $name = 'export_'.date('Y-m-d');

//        $timer = new S_Timer();
//        $timer->init();

//        $doc = new PHPExcel();

//        PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_MyColumnValueBinder() );

//        $doc->setActiveSheetIndex(0);
//        $doc->getActiveSheet()->fromArray($data, '');


//        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
//        $objWriter->setPreCalculateFormulas(false);


        $writer = Box\Spout\Writer\WriterFactory::create(Box\Spout\Common\Type::XLSX); // for XLSX files

        if($returnString) {
            $tempFilePath = sys_get_temp_dir().DIRECTORY_SEPARATOR.'export_temp_'.uniqid();
            $writer->openToFile($tempFilePath);
        }
        else
            $writer->openToBrowser($name.'.xlsx');

        $writer->addRows($data);

//        if(!$returnString) {
//            $filename = $name . '.xlsx';
//            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//            header('Content-Disposition: attachment;filename="' . $filename . '"');
//            header('Cache-Control: max-age=0'); //no cache
//        }


//        $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
//        $writer->save('php://output');
        $writer->close();

        if($returnString) {
            $stringData = file_get_contents($tempFilePath);
            @unlink($tempFilePath);
            return $stringData;
        }


//        $timer->step();
    }


}
