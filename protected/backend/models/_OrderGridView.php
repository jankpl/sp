<?php

class _OrderGridView extends Order
{
    public $__username;


    public function rules() {

        $array = array(
            array('__username,
                ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }


    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.local_id', $this->local_id, true);
        $criteria->compare('t.value_netto', $this->value_netto);
        $criteria->compare('t.value_brutto', $this->value_brutto);
        $criteria->compare('t.currency', $this->currency, true);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('t.hash', $this->hash, true);
        $criteria->compare('t.finalized', $this->finalized);
        $criteria->compare('t.paid', $this->paid);
        $criteria->compare('t.discount_value', $this->discount_value);
        $criteria->compare('t.payment_commision_value', $this->payment_commision_value);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.owner_data_id', $this->owner_data_id);
        $criteria->compare('t.payment_type_id', $this->payment_type_id, true);
        $criteria->compare('t.order_stat_id', $this->order_stat_id);
        $criteria->compare('t.product_type', $this->product_type);

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR) {
            $criteria->together  =  true;
            $criteria->with = array('user');
            $criteria->compare('user.salesman_id', Yii::app()->user->model->salesman_id);
        }

        if($this->__payment_type)
        {
            $criteria->together  =  true;
            $criteria->with = array('paymentType');
            $criteria->compare('paymentType.id',$this->__payment_type);
        }

        if($this->__username)
        {
            $criteria->together  =  true;
            $criteria->with = array('user');
            $criteria->compare('user.login',$this->__username, true);
        }

        // INSECURE - SQL INJECTION:
        /*if($this->__total_value)
        {
            $criteria->together  =  true;
            $criteria->with = array('orderValues');
            $criteria->group = 'order_id';
            $criteria->having ='SUM(value_brutto) '.$this->__total_value;

        }*/

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.date_entered DESC';
        $sort->attributes = array(
            '__payment_type' => array
            (
                'asc' => 'paymentType.name asc',
                'desc' => 'paymentType.name desc',
            ),
            '*', // add all of the other columns as sortable
        );

        return new CActiveDataProvider($this->with('paymentType'), array(
            'criteria' => $this->searchCriteria(),
            'sort'=> $sort,
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
    }

}