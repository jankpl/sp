<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.hotkeys.js');

?>
    <script>
        $(document).ready(function(){

            var isChrome = !!window.chrome;

            var bindAction = 'keypress';

            if(isChrome)
                bindAction = 'keydown';


            $('body, input').bind(bindAction, 'ctrl+s', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                $('input[name="save"]').trigger('click');
                return false;
            });

            $('body, input').bind(bindAction, 'ctrl+q', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                $('input[name="validate"]').trigger('click');
                return false;
            });

            $('body, input').bind(bindAction, 'ctrl+d', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                $('input[name="saveAndReturn"]').trigger('click');
                return false;
            });

        });
    </script>
<?php

/* @var $model Courier */

$this->menu = array(
    array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('index')),
    array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

    <h2><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h2>
    <div class="form">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'courier-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
        ));
        ?>

        <?php if(Yii::app()->user->hasFlash('previousModel'))
        {
            $previousModel = Yii::app()->user->getFlash('previousModel');
            echo TbHtml::alert(TbHtml::LABEL_COLOR_SUCCESS,'Dodano paczkę ID: '.CHtml::link($previousModel->local_id, array('/courier/courier/view', 'urlData' => $previousModel->hash), array('target' => '_blank')));
        }?>

        <div class="inline-form">

            <fieldset>
                <legend><?php echo Yii::t('courier','Package data');?></legend>

                <div style="padding: 0 30px;">
                    <div class="row inline-form">
                        <div class="col-xs-12 col-md-6">
                            <div class="row">
                                <?php echo $form->labelEx($model,'package_weight'); ?>
                                <?php echo $form->textField($model, 'package_weight'); ?>
                                <?php echo $form->error($model,'package_weight'); ?>
                            </div><!-- row -->
                            <div class="row">
                                <?php echo $form->labelEx($model,'package_size_l'); ?>
                                <?php echo $form->textField($model, 'package_size_l'); ?>
                                <?php echo $form->error($model,'package_size_l'); ?>
                            </div><!-- row -->
                            <div class="row">
                                <?php echo $form->labelEx($model,'package_size_w'); ?>
                                <?php echo $form->textField($model, 'package_size_w'); ?>
                                <?php echo $form->error($model,'package_size_w'); ?>
                            </div><!-- row -->
                            <div class="row">
                                <?php echo $form->labelEx($model,'package_size_d'); ?>
                                <?php echo $form->textField($model, 'package_size_d'); ?>
                                <?php echo $form->error($model,'package_size_d'); ?>
                            </div><!-- row -->
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="row">
                                <?php echo $form->labelEx($model,'packages_number'); ?>
                                <?php echo $form->dropDownList($model, 'packages_number', S_Useful::generateArrayOfItemsNumber(1,100)); ?>
                                <?php echo $form->error($model,'packages_number'); ?>
                            </div><!-- row -->
                            <div class="row">
                                <?php echo $form->labelEx($model,'package_value'); ?>
                                <?php echo $form->textField($model, 'package_value'); ?>
                                <?php echo $form->error($model,'package_value'); ?>
                            </div><!-- row -->
                            <div class="row">
                                <?php echo $form->labelEx($model,'package_content'); ?>
                                <?php echo $form->textField($model, 'package_content', array('maxlength' => 256)); ?>
                                <?php echo $form->error($model,'package_content'); ?>
                            </div><!-- row -->
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <fieldset>
                        <legend><?= Yii::t('courier', 'Nadawca');?></legend>
                        <?php
                        $this->renderPartial('_addressDataPremium/_form',
                            array(
                                'model' => $model->senderAddressData,
                                'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_SENDER),
                                'form' => $form,
                                'noEmail' => true,
                                'i' => UserContactBook::TYPE_SENDER,
                                'type' => UserContactBook::TYPE_SENDER,
//                    'focusFirst' => true,
                            ));
                        ?>
                    </fieldset>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <fieldset>
                        <legend><?= Yii::t('courier', 'Odbiorca');?></legend>
                        <?php
                        $this->renderPartial('_addressDataPremium/_form',
                            array(
                                'model' => $model->receiverAddressData,
                                'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_RECEIVER),
                                'form' => $form,
                                'noEmail' => true,
                                'i' => UserContactBook::TYPE_RECEIVER,
                                'type' => UserContactBook::TYPE_RECEIVER,
//                    'focusFirst' => true,
                            ));
                        ?>
                    </fieldset>
                </div>
            </div>



        </div>

        <fieldset>
            <legend><?php echo Yii::t('courier','Kontynuuj');?></legend>

            <div>
                <div class="row">
                    <?php echo $form->labelEx($model->courierTypeExternal,'regulations'); ?>
                    <?php echo $form->checkBox($model->courierTypeExternal, 'regulations'); ?>
                    <?php echo $form->error($model->courierTypeExternal,'regulations'); ?>
                </div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($model->courierTypeExternal,'regulations_rodo'); ?>
                    <?php echo $form->checkBox($model->courierTypeExternal, 'regulations_rodo'); ?>
                    <?php echo $form->error($model->courierTypeExternal,'regulations_rodo'); ?>
                </div><!-- row -->
            </div>

            <div style="text-align: center;">
                <?php echo TbHtml::submitButton(Yii::t('app', 'Validate'), array('name' => 'validate', 'title' => 'CTRL + Q')); ?>
                <?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'save', 'title' => 'CTRL + S')); ?>
                <?php echo TbHtml::submitButton(Yii::t('app', 'Save & return to form'), array('name' => 'saveAndReturn', 'title' => 'CTRL + D')); ?>
                <?php $this->endWidget();
                ?>
            </div>
        </fieldset>
    </div>
<?php $this->widget('BackLink');?>