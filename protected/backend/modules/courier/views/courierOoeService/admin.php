<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-ooe-service-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'service_name',
        array(
            'name' => 'countryListsSTAT',
            'filter' => false,
            'header' => 'Receiver countries',
        ),
        'date_entered',
        'date_updated',
        array(
            'name'=>'stat',
            'value'=>'S_Status::stat($data->stat)',
            'filter'=>CHtml::listData(S_Status::listStats(),'id','name'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update} {view} {delete} {status} {status_a}',
            'htmlOptions'=> array('style'=>'width:80px'),
            'buttons'=>array(
                'status'=>array(
                    'visible' => '$data->stat==1',
                    'label'=>'deactivate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_active.png',
                    'url' => 'Yii::app()->createUrl("/courier/courierOoeService/stat",array("id" => $data->id))',
                ),
                'status_a'=>array(
                    'visible' => '$data->stat==0',
                    'label'=>'activate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_not_active.png',
                    'url' => 'Yii::app()->createUrl("/courier/courierOoeService/stat",array("id" => $data->id))',
                ),
            ),
        ),
    ),
)); ?>


    <h4>Get list of services for OOE user</h4>

<?php echo Chtml::form(); ?>

    <table class="table table-bordered" style="width: 500px; margin: 0 auto;">
        <thead>
        <tr>
            <td>Login OOE</td>
            <td>Password OOE</td>
            <td></td>
        </tr>
        </thead>
        <tr>
            <td><?php echo Chtml::textField('login');?></td>
            <td><?php echo Chtml::passwordField('password');?></td>
            <td><?php echo TbHtml::submitButton('Get', array('name' => 'get-ooe-services'));?></td>
        </tr>
    </table>

<?php echo CHtml::endForm();?>


<?php

if($services[0]) {

    if (S_Useful::sizeof($services[1])):
        ?>
<br/>
        <h5>List of services:</h5>

        <table class="table table-condensed table-striped table-hover" style="width: 500px; margin: 0 auto;">
            <thead>
            <tr>
                <td>Service ID</td>
                <td>Service name</td>
            </tr>
            </thead>
            <?php
            foreach ($services[1] AS $service):
                ?>
                <tr>
                    <td><?php echo $service['id'];?></td>
                    <td><?php echo $service['name'];?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </table>
    <?php
    endif;
} else if(isset($services[1])) {

    echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, $services[1], array('closeText' => false));

}
?>