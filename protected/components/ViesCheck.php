<?php

class ViesCheck
{
    public static function check($vat)
    {
        $resp = self::makeRequest($vat);

        if($resp)
            return $resp->valid;
        else
            return NULL;
    }

    public static function getAddressDataAttributes($vat)
    {

        $resp = self::makeRequest($vat);

        if($resp)
        {

            $address = trim($resp->address,"\n");
            $address = explode("\n", $address);
            $address1 = $address[0];

            $address = explode(' ', $address[1]);
            $zip_code = $address[0];
            $city = $address[1];


            $attributes = [
                'company' => $resp->name,
                'country' => $resp->countryCode,
                'country_id' => CountryList::getIdByCode($resp->countryCode),
                'zip_code' => $zip_code,
                'city' => $city,
                'address_line_1' => $address1,
                'nip' => $resp->countryCode.$resp->vatNumber,
                'valid' => $resp->valid ? 'YES' : 'NO',

            ];

            return $attributes;
        } else
            return NULL;

    }

    protected static function makeRequest($vat)
    {
        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 0,
            'exceptions' => false,
        );

        $client = new SoapClient('http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl', $mode);
//        $client->__setLocation(self::URL);

        $operation = 'checkVat';

        $vat = strtoupper($vat);
        $hasPrefix = preg_match('/^([a-z]){2}/i', $vat);

        if($hasPrefix) {
            $cc = substr($vat, 0, 2);
            $vat = substr($vat,2);
        }
        else
            $cc = 'PL';

        $vat = preg_replace("/[^0-9A-Z]/", "", $vat);

        $data = [
            'vatNumber' => $vat,
            'countryCode' => $cc,
        ];

        try {
            $resp = $client->__soapCall($operation, [$operation => $data]);

            MyDump::dump('viesCheck.txt', print_r($data,1).' : '.print_r($resp,1));
        } catch (SoapFault $E) {
            return null;
        }

        return $resp;
    }



}