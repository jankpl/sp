<?php

class CourierOoeAccountsController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierOoeAccounts'),
        ));
    }

    public function actionCreate() {

        $model = new CourierOoeAccounts;

        if (isset($_POST['CourierOoeAccounts'])) {
            $model->setAttributes($_POST['CourierOoeAccounts']);


            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'CourierOoeAccounts');


        if (isset($_POST['CourierOoeAccounts'])) {
            $model->setAttributes($_POST['CourierOoeAccounts']);


            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $this->loadModel($id, 'CourierOoeAccounts')->delete();
            }
            catch(Exception $ex)
            {
                throw new CHttpException(403,'Nie udało się usunąć tego wpisu!');
            }


            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierOoeAccounts('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierOoeAccounts']))
            $model->setAttributes($_GET['CourierOoeAccounts']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionStat($id)
    {
        /* @var $model CourierOoeAccounts */


        $model = CourierOoeAccounts::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}