<h2><?php echo Yii::t('courier','Przesyłki kurierskie');?></h2>

<?php echo CHtml::link(Yii::t('courier','Lista Twoich przesyłek'),array('/courier/courier/list', '#' => 'list'), array('class' => 'btn btn-primary btn-lg')); ?><br/><br/>

<?php echo CHtml::link(Yii::t('courier','Track&Trace'),array('/courier/courier/tt'), array('class' => 'btn')); ?>

<?php echo CHtml::link(Yii::t('courier','Potwierdzenia nadania z dzisiaj (:data)', [':data' => date('Y-m-d')]),array('/courier/courier/ackCardToday'), array('class' => 'btn', 'data-preload' => true)); ?>

<?php echo CHtml::link(Yii::t('courier','Masowe zarządanie przesyłkami'),array('/courier/courierScanner'), array('class' => 'btn')); ?>

<?php echo CHtml::link(Yii::t('courier','Zapytaj o ofertę specjalną'),array('/site/contact'), array('class' => 'btn')); ?>

<hr/>


<h3><?php Yii::t('courier', 'Nadaj lub eksportuj przesyłki');?></h3>

<?php
if(!Yii::app()->user->getModel()->isPremium)
{
    echo $this->renderPartial('index_tabs/_standard', array(), true);
} else {
    ?>

    <div class="row">
        <div class="col-xs-3">
            <ul class="nav nav-tabs tabs-left">
                <li class="active"><a href="#std" data-toggle="tab"><?= Yii::t('courier','Standard'); ?></a></li>
                <?php if(Yii::app()->user->getModel()->isPremium && Yii::app()->user->model->getCourierSimpleAvailable()):?><li><a href="#smpl" data-toggle="tab"><?= Yii::t('courier','Simple'); ?></a></li><?php endif; ?>
                <?php if(Yii::app()->user->getModel()->isPremium && CourierTypeOoe::isOooAvailableForUser() && Yii::app()->user->model->getCourierOoeAvailable()):?><li><a href="#ooe" data-toggle="tab"><?= Yii::t('courier','OOE'); ?></a></li><?php endif; ?>
                <?php if(Yii::app()->user->getModel()->isPremium && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH):?><li><a href="#pr" data-toggle="tab"><?= Yii::t('courier','Premium'); ?></a></li><?php endif; ?>
                <?php if(Yii::app()->user->getModel()->isPremium && Yii::app()->user->model->getCourierDomesticAvailable()):?><li><a href="#dom" data-toggle="tab"><?=Yii::t('courier','Domestic'); ?></a></li><?php endif; ?>
            </ul>
        </div>

        <div class="col-xs-9">
            <div class="tab-content">
                <div class="tab-pane active" id="std"><?= $this->renderPartial('index_tabs/_standard', array(), true);?></div>
                <?php if(Yii::app()->user->getModel()->isPremium && Yii::app()->user->model->getCourierSimpleAvailable()):?><div class="tab-pane" id="smpl"><?= $this->renderPartial('index_tabs/_simple', array(), true);?></div><?php endif; ?>
                <?php if(Yii::app()->user->getModel()->isPremium && CourierTypeOoe::isOooAvailableForUser() && Yii::app()->user->model->getCourierOoeAvailable()):?><div class="tab-pane" id="ooe"><?= $this->renderPartial('index_tabs/_ooe', array(), true);?></div><?php endif; ?>
                <?php if(Yii::app()->user->getModel()->isPremium):?><div class="tab-pane" id="pr"><?= $this->renderPartial('index_tabs/_premium', array(), true);?></div><?php endif; ?>
                <?php if(Yii::app()->user->getModel()->isPremium && Yii::app()->user->model->getCourierDomesticAvailable()):?> <div class="tab-pane" id="dom"><?= $this->renderPartial('index_tabs/_domestic', array(), true);?></div><?php endif; ?>
            </div>
        </div>
    </div>
    <?php

}
?>
<?php
//    if(!Yii::app()->user->isGuest && Yii::app()->user->isPremium)

//        echo CHtml::link(Yii::t('courier','Szybka przesyłka'),array('courier/fastCreate'), array('class' => 'btn'));
?>

<hr/>