(function() {
    var $messageInfo;
    var $messageSuccess;
    var $messageDanger;
    var $itemsIds;

    var $actionButtons;
    var $tooMuchWarning;

    var $totalNo;
    var $successNo;
    var $failNo;
    var $successList;
    var $failList;
    var $summaryBlock;

    var myForm;

    var alreadyChecking = false;

    $(document).ready(function(){

        $messageInfo = $('#message-info');
        $messageSuccess = $('#message-success');
        $messageDanger = $('#message-danger');
        $itemsIds = $('#items-ids');

        $actionButtons = $('#action-buttons');
        $tooMuchWarning = $('#too-much-warning');

        $totalNo = $('#total-no');
        $successNo = $('#success-no');
        $failNo = $('#fail-no');
        $successList = $('#success-list');
        $failList = $('#fail-list');
        $summaryBlock = $('#summary-block');

        $itemsIds.on('change keyup', countLines);

        $itemsIds.on('focusout', filterLines);


        myForm = document.getElementById('user-scanner-form');

        myForm.onsubmit = onFormSubmit;
    });

    function onFormSubmit()
    {

        if(countLines() == 0)
        {
            alert(yii.text.noItems);
            return false;
        }

        Overlay.show();

        $summaryBlock.hide();
        $messageInfo.hide();
        $messageSuccess.hide();
        $messageDanger.hide();

        setTimeout(function () {
            checkStatus(0);
        }, 500);
    }

    function countLines()
    {
        var text = $itemsIds.val();

        var count = 0;
        if(text != '') {
            var lines = text.split(/\r|\r\n|\n/);
            count = lines.length;
        }

        if(count > yii.no.max)
        {
            $actionButtons.hide();
            $tooMuchWarning.show();
        } else {
            $actionButtons.show();
            $tooMuchWarning.hide();
        }

        $('#numer-of-rows').html(count);

        return count;
    }

    function filterLines()
    {
        var text = $(this).val();

        text = text.replace(/(?:(?:\r\n|\r|\n)\s*){2}/gm, "\r\n");
        text = text.replace(/(?:(?:\r\n|\r|\n)\s*)$/gm, "");
        text = text.replace(/^(?:(?:\r\n|\r|\n)\s*)/gm, "");
        $(this).val(text).trigger('change');
    }


    var Overlay = (function() {
        "use strict";

        var that = {};
        var $overlay = $("<div>");
        var $textBottom = $("<div>");
        var $textTop = $("<div>");
        var $loader = $("<div>");

        that.isShow = function(){
            return $("#overlay").is(':visible');
        };

        that.show = function() {
            var $form = $("#user-scanner-form");

            $overlay.addClass('overlay');
            $loader.addClass('loader');

            $overlay.append($loader);

            $textTop.addClass('loader-text-top');
            $textTop.html(yii.loader.topText);


            $textBottom.addClass('loader-text-bottom');
            $textBottom.html(yii.loader.bottomText);


            $overlay.append($textTop);
            $overlay.append($textBottom);

            $form.css('position','relative');
            $form.append($overlay);

            $textTop.hide().delay(1000).fadeIn();
            $textBottom.hide().delay(3000).fadeIn();
        };

        that.hide = function() {
            $overlay.remove();
        };

        return that;
    }());

    function checkStatus(i)
    {
        if(!i && alreadyChecking) {
            console.log('already checking...');
            return true;
        }

        alreadyChecking = true;

        console.log('check...');
        $.ajax({
            url: yii.urls.statusCheck + '&i=' + i,
            type: 'POST',
            dataType: 'json',
            cache: false,
            success: function(data) {

                console.log(data);

                var state = data.state;
                var message = data.message;
                var successNo = data.doneNo;
                var successList = data.doneList;
                var failNo = data.notDoneNo;
                var failList = data.notDoneList;
                var totalNo = data.totalNo;

                if(message != '')
                    $messageInfo.html(message).fadeIn();

                if(state == yii.stat.success || state == yii.stat.fail) {

                    failListText = '';
                    $.each(failList, function(i,v){
                        failListText += v + '\n';
                    });


                    successListText = '';
                    $.each(successList, function(i,v){
                        successListText += v + '\n';
                    });

                    $totalNo.html(totalNo);
                    $failNo.html(failNo);
                    $successNo.html(successNo);
                    $failList.val(failListText);
                    $successList.val(successListText);
                    $summaryBlock.fadeIn();

                    Overlay.hide();
                    alreadyChecking = false;

                }
                else
                {
                    console.log('recheck...');
                    setTimeout(function () {
                        i++;
                        checkStatus(i);
                    }, 500);
                }
            },
            error: function(e){
                console.log(e);
                Overlay.hide();
                $messageInfo.html(yii.loader.error).fadeIn();
            }
        });

    }

})();
