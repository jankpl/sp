<?php

class Joy
{

    const DEV = GLOBAL_CONFIG::JOY_TEST;
    const ID = GLOBAL_CONFIG::JOY_ID;
    const SECRET = GLOBAL_CONFIG::JOY_SECRET;

    /**
     * @param $track_id string Track ID for this operator is our local package ID
     * @return array|bool
     */
    public static function getTracking($track_id)
    {

        $jc = new JoyClass(self::ID, self::SECRET, self::DEV);

        try
        {
            $resp = $jc->getPackage($track_id);

            $statuses = [];

            $status = $resp->status;


            $parcelService = $resp->parcelService;
            $externalId = $resp->_embedded->trackingNumber[0];
            $return = [];

            $return['external_id'] = $externalId != '' ? $externalId.' ('.$parcelService.')' : false;

            if($status != '')
            {
                $temp = array(
                    'name' => $status,
                    'date' => '',
                    'location' => '',
                );
            }

            $trackingEvents = false;
            if($resp->_embedded->trackingEvents)
            {
                $trackingEvents = [];
                foreach($resp->_embedded->trackingEvents AS $item)
                {

                    $name = $item->message;
                    $location = NULL;
                    if (preg_match_all('/Prijal ([\w])*/i', $name, $matches)) {
                        $location = '('.$matches[0][0].')';
                        $name = str_replace($matches[0][0], '', $name);
                        $name = preg_replace('!\s+!', ' ', $name);
                    } elseif (preg_match_all('/Předaní zásilky ([\w]*)/i', $name, $matches)) {
                        $location = $matches[1][0];
                        $name = str_replace($location, '', $name);
                        $name = preg_replace('!\s+!', ' ', $name);
                    } else if (preg_match_all('/\(Huid: (\w)+\)/i', $name, $matches)) {
                        $location = $matches[0][0];
                        $name = str_replace($location, '', $name);
                        $name = preg_replace('!\s+!', ' ', $name);
                    }

                    $trackingEvents[] = [
                        'date' => $item->date,
                        'name' => $name,
                        'location' => $location,
                    ];
                }
            }


            if(is_array($trackingEvents))
            {
                if($status == 'sent')
                {
                    $statuses = array_merge([$temp], $trackingEvents);
                }
                else if($status == 'delivered')
                {
                    $statuses = array_merge($trackingEvents, [$temp]);
                }
            } else {
                $statuses[] = $temp;
            }

            if(strtoupper($resp->country) == 'AT')
            {
//                $tmStatuses = TrackingmoreClient::getTt($resp->_embedded->trackingNumber[0], 'dpd');
                $tmStatuses = Client17Track::getTt($resp->_embedded->trackingNumber[0], false, false, false, 'JOY');
                if(is_array($tmStatuses))
                    $statuses = array_merge($statuses, $tmStatuses);
            }
//            else if(strtoupper($resp->country) == 'HR')
//            {
//                $tmStatuses = TrackingmoreClient::getTt($resp->_embedded->trackingNumber[0], 'dpd');
//                $statuses = array_merge($statuses, $tmStatuses);
//            }

            $return['tt'] = $statuses;

            return $return;

        }
        catch(JoiApiException $ex)
        {
//            Yii::log('JoiApiException:'.$ex->getMessage().' ; TRACK ; '.print_r($track_id,1), CLogger::LEVEL_ERROR);
            return false;
        }

    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $returnError = true)
    {

        if ($to->name == $to->company)
            $name = $to->name;
        else
            $name = $to->getUsefulName();

        $cod = 0;
        if ($courierInternal->courier->cod_value > 0)
            $cod = $courierInternal->courier->cod_value;

        $jc = new JoyClass(self::ID, self::SECRET, self::DEV);

        $data = [
            'id' =>  $courierInternal->courier->local_id,
            'name' => $name,
            'street' => trim($to->address_line_1 . ' ' . $to->address_line_2),
            'city' => $to->city,
            'psc' => preg_replace("/[^0-9a-z]/i", "", $to->zip_code),
            'country' => strtolower($to->country0->code),
            'email' => $to->fetchEmailAddress(true),
            'phone' => str_pad($to->tel, 6, "0", STR_PAD_LEFT),
            'variableSymbol' =>  mb_substr($courierInternal->courier->local_id, -10),
            'cod' => $cod,
            'note' =>  'SP#' . $courierInternal->courier->local_id,
        ];

        try {
            $resp = $jc->createPackage($data);

            $resp2 = $jc->getPackage($courierInternal->courier->local_id);
            $barcode = $resp2->barcode;

            $array = [];

            $id = $resp->_links->self->href;
            $id = explode('=', $id);
            $id = $id[S_Useful::sizeof($id) - 1];

            if ($id != '') {

                // @ 07.03.2017 - always use sender address
                $label = KaabLabel::generate($courierInternal->courier, $courierInternal->family_member_number, $courierInternal->courier->senderAddressData, $to, false, false, false, $courierInternal->courier->package_content, 'P', $barcode);

                $return = array(
                    'status' => true,
                    'track_id' => $id,
                    'label' => $label,
                );

                array_push($array, $return);
            }


            return $array;
        } catch (JoiApyException $ex) {
            Yii::log('JoiApiException:' . $ex->getMessage() . ' ; ORDER FOR COURIER INTERNAL ; ' . print_r($courierInternal->courier->id, 1), CLogger::LEVEL_ERROR);

            if ($returnError) {
                $return = array(0 => array(
                    'error' => $ex->getMessage()));
                return $return;
            } else {
                return false;
            }
        }
    }

    public static function orderForCourierU(CourierTypeU $courierU, $returnError = true)
    {

        $to = $courierU->courier->receiverAddressData;

        if($to->name == $to->company)
            $name = $to->name;
        else
            $name = $to->getUsefulName();

        $cod = 0;
        if($courierU->courier->cod_value > 0)
            $cod = $courierU->courier->cod_value;

        $jc = new JoyClass(self::ID, self::SECRET, self::DEV);


        $data = [
            'id' =>  $courierU->courier->local_id,
            'name' => $name,
            'street' => trim($to->address_line_1 . ' ' . $to->address_line_2),
            'city' => $to->city,
            'psc' => preg_replace("/[^0-9a-z]/i", "", $to->zip_code),
            'country' => strtolower($to->country0->code),
            'email' => $to->fetchEmailAddress(true),
            'phone' => str_pad($to->tel, 6, "0", STR_PAD_LEFT),
            'variableSymbol' =>  mb_substr($courierU->courier->local_id, -10),
            'cod' => $cod,
            'note' =>  'SP#' . $courierU->courier->local_id,
        ];

        try {
            $resp = $jc->createPackage($data);

            $resp2 = $jc->getPackage($courierU->courier->local_id);
            $barcode = $resp2->barcode;

            $array = [];

            $id = $resp->_links->self->href;
            $id = explode('=', $id);
            $id = $id[S_Useful::sizeof($id)-1];

            if($id != '') {

                // @ 07.03.2017 - always use sender address
                $label = KaabLabel::generate($courierU->courier, $courierU->family_member_number, $courierU->courier->senderAddressData, $to, false, false, false, $courierU->courier->package_content, 'P', $barcode);

                $return = array(
                    'status' => true,
                    'track_id' => $id,
                    'label' => $label,
                );

                array_push($array, $return);
            }



            return $array;
        }
        catch(JoiApyException $ex)
        {
            Yii::log('JoiApiException:'.$ex->getMessage().' ; ORDER FOR COURIER U ; '.print_r($courierU->courier->id,1), CLogger::LEVEL_ERROR);


            if($returnError)
            {
                $return = array(0 => array(
                    'error' => $ex->getMessage()));
                return $return;
            } else {
                return false;
            }
        }

    }


}