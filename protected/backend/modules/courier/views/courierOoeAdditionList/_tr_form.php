<div class="form">
    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']title'); ?>
        <?php echo $form->textField($model, '['.$i.']title', array('maxlength' => 256)); ?>
        <?php echo $form->error($model,'['.$i.']title'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']description'); ?>
        <?php echo $form->textField($model, '['.$i.']description', array('maxlength' => 512)); ?>
        <?php echo $form->error($model,'['.$i.']description'); ?>
    </div><!-- row -->
</div><!-- form -->