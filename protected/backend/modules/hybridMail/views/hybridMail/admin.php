<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('hybrid-mail-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'hybrid-mail-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
    'selectableRows'=>2,
	'columns' => array(
        array(
            'name'=>'local_id',
            'header'=>'#local ID',
            'type'=>'raw',
            'value'=>'CHtml::link($data->local_id,array("/hybridMail/hybridMail/view/", "id" => $data->id))',
            'headerHtmlOptions' => array('style' => 'width: 90px;'),
        ),
        array(
            'name' => 'date_entered',
            'value' => 'substr($data->date_entered,0,-3)',
            'headerHtmlOptions' => array('style' => 'width: 100px;'),
        ),
        array(
            'name' => 'date_updated',
            'value' => 'substr($data->date_updated,0,-3)',
            'headerHtmlOptions' => array('style' => 'width: 100px;'),
        ),
        array(
            'header'=>'Pages',
            'name' =>  'pages',
        ),
        array(
            'header'=>'Receivers',
            'value'=>'(S_Useful::sizeof($data->hybridMailReceivers))',
            'name' =>  '__receivers_number',
            'filter' => false,
        ),
        array(
            'name'=>'sender_country_id',
            'header'=>'Sender country',
            'value'=>'GxHtml::valueEx($data->senderCountry)',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
            'headerHtmlOptions' => array('style' => 'width: 90px;')
        ),

        array(
            'name'=>'order_id',
            'type'=>'raw',
            'value'=>'$data->order_id !== null? CHtml::link($data->order_id,array("/order/view/", "id" => $data->order_id)) : "-"',
        ),
        array(
            'name'=>'__user_login',
            'type'=>'raw',
            'value'=>'$data->user !== null? CHtml::link($data->user->login,array("/user/view/", "id" => $data->user_id)) : "-"',
        ),
        array(
            'name' => 'daysFromStatusChange',
            'header' => 'Status age (days)',
            'filter' => false,
        ),
        array(
            'name'=>'__stat',
            'value'=>'$data->stat0->name',
            'filter'=>GxHtml::listDataEx(HybridMailStat::model()->findAllAttributes(null, true)),
            'headerHtmlOptions' => array('style' => 'width: 100px;'),
        ),
		/*
		'sender_name',
		'sender_country',
		'sender_zip_code',
		'sender_city',
		'sender_address_line_1',
		'sender_address_line_2',
		'sender_tel',
		'text',
		array(
				'name'=>'order_id',
				'value'=>'GxHtml::valueEx($data->order)',
				'filter'=>GxHtml::listDataEx(Order::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'stat',
				'value'=>'GxHtml::valueEx($data->stat0)',
				'filter'=>GxHtml::listDataEx(HybridMailStat::model()->findAllAttributes(null, true)),
				),
		'local_id',
		'file_hash',
		'hash',
		*/
		array(
            'headerHtmlOptions' => array('style' => 'width: 50px;'),
			'class' => 'CButtonColumn',
            'template'=>'{view}{down_file}{down_addr}',
            'buttons'=>array
            (
                'down_file' => array
                (
                    'label'=> TbHtml::icon(TbHtml::ICON_DOWNLOAD_ALT, array('title' => 'Download file')),
                    'url'=>'Yii::app()->createUrl("/hybridMail/hybridMail/getFile", array("urlData" => $data->hash))',
                ),
                'down_addr' => array
                (
                    'label'=>TbHtml::icon(TbHtml::ICON_ENVELOPE, array('title' => 'Download addresses')),
                    'url'=>'Yii::app()->createUrl("/hybridMail/hybridMail/getAddressLabels", array("urlData" => $data->hash))',
                ),
            ),
		),
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'selected_rows'
        ),
	),
)); ?>

<h3>Change statuses of selected items</h3>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div style="text-align: center;">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'mass-assign-stat',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('/hybridMail/hybridMail/assingStatusByGridView'),
    ));
    ?>

    <?php echo CHtml::dropDownList('hybrid_mail_stat_id','', CHtml::listData(HybridMailStat::model()->findAll(),'id','name')); ?>

    <?php
    echo Chtml::submitButton('Change', array(
        'onClick' => 'js:
    $("#hybrid-mail-ids").val($.fn.yiiGridView.getChecked("hybrid-mail-grid","selected_rows").toString());
    ;',
        'name' => 'HybridMailIds'));
    ?>

    <?php echo CHtml::hiddenField('HybridMail[ids]','', array('id' => 'hybrid-mail-ids')); ?>
    <?php
    $this->endWidget();
    ?>
</div>