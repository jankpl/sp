<?php

class Courier_CourierTypeDomestic extends Courier
{

    const MAX_WEIGHT = 50;
    const MAX_WEIGHT_PALETTE = 1000;
    const MAX_DIMENSIONS_PALETTE = 200;

    protected $_isWeightValidated = false; // used to determine whether default weight validation should be applied
    protected $_isDimensionsValidated = false; // used to determine whether default dimension validation should be applied

    public function beforeValidate()
    {
        if(is_string($this->cod_value))
            $this->cod_value = (double) ($this->cod_value);


        if(in_array($this->courierTypeDomestic->domestic_operator_id, [CourierDomesticOperator::OPERATOR_DHL_PALETA, CourierDomesticOperator::OPERATOR_DHL_F_PALETA, CourierDomesticOperator::OPERATOR_DHL_F_DLUZYCE])) {
            $this->_disableCalculationDimensionalWeight = true;
            $this->_disableAdditionalWeightAndSizeCheck = true;
        }


        if(in_array($this->courierTypeDomestic->domestic_operator_id, [CourierDomesticOperator::OPERATOR_DHL, CourierDomesticOperator::OPERATOR_DHL_F]))
        {
            $this->_calculationDimensionalWeightDHLMode = true;
        }


        if(parent::beforeValidate())
        {
            $dimensions = [
                $this->package_size_l,
                $this->package_size_d,
                $this->package_size_w,
            ];

            // biggest dimension is length and is first
            rsort($dimensions);

            if(in_array($this->courierTypeDomestic->domestic_operator_id, [CourierDomesticOperator::OPERATOR_DHL_PALETA, CourierDomesticOperator::OPERATOR_DHL_F_PALETA]))
            {
                if($this->package_weight > self::MAX_WEIGHT_PALETTE)
                    $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga palety to: {waga} kg!', ['{waga}' => self::MAX_WEIGHT_PALETTE]));

                $this->_isWeightValidated = true;

                if($this->package_size_l > self::MAX_DIMENSIONS_PALETTE)
                    $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => self::MAX_DIMENSIONS_PALETTE, '{b}' => self::MAX_DIMENSIONS_PALETTE, '{c}' => self::MAX_DIMENSIONS_PALETTE]));

                if($this->package_size_d > self::MAX_DIMENSIONS_PALETTE)
                    $this->addError('package_size_d', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => self::MAX_DIMENSIONS_PALETTE, '{b}' => self::MAX_DIMENSIONS_PALETTE, '{c}' => self::MAX_DIMENSIONS_PALETTE]));

                if($this->package_size_w > self::MAX_DIMENSIONS_PALETTE)
                    $this->addError('package_size_w', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => self::MAX_DIMENSIONS_PALETTE, '{b}' => self::MAX_DIMENSIONS_PALETTE, '{c}' => self::MAX_DIMENSIONS_PALETTE]));

                $this->_isDimensionsValidated = true;

            }
            else if(in_array($this->courierTypeDomestic->domestic_operator_id, [CourierDomesticOperator::OPERATOR_DHL_F_DLUZYCE]))
            {
                if($dimensions[0] > 400 OR
                    $dimensions[1] > 400 OR
                    $dimensions[2] > 400)
                    $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => 400, '{b}' => 400, '{c}' => 400]));

                if($dimensions[0] + $dimensions[1] + $dimensions[2] > 600) {
                    $this->addError('package_size_l', Yii::t('courier', 'Maksymalne suma wymiarów to: {n} cm!', ['{n}' => 600]));
                    $this->addError('package_size_d', Yii::t('courier', 'Maksymalne suma wymiarów to: {n} cm!', ['{n}' => 600]));
                    $this->addError('package_size_w', Yii::t('courier', 'Maksymalne suma wymiarów to: {n} cm!', ['{n}' => 600]));

                }

                $this->_isDimensionsValidated = true;
            }
            else if(in_array($this->courierTypeDomestic->domestic_operator_id, [CourierDomesticOperator::OPERATOR_DHL, CourierDomesticOperator::OPERATOR_DHL_F]))
            {
                if($dimensions[0] > 140 OR
                    $dimensions[1] > 60 OR
                    $dimensions[2] > 60)
                    $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => 140, '{b}' => 60, '{c}' => 60]));

                $this->_isDimensionsValidated = true;
            }
            else
            {
                if ($this->courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_GLS) {

                    if (!GlsClient::checkPackageDimensions($this->package_size_l, $this->package_size_d, $this->package_size_w)) {
                        $text = GlsClient::packageDimensionsInfo();
                        $this->addError('package_size_l', $text);

                        $this->_isDimensionsValidated = true;
                    }

                    if (!GlsClient::checkPackageWeight($this->package_weight, true)) {
                        $text = GlsClient::packageWeightInfo(true);
                        $this->addError('package_weight', $text);
                    }

                    $this->_isWeightValidated = true;

                } else {
                    if ($this->package_weight > self::MAX_WEIGHT)
                        $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => self::MAX_WEIGHT]));

                    $this->_isWeightValidated = true;
                }
            }

            if($this->packages_number > 1 && !$this->courierTypeDomestic->isValidMultipackage())
            {
                $this->addError('packages_number', Yii::t('courier', 'Ten operator nie obsługuje paczek wielokrotnych.'));
            }



            // RUCH:
            if($this->courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_RUCH)
            {
                if($this->package_weight > PaczkaWRuchu::MAX_WEIGHT)
                    $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => PaczkaWRuchu::MAX_WEIGHT]));
                $this->_isWeightValidated = true;

                if($dimensions[0] > PaczkaWRuchu::MAX_DIMENSIONS_1 OR
                    $dimensions[1] > PaczkaWRuchu::MAX_DIMENSIONS_2 OR
                    $dimensions[2] > PaczkaWRuchu::MAX_DIMENSIONS_3)
                    $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => PaczkaWRuchu::MAX_DIMENSIONS_1, '{b}' => PaczkaWRuchu::MAX_DIMENSIONS_2, '{c}' => PaczkaWRuchu::MAX_DIMENSIONS_3]));

                $this->_isDimensionsValidated = true;
            }

            // DEFAULT:


            if(!$this->_isWeightValidated)
            {
                if($this->package_weight > S_PackageMaxDimensions::getPackageMaxDimensions()['weight'])
                    $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => S_PackageMaxDimensions::getPackageMaxDimensions()['weight']]));

            }


            if(!$this->_isDimensionsValidated)
            {

                if($this->package_size_l > S_PackageMaxDimensions::getPackageMaxDimensions()['l'])
                    $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => S_PackageMaxDimensions::getPackageMaxDimensions()['l'], '{b}' => S_PackageMaxDimensions::getPackageMaxDimensions()['d'], '{c}' => S_PackageMaxDimensions::getPackageMaxDimensions()['w']]));

                if($this->package_size_d > S_PackageMaxDimensions::getPackageMaxDimensions()['d'])
                    $this->addError('package_size_d', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => S_PackageMaxDimensions::getPackageMaxDimensions()['l'], '{b}' => S_PackageMaxDimensions::getPackageMaxDimensions()['d'], '{c}' => S_PackageMaxDimensions::getPackageMaxDimensions()['w']]));

                if($this->package_size_w > S_PackageMaxDimensions::getPackageMaxDimensions()['w'])
                    $this->addError('package_size_w', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => S_PackageMaxDimensions::getPackageMaxDimensions()['l'], '{b}' => S_PackageMaxDimensions::getPackageMaxDimensions()['d'], '{c}' => S_PackageMaxDimensions::getPackageMaxDimensions()['w']]));

            }


            if($this->cod_value)
            {
                if($this->cod_currency == '')
                {
                    $this->cod_currency = CourierCodAvailability::CURR_PLN;
                }
                else if(!CourierCodAvailability::compareCurrences($this->cod_currency, CourierCodAvailability::CURR_PLN))
                {
                    $this->addError('cod_currency', Yii::t('courier', 'Prawidłowa waluta dla tego kraju to:').' '.CourierCodAvailability::CURR_PLN);
                }

//                // validate cod value

                if(!$this->hasErrors('cod_currency'))
                {
                    $max_cod_value = CourierCodAvailability::getCurrencyMaxValueById($this->cod_currency);
                    if($this->cod_value > $max_cod_value)
                        $this->addError('cod_value', Yii::t('courier', 'Maksymalna wartość COD dla {currency} to: {max_value}', ['{currency}' => $this->cod_currency, '{max_value}' => $max_cod_value]));
                }
            }



            return true;
        }
        return false;
    }


    public function afterValidate()
    {
//        if($this->courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL_PALETA)
//        {
//            $this->clearErrors('package_size_l');
//            $this->clearErrors('package_size_w');
//            $this->clearErrors('package_size_d');
//            $this->package_size_l = 0;
//            $this->package_size_w = 0;
//            $this->package_size_d = 0;
//        }
        parent::afterValidate();
    }

    public function rules() {


        $arrayValidate = array(

            array('ref', 'application.validators.courierRefValidator'),

            array('cod_value', 'numerical', 'min' => 0),
            array('cod_value', 'match', 'pattern'=> '/^[0-9]+(\.[0-9]{1,2})?$/'),

            array('cod_currency', 'in', 'range' => array_keys(CourierCodAvailability::getCurrencyList()), 'message' => Yii::t('courier', 'Ta waluta nie jest obsługiwana')),

            array('package_value', 'numerical', 'min'=>1, 'max' => 999999),
            array('package_value', 'match', 'pattern'=> '/^[0-9]+(\.[0-9]{1,2})?$/'),

            array('user_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?NULL:Yii::app()->user->id),
            array('admin_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?Yii::app()->user->id:NULL),

            array('packages_number', 'default', 'setOnEmpty' => true, 'value' => 1),
            array('packages_number', 'numerical', 'integerOnly'=>true, 'min' => 1),



            array('package_weight, package_size_l, package_size_w, package_size_d, package_value', 'required'),

            array('courier_type_external_id, courier_type_internal_id, sender_address_data_id, receiver_address_data_id, courier_stat_id', 'numerical', 'integerOnly'=>true),


//            array('package_size_l, package_size_w, package_size_d', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999),

            array('package_size_l', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999),
            array('package_size_w', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999),
            array('package_size_d', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999),
            array('package_weight', 'numerical', 'min' => 0.01),

            array('ref', 'length', 'max'=>64),
//            array('package_value', 'length', 'max'=>32),
            array('package_content', 'length', 'max'=>256),

            array('source', 'numerical', 'integerOnly'=>true),

            array('date_updated', 'safe'),
            array('courier_type_external_id, courier_type_internal_id, date_updated, sender_address_data_id, package_value, package_content, package_weight, package_size_l, package_size_w, package_size_d', 'default', 'setOnEmpty' => true, 'value' => null),

            array(implode(',', array_keys(self::getAttributes())),'safe', 'on' => 'clone'),
        );

        $arrayBasic = array(
            array('courier_type_external_id, courier_type_internal_id, date_entered, date_updated, local_id, hash, sender_address_data_id, receiver_address_data_id, courier_stat_id, package_weight, package_size_l, package_size_w, package_size_d, packages_number, package_value, package_content, cod, ref, user_id, admin_id, _first_status_date', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('courier_type_external_id, courier_type_internal_id, date_updated, sender_address_data_id, receiver_address_data_id, package_weight, package_size_l, package_size_w, package_size_d, package_value, package_content, cod, ref, user_id, admin_id', 'default', 'setOnEmpty' => true, 'value' => null),

            array('courier_stat_id', 'default', 'setOnEmpty' => true, 'value' => CourierStat::NEW_ORDER),
        );


        return array_merge($arrayBasic, $arrayValidate);
    }


}
