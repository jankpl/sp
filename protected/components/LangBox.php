<?php
class LangBox extends CWidget
{
    public $wrapper;

    public function run()
    {
//        $model = Language::model()->cache(1000)->findAll(array(
//        $model = Language::model()->findAll(array(
//            'condition'=>'stat = 1',
//            'order'=>'id DESC',
//        ));

        $currentLang = Language::autoLanguageCodeReturn(Yii::app()->language);

        $this->render('application.components.views.langBox', array(
            'currentLang' => $currentLang,
//            'model' => $model,
            'wrapper' => $this->wrapper));
    }
}
?>