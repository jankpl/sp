<?php

class DeutchePostClient extends GlobalOperatorWithPostal
{
    const URL = GLOBAL_CONFIG::DEUTCHE_POST_URL;
    const CLIENT_ID = GLOBAL_CONFIG::DEUTCHE_POST_CLIENT_ID;
    const CLIENT_SECRET = GLOBAL_CONFIG::DEUTCHE_POST_SECRET;
    const CLIENT_EKP = '5254619413';

    const PRODUCT_PACKET = 'GMP';
    const PRODUCT_PACKET_TRACKED = 'GPT';
    const PRODUCT_LETTER = 'GMM';
    const PRODUCT_PACKET_PLUS = 'GPP';

    const SERVICE_STD = 'STANDARD';
    const SERVICE_PRIO = 'PRIORITY';
    const SERVICE_REG = 'REGISTERED';

    public $_service = false;
    public $_product = false;

    public $label_annotation_text = false;

    public $package_value;

    public $receiver_name;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_mail;

    public $sender_country;
    public $sender_country_id;

    public $package_weight;

    public $package_content;
    public $ref;


    public static function test()
    {
        $model = new self;
        $model->receiver_country = 'PL';
        $model->receiver_name = 'Test Company';
        $model->receiver_address1 = 'Piekna 2';
        $model->receiver_city = 'Rybnik';
        $model->receiver_zip_code = '44-200';
        $model->package_weight = 1;

        $model->sender_country = 'PL';
        $model->sender_name = 'Test Company';
        $model->sender_address1 = 'Testowa 10';
        $model->sender_city = 'Warsaw';
        $model->sender_zip_code = '00-001';

        $model->package_content = 'Test 123';
        $model->local_id = '8191941991981';
        $model->ref = '8191941991981';

        $model->setService(GLOBAL_BROKERS::OPERATOR_UNIQID_DEUTCHEPOST_PACKET_PLUS);

        var_dump($model->createShipment(true));
    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    protected function setService($operator_uniq_id)
    {
        if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DEUTCHEPOST_PACKET_PLUS)
        {
            $this->_service = self::SERVICE_PRIO;
            $this->_product = self::PRODUCT_PACKET_PLUS;
        }
        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DEUTCHEPOST_PACKET_TRACKED)
        {
            $this->_service = self::SERVICE_PRIO;
            $this->_product = self::PRODUCT_PACKET_TRACKED;
        }
        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DEUTCHEPOST_PACKET)
        {
            $this->_service = self::SERVICE_PRIO;
            $this->_product = self::PRODUCT_PACKET;
        }
//        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DEUTCHEPOST_PACKET_PRIO)
//        {
//            $this->_service = self::SERVICE_PRIO;
//            $this->_product = self::PRODUCT_PACKET;
//        }
        else if($operator_uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DEUTCHEPOST_LETTER)
        {
            $this->_service = self::SERVICE_STD;
            $this->_product = self::PRODUCT_LETTER;
        }

    }


    protected static function addressLinesConverter($addressLine1, $addressLine2, $getLineNo)
    {

        if(mb_strlen($addressLine1) <= 30 && mb_strlen($addressLine2) <= 30)
        {
            if($getLineNo == 0)
                return $addressLine1;
            else if($getLineNo == 1)
                return $addressLine2;
            else if($getLineNo == 2)
                return '';

        } else {

            $address = trim($addressLine1) . ' ' . trim($addressLine2);
            $address = trim($address);

            $newAddress = [];
            $newAddress[0] = mb_substr($address, 0, 30);
            $newAddress[1] = mb_substr($address, 30, 30);
            $newAddress[2] = mb_substr($address, 60, 30);

            return trim($newAddress[$getLineNo]);

        }
    }

    public static function closeShipment($order_id)
    {
        $model = new self;

        $data = new stdClass();
        $data->contactName = 'ROMAD';
        $data->jobReference = '';
        $data->pickupType = 'CUSTOMER_DROP_OFF';
        $data->awbCopyCount = '1';

        $resp = $model->_call('dpi/shipping/v1/orders/'.$order_id.'/finalization', $data);

        return $resp;

    }

    public function createShipment($returnError = false)
    {

//        if ($returnError == true) {
//            return GlobalOperatorResponse::createErrorResponse('Not active!');
//        } else {
//            return false;
//        }


        $item = new stdClass();
        $item->id = 0;
        $item->product = $this->_product;
        $item->serviceLevel = $this->_service;
        $item->custRef = substr($this->ref,0,20);
        $item->recipient = mb_substr($this->receiver_name, 0, 30);
        $item->recipientPhone = $this->receiver_tel;
        $item->recipientFax = '';
        $item->recipientEmail = $this->receiver_mail;
        $item->addressLine1 = self::addressLinesConverter($this->receiver_address1, $this->receiver_address2, 0);
        $item->addressLine2 = self::addressLinesConverter($this->receiver_address1, $this->receiver_address2, 1);
        $item->addressLine3 = self::addressLinesConverter($this->receiver_address1, $this->receiver_address2, 2);
        $item->city = $this->receiver_city;
        $item->state = '';
        $item->postalCode = $this->receiver_zip_code;
        $item->destinationCountry = $this->receiver_country;
        $item->returnItemWanted = false;
        $item->shipmentAmount = number_format($this->package_value, 2);
        $item->shipmentCurrency = 'EUR';
        $item->shipmentGrossWeight = $this->package_weight;


        if (!in_array($this->receiver_country_id, CountryList::getUeList())) {

            $item->shipmentNaturetype = 'OTHERS';

            $item->contents = [];

            $contents = new stdClass();
//        $item->contents->contentPieceHsCode = '392690';
            $contents->contentPieceDescription = mb_substr($this->package_content, 0, 33);
            $contents->contentPieceValue = number_format($this->package_value, 2);
            $contents->contentPieceNetweight = $this->package_weight;
            $contents->contentPieceOrigin = $this->sender_country;
            $contents->contentPieceAmount = 1;

            $item->contents[] = $contents;
        }


        $orderId = ManifestQueque::getManifestId(GLOBAL_BROKERS::BROKER_DEUTCHE_POST, date('Y-m-d'), ManifestQueque::TYPE_COMMON);
        if(!$orderId) {

            $data = new stdClass();
            $data->customerEkp = self::CLIENT_EKP;
            $data->orderStatus = 'OPEN';
            $data->paperwork = new stdClass();
            $data->paperwork->contactName = 'ROMAD';
            $data->paperwork->jobReference = '';
            $data->paperwork->pickupType = 'CUSTOMER_DROP_OFF';
            $data->paperwork->awbCopyCount = '1';
            $data->items = [];

            $data->items[] = $item;

            $resp = $this->_call('dpi/shipping/v1/orders', $data);
            $resp = json_decode($resp);

            if ($resp->orderId == '') {
                if ($returnError == true) {
                    return GlobalOperatorResponse::createErrorResponse(print_r($resp->messages != '' ? $resp->messages : 'No response from DP server!', 1));
                } else {
                    return false;
                }
            }

            ManifestQueque::setManifestId(GLOBAL_BROKERS::BROKER_DEUTCHE_POST, ManifestQueque::TYPE_COMMON, $resp->orderId);

            $itemId = $resp->items[0]->id;
            $trackId = $resp->items[0]->barcode;

        } else {

            $data = [ $item ];

            $resp = $this->_call('dpi/shipping/v1/orders/'.$orderId.'/items', $data);
            $resp = json_decode($resp);

            if (!$resp OR !is_array($resp)) {
                if ($returnError == true) {
                    return GlobalOperatorResponse::createErrorResponse(print_r($resp->messages != '' ? $resp->messages : 'Something went wrong!', 1));
                } else {
                    return false;
                }
            }

            $itemId = $resp[0]->id;
            $trackId = $resp[0]->barcode;

        }

//        $orderId = $resp->orderId;
//        $awb = $resp->shipments[0]->awb;


//        $label = $this->_call('dpi/shipping/v1/shipments/'.$awb.'/awblabels', false);
        $label = $this->_call('dpi/shipping/v1/items/'.$itemId.'/label', false, false, true);

//        $details = $this->_call('dpi/shipping/v1/orders/'.$id.'', false);
//
//        var_dump($details);
//        exit;

//        $label = $this->_call('dpi/shipping/v1/items/'.$awb.'/awblabels', false);
//
//        var_dump($label);

        if($label == '')
        {
            if($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse(print_r($resp->messages != '' ? $resp->messages : 'No response from DP server!',1));
            } else {
                return false;
            }
        }

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        $im->readImageBlob($label);
        $im->setImageFormat('png8');
//
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $imgW = $im->getImageWidth();
        $imgH = $im->getImageHeight();

        if($imgW == 3000 && $imgH == 2000)
        {
            $im->chopImage(500, 0, 500, 0);
        }

        $im->rotateImage(new ImagickPixel(), 90);
//
////        if($this->label_annotation_text)
////            LabelAnnotation::createAnnotation($this->label_annotation_text, $im, 1015,730);
//
        $im->trimImage(0);
        $im->stripImage();

        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $trackId);
//
        return $return;
    }

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        if ($returnErrors == true) {
            return GlobalOperatorResponse::createErrorResponse('Not active for Courier!');
        } else {
            return false;
        }

        $model = new self;
        $model->setService($uniq_id);

        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id, GLOBAL_BROKERS::BROKER_DEUTCHE_POST);


        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierInternal->courier->getWeight(true) * 1000;

        $model->package_content = $courierInternal->courier->package_content;
        $model->ref = $courierInternal->courier->local_id;
        $model->package_value = $courierInternal->courier->getPackageValueConverted('EUR');

        // @ 19.02.2019 - special rule
        if(in_array($courierInternal->courier->user_id, [2537]))
            $model->ref = $courierInternal->courier->package_content;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        if ($returnErrors == true) {
            return GlobalOperatorResponse::createErrorResponse('Not active for Courier!');
        } else {
            return false;
        }

        $model = new self;
        $model->setService($courierU->courierUOperator->uniq_id);

        $model->label_annotation_text = $courierU->courierUOperator->label_symbol;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;


        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierU->courier->getWeight(true) * 1000;

        $model->package_content = $courierU->courier->package_content;
        $model->ref = $courierU->courier->local_id;
        $model->package_value = $courierU->courier->getPackageValueConverted('EUR');

        // @ 19.02.2019 - special rule
        if(in_array($courierU->courier->user_id, [2537]))
            $model->ref = $courierU->courier->package_content;

        return $model->createShipment($returnErrors);
    }


    public static function orderForPostal(Postal $postal, $returnErrors = false)
    {
        $model = new self;

        $model->setService($postal->postalOperator->uniq_id);

        $from = $postal->senderAddressData;
        $to = $postal->receiverAddressData;

        $model->sender_country_id = $from->country0->id;
        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->getUsefulName(true);

        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $postal->weight;

        $model->package_content = $postal->content;
        $model->ref = $postal->local_id;

        $model->package_value = $postal->getValueConverted('EUR');

        // @ 19.02.2019 - special rule
        if(in_array($postal->user_id, [2537]))
            $model->ref = $postal->content;


        return $model->createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return self::_track($no);
    }

    public static function trackForPostal($no)
    {
        return self::_track($no);
    }

    protected static function _track($no)
    {
        $model = new self;
        $resp = $model->_call('dpi/tracking/v1/trackings/'.$no, false);
        $resp = json_decode($resp);

        $statuses = new GlobalOperatorTtResponseCollection();

        if($resp->events)
        {
            foreach($resp->events AS $event)
            {
                $name = (string) $event->status;
                $date = (string) $event->timestamp;
                $date = mb_substr(str_replace('T', ' ', $date), 0, 19);

                $statuses->addItem($name, $date, NULL);
            }
        }


        return $statuses;
    }

    protected function _getToken()
    {
        $CACHE_NAME = 'DEUTCHE_POST_TOKEN';

        $token = Yii::app()->cache->get($CACHE_NAME);
        if($token === false)
        {
            $resp = $this->_call('v1/auth/accesstoken', false, true);
            $resp = json_decode($resp);
            if($resp->access_token)
            {
                $token = $resp->access_token;
            }
            Yii::app()->cache->set($CACHE_NAME, $token, 14400); // 4 hours
        }

        return $token;
    }


    protected function _call($method, $data, $loginMethod = false, $getLabel = false)
    {

        $url = self::URL.'/'.$method;

        MyDump::dump('deutche_post.txt', 'REQ : '. $url.' : '.print_r(json_encode($data), 1));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        // it's getting token
        if($loginMethod) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Accept: application-json",
                "Authorization: Basic " . base64_encode(self::CLIENT_ID . ':' . self::CLIENT_SECRET)
            ));
        } else {

            $array = [];
            $array[] = 'Content-Type: application/json';
            $array[] = 'Authorization: Bearer ' . $this->_getToken();

            if($getLabel)
                $array[] = 'Accept: image/png+6x4';
            else
                $array[] = 'Accept: application/json';

            curl_setopt($ch, CURLOPT_HTTPHEADER, $array);

        }

        if($data) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $response = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $error = curl_error($ch);

        MyDump::dump('deutche_post.txt', 'RESP : '. print_r($response, 1));
        MyDump::dump('deutche_post.txt', 'RESP CODE : '. print_r($httpcode, 1));
        MyDump::dump('deutche_post.txt', 'RESP ERROR : '. print_r($error, 1));
        return $response;

    }


}