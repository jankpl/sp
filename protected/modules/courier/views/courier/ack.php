<div class="form">
    <h2><?= Yii::t('courier', 'Potwierdzenia nadania');?></h2>
    <?php

    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
        array(
            'enableAjaxValidation' => false,
        )
    );
    ?>

    <?php
    echo $form->errorSummary($model);
    ?>

    <table class="table table-striped" style="margin: 10px auto;">
        <tr>
            <td><?php echo $form->labelEx($model,'onlyNew'); ?> (*)</td>
            <td><?php echo $form->checkBox($model, 'onlyNew'); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->labelEx($model,'ignoreOperatorsCustoms'); ?></td>
            <td><?php echo $form->checkBox($model, 'ignoreOperatorsCustoms'); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->labelEx($model,'days'); ?> (**)</td>
            <td><?php echo $form->dropDownList($model, 'days', F_GenerateAck::listOfDays()); ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><?php echo TbHtml::submitButton(Yii::t('app', 'Generuj'), array('class' => 'btn btn-lg btn-primary'));?></td>
        </tr>
    </table>
    <?php
    $this->endWidget();
    ?>

    <div class="info">
        <div class="alert alert-info">
            (*) <?php echo Yii::t('ack', 'Tylko dla paczek, dla których jeszcze nie wygnerowano karty.'); ?><br/>
           (**) <?php echo Yii::t('ack', 'Dzień liczony jest od godziny 0:00. "1" - paczki z dzisiaj.'); ?>
        </div>
    </div>
</div>

