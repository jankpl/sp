<?php

Yii::import('application.modules.postal.models._base.BasePostalUserGroup');

class PostalUserGroup extends BasePostalUserGroup
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }


    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),

            array('admin_id', 'numerical', 'integerOnly' => true),
            array('admin_id', 'in', 'range' => CHtml::listData(Admin::model()->findAll(),'id', 'id')),
            array('admin_id', 'default', 'value' => Yii::app()->user->id),

            array('name, date_entered', 'required'),
            array('name', 'length', 'max'=>45),

            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, date_updated', 'safe', 'on'=>'search'),
        );
    }

    public function getMembers()
    {
        $models = User::model()->findAllBySql('
        SELECT *
        FROM user
        WHERE user.id
        IN
        (SELECT user_id FROM postal_user_group_has_user WHERE postal_user_group_id = :group_id)',
            array(':group_id' => $this->id));

        return $models;

    }

    public function getNonMembers()
    {

        $models = User::model()->findAllBySql('
        SELECT *
        FROM user
        WHERE user.id
        NOT IN
        (SELECT user_id FROM postal_user_group_has_user)'
        );

        return $models;
    }

    public function removeMember($id)
    {
        $model = PostalUserHasUserGroup::model()->find('user_id=:user_id AND postal_user_group_id=:postal_user_group_id', array(':user_id' => $id, ':postal_user_group_id' => $this->id));

        if($model === null)
            return false;

        if($model->delete())
            return true;
        else
            return false;
    }

    public function addMember($id)
    {

        $user = User::model()->findByPk($id);

        if($user === null)
            return false;

        $model = new PostalUserHasUserGroup();
        $model->postal_user_group_id = $this->id;
        $model->user_id = $id;

        if($model->save())
            return true;
        else
            return false;
    }

    public function howManyMembers()
    {

        $models = PostalUserHasUserGroup::model()->findAll('postal_user_group_id=:group_id', array(':group_id' => $this->id));
        return S_Useful::sizeof($models);

    }

    public static function listGroups()
    {
        $models = self::model()->findAll();
        return $models;
    }

    /**
     * Returns PostalUserGroup ID or false if not found
     *
     * @param $user_id
     * @return integer|false
     */
    public static function getPostalGroupIdByUserId($user_id)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->cache(60)->createCommand();
        $cmd->selectDistinct('postal_user_group_id');
        $cmd->from('postal_user_group_has_user');
        $cmd->where('user_id = :user_id', [':user_id' => $user_id]);
        $result = $cmd->queryScalar();

        return $result;
    }

    public function getNameWithAdmin()
    {
        return $this->id.') '. $this->name.' ['.($this->admin_id ? $this->admin->login : '-').']';
    }

    public static function getListForThisAdmin()
    {
        if(AdminRestrictions::isRoutingAdmin())
            return self::model()->findAll();
        else
            return self::model()->findAll('admin_id = :admin_id', [':admin_id' => Yii::app()->user->id]);
    }

}