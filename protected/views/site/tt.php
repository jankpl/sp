<div class="form">

    <h2><?php echo Yii::t('courier','Track&trace');?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'tt-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=> false,
    ));
    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <?php echo CHtml::label(Yii::t('site','ID przesyłki'), 'id') ?>
        <?php echo CHtml::textField('id'); ?>
        <?php
        echo TbHtml::submitButton(Yii::t('site','Sprawdź'), ['class' => 'btn btn-primary']);
        $this->endWidget();
        ?>
    </div><!-- row -->


</div><!-- form -->
<?php
if(0):
    ?>
    <div style="padding: 5px; margin: 10px auto; background: #e1e1e1; border: 1px solid #bababa; width: 500px; text-align: center;">    <?php
        $this->Widget('ShowPageContent', array('id' => 16));
        ?></div>
<?php
endif;
?>
<br/>
<div class="text-center"><a href="<?= Yii::app()->createUrl('/site/ttMulti');?>" class="btn"><?= Yii::t('tt', 'Śledzenie wielu paczek na raz'); ?></a></div>
<br/><br/>


<?php echo $partial; ?>
<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
])):
    ?>
    <br/>
    <br/>
    <div class="text-center"><a href="https://carrierlist.17track.net/en/international/100033" target="_blank"><img src="<?= Yii::app()->baseUrl;?>/images/misc/17track.png" style="width: 150px;"/></a></div>
<?php
endif;
?>