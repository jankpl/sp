<?php
/* @var $model Plugins */
?>

<h1><?= Yii::t('plugins', 'Integracje/wtyczki');?> : <?= $model->name;?></h1>
<br/>
<br/>
<?= $model->pluginsTr->content;?>
<br/>
<br/>
<p class="text-center"><a href="<?= Yii::app()->createUrl('/plugins');?>"><?= Yii::t('plugins', 'Wróc do listy');?></a></p>
