<?php
/* @var $id_prefix string */
?>
    <br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-kn',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/knMulti'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('courier', 'Generate KsiążkaNadawcza'), array(
    'onClick' => 'js:
    $("#'.$id_prefix.'courier-ids-kn").val($("#couriergrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => $id_prefix.'courier-ids-kn')); ?>
<?php
$this->endWidget();
?>
<br/>
<?= Yii::t('courier', 'Dotyczy tylko przesyłek, w których operatorem jest Poczta Polska.');?>