<?php

class NBPCurrency extends CApplicationComponent
{
    const DEFAULT_EURPLN = 4;
    const DEFAULT_USDPLN = 4;
    const DEFAULT_GBPPLN = 4.7;
    const DEFAULT_UAHPLN = 0.16;

    protected $eurPln = false;
    protected $usdPln = false;
    protected $gbpPln = false;
    protected $uahPln = false;
    protected function _fetch($currency)
    {
        $result = false;
        try {

            $url = 'http://www.nbp.pl/kursy/xml/LastA.xml';
            $result = @file_get_contents($url);

            $xml = new SimpleXMLElement($result);
            $result = $xml->xpath('//tabela_kursow/pozycja[kod_waluty="'.$currency.'"]')[0]->kurs_sredni;
            $result = (string)$result;

            if ($result == '')
                $result = 0;

            $result = str_replace(',', '.', $result);
            $result = doubleval($result);
            MyDump::dump('nbp.txt', 'SUCCESS : '.print_r($result,1));
        }
        catch (Exception $ex)
        {
            MyDump::dump('nbp.txt', 'ERROR 1');
        }

        return $result;
    }


    public function getEURPLNValue()
    {
        if(!$this->eurPln)
        {
            $CACHE_NAME = 'NBP_CURRENCY_EURPLN';

            $result = Yii::app()->cache->get($CACHE_NAME);

            if($result === false)
            {
                $result = $this->_fetch('EUR');
                if($result > 0)
                {
                    Yii::app()->cache->set($CACHE_NAME, $result, 43200);
                }
            }

            $this->eurPln = $result;
        }

        if(!is_numeric($this->eurPln) OR $this->eurPln <= 0)
            return self::DEFAULT_EURPLN;
        else
            return $this->eurPln;
    }

    public function getUSDPLNValue()
    {
        if(!$this->usdPln)
        {
            $CACHE_NAME = 'NBP_CURRENCY_USDPLN';

            $result = Yii::app()->cache->get($CACHE_NAME);

            if($result === false)
            {
                $result = $this->_fetch('USD');
                if($result > 0)
                {
                    Yii::app()->cache->set($CACHE_NAME, $result, 43200);
                }
            }

            $this->usdPln = $result;
        }

        if(!is_numeric($this->usdPln) OR $this->usdPln <= 0)
            return self::DEFAULT_USDPLN;
        else
            return $this->usdPln;

    }

    public function getGBPPLNValue()
    {
        if(!$this->gbpPln)
        {
            $CACHE_NAME = 'NBP_CURRENCY_GBPPLN';

            $result = Yii::app()->cache->get($CACHE_NAME);

            if($result === false)
            {
                $result = $this->_fetch('GBP');
                if($result > 0)
                {
                    Yii::app()->cache->set($CACHE_NAME, $result, 43200);
                }
            }

            $this->gbpPln = $result;
        }

        if(!is_numeric($this->gbpPln) OR $this->gbpPln <= 0)
            return self::DEFAULT_GBPPLN;
        else
            return $this->gbpPln;

    }

    public function getUAHPLNValue()
    {
        if(!$this->uahPln)
        {
            $CACHE_NAME = 'NBP_CURRENCY_UAHPLN';

            $result = Yii::app()->cache->get($CACHE_NAME);

            if($result === false)
            {
                $result = $this->_fetch('UAH');
                if($result > 0)
                {
                    Yii::app()->cache->set($CACHE_NAME, $result, 43200);
                }
            }

            $this->uahPln = $result;
        }

        if(!is_numeric($this->uahPln) OR $this->uahPln <= 0)
            return self::DEFAULT_UAHPLN;
        else
            return $this->uahPln;

    }

    public function PLN2EUR($value)
    {
        return round($value / $this->getEURPLNValue(), 2);
    }

    public function PLN2USD($value)
    {
        return round($value / $this->getUSDPLNValue(), 2);
    }

    public function PLN2GBP($value)
    {
        return round($value / $this->getGBPPLNValue(), 2);
    }


    public function EUR2PLN($value)
    {
        return round($this->getEURPLNValue() * $value, 2);
    }

    public function USD2PLN($value)
    {
        return round($this->getUSDPLNValue() * $value, 2);
    }

    public function GBP2PLN($value)
    {
        return round($this->getGBPPLNValue() * $value, 2);
    }


    public function PLN2UAH($value)
    {
        return round($value / $this->getUAHPLNValue(), 2);
    }

    public function getLastMonthEurExcForDate($date)
    {

        $date = new DateTime($date);
        $value = NULL;
        $previousValue = false;
        $year = $date->format('Y');
        $month = $date->format('m');

        $CACHE_NAME = 'NBP_CURRENCY_CHART_'.$year;

        $file = Yii::app()->cacheFile->get($CACHE_NAME);

        if($file === false)
        {
            $file = @file_get_contents('http://www.nbp.pl/kursy/Archiwum/archiwum_tab_a_'.$year.'.csv');

            $file = explode( PHP_EOL, $file);
            Yii::app()->cacheFile->set($CACHE_NAME, $file, 3600);

        }

        $date = $year.str_pad($month,2,0,STR_PAD_LEFT);

        $eurIndex = false;
        foreach($file AS $line)
        {

            $item = explode(';', $line);

            if(!$eurIndex)
            {
                foreach($item AS $key => $value2)
                {
                    if($value2 == '1EUR') {
                        $eurIndex = $key;
                        break;
                    }

                }
            } else {

                if(substr($item[0],0,6) == $date)
                {
                    if($previousValue && $previousValue != 'euro')
                        $value = $previousValue;
                    else
                        $value = $item[$eurIndex];
                    break;
                }
                else
                    $previousValue = $item[$eurIndex];
            }
        }

        $value = str_replace(',', '.', $value);

        return $value;
    }

    protected static function toPLN($value, $sourceCurrency)
    {
        switch($sourceCurrency)
        {
            case 'PLN':
                return $value;
                break;
            case 'GBP':
                return Yii::app()->NBPCurrency->GBP2PLN($value);
                break;
            case 'EUR':
                return Yii::app()->NBPCurrency->EUR2PLN($value);
                break;
            case 'USD':
                return Yii::app()->NBPCurrency->USD2PLN($value);
                break;
            default:
                throw new Exception('Impossible currency conversion to PLN!');
        }
    }

    protected static function fromPLN($value, $targetCurrency)
    {
        switch($targetCurrency)
        {
            case 'PLN':
                return $value;
                break;
            case 'GBP':
                return Yii::app()->NBPCurrency->PLN2GBP($value);
                break;
            case 'EUR':
                return Yii::app()->NBPCurrency->PLN2EUR($value);
                break;
            case 'USD':
                return Yii::app()->NBPCurrency->PLN2USD($value);
                break;
            default:
                throw new Exception('Impossible currency conversion from PLN!');
        }
    }

    public function getCurrencyValue($value, $sourceCurrency, $targetCurrency)
    {
        $sourceCurrency = strtoupper($sourceCurrency);
        $targetCurrency = strtoupper($targetCurrency);
        $value = floatval($value);

        if($sourceCurrency == $targetCurrency)
            return $value;

        $plnValue = self::toPLN($value, $sourceCurrency);

        $targetValue = self::fromPLN($plnValue, $targetCurrency);

        return number_format(round($targetValue, 2), 2, '.','');
    }
}