<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sender_country_id'); ?>
		<?php echo $form->dropDownList($model, 'sender_country_id', GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'receiver_country_id'); ?>
		<?php echo $form->dropDownList($model, 'receiver_country_id', GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'date_entered'); ?>
		<?php echo $form->textField($model, 'date_entered'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'date_updated'); ?>
		<?php echo $form->textField($model, 'date_updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'local_id'); ?>
		<?php echo $form->textField($model, 'local_id', array('maxlength' => 12)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'hash'); ?>
		<?php echo $form->textField($model, 'hash', array('maxlength' => 64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sender_name'); ?>
		<?php echo $form->textField($model, 'sender_name', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sender_country'); ?>
		<?php echo $form->textField($model, 'sender_country', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sender_zip_code'); ?>
		<?php echo $form->textField($model, 'sender_zip_code', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sender_city'); ?>
		<?php echo $form->textField($model, 'sender_city', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sender_address_line_1'); ?>
		<?php echo $form->textField($model, 'sender_address_line_1', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sender_address_line_2'); ?>
		<?php echo $form->textField($model, 'sender_address_line_2', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sender_tel'); ?>
		<?php echo $form->textField($model, 'sender_tel', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'receiver_name'); ?>
		<?php echo $form->textField($model, 'receiver_name', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'receiver_country'); ?>
		<?php echo $form->textField($model, 'receiver_country', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'receiver_zip_code'); ?>
		<?php echo $form->textField($model, 'receiver_zip_code', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'receiver_city'); ?>
		<?php echo $form->textField($model, 'receiver_city', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'receiver_address_line_1'); ?>
		<?php echo $form->textField($model, 'receiver_address_line_1', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'receiver_address_line_2'); ?>
		<?php echo $form->textField($model, 'receiver_address_line_2', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'receiver_tel'); ?>
		<?php echo $form->textField($model, 'receiver_tel', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'stat'); ?>
		<?php echo $form->textField($model, 'stat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'mail_weight'); ?>
		<?php echo $form->textField($model, 'mail_weight'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'mail_size_l'); ?>
		<?php echo $form->textField($model, 'mail_size_l'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'mail_size_w'); ?>
		<?php echo $form->textField($model, 'mail_size_w'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'mail_size_d'); ?>
		<?php echo $form->textField($model, 'mail_size_d'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'order_id'); ?>
		<?php echo $form->dropDownList($model, 'order_id', GxHtml::listDataEx(Order::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
