<?php

class LabelQuequeCommand extends ConsoleCommand {

    const LIMIT = 50;

    public function init()
    {
        Yii::app()->getModule('courier');
        parent::init();
    }

    public function filters()
    {
        return array();
    }

    function actionDeamon()
    {
        while(1)
        {
            $models = CourierLabelNew::model()->findAll(array('condition' => 'stat = :stat', 'params' => array(':stat' => CourierLabelNew::STAT_NEW), 'order' => 'id ASC', 'limit' => self::LIMIT));

            // no more found - finish the job...
            if(S_Useful::sizeof($models))
            {
                foreach ($models AS $item) {
                    echo $item->id."\r\n";;
                    $item->runLabelOrdering();
                }
            } else
                echo '.'."\r\n";

            sleep(1);
        }
    }

    function actionRunOne($id = NULL)
    {
        Yii::app()->getModule('courier');

        $item = CourierLabelNew::model()->findAll(array('condition' => '(id = :id OR parent_id = :id)', 'params' => array(':id' => $id), 'order' => 'id ASC', 'limit' => 1));
        $item->runLabelOrdering();
        Yii::app()->end();
    }



}