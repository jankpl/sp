<?php

class HybridMailStatController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'HybridMailStat'),
        ));
    }

    public function actionCreate() {
        $model = new HybridMailStat;
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $statTr = new HybridMailStatTr('create');
            $statTr->language_id = $item->id;
            $modelTrs[$item->id] = $statTr;
        }

        if (isset($_POST['HybridMailStat'])) {
            $model->setAttributes($_POST['HybridMailStat']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            $model->validate();

            if($model->hasErrors())
                $errors = true;

            $model->save();

            foreach(Language::model()->findAll('stat = 1') AS $item)
            {
                if($modelTrs[$item->id] === null)
                    $modelTrs[$item->id] = new HybridMailStatTr;

                $modelTrs[$item->id]->setAttributes($_POST['HybridMailStatTr'][$item->id]);
                $modelTrs[$item->id]->hybrid_mail_stat_id = $model->id;
                $modelTrs[$item->id]->language_id = $item->id;

                if(! $modelTrs[$item->id]->save())
                    $errors = true;
            }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
            else
            {
                $transaction->rollback();
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionUpdate($id) {


        $model = $this->loadModel($id, 'HybridMailStat');

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = HybridMailStatTr::model()->find('hybrid_mail_stat_id=:hybrid_mail_stat_id AND language_id=:language_id', array(':hybrid_mail_stat_id' => $model->id, ':language_id' => $item->id));

            if($modelTrs[$item->id] == NULL)
            {
                $statTr = new HybridMailStatTr('create');
                $statTr->language_id = $item->id;
                $modelTrs[$item->id] = $statTr;
            }
        }

        if (isset($_POST['HybridMailStat'])) {
            $model->setAttributes($_POST['HybridMailStat']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            $model->validate();

            if($model->hasErrors())
                $errors = true;

            $model->save();

            foreach(Language::model()->findAll('stat = 1') AS $item)
            {
                if($modelTrs[$item->id] === null)
                    $modelTrs[$item->id] = new HybridMailStatTr;
                $modelTrs[$item->id]->setAttributes($_POST['HybridMailStatTr'][$item->id]);
                $modelTrs[$item->id]->hybrid_mail_stat_id = $model->id;
                $modelTrs[$item->id]->language_id = $item->id;

                if(! $modelTrs[$item->id]->save())
                    $errors = true;
            }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
            else
            {
                $transaction->rollback();
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {

        $model = HybridMailStat::model()->findByPk($id);
        if($model == NULL)
            throw new CHttpException(404);
        if($model->editable != S_Status::ACTIVE)
            throw new CHttpException(403, 'This status cannot be edited or deleted');

        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $model =  $this->loadModel($id, 'HybridMailStat');

                foreach($model->hybridMailStatTrs AS $item)
                {
                    $item->delete();


                }

                $model->delete();
            }
            catch (Exception $ex)
            {
                throw new CHttpException(400, Yii::t('app', 'You cannot delete it!'));
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {

        $model = new HybridMailStat('search');
        $model->unsetAttributes();

        if (isset($_GET['HybridMailStat']))
            $model->setAttributes($_GET['HybridMailStat']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}