<?php

class CourierUController extends Controller {

    public function beforeAction($action)
    {

        if((Yii::app()->user->model->getCourierUDomesticAvailable() && $_GET['domestic']) OR (Yii::app()->user->model->getCourierUAvailable() && !$_GET['domestic']))
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier',

            ));
            exit;
        }
    }

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function accessRules()
    {
        return array(

            array('allow',
//                'actions'=>array('create','created','index', 'list','ajaxGetOperatorList', 'ajaxGetCodCurrency', 'ajaxCheckRemoteArea', 'ajaxCheckAdditions'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect(['/courier/courierU/create']);
    }

    /**
     * Fill form with cloned package data
     *
     * @param Courier $model
     */
    protected function _clone_U_fill()
    {

        if(isset(Yii::app()->session['cloneTypeU'])) {

            $data = Yii::app()->session['cloneTypeU'];
            unset(Yii::app()->session['cloneTypeU']);

            $this->setPageState('step1_model', $data['model']);
            $this->setPageState('step1_courierTypeU', $data['modelTypeU']);
            $this->setPageState('step1_senderAddressData', $data['senderAddressData']);
            $this->setPageState('step1_receiverAddressData', $data['receiverAddressData']);
            $this->setPageState('courier_u_additions', $data['additions']);
        }

    }

    public function actionCreate($domestic = false)
    {
        if(!$domestic)
            $this->panelLeftMenuActive = 180;
        else
            $this->panelLeftMenuActive = 181;

        if(Yii::app()->session['courierUFormFinished']) // prevent going back and sending the same form again
        {
            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['courierUFormFinished']);
            $this->refresh(true);
        }

        $this->_clone_u_fill();

        //for AJAX validation
//        {
//            $model=new Courier('insert');
//            $this->performAjaxValidation($model, 'courier-form');
//            unset($model);
//        }

        if(isset($_POST['summary']))
            $this->create_step_summary($domestic); // Validate addition list, summary
        elseif (isset($_POST['finish']) OR isset($_POST['finish_goback']))
            $this->create_finish($domestic,  isset($_POST['finish_goback']) ? true : false);
        else
            $this->create_step_1($domestic); // this is the default, first time (step1)

    }

    protected function create_step_1($domestic, $validate = false)
    {
        if($validate)
        {
            if($this->create_validate_step_1($domestic) == 'break')
                return;
        }

        if(Yii::app()->user->isGuest && !$this->getPageState('loginProposition')) {

            $this->setPageState('loginProposition', 1);

            $model = new LoginForm();
            $model->returnURL = Yii::app()->createUrl('courier/create');

            $this->render('create/loginProposition', array('model' => $model));

        } else {

            $this->setPageState('form_started', true);
            $this->setPageState('start_user_id', Yii::app()->user->id);

            $this->setPageState('last_step',1); // save information about last step

            $model = new Courier_CourierTypeU(Courier_CourierTypeU::SCENARIO_STEP1);
            $model->packages_number = 1;

            if($model->package_weight)
                $model->_package_weight_total = (int) $model->packages_number * (double) $model->package_weight;

            $model->user_id = Yii::app()->user->id;

            $model->courierTypeU = new CourierTypeU(CourierTypeU::SCENARIO_STEP1);

            $model->courierTypeU->courier = $model;

            $model->senderAddressData = new CourierTypeU_AddressData('insert', UserContactBook::TYPE_SENDER);
            $model->receiverAddressData = new CourierTypeU_AddressData('insert', UserContactBook::TYPE_RECEIVER);

            // load user data as sender
            if(!sizeof($this->getPageState('step1_senderAddressData',array())))
                $model->senderAddressData->loadUserData();
            else
                $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());


            $model->attributes = $this->getPageState('step1_model',[]);
            $model->courierTypeU->attributes = $this->getPageState('step1_courierTypeU', []);

            $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);
            $modelCourierUAddition = $this->getPageState('courier_u_additions', []);

            $currency = CourierTypeU::getCurrencyIdForCurrentUser();
            $codCurrency = CourierCodAvailability::findCurrencyForCountry($model->receiverAddressData->country_id);

            $this->setPageState('last_step',1); // save information about last step

            if($model->cod_value > 0)
                $client_cod = true;
            else
                $client_cod = false;

            $model->courierTypeU->_selected_operator_id = $model->courierTypeU->courier_u_operator_id;

            if($model->senderAddressData->country_id == '' OR $model->receiverAddressData->country_id == '')
                if($domestic)
                {
                    $model->senderAddressData->country_id = CountryList::COUNTRY_PL;
                    $model->receiverAddressData->country_id = CountryList::COUNTRY_PL;
                } else {
                    $model->receiverAddressData->country_id = Yii::app()->user->model->getDefaultPackageReceiverCountryId() ? Yii::app()->user->model->getDefaultPackageReceiverCountryId() : CountryList::COUNTRY_PL;
                }

            $model->afterFinalInit();

            $model->package_weight_client = $model->package_weight;

            $this->render('create/step1',
                [
                    'currency' => $currency,
                    'model' => $model,
                    'codCurrency' => $codCurrency,
                    'client_cod' => $client_cod,
                    'modelCourierUAddition' => $modelCourierUAddition,
                    'domestic' => $domestic,
                ]);
        }
    }

    protected function create_validate_step_1($domestic)
    {
        $model = new Courier_CourierTypeU(Courier_CourierTypeU::SCENARIO_STEP1);
        $model->courierTypeU = new CourierTypeU(CourierTypeU::SCENARIO_STEP1);
        $model->courierTypeU->courier = $model;

        $model->user_id = Yii::app()->user->id;
        $model->senderAddressData = new CourierTypeU_AddressData();
        $model->receiverAddressData = new CourierTypeU_AddressData();

//        $currency = CourierTypeU::getCurrencyIdForCurrentUser();
        $errors = false;
        if(isset($_POST['Courier_CourierTypeU']))
        {

            if($domestic)
            {
                $_POST['CourierTypeU_AddressData']['sender']['country_id'] = CountryList::COUNTRY_PL;
                $_POST['CourierTypeU_AddressData']['receiver']['country_id'] = CountryList::COUNTRY_PL;
            }

            $courierAddition = [];

            $model->attributes = $_POST['Courier_CourierTypeU'];
            $model->courierTypeU->attributes = $_POST['CourierTypeU'];
            $model->senderAddressData->attributes = $_POST['CourierTypeU_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['CourierTypeU_AddressData']['receiver'];

            $model->package_weight_client = $model->package_weight;

            $selectedGroups = [];
            // Courier additions:
            if(is_array($_POST['CourierUAddition']))
                foreach($_POST['CourierUAddition'] AS $item) {
                    $temp = CourierUAdditionList::model()->findByPk($item);

                    if ($temp !== null) {
                        // select only first from group
                        if ($temp->group != '') {
                            if (isset($selectedGroups[$temp->group])) {
                                continue;
                            }
                            else {
                                $selectedGroups[$temp->group] = true;
                            }
                        }
                        array_push($courierAddition, $item);
                    }
                }

            if(!is_array($courierAddition))
                $courierAddition = [];

            $model->courierTypeU->courier = $model;
            $model->courierTypeU->addAdditions($courierAddition);

            if($model->packages_number)
                $model->package_weight = (double) $model->_package_weight_total / $model->packages_number;

            if(!isset($_POST['CourierTypeU']))
                $_POST['CourierTypeU'] = [];


            // REPLACED _client_cod_value!
            if($model->cod_value > 0)
                $model->senderAddressData->_bankAccountRequired = true;
            else
                $model->senderAddressData->_bankAccountRequired = false;

            if(!$model->validate())
                $errors = true;

            if(!$model->courierTypeU->validate())
                $errors = true;


            if(!$model->senderAddressData->validate())
                $errors = true;

            if(!$model->receiverAddressData->validate())
                $errors = true;

            $this->setPageState('step1_model',$model->getAttributes(array_merge(array_keys($_POST['Courier_CourierTypeU']),array('package_weight_client'))));  // save previous form into form state
            $this->setPageState('step1_courierTypeU',$model->courierTypeU->getAttributes(array_merge(array_keys($_POST['CourierTypeU']),array( '_package_wrapping')))); // save previous form into form state
            $this->setPageState('step1_senderAddressData', $model->senderAddressData->getAttributes(array_keys($_POST['CourierTypeU_AddressData']['sender']))); // save previous form into form state
            $this->setPageState('step1_receiverAddressData',$model->receiverAddressData->getAttributes(array_keys($_POST['CourierTypeU_AddressData']['receiver']))); // save previous form into form state
            $this->setPageState('courier_u_additions',$model->courierTypeU->_courier_additions);
        }

        $modelCourierUAddition = $this->getPageState('courier_u_additions', []);

//        $remoteZipSender = CourierDhlRemoteAreas::findZip($model->senderAddressData->zip_code, $model->senderAddressData->country_id, $model->senderAddressData->city);
//        $remoteZipReceiver = CourierDhlRemoteAreas::findZip($model->receiverAddressData->zip_code, $model->receiverAddressData->country_id, $model->receiverAddressData->city);
//
//        $currencyId = CourierTypeU::getCurrencyIdForCurrentUser();

        $currency = CourierTypeU::getCurrencyIdForCurrentUser();
        $codCurrency = CourierCodAvailability::findCurrencyForCountry($model->receiverAddressData->country_id);

        if($model->cod_value > 0)
            $client_cod = true;
        else
            $client_cod = false;

        $model->courierTypeU->_selected_operator_id = $model->courierTypeU->courier_u_operator_id;

        if($errors)
        {
            $this->render('create/step1', [

                'currency' => $currency,
                'model' => $model,
                'codCurrency' => $codCurrency,
                'client_cod' => $client_cod,
                'modelCourierUAddition' => $modelCourierUAddition,
                'domestic' => $domestic,
            ]);
            return 'break';
        }
    }

    protected function create_step_summary($domestic)
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');

        switch($lastStep)
        {
            case 1 :
                if($this->create_validate_step_1($domestic) == 'break')
                    return;
                break;
        }

        $model = new Courier_CourierTypeU;
        $model->attributes = $this->getPageState('step1_model',array());
        $model->courierTypeU = new CourierTypeU('step_1');
        $model->courierTypeU->attributes = $this->getPageState('step1_courierTypeU',array());
        $model->senderAddressData = new CourierTypeU_AddressData();
        $model->receiverAddressData = new CourierTypeU_AddressData();


        $additions = $this->getPageState('courier_u_additions',array());

        $model->courierTypeU->courier = $model;
        $model->courierTypeU->addAdditions($additions);

        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData',array());



        $model->user_id = Yii::app()->user->id;

        $this->setPageState('last_step', 'summary'); // save information about last step

        $this->render('create/summary',array(
            'model'=>$model,
        ));
    }
//
    protected function create_validate_summary($domestic)
    {
        $error = false;

        $model = new Courier_CourierTypeU;
        $model->attributes = $this->getPageState('step1_model',array());
        $model->courierTypeU = new CourierTypeU('summary');
        $model->courierTypeU->attributes = $this->getPageState('step1_courierTypeU',array());
        $model->senderAddressData = new CourierTypeU_AddressData();
        $model->receiverAddressData = new CourierTypeU_AddressData();

        $additions = $this->getPageState('courier_u_additions',array());

        $model->courierTypeU->courier = $model;
        $model->courierTypeU->addAdditions($additions);


        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData',array());

        $model->user_id = Yii::app()->user->id;


        $model->courierTypeU->regulations = $_POST['CourierTypeU']['regulations'];
        $model->courierTypeU->regulations_rodo = $_POST['CourierTypeU']['regulations_rodo'];
        $model->courierTypeU->validate(array('regulations', 'regulations_rodo'));

        $model->courierTypeU->courier = $model;

        if($model->hasErrors() OR $model->courierTypeU->hasErrors() OR $error)
        {
            $this->render('create/summary',array(
                'model'=>$model,
            ));
            return 'break';
        }

    }

    protected function create_finish($domestic, $goBack = true)
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');


        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 'summary')
        {

            if($this->create_validate_summary($domestic) == 'break')
                return;

            $this->setPageState('last_step',3); // save information about last step

            $model = new Courier_CourierTypeU;
            $model->attributes = $this->getPageState('step1_model',array());
            $model->courierTypeU = new CourierTypeU('step_1');
            $model->courierTypeU->attributes = $this->getPageState('step1_courierTypeU',array());
            $model->courierTypeU->courier = $model;
            $model->senderAddressData = new CourierTypeU_AddressData();
            $model->receiverAddressData = new CourierTypeU_AddressData();

            $additions = $this->getPageState('courier_u_additions',array());

            $model->courierTypeU->courier = $model;
            $model->courierTypeU->addAdditions($additions);

            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());
            $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData',array());

            $model->user_id = Yii::app()->user->id;

            try
            {
                $return = $model->courierTypeU->saveNewPackage();
            }
            catch(Exception $ex)
            {
                Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
                throw new CHttpException(403, 'Wystapił nieznany błąd!');
            }

            if($return)
            {
                // order labels for all of found
                /* @var $item Courier_CourierTypeU */
                if(is_array($return))
                    foreach($return AS $item)
                    {
                        CourierLabelNew::asyncCallForId($item->courierTypeU->courier_label_new_id);
                    }

                $temp = $return[0];

                // to get proper Hash
                $temp = Courier::model()->findByPk($temp->id);

                if($goBack)
                    Yii::app()->user->setFlash('previousModel', Yii::t('order','Zamówienie {no} zostało sfinalizowane! {list}', ['{no}' => CHtml::link('#'.$model->courierTypeU->order->local_id, Yii::app()->createUrl('/order/view', ['urlData' => $model->courierTypeU->order->hash]) ,['target' => '_blank', 'class' => 'btn btn-sm']), '{list}' => CHtml::link(Yii::t('order', 'Lista paczek'), Yii::app()->createUrl('/courier/courier/list', ['#' => 'list']) ,['target' => '_blank', 'class' => 'btn btn-sm'])]));

//                    Yii::app()->user->setFlash('previousModel', Yii::t('courier', 'Paczka została utworzona!{br}{br}{a}szczegóły paczki{/a}', array('{br}' => '<br/>', '{a}' => '<a href="' . Yii::app()->createUrl("/courier/courier/view", ["urlData" => $temp->hash]) . '" class="btn">', '{/a}' => '</a>')));


                // get id's of just created packages
                $_justCreated = [];
                foreach($model->courierTypeU->order->orderProduct AS $item)
                    array_push($_justCreated, $item->type_item_id);

                self::addToListOfJustCreated($_justCreated);

                if($goBack)
                    $this->redirect(array('/courier/courierU/create'));
                else {
                    $model->refresh(); // to get hash
                    $this->redirect(array('/courier/courier/view', 'urlData' => $temp->hash, 'new' => true));
                }


            } else
                throw new CHttpException(403, 'Wystapił nieznany błąd!');

        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }

    protected static function addToListOfJustCreated(array $packagesIds)
    {

        $justCreated = Yii::app()->session['internalJustCreated'];
        $justCreated = CMap::mergeArray($justCreated, $packagesIds);
        Yii::app()->session['internalJustCreated'] = $justCreated;
    }

    public function actionAjaxGetOperatorList()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $receiver_country_id = $_POST['receiver_country_id'];
            $receiver_city = $_POST['receiver_city'];
            $receiver_zip = $_POST['receiver_zip'];

            $sender_country_id = $_POST['sender_country_id'];
            $sender_city = $_POST['sender_city'];
            $sender_zip = $_POST['sender_zip'];

            $collection = $_POST['colletcion'];


            $weight = $_POST['weight'];

            $size_l = $_POST['size_l'];
            $size_d = $_POST['size_d'];
            $size_w = $_POST['size_w'];

            $operators = CourierTypeU::findAvailableOperators($receiver_country_id, $size_l, $size_d, $size_w);
            $selectedOperator = $_POST['selected_operator'];



            $courierTypeU = new CourierTypeU();
            $courierTypeU->courier = new Courier_CourierTypeU();
            $courierTypeU->courier->package_weight = $weight;
            $courierTypeU->courier->package_size_l = $size_l;
            $courierTypeU->courier->package_size_d = $size_d;
            $courierTypeU->courier->package_size_w = $size_w;


            $courierTypeU->collection = $collection;

            $courierTypeU->courier->senderAddressData = new CourierTypeU_AddressData();
            $courierTypeU->courier->senderAddressData->zip_code = $sender_zip;
            $courierTypeU->courier->senderAddressData->city = $sender_city;
            $courierTypeU->courier->senderAddressData->country_id = $sender_country_id;

            $courierTypeU->courier->receiverAddressData = new CourierTypeU_AddressData();
            $courierTypeU->courier->receiverAddressData->zip_code = $receiver_zip;
            $courierTypeU->courier->receiverAddressData->city = $receiver_city;
            $courierTypeU->courier->receiverAddressData->country_id = $receiver_country_id;

            $courierTypeU->courierUOperator = CourierUOperator::model()->findByPk($selectedOperator);

            foreach($operators AS $key => $operator)
            {
                $operators[$key]['price'] = $weight ? CourierTypeU::getPricesForRoute($courierTypeU, $receiver_country_id, $key, Yii::app()->user->getUserGroupId()) : 'false';
            }

            $html = $this->renderPartial('create/_operators', array(
                'operators' => $operators,
                'selectedOperator' => $selectedOperator,

            ), true, false);

            echo CJSON::encode(['html' => $html, 'no' => sizeof($operators)]);
        }

        Yii::app()->end();
    }

    public function actionAjaxCheckRemoteArea()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $country_id = $_POST['country_id'];
            $zip_code = $_POST['zip_code'];
            $city = $_POST['city'];
            $operator = $_POST['operator'];

            /* @var $result CourierUOperator */
            $result = CourierUOperator::model()->findByPk($operator);

            if($result) {
                $result = $result->getRemoteAreaPrice($zip_code, $country_id, $city);
                if($result)
                    $result = S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($result, true)).'/'.S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($result)).' '.CourierTypeU::getCurrencySymbolForCurrentUser().' '.Yii::t('courier','netto/brutto');
            }

            echo CJSON::encode($result);
        }

        Yii::app()->end();
    }

    public function actionAjaxGetCodCurrency()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $country_id = $_POST['country_id'];
            $currency = CourierCodAvailability::findCurrencyForCountry($country_id);

            if($currency)
                $currency = CourierCodAvailability::getCurrencyNameById($currency);

            echo CJSON::encode([
                $currency,
            ]);
        }

        Yii::app()->end();
    }

    public function actionAjaxCheckAdditions()
    {
        if(Yii::app()->request->isAjaxRequest) {

            $sender_country_id = $_POST['sender_country_id'];
            $sender_zip_code = $_POST['sender_zip_code'];
            $receiver_country_id = $_POST['receiver_country_id'];
            $receiver_zip_code = $_POST['receiver_zip_code'];
            $collection = $_POST['collection'];
            $weight = $_POST['weight'];
            $selected_operator_id = $_POST['selected_operator_id'];

            $model = new Courier_CourierTypeU();
            $model->package_weight = $weight;

            $modelSender = new CourierTypeU_AddressData();
            $modelSender->country_id = $sender_country_id;
            $modelSender->zip_code = $sender_zip_code;
            $model->senderAddressData = $modelSender;

            $modelReceiver = new CourierTypeU_AddressData();
            $modelReceiver->country_id = $receiver_country_id;
            $modelReceiver->zip_code = $receiver_zip_code;
            $model->receiverAddressData = $modelReceiver;

            $model->courierTypeU = new CourierTypeU();
            $model->courierTypeU->collection = $collection;
            $model->courierTypeU->courier = $model;

            $courierUOperator = CourierUOperator::model()->findByPk($selected_operator_id);

            $model->courierTypeU->courierUOperator = $courierUOperator;

            $modelsList = CourierUAdditionList::getAdditionsByCourier($model);
            $list = CHtml::listData($modelsList, 'id', 'id');

            echo CJSON::encode($list);
            Yii::app()->end();
        } else {
            $this->redirect(['/']);
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function actionAjaxCheckJustEbay()
    {
        $local_id = $_POST['localId'];

        Yii::app()->getModule('courier');

        $rerun = false;
        $courier = Courier::model()->findByAttributes(array('local_id' => $local_id));
        if($courier === NULL)
            $stat = EbayImport::STAT_UNKNOWN;
        else
        {
            $courierEbayImport = $courier->courierEbayImport;

            if($courierEbayImport === NULL)
                $stat = EbayImport::STAT_UNKNOWN;
            else
            {
                $stat = $courierEbayImport->stat;
            }
        }

        if($stat == EbayImport::STAT_NEW)
            $rerun = true;

        echo CJSON::encode([
                'rerun' => $rerun,
                'stat' => $stat,
            ]
        );
    }

    public function actionQuequeLabelByLocalId()
    {

        $local_id = $_POST['localId'];

        Yii::app()->getModule('courier');

        $courier = Courier::model()->findByAttributes(array('local_id' => $local_id));

        $courier_id = $courier->id;

        /* @var $parent courierTypeU */
        $parent = $courier->courierTypeU;

        $return = false;

        if($parent !== NULL)
        {
            $stat = $parent->courierLabelNew->stat;
            if($stat == CourierLabelNew::STAT_NEW) {

//                file_put_contents('dump2.txt', 'ORDER!:'.print_r($parent->id,1)."\r\n", FILE_APPEND);
                $return = $parent->courierLabelNew->runLabelOrdering(true);
//                file_put_contents('dump2.txt', 'ids:'.print_r($ids ? 'true' : 'false',1)."\r\n", FILE_APPEND);

            } else {
//                if($parent->_ebay_order_id != '')
//                    $parent->__temp_ebay_update_stat = CourierEbayImport::STAT_UNKNOWN;
//                $ids = [$courier->id];
            }



            $rerun = false;
            if($stat == CourierLabelNew::STAT_WAITING_FOR_PARENT OR ($stat == CourierLabelNew::STAT_NEW))
//            if($stat == CourierLabelNew::STAT_WAITING_FOR_PARENT OR ($stat == CourierLabelNew::STAT_NEW))
            {
                if(!$return)
                    $rerun = true;
                else
                {
                    //get current status
                    $courierRefreshed = Courier::model()->findByPk($courier_id);
                    $stat = $courierRefreshed->courierTypeU->courierLabelNew->stat;
                }
            }


            $log = '';
            if($stat == CourierLabelNew::STAT_FAILED)
            {
                $log = 'Error';
                if($courier->getSingleLabel(false, false, true) && $courier->getSingleLabel(false, false, true)->log != '')
                    $log = 'Error: '.$courier->getSingleLabel(false, false, true)->log;
            }


            $ebayStatus = EbayImport::STAT_UNKNOWN;
            if(!$rerun && $stat != CourierLabelNew::STAT_FAILED)
            {

                $ebayStatus = $parent->courier->ebayImport->stat;
            }

            $return = array(
                'request' => true,
                'stat' => $stat,
                'desc' => $log,
                'rerun' => $rerun,
            );

            if($parent->courier->_ebay_order_id != '')
                $return['ebayImport'] = $ebayStatus;


            echo CJSON::encode($return);
            exit;
        } else {
            echo CJSON::encode(array('request' => false));
            exit;
        }
    }

    public function actionImport($hash = NULL)
    {

        $this->panelLeftMenuActive = 181;

        $fileModel = new F_UImport();

        if($hash !== NULL)
        {
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
            Yii::import('zii.behaviors.CTimestampBehavior');

            $models = Yii::app()->cacheUserData->get('uImport_' . $hash.'_'.Yii::app()->session->sessionID);

            if(isset($_POST['refresh'])) {
                if(F_UImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
                }
                // save modified by validators models:
                Yii::app()->cacheUserData->set('uImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                $this->refresh();
                exit;
            }
            else if(isset($_POST['save']))
            {
//                Yii::app()->PM->m('1', 'BEFORE VALIDATE', true);
                if(F_UImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));

                    // save modified by validators models:
                    Yii::app()->cacheUserData->set('uImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                    $this->refresh();
                    exit;
                }

                try
                {
//                    Yii::app()->PM->m('1', 'BEFORE SAVE WHOLE GROUP');
                    $result = CourierTypeU::saveWholeGroup($models);
                }
                catch (Exception $ex)
                {
                    Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
                    return;
                }
                // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING


                if($result['success'])
                {
                    $import = CourierImportManager::getByHash($hash);
                    /* @var $item Courier */
                    $ids = array();
                    if(is_array($result['data']))
                        foreach($result['data'] AS $item)
                        {
                            array_push($ids, $item->id);
                        }

                    $import->addCourierIds($ids);
                    $import->save();

//                    Yii::app()->PM->m('1', 'BEFORE REDIRECT');

                    Yii::app()->cacheUserData->delete('uImport_rawdata_' . $hash.'_'.Yii::app()->session->sessionID);
                    Yii::app()->cacheUserData->delete('uImport_' . $hash.'_'.Yii::app()->session->sessionID);
                    $this->redirect(Yii::app()->createUrl('/courier/courierU/listImported', ['hash' => $hash]));
                    exit;
                }

            } else {



                // validate only after editing one item in row
                if(Yii::app()->session['courierUImport_'.$hash] == 10)
                {
                    unset(Yii::app()->session['courierUImport_'.$hash]);
                    if (sizeof($models))
                        F_UImport::validateModels($models);
                }


                if (!$models OR !sizeof($models)) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Import nie powiódł się!'));
                    $this->redirect(Yii::app()->createUrl('courier/courierU/import'));
                }

                $this->render('import/showModels', array(
                    'models' => $models,
                    'hash' => $hash,
                ));
                return;
            }
        }


        // file upload
        if(isset($_POST['F_UImport']) ) {
            $fileModel->setAttributes($_POST['F_UImport']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if (!$fileModel->validate()) {
                $this->render('import/index', array(
                    'model' => $fileModel,
                ));
                return;
            } else {
                // file upload success:

                if($fileModel->custom_settings)
                    $fileModel->applyCustomData($fileModel->custom_settings, Yii::app()->user->id);

                // generate new hash
                $hash = hash('sha256', time() . Yii::app()->user->id);

                $inputFile = $fileModel->file->tempName;

//                Yii::import('application.extensions.phpexcel.XPHPExcel');
//                XPHPExcel::init();

                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFile);


                // WITH FIX:
                if(strtoupper($inputFileType) == 'CSV')
                {
                    // SET UTF8 ENCODING IN CSV
                    $file_content = file_get_contents( $inputFile );

                    $file_content = S_Useful::forceToUTF8($file_content);
                    file_put_contents( $inputFile, $file_content );

                    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                    // FIX ENCODING

                    if($fileModel->_customSeparator)
                        $objReader->setDelimiter($fileModel->_customSeparator);
                    else
                        $objReader->setDelimiter(',');


                } else {
                    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    $objReader->setReadDataOnly(true);
                }


                // WITHOUT FIX:
//
//                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//                if(strtoupper($inputFileType) == 'CSV' && $fileModel->_customSeparator)
//                {
//                    $objReader->setDelimiter($fileModel->_customSeparator);
//                }

                $objPHPExcel = $objReader->load($inputFile);

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $data = [];
                $start = 1;

                if ($fileModel->omitFirst) {
                    $headerSeen = false;
                } else
                    $headerSeen = true;

                for ($row = $start; $row <= $highestRow; $row++) {
                    if (!$headerSeen) {
                        $headerSeen = true;
                        continue;
                    }

                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                    array_push($data, $rowData[0]);
                }

                // remove empty lines:
                $data = (array_filter(array_map('array_filter', $data)));

                $numberOfLines = sizeof($data);

                // check just by lines number
                if($numberOfLines > F_UImport::MAX_LINES)
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość wierszy w pliku to: {no}! Naliczono: {number}', array('{no}' => F_UImport::MAX_LINES, '{number}' => $numberOfLines)));
                    $this->refresh(true);
                    exit;
                }

                $numberOfPackages = F_UImport::calculateTotalNumberOfPackages($data, $fileModel->_customOrder);
                // check by number of packages
                if($numberOfPackages > F_UImport::MAX_PACKAGES) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość paczek w pliku to: {no}! Naliczono: {number}', array('{no}' => F_UImport::MAX_PACKAGES, '{number}' => $numberOfPackages)));
                    $this->refresh(true);
                    exit;
                }

                $models = F_UImport::mapAttributesToModels($data, $fileModel->_customOrder, $fileModel->cutTooLong, $fileModel->u_operator_id);
                F_UImport::validateModels($models);

                Yii::app()->cacheUserData->set('uImport_rawdata_' . $hash.'_'.Yii::app()->session->sessionID, $data, 86400);
                Yii::app()->cacheUserData->set('uImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                $this->redirect(Yii::app()->createUrl('courier/courierU/import', ['hash' => $hash]));
                exit;
            }
        }


        $this->render('import/index', array(
            'model' => $fileModel,
        ));

    }

    protected function _exportOrDeleteErroredImportData($hash, $e, $l, $export = false, $delete = false)
    {
        if (!$export && !$delete)
            return false;

        require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        if ($e) {
            $models = Yii::app()->cacheUserData->get('ebayImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        }
        elseif ($l) {
            $models = Yii::app()->cacheUserData->get('linkerImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('uImport_' . $hash . '_' . Yii::app()->session->sessionID);
            $rawData = Yii::app()->cacheUserData->get('uImport_rawdata_' . $hash . '_' . Yii::app()->session->sessionID);
        }


        if (!sizeof($models))
            return false;

        $toExport = [];

        foreach ($models AS $key => $model)
        {

            $temp = [];
            $temp[0] = $model;
            F_UImport::validateModels($temp);
            $models[$key] = $temp[0];

            if ($models[$key]->hasErrors() OR $models[$key]->senderAddressData->hasErrors() OR $models[$key]->receiverAddressData->hasErrors() OR $models[$key]->receiverAddressData->hasErrors() OR $models[$key]->courierTypeU->hasErrors()) {
                if ($export) {
                    if (!$e)
                        $toExport[] = $rawData[$key];
                    else
                        $toExport[] = $model;
                }


                if ($delete)
                    unset($models[$key]);
            }
        }

        if($delete) {
            if ($e) {
                Yii::app()->cacheUserData->set('ebayImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
            }
            else if ($l) {
                Yii::app()->cacheUserData->set('linkerImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);

            } else {
                // from file import
                Yii::app()->cacheUserData->set('uImport_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
            }

// used to force reloading list of imported packages with validation to, for example: recalculate prices
            Yii::app()->session['uImport_' . ($e ? 'e_' : '') .  ($l ? 'l_' : '') . $hash] = 10;
        }

        if($export)
        {
            if(!$e && !$l) {
                $toExport = S_XmlGenerator::fillEmptyKeys($toExport);
                S_XmlGenerator::generateXml($toExport);
            }
            else
                S_CourierIO::exportToXlsUser($toExport);
            Yii::app()->end();
        }

        if($delete)
        {
            $this->redirect(['/courier/courierU/import', 'hash' => $hash]);
        }

    }

    public function actionExportErroredImportData($hash, $e = false, $l = false)
    {
        self::_exportOrDeleteErroredImportData($hash, $e, $l,true, false);
    }

    public function actionDeleteErroredImportData($hash, $e = false, $l = false)
    {
        self::_exportOrDeleteErroredImportData($hash, $e, $l,false, true);
    }

    public function actionListImported($hash)
    {

        $this->panelLeftMenuActive = 180;


        $import = CourierImportManager::getByHash($hash);

        if($import->user_id != Yii::app()->user->id)
            throw new CHttpException(404);

        if(!$import)
            throw new CHttpException(404);

        // FASTER WAY:
        $couriers = [];
        if(is_array($import->courier_ids))
            $couriers = Courier::model()->with('courierTypeU')->with('courierTypeU.courierLabelNew')->findAllByPk($import->courier_ids);

        $CACHE_NAME = 'COURIER_U_IMPORT_INSTANCE_'.$hash;
        $cacheValue = Yii::app()->cache->get($CACHE_NAME);

        $mainInstance = false;
        if($cacheValue === false)
            $mainInstance = true;

        Yii::app()->cache->set($CACHE_NAME, 10, 60*60);

        $this->render('listImported',array(
            'import' => $import,
            'couriers' => $couriers,
            'hash' => $hash,
            'mainInstance' => $mainInstance
        ));

    }

    public function actionAjaxRemoveRowOnImport($e = false, $l = false)
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        if($e) {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);

            unset($models[$id]);
            Yii::app()->cacheUserData->set('ebayImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
        }
        elseif($l)
        {
            $models = Yii::app()->cacheUserData->get('linkerImportU_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);

            unset($models[$id]);
            Yii::app()->cacheUserData->set('linkerImportU_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('uImport_' . $hash.'_'.Yii::app()->session->sessionID);
            unset($models[$id]);

            Yii::app()->cacheUserData->set('uImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        }

        Yii::app()->session['courierUImport_'.($e ? 'e_' : '') .$hash] = 10;

        echo CJSON::encode(array('success' => true));
    }

    public function actionAjaxEditRowOnImport($e = false, $l = false)
    {

        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        if($e) {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        }
        elseif($l)
        {
            $models = Yii::app()->cacheUserData->get('linkerImportU_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);
        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('uImport_' . $hash.'_'.Yii::app()->session->sessionID);
        }

        $model = $models[$id];

        if(isset($model)) {


            $modelCourierAddition = array_keys($model->courierTypeU->_imported_additions_ok);
            $modelCourierAdditionList = CourierUAdditionList::getAdditionsByCourier($model);

            $currency = Yii::app()->PriceManager->getCurrency();

            $additionsHtml = $this->renderPartial('import/_additionsHtml', array(
                'modelCourierAdditionList'=>$modelCourierAdditionList,
                'courierAddition'=>$modelCourierAddition,
                'currency' => $currency,
            ), true, false);

            $countries = CountryList::model()->cache(60*60)->with('countryListTr')->with('countryListTrs')->findAll();

            if($e)
                $html = $this->renderPartial('importEbay/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'e' => $e, 'additionsHtml' => $additionsHtml], true, true);
            else if($l)
                $html = $this->renderPartial('importLinker/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'l' => $l, 'additionsHtml' => $additionsHtml], true, true);
            else
                $html = $this->renderPartial('import/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash, 'countriesReceiver' => $countries, 'e' => $e, 'additionsHtml' => $additionsHtml], true, true);

//            file_put_contents('htmldump.txt', print_r($html,1));

            echo CJSON::encode(array('success' => true, 'html' => $html));

        } else {
            echo CJSON::encode(array('success' => false));
        }

    }

    public function actionAjaxUpdateRowItemOnImport($e = false, $l = false)
    {
        if(Yii::app()->request->isAjaxRequest) {

            $hash = $_POST['hash'];
            $i = $_POST['i'];
            $attribute = $_POST['attribute'];
            $val = $_POST['val'];

            $possibleToEdit = [
                'receiverAddressData.name',
                'receiverAddressData.company',
                'receiverAddressData.zip_code',
                'receiverAddressData.address_line_1',
                'receiverAddressData.address_line_2',
                'receiverAddressData.city',
                'receiverAddressData.tel',
                'receiverAddressData.email',
                'senderAddressData.name',
                'senderAddressData.company',
                'senderAddressData.zip_code',
                'senderAddressData.address_line_1',
                'senderAddressData.address_line_2',
                'senderAddressData.city',
                'senderAddressData.tel',
                'senderAddressData.email',
                'package_weight',
                'package_size_l',
                'package_size_d',
                'package_size_w',
            ];
            if(!in_array($attribute, $possibleToEdit))
                return false;

            try {
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
                Yii::import('zii.behaviors.CTimestampBehavior');

                if($e)
                    $models = Yii::app()->cacheUserData->get('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
                elseif($l)
                    $models = Yii::app()->cacheUserData->get('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
                else
                    $models = Yii::app()->cacheUserData->get('uImport_' . $hash.'_'.Yii::app()->session->sessionID);

                /* @var $model Courier */
                if (is_array($models)) {
                    $model = $models[$i];

                    $submodel = false;
                    if ($model) {

                        $attribute = explode('.', $attribute);
                        if(sizeof($attribute) > 1)
                        {
                            $submodel = $attribute[0];

//                          $model = $model->$attribute[0];
                            // PHP7 fix
                            $temp = $attribute[0];
                            $model = $model->$temp;
//

                            $attribute = $attribute[1];
                        } else
                            $attribute = $attribute[0];


                        if ($model->hasAttribute($attribute)) {
                            $backupValue = $model->$attribute;
                            $model->$attribute = $val;

                            $model->validate([$attribute]);
                            if (!$model->hasErrors($attribute)) {

                                if($submodel)
                                    $models[$i]->$submodel = $model;
                                else
                                    $models[$i] = $model;

                                if($e)
                                    Yii::app()->cacheUserData->set('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                                elseif($l)
                                    Yii::app()->cacheUserData->set('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
                                else
                                    Yii::app()->cacheUserData->set('uImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);

                                echo CJSON::encode(['success' => true]);
                                return;

                            } else {

                                echo CJSON::encode([
                                    'success' => false,
                                    'msg' => strip_tags($model->getError($attribute)),
                                    'backup' => $backupValue,
                                ]);
                                return;
                            }
                        }
                    }
                }

                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie. {msg}', ['{msg}' => print_r($attribute,1)])
                ]);
                return;

            } catch (Exception $ex) {
                Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);
                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie')
                ]);
                return;

            }
        }
    }

    public function actionAjaxSaveRowOnImport($e = false, $l = false)
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        if($e) {
            // from ebay import
            $models = Yii::app()->cacheUserData->get('ebayImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID);
        }
        elseif($l)
        {
            $models = Yii::app()->cacheUserData->get('linkerImportU_models_hash_'.$hash.'_'.Yii::app()->session->sessionID);
        } else {
            // from file import
            $models = Yii::app()->cacheUserData->get('uImport_' . $hash.'_'.Yii::app()->session->sessionID);
        }
        $model = $models[$id];

        if(isset($model)) {

            /* @var $model Courier_CourierTypeU */
            $model->courierTypeU->attributes = $_POST['CourierTypeU'];
            $model->senderAddressData->attributes = $_POST['CourierTypeU_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['CourierTypeU_AddressData']['receiver'];

            $model->senderAddressData->country0 = CountryList::model()->findByPk($model->senderAddressData->country_id);
            $model->receiverAddressData->country0 = CountryList::model()->findByPk($model->receiverAddressData->country_id);

            $model->attributes = $_POST['CrierTypeU'];


            if(isset($_POST['CourierAddition']))
                $model->courierTypeU->_imported_additions = $_POST['CourierAddition'];
            else
                $model->courierTypeU->_imported_additions = [];

//            $model->validate();
//            $model->courierTypeU->validate();
//            $model->senderAddressData->validate();
//            $model->receiverAddressData->validate();

            $temp = [];
            $temp[0] = $model;
            F_UImport::validateModels($temp);
            $models[$id] = $temp[0];

            if($e) {
                // from ebay import
                Yii::app()->cacheUserData->set('ebayImportU_models_hash_' . $hash . '_' . Yii::app()->session->sessionID, $models, 86400);
            }
            elseif($l)
            {
                Yii::app()->cacheUserData->set('linkerImportU_models_hash_'.$hash.'_'.Yii::app()->session->sessionID, $models, 86400);

            } else {

                // from file import
                Yii::app()->cacheUserData->set('uImport_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            }

            // used to force reloading list of imported packages with validation to, for example: recalculate prices
            Yii::app()->session['courierUImport_'.($e ? 'e_' : '').($l ? 'l_' : '') .$hash] = 10;

            echo CJSON::encode(array('success' => true));
        } else {
            echo CJSON::encode(array('success' => false));
        }

    }


    public function actionGetLabels($hash)
    {
        if(isset($_POST['label']))
        {
            $couriers = Courier::model()->findAllByAttributes(['id' => array_keys($_POST['label']), 'user_id' => Yii::app()->user->id]);

            if(sizeof($couriers)) {


                LabelPrinter::generateLabels($couriers, $_POST['Courier']['type'],true);
                exit;
            }
        }

        Yii::app()->user->setFlash('error', Yii::t('courier','Nie znaleziono żadnych etykiet do wygenerowania!'));
        $this->redirect(Yii::app()->createUrl('/courier/courierU/listImported', ['hash' => $hash]));
        exit;
    }


    public function actionEbay($al = false, $hash = false)
    {
        $this->panelLeftMenuActive = 113;

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        // autoload list once on enter
        if($al)
        {
            Yii::app()->session->add('ebay_autoload', true);
            $this->redirect(['/courier/courierU/ebay']);
            exit;
        }


        // initialize ebayClient
        $ebayClient = ebayClient::instance();

        if($hash)
        {
            if(isset($_POST['refresh'])) {
                $this->_ebay_step3_refresh($ebayClient, $hash);
                exit;
            }
            else if(isset($_POST['save']))
            {
                $this->_ebay_step3_process($ebayClient, $hash);
                exit;
            } else
                $this->_ebay_step3($ebayClient, $hash);
        }
        else if(isset($_POST['import']))
        {
            if($this->_ebay_step1_process($ebayClient) === 'true')
                $this->_ebay_step2($ebayClient);
            else
                $this->_ebay_step1($ebayClient);

        }
        else if(isset($_POST['save-default-data']))
        {
            if($this->_ebay_step2_process($ebayClient) === 'true')
                $this->_ebay_step3($ebayClient, $hash);

        }
        else
        {
            $this->_ebay_step1($ebayClient);
        }
    }


    protected function _ebay_step1(ebayClient $ebayClient)
    {
        $orders = [];

        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $stat = (int) $_POST['stat'];
        $statEbay = (int) $_POST['statEbay'];

        if($date_from == '')
            $date_from = Yii::app()->session->get('ebay_date_from', NULL);

        if($date_to == '')
            $date_to = Yii::app()->session->get('ebay_date_to', NULL);

        if(!$stat)
            $stat = Yii::app()->session->get('stat', NULL);

        if(!$statEbay)
            $statEbay = Yii::app()->session->get('statEbay', NULL);

        $date_begining = date('Y-m-d', strtotime('-'.ebayClient::ORDERS_GOBACK_MONTHS.' months'));
        $date_end = date('Y-m-d');

        if (strtotime($date_from) > strtotime($date_to))
            $date_from = $date_to;

        if (!strtotime($date_from) OR strtotime($date_from) < strtotime($date_begining))
            $date_from = $date_end;

        if (!strtotime($date_to) OR strtotime($date_to) > strtotime($date_end))
            $date_to = $date_end;

        if (!in_array($stat, array_keys(EbayOrderItem::getStatShowProcessedList())))
            $stat = EbayOrderItem::STAT_SHOW_PROCESSED_ALL;

        if (!in_array($statEbay, array_keys(EbayOrderItem::getStatShowEbayList())))
            $statEbay = EbayOrderItem::STAT_SHOW_EBAY_READY;

        $more = false;
        // on load order
        if((isset($_POST['get-data']) OR Yii::app()->session->get('ebay_autoload', false)) && $ebayClient->isLogged()) {

            Yii::app()->session->add('ebay_date_from', $date_from);
            Yii::app()->session->add('ebay_date_to', $date_to);
            Yii::app()->session->add('stat', $stat);
            Yii::app()->session->add('statEbay', $statEbay);

            Yii::app()->session->add('ebay_autoload', false);
            list($orders, $more) = $ebayClient->getOrders($date_from, $date_to, $stat, $statEbay);
            Yii::app()->cacheUserData->set('ebayImport_'.$ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $orders, 86400);
        }

        $number = sizeof($orders);

        $this->render('importEbay/step_1', [
            'ebayClient' => $ebayClient,
            'orders' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'stat' => $stat,
            'statEbay' => $statEbay,
            'date_begining' => $date_begining,
            'date_end' => $date_end,
            'number' => $number,
            'more' => $more,
        ]);
    }

    protected function _ebay_step1_process(ebayClient $ebayClient)
    {
        $ordersToImport = [];
        $ordersCached = Yii::app()->cacheUserData->get('ebayImport_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);

        if (is_array($_POST['import'])) {
            foreach ($_POST['import'] AS $importKey => $temp) {
                $temp = $ordersCached[$importKey];

                /* @var $temp EbayOrderItem */
                // prevent already processed in system
//                if(CourierEbayImport::findActivePackageByEbayOrderId($temp->orderID) === false) {
                if($temp instanceof EbayOrderItem) {
                    $temp = $temp->convertToCourierTypeUModel();
                    array_push($ordersToImport, $temp);
//                }
                } else {
                    throw new CHttpException(403);
                }

            }

            if(sizeof($ordersToImport) > EbayImportU::MAX_IMPORT_NUMBER)
            {
                Yii::app()->user->setFlash('error', Yii::t('courier_ebay', 'Maksymalna ilość paczek do importu to: {no}!', array('{no}' => EbayImportU::MAX_IMPORT_NUMBER)));
                Yii::app()->session->add('ebay_autoload', true);
                $this->refresh(true);
                exit;
            }

            Yii::app()->cacheUserData->set('ebayImportU_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            return "true";
        }
    }

    protected function _ebay_step2(ebayClient $ebayClient)
    {
        $senderAddressData = new CourierTypeU_AddressData;
        $receiverAddressData = new CourierTypeU_AddressData;
        $package = new Courier_CourierTypeU('default_package_data');
        $ebayImport = new EbayImportU();

        $package->package_size_l = 10;
        $package->package_size_w = 10;
        $package->package_size_d = 10;
        $package->package_weight = 1;
        $package->package_value = 10;

        $senderAddressData->attributes= User::getLoggedUserAddressData();

        $_additons = [];

        $this->render('importEbay/step_2', [
            'ebayClient' => $ebayClient,
            'senderAddressData' => $senderAddressData,
            'receiverAddressData' => $receiverAddressData,
            'package' => $package,
            'ebayImport' => $ebayImport,
            '_additions' => $_additons,
        ]);
    }

    protected function _ebay_step2_process(ebayClient $ebayClient)
    {

        $senderAddressData = new CourierTypeU_AddressData;
        $receiverAddressDataDefault = new CourierTypeU_AddressData;
        $package = new Courier_CourierTypeU(Courier_CourierTypeU::SCENARIO_DEFAULT_PACKAGE_DATA);
        $ebayImport = new EbayImportU();


        $courierTypeU = new CourierTypeU();
        $courierTypeU->courier = $package;
        $package->courierTypeU = $courierTypeU;

        $_additions = $_POST['_additions'];

        $senderAddressData->attributes = $_POST['CourierTypeU_AddressData']['sender'];
        $package->attributes = $_POST['Courier_CourierTypeU'];
        $ebayImport->attributes = $_POST['EbayImportU'];

        $receiverAddressDataDefault->tel = $_POST['CourierTypeU_AddressData']['receiver']['tel'];

        $senderAddressData->validate();
        $receiverAddressDataDefault->validate('tel');
        $package->validate();
        $ebayImport->validate();

        // ignore empty tel field
        if($receiverAddressDataDefault->tel == '')
            $receiverAddressDataDefault->clearErrors('tel');

        if(!$senderAddressData->hasErrors() && !$package->hasErrors() && !$ebayImport->hasErrors() && !$receiverAddressDataDefault->hasErrors('tel')) {

            $ordersToImport = Yii::app()->cacheUserData->get('ebayImportU_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, []);

            if(is_array($ordersToImport) && sizeof($ordersToImport))
            {
                /* @var $order Courier_CourierTypeU */
                foreach($ordersToImport AS &$order)
                {
                    $order->senderAddressData = new CourierTypeU_AddressData();
                    $order->senderAddressData->attributes = $senderAddressData->attributes;

                    if($order->receiverAddressData === NULL)
                        $order->receiverAddressData = new CourierTypeU_AddressData();
                    if( $order->receiverAddressData->tel == '')
                        $order->receiverAddressData->tel = $receiverAddressDataDefault->tel;

                    $order->package_weight = $package->package_weight;
                    $order->package_size_l = $package->package_size_l;
                    $order->package_size_w = $package->package_size_w;
                    $order->package_size_d = $package->package_size_d;
                    $order->package_value = $package->package_value;

                    $order->courierTypeU->collection = $ebayImport->collection;
                    $order->courierTypeU->courier_u_operator_id = $ebayImport->operator_id;

                    $order->courierTypeU->_imported_additions = $_additions;
                }

                F_UImport::validateModels($ordersToImport);

                Yii::app()->cacheUserData->delete('ebayImport_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);
                Yii::app()->cacheUserData->set('ebayImportU_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            }

            return "true";
        }



        $this->render('importEbay/step_2', [
            'ebayClient' => $ebayClient,
            'senderAddressData' => $senderAddressData,
            'receiverAddressData' => $receiverAddressDataDefault,
            'package' => $package,
            'ebayImport' => $ebayImport,
            '_additions' => $_additions,
        ]);
    }

    protected function _ebay_step3($ebayClient, $hash)
    {
        if(!$hash)
        {
            // generate new hash
            $hash = hash('sha256', time() . $ebayClient->getSessionId());
            $ordersToImport = Yii::app()->cacheUserData->get('ebayImportU_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));

            // rename data by hash and clear previous
            Yii::app()->cacheUserData->set('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $ordersToImport);
            Yii::app()->cacheUserData->delete('ebayImportU_models_' . $ebayClient->getSessionId().'_'.Yii::app()->session->sessionID);

            $this->redirect(['/courier/courierU/ebay', 'hash' => $hash]);
            exit;

        } else {
            $ordersToImport = Yii::app()->cacheUserData->get('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu!'));

            // added refresh button instead
            // validate only after editing one item in row
//            if(Yii::app()->session['courierUImport_e_'.$hash] == 10)
//            {
//                unset(Yii::app()->session['courierUImport_e_'.$hash]);
//                if (sizeof($ordersToImport))
//                    F_UImport::validateModels($ordersToImport);
//            }

//
            $this->render('importEbay/step_3', [
                'models' => $ordersToImport,
                'hash' => $hash,
            ]);
            return;
        }

    }

    protected function _ebay_step3_refresh(ebayClient $ebayClient, $hash)
    {
        $models = Yii::app()->cacheUserData->get('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !sizeof($models))
            throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));


        if(F_UImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
        }


        Yii::app()->cacheUserData->set('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        $this->refresh();
        exit;
    }

    protected function _ebay_step3_process(ebayClient $ebayClient, $hash)
    {
        $models = Yii::app()->cacheUserData->get('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !sizeof($models))
            throw new CHttpException(500, Yii::t('courier_ebay', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));

        if(F_UImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
            Yii::app()->cacheUserData->set('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            $this->refresh();
            exit;
        }

        try
        {
            $result = CourierTypeU::saveWholeGroup($models, CourierTypeU::SOURCE_EBAY, $ebayClient->getAuthToken(), false);
        }
        catch (Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            return;
        }

        // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING
        if($result['success'])
        {
            $import = CourierImportManager::getByHash($hash);
            /* @var $item Courier */
            $ids = array();
            if(is_array($result['data']))
                foreach($result['data'] AS $item)
                {
                    array_push($ids, $item->id);
                }

            $import->source = CourierImportManager::SOURCE_EBAY;
            $import->addCourierIds($ids);
            $import->save();

            Yii::app()->cacheUserData->delete('ebayImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
            $this->redirect(Yii::app()->createUrl('/courier/courierU/listImported', ['hash' => $hash]));
            exit;
        }
    }

    public function actionAjaxGetCountryList()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $country_lists_ids = $_POST['country_list_ids'];
            $country_lists_ids = explode(',', $country_lists_ids);

            $models = CountryList::model()->with('countryListTr')->findAllByAttributes(['id' => $country_lists_ids]);

            $names = [];
            /* @var $item CountryList */
            foreach($models AS $item)
                $names[] = $item->getTranslatedName();

            echo CJSON::encode($names);
        }

        Yii::app()->end();
    }



    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public function actionLinker($al = false, $hash = false)
    {
        $this->panelLeftMenuActive = 184;

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        // autoload list once on enter
        if($al)
        {
            Yii::app()->session->add('linker_autoload', true);
            $this->redirect(['/courier/courierU/linker']);
            exit;
        }

        if($hash)
        {
            if(isset($_POST['refresh'])) {
                $this->_linker_step3_refresh($hash);
                exit;
            }
            else if(isset($_POST['save']))
            {
                $this->_linker_step3_process($hash);
                exit;
            } else
                $this->_linker_step3($hash);
        }
        else if(isset($_POST['import']))
        {
            if($this->_linker_step1_process() === 'true')
                $this->_linker_step2();
            else
                $this->_linker_step1();

        }
        else if(isset($_POST['save-default-data']))
        {
            if($this->_linker_step2_process() === 'true')
                $this->_linker_step3($hash);

        }
        else
        {
            $this->_linker_step1();
        }
    }


    protected function _linker_step1()
    {
        $orders = [];

        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $stat = (int) $_POST['stat'];
        $statLinker = (int) $_POST['statLinker'];

        if($date_from == '')
            $date_from = Yii::app()->session->get('linker_date_from', NULL);

        if($date_to == '')
            $date_to = Yii::app()->session->get('linker_date_to', NULL);

        if(!$stat)
            $stat = Yii::app()->session->get('stat', NULL);

        if(!$statLinker)
            $statLinker = Yii::app()->session->get('statLinker', NULL);

        $date_begining = date('Y-m-d', strtotime('-'.LinkerClient::ORDERS_GOBACK_MONTHS.' months'));
        $date_end = date('Y-m-d');

        if (strtotime($date_from) > strtotime($date_to))
            $date_from = $date_to;

        if (!strtotime($date_from) OR strtotime($date_from) < strtotime($date_begining))
            $date_from = $date_end;

        if (!strtotime($date_to) OR strtotime($date_to) > strtotime($date_end))
            $date_to = $date_end;

        if (!in_array($stat, array_keys(LinkerOrderItem::getStatShowProcessedList())))
            $stat = LinkerOrderItem::STAT_SHOW_PROCESSED_ALL;

        if (!in_array($statLinker, array_keys(LinkerOrderItem::getStatShowLinkerList())))
            $statLinker = LinkerOrderItem::STAT_SHOW_LINKER_ALL;

        $sd = intval($_POST['sd']);

        $sa = $_POST['sa'];
        if (!in_array($sa, LinkerClient::sortAttributesList()))
            $sa = false;

        $more = false;
        // on load order
        if((isset($_POST['get-data']) OR Yii::app()->session->get('linker_autoload', false))) {

            Yii::app()->session->add('linker_date_from', $date_from);
            Yii::app()->session->add('linker_date_to', $date_to);
            Yii::app()->session->add('stat', $stat);
            Yii::app()->session->add('statLinker', $statLinker);

            Yii::app()->session->add('linker_autoload', false);
            list($orders, $more) =  LinkerClient::getOrders($date_from, $date_to, $statLinker, LinkerImport::TARGET_COURIER, $sa, $sd, $stat);
            Yii::app()->cacheUserData->set('linkerImport_'.Yii::app()->session->sessionID, $orders, 86400);
        }

        $number = sizeof($orders);

        $this->render('importLinker/step_1', [
            'orders' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'stat' => $stat,
            'statLinker' => $statLinker,
            'date_begining' => $date_begining,
            'date_end' => $date_end,
            'number' => $number,
            'more' => $more,
            'sa' => $sa,
            'sd' => $sd,
        ]);
    }

    protected function _linker_step1_process()
    {
        $ordersToImport = [];
        $ordersCached = Yii::app()->cacheUserData->get('linkerImport_'.Yii::app()->session->sessionID);

        if (is_array($_POST['import'])) {
            foreach ($_POST['import'] AS $importKey => $temp) {
                $temp = $ordersCached[$importKey];

                /* @var $temp LinkerOrderItem */
                // prevent already processed in system
//                if(CourierLinkerImport::findActivePackageByLinkerOrderId($temp->orderID) === false) {
                if($temp instanceof LinkerOrderItem) {
                    $temp = $temp->convertToCourierTypeUModel();
                    array_push($ordersToImport, $temp);
//                }
                } else {
                    throw new CHttpException(403);
                }

            }

            if(sizeof($ordersToImport) > LinkerImportU::MAX_IMPORT_NUMBER)
            {
                Yii::app()->user->setFlash('error', Yii::t('courier_linker', 'Maksymalna ilość paczek do importu to: {no}!', array('{no}' => LinkerImportU::MAX_IMPORT_NUMBER)));
                Yii::app()->session->add('linker_autoload', true);
                $this->refresh(true);
                exit;
            }

            Yii::app()->cacheUserData->set('linkerImportU_models_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            return "true";
        }
    }

    protected function _linker_step2()
    {
        $senderAddressData = new CourierTypeU_AddressData;
        $receiverAddressData = new CourierTypeU_AddressData;
        $package = new Courier_CourierTypeU('default_package_data');
        $linkerImport = new LinkerImportU();

        $package->package_size_l = 10;
        $package->package_size_w = 10;
        $package->package_size_d = 10;
        $package->package_weight = 1;
        $package->package_value = 10;

        $senderAddressData->attributes= User::getLoggedUserAddressData();

        $_additons = [];

        $this->render('importLinker/step_2', [
            'senderAddressData' => $senderAddressData,
            'receiverAddressData' => $receiverAddressData,
            'package' => $package,
            'linkerImport' => $linkerImport,
            '_additions' => $_additons,
        ]);
    }

    protected function _linker_step2_process()
    {

        $senderAddressData = new CourierTypeU_AddressData;
        $receiverAddressDataDefault = new CourierTypeU_AddressData;
        $package = new Courier_CourierTypeU(Courier_CourierTypeU::SCENARIO_DEFAULT_PACKAGE_DATA);
        $linkerImport = new LinkerImportU();


        $courierTypeU = new CourierTypeU();
        $courierTypeU->courier = $package;
        $package->courierTypeU = $courierTypeU;

        $_additions = $_POST['_additions'];

        $senderAddressData->attributes = $_POST['CourierTypeU_AddressData']['sender'];
        $package->attributes = $_POST['Courier_CourierTypeU'];
        $linkerImport->attributes = $_POST['LinkerImportU'];

        $receiverAddressDataDefault->tel = $_POST['CourierTypeU_AddressData']['receiver']['tel'];

        $senderAddressData->validate();
        $receiverAddressDataDefault->validate('tel');
        $package->validate();
        $linkerImport->validate();

        // ignore empty tel field
        if($receiverAddressDataDefault->tel == '')
            $receiverAddressDataDefault->clearErrors('tel');

        if(!$senderAddressData->hasErrors() && !$package->hasErrors() && !$linkerImport->hasErrors() && !$receiverAddressDataDefault->hasErrors('tel')) {

            $ordersToImport = Yii::app()->cacheUserData->get('linkerImportU_models_'.Yii::app()->session->sessionID, []);

            if(is_array($ordersToImport) && sizeof($ordersToImport))
            {
                /* @var $order Courier_CourierTypeU */
                foreach($ordersToImport AS &$order)
                {
                    $order->senderAddressData = new CourierTypeU_AddressData();
                    $order->senderAddressData->attributes = $senderAddressData->attributes;

                    if($order->receiverAddressData === NULL)
                        $order->receiverAddressData = new CourierTypeU_AddressData();
                    if( $order->receiverAddressData->tel == '')
                        $order->receiverAddressData->tel = $receiverAddressDataDefault->tel;

                    $order->package_weight = $package->package_weight;
                    $order->package_size_l = $package->package_size_l;
                    $order->package_size_w = $package->package_size_w;
                    $order->package_size_d = $package->package_size_d;
                    $order->package_value = $package->package_value;

                    $order->courierTypeU->collection = $linkerImport->collection;
                    $order->courierTypeU->courier_u_operator_id = $linkerImport->operator_id;

                    $order->courierTypeU->_imported_additions = $_additions;
                }

                F_UImport::validateModels($ordersToImport);

                Yii::app()->cacheUserData->delete('linkerImport_'.Yii::app()->session->sessionID);
                Yii::app()->cacheUserData->set('linkerImportU_models_'.Yii::app()->session->sessionID, $ordersToImport, 86400);
            }

            return "true";
        }



        $this->render('importLinker/step_2', [
            'senderAddressData' => $senderAddressData,
            'receiverAddressData' => $receiverAddressDataDefault,
            'package' => $package,
            'linkerImport' => $linkerImport,
            '_additions' => $_additions,
        ]);
    }

    protected function _linker_step3($hash)
    {
        if(!$hash)
        {
            // generate new hash
            $hash = hash('sha256', time() . uniqid() . rand(99999,9999999));
            $ordersToImport = Yii::app()->cacheUserData->get('linkerImportU_models_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu!'));

            // rename data by hash and clear previous
            Yii::app()->cacheUserData->set('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $ordersToImport);
            Yii::app()->cacheUserData->delete('linkerImportU_models_'.Yii::app()->session->sessionID);

            $this->redirect(['/courier/courierU/linker', 'hash' => $hash]);
            exit;

        } else {
            $ordersToImport = Yii::app()->cacheUserData->get('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

            if(!is_array($ordersToImport) OR !sizeof($ordersToImport))
                throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu!'));

            // added refresh button instead
            // validate only after editing one item in row
//            if(Yii::app()->session['courierUImport_e_'.$hash] == 10)
//            {
//                unset(Yii::app()->session['courierUImport_e_'.$hash]);
//                if (sizeof($ordersToImport))
//                    F_UImport::validateModels($ordersToImport);
//            }

//
            $this->render('importLinker/step_3', [
                'models' => $ordersToImport,
                'hash' => $hash,
            ]);
            return;
        }

    }

    protected function _linker_step3_refresh($hash)
    {
        $models = Yii::app()->cacheUserData->get('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !sizeof($models))
            throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));


        if(F_UImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
        }


        Yii::app()->cacheUserData->set('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
        $this->refresh();
        exit;
    }

    protected function _linker_step3_process($hash)
    {
        $models = Yii::app()->cacheUserData->get('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, []);

        if(!is_array($models) OR !sizeof($models))
            throw new CHttpException(500, Yii::t('courier_linker', 'Brak prawidłowych paczek do importu! Spróbuj ponownie.'));

        if(F_UImport::validateModels($models))
        {
            Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
            Yii::app()->cacheUserData->set('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID, $models, 86400);
            $this->refresh();
            exit;
        }

        try
        {
            $result = CourierTypeU::saveWholeGroup($models, CourierTypeU::SOURCE_LINKER, false, false);
        }
        catch (Exception $ex)
        {
            Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
            return;
        }

        // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING
        if($result['success'])
        {
            $import = CourierImportManager::getByHash($hash);
            /* @var $item Courier */
            $ids = array();
            if(is_array($result['data']))
                foreach($result['data'] AS $item)
                {
                    array_push($ids, $item->id);
                }

            $import->source = CourierImportManager::SOURCE_LINKER;
            $import->addCourierIds($ids);
            $import->save();

            Yii::app()->cacheUserData->delete('linkerImportU_models_hash_' . $hash.'_'.Yii::app()->session->sessionID);
            $this->redirect(Yii::app()->createUrl('/courier/courierU/listImported', ['hash' => $hash]));
            exit;
        }
    }


}