<?php

class AddressDataList extends CWidget
{
    /**
     * @var CFormModel
     */
    public $addressData;
    public $noEmail;

    public function run()
    {

        if($this->addressData == NULL)
            return;

        $this->render('addressDataList/_listInTable',
            array(
                'model' => $this->addressData,
                'noEmail' => $this->noEmail,
            ));
    }

}