<?php

class RoyalMailClient
{
    const URL = GLOBAL_CONFIG::ROYAL_MAIL_URL;
    const CLIENT_ID = GLOBAL_CONFIG::ROYAL_MAIL_CLIENT_ID;
    const CLIENT_SECRET = GLOBAL_CONFIG::ROYAL_MAIL_CLIENT_SECRET;
    const USERNAME = GLOBAL_CONFIG::ROYAL_MAIL_USERNAME;
    const PASSWORD = GLOBAL_CONFIG::ROYAL_MAIL_PASSWORD;
    const URL_TT_PREFIX = GLOBAL_CONFIG::ROYAL_MAIL_URL_TT_PREFIX;

    const RM_MAX_WEIGHT = 3; //KG

    const SERVICE_24 = 'T';
    const SERVICE_48 = 'T';
    const SERVICE_TRACKED_RETURN_48 = 'R';

    const SERVICE_CODE_24 = 'TPN';
    const SERVICE_CODE_48 = 'TPS';
    const SERVICE_CODE_TSS = 'TSS';

    const SHIPMENT_TYPE_DELIVERY = 'Delivery';
    const SHIPMENT_TYPE_RETURN = 'Return';

    protected $_service;
    protected $_serviceCode;
    protected $_shipmentType;
    public $_receiverAddressModel;
    public $content;

    public $weight;
    public $ref;
    public $number_of_items = 1;

    public $signature = false;

    public $_additions = [];

//    const ADD_INS_1000 = 1;
//    const ADD_INS_2500 = 2;
//    const ADD_INS_5000 = 3;
//    const ADD_INS_7500 = 4;
//    const ADD_INS_10000 = 5;
    const ADD_RECORDED = 6;
//    const ADD_TRACKED_SIGNATURE = 12;
//    const ADD_SMS = 13;
//    const ADD_EMAIL = 14;
//    const ADD_SMS_AND_EMAIL = 16;
//    const ADD_LOCAL_COLLECT = 22;
//    const ADD_SAT_GUARANTED = 24;
//    const ADD_INS_750 = 11;

    protected function setServiceByCourierOperatorUniqId($courier_operator_uniq_id)
    {
        switch($courier_operator_uniq_id)
        {
            case CourierOperator::UNIQID_ROYALMAIL_24:
                $this->_service = self::SERVICE_24;
                $this->_serviceCode = self::SERVICE_CODE_24;
                $this->_shipmentType = self::SHIPMENT_TYPE_DELIVERY;
                break;
            case CourierOperator::UNIQID_ROYALMAIL_48:
                $this->_service = self::SERVICE_48;
                $this->_serviceCode = self::SERVICE_CODE_48;
                $this->_shipmentType = self::SHIPMENT_TYPE_DELIVERY;
                break;
            case CourierOperator::UNIQID_ROYALMAIL_TRACKED_RETURN_48:
                $this->_service = self::SERVICE_TRACKED_RETURN_48;
                $this->_serviceCode = self::SERVICE_CODE_TSS;
                $this->_shipmentType = self::SHIPMENT_TYPE_RETURN;
                break;
            default:
                throw new Exception('Unknown Royal Mail operator!');
        }
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $operator_uniq_id, $returnError = false)
    {

        $model = new self;
        $model->setServiceByCourierOperatorUniqId($operator_uniq_id);

        $model->_receiverAddressModel = $to;

        $model->weight = $courierInternal->courier->getWeight(true) * 1000; // in g
        $model->ref = $courierInternal->courier->local_id;

        // @ 19.02.2019 - special rule
        if(in_array($courierInternal->courier->user_id, [2537]))
            $model->ref = $courierInternal->courier->package_content;

        // @12.04.2017
        // Special rule for user YesSport (#46) - put content on label
        // @29.01.2017
        //        // Special rule for user MIFI (#1158) - put content on label
        // @23.11.2018
        //        // Special rule for user TRENA (#100) - put content on label
        if(in_array($courierInternal->courier->user_id, [46,1158, 100]))
            $model->ref = $courierInternal->courier->package_content;


        $model->number_of_items = $courierInternal->courier->packages_number;

        $model->content = $courierInternal->courier->package_content;
//

//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_1000))
//            $model->_additions[] = self::ADD_INS_1000;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_2500))
//            $model->_additions[] = self::ADD_INS_2500;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_5000))
//            $model->_additions[] = self::ADD_INS_5000;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_7500))
//            $model->_additions[] = self::ADD_INS_7500;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_10000))
//            $model->_additions[] = self::ADD_INS_10000;
        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_RECORDED))
            $model->_additions[] = self::ADD_RECORDED;
        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_SIGNATURE))
            $model->signature = true;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_SMS))
//            $model->_additions[] = self::ADD_SMS;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_EMAIL))
//            $model->_additions[] = self::ADD_EMAIL;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_SMS_AND_EMAIL))
//            $model->_additions[] = self::ADD_SMS_AND_EMAIL;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_LOCAL_COLLECT))
//            $model->_additions[] = self::ADD_LOCAL_COLLECT;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_SAT_GUARANTED))
//            $model->_additions[] = self::ADD_SAT_GUARANTED;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_750))
//            $model->_additions[] = self::ADD_INS_750;

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {

        $to = $courierTypeU->courier->receiverAddressData;

        $model = new self;

        switch($courierTypeU->courierUOperator->uniq_id)
        {
            case CourierUOperator::UNIQID_ROYALMAIL_24:
                $model->_service = self::SERVICE_24;
                $model->_serviceCode = self::SERVICE_CODE_24;
                $model->_shipmentType = self::SHIPMENT_TYPE_DELIVERY;
                break;
            case CourierUOperator::UNIQID_ROYALMAIL_48:
                $model->_service = self::SERVICE_48;
                $model->_serviceCode = self::SERVICE_CODE_48;
                $model->_shipmentType = self::SHIPMENT_TYPE_DELIVERY;
                break;
            case CourierUOperator::UNIQID_ROYALMAIL_TRACKED_RETURN_48:
                $model->_service = self::SERVICE_TRACKED_RETURN_48;
                $model->_serviceCode = self::SERVICE_CODE_TSS;
                $model->_shipmentType = self::SHIPMENT_TYPE_RETURN;
                break;
            default:
                throw new Exception('Unknown Royal Mail operator!');
        }
        $model->_receiverAddressModel = $to;

        $model->weight = $courierTypeU->courier->package_weight * 1000; // in g
        $model->ref = $courierTypeU->courier->local_id;

        // @ 19.02.2019 - special rule
        if(in_array($courierTypeU->courier->user_id, [2537]))
            $model->ref = $courierTypeU->courier->package_content;

        // @12.04.2017
        // Special rule for user YesSport (#46) - put content on label
        // @29.01.2017
        // Special rule for user MIFI (#1158) - put content on label
        // @23.11.2018
        //        // Special rule for user TRENA (#100) - put content on label
        if(in_array($courierTypeU->courier->user_id, [46,1158, 100]))
            $model->ref = $courierTypeU->courier->package_content;

        $model->number_of_items = $courierTypeU->courier->packages_number;

//
        $model->content = $courierTypeU->courier->package_content;


//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_1000))
//            $model->_additions[] = self::ADD_INS_1000;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_2500))
//            $model->_additions[] = self::ADD_INS_2500;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_5000))
//            $model->_additions[] = self::ADD_INS_5000;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_7500))
//            $model->_additions[] = self::ADD_INS_7500;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_10000))
//            $model->_additions[] = self::ADD_INS_10000;
        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_ROYALMAIL_SIGNATURE))
            $model->signature = true;
        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_ROYALMAIL_RECORDED))
            $model->_additions[] = self::ADD_RECORDED;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_SMS))
//            $model->_additions[] = self::ADD_SMS;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_EMAIL))
//            $model->_additions[] = self::ADD_EMAIL;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_SMS_AND_EMAIL))
//            $model->_additions[] = self::ADD_SMS_AND_EMAIL;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_LOCAL_COLLECT))
//            $model->_additions[] = self::ADD_LOCAL_COLLECT;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_SAT_GUARANTED))
//            $model->_additions[] = self::ADD_SAT_GUARANTED;
//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_ROYAL_MAIL_INS_750))
//            $model->_additions[] = self::ADD_INS_750;

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierReturn(CourierTypeReturn $courierTypeReturn, AddressData $to, $returnError = false)
    {

        $model = new self;

        $model->_service = self::SERVICE_TRACKED_RETURN_48;
        $model->_serviceCode = self::SERVICE_CODE_TSS;
        $model->_shipmentType = self::SHIPMENT_TYPE_RETURN;

        $model->_receiverAddressModel = $to;

        $model->weight = $courierTypeReturn->courier->package_weight * 1000; // in g
        $model->ref = $courierTypeReturn->courier->local_id;

        $model->number_of_items = 1;

        $model->content = $courierTypeReturn->courier->package_content;

        return $model->_createShipment($returnError);
    }


    public static function orderForPostal(Postal $postal)
    {
        $to = $postal->receiverAddressData;

        $model = new self;

        $model->_service = self::SERVICE_48;
        $model->_serviceCode = self::SERVICE_CODE_48;
        $model->_shipmentType = self::SHIPMENT_TYPE_DELIVERY;

        $model->_receiverAddressModel = $to;

        $model->weight = $postal->weight;
        $model->ref = $postal->local_id;

        // @ 19.02.2019 - special rule
        if(in_array($postal->user_id, [2537]))
            $model->ref = $postal->content;

        $model->number_of_items = 1;

        $model->content = $postal->content;

        return array_pop($model->_createShipment(true)); // postal requires singe item in return, not array
    }

    protected static function _addressLinesConverter($addressLine1, $addressLine2, $getLineNo)
    {
        if(mb_strlen($addressLine1) <= 35 && mb_strlen($addressLine2) <= 35)
        {
            if($getLineNo == 0)
                return $addressLine1;
            else if($getLineNo == 1)
                return $addressLine2;
            else if($getLineNo == 2)
                return '';

        } else {
            $address = trim($addressLine1) . ' ' . trim($addressLine2);
            $address = trim($address);

            $newAddress = [];
            $newAddress[0] = mb_substr($address, 0, 35);
            $newAddress[1] = mb_substr($address, 35, 35);
            $newAddress[2] = mb_substr($address, 70, 35);

            return trim($newAddress[$getLineNo]);
        }
    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    public function _createShipment($returnError)
    {

        $apiURL = self::URL;
        $dateTime = date('Y-m-d\TH:i:s');
        $applicationId = "RMG-API-G-01";
        $transactionId = rand(9999,999999999);
        $shippingDate = gmdate('Y-m-d');
        $shipmentType = $this->_shipmentType;
        $serviceOccurrence = "1";
        $serviceType = $this->_service;

        $serviceCode = $this->_serviceCode;
        $serviceFormat = "P";

        $recipientContactName = $recipientComplementaryName = '';

        if($this->_shipmentType == self::SHIPMENT_TYPE_RETURN)
        {

            $returnAddressModel = new AddressData;
            $returnAddressModel->name = 'Mail Options LTD';
            $returnAddressModel->company = '';
            $returnAddressModel->address_line_1 = 'Unit 41, Waterside Trading Centre';
            $returnAddressModel->address_line_2 = '';
            $returnAddressModel->zip_code = 'W7 2QD';
            $returnAddressModel->city = 'London';

            $countryModel = CountryList::model()->findByPk(CountryList::COUNTRY_UK);
            $returnAddressModel->country0 = $countryModel;

            $this->_receiverAddressModel = $returnAddressModel;
        }

        if($this->_receiverAddressModel->name == $this->_receiverAddressModel->company)
        {
            $recipientContactName = $this->_receiverAddressModel->name;
        }
        else if($this->_receiverAddressModel->name != $this->_receiverAddressModel->company)
        {
            $recipientContactName = $this->_receiverAddressModel->name != '' ? $this->_receiverAddressModel->name : $this->_receiverAddressModel->company;
            $recipientComplementaryName = $this->_receiverAddressModel->name != '' ? $this->_receiverAddressModel->company : '';
        }

        $recipientAddressLine1 = self::_addressLinesConverter($this->_receiverAddressModel->address_line_1, $this->_receiverAddressModel->address_line_2, 0);
        $recipientAddressLine2 = self::_addressLinesConverter($this->_receiverAddressModel->address_line_1, $this->_receiverAddressModel->address_line_2, 1);
        $recipientAddressLine3 = self::_addressLinesConverter($this->_receiverAddressModel->address_line_1, $this->_receiverAddressModel->address_line_2, 2);
        $postTown = $this->_receiverAddressModel->city;
        $postcode = $this->_receiverAddressModel->zip_code;
        $countryCode = $this->_receiverAddressModel->country0->code;

        $recipientTel = $this->_receiverAddressModel->tel;
        $recipientEmail = $this->_receiverAddressModel->email;

        $noOfItems = $this->number_of_items;
        $unitOfMeasure = "g";

        $weightValue = $this->weight;
        $sendersReference = $this->ref;



        $clientId = self::CLIENT_ID;
        $clientSecret = self::CLIENT_SECRET;
        $username = self::USERNAME;
        $password = self::PASSWORD;

        $creationDate = gmdate('Y-m-d\TH:i:s\Z');
        $nonce = mt_rand();
        $nonce_date_pwd = $nonce.$creationDate.base64_encode(sha1($password, TRUE));
        $passwordDigest = base64_encode(sha1($nonce_date_pwd, TRUE));
        $encodedNonce = base64_encode($nonce);

        $curl = curl_init();

        $post = "<soapenv:Envelope xmlns:oas=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v1=\"http://www.royalmailgroup.com/integration/core/V1\" xmlns:v2=\"http://www.royalmailgroup.com/api/ship/V2\">
   <soapenv:Header>
      <wsse:Security xmlns:wsse = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">
         <wsse:UsernameToken>
            <wsse:Username>$username</wsse:Username>
            <wsse:Password Type = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">$passwordDigest</wsse:Password>
            <wsse:Nonce EncodingType = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">$encodedNonce</wsse:Nonce>
            <wsu:Created>$creationDate</wsu:Created>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <v2:createShipmentRequest>
         <v2:integrationHeader>
            <v1:dateTime>$dateTime</v1:dateTime>
            <v1:version>2</v1:version>
            <v1:identification>
               <v1:applicationId>$applicationId</v1:applicationId>
               <v1:transactionId>$transactionId</v1:transactionId>
            </v1:identification>
         </v2:integrationHeader>
         <v2:requestedShipment>
            <v2:shipmentType>
               <code>$shipmentType</code>
            </v2:shipmentType>
            <v2:serviceOccurrence>$serviceOccurrence</v2:serviceOccurrence>
            <v2:serviceType>
               <code>$serviceType</code>
            </v2:serviceType>
            <v2:serviceOffering>
               <serviceOfferingCode>
                  <code>$serviceCode</code>
               </serviceOfferingCode>
            </v2:serviceOffering>
            <v2:serviceFormat>
               <serviceFormatCode>
                  <code>$serviceFormat</code>
               </serviceFormatCode>
            </v2:serviceFormat>";


        if(sizeof($this->_additions)):

            $post .= "<v2:serviceEnhancements>";
            foreach($this->_additions AS $additionId)
                $post .= "<v2:enhancementType><serviceEnhancementCode><code>$additionId</code></serviceEnhancementCode></v2:enhancementType>";

            $post .= "</v2:serviceEnhancements>";
        endif;


        $true = true;
        if($this->signature)
            $post .= "<v2:signature>$true</v2:signature>";

        $post .= "<v2:shippingDate>".self::_cdata($shippingDate)."</v2:shippingDate>
            <v2:recipientContact>
               <v2:name>".self::_cdata($recipientContactName)."</v2:name>
               <v2:complementaryName>".self::_cdata($recipientComplementaryName)."</v2:complementaryName>";

        if($recipientTel !='')
            $post .= "<v2:telephoneNumber><telephoneNumber>".self::_cdata($recipientTel)."</telephoneNumber></v2:telephoneNumber>";

        if($recipientEmail != '')
            $post .= "<v2:electronicAddress><electronicAddress>".self::_cdata($recipientEmail)."</electronicAddress>
               </v2:electronicAddress>";

        $post .= "</v2:recipientContact>
            <v2:recipientAddress>
               <addressLine1>".self::_cdata($recipientAddressLine1)."</addressLine1>";

        if($recipientAddressLine2 != '')
            $post .= "<addressLine2>".self::_cdata($recipientAddressLine2)."</addressLine2>";

        if($recipientAddressLine3 != '')
            $post .= "<addressLine3>".self::_cdata($recipientAddressLine3)."</addressLine3>";

        $post .= "<postTown>".self::_cdata($postTown)."</postTown>
               <postcode>".self::_cdata($postcode)."</postcode>
               <country>
                  <countryCode>
                     <code>".self::_cdata($countryCode)."</code>
                  </countryCode>
               </country>
            </v2:recipientAddress>
            <v2:items>
               <v2:item>
                  <v2:numberOfItems>$noOfItems</v2:numberOfItems>
                  <v2:weight>
                     <unitOfMeasure>
                        <unitOfMeasureCode>
                           <code>$unitOfMeasure</code>
                        </unitOfMeasureCode>
                     </unitOfMeasure>
                     <value>".self::_cdata($weightValue)."</value>
                  </v2:weight>
               </v2:item>
            </v2:items>         
            <v2:departmentReference>".self::_cdata($sendersReference)."</v2:departmentReference>
            <v2:customerReference>".self::_cdata($sendersReference)."</v2:customerReference>
            <v2:senderReference>".self::_cdata($sendersReference)."</v2:senderReference>
         </v2:requestedShipment>
      </v2:createShipmentRequest>
   </soapenv:Body>
</soapenv:Envelope>";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
                "accept: application/soap+xml",
                "accept-encoding: gzip,deflate",
                "connection: keep-alive",
                "content-type: text/xml",
                "host: api.royalmail.net",
                "soapaction: \"createShipment\"",
                "x-ibm-client-id: $clientId",
                "x-ibm-client-secret: $clientSecret"
            ),
        ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);


        MyDump::dump('royalmail.txt', 'CREATE SHIPMENT REQUEST: '.print_r($post,1));
        MyDump::dump('royalmail.txt', 'CREATE SHIPMENT ERROR: '.print_r($err,1));
        MyDump::dump('royalmail.txt', 'CREATE SHIPMENT RESPONSE: '.print_r($response,1));
        if(!$response)
        {
            if ($returnError == true) {

                $return = [0 => array(
                    'error' => $err,
                )];
                return $return;
            } else {
                return false;
            }
        }

        $response = str_ireplace(['soapenv:', 'SOAP-ENV:', 'fault:', 'xmlns:'], '', $response);
        $xml = @simplexml_load_string($response) or die('x');

        if(!sizeof($xml) OR isset($xml->Body->Fault) OR isset($xml->Body->createShipmentResponse->integrationFooter->errors))
        {
            if(!sizeof($xml))
                $text = 'Could not read XML response!';
            else if(isset($xml->Body->Fault))
                $text = (string) $xml->Body->Fault->detail->exceptionDetails->exceptionText;
            else
                $text = (string) $xml->Body->createShipmentResponse->integrationFooter->errors->error[0]->errorCode.' / '. (string) $xml->Body->createShipmentResponse->integrationFooter->errors->error[0]->errorDescription;


            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $text,
                )];
                return $return;
            } else {
                return false;
            }

        }

        $shipmentsNo = (array) $xml->Body->createShipmentResponse->completedShipmentInfo->allCompletedShipments->completedShipments->shipments->shipmentNumber;

        if($shipmentsNo == '' OR !sizeof($shipmentsNo))        {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'Shipment Number not created!',
                )];

                return $return;
            } else {
                return false;
            }
        }

        if(!is_array($shipmentsNo))
            $shipmentsNo = [ $shipmentsNo ];


        $return = [];
        foreach($shipmentsNo AS $shipmentNo)
        {
            if($shipmentNo == '')
            {
                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => 'Shipment Number not created!',
                    )];
                    return $return;
                } else {
                    return false;
                }
            }

            $dateTime = date('Y-m-d\TH:i:s');
            $creationDate = gmdate('Y-m-d\TH:i:s\Z');
            $nonce = mt_rand();
            $nonce_date_pwd = $nonce.$creationDate.base64_encode(sha1($password, TRUE));
            $passwordDigest = base64_encode(sha1($nonce_date_pwd, TRUE));
            $encodedNonce = base64_encode($nonce);

            $curl = curl_init();

            $post = "<soapenv:Envelope xmlns:oas=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v1=\"http://www.royalmailgroup.com/integration/core/V1\" xmlns:v2=\"http://www.royalmailgroup.com/api/ship/V2\">
   <soapenv:Header>
      <wsse:Security xmlns:wsse = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">
         <wsse:UsernameToken>
            <wsse:Username>$username</wsse:Username>
            <wsse:Password Type = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">$passwordDigest</wsse:Password>
            <wsse:Nonce EncodingType = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">$encodedNonce</wsse:Nonce>
            <wsu:Created>$creationDate</wsu:Created>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
 <soapenv:Body>
<v2:printLabelRequest>
<v2:integrationHeader>
            <v1:dateTime>$dateTime</v1:dateTime>
            <v1:version>2</v1:version>
            <v1:identification>
               <v1:applicationId>$applicationId</v1:applicationId>
               <v1:transactionId>$transactionId</v1:transactionId>
            </v1:identification>
</v2:integrationHeader>
<v2:shipmentNumber>$shipmentNo</v2:shipmentNumber>
<v2:localisedAddress>
<v2:recipientContact>
<v2:name>".self::_cdata($recipientContactName)."</v2:name>
<v2:complementaryName>".self::_cdata($recipientContactName)."</v2:complementaryName>";

            if($recipientTel !='')
                $post .= "<v2:telephoneNumber><telephoneNumber>".self::_cdata($recipientTel)."</telephoneNumber></v2:telephoneNumber>";

            if($recipientEmail != '')
                $post .= "<v2:electronicAddress><electronicAddress>".self::_cdata($recipientEmail)."</electronicAddress>
               </v2:electronicAddress>";

            $post .= "</v2:recipientContact>
            <v2:recipientAddress>
               <addressLine1>".self::_cdata($recipientAddressLine1)."</addressLine1>";

            if($recipientAddressLine2 != '')
                $post .= "<addressLine2>".self::_cdata($recipientAddressLine2)."</addressLine2>";

            if($recipientAddressLine3 != '')
                $post .= "<addressLine3>".self::_cdata($recipientAddressLine3)."</addressLine3>";


            $post .="<postTown>".self::_cdata($postTown)."</postTown>
<postcode>".self::_cdata($postcode)."</postcode>
</v2:recipientAddress>
</v2:localisedAddress>
</v2:printLabelRequest>
</soapenv:Body>
</soapenv:Envelope>";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiURL,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $post,
                CURLOPT_HTTPHEADER => array(
                    "accept: application/soap+xml",
                    "accept-encoding: gzip,deflate",
                    "connection: keep-alive",
                    "content-type: text/xml",
                    "host: api.royalmail.net",
                    "soapaction: \"printLabel\"",
                    "x-ibm-client-id: $clientId",
                    "x-ibm-client-secret: $clientSecret"
                ),
            ));

            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);



            MyDump::dump('royalmail.txt', 'PRINT LABEL REQUEST: '.print_r($post,1));
            MyDump::dump('royalmail.txt', 'PRINT LABEL ERROR: '.print_r($err,1));
            MyDump::dump('royalmail.txt', 'PRINT LABEL RESPONSE: '.print_r($response,1));
            if(!$response)
            {
                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => $err,
                    )];
                    return $return;
                } else {
                    return false;
                }
            }

            $response = str_ireplace(['soapenv:', 'SOAP-ENV:'], '', $response);
            $xml = @simplexml_load_string($response) or die('x');

            if(!sizeof($xml) OR isset($xml->Body->Fault) OR isset($xml->Body->printLabelResponse->integrationFooter->errors))
            {
                if(!sizeof($xml))
                    $text = 'Could not read XML response!';
                else if(isset($xml->Body->Fault))
                    $text = (string) $xml->Body->Fault->faultstring;
                else
                    $text = (string) $xml->Body->printLabelResponse->integrationFooter->errors->error[0]->errorCode.' / '. (string) $xml->Body->printLabelResponse->integrationFooter->errors->error[0]->errorDescription;


                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => $text,
                    )];
                    return $return;
                } else {
                    return false;
                }

            }


            $label = (string) $xml->Body->printLabelResponse->label;

            if($label == '')
            {
                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => 'Could not find label!',
                    )];
                    return $return;
                } else {
                    return false;
                }
            }

            $return[] = [
                'status' => true,
                'label' => $label,
                'track_id' => $shipmentNo,
            ];
        }

        $this->manifest();

        return $return;
    }


    protected function manifest()
    {

        $apiURL = self::URL;
        $applicationId = "RMG-API-G-01";
        $transactionId = rand(9999,999999999);

        $clientId = self::CLIENT_ID;
        $clientSecret = self::CLIENT_SECRET;
        $username = self::USERNAME;
        $password = self::PASSWORD;


        $dateTime = date('Y-m-d\TH:i:s');
        $creationDate = gmdate('Y-m-d\TH:i:s\Z');
        $nonce = mt_rand();
        $nonce_date_pwd = $nonce.$creationDate.base64_encode(sha1($password, TRUE));
        $passwordDigest = base64_encode(sha1($nonce_date_pwd, TRUE));
        $encodedNonce = base64_encode($nonce);

        $curl = curl_init();

        $post = "<soapenv:Envelope xmlns:oas=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v1=\"http://www.royalmailgroup.com/integration/core/V1\" xmlns:v2=\"http://www.royalmailgroup.com/api/ship/V2\">
   <soapenv:Header>
      <wsse:Security xmlns:wsse = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">
         <wsse:UsernameToken>
            <wsse:Username>$username</wsse:Username>
            <wsse:Password Type = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">$passwordDigest</wsse:Password>
            <wsse:Nonce EncodingType = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">$encodedNonce</wsse:Nonce>
            <wsu:Created>$creationDate</wsu:Created>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
 <soapenv:Body>
<v2:createManifestRequest>
<v2:integrationHeader>
            <v1:dateTime>$dateTime</v1:dateTime>
            <v1:version>2</v1:version>
            <v1:identification>
               <v1:applicationId>$applicationId</v1:applicationId>
               <v1:transactionId>$transactionId</v1:transactionId>
            </v1:identification>
</v2:integrationHeader>
</v2:createManifestRequest>
</soapenv:Body>
</soapenv:Envelope>";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
                "accept: application/soap+xml",
                "accept-encoding: gzip,deflate",
                "connection: keep-alive",
                "content-type: text/xml",
                "host: api.royalmail.net",
                "soapaction: \"createManifest\"",
                "x-ibm-client-id: $clientId",
                "x-ibm-client-secret: $clientSecret"
            ),
        ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        MyDump::dump('royalmail.txt', 'MANIFEST - REQUEST: '.print_r($post,1));
        MyDump::dump('royalmail.txt', 'MANIFEST - RESPONSE: '.print_r($response,1));
        MyDump::dump('royalmail.txt', 'MANIFEST - ERROR: '.print_r($err,1));


        curl_close($curl);
    }

    public static function track($no)
    {
        $apiURL = "https://api.royalmail.net/mailpieces/v2/$no/events";

        $clientId = "7a167bc7-f16f-4f10-bd42-4194044dacaa";
        $clientSecret = "qE8nH8lU3eB3gD1gG4iR2vP6eR6wW4kL5sF8mA1mM7fW0fB0wQ";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiURL,
            CURLOPT_RETURNTRANSFER => true,
//        CURLOPT_ENCODING => "",
//        CURLOPT_MAXREDIRS => 10,
//        CURLOPT_TIMEOUT => 30,
//        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//        CURLOPT_CUSTOMREQUEST => "POST",

            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "x-ibm-client-id: $clientId",
                "x-ibm-client-secret: $clientSecret",
                "Host: api.royalmail.net",
                "Connection: Keep-Alive",
            ),
        ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        $statuses = [];
        if ($err) {
            return false;
        } else {
            $response = json_decode($response);
            if($response->errors == '' OR (is_array($response->errors) && !sizeof($response->errors))) {
                if (is_array($response->mailPieces->events))
                    foreach ($response->mailPieces->events AS $item) {

                        if($item->eventCode == 'EVAIP') // Sender preparing item
                            continue;

                        $statuses[] = [
                            'name' => $item->eventName,
                            'date' => str_replace(['T', 'Z'], [' ', ''], $item->eventDateTime),
                            'location' => $item->locationName,
                        ];
                    }
            }
            return $statuses;
        }

        // OLD ENDPOINT

//        $apiURL = "https://api.royalmail.net/mailPieces/$no/history";
//
//        $clientId = "7a167bc7-f16f-4f10-bd42-4194044dacaa";
//        $clientSecret = "qE8nH8lU3eB3gD1gG4iR2vP6eR6wW4kL5sF8mA1mM7fW0fB0wQ";
//
//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $apiURL,
//            CURLOPT_RETURNTRANSFER => true,
////        CURLOPT_ENCODING => "",
////        CURLOPT_MAXREDIRS => 10,
////        CURLOPT_TIMEOUT => 30,
////        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
////        CURLOPT_CUSTOMREQUEST => "POST",
//
//            CURLOPT_HTTPHEADER => array(
//                "accept: application/json",
//                "x-ibm-client-id: $clientId",
//                "x-ibm-client-secret: $clientSecret",
//                "Host: api.royalmail.net",
//                "Connection: Keep-Alive",
//            ),
//        ));
//
//
//
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
//
//        $response = curl_exec($curl);
//        $err = curl_error($curl);
//
//        curl_close($curl);
//        $statuses = [];
//        if ($err) {
//            return false;
//        } else {
//            $response = json_decode($response);
//
//
//            if($response->errors == '' OR (is_array($response->errors) && !sizeof($response->errors))) {
//                if (is_array($response->trackDetail))
//                    foreach ($response->trackDetail AS $item) {
//
//                        $name = sizeof($item->messages) ? $item->messages[0]->detail : $item->status;
//
//                        if($name == 'We\'re expecting it')
//                            continue;
//
//                        $statuses[] = [
//                            'name' => $name,
//                            'date' => $item->trackDate . ' ' . $item->trackTime,
//                            'location' => $item->trackPoint,
//                        ];
//                    }
//            }
//            return $statuses;
//        }
    }


    public static function getOffices($zip_code)
    {

        $zip_code = preg_replace("/[^A-Za-z0-9]/", '', $zip_code);

        $CACHE_NAME = 'ROYAL_MAIL_OFFICES_'.$zip_code;

        $data = Yii::app()->cache->get($CACHE_NAME);

        if($data === false) {

            $apiURL = "https://api.royalmail.net/deliveryOffices/?postcode=" . $zip_code;

            $clientId = "7a167bc7-f16f-4f10-bd42-4194044dacaa";
            $clientSecret = "qE8nH8lU3eB3gD1gG4iR2vP6eR6wW4kL5sF8mA1mM7fW0fB0wQ";

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiURL,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "GET",

                CURLOPT_HTTPHEADER => array(
                    "accept: application/json",
                    "x-ibm-client-id: $clientId",
                    "x-ibm-client-secret: $clientSecret",
                    "Host: api.royalmail.net",
                    "Connection: Keep-Alive",
                    "Content-Type: application/json",
                ),
            ));


            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            $data = -1;
            if ($httpCode == 200) {
                $response = json_decode($response);

                $office = $response->deliveryOffice;

                $data = [];
                $data[] = [
                    'id' => $office->id,
                    'lat' => $office->locationDetails->latitude,
                    'lng' => $office->locationDetails->longitude,
                    'address' => $office->officeDetails->address1 . "<br/>" . $office->officeDetails->address2 . "<br/>" . $office->officeDetails->address3 . "<br/>" . $office->officeDetails->address4 . "<br/>" . $office->officeDetails->postcode,
                    'details' => NULL,
                    'hours' => NULL,
                    'name' => $office->officeDetails->name,
                ];


            }

            Yii::app()->cache->set($CACHE_NAME, $data, 60*60*24);

        }

        if($data == -1)
            $data = false;

        return $data;
    }

}

