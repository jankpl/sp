<?php
/* @var $this HybridMailController */
/* @var $model HybridMail */
/* @var $modelHybridMailAttachments HybridMailAttachment[] */
/* @var $form CActiveForm */
/* @var $modelHybridMailAddition HybridMailAddition[] */
?>

<div class="form" style="">

    <h2><?php echo Yii::t('hybridMail','HybridMail');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '5/6')); ?> | <?php echo Yii::t('app','Podsumowanie'); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'hybrid-mail-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
//    foreach($modelHybridMailAddition AS $item):
//        echo $item->name;
//
//    endforeach;
    ?>

    <h3><?php echo Yii::t('hybridMail','HybridMail');?></h3>
    <h4><?php echo Yii::t('hybridMail','Podsumowanie');?></h4>
    <table class="list hLeft" style="width: 500px;">
        <tr>
            <td><?php echo Yii::t('hybridMail','Łącznie stron na odbiorcę');?></td>
            <td><?php echo $model->getTotalPages(); ?></td>
        </tr>
        <tr>
            <td><?php echo Yii::t('hybridMail','Odbiorców');?></td>
            <td><?php echo S_Useful::sizeof($model->hybridMailReceivers); ?></td>
        </tr>
        <tr>
            <td><?php echo Yii::t('hybridMail','Cena za dodatki');?></td>
            <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($model->totalPriceFroAdditions($currency), true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($model->totalPriceFroAdditions($currency)));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','netto/brutto');?></td>
        </tr>
        <tr>
            <td><?php echo Yii::t('hybridMail','Łączna cena');?></td>
            <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price_total, true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price_total));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','netto/brutto');?></td>
        </tr>


    </table>

    <h4><?php echo Yii::t('hybridMail','Dodatki');?> <?php echo TbHtml::submitButton(Yii::t('app','edytuj'),array('name'=>'step4', 'style' => 'display: inline')); ?></h4>
    <table class="list hTop" style="width: 500px;">
        <tr>
            <td><?php echo Yii::t('hybridMail','Nazwa');?></td>
            <td><?php echo Yii::t('hybridMail','Cena za jednego odbiorcę');?></td>
        </tr>
        <?php

        /* @var $item HybridMailAdditionList */
        if(!S_Useful::sizeof($model->listAdditions(true)))
            echo '<tr><td colspan="2">'.Yii::t('app','brak').'</td></tr>';
        else
            foreach($model->listAdditions(true) AS $item): ?>
                <tr>
                    <td><?php echo $item->getClientTitle(); ?></td>
                    <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->calculatePriceByPages($model->getTotalPages(), $currency, true)));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->calculatePriceByPages($model->getTotalPages(), $currency)));?> <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','netto/brutto');?></td>

                </tr>

            <?php endforeach; ?>
    </table>

    <h3><?php echo Yii::t('hybridMail','Zawartość');?>  <?php echo TbHtml::submitButton(Yii::t('app','edytuj'),array('name'=>'step1', 'style' => 'display: inline')); ?></h3>


    <a href="<?php echo Yii::app()->createUrl('/hybridMail/hybridMail/getTmpFile', array('urlData' => $model->file_hash, 'e' => $model->_file_ext));?>" target="_blank" class="btn btn-primary"><?php echo Yii::t('hm', 'Podejrzyj plik');?></a>



    <h3><?php echo Yii::t('hybridMail','Nadawca');?> <?php echo TbHtml::submitButton(Yii::t('app','edytuj'),array('name'=>'step2', 'style' => 'display: inline')); ?></h3>

    <?php

    $this->renderPartial('_addressData/_listInTable',
        array('model' => $model->senderAddressData,
        ));

    ?>

    <h3><?php echo Yii::t('hybridMail','Odbiorcy');?> (<?php echo S_Useful::sizeof($model->hybridMailReceivers); ?>) <?php echo TbHtml::submitButton(Yii::t('app','edytuj'),array('name'=>'step3', 'style' => 'display: inline')); ?></h3>
    <?php $i = 1; ?>
    <?php foreach($model->hybridMailReceivers AS $receiver):?>

        <h4><?php echo Yii::t('hybridMail','Odbiorca nr {number}',array('{number}' => $i++));?></h4>

        <?php

        $this->renderPartial('_addressData/_listInTable',
            array('model' => $receiver->addressData,
            ));

        ?>

    <?php endforeach; ?>
<br/>
<br/>
    <div class="row">
        <?php echo $form->labelEx($model,'regulations'); ?>
        <?php echo $form->checkbox($model, 'regulations'); ?>
        <?php echo $form->error($model,'regulations'); ?>
    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step4', 'class' => 'btn-small'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Zapisz i idź do kasy'),array('name'=>'finish', 'class' => 'btn-primary'));
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p><?php echo Yii::t('hybridMail','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>
        <p><?php echo Yii::t('hybridMail','Będzie można je opłacić od razu lub też zrobić to w późniejszym terminie');?>.</p>
    </div>
</div><!-- form -->