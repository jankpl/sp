<?php

class SecuredMailClient extends GlobalOperator
{
    const URL = GLOBAL_CONFIG::SECURED_MAIL_URL;
    const URL_LABEL = GLOBAL_CONFIG::SECURED_MAIL_URL_LABEL;
    const URL_TRACKING = GLOBAL_CONFIG::SECURED_MAIL_URL_TRACKING;

    const USER = GLOBAL_CONFIG::SECURED_MAIL_USER;
    const PASSWORD = GLOBAL_CONFIG::SECURED_MAIL_PASSWORD;
    const CLIENT_NAME = GLOBAL_CONFIG::SECURED_MAIL_CLIENT_NAME;
    const CARRIER_NAME = GLOBAL_CONFIG::SECURED_MAIL_CARRIER_NAME;

    public $packages_number = 1;

    public $local_id;
    public $package_value;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_tel;
    public $sender_email;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $package_content;

    public $insurance;

    public $pod;

    const POD_SIGNED_FOR = 'S';
    const POD_TRACKED = 'T';
    const POD_TRACKED_SIGNED_FOR = 'X';
//    const POD_UNTRACKED = 'N';

    public $sms_notification = false;
    public $email_notification = false;
    public $saturday_delivery = false;


    public static function getAdditions(Courier $courier)
    {

        $CACHE_NAME = 'SECURED_MAIL_ADDITIONS_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_TRACKED_SIGNED_FOR;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_TRACKED;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_SIGNED_FOR;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_SATURDAY_DELIVERY;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_INSURANCE;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_SMS_NOTIFICATION;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_EMAIL_NOTIFICATION;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

//    public static function createShipmentByTest()
//    {
//        $model = new self;
//
//
//        $model->receiver_name = 'Jan Nowak';
//        $model->receiver_company = 'Nowak Inc';
//        $model->receiver_zip_code = 'W1T 1RJ';
//        $model->receiver_city = 'London';
//        $model->receiver_address1 = '5 BEDE CRESCENT';
//        $model->receiver_tel = '111222333';
//        $model->receiver_country = 'GB';
//
//        $model->sender_name = 'Jan Nowak';
//        $model->sender_company = 'Nowak Inc';
//        $model->sender_zip_code = 'TW4 6JS';
//        $model->sender_city = 'Hounslow';
//        $model->sender_address1 = '5 Glategny Esplanade';
//        $model->sender_tel = '111222333';
//        $model->sender_country = 'GB';
//
////        $model->receiver_name = 'Jan Nowak';
////        $model->receiver_company = 'Nowak Inc';
////        $model->receiver_zip_code = 'KW1 5NU';
////        $model->receiver_city = 'Wick';
////        $model->receiver_address1 = 'South Road';
////        $model->receiver_tel = '111222333';
////        $model->receiver_country = 'GB';
//
//
//        $model->package_weight = 1;
//        $model->packages_number = 1;
//
//        $model->package_size_l = 2;
//        $model->package_size_d = 2;
//        $model->package_size_w = 2;
//
//        $model->package_content = 'Test';
//        $model->comments = 'Order #123';
//
//        $model->local_id = 'test133'.rand(999,99999);
//
//        $model->packages_number = 2;
//
//        return $model->createShipment(true);
//    }

    protected function createShipment($returnError = false)
    {
        $sender_address = trim($this->sender_address1.' '.$this->sender_address2);
        $sender_address1 = mb_substr($sender_address,0,30);
        $sender_address2 = mb_substr($sender_address,30,30);
        $sender_address3 = mb_substr($sender_address,60,30);

        $receiver_address = trim($this->receiver_address1.' '.$this->receiver_address2);
        $receiver_address1 = mb_substr($receiver_address,0,30);
        $receiver_address2 = mb_substr($receiver_address,30,30);
        $receiver_address3 = mb_substr($receiver_address,60,30);


        $data = new stdClass();

        $data->Authentication = new stdClass();
        $data->Authentication->Username = self::USER;
        $data->Authentication->Password = self::PASSWORD;

        $data->ItemDetails = new stdClass();
        $data->ItemDetails->ItemDetail = new stdClass();

        $data->ItemDetails->ItemDetail->CarrierName = self::CARRIER_NAME;
        $data->ItemDetails->ItemDetail->ClientName = self::CLIENT_NAME;
        $data->ItemDetails->ItemDetail->ClientItemReference = $this->local_id;

        $data->ItemDetails->ItemDetail->Phone = $this->receiver_tel;
        $data->ItemDetails->ItemDetail->Email = $this->receiver_email;
        $data->ItemDetails->ItemDetail->Email = $this->receiver_email;
        $data->ItemDetails->ItemDetail->RecipientName = $this->receiver_name;
        $data->ItemDetails->ItemDetail->RecipientCompany = $this->receiver_company;
        $data->ItemDetails->ItemDetail->RecipientAddress = new stdClass();
        $data->ItemDetails->ItemDetail->RecipientAddress->AddressLine1 = $receiver_address1;
        $data->ItemDetails->ItemDetail->RecipientAddress->AddressLine2 = $receiver_address2;
        $data->ItemDetails->ItemDetail->RecipientAddress->AddressLine3 = $receiver_address3;
        $data->ItemDetails->ItemDetail->RecipientAddress->AddressLine4 = $this->receiver_city;
        $data->ItemDetails->ItemDetail->RecipientAddress->PostCode = $this->receiver_zip_code;
        $data->ItemDetails->ItemDetail->RecipientAddress->CountryCode = $this->receiver_country;

        $data->ItemDetails->ItemDetail->SenderContactName = $this->sender_name;
        $data->ItemDetails->ItemDetail->SenderContactEmail = $this->sender_email;
        $data->ItemDetails->ItemDetail->SenderContactPhone = $this->sender_tel;
        $data->ItemDetails->ItemDetail->SenderAddress = new stdClass();
        $data->ItemDetails->ItemDetail->SenderAddress->AddressLine1 = $sender_address1;
        $data->ItemDetails->ItemDetail->SenderAddress->AddressLine2 = $sender_address2;
        $data->ItemDetails->ItemDetail->SenderAddress->AddressLine3 = $sender_address3;
        $data->ItemDetails->ItemDetail->SenderAddress->AddressLine4 = $this->sender_city;
        $data->ItemDetails->ItemDetail->SenderAddress->PostCode = $this->sender_zip_code;
        $data->ItemDetails->ItemDetail->SenderAddress->CountryCode = $this->sender_country;

        $data->ItemDetails->ItemDetail->Weight = $this->package_weight * 1000;

        if($this->pod)
            $data->ItemDetails->ItemDetail->ProofOfDelivery = $this->pod;

        $data->ItemDetails->ItemDetail->ValueCurrency = 'GBP';
        $data->ItemDetails->ItemDetail->ItemContent = new stdClass();
        $data->ItemDetails->ItemDetail->ItemContent->ItemDescription = $this->package_content;
        $data->ItemDetails->ItemDetail->ItemContent->ItemQuantity = 1;
        $data->ItemDetails->ItemDetail->ItemContent->ItemValue = $this->package_value;

        $data->ItemDetails->ItemDetail->ItemLength = $this->package_size_l * 10;
        $data->ItemDetails->ItemDetail->ItemHeight = $this->package_size_d * 10;
        $data->ItemDetails->ItemDetail->ItemWidth = $this->package_size_w * 10;

        if($this->insurance)
            $data->ItemDetails->ItemDetail->InsuranceValue = $this->package_value;

        if($this->email_notification)
            $data->ItemDetails->ItemDetail->DeclineEmailNotification = $this->receiver_email;

        if($this->sms_notification)
            $data->ItemDetails->ItemDetail->DeclineSmsNotification = $this->receiver_tel;

        if($this->saturday_delivery)
            $data->ItemDetails->ItemDetail->SaturdayDelivery = true;

        $data->ItemDetails->ItemDetail->PosterShipmentReference = $this->local_id;


        $request = new stdClass();
        $request->submitItemAdviceRequest = $data;

        $resp = $this->_soapOperation('SubmitItemAdvice', $request);

        if ($resp->success == true) {
            $return = [];

            $labelReference = $resp->data->SubmitItemAdviceResult->ItemDetailResponse->CarrierItemReference;

            $context = stream_context_create(array(
                'http' => array(
                    'header'  => "Authorization: Basic " . base64_encode(self::USER.':'.self::PASSWORD)
                )
            ));
            $label = @file_get_contents(self::URL_LABEL.$labelReference, false, $context);

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            $im->readImageBlob($label);
            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $im->stripImage();

            $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $labelReference);

            return $return;
        }
        else {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->error);
            } else {
                return false;
            }
        }
    }

    protected function _soapOperation($functionName, stdClass $data)
    {
        $mode = array
        (
            'soap_version' => SOAP_1_1,  // use soap 1.1 client
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
            'exceptions' => true
        );

        $return = new stdClass();
        $return->success = false;


        MyDump::dump('secured_mail.txt', 'REQUEST: '.print_r($data,1));

        try {

            $client = new SoapClient(self::URL, $mode);
            $resp = $client->__soapCall($functionName, [$functionName => $data]);

            MyDump::dump('secured_mail.txt', 'REQUEST (SOAP): '.print_r($client->__getLastRequest(),1));
            MyDump::dump('secured_mail.txt', 'RESPONSE: '.print_r($resp,1));

        } catch (Exception $E) {
            MyDump::dump('secured_mail.txt', 'REQUEST (SOAP): '.print_r($client->__getLastRequest(),1));
            MyDump::dump('secured_mail.txt', 'ERROR: '.print_r($E->getMessage(),1));

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;
            return $return;
        }

        if($resp->SubmitItemAdviceResult->ItemDetailResponse->ErrorMessage)
        {
            $return->error = $resp->SubmitItemAdviceResult->ItemDetailResponse->ErrorMessage;
            $return->errorCode = $resp->SubmitItemAdviceResult->ItemDetailResponse->ErrorMessage;
            $return->errorDesc = $resp->SubmitItemAdviceResult->ItemDetailResponse->ErrorMessage;
        } else {
            $return->success = true;
            $return->data = $resp;
        }




        return $return;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;
        $model->sender_name = $from->name != '' ? $from->name : $from->company;
        $model->sender_company = $from->name != '' ? $from->company : NULL;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_email = $from->fetchEmailAddress(false);
        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->name != '' ? $to->name : $to->company;
        $model->receiver_company = $to->name != '' ? $to->company : NULL;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(false);
        $model->receiver_country = $to->country0->code;

        $model->package_weight = $courierInternal->courier->getWeight(true);

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->local_id = $courierInternal->courier->local_id;

        $model->packages_number = 1;
        $model->package_value = $courierInternal->courier->getPackageValueConverted('GBP');

//        $model->business = $to->company != '' ? true : false;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_EMAIL_NOTIFICATION))
            $model->email_notification = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_SMS_NOTIFICATION))
            $model->sms_notification = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_INSURANCE))
            $model->insurance = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_SATURDAY_DELIVERY))
            $model->saturday_delivery = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_SIGNED_FOR))
            $model->pod = self::POD_SIGNED_FOR;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_TRACKED))
            $model->pod = self::POD_TRACKED;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_TRACKED_SIGNED_FOR))
            $model->pod = self::POD_TRACKED_SIGNED_FOR;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnError = false)
    {

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model = new self;

        $model->sender_name = $from->name != '' ? $from->name : $from->company;
        $model->sender_company = $from->name != '' ? $from->company : NULL;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_email = $from->fetchEmailAddress(false);
        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->name != '' ? $to->name : $to->company;
        $model->receiver_company = $to->name != '' ? $to->company : NULL;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(false);
        $model->receiver_country = $to->country0->code;

        $model->package_weight = $courierU->courier->getWeight(true);

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->local_id = $courierU->courier->local_id;

        $model->packages_number = 1;
        $model->package_value = $courierU->courier->getPackageValueConverted('GBP');

//        $model->business = $to->company != '' ? true : false;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_EMAIL_NOTIFICATION))
            $model->email_notification = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_SMS_NOTIFICATION))
            $model->sms_notification = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_INSURANCE))
            $model->insurance = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_SATURDAY_DELIVERY))
            $model->saturday_delivery = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_SIGNED_FOR))
            $model->pod = self::POD_SIGNED_FOR;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_TRACKED))
            $model->pod = self::POD_TRACKED;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_TRACKED_SIGNED_FOR))
            $model->pod = self::POD_TRACKED_SIGNED_FOR;


        return $model->createShipment($returnError);
    }




    public static function track($no, CourierLabelNew $courierLabelNew)
    {
//        $context = stream_context_create(array(
//            'http' => array(
//                'header'  => "Authorization: Basic " . base64_encode(self::USER.':'.self::PASSWORD)
//                          )
//        ));

        $zip_code = $courierLabelNew->addressDataTo->zip_code;
        $zip_code = str_replace(' ', '', $zip_code);

        $data = file_get_contents(self::URL_TRACKING.'shipmenttracking/'.$no.'?postcode='.$zip_code.'&audience=Public&requestauthentication=false');

        if($data)
            $data = json_decode($data);

        $statuses = new GlobalOperatorTtResponseCollection();
        foreach($data->TrackingEvents AS $item)
        {

            $date = substr($item->EventTime,0, 19);
            $date = str_replace('T', ' ', $date);

            $statuses->addItem($item->Description, $date, $item->Location);
        }

        return $statuses;
    }

}