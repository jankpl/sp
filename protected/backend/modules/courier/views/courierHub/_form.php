<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'courier-hub-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 64)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model, 'description', array('maxlength' => 256)); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->

    <h3>Dane adresowe HUBu:</h3>

    <?php


    $this->widget('application.backend.components.AddressDataFrom', array(
        'addressData' => $model->address,
        'form' => $form,
        'noEmail' => false,
    ));

    ?>


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->