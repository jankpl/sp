<?php

Yii::import('application.modules.courier.models._base.BaseCourierUAdditionListTr');

class CourierUAdditionListTr extends BaseCourierUAdditionListTr
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
//            array('language_id, courier_u_addition_list_id, title, description', 'required'),
            array('language_id, courier_u_addition_list_id', 'required'),
            array('language_id, courier_u_addition_list_id', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>256),
            array('id, language_id, courier_u_addition_list_id, title, description, date_updated', 'safe', 'on'=>'search'),
        );
    }
}