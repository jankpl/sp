<?php

class PluginsController extends Controller {

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Plugins'),
        ));
    }

    public function actionCreate() {
        $model = new Plugins;
        $modelTrs = [];

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $operatorTr = new PluginsTr();
            $operatorTr->language_id = $item->id;
            $modelTrs[$item->id] = $operatorTr;
        }


        if (isset($_POST['Plugins'])) {
            $model->setAttributesAll($_POST['Plugins']);

            foreach($_POST['PluginsTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new PluginsTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->pluginsTrs = $modelTrs;

            if ($model->saveWithTr(false)) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'Plugins');
        $modelTrs = [];

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = PluginsTr::model()->find('plugins_id=:plugins_id AND language_id=:language_id', array(':plugins_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $modelTr = new PluginsTr();
                $modelTr->language_id = $item->id;
                $modelTr->plugins_id = $id;
                $modelTrs[$item->id] = $modelTr;
            }
        }

        if (isset($_POST['Plugins'])) {


            $model->setAttributesAll($_POST['Plugins']);

            foreach($_POST['PluginsTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new PluginsTr();
                $modelTrs[$key]->setAttributes($item);
            }



            $model->pluginsTrs = $modelTrs;


            if ($model->saveWithTr(false)) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'Plugins')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {
        $this->redirect(['/plugins/admin']);
    }

    public function actionAdmin() {
        $model = new Plugins('search');
        $model->unsetAttributes();

        if (isset($_GET['Plugins']))
            $model->setAttributes($_GET['Plugins']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionAjaxPrepareImage()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            try {
                $img = $_POST['img'];

                $img = explode(',', $img);

                if(!isset($img[1]))
                    throw new Exception;

                $img = $img[1];

                $img = base64_decode($img);

                if($img == '')
                    throw new Exception;

                $im = ImagickMine::newInstance();
                if(!$im->readImageBlob($img))
                    throw new Exception;

                if ($im->getImageWidth() > Plugins::IMG_WIDTH)
                    $im->scaleImage(Plugins::IMG_WIDTH, 0);

                if ($im->getImageHeight() > Plugins::IMG_HEIGHT)
                    $im->scaleImage(0, Plugins::IMG_HEIGHT);


                $im->setFormat('png');

                $imgString = $im->__tostring();
                $imgString = base64_encode($imgString);
                $imgString = 'data:' . $im->getImageMimeType() . ';base64,' . $imgString;

                echo CJSON::encode(['image' => $imgString]);
            }
            catch(Exception $ex)
            {
                echo CJSON::encode(['image' => false]);
            }
        } else {
            $this->redirect(['/']);
        }
    }

    public function actionStat($id)
    {
        /* @var $model Plugins */


        $model = Plugins::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        else
            throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}