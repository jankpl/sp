<?php
/* @var $this StatController */
/* @var $model CourierStatHistory */
/* @var $form CActiveForm */
/* @var $showLocation boolean */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'stat-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'status_date'); ?>
        <?php
        $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
            'attribute'=>'status_date',
            'model' => $model,
            'options'=>array(
                'hourGrid' => 4,
                'hourMin' => 0,
                'hourMax' => 23,
                'showSecond'=> true,
                'timeFormat' => 'hh:mm:ss',
                'changeMonth' => true,
                'changeYear' => true,
                'dateFormat' => 'yy-mm-dd',
            ),
        ));
        ?>   (*)
        <?php echo $form->error($model,'status_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'current_stat_id'); ?>
        <?php echo $form->dropDownList($model,'current_stat_id', CHtml::listData($statusList, 'id', 'name')); ?>
        <?php echo $form->error($model,'current_stat_id'); ?>
    </div>

    <?php if($showLocation): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'location'); ?>
        <?php echo $form->textField($model,'location'); ?>
        <?php echo $form->error($model,'location'); ?>
    </div>
<?php endif; ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
    </div>

    <?php $this->endWidget(); ?>
    <div class="note">*  if empty, the data will be automaticaly generated</div>
</div><!-- form -->