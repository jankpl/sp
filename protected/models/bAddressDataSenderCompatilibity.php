<?php


class bAddressDataSenderCompatilibity extends bBaseBehavior
{

    protected $_allowedClass = array('Courier', 'HybridMail', 'InternationalMail');

    public function getSenderCountry()
    {
        return $this->owner()->senderAddressData->country0;
    }

    public function getSender_country_id()
    {
        return $this->owner()->senderAddressData->country_id;
    }

    public function getSender_name()
    {
        return $this->owner()->senderAddressData->name;
    }

    public function getSender_company()
    {
        return $this->owner()->senderAddressData->company;
    }

    public function getSender_country()
    {
        return $this->owner()->senderAddressData->country;
    }

    public function getSender_zip_code()
    {
        return $this->owner()->senderAddressData->zip_code;
    }

    public function getSender_city()
    {
        return $this->owner()->senderAddressData->city;
    }

    public function getSender_address_line_1()
    {
        return $this->owner()->senderAddressData->address_line_1;
    }

    public function getSender_address_line_2()
    {
        return $this->owner()->senderAddressData->address_line_2;
    }

    public function getSender_tel()
    {
        return $this->owner()->senderAddressData->tel;
    }

    public function setSenderCountry($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->country0 = $val;
    }

    public function setSender_country_id($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->country_id = $val;
    }

    public function setSender_name($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->name = $val;
    }

    public function setSender_company($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->company = $val;
    }

    public function setSender_country($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->country = $val;
    }

    public function setSender_zip_code($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->zip_code = $val;
    }

    public function setSender_city($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->city = $val;
    }

    public function setSender_address_line_1($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->address_line_1 = $val;
    }

    public function setSender_address_line_2($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->address_line_2 = $val;
    }

    public function setSender_tel($val)
    {
        if($this->owner()->senderAddressData === NULL) $this->owner()->senderAddressData = new AddressData();
        $this->owner()->senderAddressData->tel = $val;
    }


}
