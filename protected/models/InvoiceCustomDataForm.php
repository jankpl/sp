<?php

class InvoiceCustomDataForm extends CFormModel
{
    public $operator_reference;
    public $shipment_date;
    public $unit_value;
    public $total_value;
    public $qty_of_items;
    public $comodities;
    public $total_pieces;
    public $content_type;
    public $country_of_orgin;
    public $hs_tariff_code;
    public $terms_of_trade;
    public $sender_tax_id;
    public $consinee_tax_id;
//
//·         Shipment date
//
//·         Unit value and total value
//
//·         Qty of items
//
//·         The option to add multiple commodities example
//
//·         Number of pieces in the consignment
//
//·         Content type (goods /document)
//
//·         country of origin (where can we change the country origin on cql as the origin will not always be the UK)
//
//·         HS tariff code (A hyperlink to searching the commodity is required https://www.trade-tariff.service.gov.uk/sections) (optional, although mandatory for some countries)
//
//·         terms of trade (on other sites I use there is a drop down option with the following terms:- DAP - Delivered At Place / DDP - Delivered Duty Paid / DAT – Delivered at Terminal / CIP - Carriage and Insurance Paid / CPT - Carriage Paid To / EXW – ExWorks / FCA - Free Carrier (optional)
//
//·         Senders tax id (optional)
//
//·         Consignees tax id (optional)

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(

        );
    }
}