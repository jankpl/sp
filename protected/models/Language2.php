<?php

/*
 * This class used to be using data from DB, but now it's hardcoded
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $short_name

 */

class Language2
{

    const ID_PL = 1;
    const ID_EN = 2;
    const ID_EN_GB = 3;
    const ID_PL_RUCH = 4;
    const ID_DE = 5;
    const ID_PL_QURIERS = 6;
    const ID_EN_CQL = 7;


//    public static function model($className=__CLASS__) {
//		return parent::model($className);
//	}

    protected static function _data()
    {
        $data = [];
        $data[self::ID_PL] = new stdClass();
        $data[self::ID_PL]->id = self::ID_PL;
        $data[self::ID_PL]->name = 'polski';
        $data[self::ID_PL]->code = 'pl';
        $data[self::ID_PL]->short_name = $data[self::ID_PL]->code;
        $data[self::ID_PL]->stat = 1;

        $data[self::ID_EN] = new stdClass();
        $data[self::ID_EN]->id = self::ID_EN;
        $data[self::ID_EN]->name = 'english';
        $data[self::ID_EN]->code = 'en';
        $data[self::ID_EN]->short_name = $data[self::ID_EN]->code;
        $data[self::ID_EN]->stat = 1;

        $data[self::ID_EN_GB] = new stdClass();
        $data[self::ID_EN_GB]->id = self::ID_EN_GB;
        $data[self::ID_EN_GB]->name = 'en_gb';
        $data[self::ID_EN_GB]->code = 'english(gb)';
        $data[self::ID_EN_GB]->short_name = $data[self::ID_EN_GB]->code;
        $data[self::ID_EN_GB]->stat = 1;

        $data[self::ID_PL_RUCH] = new stdClass();
        $data[self::ID_PL_RUCH]->id = self::ID_PL_RUCH;
        $data[self::ID_PL_RUCH]->name = 'polski(ruch)';
        $data[self::ID_PL_RUCH]->code = 'pl_ruch';
        $data[self::ID_PL_RUCH]->short_name = $data[self::ID_PL_RUCH]->code;
        $data[self::ID_PL_RUCH]->stat = 1;

        $data[self::ID_DE] = new stdClass();
        $data[self::ID_DE]->id = self::ID_DE;
        $data[self::ID_DE]->name = 'de (notifications)';
        $data[self::ID_DE]->code = 'de';
        $data[self::ID_DE]->short_name = $data[self::ID_DE]->code;
        $data[self::ID_DE]->stat = 2;

        $data[self::ID_PL_QURIERS] = new stdClass();
        $data[self::ID_PL_QURIERS]->id = self::ID_PL_QURIERS;
        $data[self::ID_PL_QURIERS]->name = 'polski (quriers)';
        $data[self::ID_PL_QURIERS]->code = 'pl_quriers';
        $data[self::ID_PL_QURIERS]->short_name = $data[self::ID_PL_QURIERS]->code;
        $data[self::ID_PL_QURIERS]->stat = 1;

        $data[self::ID_EN_CQL] = new stdClass();
        $data[self::ID_EN_CQL]->id = self::ID_PL;
        $data[self::ID_EN_CQL]->name = 'english (cql)';
        $data[self::ID_EN_CQL]->code = 'en (cql)';
        $data[self::ID_EN_CQL]->short_name = $data[self::ID_EN_CQL]->code;
        $data[self::ID_EN_CQL]->stat = 1;

        return $data;
    }

    /**
     * Fake cache...
     * @param $value Dummy value...
     * @return Language2
     */
    public function cache($value = 0)
    {
        return new self;
    }

    public static function model()
    {
        return new self;
    }

    public function findAll()
    {
        return self::_data();
    }

    public function findByPk($pk)
    {
        $data = self::_data();

        if(isset($data[$pk]))
            return $data[$pk];
        else
            return null;
    }

    public function find()
    {
        return self::_data()[self::ID_PL];
    }

    public function findByAttributes($attributes)
    {
        foreach(self::_data() AS $language)
        {
            $found = true;
            foreach($attributes AS $attrName => $attrValue)
            {
                // unknown attr
                if(!$language->$attrName)
                    return NULL;

                if($language->$attrName != $attrValue)
                    $found = false;
            }

            if($found)
                return $language;

        }
    }

    public static function nameById($id)
    {
        $data = self::_data();

        if(isset($data[$id]))
            return $data[$id]->short_name;
        else
            return null;
    }

    public static function idByName($name)
    {
        $data = self::_data();

        $name = strtoupper($name);

        $id = false;

        foreach($data AS $key => $item)
        {
            if($item->short_name == $name) {
                $id = $key;
                break;
            }
        }

        return $id;
    }

    public static function listIds()
    {
        return [
            self::PLN_ID,
            self::EUR_ID,
            self::GBP_ID,
        ];
    }

    public static function listIdsWithNames()
    {
        return [
            self::PLN_ID => self::nameById(self::PLN_ID),
            self::EUR_ID => self::nameById(self::EUR_ID),
            self::GBP_ID => self::nameById(self::GBP_ID),
        ];
    }

}