<?php

Yii::import('application.models._base.BaseOrderProduct');

class OrderProduct extends BaseOrderProduct
{
    const TYPE_COURIER = 1;
    const TYPE_HYBRID_MAIL = 2;
    const TYPE_INTERNATIONAL_MAIL = 3;
    const TYPE_POSTAL = 4;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => rand(0,9999).microtime()));
        }

        return parent::beforeSave();
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }


    public function rules()
    {
        return array(
            array('order_id', 'required', 'except' => 'before-order'),

            array('type, type_item_id, currency, value_netto, value_brutto', 'required'),
            array('type, type_item_id, paid', 'numerical', 'integerOnly' => true),
            array('value_netto, value_brutto, user_id', 'numerical'),
            array('hash', 'length', 'max' => 64),
            array('currency', 'length', 'max' => 10),
            array('date_updated', 'safe'),
            array('date_updated, ,  user_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, type, type_item_id, date_entered, date_updated, hash, currency, value_netto, value_brutto, paid,  user_id', 'safe', 'on' => 'search'),
        );
    }


    public static function createNewProduct($type, $type_item_id, $currency, $value_netto, $value_brutto)
    {
        $model = new self;
        $model->type = $type;
        $model->type_item_id = $type_item_id;
        $model->currency = $currency;
        $model->value_netto = $value_netto;
        $model->value_brutto = $value_brutto;
        $model->user_id = Yii::app()->user->id;

        $model->scenario = 'before-order';

        if(!$model->save()) {
            Yii::log(print_r($model->getErrors(),1), CLogger::LEVEL_ERROR);
            return false;
        }
        else
            return $model;
       // else {
            //Order::createOrderForProduct($model, true);
            //return $model;

        //}
    }

    /**
     * @param $type
     * @param $id
     * @return OrderProduct
     */
    public static function getModelForProductItem($type, $id)
    {
        $model = self::model()->findByAttributes(array('type' => $type, 'type_item_id' => $id));
        return $model;

    }

    /**
     * @return Courier|Postal|HybridMail|InternationalMail
     */
    public function getProductModel()
    {
        switch($this->type)
        {
            case self::TYPE_COURIER:
                return Courier::model()->findByPk($this->type_item_id);
                break;
            case self::TYPE_POSTAL:
                return Postal::model()->findByPk($this->type_item_id);
                break;
            case self::TYPE_HYBRID_MAIL:
                return HybridMail::model()->findByPk($this->type_item_id);
                break;
            case self::TYPE_INTERNATIONAL_MAIL:
                return InternationalMail::model()->findByPk($this->type_item_id);
                break;
        }

    }
}