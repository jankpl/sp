<?php
/* @var $model Order */
/* @var $model OrderStatHistory[] */
?>

<table class="list hTop" style="width: 500px;">
    <tr class="header"><td>Date</td><td>Status</td></tr>
    <?php
    $i = 0;
    foreach($model AS $itemStatHistory):
        ?>
        <tr>
            <td><?php echo $itemStatHistory->status_date;?></td>

            <td><?php echo $itemStatHistory->currentStat->name;?></td>
        </tr>
    <?php
    endforeach;
    ?>
</table>