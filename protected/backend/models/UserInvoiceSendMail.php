<?php


class UserInvoiceSendMail extends CFormModel
{
    public $title;
    public $text;



    public function rules()
    {
        return array(
            array('title, text', 'required'),
        );
    }


    public function attributeLabels()
    {

    }

    public function send($email, $name, User $model = NULL)
    {
        S_Mailer::sendToCustomer($email, $name, $this->title, $this->text, true, NULL, false, false, false, false, false, $model ? $model->source_domain : NULL );
    }

    public function sendGroup(array $emails, $name)
    {
//        S_Mailer::sendToCustomer($email, $name, $this->title, $this->text, true, NULL, false);
    }
}
