<?php
/* @var $S_UniversalTt S_UniversalTt */
?>

<h2><?php echo Yii::t('site','Status przesyłki #{number}', array('{number}' => $S_UniversalTt->getTrackId()));?></h2>

<?php
if(in_array($S_UniversalTt->getService(), [S_UniversalTt::SERVICE_COURIER, S_UniversalTt::SERVICE_POSTAL])):
    $stat_map = $S_UniversalTt->getCurrentStatMapId();
    if($stat_map):
        ?>

        <div class="row stat-ico-list">
            <div class="col-xs-12 text-center">
                <div class="stat-item-ico-container active">
                    <div class="stat-item-ico stat-ico-new"></div>
                    <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_NEW);?></div>
                </div>
                <?php
                if($stat_map == StatMap::MAP_CANCELLED):
                    ?>
                    <div class="stat-item-ico-container active">
                        <div class="stat-item-ico stat-ico-cancelled"></div>
                        <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_CANCELLED);?></div>
                    </div>
                <?php
                elseif($stat_map == StatMap::MAP_PROBLEM):
                    ?>
                    <div class="stat-item-ico-container active">
                        <div class="stat-item-ico stat-ico-problem"></div>
                        <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_PROBLEM);?></div>
                    </div>
                <?php
                elseif($stat_map == StatMap::MAP_RETURN):
                    ?>
                    <div class="stat-item-ico-container active">
                        <div class="stat-item-ico stat-ico-return"></div>
                        <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_RETURN);?></div>
                    </div>
                <?php
                else:
                    ?>
                    <div class="stat-item-ico-container <?= $stat_map >= StatMap::MAP_ACCEPTED ? 'active' : '';?>">
                        <div class="stat-item-ico stat-ico-accepted"></div>
                        <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_ACCEPTED);?></div>
                    </div>
                    <div class="stat-item-ico-container <?= $stat_map >= StatMap::MAP_DELIVERED_TO_HUB ? 'active' : '';?>">
                        <div class="stat-item-ico stat-ico-hub"></div>
                        <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_DELIVERED_TO_HUB);?></div>
                    </div>
                    <div class="stat-item-ico-container <?= $stat_map >= StatMap::MAP_IN_TRANSPORTATION ? 'active' : '';?>">
                        <div class="stat-item-ico stat-ico-transportation"></div>
                        <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_IN_TRANSPORTATION);?></div>
                    </div>
                    <div class="stat-item-ico-container <?= $stat_map >= StatMap::MAP_IN_DELIVERY ? 'active' : '';?>">
                        <div class="stat-item-ico stat-ico-delivery"></div>
                        <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_IN_DELIVERY);?></div>
                    </div>
                    <div class="stat-item-ico-container <?= $stat_map >= StatMap::MAP_DELIVERED ? 'active' : '';?>">
                        <div class="stat-item-ico stat-ico-delivered"></div>
                        <div class="stat-item-text"><?= StatMap::getMapNameById(StatMap::MAP_DELIVERED);?></div>
                    </div>
                <?php
                endif;
                ?>
            </div>
        </div>

        <div style="font-weight: bold; text-align: center; font-size: 14px; background: #D4350C; color: white; width: 100%; margin: 0 auto; padding: 5px;">
            <?php
            //            echo $S_UniversalTt->getCurrentStatMapName();
            echo $S_UniversalTt->getCurrentStatusNameFull(true);
            ?>
        </div>

    <?php

    else:
        ?>

        <div style="font-weight: bold; text-align: center; font-size: 14px; background: #D4350C; color: white; width: 100%; margin: 0 auto; padding: 5px;">
            <?php
            echo $S_UniversalTt->getCurrentStatusNameFull(true);
            ?>
        </div>

    <?php
    endif;
else:
    ?>
    <div style="font-weight: bold; text-align: center; font-size: 14px; background: #D4350C; color: white; width: 100%; margin: 0 auto; padding: 5px;">
        <?php
        echo $S_UniversalTt->getCurrentStatusNameFull(true);
        ?>
    </div>

<?php
endif;
?>

<?php
if(!isset($noMap) && $S_UniversalTt && !$S_UniversalTt->isArchived() && $S_UniversalTt->getCurrentStatMapId() != StatMap::MAP_CANCELLED && $S_UniversalTt->getCurrentStatMapId() != StatMap::MAP_RETURN):
    if($S_UniversalTt->getOwnerUserModel() === NULL OR !$S_UniversalTt->getOwnerUserModel()->getHideSenderAddressOnNotificationAndTt())
        $this->Widget('TtMap', array('location' => $S_UniversalTt->getCurrentLocation(true), 'send_location' => $S_UniversalTt->getSendLocation(), 'receiver_location' => $S_UniversalTt->getReceiverLocation(), 'stat_map' => $S_UniversalTt->getCurrentStatMapId()));
endif;
?>

<h2><?php echo Yii::t('site','Historia');?></h2>

<?php
if(!$S_UniversalTt->isArchived()):
    ?>


    <div style="text-align: center; margin: 0 auto;">

        <table class="list hTop" style="width: 100%">

            <tr class="header"><td><?php echo Yii::t('site','Data');?></td><td><?php echo Yii::t('site','Status');?></td></tr>
            <?php

            if(is_array($S_UniversalTt->getHistory()))
                foreach($S_UniversalTt->getHistory() AS $item):
                    ?>
                    <tr>
                        <td style="max-width: 200px;"><?php echo $item['date'];?></td>
                        <td><?php echo $item['location'] ? S_Useful::urlToLinkInText($item['full_text']).' ('.S_Useful::urlToLinkInText($item['location']).')' : S_Useful::urlToLinkInText($item['full_text']); ?></td>
                    </tr>
                <?php
                endforeach;
            ?>
        </table>

    </div>

<?php
else:
    ?>
    <div class="alert alert-warning"><?= Yii::t('site', 'Historia statusów tej przesyłki już wygasła.');?></div>
<?php
endif;
?>

<?php
if($S_UniversalTt->getService() == S_UniversalTt::SERVICE_COURIER):
    ?>

    <h2><?php echo Yii::t('courier','Dane paczki');?></h2>

    <div style="text-align: center; margin: 0 auto; width: 100%; max-width: 500px;">
        <table class="list hLeft" style="width: 100%">
            <tr>
                <td><?= $S_UniversalTt->getServiceModel()->getAttributeLabel('package_weight');?></td>
                <td><?= $S_UniversalTt->getServiceModel()->package_weight;?> <?= Courier::getWeightUnit();?></td>
            </tr>
            <tr>
                <td><?= Yii::t('courier', 'Wymiary paczki');?></td>
                <td><?= $S_UniversalTt->getServiceModel()->package_size_l;?> x <?= $S_UniversalTt->getServiceModel()->package_size_d;?> x <?= $S_UniversalTt->getServiceModel()->package_size_w;?> <?= Courier::getDimensionUnit();?></td>
            </tr>
            <?php
            if($S_UniversalTt->getOwnerUserModel() === NULL OR !$S_UniversalTt->getOwnerUserModel()->getHideSenderAddressOnNotificationAndTt()):
                ?>
                <tr>
                    <td><?= Yii::t('courier', 'Kraj nadawcy');?></td>
                    <td><?= $S_UniversalTt->getServiceModel()->senderAddressData->country0->name;?> (<?= $S_UniversalTt->getServiceModel()->senderAddressData->zip_code;?>)</td>
                </tr>
            <?php
            endif;
            ?>
            <tr>
                <td><?= Yii::t('courier', 'Kraj odbiorcy');?></td>
                <td><?= $S_UniversalTt->getServiceModel()->receiverAddressData->country0->name;?> (<?= $S_UniversalTt->getServiceModel()->receiverAddressData->zip_code;?>)</td>
            </tr>
            <tr>
                <td><?= Yii::t('courier', 'Data nadania');?></td>
                <td><?= substr($S_UniversalTt->getServiceModel()->date_entered,0,10);?></td>
            </tr>
            <?php
            if($S_UniversalTt->getServiceModel()->isCOD()):
                ?>
                <tr>
                    <td><?= Yii::t('courier', 'COD');?></td>
                    <td><?= $S_UniversalTt->getServiceModel()->getCodValue().' '.$S_UniversalTt->getServiceModel()->getCodCurrency();?></td>
                </tr>
            <?php
            endif;
            ?>
            <?php if($S_UniversalTt->getServiceModel()->user->source_domain == PageVersion::PAGEVERSION_CQL):
                list($operator, $tt_id) = $S_UniversalTt->getServiceModel()->getExternalOperatorsAndIds(true);
                ?>
                <tr>
                    <td><?= Yii::t('courier', 'Note1');?></td>
                    <td><?= $S_UniversalTt->getServiceModel()->note1 ? $S_UniversalTt->getServiceModel()->note1 : '-';?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('courier', 'Operator');?></td>
                    <td><?= is_array($operator) ? $operator[0] : '-';?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('courier', 'Number');?></td>
                    <td><?= is_array($tt_id) ? $tt_id[0] : '-';?></td>
                </tr>
            <?php
            endif;
            ?>
        </table>
    </div>

<?php
elseif($S_UniversalTt->getService() == S_UniversalTt::SERVICE_POSTAL):
    ?>

    <h2><?php echo Yii::t('courier','Dane paczki');?></h2>

    <div style="text-align: center; margin: 0 auto; width: 100%; max-width: 500px;">
        <table class="list hLeft" style="width: 100%">
            <tr>
                <td><?= $S_UniversalTt->getServiceModel()->getAttributeLabel('weight');?></td>
                <td><?= $S_UniversalTt->getServiceModel()->weight;?> <?= Postal::getWeightUnit();?></td>
            </tr>
            <?php
            if($S_UniversalTt->getOwnerUserModel() === NULL OR !$S_UniversalTt->getOwnerUserModel()->getHideSenderAddressOnNotificationAndTt()):
                ?>
                <tr>
                    <td><?= Yii::t('courier', 'Kraj nadawcy');?></td>
                    <td><?= $S_UniversalTt->getServiceModel()->senderAddressData->country0->name;?> (<?= $S_UniversalTt->getServiceModel()->senderAddressData->zip_code;?>)</td>
                </tr>
            <?php
            endif;
            ?>
            <tr>
                <td><?= Yii::t('courier', 'Kraj odbiorcy');?></td>
                <td><?= $S_UniversalTt->getServiceModel()->receiverAddressData->country0->name;?> (<?= $S_UniversalTt->getServiceModel()->receiverAddressData->zip_code;?>)</td>
            </tr>
            <tr>
                <td><?= Yii::t('courier', 'Data nadania');?></td>
                <td><?= substr($S_UniversalTt->getServiceModel()->date_entered,0,10);?></td>
            </tr>
            <?php
            if($S_UniversalTt->getServiceModel()->user->source_domain == PageVersion::PAGEVERSION_CQL):
                ?>
                <tr>
                    <td><?= Yii::t('courier', 'Note1');?></td>
                    <td><?= $S_UniversalTt->getServiceModel()->note1 ? $S_UniversalTt->getServiceModel()->note1 : '-';?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('courier', 'Operator');?></td>
                    <td><?= $S_UniversalTt->getServiceModel()->postalOperator ? $S_UniversalTt->getServiceModel()->postalOperator->name : '-';?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('courier', 'Number');?></td>
                    <td><?= $S_UniversalTt->getServiceModel()->postalLabel ? $S_UniversalTt->getServiceModel()->postalLabel->track_id : '-';?></td>
                </tr>
            <?php
            endif;
            ?>
        </table>
    </div>

<?php
endif;
?>



<h2><?php echo Yii::t('site','Odnośnik');?></h2>
<?php echo CHtml::link(Yii::t('site','Śledź zamówienie #{number}', array('{number}' => $S_UniversalTt->getTrackId())), Yii::app()->createAbsoluteUrl('/site/tt', array('urlData' => $S_UniversalTt->getTrackId()))); ?>



