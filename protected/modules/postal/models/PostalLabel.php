<?php

Yii::import('application.modules.postal.models._base.BasePostalLabel');

class PostalLabel extends BasePostalLabel
{
    protected $_afterfind_track_id;

    const STAT_NEW = 1;
    const STAT_NEW_DIRECT = 2; // this packages wont be run by queque - just directly
    const STAT_SUCCESS = 10;
    const STAT_FAILED = 99;

    const STAT_EXPIRED = 15;

    // some labels contains more than one page - if so, how many additional?
    public $_additional_pages = 0;

    const EXPIRATION_DAYS = 90;

//	public $_ebay_order_id;
    public $_cancel_package_on_fail;

    public $_pp_forceTTNumber;


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => NULL,
                'updateAttribute' => 'date_updated',
            )
        );
    }

    public function rules() {
        return array(
            array('source_postal_operator_id, stat', 'required'),
            array('source_postal_operator_id, stat, source_postal_operator_uniq_id', 'numerical', 'integerOnly'=>true),
            array('file_path', 'length', 'max'=>256),
            array('file_type', 'length', 'max'=>32),
            array('log, params', 'safe'),
            array('file_path, file_type, log, params, source_postal_operator_uniq_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_updated, source_postal_operator_id, file_path, file_type, stat, log, params', 'safe', 'on'=>'search'),
        );
    }


    public function afterSave()
    {
        if(($this->isNewRecord OR $this->_afterfind_track_id === NULL) && $this->track_id) {
            IdStore::createNumber(IdStore::TYPE_POSTAL, $this->postal->id, $this->track_id);
        }
        else if($this->track_id != $this->_afterfind_track_id)
        {
            IdStore::updateNumber(IdStore::TYPE_POSTAL, $this->postal->id, $this->_afterfind_track_id, $this->track_id);
        }
        return parent::afterSave();
    }

    public static function getStats()
    {
        $data = array(
            self::STAT_NEW => 'Waiting',
            self::STAT_NEW_DIRECT => 'Waiting from API',
            self::STAT_SUCCESS => 'OK',
            self::STAT_FAILED => 'Fail',
            self::STAT_EXPIRED => 'Expired',
        );

        return $data;
    }

    /**
     * Order label and return standarized array with label, stat and track id.
     * Calls additional operations like update eBay data or UPS collection
     * @param bool|false $returnArrayOfIds
     * @return array
     * @throws CDbException
     */
    public function runLabelOrdering($returnArrayOfIds = false, $force = false)
    {
        MyDump::dump('pl.txt', $this->id.' : START');

        $retries = 0;
        while($this->postal->postal_stat_id != PostalStat::PACKAGE_PROCESSING)
        {
            MyDump::dump('pl.txt', $this->id.' : NOT READY - STAT = '.$this->postal->stat);
            sleep(5);
            $this->postal->refresh();
            $retries++;

            if($retries > 10)
                return false;
        }

        $semaphore = new Semaphore(intval(Semaphore::POSTAL_LABEL_PREFIX.$this->id), Yii::app()->basePath . '/_temp/');
        try {
            $semaphore->acquire();
        } catch (Exception $e) {
            MyDump::dump('pl.txt', $this->id.' : SEM LOCKED ('.Semaphore::POSTAL_LABEL_PREFIX.intval($this->id).')');
            return false;
        }

        MyDump::dump('pl.txt', $this->id.' : AFTER SEM INIT ('.Semaphore::POSTAL_LABEL_PREFIX.intval($this->id).')');

        $this->refresh();
        if ($this->stat != self::STAT_NEW AND $this->stat != self::STAT_NEW_DIRECT) {
            MyDump::dump('pl.txt', $this->id.' : ALREADY DONE');
            return false;
        }
        $postal = $this->postal;

        if(in_array($this->postalOperator->broker_id, array_keys(GLOBAL_BROKERS::getBrokersListPostal())))
        {
            $operatorClient = GLOBAL_BROKERS::globalOperatorClientFactory($this->postalOperator->broker_id);
            $operatorName = GLOBAL_BROKERS::getBrokerNameById($this->postalOperator->broker_id);
            $return = $operatorClient::orderForPostal($postal, true);

            $return = array_pop($return); // for postal array is one element array.

            if (!$return->isSuccess()) {
                $this->stat = self::STAT_FAILED;
                $this->log = $return->getError();

                $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);
                $this->postal->addToLog($this->log, PostalLog::CAT_ERROR, $operatorName);
                $this->update(array('stat', 'log', 'date_updated'));

                // @ 14.02.2019
                // remove ref number on error
                if($this->postal->ref && in_array($this->postal->user_id, [1289]))
                {
                    $ref = $this->postal->ref;
                    $this->postal->ref = NULL;
                    $this->postal->update(['ref']);
                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
                }
            } else {

                $this->track_id = $return->getTt();
                $this->saveLabelDefault($return->getLabel());
                $this->log = '';
                $this->stat = self::STAT_SUCCESS;
                $this->update(array('track_id', 'stat', 'date_updated', 'file_path', 'file_type', 'log', 'params'));

                $this->postal->changeStat(PostalStat::PACKAGE_RETREIVE);
            }

            if ($this->postal->_ebay_order_id != '')
                $this->updateEbayData(!$return->isSuccess());

            // depreciated
//            if (!$return->isSuccess() && $this->_cancel_package_on_fail) {
//                $this->postal->changeStat(CourierStat::CANCELLED);
//
//                // @ 29.12.2017
//                // remove ref number on error
//                    if($this->postal->ref && in_array($this->postal->user_id, [1289]))
//                {
//                    $ref = $this->postal->ref;
//                    $this->postal->ref = NULL;
//                    $this->postal->update(['ref']);
//                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
//                }
//            }

        }
        else if ($this->postalOperator->broker_id == Postal::POSTAL_BROKER_OOE) {

            $packageData = OneWorldExpressPackage::createByPostal($postal, $this->postalOperator->_ooe_service, $this->postalOperator->_ooe_account, $this->postalOperator->_ooe_login, $this->postalOperator->_ooe_pass);

            $return = Yii::app()->OneWorldExpress->getLabel($packageData, true);

            if (isset($return['error'])) {
                $this->stat = self::STAT_FAILED;
                $this->log = $return['error'];

                $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);
                $this->postal->addToLog($this->log, PostalLog::CAT_ERROR, 'OOE');

                $this->update(array('stat', 'log', 'date_updated'));


                // @ 14.02.2019
                // remove ref number on error
                if($this->postal->ref && in_array($this->postal->user_id, [1289]))
                {
                    $ref = $this->postal->ref;
                    $this->postal->ref = NULL;
                    $this->postal->update(['ref']);
                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
                }
            } else {

                $this->track_id = $return['trackId'];
                $this->saveOoeLabel($return['label']);
                $this->log = $return['label'];
                $this->stat = self::STAT_SUCCESS;
                $this->update(array('track_id', 'stat', 'date_updated', 'file_path', 'file_type', 'log', 'params'));

                $this->postal->changeStat(PostalStat::PACKAGE_RETREIVE);
            }

            if ($this->postal->_ebay_order_id != '')
                $this->updateEbayData(isset($return['error']));

            // depreciated
//            if (isset($return['error']) && $this->_cancel_package_on_fail)
//            {
//                $this->postal->changeStat(CourierStat::CANCELLED);
//
//                // @ 29.12.2017
//                // remove ref number on error
//                    if($this->postal->ref && in_array($this->postal->user_id, [1289]))
//                {
//                    $ref = $this->postal->ref;
//                    $this->postal->ref = NULL;
//                    $this->postal->update(['ref']);
//                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
//                }
//            }


        } else if($this->postalOperator->broker_id == Postal::POSTAL_BROKER_POCZTA_POLSKA) {

            $return = PocztaPolskaClient::createByPostal($postal, $this->source_postal_operator_id, $this->source_postal_operator_uniq_id, $this->_pp_forceTTNumber);

            if (isset($return['error'])) {
                $this->stat = self::STAT_FAILED;
                $this->log = $return['error'];

                $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);
                $this->postal->addToLog($this->log, PostalLog::CAT_ERROR, 'PocztaPolska');
                $this->update(array('stat', 'log', 'date_updated'));

                // @ 14.02.2019
                // remove ref number on error
                if($this->postal->ref && in_array($this->postal->user_id, [1289]))
                {
                    $ref = $this->postal->ref;
                    $this->postal->ref = NULL;
                    $this->postal->update(['ref']);
                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
                }
            } else {

                $this->track_id = $return['trackId'];
                $this->savePocztaPolskaLabel($return['label'], $this->_pp_forceTTNumber ? true : false);
                $this->log = '';
                $this->stat = self::STAT_SUCCESS;
                $this->update(array('track_id', 'stat', 'date_updated', 'file_path', 'file_type', 'log', 'params'));

                $this->postal->changeStat(PostalStat::PACKAGE_RETREIVE);
            }

            if ($this->postal->_ebay_order_id != '')
                $this->updateEbayData(isset($return['error']));

            // depreciated
//            if (isset($return['error']) && $this->_cancel_package_on_fail) {
//                $this->postal->changeStat(CourierStat::CANCELLED);
//
//                // @ 29.12.2017
//                // remove ref number on error
//                    if($this->postal->ref && in_array($this->postal->user_id, [1289]))
//                {
//                    $ref = $this->postal->ref;
//                    $this->postal->ref = NULL;
//                    $this->postal->update(['ref']);
//                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
//                }
//            }


        } else if($this->postalOperator->broker_id == Postal::POSTAL_BROKER_DHLDE) {

            $return = DhlDeClient::orderForPostal($postal);

            if (isset($return['error'])) {
                $this->stat = self::STAT_FAILED;
                $this->log = $return['error'];

                $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);
                $this->postal->addToLog($this->log, PostalLog::CAT_ERROR, 'DHL_DE');
                $this->update(array('stat', 'log', 'date_updated'));


                // @ 14.02.2019
                // remove ref number on error
                if($this->postal->ref && in_array($this->postal->user_id, [1289]))
                {
                    $ref = $this->postal->ref;
                    $this->postal->ref = NULL;
                    $this->postal->update(['ref']);
                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
                }
            } else {

                $this->track_id = $return['track_id'];
                $this->saveDhlDeLabel($return['label']);
                $this->log = '';
                $this->stat = self::STAT_SUCCESS;
                $this->update(array('track_id', 'stat', 'date_updated', 'file_path', 'file_type', 'log', 'params'));

                $this->postal->changeStat(PostalStat::PACKAGE_RETREIVE);
            }

            if ($this->postal->_ebay_order_id != '')
                $this->updateEbayData(isset($return['error']));

            // depreciated
//            if (isset($return['error']) && $this->_cancel_package_on_fail) {
//                $this->postal->changeStat(CourierStat::CANCELLED);
//
//                // @ 29.12.2017
//                // remove ref number on error
//                    if($this->postal->ref && in_array($this->postal->user_id, [1289]))
//                {
//                    $ref = $this->postal->ref;
//                    $this->postal->ref = NULL;
//                    $this->postal->update(['ref']);
//                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
//                }
//            }

        } else if($this->postalOperator->broker_id == Postal::POSTAL_BROKER_POST11) {

            $return = Post11Client::orderForPostal($postal);

            if (isset($return['error'])) {
                $this->stat = self::STAT_FAILED;
                $this->log = $return['error'];

                $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);
                $this->postal->addToLog($this->log, PostalLog::CAT_ERROR, 'Post11');
                $this->update(array('stat', 'log', 'date_updated'));


                // @ 14.02.2019
                // remove ref number on error
                if($this->postal->ref && in_array($this->postal->user_id, [1289]))
                {
                    $ref = $this->postal->ref;
                    $this->postal->ref = NULL;
                    $this->postal->update(['ref']);
                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
                }
            } else {

                $this->track_id = $return['track_id'];
                $this->saveLabelDefault($return['label']);
                $this->log = '';
                $this->stat = self::STAT_SUCCESS;
                $this->update(array('track_id', 'stat', 'date_updated', 'file_path', 'file_type', 'log', 'params'));

                $this->postal->changeStat(PostalStat::PACKAGE_RETREIVE);
            }

            if ($this->postal->_ebay_order_id != '')
                $this->updateEbayData(isset($return['error']));

            // deprecated
//            if (isset($return['error']) && $this->_cancel_package_on_fail) {
//                $this->postal->changeStat(CourierStat::CANCELLED);
//
//                // @ 29.12.2017
//                // remove ref number on error
//                    if($this->postal->ref && in_array($this->postal->user_id, [1289]))
//                {
//                    $ref = $this->postal->ref;
//                    $this->postal->ref = NULL;
//                    $this->postal->update(['ref']);
//                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
//                }
//            }

        } else if($this->postalOperator->broker_id == Postal::POSTAL_BROKER_SWEDEN_POST) {

            Yii::app()->getModule('courier');
            $return = SwedenPostClient::orderForPostal($postal);

            if (isset($return['error'])) {
                $this->stat = self::STAT_FAILED;
                $this->log = $return['error'];

                $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);
                $this->postal->addToLog($this->log, PostalLog::CAT_ERROR, 'Post11');
                $this->update(array('stat', 'log', 'date_updated'));


                // @ 14.02.2019
                // remove ref number on error
                if($this->postal->ref && in_array($this->postal->user_id, [1289]))
                {
                    $ref = $this->postal->ref;
                    $this->postal->ref = NULL;
                    $this->postal->update(['ref']);
                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
                }
            } else {

                $this->track_id = $return['track_id'];
                $this->saveLabelDefault($return['label']);
                $this->log = '';
                $this->stat = self::STAT_SUCCESS;
                $this->update(array('track_id', 'stat', 'date_updated', 'file_path', 'file_type', 'log', 'params'));

                $this->postal->changeStat(PostalStat::PACKAGE_RETREIVE);
            }

            if ($this->postal->_ebay_order_id != '')
                $this->updateEbayData(isset($return['error']));

            // deprecated
//            if (isset($return['error']) && $this->_cancel_package_on_fail) {
//                $this->postal->changeStat(CourierStat::CANCELLED);
//
//                // @ 29.12.2017
//                // remove ref number on error
//                    if($this->postal->ref && in_array($this->postal->user_id, [1289]))
//                {
//                    $ref = $this->postal->ref;
//                    $this->postal->ref = NULL;
//                    $this->postal->update(['ref']);
//                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
//                }
//            }




        } else if($this->postalOperator->broker_id == Postal::POSTAL_BROKER_HUNGARY_POST) {

            $return = HuPostClient::orderForPostal($postal);

            if (isset($return['error'])) {
                $this->stat = self::STAT_FAILED;
                $this->log = $return['error'];

                $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);
                $this->postal->addToLog($this->log, PostalLog::CAT_ERROR, 'Hungary Post');
                $this->update(array('stat', 'log', 'date_updated'));


                // @ 14.02.2019
                // remove ref number on error
                if($this->postal->ref && in_array($this->postal->user_id, [1289]))
                {
                    $ref = $this->postal->ref;
                    $this->postal->ref = NULL;
                    $this->postal->update(['ref']);
                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
                }
            } else {

                $this->track_id = $return['track_id'];
                $this->saveHuPostLabel($return['label']);
                $this->log = '';
                $this->stat = self::STAT_SUCCESS;
                $this->update(array('track_id', 'stat', 'date_updated', 'file_path', 'file_type', 'log', 'params'));

                $this->postal->changeStat(PostalStat::PACKAGE_RETREIVE);
            }

            if ($this->postal->_ebay_order_id != '')
                $this->updateEbayData(isset($return['error']));

            // deprecated
//            if (isset($return['error']) && $this->_cancel_package_on_fail) {
//                $this->postal->changeStat(CourierStat::CANCELLED);
//
//                // @ 29.12.2017
//                // remove ref number on error
//                    if($this->postal->ref && in_array($this->postal->user_id, [1289]))
//                {
//                    $ref = $this->postal->ref;
//                    $this->postal->ref = NULL;
//                    $this->postal->update(['ref']);
//                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
//                }
//
//            }

        } else if($this->postalOperator->broker_id == Postal::POSTAL_BROKER_ROYAL_MAIL) {

            $return = RoyalMailClient::orderForPostal($postal);

            if (isset($return['error'])) {
                $this->stat = self::STAT_FAILED;
                $this->log = $return['error'];

                $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);
                $this->postal->addToLog($this->log, PostalLog::CAT_ERROR, 'Royal Mail');
                $this->update(array('stat', 'log', 'date_updated'));


                // @ 14.02.2019
                // remove ref number on error
                if($this->postal->ref && in_array($this->postal->user_id, [1289]))
                {
                    $ref = $this->postal->ref;
                    $this->postal->ref = NULL;
                    $this->postal->update(['ref']);
                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
                }
            } else {

                $this->track_id = $return['track_id'];
                $this->saveRoyalMailLabel($return['label']);
                $this->log = '';
                $this->stat = self::STAT_SUCCESS;
                $this->update(array('track_id', 'stat', 'date_updated', 'file_path', 'file_type', 'log', 'params'));

                $this->postal->changeStat(PostalStat::PACKAGE_RETREIVE);
            }

            if ($this->postal->_ebay_order_id != '')
                $this->updateEbayData(isset($return['error']));

            // deprecated
//            if (isset($return['error']) && $this->_cancel_package_on_fail) {
//                $this->postal->changeStat(CourierStat::CANCELLED);
//
//                // @ 29.12.2017
//                // remove ref number on error
//                    if($this->postal->ref && in_array($this->postal->user_id, [1289]))
//                {
//                    $ref = $this->postal->ref;
//                    $this->postal->ref = NULL;
//                    $this->postal->update(['ref']);
//                    $this->postal->addToLog('Removed REF number on error: '.$ref, PostalLog::CAT_ERROR);
//                }
//
//            }


        } else {
            // OPERATOR NOT KNOWN
            $error = 'Problem - nieznany operator';

            $this->stat = self::STAT_FAILED;
            $this->log = $error;
            $this->update('error', 'stat', 'date_updated');
            $this->postal->changeStat(PostalStat::CANCELLED_PROBLEM);

            $this->postal->addToLog($error, PostalLog::CAT_ERROR, 'ŚP');
        }


        try {
            $semaphore->release();
        } catch (Exception $e) {
            die('Could not release the semaphore');
        }
        MyDump::dump('pl.txt', $this->id.' : DONE!');

        return true;
    }

    public static function getOperators()
    {
        return Postal::getBrokers();
    }

    /**
     * Unserialize additional params
     * @return bool
     */
    public function afterFind()
    {
        if ($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if (is_array($this->params))
            foreach ($this->params AS $key => $item) {
                try {
                    $this->$key = $item;
                } catch (Exception $ex) {
                }
            }

        $this->_afterfind_track_id = $this->track_id;

        parent::afterFind();
    }

    /**
     * Serialize additional params (ex. Preffered pickup time)
     * @return bool
     */

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => microtime() . rand(1,9999) ));


            //  SPECIAL RULE FOR API!
            if($this->postal !== NULL && $this->stat == self::STAT_NEW && $this->postal->source == Postal::SOURCE_API)
                $this->stat = self::STAT_NEW_DIRECT;
        }

        $this->params = array(
            '_pp_forceTTNumber' => $this->_pp_forceTTNumber,
//			'_ebay_export_status' => $this->_ebay_export_status,
        );

//         serialize only if not all values are empty
        if (is_array($this->params) && array_filter($this->params))
            $this->params = self::serialize($this->params);
        else
            $this->params = NULL;

        return parent::beforeSave();
    }


    public static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    public static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }


    public function saveOoeLabel($labelPath, $asImage = true)
    {
        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

//        $basePath = Yii::app()->basePath . '/../';
//        $dir = 'uplabelsPostal/';
//        $temp = $dir . 'temp/';

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $label = @file_get_contents($labelPath, NULL, stream_context_create($arrContextOptions));


        $file_info = new finfo(FILEINFO_MIME_TYPE);
        $mime_type = $file_info->buffer($label);

        $extension = '';
        switch ($mime_type) {
            case 'image/gif'    :
                $extension = 'gif';
                break;
            case 'image/png'    :
                $extension = 'png';
                break;
            case 'image/jpeg'   :
                $extension = 'jpg';
                break;
            case 'application/pdf'   :
                $extension = 'pdf';
                break;
        }

        if ($asImage) {

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            if ($extension == 'pdf') {
//                $fileName = $this->hash . '.png';
//                $filePath = $dir . $fileName;
//                $filePathAbsolute = $basePath . $filePath;

                // temporary save image to read just first page of PDF
                try {
                    @file_put_contents($basePath . $temp . $this->hash . '.pdf', $label);
                    $im->readImage($basePath . $temp . $this->hash . '.pdf[0]');
                } catch (Exception $ex) {
                    return false;
                }

                $im->setImageFormat('png8');
                $im->trimImage(0);

                if ($im->getImageHeight() < $im->getImageWidth()) {
                    $im->rotateImage(new ImagickPixel(), 90);
                }

                if ($im->getImageAlphaChannel())
                {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }

                $im->stripImage();
                @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

//				$additionalImages = 0;
//				// NOW TRY TO SAVE ADDITIONAL PAGES - MAYBE THERE ARE SOME?
//				// limit to 5 just to prevent loop
//				while ($additionalImages < 5) {
//					$additionalImages++;
//					try {
//						$im->readImage($basePath . $temp . $this->hash . '.pdf[' . $additionalImages . ']');
//
//						$fileName = $this->hash . '_' . $additionalImages . '.png';
//						$filePathAdditional = $dir . $fileName;
//						$filePathAbsolute = $basePath . $filePathAdditional;
//
//						$im->setImageFormat('png');
//						$im->trimImage(0);
//
//						if ($im->getImageHeight() < $im->getImageWidth()) {
//							$im->rotateImage(new ImagickPixel(), 90);
//						}
//						$im->writeImage($filePathAbsolute);
//					} catch (Exception $ex) {
//						$additionalImages--; // last try was failure, so correct number o images
//						break;
//					}
//				}
//				$this->_additional_pages = $additionalImages;

                @unlink($basePath . $temp . $this->hash . '.pdf');

            } else {
                $fileName = $this->hash . '.png';
                $filePath = $dir . $fileName;
                $filePathAbsolute = $basePath . $filePath;

                $im->readImageBlob($label);

                $im->setImageFormat('png8');
                $im->trimImage(0);

                if ($im->getImageHeight() < $im->getImageWidth()) {
                    $im->rotateImage(new ImagickPixel(), 90);
                }

                if ($im->getImageAlphaChannel())
                {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }

                $im->stripImage();
                @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

            }


            $this->file_path = $filePath;
            $this->file_type = 'image/png';
        } else {


            $fileName = $this->hash . '.' . $extension;

            $filePath = $dir . $fileName;
            $filePathAbsolute = $basePath . $filePath;
            @file_put_contents($filePathAbsolute, $label);

            $this->file_path = $filePath;
            $this->file_type = $mime_type;

        }

    }

    public function handleAnnotations($label)
    {
        if($this->postal->user->getAnnotationMode())
        {
            $text = [];

            if($this->postal->user->getAnnotationText())
                $text[] = $this->postal->user->getAnnotationText();

            if($this->postal->user->getAnnotationAttribute())
            {
                switch($this->postal->user->getAnnotationAttribute())
                {
                    case LabelAnnotor::ATTR_CONTENT:
                        if($this->postal->content != '')
                            $text[] = $this->postal->content;
                        break;
                    case LabelAnnotor::ATTR_REF:
                        if($this->postal->ref != '')
                            $text[] = $this->postal->ref;
                        break;
                    case LabelAnnotor::ATTR_NOTE1:
                        if($this->postal->note1 != '')
                            $text[] = $this->postal->note1;
                        break;
                    case LabelAnnotor::ATTR_NOTE2:
                        if($this->postal->note2 != '')
                            $text[] = $this->postal->note2;
                        break;
                }
            }

            if(count($text))
                return LabelAnnotor::annotateLabel($label, $this->postal->user->getAnnotationMode(), implode($text, ' ||| '));
        }

        return $label;
    }

    public function saveLabelDefault($label)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

//        $basePath = Yii::app()->basePath . '/../';
//        $dir = 'uplabelsPostal/';

//        $fileName = $this->hash . '.png';
//        $filePath = $dir . $fileName;
//        $filePathAbsolute = $basePath . $filePath;

        // label is already png8
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($label));

        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }



    public function savePocztaPolskaLabel($label, $omitLabel = false)
    {
        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);


//        $basePath = Yii::app()->basePath . '/../';
//        $dir = 'uplabelsPostal/';
//        $temp = $dir . 'temp/';

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

//        $fileName = $this->hash . '.png';
//        $filePath = $dir . $fileName;
//        $filePathAbsolute = $basePath . $filePath;

        if($omitLabel)
        {
            $data = 'iVBORw0KGgoAAAANSUhEUgAAAfQAAABkCAMAAABHGTJPAAAAM1BMVEX///8AAADU1NRnZ2ejo6P19fUkJCTq6uq8vLzf399UVFQ/Pz+Hh4d4eHiwsLDIyMiWlpZ9gKdNAAAKu0lEQVR42uya6ZKjIBhFuewKat7/aScC8YbQGsepqalyOH+6XVgPfCJGdDqdTqfT6XQ6nU6n0+l0Op1Op9PpdDqdTqfT+RdIKZ0gg5SGR27RWi+uSeSkHH7Oa6ju8vrJLA1zJ6lYuRVHeNq8LvN0c6fXPuXkeG/dLiNnvUjD0snzrCQss27bkK7cCgDB8FABW8f6EZngRY0GlGhYAFgeSmxMw5Y70bn0xiNPyzVpfbrWEZEZF2FszrHgAayFLopVUPhEivqQjSAmX7kVpftb6cMIMg4npCcFy7t04q9Kh9yV7rExCzEDYEwKwKNUqXBSus6NIP6m0jG00gcLWO1e88kO36VbWGCi9OLILCOApeSu9At5TnowjXQGlmlZn0HRuuw50hTsM90DwGN4NsFP4XlSJxQQdMatWUb9wr2kR0HGPKhuBRCAsZHuLDAaITgA3K50StAWMO/SmetY/mpBTkhfZ+yOdMtLJpfPqR6SpwGctKaufFsBXp0A3u3SsRa3AlAKmD+lq+Kc1tU36REYJsA30nPvm0vSHxYYfpQu+exl3eNWv7EIFOSU9AjMbEQKFv6O0iU4jxWXM8PHg01+kW5h1/umWjr79pJ0Xfy10uemDvI11Y3N6TlFz0tXgHsfKwGjvKP0dXRPtXROGsbLeCx9We8wAMzP0t016TlS19I5m2umVEnOcAU8LkgXoWrELNfsbgUA4QAs79IdJzrnlT2WHlNQHAHfSl+AcO2ZroRnHKrDe5vW5bFlbEmhAWsuSF9DOls1pGrcCgDsnSw9SbJtfw6H0m3KYgamRroJwHJRekqkWFcSwHcKPoBjqp3eKj2as9JZhmQQsRjFTaWvVh6U/pNUC8gd6YzuuZ9NLX2Y7RqhL0vnGrx9ZUN0gqSHuTMWgYMAVpvfk65SY3O+fq25u6f01IGS0usVEH0dSI/FDOO7xMYot0zIOelZnTW7mzNqqYVGDSys1UocDqQTtUmPr1E6JfuAFbcCQNExUrr6bekmdwzjey3dX5Ze5u+DdSVDzi8wCxNg7XvdlpB9upPSTfqzAGNp1VjKvRUAXpsx+g+k+xTdGd+LdP1kCgCUaXbkzkpnHGo7X0asPOrZ76qaKTyxfld61AXPIi3gSnS/sfS0lnPH0v2B9OkVUxnfuZBb7OUdOZUL359xRlsAun23JC6C5Rw901lkie8jYO4sfe2t6WAhh8OFnME7I6VzR8//gXQHYN7rfDdycrOICmmBcFa6Fq/4boCpdM0g7gSA9/femDrBV91Lg7vSPSrcZ5IHMF2VzjfuwAw/1+zznnRWfzgjfU7JS3z3JWipu31mo5m4jm6d2sfdGlrDeCB9AiZdWBVQOjfKr0rnp1L1KZ01V3vSmV4fSudFvW01TbDi5tKNBXyWLkZ2DK/tSzcAXDU+Wum4KJ0ZuD3p+rt09ZvSlzU0AfHu0oUHQpHuq6nOV+Ud6f49DgxlBMg6a3VROmdzpPR2I25XOpeXZ6Q/XscWdnn9P93tgzrN5EUyN2nsQKlgs1vp7BV+zKZ0RueL0hlrxv1nuv/+THcnpHNSR0AhlBR3+8z2Lt3hCX9EYSVXN4htPG2jO+N7Jd1M6Ybr0rlWfPOoTcldAcH8JH32IjOkV5Oz0rnJ+/gPpK+te/+5FJSXUs5hdW4+pFu1wehexfdVunyyRAsugwPT5dLH7djvSKcPoF696UUuD1tW5q30ADtpKX0Eh0UrnRV4FOmJlO3/IF0EjnynsGF1s3ICYXSv4rsEsX7zRnLpRB9Ld7X0ESvch22lGxDlvm7DptOhfpnJ6Lt9UGdHNh+p5WSxMs5GHElndK/iO6WrnMF16SyVR8aX6ilvdraL3ZwLtNMiyKF0FrFsI/mG31YPcVJKcZlBrjjx9xikEV+Q93rb6nQ6nU6n0+l0Op1Op9P51d7ZLbEJAmG0ElGJv+//tFUhHpEaYOy0pbPfTRLiIu4Jq0FYRSKRSCQSiUQikUgkEolE/4S0y675Z8Wef3OlSsV3WeyitkuaRMP8tM+i5SrQD+V9tPOKnfpPPohXsCai9ibHV13a7Gdfoe372PPSMrkPMRPuZqVLuDcz2MMZO9tytNfT9RVH25WYKtaHflm/MmwLPBOgewWkF6mGG+h6d2kW9Htb1HS/BfqCsQ6guwLUFDgj2pvsbUj1wHxY913DPPX9i9cht+WslJt5Pn2gwwzoLFZo86Df205qVd27CBC0LhP6sPfxtcKxGoOJ/Yst6Ge1qd7DzLu4zh44tsOlb/oqLrqsR6SANSZN68J71WsfOhF6WVnlQI/b6vWbJmxdLvSZpXvaYH1TXTuWSB3HnnzxIpNuDnRWiNvtB0yA7tK3kKorEXrcVtu3D6GPRDqsvxR01WZQlkLHGlvEWsBk6Kwptdu7hHNAx081qbrSoEdsaeJT6L19iUPnY3HJKfDZddHhwrElQscndnv3FuhEaEOqrmTooW0IfXoMvcqBTmqbohQ61nXxlnN7NnTX040N9UAn5Rjr2JOhx23XvZln0LmQyYDeFZdbDp+haVvYyQk5E/r7SCSktpqaFuikHGNNcyL0iC0wHkF30brLgq6Li+++Y+E2kIMpD/pEwl+11zQCnQhtX6Z06FHbdtl+qs+h64aMo1+ho760lCTn/+kLDDepr9BZSo6fresbA/Tti9mHrlc45CnJgR7acgD9+jrqX7QuE7pbjV+N6pcL8A3VoVdpa9YrdDqUgeG0G+ioPvn5vb72hjwOdlBPA92GgoWcFanQ721pvrppXQ50VuP38wEdqf8FOj3dg97/yOvpeBnojPAB/W0jNDE6A3poS0+3Xf1hT7dSY2Wr+397euhvBb70c7obq60WoPNUNC/xek8eqmToUVs92ecLPTqns6c94+g7tL6BPv8oSRfHkgDKXnfnXshN5xsuihE+oE+247hzgUmGHrc1zfPBGaRHTg4R6MU9rM1vMHlVNpeMWdDhoj3oZv8PNH3MN1xoyYF+b0vyu0fQzfWfQp8C3RSXJBifeXfRWzsamwOdKNEB3Tmp18qZt6cTbr9+kQo9ZsvlXRJ0RpGoHCtgmgToS3G5h66O5ZbDsrHKhY4LgK771XSDTuJ//GwSoUdsMUiHPvG/jwcKBfv/Cp0BzKIU+lvZAM1jahOh47zmgE6NszUnR6f7sGRAv7elp+s06OS48+4T+Yr2dM4CZSn099YvubOeBZ0Y66DjF2vOwx4YvUuFHretM87pJCzlnMbOGFOPQh+q0q7dPZ/hOTi/no29EwFt2CBCE6MTocdtVeLVO1QZZtdHrutZn34GSwx6+6qKu8fGg0t2dfZQuxPK+Q56fUgFSfyHAzq8rDkRmhh9aQRWfvG9rdphDXYOS9A6+zumwKvCPmlZd8eD3XTVDN36ztSNq863bk/Q226oSmQepEkcubphDDWEflYNdE7qQLfWdjsiNDH6a7JIVMdsLfOwdUEmU6TfFDfGtj54Urxvra4FfWmxPYBOblCmQKdD52p2BjoXTTUR2ovRidAjtjz8Kw4d1Y1DPmjX+un9oclMgFvozVgg8tVDZ5k9m+cluSdv24OrZ9XaAoz2IsPKBwpdJciaK8QG1+I7267eNCl927rWK/jhy0ybsb+SYa4n9uVb661gcrtsf4hEIpFIJBKJRCKRSCQSiUQikUgkEolEIpHob+snJcRPOZGvcQEAAAAASUVORK5CYII=';
            $data = base64_decode($data);
            @file_put_contents($filePathAbsolute, $data);

        } else {

            // temporary save image to read just first page of PDF
            try {
                @file_put_contents($basePath . $temp . $this->hash . '.pdf', $label);
                $im->readImage($basePath . $temp . $this->hash . '.pdf[0]');
            } catch (Exception $ex) {
                return false;
            }

            $im->setImageFormat('png8');
            $im->trimImage(0);

            if ($im->getImageHeight() < $im->getImageWidth()) {
                $im->rotateImage(new ImagickPixel(), 90);
            }

            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }

            $im->stripImage();
            @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));


            @unlink($basePath . $temp . $this->hash . '.pdf');
        }


        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }

    public function saveDhlDeLabel($label)
    {
        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

//        $basePath = Yii::app()->basePath . '/../';
//        $dir = 'uplabelsPostal/';
//        $temp = $dir . 'temp/';

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

//        $fileName = $this->hash . '.png';
//        $filePath = $dir . $fileName;
//        $filePathAbsolute = $basePath . $filePath;

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel())
        {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->cropImage(1195, 2305, 280, 75);

        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));


        @unlink($basePath . $temp . $this->hash . '.pdf');

        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }


    public function saveHuPostLabel($label)
    {
        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

//        $basePath = Yii::app()->basePath . '/../';
//        $dir = 'uplabelsPostal/';
//        $temp = $dir . 'temp/';

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

//        $fileName = $this->hash . '.png';
//        $filePath = $dir . $fileName;
//        $filePathAbsolute = $basePath . $filePath;

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel())
        {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }

    public function saveRoyalMailLabel($label)
    {
        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);


        $label = base64_decode($label);

//        $basePath = Yii::app()->basePath . '/../';
//        $dir = 'uplabelsPostal/';
//        $temp = $dir . 'temp/';

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

//        $fileName = $this->hash . '.png';
//        $filePath = $dir . $fileName;
//        $filePathAbsolute = $basePath . $filePath;

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel())
        {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));


        @unlink($basePath . $temp . $this->hash . '.pdf');

        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }





    /**
     * Async call for label ordering. Allow ordering label in background
     * @param int $id Id of PostalLabelNew to be called
     * @param int $counter Limit number of operations
     * @param int $mdelay Time to wait before label ordering [seconds]
     */
    public static function asyncCallForId($id, $counter = 0)
    {

        $uniqId = uniqid();

        $ch = curl_init();
        $url = Yii::app()->createAbsoluteUrl('labelPostalQueque/index', array('id' => $id, 'async' => 'true', 'counter' => $counter), 'http');

        if(!YII_LOCAL)
            $url = str_replace(PageVersion::getDomainsList(true), 'swiatprzesylek.pl', $url);

        MyDump::dump('postal_async_call.txt', $uniqId.' : '. print_r($url,1));
        MyDump::dump('postal_async_call.txt', $uniqId.' : ID, COUNTER = '.print_r($id,1).' : '.print_r($counter,1));

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $resp = curl_exec($ch);

        MyDump::dump('postal_async_call.txt',$uniqId.' : RESP = '. print_r($resp,1));
        MyDump::dump('postal_async_call.txt', $uniqId.' : CURL_ERRO = '.print_r(curl_error($ch),1));

        // file_put_contents('test.txt',print_r(curl_error($ch),1), FILE_APPEND);
        curl_close($ch);
    }


    public function deleteLabel()
    {
        if ($this->file_path) {
            if (is_file($this->file_path))
                @unlink($this->file_path);

            $this->file_path = NULL;
            $this->file_type = NULL;

            return $this->update(array('file_path', 'file_type'));
        }
    }

    public static function expireOldLabels()
    {
        $toArchive = self::model()->findAll('date_updated < (NOW() - INTERVAL :days DAY) AND stat = :stat LIMIT 50', [
            ':days' => self::EXPIRATION_DAYS,
            ':stat' => self::STAT_SUCCESS
        ]);

        /* @var $item self */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {

                $item->expireLabel();

            }
    }

    public function expireLabel()
    {
        $labelPath = Yii::app()->basePath . '/../' . $this->file_path;

        $this->stat = self::STAT_EXPIRED;

        $this->update(['stat', 'date_updated']);

        if (!$this->postal->isLabelArchived()) {
            $this->postal->archive += Postal::ARCHIVED_LABEL_EXPIRED;
            $this->postal->update(['archive']);
        }

        if ($this->_additional_pages > 0) {
            for ($i = 1; $i <= $this->_additional_pages; $i++) {
                $path = explode('.', $this->file_path);
                $lastElementBeforeExtension = S_Useful::sizeof($path) - 2;

                if ($lastElementBeforeExtension >= 0) {
                    $path[$lastElementBeforeExtension] = $path[$lastElementBeforeExtension] . '_' . $i;
                    $path = implode('.', $path);

                    $additionalPAth = Yii::app()->basePath . '/../' . $path;
                    @unlink($additionalPAth);
                }
            }
        }

        @unlink($labelPath);
    }

    /**
     * Returns label as string from image
     * @param bool|false $noError
     * @return bool|string|array
     * @throws Exception
     */
    public function getFileAsString($noError = false, $rotate = false)
    {

        if (!$noError) {
            if ($this->stat == self::STAT_NEW) {
                throw new Exception(Yii::t('postal', 'Nie możemy wygenerować Twojej etykiety - nie została ona jeszcze przetworzona przez operatora!').' #'.$this->postal->local_id);
            } else if ($this->stat == self::STAT_FAILED) {

                $image = ProblemLabel::generatePostal($this->postal, $this->log);
                return $image;

            } else if ($this->stat == self::STAT_EXPIRED) {
                throw new Exception(Yii::t('postal', 'Etykieta dla tej paczki już wygasła').' #'.$this->postal->local_id);
            }
        }

        if($noError && $this->stat == self::STAT_EXPIRED)
            return false;

        if($this->postal->postal_stat_id == PostalStat::CANCELLED_PROBLEM)
        {
            $image = ProblemLabel::generatePostal($this->postal, $this->log);
            return $image;
        }

        try {

            if (@exif_imagetype(Yii::app()->basePath . '/../' . $this->file_path)) {

                if(!$rotate)
                {
                    // TEMP SOLUTION AFTER UPDATE
                    $imagick = ImagickMine::newInstance();
                    $imagick->readImage(Yii::app()->basePath . '/../' . $this->file_path);
                    $imagick->stripImage();
                    return $imagick->getImageBlob();

//                    return @file_get_contents($this->file_path);
                }
                else {

                    $imagick = ImagickMine::newInstance();
                    $imagick->readImage(Yii::app()->basePath . '/../' . $this->file_path);
                    $imagick->rotateImage(new ImagickPixel(), -90);
                    $imagick->stripImage();

                    return $imagick->getImageBlob();
                }
            }

//            $imagick = ImagickMine::newInstance();
//            $imagick->readImage(Yii::app()->basePath . '/../' . $this->file_path);
//
//            if ($imagick->valid())
//            {
//                if(!$rotate) {
//                    // TEMP SOLUTION AFTER MIGRATING TO NEW SERVER
//                    $imagick->stripImage();
//                    return $imagick->getImageBlob();
//
//                //                    return @file_get_contents($this->file_path);
//                }
//                else {
//                    $imagick->rotateImage(new ImagickPixel(), -90);
//                    $imagick->stripImage();
//                    return $imagick->getImageBlob();
//                }
//            }

            if (!$noError)
                throw new Exception(Yii::t('postal', 'Wystąpił błąd przy generowaniu Twojej etykiety! Prosimy o kontakt w tej sprawie').' #'.$this->postal->local_id);
            else
                return false;
        } catch (Exception $ex) {
            if (!$noError)
                throw new Exception(Yii::t('postal', 'Wystąpił błąd przy generowaniu Twojej etykiety! Prosimy o kontakt w tej sprawie').' #'.$this->postal->local_id);
            else
                return false;
        }

    }


    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    /**
     * Update ebay order at ebay.com with package ID and update stat of CourierEbayImport item
     * @param bool|false $error
     */
    protected function updateEbayData($error = false)
    {
        // if ordering label failed, then do not call eBay and set item to Cancelled stat
        if ($error) {
            $ebayStat = EbayImport::STAT_CANCELLED;
            Postal::updateEbayData($this->postal,'','', $this->postal->_ebay_order_id, true);

        } else {
//			$operator = 'SwiatPrzesylek.pl'; // default, just in case

            // otherwise, get second label track id and operator
            $trackId = $this->track_id;

            $this->postalOperator->name;
            $operator = $this->postalOperator->name;

            if(Postal::updateEbayData($this->postal, $operator, $trackId, $this->postal->_ebay_order_id, false))
                $ebayStat = EbayImport::STAT_SUCCESS;
            else
                $ebayStat = EbayImport::STAT_ERROR;

        }

        $this->postal->__temp_ebay_update_stat = $ebayStat;
    }


    protected static function getLabelsBasePath($hash)
    {
        $basePath = Yii::app()->basePath . '/../';
        $dir = 'uplabelsPostal/';

        $temp = $dir . 'temp/';

        $dir .= date('Ymd').'/';

        if (!file_exists($basePath.$dir)) {
            mkdir($basePath.$dir, 0777, false);
        }

        $extension = 'png';
        $mime_type = 'image/png';

        $fileName = $hash . '.' . $extension;

        $filePath = $dir . $fileName;
        $filePathAbsolute = $basePath . $filePath;

        return [$filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp];
    }

}