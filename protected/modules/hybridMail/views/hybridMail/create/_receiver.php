<div id="_receiver_<?php echo $i; ?>">
    <h3><?php echo Yii::t('hybridMail','Odbiorca nr {number}',array('{number}' => ($i+1)));?></h3>

    <?php

    $this->renderPartial('_addressData/_form',
        array('form' => $form,
            'model' => $model,
            'model2' => null,
            'i' => $i,
            'saveToContactBook' => $saveToContactBook,
            'type' => $type,
            'countries' => $countries,
        ));
    ?>
        <?php
    if($i > 0)
    {
        ?>

    <div class="row">
    <?php echo CHtml::label(Yii::t('hybridMail','Usuń tego odbiorcę'),''); ?>
    <?php echo TbHtml::button(Yii::t('app','Usuń'),array('onClick'=>'$("#_receiver_'.$i.'").remove();'));?>
</div><!-- row -->
<?php
    }
    ?>

    <div class="separator"></div>
</div>