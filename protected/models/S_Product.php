<?php

/**
 * S_Product class.
 */
class S_Product
{
    public $name;
    public $id;
    public $type;
    public $url;
    public $adminUrl;
    public $symbol;
    public $value_netto;
    public $value_brutto;

    /* @var $orderProduct OrderProduct */
    /* @var $courier Courier */
    /* @var $hybridMail HybridMail */
    public function __construct(OrderProduct $orderProduct, $new = false)
    {
        Yii::app()->getModule('courier');
        Yii::app()->getModule('postal');
        Yii::app()->getModule('hybridMail');
        Yii::app()->getModule('internationalMail');

        
        
        /* @var $order Order */
        switch($orderProduct->type)
        {
            case OrderProduct::TYPE_COURIER:
                $model = Courier::model()->findByPk($orderProduct->type_item_id);
                $this->name = Yii::t('order', 'Przesyłka kurierska').' #'.$model->local_id;
                $this->id = $model->local_id;
                $this->url = Yii::app()->createAbsoluteUrl('/courier/courier/view', array('urlData' => $model->hash, 'new' => $new));
                $this->adminUrl = Yii::app()->createAbsoluteUrl('courier/courier/view', array('id' => $model->id));
                $this->type = 1;
                $this->symbol = 'C';
                $this->value_brutto = $orderProduct->value_brutto;
                $this->value_netto = $orderProduct->value_netto;
                break;
            case Order::HYBRID_MAIL:
                $model = HybridMail::model()->findByPk($orderProduct->type_item_id);
                $this->name = 'HybridMail #'.$model->local_id;
                $this->id = $model->local_id;
                $this->url = Yii::app()->createAbsoluteUrl('/hybridMail/hybridMail/view', array('urlData' => $model->hash));
                $this->adminUrl = Yii::app()->createAbsoluteUrl('/hybridMail/hybridMail/view', array('id' => $model->id));
                $this->type = 1;
                $this->type = 'HM';
                $this->value_brutto = $orderProduct->value_brutto;
                $this->value_netto = $orderProduct->value_netto;
                break;
            case Order::INTERNATIONAL_MAIL:
                $model = InternationalMail::model()->findByPk($orderProduct->type_item_id);
                $this->name = 'InternationalMail #'.$model->local_id;
                $this->id = $model->local_id;
                $this->url = Yii::app()->createAbsoluteUrl('internationalMail/internationalMail/view', array('urlData' => $model->hash));
                $this->adminUrl = Yii::app()->createAbsoluteUrl('internationalMail/internationalMail/view', array('id' => $model->id));
                $this->type = 1;
                $this->symbol = 'IM';
                $this->value_brutto = $orderProduct->value_brutto;
                $this->value_netto = $orderProduct->value_netto;
                break;
            case Order::POSTAL:
                $model = Postal::model()->findByPk($orderProduct->type_item_id);
                $this->name = Yii::t('order', 'Postal').' #'.$model->local_id;
                $this->id = $model->local_id;
                $this->url = Yii::app()->createAbsoluteUrl('postal/postal/view', array('urlData' => $model->hash));
                $this->adminUrl = Yii::app()->createAbsoluteUrl('postal/postal/view', array('id' => $model->id));
                $this->type = 1;
                $this->symbol = 'PO';
                $this->value_brutto = $orderProduct->value_brutto;
                $this->value_netto = $orderProduct->value_netto;
                break;
        }

    }

    public static function model($order)
    {
        $S_Product = new S_Product($order);
        return $S_Product;
    }

}