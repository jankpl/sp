<?php

class MultipleSelect extends CWidget
{

    public $selector;
    public $afterAjaxUpdate = false;

    public $withChosenPlugin = false;
    public $chosenSelector = false;

    public $additionScript = false;

    public function run()
    {
        $this->render('multipleSelect/js', array(
            'selector' => $this->selector,
            'afterAjaxUpdate' => $this->afterAjaxUpdate,
            'withChosenPlugin' => $this->withChosenPlugin,
            'chosenSelector' => $this->chosenSelector,
            'additionScript' => $this->additionScript,
        ));
    }

}