<?php

Yii::import('application.modules.postal.models._base.BasePostalUserHasUserGroup');

class PostalUserHasUserGroup extends BasePostalUserHasUserGroup
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}