<?php
/* @var $model ContactForm */
?>
<?php
$this->homePage = true;
$branches = [];

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/aos/dist/aos.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/aos/dist/aos.css');

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/home.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/v3/home.min.css');
?>



<?php
$hour = date('G');
if($hour >= 6 && $hour < 16)
    $video = '1';
else if($hour >= 16 && $hour < 22)
    $video = '2';
else
    $video = '3';

$style = '';
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
    PageVersion::PAGEVERSION_ORANGE_POST,
]))
    $style = 'background-image: url(\''.Yii::app()->baseUrl.'/images/v3/bg_'.$video.'.jpg\'); min-height: 600px; width: 100%; position: relative; background-color: #5F767D; background-position: bottom;';

?>

<div class="section-main" id="home-page" style="<?= $style;?>">
    <?php
    if(in_array(Yii::app()->PV->get(), [
        PageVersion::PAGEVERSION_DEFAULT,
        PageVersion::PAGEVERSION_RS,
        PageVersion::PAGEVERSION_ORANGE_POST,
    ])):
        ?>
        <video autoplay muted loop id="bg_video" data-current-vid="<?= Yii::app()->baseUrl;?>/images/v3/<?= $video.'.mp4';?>"></video>
    <?php
    endif;
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="home-send" style="position: relative; z-index: 40;">
                    <a href="<?php echo Yii::app()->createUrl('courier/create');?>" id="home-send-top">
                        <p class="top"><?php echo Yii::t('site','Wyślij');?> <?php echo Yii::t('site','paczkę');?></p>
                        <p class="bottom">&gt; <img src="<?= Yii::app()->baseUrl;?>/theme/ruch/images/home-ico-courier.png"/> &gt;</p>
                    </a>
                    <div id="home-send-bottom">
                        <a href="<?php echo Yii::app()->createUrl('/postal/');?>"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-postal.png"/><br/><?php echo Yii::t('site','przesyłkę pocztową');?></a>
                        <?php if(!in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_RUCH, PageVersion::PAGEVERSION_QURIERS, PageVersion::PAGEVERSION_PLI])): ?><a href="<?php echo Yii::app()->createUrl('/palette/');?>"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-palette.png"/><br/><?php echo Yii::t('site','paletę');?></a><?php endif; ?>
                        <?php if(0 && !in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_RUCH, PageVersion::PAGEVERSION_QURIERS, PageVersion::PAGEVERSION_PLI])): ?><a href="<?php echo Yii::app()->createUrl('/hybridMail/');?>"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-hm.png"/><br/><?php echo Yii::t('site','e-list');?></a><?php endif; ?>
                    </div>
                </div>

                <div id="home-right" style="position: relative; z-index: 40;">
                    <div id="main-tt-block" style="position: relative;">
                        <?php
                        if(in_array(Yii::app()->PV->get(), [
                            PageVersion::PAGEVERSION_DEFAULT,
                            PageVersion::PAGEVERSION_RS,
                            PageVersion::PAGEVERSION_DE,
                        ])):
                            ?>
                            <a href="https://carrierlist.17track.net/en/international/100033" target="_blank"><img src="<?= Yii::app()->baseUrl;?>/images/misc/17track.png" style="position: absolute; right: 5px; top: 5px; width: 45%;"/></a>
                        <?php
                        endif;
                        ?>
                        <h2><?= Yii::t('site', 'Śledzenie przesyłki');?></h2>
                        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'tt-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation'=> false,
                            'action' => Yii::app()->urlManager->createUrl('/site/tt'),
                        ));
                        ?>

                        <?php echo CHtml::textField('id', '', ['placeholder' => Yii::t('site', 'Wprowadź nr przesyłki')]); ?>
                        <?php
                        echo TbHtml::submitButton('&gt;');
                        $this->endWidget();
                        ?>

                    </div>

                    <?php
                    if(in_array(Yii::app()->PV->get(), [
                        PageVersion::PAGEVERSION_DEFAULT,
                        PageVersion::PAGEVERSION_RS,
                        PageVersion::PAGEVERSION_PLI,
                        PageVersion::PAGEVERSION_CQL,
                        PageVersion::PAGEVERSION_DE,
                    ])):
                        if(Yii::app()->user->isGuest OR !Yii::app()->user->model->getHidePrices()):
                            Yii::app()->getModule('postal');
                            $countryList = CountryList::getActiveCountries(true);
                            ?>
                            <div id="main-pricing-block">
                                <div id="main-pricing-block-list">
                                    <a href="#" data-pricing-show="courier"><img src="<?= Yii::app()->baseUrl;?>/theme/ruch/images/home-ico-courier.png"/></a>
                                    <a href="#" data-pricing-show="postal"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-postal.png"/></a>
                                    <?php
                                    if(0 && in_array(Yii::app()->PV->get(), [
                                            PageVersion::PAGEVERSION_DEFAULT,
                                            PageVersion::PAGEVERSION_RS,
                                        ])):
                                        ?>
                                        <a href="#" data-pricing-show="hm"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-hm.png"/></a>
                                    <?php
                                    endif;
                                    ?>
                                </div>

                                <?php
                                $force_uid = 0;
                                if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI)
                                    $force_uid = User::PLI_GENERIC_USER_ID;
                                else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                                    $force_uid = User::CQL_GENERIC_USER_ID;
                                ?>


                                <div class="main-pricing-courier main-pricing-type" data-pricing="courier" data-force-uid="<?= $force_uid;?>" data-force-currency="<?= in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_PLI, PageVersion::PAGEVERSION_CQL]) && Yii::app()->user->isGuest ? Currency::EUR_ID : '';?>">
                                    <h2><?= Yii::t('home','Kalkulator ceny');?></h2>
                                    <p class="main-pricing-subtype"><?= Yii::t('home','kurierzy');?></p>
                                    <div class="pricing-labels">
                                        <span><?= Yii::t('home','waga');?> [kg]</span>
                                        <span><?= Yii::t('home','wymiary');?> [cm]</span>

                                    </div>
                                    <div>
                                        <input type="text" id="pricing-courier-weight" class="pricing-courier-weight" data-pricing-input="true" value="2"/>
                                        <input type="text" id="pricing-courier-dim-l" class="pricing-courier-dim" data-pricing-input="true" value="20"/>x
                                        <input type="text" id="pricing-courier-dim-w" class="pricing-courier-dim" data-pricing-input="true" value="20"/>x
                                        <input type="text" id="pricing-courier-dim-d" class="pricing-courier-dim" data-pricing-input="true" value="10"/>
                                    </div>
                                    <div class="pricing-labels">
                                        <span><?= Yii::t('home','kraj nadawcy');?></span>
                                        <span><?= Yii::t('home','kraj odbiorcy');?></span>
                                    </div>
                                    <div>
                                        <?= CHtml::dropDownList('pricing_sender', in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_PLI, PageVersion::PAGEVERSION_CQL]) ? CountryList::COUNTRY_UK : '', $countryList, ['id' => 'pricing-courier-country-sender', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                                        <?= CHtml::dropDownList('pricing_receiver', in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_PLI, PageVersion::PAGEVERSION_CQL]) ? CountryList::COUNTRY_PL : CountryList::COUNTRY_DE, $countryList, ['id' => 'pricing-courier-country-receiver', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                                    </div>

                                    <div class="main-pricing-block-price">
                                        <div class="main-pricing-note">
                                            <?php echo TbHtml::tooltip('[?]', '#', Yii::t('home', 'Cena dotyczy nadania w punkcie/oddziale Świata Przesyłek')); ?>
                                        </div>

                                        <div class="pricing-currency-label"><?= Yii::t('home','cena');?> [<span>-</span>]</div>
                                        <div class="main-pricing-block-price-amount-loading"></div>
                                        <div class="main-pricing-block-price-amount">-</div>
                                        <div class="main-pricing-block-price-amount-brutto">(<span>-</span> <?= Yii::t('home','brutto');?>)</div>
                                        <div class="main-pricing-block-price-go" data-url="<?= Yii::app()->createUrl('/courier/courier/createFP');?>"><?= Yii::t('home','zamów');?></div>
                                    </div>

                                    <a class="main-pricing-block-contact" href="#home-contact-block"><p><?= Yii::t('home','Skontaktuj się z nami, aby uzyskać wycenę!');?><br/><span class="glyphicon glyphicon-envelope"></span></p></a>
                                </div>

                                <div class="main-pricing-postal main-pricing-type" data-pricing="postal" data-force-uid="<?= Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI ? User::PLI_GENERIC_USER_ID : 0;?>" data-force-currency="<?= Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI && Yii::app()->user->isGuest ? Currency::EUR_ID : '';?>">
                                    <h2><?= Yii::t('home','Kalkulator ceny');?></h2>
                                    <p class="main-pricing-subtype"><?= Yii::t('home','przesyłki pocztowe');?></p>
                                    <div class="pricing-labels">
                                        <span><?= Yii::t('home','typ');?></span>
                                        <span><?= Yii::t('home','gabaryt');?></span>
                                        <span><?= Yii::t('home','waga');?></span>
                                    </div>
                                    <div>
                                        <?= CHtml::dropDownList('pricing_postal_type', '', Postal::getPostalTypeList(), ['id' => 'pricing-postal-type', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                                        <?= CHtml::dropDownList('pricing_postal_size', '', Postal::getSizeClassList(), ['id' => 'pricing-postal-size', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                                        <?= CHtml::dropDownList('pricing_postal_weight', '', Postal::getWeightClassList(), ['id' => 'pricing-postal-weight', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                                    </div>
                                    <div class="pricing-labels">
                                        <span><?= Yii::t('home','kraj odbiorcy');?></span>
                                    </div>
                                    <div>
                                        <?= CHtml::dropDownList('pricing_receiver', '', $countryList, ['id' => 'pricing-postal-country-receiver', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                                    </div>

                                    <div class="main-pricing-block-price">
                                        <div class="pricing-currency-label"><?= Yii::t('home','cena');?> [<span>-</span>]</div>
                                        <div class="main-pricing-block-price-amount-loading"></div>
                                        <div class="main-pricing-block-price-amount">-</div>
                                        <div class="main-pricing-block-price-amount-brutto">(<span>-</span> <?= Yii::t('home','brutto');?>)</div>
                                        <div class="main-pricing-block-price-go" data-url="<?= Yii::app()->createUrl('/postal/postal/createFP');?>"><?= Yii::t('home','zamów');?></div>
                                    </div>

                                    <a class="main-pricing-block-contact" href="#home-contact-block"><p><?= Yii::t('home','Skontaktuj się z nami, aby uzyskać wycenę!');?><br/><span class="glyphicon glyphicon-envelope"></span></p></a>
                                </div>
                                <?php
                                Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/dimensionalWeight.js', CClientScript::POS_HEAD);
                                Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/home-pricing.min.js', CClientScript::POS_END);
                                Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
                                Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
                                ?>
                            </div>
                        <?php
                        endif;
                    endif;
                    ?>
                </div>




            </div>
        </div>
    </div>
</div>
<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
    PageVersion::PAGEVERSION_ORANGE_POST,
])):
    ?>
    <div class="division-line narrow"></div>


    <div class="section-2" style="background-image: url(<?= Yii::app()->baseUrl.'/images/v3/sp_fleet2.jpg';?>); background-position: center; background-repeat: no-repeat; background-size: cover; position: relative;">
        <div class="section-2-2">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div id="home-about-block">
                            <h2><?= Yii::t('home','O nas:');?></h2>
                            <p class="text-justify"><?php
                                $this->widget('ShowPageContent', array('id' => 34));
                                ?>
                            </p>
                            <p class="text-right"><a href="<?= Yii::app()->createUrl('/page/view', array('id' => 1));?>"><?= Yii::t('home','czytaj więcej');?> &gt;</a></p>
                        </div>
                    </div>
                    <?php
                    if(in_array(Yii::app()->PV->get(), [
                        PageVersion::PAGEVERSION_DEFAULT,
                        PageVersion::PAGEVERSION_RS,
                    ])):
                        ?>
                        <div class="col-xs-12 col-sm-6">
                            <div id="home-contact-block">
                                <h2><?= Yii::t('home','Kontakt:');?></h2>
                                <p class="home-tel"><?= Yii::t('home','tel.:');?> <span class="orange"><?= Yii::t('home','22 122 12 18');?></span></p>
                                <p class="home-email">e-mail: <?= Yii::t('home','info@swiatprzesylek.pl');?></p>

                                <?php if(Yii::app()->user->hasFlash('contact')): ?>
                                    <div class="alert alert-success">
                                        <?php echo Yii::app()->user->getFlash('contact'); ?>
                                    </div>
                                <?php else: ?>
                                    <h2 style="margin: 5px 0 0 0;"><?= Yii::t('home','Zostaw wiadomość:');?></h2>


                                    <div class="form" style="margin-right: 50px; position: relative;" id="contact-form-home">
                                        <?php
                                        /* @var $form TbActiveForm */
                                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                                            'id'=>'home-contact-form',
                                            'enableClientValidation'=>true,
                                            'clientOptions'=>array(
                                                'validateOnSubmit'=>false,
                                            ),
                                        )); ?>
                                        <?php echo $form->errorSummary($model); ?>
                                        <div>
                                            <?php echo $form->textField($model,'name', ['placeholder' => $model->getAttributeLabel('name')]); ?>
                                        </div>
                                        <div>
                                            <?php echo $form->textField($model,'email', ['placeholder' => $model->getAttributeLabel('email')]); ?>
                                        </div>
                                        <div>
                                            <?php echo $form->textArea($model,'body',['placeholder' => $model->getAttributeLabel('body')]); ?>
                                        </div>
                                        <div>
                                            <?php echo $form->checkbox($model,'accept'); ?> <?= $form->label($model, 'accept');?>
                                        </div>

                                        <?php echo $form->hiddenField($model, 'verifyCode', ['id' => 'recaptcha_token']); ?>
                                        <?php echo TbHtml::submitButton('&gt', ['class' => 'text-arrow']); ?>
                                        <?php $this->endWidget(); ?>
                                        <p style="margin: 0 0 5px 0; font-weight: 300; "><?= Yii::t('home','odpowiemy tak szybko, jak to możliwe');?></p>
                                    </div><!-- form -->
                                    <script src="https://www.google.com/recaptcha/api.js?render=<?= ContactForm::GC_PUBLIC_KEY;?>"></script>
                                    <script>
                                        $('#home-contact-form').on('submit', function(e){
                                            if($('#recaptcha_token').val() == '') {
                                                e.preventDefault();
                                                grecaptcha.ready(function () {
                                                    grecaptcha.execute('<?= ContactForm::GC_PUBLIC_KEY;?>', {action: 'homepage'}).then(function (token) {
                                                        $('#recaptcha_token').val(token);
                                                        $('#home-contact-form').submit();
                                                    });
                                                });
                                            }
                                        });
                                    </script>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php
endif;
?>
<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
])):
    ?>
    <div class="section-video" style="position: relative;">
        <div class="division-line">
            <div class="container">
                <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Zobacz film o nas');?></h2>
            </div>
        </div>
        <div class="tv">
            <div id="tv-muter-wrapper"><div id="tv-muter" class=="mute"><i class="fa fa-volume-off fa-5x"></i><i class="fa fa-volume-up fa-5x"></i></div></div>
            <div class="screen" id="tv">
            </div>
        </div>
    </div>

<?php
endif;
?>
<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
    PageVersion::PAGEVERSION_ORANGE_POST,
])):
    ?>

    <?php
    $spPoints = SpPoints::getPoints();
    $no = S_Useful::sizeof($spPoints);

    $CACHE_NAME = 'SP_POINTS_HOME_'.$no;
    $branches = Yii::app()->cache->get($CACHE_NAME);

    if($branches === false)
    {
        $branches = [];
        foreach($spPoints AS $item)
        {
            $address = $item->addressData->address_line_1.' '.$item->addressData->address_line_2.' '.$item->addressData->city.' '.$item->addressData->country0->code;

            $addressHuman = '';
            if($item->spPointsTr->title != $item->addressData->name)
                $addressHuman .= $item->addressData->name.'<br/>';

            $addressHuman .= $item->addressData->company.'<br/>';
            $addressHuman .= $item->addressData->address_line_1;
            $addressHuman .= $item->addressData->address_line_2 != '' ? "<br/>".$item->addressData->address_line_2 : '';
            $addressHuman .= "<br/>".$item->addressData->zip_code.', '.$item->addressData->city;
            $addressHuman .= "<br/>".strtoupper($item->addressData->country0->countryListTr->name);

            $geoResp = GoogleApi::getCoordinatesByAddress($address);

            $lat = $geoResp[0];
            $lng = $geoResp[1];

            $branches[] = [
                'id' => $item->id,
                'name' => $item->spPointsTr->title,
                'lat' => $lat,
                'lng' => $lng,
                'address' => $addressHuman,
                'desc' => $item->spPointsTr->text,
            ];
        }
        Yii::app()->cache->set($CACHE_NAME, $branches, 60 * 60 * 1);
    }

    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/leaflet/leaflet.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/leaflet/leaflet.css');
    ?>

    <div class="map-section" style="position: relative;">
        <div class="division-line">
            <div class="container">
                <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Nasze oddziały');?></h2>
            </div>
        </div>
        <div id="sp-points-map" style="height: 600px;"></div>
    </div>



<?php
endif;
?>
<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
])):
    ?>

    <div class="section-fleet section-resizable" style="background-image: url(<?= Yii::app()->baseUrl.'/images/v3/sp_fleet.jpg';?>); background-position: center; background-repeat: no-repeat; background-size: cover; position: relative; min-height: 600px;">
        <div class="division-line division-line-overlay">
            <div class="container">
                <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Własna sieć odbiorów i doręczeń');?></h2>
            </div>
        </div>

        <div class="container" style="position: relative; min-height: 600px;">
            <div class="home-info-block" style="float: left;">
                <div class="info-intro">
                    <?php
                    $this->widget('ShowPageContent', array('id' => 72));
                    ?>
                    <div class="text-right show-more-link"><?= Yii::t('home','dowiedz się wiecej');?> &gt;&gt;</div>
                </div>
                <div class="info-content">
                    <?php
                    $this->widget('ShowPageContent', array('id' => 70));
                    ?>
                </div>
            </div>
        </div>
    </div>


    <div id="section-sp-points" class="section-fleet section-resizable" style="background-image: url(<?= Yii::app()->baseUrl.'/images/v3/sp_points.jpg';?>); background-position: center; background-repeat: no-repeat; background-size: cover; position: relative;">
        <div class="division-line division-line-overlay">
            <div class="container">
                <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Punkty nadawcze na terenie całej Unii Europejskiej');?></h2>
            </div>
        </div>
        <div class="container" style="position: relative; min-height: 600px;">
            <div class="home-info-block">
                <div class="info-intro"><?php
                    $this->widget('ShowPageContent', array('id' => 73));
                    ?>
                    <div class="text-right show-more-link">&lt;&lt; <?= Yii::t('home','dowiedz się wiecej');?></div>
                </div>
                <div class="info-content">
                    <?php
                    $this->widget('ShowPageContent', array('id' => 71));
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="division-line">
        <div class="container">
            <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Szeroka gama usług dodatkowych');?></h2>
        </div>
    </div>
    <div class="section-linker text-center" style="background: white; padding: 30px 0;" id="additional_services">
        <div class="container" >
            <div class="row">
                <div class="col-xs-12">
                    <div id="additional_services_list">
                        <img src="<?= Yii::app()->baseUrl.'/images/v3/services.png';?>" style="margin: 0 auto;" class="img-responsive"/>
                        <div data-aos="zoom-in"  style=" left: 10%; top: 7%;"><?= Yii::t('home','Weryfikacja danych odbiorcy');?></div>
                        <div data-aos="zoom-in"  style=" left: 0%; top: 33%;"><?= Yii::t('home','Doręczenie w sobotę');?></div>
                        <div data-aos="zoom-in"  style=" left: 5%; top: 55%;"><?= Yii::t('home','Doręczenie o ustalonej godzinie');?></div>
                        <div data-aos="zoom-in"  style=" left: 6%; top: 93%;"><?= Yii::t('home','R.O.D. - dokument zwrotny');?></div>
                        <div data-aos="zoom-in"  style=" left: 47%; top: 91%;"><?= Yii::t('home','Powiadomienie e-mail');?></div>
                        <div data-aos="zoom-in"  style=" left: 83%; top: 92%;"><?= Yii::t('home','Powiadomienie SMS');?></div>
                        <div data-aos="zoom-in"  style=" left: 77%; top: 50%;"><?= Yii::t('home','Potwierdzenie doręczenia');?></div>
                        <div data-aos="zoom-in"  style=" left: 60%; top: 29%;"><?= Yii::t('home','Ubezpieczenie przesyłki do 200.000 PLN');?></div>
                        <div data-aos="zoom-in"  style=" left: 48%; top: 1%;"><?= Yii::t('home','Krajowe i międzynarodowe pobrania');?></div></div>
                </div>

            </div>
        </div>
    </div>


    <div class="division-line">
        <div class="container">
            <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Elastyczne rozwiązania zwrotów konsumenckich');?></h2>
        </div>
    </div>

    <div style="background: white;">
        <div class="container">

            <div class="row">

                <div class="col-xs-12 text-center">
                    <h4 style="text-align: center; color: white; background-color: #4E5F66; padding: 15px; "><?= Yii::t('home','Dzięki usługom zwrotów konsumenckich Świata Przesyłek');?></strong> <?= Yii::t('home','możesz zaoferować swoim klientom {strong}łatwy sposób zwrotu konsumenckiego/reklamacyjnego{/strong}', ['{strong}' => '<br/><strong>', '{/strong}' => '</strong>']);?>
                    </h4>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <br/>
                            <br/>
                            <br/>
                            <img src="<?= Yii::app()->baseUrl.'/images/v3/return_l2.png';?>" style="margin: 0 auto;" class="img-responsive"/>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <img src="<?= Yii::app()->baseUrl.'/images/v3/returns_map.png';?>" style="margin: 0 auto;" class="img-responsive "/>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <a class="orange-block-link" id="returns-more-button" style="cursor: pointer;">
                        <strong style="font-size: 1.5em;"><?= Yii::t('home','110 000 punktów zwrotu w całej UE', ['{br}' => '<br/>']);?></strong><br/>
                        <?= Yii::t('home','dowiedz się wiecej');?> &gt;
                    </a>


                    <div id="returns-more" style="display: none;">

                        <h3 style="text-align: center; color: white; background-color: #4E5F66; padding: 15px; font-weight: bold; font-family: Tahoma, Arial, sans-serif;"><?= Yii::t('home','Jak przekazać klientowi etykietę zwrotną?'); ?></h3>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="panel text-center">
                                    <div class="panel-heading" style="background-color: #E39E26; color: white; font-weight: bold;"><?= Yii::t('home','Włóż do paczki wraz z wysyłanym produktem...');?></div>
                                    <div class="panel-body" style=""><img src="<?= Yii::app()->baseUrl.'/images/v3/ret_put.png';?>" style="margin: 0 auto;" class="img-responsive"/></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="panel text-center">
                                    <div class="panel-heading" style="background-color: #E39E26; color: white; font-weight: bold;"><?= Yii::t('home','...lub prześlij/dołącz link w mailu');?></div>
                                    <div class="panel-body" style=""><img src="<?= Yii::app()->baseUrl.'/images/v3/ret_send.png';?>" style="margin: 0 auto;" class="img-responsive"/></div>
                                </div>
                            </div>
                        </div>

                        <h3 style="text-align: center; color: white; background-color: #4E5F66; padding: 15px; font-weight: bold; font-family: Tahoma, Arial, sans-serif;"><?= Yii::t('home','Śledzenie drogi zwrotu');?></h3>

                        <div class="row">
                            <div class="col-xs-12">
                                <br/>
                                <div style="position: relative; font-size: 1.2vh; display: inline-block; font-weight: bold;">
                                    <img src="<?= Yii::app()->baseUrl.'/images/v3/return2.png';?>" style="margin: 0 auto;" class="img-responsive"/>
                                    <div data-aos="flip-left" data-aos-easing="ease-out-cubic" style="position: absolute; left: 5%; top: 82%;"><?= Yii::t('home','Wydruk i dołączenie{br}etykiety zwrotnej', ['{br}' => '<br/>']);?></div>
                                    <div data-aos="flip-left" data-aos-easing="ease-out-cubic" style="position: absolute; left: 20%; top: 82%;"><?= Yii::t('home','Nadanie w placówce{br}pocztowej', ['{br}' => '<br/>']);?></div>
                                    <div data-aos="flip-left" data-aos-easing="ease-out-cubic" style="position: absolute; left: 31%; top: 29%;"><?= Yii::t('home','Procesowanie{br}w sortowni pocztowej', ['{br}' => '<br/>']);?></div>
                                    <div data-aos="flip-left" data-aos-easing="ease-out-cubic" style="position: absolute; left: 48%; top: 29%;"><?= Yii::t('home','Wyjazd{br}z sortowni', ['{br}' => '<br/>']);?></div>
                                    <div data-aos="flip-left" data-aos-easing="ease-out-cubic" style="position: absolute; left: 59%; top: 80%;"><?= Yii::t('home','Wjazd do sortowni{br}Świata Przesyłek', ['{br}' => '<br/>']);?></div>
                                    <div data-aos="flip-left" data-aos-easing="ease-out-cubic" style="position: absolute; left: 75%; top: 80%;"><?= Yii::t('home','Procesowanie zwrotu w{br}sortowni Świata Przesyłek', ['{br}' => '<br/>']);?></div>
                                    <div data-aos="flip-left" data-aos-easing="ease-out-cubic" style="position: absolute; left: 91%; top: 41%;"><?= Yii::t('home','Dostawa{br}do Ciebie', ['{br}' => '<br/>']);?></div>
                                </div>
                                <br/>
                            </div>
                        </div>

                        <h3 style="text-align: center; color: white; background-color: #4E5F66; padding: 15px; font-weight: bold; font-family: Tahoma, Arial, sans-serif;"><?= Yii::t('home','Zalety prostych rozwiązań zwrotów konsumenckich Świata Przesyłek');?></h3>

                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="panel text-center">
                                    <div class="panel-heading" style="background-color: #E39E26; color: white; font-weight: bold;"><?= Yii::t('home','Prostota');?></div>
                                    <div class="panel-body" style="background-color: #F9F9F9;"><?= Yii::t('home','Proste i szybkie generowanie etykiet zwrotnych w panelu Świata Przesyłek lub przez nasze API');?></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="panel text-center">
                                    <div class="panel-heading" style="background-color: #E39E26; color: white; font-weight: bold;"><?= Yii::t('home','Zasięg');?></div>
                                    <div class="panel-body" style="background-color: #F9F9F9;"><?= Yii::t('home','Ponad 110 tysięcy punktów zwrotu towaru w 26 krajach Unii Europejskiej I Szwajcarii');?></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="panel text-center">
                                    <div class="panel-heading" style="background-color: #E39E26; color: white; font-weight: bold;"><?= Yii::t('home','Wygoda');?></div>
                                    <div class="panel-body" style="background-color: #F9F9F9;"><?= Yii::t('home','Twój klient może zwrócić produkt w wybranej przez siebie placówce.');?></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <div class="panel text-center">
                                    <div class="panel-heading" style="background-color: #E39E26; color: white; font-weight: bold;"><?= Yii::t('home','Zysk');?></div>
                                    <div class="panel-body" style="background-color: #F9F9F9;"><?= Yii::t('home','Opcja zwrotów konsumenckich Świata Przesyłek przyciągnie więcej klientów do zakupów co pozwoli Ci wyprzedzić konkurencję i osiągnąć większy zysk.');?></div>
                                </div>
                            </div>
                        </div>

                        <div id="home-return-details">
                            <h3 style="text-align: center; color: white; background-color: #4E5F66; padding: 15px; font-weight: bold; font-family: Tahoma, Arial, sans-serif;"><?= Yii::t('home','Szczegółowe warunki zwrotów z 26 krajów Unii Europejskiej');?></h3>
                            <?php
                            $data = [['be', Yii::t('home','Belgium'), Yii::t('home','all retail outlets of bpost/ Parcelshops'), '1,300 / Plan: 2,000', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['bg', Yii::t('home','Bulgaria'), Yii::t('home','all retail outlets of Bulgarian post'), '130', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['dk', Yii::t('home','Denmark'), Yii::t('home','all retail outlets of Post Danmark'), '800', '15 x 11 x 1', '100 x 50 x 50', '31,5'],
                                ['es', Yii::t('home','Estonia'), Yii::t('home','all retail outlets of Eesti Post'), '170', '15 x 11 x 1', '105 x 60 x 60', '31,5'],
                                ['fi', Yii::t('home','Finland'), Yii::t('home','all retail outlets of Posti'), '1,1', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['fr', Yii::t('home','France'), Yii::t('home','all retail outlets of La Poste'), '17,074', '15 x 11 x 1', '120 x 60 x 60', '20'],
                                ['gr', Yii::t('home','Greece'), Yii::t('home','all retail outlets of Elta Post'), '2,086', '15 x 11 x 1', '100 x 50 x 50', '20'],
                                ['gb',  Yii::t('home','UK'), Yii::t('home','all retail outlets of Post Office R'), '> 12,000', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['ir', Yii::t('home','Ireland'), Yii::t('home','all retail outlets of An Post'), '1,4', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['it', Yii::t('home','Italy'), Yii::t('home','all retail outlets of Poste Italiane'), '13,991', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['hr', Yii::t('home','Croatia'), Yii::t('home','all retail outlets of Hrvatska pošta'), '1,016', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['lv', Yii::t('home','Latvia'), Yii::t('home','all retail outlets of Latvijas Pasts'), '620', '15 x 11 x 1', '100 x 50 x 50', '30'],
                                ['lt', Yii::t('home','Lithuania'), Yii::t('home','all retail outlets of Lietuvos Pasta'), '623', '15 x 11 x 1', '120 x 60 x 60', '20'],
                                ['lu', Yii::t('home','Luxembourg'), Yii::t('home','all retail outlets of EPT'), '> 100', '15 x 11 x 1', '120 x 60 x 60', '31,5'],
                                ['mt', Yii::t('home','Malta'), Yii::t('home','alle Filialen der MaltaPost'), '40', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['nl', Yii::t('home','Netherlands'), Yii::t('home','all retail outlets of Postnl'), '2,6', '15 x 11 x 1', '120 x 50 x 50', '30'],
                                ['au', Yii::t('home','Austria'), Yii::t('home','all retail outlets of Post,at/ DHL Parcel AT'), '1,650 / Plan: 2,000', '15 x 11 x 1', '100 x 60 x 60', '31,5'],
                                ['pl', Yii::t('home','Poland'), Yii::t('home','all retail outlets of Poczta Polska'), '8,24', '15 x 11 x 1', '120 x 60 x 60', '31,5'],
                                ['pt', Yii::t('home','Portugal'), Yii::t('home','all retail outlets of CTT Expresso'), '1', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['ro', Yii::t('home','Romania'), Yii::t('home','all retail outlets of Posta Romana'), '6,998', '15 x 11 x 1', '105 x 60 x 60', '20'],
                                ['se', Yii::t('home','Sweden'), Yii::t('home','all Servicepoints of DHL Sweden'), '1,3', '15 x 11 x 3,5', '120 x 50 x 50', '20'],
                                ['ch', Yii::t('home','Switzerland'), Yii::t('home','all retail outlets of Swiss Post'), '1,86', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['sk', Yii::t('home','Slovakia'), Yii::t('home','all retail outlets of Slovenska Posta'), '1,543', '20 x 15', '120 x 60 x 60', '30'],
                                ['si', Yii::t('home','Slovenia'), Yii::t('home','all retail outlets of Posta Slovenije'), '559', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['es', Yii::t('home','Spain'), Yii::t('home','all retail outlets of Correos'), '> 10,000', '15 x 11 x 1', '120 x 60 x 60', '30'],
                                ['cz', Yii::t('home','Czech Rep.'), Yii::t('home','selected retail outlets of Ceska Posta'), '3,179', '21,5 x 15,5', '120 x 60 x 60', '30'],
                                ['hu', Yii::t('home','Hungary'), Yii::t('home','all retail outlets of Magyar Posta'), '3,867', '15 x 11 x 1', '100 x 50 x 50', '20'],
                                ['cy', Yii::t('home','Cyprus'), Yii::t('home','all retail outlets of Cyprus Post'), '56', '9 x 14', '120 x 60 x 60', '31,5'],
                            ];
                            ?>
                            <table class="table table-striped table-condensed">
                                <tr>
                                    <th></th>
                                    <th><?= Yii::t('home', 'Country');?></th>
                                    <th><?= Yii::t('home', 'Type of posting location for the end customer');?></th>
                                    <th  class="text-center"><?= Yii::t('home', 'No. posting locations');?></th>
                                    <th class="text-center"><?= Yii::t('home', 'Min dimensions [cm]');?></th>
                                    <th class="text-center"><?= Yii::t('home', 'Max dimensions [cm]');?></th>
                                    <th class="text-center"><?= Yii::t('home', 'Max weight [kg]');?></th>
                                </tr>
                                <?php
                                foreach($data AS $item):
                                    ?>
                                    <tr>
                                        <td><img src="<?= Yii::app()->baseUrl;?>/images/flags/<?= $item[0];?>.png" style="height: 15px; float: right;"/></td>
                                        <td style="font-weight: bold;"><?= $item[1];?></td>
                                        <td><?= $item[2];?></td>
                                        <td  class="text-center"><?= $item[3];?></td>
                                        <td class="text-center"><?= $item[4];?></td>
                                        <td class="text-center"><?= $item[5];?></td>
                                        <td class="text-center"><?= $item[6];?></td>
                                    </tr>
                                <?php
                                endforeach;
                                ?>
                            </table>
                        </div>

                        <br/>
                        <br/>
                        <h4 style="text-align: center; color: white; background-color: #4E5F66; padding: 15px; "><?= Yii::t('home','W przypadku pytań czy wątpliwości dotyczących usługi zwrotów konsumenckich Świata Przesyłek skontaktuj się ze swoim przedstawicielem handlowym lub skorzystaj z dedykowanego adresu email:');?><br/><br/>
                            <a href="mailto:<?= Yii::t('home','zwroty@swiatprzesylek.pl');?>" style="color: white;"><?= Yii::t('home','zwroty@swiatprzesylek.pl');?></a></h4>
                    </div>
                    <br/>
                    <br/>
                </div>

            </div>
        </div>
    </div>


<?php
endif;
?>
<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
    PageVersion::PAGEVERSION_ORANGE_POST,
])):
    ?>

    <div class="division-line hidden-xs">
        <div class="container">
            <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Ciągły rozwój');?></h2>
        </div>
    </div>


    <div style="background: black; padding: 30px 0;" class="text-center">
        <div class="container" >
            <div class="row">
                <div class="col-xs-12">
                    <div style="position: relative; font-weight: bold; font-size: 1.3em; color: white; display: inline-block;">
                        <img src="<?= Yii::app()->baseUrl.'/images/v3/stats.png';?>" style="margin: 0 auto;" class="img-responsive"/>
                        <div data-aos="flip-left"  style="position: absolute; left: 12%; top: 1%; font-size: 2vh;" ><?= Yii::t('home','0,4 mln');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 13%; top: 10%; font-size: 1.2vh;"><?= Yii::t('home','inne');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 16%; top: 24%; font-size: 2vh; color: black;"><?= Yii::t('home','9,1 mln');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 17%; top: 31%; font-size: 1.2vh; color: black;"><?= Yii::t('home','przesyłki{br}kurierskie', ['{br}' => '<br/>']);?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 10%; top: 52%; font-size: 2vh; color: black;"><?= Yii::t('home','33,5 mln');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 14%; top: 61%; font-size: 1.2vh; color: black;"><?= Yii::t('home','przesyłki{br}pocztowe', ['{br}' => '<br/>']);?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 11%; top: 82%; font-size: 2vh; "><?= Yii::t('home','43 mln');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 10%; top: 90%; font-size: 1.2vh;"><?= Yii::t('home','przesyłek w 2017 roku');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 39%; top: 16%; font-size: 4vh; "><?= Yii::t('home','388');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 31%; top: 29%; font-size: 1.5vh;"><?= Yii::t('home','nowych klientów biznesowych');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 38%; top: 35.5%; font-size: 4vh; "><?= Yii::t('home','2075');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 33%; top: 48%; font-size: 1.5vh;"><?= Yii::t('home','klientów kontynuujących{br}współpracę', ['{br}' => '<br/>']);?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 48%; top: 77.5%; font-size: 2vh; "><?= Yii::t('home','400%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 47%; top: 84%; font-size: 0.9vh;"><?= Yii::t('home','wrostu obrotu{br}względem 2016r', ['{br}' => '<br/>']);?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 79%; top: 8%; font-size: 1.8vh; "><?= Yii::t('home','4%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 79%; top: 15%; font-size: 1.2vh;"><?= Yii::t('home','Polska');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 90%; top: 16%; font-size: 1.8vh; "><?= Yii::t('home','15%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 90%; top: 23%; font-size: 1.2vh;"><?= Yii::t('home','Niemcy');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 94%; top: 40%; font-size: 1.8vh; "><?= Yii::t('home','12%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 94%; top: 47%; font-size: 1.2vh;"><?= Yii::t('home','Wielka{br}Brytania', ['{br}' => '<br/>']);?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 93%; top: 62%; font-size: 1.8vh; "><?= Yii::t('home','4%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 93%; top: 69%; font-size: 1.2vh;"><?= Yii::t('home','Irlania');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 91%; top: 75%; font-size: 1.8vh; "><?= Yii::t('home','5%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 91%; top: 82%; font-size: 1.2vh;"><?= Yii::t('home','Hiszpania');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 85%; top: 82%; font-size: 1.8vh; "><?= Yii::t('home','6%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 85%; top: 89%; font-size: 1.2vh;"><?= Yii::t('home','Węgry');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 78%; top: 88%; font-size: 1.8vh; "><?= Yii::t('home','8%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 78%; top: 95%; font-size: 1.2vh;"><?= Yii::t('home','Francja');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 70%; top: 82%; font-size: 1.8vh; "><?= Yii::t('home','6%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 70%; top: 89%; font-size: 1.2vh;"><?= Yii::t('home','Włochy');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 64%; top: 70%; font-size: 1.8vh; "><?= Yii::t('home','7%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 64%; top: 77%; font-size: 1.2vh;"><?= Yii::t('home','Holandia');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 61%; top: 56%; font-size: 1.8vh; "><?= Yii::t('home','9%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 61%; top: 63%; font-size: 1.2vh;"><?= Yii::t('home','USA');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 60%; top: 39%; font-size: 1.8vh; "><?= Yii::t('home','4%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 60%; top: 46%; font-size: 1.2vh;"><?= Yii::t('home','Kanada');?></div>

                        <div data-aos="flip-left"  style="position: absolute; left: 67%; top: 15%; font-size: 1.8vh; "><?= Yii::t('home','20%');?></div>
                        <div data-aos="flip-left"  style="position: absolute; left: 67%; top: 22%; font-size: 1.2vh;"><?= Yii::t('home','Rosja');?></div>
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php
endif;
?>
<?php
if(in_array(Yii::app()->PV->get(), [
    PageVersion::PAGEVERSION_DEFAULT,
    PageVersion::PAGEVERSION_RS,
])):
    ?>

    <div class="division-line">
        <div class="container">
            <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Dostęp do nowoczesnej platformy nadawczej');?></h2>
        </div>
    </div>
    <div class="section-linker text-center" style="background: white;">
        <img src="<?= Yii::app()->baseUrl.'/images/v3/panel.jpg';?>" style="margin: 0 auto;" class="img-responsive img-desaturated"/>
    </div>


    <div class="division-line hidden-xs">
        <div class="container">
            <h2 data-aos="flip-left" data-aos-delay="300"><?= Yii::t('home','Najlepsze ceny dystrybucji');?></h2>
        </div>
    </div>
    <div class="section-prices text-center hidden-xs" style="background: white; position: relative;">

        <div style="position: absolute; top: 25px; left: 0; right: 0; z-index: 20;">
            <div class="container text-left">
                <div style="position: relative; display: inline-block; background: rgba(100,123,131, 0.8); padding: 10px;">
                    <div style="color: white; text-align: center; font-weight: bold; font-size: 1em;"><?= Yii::t('home','Przykładowe ceny oferowane{br}nadawcy masowemu', ['{br}' => '<br/>']);?></div>
                    <img src="<?= Yii::app()->baseUrl.'/images/v3/prices_block_box.png';?>" style="margin: 10px auto;" class="img-responsive"/>
                    <div style="color: white; text-align: center; font-weight: bold; font-size: 1.2em;"><?= Yii::t('home','3 KG{br}40 cm x 40 cm x 20 cm', ['{br}' => '<br/>']);?>
                    </div>
                </div>
            </div>
        </div>


        <div style="position: relative; font-size: 1.7vh; color: white; font-weight: bold; display: inline-block; -webkit-text-stroke-width: 1px;
   -webkit-text-stroke-color: black;">
            <img src="<?= Yii::app()->baseUrl.'/images/v3/prices.png';?>" style="margin: 0 auto;" class="img-responsive"/>

            <div data-aos="flip-right" id="map-price-ie" style="position: absolute; left: 12.5%; top: 40%;"><?= Yii::t('home','5.00 €');?></div>

            <div data-aos="flip-right" id="map-price-gb" style="position: absolute; left: 19%; top: 45%;"><?= Yii::t('home','4.50 €');?></div>

            <div data-aos="flip-right" id="map-price-fr" style="position: absolute; left: 22.5%; top: 64%;"><?= Yii::t('home','5.50 €');?></div>

            <div data-aos="flip-right" id="map-price-es" style="position: absolute; left: 11%; top: 77%;"><?= Yii::t('home','6.40 €');?></div>

            <div data-aos="flip-right" id="map-price-pt" style="position: absolute; left: 3%; top: 74%;"><?= Yii::t('home','7.50 €');?></div>


            <div data-aos="flip-right" id="map-price-it" style="position: absolute; left: 32%; top: 69%;"><?= Yii::t('home','5.80 €');?></div>

            <div data-aos="flip-right" id="map-price-hr" style="position: absolute; left: 42%; top: 70%;"><?= Yii::t('home','7.50 €');?></div>

            <div data-aos="flip-right" id="map-price-ro" style="position: absolute; left: 53%; top: 68%;"><?= Yii::t('home','6.00 €');?></div>

            <div data-aos="flip-right" id="map-price-bg" style="position: absolute; left: 54%; top: 77%;"><?= Yii::t('home','6.80 €');?></div>

            <div data-aos="flip-right" id="map-price-gr" style="position: absolute; left: 50%; top: 86%;"><?= Yii::t('home','8.50 €');?></div>


            <div data-aos="flip-right" id="map-price-ua" style="position: absolute; left: 60%; top: 57%;"><?= Yii::t('home','6.50 €');?></div>


            <div data-aos="flip-right" id="map-price-ru" style="position: absolute; left: 70%; top: 41%;"><?= Yii::t('home','11.50 €');?></div>


            <div data-aos="flip-right" id="map-price-lt" style="position: absolute; left: 51%; top: 43%;"><?= Yii::t('home','4.00 €');?></div>

            <div data-aos="flip-right" id="map-price-lv" style="position: absolute; left: 51.2%; top: 38%;"><?= Yii::t('home','4.30 €');?></div>

            <div data-aos="flip-right" id="map-price-es" style="position: absolute; left: 52%; top: 33%;"><?= Yii::t('home','8.20 €');?></div>

            <div data-aos="flip-right" id="map-price-se" style="position: absolute; left: 42%; top: 22%;"><?= Yii::t('home','6.80 €');?></div>

            <div data-aos="flip-right" id="map-price-fi" style="position: absolute; left: 52%; top: 24%;"><?= Yii::t('home','7.20 €');?></div>


            <div data-aos="flip-right" id="map-price-dk" style="position: absolute; left: 33.5%; top: 40%;"><?= Yii::t('home','6.70 €');?></div>

            <div data-aos="flip-right" id="map-price-nl" style="position: absolute; left: 28%; top: 47%;"><?= Yii::t('home','4.80 €');?></div>

            <div data-aos="flip-right" id="map-price-be" style="position: absolute; left: 27.5%; top: 53%;"><?= Yii::t('home','5.20 €');?></div>

            <div data-aos="flip-right" id="map-price-de" style="position: absolute; left: 35%; top: 52%;"><?= Yii::t('home','3.90 €');?></div>


            <div data-aos="flip-right" id="map-price-cz" style="position: absolute; left: 40%; top: 58%;"><?= Yii::t('home','4.50 €');?></div>

            <div data-aos="flip-right" id="map-price-sk" style="position: absolute; left: 46%; top: 61%;"><?= Yii::t('home','3.50 €');?></div>

            <div data-aos="flip-right" id="map-price-hu" style="position: absolute; left: 46%; top: 66%;"><?= Yii::t('home','4.30 €');?></div>

            <div data-aos="flip-right" id="map-price-at" style="position: absolute; left: 39%; top: 64%;"><?= Yii::t('home','5.50 €');?></div>


        </div>
    </div>

<?php
endif;
?>
<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
                map: {
                  img: '.CJSON::encode(Yii::app()->baseUrl.'/images/v2/sp_map_ico.png').',
                  branches: '.CJSON::encode($branches).',
             },
              urls: {
                  getPriceCourier: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxGetPrice')).',
                  getPricePostal: '.CJSON::encode(Yii::app()->urlManager->createUrl('/postal/postal/ajaxGetPrice')).',
                  getPriceHm: '.CJSON::encode(Yii::app()->urlManager->createUrl('/hybridMail/hybridMail/ajaxGetPrice')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).'
              },
              pv: '.CJSON::encode(Yii::app()->PV->get()).'
          };
      ',CClientScript::POS_HEAD);
?>



