<?php

class DpdDeClient
{
    protected static $URL = GLOBAL_CONFIG::DPDDE_URL;
    protected static $PARTNER_NAME = GLOBAL_CONFIG::DPDDE_PARTNER_NAME;
    protected static $PARTNER_TOKEN = GLOBAL_CONFIG::DPDDE_PARNTER_TOKEN;
    protected static $CLOUD_ID = GLOBAL_CONFIG::DPDDE_CLOUD_ID;
    protected static $CLOUD_TOKEN = GLOBAL_CONFIG::DPDDE_CLOUD_TOKEN;

    public $packagesNumber = 1;

    public $packageWeight;
    public $packageContent;
    public $packageInternalId;
    public $ref1 = '';
    public $ref2 = '';

    public $label_annotation_text = false;

    public $cod = 0;

    public $receiverAddressDataModel;
    protected $_shipService;

    protected $_parceshlop_no = false;

    public $user_id = false;

    const SHIP_SERVICE_CLASSIC = 'Classic';
    const SHIP_SERVICE_PARCELSHOP_DELIVERY = 'Shop_Delivery';

    const SHIP_SERVICE_EXPRESS_830 = 'Express_830';
    const SHIP_SERVICE_EXPRESS_10 = 'Express_10';
    const SHIP_SERVICE_EXPRESS_12 = 'Express_12';
    const SHIP_SERVICE_EXPRESS_18 = 'Express_18';
    const SHIP_SERVICE_EXPRESS_12_SAT = 'Express_12_Saturday';
    const SHIP_SERVICE_EXPRESS_12_SAT_COD = 'Express_12_COD_Saturday';
    const SHIP_SERVICE_EXPRESS_INT = 'Express_International';

    protected function setDifferentContractHolder($uniq_id)
    {

        if($uniq_id == GLOBAL_BROKERS::OPERATOR_UNIQID_DPDDE_ORANGEPOST)
        {
            if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
            {
                self::$PARTNER_NAME = 'DPD Cloud Service Alpha2';
                self::$PARTNER_TOKEN = 'X___33879594E70436D58685';
                self::$CLOUD_ID = 'X___1135223';
                self::$CLOUD_TOKEN = 'X___D70433030325157';
            } else {
                self::$PARTNER_NAME = 'DPD Cloud Service Alpha2';
                self::$PARTNER_TOKEN = '33879594E70436D58685';
                self::$CLOUD_ID = '1135223';
                self::$CLOUD_TOKEN = 'D70433030325157';
            }
        }
    }

    public function setShipService($service = false, $withCod = false)
    {

        if(!$service)
            $service = self::SHIP_SERVICE_CLASSIC;

        if($service == self::SHIP_SERVICE_EXPRESS_12_SAT && $withCod) // exception in naming convetion for "COD" suffix
        {
            $this->_shipService = self::SHIP_SERVICE_EXPRESS_12_SAT_COD;
            return;
        }

        if ($withCod)
            $service = $service . '_COD';

        $this->_shipService = $service;
    }

    protected static function _ownExtractNumber($text, $defaultValue = '0')
    {
        $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-]+)((\s)+|$))*/i';
        $building = '';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[0][0];

        $building = trim($building);

        if(mb_strlen($building) > 8)
        {
            // max length is 8
            // if it's longer, use older, more choosy rexexp
            $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';
            if(preg_match_all($REG_PATTERN, $text, $result2))
                $building = $result2[2][0];

            $building = trim($building);
        }

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }

    protected function _createShipment($returnError = false, $shiftDays = 0)
    {
        $data = new stdClass();
        $data->OrderAction = 'startOrder';
        $data->OrderSettings = new stdClass();
        $data->OrderSettings->ShipDate = S_Useful::workDaysNextDate(date('Y-m-d'), $shiftDays);
        $data->OrderSettings->LabelSize = 'PDF_A6';
        $data->OrderSettings->LabelStartPosition = 'UpperLeft';
        $data->OrderDataList = [];

        $receiverAddressDataModel = $this->receiverAddressDataModel;
        $addressLine = strtoupper(trim($receiverAddressDataModel->address_line_1.' '.$receiverAddressDataModel->address_line_2));

        if(preg_match('/PARCELSHOP (\d+)/i', $addressLine, $match)) {
            $this->_parceshlop_no = $match[1];
            $this->_shipService = self::SHIP_SERVICE_PARCELSHOP_DELIVERY;

            $addressLine = str_replace('PARCELSHOP '.$this->_parceshlop_no, '', $addressLine);
        }

        $receiverStreetNo = trim(self::_ownExtractNumber($addressLine));
        $receiverAddressName = trim(str_replace($receiverStreetNo, '', $addressLine));

        for($i = 0; $i < $this->packagesNumber; $i++) {

            $orderItem = new stdClass();

            if($this->_parceshlop_no)
                $orderItem->ParcelShopID = $this->_parceshlop_no;

            $orderItem->ShipAddress = new stdClass();
            $orderItem->ShipAddress->Company = $receiverAddressDataModel->company;
            $orderItem->ShipAddress->Salutation = '';
            $orderItem->ShipAddress->Name = $receiverAddressDataModel->name;
            $orderItem->ShipAddress->Street = $receiverAddressName;
            $orderItem->ShipAddress->HouseNo = $receiverStreetNo;
            $orderItem->ShipAddress->ZipCode = $receiverAddressDataModel->zip_code;
            $orderItem->ShipAddress->City = $receiverAddressDataModel->city;
            $orderItem->ShipAddress->Country = $receiverAddressDataModel->country0->code;
            $orderItem->ShipAddress->State = $receiverAddressDataModel->country_id == CountryList::COUNTRY_US ? UsaZipState::getStateByZip($receiverAddressDataModel->zip_code) : '';
            $orderItem->ShipAddress->Phone = str_pad($receiverAddressDataModel->tel,5,'0',STR_PAD_LEFT);
            $orderItem->ShipAddress->Mail = $receiverAddressDataModel->fetchEmailAddress();
            $orderItem->ParcelData = new stdClass();
            $orderItem->ParcelData->YourInternalID = $this->packageInternalId. ($i ? '_'.$i : '');
            $orderItem->ParcelData->Content = mb_substr($this->packageContent,0,35);
            $orderItem->ParcelData->Weight = $this->packageWeight;
            $orderItem->ParcelData->Reference1 = $this->ref1 != '' ? $this->ref1 : $this->packageInternalId. ($i ? '_'.$i : '');
            $orderItem->ParcelData->Reference2 = $this->ref2;
            $orderItem->ParcelData->ShipService = $this->_shipService;

            if($this->cod > 0)
            {
                if($this->_parceshlop_no)
                {
                    if ($returnError == true) {
                        $return = [0 => [
                            'error' => 'COD is not allowed for PARCELSHOP delivery!'
                        ]];
                        return $return;
                    } else {
                        return false;
                    }
                }

                $orderItem->ParcelData->COD = new stdClass();
                $orderItem->ParcelData->COD->Purpose = 'Payment';
                $orderItem->ParcelData->COD->Amount = $this->cod;
                $orderItem->ParcelData->COD->Payment = 'Cash';
            }

            $data->OrderDataList[] = $orderItem;

        }


        MyDump::dump('dpdde.txt', print_r($data,1));

        $resp = $this->_curlCall('setOrder', $data);

        if ($resp->success == true) {
            $return = [];


            $labelDetails = $resp->data->LabelResponse->LabelDataList;
            if(!is_array($labelDetails))
                $labelDetails = [ $labelDetails ];

            /// PROCESS PDF WITH LABELS:
            $labelsPdf = $resp->data->LabelResponse->LabelPDF;
            // inquire label images from pdf
            $labelsPng = [];

            $uniqId = uniqid();
            $basePath = Yii::app()->basePath . '/../';
            $dir = 'uplabels/';
            $temp = $dir . 'temp/';

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            @file_put_contents($basePath . $temp . $uniqId . '.pdf', base64_decode($labelsPdf));


            for ($i = 0; $i < S_Useful::sizeof($labelDetails); $i++) {

                try {

                    $im->setResolution(300, 300);
                    $im->readImage($basePath . $temp . $uniqId . '.pdf[' . $i . ']');

//                    $im->annotateImage($draw, 50, 50, 0, 'DPD DE');


//                    $im->trimImage(0);
                    $im->setImageFormat('png8');
                    if ($im->getImageAlphaChannel()) {
                        $im->setImageBackgroundColor('white');
                        $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                    }


//                    $im->annotateImage($draw, 50, 50, 0, 'DE-DHL');
                    if($this->user_id != 2671 && $this->label_annotation_text)
                        LabelAnnotation::createAnnotation($this->label_annotation_text, $im, 58);

                    $im->trimImage(0);
                    $im->stripImage();

                    $labelsPng[] = $im->getImageBlob();

                } catch (Exception $ex) {
                    // probaby no more labels - stop searching...
                    break;
                }
            }
            @unlink($basePath . $temp . $uniqId . '.pdf');

            $i = 0;
            foreach($labelDetails AS $key => $item)
            {
                $return[] = [
                    'status' => true,
                    'label' => $labelsPng[$i],
                    'track_id' => $item->ParcelNo,
                ];

                $i++;
            }
            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            // WRONG PICKUP DATE
            if($resp->error == 'CLOUD_API_ORDER_SHIPDATE' && $shiftDays < 3)
            {
                MyDump::dump('dpdde.txt', 'SHIFT DAYS : '.$shiftDays);

                $shiftDays++;
                return $this->_createShipment($returnError, $shiftDays);
            }
            else if($resp->error == 'CLOUD_ADDRESS_STREETCODE')
            {
                $errorDesc .= '|||STREET NAME: "'.$receiverAddressName.'"; HOUSE NO: "'.$receiverStreetNo.'"';
            }

            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $errorDesc,
                )];
                return $return;
            } else {
                return false;
            }
        }

    }

    protected function _curlCall($action, $data = false, $retry = 0)
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $endpoint = self::$URL.$action;

        $dataOryginal = $data;

        if($data)
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);

        $curl = curl_init($endpoint);
        curl_setopt_array($curl, array(

                CURLOPT_POST => $data ? TRUE : FALSE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Version : ' . '100',
                    'Language : ' . 'en_EN',
                    'PartnerCredentials-Name : ' . self::$PARTNER_NAME,
                    'PartnerCredentials-Token : ' . self::$PARTNER_TOKEN,
                    'UserCredentials-cloudUserID : ' . self::$CLOUD_ID,
                    'UserCredentials-Token : ' . self::$CLOUD_TOKEN),
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        );

        if($data)
            curl_setopt($curl,   CURLOPT_POSTFIELDS, $data);

        $resp = curl_exec($curl);

        MyDump::dump('dpdde.txt', print_r($endpoint,1));
        MyDump::dump('dpdde.txt', print_r($data,1));
        MyDump::dump('dpdde.txt', print_r($resp,1));

        $resp = json_decode($resp);

        if($resp === NULL)
        {
            $return->error = curl_error($curl);
            $return->errorCode = '';
            $return->errorDesc = curl_error($curl);
        }
        else if(!isset($resp->Ack)) {
            $return->error = $resp->Message;
            $return->errorCode = '';
            $return->errorDesc = $resp->Message;
        }
        else if($resp->Ack)
        {
            $return->success = true;
            $return->data = $resp;
        } else {

            if($resp->ErrorDataList[0]->ErrorID == '9999' && $retry < 3)
            {
                MyDump::dump('dpdde.txt', 'GO WITH RETRY! : '.print_r($retry,1));

                $retry++;
                return $this->_curlCall($action, $dataOryginal, $retry);
            }

            $return->error = $resp->ErrorDataList[0]->ErrorCode;
            $return->errorCode = $resp->ErrorDataList[0]->ErrorID;
            $return->errorDesc = $resp->ErrorDataList[0]->ErrorMsgLong;
        }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $operator_uniq_id, $returnError = false)
    {
        $model = new self;

        $model->setDifferentContractHolder($operator_uniq_id);

        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($operator_uniq_id);

        $model->user_id = $courierInternal->courier->user_id;

//        $model->setProductByCourierOperatorUniqId($operator_uniq_id);

//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLDE_PAEKET_PRIO))
//            $model->setProduct(self::PRODUCT_PAKET_PRIO);


        $model->receiverAddressDataModel = $to;
        $model->packageWeight = $courierInternal->courier->getWeight(true);
        $model->packagesNumber = $courierInternal->courier->packages_number;
        $model->packageContent = $courierInternal->courier->package_content;

        $model->packageInternalId = $courierInternal->courier->local_id;

        $ref2 = $courierInternal->courier->user_id != '' ? $courierInternal->courier->user->login.'|'.$courierInternal->courier->senderAddressData->getUsefulName(true) : $courierInternal->courier->senderAddressData->getUsefulName(true);

        /* @17.08.2017 - special rule for customer VARTS - put package content on label */
        if($courierInternal->courier->user_id == 1141)
            $ref2 = $courierInternal->courier->user->login.'|'.$courierInternal->courier->package_content;


        /* @11.05.2018 - special rule for customer ANWIS*/
        if($courierInternal->courier->user_id == 2671) {
            $model->ref1 = mb_substr($courierInternal->courier->ref,0, 35);
            $ref2 = $courierInternal->courier->senderAddressData->getUsefulName(true);
        }

        $model->ref2 = mb_substr($ref2,0, 35);

        if($courierInternal->courier->cod && $courierInternal->courier->cod_value > 0)
        {
            $model->cod = $courierInternal->courier->cod_value;
        }


        $service = false;
        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_830))
            $service = self::SHIP_SERVICE_EXPRESS_830;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_10))
            $service = self::SHIP_SERVICE_EXPRESS_10;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_12))
            $service = self::SHIP_SERVICE_EXPRESS_12;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_18))
            $service = self::SHIP_SERVICE_EXPRESS_18;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_12_SAT))
            $service = self::SHIP_SERVICE_EXPRESS_12_SAT;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_INT))
            $service = self::SHIP_SERVICE_EXPRESS_INT;

        $model->setShipService($service, $model->cod);

        return $model->_createShipment($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {


        $model = new self;
        $model->setDifferentContractHolder($courierTypeU->courierUOperator->uniq_id);

        $model->label_annotation_text = $courierTypeU->courierUOperator->label_symbol;

        $model->user_id = $courierTypeU->courier->user_id;

//        $model->setProductByCourierOperatorUniqId($operator_uniq_id);

//        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_DHLDE_PAEKET_PRIO))
//            $model->setProduct(self::PRODUCT_PAKET_PRIO);

        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;


        $model->receiverAddressDataModel = $to;
        $model->packageWeight = $courierTypeU->courier->package_weight;
        $model->packagesNumber = $courierTypeU->courier->packages_number;
        $model->packageContent = $courierTypeU->courier->package_content;

        $model->packageInternalId = $courierTypeU->courier->local_id;

        $ref2 = $courierTypeU->courier->user_id != '' ? $courierTypeU->courier->user->login.'|'.$courierTypeU->courier->senderAddressData->getUsefulName(true) : $courierTypeU->courier->senderAddressData->getUsefulName(true);

        /* @17.08.2017 - special rule for customer VARTS - put package content on label */
        if($courierTypeU->courier->user_id == 1141)
            $ref2 = $courierTypeU->courier->user->login.'|'.$courierTypeU->courier->package_content;

        // @ 09.05.2018
        // DO NOT ADD REF2 for RR DONNELLEY
        if($courierTypeU->courier->user_id == 258)
            $ref2 = 'LSC Communications';

        $model->ref2 = mb_substr($ref2,0, 35);

        if($courierTypeU->courier->cod && $courierTypeU->courier->cod_value > 0)
        {
            $model->cod = $courierTypeU->courier->cod_value;
        }

        $service = false;
        if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_830))
            $service = self::SHIP_SERVICE_EXPRESS_830;
        else if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_10))
            $service = self::SHIP_SERVICE_EXPRESS_10;
        else if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_12))
            $service = self::SHIP_SERVICE_EXPRESS_12;
        else if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_18))
            $service = self::SHIP_SERVICE_EXPRESS_18;
        else if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_12_SAT))
            $service = self::SHIP_SERVICE_EXPRESS_12_SAT;
        else if($courierTypeU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_INT))
            $service = self::SHIP_SERVICE_EXPRESS_INT;

        $model->setShipService($service, $model->cod);


        return $model->_createShipment($returnError);
    }

    public static function track($number)
    {
        $model = new self;

        $statuses = [];

        $action = 'getOrderStatus/'.$number.'/null';

        $result = $model->_curlCall($action, false);


        $listOfStatuses = [
            'Start',
            'OnTheRoad',
            'DeliveryDepot',
            'CarLoad',
            'Delivered',
        ];

        if($result->success)
        {

            foreach($listOfStatuses AS $item)
            {
                $status = $result->data->OrderStatus->StatusInfoContainer->$item;

                if($status->StatusReached)
                {

                    $date = substr($status->StatusDate,6,4).'-'.substr($status->StatusDate,3,2).'-'.substr($status->StatusDate,0,2).' '.substr($status->StatusDate,12,5).':59';


                    $location = false;
                    if($item != 'Start' && $status->DepotData->Address->City && $status->DepotData->Address->Country)
                        $location = $status->DepotData->Address->City.', '.$status->DepotData->Address->Country;


                    if($status->Headline == 'Order information has been transmitted to DPD')
                        continue;

                    array_push($statuses, [
                        'name' => $status->Headline,
                        'date' => $date,
                        'location' => $location ? $location : NULL,
                    ]);
                }
            }

            return $statuses;

        }

        return false;

    }


}

