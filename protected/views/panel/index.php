<div class="row">
    <div class="col-lg-12">
        <h2><?= Yii::t('panel', 'Panel nadawcy');?></h2>
    </div>
</div>





<div class="row">
    <?php
    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI):
        ?>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?php
            Yii::app()->getModule('postal');
            $countryList = CountryList::getActiveCountries(true);
            ?>
            <div id="main-pricing-block">
                <div id="main-pricing-block-list">
                    <a href="#" data-pricing-show="courier"><img src="<?= Yii::app()->baseUrl;?>/theme/ruch/images/home-ico-courier.png"/></a>
                    <a href="#" data-pricing-show="postal"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-postal.png"/></a>
                    <?php
                    if(in_array(Yii::app()->PV->get(), [
                        PageVersion::PAGEVERSION_DEFAULT,
                        PageVersion::PAGEVERSION_RS,
                    ])):
                        ?>
                        <a href="#" data-pricing-show="hm"><img src="<?= Yii::app()->baseUrl;?>/images/v2/home-send-ico-hm.png"/></a>
                        <?php
                    endif;
                    ?>
                </div>

                <div class="main-pricing-courier main-pricing-type" data-pricing="courier" data-force-uid="<?= Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI ? User::PLI_GENERIC_USER_ID : 0;?>" data-force-currency="<?= Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI && Yii::app()->user->isGuest ? Currency::EUR_ID : '';?>">
                    <h2><?= Yii::t('home','Kalkulator ceny');?></h2>
                    <p class="main-pricing-subtype"><?= Yii::t('home','kurierzy');?></p>
                    <div class="pricing-labels">
                        <span><?= Yii::t('home','waga');?> [kg]</span>
                        <span><?= Yii::t('home','wymiary');?> [cm]</span>

                    </div>
                    <div>
                        <input type="text" id="pricing-courier-weight" class="pricing-courier-weight" data-pricing-input="true" value="2"/>
                        <input type="text" id="pricing-courier-dim-l" class="pricing-courier-dim" data-pricing-input="true" value="20"/>x
                        <input type="text" id="pricing-courier-dim-w" class="pricing-courier-dim" data-pricing-input="true" value="20"/>x
                        <input type="text" id="pricing-courier-dim-d" class="pricing-courier-dim" data-pricing-input="true" value="10"/>
                    </div>
                    <div class="pricing-labels">
                        <span><?= Yii::t('home','kraj nadawcy');?></span>
                        <span><?= Yii::t('home','kraj odbiorcy');?></span>
                    </div>
                    <div>
                        <?= CHtml::dropDownList('pricing_sender', Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI ? CountryList::COUNTRY_UK : '', $countryList, ['id' => 'pricing-courier-country-sender', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                        <?= CHtml::dropDownList('pricing_receiver', Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI ? CountryList::COUNTRY_PL : CountryList::COUNTRY_DE, $countryList, ['id' => 'pricing-courier-country-receiver', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                    </div>

                    <div class="main-pricing-block-price">
                        <div class="main-pricing-note">
                            <?php echo TbHtml::tooltip('[?]', '#', Yii::t('home', 'Cena dotyczy nadania w punkcie/oddziale Świata Przesyłek')); ?>
                        </div>

                        <div class="pricing-currency-label"><?= Yii::t('home','cena');?> [<span>-</span>]</div>
                        <div class="main-pricing-block-price-amount-loading"></div>
                        <div class="main-pricing-block-price-amount">-</div>
                        <div class="main-pricing-block-price-amount-brutto">(<span>-</span> <?= Yii::t('home','brutto');?>)</div>
                        <div class="main-pricing-block-price-go" data-url="<?= Yii::app()->createUrl('/courier/courier/createFP');?>"><?= Yii::t('home','zamów');?></div>
                    </div>

                    <a class="main-pricing-block-contact" href="#home-contact-block"><p><?= Yii::t('home','Skontaktuj się z nami, aby uzyskać wycenę!');?><br/><span class="glyphicon glyphicon-envelope"></span></p></a>
                </div>

                <div class="main-pricing-postal main-pricing-type" data-pricing="postal" data-force-uid="<?= Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI ? User::PLI_GENERIC_USER_ID : 0;?>" data-force-currency="<?= Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI && Yii::app()->user->isGuest ? Currency::EUR_ID : '';?>">
                    <h2><?= Yii::t('home','Kalkulator ceny');?></h2>
                    <p class="main-pricing-subtype"><?= Yii::t('home','przesyłki pocztowe');?></p>
                    <div class="pricing-labels">
                        <span><?= Yii::t('home','typ');?></span>
                        <span><?= Yii::t('home','gabaryt');?></span>
                        <span><?= Yii::t('home','waga');?></span>
                    </div>
                    <div>
                        <?= CHtml::dropDownList('pricing_postal_type', '', Postal::getPostalTypeList(), ['id' => 'pricing-postal-type', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                        <?= CHtml::dropDownList('pricing_postal_size', '', Postal::getSizeClassList(), ['id' => 'pricing-postal-size', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                        <?= CHtml::dropDownList('pricing_postal_weight', '', Postal::getWeightClassList(), ['id' => 'pricing-postal-weight', 'class' => 'pricing-postal', 'data-pricing-input' => 'true']); ?>
                    </div>
                    <div class="pricing-labels">
                        <span><?= Yii::t('home','kraj odbiorcy');?></span>
                    </div>
                    <div>
                        <?= CHtml::dropDownList('pricing_receiver', '', $countryList, ['id' => 'pricing-postal-country-receiver', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                    </div>

                    <div class="main-pricing-block-price">
                        <div class="pricing-currency-label"><?= Yii::t('home','cena');?> [<span>-</span>]</div>
                        <div class="main-pricing-block-price-amount-loading"></div>
                        <div class="main-pricing-block-price-amount">-</div>
                        <div class="main-pricing-block-price-amount-brutto">(<span>-</span> <?= Yii::t('home','brutto');?>)</div>
                        <div class="main-pricing-block-price-go" data-url="<?= Yii::app()->createUrl('/postal/postal/createFP');?>"><?= Yii::t('home','zamów');?></div>
                    </div>

                    <a class="main-pricing-block-contact" href="#home-contact-block"><p><?= Yii::t('home','Skontaktuj się z nami, aby uzyskać wycenę!');?><br/><span class="glyphicon glyphicon-envelope"></span></p></a>
                </div>
                <?php
                if(in_array(Yii::app()->PV->get(), [
                    PageVersion::PAGEVERSION_DEFAULT,
                    PageVersion::PAGEVERSION_RS,
                ])):
                    ?>
                    <div class="main-pricing-hm main-pricing-type" data-pricing="hm">
                        <h2><?= Yii::t('home','Kalkulator ceny');?></h2>
                        <p class="main-pricing-subtype"><?= Yii::t('home','e-listy');?></p>
                        <div class="pricing-labels">
                            <span><?= Yii::t('home','stron');?></span>
                        </div>
                        <div>
                            <?= CHtml::dropDownList('pricing_hm_pages', '', range(1,99), ['id' => 'pricing-hm-pages', 'class' => 'pricing-hm', 'data-pricing-input' => 'true']); ?>
                        </div>
                        <div class="pricing-labels">
                            <span><?= Yii::t('home','kraj odbiorcy');?></span>
                        </div>
                        <div>

                            <?= CHtml::dropDownList('pricing_hm_receiver', '', $countryList, ['id' => 'pricing-hm-country-receiver', 'class' => 'pricing-country', 'data-pricing-input' => 'true']); ?>
                        </div>

                        <div class="main-pricing-block-price">
                            <div class="pricing-currency-label"><?= Yii::t('home','cena');?> [<span>-</span>]</div>
                            <div class="main-pricing-block-price-amount-loading"></div>
                            <div class="main-pricing-block-price-amount">-</div>
                            <div class="main-pricing-block-price-amount-brutto">(<span>-</span> <?= Yii::t('home','brutto');?>)</div>
                            <div class="main-pricing-block-price-go" data-url="<?= Yii::app()->createUrl('/hybridMail/create');?>"><?= Yii::t('home','zamów');?></div>
                        </div>

                        <a class="main-pricing-block-contact" href="#home-contact-block"><p><?= Yii::t('home','Skontaktuj się z nami, aby uzyskać wycenę!');?><br/><span class="glyphicon glyphicon-envelope"></span></p></a>
                    </div>
                    <?php
                endif;
                ?>

                <?php
                Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/dimensionalWeight.js', CClientScript::POS_HEAD);
                Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/home-pricing.min.js', CClientScript::POS_END);
                Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
                Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
                ?>
            </div>

        </div>
        <?php
    endif;
    ?>

    <?php
    $messagesNo = UserMessage::getNoOfNetMessagesForUser(Yii::app()->user->id);
    if($messagesNo):
        ?>

        <div class="col-xs-12 col-sm-6 col-md-3">
            <a href="<?= Yii::app()->createUrl('/userMessage/');?>">
                <div class="text-center alert alert-warning"><?= Yii::t('panel', 'Masz nowe wiadomości na koncie!');?> (<strong><?= $messagesNo; ?></strong>)</div>
            </a>
        </div>

        <?php
    endif;
    ?>
    <?php
    $invoicesNo = UserInvoice::getNoOfNewInvoicesForUser(Yii::app()->user->id);
    if($invoicesNo):
        ?>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <a href="<?= Yii::app()->createUrl('/userInvoice/');?>">
                <div class="text-center alert alert-warning"><?= Yii::t('panel', 'Masz nowe faktury na koncie!');?> (<strong><?= $invoicesNo; ?></strong>)</div>
            </a>
        </div>
        <?php
    endif;
    ?>


    <?php
    $amoutToPayPLN = UserInvoice::sumTotalInvValue(Currency::PLN_ID, true);
    $amoutToPayEUR = UserInvoice::sumTotalInvValue(Currency::EUR_ID, true);
    $amoutToPayGBP = UserInvoice::sumTotalInvValue(Currency::GBP_ID, true);
    ?>

    <div class="col-xs-12 col-sm-6 col-md-3">
        <a href="<?= Yii::app()->createUrl('/userInvoice/');?>">
        <div class="panel panel-<?= ($amoutToPayPLN > 0 OR $amoutToPayEUR > 0 OR $amoutToPayGBP) ? 'warning' : 'success';?>">
            <div class="panel-heading">
                <div class="text-right"><div><span class="big"><?= $amoutToPayPLN ?: 0 ?></span> <?= Currency::PLN;?> | <span class="big"><?= $amoutToPayEUR ?: 0 ?></span> <?= Currency::EUR;?> | <span class="big"><?= $amoutToPayGBP ?: 0 ?></span> <?= Currency::GBP;?></div>
                    <div><?= Yii::t('invoice', 'Kwota do zapłaty');?></div>
                </div>
            </div>
        </div>
        </a>
    </div>

    <?php
    $outdatedInvoices = UserInvoice::model()->active()->ofCurrentUser()->outdated()->count();
    if($outdatedInvoices):
        ?>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <a href="<?= Yii::app()->createUrl('/userInvoice/');?>">
                <div class="text-center alert alert-warning"><?= Yii::t('invoice', 'Faktur z przekroczonym terminem płatności');?>: <strong><?= $outdatedInvoices;?></strong> </div></a>
        </div>
        <?php
    endif;
    ?>


</div>

<div id="panel-app"></div>

<?php
$script = '';

if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI)
    $script .= ' yii = {
              urls: {
                  getPriceCourier: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxGetPrice')).',
                  getPricePostal: '.CJSON::encode(Yii::app()->urlManager->createUrl('/postal/postal/ajaxGetPrice')).',
                  getPriceHm: '.CJSON::encode(Yii::app()->urlManager->createUrl('/hybridMail/hybridMail/ajaxGetPrice')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).'
              },
          },';

$script .= 'reactGlobals = {
              lang: '.CJSON::encode(Yii::app()->language).',
              urls: {
                  courier: {
                      heaviestWeight: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierHeaviestWeight')).',
                      totalDeliveredWeight: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierTotalDeliveredWeight')).',
                      averageDeliveryTime: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierAverageDeliveryTime')).',
                      totalPackagesDelivered: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierTotalPackagesDelivered')).',
                      map: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierCountriesDelivered')).',
                      statMap: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierStatMap')).',
                      packagesStatPercent: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierPackagesStatPercent')).',
                      packagesStat: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierPackagesStat')).',
                      packagesOrders: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierPackagesOrders')).',
                      lastCourierOrders: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierLastItems')).',
                      averageWeight: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierAverageWeight')).',
                      courierWeightStat: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonCourierWeightStat')).',
                      listBase: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/list')).',
                  },
                  postal: {
                      weightTypes: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonPostalWeightTypeStat')).',
                      totalPackagesDelivered: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonPostalTotalPackagesDelivered')).',
                      map: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonPostalCountriesDelivered')).',
                      statMap: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonPostalStatMap')).',
                      packagesStatPercent: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonPostalPackagesStatPercent')).',
                      packagesStat: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonPostalPackagesStat')).',
                      packagesOrders: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonPostalPackagesOrders')).',
                      lastPostalOrders: '.CJSON::encode(Yii::app()->urlManager->createUrl('/panel/jsonPostalLastItems')).',
                      listBase: '.CJSON::encode(Yii::app()->urlManager->createUrl('/postal/postal/list')).',
                     }
              },
              translation: {
                   calendar : {
                        months : '.CJSON::encode([Yii::t('panel', 'styczeń'), Yii::t('panel', 'luty'), Yii::t('panel', 'marzec'), Yii::t('panel', 'kwiecień'), Yii::t('panel', 'maj'), Yii::t('panel', 'czerwiec'), Yii::t('panel', 'lipiec'), Yii::t('panel', 'sierpień'), Yii::t('panel', 'wrzesień'), Yii::t('panel', 'październik'), Yii::t('panel', 'listopad'), Yii::t('panel', 'grudzień')]).',
                        weekdays : '.CJSON::encode([Yii::t('panel', 'pn'), Yii::t('panel', 'wt'), Yii::t('panel', 'śr'), Yii::t('panel', 'czw'), Yii::t('panel', 'pt'), Yii::t('panel', 'sb'), Yii::t('panel', 'nd')]).',
                    },
                   app : {
                        kurier : '.CJSON::encode(Yii::t('panel', 'Kurier')).',
                        postal : '.CJSON::encode(Yii::t('panel', 'Postal')).',
                        dataRangeSelectBeginning : '.CJSON::encode(Yii::t('panel', 'Od...?')).',
                        dataRangeSelectEnd : '.CJSON::encode(Yii::t('panel', '...do?')).',
                        resetDataRange : '.CJSON::encode(Yii::t('panel', 'Resetuj')).',
                        dataRangeFrom : '.CJSON::encode(Yii::t('panel', 'Zakres danych od')).',
                        dataRangeTo : '.CJSON::encode(Yii::t('panel', 'do')).',
                   },
                   courier : {
                        totalPackagesDelivered : '.CJSON::encode(Yii::t('panel', 'Łącznie dostarczonych paczek')).',      
                        averageDeliveryTime : '.CJSON::encode(Yii::t('panel', 'Średni czas dostawy paczki')).',
                        averageDeliveryTimeTooltip : '.CJSON::encode(Yii::t('panel', 'Czas liczony jest od momentu nadania przesyłki')).',
                        totalDeliveredWeight : '.CJSON::encode(Yii::t('panel', 'Całkowita waga dostarczonych paczek')).',
                        averageWeight : '.CJSON::encode(Yii::t('panel', 'Średnia waga dostarczonych paczek')).',
                        mapHeader : '.CJSON::encode(Yii::t('panel', 'Adresaci Twoich paczek')).',
                        statMapHeader : '.CJSON::encode(Yii::t('panel', 'Twoje paczki z podziałem na statusy')).',
                        statMapHeaderPercent : '.CJSON::encode(Yii::t('panel', 'Twoje paczki z podziałem na statusy [%]')).',
                        statMapChartHeader : '.CJSON::encode(Yii::t('panel', 'Twoje paczki z podziałem na statusy')).',
                        lastUpdatesHeader : '.CJSON::encode(Yii::t('panel', 'Ostatnie aktualizacje Twoich paczek')).',
                        yourOrdersHeader : '.CJSON::encode(Yii::t('panel', 'Twoje zamówienia')).',
                        weightStatHeader : '.CJSON::encode(Yii::t('panel', 'Podział na wagę')).',
                   },
                   postal : {
                        totalPackagesDelivered : '.CJSON::encode(Yii::t('panel', 'Łącznie dostarczonych przesyłek')).',
                        mapHeader : '.CJSON::encode(Yii::t('panel', 'Adresaci Twoich przesyłek')).',
                        statMapHeader : '.CJSON::encode(Yii::t('panel', 'Twoje paczki z podziałem na statusy')).',
                        statMapHeaderPercent : '.CJSON::encode(Yii::t('panel', 'Twoje paczki z podziałem na statusy [%]')).',
                        statMapChartHeader : '.CJSON::encode(Yii::t('panel', 'Twoje paczki z podziałem na statusy')).',
                        lastUpdatesHeader : '.CJSON::encode(Yii::t('panel', 'Ostatnie aktualizacje Twoich paczek')).',
                        yourOrdersHeader : '.CJSON::encode(Yii::t('panel', 'Twoje zamówienia')).',
                        sizeHeader : '.CJSON::encode(Yii::t('panel', 'Podział Twoich przesyłek wg wagi')).',
                        
                   },
                   lastItem : {
                        to : '.CJSON::encode(Yii::t('panel', 'Do:')).',
                        status : '.CJSON::encode(Yii::t('panel', 'Status:')).',
                   },
                   statMap: {
                          MAP_NEW: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_NEW)).',
                          MAP_ACCEPTED: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_ACCEPTED)).',
                          MAP_DELIVERED_TO_HUB: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_DELIVERED_TO_HUB)).',
                          MAP_IN_TRANSPORTATION: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_IN_TRANSPORTATION)).',
                          MAP_IN_DELIVERY: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_IN_DELIVERY)).',
                          MAP_DELIVERED: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_DELIVERED)).',
                          MAP_CANCELLED: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_CANCELLED)).',
                          MAP_PROBLEM: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_PROBLEM)).',
                          MAP_RETURN: '.CJSON::encode(StatMap::getMapNameById(StatMap::MAP_RETURN)).',
                   }
              }
          };
      ';


Yii::app()->clientScript->registerScript('helpers', $script,CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('//www.google.com/jsapi', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/panel/bundle.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/panel/react-day-picker.css');