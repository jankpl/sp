<?php
/**
 * Created by PhpStorm.
 * User: Janek
 * Date: 2017-05-25
 * Time: 11:08
 */
?>


<h1>Courier U Prices</h1>
<h2>Receiver country group:</h2>

<?php
$tabs = [];
/* @var $countryGroups CourierCountryGroup[] */
$countryGroups = CourierCountryGroup::model()->findAll(['condition' => 't.id IN (SELECT DISTINCT courier_country_group_id FROM courier_u_operator_2_country_group)']);
foreach($countryGroups AS $country)
    $tabs[] = [
        'label' => $country->name,
        'url' => '#cid'.$country->id,

    ];
?>

<div id="country-list">
    <?php echo TbHtml::tabs($tabs); ?>
</div>
<div id="country-content">
    <div class="text-center">-- select country --</div>
</div>

<script>
    var currentId = false;
    function loadData(id)
    {
        if(id == currentId)
            return false;

        Overlay.show();

        currentId = id;
//        $('#country-content').slideUp();

        $.ajax({
            url: '<?= Yii::app()->urlManager->createUrl('/courier/courierUPrices/ajaxGetTab');?>',
            data: { id: id },
            method: 'POST',
            dataType: 'json',
            success: function(result){

                $('#country-list a').parent('li').removeClass('active');
                $('#country-list a[href="#cid' + id + '"]').parent('li').addClass('active');

                $('#country-content').html(result);

                $('[edit-group-price]').on('click', function(){

                    var url = $(this).attr('data-url');
                    var id = $(this).closest('div').find('[group-edit-id]').val();

                    url = url.replace('GID', id);

//                    window.location.assign(url);
                    window.open(url);
                });

                $('[data-toggle]').on('click', function(){

                    $(this).closest('table').find('[data-toggle-target]').toggle();
                });

//                $('#country-content').slideDown();

                Overlay.hide();

                $('html, body').animate({
                    scrollTop: $("#country-content").offset().top,
                }, 100);

            },
            error: function(e){
                alert("Ups, something went wrong...");
                Overlay.hide();
                //alert(yii.loader.error);
                // FormControl.submit();
            }
        });
    }

    $('#country-list a').on('click', function(){

        var id = $(this).attr('href');
        id = id.replace('#cid','');
        loadData(id);
    });

    $(document).ready(function(){

        var hashId = window.location.hash;
        hashId = hashId.replace('#cid', '');
        hashId = parseInt(hashId);

        if(hashId && hashId != currentId)
        {
            loadData(hashId);
        }

    });

    var Overlay = (function() {
        "use strict";

        var that = {};
        var $overlay = $("<div>");
        var $textBottom = $("<div>");
        var $textTop = $("<div>");
        var $loader = $("<div>");

        that.isShow = function(){
            return $("#overlay").is(':visible');
        };

        that.show = function() {
            var $form = $("#content");

            $overlay.addClass('overlay');


            $loader.addClass('loader');

            $overlay.append($loader);


            $textTop.addClass('loader-text-top');
            $textTop.html('loading...');


            $textBottom.addClass('loader-text-bottom');
            $textBottom.html('please wait...');

            $overlay.append($textTop);
            $overlay.append($textBottom);

            $form.css('position','relative');
            $form.append($overlay);

            $textTop.hide().delay(1000).fadeIn();
            $textBottom.hide().delay(3000).fadeIn();
        };

        that.hide = function() {
            $overlay.remove();
        };

        return that;
    }());
</script>
