<?php

class bMailerOrder extends bBaseBehavior
{

    public function events()
    {
        return array_merge(parent::events(),

            array(
                'onAfterFinalizeOrder' => 'afterFinalizeOrder',
            )
        );

    }

    public function afterFinalizeOrder(CEvent $e)
    {
        /* @var $model Order */
        $model = $e->params['model'];

        if($model->user && $model->user->getDisableOrderNotification())
            return false;

        if($model->user_id == NULL)
            $creatorData = $model->ownerData;
        else
            $creatorData = $model->user;

        $lang = NULL;
        if($model->user)
            $lang = $model->user->getPrefLang();

        $tempLangChange = new TempLangChange();
        $tempLangChange->temporarySetLanguage($lang);

        $text = Yii::t('mail',"Nowe zamówienie");
        $text .= "\r\n";
        $text .= "\r\n";
        $text .= Yii::t('mail',"Twoje zamówienie otrzymało numer: {no}", array('{no}' => '<strong>'.$model->local_id.'</strong>'));
        $text .= "\r\n";
        $text .= "\r\n";
        $text .= Yii::t('mail',"Kliknij {a}tutaj{/a}, aby przejść do zamówienia.", array('{a}' => "<a  href=\"". PageVersion::urlDomainReplace(Yii::app()->createAbsoluteUrl('order/view', array('urlData' => $model->hash)), $lang, true)."\">", '{/a}' => "</a>"));
        $text .= "\r\n";
        $text .= "\r\n";
        $text .= Yii::t('mail',"Możesz skopiować też link:");
        $text .= "\r\n";
        $text .= "\r\n";
        $text .= PageVersion::urlDomainReplace(Yii::app()->createAbsoluteUrl('order/view', array('urlData' => $model->hash)), $lang, true)."";

        $text = nl2br($text);

        S_Mailer::sendToCustomer($creatorData->email, $creatorData->email,Yii::t('mail',"Nowe zamówienie"),$text, false, $lang, true, false, false, false, false, $model->user ? $model->user->source_domain : NULL);

        $tempLangChange->revertToBaseLanguage();
    }



}