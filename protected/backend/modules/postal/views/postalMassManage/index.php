<?php
$sid = uniqid();
/* @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('/postal/postalMassManage').'?sid='.$sid,
    'id'=>'user-scanner-form',
    'enableClientValidation' => true,
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => 'scanner-target',
    ]
)); ?>

    <h2>Mass manage - Postal</h2>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

    <p>Provice packages ids, for which you want to perform an action. Not found or invalid packages will be ignored.</p>
    <p>Max number of packages at once: <?= PostalMassManageController::MAX_NO;?></p>
    <br/>
    <iframe name="scanner-target" id="scanner-target" style="width: 0; height: 0; display: none;"></iframe>

    <div id="message-info" class="alert alert-info" style="display: none; margin: 15px 0;"></div>
    <div id="message-success" class="alert alert-success" style="display: none; margin: 15px 0;"></div>
    <div id="message-danger" class="alert alert-danger" style="display: none; margin: 15px 0;"></div>

    <div id="summary-block" style="display: none; padding-bottom: 15px;">
        <table class="table" style="max-width: 600px; margin: 15px auto;">
            <tr>
                <th class="text-right"><?= Yii::t('userScanner', 'Przetworzonych');?></th>
                <th class="text-center"><?= Yii::t('userScanner', 'Łącznie');?></th>
                <th><?= Yii::t('userScanner', 'Pominiętych');?></th>
            </tr>
            <tr style="font-size: 1.4em;">
                <td id="success-no" class="text-right" style="color: green;">0</td>
                <td id="total-no" class="text-center">0</td>
                <td id="fail-no" style="color: red;">0</td>
            </tr>
            <tr>
                <td><textarea id="success-list" style="width: 100%; resize: vertical; height: 150px; text-align: center;"></textarea></td>
                <td></td>
                <td><textarea id="fail-list" style="width: 100%; resize: vertical; height: 150px; text-align: center;"></textarea></td>
            </tr>
        </table>
    </div>
    <hr/>
    <div class="row text-center">
        <div class="span5">
            Inserted rows <strong id="numer-of-rows">0</strong><br/>
            <?= $form->textArea($model, 'items_ids',['id' => 'items-ids', 'style' => 'padding: 15px 5px; text-align: center; width: 400px; font-size: 1.1em; font-weight: bold; height: 500px; resize: vertical;', 'placeholder' => 'Provide packages ids, each one in new line']);?>
        </div>
        <div class="span6 text-left">
            <div id="action-buttons">
                <p>Bulk number (for **)</p>
                <?= CHtml::textField('bulk');?>
                <p>Sender HUB (for **)</p>
                <?= CHtml::dropDownList('hub', '', CHtml::listData(SpPoints::getPoints(),'id','name'), ['prompt' => '-']);?>
                <p>Actions for packages</p>
                <?= CHtml::submitButton('Get Manifest/ACK **', ['class' => 'btn btn-large btn-primary', 'name' => 'mode_ack']);?> <br/> <br/>
                <?= CHtml::submitButton('Get Manifest/ACK per operator **', ['class' => 'btn btn-large btn-primary', 'name' => 'mode_ack_operator']);?> <br/> <br/>
                <?= CHtml::submitButton('Get return label', ['class' => 'btn btn-large btn-primary', 'name' => 'mode_return_label']);?> <br/> <br/>
                <?= CHtml::submitButton('Get label 10x15', ['class' => 'btn btn-large btn-primary', 'name' => 'mode_label_10x15']);?><br/> <br/>
                <?= CHtml::submitButton('Export to XLS', ['class' => 'btn btn-large btn-primary', 'name' => 'mode_xls']);?> <br/> <br/>
                <?= CHtml::submitButton('Get Register Book (*)', ['class' => 'btn btn-large btn-primary', 'name' => 'mode_kn']);?> <br/> <br/>
                <?= CHtml::submitButton('Get Invoice proforma', ['class' => 'btn btn-large btn-primary', 'name' => 'mode_invpro']);?> <br/> <br/>
            </div>
            <div id="too-much-warning" style="display: none;" class="alert alert-danger text-center">
                Too much packages!
            </div>
        </div>
    </div>

<?php
$this->endWidget(); ?>

    <div style="margin: 15px;" class="text-right">
        <small>* Only for PocztaPolska</small>
    </div>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  statusCheck: '.CJSON::encode(Yii::app()->createUrl('/postal/postalMassManage/status', ['sid' => $sid])).',
              },
               loader: {
                  textTop: '.CJSON::encode('In progress...').',
                  textBottom: '.CJSON::encode('Please wait...').',
                  error:  '.CJSON::encode('Error occured! Please try again...').',
              },
              stat: {
                 success: '.CJSON::encode(PostalMassManageController::STATE_SUCCESS).',
                 fail: '.CJSON::encode(PostalMassManageController::STATE_FAIL).',
              },
              no: {
                 max: '.CJSON::encode(PostalMassManageController::MAX_NO).',
              },
              text: {
                 noItems: '.CJSON::encode('No package id provided!').',
              },
          };
      ',CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/user-scanner.js', CClientScript::POS_END);
?>