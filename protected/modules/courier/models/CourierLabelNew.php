<?php

Yii::import('application.modules.courier.models._base.BaseCourierLabelNew');

/**
 * Class CourierLabelNew
 * Class for handling all external labels requests implementing queque
 */
class CourierLabelNew extends BaseCourierLabelNew
{

    protected $_afterfind_track_id;

    public $_via_api = false;


    // not savable into DB. Just temporary.
    public $__temp_ebay_update_stat;

    public $_safepak_order_id;
    public $_ext_operator_name;


    public $_gls_consign_id;
    public $_gls_no_label;
    public $_gls_own_label;


    public $_ebay_order_id;

    public $_cancel_package_on_fail;

    public $_oversized;

    public $_internal_mode;

    public $_deliveryHubId;

    public $_glsnl_t400;
    public $_glsnl_t8913;


    public $_dpdpl_numkat;

    public $_custom_tracking_id = false;

    const SCENARIO_NO_ADDRESS = 1;

    const INTERNAL_MODE_FIRST = 1;
    const INTERNAL_MODE_SECOND = 2;
    const INTERNAL_MODE_COMMON = 3;

    // some labels contains more than one page - if so, how many additional?
    public $_additional_pages = 0;

    const STAT_NEW_DIRECT = 2; // this packages wont be run by queque - just directly
    const STAT_NEW = 0;
    const STAT_NEW_DEPENDANT = 3; // this will be called directly after first label will be generated in this two-labels set
    const STAT_SUCCESS = 10;
    const STAT_FAILED = 99;
    const STAT_FAILED_WITH_PROBLEM_LABEL = 98;
    const STAT_EXPIRED = 15;

    const STAT_WAITING_FOR_PARENT = 5;     // means that order will be generated at once together with parent package (know as: multipackage)


    const EXPIRATION_DAYS = 90;

    const OPERATOR_OWN = 9999;
    const OPERATOR_OWN_EXTERNAL_RETURN = 9998;
    const OPERATOR_UPS = 1;
    const OPERATOR_OOE = 2;
    const OPERATOR_UPS_SAVER = 3;
    const OPERATOR_INPOST = 4;
    const OPERATOR_SAFEPAK = 5;
    const OPERATOR_GLS = 6;

    const OPERATOR_RUCH = 7;

    const OPERATOR_PETER_SK = 8;

    const OPERATOR_POCZTA_POLSKA = 9;

    const OPERATOR_UPS_BIS = 10;
    const OPERATOR_UPS_SAVER_BIS = 11;

    const OPERATOR_UPS_F = 15;
    const OPERATOR_UPS_SAVER_F = 16;

    const OPERATOR_UPS_M = 17;
    const OPERATOR_UPS_SAVER_M = 18;

    const OPERATOR_UPS_EXPRESS = 12;
    const OPERATOR_UPS_EXPRESS_M = 13;

    const OPERATOR_UPS_UK = 14;

    const OPERATOR_DHL = 20;
    const OPERATOR_DHL_F = 21;

    const OPERATOR_GLS_NL = 22;

    const OPERATOR_UPS_EXPRESS_L = 25; // it's saver, not express

    const OPERATOR_UPS_SAVER_MCM = 26;
    const OPERATOR_UPS_SAVER_4VALUES = 27;

    const OPERATOR_DHLDE = 30;

    const OPERATOR_DHLEXPRESS = 35;

    const OPERATOR_UK_MAIL = 40;

    const OPERATOR_DPD_DE = 45;

    const OPERATOR_ROYALMAIL = 46;

    const OPERATOR_DPDPL = 47;

    const OPERATOR_HUNGARIAN_POST = 48;

    const OPERATOR_DPDNL = 49;

    const OPERATOR_POST11 = 50;

    const OPERATOR_LP_EXPRESS = 51;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getOperatorsWithPod()
    {
        return [
            self::OPERATOR_DHLEXPRESS,
            self::OPERATOR_DHLDE,
            self::OPERATOR_UPS,
            self::OPERATOR_UPS_BIS,
            self::OPERATOR_UPS_M,
            self::OPERATOR_UPS_UK,
            self::OPERATOR_UPS_F,
            self::OPERATOR_UPS_SAVER,
            self::OPERATOR_UPS_SAVER_F,
            self::OPERATOR_UPS_SAVER_BIS,
            self::OPERATOR_UPS_SAVER_M,
            self::OPERATOR_UPS_SAVER_MCM,
            self::OPERATOR_UPS_EXPRESS,
            self::OPERATOR_UPS_EXPRESS_L,
            self::OPERATOR_UPS_EXPRESS_M,
            self::OPERATOR_UPS_SAVER_4VALUES,
            self::OPERATOR_UK_MAIL,

        ];
    }

    public function isOperatorWithPod()
    {
        return in_array($this->operator,self::getOperatorsWithPod()) ? true : false;
    }

    public function getEpod()
    {
        switch($this->operator)
        {
            case self::OPERATOR_DHLEXPRESS:
                return DhlExpressClient::epod($this->track_id);
                break;
            case self::OPERATOR_DHLDE:
                return DhlDeClient::epod($this->track_id);
                break;
            case self::OPERATOR_UPS:
            case self::OPERATOR_UPS_BIS:
            case self::OPERATOR_UPS_M:
            case self::OPERATOR_UPS_UK:
            case self::OPERATOR_UPS_F:
            case self::OPERATOR_UPS_SAVER:
            case self::OPERATOR_UPS_SAVER_F:
            case self::OPERATOR_UPS_SAVER_BIS:
            case self::OPERATOR_UPS_SAVER_M:
            case self::OPERATOR_UPS_SAVER_MCM:
            case self::OPERATOR_UPS_EXPRESS:
            case self::OPERATOR_UPS_EXPRESS_L:
            case self::OPERATOR_UPS_EXPRESS_M:
            case self::OPERATOR_UPS_SAVER_4VALUES:
                return UpsTrackClient::epod($this->track_id);
                break;
            case self::OPERATOR_UK_MAIL:
                return UKMailClient::epod($this->track_id);
                break;
            default:
                return false;
                break;
        }
    }

    public static function getStats()
    {
        $data = array(
            self::STAT_NEW => 'Waiting',
            self::STAT_NEW_DIRECT => 'Waiting from API',
            self::STAT_NEW_DEPENDANT => 'Waiting for sibling',
            self::STAT_WAITING_FOR_PARENT => 'Waiting for parent',
            self::STAT_SUCCESS => 'OK',
            self::STAT_FAILED => 'Fail',
//            self::STAT_FAILED_WITH_PROBLEM_LABEL => 'Fail (with label)',
            self::STAT_EXPIRED => 'Expired',
        );

        return $data;
    }


    public static function getOperators()
    {
        $data = array(
            self::OPERATOR_OWN_EXTERNAL_RETURN => 'KAAB - External return',


            self::OPERATOR_UPS => 'UPS Std.',
            self::OPERATOR_OOE => 'OOE',
            self::OPERATOR_UPS_SAVER => 'UPS Saver',
            self::OPERATOR_INPOST => 'Inpost',
            self::OPERATOR_SAFEPAK => 'Safepak',
            self::OPERATOR_GLS => 'GLS',

            self::OPERATOR_RUCH => 'Ruch',

            self::OPERATOR_PETER_SK => 'KAAB_SK',

            self::OPERATOR_POCZTA_POLSKA => 'Poczta Polska',

            self::OPERATOR_GLS_NL => 'GLS NL',

            self::OPERATOR_UPS_BIS => 'UPS Std. BIS',
            self::OPERATOR_UPS_SAVER_BIS => 'UPS Saver BIS',

            self::OPERATOR_UPS_F => 'UPS Std. F',
            self::OPERATOR_UPS_SAVER_F => 'UPS Saver F',

            self::OPERATOR_UPS_EXPRESS => 'UPS Express',
            self::OPERATOR_UPS_EXPRESS_M => 'UPS Express M',

            self::OPERATOR_OWN => 'KAAB',

            self::OPERATOR_UPS_M => 'UPS Std. M',
            self::OPERATOR_UPS_SAVER_M => 'UPS Saver M',

            self::OPERATOR_UPS_EXPRESS_L => 'UPS Saver L',
            self::OPERATOR_UPS_SAVER_4VALUES => 'UPS Saver 4VALUES',

            self::OPERATOR_DHL => 'DHL',
            self::OPERATOR_DHL_F => 'DHL F',

            self::OPERATOR_UPS_UK => 'UPS UK',
            self::OPERATOR_UPS_SAVER_MCM => 'UPS Saver MCM',

            self::OPERATOR_DHLDE => 'DHL DE',
            self::OPERATOR_DHLEXPRESS => 'DHL EXPRESS',

            self::OPERATOR_UK_MAIL => 'UK Mail',

            self::OPERATOR_DPD_DE => 'DPD DE',

            self::OPERATOR_ROYALMAIL => 'Royal Mail',


            self::OPERATOR_DPDPL => 'DPD PL',
            self::OPERATOR_HUNGARIAN_POST => 'HUNGARIAN POST',

            self::OPERATOR_DPDNL => 'DPD NL',

            self::OPERATOR_POST11 => 'POST11',

            self::OPERATOR_LP_EXPRESS => 'LP EXPRESS',
        );

        $data = $data + GLOBAL_BROKERS::getBrokersList();

        asort($data);

        return $data;
    }

    public function isOversized()
    {
        return $this->_oversized;
    }

    public function howManyAdditionalPages()
    {
        return $this->_additional_pages;
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            )
        );
    }

    public function getLogMessage()
    {
        return Dictionary::getTranslatedText($this->log, Dictionary::TYPE_OPERATOR_COURIER, $this->operator);
    }

    /**
     * Unserialize additional params (ex. Preffered pickup time)
     * @return bool
     */
    public function afterFind()
    {
        $this->_afterfind_track_id = $this->track_id;

        if ($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if (is_array($this->params))
            foreach ($this->params AS $key => $item) {
                try {
                    $this->$key = $item;
                } catch (Exception $ex) {
                }
            }

        parent::afterFind();
    }

    /**
     * Serialize additional params (ex. Preffered pickup time)
     * @return bool
     */

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => $this->data . $this->date_entered . microtime() . rand(1,9999)));


            if($this->courier !== NULL && $this->stat == self::STAT_NEW)
            {
                //  SPECIAL RULE FOR API!
                if( in_array($this->courier->source, [Courier::SOURCE_API, Courier::SOURCE_AUTOMATIC_API])) {
//                if($this->courier->getType() == Courier::TYPE_DOMESTIC)
//                    $this->stat = self::STAT_NEW_DIRECT;
                    if ($this->courier->getType() == Courier::TYPE_INTERNAL) {
                        if ($this->_internal_mode == self::INTERNAL_MODE_COMMON) {
                            $this->stat = self::STAT_NEW_DIRECT;
                            $this->_via_api = true;
                        } else if ($this->_internal_mode == self::INTERNAL_MODE_FIRST && !$this->courier->user->getAllowCourierInternalExtendedLabel()) {
                            $this->stat = self::STAT_NEW_DIRECT;
                            $this->_via_api = true;
                        } else if ($this->_internal_mode == self::INTERNAL_MODE_SECOND && $this->courier->user->getAllowCourierInternalExtendedLabel()) {
                            $this->stat = self::STAT_NEW_DIRECT;
                            $this->_via_api = true;
                        }

                        //  SPECIAL RULE AND FOR THE REST IN DUAL OPERATOR

                        if ($this->_internal_mode == self::INTERNAL_MODE_FIRST && $this->courier->user->getAllowCourierInternalExtendedLabel()) {
                            $this->stat = self::STAT_NEW_DEPENDANT;
                            $this->_via_api = true;
                        } else if ($this->_internal_mode == self::INTERNAL_MODE_SECOND && !$this->courier->user->getAllowCourierInternalExtendedLabel()) {
                            $this->stat = self::STAT_NEW_DEPENDANT;
                            $this->_via_api = true;
                        }


                    } else if (in_array($this->courier->getType(), [Courier::TYPE_U, Courier::TYPE_RETURN]) OR $this->courier->courier_type == Courier::TYPE_RETURN) {
                        $this->stat = self::STAT_NEW_DIRECT;
                        $this->_via_api = true;
                    }
                } else {

                    //  SPECIAL RULE AND FOR THE REST IN DUAL OPERATOR
                    if ($this->courier->getType() == Courier::TYPE_INTERNAL) {

                        if ($this->_internal_mode == self::INTERNAL_MODE_FIRST && $this->courier->user->getAllowCourierInternalExtendedLabel()) {
                            $this->stat = self::STAT_NEW_DEPENDANT;
                        } else if ($this->_internal_mode == self::INTERNAL_MODE_SECOND && !$this->courier->user->getAllowCourierInternalExtendedLabel()) {
                            $this->stat = self::STAT_NEW_DEPENDANT;
                        }
                    }



                }
            }
//            $this->stat = self::STAT_NEW_DIRECT;

//            MyDump::dump('clnlog.txt', print_r($this->courier->getType(),1));
//            MyDump::dump('clnlog.txt', print_r($this->courier->courierTypeReturn,1));
//            MyDump::dump('clnlog.txt', print_r($this->courier->courierTypeReturn,1));
//            MyDump::dump('clnlog.txt', print_r($this->courier->source,1));
        }

        $this->params = array(
//            '_safepak_order_id' => $this->_safepak_order_id,
            '_oversized' => $this->_oversized,
            '_ebay_order_id' => $this->_ebay_order_id,
            '_cancel_package_on_fail' => $this->_cancel_package_on_fail,
            '_additional_pages' => $this->_additional_pages,
            '_internal_mode' => $this->_internal_mode,
            '_gls_consign_id' => $this->_gls_consign_id,
            '_gls_no_label' => $this->_gls_no_label,
            '_deliveryHubId' => $this->_deliveryHubId,
            '_glsnl_t400' => $this->_glsnl_t400,
            '_glsnl_t8913' => $this->_glsnl_t8913,
            '_ext_operator_name' => $this->_ext_operator_name,
            '_via_api' => $this->_via_api,
            '_custom_tracking_id' => $this->_custom_tracking_id,
            '_dpdpl_numkat' => $this->_dpdpl_numkat,
        );

        // serialize only if not all values are empty
        if (array_filter($this->params))
            $this->params = self::serialize($this->params);
        else
            $this->params = NULL;

        return parent::beforeSave();
    }

    public function afterSave()
    {
        if(($this->isNewRecord OR $this->_afterfind_track_id === NULL) && $this->track_id) {
            IdStore::createNumber(IdStore::TYPE_COURIER, $this->courier_id, $this->track_id);
        }
        else if($this->track_id != $this->_afterfind_track_id)
        {
            IdStore::updateNumber(IdStore::TYPE_COURIER, $this->courier_id, $this->_afterfind_track_id, $this->track_id);
        }
        return parent::afterSave();
    }

    public function rules()
    {
        return array(
            array('courier_id', 'required'),
            array('address_data_from_id, address_data_to_id', 'required', 'except' => self::SCENARIO_NO_ADDRESS),
            array('courier_id,  address_data_from_id, address_data_to_id, parent_id, operator, downloaded, stat', 'numerical', 'integerOnly' => true),
            array('hash', 'length', 'max' => 64),
            array('file_type', 'length', 'max' => 32),
            array('file_path', 'length', 'max' => 128),
            array('track_id', 'length', 'max' => 128),
            array('_ext_operator_name', 'length', 'max' => 32),
            array('date_updated, params', 'safe'),
            array('parent_id, date_updated, file_type, file_path, track_id, data, log', 'default', 'setOnEmpty' => true, 'value' => null),
            array('triggers_pickup', 'default', 'setOnEmpty' => true, 'value' => false),

            array('downloaded', 'default', 'setOnEmpty' => true, 'value' => false),
            array('params', 'default', 'setOnEmpty' => true, 'value' => NULL),
            array('stat', 'default', 'setOnEmpty' => true, 'value' => self::STAT_NEW),

            array('operator', 'default', 'setOnEmpty' => true, 'value' => 0),

            array('id, date_entered, date_updated, courier_id, parent_id, hash, data, file_type, file_path, track_id, operator, downloaded, stat, log, triggers_pickup', 'safe', 'on' => 'search'),
        );
    }

    public static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    public static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    public static function _preprocessZebraImagick(Imagick $imagick)
    {
        $imagick->setResolution(300, 300);
        // REMOVING ALPHA TO BE REMOVED AS DONE BEFORE?
        if ($imagick->getImageAlphaChannel()) {
            $imagick->setImageBackgroundColor('white');
            $imagick->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $imagick->trimImage(0);

        if($imagick->getImageWidth() > $imagick->getImageHeight())
            $imagick->rotateImage(new ImagickPixel(), 90);

        $imagick->borderImage('white', 20, 20);
        $imagick->scaleImage(PrintServer::ZEBRA_PAPER_WIDTH,PrintServer::ZEBRA_PAPER_HEIGHT, true);
        $imagick->stripImage();

        return $imagick;
    }

    /**
     * Returns label as string from image
     * @param bool|false $noError
     * @param bool|true $omitAdditionalPages If false, response will be array containng one or more pages
     * @return bool|string|array
     * @throws Exception
     */
    public function getFileAsString($noError = false, $omitAdditionalPages = true, $forceZebraRollSize = false)
    {
        // Yii::app()->courierLabelNewLog2->saveToLog($this, CourierLabelNewLog2::WHAT_GET_FILE_START);
        // CourierLabelNewLog::saveToLog($this, // CourierLabelNewLog::WHAT_GET_FILE_START);


        $data = false;
        if (!$noError) {
            if ($this->stat == self::STAT_NEW OR $this->stat == self::STAT_WAITING_FOR_PARENT) {
                throw new CHttpException(ErrorCodes::LABEL_PROBLEM, Yii::t('courier', 'Nie możemy wygenerować Twojej etykiety - nie została ona jeszcze przetworzona przez operatora!').' (#'.$this->courier->local_id.')');
            } else if ($this->stat == self::STAT_FAILED OR $this->courier->courier_stat_id == CourierStat::CANCELLED_PROBLEM) {

//                if($this->courier->user_id == User::USER_4VALUES)
                {
                    $image = ProblemLabel::generate($this->courier, $this->getLogMessage());
                    if($forceZebraRollSize)
                    {
                        $imagick = ImagickMine::newInstance();
                        $imagick->readImageBlob($image);
                        $imagick = self::_preprocessZebraImagick($imagick);
                        $image = $imagick->getImageBlob();

                    }

                    return [ $image ];
                }
//                else
//                    throw new CHttpException(ErrorCodes::LABEL_PROBLEM, Yii::t('courier', 'Wystąpił błąd przy generowaniu Twojej etykiety! Prosimy o kontakt w tej sprawie').' (#'.$this->courier->local_id.')');

            } else if ($this->stat == self::STAT_EXPIRED) {
                throw new CHttpException(ErrorCodes::LABEL_PROBLEM, Yii::t('courier', 'Etykieta dla tej paczki już wygasła').' (#'.$this->courier->local_id.')');
            }
        }

        if($noError && $this->stat == self::STAT_EXPIRED)
            return false;

//        if($this->courier->user_id == User::USER_4VALUES && $this->stat == self::STAT_FAILED)
        if($this->stat == self::STAT_FAILED AND $this->courier->courier_stat_id == CourierStat::CANCELLED_PROBLEM)
        {
            $image = ProblemLabel::generate($this->courier, $this->getLogMessage());
            if($forceZebraRollSize)
            {
                $imagick = ImagickMine::newInstance();
                $imagick->readImageBlob($image);
                $imagick = self::_preprocessZebraImagick($imagick);
                $image = $imagick->getImageBlob();

            }

            return [ $image ];
        }

        try {

            // SPECAIL RULE FOR GLS WITH PICK&SHIP WHEN NO LABEL IS PROVIDED
            if ($this->_gls_no_label) {
                if ($this->courier->courierTypeInternal)
                    $no = $this->courier->courierTypeInternal->family_member_number;
                else if ($this->courier->courierTypeDomestic)
                    $no = $this->courier->courierTypeDomestic->family_member_number;
                else
                    $no = 1;

                $return = KaabLabel::generate($this->courier, $no, $this->addressDataFrom, $this->addressDataTo, false, false, true, 'VIA GLS (' . $this->track_id . ')');

                if (!$omitAdditionalPages)
                    $return = [$return];

                // Yii::app()->courierLabelNewLog2->saveToLog($this, CourierLabelNewLog2::WHAT_GET_FILE_END);
                // CourierLabelNewLog::saveToLog($this, // CourierLabelNewLog::WHAT_GET_FILE_END);
                return $return;
            }




            if ($omitAdditionalPages) {

                if (@exif_imagetype(Yii::app()->basePath . '/../' . $this->file_path)) {

                    if($forceZebraRollSize) {

                        $imagick = ImagickMine::newInstance();
                        $imagick->readImage(Yii::app()->basePath . '/../' . $this->file_path);
                        $imagick = self::_preprocessZebraImagick($imagick);
                        $image = $imagick->getImageBlob();
                    } else {
                        if(in_array($this->operator, [self::OPERATOR_DHL, self::OPERATOR_DHL_F]))
                        {
                            $imagick = ImagickMine::newInstance();
                            $imagick->readImage(Yii::app()->basePath . '/../' . $this->file_path);
                            $imagick->stripImage();
                            $image = $imagick->getImageBlob();
                        } else
                            $image = @file_get_contents(Yii::app()->basePath . '/../' . $this->file_path);
                    }

                    // CourierLabelNewLog::saveToLog($this, // CourierLabelNewLog::WHAT_GET_FILE_END);
                    // Yii::app()->courierLabelNewLog2->saveToLog($this, CourierLabelNewLog2::WHAT_GET_FILE_END);

                    return $image;
                }
            } else {

                $return = [];

                if (@exif_imagetype(Yii::app()->basePath . '/../' . $this->file_path)) {

                    if($forceZebraRollSize) {
                        $imagick = ImagickMine::newInstance();
                        $imagick->readImage(Yii::app()->basePath . '/../' . $this->file_path);
                        $imagick = self::_preprocessZebraImagick($imagick);
                        $image = $imagick->getImageBlob();
                    } else {

                        if(in_array($this->operator, [self::OPERATOR_DHL, self::OPERATOR_DHL_F]))
                        {
                            $imagick = ImagickMine::newInstance();
                            $imagick->readImage(Yii::app()->basePath . '/../' . $this->file_path);
                            $imagick->stripImage();
                            $image = $imagick->getImageBlob();
                        } else
                            $image = @file_get_contents(Yii::app()->basePath . '/../' . $this->file_path);
                    }

                    $return[0] = $image;
                }

                for ($i = 1; $i <= $this->_additional_pages; $i++) {

                    // add _{NUMBER_OF_PAGE} just before extension...
                    $path = explode('.', $this->file_path);
                    $lastElementBeforeExtension = S_Useful::sizeof($path) - 2;

                    if ($lastElementBeforeExtension >= 0) {
                        $path[$lastElementBeforeExtension] = $path[$lastElementBeforeExtension] . '_' . $i;
                        $path = implode('.', $path);

                        // and check and return this image

                        if (@exif_imagetype(Yii::app()->basePath . '/../' . $path)) {

                            if ($forceZebraRollSize) {
                                $imagick = ImagickMine::newInstance();
                                $imagick->readImage(Yii::app()->basePath . '/../' . $path);
                                $imagick = self::_preprocessZebraImagick($imagick);
                                $image = $imagick->getImageBlob();
                            } else {

                                if(in_array($this->operator, [self::OPERATOR_DHL, self::OPERATOR_DHL_F]))
                                {
                                    $imagick = ImagickMine::newInstance();
                                    $imagick->readImage(Yii::app()->basePath . '/../' . $path);
                                    $imagick->stripImage();
                                    $image = $imagick->getImageBlob();
                                } else
                                    $image = @file_get_contents(Yii::app()->basePath . '/../' . $path);
                            }

                            $return[$i] = $image;

                        }
                    } else
                        continue;
                }

                // Yii::app()->courierLabelNewLog2->saveToLog($this, CourierLabelNewLog2::WHAT_GET_FILE_END);
                return $return;
            }

            if (!$noError)
                throw new CHttpException(ErrorCodes::LABEL_PROBLEM,Yii::t('courier', 'Wystąpił błąd przy generowaniu Twojej etykiety! Prosimy o kontakt w tej sprawie').' (#'.$this->courier->local_id.')');
            else
                return false;
        } catch (Exception $ex) {
            Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);

            if (!$noError)
                throw new CHttpException(ErrorCodes::LABEL_PROBLEM, Yii::t('courier', 'Wystąpił błąd przy generowaniu Twojej etykiety! Prosimy o kontakt w tej sprawie').' (#'.$this->courier->local_id.')');
            else
                return false;
        }

        if (!$data && !$noError)
            throw new CHttpException(ErrorCodes::LABEL_PROBLEM,Yii::t('courier', 'Wystąpił błąd przy generowaniu Twojej etykiety! Prosimy o kontakt w tej sprawie').' (#'.$this->courier->local_id.')');
        else if (!$data)
            return false;
        else {
            // CourierLabelNewLog::saveToLog($this, // CourierLabelNewLog::WHAT_GET_FILE_END);
            return $data;
        }
    }


    public static function asyncCallForId2($id, $counter = 0)
    {
        exec("C:\Dane\WWW\Localhost\httpd\SwiatPrzesylek.pl\swiatPrzesylek_ms5\protected\yiic.bat labelQueque index --id=".$id);
//        exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic labelQueque index --id=".$id." > /dev/null &");
    }


    /**
     * Async call for label ordering. Allow ordering label in background
     * @param int $id Id of CourierLabelNew to be called
     * @param int $counter Limit number of operations
     * @param int $mdelay Time to wait before label ordering [seconds]
     */
    public static function asyncCallForId($id, $counter = 0)
    {
//        save into cache with date and minute in name _user _id//
        $CACHE_NAME = 'ASYNC_LABEL_QUEQUE_'.Yii::app()->user->id.'_'.date('Ymdhi');
//        MyDump::dump('label_async_url.txt', 'CN:'.$CACHE_NAME);

        $check = Yii::app()->cache->get($CACHE_NAME);

//        MyDump::dump('label_async_url.txt', 'CHECK:'.print_r($check,1));
        if($check === false)
        {
            Yii::app()->cache->set($CACHE_NAME, 0, 60);
        } else {

            if ($check === 10) {
                return false;
            }
            Yii::app()->cache->set($CACHE_NAME, (intval($check) + 1), 60);
        }

        $counter = $check;

        $ch = curl_init();
        $url = Yii::app()->createAbsoluteUrl('labelQueque/index', array('id' => $id, 'async' => 'true', 'counter' => $counter), 'http');

        if(!YII_LOCAL)
            $url = str_replace(PageVersion::getDomainsList(true), 'swiatprzesylek.pl', $url);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec($ch);

        curl_close($ch);
    }


    const SPECIAL_MODE_TYPE_OOE = 1;
    const SPECIAL_MODE_GLS_NL = 2;
    const SPECIAL_MODE_DHL = 3;
    const SPECIAL_MODE_GLS = 4;
    const SPECIAL_MODE_TYPE_INTERNAL_OOE = 5;
    const SPECIAL_MODE_DPDPL = 6;


    protected function _processOrderAdapter(array $globalOperatorResponse, $logSourcePrefix, $blockPickup, Courier $courier, &$arrayOfIds, $specialMode = false)
    {
        $multipackage = GLOBAL_BROKERS::isMultipackageBroker($this->operator);
        $operatorName = GLOBAL_BROKERS::getBrokerNameById($this->operator);

        /* @var $globalOperatorResponse GlobalOperatorResponse[] */
        $responseArray = [];
        foreach($globalOperatorResponse AS $item)
        {

            if($item->isSuccess()) {
                $data = [
                    'status' => true,
                    'label' => $item->getLabel(),
                    'track_id' => $item->getTt(),
                    '_additional_pages' => $item->getLabelsNo() - 1,
                ];

                if($item->getCustomTrackingId())
                    $data['_custom_tracking_id'] = $item->getCustomTrackingId();

                if($item->getAckType())
                {
                    $data['ack'] = $item->getAck();
                    $data['ack_type'] = $item->getAckType();
                    $data['ack_source'] = $this->operator;
                }
                $responseArray[] = $data;
            } else {
                $responseArray[0] = [
                    'status' => false,
                    'error' => $item->getError() != '' ? $item->getError() : 'Unknown error #0',
                ];
                break;
            }
        }

        $this->_processOrder($responseArray, $multipackage, $operatorName, false, $item->getCollectionObjName(), $item->getCollectionMethodName(), $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, $specialMode);
    }

    /**
     * @param array $response Response from operator implementation method
     * @param bool $multipackage Whenter array can contain more than one pagage in response
     * @param string $operatorName Operator name used to logs
     * @param string $savingMethod Name of label's saving method from this class to use
     * @param string $collectionObj Name of Class to use for collectionMethod - if collection is separate method for particular operator
     * @param string $collectionMethod Name of method in $collectionObj to use for ordering package Collection
     * @param string $logSourcePrefix Prefix to use in error logging (for Interian - if it's first or second package)
     * @param bool $blockPickup Whether to block calling collection method
     * @param Courier $courier
     * @param array $arrayOfIds Array of processed packages Id's - used for reporing of ordering packages
     * @param bool $specialMode Used for special processing operations for some operators
     */
    protected function _processOrder(array $response, $multipackage, $operatorName, $savingMethod, $collectionObj, $collectionMethod, $logSourcePrefix, $blockPickup, Courier $courier, &$arrayOfIds, $specialMode = false)
    {
        // Yii::app()->courierLabelNewLog2->saveToLog($this, CourierLabelNewLog2::WHAT_PROCESS_LABEL_START);
        // CourierLabelNewLog::saveToLog($this, // CourierLabelNewLog::WHAT_PROCESS_LABEL_START);
        $first = true;
        foreach ($response AS $item) {

            if ($first OR !$multipackage) {

                self::_processOrder_Item($this, true, $item, $savingMethod, $collectionObj, $collectionMethod, $operatorName, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, $specialMode);

                $first = false;
            } else {

                $child = self::model()->find(array('condition' => 'parent_id = :parent AND stat = :stat', 'params' => array(':parent' => $this->id, ':stat' => self::STAT_WAITING_FOR_PARENT), 'order' => 'id ASC', 'limit' => 1));
                if ($child !== NULL) {

                    self::_processOrder_Item($child, false, $item, $savingMethod, $collectionObj, $collectionMethod, $operatorName, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, $specialMode);
                }
            }
        }
        // Yii::app()->courierLabelNewLog2->saveToLog($this, CourierLabelNewLog2::WHAT_PROCESS_LABEL_END);
        // CourierLabelNewLog::saveToLog($this, // CourierLabelNewLog::WHAT_PROCESS_LABEL_END);
    }

    protected function _processOrder_Item(CourierLabelNew $that, $isParent, $item, $savingMethod, $collectionObj, $collectionMethod, $operatorName, $logSourcePrefix, $blockPickup, Courier $courier, &$arrayOfIds, $specialMode = false)
    {

        if($isParent && ($that->_internal_mode == self::INTERNAL_MODE_SECOND && $that->courier->user->getAllowCourierInternalExtendedLabel() OR $that->_internal_mode == self::INTERNAL_MODE_FIRST && !$that->courier->user->getAllowCourierInternalExtendedLabel()))
        {
            /* @var $sibling CourierLabelNew */
            $sibling = CourierLabelNew::model()->find('courier_id = :courier_id AND id != :id AND stat = :stat', [
                ':courier_id' => $that->courier_id,
                ':id' => $that->id,
                ':stat' => self::STAT_NEW_DEPENDANT
            ]);

            if($sibling) {
                $sibling->runLabelOrdering();
                $sibling->refresh();

                if($sibling->stat == self::STAT_FAILED) {
                    $item['error'] = '2ND LABEL: ' . $sibling->log; // force error on first label if second crashed
                    MyDump::dump('cnl_sibling.txt', print_r($this->id,1));
                }
            }

        }

        // IF THERE WAS PROBLEM WITH ORDER
        if (isset($item['error'])) {

            self::_processOrder_OnError($that, $isParent,$item,$operatorName, $logSourcePrefix, $arrayOfIds, $specialMode);

            // ON SUCCESS ORDER:
        } else {

            if($specialMode == self::SPECIAL_MODE_GLS)
            {
                if ($that->_gls_no_label != true) {
                    try {
                        $that->saveGlsLabel($item['label'], $item['ownLabel']);
                    } // on catch, act as order failure
                    catch (Exception $ex) {

                        self::_processOrder_OnError($that, $isParent, $item, $operatorName, $logSourcePrefix, $arrayOfIds, $specialMode);
                        // break current loop action and jump to next loop
                        return false;
                    }
                }
            }

            self::_processOrder_OnSuccess($that, $isParent, $item, $savingMethod, $collectionObj, $collectionMethod, $blockPickup, $courier, $arrayOfIds, $specialMode);
        }

        if ($that->_ebay_order_id != '')
            $that->updateEbayData(isset($item['error']));

        if (isset($item['error']) && $that->_cancel_package_on_fail)
            $that->courier->changeStat(CourierStat::CANCELLED);
    }

    protected function _processOrder_OnError(CourierLabelNew $that, $isParent, $item, $operatorName, $logSourcePrefix, &$arrayOfIds, $specialMode)
    {

        array_push($arrayOfIds, ['id' => $that->courier_id, 'stat' => self::STAT_FAILED]);
//        $that->stat = self::STAT_FAILED_WITH_PROBLEM_LABEL;
        $that->stat = self::STAT_FAILED;
        $that->log = $item['error'];

        if($specialMode == self::SPECIAL_MODE_GLS)
        {
            $that->_gls_consign_id = $item['gls_consign_id'];
            $that->_gls_no_label = $item['gls_no_label'];
        }

//        $problemLabel = $label = ProblemLabel::generate($that->courier, strip_tags($item['error']));
//        $that->saveProblemLabel($problemLabel);

//        $that->update('error', 'stat', 'date_updated', 'log', 'params', 'file_path', 'file_type');
        $that->update('error', 'stat', 'date_updated', 'log', 'params');

        // @ 20.03.2017
        // Change stat depending on extended label generating setting
        if($that->courier->user->getAllowCourierInternalExtendedLabel())
        {
            // IF USER HAS EXTENDED LABEL GENERATION
            // DO NOT SET PROBLEM STATUS FOR FIRST LABEL OF TWO IN INTERNAL PACKAGES

            if ($that->_internal_mode != self::INTERNAL_MODE_FIRST)
                $that->onProblem();
        } else {
            // DO NOT SET PROBLEM STATUS FOR SECOND LABEL OF TWO IN INTERNAL PACKAGES
            if ($that->_internal_mode != self::INTERNAL_MODE_SECOND)
                $that->onProblem();
        }

        // @ 29.12.2017
        // remove ref number on error
        if($that->courier->ref)
        {
            $ref = $that->courier->ref;
            $that->courier->ref = NULL;
            $that->courier->update(['ref']);
            $that->courier->addToLog('Removed REF number on error: '.$ref, CourierLog::CAT_ERROR);
        }

        // OLD WAY:
        // DO NOT SET PROBELM STATUS FOR SECOND LABEL OF TWO IN INTERNAL PACKAGES
//        if ($this->_internal_mode != self::INTERNAL_MODE_SECOND)
//            $this->courier->changeStat(CourierStat::PACKAGE_PROBLEMS);

        $that->courier->addToLog($that->log, CourierLog::CAT_ERROR, $logSourcePrefix . $operatorName);

        if($that->courier->user_id == User::USER_4VALUES)
            PrintServer::newCourierLabelNotify($that);

        if($isParent)
            self::updateChildrenWithError($that->id, $that->log, $arrayOfIds, $that->_cancel_package_on_fail, CourierLog::CAT_ERROR, $logSourcePrefix . $operatorName, $that->_internal_mode);
    }

    protected function _processOrder_OnSuccess(CourierLabelNew $that, $isParent, $item, $savingMethod, $collectionObj, $collectionMethod, $blockPickup, Courier $courier, &$arrayOfIds, $specialMode = false)
    {
        array_push($arrayOfIds, ['id' => $that->courier_id, 'stat' => self::STAT_SUCCESS]);

        if($that->courier->_generate_return && (
                $this->_internal_mode == '' OR
                $this->_internal_mode == self::INTERNAL_MODE_COMMON OR
                ($this->_internal_mode == self::INTERNAL_MODE_SECOND && $this->courier->user->getAllowCourierInternalExtendedLabel()) OR
                ($this->_internal_mode == self::INTERNAL_MODE_FIRST && !$this->courier->user->getAllowCourierInternalExtendedLabel())))
            CourierTypeReturn::createPackageOnCourier($that->courier, true, $this->_via_api);


        if($item['_additional_pages'])
            $that->_additional_pages = $item['_additional_pages'];

        if($specialMode != self::SPECIAL_MODE_GLS) {

            if(!$savingMethod)
                $savingMethod = 'saveLabelDefault';

            call_user_func(array($that, $savingMethod), $item['label']);
        }

        if($item['ack'] && $item['ack_source'] && $item['ack_type'])
            CourierExtExtAckCards::saveNewAckCard($that, $item['ack'], $item['ack_source'], $item['ack_type']);

        $that->track_id = $item['track_id'];
        $that->stat = self::STAT_SUCCESS;


        if($item['_custom_tracking_id'])
            $that->_custom_tracking_id = $item['_custom_tracking_id'];

        if($specialMode == self::SPECIAL_MODE_GLS)
        {
            $that->_gls_consign_id = $item['gls_consign_id'];
            $that->_gls_no_label = $item['gls_no_label'];
        }

        if($specialMode == self::SPECIAL_MODE_DPDPL)
        {
            $that->_dpdpl_numkat = $item['_dpdpl_numkat'];
        }

        if($specialMode == self::SPECIAL_MODE_GLS_NL)
        {
            $that->_glsnl_t8913 = $item['t8913'];
            $that->_glsnl_t400 = $item['t400'];
        }

        if($specialMode == self::SPECIAL_MODE_TYPE_INTERNAL_OOE OR $specialMode == self::SPECIAL_MODE_TYPE_OOE)
        {
            $that->track_id = $item['trackId'];
            $that->log = $item['label'];
        }

        if ($item['oversized'])
            $that->_oversized = true;

        $that->update('track_id', 'stat', 'date_updated', 'file_path', 'file_type');

        if($isParent && $specialMode == self::SPECIAL_MODE_TYPE_OOE)
        {
            $that->courier->courierTypeOoe->remote_id = $item['trackId'];
            $that->courier->courierTypeOoe->label_url = $item['label'];
            $that->courier->courierTypeOoe->update(['remote_id', 'label_url']);
        }



        // @ 20.03.2017
        // Change stat depending on extended label generating setting
        if($that->courier->user->getAllowCourierInternalExtendedLabel())
        {
            // IF USER HAS EXTENDED LABEL GENERATION
            // DO NOT SET PROBLEM STATUS FOR FIRST LABEL OF TWO IN INTERNAL PACKAGES
            if ($that->_internal_mode != self::INTERNAL_MODE_FIRST)
                $that->courier->changeStat(CourierStat::PACKAGE_RETREIVE);
        } else {
            // DO NOT SET PROBLEM STATUS FOR SECOND LABEL OF TWO IN INTERNAL PACKAGES
            if ($that->_internal_mode != self::INTERNAL_MODE_SECOND)
                $that->courier->changeStat(CourierStat::PACKAGE_RETREIVE);
        }




        if ($that->triggers_pickup) {
            // Prepare fake DHL CourierCollect object with real DHL collect_id, because collection is done during label ordering
            if($specialMode == self::SPECIAL_MODE_DHL) {
                if ($item['collect_id'])
                    CourierCollect::createReadyCollection($item['collect_id'], $that->courier_id, CourierCollect::OPERATOR_DHL);
            }
            else if($specialMode == self::SPECIAL_MODE_DPDPL)
            {
                if($item['collect_id'])
                    CourierCollect::createReadyCollection($item['collect_id'], $that->courier_id, CourierCollect::OPERATOR_DPDPL);
            } else {
                if ($isParent && !$blockPickup && $collectionObj && $collectionMethod) {
                    call_user_func(array($collectionObj, $collectionMethod), $courier);
                }
            }
        }



        // PRINT SERVER:
        PrintServer::newCourierLabelNotify($that);

        // LABEL MAILER
        if($that->courier->_sendLabelOnMail)
            LabelMailer::newCourierLabelNotify($that);

        // INVOICE
        if($isParent
            &&
            $this->courier->courierTypeInternal &&
            $this->courier->courierTypeInternal->family_member_number == 1
            && $this->courier->courierTypeInternal->order->payment_type_id == PaymentType::PAYMENT_TYPE_ONLINE
        )
        {
            // Create invoice depending on extended label generating setting
            if ($isParent && $that->courier->user->getAllowCourierInternalExtendedLabel()) {
                // IF USER HAS EXTENDED LABEL GENERATION
                // DO NOT GENERATE INVOICE ON FIRST LABEL OF TWO IN INTERNAL PACKAGES
                if ($that->_internal_mode != self::INTERNAL_MODE_FIRST)
                    $this->courier->courierTypeInternal->order->createInvoice();
            } else {
                // DO NOT GENERATE INVOICE ON SECOND LABEL OF TWO IN INTERNAL PACKAGES
                if ($that->_internal_mode != self::INTERNAL_MODE_SECOND)
                    $this->courier->courierTypeInternal->order->createInvoice();
            }
        }

    }

    /**
     * Order label and return standarized array with label, stat and track id.
     * Calls additional operations like update eBay data or UPS collection
     * @param bool|false $returnArrayOfIds
     * @return array
     * @throws CDbException
     */
    public function runLabelOrdering($returnArrayOfIds = false)
    {
        MyDump::dump('cln.txt', $this->id.' : START');

        $semaphore = new Semaphore(intval(Semaphore::COURIER_LABEL_NEW_PREFIX.$this->id), Yii::app()->basePath . '/_temp/');
        try {
            $semaphore->acquire();
        } catch (Exception $e) {
            MyDump::dump('cln.txt', $this->id.' : SEM LOCKED ('.Semaphore::COURIER_LABEL_NEW_PREFIX.intval($this->id).')');
            return false;
        }

        MyDump::dump('cln.txt', $this->id.' : AFTER SEM INIT ('.Semaphore::COURIER_LABEL_NEW_PREFIX.intval($this->id).')');

        // Yii::app()->courierLabelNewLog2->saveToLog($this, CourierLabelNewLog2::WHAT_ORDER_LABEL_START);
        // CourierLabelNewLog::saveToLog($this, // CourierLabelNewLog::WHAT_ORDER_LABEL_START);

        $this->refresh();
        if ($this->stat != self::STAT_NEW AND $this->stat != self::STAT_NEW_DIRECT AND $this->stat != self::STAT_NEW_DEPENDANT) {
            MyDump::dump('cln.txt', $this->id.' : ALREADY DONE');
            return false;
        }

        MyDump::dump('cln.txt', $this->id.' : GO!');

        $blockPickup = false;

        $arrayOfIds = [];

        $courier = Courier::model()->findByPk($this->courier_id);
        $from = AddressData::model()->findByPk($this->address_data_from_id);
        $to = AddressData::model()->findByPk($this->address_data_to_id);

        // PICKUP_MOD / 14.09.2016
//        if ($courier->getType() == Courier::TYPE_INTERNAL && !$courier->courierTypeInternal->with_pickup)
        if ($courier->getType() == Courier::TYPE_INTERNAL && $courier->courierTypeInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR) {
            $blockPickup = true;
        }
//        else if ($courier->getType() == Courier::TYPE_INTERNAL_SIMPLE && !$courier->courierTypeInternalSimple->with_pickup) {
//            $blockPickup = true;
//        }

        $logSourcePrefix = '';
        if ($this->_internal_mode == self::INTERNAL_MODE_FIRST)
            $logSourcePrefix = 'PICKUP / ';
        else if ($this->_internal_mode == self::INTERNAL_MODE_SECOND)
            $logSourcePrefix = 'DELIVERY / ';


        // NO COD FOR FIRST PACKAGE OF TWO
        if ($this->_internal_mode == self::INTERNAL_MODE_FIRST)
        {
            $courier->cod = $courier->courierTypeInternal->courier->cod = 0;
            $courier->cod_value = $courier->courierTypeInternal->courier->cod_value = 0;
            $courier->cod_currency = $courier->courierTypeInternal->courier->cod_currency = NULL;
        }

        //check cache once more
//        usleep(100000);
//        $CV = $CACHE->get($CACHE_NAME);
//        MyDump::dump('cln.txt', $this->id.' : '.$CACHE_VALUE.' : RECHECK DUPLICATED? : '.$CV);
//        if ($CV !== false && $CV != $CACHE_VALUE)
//        {
//            MyDump::dump('cln.txt', $this->id.' : '.$CACHE_VALUE.' : RECHECK DUPLICATED HIT!');
//            return false;
//        }

//        MyDump::dump('cln.txt', $this->id.' : '.$CACHE_VALUE.' : EXECUTE!');

        if(in_array($this->operator, array_keys(GLOBAL_BROKERS::getBrokersList()))) {

            try {
                $operatorClient = GLOBAL_BROKERS::globalOperatorClientFactory($this->operator);

                if ($courier->courierTypeInternal) {

                    $data = self::unserialize($this->data);
                    $response = $operatorClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, $data['operator_uniq_id'], $this, $blockPickup, true);
                } else if ($courier->courierTypeU) {
                    $response = $operatorClient::orderForCourierU($courier->courierTypeU, $this, true);
                } else if ($courier->courierTypeReturn) {
                    $response = $operatorClient::orderForCourierReturn($courier->courierTypeReturn, $this, true);
                } else {
                    $this->stat = self::STAT_FAILED;
                    $this->update(array('stat'));
                    exit;
                }
            } catch (Exception $ex) {
                Yii::log(print_r($ex->getMessage(), 1), CLogger::LEVEL_ERROR);
                $response = GlobalOperatorResponse::createErrorResponse('Unexpected error!');
            }

            $this->_processOrderAdapter($response, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        }
        else if ($this->operator == self::OPERATOR_OWN ) {

            array_push($arrayOfIds, ['id' => $this->courier_id, 'stat' => self::STAT_SUCCESS]);
            $this->track_id = NULL;

            $no = 1;
            if($courier->courierTypeInternal)
            {
                $no = $courier->courierTypeInternal->family_member_number;
            }
            else if($courier->courierTypeDomestic)
            {
                $no = $courier->courierTypeDomestic->family_member_number;
            }

            $resp = [];
            $temp['status'] = true;
            $temp['track_id'] = NULL;

            try
            {
                $temp['label'] = \KaabLabel::generate($courier, $no);
                array_push($resp, $temp);
            }
            catch(Exception $ex)
            {
                Yii::log(print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
                $resp = [0 => [
                    'error' => print_r('Unexpected error!',1),
                ]];
            }

            $this->_processOrder($resp, false, 'KAAB', 'saveOwnLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        }
        else if (UpsSoapShipClient::isCourierLabelNewOperatorUps($this->operator)) {


            try
            {
                $upsOperator = UpsSoapShipClient::mapCourierLabelNewOperatorUps($this->operator);

                if ($courier->courierTypeInternal) {

                    $response = UpsSoapShipClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true, $upsOperator);
                }
                else if ($courier->courierTypeU) {

                    $response = UpsSoapShipClient::orderForCourierU($courier->courierTypeU, $from, $to, true);

//            } else if ($courier->courierTypeInternalSimple) {
//                if(UpsSoapShipClient::isCourierLabelNewOperatorUpsSaver($this->operator))
//                    $upsOperator = UpsSoapShipClient::SERVICE_SAVER;
//                else
//                    $upsOperator = UpsSoapShipClient::SERVICE_STANDARD;
//
//                $response = UpsSoapShipClient::orderForCourierInternalSimple($courier->courierTypeInternalSimple, false, true, $upsOperator);

                } else if ($courier->courierTypeDomestic) {

                    $response = UpsSoapShipClient::orderForCourierDomestic($courier->courierTypeDomestic, true);

                } else {
                    $this->stat = self::STAT_FAILED;
                    $this->update(array('stat'));
                    exit;
                }
            }
            catch(Exception $ex)
            {
                Yii::log(print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
                $response = [0 => [
                    'error' => print_r('Unexpected error!',1),
                ]];
            }

            $this->_processOrder($response, true, 'UPS', false, Yii::createComponent('application.components.UpsCollectClient'), 'quequeCollectionForCourier', $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_OOE) {
            $type = $this->courier->getType();

            // OOE CAN BE FROM TYPE INTERNAL...
            if ($type == Courier::TYPE_INTERNAL) {

                $data = self::unserialize($this->data);

                $courierOperator = CourierOperator::model()->findByPk($data['courier_operator_id']);
                $packageData = OneWorldExpressPackage::createByCourierTypeInternal($courier->courierTypeInternal, $from, $to, $courierOperator->get_Ooe_service(), $courierOperator->get_Ooe_account(), $courierOperator->get_Ooe_login(), $courierOperator->get_Ooe_pass());
                $return = Yii::app()->OneWorldExpress->getLabel($packageData, true);

                $this->_processOrder([$return], false, 'OOE', 'saveOoeLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, self::SPECIAL_MODE_TYPE_INTERNAL_OOE);


                // ...OR OOE CAN BE FROM TYPE OOE
            } else if ($type == Courier::TYPE_INTERNAL_OOE) {

                $this->courier->courierTypeOoe->setOoeAccountData($this->courier->user_id);

                $differentReference = false;
                if ($this->courier->courierTypeOoe->_ebay_order_id != '')
                    $differentReference = $this->courier->courierTypeOoe->_ebay_order_id;

                $packageData = OneWorldExpressPackage::createByCourierTypeOoe($this->courier->courierTypeOoe, $differentReference);
                $return = Yii::app()->OneWorldExpress->getLabel($packageData, true);


                $this->_processOrder([$return], false, 'OOE', 'saveOoeLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, self::SPECIAL_MODE_TYPE_OOE);

            }
            else if ($type == Courier::TYPE_U) {

                $data = self::unserialize($this->data);

                $packageData = OneWorldExpressPackage::createByCourierTypeU($courier->courierTypeU);
                $return = Yii::app()->OneWorldExpress->getLabel($packageData, true);

                $this->_processOrder([$return], false, 'OOE', 'saveOoeLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, self::SPECIAL_MODE_TYPE_INTERNAL_OOE);


            }
        } else if ($this->operator == self::OPERATOR_INPOST) {
            $data = self::unserialize($this->data);

            try
            {

                if ($courier->courierTypeInternal) {
                    $response = Inpost::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true, $this->_internal_mode, $this->_deliveryHubId, $data['operator_uniq_id']);
                } else if ($courier->courierTypeDomestic) {
                    $response = Inpost::orderForCourierDomestic($courier->courierTypeDomestic, true);
                } else if ($courier->courierTypeU) {
                    $response = Inpost::orderForCourierU($courier->courierTypeU, true);
                } else {
                    $this->stat = self::STAT_FAILED;
                    $this->update(array('stat'));
                    exit;
                }
            }
            catch(Exception $ex)
            {
                Yii::log(print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
                $response = [0 => [
                    'error' => print_r('Unexpected error!',1),
                ]];
            }

            $this->_processOrder($response, true, 'Inpost', 'saveInpostLabel', 'Inpost', 'quequeCollectionForCourier', $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_RUCH) {


            if ($courier->courierTypeDomestic) {
                $response = PaczkaWRuchu::orderForCourierDomestic($courier->courierTypeDomestic, true);
            }
            else if ($courier->courierTypeInternal) {
                $response = PaczkaWRuchu::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true);
            }
            else if ($courier->courierTypeU) {
                $response = PaczkaWRuchu::orderForCourierU($courier->courierTypeU, true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'RUCH', 'saveRuchLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_PETER_SK) {

            if ($courier->courierTypeInternal) {
                $response = Joy::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true);
            }
            else if ($courier->courierTypeU) {
                $response = Joy::orderForCourierU($courier->courierTypeU,true);
            }
            else
            {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'KAAB_SK', 'saveJoyLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_POCZTA_POLSKA) {

            $data = self::unserialize($this->data);

            if ($courier->courierTypeInternal) {
                $response = PocztaPolskaClient::createByCourier($courier, $from, $to, $data['courier_operator_id'], $data['operator_uniq_id']);
            }
            else if ($courier->courierTypeU) {
                $response = PocztaPolskaClient::createByCourierU($courier->courierTypeU);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'Poczta Polska', 'savePocztaPolskaLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_GLS_NL) {

            $data = self::unserialize($this->data);

            try
            {

                if ($courier->courierTypeInternal) {
                    $response = GlsNlClient::createByCourierInternal($courier->courierTypeInternal, $from, $to, $data['courier_operator_id'], !$blockPickup && $this->triggers_pickup);
                }
                else if ($courier->courierTypeU) {
                    $response = GlsNlClient::createByCourierU($courier->courierTypeU);

                } else {
                    $this->stat = self::STAT_FAILED;
                    $this->update(array('stat'));
                    exit;
                }

            }
            catch(Exception $ex)
            {
                Yii::log(print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
                $response = [0 => [
                    'error' => print_r('Unexpected error!',1),
                ]];
            }

            $this->_processOrder($response, true, 'GLS NL', 'saveGlsNLLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, self::SPECIAL_MODE_GLS_NL);

        } else if ($this->operator == self::OPERATOR_DHL OR $this->operator == self::OPERATOR_DHL_F) {


            try
            {

                if ($courier->courierTypeInternal) {
                    $response = DhlClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true, $this->operator, !$blockPickup && $this->triggers_pickup);
                }
                else if ($courier->courierTypeU) {
                    $response = DhlClient::orderForCourierU($courier->courierTypeU, true);
                } else if ($courier->courierTypeDomestic) {
                    $response = DhlClient::orderForCourierDomestic($courier->courierTypeDomestic, true);
                } else {
                    $this->stat = self::STAT_FAILED;
                    $this->update(array('stat'));
                    exit;
                }

            }
            catch(Exception $ex)
            {
                Yii::log(print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
                $response = [0 => [
                    'error' => print_r('Unexpected error!',1),
                ]];
            }

            $this->_processOrder($response, true, 'DHL', false, 'CourierCollect', 'createReadyCollection', $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, self::SPECIAL_MODE_DHL);

        } else if ($this->operator == self::OPERATOR_DHLDE) {

            $data = self::unserialize($this->data);

            try
            {

                if ($courier->courierTypeInternal) {
                    $response = DhlDeClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, $data['operator_uniq_id'], true, !$blockPickup && $this->triggers_pickup);
                }
                else if ($courier->courierTypeU) {
                    $response = DhlDeClient::orderForCourierU($courier->courierTypeU, true);
                }
                else if ($courier->courierTypeReturn) {
                    $response = DhlDeClient::orderForCourierReturn($courier->courierTypeReturn, $to, true);
                } else {
                    $this->stat = self::STAT_FAILED;
                    $this->update(array('stat'));
                    exit;
                }

            }
            catch(Exception $ex)
            {
                Yii::log(print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
                $response = [0 => [
                    'error' => print_r('Unexpected error!',1),
                ]];
            }

            $this->_processOrder($response, true, 'DHL DE', 'saveLabelDefault', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);
        } else if ($this->operator == self::OPERATOR_DHLEXPRESS) {

            $data = self::unserialize($this->data);

            if ($courier->courierTypeInternal) {
                $response = DhlExpressClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, $data['operator_uniq_id'], true, !$blockPickup && $this->triggers_pickup);
            }
            else if ($courier->courierTypeU) {
                $response = DhlExpressClient::orderForCourierU($courier->courierTypeU, true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'DHL EXPRESS', 'saveDhlExpressLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);
        } else if ($this->operator == self::OPERATOR_UK_MAIL) {

            $data = self::unserialize($this->data);

            try
            {

                if ($courier->courierTypeInternal) {
                    $response = UKMailClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, $data['operator_uniq_id'], true, !$blockPickup && $this->triggers_pickup);
                }
                else if ($courier->courierTypeU) {
                    $response = UKMailClient::orderForCourierU($courier->courierTypeU, true);
                } else {
                    $this->stat = self::STAT_FAILED;
                    $this->update(array('stat'));
                    exit;
                }

            }
            catch(Exception $ex)
            {
                Yii::log(print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
                $response = [0 => [
                    'error' => print_r('Unexpected error!',1),
                ]];
            }

            $this->_processOrder($response, true, 'Uk Mail', 'saveUkMailLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);
        } else if ($this->operator == self::OPERATOR_DPD_DE) {

            $data = self::unserialize($this->data);

            try
            {

                if ($courier->courierTypeInternal) {
                    $response = DpdDeClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, $data['operator_uniq_id'], true);
                }
                else  if ($courier->courierTypeU) {
                    $response = DpdDeClient::orderForCourierU($courier->courierTypeU, true);
                } else {
                    $this->stat = self::STAT_FAILED;
                    $this->update(array('stat'));
                    exit;
                }

            }
            catch(Exception $ex)
            {
                Yii::log(print_r($ex->getMessage(),1), CLogger::LEVEL_ERROR);
                $response = [0 => [
                    'error' => print_r('Unexpected error!',1),
                ]];
            }

            $this->_processOrder($response, true, 'DPD DE', false, false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_POST11) {

            if ($courier->courierTypeInternal) {
                $response = Post11Client::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true);
            }
            else  if ($courier->courierTypeU) {
                $response = Post11Client::orderForCourierU($courier->courierTypeU, true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'POST11', false, false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_LP_EXPRESS) {

            if ($courier->courierTypeInternal) {
                $response = LpExpressClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true);
            }
            else  if ($courier->courierTypeU) {
                $response = LpExpressClient::orderForCourierU($courier->courierTypeU, true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'LP Express', false, false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_GLS) {

            if ($courier->courierTypeInternal) {
                $response = GlsClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true, $this->_internal_mode);
            } else  if ($courier->courierTypeU) {
                $response = GlsClient::orderForCourierU($courier->courierTypeU, true);
            } else if ($courier->courierTypeDomestic) {
                $response = GlsClient::orderForCourierDomestic($courier->courierTypeDomestic, true);
//            } else if ($courier->courierTypeInternalSimple) {
//                $response = GlsClient::orderForCourierInternalSimple($courier->courierTypeInternalSimple, true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'GLS', 'saveGlsLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, self::SPECIAL_MODE_GLS);
        } else if ($this->operator == self::OPERATOR_ROYALMAIL) {

            $data = self::unserialize($this->data);

            if ($courier->courierTypeInternal) {
                $response = RoyalMailClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, $data['operator_uniq_id'], true);
            }
            else if ($courier->courierTypeU) {
                $response = RoyalMailClient::orderForCourierU($courier->courierTypeU, true);
            }
            else if ($courier->courierTypeReturn) {
                $response = RoyalMailClient::orderForCourierReturn($courier->courierTypeReturn, $to, true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'RoyalMail', 'saveRoyalMailLabel', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);

        } else if ($this->operator == self::OPERATOR_DPDPL) {

            $data = self::unserialize($this->data);

            if ($courier->courierTypeInternal) {
                $response = DpdPlClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, $data['operator_uniq_id'],true, !$blockPickup);
            }
            else if ($courier->courierTypeU) {
                $response = DpdPlClient::orderForCourierU($courier->courierTypeU,true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'DpdPl', false, false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds, self::SPECIAL_MODE_DPDPL);
        } else if ($this->operator == self::OPERATOR_DPDNL) {

            $data = self::unserialize($this->data);

            if ($courier->courierTypeInternal) {
                $response = DpdNlClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, $data['operator_uniq_id'], true);
            }
            else if ($courier->courierTypeU) {
                $response = DpdNlClient::orderForCourierU($courier->courierTypeU,true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'DpdNl', false, false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);
        } else if ($this->operator == self::OPERATOR_HUNGARIAN_POST) {

//            $data = self::unserialize($this->data);

            if ($courier->courierTypeInternal) {
                $response = HuPostClient::orderForCourierInternal($courier->courierTypeInternal, $from, $to, true);
            }
            else if ($courier->courierTypeU) {
                $response = HuPostClient::orderForCourierU($courier->courierTypeU, true);
            } else {
                $this->stat = self::STAT_FAILED;
                $this->update(array('stat'));
                exit;
            }

            $this->_processOrder($response, true, 'HungaryPost', 'saveHungaryPost', false, false, $logSourcePrefix, $blockPickup, $courier, $arrayOfIds);
        } else {
            // OPERATOR NOT KNOWN
            $error = 'Problem - nieznany operator';

            array_push($arrayOfIds, ['id' => $this->courier_id, 'stat' => self::STAT_FAILED]);
            $this->stat = self::STAT_FAILED;
            $this->log = $error;
            $this->update('error', 'stat', 'date_updated');

            // DO NOT SET PROBELM STATUS FOR SECOND LABEL OF TWO IN INTERNAL PACKAGES
            if ($this->_internal_mode != self::INTERNAL_MODE_SECOND)
                $this->onProblem();

            $this->courier->addToLog($error, CourierLog::CAT_ERROR, $logSourcePrefix . 'ŚP');
        }

        // Yii::app()->courierLabelNewLog2->saveToLog($this, CourierLabelNewLog2::WHAT_ORDER_LABEL_END);
// CourierLabelNewLog::saveToLog($this, // CourierLabelNewLog::WHAT_ORDER_LABEL_END);

        try {
            $semaphore->release();
        } catch (Exception $e) {
            die('Could not release the semaphore');
        }
        MyDump::dump('cln.txt', $this->id.' : DONE!');

        if ($returnArrayOfIds)
            return $arrayOfIds;
        else
            return true;
    }

    /**
     * For multipackages, fill children with errors
     */
    protected static function updateChildrenWithError($parentId, $log, &$arrayOfIds = [], $cancelPackageOnFail = false, $logCat = NULL, $logSource = NULL, $internal_mode = false, $problemLabel = false)
    {
        /* @var $item CourierLabelNew */
        $children = self::model()->findAll(array('condition' => 'parent_id = :parent AND stat = :stat', 'params' => array(':parent' => $parentId, ':stat' => self::STAT_WAITING_FOR_PARENT)));
        foreach ($children AS $item) {
//            $item->stat = self::STAT_FAILED_WITH_PROBLEM_LABEL;
            $item->stat = self::STAT_FAILED;
            $item->log = $log;

//            $problemLabel = $label = ProblemLabel::generate($item->courier, strip_tags($log));
//            $item->saveProblemLabel($problemLabel);


//            $item->update('error', 'stat', 'date_updated', 'file_path', 'file_type');
            $item->update('error', 'stat', 'date_updated');


            // BEFORE 20.03.2017
            // DO NOT SET PROBELM STATUS FOR SECOND LABEL OF TWO IN INTERNAL PACKAGES
//            if ($internal_mode != self::INTERNAL_MODE_SECOND)
//                $item->courier->changeStat(CourierStat::PACKAGE_PROBLEMS);

            // @ 20.03.2017
            // Change stat depending on extended label generating setting
            if($item->courier->user->getAllowCourierInternalExtendedLabel())
            {
                // IF USER HAS EXTENDED LABEL GENERATION
                // DO NOT SET PROBLEM STATUS FOR FIRST LABEL OF TWO IN INTERNAL PACKAGES

                if ($internal_mode != self::INTERNAL_MODE_FIRST)
                    $item->onProblem();
            } else {
                // DO NOT SET PROBLEM STATUS FOR SECOND LABEL OF TWO IN INTERNAL PACKAGES
                if ($internal_mode != self::INTERNAL_MODE_SECOND)
                    $item->onProblem();
            }

            $item->courier->addToLog('Error occured of parent package!', CourierLog::CAT_ERROR);
            $item->courier->addToLog($log, $logCat, $logSource);

            if ($cancelPackageOnFail)
                $item->courier->changeStat(CourierStat::CANCELLED);

            array_push($arrayOfIds, ['id' => $item->courier_id, 'stat' => self::STAT_FAILED]);

            if($item->courier->user_id == User::USER_4VALUES)
                PrintServer::newCourierLabelNotify($item);
        }

    }

    public function onProblem()
    {
        $this->courier->changeStat(CourierStat::CANCELLED_PROBLEM);


        if($this->courier->getType() == Courier::TYPE_INTERNAL)
        {
            if($this->courier->courierTypeInternal && $this->courier->courierTypeInternal->order && $this->courier->courierTypeInternal->order->payment_type_id == PaymentType::PAYMENT_TYPE_ONLINE)
            {

                $to = [
                    'jankopes@swiatprzesylek.pl',
//                    'piotrkocon@swiatprzesylek.pl',
//                    'cs@swiatprzesylek.pl',
                ];

                $text = 'Item paid online cancelled by problem: '.'<br/>';
                $text .= '<strong>'.$this->courier->local_id.'</strong><br/><br/>';
                $text .= 'Amount:'.'<br/>';
                $text .= '<strong>'.$this->courier->courierTypeInternal->order->value_brutto.' '.$this->courier->courierTypeInternal->order->currency.'</strong>'.'<br/><br/>';
                $text .= 'User:'.'<br/>';
                $text .= '<strong>'.$this->courier->user->login.'</strong>'.'<br/>';
                $text .= 'Sender:'.'<br/>';
                $text .= $this->courier->senderAddressData->name.' '.$this->courier->senderAddressData->company.'</br>';
                $text .= $this->courier->senderAddressData->address_line_1.'</br>';
                $text .= $this->courier->senderAddressData->address_line_2 ? $this->courier->senderAddressData->address_line_2.'</br>' : '';
                $text .= $this->courier->senderAddressData->zip_code.'<br/>';
                $text .= $this->courier->senderAddressData->city.'<br/>';
                $text .= $this->courier->senderAddressData->country;

                S_Mailer::send($to, $to, 'Online paid item problem: '.$this->courier->local_id, $text, true);
            }


        }
    }

    public function deleteLabel()
    {
        if ($this->file_path) {
            if (is_file($this->file_path))
                @unlink($this->file_path);

            $this->file_path = NULL;
            $this->file_type = NULL;

            return $this->update(array('file_path', 'file_type'));
        }
    }

    /**
     * Update ebay order at ebay.com with package ID and update stat of CourierEbayImport item
     * @param bool|false $error
     */
    protected function updateEbayData($error = false)
    {
        // if ordering label failed, then do not call eBay and set item to Cancelled stat
        if ($error) {
            $ebayStat = EbayImport::STAT_CANCELLED;
            Courier::updateEbayData($this->courier,'','', $this->_ebay_order_id, true);

        } else {
            $operator = 'SwiatPrzesylek.pl'; // default, just in case

            // if user has NOT external ids option enabled, pass to eBay local_and SwiatPrzeysylek as operator
            if($this->courier->user_id != NULL && !$this->courier->user->getShowCourierInternalOperatorsData())
            {
                $trackId = $this->courier->local_id;
            }
            else
            {
                // otherwise, get second label track id and operator
                $trackId = $this->track_id;

                if ($this->operator == self::OPERATOR_OOE) {
                    $type = $this->courier->getType();

                    if ($type == Courier::TYPE_INTERNAL) {

                        $data = self::unserialize($this->data);
                        $courierOperator = CourierOperator::model()->findByPk($data['courier_operator_id']);
                        $operator = $courierOperator->name;
                    }
                    else if ($type == Courier::TYPE_U) {
                        $operator = $this->courier->courierTypeU->courierUOperator->name_customer;;

                    } else {
                        // courierTypeOoe
                        $courierOoeService = CourierOoeService::model()->findByAttributes(['service_name' => $this->courier->courierTypeOoe->service_code]);
                        $operator = $courierOoeService->title;
                    }
                } else {
                    $operators = self::getOperators();

                    if (isset($operators[$this->operator]))
                        $operator = $operators[$this->operator];
                }
            }

            try {
                if (Courier::updateEbayData($this->courier, $operator, $trackId, $this->_ebay_order_id, false))
                    $ebayStat = EbayImport::STAT_SUCCESS;
                else
                    $ebayStat = EbayImport::STAT_ERROR;
            }
            catch (Exception $ex)
            {
                $ebayStat = EbayImport::STAT_ERROR;
            }

        }


        $this->__temp_ebay_update_stat = $ebayStat;

    }


    public static function expireOldLabels()
    {
        $toArchive = self::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY) AND stat = :stat LIMIT 50', [
            ':days' => self::EXPIRATION_DAYS,
            ':stat' => self::STAT_SUCCESS
        ]);

        /* @var $item self */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {

                $item->expireLabel();

            }
    }

    public function expireLabel()
    {
        $labelPath = Yii::app()->basePath . '/../' . $this->file_path;

        $this->stat = self::STAT_EXPIRED;

        $this->update(['stat', 'date_updated']);

        if (!$this->courier->isLabelArchived()) {
            $this->courier->archive += Courier::ARCHIVED_LABEL_EXPIRED;
            $this->courier->update(['archive']);
        }

        if ($this->_additional_pages > 0) {
            for ($i = 1; $i <= $this->_additional_pages; $i++) {
                $path = explode('.', $this->file_path);
                $lastElementBeforeExtension = S_Useful::sizeof($path) - 2;

                if ($lastElementBeforeExtension >= 0) {
                    $path[$lastElementBeforeExtension] = $path[$lastElementBeforeExtension] . '_' . $i;
                    $path = implode('.', $path);

                    $additionalPAth = Yii::app()->basePath . '/../' . $path;
                    @unlink($additionalPAth);
                }
            }
        }

        @unlink($labelPath);
    }



    public static function updateExternalId($current_external_id, $operator_id, $new_external_id)
    {

        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $result = $cmd->update((new self)->tableName(),[
            'track_id' => $new_external_id
        ],
            'track_id = :current AND operator = :operator',
            [
                ':current' => $current_external_id,
                ':operator' => $operator_id
            ]
        );

        MyDump::dump('peterSkTtUpdate.txt','OLD: '.$current_external_id.' | NEW: '.$new_external_id.' | Operator: '.$operator_id.' | '.print_r($result,1));

    }


// LABEL SAVING METHODS:
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

    public function saveProblemLabel($label)
    {
        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        @file_put_contents($filePathAbsolute, $this->handleAnnotations($label));

        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }


    public function saveOwnLabel($label)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        @file_put_contents($filePathAbsolute, $this->handleAnnotations($label));

        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }

    public function saveGlsNlLabel($label)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        // temporary save image to read just first page of PDF
        try {
            @file_put_contents($basePath . $temp . $this->hash . '.pdf', $label);
            $im->readImage($basePath . $temp . $this->hash . '.pdf[0]');
        } catch (Exception $ex) {
            return false;
        }

        $im->setImageFormat('png');
        $im->trimImage(0);

        if ($im->getImageHeight() < $im->getImageWidth()) {
            $im->rotateImage(new ImagickPixel(), 90);
        }
//        $im->writeImage($filePathAbsolute);
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
//        $im->trimImage(0);
        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));


        @unlink($basePath . $temp . $this->hash . '.pdf');

        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }

    public function savePocztaPolskaLabel($label)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        // temporary save image to read just first page of PDF
        try {
            @file_put_contents($basePath . $temp . $this->hash . '.pdf', $label);
            $im->readImage($basePath . $temp . $this->hash . '.pdf[0]');
        } catch (Exception $ex) {
            return false;
        }

        $im->setImageFormat('png');
        $im->trimImage(0);

        if ($im->getImageHeight() < $im->getImageWidth()) {
            $im->rotateImage(new ImagickPixel(), 90);
        }
//        $im->writeImage($filePathAbsolute);
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

        @unlink($basePath . $temp . $this->hash . '.pdf');

        $this->file_path = $filePath;
        $this->file_type = 'image/png';
    }


    public function saveOoeLabel($labelPath)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $label = @file_get_contents($labelPath, NULL, stream_context_create($arrContextOptions));

        // 28.10.2016
        // If label is empty, wait and try again - sometimes OWE labels are not ready directly after receiving API response
        if($label == '')
        {

            for($retry = 0; $retry < 3; $retry++)
            {
                sleep(1 + 5 * $retry);
                $label = @file_get_contents($labelPath, NULL, stream_context_create($arrContextOptions));

                if($label != '')
                    break;
            }
        }


        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300,300);

        // temporary save image to read just first page of PDF
        try {
            @file_put_contents($basePath . $temp . $this->hash . '.pdf', $label);
            $im->readImage($basePath . $temp . $this->hash . '.pdf[0]');
        } catch (Exception $ex) {
//                    file_put_contents('owe_relabel_log.txt',print_r($this->id,1) .' : UNABLE TO SAVE | '.date('Y-m-d h:i:s').' LABEL SIZE: '. strlen($label) .' LABEL CONTENT: '.substr($label,0,100) ."\r\n", FILE_APPEND);
//                    file_put_contents('owe_relabel_log_'.$this->id.'.txt',print_r($ex,1));
            return false;
        }

        $im->setImageFormat('png8');

        if ($im->getImageHeight() < $im->getImageWidth()) {
            $im->rotateImage(new ImagickPixel(), 90);
        }
//                $im->writeImage($filePathAbsolute);
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        /* @07.06.2018 - special rule for user LSC_EUR */
        if($this->courier->user_id == 1928 && $this->courier->receiverAddressData->country_id == CountryList::COUNTRY_NL) {
            $draw = new ImagickDraw();
            $draw->setFontSize(35);
            $draw->setTextUnderColor(new ImagickPixel('white'));
            $im->annotateImage($draw, 85, 1600, 0, $this->courier->package_content);
        }

        $im->trimImage(0);

        $im->stripImage();
        @file_put_contents($filePathAbsolute, $im->getImageBlob());

        $additionalImages = 0;
        // NOW TRY TO SAVE ADDITIONAL PAGES - MAYBE THERE ARE SOME?
        // limit to 5 just to prevent loop
        while ($additionalImages < 5) {
            $additionalImages++;
            try {
                $im->readImage($basePath . $temp . $this->hash . '.pdf[' . $additionalImages . ']');

                $fileName = $this->hash . '_' . $additionalImages . '.png';
                $filePathAdditional = $dir . $fileName;
                $filePathAbsolute = $basePath . $filePathAdditional;

                $im->setImageFormat('png8');
                $im->trimImage(0);

                if ($im->getImageHeight() < $im->getImageWidth()) {
                    $im->rotateImage(new ImagickPixel(), 90);
                }
//                        $im->writeImage($filePathAbsolute);
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }
                $im->stripImage();
                @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));
            } catch (Exception $ex) {
                $additionalImages--; // last try was failure, so correct number o images
                break;
            }
        }
        $this->_additional_pages = $additionalImages;

        @unlink($basePath . $temp . $this->hash . '.pdf');

        $this->file_path = $filePath;
        $this->file_type = $mime_type;

    }


    public function saveJoyLabel($label)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300,300);

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }
//
//    public function saveUpsLabel($label)
//    {
//
//        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);
//
//        @file_put_contents($filePathAbsolute, $label);
//
//        $this->file_path = $filePath;
//        $this->file_type = $mime_type;
//    }

    /**
     * @param string $label PDF label
     * @param bool|false $ownLabel Whether it's label originating from GLS system or it's label generated by our system (in case of collect option on)
     * @return bool
     */
    public function saveGlsLabel($label, $ownLabel = false)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);


        $im = ImagickMine::newInstance();
        $im->setResolution(300,300);

        // temporary save image to read just first page of PDF
        try {
            @file_put_contents($basePath . $temp . $this->hash . '.pdf', $label);
            $im->readImage($basePath . $temp . $this->hash . '.pdf[0]');
        } catch (Exception $ex) {
            return false;
        }

        if(!$ownLabel)
            $im->rotateImage(new ImagickPixel(), 90);

        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $im->trimImage(0);
        $im->stripImage();
        file_put_contents($filePathAbsolute, $im->getImageBlob());

        $additionalImages = 0;
        // NOW TRY TO SAVE ADDITIONAL PAGES - MAYBE THERE ARE SOME?
        // limit to 5 just to prevent loop
        while ($additionalImages < 5) {
            $additionalImages++;
            try {
                $im->readImage($basePath . $temp . $this->hash . '.pdf[' . $additionalImages . ']');

                $fileName = $this->hash . '_' . $additionalImages . '.png';
                $filePathAdditional = $dir . $fileName;
                $filePathAbsolute = $basePath . $filePathAdditional;

                if(!$ownLabel)
                    $im->rotateImage(new ImagickPixel(), 90);

                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }
                $im->trimImage(0);
                $im->stripImage();
                file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

            } catch (Exception $ex) {
                $additionalImages--; // last try was failure, so correct number o images
                break;
            }
        }
        $this->_additional_pages = $additionalImages;

        @unlink($basePath . $temp . $this->hash . '.pdf');

        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }

    public function saveInpostLabel($label)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300,300);
        $im->readImageBlob($label);
        $im->setImageFormat('png8');
        $im->stripImage();

        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }

    public function saveDhlExpressLabel($labels)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        if(!is_array($labels))
            $labels = [];

        // start from -1 because first label is not additional
        $images = 0;
        foreach($labels AS $label)
        {
            if($images > 0)
                $fileName = $this->hash . '_' . $images . '.' . $extension;

            $filePathAdditional = $dir . $fileName;
            $filePathAbsolute = $basePath . $filePathAdditional;
            @file_put_contents($filePathAbsolute, $this->handleAnnotations($label));

            $images++;
        }

        if($images > 1)
            $this->_additional_pages = $images - 1;

        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }
//
//    public function saveDhlLabel($label)
//    {
//        // label for DHL is already png8
//
//        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);
//
//        file_put_contents($filePathAbsolute, $label);
//
//        $this->file_path = $filePath;
//        $this->file_type = $mime_type;
//    }

    public function saveRuchLabel($label)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($label);
//        $im->setImageFormat('png8');
        $im->trimImage(0);

        $combined = new Imagick();
        $combined->addImage($im);
        $combined->addImage(PaczkaWRuchu::getLogoToMerge());
        $combined->resetIterator();
        $combined = $combined->appendImages(true);

        $combined->setResolution(300, 300);
        $combined->setImageFormat('png8');

        $combined->rotateImage(new ImagickPixel(), 90);

        if($combined->getImageAlphaChannel())
        {
            $combined->setImageBackgroundColor('white');
            $combined->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $combined->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($combined->getImageBlob()));
//        $combined->writeImage($filePathAbsolute);

        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }

    public function saveUkMailLabel($label)
    {
        // label  is already png


        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($label);

        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel())
        {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }


        $im->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
        $im->rotateImage(new ImagickPixel(), 90);

//        $im->flattenImages();
        $im->trimImage(0);
        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }

//    public function saveDhlDeLabel($label)
//    {
//        $multiplication = 2;
//        $baseDpi = 300;
//
//        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);
//
//        $im = ImagickMine::newInstance();
//        $im->setResolution($baseDpi * $multiplication, $baseDpi * $multiplication);
//        $im->readImageBlob($label);
//
//        $im->cropImage(1195* $multiplication, 2305* $multiplication, 280* $multiplication, 75* $multiplication);
//
//        $draw = new ImagickDraw();
//        $draw->setFontSize(35 * $multiplication);
//        $draw->setTextUnderColor(new ImagickPixel('white'));
//
//
//        $text = $this->courier->package_content;
//
//        /* @11.05.2018 - special rule for customer ANWIS*/
//        if($this->courier->user_id == 1720)
//            $text = $this->courier->ref;
//
////        if($this->courier->user_id == 8) {
////            $text = $this->courier->senderAddressData->getUsefulName(true) . ', ' . trim($this->courier->senderAddressData->address_line_1 . ' ' . $this->courier->senderAddressData->address_line_2) . ', ' . $this->courier->senderAddressData->zip_code . ' ' . $this->courier->senderAddressData->city . ', ' . $this->courier->senderAddressData->country0->code;
////            $draw->setFontSize(20 * $multiplication);
////        }
//
//        $im->resizeImage($im->getImageWidth()+500,$im->getImageHeight(),Imagick::FILTER_CATROM, 1);
//        $im->annotateImage($draw, 890* $multiplication, 168* $multiplication, 0,'ref: '.$text);
//
//        $im->setImageFormat('png8');
//        if ($im->getImageAlphaChannel())
//        {
//            $im->setImageBackgroundColor('white');
//            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//        }
//
//        $im->trimImage(0);
//        $im->stripImage();
//
//        @file_put_contents($filePathAbsolute, $im->getImageBlob());
//
//        $this->file_path = $filePath;
//        $this->file_type = $mime_type;
//    }


//    public function saveDpdDeLabel($label)
//    {
//        // label  is already png
//        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);
//
//        @file_put_contents($filePathAbsolute, $label);
//
//        $this->file_path = $filePath;
//        $this->file_type = $mime_type;
//    }

    public function saveHungaryPost($label)
    {
        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($label);

        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel())
        {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }



    public function saveRoyalMailLabel($label)
    {
        // label is base64 encoded PDF
        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob(base64_decode($label));

        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel())
        {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $draw = new ImagickDraw();
        $draw->setFontSize(40 );
        $draw->setTextUnderColor(new ImagickPixel('white'));
        $im->annotateImage($draw, 35, 1750, 0,'KAAB/'.$this->courier->user->login);

        $im->stripImage();
        @file_put_contents($filePathAbsolute, $this->handleAnnotations($im->getImageBlob()));

        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }

    public function handleAnnotations($label)
    {
        if($this->courier->user->getAnnotationMode())
        {
            $text = [];

            if($this->courier->user->getAnnotationText())
                $text[] = $this->courier->user->getAnnotationText();

            if($this->courier->user->getAnnotationAttribute())
            {
                switch($this->courier->user->getAnnotationAttribute())
                {
                    case LabelAnnotor::ATTR_CONTENT:
                        if($this->courier->package_content != '')
                            $text[] = $this->courier->package_content;
                        break;
                    case LabelAnnotor::ATTR_REF:
                        if($this->courier->ref != '')
                            $text[] = $this->courier->ref;
                        break;
                    case LabelAnnotor::ATTR_NOTE1:
                        if($this->courier->note1 != '')
                            $text[] = $this->courier->note1;
                        break;
                    case LabelAnnotor::ATTR_NOTE2:
                        if($this->courier->note2 != '')
                            $text[] = $this->courier->note2;
                        break;
                }
            }

            if(count($text))
                return LabelAnnotor::annotateLabel($label, $this->courier->user->getAnnotationMode(), implode($text, ' ||| '));
        }

        return $label;
    }

    public function saveLabelDefault($label)
    {

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);

        // label is already png8
        if($this->_additional_pages && $this->_additional_pages > 0)
        {
            $images = 0;
            foreach($label AS $labelItem)
            {
                if($images > 0)
                    $fileName = $this->hash . '_' . $images . '.' . $extension;

                $filePathAdditional = $dir . $fileName;
                $filePathAbsolute = $basePath . $filePathAdditional;

                $labelItem = $this->handleAnnotations($labelItem);

                @file_put_contents($filePathAbsolute, $labelItem);

                $images++;
            }
        } else {
            $label = $this->handleAnnotations($label);

            @file_put_contents($filePathAbsolute, $label);
        }



        $this->file_path = $filePath;
        $this->file_type = $mime_type;
    }

    public function saveExternalReturnLabel($label)
    {

        $this->refresh(); // to get proper hash

        list($filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp) = self::getLabelsBasePath($this->hash);


        // label is already png8

        @file_put_contents($filePathAbsolute, $label);

        $this->file_path = $filePath;
        $this->file_type = $mime_type;

        $this->update(['file_path', 'file_type']);
    }

    protected static function getLabelsBasePath($hash)
    {
        $basePath = Yii::app()->basePath . '/../';
        $dir = 'uplabels/';

        $temp = $dir . 'temp/';

        $dir .= date('Ymd').'/';

        if (!file_exists($basePath.$dir)) {
            mkdir($basePath.$dir, 0777, false);
        }

        $extension = 'png';
        $mime_type = 'image/png';

        $fileName = $hash . '.' . $extension;

        $filePath = $dir . $fileName;
        $filePathAbsolute = $basePath . $filePath;

        return [$filePath, $filePathAbsolute, $mime_type, $dir, $extension, $fileName, $basePath, $temp];
    }
}