<?php

Yii::import('application.modules.courier.models._base.BaseCourierUCustomOperators4UserGroup');

class CourierUCustomOperators4UserGroup extends BaseCourierUCustomOperators4UserGroup
{
    public $_courier_country_group_id;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function getStatList()
    {
        $array = CourierUOperator2CountryGroup::getStatList();
        $array[NULL] = 'default';

        return $array;
    }

    public function getStatName()
    {
        return isset(self::getStatList()[$this->stat]) ? self::getStatList()[$this->stat] : '';
    }
}