<?php

class CzechPostSubmitController extends Controller
{


    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_ADMIN.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    function actionIndex($removeDuplicates = true)
    {
        Yii::app()->getModule('postal');


        if (isset($_POST['items_ids'])) {

            $mode = intval($_POST['mode']);
            $file_no = intval($_POST['file_no']);
            $force_ddu_mode = intval($_POST['force_ddu_mode']);

            if(!in_array($mode, [1,2]))
            {
                Yii::app()->user->setFlash('error', 'Select mode!');
                $this->refresh();
                exit;
            }


            $items_ids = $_POST['items_ids'];
            $items_ids = explode(PHP_EOL, $items_ids);

            foreach ($items_ids AS $key => $item) {

                $item = trim(preg_replace("/[^\w]+/", "", $item));

                if ($item == '')
                    unset($items_ids[$key]);
                else
                    $items_ids[$key] = "'" . $item . "'";
            }

            $models = [];

//            if ($mode != 1 && S_Useful::sizeof($items_ids) > 500)
//            {
//                Yii::app()->user->setFlash('error', 'Maximum 500 items at once for API');
//                $this->refresh();
//                exit;
//            }

            if (S_Useful::sizeof($items_ids)) {

                $items_ids = implode(',', $items_ids);

                $models = Postal::model()->with('postalLabel')->with('postalAdditionalExtId')->with('postalOperator')->findAll(['condition' => '(postalLabel.track_id IN (' . $items_ids . ') OR postalAdditionalExtId.external_id IN (' . $items_ids . '))', 'order' => 't.id DESC']);

                if(!S_Useful::sizeof($models))
                {
                    Yii::app()->user->setFlash('error', 'Not a single item found!');
                    $this->refresh();
                }

                $foundDomestic = false;
                $foundInt = false;

                foreach($models AS $item)
                {
                    if($item->receiverAddressData->country_id == CountryList::COUNTRY_CZ)
                        $foundDomestic = true;
                    else
                        $foundInt = true;
                }

                if($foundDomestic && $foundInt)
                    Yii::app()->user->setFlash('error', 'Please do NOT sent INT & DOMESTIC items together');


                $resp = false;
                $errors = false;


                Config::setData(Config::CZECH_POST_FILE_NO, ++$file_no);


                if($removeDuplicates)
                {
                    $alreadyFound = [];
                    foreach($models AS $key => $item)
                    {
                        if(in_array($item->local_id, $alreadyFound) OR in_array($item->postalLabel->track_id, $alreadyFound))
                            unset($models[$key]);

                        $alreadyFound[] = $item->local_id;
                        $alreadyFound[] = $item->postalLabel->track_id;
                    }
                }

                try {
                    $resp = CzechPostClient::submitData($models, $mode == 1 ? true : false, $file_no, $force_ddu_mode);
                } catch (Exception $ex) {
                    $errors = $ex->getMessage();
                }


                if($errors)
                    Yii::app()->user->setFlash('error', 'Found errors: '.print_r($errors,1));

                if($resp)
                    Yii::app()->user->setFlash('success', 'Operation succeded: '.print_r($resp,1));

                $this->refresh();
                Yii::app()->end();
            }


            if(!S_Useful::sizeof($models)) {
                Yii::app()->user->setFlash('error', 'Zero items found!');
                $this->refresh();
                exit;
            }

        }

        $this->render('index', [

        ]);
    }



}