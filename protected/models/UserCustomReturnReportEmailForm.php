<?php

class UserCustomReturnReportEmailForm extends CFormModel
{
    public $email_1;
    public $email_2;
    public $email_3;


    public function rules()
    {
        return array(
            array('email_1, email_2, email_3', 'email'),
        );
    }

    public function init()
    {
        $model = Yii::app()->user->getModel();

        $this->email_1 = $model->getReturnReportEmail();
        $this->email_2 = $model->getReturnReportEmail2();
        $this->email_3 = $model->getReturnReportEmail3();

        return parent::init();
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'email_1' => Yii::t('user', 'Adres email').' 1',
            'email_2' => Yii::t('user', 'Adres email').' 2',
            'email_3'=> Yii::t('user', 'Adres email').' 3',
        );
    }


    public function save()
    {
        if($this->validate())
        {
            $model = Yii::app()->user->getModel();

            $model->setReturnReportEmail($this->email_1);
            $model->setReturnReportEmail2($this->email_2);
            $model->setReturnReportEmail3($this->email_3);

            return true;
        }

        return false;
    }
}