<?php

Yii::import('application.models._base.BasePrice');

class Price extends BasePrice
{
    const PLN = 1;
    const EUR = 2;
    const GBP = 3;


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function generateByPost(array $post)
    {
        $model = new Price;

        $priceValues = [];

        /* @var $currency PriceCurrency */
        foreach(PriceCurrency::model()->findAll() AS $currency)
        {
            $priceValue = new PriceValue();
            $priceValue->currency_id = $currency->id;
            $priceValue->currency_name = $currency->name;
            $priceValue->value = $post[$currency->id]['value'];

            array_push($priceValues, $priceValue);
        }

        $model->priceValues = $priceValues;

        return $model;
    }

    public function validateWithChildren()
    {

        $errors = false;
        if(!$this->validate())
            $errors = true;


        foreach($this->priceValues AS $priceValue)
        {
            $priceValue->setScenario(PriceValue::SCENARIO_VALIDATE_PRICES);
            if(!$priceValue->validate())
                $errors = true;
        }

        return $errors ? false : true;
    }

    public function getErrorsWithChildren()
    {
        $errors = [];

        if(!$this->validate())
            $errors[] = $this->getErrors();

        foreach($this->priceValues AS $priceValue)
        {
            if(!$priceValue->validate())
                $errors[] = $priceValue->getErrors();
        }

        return $errors;
    }

    public function saveWithPrices($ownTransaction = false)
    {

        $errors = false;

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        if(!$this->save())
            $errors = true;

        foreach($this->priceValues AS $priceValue)
        {
            $priceValue->price_id = $this->id;
            if(!$priceValue->save())
                $errors = true;

        }

        if($ownTransaction)
        {
            if($errors)
                $transaction->rollback();
            else
                $transaction->commit();
        }

        if($errors)
            return false;
        else
            return $this->id;

    }

    public function getValue($currency_id)
    {
        if($this->priceValues === NULL)
            $this->priceValues = PriceValue::model()->findByAttributes(array('price_id' => $this->id));

        foreach($this->priceValues AS $key => $priceValue)
        {
            if($priceValue->currency_id == $currency_id)
                return $this->priceValues[$key];
        }

        $priceValue = new PriceValue();
        $priceValue->currency_id = $currency_id;
        $priceValue->value = 0;
        return $priceValue;


    }

    public function afterConstruct()
    {
        $priceValues = [];
        foreach(PriceCurrency::model()->findAll() AS $currency)
        {
            $priceValue = new PriceValue();
            $priceValue->currency_id = $currency->id;
            array_push($priceValues, $priceValue);
        }

        $this->priceValues = $priceValues;

        return parent::afterConstruct();
    }

    public function getPLN()
    {
        return $this->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => self::PLN)))[0]->value;
    }

    public function getEUR()
    {
        return $this->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => self::EUR)))[0]->value;
    }

    public function getGBP()
    {
        return $this->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => self::GBP)))[0]->value;
    }

    /**
     * Returns value for provided price_id and currency_id. Returns NULL if not found
     *
     * @param $price_id
     * @param $currency_id
     * @return float|null
     */
    public static function getValueInCurrency($price_id, $currency_id)
    {
        $value = PriceValue::model()->cache(60)->findByAttributes(['price_id' => $price_id, 'currency_id' => $currency_id]);
//        file_put_contents('dump.txt', print_r($value === NULL,1));

        if($value === NULL)
            return NULL;
        else
            return $value->value;
    }

    public function deleteValues($beginTransaction = false)
    {
        if($beginTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        try {
            foreach ($this->priceValues AS $item) {
                if (!$item->delete())
                    $errors = true;
            }
        }
        catch(Exception $ex)
        {
            $errors = true;
        }


        if ($beginTransaction) {
            if ($errors)
                $transaction->rollback();
            else
                $transaction->commit();
        }

        return !$errors;
    }
}