<?php

Yii::import('application.modules.internationalMail.models._base.BaseInternationalMailAddition');

class InternationalMailAddition extends BaseInternationalMailAddition
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(

            array('name, description, price, international_mail_id', 'required'),
            array('price', 'numerical'),
            array('name', 'length', 'max'=>45),
            array('international_mail_id', 'length', 'max'=>10),
            array('id, name, description, price, international_mail_id', 'safe', 'on'=>'search'),
        );
    }


    public function getClientTitle()
    {
        return $this->name;
    }

    public function getClientDesc()
    {
        return $this->description;
    }
}