<?php
// LAYOUT CONFIGURATION

$colWidth[] = 20;
$colWidth[] = 70;
$colWidth[] = 90;
$colWidth[] = 70;
$colWidth[] = 100;
$colWidth[] = 100;
$colWidth[] = 100;
$colWidth[] = 70;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 100;
$colWidth[] = 100;
$colWidth[] = 200;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;


$totalWidth = 0;
foreach($colWidth As $item)
    $totalWidth += $item;

$height = S_Useful::sizeof($models) * 31 + 60;
if($height > 850)
    $height = 850
?>


<?php $modelTemp = new Courier;?>
<?php $modelTemp2 = new AddressData();?>
<?php $modelTemp3 = new CourierTypeDomestic();?>

<?php AddressDataSuggester::registerAssets(); ?>
<?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>

<?php
// WE DO NOT NEED FORM HERE - ALL DATA IS IN CACHE
//$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
//    array(
//        'enableAjaxValidation' => false,
//    )
//);
?>

<div class="text-center">
    <a href="<?php echo Yii::app()->createUrl('/courier/courierDomestic/import');?>" class="btn btn-warning"><?php echo Yii::t('courier', 'Od nowa');?></a>
    <br/>
</div>
<hr/>
<div class="text-center">
    <a href="#bottom">[<?php echo Yii::t('courier', 'skocz na dół');?>]</a>
</div>
<br/>
<?php
$this->widget('FlashPrinter');
?>


<div style="position: relative;">
    <div class="overlay"><div class="loader"></div></div>
    <?php
    // for upper scroll:
    ?>
    <div style="width: 100%; position: relative; height: 20px; overflow-x: scroll;" class="dummy-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; height: 20px;">
        </div>
    </div>
    <?php
    // end for upper scroll
    ?>
    <div style="width: 100%; position: relative; height: 60px; overflow: hidden;" class="header-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; overflow: hidden;">
            <table class="list hTop" style="font-size: 10px; text-align: left !important; table-layout: fixed; height: 60px;">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <tr>
                    <td colspan="14"><?php echo Yii::t('courier','Packages');?></td>
                    <td colspan="8" ><?php echo Yii::t('courier','Sender');?></td>
                    <td colspan="8"><?php echo Yii::t('courier','Receiver');?></td>
                </tr>
                <tr>
                    <td>#</td>
                    <td><?php echo Yii::t('courier', 'Is valid');?></td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_weight')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_l')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_w')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_d')); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('packages_number'); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_value')); ?> [PLN]
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_content')); ?>
                    </td>
                    <td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('ref')); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('cod_value'); ?> [<?= CourierCodAvailability::CURR_PLN;?>]
                    </td>
                    <td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'bankAccount'); ?>
                    </td>
                    <td style="border-left: 2px dashed gray;">
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td><td style="border-left: 2px dashed gray;">
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td>
                </tr>
            </table>

        </div>
    </div>
    <div style="width: 100%; position: relative; height: <?= $height;?>px; overflow-x: scroll;" class="real-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute;">

            <table class="list list-special" style="font-size: 10px; text-align: left !important; table-layout: fixed;">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <?php

                foreach($models AS $key => $item):
                    $this->renderPartial('import/_item',
                        array(
                            'model' => $item,
                            'i' => $key,
                        ));

                endforeach;
                ?>
            </table>

        </div>
    </div>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
        array(
            'enableAjaxValidation' => false,
            'id' => 'import-form',
        )
    );
    ?>

    <div class="navigate" style="text-align: center;" id="bottom">
        <?php echo TbHtml::submitButton(Yii::t('courier', 'Save this part of data'), array('name' => 'save', 'class' => 'save-packages btn-primary')); ?>
    </div>

    <?php
    $this->endWidget();
    ?>
</div>

    <div class="modal fade " tabindex="-1" role="dialog" id="edit-modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="edit-modal-content">
            </div>
        </div>
    </div>


<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  removeRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierDomestic/ajaxRemoveRowOnImport')).',
                  editRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierDomestic/ajaxEditRowOnImport')).',
                  updateRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierDomestic/ajaxUpdateRowItemOnImport')).',
              },
               text: {
                  unknownError: '.CJSON::encode(Yii::t('site', 'Wystąpił nieznany błąd!')).',
                  beforeLeaving: '.CJSON::encode(Yii::t('site', 'Jesteś na pewno chcesz opuścić tę stronę? Wprowadzone dane mogą zostać utracone')).',
              },
              misc: {
                  hash: '.CJSON::encode($hash).',
              },
          };
      ',CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/importListModels.js', CClientScript::POS_HEAD);
?>