<?php

class CorrectionNoteController extends Controller
{

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
//                'expression'=>'0 >= Yii::app()->user->authority && in_array(Yii::app()->user->id, [2,5,16,3])',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new CorrectionNote();

        if(isset(Yii::app()->session['correctionNoteByUserInvoice']))
        {
            $data = Yii::app()->session['correctionNoteByUserInvoice'];
            $model->creatorAddressModel->attributes = $data['creator_attributes'];

//            $model->document = $data['document'];

            $model->saveToUserInvoice = $data['user_id'];

            $value = round(40 * (double) Yii::app()->NBPCurrency->getLastMonthEurExcForDate($data['payment_date']),2);
            $model->amount = $value;

            $model->textAfter = 'z tytułu rekompensaty za koszty odzyskiwania należności za fakturę '.$data['document'].' zgodnie z art. 10 w zw. z art. 7 ustawy o terminach zapłaty w transakcjach handlowych z dnia 8 marca 2013 r. (Dz.U. 2013 poz. 403). Kwota 40 EUR przeliczona na złote według średniego kursu Euro ogłoszonego przez Narodowy Bank Polski ostatniego dnia roboczego miesiąca poprzedzającego miesiąc, w którym świadczenie pieniężne stało się wymagalne.';

            unset(Yii::app()->session['correctionNoteByUserInvoice']);
        }

        if(isset($_POST['CorrectionNote']))
        {
            $errors = false;
            $model->attributes = $_POST['CorrectionNote'];

            $model->file_sheet = CUploadedFile::getInstance($model, 'file_sheet');

            $model->creatorAddressModel->attributes = $_POST['AddressData_Order']['creator'];
            $model->receiverAddressModel->attributes = $_POST['AddressData_Order']['receiver'];

            if(!$model->creatorAddressModel->validate())
                $errors = true;

            if(!$model->receiverAddressModel->validate())
                $errors = true;

            if(!$model->validate())
                $errors = true;

            if(!$errors) {
                $resp = $model->create();

                if ($resp) {
                    $text = 'Document created and saved: <a href="' . Yii::app()->createUrl('/userInvoice/view', ['id' => $resp->id]) . '" target="_blank" class="btn">view invoice (#' . $resp->id . ')</a>';
                    Yii::app()->user->setFlash('success', $text);
                    $this->refresh();
                }

            }
        }

        $this->render('create',[
            'model' => $model,
        ]);
    }

    public function actionCreateByUserInvoice($id)
    {

        /* @var $userInvoice UserInvoice */
        $userInvoice = UserInvoice::model()->findByPk($id);
        if($userInvoice === NULL)
            throw new CHttpException(404);


        $data = [];

        $data['document'] = $userInvoice->title;
        $data['payment_date'] = $userInvoice->inv_payment_date;

        $data['user_id'] = $userInvoice->user_id;

        $data['creator_attributes'] = $userInvoice->user->getAddressDataOrderModel()->attributes;
        $data['creator_attributes']['_nip'] = $userInvoice->user->getAddressDataOrderModel()->_nip;

        Yii::app()->session['correctionNoteByUserInvoice'] = $data;

        $this->redirect(['/correctionNote/create']);
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(['create']);
    }


}

