<h2 id="list"><?= Yii::t('courier', 'Eksport przesyłek');?></h2>
<br/>
<br/>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file-domestic',
    'action' => Yii::app()->createUrl('/courier/courier/exportToFileAll', ['mode' => $type]),
    'htmlOptions' => [
        'class' => 'do-not-block',
    ]
));
?>
<?php
echo TbHtml::submitButton(Yii::t('courier','Eksportuj wszyskie przesyłki do .xls').' :', ['style' => 'margin-top: -6px; margin-right: 5px;']);
echo Chtml::dropDownList('period', '', S_Useful::listPeriods(), ['style' => 'height: 30px;']);
$this->endWidget();
?>
