<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-stat-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('maxlength' => 256, 'style' => "width: 400px;", 'readonly' => (!$model->isNewRecord && ( $model->external_service_id!=NULL OR !$model->editable)?true:false))); ?>
        <?php echo $form->error($model,'name'); ?>
    </div><!-- row -->

    <?php
    foreach($modelTrs AS $item):
        echo $form->errorSummary($item);
    endforeach;
    ?>



    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):
            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';
        endforeach;
        ?>
    </div>


    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

            $model_tr = $modelTrs[$languageItem->id];

            $this->renderPartial('_tr_form', array(
                'model' => $model_tr,
                'form' => $form,
                'i' => $model_tr->language_id,
            ));

            echo'</div>';

        endforeach;
        ?>
    </div>

    <?php

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');

    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->

