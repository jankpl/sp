<?php

Yii::import('backend.extensions.selgridview.BootSelGridView');

class BackendGridView extends BootSelGridView
{
    public $addingHeaders = array();

    public function renderTableHeader() {
        if (!empty($this->addingHeaders))
            $this->multiRowHeader();

        parent::renderTableHeader();
    }

    protected function multiRowHeader() {
        echo CHtml::openTag('thead') . "\n";
        foreach ($this->addingHeaders as $row) {
            $this->addHeaderRow($row);
        }
        echo CHtml::closeTag('thead') . "\n";
    }

    // each cell value expects array(array($text,$colspan,$options), array(...))
    protected function addHeaderRow($row) {
        // add a single header row
        echo CHtml::openTag('tr') . "\n";
        // inherits header options from first column
        $fcol = 0;
        while (!isset($this->columns[$fcol])) {
            $fcol++;
        }
        $options = $this->columns[$fcol]->headerHtmlOptions;

        foreach ($row as $header) {
            $options['colspan'] = $header['colspan'];
            $cellOptions=($header['options'] + $options);
            echo CHtml::openTag('th', $cellOptions);
            echo $header['text'];
            echo CHtml::closeTag('th');
        }
        echo CHtml::closeTag('tr') . "\n";
    }
}