<?php

class _UserInvoiceGridViewUser extends UserInvoice
{
    public $_isPaid;

    public function rules() {

        $array = array(

            array('
            _isPaid,
            ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    public function attributeLabels() {

        $array = array(
            '_isPaid' => Yii::t('invoice', 'Opłacona'),
        );

        $array = array_merge(parent::attributeLabels(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', Yii::app()->user->id);

        if($this->_isPaid === '1')
        {
            $criteria->addCondition('paid_value >= inv_value');
        }
        else if($this->_isPaid === '0')
        {
            $criteria->addCondition('paid_value < inv_value');
        }

        $criteria->addCondition('stat > 0');

        $criteria->compare('inv_value', $this->inv_value, true);
        $criteria->compare('inv_currency', $this->inv_currency);
        $criteria->compare('inv_date', $this->inv_date, true);
        $criteria->compare('inv_payment_date', $this->inv_payment_date, true);

        $criteria->compare('stat_inv', $this->stat_inv);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('note', $this->note, true);


        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 'inv_date DESC';
//        $sort->attributes = array(
//            '__sender_country_id' => array(
//                'asc' => 'senderAddressData.country ASC',
//                'desc' => 'senderAddressData.country DESC',
//            ),
//            '__receiver_country_id' => array(
//                'asc' => 'receiverAddressData.country ASC',
//                'desc' => 'receiverAddressData.country DESC',
//            ),
//            '__stat' => array(
//
//
//                'asc' => 'courierStatTr.short_text ASC',
//                'desc' => 'courierStatTr.short_text DESC',
//            ),
//            '*', // add all of the other columns as sortable
//        );
        $criteria = $this->searchCriteria();

        // if there are no conditions (except for User ID), use simple count of packages to improve performance
        $totalCount = NULL;
//        if ($criteria->condition == 'user_id=:ycp0') {
//            $cmd = Yii::app()->db->createCommand();
//            $cmd->select('COUNT(id)')->from((new self)->tableName())->where('user_id = :user_id', [':user_id' => Yii::app()->user->id]);
//            $totalCount = $cmd->queryScalar();
//        }


        return new CActiveDataProvider($this

            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize'=> Yii::app()->user->getState('pageSize', 15),
                ),
            ));

    }


}