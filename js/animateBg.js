function animateBg(images, $bg1, $bg2)
{
    $bg2.hide();
    $bg1.hide();

    loopBg(0,0);

    function loopBg(i,delay)
    {
        var src = images[i];

        var $preloader = $('<img>');
        $preloader.on('load', function() {

            setTimeout(function(){

                if($bg2.is(':visible'))
                {
                    $bg1.css('background','url(' + src + ') no-repeat center center fixed');
                    $bg1.css('background-size','cover');
                    $bg2.fadeOut();
                } else if($bg1.is(':hidden')) {
                    // first load
                    $bg1.css('background', 'url(' + src + ') no-repeat center center fixed');
                    $bg1.css('background-size','cover');
                    $bg1.fadeIn();
                } else {
                    $bg2.css('background', 'url(' + src + ') no-repeat center center fixed');
                    $bg2.css('background-size','cover');
                    $bg2.fadeIn();
                }
                i = ++i % images.length;
                loopBg(i, 6000);
            }, delay);
        }).attr('src', src);
    }
}