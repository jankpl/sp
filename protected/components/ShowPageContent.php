<?php
class ShowPageContent extends CWidget
{
    public $id;

    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'page_id=:page_id AND language_id=:language_id AND stat = 1';
        $criteria->params = array(
            ':page_id'=>$this->id,
            ':language_id'=>Yii::app()->params['language'],
        );
        $model = PageTr::model()->find($criteria);

        if($model === NULL)
            return;

        $this->render('showPageContent', array(
            'model' => $model,
        ));


    }
}
?>