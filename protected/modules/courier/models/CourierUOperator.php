<?php

Yii::import('application.modules.courier.models._base.BaseCourierUOperator');

class CourierUOperator extends BaseCourierUOperator
{
    const STAT_ACTIVE = 1;
    const STAT_INACTIVE = 0;

    const IMG_HEIGHT = 150;
    const IMG_WIDTH = 150;

    const BROKER_UPS = 1;
    const BROKER_DHL = 2;
    const BROKER_PP = 3;
    const BROKER_OOE = 4;
    const BROKER_GLSNL = 5;
    const BROKER_DHLDE = 6;
    const BROKER_DHLEXPRESS = 7;
    const BROKER_UKMAIL = 8;
    const BROKER_ROYALMAIL = 9;
    const BROKER_GLS = 10;
    const BROKER_PETER_SK = 11;
    const BROKER_DPDDE = 12;
    const BROKER_DPDPL = 13;
    const BROKER_HUNGARIAN_POST = 14;
    const BROKER_INPOST = 15;
    const BROKER_DPDNL = 16;
    const BROKER_POST11 = 17;
    const BROKER_LP_EXPRESS = 18;
    const BROKER_RUCH = 19;

    const UNIQID_UPS = 100;
    const UNIQID_UPS_SAVER = 101;
    const UNIQID_UPS_BIS = 102;
    const UNIQID_UPS_BIS_SAVER = 103;
    const UNIQID_UPS_UK = 104;
    const UNIQID_UPS_F = 105;
    const UNIQID_UPS_F_SAVER = 106;
    const UNIQID_UPS_M = 107;
    const UNIQID_UPS_M_SAVER = 108;
    const UNIQID_UPS_L = 109;
    const UNIQID_UPS_MCM_SAVER = 110;
    const UNIQID_UPS_L_SAVER = 111;

    const UNIQID_DHL = 200;
    const UNIQID_DHL_F = 201;
    const UNIQID_DHL_F_PALETY = 202;
    const UNIQID_DHL_F_DLUZYCE = 203;

    const UNIQID_PP_WROCLAW_POLECONA_STD = 300;
    const UNIQID_PP_WROCLAW_POLECONA_PRIO = 301;

    const UNIQID_PP_NYSA_POLECONA_STD = 302;
    const UNIQID_PP_NYSA_POLECONA_PRIO = 303;

    const UNIQID_PP_WARSZAWA_POLECONA_STD = 304;
    const UNIQID_PP_WARSZAWA_POLECONA_PRIO = 305;

    const UNIQID_PP_BOCHNIA_POLECONA_STD = 306;
    const UNIQID_PP_BOCHNIA_POLECONA_PRIO = 307;

    const UNIQID_PP_FIREBUSINESS_POLECONA_STD = 308;
    const UNIQID_PP_FIREBUSINESS_POLECONA_PRIO = 309;

    const UNIQID_PP_WROCLAW_PACZKA = 310;

    const UNIQID_GLSNL = 500;
    const UNIQID_GLSNL_EURO = 501;

    const UNIQID_DHLDE_PAKET = 600;
    const UNIQID_DHLDE_PAKET_INT = 601;
    const UNIQID_DHLDE_EUROPAKET = 602;
    const UNIQID_DHLDE_PAKET_AT = 603;
    const UNIQID_DHLDE_PAKET_CONNECT_AT = 604;
    const UNIQID_DHLDE_PAKET_INT_AT = 605;
    const UNIQID_DHLDE_PAKET_CARMAT = 606;
    const UNIQID_DHLDE_PAKET_INT_CARMAT = 607;
    const UNIQID_DHLDE_PAKET_AT_CARMAT = 608;
    const UNIQID_DHLDE_PAKET_GIMEDIA = 609;
    const UNIQID_DHLDE_PAKET_INT_GIMEDIA = 610;
    const UNIQID_DHLDE_PAKET_AT_GIMEDIA = 611;

    const UNIQID_DHLEXPRESS = 700;
    const UNIQID_DHLEXPRESS_FB = 701;
    const UNIQID_DHLEXPRESS_SP = 702;

    const UNIQID_UKMAIL = 800;
    const UNIQID_UKMAIL_3RD = 801;
    const UNIQID_UKMAIL_RETURN = 802;

    const UNIQID_ROYALMAIL_24 = 900;
    const UNIQID_ROYALMAIL_48 = 901;
    const UNIQID_ROYALMAIL_TRACKED_RETURN_48 = 902;

    const UNIQID_GLS = 1000;

    const UNIQID_DPDDE = 1100;

    const UNIQID_PETERSK = 1200;

    const UNIQID_DPDPL = 1300;
    const UNIQID_DPDPL_V = 1301;

    const UNIQID_HUNGARIAN_POST = 1400;

    const UNIQID_INPOST = 1500;
    const UNIQID_INPOST_FMS = 1501;

    const UNIQID_DPDNL = 1600;

    const UNIQID_POST11 = 1700;

    const UNIQID_LP_EXPRESS = 1800;

    const UNIQID_SWEDEN_POST = 1900;
    const UNIQID_DEUTCHE_POST = 2000;

    const DIM_WEIGHT_MODE_DEFAULT = 1;
    const DIM_WEIGHT_MODE_DHL = 2;

    private $_transaction;
    public $_disable_inner_transaction = false;

    public $__img_temp;

    public $_ooe_login;
    public $_ooe_account;
    public $_ooe_pass;
    public $_ooe_service;

    public $__countries;

    public $_bg_color;
    public $_font_color;


    public $_max_dim_l;
    public $_max_dim_w;
    public $_max_dim_d;

    public $_max_dim_nst_l;
    public $_max_dim_nst_w;
    public $_max_dim_nst_d;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    public function rules() {
        return array(
            array('partner_id', 'in', 'range' => array_keys(Partners::getParntersList())),

            array('name, broker_id, has_dim_weight, stat, name_customer', 'required'),
            array('uniq_id, broker_id, has_dim_weight, stat, dim_weight_factor', 'numerical', 'integerOnly'=>true),
            array('_max_dim_l, _max_dim_w, _max_dim_d, _max_dim_nst_l, _max_dim_nst_w, _max_dim_nst_d', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>32),
            array('label_symbol', 'length', 'max'=>10),
            array('name_customer', 'length', 'max'=>256),
            array('img', 'length', 'max'=>256),
            array('name', 'unique'),
            array('date_updated, uniq_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, date_updated, uniq_id, broker_id, has_dim_weight, stat, name_customer, img, label_symbol, max_dim, max_dim_nst, dim_weight_factor', 'safe', 'on'=>'search'),
        );
    }


    public static function getStatList()
    {
        $data = [];

        $data[self::STAT_INACTIVE] = 'Locked';
        $data[self::STAT_ACTIVE] = 'Active';


        return $data;
    }

    public function getStatName()
    {
        return isset(self::getStatList()[$this->stat]) ? self::getStatList()[$this->stat] : '';
    }

    public function saveWithTr($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        if(!$this->save())
            $errors = true;

        foreach($this->courierUOperatorTrs AS $item)
        {
            $item->courier_u_operator_id = $this->id;
            if(!$item->save()) {
                $errors = true;
            }
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

    /**
     * Returns array with ID's of operators for giver broker
     * @param $broker_id
     * @return array|CDbDataReader
     */
    public static function getOperatorsForBroker($broker_id)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        $cmd->select('id');
        $cmd->from((new self)->tableName());
        $cmd->where('broker_id = :broker', [':broker' => $broker_id]);
        return $cmd->queryColumn();
    }

    public static function getBrokerList($onlyAvailableToAdd = false)
    {
        $data = [];

        $data[self::BROKER_OOE] = 'OOE';

        if(!$onlyAvailableToAdd) {
            $data[self::BROKER_UPS] = 'UPS';
            $data[self::BROKER_DHL] = 'DHL';
            $data[self::BROKER_PP] = 'POCZTA POLSKA';
            $data[self::BROKER_GLSNL] = 'GLS NL';
            $data[self::BROKER_DHLDE] = 'DHL DE';
            $data[self::BROKER_DHLEXPRESS] = 'DHL EXPRESS';
            $data[self::BROKER_UKMAIL] = 'UK MAIL';
            $data[self::BROKER_ROYALMAIL] = 'ROYAL MAIL';
            $data[self::BROKER_GLS] = 'GLS';
            $data[self::BROKER_PETER_SK] = 'KAAB SK';
            $data[self::BROKER_DPDDE] = 'DPD DE';
            $data[self::BROKER_DPDPL] = 'DPD PL';
            $data[self::BROKER_DPDNL] = 'DPD NL';
            $data[self::BROKER_HUNGARIAN_POST] = 'HUNGARIAN POST';
            $data[self::BROKER_INPOST] = 'INPOST';
            $data[self::BROKER_POST11] = 'POST11';
            $data[self::BROKER_LP_EXPRESS] = 'LP EXPRESS';
            $data[self::BROKER_RUCH] = 'Ruch';

            $data = $data + GLOBAL_BROKERS::getBrokersList();

        }
        return $data;
    }

    public static function brokerToCourierLabelNewOperator($broker_id)
    {
        if(in_array($broker_id, array_keys(GLOBAL_BROKERS::getBrokersList()))) // for items from GLOBAL_BROKERS
            return $broker_id;
        else
            switch($broker_id)
            {
                case self::BROKER_OOE:
                    return CourierLabelNew::OPERATOR_OOE;
                    break;
                case self::BROKER_UPS:
                    return CourierLabelNew::OPERATOR_UPS;
                    break;
                case self::BROKER_DHL:
                    return CourierLabelNew::OPERATOR_DHL;
                    break;
                case self::BROKER_PP:
                    return CourierLabelNew::OPERATOR_POCZTA_POLSKA;
                    break;
                case self::BROKER_GLSNL:
                    return CourierLabelNew::OPERATOR_GLS_NL;
                    break;
                case self::BROKER_DHLDE:
                    return CourierLabelNew::OPERATOR_DHLDE;
                    break;
                case self::BROKER_DHLEXPRESS:
                    return CourierLabelNew::OPERATOR_DHLEXPRESS;
                    break;
                case self::BROKER_UKMAIL:
                    return CourierLabelNew::OPERATOR_UK_MAIL;
                    break;
                case self::BROKER_ROYALMAIL:
                    return CourierLabelNew::OPERATOR_ROYALMAIL;
                    break;
                case self::BROKER_GLS:
                    return CourierLabelNew::OPERATOR_GLS;
                    break;
                case self::BROKER_PETER_SK:
                    return CourierLabelNew::OPERATOR_PETER_SK;
                    break;
                case self::BROKER_DPDDE:
                    return CourierLabelNew::OPERATOR_DPD_DE;
                    break;
                case self::BROKER_HUNGARIAN_POST:
                    return CourierLabelNew::OPERATOR_HUNGARIAN_POST;
                    break;
                case self::BROKER_DPDPL:
                    return CourierLabelNew::OPERATOR_DPDPL;
                    break;
                case self::BROKER_DPDNL:
                    return CourierLabelNew::OPERATOR_DPDNL;
                    break;
                case self::BROKER_INPOST:
                    return CourierLabelNew::OPERATOR_INPOST;
                    break;
                case self::BROKER_POST11:
                    return CourierLabelNew::OPERATOR_POST11;
                    break;
                case self::BROKER_LP_EXPRESS:
                    return CourierLabelNew::OPERATOR_LP_EXPRESS;
                    break;
                case self::BROKER_RUCH:
                    return CourierLabelNew::OPERATOR_RUCH;
                    break;
            }
    }

    public static function getMultiBrokers()
    {
        $data = [
            self::BROKER_UPS,
            self::BROKER_DHL,
            self::BROKER_DHLDE,
            self::BROKER_GLS,
            self::BROKER_UKMAIL,
            self::BROKER_DPDDE,
            self::BROKER_DHLEXPRESS,
            self::BROKER_ROYALMAIL,
            self::BROKER_GLSNL,
            self::BROKER_HUNGARIAN_POST,
            self::BROKER_INPOST,
            self::BROKER_DPDPL,
            self::BROKER_DPDNL,
            self::BROKER_POST11,
        ];

        $data = $data + GLOBAL_BROKERS::getMultipackageBrokers();

        return $data;
    }

    public static function getCollectableBrokers()
    {
        $data = [
            self::BROKER_UPS,
            self::BROKER_DHL,
            self::BROKER_GLS,
            self::BROKER_UKMAIL,
            self::BROKER_DHLEXPRESS,
            self::BROKER_DPDPL,
            self::BROKER_LP_EXPRESS,
            self::BROKER_INPOST,
        ];

        $data = $data + GLOBAL_BROKERS::getCollectionBrokers();

        return $data;
    }

    /**
     * @return array Array with uniq_ids
     */
    public static function getCustomTypeOperators($returnIds = false)
    {
        $uniq_ids = [
            self::UNIQID_DHL_F_DLUZYCE,
            self::UNIQID_DHL_F_PALETY
        ];

        if($returnIds)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id')->from('courier_u_operator')->where(['in', 'uniq_id', $uniq_ids]);
            return $cmd->queryColumn();
        } else {
            return $uniq_ids;
        }


    }

    public static function getBrokerName($id)
    {
        return isset(self::getBrokerList()[$id]) ? self::getBrokerList()[$id] : '';
    }

    public static function getBrokerById($operator_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('broker_id');
        $cmd->from((new self)->tableName());
        $cmd->where('id = :id', [':id' => $operator_id]);
        return $cmd->queryScalar();
    }

    public static function getUniqIdById($operator_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('uniq_id');
        $cmd->from((new self)->tableName());
        $cmd->where('id = :id', [':id' => $operator_id]);
        return $cmd->queryScalar();
    }


    public function getEditable()
    {
//        if($this->id == self::OPERATOR_OWN OR UpsSoapShipClient::isCourierOperatorUps($this->id))
//            return false;
//        else
        return true;
    }

    public function getNameWithIdAndBroker()
    {
        $broker = self::getBrokerName($this->broker_id);
        $name = $this->id.') '.$this->name;
        if($broker != '')
            $name .= ' ['.self::getBrokerName($this->broker_id).']';

        return $name;
    }

    public static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    public static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    /**
     * Unserialize additional params (ex. Preffered pickup time)
     * @return bool
     */
    public function afterFind()
    {
        if ($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if (is_array($this->params))
            foreach ($this->params AS $key => $item) {
                try {
                    $this->$key = $item;
                } catch (Exception $ex) {
                }
            }

        if($this->max_dim)
            $this->max_dim = json_decode($this->max_dim);

        if($this->max_dim_nst)
            $this->max_dim_nst = json_decode($this->max_dim_nst);

        if($this->img)
        {
            $img = @file_get_contents(self::getImgPath().$this->img);
            if($img)
            {
                $this->__img_temp = 'data:image/x-png;base64,'.base64_encode($img);
            }
        }

        parent::afterFind();
    }

    protected static function getImgPath()
    {
        $path = Yii::app()->getBasePath().DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'upcommon'.DIRECTORY_SEPARATOR.'/courierUOperator'.DIRECTORY_SEPARATOR;

        return $path;
    }

    public function getImgUrl()
    {
        if($this->img)
            return Yii::app()->baseUrl.'/upcommon/courierUOperator/'.$this->img;
        else
            return false;
    }

    protected static function _savableParams()
    {
        return [
            '_ooe_account',
            '_ooe_login',
            '_ooe_pass',
            '_ooe_service',
            '_font_color',
            '_bg_color',
        ];
    }

    public function getFontColor()
    {
        if($this->_font_color == '')
            return '000000';
        else
            return $this->_font_color;
    }

    public function getBgColor()
    {
        if($this->_bg_color == '')
            return '9aacaf';
        else
            return $this->_bg_color;
    }

    public function afterValidate()
    {
        if($this->_max_dim_l OR $this->_max_dim_d OR $this->_max_dim_w)
        {
            if(!$this->_max_dim_l)
                $this->addError('_max_dim_l', 'Provide value!');

            if(!$this->_max_dim_d)
                $this->addError('_max_dim_d', 'Provide value!');

            if(!$this->_max_dim_w)
                $this->addError('_max_dim_w', 'Provide value!');
        }

        if($this->_max_dim_nst_l OR $this->_max_dim_nst_d OR $this->_max_dim_nst_w)
        {
            if(!$this->_max_dim_nst_l)
                $this->addError('_max_dim_nst_l', 'Provide value!');

            if(!$this->_max_dim_nst_d)
                $this->addError('_max_dim_nst_d', 'Provide value!');

            if(!$this->_max_dim_nst_w)
                $this->addError('_max_dim_nst_w', 'Provide value!');
        }

        parent::afterValidate(); // TODO: Change the autogenerated stub
    }

    public function beforeSave()
    {
        if(!$this->_disable_inner_transaction)
            $this->_transaction = Yii::app()->db->beginTransaction();

        $params = [];
        foreach(self::_savableParams() AS $item)
            $params[$item] = $this->$item;

        $this->params = $params;

        // serialize only if not all values are empty
        if (array_filter($this->params)) {
            $this->params = self::serialize($this->params);

            if(mb_strlen($this->params) > 500) {
                Yii::log('Params too long for Courier model! '.print_r($this->params,1), CLogger::LEVEL_ERROR);
                throw new Exception('Params too long for CourierUOperator model!');
            }
        }
        else
            $this->params = NULL;


        if($this->__img_temp)
        {
            $imagick = ImagickMine::newInstance();

            $img = explode(',', $this->__img_temp);
            $img = $img[1];
            $img = base64_decode($img);

            if($img)
            {
                $imagick->readImageBlob($img);
                if($imagick->valid())
                {
                    $fileName = md5(microtime(true));
                    $fullPath = self::getImgPath().$fileName.'.png';
                    @file_put_contents($fullPath, $img);

                    $this->img = $fileName.'.png';
                }
            }
        }

        if(is_array($this->max_dim)) {
            $temp = $this->max_dim;
            rsort($temp);
            $this->max_dim = $temp;
        }

        if(is_array($this->max_dim_nst)) {
            $temp = $this->max_dim_nst;
            rsort($temp);
            $this->max_dim_nst = $temp;
        }


        if($this->max_dim)
            $this->max_dim = json_encode($this->max_dim);

        if($this->max_dim_nst)
            $this->max_dim_nst = json_encode($this->max_dim_nst);

        return parent::beforeSave();
    }

    public function afterSave()
    {

        if(!is_array($this->__countries))
            $this->__countries = [];

        $cmd = Yii::app()->db->createCommand();
        $countriesCurrent = $cmd->select('courier_country_group_id')->from((new CourierUOperator2CountryGroup())->tableName())->where('courier_u_operator_id = :courier_operator', [':courier_operator' => $this->id])->queryColumn();

        $toAdd = array_diff($this->__countries, $countriesCurrent);
        $toRemove = array_diff($countriesCurrent, $this->__countries);

        if(S_Useful::sizeof($toRemove)) {
            array_walk($toRemove, function(&$value, &$key) {
                $value = intval($value);
            });
            $toRemove = implode(',', $toRemove);
            $cmd->delete((new CourierUOperator2CountryGroup())->tableName(), 'courier_country_group_id IN (' . $toRemove . ') AND courier_u_operator_id = :operator_id', [':operator_id' => $this->id]);
        }

        if(S_Useful::sizeof($toAdd)) {
            $insertArray = [];
            foreach ($toAdd AS $country_id)
                $insertArray[] = [
                    'courier_country_group_id' => $country_id,
                    'courier_u_operator_id' => $this->id,
                    'stat' => 1,
                ];

            $builder = Yii::app()->db->schema->commandBuilder;
            $command = $builder->createMultipleInsertCommand((new CourierUOperator2CountryGroup())->tableName(), $insertArray);
            $command->execute();
        }

        if(!$this->_disable_inner_transaction)
            $this->_transaction->commit();

        return parent::afterSave();
    }



    public function setAttributesAll(array $data)
    {
        foreach($data AS $key => $item)
        {
            try {
                $this->$key = $item;
            } catch (Exception $ex) {
            }
        }
    }

    /**
     * @return CourierCountryGroup[]
     */
    public function getAllCountiesGroup()
    {
        $models = CourierCountryGroup::model()->primary()->alphhabetical()->findAll();

        return $models;
    }

    /**
     * @return CourierCountryGroup[]
     */
    public function getAvailableCountiesGroup()
    {
        $models = CourierCountryGroup::model()->primary()->alphhabetical()->findAll('t.id NOT IN (SELECT courier_country_group_id FROM courier_u_operator_2_country_group WHERE courier_u_operator_id = :operator_id)', [':operator_id' => $this->id]);

        return $models;
    }

    /**
     * @return CourierCountryGroup[]
     */
    public function getActiveCountriesGroup()
    {
        if($this->isNewRecord)
            return [];

        $models = CourierCountryGroup::model()->primary()->alphhabetical()->findAll('t.id IN (SELECT courier_country_group_id FROM courier_u_operator_2_country_group WHERE courier_u_operator_id = :operator_id)', [':operator_id' => $this->id]);

        return $models;
    }

    public function isCollectable()
    {
        return CourierTypeU::isCollectableByBrokerId($this->broker_id);
    }

    public function getDimWeightMode()
    {
        if($this->has_dim_weight)
        {
            return self::courierBrokerId2DimWeightMode($this->broker_id);
        }

        return false;
    }


    public static function courierBrokerId2DimWeightMode($broker_id)
    {
        if($broker_id == self::BROKER_DHL)
            return self::DIM_WEIGHT_MODE_DHL;
        else
            return self::DIM_WEIGHT_MODE_DEFAULT;
    }

    /**
     * @param $country_id
     * @return Price
     */
    public function getCodPriceForCountryGroup($country_id)
    {
        $model  = CourierUOperator2CountryGroup::findForOperatorAndCountryId($this->id, $country_id);
        return $model->price;
    }

    public function getRemoteAreaPrice($zip_code, $country_id, $city)
    {
        if($this->broker_id == self::BROKER_DHL)
            return self::_getRemoteAreaPriceDhl($zip_code, $country_id, $city);
        else if($this->broker_id == self::BROKER_DHLEXPRESS)
            return self::_getRemoteAreaPriceDhl($zip_code, $country_id, $city);
        else
            return false;
    }

    protected function _getRemoteAreaPriceDhl($zip_code, $country_id, $city)
    {
        /* @var $result CourierDhlRemoteAreas|boolean */
        $result = CourierDhlRemoteAreas::findZip($zip_code, $country_id, $city);
        if($result)
        {
            $result = $result->getPrice(CourierTypeU::getCurrencyIdForCurrentUser());
        }

        return $result;
    }

    protected function _getRemoteAreaPriceDhlExpress($zip_code, $country_id, $city)
    {


        $courier = new Courier_CourierTypeU();
        $courier->senderAddressData = new CourierTypeU_AddressData;
        $courier->receiverAddressData = new CourierTypeU_AddressData;
        $courier->courierTypeU = new CourierTypeU;
        $courier->courierTypeU->courier = $courier;


        $courier->package_weight = 1;
        $courier->package_size_l = 10;
        $courier->package_size_d = 10;
        $courier->package_size_w = 10;

        $courier->senderAddressData->country_id = $country_id;
        $courier->senderAddressData->city = $city;
        $courier->senderAddressData->zip_code = $zip_code;

        $courier->receiverAddressData->country_id = $country_id;
        $courier->receiverAddressData->city = $city;
        $courier->receiverAddressData->zip_code = $zip_code;

        /* @var $result CourierDhlRemoteAreas|boolean */
        $result = DhlExpressClient::getRateRequestCourierTypeU($courier->courierTypeU, $courier->senderAddressData, $courier->receiverAddressData, self::UNIQID_DHLEXPRESS, true);
        if($result)
        {
            $result = $result->getPrice(CourierTypeU::getCurrencyIdForCurrentUser());
        }

        return $result;
    }

    public static function hasCodByIdAndCountry($operator_id, $country_id)
    {

        $receiver_country_group_id = CourierCountryGroup::getGroupForCountryId($country_id);

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('cod');
        $cmd->from('courier_u_operator_2_country_group');
        $cmd->where('courier_u_operator_id = :operator_id AND courier_country_group_id = :country_group_id', [
            ':operator_id' => $operator_id,
            ':country_group_id' => $receiver_country_group_id,
        ]);
        $result = $cmd->queryScalar();

        return $result;
    }

    /**
     * @param $country_id int If country is false, return result for all countries
     * @param $user_id
     * @return array
     */
    public static function getOperatorsForCountryAndUser($country_id, $user_id)
    {

        $user_group_id = User::getUserGroupId($user_id);

        if($country_id)
            $country_group = CourierCountryGroup::getGroupForCountryId($country_id);

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('cuo.id AS id, cuo.stat AS cuo_stat, name_customer, cuco4cg.stat AS cuco4cg_stat, cuo2cg.stat AS cuo2cg_stat');
        $cmd->from('courier_u_operator cuo');
        $cmd->leftJoin('courier_u_operator_2_country_group cuo2cg', 'cuo.id = cuo2cg.courier_u_operator_id');

        if($user_group_id)
            $cmd->leftJoin('courier_u_custom_operators_4_user_group cuco4cg', 'cuco4cg.courier_u_operator_2_country_group_id = cuo2cg.id AND cuco4cg.user_group_id = :ugi', [':ugi' => $user_group_id]);
        else
            $cmd->leftJoin('courier_u_custom_operators_4_user_group cuco4cg', 'cuco4cg.courier_u_operator_2_country_group_id = cuo2cg.id AND cuco4cg.user_group_id IS NULL');

        if($country_id)
            $cmd->where('cuo2cg.courier_country_group_id = :cgi', [':cgi' => $country_group]);

        $result = $cmd->queryAll();


        $operators = [];
        foreach($result AS $item)
        {
            if(
                ($item['cuo2cg_stat'] == 1 OR
                    ($item['cuo2cg_stat'] === NULL && $item['cuo4cg_stat'] == 1)
                ) AND $item['cuo_stat'] == 1
            )
                $operators[] = [
                    'id' => $item['id'],
                    'name' => $item['name_customer'],
                ];
        }

        return $operators;
    }
}