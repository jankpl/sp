<?php

Yii::import('application.models._base.BaseUserContactBook');

class UserContactBook extends BaseUserContactBook
{

    const TYPE_SENDER = 'sender';
    const TYPE_RECEIVER = 'receiver';

    protected static $_types_enum;

    public function getDateEntered()
    {
        return explode(' ', $this->date_entered)[0];
    }

    public static function getTypes_enum()
    {
        $array =  Array(self::TYPE_RECEIVER => Yii::t('m_userContactBook','Odbiorca'), self::TYPE_SENDER =>  Yii::t('m_userContactBook','Nadawca'));


        return $array;
    }

    public static function representingColumn() {
        return 'representingName';
    }

    public function getRepresentingName()
    {
        return trim($this->name.' '.$this->company);
    }

    public function getRepresentingNameForSearch()
    {
        return trim($this->name.' '.$this->company.' | '.$this->address_line_1.' | '.$this->city .' | '.$this->zip_code.' | '.$this->country);
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /* public function init()
    {
        if($this->addressData === null) $this->addressData = new AddressData();
        return parent::init();
    }*/

    public function delete()
    {
        AddressData::model()->findByPk($this->address_data_id)->delete();

        return parent::delete();
    }

    public function instantiate($attributes)
    {
        if($this->addressData === null) $this->addressData = new AddressData();
        return parent::instantiate($attributes);
    }

    public function rules() {
        return array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('user_id', 'default',
                'value'=>Yii::app()->user->isGuest?null:Yii::app()->user->id, 'on'=>'insert'),


            array('type', 'required'),

            //array('address_data_id', 'required', 'on' => 'insert'),

            array('address_data_id', 'numerical', 'integerOnly'=>true),
            array('type', 'length', 'max'=>8),
            array('user_id', 'length', 'max'=>10),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, address_data_id, type, user_id,
            __name, __company, __city, __country, __zip_code, __address_line_1, __tel, __email
            ', 'safe', 'on'=>'search'),
        );
    }

    // dla kompatybilności
    public function getCountry0()
    {
        return  $this->addressData->country0;
    }

    public function getCountry_id()
    {
        return  $this->addressData->country_id;
    }

    public function getName()
    {
        return  $this->addressData->name;
    }

    public function getCompany()
    {
        return  $this->addressData->company;
    }

    public function getCountry()
    {
        return  $this->addressData->country;
    }

    public function getZip_code()
    {
        return  $this->addressData->zip_code;
    }

    public function getCity()
    {
        return  $this->addressData->city;
    }

    public function getAddress_line_1()
    {
        return  $this->addressData->address_line_1;
    }

    public function getAddress_line_2()
    {
        return  $this->addressData->address_line_2;
    }

    public function getTel()
    {
        return  $this->addressData->tel;
    }

    public function getEmail()
    {
        return  $this->addressData->email;
    }

    public function setCountry0($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->country0 = $val;
    }

    public function setCountry_id($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->country_id = $val;
    }

    public function setName($val)
    {
        if($this->addressData === null) $this->addressData = new AddressData();
        $this->addressData->name = $val;
    }

    public function setCompany($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->company = $val;
    }

    public function setCountry($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->country = $val;
    }

    public function setZip_code($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->zip_code = $val;
    }

    public function setCity($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->city = $val;
    }

    public function setAddress_line_1($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->address_line_1 = $val;
    }

    public function setAddress_line_2($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->address_line_2 = $val;
    }

    public function setTel($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->tel = $val;
    }

    public function setEmail($val)
    {
        if($this->addressData === NULL) $this->addressData = new AddressData();
        $this->addressData->email = $val;
    }

    public function validateWithAddressData()
    {
        $errors = false;

        if(!$this->validate())
            $errors = true;

        if($this->addressData === null)
            $errors = true;
        else
            if(!$this->addressData->validate())
                $errors = true;

        if($errors)
            return false;
        else
            return true;

    }

    public function saveWithAddressData($ownTransaction = true, $paramsAlreadyEncoded = false)
    {
        $errors = false;

        if($this->addressData === null)
            return false;

        $transaction = null;
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();


        if($paramsAlreadyEncoded)
            $this->addressData->_paramsAlreadyEncoded = true;

        if($this->type == self::TYPE_RECEIVER)
            $this->addressData->setBankAccount('');

        if(!$this->addressData->save())
            $errors = true;


        if(!$errors)
        {
            $this->address_data_id = $this->addressData->id;

            if(!$this->save())
                $errors = true;
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();
            return false;
        }

        if($ownTransaction)
            $transaction->commit();
        return true;
    }
    //

    public function setAttributesWithPrefix($attributes, $prefix)
    {

        if(!is_array($attributes))
            return false;

        foreach($attributes AS $key => $value)
        {
            if(!preg_match('/^'.$prefix.'.*/', $key))
                continue;

            $key = str_replace($prefix,'', $key);

            if(!$this->hasAttribute($key))
                continue;

            $this->$key = $value;
        }

        return true;
    }

    /**
     * Function for compatybility - sometimes list contains just ids, and sometimes - models
     * @param $countries
     * @return array
     */
    public static function convertCountryListToIds($countries)
    {
        $countries_ids = [];
        if(is_array($countries))
        {
            foreach($countries AS $country)
            {
                if($country instanceof CountryList)
                    array_push($countries_ids, $country->id);
                else if(is_numeric($country))
                    array_push($countries_ids, $country);
            }
        }

        return $countries_ids;
    }

    public static function listContactBookItems($type, $countries = NULL, $searchFragment = false)
    {

        $CACHE_DEPENDENCY = new CDbCacheDependency('SELECT MAX(date_entered) FROM '.(new self)->tableName().' WHERE user_id = '.intval(Yii::app()->user->id));

        if($countries!== NULL)
        {
            if(is_array($countries) && S_Useful::sizeof($countries)) {

                $countries_ids = [];
                foreach($countries AS $country)
                {
                    if($country instanceof CountryList)
                        array_push($countries_ids, intval($country->id));
                    else if(is_numeric($country))
                        array_push($countries_ids, intval($country));
                }

                $criteria = new CDbCriteria();
                $criteria->addCondition('user_id=:user_id');
                $criteria->addCondition('type=:type');
                $criteria->addCondition('country_id IN ('.implode(',',$countries_ids).')');
//
                if($searchFragment) {
                    $criteria->addCondition('addressData.company LIKE :search OR addressData.name LIKE :search');
                    $criteria->params = array(':user_id' => Yii::app()->user->id, ':type' => $type, ':search' => '%'.$searchFragment.'%');
                } else {
                    $criteria->params = array(':user_id' => Yii::app()->user->id, ':type' => $type);
                }

                $criteria->order = 't.id DESC';

                $items = UserContactBook::model()->cache(60*60, $CACHE_DEPENDENCY)->with('addressData')->findAll($criteria);


                return $items;
            }
        }


        if($searchFragment) {
            $items = UserContactBook::model()->cache(60*60, $CACHE_DEPENDENCY)->with('addressData')->findAll(array('condition' =>
                'user_id=:user_id
            AND type=:type
            AND (addressData.company LIKE :search OR addressData.name LIKE :search)
            ',
                'params' =>

                    array(
                        ':user_id'=>Yii::app()->user->id,
                        ':type' => $type,
                        ':search' => '%'.$searchFragment.'%',

                    ),
                'order' => 'addressData.name ASC, addressData.company ASC, t.id DESC',

            ));

        } else {
            $items = UserContactBook::model()->cache(60*60, $CACHE_DEPENDENCY)->with('addressData')->findAll(array('condition' =>
                'user_id=:user_id
            AND type=:type
            ',
                'params' =>

                    array(
                        ':user_id'=>Yii::app()->user->id,
                        ':type' => $type,

                    ),
                'order' => 'addressData.name ASC, addressData.company ASC, t.id DESC',

            ));
        }



        return $items;

    }

//    public static function createAndSave($ownTransaction = false, $attributes, $saveAs = 'receiver', $validate = false)
//    {
//        if (Yii::app()->user->isGuest OR !Yii::app()->params['frontend'])
//            return false;
//
//        if ($ownTransaction)
//            $transaction = Yii::app()->db->beginTransaction();
//
//        $model = new UserContactBook();
//        $model->type = $saveAs;
//        $model->addressData = new AddressData_ContactBook();
//        $model->addressData->attributes =  $attributes;
//        if($validate)
//            $model->validateWithAddressData();
//
//
//        if($validate && $model->hasErrors())
//        {
//            if($ownTransaction)
//                $transaction->rollBack();
//            return $model->getErrors();
//        }
//        else
//        {
//            $model->saveWithAddressData(false);
//
//            if($ownTransaction)
//                $transaction->commit();
//        }
//
//        return true;
//    }

    public static function createAndSave($ownTransaction = false, $attributes, $saveAs = 'receiver', $validate = false, $omitDuplicates = true, $includeBankAccountData = false)
    {

           if (Yii::app()->user->isGuest OR !Yii::app()->params['frontend'])
            return false;

        $user_id = Yii::app()->user->id;
        $user_id = intval($user_id);


        if ($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        if ($omitDuplicates) {

            $CACHE_DEPENDENCY = new CDbCacheDependency('SELECT COUNT(*) FROM '.(new UserContactBook())->tableName().' WHERE user_id = "'.$user_id.'"');

            /* @var $cmd CDbCommand */
            $cmd = YII::app()->db->cache(60*60, $CACHE_DEPENDENCY)->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from((new UserContactBook())->tableName() . ' cb');
            $cmd->join((new AddressData())->tableName() . ' ad', 'cb.address_data_id = ad.id');
            $cmd->where('cb.type = :type AND cb.user_id = :user_id', ['type' => $saveAs, 'user_id' => $user_id]);

            foreach ($attributes AS $key => $value) {
                if (in_array($key, [
                    'date_entered',
                    'params',
                    'id',
                    'country' // is filled on beforeSave()
                ]))
                    continue;

                if ($value == '')
                    $cmd->andWhere('ad.' . $key . ' IS NULL');
                else
                    $cmd->andWhere('ad.' . $key . ' = :' . $key, [':' . $key => strtoupper($value)]);
            }
            $result = $cmd->queryScalar();

            // this contact is already saved
            if ($result)
                return false;
        }

        if(!$includeBankAccountData) {

            $attributes['params'] = AddressData::unserialize($attributes['params']);

            if(isset($attributes['params']) && is_array($attributes['params']))
                $attributes['params'][AddressData::PARAMS_BANK_ACCOUNT] = '';

            $attributes['params'] = AddressData::serialize($attributes['params']);
        }



        $model = new UserContactBook();
        $model->type = $saveAs;
        $model->addressData = new AddressData_ContactBook();
        $model->addressData->attributes =  $attributes;
        if($validate)
            $model->validateWithAddressData();


        if($validate && $model->hasErrors())
        {
            if($ownTransaction)
                $transaction->rollBack();
            return $model->getErrors();
        }
        else
        {
            $model->saveWithAddressData(false, true);

            if($ownTransaction)
                $transaction->commit();
        }

        return true;
    }


    public static function saveWholeGroup(array $items)
    {
        $errors = false;

        $packages = [];

        /* @var $item UserContactBook */
        foreach($items AS $key => $item)
        {

            $return = $item->createAndSave(false, $item->addressData->attributes, $item->type);

            if($return) {
                array_push($packages, $return);
            }
            else
                $errors = true;
        }


        if($errors)
        {
            return ['success' => false];
        } else {
            return ['success' => true];
        }

    }

}