var Preloader = (function() {
    "use strict";

    var that = {};
    var $overlay = $("<div>");
    var $loader = $("<div>");
    var initialized = false;

    that.isShow = function(){
        return $("#overlay").is(':visible');
    };

    that.show = function() {
        if(!initialized)
        {
            $overlay.addClass('preloader');
            $loader.addClass('preloader-animation');
            $overlay.append($loader);

            $('body').prepend($overlay);

            initialized = true;
        }



        $overlay.show();
    };

    that.hide = function() {
        $overlay.hide();
    };

    that.init = function($elem) {

        //$overlay.addClass('preloader');
        //$loader.addClass('preloader-animation');
        //$overlay.append($loader);
        //
        //$($elem).prepend($overlay);
    };

    return that;
}());