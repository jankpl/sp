<?php
/* @var $model Courier */
/* @var $currency integer */
?>

<div class="form">

    <h2><?php echo Yii::t('courier','Podsumowanie');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '2/2')); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Parametry przesyłki');?></h4>
            <table class="detail-view" style="width: 550px; margin: 0 auto;">
                <tr class="odd">
                    <th><?php echo Yii::t('courier', 'Przyjęta waga');?>  [<?php echo Courier::getWeightUnit();?>]</th>
                    <td><?php echo $model->courierTypeOoe->getFinalWeight(); ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('courier','Package dimensions');?> [<?php echo Courier::getDimensionUnit();?>]</th>
                    <td><?php echo $model->package_size_l.' x '.$model->package_size_d.' x '.$model->package_size_w; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('package_value');?></th>
                    <td><?php echo $model->package_value; ?> PLN</td>
                </tr>
                <tr class="even">
                    <th><?php echo $model->getAttributeLabel('package_content');?></th>
                    <td><?php echo $model->package_content; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->courierTypeOoe->getAttributeLabel('service_name');?></th>
                    <td><?php echo $model->courierTypeOoe->findServiceName(true); ?></td>
                </tr>
                <?php
                if(OneWorldExpressPackage::isPalletways($model->courierTypeOoe->service_code)):?>
                    <tr class="even">
                        <td><?php echo $model->courierTypeOoe->getAttributeLabel('_palletway_size');?></td>
                        <td><?php echo CourierTypeOoe::getPalletwayTypes()[$model->courierTypeOoe->_palletway_size]; ?></td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6">

            <h4><?php echo Yii::t('courier','Additions');?></h4>
            <table class="detail-view" style="width: 550px; margin: 0 auto;">
                <tr class="odd">
                    <th style="text-align: left;"><?php echo Yii::t('courier','Nazwa');?></th>
                </tr>
                <?php

                /* @var $item CourierAdditionList */
                if(!S_Useful::sizeof($model->courierTypeOoe->listAdditions(true)))
                    echo '<tr><td colspan="2">'.Yii::t('app','Brak').'</td></tr>';
                else
                    foreach($model->courierTypeOoe->listAdditions(true) AS $item): ?>
                        <tr class="even">
                            <td><?php echo $item->getClientTitle(); ?></td>
                        </tr>

                    <?php endforeach; ?>
            </table>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Nadawca');?></h4>

            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));

            ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Odbiorca');?> </h4>
            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));

            ?>
        </div>

    </div>

    <h3 class="text-center"><?php echo Yii::t('site', 'Regulamin');?></h3>

    <div class="row">
        <table class="detail-view">
            <tr class="odd">
                <td><?php echo $form->labelEx($model->courierTypeOoe,'regulations'); ?>
                    <?php echo $form->checkbox($model->courierTypeOoe, 'regulations'); ?>
                    <?php echo $form->error($model->courierTypeOoe,'regulations'); ?></td>
            </tr>
        </table>


    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step4', 'class' => 'btn btn-sm'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę'),array('name'=>'finish', 'class' => 'btn btn-primary'));
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p><?php echo Yii::t('courier','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>
    </div>
</div><!-- form -->