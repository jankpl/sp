<br/>
<?php
if(is_array($models) && S_Useful::sizeof($models)):
        echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('courier', 'Znaleziono paczek:').' '.S_Useful::sizeof($models), array('closeText' => false));
        ?>
        <table class="table table-bordered text-center">
            <tr>
                <?php
                $hash = [];
                foreach($models AS $item)
                    $hash[] = $item->hash;

                foreach (LabelPrinter::getLabelGenerateTypes() AS $key => $item):
                    ?>
                    <td><?php echo TbHtml::link($item, array('/courier/courier/CarriageTicket', 'urlData' => $hash, 'type' => $key), array('class' => 'btn')); ?></td>
                <?php
                endforeach;
                ?>
            </tr>
        </table>
        <?php
else:
    echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, Yii::t('courierLabelPrinter', 'Nie znaleziono paczki!'), array('closeText' => false));
endif;
?>

