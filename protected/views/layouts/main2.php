<?php
// FOR PRICE CALCULATORS
if(!$this->blockBootstrap)
{
    Yii::app()->bootstrap->register();
} else {
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/new/bootstrap.min.css');
}
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/system.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/form.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/style.css?v201901");
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/new/bootstrap.vertical-tabs.min.css');
//Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/bootstrap-responsive.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/bootstrap-customize-3.css");

Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/v2/style.css?v201901");

if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH)
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/theme/ruch/css/style.css");
else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI)
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/theme/pli/css/style.css");
else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS)
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/theme/quriers/css/style.css");
else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/theme/cql/css/style.css");

//Yii::app()->clientScript->registerScriptFile('https://use.fontawesome.com/f8722a5953.js');
//Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/font-awesome/font-awesome.min.css");
?>
<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML>
<html lang="<?= Yii::app()->PV->getLangIsoCode();?>">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo Yii::t('meta','description');?>" />
    <meta name="keywords" content="<?php echo Yii::t('meta','keywords');?>" />
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700&subset=latin-ext" rel="stylesheet">
    <link href="<?= Yii::app()->request->baseUrl;?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <?php
    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH):
        ?>
        <link rel="Shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/ruch/images/favicon.ico" />
    <?php
    elseif(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI):
        ?>
        <link rel="Shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/pli/images/favicon.ico" />
    <?php
    elseif(Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS):
        ?>
        <link rel="Shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/quriers/images/favicon.ico" />
    <?php
    else:
        ?>
        <link rel="Shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/layout/favicon.ico" />
    <?php
    endif;
    ?>

    <?php
    if(YII_LOCAL):
        ?>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
    <?php
    endif;
    ?>

    <?php

    if(Yii::app()->NPS->isNewPanelSet()) {
        $css = '.gridview-container
                {
                    overflow-x: scroll;
                }';
        Yii::app()->clientScript->registerCss('gv', $css);
    }
    ?>

</head>
<body>
<?php
if(Config::getData(Config::SITE_MESSAGE_ACTIVE)):
    ?>
    <div class="alert alert-info alert-dismissible"  style="margin-bottom: 0; z-index: 9999; position: relative;"><div class="container text-center"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_QURIERS, PageVersion::PAGEVERSION_RUCH]) ? Config::getData(Config::SITE_MESSAGE_CONTENT) :  Config::getData(Config::SITE_MESSAGE_CONTENT_OTHER);?>
        </div>
    </div>
<?php
endif;
?>
<?php
if(!Yii::app()->user->isGuest && Yii::app()->user->model->disableOrderingNewItems):
    ?>
    <div class="alert alert-danger" style="margin-bottom: 0; z-index: 9999; position: relative;"><div class="container text-center"><?= Yii::t('site', 'To konto ma zablokowaną możliwosć składania nowych zamówień.');?></div></div>
<?php
endif;
?>
<?php
if(!Yii::app()->user->isGuest && Yii::app()->user->model->getDebtBlockActive()):
    ?>
    <div class="alert alert-danger" style="margin-bottom: 0; z-index: 9999; position: relative;"><div class="container text-center"> <?= Config::getData(Config::DEBT_BLOCK_CONTENT.'['.Yii::app()->language.']');?>
        </div>
    </div>
<?php
endif;
?>
<div class="top-header" style="z-index: 50;">
    <div class="top-header-orange"></div>
    <div class="container" style="position: relative;">
        <div class="row">
            <div class="col-xs-12 col-md-3 top-logo"><a href="<?= Yii::app()->getBaseUrl(true);?>" title="Home">
                    <?php
                    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH)
                        $logo_src = Yii::app()->baseUrl.'/theme/ruch/images/logo.png';
                    else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS)
                        $logo_src = Yii::app()->baseUrl.'/theme/quriers/images/logo.png';
                    else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                        $logo_src = Yii::app()->baseUrl.'/theme/cql/images/logo.png';
                    else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_ORANGE_POST)
                        $logo_src = Yii::app()->baseUrl.'/theme/orange-post/images/logo.png';
                    else
                        switch(Yii::app()->language)
                        {
                            case 'pl':
                                $logo_src = Yii::app()->baseUrl.'/images/v2/logo.png';
                                break;
                            case 'en_gb':
                                $logo_src = Yii::app()->baseUrl.'/theme/pli/images/logo.png';
                                break;
                            default:
                                $logo_src = Yii::app()->baseUrl.'/images/layout/new/logo_en.png';
                                break;
                        }
                    ?>
                    <img src="<?= $logo_src;?>" id="logo"/>
                </a></div>
            <div class="col-xs-12 col-md-4 top-menu">

                <div id="lang-menu">
                    <?php
                    if(YII_LOCAL):
                        ?>

                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_DEFAULT ? '<span>pl</span>' : '<a href="./?lang=pl">pl</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_EN ? '<span>en</span>' : '<a href="./?lang=en">en</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_DE ? '<span>de</span>' : '<a href="./?lang=de">de</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_UA ? '<span>uk</span>' : '<a href="./?lang=ua">uk</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_RS ? '<span>int</span>' : '<a href="./?lang=en_rs">int</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI ? '<span>uk</span>' : '<a href="./?lang=en_uk">uk</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH ?'<span>ruch</span>' : '<a href="./?lang=pl_ruch">ruch</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS ?'<span>quriers</span>' : '<a href="./?lang=pl_quriers">quriers</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL ?'<span>cql</span>' : '<a href="./?lang=en_cql">cql</a>';?>
                        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_ORANGE_POST ?'<span>orange</span>' : '<a href="./?lang=en_orangep">orange</a>';?>

                    <?php
                    else:
                        if(in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS])):
                            ?>

                            <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_DEFAULT ? '<span>pl</span>':'<a href="http://www.swiatprzesylek.pl">pl</a>';?>
                            <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_RS ? '<span>en</span>':'<a href="http://www.royalshipments.com">en</a>';?>

                        <?php
                        endif;
                    endif;
                    ?>
                </div>


                <div id="fly-sender-form">
                    <ul>
                        <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/courier/courier/');?>"><?php echo Yii::t('site', 'Courier');?></a></li>
                        <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI):?><li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/hybridMail/hybridMail/');?>"><?php echo Yii::t('site', 'Hybrid Mail');?></a></li><?php endif;?>
                        <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI):?><li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/internationalMail/internationalMail/');?>"><?php echo Yii::t('site', 'International Mail');?></a></li><?php endif;?>
                        <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI):?><li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/specialTransport/specialTransport/');?>"><?php echo Yii::t('site', 'Special Transport');?></a></li><?php endif;?>
                        <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH && Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI):?><li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/palette/palette/');?>"><?php echo Yii::t('site', 'Palette');?></a></li><?php endif;?>
                        <?php
                        Yii::app()->getModule('postal');
                        if(Postal::isAvailableForUser()):
                            ?>
                            <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/postal/postal/');?>"><?php echo Yii::t('site', 'Postal');?></a></li>
                        <?php
                        endif;
                        ?>
                    </ul>
                </div>

                <script>
                    $('#show-sender-form').on('click', function(event){
                        $('#fly-sender-form').slideDown();
                        $('#fly-account-form').hide();
                        event.stopPropagation();
                    });

                    $('body').on('click', function(){
                        $('#fly-sender-form').hide();
                    });

                    $('#fly-sender-form').click(function(event){
                        console.log($(this));
                        event.stopPropagation();
                    });
                </script>

            </div>


            <div class="col-xs-12 col-md-2" style="background-color: #4F656C; color: white; line-height: 80px; text-align: center;">
                <a href="<?= Yii::app()->createUrl('/panel');?>" style="color: white; font-size: 1.3em;"><?= Yii::t('site', 'Panel nadawcy');?></a>
            </div>
            <?php
            if(Yii::app()->user->isGuest):
                ?>
                <div class="col-xs-12 col-md-3 top-header-login">
                    <i class="fa fa-user" aria-hidden="true"></i> <a href="#" id="show-account-form"><?= Yii::t('home', 'Zaloguj się');?></a><?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI):?> | <?= CHtml::link(Yii::t('site', 'Zarejestruj'), ['/user/create']);?><?php endif; ?>

                    <div id="fly-account-form">
                        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id'=>'login-form-home',
                            'enableClientValidation'=>false,
                            'clientOptions'=>array(
                                'validateOnSubmit'=>false,
                            ),
                            'action'=>$this->createAbsoluteUrl('/site/login', array(),'https'),
                            'htmlOptions' => array('class' => 'navbar-form'),
                        ));
                        $model = new LoginForm();
                        ?>
                        <?php echo $form->textField($model,'username',array('placeholder' => Yii::t('site','login...'))); ?>
                        <?php echo $form->passwordField($model,'password',array('placeholder' => Yii::t('site','hasło...'))); ?>
                        <?php echo TbHtml::submitButton('&gt;',array('title' => Yii::t('site','Zaloguj się!'))); ?>
                        <?php $this->endWidget(); ?>
                    </div>

                </div>
            <?php
            else:
                ?>
                <div class="col-xs-12 col-md-3 top-header-account">
                    <p class="hello-top"><?= Yii::t('home', 'Witaj {login}', ['{login}' => '<strong>'.Yii::app()->user->name.'</strong>']);?></p>
                    <?php echo CHtml::link(Yii::t('site','Wyloguj się'), array('/site/logout')); ?>
                    <?php
                    if(!Yii::app()->NPS->isNewPanelSet()):
                        ?>
                        | <a href="<?php echo Yii::app()->createUrl('/userMessage/');?>"><?php echo Yii::t('site','Twoje wiadomości ({no})', array('{no}' => UserMessage::getNoOfNetMessagesForUser(Yii::app()->user->id)));?></a> | <a href="#" id="show-account-form"><?= Yii::t('home', 'Twoje konto');?></a>

                        <div id="fly-account-form">
                            <ul>
                                <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/user/panel');?>"><?php echo Yii::t('site','Panel zarządzania');?></a></li>
                                <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/user/update');?>"><?php echo Yii::t('site','Ustawienia konta');?></a></li>
                                <li></li>
                                <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/userContactBook/');?>"><?php echo Yii::t('user','Książka adresowa');?></a></li>
                                <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/userBalance/');?>"><?php echo Yii::t('user','Bilans');?></a></li>
                                <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/order/');?>"><?php echo Yii::t('user','Zamówienia');?></a></li>
                            </ul>

                        </div>
                    <?php
                    else:
                        ?>
                        | <a href="<?php echo Yii::app()->createUrl('/user/update');?>"><?php echo Yii::t('site','Ustawienia konta');?></a>
                    <?php
                    endif;
                    ?>
                </div>
            <?php
            endif;
            ?>

        </div>




    </div>
</div>
<script>
    $('#show-account-form').on('click', function(event){
        $('#fly-account-form').slideDown();
        $('#fly-sender-form').hide();
        event.stopPropagation();
    });

    $('body').on('click', function(){
        $('#fly-account-form').hide();
    });

    $('#fly-account-form').click(function(event){
        console.log($(this));
        event.stopPropagation();
    });
</script>

<?= $content; ?>

<footer>
    <div class="section-6">
        <div class="container">
            <?php
            if(in_array(Yii::app()->PV->get(), [
                PageVersion::PAGEVERSION_DEFAULT,
                PageVersion::PAGEVERSION_RS,
                PageVersion::PAGEVERSION_ORANGE_POST,
            ])):
            ?>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <br/>
                    <?php
                    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_ORANGE_POST):
                        ?>
                        <img src="<?= Yii::app()->baseUrl.'/theme/orange-post/images/logo_footer.png';?>" class="img-responsive" style="margin: 0 auto;"/>
                    <?php
                    else:
                        ?>
                        <img src="<?= Yii::app()->baseUrl.'/images/v3/logo_footer.png';?>" class="img-responsive" style="margin: 0 auto;"/>
                    <?php
                    endif;
                    ?>
                    <br/>
                    <br/>
                    <br/>
                </div>
            </div>
            <?php
            if(in_array(Yii::app()->PV->get(), [
                PageVersion::PAGEVERSION_DEFAULT,
                PageVersion::PAGEVERSION_RS,
            ])):
                ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 footer-business">
                        <strong><?= Yii::t('site', 'Obsługa klienta biznesowego:');?></strong><br/><br/>
                        Świat Przesyłek Sp. z o.o.<br/>
                        ul. Grodkowska 40, 48-300 Nysa<br/>
                        NIP: 753-244-94-60<br/>
                        KRS: 0000732989<br/><br/>
                        <?= Yii::t('site', 'Operator pocztowy:');?> B-00652<br/>
                        <?= Yii::t('site', 'Telefon:');?> 22 122 12 18<br/>
                        E-mail: info@ swiatprzesylek.pl<br/><br/>
                        BANK ING:<br/>
                        PLN: 27 1050 1490 1000 0090 3153 6692<br/>
                        EUR: 05 1050 1490 1000 0900 3153 6700<br/>
                        BIC: INGPLPW<br/><br/>
                        <a href="<?= Yii::app()->createUrl('/site/regulations');?>"><?= Yii::t('site', 'Regulamin');?></a><br/>
                        <a href="<?= Yii::app()->createUrl('/price');?>"><?= Yii::t('site', 'Cennik');?></a><br/><br/>
                    </div>
                    <div class="col-xs-12 col-sm-6 footer-individual">
                        <strong><?= Yii::t('site', 'Obsługa klienta indywidualnego oraz średnich przedsiębiorstw:');?></strong><br/><br/>
                        Świat Przesyłek Piotr Kocoń<br/>
                        ul. Grodkowska 40, 48-300 Nysa<br/>
                        NIP: 749-203-54-49<br/><br/><br/>
                        <?= Yii::t('site', 'Operator pocztowy:');?> B-0051<br/>
                        <?= Yii::t('site', 'Telefon:');?> 22 122 12 18<br/>
                        E-mail: info@ swiatprzesylek.pl<br/><br/>
                        BANK ING:<br/>
                        PLN: 26 1050 1490 1000 0091 3777 5475<br/>
                        EUR: 77 1050 1490 1000 0091 3777 5624<br/>
                        BIC: INGPLPW<br/><br/>
                        <a href="<?= Yii::app()->createUrl('/site/regulations');?>"><?= Yii::t('site', 'Regulamin');?></a><br/>
                        <a href="<?= Yii::app()->createUrl('/price');?>"><?= Yii::t('site', 'Cennik');?></a><br/><br/>
                    </div>
                </div>
                <?php
                elseif(0 && in_array(Yii::app()->PV->get(), [
                    PageVersion::PAGEVERSION_ORANGE_POST,
                ])):
                    ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 footer-business">
                            <strong>Visitors address</strong><br/><br/>
                            Orange Post N.V.<br/>
                            Westerdreef 5K<br/>
                            2152 CS Nieuw-Vennep<br/>
                            The Netherlands<br/><br/>
                        </div>
                        <div class="col-xs-12 col-sm-6 footer-individual">
                            <strong>Postal address</strong><br/><br/>
                            Orange Post N.V.<br/>
                            Postbus 209<br/>
                            2150 AE Nieuw-Vennep<br/>
                            The Netherlands<br/><br/>
                        </div>
                        <div class="col-xs-12 text-center">
                            Phone: +31 (0) 85 273 6065<br/>
                            Fax: +31 (0) 85 273 6067<br/>
                            E-mail:  info@orange-post.nl
                        </div>
                    </div>
                <?php
                endif;
                ?>
            <?php
            endif;
            ?>
            <div class="row" id="credits">
                <div class="col-xs-12">
                    <br/>
                    <br/>
                    (c) <?php echo date('Y');?>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php
if(!YII_LOCAL):
    ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-52271630-1', 'swiatprzesylek.pl');
        ga('send', 'pageview');
    </script>
<?php
endif;
?>
<?php
$this->widget('RodoInfo');
?>
<script>
    $('[data-preload]').on('click', function(e){
        var textCache = $(this).html();

        $(this).addClass('itemLoading');
        $(this).html('<?= Yii::t('site','Trwa ładowanie...');?>');
        $(this).on('click', function(e){
            e.preventDefault();
        });

        var $that = $(this);

        $(window).on('blur', function(){
            $that.unbind('click');
            $that.removeClass('itemLoading');
            $that.html(textCache);
        })
    });
</script>
<div id="main-modal" role="dialog" tabindex="-1" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/verify-login-status.js', CClientScript::POS_END);
?>
<?php
if(0 && in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS]) AND !YII_LOCAL):
    ?>
    <script type="text/javascript">
        var __cp = {};
        __cp.license = 1963031909;

        (function(){
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https://' : 'http://') + 'cdn.chatpirate.com/plugin.js';
            var sc = document.getElementsByTagName('script')[0]; sc.parentNode.insertBefore(s, sc);
        })();
    </script>
<?php
endif;
?>
<?php
if(in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS]) AND !YII_LOCAL):
    ?>
    <script type="text/javascript" src="https://assets.freshdesk.com/widget/freshwidget.js"></script>
    <script type="text/javascript">
        FreshWidget.init("", {"queryString": "&widgetType=popup&widgetView=no&formTitle=Formularz+reklamacyjny&submitTitle=Wy%C5%9Blij&submitThanks=Dzi%C4%99kujemy%2C+Twoje+zg%C5%82oszenie+zosta%C5%82o+przekazane+do+dzia%C5%82u+reklamacyjnego", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "Zgłoś reklamację", "buttonColor": "white", "buttonBg": "#006063", "alignment": "2", "offset": "235px", "submitThanks": "Dziękujemy, Twoje zgłoszenie zostało przekazane do działu reklamacyjnego", "formHeight": "500px", "url": "https://premiumcs.swiatprzesylek.pl"} );
    </script>
<?php
endif;
?>
<?php
if(0 && in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS])):
    ?>
    <div id="right-flayer" class="hidden-xs">
        <a href="<?= Yii::app()->urlManager->createUrl('/site/tt');?>" target="_blank" id="hbm_tt" class="flayer-item"></a>
        <a href="https://www.facebook.com/SWIATPRZESYLEK" target="_blank" id="hbm_fb" class="flayer-item"></a>
    </div>
<?php
endif;
?>
</body>
</html>