<?php

Yii::import('application.models._base.BaseUserMessage');

class UserMessage extends BaseUserMessage
{
    const UNPUBLISHED = 0;
    const PUBLISHED = 1;
    const SEEN = 2;

    const FINAL_DIR = 'upadmin/messages/';

    public $_file;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    public function rules() {
        return array(
            array('user_id', 'required'),
            array('stat, user_id, no', 'numerical', 'integerOnly'=>true),
            array('file_name', 'length', 'max'=>128),
            array('file_path', 'length', 'max'=>256),
            array('date, file_type', 'length', 'max'=>32),
            array('hash', 'length', 'max'=>64),
            array('date_updated, text', 'safe'),
            array('date_updated, file_path, file_name, file_type, text', 'default', 'setOnEmpty' => true, 'value' => null),
            array('stat', 'default', 'setOnEmpty' => true, 'value' => self::UNPUBLISHED),
            array('id, date_entered, date_updated, file_path, file_name, file_type, text, date, user_id, no, hash, stat', 'safe', 'on'=>'search'),
        );
    }

    public static function getStatArray()
    {
        $array = [
            self::UNPUBLISHED => 'Unpublished',
            self::PUBLISHED => 'Published',
            self::SEEN => 'Seen',
        ];

        return $array;
    }

    public static function getStatArrayUser()
    {
        $array = [
            self::PUBLISHED => Yii::t('user_message', 'Unseen'),
            self::SEEN => Yii::t('user_message', 'Seen'),
        ];

        return $array;
    }

    public static function getFinalDir($fileName)
    {
        $path = realpath(self::FINAL_DIR);
        $path = $path.'/'.$fileName;
        return $path;
    }

    public function beforeSave()
    {
        if ($this->isNewRecord)
        {
            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => microtime().uniqid()));
        }

        if($this->date == '')
            $this->date = date('Y-m-h h:i');

        return parent::beforeSave();
    }

    public function deleteWithFile()
    {

        if($this->file_name !== NULL)
        {
            @unlink($this->file_path);

        }

        return $this->delete();
    }

    public function publish()
    {
        $this->stat = self::PUBLISHED;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('no');
        $cmd->from('user_message');
        $cmd->where('user_id = :user_id AND (stat = 1 OR stat = 2)', array(':user_id' => $this->user_id));
        $cmd->order('no desc');
        $cmd->limit(1);
        $count = $cmd->queryScalar();

        $count = intval($count);

        $count++;
        $this->no = $count;

        return $this->update(array('stat', 'no'));
    }

    public function notifyAboutPublished()
    {

        $lang = $this->user->getPrefLang();

        $tempLangChange = new TempLangChange();
        $tempLangChange->temporarySetLanguage($lang);


        S_Mailer::sendToCustomer($this->user->getAccountingEmail(true),$this->user->name, Yii::t('user_mail','Nowa wiadomosć'), Yii::t('user_mail',"Na Twoim koncie pojawiła się nowa wiadomość!{br}", array('{br}' => '<br/>')), false, $lang, true, false, false, false, false, $this->user->source_domain);


        if($this->user->tel != '')
        {
            $smsApi = Yii::createComponent('application.components.SmsApi');
            $smsApi->sendSms($this->user->tel, Yii::t('user_sms', 'Na Twoim koncie pojawila sie nowa wiadomosc!'), $this->user->source_domain);
        }

        $tempLangChange->revertToBaseLanguage();
    }

    public function seen()
    {
        if($this->stat == self::SEEN)
            return true;


        $this->stat = self::SEEN;
        return $this->update(array('stat'));
    }

    public static function getNoOfNetMessagesForUser($user_id)
    {
//        $models = UserMessage::model()->cache(60*15)->findAllByAttributes(array('user_id' => $user_id, 'stat' => UserMessage::PUBLISHED));
        $models = UserMessage::model()->findAllByAttributes(array('user_id' => $user_id, 'stat' => UserMessage::PUBLISHED));

        return S_Useful::sizeof($models);

    }

}
