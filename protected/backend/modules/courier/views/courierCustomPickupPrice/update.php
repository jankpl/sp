<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label' =>'List ' . $model->label(2), 'url'=>array('index')),
    array('label' =>'View ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
);
?>
    <h1>Update Custom Pickup Price:</h1>
<h3><?= $model->typeName;?> | <?= $model->sizeName; ?> | <?= $model->countryList->name;?></h3>
<?php
if($userGroup !== NULL):
    ?>
    <div class="alert alert-warning">
        <h2>For user group: <?= $userGroupModel->name;?></h2>
    </div>
    <?php
else:
    ?>
    <div class="alert alert-info">
        <p>Edit custom settings for user group:</p>
        <?= CHtml::dropDownList('user_group','', $listUserGroups, array('id' => 'group_edit')); ?>
        <?= CHtml::button('Edit',array('onClick' => 'window.location.assign("'.Yii::app()->urlManager->createUrl('/courier/courierCustomPickupPrice/update', ['id' => $model->id]).'?user_group=" + $("#group_edit").val())')); ?>
    </div>
    <?php
endif;
?>

<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'modelTrs' => $modelTrs,
    'userGroup' => $userGroup,

));
?>