<?php
/* @var integer $packageWeight */
/* @var integer $userGroup */
/* @var $countryList CountryList[] */


?>

    <h1>Simulate courier pricing</h1>

    <div class="form flash-notice" style="text-align: center;">
        <?php
        $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'simulate',
            'enableAjaxValidation' => false,
        ));

        ?>
        <div class="row">
            <?php echo CHtml::label('Package origination', 'from'); ?>
            <?php echo CHtml::dropDownList('from', $from, CHtml::listData($countryGroupList, 'id', 'name')); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo CHtml::label('Package weight', 'package_weight'); ?>
            <?php echo CHtml::textField('package_weight', $packageWeight, array('maxlength' => 10)); ?>  <?php echo Courier::getWeightUnit();?>
        </div><!-- row -->
        <div class="row">
            <?php echo CHtml::label('Package size l', 'package_size_l'); ?>
            <?php echo CHtml::textField('package_size_l', $package_size_l, array('maxlength' => 10)); ?>  <?php echo Courier::getDimensionUnit();?>
        </div><!-- row -->
        <div class="row">
            <?php echo CHtml::label('Package size d', 'package_size_d'); ?>
            <?php echo CHtml::textField('package_size_d', $package_size_d, array('maxlength' => 10)); ?>  <?php echo Courier::getDimensionUnit();?>
        </div><!-- row -->
        <div class="row">
            <?php echo CHtml::label('Package size w', 'package_size_w'); ?>
            <?php echo CHtml::textField('package_size_w', $package_size_w, array('maxlength' => 10)); ?>  <?php echo Courier::getDimensionUnit();?>
        </div><!-- row -->
        <div class="row">
            <?php echo CHtml::label('Pickup mode', 'with_pickup'); ?>
            <?php echo CHtml::dropDownList('with_pickup', $withPickup, CourierTypeInternal::getWithPickupList(false)); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo CHtml::label('User group', 'user_group'); ?>

            <?php
            $userGroupList = CHtml::listData(UserGroup::listGroups(),'id','nameWithId');
            $userGroupList[''] = '-';

            ?>

            <?php echo CHtml::dropDownList('user_group', $userGroup, $userGroupList); ?>


        </div><!-- row -->

        <div class="row">
            <?php echo CHtml::label('Currency', 'currency'); ?>


            <?php echo CHtml::dropDownList('currency', $currency, CHtml::listData($currencyList,'id','name')); ?>
        </div><!-- row -->

        <?php
        echo GxHtml::submitButton(Yii::t('app', 'Simulate', array('name' => 'simulate')));
        $this->endWidget();
        ?>
    </div>

    <br/><br/>

<?php
if(isset($_POST['from']) && is_array($countryGroupList)):
    ?>

    <?php
    echo'<h3>Package weight: '.$packageWeight.' '.Courier::getWeightUnit().'</h3>';
    echo'<h3>With pickup: '.($withPickup?'yes':'no').'</h3>';
    echo'<h3>User group #: '.$userGroup.'</h3>';

    echo '<table class="list hTop" style="width: 100%;">
        <tr>
        <td>From (group)</td>
        <td>To (group)</td>
        <td>Valid</td>
        <td>Pickup HUB</td>
        <td>Destination HUB</td>
        <td>Pickup Operator</td>
        <td>Destination Operator</td>
        <td>Pickup price netto</td>
        <td>Delivery price netto</td>
        <td>Total price netto</td>
        </tr>';

    /* @ var $countryGroupFrom CourierCountryGroup */
    /* @ var $countryGroupTo CourierCountryGroup */


    $countryGroupFrom = CourierCountryGroup::model()->findByPk($from);


    //foreach($countryGroupList AS $countryGroupFrom)
    {

        foreach($countryGroupList AS $countryGroupTo)
        {

            $countryFrom = $countryGroupFrom->courierCountryGroupHasCountries[0]->country0;
            $countryTo = $countryGroupTo->courierCountryGroupHasCountries[0]->country0;


            if($countryFrom === NULL OR $countryTo === NULL)
                continue;

            $courier = new Courier;
            $courier->overrideOwnerUserGroupId($userGroup);
            $courier->courierTypeInternal = new CourierTypeInternal();
            $courier->courierTypeInternal->courier = $courier;
            $courier->package_weight = $packageWeight;
            $courier->package_size_l = $package_size_l;
            $courier->package_size_d = $package_size_d;
            $courier->package_size_w = $package_size_w;
            $courier->courierTypeInternal->final_weight =$packageWeight;
            $courier->senderAddressData = new AddressData();
            $courier->receiverAddressData = new AddressData();

            $courier->senderAddressData->country_id = $countryFrom->id;
            $courier->receiverAddressData->country_id = $countryTo->id;
            $courier->packages_number = 1;
            $courier->courierTypeInternal->with_pickup = $withPickup;


            $possible = $courier->courierTypeInternal->isCarriagePossible($userGroup);

            if($possible)
                $color = 'green';
            else
                $color = 'red';

            echo'<tr>
                 <td style="font-weight: bold;">'.$countryGroupFrom->name.'</td>
                 <td style="font-weight: bold;">'.$countryGroupTo->name.'</td>
                 <td style="width: 15px; background: '.$color.';"></td>
                 <td>'.$courier->courierTypeInternal->getPickupHub(true)->getNameWithId().'</td>
                 <td>'.$courier->courierTypeInternal->getDeliveryHub(true)->getNameWithId().'</td>
                 <td>'.$courier->courierTypeInternal->getPickupOperator(true)->getNameWithIdAndBroker().'</td>
                 <td>'.$courier->courierTypeInternal->getDeliveryOperator(true)->getNameWithIdAndBroker().'</td>
                 <td>'.(($possible)?S_Price::formatPrice($courier->courierTypeInternal->pickupPrice($currency->id, false, $userGroup)).' '.$currency->symbol:'-').'</td>
                 <td>'.(($possible)?S_Price::formatPrice($courier->courierTypeInternal->deliveryPrice($currency->id, false, $userGroup)).' '.$currency->symbol:'-').'</td>     
                 <td>'.(($possible)?S_Price::formatPrice($courier->courierTypeInternal->priceTotal($currency->id, false, $userGroup)).' '.$currency->symbol:'-').'</td>';
            echo'</tr>';
        }

        echo'<tr>
    <td colspan="8" style="height: 5px;"></td>
    </tr>';

    }
    echo '</table>';


endif;
?>