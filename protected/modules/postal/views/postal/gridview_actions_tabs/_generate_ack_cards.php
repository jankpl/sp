<?php
/* @var $id_prefix string */
?>
<br/>
<?= $this->renderPartial('/_listActions/_generateAckCardMulti', ['id_prefix' => $id_prefix]); ?>
<br/>
<small><?= Yii::t('postal', 'Cancelled packages will be omitted.');?></small>
