<?php

class Courier_CourierTypeU extends Courier
{

    public $_package_weight_total;
    public $_client_cod;
//    public $_client_cod_value;

    const SCENARIO_DEFAULT_PACKAGE_DATA = 'default_package_data';
    const SCENARIO_STEP1 = 'step1';

//    public function checkClientCodValue($attribute,$params)
//    {
//        if($this->_client_cod)
//        {
//            if(!($this->_client_cod_value > 0))
//                $this->addError('_client_cod_value', Yii::t('courier', 'Podaj wartość COD dla paczki!'));
//        }
//    }

    public function rules() {


        $arrayValidate = array(
            ['_inpost_locker_active', 'boolean'],
            ['_ruch_receiver_point, _ruch_receiver_point_address', 'application.validators.lettersNumbersBasicValidator'],
            ['_inpost_locker_delivery_address, _inpost_locker_delivery_id', 'application.validators.lettersNumbersBasicValidator'],



            array('value_currency', 'default', 'value' => 'PLN'),
            array('value_currency', 'in', 'range' => self::getCurrencyList(), 'allowEmpty' => true),

            array('ref', 'application.validators.courierRefValidator'),

//            array('user_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?NULL:Yii::app()->user->id),

//            array('_client_cod_value', 'numerical', 'max' =>  PaymentType::MAX_COD_AMOUNT, 'min' => 0),
//            array('_client_cod_value', 'checkClientCodValue', 'on' => 'step1'),

            array('cod_value', 'numerical', 'min' => 0),
            array('cod_value', 'match', 'pattern'=> '/^[0-9]+(\.[0-9]{1,2})?$/'),


            array('cod_currency', 'in', 'range' => array_keys(CourierCodAvailability::getCurrencyList()) , 'message' => Yii::t('courier', 'Ta waluta nie jest obsługiwana')),

            array('package_value', 'numerical', 'min'=>1, 'max' => 999999),
            array('package_value', 'match', 'pattern'=> '/^[0-9]+(\.[0-9]{1,2})?$/'),

            array('packages_number', 'numerical', 'integerOnly'=>true, 'min' => 1),
            array('packages_number', 'default', 'setOnEmpty' => true, 'value' => 1),

            array('package_weight, package_size_l, package_size_w, package_size_d', 'required'),
            array('package_content', 'required', 'except' => self::SCENARIO_DEFAULT_PACKAGE_DATA),
            array('package_size_l', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999),
            array('package_size_w', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999),
            array('package_size_d', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999),
            array('package_weight, package_weight_client', 'numerical', 'min' => 0.01, 'max' => 1000),


            array('user_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?NULL:Yii::app()->user->id),
            array('admin_id', 'default', 'setOnEmpty' => true, 'value' => Yii::app()->user->isAdmin()?Yii::app()->user->id:NULL),
            array('package_weight, package_weight, package_size_l, package_size_w, package_size_d, package_value', 'required'),

            array('courier_type_external_id, sender_address_data_id, receiver_address_data_id, courier_stat_id', 'numerical', 'integerOnly'=>true),

//            array('package_size_l, package_size_w, package_size_d', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('package_weight', 'numerical', 'min' => 0.01),

            array('_package_weight_total', 'numerical', 'min' => 0.01),
            array('_package_weight_total', 'required', 'on' => self::SCENARIO_STEP1),

            array('ref', 'length', 'max'=>64),
            array('note1, note2', 'length', 'max'=>50),

            array('_sendLabelOnMail', 'boolean', 'allowEmpty'=> true),


            array('source', 'numerical', 'integerOnly'=>true),

            array('date_updated, _generate_return', 'safe'),
            array('courier_type_external_id, date_updated, sender_address_data_id, package_value, package_content, package_weight, package_size_l, package_size_w, package_size_d, _client_cod', 'default', 'setOnEmpty' => true, 'value' => null),
        );

        $arrayBasic = array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert', 'setOnEmpty' => false),

            array('courier_type_external_id, date_updated, local_id, hash, sender_address_data_id, receiver_address_data_id, courier_stat_id, package_weight, package_size_l, package_size_w, package_size_d, packages_number, package_value, package_content, cod, ref, user_id, admin_id, _first_status_date, note1, note2', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('courier_type_external_id, date_updated, sender_address_data_id, receiver_address_data_id, package_weight, package_size_l, package_size_w, package_size_d, package_value, package_content, cod, ref, user_id, admin_id', 'default', 'setOnEmpty' => true, 'value' => null),

            array('courier_stat_id', 'default', 'setOnEmpty' => true, 'value' => CourierStat::NEW_ORDER),
        );


        return array_merge($arrayBasic, $arrayValidate);
    }

    public function attributeLabels() {
        $labels = array(
            '_package_weight_total' => Yii::t('courier', 'Packages total weight'),
//            '_client_cod_value' => Yii::t('courier', 'COD Value'),
        );

        return array_merge($labels, parent::attributeLabels());
    }

    public function beforeValidate()
    {

        if($this->_ruch_receiver_point_active)
        {
            if($this->_ruch_receiver_point == '')
                $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybierz punkt odbioru paczki RUCH!'));

            if(!$this->hasErrors('_ruch_receiver_point')) {
                if ($this->cod_valu > 0) {
                    if (!PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, true)) {
                        if (PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, false)) {
                            $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybrany punkt RUCH nie obsługuje paczek COD!'));
                        } else
                            $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybranych punk RUCH nie jest poprawny!'));
                    }
                } else {
                    if (!PaczkaWRuchu::validatePoint($this->_ruch_receiver_point, false))
                        $this->addError('_ruch_receiver_point', Yii::t('courier', 'Wybranych punk RUCH nie jest poprawny!'));
                }
            }
        }


        if($this->_inpost_locker_active)
        {
            if($this->_inpost_locker_delivery_id == '')
            {
                $this->addError('_inpost_locker_delivery_id', Yii::t('courier', 'Wybierz punkt z listy.'));
            }
        }


        if(is_string($this->cod_value))
            $this->cod_value = (double) ($this->cod_value);

        // for u Form
        if($this->_client_cod)
        {
            if(!($this->cod_value > 0))
                $this->addError('cod_value', Yii::t('courier', 'Podaj wartość COD dla paczki!'));
        }

        if($this->_generate_return)
        {
            if(!CourierTypeReturn::isAutomaticReturnAvailable($this->receiverAddressData->country_id))
            {
                $this->_generate_return = false;
            }
//                $this->addError('_generate_return', Yii::t('courier', 'Etykieta zwrotna nie jest dostępna dla tych parametrów!'));
        }

        if($this->cod_value)
        {
            if(!$this->findCodCurrency())
                $this->addError('cod_value', Yii::t('courier', 'COD nie jest dostępne dla tych parametrów!'));
            else {

                if ($this->cod_currency == '') {
                    $this->cod_currency = $this->findCodCurrency();
                } else if (!CourierCodAvailability::validateCurrencyNameForCountry($this->cod_currency, $this->receiverAddressData->country_id)) {
                    $properCurrency = CourierCodAvailability::findCurrencyForCountry($this->receiverAddressData->country_id);
                    $properCurrency = CourierCodAvailability::getCurrencyNameById($properCurrency);
                    $this->addError('cod_currency', Yii::t('courier', 'Prawidłowa waluta dla tego kraju to:') . ' ' . $properCurrency);
                }

                if(!$this->hasErrors('cod_currency'))
                {
                    $max_cod_value = CourierCodAvailability::getCurrencyMaxValueById($this->cod_currency);
                    if ($this->cod_value > $max_cod_value)
                        $this->addError('cod_value', Yii::t('courier', 'Maksymalna wartość COD dla {currency} to: {max_value}', ['{currency}' => $this->cod_currency, '{max_value}' => $max_cod_value]));
                }
            }
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    protected static function rabenHeightToWeight($height)
    {
        if($height < 40)
            $weight = false;
        else if($height < 80)
            $weight = 100.01;
        else if($height < 120)
            $weight = 200.01;
        else if($height < 150)
            $weight = 300.01;
        else if($height < 180)
            $weight = 400.01;
        else
            $weight = 500.01;

        return $weight;
    }

    public function afterValidate()
    {

        $_isWeightValidated = $_isDimensionsValidated = false;

        if($this->user && $this->user->getDisableDimensionLimits())
            $_isDimensionsValidated = true;

//        if(in_array($this->courierTypeDomestic->domestic_operator_id, [CourierDomesticOperator::OPERATOR_DHL_PALETA, CourierDomesticOperator::OPERATOR_DHL_F_PALETA])) {
//            $this->_disableCalculationDimensionalWeight = true;
//            $this->_disableAdditionalWeightAndSizeCheck = true;
//        }

//        if(parent::afterValidate())
        {
            $dimensions = [
                $this->package_size_l,
                $this->package_size_d,
                $this->package_size_w,
            ];

            // biggest dimension is length and is first
            rsort($dimensions);

            // DEFAULT:

            // RoyalMail:
            if ($this->courierTypeU->courierUOperator->broker_id == CourierUOperator::BROKER_ROYALMAIL) {
                if ($this->package_weight > RoyalMailClient::RM_MAX_WEIGHT) {
                    $this->clearErrors('package_weight');
                    $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => RoyalMailClient::RM_MAX_WEIGHT]));
                }
                $_isWeightValidated = true;
            }


            /* @ 07.11.2017 - Special rule for PALETY */
            if ($this->courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHL_F_PALETY) {
                $paletteMaxDimension = 200;

                $_isWeightValidated = true;
                $_isDimensionsValidated = true;

                if ($this->package_weight > 1000) {
                    $this->clearErrors('package_weight');
                    $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => 1000]));
                }

                if(!$_isDimensionsValidated) {

                    if ($this->package_size_l > $paletteMaxDimension) {
                        $this->clearErrors('package_size_l');
                        $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => $paletteMaxDimension, '{b}' => $paletteMaxDimension, '{c}' => $paletteMaxDimension]));

                    }
                    if ($this->package_size_d > $paletteMaxDimension) {
                        $this->clearErrors('package_size_d');
                        $this->addError('package_size_d', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => $paletteMaxDimension, '{b}' => $paletteMaxDimension, '{c}' => $paletteMaxDimension]));
                    }

                    if ($this->package_size_w > $paletteMaxDimension) {
                        $this->clearErrors('package_size_w');
                        $this->addError('package_size_w', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => $paletteMaxDimension, '{b}' => $paletteMaxDimension, '{c}' => $paletteMaxDimension]));
                    }
                }

            } else if ($this->courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHL_F_DLUZYCE) {

               if(!$_isDimensionsValidated) {
                   if ($dimensions[0] > 400 OR
                       $dimensions[1] > 400 OR
                       $dimensions[2] > 400) {
                       $this->clearErrors('package_size_l');
                       $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => 400, '{b}' => 400, '{c}' => 400]));
                   }

                   if ($dimensions[0] + $dimensions[1] + $dimensions[2] > 600) {
                       $this->clearErrors('package_size_l');
                       $this->clearErrors('package_size_d');
                       $this->clearErrors('package_size_w');
                       $this->addError('package_size_l', Yii::t('courier', 'Maksymalne suma wymiarów to: {n} cm!', ['{n}' => 600]));
                       $this->addError('package_size_d', Yii::t('courier', 'Maksymalne suma wymiarów to: {n} cm!', ['{n}' => 600]));
                       $this->addError('package_size_w', Yii::t('courier', 'Maksymalne suma wymiarów to: {n} cm!', ['{n}' => 600]));

                   }
               }

                $_isDimensionsValidated = true;
            }


            $flashNote = [];
            /* @ 07.11.2017 - Special rule for RABEN */
            if ($this->courierTypeU->courierUOperator->broker_id == GLOBAL_BROKERS::BROKER_RABEN) {


                if(!$_isDimensionsValidated) {

                    if ($this->package_size_l != 120) {
                        $this->package_size_l = 120;
                        $flashNote[] = Yii::t('courier', 'Dłgość palety to 120cm!');
                    }

                    if ($this->package_size_w != 80) {
                        $this->package_size_w = 80;
                        $flashNote[] = Yii::t('courier', 'Szerokość palety to 80cm!');
                    }

                    if ($this->package_size_d > 200) {
                        $this->addError('package_size_w', Yii::t('courier', 'Maksymalna wysokość palety to 200cm!'));
                    }

                }

                if($this->package_weight < self::rabenHeightToWeight($this->package_size_d))
                {
                    $this->package_weight = self::rabenHeightToWeight($this->package_size_d);
                    $flashNote[] = 'Waga została automatycznie skorygowana dla podanej wysokośći!';
                }

                $_isWeightValidated = true;
                $_isDimensionsValidated = true;

                if ($this->package_weight > 1000) {
                    $this->clearErrors('package_weight');
                    $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => 1000]));
                }

            }


            if (!$_isWeightValidated) {
                if ($this->package_weight > S_PackageMaxDimensions::getPackageMaxDimensions()['weight']) {
                    $this->clearErrors('package_weight');
                    $this->addError('package_weight', Yii::t('courier', 'Maksymalna waga paczki to: {waga} kg!', ['{waga}' => S_PackageMaxDimensions::getPackageMaxDimensions()['weight']]));
                }
            }


            if (!$_isDimensionsValidated) {

                if ($this->package_size_l > S_PackageMaxDimensions::getPackageMaxDimensions()['l']) {
                    $this->clearErrors('package_size_l');
                    $this->addError('package_size_l', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => S_PackageMaxDimensions::getPackageMaxDimensions()['l'], '{b}' => S_PackageMaxDimensions::getPackageMaxDimensions()['d'], '{c}' => S_PackageMaxDimensions::getPackageMaxDimensions()['w']]));
                }

                if ($this->package_size_d > S_PackageMaxDimensions::getPackageMaxDimensions()['d'])
                {
                    $this->clearErrors('package_size_d');
                    $this->addError('package_size_d', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => S_PackageMaxDimensions::getPackageMaxDimensions()['l'], '{b}' => S_PackageMaxDimensions::getPackageMaxDimensions()['d'], '{c}' => S_PackageMaxDimensions::getPackageMaxDimensions()['w']]));
                }

                if ($this->package_size_w > S_PackageMaxDimensions::getPackageMaxDimensions()['w'])
                {
                    $this->clearErrors('package_size_w');
                    $this->addError('package_size_w', Yii::t('courier', 'Maksymalne wymiary paczki to: {a}x{b}x{c} cm!', ['{a}' => S_PackageMaxDimensions::getPackageMaxDimensions()['l'], '{b}' => S_PackageMaxDimensions::getPackageMaxDimensions()['d'], '{c}' => S_PackageMaxDimensions::getPackageMaxDimensions()['w']]));
                }

            }

        }

        if($this->_inpost_locker_active && $this->_inpost_locker_delivery_id)
        {
            if($dimensions[2] > 8 && $dimensions[2] <= 19)
            {
                $this->courierTypeU->_courier_additions[] = CourierUAdditionList::getIdByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_SIZE_B);
                $flashNote[] = Yii::t('courier', 'Ze względu na podane parametry paczki został automatycznie wybrany dodatek gabarytowy Inpost!');
            }
            else if($dimensions[2] > 19)
            {
                $this->courierTypeU->_courier_additions[] = CourierUAdditionList::getIdByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_SIZE_C);
                $flashNote[] = Yii::t('courier', 'Ze względu na podane parametry paczki został automatycznie wybrany dodatek gabarytowy Inpost!');
            }

        }

        if(S_Useful::sizeof($flashNote))
            Yii::app()->user->setFlash('notice', implode('</br/>', $flashNote));


        parent::afterValidate();
    }

    public function findCodCurrency()
    {
        $codCurrency = CourierCodAvailability::findCurrencyForCountry($this->receiverAddressData->country_id);
        if($codCurrency)
        {
            $codCurrency = CourierCodAvailability::getCurrencyNameById($codCurrency);
        }

        return $codCurrency;
    }

    public function validateCodCurrency()
    {
        return CourierCodAvailability::validateCurrencyNameForCountry($this->cod_currency, $this->receiverAddressData->country_id);
    }

    public function beforeSave()
    {
        if($this->cod_value && $this->cod_currency === NULL)
            $this->cod_currency = $this->findCodCurrency();

        $dw = $this->courierTypeU->getFinalWeight();

        if($dw > $this->package_weight) {
            $this->package_weight_client = $this->package_weight;
            $this->package_weight = $dw;
        }
//        else {
////            $this->package_weight = $this->package_weight_client;
//        }

        $this->courier_type = self::TYPE_U;

        $this->operator_partner_id = $this->courierTypeU->courierUOperator->partner_id;


        return parent::beforeSave();
    }
}