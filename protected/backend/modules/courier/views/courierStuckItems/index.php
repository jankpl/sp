<?php
/* @var $models Courier[] */
?>

<table class="table table-striped table-bordered table-condensed">
    <tr>
        <th>#</th>
        <th>ID</th>
        <th>Operator</th>
        <th>date_entered</th>
        <th>date_updated</th>
        <th>days</th>
        <th>last status</th>
        <th>last status map</th>
        <th>customer</th>
    </tr>
    <?php
    $i = 1;
    $diff = 60 * 60 * 24;
    $brokerList = CourierLabelNew::getOperators();
    foreach($models AS $model):
        $brokers = $model->listBrokers();
        array_walk($brokers, function(&$item1, $key) use ($brokerList){
            $item1 = $brokerList[$item1];
        });
        ?>
        <tr>
            <td><?= $i;?></td>
            <td><a href="<?= Yii::app()->createUrl('/courier/courier/view', ['id' => $model->id]);?>" target="_blank"><?= $model->local_id;?></a></td>
            <td><?= implode(', ', $brokers);?></td>
            <td><?= substr($model->date_entered,0 , 10);?></td>
            <td><?= substr($model->date_updated,0 , 10);?></td>
            <td><?= round((time() - strtotime($model->date_updated)) / ($diff));?></td>
            <td><?= $model->stat->name;?></td>
            <td><?= $model->stat_map_id;?></td>
            <td><?= $model->user->login;?></td>
        </tr>


        <?php
        $i++;
    endforeach;
    ?>
</table>
