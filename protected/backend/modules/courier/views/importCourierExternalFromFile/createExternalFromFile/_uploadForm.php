<div class="form">
    <?php

    $form = $this->beginWidget('CActiveForm',
        array(
            'enableAjaxValidation' => false,
            'htmlOptions' =>
            array('enctype' => 'multipart/form-data'),
        )
    );
    ?>

    <h2>Import packages type External from file</h2>

    <?php

    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }

    echo $form->errorSummary($model);
    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'file'); ?>
        <?php echo $form->fileField($model, 'file'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'split'); ?>
        <?php echo $form->dropDownList($model, 'split', F_UploadFile::$_splitArray); ?>
    </div>
    <div class="row">

        <?php echo $form->checkBox($model, 'omitFirst'); ?>
        <?php echo $form->labelEx($model,'omitFirst', array('style' => 'display: inline;')); ?>
    </div>

    <div class="navigate">

        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('size' => TbHtml::BUTTON_SIZE_LARGE));
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p>Allowed file type: .csv,.xls,.xlsx.</p>
        <p>The file can contain following columns:<br/>
            [status <?php echo TbHtml::tooltip('?','#','Can be status Id, status name, short name or full name in any language. If not provided, will be provided with first status');?>],
            [first_status_date],
            [package_weight],
            [package_size_l],
            [package_size_w],
            [package_size_d],
            [packages_number],
            [package_value],
            [package_content],
            [user_id],
            [courier_external_operator_id],
            [external_id],
            [sender_name],
            [sender_company],
            [sender_country],
            [sender_zip_code],
            [sender_city],
            [sender_address_line_1],
            [sender_address_line_2],
            [sender_tel],
            [receiver_name],
            [receiver_company],
            [receiver_country],
            [receiver_zip_code],
            [receiver_city],
            [receiver_address_line_1],
            [receiver_address_line_2],
            [receiver_tel],
            [ref]
        </p>
    </div>
</div>
<?php echo TbHtml::link('Download file template', Yii::app()->baseUrl.'/misc/backend/ipff_template.xlsx', array('target' => '_blank', 'class' => 'btn', 'download' => 'ipff_template.xlsx'));?>
<br/>
<br/>