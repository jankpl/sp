<?php

class UserMessageController extends Controller
{

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('@'),
				'expression'=>'0 >= Yii::app()->user->authority',
			),
			array('deny',  // block rest of actions
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id, 'UserMessage'),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new UserMessage;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserMessage']))
		{
			$model->attributes=$_POST['UserMessage'];
			$model->_file = CUploadedFile::getInstance($model,'_file');

			$model->validate();
			$errors = false;

			if(!$model->hasErrors())
			{

				$transaction = Yii::app()->db->beginTransaction();
				if(!$model->save())
					$errors = true;

				if($model->_file !== NULL) {

					// to get proper hash
					$modelNew = UserMessage::model()->findByPk($model->id);


					$path = UserMessage::getFinalDir($modelNew->hash.'.'.$model->_file->extensionName);
					if (!$model->_file->saveAs($path)) {
						$errors = true;
					} else {
						$model->file_path = $path;
						$model->file_name = $model->_file->name;
						$model->file_type = mime_content_type($path);
						$model->update(array('file_path', 'file_name', 'file_type'));
					}
				}

				if($errors)
				{
					$transaction->rollback();
				} else {
					$transaction->commit();

					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id, 'UserMessage');


		if($model->stat > UserMessage::UNPUBLISHED)
		{
			throw new CHttpException(403, 'This message has already been published!');
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserMessage']))
		{
			$model->attributes=$_POST['UserMessage'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{

		$model = UserMessage::model()->findByPk($id);

		if($model === NULL) {
			throw new CHttpException(404);
		}

		if($model->stat > UserMessage::UNPUBLISHED)
		{
			throw new CHttpException(403, 'You can\'t delete published messages!');
		}

		$model->deleteWithFile();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('/userMessage/admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new UserMessage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UserMessage']))
			$model->attributes=$_GET['UserMessage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	public function actionPublish($id)
	{
		$model = UserMessage::model()->findByPk($id);

		if($model === NULL) {
			throw new CHttpException(404);
		}

		if($model->stat > UserMessage::UNPUBLISHED)
		{
			throw new CHttpException(403, 'This message has already been published!');
		}

		if($model->publish())
            $model->notifyAboutPublished();

		$this->redirect(array('view','id'=>$model->id));
	}

	public function actionGetFile($id)
	{
		$model = UserMessage::model()->findByPk($id);

		if($model === NULL) {
			throw new CHttpException(404);
		}

		if($model->file_path == NULL)
			throw new CHttpException(404);

		$path = $model->file_path;

		return Yii::app()->getRequest()->sendFile($model->file_name, @file_get_contents($path), $model->file_type);
	}
}

