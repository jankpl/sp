<?php

class Report_CustomersCourierReturnScan extends Report
{
    const SEPARATOR = '; ';

    private static $reportType = self::TYPE_CUSTOMERS_COURIER_RETURN_SCAN;

    public static function checkItem(Courier $courier)
    {
        // EOBUWIE has it's own reporting
        if(in_array($courier->user_id, [947,948, 2036, 2037]))
            return false;

        // Works only on returns
        if($courier->stat_map_id != StatMap::MAP_RETURN)
            return false;

        $date = date('Y-m-d');
        $model = self::model()->find('type = :type AND DATE(date_entered) = :date AND stat = 0 AND what_id = :what_id', [':type' => self::$reportType, ':date' => $date, ':what_id' => $courier->user_id]);

        $no = 1;
//        $id = $courier->listExternalIdsByCourierLabelNew(true, self::SEPARATOR).self::SEPARATOR;
        $id = $courier->local_id.self::SEPARATOR;

        if($model)
        {
            $model->content->no += $no;
            $model->content->ids .= $id;
        } else {
            $model = new self;
            $model->type = self::$reportType;
            $model->stat = 0;
            $model->what_id = $courier->user_id;

            $model->content = new stdClass();
            $model->content->no = $no;
            $model->content->ids = $id;
        }

        return $model->save();
    }

    public static function processAllReports()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('what_id')
            ->from((new self)->tableName())
            ->where('type = :type AND stat = 0 AND date_entered < :date', [
                ':type' => self::$reportType,
                ':date' => date('Y-m-d'),
            ]);

        $user_ids = $cmd->queryColumn();

        foreach($user_ids AS $user_id)
            self::processReport($user_id);
    }

    public static function processReport($user_id)
    {

        $models = self::model()->findAll('type = :type AND stat = 0 AND date_entered < :date AND what_id = :what_id', [
            ':type' => self::$reportType,
            ':date' => date('Y-m-d'),
            ':what_id' => $user_id,
        ]);

        if(!S_Useful::sizeof($models))
            return false;

        /* @var $user User */
        $user = User::model()->findByPk($user_id);

        $lang = $user->getPrefLang();

        $report = Yii::t('mail_courier_return_report', 'Szanowny Kliencie,{br}{br}w załączeniu przesyłamy raport zawierający zestawienie przesyłek zwracanych z powodu niemożliwości doręczenia ich adresatowi z przyczyn niezależnych od Operatora (tj. np. z powodu braku lub odmowy odbioru przesyłki przez adresata, etc.). W związku z nadchodzącym terminem wydania Państwu ww. przesyłek, prosimy o dokładną weryfikację liczby faktycznie wydanych przesyłek z załączonym zestawieniem i zgłoszenie ewentualnych niezgodności pomiędzy ww. zestawieniem, a liczbą faktycznie zwróconych przesyłek, w nieprzekraczalnym terminie 3 dni roboczych od dnia wydania Państwu zwracanych przesyłek.{br}Jednocześnie oświadczamy, że wszelkie reklamacje na liczbę faktycznie wydanych przesyłek, złożone po wskazanym powyżej terminie zostaną pozostawione bez rozpoznania.', ['{br}' => '<br/>'])."<br/>";
        $report .= "<br/><br/>";

        $report .= Yii::t('mail_courier_return_report', 'Courier return scans report')."<br/>";
        $report .= "<br/><br/>";
        $report .= Yii::t('mail_courier_return_report', 'User:').' '.$user->login."<br/>";
        $report .= '<table border="1">
<tr style="font-weight: bold;"><td>'.Yii::t('mail_courier_return_report', 'Date').'</td><td>'.Yii::t('mail_courier_return_report', 'No').'</td><td>'.Yii::t('mail_courier_return_report', 'Ids').'</td></tr>
';

        foreach($models AS $model)
        {
            $report .= '<tr><td style="vertical-align: top;">'.substr($model->date_entered,0,10).'</td><td style="text-align: center; vertical-align: top;">'.$model->content->no.'</td><td style="vertical-align: top;">'.str_replace(self::SEPARATOR, '<br/>', $model->content->ids).'</td></tr>';

            $model->stat = 1;
            $model->date_sent = date('Y-m-d H:i:s');
            $model->update(['stat', 'date_sent']);
        }

        $report .= '</table>';

        $toAddress = $user->email;

        $to = [];
        if ($user->getReturnReportEmail(false))
            $to[] = $user->getReturnReportEmail(false);

        if ($user->getReturnReportEmail2(false))
            $to[] = $user->getReturnReportEmail2(false);

        if ($user->getReturnReportEmail3(false))
            $to[] = $user->getReturnReportEmail3(false);

        if (!S_Useful::sizeof($to))
            $to[] = $toAddress;

        $to[] = 'zwroty@swiatprzesylek.pl';

        S_Mailer::sendToCustomer($to, $to, 'Courier return scans report ('.$user->login.')', $report, false, $lang, true, false, false, false, false, $user->source_domain, false);
    }

}