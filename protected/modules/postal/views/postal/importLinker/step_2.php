<?php
?>
<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'default-data2',
//    'htmlOptions' => ['data-edit-form-id' => $id],
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));
    ?>

    <div id="ebay-form">
        <div style="float: left; width: 48%;">

            <h4><?= Yii::t('courier_ebay', 'Domyślne dane paczki');?></h4>
            <div class="row">
                <?php echo $form->labelEx($package, 'postal_type'); ?>
                <?php echo $form->dropDownList($package, 'postal_type', Postal::getPostalTypeList()); ?>
                <?php echo $form->error($package, 'postal_type'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'size_class'); ?>
                <?php echo $form->dropDownList($package, 'size_class', Postal::getSizeClassList()); ?>
                <?php echo $form->error($package, 'size_class'); ?>  <?php echo TbHtml::tooltip('[!]', '#', Yii::t('postal', 'Będzie automatycznie podwyższona jeżeli waga paczki będzie przekraczać limit dla wybranej kategorii.')); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'target_sp_point'); ?>
                <?php
                $points = SpPoints::getPoints();
                echo $form->dropDownList($package, 'target_sp_point', CHtml::listData($points, 'id' ,'spPointsTr.title')); ?>
                <?php echo $form->error($package, 'target_sp_point'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->label($package, 'value'); ?>
                <?php echo $form->textField($package, 'value', ['maxlength' => 32]);  ?> <?php echo $form->dropDownList($package, 'value_currency', Postal::getCurrencyList());  ?>
                <?php echo $form->error($package, 'value');  ?>
            </div>
        </div>
        <div style="float: right; width: 48%;">
            <h4><?= Yii::t('courier_ebay', 'Dane nadawcy paczek');?></h4>
            <?php
            $this->renderPartial('_addressData/_form',
                array(
                    'model' => $senderAddressData,
                    'form' => $form,
                    'noEmail' => false,
                    'type' => 'sender',
                    'noErrorSummery' => true,
                )
            );
            ?>
        </div>
    </div>

    <div class="navigate" style="clear: both;">
        <input type="submit" value="<?= Yii::t('courier_ebay', 'Dalej');?>" name="save-default-data" class="btn btn-primary"/>
    </div>
    <?php $this->endWidget(); ?>
    <div style="text-align: center;">
        <a class="btn btn-xs" href="<?= Yii::app()->createUrl('/postal/postal/linker');?>"><?= Yii::t('courier_ebay','zacznij od nowa');?></a>
    </div>
</div>



