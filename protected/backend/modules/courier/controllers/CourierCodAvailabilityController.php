<?php

class CourierCodAvailabilityController extends Controller {


    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {

        $model = CourierCodAvailability::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $this->render('view',array(
            'model' => $model,
        ));

    }

    public function actionCreate() {
        $model = new CourierCodAvailability;

//        $this->performAjaxValidation($model, 'courier-country-group-form');

        if (isset($_POST['CourierCodAvailability'])) {
            $model->setAttributes($_POST['CourierCodAvailability']);

            $operatorsInternal = $_POST['operators-internal'];
            if(!is_array($operatorsInternal))
                $operatorsInternal = [];

            if ($model->saveWithOperators($operatorsInternal))
                $this->redirect(array('view', 'id' => $model->id));

        }

        $this->render('create', [
            'model' => $model
                ]
        );
    }

    public function actionUpdate($id) {


        /* @var $model CourierCodAvailability */
        $model = CourierCodAvailability::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        if (isset($_POST['CourierCodAvailability'])) {
            $model->setAttributes($_POST['CourierCodAvailability']);

            $operatorsInternal = $_POST['operators-internal'];
            if(!is_array($operatorsInternal))
                $operatorsInternal = [];

            if ($model->saveWithOperators($operatorsInternal))
                $this->redirect(array('view', 'id' => $model->id));

        }

        $this->render('update', [
                'model' => $model
            ]
        );
    }

    public function actionDelete($id) {

        try
        {
            if (Yii::app()->getRequest()->getIsPostRequest()) {
                $this->loadModel($id, 'CourierCodAvailability')->deleteWithOperators();

                if (!Yii::app()->getRequest()->getIsAjaxRequest())
                    $this->redirect(array('admin'));
            } else
                throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));

        }
        catch (Exception $ex)
        {
            throw new CHttpException(403,'Nie można usunąć tej pozycji!');
        }

    }

    public function actionIndex() {

        $this->redirect(array('courierCodAvailability/admin'));
    }

    public function actionAdmin() {
        $model = new CourierCodAvailability('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierCodAvailability']))
            $model->setAttributes($_GET['CourierCodAvailability']);


        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionListAllGroups()
    {

        $models = CourierCodAvailability::model()->with('countryLists')->findAll();

        $this->render('listAllGroups',array(
            'models' => $models,
        ));
    }

}