<?php

class StatController extends Controller {

    const TYPE_COURIER = 1;
    const TYPE_HM = 2;
    const TYPE_IM = 3;
    const TYPE_POSTAL = 4;

    protected $_type;
    protected $_model;
    protected $_statusList;


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    protected function setTypeAndModel($type, $id)
    {
        if($type == self::TYPE_COURIER)
        {
            Yii::app()->getModule('courier');
            $model = CourierStatHistory::model()->findByPk($id);
            if($model === NULL)
                throw new CHttpException(404);
            else
                $this->_model = $model;

            $this->_statusList = CourierStat::model()->findAll();
        }
        else if($type == self::TYPE_POSTAL)
        {
            Yii::app()->getModule('postal');
            $model = PostalStatHistory::model()->findByPk($id);
            if($model === NULL)
                throw new CHttpException(404);
            else
                $this->_model = $model;

            $this->_statusList = PostalStat::model()->findAll();
        }
//        else if($type == self::TYPE_HM)
//        {
//            Yii::app()->getModule('hybridMail');
//            $model = HybridMailStatHistory::model()->findByPk($id);
//            if($model === NULL)
//                throw new CHttpException(404);
//            else
//                $this->_model = $model;
//
//            $this->_statusList = HybridMailStat::model()->findAll();
//        }
//        else if($type == self::TYPE_IM)
//        {
//            Yii::app()->getModule('internationalMail');
//            $model = InternationalMailStatHistory::model()->findByPk($id);
//            if($model === NULL)
//                throw new CHttpException(404);
//            else
//                $this->_model = $model;
//
//            $this->_statusList = InternationalMailStat::model()->findAll();
//        }
        else
            throw new CHttpException(404);

        $this->_model->attachbehavior('bUniversalStatManager', new bUniversalStatManager);

    }

    public function actionHide($id, $type, $return)
    {
        $this->setTypeAndModel($type, $id);


        if($this->_model->hideStatUSM())
            $this->redirect(base64_decode($return));
    }

    public function actionShow($id, $type, $return)
    {
        $this->setTypeAndModel($type, $id);

        if($this->_model->showStatUSM())
            $this->redirect(base64_decode($return));
    }


    public function actionEdit($id, $type, $return)
    {

        $this->setTypeAndModel($type, $id);

        if($_POST)
        {
            $data = array_shift($_POST);


            $status_date = $data['status_date'];

            $current_stat_id = $data['current_stat_id'];

            $this->_model->status_date = $status_date;
            $this->_model->current_stat_id = $current_stat_id;

            if($this->_model->updateStatUSM())
                $this->redirect(base64_decode($return));

        }

        $this->render('edit', array(
            'model' => $this->_model,
            'statusList' => $this->_statusList,
            'showLocation' => $type == self::TYPE_COURIER ? true : false,
        ));


    }


//    public function actionTest()
//    {
//        Yii::app()->getModule('courier');
//        $models = CourierStatHistory::model()->withHidden()->findAll();
//
//        var_dump(S_Useful::sizeof($models));
//    }

}