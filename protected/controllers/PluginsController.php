<?php

class PluginsController extends Controller
{

    public function actionIndex()
    {

        $this->panelLeftMenuActive = 708;

        $models = Plugins::model()->findAllByAttributes(['stat' => S_Status::ACTIVE]);

        $this->render('index',array(
            'models' => $models,
        ));
    }

    public function actionView($id)
    {
        $this->panelLeftMenuActive = 708;

        $model = Plugins::model()->with('pluginsTr')->findByAttributes(['stat' => S_Status::ACTIVE, 'id' => $id]);

        if($model === NULL)
            throw new CHttpException(404);

        $this->render('view',array(
            'model' => $model,
        ));
    }

}

