<?php


class Pod2FtpController extends CController {

    /**
     * Cron action for downloading ePods from operators and put them in FTP directory
     */
    public static function actionIndex()
    {
        exit;


        $CACHE_NAME = 'pod2ftp';
        $cache = Yii::app()->cache->get($CACHE_NAME);

        MyDump::dump('pod2ftp.txt', "START");
        // ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS

        $cache = false;
        if ($cache !== false) {
            MyDump::dump('pod2ftp.txt', "AIP");
            Yii::app()->end();
        }
        Yii::app()->cache->set($CACHE_NAME, 10, 60 * 5);


        Yii::app()->getModule('courier');

        // run all delivered packages that are not in processed list (or with fail) and save to particular FTP account
        $cmd = Yii::app()->db->createCommand();
        $processed = $cmd->select('item_id, stat, date_entered')->from('pod2ftp')->where('date_entered > (NOW() - INTERVAL :days_back DAY)', [
            ':days_back' => 90

        ])->queryAll();

        $done = [];
        $failed = [];

        $failedDate = [];

        MyDump::dump('pod2ftp.txt', "TO PROCESS: ".S_Useful::sizeof($processed));

        // split results for done and failed
        // for failed, count number of tries
        foreach($processed AS $item)
        {
            if($item['stat'] == 1)
                $done[] = $item['item_id'];
            else
            {
                if(isset($failed[$item['item_id']]))
                    $failed[$item['item_id']] = 0;
                else
                    $failed[$item['item_id']]++;

                if(!isset($failedDate[$item['item_id']]) OR $failedDate[$item['item_id']] < $item['date_entered'])
                    $failedDate[$item['item_id']] = $item['date_entered'];
            }
        }

        $timestampTest = strtotime("-6 hours");
        // if number of failed tries is above the limit, ignore it (so treat as done)
        foreach($failed AS $item_id => $tries)
        {
            if($tries > 6) {
                echo 'za duzo prób';
                echo "\r\n";
                $done[] = $item_id;
            }
            else if(strtotime($failedDate[$item_id]) > $timestampTest) // ignore items checked less than X hours ago
            {
                $done[] = $item_id;
                echo 'sprawdzano ostatnio';
                echo "\r\n";
            }
        }

        if(!S_Useful::sizeof($done))
            $done = [0];

        $done = implode(',', $done);
        $couriers = Courier::model()->findAll('user_id = :user_id AND stat_map_id = :stat_map_id AND id NOT IN ('.$done.') AND date_updated > (NOW() - INTERVAL :days_back DAY) LIMIT 1000', [
            ':user_id' => User::USER_4VALUES,
            ':stat_map_id' => StatMap::MAP_DELIVERED,
            ':days_back' => 90
        ]);


        MyDump::dump('pod2ftp.txt', "TO PROCESS COURIERS: ".S_Useful::sizeof($couriers));

//        $sftpUps = new SftpClient('partner-sftp.izettle.com', 9889, 'ups-aa2gu5', 'Ahpah5ahyeZu8le');
//        $sftpDhl = new SftpClient('partner-sftp.izettle.com', 9889, 'dhl-OaG8u', 'oPheeGhaime6The');

//        $sftpUps = new SftpClient('ftp.jtkprojekt3.nazwa.pl', 22, 'jtkprojekt3_epod', 'Epodtemp2017');
//        $sftpDhl = new SftpClient('ftp.jtkprojekt3.nazwa.pl', 22, 'jtkprojekt3_epod', 'Epodtemp2017');

        $insertArray = [];
        $successCounter = 0;
        $failedCounter = 0;
        /* @var $courier Courier */
        foreach($couriers AS $courier)
        {
            $success = false;
            /* @ var $labels CourierLabelNew[] */
            $labels = $courier->courierLabelNew;

            if(S_Useful::sizeof($labels > 1))
            {
                foreach($labels AS $key => $item)
                {
                    if($item->_internal_mode == CourierLabelNew::INTERNAL_MODE_FIRST)
                    {
                        unset($labels[$key]);
                        break;
                    }
                }
            }

            $label = array_pop($labels);
            if($label && $label->isOperatorWithPod()) {

                $pod = $label->getEpod();
                if($pod) {

                    $name = $courier->ref;
                    if($name == '')
                        $name = $courier->local_id;

                    $filename = $name . '.pdf';


                    $basePath = Yii::app()->basePath . '/_temp/ftp_epod/4values/';
//                    $dir = date('Ymd').'/';
                    $dir = '';

                    if (!file_exists($basePath.'ups/'.$dir)) {
                        mkdir($basePath.'ups/'.$dir, 0777, false);
                    }


                    if (!file_exists($basePath.'dhl/'.$dir)) {
                        mkdir($basePath.'dhl/'.$dir, 0777, false);
                    }

                    if(in_array($label->operator, [
                        CourierLabelNew::OPERATOR_UPS,
                        CourierLabelNew::OPERATOR_UPS_BIS,
                        CourierLabelNew::OPERATOR_UPS_M,
                        CourierLabelNew::OPERATOR_UPS_UK,
                        CourierLabelNew::OPERATOR_UPS_F,
                        CourierLabelNew::OPERATOR_UPS_SAVER,
                        CourierLabelNew::OPERATOR_UPS_SAVER_F,
                        CourierLabelNew::OPERATOR_UPS_SAVER_BIS,
                        CourierLabelNew::OPERATOR_UPS_SAVER_M,
                        CourierLabelNew::OPERATOR_UPS_SAVER_MCM,
                        CourierLabelNew::OPERATOR_UPS_EXPRESS,
                        CourierLabelNew::OPERATOR_UPS_EXPRESS_L,
                        CourierLabelNew::OPERATOR_UPS_EXPRESS_M,
                        CourierLabelNew::OPERATOR_UPS_SAVER_4VALUES,
                    ]))
                    {

                        if(@file_put_contents($basePath.'ups/'.$dir . $filename, $pod))
                            $success = true;

//                        if($sftpUps->sendFile($pod, 'ups-poland/'.$filename))

                    }
                    else if(in_array($label->operator, [
                        CourierLabelNew::OPERATOR_DHLEXPRESS,
                    ]))
                    {
                        if(@file_put_contents($basePath.'dhl/'.$dir . $filename, $pod))
                            $success = true;

//                     if($sftpDhl->sendFile($pod, 'dhl-poland/'.$filename))

                    }

//                    $success = true;


                }
            }

            if($success) {
                $successCounter++;
                $insertArray[] =
                    [
                        'item_id' => $courier->id,
                        'stat' => 1
                    ];
            }
            else {
                $failedCounter++;
                $insertArray[] =
                    [
                        'item_id' => $courier->id,
                        'stat' => 0
                    ];
            }
        }
//
//        $sftpUps->closeConnection();
//        $sftpDhl->closeConnection();

        if(S_Useful::sizeof($insertArray)) {
            $builder = Yii::app()->db->schema->commandBuilder;
            $command = $builder->createMultipleInsertCommand('pod2ftp', $insertArray);
            $command->execute();
        }

        MyDump::dump('pod2ftp.txt', "P ".S_Useful::sizeof($couriers).'; S:'.$successCounter.'; F:'.$failedCounter);

        Yii::app()->end();
    }

}
