<?php

class _PostalStatGridView extends PostalStat
{
    public $_postalsStat;
    public $_map_id;


    public function rules() {

        $array = array(
            array('_postalsStat, _map_id
                ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }


    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;


        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('t.stat', $this->stat);
        $criteria->compare('t.editable', $this->editable);
        $criteria->compare('t.operator_source_id', $this->operator_source_id);
        $criteria->compare('t.notify', $this->notify);
        $criteria->compare('statMapMapping.map_id', $this->_map_id);


        $postal_table = Postal::model()->tableName();
        $postal_count_sql = "(select count(*) from $postal_table cs where cs.postal_stat_id = t.id)";

        $criteria->select = array(
            '*',
            $postal_count_sql . " as postal_count",
        );


        if($this->_postalsStat)
        {
            $criteria->compare($postal_count_sql, $this->_postalsStat);
        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id ASC';
        $sort->attributes = array(
            '_postalsStat' => array(
                'asc' => 'postal_count ASC',
                'desc' => 'postal_count DESC',
            ),
            '*', // add all of the other columns as sortable
        );


        return new CActiveDataProvider($this->with('postalsSTAT')->with('statMapMapping'), array(
            'criteria' => $this->searchCriteria(),
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

    }

}