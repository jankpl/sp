<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Technical break | Przerwa techniczna</title>
    <link rel="stylesheet" type="text/css" href="pause/style.css" />
</head>
<body>
<div>
    <div style="clear: both; text-align: center; padding: 10px;">
        <img src="pause/logo_other.png"/>
    </div>
    <div style="width: 48%; float: left; text-align: right;">
        <h1>Technical break</h1>
        <h3>We'll be back soon...</h3>
    </div>
    <div style="width: 48%; float: right;  text-align: left;">
        <h1>Przerwa techniczna</h1>
        <h3>Zapraszamy wkrótce...</h3>
    </div>
</div>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-52271630-1', 'swiatprzesylek.pl');
    ga('send', 'pageview');
</script>
</body>
</html>


