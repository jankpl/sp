<?php

class HybridMailAdditionListController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'HybridMailAdditionList'),
        ));
    }

    public function actionCreate() {

        $model = new HybridMailAdditionList;
        $model->priceFirstPage = new Price();
        $model->priceNextPage = new Price();
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $additionTr = new HybridMailAdditionListTr();
            $additionTr->language_id = $item->id;
            $modelTrs[$item->id] = $additionTr;
        }

        if (isset($_POST['HybridMailAdditionList'])) {
            $model->setAttributes($_POST['HybridMailAdditionList']);
            $model->priceFirstPage = Price::generateByPost($_POST['PriceValue']['first_page']);
            $model->priceNextPage = Price::generateByPost($_POST['PriceValue']['next_page']);

            foreach($_POST['HybridMailAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new HybridMailAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->hybridMailAdditionListTrs = $modelTrs;

            if ($model->saveWithTrAndPrice()) {
                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,

        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'HybridMailAdditionList');

        $modelTrs = Array();


        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = HybridMailAdditionListTr::model()->find('hybrid_mail_addition_list_id=:hybrid_mail_addition_list_id AND language_id=:language_id', array(':hybrid_mail_addition_list_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $additionTr = new HybridMailAdditionListTr();
                $additionTr->language_id = $item->id;
                $additionTr->hybrid_mail_addition_list_id = $id;
                $modelTrs[$item->id] = $additionTr;
            }
        }


        if (isset($_POST['HybridMailAdditionList'])) {
            $model->setAttributes($_POST['HybridMailAdditionList']);

            $model->priceFirstPage = Price::generateByPost($_POST['PriceValue']['first_page']);
            $model->priceNextPage = Price::generateByPost($_POST['PriceValue']['next_page']);

            foreach($_POST['HybridMailAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new HybridMailAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->hybridMailAdditionListTrs = $modelTrs;


            if ($model->saveWithTrAndPrice()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $this->loadModel($id, 'HybridMailAdditionList')->delete();
            }
            catch(Exception $ex)
            {
                throw new CHttpException(403,'Nie udało się usunąć tego wpisu!');
            }


            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new HybridMailAdditionList('search');
        $model->unsetAttributes();

        if (isset($_GET['HybridMailAdditionList']))
            $model->setAttributes($_GET['HybridMailAdditionList']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionStat($id)
    {
        /* @var $model HybridMailAdditionList */


        $model = HybridMailAdditionList::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}