<?php

class TestCommand extends ConsoleCommand {

    public function actionRun() {

        // echo date('Y-m-d H:i:s');
    }


    public function actionExp100()
    {

        Yii::app()->getModule('courier');

        $period = '2019-05';

        $cmd = Yii::app()->db->createCommand();

        $models = $cmd->select('courier.id')->from('courier')->join('user', 'courier.user_id = user.id')->where('courier.date_entered LIKE :date', [':date' => $period . '%'])->queryColumn();
//


//        $models = array_slice($models, 0,500);

        $courierAttributes = [
            'local_id',
            'date_entered',
            'courier_stat_id',
            'package_weight',
            'package_size_l',
            'package_size_w',
            'package_size_d',
            'packages_number',
            'package_value',
            'package_content',
            'cod',
            'user_id',
            'cod_currency',
            'cod_value',
            'ref',
            'date_delivered',
            'date_attempted_delivery',
            'package_weight_client'
        ];

        $addressDataAttributes = [
            'name',
            'company',
            'country',
            'city',
            'zip_code',
            'address_line_1',
            'address_line_2',
            'tel',
            'email'
        ];

        $addressDataAttributesSender = array_merge($addressDataAttributes, [
            'bankAccount',
        ]);;

        return self::_exportToXls2($models, $courierAttributes, $addressDataAttributes, true, true, $addressDataAttributesSender, false, false, true);


    }

    protected static function _exportToXls2(&$modelsBase, array $courierAttributes, array $addressDataAttributes, $addPackageType = false, $addExternalIds = true, $senderAddressDataAttributes = false, $forUser = false, $returnString = false, $modelsAsDataReaderObj = false)
    {
//         $start = microtime(true);

//        var_dump($modelsBase);

        $k = 1;
        $total = sizeof($modelsBase);

        $name = 'export_'.date('Y-m-d');

        $done = [];

        if(!$senderAddressDataAttributes)
            $senderAddressDataAttributes = $addressDataAttributes;

        // specific fileds for particular courier types
        $specificHeader = [];

//        array_push($specificHeader, 'domestic COD');

        $packageTypeHeader = [];
        if($addPackageType)
        {
            array_push($packageTypeHeader, 'type');
        }

        $packageExternalIdsHeader = [];
        if($addExternalIds)
        {
            array_push($packageExternalIdsHeader, 'external ids');
        }

        $senderHeader = [];
        foreach($senderAddressDataAttributes AS $key => $item)
        {
            $senderAttributes[$key] = 'sender '.$item;
        }

        $receiverHeader = [];
        foreach($addressDataAttributes AS $key => $item)
            $receiverAttributes[$key] = 'receiver '.$item;

        // add status name:
        $courierAttributesWithStat = $courierAttributes;
        $courierAttributesWithStat[] = 'stat name';

        // add status map name && last date:
        $statMapHead = [];
        $statMapHead[] = 'stat map name';
        $statMapHead[] = 'stat map last date';

        if(!$forUser)
            $statMapHead[] = 'salesman';

        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Order ID' : 'note1';
        $arrayHeader[] = (Yii::app()->params['frontend'] && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) ? ' Optional Reference' : 'note2';
        $arrayHeader[] = 'date_activated';
        $arrayHeader[] = 'date_delivered';

        $arrayHeader = array_merge($courierAttributesWithStat, $packageTypeHeader, $senderAttributes, $receiverAttributes, $packageExternalIdsHeader, $specificHeader, $statMapHead);




//        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
//        $cacheSettings = array( 'memoryCacheSize' => '512MB');
//        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

//        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

//        $doc->setActiveSheetIndex(0);
//        $doc->getActiveSheet()->fromArray($arrayHeader, '', 'A1');



//        $spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();
//        $sheet = $spreadsheet->getActiveSheet();

        $filePath =  Yii::app()->basePath.'/../courier_export_'.date('YmdHis').'.xlsx';


//        $sheet->fromArray($arrayHeader, '', 'A1');
        $writer = Box\Spout\Writer\WriterFactory::create(Box\Spout\Common\Type::XLSX); // for XLSX files
//        $writer->openToBrowser('export.xlsx');
        $writer->openToFile($filePath);
        $writer->addRow($arrayHeader);

//        $array = [];
//        $array[0] = $arrayHeader;

        $i = 1;

        $CHUNK_SIZE = 15000;

        if($modelsAsDataReaderObj)
        {
            $modelsContainer = array_chunk($modelsBase, $CHUNK_SIZE);
        } else
            $modelsContainer = [ $modelsBase ];

        unset($modelsBase);

        $i = 0;
//        MyDump::dump('exportOpt2.txt', $CHUNK_SIZE.': START: '.print_r(self::convert(memory_get_usage(true)),1));
        foreach($modelsContainer AS $keyContainer => &$models) {


//            MyDump::dump('exportOpt2.txt', 'CHUNK ('.$keyContainer.'): '.print_r(self::convert(memory_get_usage(true)),1));
            // echo '02: '.round((microtime(true) - $start),4)."\r\n";
            // $start = microtime(true);

            if($modelsAsDataReaderObj)
                $modelsAr = Courier::model()->with('senderAddressData')->with('receiverAddressData')->with('courierTypeInternal')->with('user')->findAllByAttributes(['id' => $models]);

            // echo '03: '.round((microtime(true) - $start),4)."\r\n";
            // $start = microtime(true);
//            $modelsAr = array_slice($modelsAr,10);

            unset($modelsContainer[$keyContainer]);

            foreach ($modelsAr AS $iter => &$model) {
                echo '.';
                if($modelsAsDataReaderObj)
                    $iter = $iter + $CHUNK_SIZE * $keyContainer;

//                // Create AR model with raw array of data
//                if ($modelsAsDataReaderObj) {
//                    $model = Courier::model()->populateRecord($model);
//                }


                //            $model = $models[$iter];

                $packageTypeData = [];
                if ($addPackageType) {
                    array_push($packageTypeData, $model->getTypeName($forUser ? true : false));
                }

                // echo '04: '.round((microtime(true) - $start),4)."\r\n";
                // $start = microtime(true);
                $packageExternalIds = [];
                if ($addExternalIds) {
                    $externals = CourierExternalManager::listExternalServices($model);

                    foreach ($externals AS $external) {
                        array_push($packageExternalIds, CourierExternalManager::getNameForType($external['type']) . ';' . $external['id']);
                    }
                }
                // echo '05: '.round((microtime(true) - $start),4)."\r\n";
                // $start = microtime(true);

                // merge all external ids into one cell
                $packageExternalIds = [implode('||', $packageExternalIds)];

                if ($model->senderAddressData == NULL)
                    $model->senderAddressData = new AddressData;

                if ($model->receiverAddressData == NULL)
                    $model->receiverAddressData = new AddressData;


                $dataCourier = $model->getAttributes($courierAttributes);
                // add status name:
                $dataCourier[] = $model->stat->name;

//            $dataTypeInternal =  array_combine($typeInternalAttributes, array_fill(0,S_Useful::sizeof($typeInternalAttributes),''));
//            if($model->courierTypeInternal != NULL)
//                $dataTypeInternal = $model->courierTypeInternal->getAttributes($typeInternalAttributes);

//            $dataSender = $model->senderAddressData->getAttributes($senderAddressDataAttributes);
                $dataReceiver = $model->receiverAddressData->getAttributes($addressDataAttributes);


                $temp = [];
                foreach ($senderAddressDataAttributes AS $key => $item)
                    $temp['sender_' . $key] = $model->senderAddressData->$item;
                $dataSender = $temp;

                $temp = [];
                foreach ($dataReceiver AS $key => $item)
                    $temp['receiver_' . $key] = $item;
                $dataReceiver = $temp;

                $specificContent = [];
//            if($model->courierTypeDomestic)
//            {
//                array_push($specificContent, $model->courierTypeDomestic->cod_value.' '.$model->courierTypeDomestic->cod_currency);
//            } else {
//                array_push($specificContent, '');
//            }

                // add status map name && last date:
                $statMap = [];
                $statMap[] = $model->statMapName;
                $statMap[] = $model->getLastStatMapUpdate();

//                $array[$i] = array_merge($dataCourier, $packageTypeData, $dataSender, $dataReceiver, $packageExternalIds, $specificContent, $statMap);

                $i++;

                $temp = array_merge($dataCourier, $packageTypeData, $dataSender, $dataReceiver, $packageExternalIds, $specificContent, $statMap);

                if(!$forUser)
                    $temp[] = $model->user ? $model->user->getSalesmanName() : '';

                $temp[] = $model->note1;
                $temp[] = $model->note2;
                $temp[] = $model->date_activated;
                $temp[] = $model->date_delivered;

                $writer->addRow($temp);
                // echo '06: '.round((microtime(true) - $start),4)."\r\n";
                // $start = microtime(true);
//                $doc->getActiveSheet()->fromArray(array_merge($dataCourier, $packageTypeData, $dataSender, $dataReceiver, $packageExternalIds, $specificContent, $statMap), '', 'A'.$i);
//                unset($line);


                $done[$iter] = $model->id;

//                if ($modelsAsDataReaderObj)
//                    unset($model);
//                else
//                    unset($models[$iter]);
                $modelsAr[$iter] = NULL;
                unset($modelsAr[$iter]);
                $model = NULL;
                unset($model);
            }

//            MyDump::dump('exportOpt2.txt', 'CHUNK BEFORE END: '.print_r(self::convert(memory_get_usage(true)),1));
            $modelsAr = NULL;
            unset($modelsAr);
            gc_collect_cycles();
//            MyDump::dump('exportOpt2.txt', 'CHUNK END: '.print_r(self::convert(memory_get_usage(true)),1));
        }
        // echo '07: '.round((microtime(true) - $start),4)."\r\n";
        // $start = microtime(true);

        $modelsContainer = NULL;
        unset($modelsContainer);
//        gc_collect_cycles();
//        MyDump::dump('exportOpt2.txt', 'BEFORE TO XLS: '.print_r(self::convert(memory_get_usage(true)),1));

//        $array = array_chunk($array, 5000);
//
//        $i = 0;
//        foreach($array AS &$chunk)
//        {
//            MyDump::dump('exportOpt2.txt', 'XLS CHUNK START ('.$i.'): '.print_r(self::convert(memory_get_usage(true)),1));
//
//            $sheet->fromArray($chunk, '', 'A'.(1+($i*5000)));
//
////            $doc->getActiveSheet()->fromArray($chunk, '', 'A'.(1+($i*5000)));
//
//            $chunk = NULL;
//            unset($chunk);
        gc_collect_cycles();
//
//            $i++;
//            MyDump::dump('exportOpt2.txt', 'XLS CHUNK END ('.$i.'): '.print_r(self::convert(memory_get_usage(true)),1));
//        }
        // echo '08: '.round((microtime(true) - $start),4)."\r\n";
        // $start = microtime(true);

        $array = NULL;
        unset($array);
//        gc_collect_cycles();
//
//        MyDump::dump('exportOpt2.txt', 'END: '.print_r(self::convert(memory_get_usage(true)),1));

//        $doc->getActiveSheet()->fromArray($array, '');

//        // echo 'ok';
//        exit;


//        $filename = $name . '.xlsx';
//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//        header('Content-Disposition: attachment;filename="' . $filename . '"');
//        header('Cache-Control: max-age=0'); //no cache

//        $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
//        $writer->save('php://output');
        $writer->close();
//        $objWriter->save('php://output');

        echo '!';
        S_Mailer::send('jtk@hss.pl', 'test', 'test zaraz bedzie', 'test', false, NULL, NULL, true);
//        S_Mailer::send('jtk@hss.pl', 'test', 'test', 'test', false, NULL, NULL, true, $filePath, 'exp.xlsx');

//@unlink($filePath);

    }
}