<?php

Yii::import('application.models._base.BaseFaqTr');

class FaqTr extends BaseFaqTr
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),

            array('stat', 'default',  'value'=> S_Status::ACTIVE),

            array('faq_id, language_id, title', 'required'),
            array('faq_id, language_id, stat', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>256),
            array('date_updated, text', 'safe'),
            array('date_updated, text', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, faq_id, language_id, date_entered, date_updated, title, text, stat', 'safe', 'on'=>'search'),
        );
    }

    public function afterSave()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->update('faq',
            array('date_updated' => new CDbExpression('NOW()')),
            'id=:id',
            array(':id' => $this->faq_id));
    }
}