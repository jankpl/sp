<?php

Yii::import('application.modules.courier.models._base.BaseCourierInternalOperatorChange');

class CourierInternalOperatorChange extends BaseCourierInternalOperatorChange
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => true,  'on'=>'update'),
            array('active', 'default', 'value'=> 0, 'on'=>'insert'),
            array('admin_id', 'default', 'value'=> Yii::app()->user->id),

            array('from_operator_id, to_operator_id', 'required'),
            array('admin_id, from_operator_id, to_operator_id, active', 'numerical', 'integerOnly' => true),
            array('id, date_entered, date_updated, admin_id, from_operator_id, to_operator_id, active', 'safe', 'on'=>'search'),
        );
    }


    public static function filterOperator($source)
    {
        $returnModel = false;
        if(is_object($source))
        {
            $returnModel = true;
            $from_operator_id = $source->id;
        }

        $cmd = Yii::app()->db->cache(60)->createCommand();
        $cmd->select('to_operator_id')->from('courier_internal_operator_change')->where('from_operator_id = :from_operator_id AND active = 1', [
            ':from_operator_id' => $from_operator_id
        ]);

        $to_operator_id = $cmd->queryScalar();

        if($to_operator_id)
        {
            if($returnModel)
                return CourierOperator::model()->cache(60)->findByPk($to_operator_id);
            else
                return $to_operator_id;
        }
        else
        {
            return $source; // nothing changes
        }
    }


    public static function listAvailableFromOperators()
    {
        $criteria = new CDbCriteria();
        $criteria->addNotInCondition("id", CHtml::listData(self::model()->findAll(),'from_operator_id', 'from_operator_id'));

        $models = CourierOperator::model()->findAll($criteria);
        return $models;
    }
}