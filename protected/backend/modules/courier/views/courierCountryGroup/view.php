<?php
/* @var $model CourierCountryGroup */
?>

<?php
$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
    array('label'=>'Delete ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
);
?>

    <h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
foreach (Yii::app()->user->getFlashes() as
         $key => $message) {
    echo '<div class="flash-' . $key . '">' .
        $message . '</div>';
}
?>


<h2>Summary</h2>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'name',
        'date_entered',
        'date_updated',
        array(
            'name' => 'pickup_hub',
            'value' => $model->pickupHub,
        ),
        array(
            'name' => 'pickup_operator',
            'value' => $model->pickupOperator,
        ),
        array(
            'name' => 'delivery_hub',
            'value' => $model->deliveryHub,
        ),
        array(
            'name' => 'delivery_operator',
            'value' => $model->deliveryOperator,
        ),
    ),
)); ?>

    <h2><?php echo GxHtml::encode($model->getRelationLabel('countryLists')); ?></h2>
<?php
echo GxHtml::openTag('ul');
foreach($model->countryLists as $relatedModel) {
    echo GxHtml::openTag('li');
    echo GxHtml::encode(GxHtml::valueEx($relatedModel));
    echo GxHtml::closeTag('li');
}
echo GxHtml::closeTag('ul');
?>