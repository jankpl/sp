<?php

/**
 * This is the model base class for the table "courier_import_settings".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CourierImportSettings".
 *
 * Columns in table "courier_import_settings" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property string $name
 * @property string $date_entered
 * @property string $date_updated
 * @property string $data
 * @property integer $user_id
 * @property integer $type
 *
 */
abstract class BaseCourierImportSettings extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'courier_import_settings';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CourierImportSettings|CourierImportSettings', $n);
	}

	public static function representingColumn() {
		return 'name';
	}

	public function rules() {
		return array(
			array('name, date_entered, data', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('date_updated', 'safe'),
			array('date_updated, user_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name, date_entered, date_updated, data, user_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
			'date_entered' => Yii::t('app', 'Date Entered'),
			'date_updated' => Yii::t('app', 'Date Updated'),
			'data' => Yii::t('app', 'Data'),
			'user_id' => Yii::t('app', 'User'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('date_entered', $this->date_entered, true);
		$criteria->compare('date_updated', $this->date_updated, true);
		$criteria->compare('data', $this->data, true);
		$criteria->compare('user_id', $this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}