<?php

class Report_PocztaPolskaFirebusinessSent extends Report
{
    const SEPARATOR = '; ';

    protected static function fillArray(&$array, $key, $value)
    {
        if(!isset($array[$key]))
            $array[$key] = 0;

        $array[$key] += intval($value);
    }

    public static function processReport()
    {
        Yii::app()->getModule('postal');

        $date = date('Y-m');
        $model = self::model()->find('type = :type AND DATE(date_entered) LIKE :date AND stat = 1', [':type' => self::TYPE_POCZTAPOLSKA_FIREBUSINESS_SENT, ':date' => $date.'-%']);

        if($model)
            return false;

        $datePrevious = date('Y-m', strtotime('-1 months'));

        $cmd = Yii::app()->db->createCommand();
        $values = $cmd->select('weight, COUNT(*) AS no')->from('poczta_polska_waiting')->join('postal', 'type_id = postal.id')->where('pp_account_id = :pp_account_id AND poczta_polska_waiting.date_entered LIKE :date AND type = :type', [
            ':pp_account_id' => PocztaPolskaClient::ACC_FIREBUSINESS,
            ':date' => $datePrevious.'-%',
            ':type' => PocztaPolskaWaiting::TYPE_POSTAL,
        ])->group('weight_class')->queryAll();

        $data = [];

        foreach($values AS $value)
        {
            $w = intval($value['weight']);
            if($w <= 50)
                self::fillArray($data, '0-50', $value['no']);
//                $data['0-50'] = $value['no'];
            else if($w <=100)
                self::fillArray($data, '51-100', $value['no']);
//                $data['51-100'] = $value['no'];
            else if($w <= 350)
                self::fillArray($data, '101-350', $value['no']);
//                $data['101-350'] = $value['no'];
            else if($w <= 500)
                self::fillArray($data, '351-500', $value['no']);
//                $data['351-500'] = $value['no'];
            else if($w <= 1000)
                self::fillArray($data, '501-1000', $value['no']);
//                $data['501-1000'] = $value['no'];
            else if($w <= 2000)
                self::fillArray($data, '1001-2000', $value['no']);
//                $data['1001-2000'] = $value['no'];
        }


        $model = new self;
        $model->type = self::TYPE_POCZTAPOLSKA_FIREBUSINESS_SENT;

        $model->date_entered = date('Y-m-d H:i:s');
        $model->date_sent = date('Y-m-d H:i:s');
        $model->content = json_encode($data);
        $model->what_id = 0;


        $report = 'Poczta Polska (piotrgemel@fire-business.com.pl) report'."<br/>";
        $report .= "<br/>";
        $report .= 'Generated on: '.$date.' for previous month';
        $report .= "<br/><br/>";
        $report .= '<table border="1">
<tr style="font-weight: bold;"><td>Weight range [g]</td><td colspan="2">No</td></tr>';

        $total = 0;
        foreach($data AS $range => $value) {
            $report .= '<tr><td style="vertical-align: top;">'.$range.'</td><td style="text-align: center; vertical-align: top;">' . $value . '</td></tr>';
            $total += $value;
        }
        $report .= '<tr><td style="vertical-align: top;">TOTAL</td><td style="text-align: center; vertical-align: top;">' . $total . '</td></tr>';

        $model->stat = 1;

        $model->save();

        $report .= '</table>';


        $to = [
            'jankopec@swiatprzesylek.pl',
            'jbokhorst@kaabnl.nl',
            'abokhorst@kaabnl.nl',
        ];

        S_Mailer::send($to, $to, 'Poczta Polska (piotrgemel@fire-business.com.pl) report', $report, false);
    }

}