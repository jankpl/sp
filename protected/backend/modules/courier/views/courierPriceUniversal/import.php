<?php
/* @var $countriesModels array */
/* @var $countriesModelsOld array */
?>

<h1>Import courier price</h1>

<br/>
<a href="<?= Yii::app()->createUrl('/courier/courierPriceUniversal/import');?>" class="btn btn-danger">Start again</a>
<br/>
<br/>
<br/>

<div class="form">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-routing-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ));
    ?>
    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <hr/>


    <?php
    $error = false;
    foreach($countriesModels AS $country_id => $models):

        $modelsOld = $countriesModelsOld[$country_id];
        $countryModel = CourierCountryGroup::model()->findByPk($country_id);

        ?>
        <div>
            <?php
            //            if($models['M']->hasErrors())
            //                $error = true;

            echo '<h2>'.$countryModel->name.'</h2>';
            ?>


            <?php
            $this->renderPartial('import_item', [
                'form' => $form,
                'models' => $models,
                'modelsOld' => $modelsOld,
                'error' => &$error,
            ]);
            ?>

            <div style="clear: both;"></div>
        </div>
        <hr/>
    <?php
    endforeach;
    ?>



    <?php
    if($error):
        ?>
        <div class="alert alert-danger text-center">You need to correct all errors in file and reupload it!</div>
    <?php
    else:
        ?>

        <div class="alert alert-warning text-center">File is ok! Make sure you know what you are doing!</div>


        <div class="text-center">

            <?= CHtml::dropDownList('confirmation', '', [0 => 'I do not know', 1 => 'I am sure']);?>

            <?php
            $group = CHtml::listData(UserGroup::listGroups(), 'id', 'name');
            $group = [ '-1' => '-- GLOBAL --'] + $group;
            ?>

        </div>
        <br/>
        <br/>
        <br/>
        <div class="text-center">
            <?php echo CHtml::submitButton('Save data', ['class' => 'btn btn-xl btn-danger', 'name' => 'confirm-import']); ?>
        </div>


    <?php
    endif;
    ?>


    <?php
    $this->endWidget();
    ?>

</div>

<script>
    $('[data-old-show]').on('mouseenter', function(){
        $(this).closest('.item').find('[data-old]').show();
    });

    $('[data-old-show]').on('mouseleave', function(){
        $(this).closest('.item').find('[data-old]').hide();
    });
</script>