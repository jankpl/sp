<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
    array('label'=>Yii::t('app', 'Moje konto') , 'url'=>array('manageOwn')),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

    <p>
        You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
    </p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'admin-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'login',
        'email',
        array(
            'name' => 'authority',
            'value' => '$data->authorityName',
            'filter' => Admin::getAuthoritiesNames(),
        ),
        'date_entered',
        array(
            'name' => 'salesman_id',
            'value' => '$data->salesman->name',
            'filter' => CHtml::listData(Salesman::getAllSalesmen(),'id','name'),
        ),
        array(
            'name'=>'stat',
            'value'=>'S_Status::stat($data->stat)',
            'filter'=>CHtml::listData(S_Status::listStats(),'id','name'),
        ),
        [
            'name' => 'login_date',
            'htmlOptions'=> array('style'=>'width:150px'),
        ],
        [
            'name' => 'password_date',
            'htmlOptions'=> array('style'=>'width:150px'),
        ],
        [
            'name' => 'scanning_sp_point_id',
            'value' => '$data->scanningSpPoint ? $data->scanningSpPoint->nameWithId : ""',
            'filter' => CHtml::listData(SpPoints::model()->findAll(),'id', 'nameWithId'),
        ],
        [
            'name' => 'partner_id',
            'value' => 'Partners::getPartnerNameById($data->partner_id)',
            'filter' => Partners::getParntersList(),
        ],
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update} {view} {delete} {status} {status_a}',
            'htmlOptions'=> array('style'=>'width:80px'),
            'buttons'=>array(
                'status'=>array(
                    'visible' => '$data->stat==1',
                    'label'=>'deactivate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_active.png',
                    'url' => 'Yii::app()->createUrl("admin/stat",array("id" => $data->id))',
                ),
                'status_a'=>array(
                    'visible' => '$data->stat==0',
                    'label'=>'activate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_not_active.png',
                    'url' => 'Yii::app()->createUrl("admin/stat",array("id" => $data->id))',
                ),
            ),
        ),
    ),
)); ?>