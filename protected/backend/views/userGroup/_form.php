<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'user-group-form',
	'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->

    <?php
    if(AdminRestrictions::isUltraAdmin()):
        ?>

        <div class="row">
            <?php echo $form->labelEx($model,'admin_id'); ?>
            <?php echo $form->dropDownList($model, 'admin_id', CHtml::listData(Admin::model()->findAll(),'id', 'login'),['prompt' => '-']); ?>
            <?php echo $form->error($model,'admin_id'); ?>
        </div><!-- row -->

    <?php
    endif;
    ?>


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->


