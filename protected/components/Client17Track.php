<?php

class Client17Track
{
    const TOKEN = '792DD774336314C3C27A04BB260CF2CF';
    const URL = 'http://v5-api.17track.net:8044';

    /**
     * @param $no
     * @return bool|GlobalOperatorTtResponseCollection
     */
    protected static function _getTracking($no, $justFirst = false, $justLast = false, $forceOperatorId = false, $source2log = '', $ignoreLimits = false)
    {
        $id = uniqid();

        $CACHE = Yii::app()->cacheFile;
        $CACHE_NAME = '17TRACK_LIMITER_'.$no;

        $limit = $CACHE->get($CACHE_NAME);

        MyDump::dump('17track.txt', $id.': START: '. $no);

//        if($source2log)
//            MyDump::dump('17track.txt', $id.': SOURCE: '. $source2log);

        if($ignoreLimits OR $limit === false)
        {
            if($source2log == 'PostalExtMan')
                $delayTime = 60*60*48;
            else
                $delayTime = 60*60*12;

            $CACHE->set($CACHE_NAME, 10, $delayTime);
        } else {
            MyDump::dump('17track.txt', $id.': LIMITED ('.$source2log.'): '. $no);
            return false;
        }

        $headers = [];
        $headers[] = '17token: '.self::TOKEN;
        $headers[] = 'Connection: close';
        $headers[] = 'Keep-Alive: timeout=150';
        $headers[] = 'Accept-Encoding: gzip, deflate';

        $url = self::URL.'/handlertrack.ashx?nums='.$no;

        if($forceOperatorId)
        {
            if(is_array($forceOperatorId))
                $url .= '&fc='.$forceOperatorId[0];
            else
                $url .= '&sc='.$forceOperatorId[1];
        }

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
                CURLOPT_POST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false
            )

        );

        MyDump::dump('17track.txt', $id.': REQUESTING ('.$source2log.'): '.$no.' : '.print_r($forceOperatorId,1));

        $resp = curl_exec($curl);

        MyDump::dump('17track.txt', $id.': RESPONSE:' .print_r($resp,1));

        $resp = @json_decode($resp);

        if($resp && $resp->msg == 'Ok')
        {
            $statuses = new GlobalOperatorTtResponseCollection();

            $data1 = $resp->dat[0]->track->z1;
            $data2 = $resp->dat[0]->track->z2;

            if($data1 == '' && $data2 == '' && !$forceOperatorId && !$justFirst && !$justLast)
            {
                $yt = @json_decode($resp->dat[0]->yt);

                $forceIds = $yt->d;
                if(!is_array($forceIds))
                    $forceIds = false;

                return self::_getTracking($no, false, false, $forceIds);
            }

            if(!$justLast)
                if(is_array($data1))
                    foreach($data1 AS $item) {
                        $name = $item->z;
                        $date = $item->a;
                        $location =  $item->c;

                        self::filterStatus($name, $location);

                        $statuses->addItem($name, $date, $location);
                    }

            if(!$justFirst)
                if(is_array($data2))
                    foreach($data2 AS $item) {
                        $name = $item->z;
                        $date = $item->a;
                        $location = $item->c;

                        self::filterStatus($name, $location);

                        $statuses->addItem($name, $date, $location);
                    }


            return $statuses;
        }

        return false;
    }

    protected static function filterStatus(&$name, &$location)
    {
        if(preg_match('/Deliver item, Signed By: (.*)/i', $name, $matches)) {
            $name = 'Deliver item';
            $location = trim($location . ' (Signed By: '.trim($matches[1]).')');
        }
        else if(preg_match('/【(.*)】 S.F. Express has picked up the shipment/iu', $name, $matches)) {
            $name = 'S.F. Express has picked up the shipment';
            $location = trim($matches[1]);
        }
        else if(preg_match('/【(.*)】 Shipment transiting to next station/iu', $name, $matches)) {
            $name = 'Shipment transiting to next station';
            $location = trim($matches[1]);
        }
        else if(preg_match('/【(.*)】Failed to send shipments out as the shipper\'s reason/iu', $name, $matches)) {
            $name = 'Failed to send shipments out as the shipper\'s reason';
            $location = trim($matches[1]);
        }
        else if(preg_match('/【(.*)】 Shipment ready for leaving the warehouse./iu', $name, $matches)) {
            $name = 'Shipment ready for leaving the warehouse';
            $location = trim($matches[1]);
        }
        else if(preg_match('/【(.*)】 Shipment arrive at (.*) distribution center/iu', $name, $matches)) {
            $name = 'Shipment arrive at distribution center';
            $location = trim($matches[1]);
        }
        else if(preg_match('/Available for Pickup -> Your item arrived at the (.*) at  and is ready for pickup./i', $name, $matches)) {
            $name = 'Available for Pickup -> Your item arrived and is ready for pickup.';
            $location = trim($matches[1]);
        }
        else if(preg_match('/Delivery success, (.*)/i', $name, $matches)) {
            $name = 'Delivery success';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/Departed USPS Regional Facility,Your item departed our USPS facility in (.*) on (.*). The item is currently in transit to the destination./i', $name, $matches)) {
            $name = 'Departed USPS Regional Facility,Your item departed our USPS facility. The item is currently in transit to the destination.';
            $location = trim($matches[1]);
        }
        else if(preg_match('/Your item departed our USPS facility in (.*) on (.*). The item is currently in transit to the destination./i', $name, $matches)) {
            $name = 'Your item departed our USPS facility. The item is currently in transit to the destination.';
            $location = trim($matches[1]);
        }
        else if(preg_match('/Delivery failure, Addressee not at address indicated; Addressee\'s office close, Addressee contacted - awaiting reply (.*)/i', $name, $matches)) {
            $name = 'Delivery failure, Addressee not at address indicated; Addressee\'s office close, Addressee contacted - awaiting reply';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/DISPATCH LOADED \(--> (.*)\)/i', $name, $matches)) {
            $name = 'DISPATCH LOADED';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/DISPATCH CLOSED \(--> (.*)\)/i', $name, $matches)) {
            $name = 'DISPATCH CLOSED';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/Delivered -> Your item was delivered at (.*) in (.*)/i', $name, $matches)) {
            $name = 'Delivered -> Your item was delivered';
            $location = '('.trim($matches[2]).')';
        }
        else if(preg_match('/Sūtījums nosūtīts uz Piegādes punktu: (.*)/iu', $name, $matches)) {
            $name = 'Sūtījums nosūtīts uz Piegādes punktu';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/Departed from Airport to destination country, (.*)/i', $name, $matches)) {
            $name = 'Departed from Airport to destination country';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/Departed from Carrier, (.*)/i', $name, $matches)) {
            $name = 'Departed from Carrier';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/【(.*)】 Shipment arrive at the warehouse\./iu', $name, $matches)) {
            $name = 'Shipment arrive at the warehouse.';
            $location = trim($matches[1]);
        }
        else if(preg_match('/Arrived at the first mile sorting center, (.*)/i', $name, $matches)) {
            $name = 'Arrived at the first mile sorting center';
            $location = trim($matches[1]);
        }
        else if(preg_match('/Arrived at Carrier, (.*)/i', $name, $matches)) {
            $name = 'Arrived at Carrier';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/Picked up, (.*)/i', $name, $matches)) {
            $name = 'Picked up';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/Sūtījums saņemts, izdrukāts un piegādāts informatīvs paziņojums Piegādes punktā: (.*)/iu', $name, $matches)) {
            $name = 'Sūtījums saņemts, izdrukāts un piegādāts informatīvs paziņojums Piegādes punktā';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/从机场发往目的国, (.*)/iu', $name, $matches)) {
            $name = '从机场发往目的国';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/从物流服务商处发出, (.*)/iu', $name, $matches)) {
            $name = '从物流服务商处发出';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/离开头程分拣中心, (.*)/iu', $name, $matches)) {
            $name = '离开头程分拣中心';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/到达头程分拣中心, (.*)/iu', $name, $matches)) {
            $name = '到达头程分拣中心';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('/到达物流服务商处, (.*)/iu', $name, $matches)) {
            $name = '到达物流服务商处';
            $location = '('.trim($matches[1]).')';
        }
        else if(preg_match('%Votre colis sera présenté à votre adresse demain (ou le prochain jour ouvré en cas de dimanche ou jour férié). Vous pouvez reprogrammer votre livraison ! Choisissez une date ou un point de retrait en vous rendant sur www.laposte.fr/particulier/modification-livraison.%iu', $name, $matches)) {
            $name = 'Votre colis sera présenté à votre adresse demain (ou le prochain jour ouvré en cas de dimanche ou jour férié). Vous pouvez reprogrammer votre livraison ! Choisissez une date ou un point de retrait en vous rendant.';
            $location = '(www.laposte.fr/particulier/modification-livraison.)';
        }
        else if(preg_match('%Votre colis n\'a pas pu vous être remis. Vous pouvez reprogrammer votre livraison ! Choisissez une date ou un point de retrait en vous rendant sur www.laposte.fr/particulier/modification-livraison.%iu', $name, $matches)) {
            $name = 'Votre colis n\'a pas pu vous être remis. Vous pouvez reprogrammer votre livraison ! Choisissez une date ou un point de retrait en vous rendant.';
            $location = '(www.laposte.fr/particulier/modification-livraison)';
        }
    }

    public static function getTt($no, $forceOperatorId = false, $justFirst = false, $justLast = false, $source2log = '', $ignoreLimits = false)
    {
        $statuses = self::_getTracking($no, $justFirst, $justLast, $forceOperatorId, $source2log, $ignoreLimits);
        if(!$statuses)
            return false;

        return $statuses->adapterToCourierExternalManagerArray();
    }

    public static function getTtPostal($no, $justFirst = false, $justLast = false, $source2log = '')
    {
        $statuses = self::_getTracking($no, $justFirst, $justLast, false, $source2log);
        if(!$statuses)
            return false;

        return $statuses->adapterToPostalExternalManagerArray();

    }

    public static function getTtGlobalBrokers($no, $justFirst = false, $justLast = false, $forceOperatorId = false, $source2log = '')
    {
        return self::_getTracking($no, $justFirst, $justLast, $forceOperatorId, $source2log);

    }

}