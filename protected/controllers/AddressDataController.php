<?php

class AddressDataController extends Controller
{

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }


    public function actionAjaxGetAddressData()
    {

        $id = $_POST['id'];

        /* @var $model User */
        $model = AddressData::model()->findByPk($id);

        echo CJSON::encode($model->attributes);
    }


}