<?php
/* @var $attrBaseName String */
?>
<div class="addressDataSwitch" style="" address-data-switch="<?= $attrBaseName;?>" title="<?= Yii::t('site', 'Zamień dane adresowe');?>">
    <i class="fa fa-exchange" aria-hidden="true"></i>
</div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/addressDataSwitch.js', CClientScript::POS_END);