<?php

class DebtCronController extends CController {


    public function filters()
    {
        return array();
    }

    public function actionIndex()
    {
        $smsApi = Yii::createComponent('application.components.SmsApi');

        /* @var $invoices UserInvoice[] */
        $invoices = UserInvoice::model()->outdated()->active()->findAll();
        $invoicesByUser = [];

        foreach($invoices AS $invoice)
        {
            if(!isset($invoicesByUser[$invoice->user_id]))
                $invoicesByUser[$invoice->user_id] = [];

            $invoicesByUser[$invoice->user_id][] = $invoice;
        }

        /* @var $userInvoices int[] */
        /* @var $item UserInvoice */
        foreach($invoicesByUser AS $user_id => $userInvoices)
        {
            $userModel = User::model()->findByPk($user_id);

            if($userModel->getParentAccountingUser())
                continue; // check only parent accounting user

            $minDays = -1;
            $maxDays = 0;
            $totalDebt = [];
            $totalDebt[Currency::PLN_ID] = 0;
            $totalDebt[Currency::EUR_ID] = 0;
            $totalDebt[Currency::GBP_ID] = 0;

            $debtList = [];

            $overdueInvNo = [];

            foreach($userInvoices AS $item)
            {
                $overdueDays = $item->getOverdueDays();

                if($overdueDays && in_array($item->stat, [UserInvoice::PUBLISHED, UserInvoice::SEEN]) && $item->stat_inv == UserInvoice::INV_STAT_NONE)
                {
                    if($overdueDays < $minDays OR $minDays == -1)
                        $minDays = $overdueDays;

                    if($overdueDays > $maxDays)
                        $maxDays = $overdueDays;

                    $diff = $item->inv_value - $item->paid_value;

                    $totalDebt[$item->inv_currency] += $diff;

                    $debtList[] = '<strong>'.$item->inv_value.' '.Currency::nameById($item->inv_currency).'</strong> : '.$item->inv_payment_date.' : '.$item->title;

                    $overdueInvNo[] = $item->title;
                }
            }

            $blockStat = $userModel->getDebtBlock();

            if($minDays >= 2 && $blockStat != DebtBlock::STAT_NEVER_DONT_NOTIFY)
            {
                $totalValues = [];
                foreach($totalDebt AS $curr => $value)
                    $totalValues[] = S_Price::formatPrice($value).' '.Currency::nameById($curr);

                $totalValues = implode('|', $totalValues);

                $lang = $userModel->getPrefLang();

                $tempLangChange = new TempLangChange();
                $tempLangChange->temporarySetLanguage($lang);

                $debtListText = "<br/>";
                foreach($debtList AS $debt)
                    $debtListText .= $debt."<br/>";

                $mails = [];
                if($userModel->getAccountingEmail(false))
                    $mails[] = $userModel->getAccountingEmail(false);

                if($userModel->getAccountingEmail2(false))
                    $mails[] = $userModel->getAccountingEmail2(false);

                if($userModel->getAccountingEmail3(false))
                    $mails[] = $userModel->getAccountingEmail3(false);

                if(!S_Useful::sizeof($mails))
                    $mails[] = $userModel->email;

                /* @var $userModel User */
                if(!DebtCollectionNotification::hasMailBeenSent($userModel->id) && in_array($userModel->source_domain, [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS]))
                {

                    foreach($mails AS $mail)
                        S_Mailer::sendToCustomer($mail,$userModel->name, Yii::t('debtNotification','Masz zaległości na swoim koncie'), Yii::t('debtNotification', 'Na Twoim koncie istnieją nieopłacone faktury z przekroczonym terminem!{br}Prosimy o ich uregulowanie.{br}{br}Lista zaległości:{list}', array('{br}' => '<br/>', '{list}' => $debtListText)), true, $lang, true, false, false, false, false, $userModel->source_domain);

                    MyDump::dump('debtCron.txt', 'SEND MAIL - USER: '.$userModel->login.' ('.$userModel->id.') : '.print_r($mails,1));
                    DebtCollectionNotification::mailSent($userModel->id);
                }

                if($minDays == DebtBlock::SMS_DAY) {

                    $smsContent = Yii::t('debtNotification', 'Masz zaległości na koncie na łączną kwotę {n}. Największe przekroczenie terminu: {d} dni', ['{n}' => $totalValues, '{d}' => $maxDays]);

                    $tel = false;
                    if($userModel->getAccountingTel())
                        $tel = $userModel->getAccountingTel();
                    else if($userModel->tel)
                        $tel = $userModel->tel;

                    if(!DebtCollectionNotification::hasSmsBeenSent($userModel->id) && in_array($userModel->source_domain, [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS]))
                    {
                        if($tel)
                            $smsApi->sendSms($tel, $smsContent, $userModel->source_domain);

                        MyDump::dump('debtCron.txt', 'SEND SMS - USER: '.$userModel->login.' ('.$userModel->id.') : '.print_r($tel,1));
                        DebtCollectionNotification::smsSent($userModel->id);
                    }
                }

                $tempLangChange->revertToBaseLanguage();
            }

            // LOCK ACCOUNT:
            $blockDays = $userModel->getDebtBlockDays(true);

            if($blockStat == DebtBlock::STAT_NONE && $maxDays >= $blockDays)
            {
                $userModel->setDebtBlock(DebtBlock::STAT_BLOCK, true);

                UserLog::addToLog('Account locked by CRON - overdue invoices: '.implode(', ', $overdueInvNo), $userModel->id, true);

                MyDump::dump('debtCron.txt', 'BLOCK - USER: '.$userModel->login.' ('.$userModel->id.')');
            }

        }

        // TRY TO UNLOCK ACCOUNTS
        $user_ids = DebtBlock::listLockedUserIds();
        foreach($user_ids AS $user_id)
            DebtBlock::tryToUnlockAccount($user_id, true);


    }

}