<?php

class S_Timer
{
    protected $start;
    protected $last;

    public function init()
    {
        $this->start = microtime(true);
        $this->last = $this->start;

        echo 'START'."\r\n<br/>";
    }

    public function step($name = false)
    {
        $now = microtime(true);
        $sinceStart = $now - $this->start;
        $sinceLast = $now - $this->last;

        $this->last = $now;

        echo 'STEP "'.$name.'" : +'.str_pad(round($sinceLast,2), 5, '0',STR_PAD_LEFT).' (+'.str_pad(round($sinceStart,2), 5, '0',STR_PAD_LEFT).')'."\r\n<br/>";
    }

}