<div class="form">

    <?php echo $form->hiddenField($model, '['.$i.']id'); ?>
    <?php echo $form->hiddenField($model, '['.$i.']faq_id'); ?>
    <?php echo $form->hiddenField($model, '['.$i.']language_id'); ?>


    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']title'); ?>
        <?php echo $form->textField($model, '['.$i.']title', array('maxlength' => 256)); ?>
        <?php echo $form->error($model,'['.$i.']title'); ?>
    </div><!-- row -->
    <?php
    /*
    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']intro'); ?>
        <?php
        $this->widget('ext.editMe.widgets.ExtEditMe', array(
            'model'=>$model,
            'attribute'=>'['.$i.']intro',
            'resizeMode' => 'vertical',
            'width' => '580',
            'toolbar' => array(
                array(
                    'NewFaq', 'Preview',
                ),
                array(
                    'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',
                ),
                array(
                    'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat',
                ),
                array(
                    'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Table', 'HorizontalRule','SpecialChar', 'Font',  'FontSize', 'TextColor',
                ),
            ),
            'advancedTabs' => false,
            'ckeConfig' => array(
                'forcePasteAsPlainText' => true,
            )
        ));
        ?>
        <?php echo $form->error($model,'['.$i.']intro'); ?>
    </div><!-- row -->
        <?php
    */
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']text'); ?>
        <?php
        $this->widget('ext.editMe.widgets.ExtEditMe', array(
            'model'=>$model,
            'attribute'=>'['.$i.']text',
            'resizeMode' => 'vertical',
            'width' => '580',
            'toolbar' => array(
                array(
                    'NewFaq', 'Preview',
                ),
                array(
                    'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',
                ),
                array(
                    'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat',
                ),
                array(
                    'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Table', 'HorizontalRule','SpecialChar', 'Font',  'FontSize', 'TextColor',
                ),
            ),
            'advancedTabs' => false,
            'ckeConfig' => array(
                'forcePasteAsPlainText' => true,
            )
        ));
        ?>
        <?php echo $form->error($model,'['.$i.']text'); ?>
    </div><!-- row -->


</div><!-- form -->