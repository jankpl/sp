<?php
class TtMap extends CWidget
{

    public $location;
    public $send_location;
    public $receiver_location;
    public $stat_map;

    public function run()
    {
        $location = trim($this->location);
        $location = strip_tags($location);
        $location = CHtml::encode($location);

//        if($location)
            $this->render('ttMap', array('location' => $location, 'send_location' => $this->send_location,  'receiver_location' => $this->receiver_location, 'stat_map' => $this->stat_map));
    }
}
?>