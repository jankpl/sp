<?php

/* @var $model CourierTypeExternalReturn */
?>

<h1>Create <?php echo GxHtml::encode($model->label()); ?></h1>
<div class="form">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ));
    ?>



    <div style="overflow: auto;">
        <div style="width: 32%; float: left;">
            <h4>Package details</h4>

            <div class="row">
                <?php echo $form->labelEx($model->courier,'user_id'); ?>
                <?php echo $form->dropDownList($model->courier, 'user_id', CHtml::listData(User::model()->findAll(), 'id', 'login'), ['prompt' => '-', 'data-chosen-widget' => 'true']); ?>
                <?php echo $form->error($model->courier,'user_id'); ?>
            </div><!-- row -->

            <div class="row">
                <?php echo $form->labelEx($model->courier,'package_weight'); ?>
                <?php echo $form->textField($model->courier, 'package_weight', array('maxlength' => 13)); ?>
                <?php echo $form->error($model->courier,'package_weight'); ?>
            </div><!-- row -->

            <div class="row">
                <?php echo $form->labelEx($model->courierLabelNew,'track_id'); ?>
                <?php echo $form->textField($model->courierLabelNew, 'track_id', array('maxlength' => 128)); ?>
                <?php echo $form->error($model->courierLabelNew,'track_id'); ?>
            </div><!-- row -->

            <div class="row">
                <?php echo $form->labelEx($model->courierLabelNew,'_ext_operator_name'); ?>
                <?php echo $form->textField($model->courierLabelNew, '_ext_operator_name', array('maxlength' => 32)); ?>
                <?php echo $form->error($model->courierLabelNew,'_ext_operator_name'); ?>
            </div><!-- row -->


        </div>

        <div style="width: 32%; float: right;">
            <h4>Return FROM</h4>
            <?php

            $this->widget('application.backend.components.AddressDataFrom', array(
                'addressData' => $model->courier->senderAddressData,
//            'listAddressData' => $listAddressData,
                'form' => $form,
//        'noEmail' => true,
                'groupName' => 'sender',
            ));

            ?>

        </div>

        <div style="width: 32%; float: right;">
            <h4>Return TO</h4>
            <?php

            $this->widget('application.backend.components.AddressDataFrom', array(
                'addressData' => $model->courier->receiverAddressData,
//            'listAddressData' => $listAddressData,
                'form' => $form,
//        'noEmail' => true,
                'groupName' => 'receiver',
            ));

            ?>

        </div>
    </div>


    <hr/>

    <div style="text-align: center;">
        <?php echo TbHtml::submitButton('Create', array('name' => 'save', 'size' => TbHtml::BUTTON_SIZE_LARGE)); ?>
        <?php $this->endWidget();
        ?>
    </div>
</div>

<?php

$script = "
     $('[data-chosen-widget]').chosen({allow_single_deselect:true});
        $('.chosen-drop').css('width', '300px');
        $('[data-chosen-widget]').hide();
    ";
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.jquery.min.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.min.css');
Yii::app()->clientScript->registerScript('chosen-widget', $script, CClientScript::POS_READY);