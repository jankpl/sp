<?php

class AddNewOAController extends Controller
{

    public function beforeAction($action)
    {
        if(Yii::app()->user->isGuest OR Yii::app()->user->id != 8)
            exit;

        return parent::beforeAction($action);
    }


    public function actionAddNewOperatorsToDb()
    {


        Yii::app()->getModule('courier');
        Yii::app()->getModule('postal');

        $internal = true;
        $u = true;
        $postal = false;


        $broker_id = GLOBAL_BROKERS::BROKER_YUNEXPRESS_SYNCH;
        $partner_id = Partners::PARTNER_SP;
        $stat = 1;

        $data = [];

        $data[] = [
            'name' => 'DHL DE Paket YE',
            'uniq_id' => GLOBAL_BROKERS::OPERATOR_UNIQID_YUNEXPRESS_SYNCHR_DHLDE,
        ];
//
//        $data[] = [
//            'name' => 'Yodel Minipack 24 _SP',
//            'uniq_id' => GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_YODEL_MINIPACK24,
//        ];
//
//        $data[] = [
//            'name' => 'Yodel Minipack 48 _SP',
//            'uniq_id' => GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_YODEL_MINIPACK48
//        ];
//
//        $data[] = [
//            'name' => 'Yodel Home 24 _SP',
//            'uniq_id' => GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_YODEL_HOME24,
//        ];
//
//        $data[] = [
//            'name' => 'Yodel Home 24 POD _SP',
//            'uniq_id' => GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_YODEL_HOME24POD
//        ];
//
//        $data[] = [
//            'name' => 'UPS DropOff _SP',
//            'uniq_id' => GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_UPS_DROPOFF,
//        ];
//
//        $data[] = [
//            'name' => 'Huxloe Hermes DropOff _SP',
//            'uniq_id' => GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_HUXLOE_HERMES_DROPOFF,
//        ];
//
//        $data[] = [
//            'name' => 'Huxloe Hux Standard _SP',
//            'uniq_id' => GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_HUXLOE_HUX_STANDARD,
//        ];

        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;
        try {
            if ($internal) {

                foreach($data AS $key => $operatorItem)
                {
                    $model = new CourierOperator();
                    $model->broker = $broker_id;
                    $model->name = $operatorItem['name'];
                    $model->stat = $stat;
                    $model->partner_id = $partner_id;
                    if(isset($operatorItem['uniq_id']))
                        $model->uniq_id = $operatorItem['uniq_id'];

                    if(!$model->save())
                        $errors = true;

                    var_dump($model->getErrors());

                }

            }


            if ($u) {

                foreach($data AS $key => $operatorItem)
                {
                    $model = new CourierUOperator();
                    $model->_disable_inner_transaction = true;
                    $model->broker_id = $broker_id;
                    $model->name = $operatorItem['name'];
                    $model->name_customer = $model->name;
                    $model->stat = $stat;
                    $model->partner_id = $partner_id;
                    if(isset($operatorItem['uniq_id']))
                        $model->uniq_id = $operatorItem['uniq_id'];
                    $model->has_dim_weight = 0;

                    if(!$model->save())
                        $errors = true;

                    var_dump($model->getErrors());

                }

            }



            if ($postal) {

                foreach($data AS $key => $operatorItem)
                {
                    $model = new PostalOperator();
                    $model->broker_id = $broker_id;
                    $model->name = $operatorItem['name'];
                    $model->label_mode = PostalOperator::LABEL_MODE_WHOLE_EXTERNAL;
                    $model->stat = $stat;
                    if(isset($operatorItem['uniq_id']))
                        $model->uniq_id = $operatorItem['uniq_id'];

                    if(!$model->save())
                        $errors = true;

                    var_dump($model->getErrors());

                }

            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            var_dump($ex);
            exit;
        }

        if(!$errors)
        {
            $transaction->commit();
            echo 'jest ok!';
            exit;
        }

        $transaction->rollback();
        echo 'błąd...';

    }


    public function actionAddNewAdditionsToDb()
    {
        Yii::app()->getModule('courier');


        $internal = true;
        $u = false;

//        $broker_id = GLOBAL_BROKERS::BROKER_APACZKA;
        $broker_id = CourierOperator::BROKER_DPDPL;

//        $data[CourierUAdditionList::UNIQID_RUCH_INSURANCE] = [
//            'name' => 'Ruch - Ubezpieczenie',
////            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_10,
//        ];

        $data[GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_CUD] = [
            'name' => 'DPD PL - CUD',
            'group' => NULL,
        ];

        $data[GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_IN_PERS] = [
            'name' => 'DPD PL - In Person',
            'group' => NULL,
        ];

        $data[GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_PRIV_PERS] = [
            'name' => 'DPD PL - Private Person',
            'group' => NULL,
        ];

        $data[GLOBAL_BROKERS::ADDITION_UNIQID_DPDPL_DOX] = [
            'name' => 'DPD PL - DOX',
            'group' => NULL,
        ];

//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ROD] = [
//            'name' => 'Raben - ROD',
//            'group' => NULL,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ISMS] = [
//            'name' => 'Raben - ISMS',
//            'group' => NULL,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_ADVISE] = [
//            'name' => 'Raben - Advise',
//            'group' => NULL,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_MANUAL_LOADING] = [
//            'name' => 'Raben - Manual loading',
//            'group' => NULL,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8] = [
//            'name' => 'Raben - Delivery 8',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_10] = [
//            'name' => 'Raben - Delivery 10',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_12] = [
//            'name' => 'Raben - Delivery 12',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_SAT] = [
//            'name' => 'Raben - Delivery Saturday',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_NEXT_DAY] = [
//            'name' => 'Raben - Delivery Next Day',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_SPECIAL] = [
//            'name' => 'Raben - Delivery Special',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8,
//        ];
//
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_NIGHT] = [
//            'name' => 'Raben - Delivery night',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_8,
//        ];
//
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_RABEN_DELIVERY_WITH_LIFT] = [
//            'name' => 'Raben - Delivery with lift',
//            'group' => null,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_SIZE_C] = [
//            'name' => 'Inpost - Gabaryt C',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_SIZE_B,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_1000] = [
//            'name' => 'Inpost - Insurance 1000',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_1000,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_5000] = [
//            'name' => 'Inpost - Insurance 5000',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_1000,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_10000] = [
//            'name' => 'Inpost - Insurance 10 000',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_1000,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_20000] = [
//            'name' => 'Inpost - Insurance 20 000',
//            'group' => GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_1000,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_ROD] = [
//            'name' => 'Inpost - Insurance ROD',
//            'group' => '',
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_NOTIFICATION_EMAIL] = [
//            'name' => 'Inpost - Email notification',
//            'group' => '',
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_NOTIFICATION_SMS] = [
//            'name' => 'Inpost - SMS notification',
//            'group' => '',
//        ];

//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_SMS_NOTIFICATION] = [
//            'name' => 'Direct Link - SMS Notification',
//            'group' => '',
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_DIRECT_LINK_CHANGE_DELIVERY_ADDRESS] = [
//            'name' => 'Direct Link - Change delivery address',
//            'group' => '',
//        ];


//        $data = [];
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_EMAIL_NOTIFICATION] = [
//            'name' => 'Secured Mail - Email Notification',
//            'group' => '',
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_SMS_NOTIFICATION] = [
//            'name' => 'Secured Mail - SMS Notification',
//            'group' => '',
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_INSURANCE] = [
//            'name' => 'Secured Mail - Insurance',
//            'group' => '',
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_SATURDAY_DELIVERY] = [
//            'name' => 'Secured Mail - Saturday Delivery',
//            'group' => '',
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_SIGNED_FOR] = [
//            'name' => 'Secured Mail - POD Signed For',
//            'group' => 8004,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_TRACKED] = [
//            'name' => 'Secured Mail - POD Tracked',
//            'group' => 8004,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_SECURED_MAIL_POD_TRACKED_SIGNED_FOR] = [
//            'name' => 'Secured Mail - POD Tracked & Signed For',
//            'group' => 8004,
//        ];
////
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_AEROSOL] = [
//            'name' => 'Whistl - aerosol',
//            'group' => NULL,
//        ];
//
//        $data[GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_POD] = [
//            'name' => 'Whistl - POD',
//            'group' => NULL,
//        ];



        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;
        try {
            if ($internal) {

                foreach($data AS $uniq_id => $additionItem)
                {
                    $model = new CourierAdditionList();
                    $model->name = $additionItem['name'];
                    $model->uniq_id = $uniq_id;
                    $model->stat = 1;
                    $model->broker_id = $broker_id;
                    $model->group = $additionItem['group'];

                    $price = new Price();
                    if(!$price->save())
                        $errors = true;

                    $modelTrs = [];

                    foreach(Language::model()->findAll('stat = 1') AS $lang)
                    {
                        $additionTr = new CourierAdditionListTr();
                        $additionTr->language_id = $lang->id;
                        $additionTr->title =  $model->name;
                        $additionTr->description = $model->name;
                        $modelTrs[$lang->id] = $additionTr;
                    }


                    /* @var $currency PriceCurrency */
                    foreach(PriceCurrency::model()->findAll() AS $currency)
                    {
                        $priceValue = new PriceValue();
                        $priceValue->currency_id = $currency->id;
                        $priceValue->currency_name = $currency->name;
                        $priceValue->value = 0;
                        $priceValue->price_id = $price->id;

                        if(!$priceValue->save())
                            $errors = true;
                    }

                    $model->price_id = $price->id;
                    $model->courierAdditionListTrs = $modelTrs;

                    if(!$model->save())
                        $errors = true;

                    foreach($model->courierAdditionListTrs AS $item)
                    {
                        $item->courier_addition_list_id = $model->id;
                        if(!$item->save()) {
                            $errors = true;
                        }
                    }

                }


            }


            if ($u) {

                foreach($data AS $uniq_id => $additionItem)
                {
                    $model = new CourierUAdditionList();
                    $model->name = $additionItem['name'];
                    $model->uniq_id = $uniq_id;
                    $model->stat = 1;
                    $model->broker_id = $broker_id;
                    $model->group = $additionItem['group'];

                    $price = new Price();
                    if(!$price->save())
                        $errors = true;

                    $modelTrs = [];

                    foreach(Language::model()->findAll('stat = 1') AS $lang)
                    {
                        $additionTr = new CourierUAdditionListTr();
                        $additionTr->language_id = $lang->id;
                        $additionTr->title =  $model->name;
                        $additionTr->description = $model->name;
                        $modelTrs[$lang->id] = $additionTr;
                    }


                    /* @var $currency PriceCurrency */
                    foreach(PriceCurrency::model()->findAll() AS $currency)
                    {
                        $priceValue = new PriceValue();
                        $priceValue->currency_id = $currency->id;
                        $priceValue->currency_name = $currency->name;
                        $priceValue->value = 0;

                        $priceValue->price_id = $price->id;

                        if(!$priceValue->save())
                            $errors = true;
                    }


                    $model->price_id = $price->id;
                    $model->courierUAdditionListTrs = $modelTrs;

                    if(!$model->save())
                        $errors = true;

                    foreach($model->courierUAdditionListTrs AS $item)
                    {
                        $item->courier_u_addition_list_id = $model->id;
                        if(!$item->save()) {
                            $errors = true;
                        }
                    }

                }

            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            var_dump($ex);
            exit;
        }


        if(!$errors)
        {
            $transaction->commit();
            echo 'jest ok!';
            exit;
        }

        $transaction->rollback();
        echo 'błąd...';


    }

}
