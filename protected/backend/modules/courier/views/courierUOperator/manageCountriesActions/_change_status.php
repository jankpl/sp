<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'change-status',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courierUOperator/manageCouriersChangeStatByGV', ['operator_id' => $operator_id]),
    'htmlOptions' => [
        'class' => 'confirm-me',
    ]
));
?>


<?php echo CHtml::dropDownList('ChangeStat[stat]',NULL, CourierUOperator2CountryGroup::getStatList()); ?>
<br/>
<?php
echo TbHtml::submitButton('Set', array(
    'onClick' => 'js:
    $(".gv-manage-countries-ids").val($("#gv-manage-countries").selGridView("getAllSelection").toString());
   ',
    'name' => 'Ids', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>
<?php echo CHtml::hiddenField('ChangeStat[ids]','', array('class' => 'gv-manage-countries-ids')); ?>
<?php
$this->endWidget();
?>
