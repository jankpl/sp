<?php
Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?key='.GLOBAL_CONFIG::GOOGLE_MAPS_API_KEY_DOMAINS, CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCssFile('https://geowidget.easypack24.net/css/easypack.css');
Yii::app()->clientScript->registerScriptFile('https://geowidget.easypack24.net/js/sdk-for-javascript.js', CClientScript::POS_HEAD);
$css = '.easypack-widget:not(.mobile) .scroll-box .viewport
    {
        min-height: 360px;
    }
    .map-widget
    {
        min-height: 400px;
    }';
Yii::app()->clientScript->registerCss('inpost-fix', $css);

$script = 'window.easyPackAsyncInit = function () {
        easyPack.init({});
        var map = easyPack.mapWidget("easypack-map", function(point) {                      
            $("#_inpost_locker_delivery_id").val(point.name);
            $("#_inpost_locker_delivery_address").val(point.address.line1 + ", " + point.address.line2);
            
            $("#modal-inpost").modal("toggle");
        });
                
        $(document).ready(function(){
            var $inpostLockerPart = $("#inport_locker_part");        
            $("#inpost_locker_toggle").on("change", function(){
             if($(this).is(":checked"))
                $inpostLockerPart.slideDown();
             else
                $inpostLockerPart.slideUp();             
            });
        
        });
    };';
Yii::app()->clientScript->registerScript('inpost-run', $script, CClientScript::POS_END);
?>

<fieldset>
    <legend><?= Yii::t('courier', 'Inpost Paczkomaty');?></legend>

    <?php echo $form->labelEx($submodel,'_inpost_locker_active'); ?>
    <?php echo $form->checkBox($submodel, '_inpost_locker_active', array('data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak'), 'data-bootstrap-checkbox' =>true, 'data-default-class' => 'btn-md', 'data-on-class' => 'btn-md btn-primary', 'data-off-class' => 'btn-md btn-default', 'id' => 'inpost_locker_toggle')); ?>
    <?php echo $form->error($submodel,'_inpost_locker_active'); ?>
    <br/>
    <br/>
    <div class="alert alert-info text-center" id="inport_locker_part" style="display: <?= $submodel->_inpost_locker_active ? '' : 'none';?>">

        <p><?= Yii::t('courier', 'Wybierz punkt, do którego ma być dostarczona paczka.');?></p>
        <?php echo $form->error($submodel,'_inpost_locker_delivery_address'); ?>
        <?php echo $form->textField($submodel, '_inpost_locker_delivery_address', ['id' => '_inpost_locker_delivery_address', 'readonly' => true, 'placeholder' => Yii::t('courier', '-- kliknij, aby wybrać paczkomat --'), 'style' => 'text-align: center; cursor: pointer; width: 80%; max-width: 500px', 'data-toggle' => "modal", 'data-target' => "#modal-inpost"]); ?>
        <?php echo $form->error($submodel,'_inpost_locker_delivery_id'); ?>

        <?php echo $form->hiddenField($submodel, '_inpost_locker_delivery_id', ['id' => '_inpost_locker_delivery_id']); ?>

    </div>

</fieldset>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-inpost">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modal-content">
            <div style="width: 100%; height: 100%; margin: 20px auto;">
                <div id="easypack-map"></div>
            </div>
        </div>
    </div>
</div>