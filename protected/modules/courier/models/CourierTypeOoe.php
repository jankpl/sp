<?php

Yii::import('application.modules.courier.models._base.BaseCourierTypeOoe');

class CourierTypeOoe extends BaseCourierTypeOoe
{

    public $_accountCode;
    public $_accountUsername;
    public $_accountPassword;
    public $_additions;
    public $regulations;


    const PALLETWAY_FULL = 1;
    const PALLETWAY_HALF = 2;
    const PALLETWAY_QUARTER = 3;

    const SOURCE_EBAY = 1;

    public $_palletway_size;
    public $_ebay_order_id;


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bAcceptRegulationsByDefault' =>
                    [
                        'class'=>'application.models.bAcceptRegulationsByDefault'
                    ],
            ]
        );
    }

    public static function getPalletwayTypes()
    {
        $data[self::PALLETWAY_FULL] = Yii::t('courier', 'Cała paleta');
        $data[self::PALLETWAY_HALF] = Yii::t('courier', 'Pół palety');
        $data[self::PALLETWAY_QUARTER] = Yii::t('courier', 'Ćwierć palety');

        return $data;
    }

    protected static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    protected static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    public function beforeValidate()
    {
        if($this->service_code == '' && $this->courier_ooe_service_id != '')
            $this->service_code = CourierOoeService::mapIdToServiceName($this->courier_ooe_service_id);

        if($this->service_code != '' && $this->courier_ooe_service_id == '')
            $this->courier_ooe_service_id = CourierOoeService::mapServiceNameToId($this->service_code);

        return parent::beforeValidate();
    }

    /**
     * Serialize additional params (ex. Palletway size)
     * @return bool
     */
    public function beforeSave()
    {
        $this->params = array(
            '_palletway_size' => $this->_palletway_size,
            '_ebay_order_id' => $this->_ebay_order_id,

        );

//        /* @var $cmd CDbCommand */
//        $cmd = Yii::app()->db->createCommand();
//        $check = $cmd->select('COUNT(*)')->from((new CourierOoeService())->tableName())->where('service_name = :service_code', [':service_code' => $this->service_code])->queryScalar();
//
//        if(!$check)
//            $this->service_code = CourierOoeService::model()->findByPk($this->service_code)->service_name;


        // forms we use service ID and in DB we want service name
        // because of evolution...
        if($this->service_code == '' && $this->courier_ooe_service_id != '')
            $this->service_code = CourierOoeService::mapIdToServiceName($this->courier_ooe_service_id);

        if($this->service_code != '' && $this->courier_ooe_service_id == '')
            $this->courier_ooe_service_id = CourierOoeService::mapServiceNameToId($this->service_code);

        if($this->params !== NULL)
            $this->params = self::serialize($this->params);

        if($this->service_name == '')
        {
            $cos = CourierOoeService::model()->findByAttributes(['service_name' => $this->service_code]);
            if($cos !== NULL)
                $this->service_name = $cos->title;
        }

        return parent::beforeSave();
    }

    /**
     * Unserialize additional params (ex. Palletway size)
     * @return bool
     */
    public function afterFind()
    {
        if($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if(is_array($this->params))
            foreach($this->params AS $key => $item)
            {
                try {
                    $this->$key = $item;
                }
                catch (Exception $ex)
                {}
            }

        parent::afterFind();
    }



    public static function getCountryList($service_id = NULL)
    {

        if($service_id == NULL)
            $models = CountryList::model()->cache(60)->with('countryListTr')->findAll(array('order' => 't.name ASC'));
        else
            $models = CourierOoeService::getReceiverCountriesForService($service_id);

        return $models;
    }

    public function addAdditions($additions)
    {
        $this->_additions = $additions;
    }

    /**
     * @param bool $forceNotSaved Ignore that model has already ID and try to get additions by array of additions ids
     * @return array|mixed|null|static
     */
    public function listAdditions($forceNotSaved = false)
    {
        if($this->id == NULL OR $forceNotSaved)
            $additions = CourierOoeAdditionList::model()->findAllByAttributes(array('id' => $this->_additions));
        else
            $additions = CourierOoeAddition::model()->findAllByAttributes(array('courier_id' => $this->id));

        return $additions;
    }

    public function rules() {
        return array(
            array('stat', 'default', 'setOnEmpty' => true, 'value' => S_Status::ACTIVE),

            array('service_code', 'required', 'on' => 'form, step_1, summary'),

            array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('courier', 'Musisz zaakceptować regulamin.'), 'on' => 'summary'),

            array('courier_id', 'required', 'except' => 'step_1'),
            array('stat, _palletway_size, courier_ooe_service_id', 'numerical', 'integerOnly'=>true),
            array('courier_id', 'length', 'max'=>10),
            array('remote_id, service_name, service_code', 'length', 'max'=>128),
            array('label_url', 'length', 'max'=>256),
            array('remote_id, label_url', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, courier_id, remote_id, label_url, stat, service_name, service_code, courier_ooe_service_id', 'safe', 'on'=>'search'),
        );
    }

    public function isItTooBig()
    {
        return S_PackageMaxDimensions::isPackageTooBig($this->courier);
    }

    public function isItTooHeavy()
    {
        return S_PackageMaxDimensions::isPackageTooHeavy($this->courier);
    }


    public function getMaxDimensions()
    {
        return S_PackageMaxDimensions::getPackageMaxDimensions();
    }

    public function getMaxWeight()
    {
        return S_PackageMaxDimensions::getPackageMaxDimensions()['weight'];
    }

    public function getDimensionWeight()
    {
        return S_PackageMaxDimensions::calculateDimensionsWeight($this->courier);
    }

    public function isDimensionWeightBigger()
    {
        if(S_PackageMaxDimensions::calculateDimensionsWeight($this->courier) > $this->courier->package_weight)
            return true;
        else
            return false;
    }

    public function getFinalWeight()
    {
        return S_PackageMaxDimensions::getFinalWeight($this->courier);
    }

    public function setOoeAccountData($user_id = NULL)
    {
        $data = CourierOoeAccounts::getCredentialsForUser($user_id);
        $this->_accountCode = $data['account_code'];
        $this->_accountPassword = $data['pass'];
        $this->_accountUsername = $data['login'];
    }

    /**
     * Returns list of services for currently logged user
     * @param bool|true $onlyEnabled Whether to return all services fetched directly from OOE API or just activated by admin
     * @param bool|false $returnModels Whether to return array [SERVICE_ID => SERVICE_TITLE] or [SERVICE_ID => CourierOoeService model]. Works only when first parameter is also true.
     * @return array|mixed
     */
    public function getServiceList($onlyEnabled = true, $returnModels = false)
    {
        $CACHE_NAME = 'getServiceList_'.$onlyEnabled.'_'.$returnModels.'_'.Yii::app()->user->id;

        $services = Yii::app()->cache->get($CACHE_NAME);

        if($services === false OR (is_array($services) && !S_Useful::sizeof($services))) {
            $data = CourierOoeAccounts::getCredentialsForUser();
            $services = Yii::app()->OneWorldExpress->getServicesForUser($data['login'], $data['pass'], false, true);

            if (!is_array($services))
                $services = array();

            if ($onlyEnabled)
                    $services = CourierOoeService::getEnabledServices($services, $returnModels);

            Yii::app()->cache->set($CACHE_NAME, $services, 60*30);
        }

        return $services;
    }



    public function isServiceAvailable($service_id)
    {
        $service_list = $this->getServiceList();
        $service_list = array_keys($service_list);

        if(in_array($service_id, $service_list))
            return true;

        return false;
    }

    public static function isOooAvailableForUser()
    {

        return User::getCourierOoeIsAvailableForUser();

//        $data = CourierOoeAccounts::getCredentialsForUser();
//        if(!$data)
//            return false;
//        else
//            return true;
    }

    public static function areServicesAvailableForUser()
    {
        if(!self::isOooAvailableForUser())
            return false;

        $data = CourierOoeAccounts::getCredentialsForUser();
        $services = Yii::app()->OneWorldExpress->getServicesForUser($data['login'], $data['pass']);

        if(!is_array($services) OR !S_Useful::sizeof($services))
            return false;
        else
            return true;
    }


    public function saveNewPackage($quequeLabelNew = false, $differentReference = false, $cancelPackageOnFailure = false)
    {
        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        try
        {

            if (!$this->courier->saveWithAddressData(false))
                $errors = true;

            $this->setOoeAccountData();

            $this->courier_id = $this->courier->id;

            if (!$this->save())
                $errors = true;

            /* @var $item CourierOoeAdditionList */
            foreach($this->listAdditions(true) AS $item)
            {
                $addition = new CourierOoeAddition();
                $addition->courier_id = $this->id;
                $addition->description = $item->courierOoeAdditionListTr->description;
                $addition->name = $item->courierOoeAdditionListTr->title;
                $addition->courier_ooe_addition_list_id = $item->id;

                if(!$addition->save())
                    $errors = true;
            }

            if(!$errors) {
                $this->courier->changeStat(CourierStat::PACKAGE_PROCESSING);
                if ($quequeLabelNew) {

                    $label = $this->finalizeDetailsOnQueque($cancelPackageOnFailure);

                    if($label instanceof CourierLabelNew)
                    {
                        $transaction->commit();

                        return $this;
                    } else {
                        $transaction->rollback();
                        return false;
                    }


                } else {

                    $packageData = OneWorldExpressPackage::createByCourierTypeOoe($this, $differentReference);
                    $return = Yii::app()->OneWorldExpress->getLabel($packageData);

                    if(isset($return['error']))
                    {
                        return array(false, Yii::t('courier', 'Twoje zamówienie OOE nie powiodło się! Skontakuj się z nami w tej sprawie. Komunikat błędu: {error}', array('{error}' => $return['error'])));
                    }
                    else if (!$return)
                        $errors = true;
                    else {
                        $label = $return['label'];
                        $track_id = $return['trackId'];



//                        $this->courier->courier_stat_id = CourierStat::PACKAGE_RETREIVE;
//                        if(!$this->courier->update(array('courier_stat_id')))
//                            $errors = true;



                        $this->label_url = $label;
                        $this->remote_id = $track_id;

                        $this->courier->changeStat(CourierStat::PACKAGE_RETREIVE);

                        if (!$this->update(array('label_url', 'remote_id')))
                            $errors = true;
                    }
                }

                if ($errors) {
                    $transaction->rollback();
                    return array(false, Yii::t('courier', 'Twoje zamówienie OOE nie powiodło się! Skontakuj się z nami w tej sprawie.'));
                } else {
                    $transaction->commit();
                    return array(true, '');
                }

            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            return array(false, Yii::t('courier', 'Twoje zamówienie OOE nie powiodło się! Skontakuj się z nami w tej sprawie. Komunikat błędu: {error}', array('{error}' => $ex->getMessage())));
        }

    }

    /**
     * Queque label order when run in queque mode
     * @return CourierLabelNew
     */
    public function finalizeDetailsOnQueque($cancelPackageOnFailure = false)
    {
        $courierLabelNew = new CourierLabelNew();
        $courierLabelNew->triggers_pickup = true;
        $courierLabelNew->operator = CourierLabelNew::OPERATOR_OOE;

        $courierLabelNew->courier_id = $this->courier->id;
        $courierLabelNew->address_data_from_id = $this->courier->sender_address_data_id;
        $courierLabelNew->address_data_to_id = $this->courier->receiver_address_data_id;
        $courierLabelNew->stat = CourierLabelNew::STAT_NEW;

        if($cancelPackageOnFailure)
            $courierLabelNew->_cancel_package_on_fail = true;

        if($this->_ebay_order_id != '')
            $courierLabelNew->_ebay_order_id = $this->_ebay_order_id;


        $courierLabelNew->save();

        $this->courier_label_new_id = $courierLabelNew->id;
        $this->save(array('courier_label_new_id', 'date_updated'));

        return $courierLabelNew;
    }


    public function generateCarriageTicket()
    {

        // TODO
        // IF COURIER LABEL NEW MODE !

        try {
            $url = $this->label_url;

            $f1 = @fopen($url, 'r');
            $fcontent = $contents = @stream_get_contents($f1);

            if(!$fcontent)
                throw new CHttpException(403, Yii::t('courier', 'Błąd generowania etykiety #{no}. Skontaktuj się z nami w tej sprawie.', array('{no}' =>$this->courier->local_id)));

            return Yii::app()->getRequest()->sendFile('label.pdf', ($fcontent), 'application/pdf');
        }
        catch(Exception $ex)
        {
            throw new CHttpException(403, Yii::t('courier', 'Błąd generowania etykiety #{no}. Skontaktuj się z nami w tej sprawie.', array('{no}' =>$this->courier->local_id)));
        }
    }


    public static function saveWholeGroup(array $couriers, $source = false, $misc = false, $cancelPackageOnFailure = false)
    {
        $errors = false;

        $packages = [];

        /* @var $courier Courier */
        foreach($couriers AS $key => $courier)
        {
            if($courier->courierTypeOoe->courier === NULL)
                $courier->courierTypeOoe->courier = $courier;

            $return = $courier->courierTypeOoe->saveNewPackage(true, $courier->courierTypeOoe->_ebay_order_id, $cancelPackageOnFailure);

            if($return instanceof CourierTypeOoe) {
                array_push($packages, $return);

                if($source == self::SOURCE_EBAY)
                {
                    // maybe already exists? Then o
                    $ebayImport = EbayImport::model()->findByAttributes(['ebay_order_id' => $return->_ebay_order_id, 'user_id' => $return->courier->user_id, 'target' => EbayImport::TARGET_COURIER]);
                    if($ebayImport === NULL) {
                        $ebayImport = new EbayImport();
                        $ebayImport->user_id = $return->courier->user_id;
                    }
                    $ebayImport->target_item_id = $return->courier->id;
                    $ebayImport->ebay_order_id = $return->_ebay_order_id;
                    $ebayImport->stat = EbayImport::STAT_NEW;
                    $ebayImport->authToken = $misc;
                    $ebayImport->target = EbayImport::TARGET_COURIER;
                    $ebayImport->save();
                }


            }
        }


        if($errors)
        {
            return ['success' => false, 'data' => $couriers];
        } else {
            return ['success' => true, 'data' => $packages];
        }

    }

    /**
     * Function returns remote ID
     * @return string
     */
    public function findRemoteId()
    {
        if($this->courier_label_new_id == NULL)
            return $this->remote_id;
        else
            return $this->courierLabelNew->track_id;
    }

    /**
     * Function returns remote service name
     *
     * @param bool|false $withFetch Whether to get service name from DB first
     * @return string
     */
    public function findServiceName($withFetch = false)
    {
        if($this->courierLabelNew !== NULL)
        {
            // operator has benn changed by administrator
            if($this->courierLabelNew->operator != CourierLabelNew::OPERATOR_OOE)
            {
                return CourierLabelNew::getOperators()[$this->courierLabelNew->operator];
            }
        }


        if($withFetch)
            $this->fetchServiceName();



        if($this->service_name != '')
            return $this->service_name;
        else
        {
            if($this->service_code != '')
                return $this->service_code;
            else if($this->courier_ooe_service_id != '')
                return CourierOoeService::mapIdToServiceName($this->courier_ooe_service_id);
            else
                return '-';

        }
    }

    protected function fetchServiceName()
    {

        if($this->service_name == '')
        {
            $cos = CourierOoeService::model()->findByAttributes(['service_name' => $this->service_code]);
            if($cos !== NULL)
                $this->service_name = $cos->title;
        }

    }
}