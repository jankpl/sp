<div class="alert alert-success text-center">
    <h4><?php echo Yii::t('courier','Karta potwierdzenia');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t('courier','Generuj kartę potwierdzenia'), array('/courier/courier/ackCard', 'hash' => $model->hash), array('target' => '_blank', 'class' => 'btn'));?>
</div>
