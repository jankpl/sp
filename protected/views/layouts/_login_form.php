<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'login-form-home',
    'enableClientValidation'=>false,
    'clientOptions'=>array(
        'validateOnSubmit'=>false,
    ),
    'action'=>$this->createAbsoluteUrl('/site/login', array(),'https'),
    'htmlOptions' => array('class' => 'navbar-form'),
));
$model = new LoginForm();
?>
<?php echo $form->textField($model,'username',array('class' => 'col-xs-2', 'placeholder' => Yii::t('site','login...'))); ?>
<?php echo $form->passwordField($model,'password',array('class' => 'col-xs-2', 'placeholder' => Yii::t('site','hasło...'))); ?>
<?php echo TbHtml::submitButton('',array('id'=>'header-login-button', 'title' => Yii::t('site','Zaloguj się!'))); ?>
<?php $this->endWidget(); ?>
