<?php

class S_PreferredDeliveryTime
{
    protected static $_deliveryTime = Array(
        'do 12:00' => 'do 12:00',
        'po 12:00' => 'po 12:00',
    );

    public static function getDeliveryTimeArray()
    {
        return self::$_deliveryTime;
    }
}