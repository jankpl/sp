
<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'country-group-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        [
            'name' => 'name',
            'header' => 'Country Group'
        ],
        array(
            'header' => 'No of countries',
            'name' => 'countryListsCOUNT',
            'filter' => false,
        ),
        [
            'header' => 'No of group prices',
            'value' => 'PostalRouting::getNumberOfGroupPrices($data->id)'
        ],
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>


<h2>Import / export</h2>

<?php echo CHtml::link('Import',array('import'),array('class' => 'likeButton'));?> <?php echo CHtml::link('Export',array('export'),array('class' => 'likeButton'));?>

