<?php $this->beginContent('//layouts/_panel'); ?>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= $content;?>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
<?php $this->endContent(); ?>
