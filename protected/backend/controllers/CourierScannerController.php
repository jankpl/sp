<?php

class CourierScannerController extends Controller
{


    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('*'),
                'expression' => Admin::AUTHORITY_OPERATOR.' == Yii::app()->user->authority',
            ),
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_COURIER_SCANNER.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($hash = NULL, $a = false)
    {
        Yii::app()->getModule('courier');

        $step = 1;
        $selectedStatusName = '';
        $automatic = $a;

        $sessionData = Yii::app()->session['scannerData'];
        $model = new CourierScannerForm();

        $statusList = CourierStat::model()->findAllByAttributes(['id' => [70,3,90,99,149,
            3668,2519,971,1207,2853,2854,6915,2819]]);

        if($hash !== NULL && is_array($sessionData[$hash]))
        {
            $model->scenario = 'step2';
            $step = 2;
            $model->setAttributes($sessionData[$hash]);

            if(isset($_POST['CourierScannerForm']['package_ids']))
            {
                $automatic = $_POST['automatic'];

                $packagesIds = $_POST['CourierScannerForm']['package_ids'];
                $packagesIds = explode(PHP_EOL, $packagesIds);

                $model->package_ids = $packagesIds;

                if($model->validate())
                {
                    $courierModels = $model->returnCourierModels();

                    $changedCounter = 0;
                    $errorPackages = [];
                    foreach($courierModels AS $key => $courierModel) {

                        if (is_array($courierModel) && S_Useful::sizeof($courierModel) == 1)
                            $courierModel = $courierModel[0];

                        if (is_array($courierModel) && S_Useful::sizeof($courierModel) > 1)
                        {
                            $errorPackages[] = $model->package_ids[$key].' : More than one package found with this ID!';
                        }
                        else if (!($courierModel instanceof Courier)) {
                            $errorPackages[] = $model->package_ids[$key].' : Package not found!';
                        } else {

                            /* @var $courierModel Courier */
                            if ($courierModel->changeStat($model->stat_id, 1000 + Yii::app()->user->id, NULL, false, $model->location, false, false, false, false, false, Yii::app()->user->getModel()->scanning_sp_point_id)) {
                                $changedCounter++;
//                                Yii::app()->user->setFlash('success', 'Status has been set! <a class="btn btn-mini" href="' . Yii::app()->getBaseUrl(true) . '/tt/' . $courierModel->local_id . '" target="_blank">T&T details</a>');

                                Report_EobuwieCourierScan::checkItem($courierModel);

                                if(in_array(Yii::app()->user->id, [23,78])) // dsobol @ 27.07.2018 + zwrotywro @12.04.2019
                                    Report_CustomersCourierReturnScan::checkItem($courierModel);

                                if(Yii::app()->user->id == 19)
                                    Report_AdminCourierScan::checkItem($courierModel, Yii::app()->user->id);

                            } else {
                                $errorPackages[] = $model->package_ids[$key].' : Status not changed!';
//                                Yii::app()->user->setFlash('error', 'Status has NOT been set! Please try again....');
                            }

                        }
                    }

                    if(S_Useful::sizeof($errorPackages))
                        Yii::app()->user->setFlash('error', implode('<br/>',$errorPackages));

                    if($changedCounter)
                        Yii::app()->user->setFlash('success', 'Packages with statuses changed: '.$changedCounter);

                    // @var 14.12.2017 - create scan reports for admin #19
                    if(Yii::app()->user->id == 19)
                        Report_AdminCourierScan::addFailedNo(Yii::app()->user->id, S_Useful::sizeof($errorPackages));

                    $this->redirect(['/courierScanner/index', 'hash' => $hash, 'a' => $automatic]);
                    Yii::app()->end();
                }
            }

        }
        else if(isset($_POST['CourierScannerForm']))
        {
            $model->setAttributes($_POST['CourierScannerForm']);

            if($model->validate()) {


                $hash = uniqid();

                $config = [
                    'stat_id' => $model->stat_id,
                    'location' => $model->location,
                ];

                if (!is_array($sessionData))
                    $sessionData = [];

                $sessionData[$hash] = $config;

                Yii::app()->session['scannerData'] = $sessionData;

                $this->redirect(['/courierScanner/index', 'hash' => $hash]);
            }
        }

        $this->render('index',
            [
                'step' => $step,
                'model' => $model,
                'automatic' => $automatic,
                'statusList' => $statusList,
            ]);
    }
}