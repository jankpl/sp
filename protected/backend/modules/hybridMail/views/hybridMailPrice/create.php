<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>'List ' . $model->label(2), 'url' => array('index')),
);
?>

<h1>Create <?php echo GxHtml::encode($model->label()); ?></h1>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'hybrid-mail-price-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'country_group_id'); ?>
        <?php echo $form->dropDownList($model, 'country_group_id', CountryGroup::model()->findAll()); ?>
        <?php echo $form->error($model,'country_group_id'); ?>
    </div><!-- row -->

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->