<?php
// LAYOUT CONFIGURATION

$colWidth[] = 20;
$colWidth[] = 70;
$colWidth[] = 90;
$colWidth[] = 70;
$colWidth[] = 100;
$colWidth[] = 100;
$colWidth[] = 100;
$colWidth[] = 100;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 120;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;
$colWidth[] = 230;


$totalWidth = 0;
foreach($colWidth As $item)
    $totalWidth += $item;

$height = S_Useful::sizeof($models) * 31 + 60;
if($height > 850)
    $height = 850

?>

<?php $modelTemp = new Postal;?>
<?php $modelTemp2 = new AddressData();?>

<?php AddressDataSuggester::registerAssets(); ?>
<?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>

<?php
// WE DO NOT NEED FORM HERE - ALL DATA IS IN CACHE
//$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
//    array(
//        'enableAjaxValidation' => false,
//    )
//);
?>

<div class="text-center">
    <a href="<?php echo Yii::app()->createUrl('/postal/postal/import');?>" class="btn btn-warning"><?php echo Yii::t('postal', 'Od nowa');?></a>
    <br/>
</div>
<hr/>
<div class="text-center">
    <a href="#bottom">[<?php echo Yii::t('postal', 'skocz na dół');?>]</a>
</div>
<br/>
<?php
$this->widget('FlashPrinter');
?>


<div style="position: relative;">
    <div class="overlay"><div class="loader"></div></div>
    <?php
    // for upper scroll:
    ?>
    <div style="width: 100%; position: relative; height: 20px; overflow-x: scroll;" class="dummy-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; height: 20px;">
        </div>
    </div>
    <?php
    // end for upper scroll
    ?>
    <div style="width: 100%; position: relative; height: 60px; overflow: hidden;" class="header-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; overflow: hidden;">
            <table class="list hTop" style="font-size: 10px; text-align: left !important; table-layout: fixed; height: 60px;">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <tr>
                    <td colspan="16"><?php echo Yii::t('postal','Packages');?></td>
                    <td colspan="9"><?php echo Yii::t('postal','Sender');?></td>
                    <td colspan="9"><?php echo Yii::t('postal','Receiver');?></td>
                </tr>
                <tr>
                    <td>#</td>
                    <td><?php echo Yii::t('postal', 'Is valid');?></td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td style="text-align: center;">
                        <?php
                        if(!Yii::app()->user->model->getHidePrices()):
                            ?>
                            <?= Yii::t('postal', 'Price'); ?> (*)
                        <?php
                        endif;
                        ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('postal_type'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('size_class'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('weight'); ?> [g]
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('weight_class'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('content'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('ref'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('note1'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('note2'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('target_sp_point'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('value'); ?>
                    </td>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('value_currency'); ?>
                    </td>
                    <td style="border-left: 2px dashed gray;">
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td><td style="border-left: 2px dashed gray;">
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="width: 100%; position: relative; height: <?= $height;?>px; overflow-x: scroll;" class="real-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute;">

            <table class="list list-special" style="font-size: 10px; text-align: left !important; table-layout: fixed;" id="data-table">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <?php
                $errors = false;
                $totalPrice = 0;
                $points = SpPoints::getPoints();
                foreach($models AS $key => $item):
                    $this->renderPartial('import/_item',
                        array(
                            'model' => $item,
                            'i' => $key,
                            'points' => $points,
                        ));

                    $totalPrice += $item->_price_total;
                    if($item->hasErrors() OR $item->senderAddressData->hasErrors() OR $item->receiverAddressData->hasErrors())
                        $errors = true;
                endforeach;
                ?>
            </table>
        </div>
    </div>
    <?php
    if($errors):
        ?>
        <br/>
        <br/>
        <a href="#" id="errors-up" class="btn"><?= Yii::t('postal', 'Błędne wiersze na górę listy');?></a>
        <a href="<?= Yii::app()->createUrl('/postal/postal/exportErroredImportData', ['hash' => $hash]);?>" class="btn btn-primary"><?= Yii::t('postal', 'Eksport błędnych wierszy do pliku .xls');?></a>
        <a href="<?= Yii::app()->createUrl('/postal/postal/deleteErroredImportData', ['hash' => $hash]);?>" class="btn btn-warning"><?= Yii::t('postal', 'Usuń błędne wiersze');?></a>
    <?php
    endif;
    ?>
    <br/>
    <br/>
    <?php
    if(!Yii::app()->user->model->getHidePrices()):
        ?>
        <div class="alert alert-info">
            <p><?= Yii::t('postal', 'Łączna cena');?> (*): <strong><?= $totalPrice;?> <?= Yii::app()->PriceManager->getCurrencyCode();?></strong></p>

            <p class="text-right" style=""><small>* <?= Yii::t('postal', 'Cena jest szacunkowa i nie uwzględnia indywidualnych rabatów użytkownika');?></small></p>
        </div>
    <?php
    endif;
    ?>

    <div class="alert alert-warning">
        <?= Yii::t('postal', 'Przejście dalej spowoduje zamówienie paczek i naliczenie opłat.'); ?>
    </div>

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
        array(
            'enableAjaxValidation' => false,
        )
    );
    ?>

    <div class="navigate" style="text-align: center;" id="bottom">
        <?php echo TbHtml::submitButton(Yii::t('postal', 'Refresh'), array('name' => 'refresh', 'class' => 'save-packages btn-xs')); ?>
        <br/>
        <br/>
        <?php echo TbHtml::submitButton(Yii::t('postal', 'Save this part of data'), array('name' => 'save', 'class' => 'save-packages btn-primary')); ?>
    </div>

    <?php
    $this->endWidget();
    ?>
</div>


<div class="modal fade " tabindex="-1" role="dialog" id="edit-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="edit-modal-content">
        </div>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  removeRowOnImport: '.CJSON::encode(Yii::app()->createUrl('postal/postal/ajaxRemoveRowOnImport')).',
                  editRowOnImport: '.CJSON::encode(Yii::app()->createUrl('postal/postal/ajaxEditRowOnImport')).',
                  updateRowOnImport: '.CJSON::encode(Yii::app()->createUrl('postal/postal/ajaxUpdateRowItemOnImport')).',
              },
               text: {
                  unknownError: '.CJSON::encode(Yii::t('site', 'Wystąpił nieznany błąd!')).',
              },
              misc: {
                  hash: '.CJSON::encode($hash).',
              },
          };
      ',CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/importListModels.js', CClientScript::POS_HEAD);
?>

<script>
    $(document).ready(function(){
        var hash = location.hash.substr(1);
        if(hash == 'errorsUp')
            errorsUp();
    });

    $('#errors-up').on('click', function(e){

        e.preventDefault();
        window.location.hash = 'errorsUp';
        errorsUp();
    });

    function errorsUp()
    {
        $('tr.row-errored').each(function(i,v){
            $(v).detach().prependTo('#data-table tbody');
        });
    }
</script>
