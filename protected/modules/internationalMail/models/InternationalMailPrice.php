<?php

Yii::import('application.modules.internationalMail.models._base.BaseInternationalMailPrice');

class InternationalMailPrice extends BaseInternationalMailPrice
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('creation_admin_id', 'default',
                'value'=>Yii::app()->user->id, 'setOnEmpty' => false, 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on'=>'update'),
            array('update_admin_id', 'default',
                'value'=>Yii::app()->user->id, 'setOnEmpty' => false, 'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),

            array('stat', 'numerical', 'integerOnly'=>true),
            array('creation_admin_id, update_admin_id', 'length', 'max'=>10),
            array('date_updated', 'safe'),
            array('date_updated, update_admin_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, creation_admin_id, update_admin_id, stat', 'safe', 'on'=>'search'),
        );
    }

    protected static $_maxWeight = 9999;
    protected static $_maxPrice = 9999;

    public function getInternationalMailGroupId()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('country_group');
        $cmd->from('international_mail_price_has_group');
        $cmd->andWhere('international_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            return $result;

        }

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('country_group');
        $cmd->from('international_mail_delivery_price_has_hub');
        $cmd->where('international_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        return $result;
    }

    public function getUserGroupId()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('user_group_id');
        $cmd->from('international_mail_price_has_group');
        $cmd->andWhere('international_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            return $result;

        }

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('user_group_id');
        $cmd->from('international_mail_delivery_price_has_hub');
        $cmd->where('international_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        return $result;
    }

    public static function loadOrCreatePrice($country_group, $user_group = null)
    {
        if($user_group == NULL)
            $model = InternationalMailPrice::model()->with('internationalMailPriceHasGroups')->find('internationalMailPriceHasGroups.country_group=:id AND user_group_id IS NULL', array(':id' => $country_group));
        else
            $model = InternationalMailPrice::model()->with('internationalMailPriceHasGroups')->find('internationalMailPriceHasGroups.country_group=:id AND user_group_id = :group', array(':id' => $country_group, ':group' => $user_group));

        if($model == NULL)
            $model = new InternationalMailPrice();


        return $model;
    }

    public function deleteWithRelated()
    {
        $success = false;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id');
        $cmd->from('international_mail_price_has_group');
        $cmd->andWhere('international_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('international_mail_price_value','international_mail_price_id = :id', array(':id' => $this->id));
            $cmd->delete('international_mail_price_has_group','id = :id', array(':id' => $result));
            $cmd->delete('international_mail_price','id = :id', array(':id' => $this->id));

            $success = true;
        }

        return $success;
    }

    public function loadValueItems()
    {
        $internationalMailPriceValues = [];

        if($this->id != NULL)
            $internationalMailPriceValues = InternationalMailPriceValue::model()->findAll(array('condition' => 'international_mail_price_id = :international_mail_price_id', 'params' => array(':international_mail_price_id' => $this->id), 'order' => 'weight_top_limit ASC'));

        if(!S_Useful::sizeof($internationalMailPriceValues))
            $internationalMailPriceValues[0] = new InternationalMailPriceValue();

        return $internationalMailPriceValues;
    }

    public static function getMaxWeight()
    {
        return self::$_maxWeight;
    }

    public static function getMaxPrice()
    {
        return self::$_maxPrice;
    }

    public function generateDefaultPriceValue()
    {
        $maxPriceModel = new InternationalMailPriceValue();
        $maxPriceModel->international_mail_price_id = $this->id;
        $maxPriceModel->weight_top_limit = self::getMaxWeight();
        $maxPriceModel->pricePerItem = new Price();
        $maxPriceModel->pricePerWeight = new Price();


        return $maxPriceModel;
    }

    /**
     * Returns array with price components of price for given user group
     *
     * @param $weight float
     * @param $country_from int
     * @param $user_group_id int
     * @return array
     */
    protected static function getPriceDataForUserGroup($currency, $weight, $country_to, $user_group_id)
    {


        if($user_group_id == NULL)
            return false;

        $country_group = CountryGroup::getGroupForCountryId($country_to);


        $cmd = Yii::app()->db->createCommand();
        $cmd->select('pricePerItem.value AS price_per_item, pricePerWeight.value AS price_per_weight');
        $cmd->from('international_mail_price_value');
        $cmd->join('international_mail_price', 'international_mail_price_value.international_mail_price_id = international_mail_price.id');
        $cmd->join('international_mail_price_has_group', 'international_mail_price_has_group.international_mail_price_item = international_mail_price.id');
        $cmd->join('country_group', 'international_mail_price_has_group.country_group = country_group.id');
        $cmd->join('price_value AS pricePerItem', 'international_mail_price_value.price_per_item = pricePerItem.price_id AND pricePerItem.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->join('price_value AS pricePerWeight', 'international_mail_price_value.price_per_weight = pricePerWeight.price_id AND pricePerWeight.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('weight_top_limit >= :weight', array(':weight' => $weight));
        $cmd->andWhere('country_group.id = :country_group', array(':country_group' => $country_group));
        $cmd->andWhere('international_mail_price_has_group.user_group_id = :user_id', array(':user_id' => $user_group_id));
        $cmd->order('weight_top_limit ASC');
        $cmd->limit('1');
        $result = $cmd->queryRow();

        return $result;

    }

    /**
     * Returns array with price components of price
     *
     * @param $weight float
     * @param $country_from int
     * @param $user_group_id int
     * @return array
     */
    protected static function getPriceData($currency, $weight, $country_to, $user_group_id)
    {


        $country_group = CountryGroup::getGroupForCountryId($country_to);

        $priceForUser = self::getPriceDataForUserGroup($currency, $weight, $country_to, $user_group_id); // find if user has his own price

        if($priceForUser)
            return $priceForUser;

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('pricePerItem.value AS price_per_item, pricePerWeight.value AS price_per_weight');
        $cmd->from('international_mail_price_value');
        $cmd->join('international_mail_price', 'international_mail_price_value.international_mail_price_id = international_mail_price.id');
        $cmd->join('international_mail_price_has_group', 'international_mail_price_has_group.international_mail_price_item = international_mail_price.id');
        $cmd->join('country_group', 'international_mail_price_has_group.country_group = country_group.id');
        $cmd->join('price_value AS pricePerItem', 'international_mail_price_value.price_per_item = pricePerItem.price_id AND pricePerItem.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->join('price_value AS pricePerWeight', 'international_mail_price_value.price_per_weight = pricePerWeight.price_id AND pricePerWeight.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('weight_top_limit >= :weight', array(':weight' => $weight));
        $cmd->andWhere('country_group.id = :country_group', array(':country_group' => $country_group));
        $cmd->andWhere('international_mail_price_has_group.user_group_id IS NULL');
        $cmd->order('weight_top_limit ASC');
        $cmd->limit('1');
        $result = $cmd->queryRow();
        return $result;
    }


    /**
     * Returns internationalMail pickup price based on given arguments.
     *
     * @param $weight float Package weight in defined unit
     * @param $country_from int Sender country ID
     * @param null $user_group_id int User group ID
     * @return float Returns price in defined unit
     */
    public static function calculatePriceEach($currency, $weight, $country_to, $user_group_id = null)
    {

        $priceData = self::getPriceData($currency, $weight, $country_to, $user_group_id);
        $price_per_item = $priceData['price_per_item'];
        $price_per_weight = $priceData['price_per_weight'];

        $price = $price_per_item + round($price_per_weight * $weight,2);

        return $price;

    }




    public function toggleStat()
    {
        if(!S_Useful::sizeof($this->internationalMailPriceValues))
            return false;

        $this->stat = abs($this->stat - 1);
        return $this->save();
    }


}