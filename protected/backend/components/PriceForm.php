<?php

class PriceForm extends CWidget
{
    /**
     * @var CFormModel
     */
    public $model;
    public $formFieldPrefix;
    public $form;

    public $vertical = true;
    public $readonly = false;

    public function run()
    {

        if($this->model === NULL)
            $this->model = new Price();

        $this->render('priceForm/_form_base',
            array(
                'model' => $this->model,
                'formFieldPrefix' => $this->formFieldPrefix,
                'vertical' => $this->vertical,
                'readonly' => $this->readonly,
            ));
    }

}