<?php

class S_OrderIO
{

    public static function exportToXls(array $models)
    {
        $orderAttributes = [
            'id',
            'date_entered',
            'user_id',
            'product_type',
            'value_netto',
            'value_brutto',
            'currency',
            'payment_type_id',
            'order_stat_id',
            'order_stat',
            'packages_ids',
            'sender_name',
            'sender_company',
            'sender_address_line_1',
            'sender_address_line_2',
            'sender_zip_code',
            'sender_city',
            'sender_country',
            'receiver_name',
            'receiver_company',
            'receiver_address_line_1',
            'receiver_address_line_2',
            'receiver_zip_code',
            'receiver_city',
            'receiver_country',
            'note'
        ];

        self::_exportToXls($models, $orderAttributes);
    }

    protected static function _exportToXls(array $models, array $orderAttributes)
    {



        $arrayHeader = array_merge($orderAttributes);

        $array = [];
        $array[0] = $arrayHeader;

        $i = 1;

        /* @var $model Order */

        foreach($models AS $model)
        {

            $products = $model->orderProduct;

            $products_ids = [];
            if(is_array($products))
                foreach($products AS $product)
                {
                    $cmd = Yii::app()->db->createCommand();
                    if($product->type == OrderProduct::TYPE_COURIER)
                    {
                        $cmd->select('local_id');
                        $cmd->from((new Courier())->tableName());
                        $cmd->where('id = :id', [':id' => $product->type_item_id]);
                        $cmd->limit(1);
                        $res = $cmd->queryScalar();
                        if($res)
                            array_push($products_ids, $res);
                    }
                    else if($product->type == OrderProduct::TYPE_HYBRID_MAIL)
                    {
                        $cmd->select('local_id');
                        $cmd->from((new HybridMail())->tableName());
                        $cmd->where('id = :id', [':id' => $product->type_item_id]);
                        $cmd->limit(1);
                        $res = $cmd->queryScalar();
                        if($res)
                            array_push($products_ids, $res);
                    }
                    else if($product->type == OrderProduct::TYPE_INTERNATIONAL_MAIL)
                    {
                        $cmd->select('local_id');
                        $cmd->from((new InternationalMail())->tableName());
                        $cmd->where('id = :id', [':id' => $product->type_item_id]);
                        $cmd->limit(1);
                        $res = $cmd->queryScalar();
                        if($res)
                            array_push($products_ids, $res);
                    }
                }

            $products_ids = implode(';', $products_ids);

            $note = '';


            if($product->type == OrderProduct::TYPE_COURIER)
            {
                // sender and receiver is always the same, so just take from first model
                $productModel = Courier::model()->with('senderAddressData')->with('receiverAddressData')->findByPk($products[0]->type_item_id);
                $senderAddressData = $productModel->senderAddressData;
                $receiverAddressData = $productModel->receiverAddressData;
            }
            else if($product->type == OrderProduct::TYPE_INTERNATIONAL_MAIL)
            {
                // sender is always the same, so just take from first model
                $productModel = InternationalMail::model()->with('senderAddressData')->with('receiverAddressData')->findByPk($products[0]->type_item_id);
                $senderAddressData = $productModel->senderAddressData;
                $receiverAddressData = $productModel->receiverAddressData;
            }
            else if($product->type == OrderProduct::TYPE_HYBRID_MAIL)
            {
                // sender is always the same, so just take from first model
                $productModel = HybridMail::model()->with('senderAddressData')->with('hybridMailReceivers')->findByPk($products[0]->type_item_id);
                $senderAddressData = $productModel->senderAddressData;

                // just get first receiver and add note, that next may be different
                $receiverAddressData = $productModel->hybridMailReceivers[0]->addressData;

                if(S_Useful::sizeof($productModel->hybridMailReceivers) > 1)
                    $note = '/ <-- just first receiver. Subsequent receivers may be different. /';
            }

            $dataOrder = array(
                $model->id,
                $model->date_entered,
                $model->user->login,
                Order::getProductName($model->product_type),
                $model->value_netto,
                $model->value_brutto,
                $model->currency,
                PaymentType::getPaymentTypeName($model->payment_type_id),
                $model->orderStat->id,
                $model->orderStat->name,
                $products_ids,
                $senderAddressData->name,
                $senderAddressData->company,
                $senderAddressData->address_line_1,
                $senderAddressData->address_line_2,
                $senderAddressData->zip_code,
                $senderAddressData->city,
                $senderAddressData->country0->code,
                $receiverAddressData->name,
                $receiverAddressData->company,
                $receiverAddressData->address_line_1,
                $receiverAddressData->address_line_2,
                $receiverAddressData->zip_code,
                $receiverAddressData->city,
                $receiverAddressData->country0->code,
                $note,
            );

            $array[$i] = array_merge($dataOrder);
            $i++;
        }

//
//
//        header("Content-Disposition: attachment; filename=\"export.xls\"");
//        header("Content-Type: application/vnd.ms-excel;");
//        header("Pragma: no-cache");
//        header("Expires: 0");
//        $out = fopen("php://output", 'w');
//        foreach ($array as $data)
//        {
//            fputcsv($out, $data,"\t");
//        }
//        fclose($out);

        S_XmlGenerator::generateXml($array);

//
//        require_once(Yii::getPathOfAlias('application.vendor.excel_xml').'/Excel_XML.php');
//        $xls = new Excel_XML('UTF-8', true, 'Export');
//        $xls->addArray($array);
//        $xls->generateXML('Export');
        Yii::app()->end();

    }


}