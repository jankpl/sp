<?php

class UKMailClient
{
    const WSDL_PREFIX = GLOBAL_CONFIG::UKMAIL_WSDL_PREFIX;
    const USERNAME = GLOBAL_CONFIG::UKMAIL_USERNAME;
    const PASSWORD = GLOBAL_CONFIG::UKMAIL_PASSWORD;
    const ACCOUNT_NO = GLOBAL_CONFIG::UKMAIL_ACCOUNT_NO;
    const WSDL_TT = GLOBAL_CONFIG::UKMAIL_WSDL_TT;

    protected $from_contactName;
    protected $from_businessName;
    protected $from_address1;
    protected $from_address2;
    protected $from_postalTown;
    protected $from_postCode;
    protected $from_countryCode;
    protected $from_email;
    protected $from_telephone;

    protected $to_contactName;
    protected $to_businessName;
    protected $to_address1;
    protected $to_address2;
    protected $to_postalTown;
    protected $to_postCode;
    protected $to_countryCode;
    protected $to_email;
    protected $to_telephone;

    protected $preDeliveryNotification = false;
    protected $customersRef;
    protected $items = 1;
    protected $weight;
    protected $_serviceKey;
    protected $confirmationOfDelivery = false;
    protected $exchangeOnDelivery = false;
    protected $extendedCover = false;
    protected $signatureOptional = true;
    protected $BookIn = false;
    protected $codAmount = 0;
    protected $longLenght = false;

    // INT:
    protected $goodsDescription1;
    protected $lenght;
    protected $width;
    protected $height;
    protected $noDangerousGoods;
    protected $inFreeCirculationEU;
    protected $invoiceType;
    protected $value;

    // Return:
    protected $collectionDate;

    // 3RD:
    protected $collectionAddress1;
    protected $collectionAddress2;
    protected $collectionAddress3;
    protected $collectionPostalTown;
    protected $collectionPostalCode;
    protected $collectionCountryCode;
    protected $collectionEmail;

    protected $_type = false;


    const TYPE_DOMESTIC = 1;
    const TYPE_INT = 2;
    const TYPE_RETURN = 3;
    const TYPE_3RD = 4;

    const SIGNATURE_AND_NEIGHBOUR = 1;
    const SIGNATURE_ONLY = 2;
    const SAFE_OR_SIGNATURE_NEIGHBOUR = 3;

    const ADD_NEXT = 1;
    const ADD_NEXT_9 = 2;
    const ADD_NEXT_1030 = 3;
    const ADD_NEXT_12 = 4;
    const ADD_NEXT_SATURDAY = 5;
    const ADD_NEXT_SATURDAY_9 = 6;
    const ADD_NEXT_SATURDAY_1030 = 7;
    const ADD_48 = 8;
    const ADD_48PLUS = 9;

    const INT_ROAD = 1;
    const INT_AIR = 2;

    public static function getIntServiceCode($type)
    {
        if($type == self::INT_ROAD)
            return 204;
        else if($type == self::INT_AIR)
            return 101;
    }

    protected static function getDomesticServiceCode($signature, $add = 0)
    {

        $data = [
            self::SIGNATURE_AND_NEIGHBOUR => [
                self::ADD_NEXT => 1, // no additions = Next Day
                self::ADD_NEXT_9 => false,
                self::ADD_NEXT_1030 => 9,
                self::ADD_NEXT_12 => 2,
                self::ADD_NEXT_SATURDAY => 4,
                self::ADD_NEXT_SATURDAY_9 => false,
                self::ADD_NEXT_SATURDAY_1030 => 7,
                self::ADD_48 => 48,
                self::ADD_48PLUS => false,
            ],
            self::SIGNATURE_ONLY => [
                self::ADD_NEXT => 220, //  no additions = Next Day
                self::ADD_NEXT_9 => 3,
                self::ADD_NEXT_1030 => 222,
                self::ADD_NEXT_12 => 221,
                self::ADD_NEXT_SATURDAY => 225,
                self::ADD_NEXT_SATURDAY_9 => 5,
                self::ADD_NEXT_SATURDAY_1030 => 226,
                self::ADD_48 => false,
                self::ADD_48PLUS => false,
            ],
            self::SAFE_OR_SIGNATURE_NEIGHBOUR => [
                self::ADD_NEXT => 210, //  no additions = Next Day
                self::ADD_NEXT_9 => false,
                self::ADD_NEXT_1030 => 212,
                self::ADD_NEXT_12 => 211,
                self::ADD_NEXT_SATURDAY => 215,
                self::ADD_NEXT_SATURDAY_9 => false,
                self::ADD_NEXT_SATURDAY_1030 => 216,
                self::ADD_48 => 72,
                self::ADD_48PLUS => 72,
            ]
        ];
        return isset($data[$signature][$add]) ? $data[$signature][$add] : false;
    }

    protected static function getReturnServiceCode($add = 0)
    {
        $data = [
            self::ADD_NEXT => 401, // no additions = Next Day
            self::ADD_NEXT_9 => 403,
            self::ADD_NEXT_1030 => 409,
            self::ADD_NEXT_12 => 402,
            self::ADD_NEXT_SATURDAY => 404,

        ];

        return isset($data[$add]) ? $data[$add] : false;
    }

    protected static function get3rdServiceCode($add = 0)
    {
        $data = [
            self::ADD_NEXT => 451, // no additions = Next Day
            self::ADD_NEXT_9 => 453,
            self::ADD_NEXT_1030 => 459,
            self::ADD_NEXT_12 => 452,
            self::ADD_NEXT_SATURDAY => 454,

        ];

        return isset($data[$add]) ? $data[$add] : false;
    }

    protected static function _clearToken()
    {
        $CACHE_NAME = 'UK_MAIL_AUTH_TOKEN';
        Yii::app()->cache->set($CACHE_NAME, false, 60 * 60 * 6);

        MyDump::dump('ukpost-rr.txt', 'CLEAR TOKEN');
    }

    protected static function getAuthToken()
    {

        $result = new stdClass();

        $CACHE_NAME = 'UK_MAIL_AUTH_TOKEN';
        $token = Yii::app()->cache->get($CACHE_NAME);

        if($token === false)
        {
            $data = new stdClass();
            $data->loginWebRequest = new stdClass();
            $data->loginWebRequest->Password = self::PASSWORD;
            $data->loginWebRequest->Username = self::USERNAME;

            $result = self::go('login', 'UKMAuthenticationServices/UKMAuthenticationService.svc?wsdl', $data);
            // no soap error but maybe error in response?
            if($result->success)
            {
                if($result->data->LoginResult->Result != 'Successful')
                {
                    $result->success = false;
                    $result->error = $result->data->LoginResult->Errors->UKMWebError->Description;
                    $result->errorDesc = serialize($result->data->LoginResult->Errors);
                    $result->errorCode = $result->data->LoginResult->Errors->UKMWebError->Code;
                    $result->data = NULL;
                }
            }

            if($result->success) {
                $token = $result->data->LoginResult->AuthenticationToken;
                Yii::app()->cache->set($CACHE_NAME, $token, 60 * 60 * 20);
            }
        }

        if($token)
        {
            $result->success = true;
            $result->data = new stdClass();
            $result->data->token = $token;
        }

        return $result;
    }



    public static function getCollectionNumber($offsetDate = 0, $isRetry = false)
    {
        $collectionDate = new DateTime();

        if(date('H') < 9)
            $collectionStartTime = 'T09:00:00.000';
        else
            $collectionStartTime = 'T'.date('H:i:s').'.000';

        if($offsetDate OR ((date('H') == 16 && date('i') > 59) OR date('H') > 16))
        {
            if(!$offsetDate)
                $offsetDate++;

            $collectionStartTime = 'T09:00:00.000';
        }

        if($offsetDate)
            $collectionDate->modify('+'.$offsetDate.' day');

        $collectionDate = $collectionDate->format('Y-m-d');

        $collectionEndTime = 'T17:00:00.000';

        $data = new stdClass();
        $data->request = new stdClass();


        $token = self::getAuthToken();
        if(!$token->success)
        {
            return $token;
        }

        $data->request->AuthenticationToken = $token->data->token;

        $data->request->Username = self::USERNAME;

        $data->request->AccountNumber = self::ACCOUNT_NO;

        $data->request->ClosedForLunch = false;
        $data->request->EarliestTime = $collectionDate.$collectionStartTime;
        $data->request->LatestTime = $collectionDate.$collectionEndTime;
        $data->request->RequestedCollectionDate = $collectionDate.$collectionStartTime;


        $result = self::go('bookCollection', 'UKMCollectionServices/UKMCollectionService.svc?wsdl',  $data);

        // no soap error but maybe error in response?
        if($result->success)
        {
            if($result->data->BookCollectionResult->Result == 'Successful') {
                $collectionNo = $result->data->BookCollectionResult->CollectionJobNumber;

                $result->data = new stdClass();
                $result->data->collectionNo = $collectionNo;

            }
            elseif(in_array($result->data->BookCollectionResult->Errors->UKMWebError->Code, ['2050', '16384']) && !$isRetry)
            {

                MyDump::dump('ukpost-rr.txt', 'RETRY WITH CLEAR TOKEN');

                // authentication error - reset token and try again
                self::_clearToken();
                return self::getCollectionNumber($offsetDate, true);
            }
            elseif($offsetDate < 7) {

                MyDump::dump('ukpost-rr.txt', 'OFFSET DATE: '.print_r($offsetDate,1));

                $offsetDate++;
                return self::getCollectionNumber($offsetDate, $isRetry);

            } else {
                $result->success = false;
                $result->error = $result->data->BookCollectionResult->Errors->UKMWebError->Description;
                $result->errorDesc = serialize($result->data->BookCollectionResult->Errors);
                $result->errorCode = $result->data->BookCollectionResult->Errors->UKMWebError->Code;
                $result->data = NULL;
            }
        }


        return $result;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierTypeInternal, AddressData $from, AddressData $to, $operator_uniq_id, $returnError = false, $withCollect = false)
    {
        $model = new self;

        $model->customersRef = '#'.$courierTypeInternal->courier->local_id;

        // SPECIAL VALUE FOR 4VALUES - 22.03.2017
        if($courierTypeInternal->courier->user_id == User::USER_4VALUES)
            if($courierTypeInternal->courier->ref != '')
                $model->customersRef =  mb_substr($courierTypeInternal->courier->ref,2);

        $model->weight = $courierTypeInternal->courier->getWeight(true);
        $model->lenght = $courierTypeInternal->courier->package_size_l;
        $model->width = $courierTypeInternal->courier->package_size_w;
        $model->height = $courierTypeInternal->courier->package_size_d;


        $model->from_contactName = $from->name;
        $model->from_businessName = $from->company;
        $model->from_countryCode = $from->country0->code;
        $model->from_postCode = $from->zip_code;
        $model->from_postalTown = $from->city;
        $model->from_address1 = $from->address_line_1;
        $model->from_address2 = $from->address_line_2;
        $model->from_telephone = $from->tel;
        $model->from_email = $from->email;

        $model->to_contactName = $to->name;
        $model->to_businessName = $to->company;
        $model->to_countryCode = $to->country0->code;
        $model->to_postCode = $to->zip_code;
        $model->to_postalTown = $to->city;
        $model->to_address1 = $to->address_line_1;
        $model->to_address2 = $to->address_line_2;
        $model->to_telephone = $to->tel;
        $model->to_email = $to->email;

        $model->goodsDescription1 = $courierTypeInternal->courier->package_content;

        $model->items = $courierTypeInternal->courier->packages_number;
        $model->value = $courierTypeInternal->courier->getPackageValueConverted('EUR');

        $model->codAmount = $courierTypeInternal->courier->cod_value;

        if($operator_uniq_id == CourierOperator::UNIQID_UK_MAIL) {

            if (!in_array($from->country_id, [CountryList::COUNTRY_UK, CountryList::COUNTRY_IE]) OR !in_array($to->country_id, [CountryList::COUNTRY_UK, CountryList::COUNTRY_IE])) {
                // int:
                $model->_type = self::TYPE_INT;

                // default
                $model->_serviceKey = self::getIntServiceCode(self::INT_ROAD);

                if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_AIR)) {
                    $model->_serviceKey = self::getIntServiceCode(self::INT_AIR);
                }
            } else {
                // domestic:
                $model->_type = self::TYPE_DOMESTIC;

                // default
                $signature = self::SIGNATURE_AND_NEIGHBOUR;

                if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_SPECIFIED_ADDRESS_ONLY)) {
                    $signature = self::SIGNATURE_ONLY;
                } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_LEAVE_SAFE_OR_SIG_NEIGH)) {
                    $signature = self::SAFE_OR_SIGNATURE_NEIGHBOUR;
                }

                $add = self::ADD_NEXT;

                if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_9)) {
                    $add = self::ADD_NEXT_9;
                } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_1030)) {
                    $add = self::ADD_NEXT_1030;
                } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_12)) {
                    $add = self::ADD_NEXT_12;
                } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_SAT)) {
                    $add = self::ADD_NEXT_SATURDAY;
                } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_SAT_9)) {
                    $add = self::ADD_NEXT_SATURDAY_9;
                } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_SAT_1030)) {
                    $add = self::ADD_NEXT_SATURDAY_1030;
                } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_48_HOUR)) {
                    $add = self::ADD_48;
                } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_48_HOUR_PLUS)) {
                    $add = self::ADD_48PLUS;
                }

                $model->_serviceKey = self::getDomesticServiceCode($signature, $add);
            }
        }
        else if($operator_uniq_id == CourierOperator::UNIQID_UK_MAIL_3RD)
        {
            $model->_type = self::TYPE_3RD;

            $add = self::ADD_NEXT;
            if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_9)) {
                $add = self::ADD_NEXT_9;
            } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_1030)) {
                $add = self::ADD_NEXT_1030;
            } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_12)) {
                $add = self::ADD_NEXT_12;
            } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_SAT)) {
                $add = self::ADD_NEXT_SATURDAY;
            }

            $model->_serviceKey = self::get3rdServiceCode($add);
        }
        else if($operator_uniq_id == CourierOperator::UNIQID_UK_MAIL_RETURN)
        {
            $model->_type = self::TYPE_RETURN;

            $add = self::ADD_NEXT;
            if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_9)) {
                $add = self::ADD_NEXT_9;
            } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_1030)) {
                $add = self::ADD_NEXT_1030;
            } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_NEXT_DAY_12)) {
                $add = self::ADD_NEXT_12;
            } else if ($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UKMAIL_SAT)) {
                $add = self::ADD_NEXT_SATURDAY;
            }

            $model->_serviceKey = self::getReturnServiceCode($add);
        }

        if(!$model->_serviceKey)
        {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'This combination of services in invalid!',
                )];
                return $return;
            } else {
                return false;
            }
        }


        return $model->orderPackage($courierTypeInternal->courier, $returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {
        $model = new self;

        $model->customersRef = '#'.$courierTypeU->courier->local_id;

        // SPECIAL VALUE FOR 4VALUES - 22.03.2017
        if($courierTypeU->courier->user_id == User::USER_4VALUES)
            if($courierTypeU->courier->ref != '')
                $model->customersRef =  mb_substr($courierTypeU->courier->ref,2);

        $model->weight = $courierTypeU->courier->package_weight;
        $model->lenght = $courierTypeU->courier->package_size_l;
        $model->width = $courierTypeU->courier->package_size_w;
        $model->height = $courierTypeU->courier->package_size_d;

        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model->from_contactName = $from->name;
        $model->from_businessName = $from->company;
        $model->from_countryCode = $from->country0->code;
        $model->from_postCode = $from->zip_code;
        $model->from_postalTown = $from->city;
        $model->from_address1 = $from->address_line_1;
        $model->from_address2 = $from->address_line_2;
        $model->from_telephone = $from->tel;
        $model->from_email = $from->email;

        $model->to_contactName = $to->name;
        $model->to_businessName = $to->company;
        $model->to_countryCode = $to->country0->code;
        $model->to_postCode = $to->zip_code;
        $model->to_postalTown = $to->city;
        $model->to_address1 = $to->address_line_1;
        $model->to_address2 = $to->address_line_2;
        $model->to_telephone = $to->tel;
        $model->to_email = $to->email;

        $model->goodsDescription1 = $courierTypeU->courier->package_content;

        $model->items = $courierTypeU->courier->packages_number;
        $model->value = $courierTypeU->courier->getPackageValueConverted('EUR');

        $model->codAmount = $courierTypeU->courier->cod_value;

        if($courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_UKMAIL) {

            if (!in_array($from->country_id, [CountryList::COUNTRY_UK, CountryList::COUNTRY_IE]) OR !in_array($to->country_id, [CountryList::COUNTRY_UK, CountryList::COUNTRY_IE])) {
                // int:
                $model->_type = self::TYPE_INT;

                // default
                $model->_serviceKey = self::getIntServiceCode(self::INT_ROAD);

                if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_AIR)) {
                    $model->_serviceKey = self::getIntServiceCode(self::INT_AIR);
                }
            } else {
                // domestic:
                $model->_type = self::TYPE_DOMESTIC;

                // default
                $signature = self::SIGNATURE_AND_NEIGHBOUR;

                if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_SPECIFIED_ADDR_ONLY)) {
                    $signature = self::SIGNATURE_ONLY;
                } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_LEAVE_SAFE_OR_SIG)) {
                    $signature = self::SAFE_OR_SIGNATURE_NEIGHBOUR;
                }

                $add = self::ADD_NEXT;

                if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_9)) {
                    $add = self::ADD_NEXT_9;
                } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_1030)) {
                    $add = self::ADD_NEXT_1030;
                } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_12)) {
                    $add = self::ADD_NEXT_12;
                } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_SATURDAY)) {
                    $add = self::ADD_NEXT_SATURDAY;
                } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_SATURDAY_9)) {
                    $add = self::ADD_NEXT_SATURDAY_9;
                } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_SATURDAY_1030)) {
                    $add = self::ADD_NEXT_SATURDAY_1030;
                } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_48)) {
                    $add = self::ADD_48;
                } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_48_PLUS)) {
                    $add = self::ADD_48PLUS;
                }

                $model->_serviceKey = self::getDomesticServiceCode($signature, $add);
            }
        }
        else if($courierTypeU->courierUOperator->uniq_id == CourieruOperator::UNIQID_UKMAIL_3RD)
        {
            $model->_type = self::TYPE_3RD;

            $add = self::ADD_NEXT;
            if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_9)) {
                $add = self::ADD_NEXT_9;
            } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_1030)) {
                $add = self::ADD_NEXT_1030;
            } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_12)) {
                $add = self::ADD_NEXT_12;
            } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_SATURDAY)) {
                $add = self::ADD_NEXT_SATURDAY;
            }

            $model->_serviceKey = self::get3rdServiceCode($add);
        }
        else if($courierTypeU->courierUOperator->uniq_id == CourieruOperator::UNIQID_UKMAIL_RETURN)
        {
            $model->_type = self::TYPE_RETURN;

            $add = self::ADD_NEXT;
            if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_9)) {
                $add = self::ADD_NEXT_9;
            } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_1030)) {
                $add = self::ADD_NEXT_1030;
            } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_NEXT_DAY_12)) {
                $add = self::ADD_NEXT_12;
            } else if ($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UKMAIL_SATURDAY)) {
                $add = self::ADD_NEXT_SATURDAY;
            }

            $model->_serviceKey = self::getReturnServiceCode($add);
        }

        if(!$model->_serviceKey)
        {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'This combination of services in invalid!',
                )];
                return $return;
            } else {
                return false;
            }
        }


        return $model->orderPackage($courierTypeU->courier, $returnError);
    }

    protected static function addressLinesConverter($addressLine1, $addressLine2, $getLineNo)
    {
        if(mb_strlen($addressLine1) <= 40 && mb_strlen($addressLine2) <= 40)
        {
            if($getLineNo == 0)
                return $addressLine1;
            else if($getLineNo == 1)
                return $addressLine2;
            else if($getLineNo == 2)
                return '';

        } else {

            $address = trim($addressLine1) . ' ' . trim($addressLine2);
            $address = trim($address);

            $newAddress = [];
            $newAddress[0] = mb_substr($address, 0, 40);
            $newAddress[1] = mb_substr($address, 40, 40);
            $newAddress[2] = mb_substr($address, 80, 40);

            return trim($newAddress[$getLineNo]);

        }
    }

    public static function test()
    {

        $model = new self;
        $model->from_address1 = '15-17 Test Street';
        $model->from_countryCode = 'GB';
        $model->from_postalTown = 'London';
        $model->from_postCode = 'W1T 1RJ';
        $model->ContactName = 'Jan Nowak';
        $model->BusinessName = 'Nowak Inc';
        $model->Email = 'asddsaasd@dsasdasad.pl';
        $model->Telephone = '13213212313';
        $model->from_contactName = 'abc';

        $model->items = 2;
        $model->weight = 2;
        $model->lenght = 2;
        $model->width = 2;
        $model->height = 2;

//        $model = new self;
//        $model->from_address1 = 'Test street 1';
//        $model->from_countryCode = 'PL';
//        $model->from_postalTown = 'Wroclaw';
//        $model->from_postCode = '53-416';
//        $model->ContactName = 'Jan Nowak';
//        $model->BusinessName = 'Nowak Inc';
//        $model->Email = 'asddsaasd@dsasdasad.pl';
//        $model->Telephone = '13213212313';
//        $model->contactName = 'abc';

//        $model = new self;
//        $model->from_address1 = '35, rue du Faubourg St Honoré';
//        $model->from_countryCode = 'PL';
//        $model->from_postalTown = 'Paris';
//        $model->from_postCode = '75383';
//        $model->contactName = 'British Embassy Paris';
//        $model->businessName = 'British Embassy Paris';

        return $model->orderPackage();
    }

    protected function orderPackage(Courier $courier, $returnError = true, $tryNo = 0)
    {
        $data = new stdClass();
        $data->request = new stdClass();

        $fromName = $fromCompany = '';
        if($this->from_contactName == $this->from_businessName)
            $fromName = $this->from_contactName;
        else if($this->from_contactName == '')
        {
            $fromName = $this->from_businessName;
        } else {
            $fromName = $this->from_contactName;
            $fromCompany = $this->from_businessName;
        }

        $toName = $toCompany = '';
        if($this->to_contactName == $this->to_businessName)
            $toName = $this->to_contactName;
        else if($this->to_contactName == '')
        {
            $toName = $this->to_businessName;
        } else {
            $toName = $this->to_contactName;
            $toCompany = $this->to_businessName;
        }

        if($this->_type == self::TYPE_DOMESTIC) {
            $serviceName = 'AddDomesticConsignment';

            $data->request->ContactName = $toName;
            $data->request->BusinessName = $toCompany;

            $data->request->Address = new stdClass();
            $data->request->Address->Address1 = self::addressLinesConverter($this->to_address1, $this->to_address2, 0);
            $data->request->Address->Address2 = self::addressLinesConverter($this->to_address1, $this->to_address2, 1);
            $data->request->Address->Address3 = self::addressLinesConverter($this->to_address1, $this->to_address2, 2);

            $data->request->Address->PostalTown = $this->to_postalTown;
            $data->request->Address->Postcode = $this->to_postCode;
            $data->request->Address->CountryCode = self::countryCode2To3($this->to_countryCode);
            $data->request->Email = $this->to_email;
            $data->request->Telephone = $this->to_telephone;
            $data->request->PreDeliveryNotification = 'NonRequired';

            $customerRef = $this->customersRef;
            if(mb_strlen($customerRef) > 17)
            {
                $customerRef1 = mb_substr($customerRef,0,14).'...';
                $customerRef2 = '...'.mb_substr($customerRef,14,99);
            } else {
                $customerRef1 = $customerRef;
                $customerRef2 = mb_substr($fromName.','.$this->from_postalTown,0,20);
            }

            $data->request->CustomersRef = $customerRef1;
            $data->request->AlternativeRef = $customerRef2;


            $data->request->Items = $this->items;
            $data->request->Weight = ceil($this->weight);

            $data->request->ConfirmationOfDelivery = false;
            $data->request->ExchangeOnDelivery = false;
            $data->request->ExtendedCover = false;
            $data->request->SignatureOptional = false;
            $data->request->BookIn = false;
            $data->request->CODAmount = $this->codAmount > 0 ? $this->codAmount : 0;
            $data->request->LongLength = ($this->height > 140 OR $this->lenght > 150 OR $this->width > 140) ? true : false;

            $collectionNo = self::getCollectionNumber();
            if (!$collectionNo->success) {

                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => $collectionNo->error,
                    )];
                    return $return;
                } else {
                    return false;
                }
            }
            $data->request->CollectionJobNumber = $collectionNo->data->collectionNo;

        }
        else if($this->_type == self::TYPE_INT)
        {
            $serviceName = 'AddInternationalConsignment';

            $data->request->ContactName = $toName;
            $data->request->BusinessName = $toCompany;

            $data->request->Address = new stdClass();
            $data->request->Address->Address1 = self::addressLinesConverter($this->to_address1, $this->to_address2, 0);
            $data->request->Address->Address2 = self::addressLinesConverter($this->to_address1, $this->to_address2, 1);
            $data->request->Address->Address3 = self::addressLinesConverter($this->to_address1, $this->to_address2, 2);
            $data->request->Address->PostalTown = $this->to_postalTown;
            $data->request->Address->County = $this->to_countryCode == 'US' ? UsaZipState::getStateByZip($this->to_postCode) : '';
            $data->request->Address->Postcode = $this->to_postCode;
            $data->request->Address->CountryCode = self::countryCode2To3($this->to_countryCode);

            $data->request->Email = $this->to_email;
            $data->request->Telephone = $this->to_telephone;

            $data->request->CustomersRef = $this->customersRef;
            $data->request->AlternativeRef = mb_substr($fromName.','.$this->from_postalTown,0,20);
            $data->request->Items = $this->items;
            $data->request->Weight = $this->weight;

            $data->request->ConfirmationOfDelivery = false;
            $data->request->ExtendedCoverRequired = false;
            $data->request->GoodsDescription1 = $this->goodsDescription1;
            $data->request->DocumentsOnly = false;
            $data->request->Value = $this->value;
            $data->request->CurrencyCode = 'EUR';

            $data->request->Height = $this->height;
            $data->request->Length = $this->lenght;
            $data->request->Width = $this->width;

            $data->request->NoDangerousGoods = true;
            $data->request->InFreeCirculationEU = true;
            $data->request->InvoiceType = 'Commercial';

            $collectionNo = self::getCollectionNumber();
            if (!$collectionNo->success) {

                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => $collectionNo->error,
                    )];
                    return $return;
                } else {
                    return false;
                }
            }
            $data->request->CollectionJobNumber = $collectionNo->data->collectionNo;

        }
        else  if($this->_type == self::TYPE_RETURN)
        {
            $serviceName = 'AddReturnConsignment';


            $data->request->CollectionContactName = $fromName;
            $data->request->CollectionBusinessName = $fromCompany;

            $data->request->Address = new stdClass();
            $data->request->Address->Address1 = self::addressLinesConverter($this->from_address1, $this->from_address2, 0);
            $data->request->Address->Address2 = self::addressLinesConverter($this->from_address1, $this->from_address2, 1);
            $data->request->Address->Address3 = self::addressLinesConverter($this->from_address1, $this->from_address2, 2);
            $data->request->Address->PostalTown = $this->from_postalTown;
            $data->request->Address->Postcode = $this->from_postCode;
            $data->request->Address->County = $this->from_countryCode == 'US' ? UsaZipState::getStateByZip($this->from_postCode) : '';
            $data->request->Address->CountryCode = self::countryCode2To3($this->from_countryCode);

            $data->request->CollectionEmail = $this->from_email;
            $data->request->CollectionTelephone = $this->from_telephone;

            $data->request->CollectionCustomersRef = $this->customersRef;

            //
            $offsetDate = 0;
            $collectionDate = new DateTime();

            $collectionStartTime = 'T'.date('H:i:s').'.000';
            if($offsetDate OR ((date('H') == 16 && date('i') > 59) OR date('H') > 16))
            {
                if(!$offsetDate)
                    $offsetDate++;

                $collectionStartTime = 'T09:00:00.000';
            }

            if($offsetDate)
                $collectionDate->modify('+'.$offsetDate.' day');

            $collectionDate = $collectionDate->format('Y-m-d');
            $collectionEndTime = 'T17:00:00.000';

            $data->request->CollectionDate = $collectionDate.$collectionStartTime;
            $data->request->CollectionTimeReady =  $collectionDate.$collectionStartTime;
            $data->request->CollectionOpenLunchtime = true;
            $data->request->CollectionLatestPickup = $collectionDate.$collectionEndTime;
            //

            $data->request->BookIn = false;
        }
        else if($this->_type == self::TYPE_3RD)
        {
            $serviceName = 'AddSendToThirdParty';

            //
            $offsetDate = 0;
            $collectionDate = new DateTime();

            $collectionStartTime = 'T'.date('H:i:s').'.000';
            if($offsetDate OR ((date('H') == 16 && date('i') > 59) OR date('H') > 16))
            {
                if(!$offsetDate)
                    $offsetDate++;

                $collectionStartTime = 'T09:00:00.000';
            }

            if($offsetDate)
                $collectionDate->modify('+'.$offsetDate.' day');

            $collectionDate = $collectionDate->format('Y-m-d');
            $collectionEndTime = 'T17:00:00.000';

            $data->request->CollectionDate = $collectionDate.$collectionStartTime;
            $data->request->CollectionTimeReady =  $collectionDate.$collectionStartTime;
            $data->request->CollectionOpenLunchtime = true;
            $data->request->CollectionLatestPickup = $collectionDate.$collectionEndTime;
            //

            $data->request->CollectionContactName = $fromName;
            $data->request->CollectionBusinessName = $fromCompany;

            $data->request->CollectionAddress = new stdClass();
            $data->request->CollectionAddress->Address1 = self::addressLinesConverter($this->from_address1, $this->from_address2, 0);
            $data->request->CollectionAddress->Address2 = self::addressLinesConverter($this->from_address1, $this->from_address2, 1);
            $data->request->CollectionAddress->Address3 = self::addressLinesConverter($this->from_address1, $this->from_address2, 2);
            $data->request->CollectionAddress->PostalTown = $this->from_postalTown;
            $data->request->CollectionAddress->Postcode = $this->from_postCode;
            $data->request->CollectionAddress->County = $this->from_countryCode == 'US' ? UsaZipState::getStateByZip($this->from_postCode) : '';
            $data->request->CollectionAddress->CountryCode = self::countryCode2To3($this->from_countryCode);
            $data->request->CollectionEmail = $this->from_email;
            $data->request->CollectionTelephone = $this->from_telephone;

            $data->request->CollectionCustomersRef = $this->customersRef;
            $data->request->BookIn = false;

            $data->request->DeliveryContactName = $toName;
            $data->request->DeliveryBusinessName = $toCompany;
            $data->request->DeliveryAddress = new stdClass();
            $data->request->DeliveryAddress->Address1 = self::addressLinesConverter($this->to_address1, $this->to_address2, 0);
            $data->request->DeliveryAddress->Address2 = self::addressLinesConverter($this->to_address1, $this->to_address2, 1);
            $data->request->DeliveryAddress->Address3 = self::addressLinesConverter($this->to_address1, $this->to_address2, 2);
            $data->request->DeliveryAddress->PostalTown = $this->to_postalTown;
            $data->request->DeliveryAddress->Postcode = $this->to_postCode;
            $data->request->DeliveryAddress->County = $this->to_countryCode == 'US' ? UsaZipState::getStateByZip($this->to_postCode) : '';
            $data->request->DeliveryAddress->CountryCode = self::countryCode2To3($this->to_countryCode);
            $data->request->DeliveryEmail = $this->from_email;
            $data->request->DeliveryTelephone = $this->from_telephone;

        } else {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'Unknown service!',
                )];
                return $return;
            } else {
                return false;
            }
        }
        $data->request->ServiceKey = $this->_serviceKey;

        $token = self::getAuthToken();
        if(!$token->success) {

            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $token->errorDesc,
                )];
                return $return;
            } else {
                return false;
            }
        }

        $data->request->Username = self::USERNAME;
        $data->request->AuthenticationToken = $token->data->token;
        $data->request->AccountNumber = self::ACCOUNT_NO;

        $result = self::go($serviceName, 'UKMConsignmentServices/UKMConsignmentService.svc?wsdl', $data);

        $return = [];

        $resultNode = $serviceName.'Result';

        // no soap error but maybe error in response?
        if($result->success)
        {
            if($result->data->$resultNode->Result != 'Successful')
            {

                $resultNode = $result->data->$resultNode->Errors->UKMWebError;
                if(!is_array($resultNode))
                    $resultNode = [$resultNode];

                $result->success = false;
                $result->error = $resultNode[0]->Description;
                $result->errorDesc = serialize($result->data->$resultNode->Errors);
                $result->errorCode = $resultNode[0]->Code;
                $result->data = NULL;

                if(in_array($resultNode[0]->Code, ['2050','16384']))
                {
                    self::_clearToken();
                    if(!$tryNo)
                    {
                        MyDump::dump('ukmail.txt', 'RETRY!');
                        return self::orderPackage($courier, $returnError, 1);
                    }
                }
            }
        }

        if($result->success == true)
        {
            $trackId = $result->data->$resultNode->ConsignmentNumber;



            if(in_array($serviceName, ['AddSendToThirdParty', 'AddReturnConsignment']))
            {
                $labels = [];
                $labels[] = KaabLabel::generate($courier, 1, $courier->senderAddressData, $courier->receiverAddressData, false, 1, true, 'VIA UK MAIL ('.$trackId.')');

            }
            else
            {

                $labels = $result->data->$resultNode->Labels->base64Binary;
                if (!is_array($labels))
                    $labels = [$labels];

                if (!S_Useful::sizeof($labels))
                    if ($returnError == true) {
                        $return = [0 => array(
                            'error' => 'Seems ok, but no labels from UK POST...',
                        )];
                        return $return;
                    } else {
                        return false;
                    }

            }

            $i = 0;

            foreach ($labels AS $item) {
                $return[] = [
                    'status' => true,
                    'label' => $item,
                    'track_id' => $trackId,
                ];
                $i++;
            }

            return $return;
        } else {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $result->error,
                )];
                return $return;
            } else {
                return false;
            }
        }
    }

    protected static function go($action, $service, $params)
    {

        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),

        );

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        MyDump::dump('ukpost-rr.txt', print_r($params,1));
        try
        {

            $client = new SoapClient(self::WSDL_PREFIX.$service, $mode);
            $resp = $client->__soapCall($action, [$action => $params]);

            MyDump::dump('ukpost-rr.txt', print_r($resp,1));

        } catch (SoapFault $E) {

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('ukpost-rr.txt', print_r($E,1));

            return $return;

        } catch (Exception $E) {

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;

            MyDump::dump('ukpost-rr.txt', print_r($E,1));

            return $return;
        }

        $return->success = true;
        $return->data = $resp;

        return $return;
    }

    public static function track($number)
    {

        $data = new stdClass();

        $token = self::getAuthToken();
        if(!$token->success)
            return false;

        $data->UserName = self::USERNAME;
        $data->Password = self::PASSWORD;
        $data->Token = $token->data->token;
        $data->ConsignmentNumber = $number;
        $data->ConsignmentKey = 0;
//        $data->MaxResults = 99;

        $mode = array
        (
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),

        );


        try
        {

            $client = new SoapClient(self::WSDL_TT, $mode);
            $resp = $client->__soapCall('ConsignmentTrackingGetFullDetailsV3', ['ConsignmentTrackingGetFullDetailsV3' => $data]);

        } catch (SoapFault $E) {
            return false;
        } catch (Exception $E) {
            return false;
        }


        if($resp && $resp->ConsignmentTrackingGetFullDetailsV3Result->ResultState == 'Successful' )
        {
            $statuses = [];

            // basedOnStatuses
            $data = $resp->ConsignmentTrackingGetFullDetailsV3Result->FullConsignmentDetails->ConsignmentStatuses->GetConsignmentDetailsStatus;
            if($data) {
                if(!is_array($data))
                    $data = [ $data ];

                foreach ($data AS $item) {

                    $status = (string) $item->StatusDescription;
                    $date = (string) $item->StatusTimeStamp;
                    $location = NULL;

                    if($status == 'Delivered') {

                        $location = (string)          $resp->ConsignmentTrackingGetFullDetailsV3Result->FullConsignmentDetails->ConsignmentPods->GetFullConsignmentDetailsPodV2->PodDescription .' (' . (string)          $resp->ConsignmentTrackingGetFullDetailsV3Result->FullConsignmentDetails->ConsignmentPods->GetFullConsignmentDetailsPodV2->PodRecipientName . ')';
                    }

                    if ($status == '')
                        continue;

                    $statuses[strtotime($date)] = [
                        'name' => $status,
                        'date' => $date,
                        'location' => $location,
                    ];
                }


            }

            // BASED ON SCANS:
            $data = $resp->ConsignmentTrackingGetFullDetailsV3Result->FullConsignmentDetails->ConsignmentScans->ConsignmentScan;

            if($data) {
                if(!is_array($data))
                    $data = [ $data ];

                foreach ($data AS $item) {

                    $status = (string) $item->ScanType;
                    $date = (string) $item->ScanTimeStamp;
                    $location = (string) $item->ScanLocation;

                    if ($status == '')
                        continue;

                    $statuses[strtotime($date)] = [
                        'name' => $status,
                        'date' => $date,
                        'location' => $location,
                    ];
                }
            }

            // sort by date
            ksort($statuses);

            return $statuses;
        }

        return false;
    }

    /**
     * @param string $number
     * @return bool|string Returns false or PDF string with ePod
     */
    public static function epod($number)
    {
        $CACHE_NAME = 'UKMAIL_EPOD_' . $number;
        $result = Yii::app()->cacheUserData->get($CACHE_NAME);

        if ($result === false) {

            $data = new stdClass();

            $token = self::getAuthToken();
            if(!$token->success)
                return false;

            $data->UserName = self::USERNAME;
            $data->Password = self::PASSWORD;
            $data->Token = $token->data->token;
            $data->ConsignmentNumber = $number;
            $data->ConsignmentKey = 0;

            $mode = array
            (
                'soap_version' => SOAP_1_1,
                'trace' => 1,
                'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
            );

            try
            {

                $client = new SoapClient(self::WSDL_TT, $mode);
                $resp = $client->__soapCall('ConsignmentTrackingGetFullDetailsV3', ['ConsignmentTrackingGetFullDetailsV3' => $data]);

            } catch (SoapFault $E) {
                return false;
            } catch (Exception $E) {
                return false;
            }


            if($resp && $resp->ConsignmentTrackingGetFullDetailsV3Result->ResultState == 'Successful' )
            {

                $img = $resp->ConsignmentTrackingGetFullDetailsV3Result->FullConsignmentDetails->Signatures->SignatureDetails->SignatureImageArray;

                if($img) {

                    // GENERATE OWN:
                    $pdf = PDFGenerator::initialize();
                    $pdf->setFontSubsetting(false);

                    $pdf->addPage('H', 'A4');
                    $width = $pdf->getPageWidth() / 2;
                    $pdf->Image('@' . ($img), 5, 5, $width, '', '', 'N', false);

                    $result = $pdf->Output('epod.pdf', 'S');

                    Yii::app()->cacheUserData->set($CACHE_NAME, $result, (60*60*24));
                }
            }
        }

        return $result;
    }


    protected static function countryCode2To3($code)
    {
        $table = [
            'AX' => 'ALA',
            'AF' => 'AFG',
            'AL' => 'ALB',
            'DZ' => 'DZA',
            'AS' => 'ASM',
            'AD' => 'AND',
            'AO' => 'AGO',
            'AI' => 'AIA',
            'AQ' => 'ATA',
            'AG' => 'ATG',
            'AR' => 'ARG',
            'AM' => 'ARM',
            'AW' => 'ABW',
            'AU' => 'AUS',
            'AT' => 'AUT',
            'AZ' => 'AZE',
            'BS' => 'BHS',
            'BH' => 'BHR',
            'BD' => 'BGD',
            'BB' => 'BRB',
            'BY' => 'BLR',
            'BE' => 'BEL',
            'BZ' => 'BLZ',
            'BJ' => 'BEN',
            'BM' => 'BMU',
            'BT' => 'BTN',
            'BO' => 'BOL',
            'BA' => 'BIH',
            'BW' => 'BWA',
            'BV' => 'BVT',
            'BR' => 'BRA',
            'IO' => 'IOT',
            'BN' => 'BRN',
            'BG' => 'BGR',
            'BF' => 'BFA',
            'BI' => 'BDI',
            'KH' => 'KHM',
            'CM' => 'CMR',
            'CA' => 'CAN',
            'CV' => 'CPV',
            'KY' => 'CYM',
            'CF' => 'CAF',
            'TD' => 'TCD',
            'CL' => 'CHL',
            'CN' => 'CHN',
            'CX' => 'CXR',
            'CC' => 'CCK',
            'CO' => 'COL',
            'KM' => 'COM',
            'CD' => 'COD',
            'CG' => 'COG',
            'CK' => 'COK',
            'CR' => 'CRI',
            'CI' => 'CIV',
            'HR' => 'HRV',
            'CU' => 'CUB',
            'CY' => 'CYP',
            'CZ' => 'CZE',
            'DK' => 'DNK',
            'DJ' => 'DJI',
            'DM' => 'DMA',
            'DO' => 'DOM',
            'EC' => 'ECU',
            'EG' => 'EGY',
            'SV' => 'SLV',
            'GQ' => 'GNQ',
            'ER' => 'ERI',
            'EE' => 'EST',
            'ET' => 'ETH',
            'FK' => 'FLK',
            'FO' => 'FRO',
            'FJ' => 'FJI',
            'FI' => 'FIN',
            'FR' => 'FRA',
            'GF' => 'GUF',
            'PF' => 'PYF',
            'TF' => 'ATF',
            'GA' => 'GAB',
            'GM' => 'GMB',
            'GE' => 'GEO',
            'DE' => 'DEU',
            'GH' => 'GHA',
            'GI' => 'GIB',
            'GR' => 'GRC',
            'GL' => 'GRL',
            'GD' => 'GRD',
            'GP' => 'GLP',
            'GU' => 'GUM',
            'GT' => 'GTM',
            'GN' => 'GIN',
            'GW' => 'GNB',
            'GY' => 'GUY',
            'HT' => 'HTI',
            'HM' => 'HMD',
            'HN' => 'HND',
            'HK' => 'HKG',
            'HU' => 'HUN',
            'IS' => 'ISL',
            'IN' => 'IND',
            'ID' => 'IDN',
            'IR' => 'IRN',
            'IQ' => 'IRQ',
            'IE' => 'IRL',
            'IL' => 'ISR',
            'IT' => 'ITA',
            'JM' => 'JAM',
            'JP' => 'JPN',
            'JO' => 'JOR',
            'KZ' => 'KAZ',
            'KE' => 'KEN',
            'KI' => 'KIR',
            'KP' => 'PRK',
            'KR' => 'KOR',
            'KW' => 'KWT',
            'KG' => 'KGZ',
            'LA' => 'LAO',
            'LV' => 'LVA',
            'LB' => 'LBN',
            'LS' => 'LSO',
            'LR' => 'LBR',
            'LY' => 'LBY',
            'LI' => 'LIE',
            'LT' => 'LTU',
            'LU' => 'LUX',
            'MO' => 'MAC',
            'MK' => 'MKD',
            'MG' => 'MDG',
            'MW' => 'MWI',
            'MY' => 'MYS',
            'MV' => 'MDV',
            'ML' => 'MLI',
            'MT' => 'MLT',
            'MH' => 'MHL',
            'MQ' => 'MTQ',
            'MR' => 'MRT',
            'MU' => 'MUS',
            'YT' => 'MYT',
            'MX' => 'MEX',
            'FM' => 'FSM',
            'MD' => 'MDA',
            'MC' => 'MCO',
            'MN' => 'MNG',
            'MS' => 'MSR',
            'MA' => 'MAR',
            'MZ' => 'MOZ',
            'MM' => 'MMR',
            'NA' => 'NAM',
            'NR' => 'NRU',
            'NP' => 'NPL',
            'NL' => 'NLD',
            'AN' => 'ANT',
            'NC' => 'NCL',
            'NZ' => 'NZL',
            'NI' => 'NIC',
            'NE' => 'NER',
            'NG' => 'NGA',
            'NU' => 'NIU',
            'NF' => 'NFK',
            'MP' => 'MNP',
            'NO' => 'NOR',
            'OM' => 'OMN',
            'PK' => 'PAK',
            'PW' => 'PLW',
            'PS' => 'PSE',
            'PA' => 'PAN',
            'PG' => 'PNG',
            'PY' => 'PRY',
            'PE' => 'PER',
            'PH' => 'PHL',
            'PN' => 'PCN',
            'PL' => 'POL',
            'PT' => 'PRT',
            'PR' => 'PRI',
            'QA' => 'QAT',
            'RE' => 'REU',
            'RO' => 'ROU',
            'RU' => 'RUS',
            'RW' => 'RWA',
            'SH' => 'SHN',
            'KN' => 'KNA',
            'LC' => 'LCA',
            'PM' => 'SPM',
            'VC' => 'VCT',
            'WS' => 'WSM',
            'SM' => 'SMR',
            'ST' => 'STP',
            'SA' => 'SAU',
            'SN' => 'SEN',
            'CS' => 'SCG',
            'SC' => 'SYC',
            'SL' => 'SLE',
            'SG' => 'SGP',
            'SK' => 'SVK',
            'SI' => 'SVN',
            'SB' => 'SLB',
            'SO' => 'SOM',
            'ZA' => 'ZAF',
            'GS' => 'SGS',
            'ES' => 'ESP',
            'LK' => 'LKA',
            'SD' => 'SDN',
            'SR' => 'SUR',
            'SJ' => 'SJM',
            'SZ' => 'SWZ',
            'SE' => 'SWE',
            'CH' => 'CHE',
            'SY' => 'SYR',
            'TW' => 'TWN',
            'TJ' => 'TJK',
            'TZ' => 'TZA',
            'TH' => 'THA',
            'TL' => 'TLS',
            'TG' => 'TGO',
            'TK' => 'TKL',
            'TO' => 'TON',
            'TT' => 'TTO',
            'TN' => 'TUN',
            'TR' => 'TUR',
            'TM' => 'TKM',
            'TC' => 'TCA',
            'TV' => 'TUV',
            'UG' => 'UGA',
            'UA' => 'UKR',
            'AE' => 'ARE',
            'GB' => 'GBR',
            'US' => 'USA',
            'UM' => 'UMI',
            'UY' => 'URY',
            'UZ' => 'UZB',
            'VU' => 'VUT',
            'VA' => 'VAT',
            'VE' => 'VEN',
            'VN' => 'VNM',
            'VG' => 'VGB',
            'VI' => 'VIR',
            'WF' => 'WLF',
            'EH' => 'ESH',
            'YE' => 'YEM',
            'ZM' => 'ZMB',
            'ZW' => 'ZWE',
        ];

        return isset($table[$code]) ? $table[$code] : false;
    }

}