<?php

class CourierExternalManager
{
    const TYPE_OTHER = 1;
    const TYPE_UPS = 2;
    const TYPE_OOE = 3;
    const TYPE_INPOST = 4;
    const TYPE_SAFEPAK = 5;
    const TYPE_GLS = 6;
    const TYPE_RUCH = 7;
    const TYPE_DHL = 8;
    const TYPE_PETER_SK = 9;
    const TYPE_POCZTA_POLSKA = 10;
    const TYPE_GLS_NL = 11;
    const TYPE_DHLDE = 12;
    const TYPE_DHLEXPRESS = 13;
    const TYPE_UK_MAIL = 14;
    const TYPE_DPD_DE = 15;
    const TYPE_ROYAL_MAIL = 16;
    const TYPE_DPDPL = 17;
    const TYPE_HUNGARIAN_POST = 18;
    const TYPE_DPDNL = 19;
    const TYPE_POST11 = 20;
    const TYPE_LP_EXPRESS = 21;

    const TYPE_17TRACK = 25;

    public static function getNameForType($type)
    {
        $name = '';

        if(in_array($type, array_keys(GLOBAL_BROKERS::getBrokersList()))) // for items from GLOBAL_BROKERS
            return GLOBAL_BROKERS::getBrokerNameById($type);
        else
            switch($type)
            {
                case self::TYPE_UPS:
                    $name = 'UPS';
                    break;
                case self::TYPE_OOE:
                    $name = 'OOE';
                    break;
                case self::TYPE_INPOST:
                    $name = 'Inpost';
                    break;
                case self::TYPE_RUCH:
                    $name = 'Ruch';
                    break;
                case self::TYPE_PETER_SK:
                    $name = 'KAAB_SK';
                    break;
                case self::TYPE_GLS:
                    $name = 'GLS';
                    break;
                case self::TYPE_SAFEPAK:
                    $name = 'Safepak';
                    break;
                case self::TYPE_DHL:
                    $name = 'DHL';
                    break;
                case self::TYPE_POCZTA_POLSKA:
                    $name = 'Poczta Polska';
                    break;
                case self::TYPE_GLS_NL:
                    $name = 'GLS NL';
                    break;
                case self::TYPE_DHLDE:
                    $name = 'DHL DE';
                    break;
                case self::TYPE_DHLEXPRESS:
                    $name = 'DHL EXPRESS';
                    break;
                case self::TYPE_UK_MAIL:
                    $name = 'Uk Mail';
                    break;
                case self::TYPE_DPD_DE:
                    $name = 'DPD DE';
                    break;
                case self::TYPE_ROYAL_MAIL:
                    $name = 'Royal Mail';
                    break;
                case self::TYPE_DPDPL:
                    $name = 'DPD PL';
                    break;
                case self::TYPE_HUNGARIAN_POST:
                    $name = 'HUNGARIAN POST';
                    break;
                case self::TYPE_DPDNL:
                    $name = 'DPD NL';
                    break;
                case self::TYPE_POST11:
                    $name = 'Post11';
                    break;
                case self::TYPE_LP_EXPRESS:
                    $name = 'LP Express';
                    break;
                case self::TYPE_OTHER:
                    $name = 'OTHER';
                    break;
            }

        return $name;

    }

    public static function sortArray(array $data)
    {
        usort($data, array("CourierExternalManager", "sortFunctionReversed"));
        return $data;
    }

    protected static function sortFunction( $a, $b ) {
        return strtotime($b["date"]) - strtotime($a["date"]);
    }

    public static function sortArrayNewestFirst(array $data)
    {

        usort($data, array("CourierExternalManager", "sortFunction"));

        return $data;
    }

    protected static function sortFunctionReversed( $a, $b ) {
        return strtotime($a["date"]) - strtotime($b["date"]);
    }

    public static function sortArrayOldestFirst(array $data)
    {
        usort($data, array("CourierExternalManager", "sortFunctionReversed"));
        return $data;
    }

    public static function listExternalServices(Courier $courier)
    {

        $externalData = array();

        if($courier->getType() == Courier::TYPE_INTERNAL_OOE)
        {

            if($courier->courierTypeOoe->courierLabelNew != NULL)
            {
                $type = self::courierLabelNewOperatorToCourierExternalManagerOperator($courier->courierTypeOoe->courierLabelNew->operator);
                $temp = array(
                    'type' => $type,
                    'id' => $courier->courierTypeOoe->courierLabelNew->track_id,
                    'courierLabelNew' => $courier->courierTypeOoe->courierLabelNew,
                );
                array_push($externalData, $temp);
            }
            // for old compatilibity:
            else if($courier->courierTypeOoe->remote_id != '')
            {
                $temp = array(
                    'type' => self::TYPE_OOE,
                    'id' => $courier->courierTypeOoe->remote_id
                );
                array_push($externalData, $temp);
            }
            return $externalData;

        }
        else if($courier->getType() == Courier::TYPE_INTERNAL)
        {
            if($courier->courierTypeInternal->pickupLabel->track_id !='')
            {

                $type = self::courierLabelNewOperatorToCourierExternalManagerOperator($courier->courierTypeInternal->pickupLabel->courierLabelNew->operator);

                $temp = array(
                    'type' => $type,
                    'id' => $courier->courierTypeInternal->pickupLabel->track_id,
                    'courierLabelNew' => $courier->courierTypeInternal->pickupLabel->courierLabelNew,
//                            'justPickup' => true, // not to send notifications about stat updates from first operator

                );


                // if there is also delivery operator, than convert "DELIVERED" status from first operator to "DELIVERED_TO_HUB"
                if($courier->courierTypeInternal->delivery_operator_ordered) {
                    $temp['convertDeliveryStatus'] = true;
                }

                array_push($externalData, $temp);
            }

            if($courier->courierTypeInternal->deliveryLabel->track_id != '')
            {
                $type = self::courierLabelNewOperatorToCourierExternalManagerOperator($courier->courierTypeInternal->deliveryLabel->courierLabelNew->operator);

                $temp = array(
                    'type' => $type,
                    'id' => $courier->courierTypeInternal->deliveryLabel->track_id,
                    'courierLabelNew' => $courier->courierTypeInternal->deliveryLabel->courierLabelNew,
                );


                array_push($externalData, $temp);
            }

            if($courier->courierTypeInternal->commonLabel->track_id != '')
            {


                $type = self::courierLabelNewOperatorToCourierExternalManagerOperator($courier->courierTypeInternal->commonLabel->courierLabelNew->operator);


                $temp = array(
                    'type' => $type,
                    'id' => $courier->courierTypeInternal->commonLabel->track_id,
                    'courierLabelNew' => $courier->courierTypeInternal->commonLabel->courierLabelNew,
                );


                array_push($externalData, $temp);
            }
        }
//        else if($courier->getType() == Courier::TYPE_INTERNAL_SIMPLE)
//        {
//
//            if($courier->courierTypeInternalSimple->courierExtOperatorDetails->courierLabelNew !== NULL)
//            {
//                $type = self::courierLabelNewOperatorToCourierExternalManagerOperator($courier->courierTypeInternalSimple->courierExtOperatorDetails->courierLabelNew->operator);
//
//                $temp = array(
//                    'type' => $type,
//                    'id' => $courier->courierTypeInternalSimple->courierExtOperatorDetails->courierLabelNew->track_id,
//                    'courierLabelNew' => $courier->courierTypeInternalSimple->courierExtOperatorDetails->courierLabelNew,
//                );
//                array_push($externalData, $temp);
//            }
//            // old compatibility:
//            else
//            {
//                $type = self::courierInternalSimpleOperatorToCourierExternalManagerOperator( $courier->courierTypeInternalSimple->operator );
//                if ( $type ) {
//                    if ( $courier->courierTypeInternalSimple->courierExtOperatorDetails->track_id != '' ) {
//                        $temp = array(
//                            'type' => $type,
//                            'id'   => $courier->courierTypeInternalSimple->courierExtOperatorDetails->track_id
//                        );
//                        array_push( $externalData, $temp );
//                    }
//                }
//            }
//
//        }
//        else if($courier->getType() == Courier::TYPE_DOMESTIC)
//        {
//
//            $type = self::courierLabelNewOperatorToCourierExternalManagerOperator($courier->courierTypeDomestic->courierLabelNew->operator);
//
//            if($type)
//            {
//                if($courier->courierTypeDomestic->courierLabelNew->track_id != '')
//                {
//                    $temp = array(
//                        'type' => $type,
//                        'id' => $type != self::TYPE_SAFEPAK ? $courier->courierTypeDomestic->courierLabelNew->track_id : $courier->courierTypeDomestic->courierLabelNew->_safepak_order_id,
//                        'receiver_tel' => $courier->courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_RUCH ? $courier->receiverAddressData->tel : '', // required for RUCH TT
//                        'courierLabelNew' => $courier->courierTypeDomestic->courierLabelNew,
//                    );
//                    array_push($externalData, $temp);
//                }
//            }
//
//
//        }
        else if($courier->getType() == Courier::TYPE_U)
        {
            $type = self::courierLabelNewOperatorToCourierExternalManagerOperator($courier->courierTypeU->courierLabelNew->operator);
            if($courier->courierTypeU->courierLabelNew->track_id != '')
            {
                $temp = array(
                    'type' => $type,
                    'id' => $courier->courierTypeU->courierLabelNew->track_id,
                    'courierLabelNew' => $courier->courierTypeU->courierLabelNew,

                );
                array_push($externalData, $temp);
            }

        }

        else if($courier->getType() == Courier::TYPE_RETURN)
        {
            $type = self::courierLabelNewOperatorToCourierExternalManagerOperator($courier->courierTypeReturn->courierLabelNew->operator);
            if($courier->courierTypeReturn->courierLabelNew->track_id != '')
            {
                $temp = array(
                    'type' => $type,
                    'id' => $courier->courierTypeReturn->courierLabelNew->track_id,
                    'courierLabelNew' => $courier->courierTypeReturn->courierLabelNew,

                );
                array_push($externalData, $temp);
            }



        }

        else if($courier->getType() == Courier::TYPE_EXTERNAL_RETURN)
        {

            if($courier->courierTypeExternalReturn->courierLabelNew->_ext_operator_name != '')
            {
                $temp = array(
                    'type' => $courier->courierTypeExternalReturn->courierLabelNew->_ext_operator_name,
                    'id' => $courier->courierTypeReturn->courierLabelNew->track_id,
                    'courierLabelNew' => $courier->courierTypeReturn->courierLabelNew,

                );
                array_push($externalData, $temp);
            }



        }

        return $externalData;
    }


    public static function updateStatusForPackage(Courier $courier, $blockNotification = false, $forceService = false, $forceTrackingId = false)
    {


        Yii::app()->db->createCommand()->update('tt_log', ['last' => 0], 'item_type = :item_type AND item_id =:item_id AND last = 1', [':item_type' => TtLog::TYPE_COURIER, ':item_id' => $courier->id]);
        Yii::app()->db->createCommand()->insert('tt_log', ['item_type' => TtLog::TYPE_COURIER, 'item_id' => $courier->id, 'last' => 1]);

        if(!$forceService && !$forceTrackingId && in_array($courier->user_id, [
                2125, // EUROPAKA
//            8
            ])) // additional tracking based on ref numbers
        {

            if($courier->user_id == 2125)
            {
                self::updateStatusForPackage($courier, false, self::TYPE_GLS, $courier->ref);
            }
//            else if($courier->user_id == 8)
//            {
//                self::updateStatusForPackage($courier, false, self::TYPE_GLS, $courier->note1);
//            }
        }

        if($forceService && $forceTrackingId)
        {
            $externalServices = [
                [
                    'type' => $forceService,
                    'id' => $forceTrackingId,
                    'convertDeliveryStatus' => true,
                ]
            ];
        } else
            $externalServices = CourierExternalManager::listExternalServices($courier);

        if(!is_array($externalServices) OR !S_Useful::sizeof($externalServices))
            return false;
        else
            foreach($externalServices AS $service)
            {

                if(in_array($service['type'], array_keys(GLOBAL_BROKERS::getBrokersList()))) {

                    $operatorClient = GLOBAL_BROKERS::globalOperatorClientFactory($service['type']);
                    $response = $operatorClient::track($service['id'], $service['courierLabelNew']);

                    if($response instanceof GlobalOperatorTtResponseCollection)
                        $data = $response->adapterToCourierExternalManagerArray();

                    $statusServiceId = $service['type'];
                }
                else if($service['type'] == CourierExternalManager::TYPE_17TRACK) {
                    $data = Client17Track::getTt($service['id'], false, false, false, 'CourierExtMan');

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_17TRACK;
                }
                else if($service['type'] == CourierExternalManager::TYPE_OOE)
                {
                    $data = Yii::app()->OneWorldExpress->track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_OOE;

                } else if($service['type'] == CourierExternalManager::TYPE_UPS) {
                    $data = UpsTrackClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_UPS;
                } else if($service['type'] == CourierExternalManager::TYPE_POCZTA_POLSKA) {
                    $data = PocztaPolskaClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_POCZTA_POLSKA;
                } else if($service['type'] == CourierExternalManager::TYPE_GLS_NL) {
                    $data = GlsNlClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_GLS_NL;
                } else if($service['type'] == CourierExternalManager::TYPE_INPOST) {
                    $data = Inpost::getTracking($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_INPOST;
                } else if($service['type'] == CourierExternalManager::TYPE_RUCH) {
                    $data = PaczkaWRuchu::getTracking($service['id'], $service['receiver_tel']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_RUCH;
                } else if($service['type'] == CourierExternalManager::TYPE_PETER_SK) {
//                    $data = Joy::getTracking($service['id']);

                    $data = Joy::getTracking($courier->local_id);

                    // when there is new external ID from PETER SK and our current external id is still our local ID - update DB with new tracking id provided by PETER SK
                    if($data['external_id'] && $service['id'] == $courier->local_id) {
                        CourierLabelNew::updateExternalId($courier->local_id, CourierLabelNew::OPERATOR_PETER_SK, $data['external_id']);
                    }

                    $data = $data['tt'];

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_PETER_SK;
                } else if($service['type'] == CourierExternalManager::TYPE_DHL) {

                    if($service['courierLabelNew'] instanceof CourierLabelNew)
                        $customOperator = $service['courierLabelNew']->operator;
                    else
                        $customOperator = false;

                    $data = DhlClient::track($service['id'], $customOperator);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_DHL;
                } else if($service['type'] == CourierExternalManager::TYPE_DHLDE) {

                    $data = DhlDeClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_DHLDE;
                } else if($service['type'] == CourierExternalManager::TYPE_DHLEXPRESS) {

                    $data = DhlExpressClient::track($service['id']);

                     $statusServiceId = CourierStat::EXTERNAL_SERVICE_DHLEXPRESS;
                } else if($service['type'] == CourierExternalManager::TYPE_UK_MAIL) {

                    $data = UKMailClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_UK_MAIL;
                } else if($service['type'] == CourierExternalManager::TYPE_DPD_DE) {

                    $data = DpdDeClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_DPD_DE;
                } else if($service['type'] == CourierExternalManager::TYPE_ROYAL_MAIL) {

                    $data = RoyalMailClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_ROYAL_MAIL;

                } else if($service['type'] == CourierExternalManager::TYPE_DPDPL) {

                    $data = DpdPlClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_DPDPL;
                } else if($service['type'] == CourierExternalManager::TYPE_DPDNL) {

                    $data = DpdNlClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_DPDNL;
                } else if($service['type'] == CourierExternalManager::TYPE_POST11) {

                    $data = Post11Client::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_POST11;
                } else if($service['type'] == CourierExternalManager::TYPE_LP_EXPRESS) {

                    $data = LpExpressClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_LP_EXPRESS;
                } else if($service['type'] == CourierExternalManager::TYPE_HUNGARIAN_POST) {

                    $data = HuPostClient::track($service['id']);

                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_HUNGARIAN_POST;

                } else if($service['type'] == CourierExternalManager::TYPE_GLS) {
                    $data = GlsTrackClient::track($service['id']);
                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_GLS;
                } else if($service['type'] == CourierExternalManager::TYPE_SAFEPAK) {
                    $data = Safepak::getTracking($service['id']);
                    $statusServiceId = CourierStat::EXTERNAL_SERVICE_SAFEPAK;
                } else {
                    // no known external services to update
                    continue;
                }

                if(!is_array($data) OR !S_Useful::sizeof($data))
                    continue;
                else
                {
                    $data = CourierExternalManager::sortArray($data);
                    self::processUpdateDate($courier, $data, $blockNotification, $statusServiceId, $service);
                }
            }
        return true;
    }

    public static function processUpdateDate(Courier $courier, array $data, $blockNotification, $statusServiceId, $service)
    {
        // clear empty multidimensional
        $data = array_filter(array_map('array_filter', $data));

        if(!sizeof($data))
            return true;

        // check last status first - it it's already in DB, then we have it ALL
        {
            $mostRecentStatus = end($data);

            $stat_id = CourierStat::getOrCreateAndGetId($mostRecentStatus['name'], $statusServiceId, true, $service['convertDeliveryStatus']);
            if($stat_id) {
                $date_converted = CourierStat::convertDate($mostRecentStatus['date']);

                $cmd = Yii::app()->db->createCommand();
                $cmd->select('COUNT(*)');
                $cmd->from('courier_stat_history');
                $cmd->where('courier_id = :courier_id', array(':courier_id' => $courier->id));

                // because for example Safepak do not provide data for status
                if ($date_converted != '')
                    $cmd->andWhere('status_date = :status_date', array(':status_date' => $date_converted));
                $cmd->andWhere('current_stat_id = :status_id', array(':status_id' => $stat_id));

                $result = $cmd->queryScalar();

                // already in DB, so do not continue to update stat
                if ($result)
                    return true;
            }
        }

        // check if there is "DELIVERED" status, but it's not last
       if(0){
            $hasDeliveredStat = false;
            $deliveredDate = NULL;
            $temp = false;

            $dataBackup = $data; // SHOULD BE COPY?
            foreach ($data AS $key => $item) {
                if (!$hasDeliveredStat) {

                    $stat_id = CourierStat::getOrCreateAndGetId($item['name'], $statusServiceId, true, $service['convertDeliveryStatus']);
                    $map_id = StatMapMapping::findMapForStat($stat_id, StatMapMapping::TYPE_COURIER);

                    if ($map_id == StatMap::MAP_DELIVERED) {
                        $deliveredDate = CourierStat::convertDate($item['date']);
                        $hasDeliveredStat = true;

                        // remove delivered stat from array
                        $temp = $data[$key];
                        unset($data[$key]);
                    }
                } else {
                    // after we already have delivered stat - check if date is not from future - if so - ignore it
                    if (strtotime(CourierStat::convertDate($item['date'])) > strtotime($deliveredDate)) {
                        unset($data[$key]);
                        MyDump::dump('data_fix_delivered.txt', 'COURIER: ' . $courier->id . ' :' . print_r($dataBackup, 1));
                    }
                }

                if ($temp) // give back delivered stat to the end
                    $data[] = $temp;
            }
        }





        foreach($data AS $item)
        {

            $stat_id = CourierStat::getOrCreateAndGetId($item['name'], $statusServiceId, false, $service['convertDeliveryStatus']);

            $date_converted = CourierStat::convertDate($item['date']);
            $location = trim(strip_tags($item['location']));

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from('courier_stat_history');
            $cmd->where('courier_id = :courier_id', array(':courier_id' => $courier->id));

            // because for example Safepak do not provide data for status
            if($date_converted != '')
                $cmd->andWhere('status_date = :status_date', array(':status_date' => $date_converted));


            $cmd->andWhere('current_stat_id = :status_id', array(':status_id' => $stat_id));
            //$cmd->andWhere('location = :location', array(':location' => $location));
            $result = $cmd->queryScalar();

            // already in DB, so do not continue to update stat
            if($result)
            {
                break;

            } else {
                $doNotNotify = $blockNotification;
                if($service['justPickup'] == true)
                    $doNotNotify = true;

                $stat_id = CourierStat::getOrCreateAndGetId($item['name'], $statusServiceId, true, $service['convertDeliveryStatus']);

                // save status into package
                $courier->changeStat($stat_id, $statusServiceId, $item['date'], false, $location, true, $doNotNotify, true);
            }
        }
    }


    /**
     * Method searches for packages connected to provided remoteId and returns array of found courier Id's
     * @param $trackId string|array Remote Id to be found
     * @return Courier[] Array of courier Id's
     */
    public static function findCourierIdsByRemoteId($inputId, $withRef = false, $ignoreSpecialRules = false, $forceExternalBroker = false)
    {

        if(!is_array($inputId))
            $trackIdArray = [ $inputId ];
        else
            $trackIdArray = $inputId;

        $trackIdArray2 = [];

        $colissimoItems = [];

        if(!$ignoreSpecialRules)
            foreach($trackIdArray AS $key => $trackId) {

                // special rule for Colissimo @ 17.12.2018
                if (mb_substr($trackId, 0, 3) == '%00' && in_array(strlen($trackId), [27,28]))
                    $colissimoItems[] = $trackId;

                // special rule for EUROPAKA GLS REF @23.10.2018
                if (mb_substr($trackId, 0, 2) == '26' && strlen($trackId) == 12)
                    $trackId = mb_substr($trackId, 0, -1);


                // @ 21.04.2017
                // Special rule for DPD_DE scanned numbers
                else if (mb_substr($trackId, 0, 1) == '%' && strlen($trackId) == 28) {

                    // @ 07.03.2018
                    // Special rule for DPD_IR scanned numbers
                    if (mb_substr($trackId, 0, 2) == '%0' && strlen($trackId) == 28) {
                        $trackId2 = mb_substr($trackId, 12, 9);
                        $trackIdArray2[] = $trackId2;
                    }

                    // DPD_DE
                    $trackId = mb_substr($trackId, 8, 14);

                }
                // @ 26.04.2017
                // Special rule for YODEL scanned numbers
                else if (mb_substr($trackId, 0, 2) == 'JJ' && strlen($trackId) == 19)
                    $trackId = mb_substr($trackId, 1);

                // @ 26.04.2017
                // Special rule for GLS scanned numbers
                else if (mb_substr($trackId, 2, 2) == '68' && strlen($trackId) == 12)
                    $trackId = mb_substr($trackId, 0, -1);

                // @ 12.06.2017
                // Special rule for UK MAIL scanned numbers
                else if (mb_substr($trackId, 0, 1) == 'A' && strlen($trackId) == 23)
                    $trackId = mb_substr($trackId, 6, -3);


                // @ 07.03.2018
                // Special rule for UK_MAIL scanned numbers
                else if (mb_substr($trackId, 0, 1) == 'A' && strlen($trackId) == 22)
                    $trackId = mb_substr($trackId, 5, 14);

                // @ 28.11.2018
                // Special rule for GLS IT scanned numbers
                else if (mb_substr($trackId, 0, 2) == 'BZ' && in_array(strlen($trackId), [16,18]))
                    $trackId = mb_substr($trackId, 0, 11);

                // special rule for PAccoReverse @ 30.11.2018
                else if (mb_substr($trackId, 0, 3) == 'APO' && strlen($trackId) == 16)
                    $trackId = mb_substr($trackId, 3);

                // special rule for JOY/KAAB_SK Direct @07.01.2019
                else if (mb_substr($trackId, 0, 1) == '0' && strlen($trackId) == 12)
                    $trackId = mb_substr($trackId, 1);
                // special rule for PeterSK Direct @ 30.01.2019
                else if(strlen($trackId) == 21 && preg_match("/^[a-z]{1}[0-9]{4}$/i", mb_substr($trackId, 16, 5)))
                    $trackId = mb_substr($trackId, 0, 13);
                // special rule for PeterSK Direct @ 30.01.2019
                else if(strlen($trackId) == 17 && preg_match("/^\-{1}[0-9]{5}$/i", mb_substr($trackId, 11, 6)))
                    $trackId = mb_substr($trackId, 0, 11);

                $trackIdArray[$key] = $trackId;
            }

        $found = [];


//            return IdStore::returnModelsByIds(IdStore::TYPE_COURIER, $trackIdArray);

        foreach($trackIdArray2 AS $item)
            $trackIdArray[] = $item;

        $trackIdSortOrder = '';
        foreach($trackIdArray AS $item)
            $trackIdSortOrder .= "'".preg_replace("/[^[:alnum:]]/u", '', $item)."',";

        $trackIdSortOrder = substr( $trackIdSortOrder,0,-1);

        if($trackIdSortOrder=='')
            $trackIdSortOrder = "''";

        // LABEL NEW
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier_id');
        $cmd->from((new CourierLabelNew())->tableName());
        $cmd->where(['in', 'track_id', $trackIdArray]);

        if($forceExternalBroker)
            $cmd->andWhere('operator = :operator', [':operator' => $forceExternalBroker]);

        $cmd->order('FIELD(track_id, '.$trackIdSortOrder.')');
        $result = $cmd->queryAll();

        if(is_array($result))
            foreach($result AS $item)
            {
                $found[$item['courier_id']] = true;
            }



        // for Colissimo - where last digit is different and beginning needs to be cut, example:
        // %007835016A1564894970801250 => 6A15648949703

        if(S_Useful::sizeof($colissimoItems)) {


            foreach($colissimoItems AS $colissimoItem) {

                if(strlen($colissimoItem) == 27)
                    $start = 9;
                else
                    $start = 10;

                $colissimoItem = substr($colissimoItem,$start, 12);

                /* @var $cmd CDbCommand */
                $cmd = Yii::app()->db->createCommand();
                $cmd->select('courier_id');
                $cmd->from((new CourierLabelNew())->tableName());
                $cmd->where(['like', 'track_id', $colissimoItem . '_']);
                $cmd->andWhere(['operator' => GLOBAL_BROKERS::BROKER_COLISSIMO]);
                $result = $cmd->queryAll();
//
                if (is_array($result))
                    foreach ($result AS $item) {
                        $found[$item['courier_id']] = true;
                    }

            }

        }

////      // EXT OPERATOR DETAILS
//        $cmd = Yii::app()->db->createCommand();
//        $cmd->select('id');
//        $cmd->from((new CourierExtOperatorDetails())->tableName());
//        $cmd->where('track_id_old = :trackId', [':trackId' => $trackId]);
//        $result = $cmd->queryScalar();
//
//        if($result)
//        {
//            $cmd = Yii::app()->db->createCommand();
//            $cmd->select('courier_id');
//            $cmd->from((new CourierTypeInternalSimple())->tableName());
//            $cmd->where('courier_ext_operator_details_id = :id', [':id' => $result]);
//            $result = $cmd->queryScalar();
//
//            $found[$result] = true;
//        }


        // CourierAdditionalExtId
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier_id');
        $cmd->from((new CourierAdditionalExtId())->tableName());
        $cmd->where(['in', 'external_id', $trackIdArray]);
        $cmd->order('FIELD(external_id, '.$trackIdSortOrder.')');
        $result = $cmd->queryAll();

        if(is_array($result))
            foreach($result AS $item)
            {
                $found[$item['courier_id']] = true;
            }

        if(!$forceExternalBroker) {

//      // TYPE OOE:
//            $cmd = Yii::app()->db->createCommand();
//            $cmd->select('courier_id');
//            $cmd->from((new CourierTypeOoe())->tableName());
////        $cmd->where('remote_id = :trackId', [':trackId' => $trackId]);
//            $cmd->where(['in', 'remote_id', $trackIdArray]);
//            $cmd->order('FIELD(remote_id, '.$trackIdSortOrder.')');
//            $result = $cmd->queryAll();
////
//            if (is_array($result))
//                foreach ($result AS $item) {
//                    $found[$item['courier_id']] = true;
//                }


            /* @var $cmd CDbCommand */
            // BY LOCAL ID OR NOTE:
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id');
            $cmd->from((new Courier())->tableName());
//            $cmd->where('ref = :trackId', [':trackId' => $trackId]);

            $cmd->where(['in', 'local_id', $trackIdArray]);

            $cmd->orWhere('(note1 IN ('.implode(',', array_map(function($str){ return sprintf("'%s'", $str);}, filter_var_array($trackIdArray, FILTER_SANITIZE_MAGIC_QUOTES))).') AND user_id = 1966)'); // Special search rule for EPAKA

            // BY REF:
            if ($withRef) {

                $cmd->orWhere(['in', 'ref', $trackIdArray]);
//                $cmd->orWhere(['in', 'SUBSTRING_INDEX(ref,"_", 1)', $trackIdArray]); // get all REF packages from Multipack

                foreach($trackIdArray AS $item) // get all REF packages from Multipack
                    $cmd->orWhere(['like', 'ref', $item.'\_%']); // get all REF packages from Multipack

                $cmd->order('FIELD(ref, '.$trackIdSortOrder.')');
            } else
                $cmd->order('FIELD(local_id, '.$trackIdSortOrder.')');

            $result = $cmd->queryAll();
//
            if (is_array($result))
                foreach ($result AS $item) {
                    $found[$item['id']] = true;
                }

        }

        if(!S_Useful::sizeof($found) && !$ignoreSpecialRules)
            return self::findCourierIdsByRemoteId($inputId, $withRef, true, $forceExternalBroker);

        if($found)
            $criteria = new CDbCriteria(['order'=>'FIELD(id, '.implode(',', array_keys($found)).')']);
        else
            $criteria = '';

        $models = Courier::model()->findAllByAttributes(['id' => array_keys($found)], $criteria);

        // temporary remove from search results packages older than 365 days to fix duplicating errors
//        foreach($models AS $key => $model)
//        {
//            if((time() - strtotime($model->date_entered)) > (86400 * 365))
//                unset($models[$key]);
//        }

        return $models;
    }

    public static function courierOperatorToCourierExternalManagerOperator($courierOperator)
    {
        Yii::import('application.components.UpsSoapShipClient');

        if (UpsSoapShipClient::isCourierOperatorUps($courierOperator))
            return self::TYPE_UPS;
        else if ($courierOperator == CourierOperator::OPERATOR_INPOST)
            return self::TYPE_INPOST;
        else if ($courierOperator == CourierOperator::OPERATOR_GLS)
            return self::TYPE_GLS;
        else if ($courierOperator == CourierOperator::OPERATOR_OWN)
            return self::TYPE_OTHER;
        else
            return self::TYPE_OOE;
    }

    public static function courierLabelNewOperatorToCourierExternalManagerOperator($courierLabelNewOperator)
    {
        Yii::import('application.components.UpsSoapShipClient');

        if(in_array($courierLabelNewOperator, array_keys(GLOBAL_BROKERS::getBrokersList()))) // for items from GLOBAL_BROKERS
            return $courierLabelNewOperator;
        else if(UpsSoapShipClient::isCourierLabelNewOperatorUps($courierLabelNewOperator))
            return self::TYPE_UPS;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_INPOST)
            return self::TYPE_INPOST;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_RUCH)
            return self::TYPE_RUCH;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_PETER_SK)
            return self::TYPE_PETER_SK;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_POCZTA_POLSKA)
            return self::TYPE_POCZTA_POLSKA;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_GLS_NL)
            return self::TYPE_GLS_NL;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_DHL OR $courierLabelNewOperator == CourierLabelNew::OPERATOR_DHL_F)
            return self::TYPE_DHL;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_DHLDE)
            return self::TYPE_DHLDE;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_DHLEXPRESS)
            return self::TYPE_DHLEXPRESS;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_UK_MAIL)
            return self::TYPE_UK_MAIL;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_DPD_DE)
            return self::TYPE_DPD_DE;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_GLS)
            return self::TYPE_GLS;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_ROYALMAIL)
            return self::TYPE_ROYAL_MAIL;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_SAFEPAK)
            return self::TYPE_SAFEPAK;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_DPDPL)
            return self::TYPE_DPDPL;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_DPDNL)
            return self::TYPE_DPDNL;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_LP_EXPRESS)
            return self::TYPE_LP_EXPRESS;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_POST11)
            return self::TYPE_POST11;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_HUNGARIAN_POST)
            return self::TYPE_HUNGARIAN_POST;
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_OOE)
            return self::TYPE_OOE;
        else
            return false;
    }

    public static function courierDomesticOperatorToCourierExternalManagerOperator($courierDomesticOperator)
    {
        if($courierDomesticOperator == CourierDomesticOperator::BROKER_INPOST)
        {
            return self::TYPE_INPOST;
        }
        else if($courierDomesticOperator == CourierDomesticOperator::BROKER_RUCH)
        {
            return self::TYPE_RUCH;
        }
        else if($courierDomesticOperator == CourierDomesticOperator::BROKER_GLS)
        {
            return self::TYPE_GLS;
        }
        else if($courierDomesticOperator == CourierDomesticOperator::BROKER_SAFEPAK)
        {
            return self::TYPE_SAFEPAK;
        }
        else if($courierDomesticOperator == CourierDomesticOperator::BROKER_UPS)
        {
            return self::TYPE_UPS;
        }
        else if($courierDomesticOperator == CourierDomesticOperator::BROKER_DHL)
        {
            return self::TYPE_DHL;
        }
        else
            return false;
    }

    public static function courierInternalSimpleOperatorToCourierExternalManagerOperator($courierInternalSimpleOperator)
    {

        if($courierInternalSimpleOperator == CourierTypeInternalSimple::OPERATOR_UPS OR $courierInternalSimpleOperator == CourierTypeInternalSimple::OPERATOR_UPS_SAVER)
        {
            return self::TYPE_UPS;
        }
        else if($courierInternalSimpleOperator == CourierTypeInternalSimple::OPERATOR_GLS)
        {
            return self::TYPE_GLS;

        } else
            return false;
    }


    /**
     * Used for TT where source data contains data of more than one package - for grouping TT
     * @param array $ttData
     * @param $broker
     */
    public static function groupUpdateTt(array $ttData, $broker)
    {
        foreach($ttData AS $ttNumber => $ttDataForItem)
        {
            $couriers = CourierExternalManager::findCourierIdsByRemoteId($ttNumber, false, true);

            MyDump::dump('track_found.txt', print_r(S_Useful::sizeof($couriers),1));

            /* @var $courier Courier */
            foreach($couriers AS $courier)
            {
                if($courier->isStatChangeByTtAllowed())
                {
                    // additional data
                    $temp = [];
                    // check if it's pickup label for internal and there is also delivery label
                    if($courier->getType() == Courier::TYPE_INTERNAL)
                    {
                        if($courier->courierTypeInternal->pickupLabel && $courier->courierTypeInternal->pickupLabel->getTrack_id() == $ttNumber && $courier->courierTypeInternal->delivery_operator_ordered)
                            $temp['convertDeliveryStatus'] = true;
                    }

                    $ttDataForItem = CourierExternalManager::sortArray($ttDataForItem);
                    self::processUpdateDate($courier, $ttDataForItem, false, $broker, $temp);
                }
            }
        }
    }

}