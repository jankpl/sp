<?php

class FtpProcessorController extends CController
{
    const LOCAL = false;

    public function actionIndex()
    {
        $semaphore = new Semaphore(intval(Semaphore::FTP_PROCESSOR_PREFIX), Yii::app()->basePath . '/_temp/');
        try {
            $semaphore->acquire();
            MyDump::dump('ftp_processor.txt', $this->id.' : SEM ACQUIRED ('.Semaphore::FTP_PROCESSOR_PREFIX.')');
        } catch (Exception $e) {
            MyDump::dump('ftp_processor.txt', $this->id.' : SEM LOCKED ('.Semaphore::FTP_PROCESSOR_PREFIX.')');
            echo 'SEM LOCKED!';
            Yii::app()->end();
        }


        if(self::LOCAL)
            $dir = Yii::app()->basePath.'/../../ftp2process/';
        else
            $dir = Yii::app()->basePath.'/../../ftp2processTest/';

        $files = scandir($dir);

        foreach($files AS $file) {


            $filePath = $dir . $file;

            $ext = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));
            if (!in_array($ext, [
                'csv',
                'xls',
                'xlsx'
            ]))
                continue;

            MyDump::dump('ftp_processor.txt', 'PROCESS FILE: '.$file);

            $fileVariables = explode('%%', $file);
            $forceDDPmode = false;
            $forceDDUmode = false;
            $customReceiverAddress = false;

            foreach ($fileVariables AS $variable) {

                if (filter_var($variable, FILTER_VALIDATE_EMAIL))
                    $customReceiverAddress = $variable;
                else if (strcasecmp($variable, 'DDP') == 0)
                    $forceDDPmode = true;
                else if (strcasecmp($variable, 'DDU') == 0)
                    $forceDDUmode = true;
            }

            $inputFileType = PhpOffice\PhpSpreadsheet\IOFactory::identify($filePath);

            MyDump::dump('ftp_processor.txt', 'VAR: '.print_r($customReceiverAddress,1).' | '.print_r($forceDDPmode,1).' | '.print_r($forceDDUmode,1).' | ');
            // WITH FIX:
            if (strtoupper($inputFileType) == 'CSV') {
                // SET UTF8 ENCODING IN CSV
                $file_content = file_get_contents($filePath);

                $file_content = S_Useful::forceToUTF8($file_content);
                file_put_contents($filePath, $file_content);

                $objReader = PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
                $objReader->setDelimiter(',');
                // FIX ENCODING


            } else {
                $objReader = PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
            }


            // WITHOUT FIX:
//
//                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//                if(strtoupper($inputFileType) == 'CSV' && $fileModel->_customSeparator)
//                {
//                    $objReader->setDelimiter($fileModel->_customSeparator);
//                }

            $objPHPExcel = $objReader->load($filePath);

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $data = [];
            $start = 1;

            if (true) { // omit headers
                $headerSeen = false;
            } else
                $headerSeen = true;

            for ($row = $start; $row <= $highestRow; $row++) {
                if (!$headerSeen) {
                    $headerSeen = true;
                    continue;
                }

                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                array_push($data, $rowData[0]);
            }

            // remove empty lines:
            $data = (array_filter(array_map('array_filter', $data)));

//            $numberOfLines = S_Useful::sizeof($data);

            $_identity = new UserIdentity(false, false);

            if (self::LOCAL)
                $identity = $_identity->authenticateFromSystem(8, 'Test3');
            else
                $identity = $_identity->authenticateFromSystem(2404, 'S.F.X_DDU');

            if ($identity)
                Yii::app()->user->login($_identity);

            if (Yii::app()->user->isGuest)
                exit;


            Yii::app()->getModule('postal');
            $i = 0;
            $models = [];
            foreach ($data AS $key => $item) {
                $i++;

                $postal = new Postal();
                $postal->user_id = Yii::app()->user->id;
                $postal->_import_raw_data_key = $key;

                $postal->source = Postal::SOURCE_FILE_IMPORT;

                $postal->senderAddressData = new Postal_AddressData();
                $postal->receiverAddressData = new Postal_AddressData();

                $postal->senderAddressData->setSmsNotifyActive();
                $postal->receiverAddressData->setSmsNotifyActive();

                $postal->senderAddressData->_postal = $postal;
                $postal->receiverAddressData->_postal = $postal;

                if (preg_match('/^UY/', $item[2]))
                    $postal->postal_type = Postal::POSTAL_TYPE_PRIO;
                else
                    $postal->postal_type = Postal::POSTAL_TYPE_REG;

//                $postal->postal_type = F_PostalImport::postalTypeNameToId('REG');
//
                $reg = false;
                if ($postal->postal_type == Postal::POSTAL_TYPE_REG)
                    $reg = true;


                $postal->weight = $item[15] * 1000;
                $postal->size_class = 3;

                $postal->value = $item[17];
                $postal->value_currency = 'USD';

                $postal->target_sp_point = 1;


                $postal->senderAddressData->name = '';
                $postal->senderAddressData->company = 'SF-Express';
                $postal->senderAddressData->country_id = CountryList::COUNTRY_PL;
                $postal->senderAddressData->country0 = F_PostalImport::getCountryFromCache($postal->senderAddressData->country_id);
                $postal->senderAddressData->zip_code = '03-877';
                $postal->senderAddressData->city = 'Sękocin Nowy';

                $postal->senderAddressData->address_line_1 = 'ul. Utrata 4';
                $postal->senderAddressData->address_line_2 = 'RUSAK Business Services Sp. z o.o.';

                $postal->senderAddressData->tel = '';
                $postal->senderAddressData->email = '';

                $receiverName = $item[3];
                $receiverCompany = '';
                F_PostalImport::_distributeTooLongField($receiverName, $receiverCompany, 'receiver_name', 'receiver_company', true, $reg);
                F_PostalImport::_distributeTooLongField($receiverCompany, $receiverName, 'receiver_company', 'receiver_name', true, $reg);


                $postal->receiverAddressData->name = $receiverName;
                $postal->receiverAddressData->company = $receiverCompany;

                $postal->receiverAddressData->country_id = CountryList::findIdByText($item[10]);
                $postal->receiverAddressData->country0 = F_PostalImport::getCountryFromCache($postal->receiverAddressData->country_id);
                $postal->receiverAddressData->zip_code = $item[4];
                $postal->receiverAddressData->city = $item[6];

                $receiverAddressLine1 = $item[7];
                $receiverAddressLine2 = '';
                F_PostalImport::_distributeTooLongField($receiverAddressLine1, $receiverAddressLine2, 'receiver_address_line_1', 'receiver_address_line_2', true, $reg);

                $postal->receiverAddressData->address_line_1 = $receiverAddressLine1;
                $postal->receiverAddressData->address_line_2 = $receiverAddressLine2;

                $postal->receiverAddressData->tel = $item[8];
                $postal->receiverAddressData->email = '';

                $postal->content = $item[12];
                $postal->ref = $item[1];
                $postal->note1 = $item[14] . ':' . $item[11];
                $postal->note2 = '';

                $postal->_pp_forceTTNumber = $item[2];

                array_push($models, $postal);
            }


            $returnErrors = [];
            $foundErrors = F_PostalImport::validateModels($models, $returnErrors);

            $now = date('YmdHis');

            file_put_contents($dir . $file . '.' . $now . '.backup', file_get_contents($dir . $file));
            @unlink($dir . $file);

            if (!$foundErrors) {
                try {
                    $result = Postal::saveWholeGroupNotMass($models);
                } catch (Exception $ex) {
                    Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);
                    return;
                }

                file_put_contents($dir . $file . '.' . $now . '.resultOK', 'SUCCESFULLY PROCESED ITEMS: ' . S_Useful::sizeof($data));
                S_Mailer::send('jankopec@swiatprzesylek.pl', 'jankopec@swiatprzesylek.pl', 'SUCCESS - FTP GENERATED LABELS: ' . $file, 'SUCCESFULLY PROCESED ITEMS: ' . S_Useful::sizeof($data), false);

                if($customReceiverAddress)
                    S_Mailer::send($customReceiverAddress, $customReceiverAddress, 'SUCCESS - FTP GENERATED LABELS: ' . $file, 'SUCCESFULLY PROCESED ITEMS: ' . S_Useful::sizeof($data), false);


                MyDump::dump('ftp_processor.txt', 'SUCCESFULLY PROCESED ITEMS: ' . S_Useful::sizeof($data));
            } else {
                file_put_contents($dir . $file . '.' . $now . '.resultFAIL', print_r($returnErrors, 1));
                S_Mailer::send('jankopec@swiatprzesylek.pl', 'jankopec@swiatprzesylek.pl', 'FAIL - FTP GENERATED LABELS: ' . $file, nl2br(print_r($returnErrors, 1)), false);
                if($customReceiverAddress)
                    S_Mailer::send($customReceiverAddress, $customReceiverAddress, 'FAIL - FTP GENERATED LABELS: ' . $file, nl2br(print_r($returnErrors, 1)), false);

                MyDump::dump('ftp_processor.txt', 'FAIL - FTP GENERATED LABELS: ' . $file, print_r($returnErrors, 1));
            }


            if (!$foundErrors) {

                $retries = 0;
                $allReady = false;
                while (!$allReady) {
                    $allReady = true;
                    /* @var $model Postal */
                    foreach ($models AS $model) {
                        $model->refresh();
                        if ($model->postal_stat_id == PostalStat::PACKAGE_PROCESSING OR $model->postal_stat_id == PostalStat::NEW_ORDER) {
                            $model->postalLabel->refresh();
                            if ($model->postalLabel && $model->postalLabel->stat == PostalLabel::STAT_NEW)
                                $model->postalLabel->runLabelOrdering();

                            $allReady = false;
                            break;
                        }
                    }

                    if ($allReady)
                        break;

                    sleep(30);
                    $retries++;

                    if ($retries > 10) {
                        $allReady = false;
                        break;
                    }
                }

                if ($allReady) {
                    foreach ($models AS $key => $model) {
                        if ($model->stat_map_id == StatMap::MAP_CANCELLED)
                            unset($models[$key]);
                    }

                    if (S_Useful::sizeof($models)) {

                        if($forceDDPmode) {
                            $pdf = PDFGenerator::initialize();
                            foreach ($models AS $model)
                                $pdf = CzechPostClient::generateLabelByPostal($model, false, false, false, false, $pdf, 2, true);

                            $pdf = $pdf->Output('cp.pdf', 'S');
                        } else if($forceDDUmode) {
                            $pdf = PDFGenerator::initialize();
                            foreach ($models AS $model)
                                $pdf = CzechPostClient::generateLabelByPostal($model, false, false, false, false, $pdf, 1, true);

                            $pdf = $pdf->Output('cp.pdf', 'S');
                        } else
                            $pdf = PostalLabelPrinter::generateLabels($models, PostalLabelPrinter::PDF_ROLL, true, false, true);

                        S_Mailer::send('jankopec@swiatprzesylek.pl', 'jankopec@swiatprzesylek.pl', 'FTP GENERATED LABELS: ' . $file, 'There should be something in attachment for you... :)', false, NULL, NULL, true, false, 'lab.pdf', false, false, NULL, $pdf);

                        if($customReceiverAddress)
                            S_Mailer::send($customReceiverAddress, $customReceiverAddress, 'FTP GENERATED LABELS: ' . $file, 'There should be something in attachment for you... :)', false, NULL, NULL, true, false, 'lab.pdf', false, false, NULL, $pdf);

                        MyDump::dump('ftp_processor.txt', 'LABELS SENT');
                    }
                }
            }
        }

        try {
            $semaphore->release();
            MyDump::dump('ftp_processor.txt', 'SEMAPHORE RELEASED');
        } catch (Exception $e) {
            MyDump::dump('ftp_processor.txt', 'SEMAPHORE RELEASE FAILED');
            die('Could not release the semaphore');
        }
    }




}