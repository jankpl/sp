<?php
/* @var $form CActiveForm */
/* @var $model F_B2C2CorreosForm */
/* @var $fileData array */

?>

    <h2>B2C2Correos</h2>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>


<?php
if($fileData):
    ?>

    <table class="table table-striped table-bordered">
        <tr>
            <th style="text-align: right;">Oryginal TT</th>
            <th>New TT</th>
            <th style="text-align: center;">Found</th>
        </tr>
        <?php
        foreach($fileData AS $line):
            ?>
            <tr>
                <td style="text-align: right;"><?= $line[0];?></td>
                <td><?= $line[1];?></td>
                <td style="text-align: center;"><?= F_B2C2CorreosForm::isOryginalNoValid($line[0]) ? '<span style="color: green;">T</span>' : '<span style="color: red;">N</span>';?></td>
            </tr>

        <?php
        endforeach;
        ?>
    </table>

    <div class="text-center">
        <a href="<?= Yii::app()->createUrl('/b2C2Correos/index', ['hash' => $hash, 'confirm' => md5($hash)]);?>" class="btn btn-warning btn-lg">Apply new TT numbers</a>
    </div>
<br/>
<br/>
<br/>

    <div style="text-align: center;">
        <a href="<?= Yii::app()->createUrl('/b2C2Correos/');?>" class="btn btn-mini">Restart</a>
    </div>
<?php
else:
    ?>

    <div class="form" style="position: relative;">
        <div class="ajax-preloader"><div></div></div>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'=>'b2c2c-form',
            'enableClientValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )); ?>

        <?php
        foreach(Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
        }
        ?>

        <table class="table table-striped" style="margin: 0 auto; width: 500px;">
            <tr>
                <td style="text-align: center;">     <?php  echo $form->fileField($model, 'file', ['id' => 'file-field']); ?>
                    <?php echo $form->error($model,'file'); ?></td>
            </tr>
            <tr>
                <td style="text-align: center;"><?= CHtml::submitButton('Submit file', ['class' => 'btn btn-large btn-primary']);?></td>
            </tr>
        </table>
        <?php $this->endWidget(); ?>
    </div>

<?php
endif;
