<?php

class S_Useful
{
    /**
     * @param int $limit
     * @param null $limit2 In order to keep compatibility with some old method calls
     * @return array
     */
    static public function generateArrayOfItemsNumber($limit = 100, $limit2 = NULL)
    {

        if($limit2 !== NULL && $limit2 > $limit)
            $limit = $limit2;

        $numbers = Array();
        for($i = 1; $i<=$limit; $i++) $numbers[$i] = $i;

        return $numbers;
    }

    static public function chartColors($i = null)
    {
        $colors[] = '#06aecf';
        $colors[] = '#6aba76';
        $colors[] = '#e8040f';
        $colors[] = '#7743b1';
        $colors[] = '#013fce';
        $colors[] = '#3fdf2a';
        $colors[] = '#e5d805';
        $colors[] = '#9b3b15';
        $colors[] = '#f4a01b';
        $colors[] = '#df50cd';


        if($i!== null)
            return $colors[$i];


        return $colors;
    }

    static public function hex2rgb($hex, $inline = NULL) {
        $hex = str_replace("#", "", $hex);

        if(strlen($hex) == 3) {
            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
            $r = hexdec(substr($hex,0,2));
            $g = hexdec(substr($hex,2,2));
            $b = hexdec(substr($hex,4,2));
        }
        $rgb = array($r, $g, $b);

        if($inline !== NULL)
            return implode(",", $rgb); // returns the rgb values separated by commas

        return $rgb; // returns an array with the rgb values
    }

    static public function correctImgPathInText($text)
    {
        $basePath = Yii::app()->baseUrl;

        $imagesOrg = [];
        $linksOrg = [];
        $imagesRep = [];
        $linksRep = [];

        $dom=new DomDocument( "1.0",'UTF-8');
        libxml_use_internal_errors(true);
        $dom->loadHTML('<?xml encoding="UTF-8">'.$text);
        libxml_use_internal_errors(false);
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $image) {


            $src=$image->attributes->getNamedItem("src")->value;
            array_push($imagesOrg, $src);

            if (strpos($basePath, $src)<=0 && substr($src,0,7)!='http://') {
                $image->attributes->getNamedItem("src")->value=$basePath.'/'.$src;
            }
        }

        $links = $dom->getElementsByTagName('a');
        foreach ($links as $link) {
            $href=$link->attributes->getNamedItem("href")->value;
            array_push($linksOrg, $href);
            if (strpos($basePath, $link)<=0 && substr($href,0,7)!='http://') {
                $link->attributes->getNamedItem("href")->value=$basePath.'/'.$href;
            }
        }

        foreach($imagesOrg AS $key => $image)
        {
            if (strpos($basePath, $image)<=0 && substr($image,0,7)!='http://') {

                if($basePath != '')
                    $newImage = $basePath.'/'.$image;
                else
                    $newImage = $image;
                $imagesRep[$key] = $newImage;
            } else
                $imagesRep[$key] = $image;
        }

        foreach($linksOrg AS $key => $link)
        {
            if (strpos($basePath, $link)<=0 && substr($link,0,7)!='http://' && substr($link,0,7)!='mailto:') {

                $newLink = $basePath.'/'.$link;
                $linksRep[$key] = $newLink;
            } else
                $linksRep[$key] = $link;
        }

        $text = str_replace($imagesOrg, $imagesRep, $text);
        $text = str_replace($linksOrg, $linksRep, $text);



        return $text;

    }

//    public static function removePolishCharacters($string)
//    {
//        return self::removeNationalCharacters($string);
//
//        $unPretty = array('/ä/', '/ö/', '/ü/', '/Ä/', '/Ö/', '/Ü/', '/ß/',
//            '/ą/', '/Ą/', '/ć/', '/Ć/', '/ę/', '/Ę/', '/ł/', '/Ł/' ,'/ń/', '/Ń/', '/ó/', '/Ó/', '/ś/', '/Ś/', '/ź/', '/Ź/', '/ż/', '/Ż/',
//            '/Š/','/Ž/','/š/','/ž/','/Ÿ/','/Ŕ/','/Á/','/Â/','/Ă/','/Ä/','/Ĺ/','/Ç/','/Č/','/É/','/Ę/','/Ë/','/Ě/','/Í/','/Î/','/Ď/','/Ń/',
//            '/Ň/','/Ó/','/Ô/','/Ő/','/Ö/','/Ř/','/Ů/','/Ú/','/Ű/','/Ü/','/Ý/','/ŕ/','/á/','/â/','/ă/','/ä/','/ĺ/','/ç/','/č/','/é/','/ę/',
//            '/ë/','/ě/','/í/','/î/','/ď/','/ń/','/ň/','/ó/','/ô/','/ő/','/ö/','/ř/','/ů/','/ú/','/ű/','/ü/','/ý/','/˙/',
//            '/Ţ/','/ţ/','/Đ/','/đ/','/ß/','/Œ/','/œ/','/Ć/','/ć/','/ľ/');
//
//        $pretty  = array('ae', 'oe', 'ue', 'Ae', 'Oe', 'Ue', 'ss',
//            'a', 'A', 'c', 'C', 'e', 'E', 'l', 'L', 'n', 'N', 'o', 'O', 's', 'S', 'z', 'Z', 'z', 'Z',
//            'S','Z','s','z','Y','A','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N',
//            'O','O','O','O','O','O','U','U','U','U','Y','a','a','a','a','a','a','c','e','e','e',
//            'e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y',
//            'TH','th','DH','dh','ss','OE','oe','AE','ae','u');
//
//        $string = preg_replace($unPretty, $pretty, $string);
//        return $string;
//
//
//    }

    public static function removeNationalCharacters($string, $exceptPolish = false)
    {
        $string = self::transliterateCyrilic($string);

        $replace = [
            'ä' => 'ae',
            'å' => 'a',
            'æ' => 'ae',
            'ğ' => 'g',
            'ë' => 'e',
            'ö' => 'oe',
            'ø' => 'o',
            'ß' => 'SS',
            'ş' => 's',
            'ü' => 'ue',
            'ÿ' => 'y',
            'Ä' => 'AE',
            'Å' => 'A',
            'Æ' => 'Ae',
            'Ğ' => 'G',
            'Ë' => 'E',
            'Ö' => 'OE',
            'Ø' => 'O',
            'Ş' => 'S',
            'Ü' => 'UE',
            'Ÿ' => 'Y',
            'é' => 'e',
            'è' => 'e',
            'à' => 'a',
            'ù' => 'u',
            'â' => 'a',
            'ê' => 'e',
            'î' => 'i',
            'ô' => 'o',
            'û' => 'u',
            'ï' => 'i',
            'ç' => 'c',
            'É' => 'E',
            'È' => 'E',
            'À' => 'A',
            'Ù' => 'U',
            'Â' => 'A',
            'Ê' => 'E',
            'Î' => 'I',
            'Ô' => 'O',
            'Û' => 'U',
            'Ï' => 'I',
            'Ç' => 'C',
            'á' => 'a',
            'í' => 'i',
            'ú' => 'u',
            'ñ' => 'n',
            'Á' => 'A',
            'Í' => 'I',
            'Ú' => 'U',
            'Ñ' => 'N',
            'Ž' => 'Z',
            'º' => 'o',
            'Š' => 'S',
            'š' => 's',
            'Ľ' => 'L',
            'ľ' => 'l',
            'Č' => 'C',
            'Ď' => 'D',
            'Ĺ' => 'L',
            'Ň' => 'N',
            'Ó' => 'O',
            'Ŕ' => 'R',
            'Ť' => 'T',
            'Ý' => 'Y',
            'č' => 'c',
            'ď' => 'd',
            'ĺ' => 'l',
            'ň' => 'n',
            'ó' => 'o',
            'ŕ' => 'r',
            'ť' => 't',
            'ý' => 'y',
            'ž' => 'z',
            'Ū' => 'U',
            'ū' => 'u',
            'Ī' => 'I',
            'ī' => 'i',
            'Ã' => 'A',
            'ã' => 'a',
            'Ő' => 'O',
            'ő' => 'o',
            'Ů' => 'U',
            'ů' => 'u',
            'Õ' => 'O',
            'õ' => 'o',
            'Ě' => 'E',
            'ě' => 'e',
            'Ř' => 'R',
            'ř' => 'r',
            'Ă' => 'A',
            'ă' => 'a',
            'Ò' => 'O',
            'ò' => 'o',
        ];

        $string = str_replace(array_keys($replace), $replace, $string);

        if(!$exceptPolish)
        {
            $replace = [
                'ę' => 'e',
                'ó' => 'o',
                'ą' => 'a',
                'ś' => 's',
                'ł' => 'l',
                'ż' => 'z',
                'ź' => 'z',
                'ć' => 'c',
                'ń' => 'n',
                'Ę' => 'E',
                'Ó' => 'O',
                'Ą' => 'A',
                'Ś' => 'S',
                'Ł' => 'L',
                'Ż' => 'Z',
                'Ź' => 'Z',
                'Ć' => 'C',
                'Ń' => 'N',
            ];

            $string = str_replace(array_keys($replace), $replace, $string);
        }

        return $string;
    }


    protected static function transliterateCyrilic($text)
    {
        $roman = array("Sch","sch",'Yo','Zh','Kh','Ts','Ch','Sh','Yu','ya','yo','zh','kh','ts','ch','sh','yu','ya','A','B','V','G','D','E','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','','Y','','E','a','b','v','g','d','e','z','i','y','k','l','m','n','o','p','r','s','t','u','f','','y','','e');
        $cyrillic = array("Щ","щ",'Ё','Ж','Х','Ц','Ч','Ш','Ю','я','ё','ж','х','ц','ч','ш','ю','я','А','Б','В','Г','Д','Е','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Ь','Ы','Ъ','Э','а','б','в','г','д','е','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','ь','ы','ъ','э');

        return str_replace($cyrillic, $roman, $text);
    }

    /**
     * Function adding space between numbers and letters. Used because some operators need do have just number in address for better understaing. For example: "Street 5a" => "Street 5 a"
     * @param $text
     * @return mixed
     */
    public static function divideNumberAndLetter($text)
    {
        return preg_replace('/(?<=[a-z]|\/|\.|-)(?=\d)|(?<=\d)(?=[a-z]|\/|\.|-)/i', ' ', $text);
    }



    /**
     * Returns data formatted for usage by jQuery filed suggestion (item from ContactBook or country form list)
     * @param $models
     * @param $valueField
     * @param $textField
     * @param $descField
     * @param string $groupField
     * @param bool|true $encodeJson
     * @return array|string
     */
    public static function listDataForAutocomplete($models,$valueField,$textField, $descField, $groupField='', $encodeJson = true)
    {


        $listData=array();
        if($groupField==='')
        {
            foreach($models as $model)
            {
                $value = $model->$valueField;
                $text = $model->$textField;
                $desc = $model->$descField;
                $listData[$value]=array(
                    'label' => $text,
                    'desc' => $desc,

                    //'id' => $value
                );
            }
        }
        else
        {
            foreach($models as $model)
            {
                $group = $model->$groupField;
                $value = $model->$valueField;
                $text = $model->$textField;
                $desc = $model->$descField;
                if($group===null)
                    $listData[$value]=array(
                        'label' => $text,
                        'desc' => $desc,

                        //'id' => $value
                    );
                else
                    $listData[$group][$value]=array(
                        'label' => $text,
                        'desc' => $desc,

                        // 'id' => $value
                    );
            }
        }

        // remove duplicates
        $listData = array_map("unserialize", array_unique(array_map("serialize", $listData)));
        // add IDs
        foreach($listData AS $key => $item)
        {
            $listData[$key]['value'] = $key;
        }

        if($encodeJson)
            $listData = CJSON::encode($listData);

        return $listData;
    }

    public static function trimBetter($text)
    {
        $text = preg_replace('/\s+/', ' ', $text);
        return $text;
    }


    public static function workDaysNextDate($date1,$workDays, $withSaturday = false) {
        $workDays = (int)$workDays;
        if ($workDays < 0)
            return null;
        $date1=strtotime('-1 day',strtotime($date1));
        $lastYear = null;
        $hol=array('01-01','05-01','05-03','08-15','11-01','11-11','12-25','12-26');
        $i = 0;
        while ($i<=$workDays) {
            $date1=strtotime('+1 day',$date1);
            $year = date('Y', $date1);
            if ($year !== $lastYear){
                $lastYear = $year;
                $easter = date('m-d', easter_date($year));
                $date = strtotime($year . '-' . $easter);
                $easterSec = date('m-d', strtotime('+1 day', $date));
                $cc = date('m-d', strtotime('+60 days', $date));
                $hol[8] = $easter;
                $hol[9] = $easterSec;
                $hol[10] = $cc;
            }
            $weekDay=date('w',$date1);
            if (!($weekDay==0 || ($weekDay==6 && !$withSaturday) || in_array(date('m-d',$date1),$hol)))
                $i++;

        }
        return date('Y-m-d',$date1);


    }

    /**
     * Function cloning ORM models
     *
     * @param CModel $target
     * @param CModel $source
     * @return CModel
     */
    public static function cloneOrmModels(CModel $target, CModel $source)
    {
        $target->scenario = 'clone';
        $target->attributes = $source->attributes;

        if(method_exists($source, 'clonableAttributes'))
            foreach($source->clonableAttributes() AS $key => $item)
                $target->$item = $source->$item;

        $pk = $target->getPrimaryKey();

        if(!is_array($pk) && $pk != '')
            $target->$pk = '';

        $target->id = NULL;
        $target->isNewRecord = true;

        return $target;
    }

    /**
     * Function returning last months as array
     *
     * @return array
     */
    public static function listPeriods()
    {
        $periods = [];
        $year = date('Y');
        $month = date('m');
        for($i = 0; $i <= 36; $i ++)
        {
            if($i) {
                $month--;
                if (!$month) {
                    $year--;
                    $month = 12;
                }
            }

            $month = str_pad($month,2,'0',STR_PAD_LEFT);

            $temp = $month.'.'.$year;

            $temp2 = str_replace('.','', $temp);
            $periods[$temp2] = $temp;
        }

        return $periods;
    }

    /**
     * Tries to convert encoding to UTF-8
     *
     * @param $string string Input string
     * @return string string Output string
     */
    public static function forceToUTF8($string) {

        // maybe it's already UTF?
        if($stringUTF8 = @iconv('UTF-8', 'UTF-8', $string))
            return $stringUTF8;

        $win2utf = array(
            "\xb9" => "\xc4\x85", "\xa5" => "\xc4\x84", "\xe6" => "\xc4\x87", "\xc6" => "\xc4\x86",
            "\xea" => "\xc4\x99", "\xca" => "\xc4\x98", "\xb3" => "\xc5\x82", "\xa3" => "\xc5\x81",
            "\xf3" => "\xc3\xb3", "\xd3" => "\xc3\x93", "\x9c" => "\xc5\x9b", "\x8c" => "\xc5\x9a",
            "\x9f" => "\xc5\xba", "\xaf" => "\xc5\xbb", "\xbf" => "\xc5\xbc", "\xac" => "\xc5\xb9",
            "\xf1" => "\xc5\x84", "\xd1" => "\xc5\x83", "\x8f" => "\xc5\xb9");

        $countWin = 0;
        $found = 0;
        foreach($win2utf as $win => $utf) {
            $pos = strpos($string, $win);


            if($pos !== false) {
                $found++;
            }

            if($pos && substr(@iconv('WINDOWS-1250', 'UTF-8', $string), $pos+$countWin, 2) == $utf) {
                $countWin++;
            }
        }

        // probably WINDOWS-1250
        if($countWin > 0 && $found == $countWin) {
            if($stringUTF8 = @iconv('WINDOWS-1250', 'UTF-8', $string))
                return $stringUTF8;
        }

        // probably ISO-8859-2
        if($found) {
            if($stringUTF8 = @iconv('ISO-8859-2', 'UTF-8', $string))
                return $stringUTF8;
        }

        // try again Windows
        if($stringUTF8 = @iconv('WINDOWS-1250', 'UTF-8', $string))
            return $stringUTF8;

        // try again ISO
        if($stringUTF8 = @iconv('ISO-8859-2', 'UTF-8', $string))
            return $stringUTF8;

        // failed - return orginal string
        return $string;
    }

    public static function inlineString($text)
    {
        return str_replace(array("\r\n", "\n", "\r"), '', $text);
    }

    public static function secondsToNiceTime($seconds, $withSeconds = false)
    {

        $h = floor($seconds / 3600);
        $rest_seconds = $seconds - $h * 3600;

        $m = floor($rest_seconds / 60);
        $rest_seconds = $rest_seconds - $m * 60;

        $s = round($rest_seconds, 3);
        $s = number_format($s, 3, '.', '');

//        $h = str_pad($h, 2, '0', STR_PAD_LEFT);
//        $m = str_pad($m, 2, '0', STR_PAD_LEFT);
//        $s = str_pad($s, 6, '0', STR_PAD_LEFT);


        $time = '';
        if($h)
            $time = $h."g. ";

        $time .= $m."min.";

        if($withSeconds)
            $time .= " " . $s;

        return $time;
    }

    /**
     * Checks if date yyyy-mm-dd is valid
     * @param $date string Format yyyy-mm-dd
     * @return bool
     */
    public static function checkDate($date)
    {
        $date = explode('-', $date);

        if(S_Useful::sizeof($date) != 3)
            return false;

        return checkdate(intval($date[1]), intval($date[2]), intval($date[0]));
    }

    public static function convertCommaToDec($value)
    {
        return str_replace(',', '.', $value);
    }

    public static function nicePrintR($data)
    {

        $text = print_r($data,1);
        $text = str_replace('Array', '', $text);
        $text = '<pre>'.$text.'</pre>';
        return $text;
    }

    public static function arrayToNiceString($array, $showKey = true, $sep = '; ')
    {
        if(!is_array($array))
            return (string) $array;

        foreach($array AS $key => $item)
        {
            if(is_array($item))
                return ($showKey && is_string($key) ? ($key.' : ') : '').self::arrayToNiceString($item, $sep);
            else
                return trim(($showKey && is_string($key) ? ($key.' : ') : '').$item.$sep);
        }
    }

    public static function urlToLinkInText($string, $withStripTagsBefore = true)
    {

        if($withStripTagsBefore)
            $string = strip_tags($string);

        $reg_exUrl = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";

        // Check if there is a url in the text
        if(preg_match($reg_exUrl, $string, $url)) {

            if(strpos( $url[0], ":" ) === false){
                $link = 'http://'.$url[0];
            }else{
                $link = $url[0];
            }

            // make the urls hyper links
            $string = preg_replace($reg_exUrl, '<a href="'.$link.'" title="'.$url[0].'" target="_blank"><u>'.$url[0].'</u></a>', $string);

        }

        return $string;
    }


    public static function transliterateGrek($text)
    {
        $data = [
            'Α' => 'A',
            'Αι' => 'Ai',
            'Αυ' => 'Av',
            'Β' => 'V',
            'Γ' => 'G',
            'Γκ' => 'Gk',
            'Δ' => 'D',
            'Ε' => 'E',
            'Ει' => 'Ei',
            'Ευ' => 'Ev',
            'Ζ' => 'Z',
            'Η' => 'I',
            'Ηυ' => 'Iv',
            'Θ' => 'Th',
            'Ι' => 'I',
            'Κ' => 'K',
            'Λ' => 'L',
            'Μ' => 'M',
            'Μπ' => 'B',
            'Ν' => 'N',
            'Ντ' => 'Nt',
            'Ξ' => 'X',
            'Ο' => 'O',
            'Οι' => 'Oi',
            'Ου' => 'Ou',
            'Π' => 'P',
            'Ρ' => 'R',
            'Σ' => 'S',
            'Τ' => 'T',
            'Υ' => 'Y',
            'Υι' => 'Yi',
            'Φ' => 'F',
            'Χ' => 'Ch',
            'Ψ' => 'Ps',
            'Ω' => 'O',
            'Ωυ' => 'Oy',
            'α' => 'a',
            'αι' => 'ai',
            'αυ' => 'av',
            'β' => 'v',
            'γ' => 'g',
            'γγ' => 'ng',
            'γκ' => 'gk',
            'γξ' => 'nx',
            'γχ' => 'nch',
            'δ' => 'd',
            'ε' => 'e',
            'ει' => 'ei',
            'ευ' => 'ev',
            'ζ' => 'z',
            'η' => 'i',
            'ηυ' => 'iv',
            'θ' => 'th',
            'ι' => 'i',
            'κ' => 'k',
            'λ' => 'l',
            'μ' => 'm',
            'μπ' => 'b',
            'ν' => 'n',
            'ντ' => 'nt',
            'ξ' => 'x',
            'ο' => 'o',
            'οι' => 'oi',
            'ου' => 'ou',
            'π' => 'p',
            'ρ' => 'r',
            'σ' => 's',
            'ς' => 's',
            'τ' => 't',
            'υ' => 'y',
            'υι' => 'yi',
            'φ' => 'f',
            'χ' => 'ch',
            'ψ' => 'ps',
            'ω' => 'o',
            'ωυ' => 'oy',
            'Ή' => 'H',
            'Ί' => 'I',
            'ή' => 'h',
            'Ά' => 'A',
            'ά' => 'a',
        ];


        $roman = $data;
        $greek = array_keys($data);

        return str_replace($greek, $roman, $text);
    }

    public static function sizeof($val)
    {
        return self::sizeof2($val);

        if(is_array($val) || $val instanceof Countable)
        {
            return sizeof($val);
        }

        return false;
    }

    public static function sizeof2($val)
    {
        if(is_array($val) || $val instanceof Countable)
        {
            return sizeof($val);
        } else if(isset($val))
            return true;

        return false;
    }

    static function compareAttributesBeforeAfter(array $before, array $after, $returnAsString = false, $excludeAttr = ['date_updated'])
    {
        $totalDiffAttr = array_keys(array_diff_assoc($before, $after) + array_diff_assoc($after, $before));

        $result = [];

        foreach($totalDiffAttr AS $attr)
        {
            if(in_array($attr, $excludeAttr))
                continue;

            $result[$attr] = [
                $before[$attr],
                $after[$attr]
            ];
        }

        if($returnAsString)
        {
            $return = [];

            foreach($result AS $attr => $item)
            {
                $return[] = '['.$attr.']: "'.$item[0].'" => "'.$item[1].'"';

            }

            $return = implode(' ||| ', $return);
            return $return;
        } else
            return $result;
    }

}