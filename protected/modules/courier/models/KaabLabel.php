<?php

class KaabLabel
{
    /**
     * Generates KAAB own label and returns it as Image
     * @param Courier $item
     * @param integer $i
     * @param AddressData|NULL $senderData
     * @param AddressData|NULL $receiverData
     * @param boolean|false $receiverData Whether return string or array
     * @param integer|false $totalNumber Total number of labels (just for printing it)
     * @param string|false $customNote Custom note
     * @param string|false $specialSymbol Special symbol to be placed on label
     * @return String
     */
    static function generate(Courier $item, $i, AddressData $senderData = NULL, AddressData $receiverData = NULL, $asArray = false, $totalNumber = false, $asImageBlob = true, $customNote = false, $specialSymbol = false, $secondBarcode = false)
    {
        if(!$totalNumber)
            $totalNumber = $item->packages_number;


        $pageW = 100;
        $pageH = 113;

        // GENERATE OWN:
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pdf->addPage('H', array($pageW,$pageH));

        $margins = 1;

        if($senderData !== NULL)
            $item->senderAddressData = $senderData;

        if($receiverData !== NULL)
            $item->receiverAddressData = $receiverData;

        $note = $customNote;

        /* @var $pdf TCPDF */
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $yBase = 0;
        $xBase = 0;

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->MultiCell($blockWidth, $blockHeight, '', 1, 'J', false, 0, $xBase + $margins, $yBase + $margins, true, 0, false, true, 0);

        $pdf->Image('@'.PDFGenerator::getLogo(), $xBase + $margins + 4, $pdf->GetY() + 1, 8,'','','N',false);

        $pdf->SetTextColor(0,0,0);

        $pdf->SetFont('freesans', '', 20);
        $pdf->MultiCell($blockWidth - 20,10, "#".$item->local_id, 0, 'R', false, 1, $xBase + 17 + $margins, $yBase + $margins + 1, true, 0, false, true, 0);

        $pdf->Line($xBase + $margins,$yBase + $margins + 10,$xBase + $blockWidth + $margins, $yBase + $margins + 10);

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

        $pdf->SetFont('freesans', 'B', 8);

        $pdf->MultiCell(($blockWidth /2) - 4,5, 'FROM:', 0, 'L', false, 1, $xBase + $margins + 1, $yBase + 12 + $margins, true, 0, false, true, 0);

        $pdf->MultiCell(($blockWidth /2) - 4,5, 'SHIP TO:', 0, 'R', false, 1, $xBase + $blockWidth/2 + 3 + $margins , $yBase + 12 + $margins, true, 0, false, true, 0);

        $pdf->SetFont('freesans', '', 10);


        $text = '';
        if($item->senderAddressData->company != '')
            $text .= $item->senderAddressData->company."\r\n";
        if($item->senderAddressData->name != '')
            $text .= $item->senderAddressData->name."\r\n";
        if($item->senderAddressData->address_line_1 != '')
            $text .= $item->senderAddressData->address_line_1."\r\n";
        if($item->senderAddressData->address_line_2 != '')
            $text .= $item->senderAddressData->address_line_2."\r\n";
        if($item->senderAddressData->zip_code != '')
            $text .= $item->senderAddressData->zip_code."\r\n";
        if($item->senderAddressData->city != '')
            $text .= $item->senderAddressData->city."\r\n";
        if($item->senderAddressData->country != '')
            $text .= $item->senderAddressData->country."\r\n";

        $pdf->MultiCell(($blockWidth /2) - 4,33, $text, 0, 'L', false, 1, $xBase + $margins + 1, $yBase + 17 + $margins, true, 0, false, true, 33, '', true);

        $text = '';
        if($item->receiverAddressData->company != '')
            $text .= $item->receiverAddressData->company."\r\n";
        if($item->receiverAddressData->name != '')
            $text .= $item->receiverAddressData->name."\r\n";
        if($item->receiverAddressData->address_line_1 != '')
            $text .= $item->receiverAddressData->address_line_1."\r\n";
        if($item->receiverAddressData->address_line_2 != '')
            $text .= $item->receiverAddressData->address_line_2."\r\n";
        if($item->receiverAddressData->zip_code != '')
            $text .= $item->receiverAddressData->zip_code."\r\n";
        if($item->receiverAddressData->city != '')
            $text .= $item->receiverAddressData->city."\r\n";
        if($item->receiverAddressData->country != '')
            $text .= $item->receiverAddressData->country."\r\n";


        $pdf->MultiCell(($blockWidth /2) - 4,33, $text, 0, 'R', false, 1, $xBase + $blockWidth/2 + 3 + $margins , $yBase + 17 + $margins, true, 0, false, true, 33, '', true);

        $pdf->Line($xBase + $margins + ($blockWidth /2), $yBase + $margins + 13,$xBase + $margins + ($blockWidth /2), $yBase + $margins + 50);

        $pdf->Line($xBase + $margins, $yBase + $margins + 50,$xBase + $blockWidth + $margins, $yBase + $margins + 50);

        $text =  "Size: $item->package_size_l x $item->package_size_w x $item->package_size_d"." [cm] \r\n".
            "Weight: $item->package_weight"." [kg]";

        $pdf->MultiCell(($blockWidth /2) - 6,10, $text, 0, 'L', false, 1, $xBase + $margins + 1, $yBase + 51 + $margins, true, 0, false, true, 10,'', true);


        if(!$i)
            $i = 1;
        $pdf->SetFont('freesans', '', 20);
        $text = $i.' / '.$totalNumber;
        $pdf->MultiCell(45,'', $text, 0, 'R', false, 1, $xBase + $blockWidth/2 + 3 , $yBase + 51 + $margins, true, 0, false, true, 0);


        $pdf->SetFont('freesans', '', 10);
        $pdf->Line($xBase + $margins + ($blockWidth /2), $yBase + $margins + 51,$xBase + $margins + ($blockWidth /2), $yBase + $margins + 61);

        $pdf->Line($xBase + $margins, $yBase + $margins + 61,$xBase + $blockWidth + $margins, $yBase + $margins + 61);

        $text =  "Cust. ID: ".$item->user_id;
        $pdf->MultiCell(50,10, $text, 0, 'L', false, 1, $xBase + $margins + 1, $yBase + 62 + $margins, true, 0, false, true, 0);

        $pdf->SetFont('freesans', '', 8);
        $text =  "Ref. no: ".$item->ref;
        $pdf->MultiCell(50,5, $text, 0, 'L', false, 1, $xBase + $margins + 1, $yBase + 67 + $margins, true, 0, false, true, 5,'', true);

        $text =  "Note: ".$note;
        $pdf->MultiCell(50,5, $text, 0, 'L', false, 1, $xBase + $margins + 1, $yBase + 72 + $margins, true, 0, false, true, 5,'', true);

        if($secondBarcode)
            $pdf->write1DBarcode($secondBarcode, 'C128',  52, $yBase + 62 + $margins, 36, 15, 0.4, $barcodeStyle, 'N');


        if($specialSymbol)
        {
            $pdf->SetFont('freesans', 'B', 35);
            $pdf->MultiCell(14,15, $specialSymbol, 0, 'C', false, 1, $xBase + $blockWidth - 13, $yBase + 62, true, 0, false, true, 15, 'T', true);
        }


        $pdf->Line($xBase + $margins, $yBase + $margins + 77,$xBase + $blockWidth + $margins, $yBase + $margins + 77);

        $pdf->SetFont('freesans', '', 10);

        if($item->cod)
        {
            $text =  "COD: ".$item->cod_value.' '.$item->cod_currency;
            $pdf->MultiCell(($blockWidth) - 4,5, $text, 0, 'R', false, 1, $xBase + $margins + 3, $yBase + 78 + $margins, true, 0, false, true, 5, '', true);
        }

        $pdf->Line($xBase + $margins, $yBase + $margins + 83,$xBase + $blockWidth + $margins, $yBase + $margins + 83);


        //$pdf->write1DBarcode($item->local_id, 'C128', $xBase + $margins + 0, $yBase + $margins + 82, $blockWidth - 0, 22,1, $barcodeStyle);
        $pdf->write1DBarcode($item->local_id, 'C128',  $xBase + $margins + 15, $yBase + $margins + 84, '', 18, 0.4, $barcodeStyle, 'N');


        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins, $yBase + $margins + 102, $xBase + $blockWidth + $margins, $yBase + $margins + 102);

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell($blockWidth,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, $xBase + $margins , $yBase + 102 + $margins, true, 0, false, true, 0);
        $pdf->MultiCell($blockWidth,6, 'Member of KAAB GROUP', 0, 'C', false, 1, $xBase + $margins , $yBase + 106 + $margins, true, 0, false, true, 0);

        $pdfString = $pdf->Output('asString', 'S');

        if($asImageBlob) {
            $img = ImagickMine::newInstance();
            $img->setResolution(300, 300);
            $img->readImageBlob($pdfString);
            $img->setImageFormat('png8');

            if ($img->getImageAlphaChannel()) {
                $img->setImageBackgroundColor('white');
                $img->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $img->stripImage();
            $image = $img->getImageBlob();
        } else
            $image = $pdfString;

        if($asArray)
            $image = [$image];

        return $image;
    }
}