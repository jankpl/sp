<?php

class _UserInvoiceGridView extends UserInvoice
{
    public $_isPaid;

    public function rules() {

        $array = array(

            array('
            _isPaid,
            ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        if($this->_isPaid === '1')
        {
            $criteria->addCondition('paid_value >= inv_value');
        }
        else if($this->_isPaid === '0')
        {
            $criteria->addCondition('paid_value < inv_value');
        }


        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);

        $criteria->compare('stat', $this->stat);
        $criteria->compare('stat_inv', $this->stat_inv);
        $criteria->compare('inv_currency', $this->inv_currency);

        $criteria->compare('title', $this->title, true);
        $criteria->compare('no', $this->no);
        $criteria->compare('source', $this->source);

        $criteria->compare('user_id', $this->user_id);

        $criteria->compare('inv_value', $this->inv_value, true);
        $criteria->compare('paid_value', $this->paid_value, true);
        $criteria->compare('inv_payment_date', $this->inv_payment_date, true);
        $criteria->compare('inv_date', $this->inv_date, true);

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
            $criteria->compare('user.salesman_id', Yii::app()->user->model->salesman_id);

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';

        $criteria = $this->searchCriteria();

        // if there are no conditions (except for User ID), use simple count of packages to improve performance
        $totalCount = NULL;



        return new CActiveDataProvider($this

            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize'=> Yii::app()->user->getState('pageSize', 50),
                ),
            ));

    }


}