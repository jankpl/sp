<?php

class QuequeCommand extends ConsoleCommand {

    public function getTheme(){
        return NULL;
    }

    public function actionIndex() {

         $CACHE_NAME = 'QUEQUE_LOCK_CMD';

        if (Yii::app()->cache->get($CACHE_NAME) !== false) {
            MyDump::dump('queque-lock-cmd.txt','LOCK : ');
            return false;
        }
        Yii::app()->cache->set($CACHE_NAME, 10, 15);

        $load = S_SystemLoad::get();
        $isHigh = S_SystemLoad::isHigh();

        MyDump::dump('queque_load-cmd.txt', ($isHigh ? 'BLOCK' : 'GO').' : '.print_r($load,1));

        if($isHigh)
            return false;

        $id = uniqid();

        MyDump::dump('queque-cmd.txt',$id.':START');
        Yii::app()->Queque->run(500);
        MyDump::dump('queque-cmd.txt',$id.':END');
        // there is more
        if(Yii::app()->Queque->howLong())
        {
            MyDump::dump('queque-cmd.txt',$id.':MORE');
//            $path = Yii::app()->createAbsoluteUrl('/queque/index', ['more' => ++$more]);
//
//            header("Location: ".$path);
//            exit;
            Yii::app()->end();

        } else {
            // clear old items if there is nothing else to do...
            Yii::app()->Queque->clearOldAndFinished();

            PhpErrorToYiiLog::run('rror.txt', 'PHP FRONT ERROR');
            PhpErrorToYiiLog::run('rror2.txt', 'PHP BACK ERROR');
        }

    }

}