<?php

class PostalLabelQuequeCommand extends ConsoleCommand {

    const LIMIT = 50;

    public function init()
    {
        Yii::app()->getModule('postal');
        parent::init();
    }

    public function filters()
    {
        return array();
    }

    function actionDeamon()
    {
        while(1)
        {
            $models = PostalLabel::model()->with('postal')->findAll(array('condition' => 'stat = :stat', 'params' => array(':stat' => PostalLabel::STAT_NEW), 'order' => 't.id ASC', 'limit' => self::LIMIT));

            // no more found - finish the job...
            if(S_Useful::sizeof($models))
            {
                foreach ($models AS $item) {
                    echo $item->postal->local_id."\r\n";
                    MyDump::dump('cmd_postal_label.txt', 'Process: '.$item->postal->local_id);
                    $item->runLabelOrdering();
                }
            } else
                echo '.'."\r\n";

            sleep(1);
        }
    }

    function actionRunOne($id = NULL)
    {
        Yii::app()->getModule('courier');

        $item = PostalLabel::model()->findAll(array('condition' => '(id = :id OR parent_id = :id)', 'params' => array(':id' => $id), 'order' => 'id ASC', 'limit' => 1));
        $item->runLabelOrdering();
        Yii::app()->end();
    }



}