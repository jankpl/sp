<?php
/* @var $id_prefix string */
?>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-invoice',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/invoice'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('courier', 'Generate Invoice'), array(
    'onClick' => 'js:
    $("#'.$id_prefix.'courier-ids-invoice").val($("#couriergrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => $id_prefix.'courier-ids-invoice')); ?>
<?php
$this->endWidget();
?>

<br/>
<small><?= Yii::t('courier', 'Cancelled packages will be omitted.');?></small>
