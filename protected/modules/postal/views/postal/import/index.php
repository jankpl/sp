<div id="upload-form">
    <div class="form">
        <?php

        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
            array(
                'enableAjaxValidation' => false,
                'htmlOptions' =>
                    array('enctype' => 'multipart/form-data'),
            )
        );
        ?>

        <h2><?php echo Yii::t('postal','Import packages from file');?></h2>

        <?php
        $this->widget('FlashPrinter');
        ?>

        <?php

        echo $form->errorSummary($model);
        ?>

        <table class="table table-striped" style="margin: 10px auto;">
            <tr>
                <td style="width: 300px;"><?php echo $form->labelEx($model,'file', ['style' => 'width: 100%;']); ?></td>
                <td><?php echo $form->fileField($model, 'file'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'omitFirst', ['style' => 'width: 100%;']); ?></td>
                <td><?php echo $form->checkBox($model, 'omitFirst'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'cutTooLong', ['style' => 'width: 100%;']); ?></td>
                <td><?php echo $form->checkBox($model, 'cutTooLong'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'custom_settings', ['style' => 'width: 100%;']); ?></td>
                <td>
                    <?php echo $form->dropdownList($model,'custom_settings', CHtml::listData(CourierImportSettings::listModelsForUserId(Yii::app()->user->id, CourierImportSettings::POSTAL),'id','name'), ['id' => 'custom-settings', 'prompt' => '-']); ?> | <input type="button" class="btn btn-xs btn-default" id="open-settings-modal" data-settings-modal="edit" value="<?= Yii::t('postal', 'Customize settings');?>" style="display: none;"/> <input type="button" data-settings-modal="new" class="btn btn-xs btn-info" id="new-settings-modal" value="<?= Yii::t('postal', 'Add new settings');?>"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><?php echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('class' => 'btn btn-lg btn-primary'));?></td>
            </tr>
        </table>
        <?php
        $this->endWidget();
        ?>

        <div class="info">
            <div class="alert alert-info">
                <?php echo Yii::t('postal', 'Maksymalna liczba wierszy w pliku to: {number1}. Maksymalna liczba paczek w pliku to: {number2}.', array('{number1}' => '<strong>'.F_PostalImport::MAX_LINES.'</strong>', '{number2}' => '<strong>'.F_PostalImport::MAX_PACKAGES.'</strong>')); ?>
            </div>
            <p><?php echo Yii::t('postal','Allowed file type:');?> .csv,.xls,.xlsx.</p>
            <br/>
            <p><?php echo Yii::t('postal','The file must contain following columns:');?><br/>
                [postal_type],
                [size_class],
                [weight],
                [traget_sp_point],
                [sender_name],
                [sender_company],
                [sender_zip_code],
                [sender_city],
                [sender_address_line_1],
                [sender_address_line_2],
                [sender_tel],
                [sender_email],
                [receiver_name],
                [receiver_company],
                [receiver_zip_code],
                [receiver_city],
                [receiver_address_line_1],
                [receiver_address_line_2],
                [receiver_tel],
                [receiver_email],
                [content],
                [ref],
                [value],
                [value_currency],
                [note1],
                [note2]
            </p>
            <br/>
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <p>
                        [postal_type]:</p>
                    <ul>
                        <?php
                        foreach(Postal::getPostalTypeList() AS $key => $item):
                            ?>
                            <li><strong><?= Postal::postalTypeIdToShortName($key);?></strong> - <?= $item;?></li>
                            <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <p>
                        [size_class]:</p>
                    <ul>
                        <?php
                        foreach(Postal::getSizeClassList() AS $key => $item):
                            ?>
                            <li><strong><?= $key;?></strong> - <?= $item;?></li>
                            <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <p>
                        [target_sp_point]:</p>
                    <ul>
                        <?php
                        /* @var $item SpPoints */
                        foreach(SpPoints::getPoints() AS $key => $item):
                            ?>
                            <li><strong><?= $item->id;?></strong> - <?= $item->spPointsTr->title;?></li>
                            <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <p>
                        [value_currency]:</p>
                    <ul>
                        <?php

                        foreach(Postal::getCurrencyList() AS $key => $item):
                            ?>
                            <li><strong><?= $item; ?></strong></li>
                        <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
            </div>

            <br/>
            <?php echo CHtml::link(Yii::t('postal','Download file template'), Yii::app()->baseUrl.'/misc/ipff_template_postal.xlsx', array('target' => '_blank', 'class' => 'btn btn-success', 'download' => 'ipff_template_postal.xlsx'));?>
        </div>
    </div>
</div>
<?php
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/custom-theme/jquery-ui-1.10.3.custom.min.css');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courier-import-settings.js');

$script = "prepareSettingsModal({
        textConfirmDelete: '".Yii::t('postal', 'Na pewno?')."',
        textError: '".Yii::t('postal', 'Wystąpił bład zapisywania. Spróbuj ponownie.')."',
        ajaxUrl: '".Yii::app()->urlManager->createUrl('/courier/courier/ajaxImportModalSettings')."',
         type: '".CourierImportSettings::POSTAL."',
    });";
Yii::app()->clientScript->registerScript('postal-import-settings', $script);
?>