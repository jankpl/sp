<?php

class _UserDebtCollectionGridView extends UserDebtCollection
{
    public $_isPaid;
    public $_daysAfterPayment;
    public $_salesman_email;
    public $_debt_status;
    public $_debt_status_date;
    public $_contract_type;
    public $_source;

    public function rules() {

        $array = array(

            array('
            _isPaid, _daysAfterPayment, _salesman_email, _debt_status, _debt_status_date, _contract_type, _source
            ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;
        $criteria->select = [
            'IF(inv_value > paid_value AND stat_inv != ' . UserInvoice::INV_STAT_CANCELLED . ', DATEDIFF(CURDATE(),inv_payment_date), 0) AS overdue_days',
            '*',
            'userInvoiceDebtOptionsLast.*'
        ];

        if ($this->_isPaid === '1') {
            $criteria->addCondition('paid_value >= inv_value');
        } else if ($this->_isPaid === '2') {
            $criteria->addCondition('paid_value < inv_value && paid_value > 0');
        } else if ($this->_isPaid === '0') {
            $criteria->addCondition('paid_value = 0');
        }

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);

//        $criteria->compare('stat', $this->stat);
        $criteria->compare('stat_inv', $this->stat_inv);
        $criteria->compare('inv_currency', $this->inv_currency);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('no', $this->no);
        $criteria->compare('source', $this->source);
        $criteria->compare('inv_date', $this->inv_date, true);
        $criteria->compare('inv_payment_date', $this->inv_payment_date, true);
        $criteria->compare('inv_value', $this->inv_value, true);
        $criteria->compare('paid_value', $this->paid_value, true);
        $criteria->compare('source', $this->_source);

        $criteria->compare('t.user_id', $this->user_id);

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
            $criteria->compare('user.salesman_id', Yii::app()->user->model->salesman_id);

        $this->with('userInvoiceDebtOptionsLast');
        if ($this->_debt_status)
            $criteria->compare('userInvoiceDebtOptionsLast.stat', $this->_debt_status);


        if ($this->_debt_status_date)
            $criteria->compare('userInvoiceDebtOptionsLast.date_entered', $this->_debt_status_date, true);


        if($this->_salesman_email)
        {
            $criteria->join = 'JOIN user_options user_options_s ON (name = "salesman" AND user_options_s.user_id = t.user_id) JOIN salesman ON (salesman.id = user_options_s.value AND salesman.email LIKE "%'.$this->_salesman_email.'%")';
        }


        if($this->_contract_type)
        {
            $criteria->join = 'JOIN user_options user_options_s2 ON (name = "getContractType" AND user_options_s2.user_id = t.user_id AND user_options_s2.value = '.intval($this->_contract_type).')';
        }


        $criteria->addCondition('inv_payment_date <= '.new CDbExpression('NOW()'));

//        $criteria->addCondition('t.stat = '.UserInvoice::PUBLISHED.' OR t.stat = '.UserInvoice::SEEN);

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes = array(
            '_daysAfterPayment' => array(
                'asc' => 'overdue_days ASC',
                'desc' => 'overdue_days DESC',
            ),
            '*', // add all of the other columns as sortable
        );

        $criteria = $this->searchCriteria();
        // if there are no conditions (except for User ID), use simple count of packages to improve performance
        $totalCount = NULL;


        return new CActiveDataProvider($this

            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize'=> Yii::app()->user->getState('pageSize', 50),
                ),
            ));

    }


}