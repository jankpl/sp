<?php

Yii::import('application.models._base.BaseOrderStat');

class OrderStat extends BaseOrderStat
{
	const NEW_ORDER = 1;
    //const NOT_FINALIZED = 2;
    const WAITING_FOR_PAYMENT = 2;
    const FINISHED = 3;
    //const FINISHED = 5;
    const CANCELLED = 99;


    public static function model($className=__CLASS__) {
		return parent::model($className);
	}


    public function getNameTr()
    {
        return Order::statTranslations()[$this->id];
    }
}