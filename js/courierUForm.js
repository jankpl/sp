(function($) {

    var COOKIE_TOGGLE_ADDITIONS = 'courierUToggleAdditons';
    var COOKIE_SORTING_ORDER = 'courierUSortingOrder';

    var ignoreFirstUpdate = true;

    var viewTimeoutHandler;
    var formActive = true;

    var senderCountryFullCache;

    var isCodShown = false;
    var isCodAvailable = false;
    var selectedOperatorId = false;
    var dimWeightMode = false;

    var currentSortOrder = false;

    var collectionAvailable = false;
    var collectionSelected = false;


    $(document).ready(function(){

        checkAutomaticReturnAvailable();
        // setAutomaticReturn();

        $('[data-bootstrap-checkbox]').checkboxpicker({
            offClass : 'btn-default btn-lg',
            onClass : 'btn-primary btn-lg',
            defaultClass: 'btn-lg',
        });

        $('#_collection').on('change', function(){

            if($(this).is(':checked'))
            {
                collectionSelected = true;
            }
            else
            {
                collectionSelected = false;
            }
            getRemoteArea('sender');
        });




        $('#show-requirements').on('click', toggleRequirements);

        preToggleAdditions(getCookie(COOKIE_TOGGLE_ADDITIONS), false);

        setOperatorsSortingOrder(getCookie(COOKIE_SORTING_ORDER));

        $('[data-sort]').on('click', changeOperatorsSortingOrder);

        $('[data-package-wrapping]').on('change', wrapping2Addition);

        $('.courier-u-additions-toggle a').on('click', function(){
            toggleAdditions();
        });



        if($('#_client_cod').is(':checked'))
            isCodShown = true;

        var _pre_selected_operator_id = $('#_selected_operator_id').val();
        if(_pre_selected_operator_id)
            selectedOperatorId = _pre_selected_operator_id;

        idCodShown = $('#cod-details').is(':visible');

        var data = $('#country_label_sender').attr('data-content');

        try {
            senderCountryFullCache = JSON.parse(data);
        } catch(e) {
        }

        $('#Courier_CourierTypeU_package_weight').on('change', updateView);


        prepareAdditions();

        if($('[data-address-more]').find('input[type=text]').val()!='')
            $('[data-address-more]').show();

        $('[data-show-address-more]').on('click', showAddressMore);


        $("[data-dimensions]").TouchSpin({
            step: 1,
            decimals: 0,
            max: 400,
            min: 1,
            boostat: 5,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });

        $("[data-weight]").TouchSpin({
            step: 0.1,
            decimals: 2,
            boostat: 5,
            max: parseInt($(this).attr('data-max-weight')),
            min: 0.01,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });

        $("[data-weight-total]").TouchSpin({
            step: 0.1,
            decimals: 2,
            boostat: 5,
            max: (parseInt($(this).attr('data-max-weight')) * parseInt($(this).attr('data-max-no'))),
            min: 0.01,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });

        $('.bootstrap-touchspin-down').attr('tabindex', -1);
        $('.bootstrap-touchspin-up').attr('tabindex', -1);

        $('[data-address-line-2]').on('change', function(){
            $(this).closest('[data-address-more]').slideDown();
            $(this).closest('.row').prev().find('[data-show-address-more]').hide();
        });

        $('#courier-u-form').on('submit', function(e)
        {
            if(!formActive) {
                e.preventDefault();
                $('.overlay').remove();
                return false;
            }
        });

        // $('#_client_cod').on('change', codChanged);


        $('#Courier_CourierTypeU_packages_number, #Courier_CourierTypeU__package_weight_total, #Courier_CourierTypeU_package_weight').on('change', checkWeight);
        $('#Courier_CourierTypeU_package_size_l, #Courier_CourierTypeU_package_size_w, #Courier_CourierTypeU_package_size_d').on('change', dimWeightValidation());


        $('[name="country_suggester_trigger"]').on('change', updateView);

        $('[data-dependency]').on('change', updateView);

        dimWeightValidation();

        checkWeight();

        $('[data-packages-no]').on('change', updatePackagesNo);

        updatePackagesNo();

        getOperators();




        $('[data-remote-area="sender"]').on('change', function(){
            getRemoteArea('sender');
        });

        $('[data-remote-area="receiver"]').on('change', function(){
            getRemoteArea('receiver');
        });






    });

    var FormControl = (function() {
        "use strict";

        var that = {};

        that.submit = function () {
            var $form = $("#courier-u-form");

            var $input = $('<input>');
            $input.attr('name', 'forceReload');
            $form.append($input);

            formActive = true;
            $form.submit();
        };
        return that;
    }());

    var Overlay = (function() {
        "use strict";

        var that = {};
        var $overlay = $("<div>");
        var $textBottom = $("<div>");
        var $textTop = $("<div>");
        var $loader = $("<div>");

        that.isShow = function(){
            return $("#overlay").is(':visible');
        };

        that.show = function() {
            var $form = $("#courier-u-form");

            $overlay.addClass('overlay');


            $loader.addClass('loader');

            $overlay.append($loader);


            $textTop.addClass('loader-text-top');
            $textTop.html(yii.loader.textTop);


            $textBottom.addClass('loader-text-bottom');
            $textBottom.html(yii.loader.textBottom);

            $overlay.append($textTop);
            $overlay.append($textBottom);

            $form.css('position','relative');
            $form.append($overlay);

            $textTop.hide().delay(1000).fadeIn();
            $textBottom.hide().delay(3000).fadeIn();
        };

        that.hide = function() {
            $overlay.remove();
        };

        return that;
    }());

    function wrapping2Addition()
    {
        if($('[data-package-wrapping]').val() != 'box')
        {
            if(!$('[data-addition-id=1]').parent().find('a[is=1]').hasClass('active') && $.inArray( selectedOperatorId.toString(), yii.customTypeOperatorsIds) == -1) {

                preToggleAdditions(false, true);

                $.notify({
                    icon: 'glyphicon glyphicon-flag',
                    message: yii.message.customWrapping,
                    type: 'warning',
                },{
                    allow_dismiss: false,
                    showProgressbar: true,
                });
                $('[data-addition-id=1]').trigger('change').attr('checked', true);
            }
        }
    }

    function operatorsSorting()
    {
        var $operatorsPlaceholder = $('#operators-list .courier-u-operators-selector-form');

        if(currentSortOrder) {

            _markSortingOrder();

            var $operators = $('#operators-list').find('label');

            var temp = currentSortOrder.split('|');
            var sortAttr = temp[0];
            var reverse = temp[1];

            $operators.sort(function (a, b) {

                var contentA = ( $(a).attr('data-' + sortAttr));
                var contentB = ( $(b).attr('data-' + sortAttr));

                var availableA = ( $(a).attr('data-available'));
                var availableB = ( $(b).attr('data-available'));

                try {
                    reverse = JSON.parse(reverse);
                } catch (e) {
                    reverse = false;
                }

                if ((!availableA || !availableB) && availableA != availableB) {
                    return (availableA > availableB) ? -1 : (availableA < availableB) ? 1 : 0;
                }

                if (sortAttr === 'name') {
                    var temp = contentA;
                    contentA = contentB;
                    contentB = temp;
                }

                if (!reverse)
                    return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
                else
                    return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;

            });

            $operatorsPlaceholder.empty();

            $operators.each(function (i, v) {
                $operatorsPlaceholder.append(v);
            });
        }

        $operatorsPlaceholder.find('input[name="CourierTypeU[courier_u_operator_id]"]').unbind('change').on('change', operatorChanged);

    }

    function changeOperatorsSortingOrder()
    {

        var sortOrder = $(this).attr('data-sort');

        if(currentSortOrder)
        {
            var temp = currentSortOrder.split('|');
            var sortAttr = temp[0];
            var reverse = temp[1];

            try {
                reverse = JSON.parse(reverse);
            } catch(e) {
                reverse = false;
            }

            if(sortAttr == sortOrder)
                reverse = !reverse;
            else
                reverse = false;

            currentSortOrder = sortOrder + '|' + reverse;
        } else {
            currentSortOrder = sortOrder + '|' + false;
        }

        setCookie(COOKIE_SORTING_ORDER,currentSortOrder,365);
        operatorsSorting();
    }

    function setOperatorsSortingOrder(order)
    {

        if(order && currentSortOrder != order) {

            currentSortOrder = order;
            operatorsSorting();
        }
    }

    function _markSortingOrder()
    {
        var ARROW = 'glyphicon glyphicon-arrow-down';
        var ARROW_REVERSE = 'glyphicon glyphicon-arrow-up';

        var temp = currentSortOrder.split('|');
        var sortAttr = temp[0];
        var reverse = temp[1];

        var cssClass = ARROW;

        try {
            reverse = JSON.parse(reverse);
        } catch(e) {
            reverse = false;
        }

        if(reverse)
            cssClass = ARROW_REVERSE;

        $('[data-sort] span').removeClass();
        $('[data-sort="' + sortAttr + '"] span').addClass(cssClass);

    }

    function preToggleAdditions(forceHide, forceShow)
    {
        try {
            forceHide = JSON.parse(forceHide);
            forceShow = JSON.parse(forceShow);
        } catch(e) {
            forceHide = false;
            forceShow = false;
        }


        if(forceHide)
        {
            _hideAdditions(true);
        }
        else if(forceShow)
        {
            _showAdditions();
        }
    }


    function toggleAdditions()
    {

        var $additonsList = $('.additions-list-wrapper');

        if($additonsList.is(':visible'))
        {
            _hideAdditions(false);
        } else {
            _showAdditions();
        }
    }

    function _hideAdditions(fast)
    {
        var $button = $('.courier-u-additions-toggle a');
        var $additonsList = $('.additions-list-wrapper');

        $button.html($button.attr('data-down'));

        if(fast)
            $additonsList.hide();
        else
            $additonsList.slideUp();

        setCookie(COOKIE_TOGGLE_ADDITIONS,true,365);
    }

    function _showAdditions()
    {
        var $button = $('.courier-u-additions-toggle a');
        var $additonsList = $('.additions-list-wrapper');

        $button.html($button.attr('data-up'));
        $additonsList.slideDown();
        setCookie(COOKIE_TOGGLE_ADDITIONS,false,365);
    }

    function operatorChanged() {

        var id = parseInt($(this).closest('label').attr('data-operator-id'));
        selectedOperatorId = id;

        $('#_selected_operator_id').val(id);

        var broker_id = parseInt($(this).closest('label').attr('data-broker-id'));
        var cod = parseInt($(this).closest('label').attr('data-cod'));
        var collection = parseInt($(this).closest('label').attr('data-collection'));
        var cod_price_n = ($(this).closest('label').attr('data-cod-n'));
        var cod_price_b = ($(this).closest('label').attr('data-cod-b'));

        specialOperatorDescription($('input[name="CourierTypeU_AddressData[receiver][country_id]"]').val(), broker_id);

        collectionAvailable = parseInt($(this).closest('label').attr('data-collection'));
        dimWeightMode = $(this).closest('label').attr('data-dim-weight-mode');

        dimWeightValidation();

        additionsSelectAvailable(broker_id, cod, collection);
        printCodPrice(cod_price_n, cod_price_b);

        getRemoteArea('sender');
        getRemoteArea('receiver');



        $('[data-continue]').show();
        enableButton($('[data-continue-button]'));

    }

    function dimWeightValidation()
    {
        if(dimWeightMode) {

            calculateDimensionaWeight(dimWeightMode);
            $('[data-dim-weight-info]').show();
        }
        else
            $('[data-dim-weight-info]').hide();
    }

    function showCodDetails()
    {
        getCodCurrency();
        if(isCodAvailable)
            $('#cod-details').show();

    }

    function printCodPrice(cod_price_n, cod_price_b)
    {
        if(parseInt(cod_price_n) == 0 || cod_price_n === false)
            cod_price_n = cod_price_b = '-';

        $('#cod-price-n-value').html(cod_price_n);
        $('#cod-price-b-value').html(cod_price_b);
    }


    function additionsSelectAvailable(broker_id, cod, collection)
    {
        checkAdditions();

        if(broker_id)
        {
            $('[data-addition-broker-id]').not('[data-addition-broker-id=' + broker_id + ']').not('[data-addition-broker-id=""]').not('[data-addition-id="cod"]').addClass('not_available').hide();
            $('[data-addition-broker-id=' + broker_id + '],[data-addition-broker-id=""]').removeClass('not_available').show();
        } else {
            $('[data-addition-broker-id]').not('[data-addition-id="cod"]').addClass('not_available');
        }

        if (cod) {

            isCodAvailable = true;
            getCodCurrency();

            if(isCodAvailable) {

                $('[data-addition-id="cod"]').removeClass('not_available');
                if (isCodShown)
                    showCodDetails();
            }
        }
        else
        {
            $('#cod-details').hide();
            $('[data-addition-id="cod"]').addClass('not_available');
        }

        if (collection) {
            $('[data-addition-id="collection"]').removeClass('not_available');
        }
        else
        {
            $('[data-addition-id="collection"]').addClass('not_available');
        }


        $('[data-addition-broker-id=""]:not([data-addition-id="cod"]):not([data-addition-id="collection"])').removeClass('not_available');

        var $additions = $('.additions-list').find('.addition_item');

        $additions.sort(function (a, b) {

            var contentA = ( $(a).hasClass('not_available'));
            var contentB = ( $(b).hasClass('not_available'));
            return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;

        });

        $('.additions-list').empty();

        $additions.each(function (i, v) {
            $('.additions-list').append(v);
        });

        $('[data-courier-addition]').unbind().removeData();
        $('[data-courier-addition]').next('div').remove();
        $('#_client_cod').unbind('change').on('change', codChanged);
        $('[data-copy-bank-account]').unbind('click').on('click', function(){
            $('[data-bank-account-target]').val($(this).attr('data-copy-bank-account'))
        });
        $('[data-courier-addition]').checkboxpicker({
            offClass : 'btn-default btn-sm',
            onClass : 'btn-primary btn-sm',
            defaultClass: 'btn-sm',
        });

    }

    function getOperators()
    {

        var $additionList = $('.additions-list');

        var $operatorsList = $('#operators-list');
        var $operatorsEmpty = $('#operators-empty');
        var $operatorsNone = $('#operators-none');
        var $operatorsContent = $('#operators-content');

        var $notAvailable = $('[data-not-available]');
        var $continueButton = $('[data-continue-button]');

        $('[data-addition-broker-id!=""]').find('.item_col_img div').empty();
        $('[data-addition-broker-id=""]').find('.item_col_img div').empty();
        $('[data-addition-id="cod"]').find('.item_col_img div').empty();

        var receiver_country_id = $('#CourierTypeU_AddressData_receiver_country_id').val();
        var weight = $('[data-package-weight]').val();
        var size_l = $('[data-dimensions="l"]').val();
        var size_d = $('[data-dimensions="d"]').val();
        var size_w = $('[data-dimensions="w"]').val();

        if(receiver_country_id && size_l && size_d && size_w && weight) {


            $.ajax({
                url: yii.urls.getOperators,
                data: {receiver_country_id: receiver_country_id, weight: weight, selected_operator: selectedOperatorId, size_l : size_l, size_d : size_d, size_w : size_w},
                method: 'POST',
                dataType: 'json',
                success: function (result) {

                    $operatorsEmpty.hide();

                    if (result.no) {
                        $operatorsList.html(result.html);
                        $operatorsContent.show();
                        $operatorsNone.hide();
                        $notAvailable.hide();
                        $continueButton.prop('disabled', false).removeClass('disabled');
                        $additionList.show();
                    } else {
                        $operatorsContent.hide();
                        $operatorsList.html('');
                        $operatorsNone.show();
                        $notAvailable.show();
                        $continueButton.prop('disabled', true).addClass('disabled');
                        $additionList.hide();
                    }


                    // $('[data-addition-broker-id=""]').find('.item_col_img div').html('');

                    var $noBrokersAdditions = $('[data-addition-broker-id=""]').not('[data-addition-id="cod"]').find('.item_col_img div');
                    var $codAddition = $('[data-addition-id="cod"]').find('.item_col_img div');

                    $('#operators-list [data-operator-id]').each(function (i, v) {

                        var bg = $(v).find('.operator-content-div').css('background-color');
                        var font = $(v).find('.operator-u-name').css('color');
                        var name = $(v).find('.operator-u-name').text();

                        var $div = $('<div>');
                        $div.html(name).css('background-color', bg).css('color', font).addClass('img-html');

                        $noBrokersAdditions.append($div);
                        if (parseInt($(v).attr('data-cod')) === 1) {

                            var $div2 = $div.clone();

                            $codAddition.append($div2);
                            $codAddition.show();
                        }

                        $('[data-addition-broker-id="' + $(v).attr('data-broker-id') + '"]').find('.item_col_img div').append($div);

                    });
                    // $('[data-addition-broker-id=""]').find('.item_col_img div').show();


                    $checkedOperator = $('input[name="CourierTypeU[courier_u_operator_id]"]:checked');

                    if ($checkedOperator.length) {
                        var broker_id = parseInt($checkedOperator.closest('label').attr('data-broker-id'));
                        var cod = parseInt($checkedOperator.closest('label').attr('data-cod'));
                        var collection = parseInt($checkedOperator.closest('label').attr('data-collection'));
                        var cod_price_n = ($checkedOperator.closest('label').attr('data-cod-n'));
                        var cod_price_b = ($checkedOperator.closest('label').attr('data-cod-b'));
                        dimWeightMode = $checkedOperator.closest('label').attr('data-dim-weight-mode');

                        dimWeightValidation();

                        additionsSelectAvailable(broker_id, cod, collection);
                        printCodPrice(cod_price_n, cod_price_b);
                    } else {
                        additionsSelectAvailable(false, false, false);
                        printCodPrice(false, false);
                    }


                    specialOperatorDescription(receiver_country_id, broker_id);

                    operatorsSorting();
                    $('[rel="tooltip"]').tooltip();

                    Overlay.hide();
                },
                error: function (e) {
                    console.log(e);
                }
            });

        } else {
            $operatorsContent.hide();
            $operatorsList.html('');
            $operatorsNone.hide();
            $operatorsEmpty.show();
            $notAvailable.show();
            $continueButton.attr('disabled', true).addClass('disabled');
            Overlay.hide();
        }
    }

    function specialOperatorDescription(receiver_country_id, broker_id)
    {

        if(receiver_country_id && broker_id) {
            $('[data-delivery-broker-id-show]').not('[data-delivery-broker-id-show="' + broker_id + '"]').not('[data-delivery-target-country-id-show="' + receiver_country_id + '"]').slideUp();
            $('[data-delivery-broker-id-show="' + broker_id + '"][data-delivery-target-country-id-show="' + receiver_country_id + '"]').slideDown();
        }
        else
            $('[data-delivery-broker-id-show]').hide();
    }

    function getCodCurrency()
    {
        if(!isCodAvailable)
            return false;

        var receiver_country_id = $('#CourierTypeU_AddressData_receiver_country_id').val();

        $.ajax({
            url: yii.urls.getCodCurrency,
            data: { country_id: receiver_country_id},
            method: 'POST',
            dataType: 'json',
            success: function(result){

                if(result) {
                    $('#cod-currency-name').html(result);
                    isCodAvailable = true;
                } else
                    isCodAvailable = false;

            },
            error: function(e){

                console.log(e);
            }
        });
    }




    function getRemoteArea(type)
    {
        var $target = $('[data-remote-area-info=' + type + ']');
        var $target = $('[data-remote-area-info=' + type + ']');

        $target.hide();


        if(type == 'sender' && (!collectionAvailable || !collectionSelected))
            return false;

        var country_id = $('input[name="CourierTypeU_AddressData[' + type + '][country_id]"]').val();
        var zip_code = $('input[name="CourierTypeU_AddressData[' + type + '][zip_code]"]').val();
        var city = $('input[name="CourierTypeU_AddressData[' + type + '][city]"]').val();


        if(country_id == '' || zip_code == '' || city == '' || selectedOperatorId == '')
            return false;

        $.ajax({
            url: yii.urls.checkRemoteArea,
            data: { country_id: country_id, zip_code: zip_code, city: city, operator: selectedOperatorId},
            method: 'POST',
            dataType: 'json',
            success: function(result){

                if(result)
                {

                    $target.find('span').html(result);
                    $target.show();
                }
            },
            error: function(e){

                console.log(e);
            }
        });
    }

    function checkAdditions()
    {

        var sender_country_id = $('[data-country="sender"]').val();
        var receiver_country_id = $('[data-country="receiver"]').val();
        var sender_zip_code = $('[data-zip-code="sender"]').val();
        var receiver_zip_code = $('[data-zip-code="receiver"]').val();
        var weight = $('[data-package-weight]').val();
        var collection = collectionAvailable && collectionSelected;

        $.ajax({
            url: yii.urls.checkAdditions,
            data: { sender_country_id: sender_country_id, receiver_country_id: receiver_country_id, collection: collection, weight: weight, sender_zip_code : sender_zip_code, receiver_zip_code : receiver_zip_code, selected_operator_id : selectedOperatorId },
            method: 'POST',
            dataType: 'json',
            success: function(result){

                var resultArray = Object.values(result);

                $('.addition_item').not('.not_available').not('[data-addition-id="cod"]').not('[data-addition-id="collection"]').each(function(i,v){

                    var id = $(v).attr('data-addition-id');

                    if(resultArray.indexOf(id) == -1)
                        $(v).addClass('not_available');
                });


                console.log();
            },
            error: function(e){
                //alert(yii.loader.error);
                // FormControl.submit();
            }
        });
    }

    function toggleRequirements(){

        var $requirements = $('#requirements');

        if($requirements.is(':visible'))
            $requirements.slideUp();
        else
            $requirements.slideDown();

    }


    function codChanged() {
        if ($(this).is(':checked')) {
            showCodDetails();
            isCodShown = true;
        }
        else {
            $('#cod-details').hide();
            isCodShown = false;
        }
    }


    function prepareAdditions()
    {
        checkboxAddtions();

        $("[data-group]").on("change", additionsGroup);

        // $(".addition_item").children().not(".with-checkbox").not(".not-available-addition-wrapper").css("cursor", "pointer");
        // $(".addition_item").children().not(".with-checkbox").on('click', additionsClickableAll);

        additionsSelectAvailable(false, false, false);
        printCodPrice(false, false);
    }

    function additionsClickableAll()
    {
        var $checkBox = $(this).parent().find("input[type=checkbox]");
        $checkBox.prop("checked", !$checkBox.prop("checked"));
    }

    function additionsGroup()
    {

        if($(this).attr("data-no-not-trigger"))
        {
            $(this).removeAttr("data-no-not-trigger");
        } else {
            var dataGroup = $(this).attr("data-group");
            if(dataGroup)
            {
                $(this).attr("data-no-not-trigger", true);
                $("[data-group=" + dataGroup + "]").not($(this)).prop("checked", false);

            }
        }
    }

    function checkboxAddtions()
    {

        $('[data-courier-addition][data-group]:checked').attr("data-no-not-trigger", true);

    }

    function disableButton($button)
    {
        $button.prop('disabled', true);
        $button.addClass('disabled');
    }

    function enableButton($button)
    {
        $button.prop('disabled', false);
        $button.removeClass('disabled');
    }

    function showAddressMore()
    {
        $(this).parent().parent().parent().find('[data-address-more]').slideDown();
        $(this).remove();
    }

    function updateView(force) {

        if (ignoreFirstUpdate) {
            ignoreFirstUpdate = false;
            return true;
        }

        if (!force || force === undefined || force.target) {

            if ($("input[data-dependency=10]").filter(function () {
                return $.trim($(this).val()).length == 0
            }).length > 0)
                return true; // not all required fileds are filled
        }

        $('[data-continue]').hide();

        disableButton($('[data-continue-button]'));

        checkAutomaticReturnAvailable();

        clearTimeout(viewTimeoutHandler);
        viewTimeoutHandler = setTimeout(_updateView, 500);
    }

    function _updateView()
    {
        Overlay.show();
        getOperators();
        setAutomaticReturn();
        dimWeightValidation()
    }

    function checkWeight()
    {
        var newVal;
        var $no = $('#Courier_CourierTypeU_packages_number');
        var $weightOne = $('#Courier_CourierTypeU_package_weight');
        var $weightTotal = $('#Courier_CourierTypeU__package_weight_total');

        var triggerId = $(this).attr('id');

        if(triggerId == undefined)
            triggerId = $weightOne.attr('id');

        var no = parseInt($no.val());
        var weightOne = parseFloat($weightOne.val());
        var weightTotal = parseFloat($weightTotal.val());

        if(triggerId == $no.attr('id'))
        {
            if(!isNaN(weightTotal)) {

                newVal = parseFloat(weightTotal / no).toFixed(2);

                if(newVal != $weightOne.val()) {
                    $weightOne.val(newVal);
                    $weightOne.trigger('change');
                }

            }
        }
        else if(triggerId == $weightOne.attr('id'))
        {
            if(!isNaN(weightOne))
                $weightTotal.val(parseFloat(weightOne * no).toFixed(2));
        }
        else if(triggerId == $weightTotal.attr('id'))
        {
            if(!isNaN(weightTotal)) {

                newVal = parseFloat(weightTotal / no).toFixed(2);

                if(newVal != $weightOne.val()) {
                    $weightOne.val(newVal);
                    $weightOne.trigger('change');
                }
            }
        }

    }

    function calculateDimensionaWeight(dimWeightMode)
    {
        var l = $("#Courier_CourierTypeU_package_size_l").val();
        var w = $("#Courier_CourierTypeU_package_size_w").val();
        var d = $("#Courier_CourierTypeU_package_size_d").val();

        var dw = 0;
        if(dimWeightMode == yii.dimWeightMode.dhl)
            dw = DimensionalWeight.calculateDhl(l,w,d);
        else
            dw = DimensionalWeight.calculate(l,w,d);

        $("#_dw_show").html(dw);
    }

    function updatePackagesNo()
    {
        $('[data-packages-no-show]').html($('[data-packages-no]').val());
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function checkAutomaticReturnAvailable()
    {

        var $returnBlock = $('[data-return]');
        var receiver_country_id = $('#CourierTypeU_AddressData_receiver_country_id').val();

        $.ajax({
            url: yii.urls.checkAutomaticReturn,
            data: { country_id: receiver_country_id},
            method: 'POST',
            dataType: 'json',
            success: function(result){


                if(result) {
                    $returnBlock.show();
                } else
                    $returnBlock.hide();

            },
            error: function(e){

                console.log(e);
            }
        });
    }

    function setAutomaticReturn()
    {

        var currentCountry = parseInt($('#CourierTypeU_AddressData_receiver_country_id').val());
        var defaultReturnCountries = yii.automaticReturnCountries;
        var $generateReturnCheckbox = $('[data-generate-return]');

        if(defaultReturnCountries.indexOf(currentCountry) >= 0) {
            // console.log('AR: t');

            if($generateReturnCheckbox.is(':checked'))
            {

            } else {
                // console.log('AR: tr');
                // $generateReturnCheckbox.prop('checked', true);
                $generateReturnCheckbox.trigger('click');
            }

        }
        else {
            // console.log('AR: n');
            if($generateReturnCheckbox.is(':checked'))
            {
                // console.log('AR: tr');
                $generateReturnCheckbox.trigger('click');
                // $generateReturnCheckbox.prop('checked', false);
            } else {
                // $generateReturnCheckbox.prop('checked', true);

            }
        }
    }



})(jQuery);