<?php

class CourierDomesticController extends Controller {

    const IMPORT_SPLIT_AFTER = 50;


    public function beforeAction($action)
    {
        if(Yii::app()->user->model->getCourierDomesticAvailable())
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier',

            ));
            exit;
        }
    }

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('create','created','index', 'list', 'ajaxGetAdditionList', 'import', 'listImported', 'quequeLabelByLocalId', 'generateLabels', 'ajaxRemoveRowOnImport', 'ajaxUpdateRowItemOnImport', 'ajaxEditRowOnImport', 'ajaxSaveRowOnImport', 'getLabels', 'getLabelsGroup'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect(['/courier/']);
    }

    /**
     * Fill form with cloned package data
     *
     * @param Courier $model
     */
    protected function _clone_domestic_fill()
    {

        if(isset(Yii::app()->session['cloneTypeDomestic'])) {

            $data = Yii::app()->session['cloneTypeDomestic'];
            unset(Yii::app()->session['cloneTypeDomestic']);

            $this->setPageState('step1', $data['model']);
            $this->setPageState('step1_domestic', $data['modelTypeDomestic']);
            $this->setPageState('step1_sender', $data['senderAddressData']);
            $this->setPageState('step1_receiver', $data['receiverAddressData']);
            $this->setPageState('step1_additions', $data['additions']);
        }
    }

    public function actionCreate()
    {
        $this->panelLeftMenuActive = 141;

        if(!Yii::app()->user->getModel()->isPremium)
            throw new CHttpException(403);

        if(Yii::app()->session['courierFormFinished']) // prevent going back and sending the same form again
        {
            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['courierFormFinished']);
            $this->refresh(true);
        }

        $this->_clone_domestic_fill();

        //for AJAX validation
//        {
//            $model=new Courier('insert');
//            $this->performAjaxValidation($model, 'courier-form');
//            unset($model);
//        }

        if(isset($_POST['summary']))
            $this->create_step_summary(); // Validate addition list, summary
        elseif (isset($_POST['finish']))
            $this->create_finish();
        else
            $this->create_step_1(); // this is the default, first time (step1)

    }

    protected function create_step_1()
    {
        $this->setPageState('form_started', true);
        $this->setPageState('start_user_id', Yii::app()->user->id);

        $model=new Courier_CourierTypeDomestic('step1');
        $model->attributes = $this->getPageState('step1',array());

        $model->courierTypeDomestic = new CourierTypeDomestic('step_1');
        $model->courierTypeDomestic->attributes = $this->getPageState('step1_domestic',array());
//        $model->senderAddressData = new CourierTypeDomestic_AddressData();
        $model->senderAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_SENDER);
//        $model->receiverAddressData = new CourierTypeDomestic_AddressData();
        $model->receiverAddressData = new CourierTypeDomestic_AddressData('insert', UserContactBook::TYPE_RECEIVER);

        $additions = $this->getPageState('step1_additions',array());

        $temp = $model->senderAddressData;
        $model->senderAddressData->loadUserData();
        if($model->senderAddressData->country_id != CountryList::COUNTRY_PL)
            $model->senderAddressData = $temp;

        $model->senderAddressData->attributes = $this->getPageState('step1_sender',array());
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiver',array());

        $model->courierTypeDomestic->courier = $model;

        $this->setPageState('last_step',1); // save information about last step


        if($model->courierTypeDomestic->domestic_operator_id) {

            $modelCourierAdditionList = CourierDomesticAdditionList::getAdditionsByCourierDomestic($model->courierTypeDomestic);

            $additionsHtml = $this->renderPartial('_additionList', array(
                'models' => $modelCourierAdditionList,
                'courierAddition' => $additions,
            ), true, true);
        } else
            $additionsHtml = '';

        $this->render('create/step1',array(
            'model'=>$model,
            'additionsHtml' => $additionsHtml,
            'additions' => $additions,
            'jumpToEnd' => $this->getPageState('jump_to_end',false),
        ));


    }

    protected function create_validate_step_1()
    {

        $error = false;

        $model = new Courier_CourierTypeDomestic();
        $model->attributes = $_POST['Courier_CourierTypeDomestic'];
        $model->courierTypeDomestic = new CourierTypeDomestic('step_1');
        $model->courierTypeDomestic->attributes = $_POST['CourierTypeDomestic'];
        $model->senderAddressData = new CourierTypeDomestic_AddressData();
        $model->receiverAddressData = new CourierTypeDomestic_AddressData();

        $model->senderAddressData->attributes = $_POST['CourierTypeDomestic_AddressData']['sender'];
        $model->receiverAddressData->attributes = $_POST['CourierTypeDomestic_AddressData']['receiver'];

        $model->senderAddressData->country_id = CountryList::COUNTRY_PL;
        $model->receiverAddressData->country_id = CountryList::COUNTRY_PL;

        $additions = $_POST['CourierAddition'];
        if(!is_array($additions))
            $additions = [];


        $model->validate();

        $model->courierTypeDomestic->validate();

        if($model->cod_value > 0)
            $model->senderAddressData->_bankAccountRequired = true;
        else
            $model->senderAddressData->_bankAccountRequired = false;

        $model->senderAddressData->validate();
        $model->receiverAddressData->validate();
        $model->courierTypeDomestic->courier = $model;

        $this->setPageState('step1',$model->getAttributes(array_keys($_POST['Courier_CourierTypeDomestic']))); // save previous form into form state
        $this->setPageState('step1_domestic',$model->courierTypeDomestic->getAttributes(array_keys($_POST['CourierTypeDomestic']))); // save previous form into form state
        $this->setPageState('step1_additions', $additions); // save previous form into form state
        $this->setPageState('step1_sender',$model->senderAddressData->getAttributes(array_merge(array('country_id'), array_keys($_POST['CourierTypeDomestic_AddressData']['sender'])))); // save previous form into form state
        $this->setPageState('step1_receiver',$model->receiverAddressData->getAttributes(array_merge(array('country_id'), array_keys($_POST['CourierTypeDomestic_AddressData']['receiver'])))); // save previous form into form state


        if($model->courierTypeDomestic->domestic_operator_id) {

            $modelCourierAdditionList = CourierDomesticAdditionList::getAdditionsByCourierDomestic($model->courierTypeDomestic);

            $additionsHtml = $this->renderPartial('_additionList', array(
                'models' => $modelCourierAdditionList,
                'courierAddition' => $additions,
            ), true, true);
        } else
            $additionsHtml = '';

        if($model->hasErrors() OR $model->courierTypeDomestic->hasErrors() OR $model->senderAddressData->hasErrors() OR $model->receiverAddressData->hasErrors() OR $error)
        {
            $this->render('create/step1',array(
                'model'=>$model,
                'additionsHtml' => $additionsHtml,
                'jumpToEnd' => $this->getPageState('jump_to_end',false),
            ));
            return 'break';
        }
    }

    protected function create_step_summary()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');

        switch($lastStep)
        {
            case 1 :
                if($this->create_validate_step_1() == 'break')
                    return;
                break;
        }

        $model = new Courier_CourierTypeDomestic;
        $model->attributes = $this->getPageState('step1',array());
        $model->courierTypeDomestic = new CourierTypeDomestic('step_1');
        $model->courierTypeDomestic->attributes = $this->getPageState('step1_domestic',array());
        $model->senderAddressData = new CourierTypeDomestic_AddressData();
        $model->receiverAddressData = new CourierTypeDomestic_AddressData();

        $additions = $this->getPageState('step1_additions',array());

        $model->courierTypeDomestic->addAdditions($additions);

        $model->senderAddressData->attributes = $this->getPageState('step1_sender',array());
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiver',array());

        $model->user_id = Yii::app()->user->id;

        $this->setPageState('last_step', 'summary'); // save information about last step

        $this->render('create/summary',array(
            'model'=>$model,
        ));
    }

    protected function create_validate_summary()
    {


        $error = false;

        $model = new Courier_CourierTypeDomestic;
        $model->attributes = $this->getPageState('step1',array());
        $model->courierTypeDomestic = new CourierTypeDomestic('summary');
        $model->courierTypeDomestic->attributes = $this->getPageState('step1_domestic',array());
        $model->senderAddressData = new CourierTypeDomestic_AddressData();
        $model->receiverAddressData = new CourierTypeDomestic_AddressData();

        $additions = $this->getPageState('step1_additions',array());

        $model->courierTypeDomestic->addAdditions($additions);


        $model->senderAddressData->attributes = $this->getPageState('step1_sender',array());
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiver',array());

        $model->user_id = Yii::app()->user->id;


        $model->courierTypeDomestic->regulations = $_POST['CourierTypeDomestic']['regulations'];
        $model->courierTypeDomestic->validate(array('regulations'));

        $model->courierTypeDomestic->courier = $model;

        if($model->hasErrors() OR $model->courierTypeDomestic->hasErrors() OR $error)
        {

            $this->render('create/summary',array(
                'model'=>$model,
            ));
            return 'break';
        }

    }

    protected function create_finish()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');


        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 'summary')
        {

            if($this->create_validate_summary() == 'break')
                return;

            $this->setPageState('last_step',3); // save information about last step

            $model = new Courier_CourierTypeDomestic;
            $model->attributes = $this->getPageState('step1',array());
            $model->courierTypeDomestic = new CourierTypeDomestic('step_1');
            $model->courierTypeDomestic->attributes = $this->getPageState('step1_domestic',array());
            $model->courierTypeDomestic->courier = $model;
            $model->senderAddressData = new CourierTypeDomestic_AddressData();
            $model->receiverAddressData = new CourierTypeDomestic_AddressData();

            $additions = $this->getPageState('step1_additions',array());


            $model->courierTypeDomestic->addAdditions($additions);

            $model->senderAddressData->attributes = $this->getPageState('step1_sender',array());
            $model->receiverAddressData->attributes = $this->getPageState('step1_receiver',array());

            $model->courierTypeDomestic->courier = $model;

            $model->user_id = Yii::app()->user->id;

            try
            {
                $return = $model->courierTypeDomestic->saveNewPackage();
            }
            catch(Exception $ex)
            {
                Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
                throw new CHttpException(403, 'Wystapił nieznany błąd!');
            }

            if($return)
            {
                // order labels for all of found
                /* @var $item CourierTypeDomestic */
                if(is_array($return))
                    foreach($return AS $item)
                    {
                        CourierLabelNew::asyncCallForId($item->courier_label_new_id);
                    }

                $temp = $return[0];

                if(S_Useful::sizeof($return) > 1) {
                    if($temp->family_member_number == 1)
                        $parent = $temp->courier->id;
                    else {
                        $parent = $temp->parent_courier_id;
                    }

                    Yii::app()->user->setFlash('previousModel', Yii::t('courier', 'Paczki zostały utworzone!{br}{br}{a}szczegóły paczek{/a}', array('{br}' => '<br/>', '{a}' => '<a href="' . Yii::app()->createUrl("/courier/courierDomestic/list", ["parent_group" => $parent]) . '" class="btn">', '{/a}' => '</a>')));
                }
                else
                {
                    // to get proper Hash
                    $temp = Courier::model()->findByPk($temp->courier->id);
                    Yii::app()->user->setFlash('previousModel', Yii::t('courier', 'Paczka została utworzona!{br}{br}{a}szczegóły paczki{/a}', array('{br}' => '<br/>', '{a}' => '<a href="' . Yii::app()->createUrl("/courier/courier/view", ["urlData" => $temp->hash]) . '" class="btn">', '{/a}' => '</a>')));
                }

                $this->redirect(array('/courier/courierDomestic/create'));

            } else
                throw new CHttpException(403, 'Wystapił nieznany błąd!');

        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }


    public function actionAjaxGetAdditionList()
    {
        $courierAddition = $_POST['CourierAddition'];



        if(!is_array($courierAddition))
            $courierAddition = [];

        $courier = new Courier_CourierTypeDomestic;
        $courier->courierTypeDomestic = new CourierTypeDomestic();
        $courier->senderAddressData = new CourierTypeDomestic_AddressData();
        $courier->receiverAddressData = new CourierTypeDomestic_AddressData();
        $courier->courierTypeDomestic->courier = $courier;

        $courier->attributes = $_POST['Courier_CourierTypeDomestic'];
        $courier->courierTypeDomestic->attributes = $_POST['CourierTypeDomestic'];
        $courier->senderAddressData->attributes = $_POST['CourierTypeDomestic_AddressData']['sender'];
        $courier->receiverAddressData->attributes = $_POST['CourierTypeDomestic_AddressData']['receiver'];

        $modelCourierAdditionList = CourierDomesticAdditionList::getAdditionsByCourierDomestic($courier->courierTypeDomestic);

        $html = $this->renderPartial('_additionList', array(
            'models' => $modelCourierAdditionList,
            'courierAddition' => $courierAddition,
        ), true, true);



        echo CJSON::encode($html);

    }




    public function actionImport($hash = NULL)
    {

        $this->panelLeftMenuActive = 142;

        $fileModel = new F_DomesticImport();


        if($hash !== NULL)
        {
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
            require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
            Yii::import('zii.behaviors.CTimestampBehavior');


            $models = Yii::app()->cacheUserData->get('domesticImport_' . $hash);

            if(isset($_POST['save']))
            {

                if(F_DomesticImport::validateModels($models))
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Popraw najpierw wszyskie błędy!'));
                    Yii::app()->cacheUserData->set('domesticImport_' . $hash, $models, 86400);
                    $this->refresh();
                    exit;
                }


                try
                {
                    $result = CourierTypeDomestic::saveWholeGroup($models);
                }
                catch (Exception $ex)
                {
                    Yii::log(print_r($ex,1),CLogger::LEVEL_ERROR);
                    return;
                }
                // ON SUCCESS ADD PACKAGES ID TO COURIER IMPORT MANAGER AND CALL LABEL ORDERING


                if($result['success'])
                {
                    $import = CourierImportManager::getByHash($hash);
                    /* @var $item CourierTypeDomestic */
                    $ids = array();
                    if(is_array($result['data']))
                        foreach($result['data'] AS $item)
                        {
                            array_push($ids, $item->courier_id);
                        }

                    $import->addCourierIds($ids);
                    $import->save();

                    Yii::app()->cacheUserData->delete('domesticImport_' . $hash);
                    $this->redirect(Yii::app()->createUrl('/courier/courierDomestic/listImported', ['hash' => $hash]));
                    exit;
                }

            } else {

                //validation is perfored before
//                $models = F_DomesticImport::validateModels($models);

                if (!$models OR !S_Useful::sizeof($models)) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Import nie powiódł się!'));
                    $this->redirect(Yii::app()->createUrl('courier/courierDomestic/import'));
                }

                $this->render('import/showModels', array(
                    'models' => $models,
                    'hash' => $hash,
                ));
                return;
            }
        }


        // file upload
        if(isset($_POST['F_DomesticImport']) ) {
            $fileModel->setAttributes($_POST['F_DomesticImport']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if (!$fileModel->validate()) {
                $this->render('import/index', array(
                    'model' => $fileModel,
                ));
                return;
            } else {
                // file upload success:

                // generate new hash
                $hash = hash('sha256', time() . Yii::app()->user->id);

                $inputFile = $fileModel->file->tempName;

//                Yii::import('application.extensions.phpexcel.XPHPExcel');
//                XPHPExcel::init();

                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFile);
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($inputFile);

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $data = [];
                $start = 1;

                if ($fileModel->omitFirst) {
                    $headerSeen = false;
                } else
                    $headerSeen = true;

                for ($row = $start; $row <= $highestRow; $row++) {
                    if (!$headerSeen) {
                        $headerSeen = true;
                        continue;
                    }

                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                    array_push($data, $rowData[0]);
                }



                // remove empty lines:
                $data = (array_filter(array_map('array_filter', $data)));

                $numberOfLines = S_Useful::sizeof($data);

                // check just by lines number
                if($numberOfLines > F_DomesticImport::MAX_LINES)
                {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość wierszy w pliku to: {no}! Naliczono: {number}', array('{no}' => F_DomesticImport::MAX_LINES, '{number}' => $numberOfLines)));
                    $this->refresh(true);
                    exit;
                }

                $numberOfPackages = F_DomesticImport::calculateTotalNumberOfPackages($data);
                // check by number of packages
                if($numberOfPackages > F_DomesticImport::MAX_PACKAGES) {
                    Yii::app()->user->setFlash('error', Yii::t('courier', 'Maksymalna ilość paczek w pliku to: {no}! Naliczono: {number}', array('{no}' => F_DomesticImport::MAX_PACKAGES, '{number}' => $numberOfPackages)));
                    $this->refresh(true);
                    exit;
                }

                $models = F_DomesticImport::mapAttributesToModels($data, $fileModel->domestic_operator_id);
                F_DomesticImport::validateModels($models);

                Yii::app()->cacheUserData->set('domesticImport_' . $hash, $models, 86400);


                $this->redirect(Yii::app()->createUrl('courier/courierDomestic/import', ['hash' => $hash]));
                exit;
            }
        }

        $this->render('import/index', array(
            'model' => $fileModel,
        ));

    }


    public function actionList($parent_group = NULL)
    {
        $this->panelLeftMenuActive = 140;

        if($parent_group !== NULL) {
            $parent = Courier::model()->findByAttributes(['id' => $parent_group, 'user_id' => Yii::app()->user->id]);
            if($parent->courierTypeDomestic->family_member_number > 1)
                throw new CHttpException(404);
        }

        $model = new _CourierDomesticGridViewUser('search');

        $model->unsetAttributes();

        if (isset($_GET['CourierDomesticGridViewUser']))
            $model->setAttributes($_GET['CourierDomesticGridViewUser']);

        $this->render('list', array(
            'model' => $model,
            'parent_group' => $parent_group,
        ));
    }


    public function actionListImported($hash)
    {
        $this->panelLeftMenuActive = 140;


        $import = CourierImportManager::getByHash($hash);

        if($import->user_id != Yii::app()->user->id)
            throw new CHttpException(404);

        if(!$import)
            throw new CHttpException(404);

//        $couriers = [];
//        if(is_array($import->courier_ids))
//            foreach($import->courier_ids AS $courier_id)
//            {
//                array_push($couriers, Courier::model()->findByPk($courier_id));
//            }

        $couriers = Courier::model()->with('courierTypeDomestic')->with('courierTypeDomestic.courierLabelNew')->findAllByPk($import->courier_ids);



        $this->render('listImported',array(
            'import' => $import,
            'couriers' => $couriers,
            'hash' => $hash,
        ));

    }

    public function actionGetLabels($hash)
    {
        if(isset($_POST['label']))
        {
            $couriers = Courier::model()->findAllByAttributes(['id' => array_keys($_POST['label']), 'user_id' => Yii::app()->user->id]);

            if(S_Useful::sizeof($couriers)) {
                LabelPrinter::generateLabels($couriers, $_POST['Courier']['type'],true);
                exit;
            }
        }

        Yii::app()->user->setFlash('error', 'Nie znaleziono żadnych etykiet do wygenerowania!');
        $this->redirect(Yii::app()->createUrl('/courier/courierDomestic/listImported', ['hash' => $hash]));
        exit;
    }

    public function actionGetLabelsGroup($parent_group)
    {
        $type = $_POST['type'];

        /* @var $parent Courier */
        $parent = Courier::model()->findByAttributes(['id' => $parent_group, 'user_id' => Yii::app()->user->id]);
        if ($parent->courierTypeDomestic->family_member_number > 1)
            throw new CHttpException(404);

        $couriers = [];
        array_push($couriers, $parent);
        foreach ($parent->courierTypeDomestic->getChildrenCourierDomestic() AS $child) {
            array_push($couriers, $child->courier);
        }


        if (S_Useful::sizeof($couriers)) {
            LabelPrinter::generateLabels($couriers, $type);
            exit;
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionQuequeLabelByLocalId()
    {

        $local_id = $_POST['localId'];

        Yii::app()->getModule('courier');

        $courier = Courier::model()->findByAttributes(array('local_id' => $local_id));

        $parent = CourierLabelNew::model()->findByAttributes(array('courier_id' => $courier->id));

        if($parent instanceof CourierLabelNew)
        {

            if($parent->stat == CourierLabelNew::STAT_NEW) {
                $ids = $parent->runLabelOrdering(true);
            }
            echo CJSON::encode(array('request' => true, 'stat' => $parent->stat, 'ids' => $ids));
            exit;
        } else {
            echo CJSON::encode(array('request' => false));
            exit;
        }
    }

    public function actionAjaxRemoveRowOnImport()
    {
        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        $models = Yii::app()->cacheUserData->get('domesticImport_' . $hash);
        unset($models[$id]);

        Yii::app()->cacheUserData->set('domesticImport_' . $hash, $models, 86400);

        echo CJSON::encode(array('success' => true));
    }

    public function actionAjaxUpdateRowItemOnImport()
    {
        if(Yii::app()->request->isAjaxRequest) {

                      $hash = $_POST['hash'];
            $i = $_POST['i'];
            $attribute = $_POST['attribute'];
            $val = $_POST['val'];


            $possibleToEdit = [
                'receiverAddressData.name',
                'receiverAddressData.company',
                'receiverAddressData.zip_code',
                'receiverAddressData.address_line_1',
                'receiverAddressData.address_line_2',
                'receiverAddressData.city',
                'receiverAddressData.tel',
                'receiverAddressData.email',
                'senderAddressData.name',
                'senderAddressData.company',
                'senderAddressData.zip_code',
                'senderAddressData.address_line_1',
                'senderAddressData.address_line_2',
                'senderAddressData.city',
                'senderAddressData.tel',
                'senderAddressData.email',
                'package_weight',
                'package_size_l',
                'package_size_d',
                'package_size_w',
            ];
            if(!in_array($attribute, $possibleToEdit))
                return false;

            try {
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/courierRefValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/zipCodeValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/lettersNumbersBasicValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/telValidator.php');
                require_once(YiiBase::getPathOfAlias("webroot") . '/protected/validators/eitherOneValidator.php');
                Yii::import('zii.behaviors.CTimestampBehavior');

                $models = Yii::app()->cacheUserData->get('domesticImport_'.$hash);

                /* @var $model Courier */
                if (is_array($models)) {
                    $model = $models[$i];

                    $submodel = false;
                    if ($model) {

                        $attribute = explode('.', $attribute);
                        if(S_Useful::sizeof($attribute) > 1)
                        {
                            $submodel = $attribute[0];

//                            $model = $model->$attribute[0];
                            // PHP7 fix
                            $temp = $attribute[0];
                            $model = $model->$temp;

                            $attribute = $attribute[1];
                        } else
                            $attribute = $attribute[0];


                        if ($model->hasAttribute($attribute)) {
                            $backupValue = $model->$attribute;
                            $model->$attribute = $val;

                            $model->validate([$attribute]);
                            if (!$model->hasErrors($attribute)) {

                                if($submodel)
                                    $models[$i]->$submodel = $model;
                                else
                                    $models[$i] = $model;

                                Yii::app()->cacheUserData->set('domesticImport_' . $hash, $models, 86400);

                                echo CJSON::encode(['success' => true]);
                                return;

                            } else {

                                echo CJSON::encode([
                                    'success' => false,
                                    'msg' => strip_tags($model->getError($attribute)),
                                    'backup' => $backupValue,
                                ]);
                                return;
                            }
                        }
                    }
                }

                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie. {msg}', ['{msg}' => print_r($attribute,1)])
                ]);
                return;

            } catch (Exception $ex) {
                Yii::log(print_r($ex, 1), CLogger::LEVEL_ERROR);
                echo CJSON::encode([
                    'success' => false,
                    'msg' => Yii::t('courier_ebay', 'Zapis nie powiódł się! Spróbuj ponownie')
                ]);
                return;

            }
        }
    }

    public function actionAjaxEditRowOnImport()
    {

        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');

        $models = Yii::app()->cacheUserData->get('domesticImport_' . $hash);
        $model = $models[$id];

        if(isset($model)) {

            $html = $this->renderPartial('import/_edit_row', ['model' => $model, 'id' => $id, 'hash' => $hash], true, true);

            echo CJSON::encode(array('success' => true, 'html' => $html));
        } else {
            echo CJSON::encode(array('success' => false));
        }

    }

    public function actionAjaxSaveRowOnImport()
    {

        $hash = $_POST['hash'];
        $id = $_POST['id'];

        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/courierRefValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/zipCodeValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/lettersNumbersBasicValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/telValidator.php');
        require_once(YiiBase::getPathOfAlias("webroot").'/protected/validators/eitherOneValidator.php');
        Yii::import('zii.behaviors.CTimestampBehavior');


        $models = Yii::app()->cacheUserData->get('domesticImport_' . $hash);
        $model = $models[$id];

        if(isset($model)) {

            /* @var $model Courier_CourierTypeDomestic */
            $model->courierTypeDomestic->attributes = $_POST['CourierTypeDomestic'];
            $model->senderAddressData->attributes = $_POST['CourierTypeDomestic_AddressData']['sender'];
            $model->receiverAddressData->attributes = $_POST['CourierTypeDomestic_AddressData']['receiver'];
            $model->attributes = $_POST['Courier_CourierTypeDomestic'];

            $model->validate();
            $model->courierTypeDomestic->validate();
            $model->senderAddressData->validate();
            $model->receiverAddressData->validate();

            $models[$id] = $model;

            Yii::app()->cacheUserData->set('domesticImport_' . $hash, $models, 86400);

            echo CJSON::encode(array('success' => true));
        } else {
            echo CJSON::encode(array('success' => false));
        }

    }

}