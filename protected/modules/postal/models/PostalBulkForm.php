<?php

class PostalBulkForm extends CFormModel
{
	public $regulations;
	public $regulations_rodo;

	public function rules()
	{
		return array_merge(RodoRegulations::regulationsRules(''), array(
//			array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('postal', 'Musisz zaakceptować regulamin.')),
//			array('regulations', 'numerical', 'integerOnly' => true),
		));


	}

	public function behaviors(){

		return CMap::mergeArray(parent::behaviors(),
			[
                'bAcceptRegulationsByDefault' =>
                    [
                        'class'=>'application.models.bAcceptRegulationsByDefault'
                    ],
			]
		);
	}


	public function attributeLabels()
	{
        return array_merge(RodoRegulations::attributeLabels(),
            array());

	}

}