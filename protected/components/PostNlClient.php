<?php

class PostNlClient extends GlobalOperator
{

    const URL = GLOBAL_CONFIG::POSTNL_URL;
    const APIKEY = GLOBAL_CONFIG::POSTNL_KEY;

    const CUSTOMER_CODE = 'DMWG';
    const COLLECT_LOCATION = '100548';
    const CUSTOMER_ID = '10582989';

    public $label_annotation_text = false;

    public $package_value;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_tel;
    public $sender_mail;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_mail;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $packages_number = 1;

    public $package_content;
    public $ref;


    public static function test()
    {
        $model = new self;
        var_dump($model->createShipment());
    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    protected static function _ownExtractNumber($text, $defaultValue = '0')
    {
        $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-]+)((\s)+|$))*/i';
        $building = '';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[0][0];

        $building = trim($building);

        if(mb_strlen($building) > 8)
        {
            // max length is 8
            // if it's longer, use older, more choosy rexexp
            $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';
            if(preg_match_all($REG_PATTERN, $text, $result2))
                $building = $result2[2][0];

            $building = trim($building);
        }

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }


    public function createShipment($returnError = false)
    {

        $data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
xmlns:bar="http://postnl.nl/cif/services/BarcodeWebService/" 
xmlns:tpp="http://postnl.nl/cif/domain/BarcodeWebService/">
   <soapenv:Body>
      <bar:GenerateBarcode>
         <tpp:Message>
            <tpp:MessageID>1</tpp:MessageID>           
         </tpp:Message>
         <tpp:Customer>
            <tpp:CustomerCode>'.self::CUSTOMER_CODE.'</tpp:CustomerCode>
            <tpp:CustomerNumber>'.self::CUSTOMER_ID.'</tpp:CustomerNumber>
         </tpp:Customer>
         <tpp:Barcode>
            <tpp:Type>3S</tpp:Type>
            <tpp:Range>'.self::CUSTOMER_CODE.'</tpp:Range>
            <tpp:Serie>1000000-2000000</tpp:Serie>
         </tpp:Barcode>
      </bar:GenerateBarcode>
   </soapenv:Body>
</soapenv:Envelope>';

        $urlSuffix = 'v1_1/barcode';

        $resp = $this->_soapOperation($urlSuffix, $data);

        if(!$resp->success)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->errorDesc);
            } else {
                return false;
            }
        }

        $barcode = (string) $resp->data->Body->GenerateBarcodeResponse->Barcode;

        ////////////////////////////////////////////////////

        $senderAddressLine = trim($this->sender_address1.' '.$this->sender_address2);
        $senderStreetNo = trim(self::_ownExtractNumber($senderAddressLine));
        $senderAddressName = trim(str_replace($senderStreetNo, '', $senderAddressLine));

        $receiverAddressLine = trim($this->receiver_address1.' '.$this->receiver_address2);
        $receiverStreetNo = trim(self::_ownExtractNumber($receiverAddressLine));
        $receiverAddressName = trim(str_replace($receiverStreetNo, '', $receiverAddressLine));

        $data = '<?xml version="1.0" encoding="utf-8"?>
                    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://postnl.nl/cif/services/LabellingWebService/">
                    <SOAP-ENV:Body>
                    <ns1:GenerateLabel>
                     <ns1:Customer>
                           <ns1:Address>
                                  <ns1:AddressType>02</ns1:AddressType>
                                  <ns1:City>'.self::_cdata($this->sender_city).'</ns1:City>
                                  <ns1:CompanyName>'.self::_cdata($this->sender_company).'</ns1:CompanyName>
                                  <ns1:Countrycode>'.self::_cdata($this->sender_country).'</ns1:Countrycode>                    
                                  <ns1:HouseNr>'.self::_cdata($senderStreetNo).'</ns1:HouseNr>
                                  <ns1:Name>'.self::_cdata($this->sender_name).'</ns1:Name>
                                  <ns1:Street>'.self::_cdata($senderAddressName).'</ns1:Street>
                                  <ns1:Zipcode>'.self::_cdata($this->sender_zip_code).'</ns1:Zipcode>
                           </ns1:Address>
                           <ns1:CollectionLocation>'.self::COLLECT_LOCATION.'</ns1:CollectionLocation>
                           <ns1:CustomerCode>'.self::CUSTOMER_CODE.'</ns1:CustomerCode>
                           <ns1:CustomerNumber>'.self::CUSTOMER_ID.'</ns1:CustomerNumber>
                    </ns1:Customer>
                    <ns1:Message>
                           <ns1:MessageID>1</ns1:MessageID>
                           <ns1:MessageTimeStamp>'.date('d-m-Y H:i:s').'</ns1:MessageTimeStamp>
                           <ns1:Printertype>GraphicFile|JPG 200 dpi</ns1:Printertype>
                    </ns1:Message>
                    <ns1:Shipments>
                           <ns1:Shipment>
                                  <ns1:Addresses>
                                        <ns1:Address>
                                               <ns1:AddressType>01</ns1:AddressType>
                                                <ns1:City>'.self::_cdata($this->receiver_city).'</ns1:City>
                                                <ns1:CompanyName>'.self::_cdata($this->receiver_company).'</ns1:CompanyName>
                                                <ns1:Countrycode>'.self::_cdata($this->receiver_country).'</ns1:Countrycode>                    
                                                <ns1:HouseNr>'.self::_cdata($receiverStreetNo).'</ns1:HouseNr>
                                                <ns1:Name>'.self::_cdata($this->receiver_name).'</ns1:Name>
                                                <ns1:Street>'.self::_cdata($receiverAddressName).'</ns1:Street>
                                                <ns1:Zipcode>'.self::_cdata($this->receiver_zip_code).'</ns1:Zipcode>
                                        </ns1:Address>
                                  </ns1:Addresses>
                                  <ns1:Barcode>'.$barcode.'</ns1:Barcode>
                                  <ns1:Contacts>
                                        <ns1:Contact>
                                               <ns1:ContactType>01</ns1:ContactType>
                                               <ns1:Email>'.$this->receiver_mail.'</ns1:Email>
                                               <ns1:SMSNr>'.$this->receiver_tel.'</ns1:SMSNr>
                                        </ns1:Contact>
                                  </ns1:Contacts>
                                  <ns1:Dimension>
                                        <ns1:Height>'.($this->package_size_d * 10).'</ns1:Height>
                                        <ns1:Length>'.($this->package_size_l * 10).'</ns1:Length>
                                        <ns1:Width>'.($this->package_size_w * 10).'</ns1:Width>
                                        <ns1:Weight>'.($this->package_weight * 1000).'</ns1:Weight>
                                  </ns1:Dimension>
                                  <ns1:ProductCodeDelivery>3085</ns1:ProductCodeDelivery>
                                  <ns1:Content>'.self::_cdata($this->package_content).'</ns1:Content>
                                  <ns1:Reference>'.self::_cdata($this->ref).'</ns1:Reference>
                           </ns1:Shipment>
                    </ns1:Shipments>
                    </ns1:GenerateLabel>
                    </SOAP-ENV:Body>
                    </SOAP-ENV:Envelope>';

        $urlSuffix = 'v2_1/label';


        $resp = $this->_soapOperation($urlSuffix, $data);

        if(!$resp->success)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->errorDesc);
            } else {
                return false;
            }
        }


        $return = [];
        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        $ttNumber = (string) $resp->data->Body->GenerateLabelResponse->ResponseShipments->ResponseShipment->Barcode;

        $im->readImageBlob(base64_decode($resp->data->Body->GenerateLabelResponse->ResponseShipments->ResponseShipment->Labels->Label->Content));
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        if($this->label_annotation_text)
            LabelAnnotation::createAnnotation($this->label_annotation_text, $im, 1015,730);

        $im->trimImage(0);
        $im->rotateImage(new ImagickPixel(), 90);
        $im->stripImage();


        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);

        return $return;

    }

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;
        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id, GLOBAL_BROKERS::BROKER_DHLNL);

        $senderName = $from->name;
        $senderCompany = $from->company;

        $model->sender_name = $senderName;
        $model->sender_company = $senderCompany;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierInternal->courier->getWeight(true);

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->ref = $courierInternal->courier->local_id;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;
        $model->label_annotation_text = $courierU->courierUOperator->label_symbol;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;


        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierU->courier->package_weight;
        $model->packages_number = $courierU->courier->packages_number;

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->ref = $courierU->courier->local_id;

        return $model->createShipment($returnErrors);
    }


    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        $model = new self;

        $data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ship="http://postnl.nl/cif/services/ShippingStatusWebService/" xmlns:tpp="http://postnl.nl/cif/domain/ShippingStatusWebService/">
   <soapenv:Body>
     <ship:CompleteStatus>
       <tpp:Message>
         <tpp:MessageID>1</tpp:MessageID>     
       </tpp:Message>
       <tpp:Customer>
 <tpp:CustomerCode>'.self::CUSTOMER_CODE.'</tpp:CustomerCode>
            <tpp:CustomerNumber>'.self::CUSTOMER_ID.'</tpp:CustomerNumber>
       </tpp:Customer>
       <tpp:Shipment>
         <tpp:Barcode>'.$no.'</tpp:Barcode>
         <tpp:Reference/>
       </tpp:Shipment>
     </ship:CompleteStatus>
   </soapenv:Body>
</soapenv:Envelope>';

        $urlSuffix = 'v1_6/status';

        $resp = $model->_soapOperation($urlSuffix, $data);

        $data = $resp->data->Body->CompleteStatusResponse->Shipments->CompleteStatusResponseShipment->Events;

        $statuses = new GlobalOperatorTtResponseCollection();
        if($data)
            foreach($data->CompleteStatusResponseEvent AS $item)
            {
                $code = (string) $item->Code;

                if($code == '02M') // ignore this status
                    continue;

                $name = (string) $item->Description;
                $date = (string) $item->TimeStamp;

                $date = substr($date, 6,4).'-'.substr($date, 3,2).'-'.substr($date, 0,2).' '.substr($date, 11,8);

                $statuses->addItem($name, $date, NULL);
            }


        return $statuses;
    }

    protected function _soapOperation($urlSuffix, $data)
    {
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: ".self::URL.$urlSuffix,
            "Content-length: ".strlen($data),
            "apikey: ".self::APIKEY,
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, self::URL.$urlSuffix);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        MyDump::dump('post_nl.txt', print_r($urlSuffix.' : '.$data, 1));
        MyDump::dump('post_nl.txt', print_r($response, 1));

        $response = str_replace("<soap:Body>","",$response);
        $response = str_replace("</soap:Body>","",$response);
        $response = str_replace('soap:', '', $response);
        $response = str_replace('a:', '', $response);
        $response = str_replace('s:', '', $response);


        // convertingc to XML
        $response = @simplexml_load_string($response);

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        if(curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200)
        {
            $error = curl_error($ch);

            if($error != '')
                $error .= ' : ';

            $error .= $response->Body->Fault->faultcode .' : '.$response->Body->Fault->detail->CifException->Errors->ExceptionData->ErrorMsg;

            $return->error = $error != '' ? $error : 'Unknown error!';
            $return->errorCode = '';
            $return->errorDesc = $error != '' ? $error : 'Unknown error!';
        }
        else
        {
            $return->success = true;
            $return->data = $response;
        }

        curl_close($ch);

        return $return;

    }

}