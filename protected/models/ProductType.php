<?php

Yii::import('application.models._base.BaseProductType');

class ProductType extends BaseProductType
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}