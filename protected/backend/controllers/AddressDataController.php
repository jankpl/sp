<?php

class AddressDataController extends Controller
{

    public function accessRules()
    {
        return array(

            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionAjaxGetAddressData()
    {

        $id = $_POST['id'];

        /* @var $model User */
        $model = AddressData::model()->findByPk($id);

        echo CJSON::encode($model->attributes);
    }

    public function actionEditBankAccountForCourier($id, $courier_id)
    {
        Yii::app()->getModule('courier');


        /* @var $model AddressData */
        /* @var $courier Courier */
        $model = AddressData::model()->findByPk($id);
        $courier = Courier::model()->findByPk($courier_id);

        if($model === NULL or $courier === NULL)
            throw new CHttpException(404);


        $previousBankAccountNumber = $model->getBankAccount();
        if (isset($_POST['AddressData'])) {
            $model->setBankAccount($_POST['AddressData']['bankAccount']);

            $newBankAccountNumber = $model->getBankAccount();
            if ($model->update()) {

                $courier->addToLog('Changed bank account number from: "'.$previousBankAccountNumber.'" to: "'.$newBankAccountNumber.'"', CourierLog::CAT_INFO, Yii::app()->user->name);

                Yii::app()->user->setFlash('success', 'Bank account number has been updated!');
                $this->redirect(array('/courier/courier/view', 'id' => $courier->id));
            }
        }

        $this->render('editBankAccountForCourier', array(
            'model' => $model
        ));

    }

}