<?php

class _CourierCountryGroup_CourierPriceUniversal extends CourierCountryGroup
{
    public function getHasPrice($type)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from('courier_price_universal');
        $cmd->where('type = :type AND courier_country_group_id = :courier_country_group_id AND user_group_id IS NULL', [':type' => $type, ':courier_country_group_id' => $this->id]);

        return $cmd->queryScalar();
    }

    public function getNoOfGroupPrices($type)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from('courier_price_universal');
        $cmd->where('type = :type AND courier_country_group_id = :courier_country_group_id AND user_group_id IS NOT NULL', [':type' => $type, ':courier_country_group_id' => $this->id]);

        return $cmd->queryScalar();
    }
}