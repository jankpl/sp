<?php

class DictionaryController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Dictionary'),
        ));
    }

    public function actionUpdate($id) {


        $model = $this->loadModel($id, 'Dictionary');

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = DictionaryTr::model()->find('dictionary_id=:dictionary_id AND language_id=:language_id', array(':dictionary_id' => $model->id, ':language_id' => $item->id));

            if($modelTrs[$item->id] == NULL)
            {
                $statTr = new DictionaryTr('create');
                $statTr->language_id = $item->id;
                $modelTrs[$item->id] = $statTr;
            }
        }

        if (isset($_POST['Dictionary'])) {
            $model->setAttributes($_POST['Dictionary']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            $model->validate();

            if($model->hasErrors())
                $errors = true;

            $model->save();

            foreach(Language::model()->findAll('stat = 1') AS $item)
            {
                if($modelTrs[$item->id] === null)
                    $modelTrs[$item->id] = new DictionaryTr;
                $modelTrs[$item->id]->setAttributes($_POST['DictionaryTr'][$item->id]);
                $modelTrs[$item->id]->dictionary_id = $model->id;
                $modelTrs[$item->id]->language_id = $item->id;

                if($modelTrs[$item->id]->target_text != '')
                    if(!$modelTrs[$item->id]->save())
                        $errors = true;
            }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
            else
            {
                $transaction->rollback();
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }


    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {

        $model = new Dictionary('search');
        $model->unsetAttributes();

        if (isset($_GET['Dictionary']))
            $model->setAttributes($_GET['Dictionary']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}