<?php

class CreateForm extends CFormModel
{

    public $no;
    public $date;
    public $company;
    public $form;
    public $nip;
    public $address_line_1;
    public $address2;
    public $sad;
    public $krs;
    public $regon;
    public $kapital;
    public $person1;
    public $person2;
    public $email;
    public $person_contact;
    public $person_contact_email;
    public $person_rozliczenia;
    public $person_rozliczenia_email;
    public $person_reklamacje;
    public $person_reklamacje_email;
    public $salesman;
    public $salesman_tel;
    public $account_no;

    public $receiver_name;
    public $receiver_surname;
    public $receiver_email;
    public $receiver_tel;
    public $receiver_country;


    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('receiver_name, receiver_surname, receiver_email, receiver_country, receiver_tel', 'required', 'except' => 'preview'),
            array('receiver_country', 'length', 'min' => 2, 'max' => 2),
            array('receiver_email, email, person_reklamacje_email, person_contact_email, person_rozliczenia_email', 'email'),
            array('no, date, company, nip, address_line_1, address2, person1, email, salesman, salesman_tel, account_no', 'required',),
            array('no, date, company, form, nip, address_line_1, address2, sad, krs, regon, kapital, person1, person2, email, person_contact, person_contact_email, person_rozliczenia, person_rozliczenia_email, person_reklamacje, person_reklamacje_email, salesman, salesman_tel, account_no', 'safe',),
        );
    }

    public function attributeLabels()
    {
        return array(
            'no' => 'NR_UMOWY',
            'date' => 'DATA_UMOWY',
            'company' => 'NAZWA_FIRMY',
            'form' => 'FORMA_PRAWNA',
            'nip' => 'NIP',
            'address_line_1' => 'ADRES_1',
            'address2' => 'ADRES_2',
            'sad' => 'SAD',
            'krs' => 'KRS',
            'regon' => 'REGON',
            'kapital' => 'KAPITAL',
            'person1' => 'OSOBA_1',
            'person2' => 'OSOBA_2',
            'email' => 'EMAIL',
            'person_contact' => 'OSOBA_KONAKT',
            'person_contact_email' => 'OSOBA_KONTAKT_EMAIL',
            'person_rozliczenia' => 'OSOBA_ROZLICZENIA',
            'person_rozliczenia_email' => 'OSOBA_ROZLICZENIA_EMAIL',
            'person_reklamacje' => 'OSOBA_REKLAMACJE',
            'person_reklamacje_email' => 'OSOBA_REKLAMACJE_EMAIL',
            'salesman' => 'OPIEKUN',
            'salesman_tel' => 'OPIEKUN_TEL',
            'account_no' => 'NR_KONTA'
        );
    }

    public function initByUser(User $model)
    {
        $this->company = $model->company;
        $this->nip = $model->nip;
        $this->address_line_1 = $model->address_line_1;
        $this->address2 = ($model->address_line_2 ? $model->address_line_2.', ' : '').$model->zip_code.' '.$model->city.', '.$model->country0->code;
        $this->email = $model->email;
        $this->salesman = $model->salesman ? $model->salesman->name : NULL;
        $this->salesman_tel = $model->salesman ? $model->salesman->tel : NULL;
        $this->person_rozliczenia_email = $model->getAccountingEmail();
        $this->account_no = $model->getBankAccount();
    }
}