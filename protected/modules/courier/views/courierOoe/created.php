<?php
/* @var $model Courier */

?>

<div class="form">

    <h2><?php echo Yii::t('app','Krok {step}', array('{step}' => '5/5')); ?></h2>

    <?php if(Yii::app()->user->hasFlash('package-created-success'))
    {
        echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, Yii::app()->user->getFlash('package-created-success'), array('closeText' => false));
    }
    else if(Yii::app()->user->hasFlash('package-created-error'))
    {
        echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, Yii::app()->user->getFlash('package-created-error'), array('closeText' => false));
    }
    else
        $this->redirect(array('/courier/courier/view', 'urlData' => $model->hash));
    ?>

    <div style="text-align: center;">
        <?php echo CHtml::link(Yii::t('courier','Szczegóły paczki'),Yii::app()->createUrl('/courier/courier/view', array('urlData' => $model->hash)),array('class' => 'btn'));?>
    </div>
</div>