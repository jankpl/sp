<?php

class F_B2C2CorreosForm extends CFormModel
{

    public $file;


    public function rules() {
        return array(
            array('file', 'required'),
            array('file', 'file',  'allowEmpty' => false, 'maxSize' => 512400, 'types' => 'csv, xls, xlsx'),
        );
    }

    public function fileToArray($fileName)
    {
//        Yii::import('application.extensions.phpexcel.XPHPExcel');
//        XPHPExcel::init();

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($fileName);


        // WITH FIX:
        if(strtoupper(($inputFileType)) == 'CSV')
        {
            // SET UTF8 ENCODING IN CSV
            $file_content = file_get_contents( $fileName );

            $file_content = S_Useful::forceToUTF8($file_content);
            file_put_contents( $fileName, $file_content );

            $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objReader->setDelimiter(',');
            // FIX ENCODING

        } else {
            $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
        }


        $objPHPExcel = $objReader->load($fileName);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $data = [];
        $start = 1;


        for ($row = $start; $row <= $highestRow; $row++) {

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);

            $rowDataArray = $rowData[0];

            array_push($data, $rowDataArray);
        }

        // remove empty lines:
        $data = (array_filter(array_map('array_filter', $data)));

        return $data;
    }

    /**
     * @param $no
     * @return int CourierLabelNew id
     */
    public static function isOryginalNoValid($no)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier_label_new.id')
            ->from('courier_label_new')
            ->join('courier', 'courier.id = courier_label_new.courier_id')
            ->where('track_id = :track_id AND courier.user_id = :user_id', [
                ':track_id' => $no,
                ':user_id' => 947 // eobuwie
                ]);

        return $cmd->queryScalar();
    }


}