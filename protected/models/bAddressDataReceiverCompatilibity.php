<?php


class bAddressDataReceiverCompatilibity extends bBaseBehavior
{

    protected $_allowedClass = array('Courier', 'InternationalMail');

    public function getReceiverCountry()
    {
        return $this->owner()->receiverAddressData->country0;
    }

    public function getReceiver_country_id()
    {
        return $this->owner()->receiverAddressData->country_id;
    }

    public function getReceiver_name()
    {
        return $this->owner()->receiverAddressData->name;
    }

    public function getReceiver_company()
    {
        return $this->owner()->receiverAddressData->company;
    }

    public function getReceiver_country()
    {
        return $this->owner()->receiverAddressData->country;
    }

    public function getReceiver_zip_code()
    {
        return $this->owner()->receiverAddressData->zip_code;
    }

    public function getReceiver_city()
    {
        return $this->owner()->receiverAddressData->city;
    }

    public function getReceiver_address_line_1()
    {
        return $this->owner()->receiverAddressData->address_line_1;
    }

    public function getReceiver_address_line_2()
    {
        return $this->owner()->receiverAddressData->address_line_2;
    }

    public function getReceiver_tel()
    {
        return $this->owner()->receiverAddressData->tel;
    }

    public function setReceiverCountry($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->country0 = $val;
    }

    public function setReceiver_country_id($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->country_id = $val;
    }

    public function setReceiver_name($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->name = $val;
    }

    public function setReceiver_company($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->company = $val;
    }

    public function setReceiver_country($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->country = $val;
    }

    public function setReceiver_zip_code($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->zip_code = $val;
    }

    public function setReceiver_city($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->city = $val;
    }

    public function setReceiver_address_line_1($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->address_line_1 = $val;
    }

    public function setReceiver_address_line_2($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->address_line_2 = $val;
    }

    public function setReceiver_tel($val)
    {
        if($this->owner()->receiverAddressData === NULL) $this->owner()->receiverAddressData = new AddressData();
        $this->owner()->receiverAddressData->tel = $val;
    }


}
