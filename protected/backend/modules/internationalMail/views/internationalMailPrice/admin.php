<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);


?>

<h1>Manage internationalMail price</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'international-mail-country-group-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'date_updated',
        array(
            'header' => 'Price',
            'value' => '($data->internationalMailPriceHasGroupsSTAT)>0?"yes":"no"',
        ),
        array(
            'header' => 'Group prices',
            'value' => '($data->internationalMailPriceHasGroupsSTATGroup)',
        ),
        array(
            'header' => 'Countries',
            'name' => 'countryListsCOUNT',
            'filter' => false,
        ),


        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        ),
    ),
)); ?>
