<?php

/**
 * S_Payment class.
 */
class S_Courier_Id_From_File extends CModel
{
    public $local_id;
    public $remote_id;

    public function validateLocalId($attribute,$params)
    {

        $model = Courier::model()->find('local_id=:local_id', array(':local_id' => $this->$attribute));

        if($model === null)
            $this->addError($attribute, 'nie znaleziono przesyłki o takim ID');
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */

    public function save()
    {
        /* @var $model Courier */
        $model = Courier::model()->find('local_id=:local_id', array(':local_id' => $this->local_id));

        if($model === null)
            return false;

        $model->courier_carrier_id = 1;
        $model->outside_id = $this->remote_id;
        $model->changeStat(3);

        if(!$this->validate())
            return false;

        if($model->save())
            return true;
        else
            return false;
    }

    public function attributeNames()
    {
        return array(
            'local_id'=>'Local ID',
            'remote_id'=>'Remote ID',
        );
    }

    public function rules() {
        return array(

            array('local_id', 'validateLocalId'),
            array('local_id, remote_id', 'required'),
        );
    }
}