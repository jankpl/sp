<?php if(!S_Useful::sizeof($model)):?>

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, Yii::t('user_message', 'There are no messages!'), array('closeText' => false)); ?>

<?php else:; ?>

    <table class="table table-striped">
        <tr >
            <td><?php echo Yii::t('user_message', 'No.');?></td>
            <td><?php echo Yii::t('user_message', 'Date');?></td>
            <td><?php echo Yii::t('user_message', 'File');?></td>
            <td><?php echo Yii::t('user_message', 'Action');?></td>
        </tr>
        <?php foreach($model AS $item):?>
        <tr>
            <td><?php echo $item->no; ?></td>
            <td><?php echo $item->date; ?></td>
            <td><?php echo $item->file_name !== NULL? TbHtml::icon(TbHtml::ICON_OK) : TbHtml::icon(TbHtml::ICON_REMOVE) ?></td>
            <td><?php echo CHtml::link(Yii::t('user_message', 'View'), array('/userMessage/view', 'no' => $item->no), array('class' => 'btn btn-primaty')); ?></td>
        </tr>
        <?php endforeach;?>
    </table>

<?php endif; ?>