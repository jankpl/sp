<?php

Yii::import('application.models._base.BaseReport');

class Report extends BaseReport
{
    const TYPE_ADMIN_COURIER_SCAN = 1;
    const TYPE_EOBUWIE_COURIER_SCAN = 2;
    const TYPE_CUSTOMERS_COURIER_RETURN_SCAN = 3;
    const TYPE_POCZTAPOLSKA_FIREBUSINESS_SENT = 4;
    const TYPE_COD_SETTLED = 5;
    const TYPE_COURIER_SENT_NUMBERS = 6;
    const TYPE_EOBUWIE_DELIVERED = 7;

    public function rules() {
        return array(
            array('type, what_id', 'required'),
            array('what_id, stat, type', 'numerical', 'integerOnly'=>true),
            array('date_sent', 'safe'),
            array('date_sent', 'default', 'setOnEmpty' => true, 'value' => null),
            array('stat', 'default', 'setOnEmpty' => true, 'value' => 0),
            array('id, type, what_id, content, date_entered, date_sent, stat', 'safe', 'on'=>'search'),
        );
    }

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(), array(
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                    'createAttribute' => 'date_entered',
                    'updateAttribute' => NULL,
                ),
            )
        );
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function afterFind()
    {
        $this->content = json_decode($this->content);
        parent::afterFind();
    }

    public function beforeSave()
    {
        $this->content = json_encode($this->content);
        return parent::beforeSave();
    }

//   abstract public static function addItem();
//   abstract public static function processReport();

}