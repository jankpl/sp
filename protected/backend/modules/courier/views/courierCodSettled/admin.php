<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);


?>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<h2>Manage Courier COD Settle</h2>
<br/>
<a href="<?= Yii::app()->createUrl('/courier/courierCodSettled/stat');?>" class="btn btn-primary btn-lg">Show statistics</a>
<br/><br/>


<div class="gridview-container">
    <div class="gridview-overlay"></div>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;"><?= Yii::t('site', 'Zaznaczonych pozycji:');?> <span class="selected-no" style="font-weight: bold;">0</span></div>

    <?php
    $this->widget('MultipleSelect', array('selector' => '#stat-selector'));
    ?>
    <?php
    echo '<input type="hidden" class="grid-data"/>';
    $this->widget('application.components.SPGridViewNoGet', array(
        'id' => 'courier-cod-settle-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows'=>2,
        'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'afterAjaxUpdate' => true), true),
        'selectionChanged'=>'function(id){ checkNumberOfChecks(id); }',
        'columns' => array(
            [
                'name' => '_settledId',
                'value' => '$data->courierCodSettled->id',
            ],
            [
                'name' => '_isSettled',
                'value' => '$data->courierCodSettled ? "yes" : "no"',
                'filter' => [-1 => 'no', 1 => 'yes'],
            ],
            [
                'name' => '_settleDateEntered',
                'value' => '$data->courierCodSettled ? $data->courierCodSettled->date_entered : "-"',
            ],
            [
                'name'=>'_type',
                'value'=>'$data->courierCodSettled ? $data->courierCodSettled->getTypeName() : ""',//
                'filter'=> CourierCodSettled::listType()
            ],
            [
                'name' => '_settleDate',
                'value'=>'$data->courierCodSettled ? $data->courierCodSettled->settle_date : ""',//
//            'htmlOptions' => array('style'=>'text-align: right;'),
            ],
            'local_id',
            [
                'name' => 'date_entered',
                'header' => 'Package date',
                'value' => 'substr($data->date_entered,0,10)',
            ],
            array(
                'name'=>'stat_map_id',
                'value'=>'StatMap::getMapNameById($data->stat_map_id)',//
                'filter'=> StatMap::mapNameList(),
            ),
//		'courier.local_id',
            [
                'name' => 'cod_value',
                'value' => 'S_Price::formatPrice($data->cod_value)',
                'htmlOptions' => array('style'=>'text-align: right; width:120px; '),
            ],
            [
                'name' => 'cod_currency',
                'filter' => CourierCodAvailability::getCurrencyList()
//            'value' => 'S_Price::formatPrice($data->cod_value)." PLN"',
//            'htmlOptions' => array('style'=>'text-align: right;'),
            ],
            array(
                'name'=>'_user_login',
                'header'=>'User',
                'value' => '$data->user->login',
            ),
            [
                'name'=>'_source',
                'value'=>'$data->courierCodSettled ? $data->courierCodSettled->getSourceName() : ""',//
                'filter'=> CourierCodSettled::listSource()
            ],
            [
                'header' => 'Action',
                'class' => 'CButtonColumn',
                'template'=>'{update} ',
                'htmlOptions' => array('style'=>'width:20px; text-align: right;'),
            ],
            [
                'class'=>'MyCheckBoxColumn',
                'id'=>'selected_rows',
//            'disabled' => '$data->courierCodSettled',
            ],
        ),
    )); ?>

    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;"><?= Yii::t('site', 'Zaznaczonych pozycji:');?> <span class="selected-no" style="font-weight: bold;">0</span></div>
</div>


<?php
$script = 'function checkNumberOfChecks(id)
    {
        var number = $("#courier-cod-settle-grid").selGridViewNoGet("getAllSelection").length;

        $(".selected-no").html(number);
        if(number == 0)
        {
             $(".too-few-curain").show();
             $(".too-much-alert").addClass("hidden");
        }
        else if(number > 500)
        {
            //bootbox.alert("'.Yii::t('courier', 'Wybrano zbyt dużo pozycji na raz!').' Max: 500");
            $(".too-few-curain").show();
            $(".too-much-alert").removeClass("hidden");
        } else {
            $(".too-few-curain").hide();
            $(".too-much-alert").addClass("hidden");
        }
    }';

Yii::app()->clientScript->registerScript('cnoc', $script);
?>

<h3>Actions for selected</h3>

<?php echo TbHtml::tabbableTabs(array(
    array('label' => 'Export to file', 'content' => $this->renderPartial('admin_actions_tabs/_export_to_file', array(), true)),
    array('label' => 'Settle COD', 'content' => $this->renderPartial('admin_actions_tabs/_settle', array('settleFormModel' => $settleFormModel), true), 'id' => 'settle_tab'),
//    array('label' => 'Unsettle COD', 'content' => $this->renderPartial('admin_actions_tabs/_unsettle', array(), true)),
), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>

<script>
    $(document).ready(function(){

        if(window.location.hash == '#settle_tab') {
            $('.tabbable a[href="#settle_tab"]').trigger('click');
        }
    })
</script>

