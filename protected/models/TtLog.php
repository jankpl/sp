<?php

Yii::import('application.models._base.BaseTtLog');

class TtLog extends BaseTtLog
{
	CONST TYPE_COURIER = 1;
	CONST TYPE_POSTAL = 2;

	CONST KEEP_LOG_DAYS = 3;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function clearOldData()
	{
		Yii::app()->db->createCommand()->delete('tt_log', 'date_entered < (NOW() - INTERVAL :days DAY) AND last = 0', [':days' => self::KEEP_LOG_DAYS] );
	}
}