<div style="position: fixed; padding: 10px; width: 100%; bottom: 0; left: 0; z-index: 255; background: rgba(128,128,128,0.9); border-top: 1px solid white; cursor: pointer; color: white; text-align: center;" id="cookiesInfo">
<?php echo Yii::t('cookiesInfo', 'Ta strona używa plików cookies. Korzystając z niej wyrażasz zgodę na ich używanie. Kliknij, aby nie pokazywać więcej tego komunikatu.');?>
</div>
<script>
    $('#cookiesInfo').on('click', function(){
        document.cookie="cookiesInfo=10; expires=<?php echo (new DateTime())->add(new DateInterval('P1Y'))->format('D, j M Y,h:i:s');?> UTC; path=/";
        $(this).slideUp();
    });
</script>