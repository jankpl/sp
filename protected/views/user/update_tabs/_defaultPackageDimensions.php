<div class="form">
    <?php
    Yii::app()->getModule('courier');
    $courier = new Courier;
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'default-packages-sizes',
        'enableClientValidation'=>true,
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'dimensions']),
    ));
    ?>

    <h3><?php echo Yii::t('user', 'Domyślne parametry paczek');?></h3>

    <div class="alert alert-info"><?= Yii::t('courier', 'Ta opcja powoduje automatyczne uzupełnienie wymiarów oraz wagi Twoich paczek.');?></div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <table class="table">
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $courier->getAttributeLabel('package_weight');?> [<?= Courier::getWeightUnit();?>]</th>
                    <td><?= $form->textField($model, 'defaultPackageWeight', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPackageWeight');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $courier->getAttributeLabel('package_size_l');?> [<?= Courier::getDimensionUnit();?>]</th>
                    <td><?= $form->textField($model, 'defaultPackageSizeL', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPackageSizeL');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $courier->getAttributeLabel('package_size_d');?> [<?= Courier::getDimensionUnit();?>]</th>
                    <td><?= $form->textField($model, 'defaultPackageSizeD', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPackageSizeD');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $courier->getAttributeLabel('package_size_w');?> [<?= Courier::getDimensionUnit();?>]</th>
                    <td><?= $form->textField($model, 'defaultPackageSizeW', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPackageSizeW');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $courier->getAttributeLabel('package_value');?></th>
                    <td><?= $form->textField($model, 'defaultPackageValue', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPackageValue');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $courier->getAttributeLabel('value_currency');?></th>
                    <td><?= $form->dropDownList($model, 'defaultPackageValueCurrency', Courier::getCurrencyList());?><?= $form->error($model, 'defaultPackageValueCurrency');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $courier->getAttributeLabel('package_content');?></th>
                    <td><?= $form->textField($model, 'defaultPackageContent', ['class' => 'text-center']);?><?= $form->error($model, 'defaultPackageContent');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= (new CourierTypeU())->getAttributeLabel('_package_wrapping');?></th>
                    <td><?= $form->dropDownList($model, 'defaultPackageWrapping', User::getWrappingList(), ['prompt' => '-']);?><?= $form->error($model, 'defaultPackageWrapping');?> <?= Yii::t('courier', 'Opakowanie inne niż domyślne może powodować naliczenie dodatkowych opłat');?></td>
                </tr>
                <tr>
                    <th class="text-right" style="width: 25%;"><?= $model->getAttributeLabel('defaultPackageReceiverCountryId');?></th>
                    <td><?= $form->dropDownList($model, 'defaultPackageReceiverCountryId', CountryList::getActiveCountries(true), ['prompt' => '-']);?><?= $form->error($model, 'defaultPackageReceiverCountryId');?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'savePackageDefault'));?></td>
                </tr>
            </table>
        </div>
    </div>

    <?php
    $this->endWidget();
    ?>

    <h3><?php echo Yii::t('user', 'Typy nadania paczek "Standard"');?></h3>
    <br/>

    <?php
    Yii::app()->getModule('courier');
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'default-package-type',
        'enableClientValidation'=>true,
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'dimensions']),
    ));
    ?>

    <h4><?php echo Yii::t('user', 'Domyślny typ nadania');?></h4>

    <?php
    $data = CourierTypeInternal::getWithPickupList(true, true);

    $favPoints = CourierSpPointFav::getPointsForUser();
    unset($data[CourierTypeInternal::WITH_PICKUP_NONE]);

    $final = [];
    foreach($data AS $key => $item)
        $final[] = [
            'id' => $key,
            'text' => $item,
            'group' => NULL,
        ];

    /* @var $item CourierSpPointFav */
    foreach($favPoints AS $item)
        $final[] = [
            'id' => 'F@@@'.$item->getComplexId(),
            'text' => $item->getUsefulName(),
            'group' => CourierTypeInternal::getPickupTypeName(CourierTypeInternal::WITH_PICKUP_NONE),
        ];

    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <table class="table">
                <tr>
                    <th class="text-right" style="width: 25%;"></th>
                    <td><?= $form->dropDownList($model, 'courierTypeInternalDefaultPickup', CHtml::listData($final, 'id', 'text', 'group'), ['prompt' => '-']);?><?= $form->error($model, 'courierTypeInternalDefaultPickup');?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'saveCourierTypeInternalPickupDefault'));?></td>
                </tr>
            </table>
        </div>
    </div>

    <?php
    $this->endWidget();
    ?>

    <?php
    Yii::app()->getModule('courier');
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'fav-package-types',
        'enableClientValidation'=>true,
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'dimensions']),
    ));
    ?>

    <h4><?php echo Yii::t('user', 'Ulubione typy nadania');?></h4>

    <?php

    $favPoints = CourierSpPointFav::getPointsForUser();
    /* @var $item CourierSpPointFav */
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <table class="table">
                <?php
                $i = 1;
                if(!S_Useful::sizeof($favPoints))
                    echo '<tr><td>--'.Yii::t('user', 'brak').'--</td>';
                else
                foreach($favPoints AS $item):
                    ?>
                    <tr>
                        <td><?= $i; ?></td>
                        <td><?= $item->getUsefulName(); ?></td>
                        <td><?= CHtml::link(Yii::t('user', 'usuń'), ['/user/deleteFavPoint', 'id' => $item->id], ['class' => 'btn btn-warning btn-xs']);?></td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </table>
        </div>
    </div>

    <?php
    $this->endWidget();
    ?>

</div>