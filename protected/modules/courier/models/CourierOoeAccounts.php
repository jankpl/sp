<?php

Yii::import('application.modules.courier.models._base.BaseCourierOoeAccounts');

class CourierOoeAccounts extends BaseCourierOoeAccounts
{
    public $_user; // for grid view


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('login, pass, account_code, user_id, stat', 'required'),
			array('stat', 'numerical', 'integerOnly'=>true),
			array('login, pass, account_code', 'length', 'max'=>128),
			array('user_id', 'length', 'max'=>10),
			array('date_updated', 'safe'),
			array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, date_entered, date_updated, login, pass, account_code, user_id, stat, _user', 'safe', 'on'=>'search'),
		);
	}


	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_entered',
				'updateAttribute' => 'date_updated',
			),
		);
	}

	public static function listAvailableUsers($exceptThisOne = NULL)
	{

		if($exceptThisOne !== NULL)
			$users = User::model()->findAllBySql('SELECT * FROM user WHERE id NOT IN (SELECT user_id FROM courier_ooe_accounts) OR user.id = '.$exceptThisOne);
		else
			$users = User::model()->findAllBySql('SELECT * FROM user WHERE id NOT IN (SELECT user_id FROM courier_ooe_accounts)');

		return $users;
	}

	public function toggleStat()
	{

		$this->stat = abs($this->stat - 1);
		return $this->update(array('stat'));
	}


	public static function getCredentialsForUser($user_id = NULL)
	{
		return User::getCourierOoeCredentialsForUser($user_id);

//		if($user_id === NULL && !Yii::app()->user->isGuest)
//			$user_id = Yii::app()->user->id;
//
//		$CACHE_NAME = 'getCredentialsForUser_'.$user_id;
//
//		$result = Yii::app()->cache->get($CACHE_NAME);
//		if($result === false) {
//
//			if ($user_id) {
//				$cmd = Yii::app()->db->createCommand();
//				$cmd->select('login, pass, account_code');
//				$cmd->from('courier_ooe_accounts');
//				$cmd->where('user_id = :user_id AND stat = :stat', array(':user_id' => $user_id, ':stat' => S_Status::ACTIVE));
//				$result = $cmd->queryRow();
//			} else
//				$result = false;
//
//			Yii::app()->cache->set($CACHE_NAME, $result, 60*5);
//		}
//		return $result;
	}


    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('pass', $this->pass, true);
        $criteria->compare('account_code', $this->pass, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('stat', $this->stat);

        if($this->_user)
        {
            $criteria->together  =  true;
            $criteria->with = array('user');
            $criteria->compare('user.login',$this->_user, true);
        }


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes = array(
            '_user' => array
            (
                'asc' => 'user.login asc',
                'desc' => 'user.login desc',
            ),
            '*', // add all of the other columns as sortable
        );

        return new CActiveDataProvider($this->with('user'), array(
            'criteria' => $criteria,
            'sort'=> $sort,
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
    }



}