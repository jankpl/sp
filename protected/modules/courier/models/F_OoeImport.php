<?php

class F_OoeImport extends CFormModel {

    public $file;
    public $omitFirst;
    public $split;
    public $ooe_operator_id;
    public $custom_settings;

    public $_customOrder = [];
    public $_customSeparator = false;


    const MAX_LINES = 1000;
    const MAX_PACKAGES = 1000;

    /**
     * Function returns array of possible data separators
     * @return array
     */
    public static function listOfSeparators()
    {
        $data = [
            0 => ',',
            1 => ';',
            2 => '-',
            3 => '|',
        ];

        return $data;
    }

    /**
     * Function fetches User custom settings for import file
     *
     * @param null $user_id
     */
    public function applyCustomData($id, $user_id = NULL)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        $model = CourierImportSettings::getModelForUserId($id, $user_id);
        $data = $model->data;

        $this->_customOrder = $data['order'];
        $this->_customSeparator = self::listOfSeparators()[$data['separator']];
    }

    //
    // SORTABLE ATTRIBUTES

    /**
     * Return list of attributes for import
     *
     * @param array $customOrder Array with custom order
     * @param bool|true $reorderKeys Whether to reorder keys. False on displaying order with widget
     * @return array
     */
    public static function getAttributesList($customOrder = [], $reorderKeys = true)
    {
        $attributes = Array(
            0 => 'package_weight',
            1 => 'package_size_l',
            2 => 'package_size_w',
            3 => 'package_size_d',

            4 => 'package_value',
            5 => 'package_content',


            6 => 'sender_name',
            7 => 'sender_company',
            8 => 'sender_country',
            9 => 'sender_zip_code',
            10 => 'sender_city',
            11 => 'sender_address_line_1',
            12 => 'sender_address_line_2',
            13 => 'sender_tel',
            14 => 'sender_email',

            15 => 'receiver_name',
            16 => 'receiver_company',
            17 => 'receiver_country',
            18 => 'receiver_zip_code',
            19 => 'receiver_city',
            20 => 'receiver_address_line_1',
            21 => 'receiver_address_line_2',
            22 => 'receiver_tel',
            23 => 'receiver_email',

            24 => 'ref',

//            24 => 'options',
        );

        // apply custom order
        if(is_array($customOrder)) {
            $attributes = array_replace(array_flip($customOrder), $attributes);
            if($reorderKeys)
                $attributes = array_values($attributes);
        }

        return $attributes;
    }

    //



    public function rules() {
        return array(
            array('file', 'file',  'allowEmpty' => false,
                'maxSize' => 512400, 'types' => 'csv, xls, xlsx'),
            array('ooe_operator_id', 'required'),
            array('custom_settings', 'numerical', 'integerOnly' => true),
            array('omitFirst', 'safe'),
            array('split', 'numerical'),
        );
    }

    public function attributeLabels() {
        return array(
            'file' => Yii::t('app', 'File'),
            'omitFirst' => Yii::t('app', 'Omit first line (header)'),
            'split' => Yii::t('app', 'Split after'),
            'ooe_operator_id' => Yii::t('app', 'Operator'),
            'custom_settings' => Yii::t('app', 'Custom file settings'),
        );
    }


    public static function getAttributesMap($customOrder = [])
    {

        // mapping attributes to columns in file
        $attributes = array_flip(self::getAttributesList($customOrder));

        return $attributes;
    }

    public static function calculateTotalNumberOfPackages(array $data, $customOrder = [])
    {


        $number = 0;
        foreach($data AS $item)
            $number += $item[self::getAttributesMap($customOrder)['packages_number']];

        return $number;

    }

    public static function mapAttributesToModels(array $data, $ooe_operator_id, $customOrder = [])
    {

        $mapOfAttributes = self::getAttributesMap($customOrder);

        $models = [];

        $i = 0;
        foreach($data AS $item)
        {
            $i++;

            $courier = new Courier_CourierTypeOoe();
            $courier->source = Courier::SOURCE_FILE_IMPORT;

            $courier->courierTypeOoe = new courierTypeOoe('step_1');
            $courier->senderAddressData = new CourierTypeOoe_AddressData();
            $courier->receiverAddressData = new CourierTypeOoe_AddressData();

            $courier->courierTypeOoe->courier_ooe_service_id = $ooe_operator_id;
//            $courier->courierTypeOoe->cod_value = $item[self::getAttributesMap()['cod']];

            $courier->package_weight = S_Useful::convertCommaToDec($item[$mapOfAttributes['package_weight']]);
            $courier->package_size_l = S_Useful::convertCommaToDec($item[$mapOfAttributes['package_size_l']]);
            $courier->package_size_w = S_Useful::convertCommaToDec($item[$mapOfAttributes['package_size_w']]);
            $courier->package_size_d = S_Useful::convertCommaToDec($item[$mapOfAttributes['package_size_d']]);
            $courier->packages_number = S_Useful::convertCommaToDec($item[$mapOfAttributes['packages_number']]);
            $courier->package_value = S_Useful::convertCommaToDec($item[$mapOfAttributes['package_value']]);
            $courier->package_content = $item[$mapOfAttributes['package_content']];


            $courier->senderAddressData->name = $item[$mapOfAttributes['sender_name']];
            $courier->senderAddressData->company = $item[$mapOfAttributes['sender_company']];
            $courier->senderAddressData->country_id = CountryList::findIdByText($item[$mapOfAttributes['sender_country']]);
            $courier->senderAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->senderAddressData->country_id);
            $courier->senderAddressData->zip_code = $item[$mapOfAttributes['sender_zip_code']];
            $courier->senderAddressData->city = $item[$mapOfAttributes['sender_city']];
            $courier->senderAddressData->address_line_1 = $item[$mapOfAttributes['sender_address_line_1']];
            $courier->senderAddressData->address_line_2 = $item[$mapOfAttributes['sender_address_line_2']];
            $courier->senderAddressData->tel = $item[$mapOfAttributes['sender_tel']];
            $courier->senderAddressData->email = $item[$mapOfAttributes['sender_email']];

            $courier->receiverAddressData->name = $item[$mapOfAttributes['receiver_name']];
            $courier->receiverAddressData->company = $item[$mapOfAttributes['receiver_company']];
            $courier->receiverAddressData->country_id = CountryList::findIdByText($item[$mapOfAttributes['receiver_country']]);
            $courier->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->receiverAddressData->country_id);
            $courier->receiverAddressData->zip_code = $item[$mapOfAttributes['receiver_zip_code']];
            $courier->receiverAddressData->city = $item[$mapOfAttributes['receiver_city']];
            $courier->receiverAddressData->address_line_1 = $item[$mapOfAttributes['receiver_address_line_1']];
            $courier->receiverAddressData->address_line_2 = $item[$mapOfAttributes['receiver_address_line_2']];
            $courier->receiverAddressData->tel = $item[$mapOfAttributes['receiver_tel']];
            $courier->receiverAddressData->email = $item[$mapOfAttributes['receiver_email']];

            $courier->ref = $item[self::getAttributesMap()['ref']];

            $courier->courierTypeOoe->courier = $courier;

//            file_put_contents('ccc.txt', print_r(CountryList::findIdByText($item[self::getAttributesMap()['receiver_country']]),1), FILE_APPEND);
//            $options = $item[self::getAttributesMap()['options']];
//            $options = explode('|', $options);
//
//            if(is_array($options) && S_Useful::sizeof($options))
//            {
//                $courier->courierTypeOoe->addAdditions($options);
//            }

//            if($validate) {
//                $courier->validate();
//                $courier->receiverAddressData->validate();
//                $courier->senderAddressData->validate();
//                $courier->courierTypeOoe->validate();
//            }

            array_push($models, $courier);
        }

        return $models;
    }

    public static function validateModels(array &$models)
    {
        $refNumbers = [];

        $errors = false;
        foreach($models AS $key => $model)
        {
            $models[$key]->validate();
            $models[$key]->receiverAddressData->validate();
            $models[$key]->senderAddressData->validate();
            $models[$key]->courierTypeOoe->validate();


            // validate receiver country for selected OOE service
            if(!self::isReceiverCountryValid($models[$key]))
                $models[$key]->receiverAddressData->addError('country_id', Yii::t('courier', 'Wybrany operator nie obsłuje tego państwa'));

            if($models[$key]->hasErrors() OR $models[$key]->receiverAddressData->hasErrors() OR $models[$key]->senderAddressData->hasErrors() OR $models[$key]->courierTypeOoe->hasErrors())
                $errors = true;

            if($models[$key]->ref)
            {
                if(isset($refNumbers[$models[$key]->ref]))
                {
                    $models[$key]->addError('ref', Yii::t('courier', 'Numer REF musi być unikatowy!'));
                    $errors = true;
                }
                $refNumbers[$models[$key]->ref] = true;
            }
        }

        return $errors;
    }

//    public static function hasGroupErrors(array &$models)
//    {
//        $errors = false;
//        foreach($models AS $key => $model)
//        {
//            if(!$models[$key]->validate())
//                $errors = true;
//
//            if(!$models[$key]->receiverAddressData->validate())
//                $errors = true;
//
//            if(!$models[$key]->senderAddressData->validate())
//                $errors = true;
//
//            if(!$models[$key]->courierTypeOoe->validate())
//                $errors = true;
//
//            if(!self::isReceiverCountryValid($models[$key]))
//                $errors = true;
//        }
//        return $errors;
//    }


    public static function isReceiverCountryValid(Courier $courier)
    {
        $receiverCountry = CHtml::listData(CourierTypeOoe::getCountryList($courier->courierTypeOoe->courier_ooe_service_id), 'id', 'id');
        if(in_array($courier->receiverAddressData->country_id, $receiverCountry))
            return true;
        else
            return false;
    }

}