<?php

//if($_SERVER['REMOTE_ADDR'] == '127.0.0.1' OR $_SERVER['REMOTE_ADDR'] == '::1') {
    define('YII_LOCAL', true);
    define('YII_DEBUG_PROFILING', true);
//}
//else {
//    define('YII_LOCAL', false);
//    define('YII_DEBUG_PROFILING', false);
//}


error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
ini_set('display_errors', '0');

ini_set("log_errors", 1);
ini_set("error_log", "../rror.txt");


require_once(dirname(__FILE__).'/../vendor/autoload.php');

// change the following paths if necessary
$yiic=dirname(__FILE__).'/../vendor/yiisoft/yii/framework/yiic.php';
$config=dirname(__FILE__).'/config/console.php';

require_once($yiic);
