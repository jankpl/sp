<?php

class _CourierCodSettledGridView extends Courier
{
    public $_settleDateEntered;
    public $_settleDate;
    public $_settledId;
    public $_isSettled;
    public $_user_login;
    public $_type;
    public $_source;

    public function rules() {
        $array = array(
            array('_settleDateEntered, _settleDate, cod_currency, _user_login, _settledId, _isSettled, _type, _source','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.cod', true);


        $criteria->compare('t.local_id', $this->local_id);
        $criteria->compare('t.cod_currency', $this->cod_currency);

        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.stat_map_id', $this->stat_map_id);

        $criteria->together = true;
        $criteria->with = array('courierCodSettled');

        if($this->_isSettled == 1)
        {

            $criteria->addCondition('courierCodSettled.id IS NOT NULL');
        }

        if($this->_isSettled == -1)
        {
            $criteria->together = true;
            $criteria->with = array('courierCodSettled');
            $criteria->addCondition('courierCodSettled.id IS NULL');
        }

        if($this->_settledId)
        {
            $criteria->together = true;
            $criteria->with = array('courierCodSettled');
            $criteria->compare('courierCodSettled.id', $this->_settledId);
        }

        if($this->_settleDateEntered)
        {
            $criteria->together = true;
            $criteria->with = array('courierCodSettled');
            $criteria->compare('courierCodSettled.date_entered',$this->_settleDateEntered, true);
        }

        if($this->_settleDate)
        {
            $criteria->together = true;
            $criteria->with = array('courierCodSettled');
            $criteria->compare('courierCodSettled.settle_date',$this->_settleDate, true);
        }

        if($this->_type)
        {
            $criteria->together = true;
            $criteria->with = array('courierCodSettled');
            $criteria->compare('courierCodSettled.type',$this->_type, true);
        }

        if($this->_source OR $this->_source === "0")
        {
            $criteria->together = true;
            $criteria->with = array('courierCodSettled');
            $criteria->compare('courierCodSettled.source',$this->_source, true);
        }

        if($this->_user_login)
        {
            $criteria->together  =  true;
            $criteria->with = array('user');
            $criteria->compare('user.login',$this->_user_login,true);
        }

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR) {
            $criteria->together  =  true;
            $criteria->with = array('courier');
            $criteria->with = array('courier.user');
            $criteria->compare('courier.user.salesman_id', Yii::app()->user->model->salesman_id);

        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 'courierCodSettled.id DESC';
        $sort->attributes = array(
            '_settledId'=>array(
                'asc'=>'courierCodSettled.id ASC',
                'desc'=>'courierCodSettled.id DESC',
            ),
            '_settleDate'=>array(
                'asc'=>'courierCodSettled.settle_date ASC',
                'desc'=>'courierCodSettled.settle_date DESC',
            ),
            '_type'=>array(
                'asc'=>'courierCodSettled.type ASC',
                'desc'=>'courierCodSettled.type DESC',
            ),
            '_source'=>array(
                'asc'=>'courierCodSettled._source ASC',
                'desc'=>'courierCodSettled._source DESC',
            ),
            '_settleDateEntered'=>array(
                'asc'=>'courierCodSettled.date_entered ASC',
                'desc'=>'courierCodSettled.date_entered DESC',
            ),
            '_user_login'=>array(
                'asc'=>'user.login ASC',
                'desc'=>'user.login DESC',
            ),
            '*', // add all of the other columns as sortable
        );

        return new CActiveDataProvider($this->with('user')->with('courierCodSettled'), array(
            'criteria' => $this->searchCriteria(),
            'sort'=> $sort,
            'Pagination' => array (
                'PageSize' => 50,
            ),
        ));

    }



}