<?php

class DhlClient
{

    const OPT_PICK_DR = 0;
    const OPT_PICK_EX = 1;

    const URL = GLOBAL_CONFIG::DHL_URL;

    const USERNAME = GLOBAL_CONFIG::DHL_USERNAME;
    const PASSWORD = GLOBAL_CONFIG::DHL_PASSWORD;
    const USER_NO =  GLOBAL_CONFIG::DHL_USER_NO;

    const USERNAME_F = GLOBAL_CONFIG::DHL_USERNAME_F;
    const PASSWORD_F = GLOBAL_CONFIG::DHL_PASSWORD_F;
    const USER_NO_F = GLOBAL_CONFIG::DHL_USER_F_NO;

    protected $_username;
    protected $_password;
    protected $_user_no;

    protected $_service;

    public $withCollect = true;
    public $isPallet = false;

    public $isDluzyce = false;

    public $reference;

    public $package_value;

    public $sender_name;
    public $sender_address;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_tel;
    public $sender_mail;

    public $receiver_name;
    public $receiver_address;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_tel;
    public $receiver_mail;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $packages_number = 1;
    public $package_content;

    public $cod_value = 0;

    public $cod_currency = false;

    public $insurance_value = 0;

    protected $_opt_rod = false;
    protected $_opt_dor_1822 = false;
    protected $_opt_dor_sb = false;
    protected $_opt_nad_sb = false;
    protected $_opt_ubez = false;


    protected $_opt_dom09 = false;
    protected $_opt_dom12 = false;
    protected $_opt_connect = false;

    public $pickup_tommorow_earliest = false;

    const OPT_DOM09 = 'domesticExpress9';
    const OPT_DOM12 = 'domesticExpress12';
    const OPT_DOR_1822 = 'deliveryEvening';
    const OPT_NAD_SB = 'pickupOnSaturday';
    const OPT_DOR_SB = 'deliverySaturday';


    const STD_PICKUP_FROM = 'drPickupFrom';
    const EX_PICKUP_FROM = 'exPickupFrom';

    const STD_PICKUP_TO = 'drPickupTo';
    const EX_PICKUP_TO = 'exPickupTo';

    public static function createShipmentByTest()
    {
        $model = new self;
        $model->withCollect = true;

        $addressData = new AddressData();
        $addressData->name = 'DRESOWKA.PL';
        $addressData->company = 'CREATA SP. Z O.O.';
        $addressData->address_line_1 = 'UL. POPRZECZNA 7';
        $addressData->address_line_2 = '48-340 GŁUCHOŁAZY';
        $addressData->city = 'GŁUCHOŁAZY';
        $addressData->country_id = 1;
        $addressData->zip_code = '48-340';
        $addressData->tel = 1;

        $model->sender_name = $addressData->getUsefulName();
        $model->sender_zip_code = preg_replace('/\D/', '', $addressData->zip_code);
        $model->sender_city = $addressData->city;
        $model->sender_address = $addressData->address_line_1.' '.$addressData->address_line_2;
        $model->sender_tel = $addressData->tel;
        $model->sender_country = $addressData->country0->code;

        $model->receiver_name = 'Jan Nowak';
        $model->receiver_zip_code = '50304';
        $model->receiver_city = 'Wrocław';
        $model->receiver_address = 'Testowa 1/2';
        $model->receiver_tel = '111222333';
        $model->receiver_country = 'PL';

        $model->package_weight = 0.5;
        $model->packages_number = 1;

        $model->package_size_l = 102;
        $model->package_size_d = 73;
        $model->package_size_w = 30;

        $model->package_content = 'Test';
        $model->comments = 'Order #123';

        $model->cod_value = 0;

        $model->isPallet = false;

//        $model->_opt_dom12 = true;
//        $model->_opt_nad_sb = true;

        $model->rod = true;

        $model->setAccountByCourierLabelNewOperator(CourierLabelNew::OPERATOR_DHL);


        return $model->createShipment(true);
    }

    protected function _soapOperation($functionName, stdClass $data)
    {
        $mode = array
        (
//            'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false)))
        );

        $return = new stdClass();
        $return->success = false;

        $data->authData = new stdClass();
        $data->authData->username = $this->_username;
        $data->authData->password = $this->_password;

        if($functionName != 'getTrackAndTraceInfo')
            MyDump::dump('dhl.txt', 'REQUEST: '.print_r($data,1));
        else
            MyDump::dump('dhl-pl-track.txt', 'REQUEST: '.print_r($data,1));

        try {

            $client = new SoapClient(self::URL , $mode);
            $resp = $client->__soapCall($functionName, [$functionName => $data]);

            if($functionName != 'getTrackAndTraceInfo') {
                $respDump = unserialize(serialize($resp));
//
                if($respDump->createShipmentResult && $respDump->createShipmentResult->label && $respDump->createShipmentResult->label->labelContent)
                    $respDump->createShipmentResult->label->labelContent = '(...TRUNCATED LABEL BASE64...)';

                MyDump::dump('dhl.txt', 'RESPONSE: ' . print_r($respDump, 1));
            }


        } catch (SoapFault $E) {

            if($functionName != 'getTrackAndTraceInfo')
                MyDump::dump('dhl.txt', 'ERROR: '.print_r($E->getMessage(),1));

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;
            return $return;

        } catch (Exception $E) {

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;
            return $return;
        }

        $return->success = true;
        $return->data = $resp;

        return $return;
    }



    protected function setService()
    {
        if($this->sender_country != 'PL' OR $this->receiver_country != 'PL')
            $this->_service = 'PI';
        else if($this->_opt_dom09)
            $this->_service = '09';
        else if($this->_opt_dom12)
            $this->_service = '12';
        else if($this->_opt_connect)
            $this->_service = 'EK';
        else
            $this->_service = 'AH';
    }

    protected function setAccountByCourierLabelNewOperator($courierLabelNewOperator)
    {
        if($courierLabelNewOperator == CourierLabelNew::OPERATOR_DHL)
        {
            $this->_password = self::PASSWORD;
            $this->_username = self::USERNAME;
            $this->_user_no = self::USER_NO;
        }
        else if($courierLabelNewOperator == CourierLabelNew::OPERATOR_DHL_F)
        {
            $this->_password = self::PASSWORD_F;
            $this->_username = self::USERNAME_F;
            $this->_user_no = self::USER_NO_F;
        }
        else
            throw new CHttpException(400, 'Unknow operator!');
    }

//    public static function getVersion()
//    {
//        var_dump(self::_soapOperation('getVersion', new stdClass()));
//    }
//
//    public static function getMyShipments()
//    {
//        $data = new stdClass();
//        $data->createdFrom = '2016-01-01';
//        $data->createdTo = '2016-03-01';
//        $data->offset = 0;
//
//        var_dump(self::_soapOperation('getMyShipments', $data));
//
//    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $returnError = false, $labelNewOperator, $withCollect = false)
    {

        $model = new self;

        $model->setAccountByCourierLabelNewOperator($labelNewOperator);


        // verify collection
        if($withCollect)
        {
            // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
            if($withCollect && $courierInternal->parent_courier_id != NULL)
                $withCollect = false;

            // For Type Internal do not call if without pickup option
            // PICKUP_MOD / 14.09.2016
//            if($withCollect && $courier != NULL && !$courier->with_pickup)
            if($withCollect && $courierInternal != NULL && $courierInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR)
                $withCollect = false;

            // DO NOT CALL UPS COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
            if($withCollect && $courierInternal->courier->user != NULL && $courierInternal->courier->user->getUpsCollectBlock())
                $withCollect = false;
        }

        $model->withCollect = $withCollect;

        $model->sender_name = $from->getUsefulName();
        $model->sender_zip_code = preg_replace('/\D/', '', $from->zip_code);
        $model->sender_city = $from->city;
        $model->sender_address = $from->address_line_1.' '.$from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_mail = $from->fetchEmailAddress();

        $model->receiver_name = $to->getUsefulName();
        $model->receiver_zip_code = preg_replace('/\D/', '', $to->zip_code);
        $model->receiver_city = $to->city;
        $model->receiver_address = $to->address_line_1.' '.$to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_mail = $to->fetchEmailAddress();

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->packages_number = $courierInternal->courier->packages_number;

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->reference = 'ŚP #'.$courierInternal->courier->local_id;

        $model->cod_value = 0;
        if($courierInternal->courier->cod && $courierInternal->courier->cod_value > 0) {
            $model->cod_value = $courierInternal->courier->cod_value;
            $model->cod_currency = $courierInternal->courier->cod_currency;
        }

        if($courierInternal->hasAdditionById(CourierAdditionList::DHL_09))
            $model->_opt_dom09 = true;

        if($courierInternal->hasAdditionById(CourierAdditionList::DHL_12))
            $model->_opt_dom12 = true;

        if($courierInternal->hasAdditionById(CourierAdditionList::DHL_ROD))
            $model->_opt_rod = true;

        if($courierInternal->hasAdditionById(CourierAdditionList::DHL_DOR_1822))
            $model->_opt_dor_1822 = true;

        if($courierInternal->hasAdditionById(CourierAdditionList::DHL_DOR_SB))
            $model->_opt_dor_sb = true;

        if($courierInternal->hasAdditionById(CourierAdditionList::DHL_NAD_SB))
            $model->_opt_nad_sb = true;

        if($courierInternal->hasAdditionById(CourierAdditionList::DHL_UBEZ))
            $model->insurance_value = $courierInternal->courier->getPackageValueConverted('PLN');

        /* @ 05.09.2018 */
        /* Special rule for user "Europaka" - collection can be no sooner than "yesterday" with exception for weekend */
        if($courierInternal->courier->user_id == 2125 && date('N') < 6)
            $model->pickup_tommorow_earliest = true;

        return $model->createShipment($returnError);
    }

    public static function orderForCourierU(CourierTypeU $courierU, $returnError = false)
    {

        $model = new self;

        if($courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHL)
        {
            $model->_password = self::PASSWORD;
            $model->_username = self::USERNAME;
            $model->_user_no = self::USER_NO;
        }
        else if($courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHL_F
            OR $courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHL_F_PALETY
            OR $courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHL_F_DLUZYCE)
        {
            $model->_password = self::PASSWORD_F;
            $model->_username = self::USERNAME_F;
            $model->_user_no = self::USER_NO_F;
        }
        else
            throw new CHttpException(400, 'Unknow operator!');


        $withCollect = false;
        if($courierU->collection)
            $withCollect = true;

        // verify collection
        if($withCollect)
        {
            // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
            if($withCollect && $courierU->parent_courier_id != NULL)
                $withCollect = false;
        }

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->withCollect = $withCollect;

        $model->sender_name = $from->getUsefulName();
        $model->sender_zip_code = preg_replace('/\D/', '', $from->zip_code);
        $model->sender_city = $from->city;
        $model->sender_address = $from->address_line_1.' '.$from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_mail = $from->fetchEmailAddress();

        $model->receiver_name = $to->getUsefulName();
        $model->receiver_zip_code = preg_replace('/\D/', '', $to->zip_code);
        $model->receiver_city = $to->city;
        $model->receiver_address = $to->address_line_1.' '.$to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_mail = $to->fetchEmailAddress();

        $model->package_weight = $courierU->courier->getWeight(true);
        $model->packages_number = $courierU->courier->packages_number;

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->reference = 'ŚP #'.$courierU->courier->local_id;

        $model->cod_value = 0;
        if($courierU->courier->cod && $courierU->courier->cod_value > 0) {
            $model->cod_value = $courierU->courier->cod_value;
            $model->cod_currency = $courierU->courier->cod_currency;
        }


        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHL_DOMESTIC_09))
            $model->_opt_dom09 = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHL_DOMESTIC_12))
            $model->_opt_dom12 = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHL_ROD))
            $model->_opt_rod = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHL_DELIVERY_1822))
            $model->_opt_dor_1822 = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHL_DELIVERY_SATURDAY))
            $model->_opt_dor_sb = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHL_SEND_SATURDAY))
            $model->_opt_nad_sb = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_DHL_INSURANCE))
            $model->insurance_value = $courierU->courier->getPackageValueConverted('PLN');

        if($courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHL_F_PALETY)
            $model->isPallet = true;
        else if($courierU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHL_F_DLUZYCE)
            $model->isDluzyce = true;


        /* @ 05.09.2018 */
        /* Special rule for user "Europaka" - collection can be no sooner than "yesterday" with exception for weekend */
        if($courierU->courier->user_id == 2125 && date('N') < 6)
            $model->pickup_tommorow_earliest = true;

        return $model->createShipment($returnError);
    }

    public static function orderForCourierDomestic(CourierTypeDomestic $courierDomestic, $returnError = false)
    {
        $model = new self;

        if(in_array($courierDomestic->domestic_operator_id ,
            [CourierDomesticOperator::OPERATOR_DHL_F,
                CourierDomesticOperator::OPERATOR_DHL_F_PALETA,
                CourierDomesticOperator::OPERATOR_DHL_F_DLUZYCE,
            ]
        ))
            $model->setAccountByCourierLabelNewOperator(CourierLabelNew::OPERATOR_DHL_F);
        else
            $model->setAccountByCourierLabelNewOperator(CourierLabelNew::OPERATOR_DHL);


        $withCollect = true;

        // verify collection
        if($withCollect)
        {
            // DO NOT CALL UPS COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
            if($withCollect && $courierDomestic->courier->user != NULL && $courierDomestic->courier->user->getDomesticCollectBlock())
                $withCollect = false;
        }

        $model->withCollect = $withCollect;

        $model->sender_name = $courierDomestic->courier->senderAddressData->getUsefulName();
        $model->sender_zip_code = preg_replace('/\D/', '', $courierDomestic->courier->senderAddressData->zip_code);
        $model->sender_city = $courierDomestic->courier->senderAddressData->city;
        $model->sender_address = $courierDomestic->courier->senderAddressData->address_line_1.' '.$courierDomestic->courier->senderAddressData->address_line_2;
        $model->sender_tel = $courierDomestic->courier->senderAddressData->tel;
        $model->sender_country = $courierDomestic->courier->senderAddressData->country0->code;
        $model->sender_mail = $courierDomestic->courier->senderAddressData->fetchEmailAddress();

        $model->receiver_name = $courierDomestic->courier->receiverAddressData->getUsefulName();
        $model->receiver_zip_code = preg_replace('/\D/', '', $courierDomestic->courier->receiverAddressData->zip_code);
        $model->receiver_city = $courierDomestic->courier->receiverAddressData->city;
        $model->receiver_address = $courierDomestic->courier->receiverAddressData->address_line_1.' '.$courierDomestic->courier->receiverAddressData->address_line_2;
        $model->receiver_tel = $courierDomestic->courier->receiverAddressData->tel;
        $model->receiver_country = $courierDomestic->courier->receiverAddressData->country0->code;
        $model->receiver_mail = $courierDomestic->courier->receiverAddressData->fetchEmailAddress();


        $model->package_weight = $courierDomestic->courier->getWeight(true);
        $model->packages_number = $courierDomestic->courier->packages_number;

        $model->package_size_l = $courierDomestic->courier->package_size_l;
        $model->package_size_d = $courierDomestic->courier->package_size_d;
        $model->package_size_w = $courierDomestic->courier->package_size_w;

        $model->reference = 'ŚP #'.$courierDomestic->courier->local_id;
        $model->package_content = $courierDomestic->courier->package_content != '' ? $courierDomestic->courier->package_content : $model->reference ;

        $model->cod_value = 0;
        if($courierDomestic->courier->cod) {
            $model->cod_value = $courierDomestic->courier->cod_value;
            $model->cod_currency = $courierDomestic->courier->cod_currency;
        }

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::DHL_09))
            $model->_opt_dom09 = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::DHL_12))
            $model->_opt_dom12 = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::DHL_ROD))
            $model->_opt_rod = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::DHL_DOR_1822))
            $model->_opt_dor_1822 = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::DHL_DOR_SB))
            $model->_opt_dor_sb = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::DHL_NAD_SB))
            $model->_opt_nad_sb = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::DHL_UBEZ))
            $model->insurance_value = $courierDomestic->courier->package_value;


        if($courierDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL_PALETA OR $courierDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL_F_PALETA)
            $model->isPallet = true;

        if($courierDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_DHL_F_DLUZYCE)
            $model->isDluzyce = true;

        return $model->createShipment($returnError);
    }

    protected static function convertAddress($address)
    {

        $street = '';
        $building = '.';
        $local = '';

        $address = trim($address);
        $ln = mb_strlen($address);

        if($ln <= 35)
        {
            $street = $address;
        }
        else if($ln <= 45)
        {
            $street = mb_substr($address,0,35);
            $building = mb_substr($address,35,10);
        }
//        else if($ln <= 50)
//        {
//            $street = substr($address,0,35);
//            $building = substr($address,35,10);
//            $local = substr($address,45,5);
//        }
        else
        {
            $street = mb_substr($address,0,35);
            $building = mb_substr($address,35,10);
            $local = mb_substr($address,45,5);
        }
//        else
//        {
//            throw new CHttpException(ErrorCodes::DHL_UNCONVERTABLE_ADDRESS, Yii::t('courier', 'Podany adres nie może zostać przetworzony, ponieważ jest za długi! Dotyczy: {adres}', ['{adres}' => $address]));
//        }

        return [
            'street' => $street,
            'building' => $building,
            'local' => $local,
        ];

    }

    public function createShipment($returnError = false, $shiftDays = 0, $pickupDate = false, $shiftOpt = false)
    {

        if($this->_user_no == self::USER_NO)
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'Operator is not active',
                )];
                return $return;
            } else {
                return false;
            }


        $this->setService();

        $data = new stdClass();
        $data->shipment = new stdClass();

        $data->shipment->content = mb_substr($this->package_content,0,30);
        $data->shipment->reference = $this->reference;

        $data->shipment->shipmentInfo = new stdClass();

        if ($this->withCollect)
            $data->shipment->shipmentInfo->dropOffType = 'REQUEST_COURIER';
        else
            $data->shipment->shipmentInfo->dropOffType = 'REGULAR_PICKUP';

        $data->shipment->shipmentInfo->serviceType = $this->_service;

        $data->shipment->shipmentInfo->labelType = 'BLP';

        $data->shipment->shipmentInfo->billing = new stdClass();

        $data->shipment->shipmentInfo->billing->shippingPaymentType = 'USER';
        $data->shipment->shipmentInfo->billing->billingAccountNumber = $this->_user_no;
        $data->shipment->shipmentInfo->billing->paymentType = 'BANK_TRANSFER';


        $data->shipment->shipmentInfo->shipmentTime = new stdClass();

        // date is provided when method returned error due to wrong pickup date
        if(!$pickupDate) {
            $pickupDate = date('Y-m-d');
        }


        // shift last used date that caused error response
        if($shiftDays OR $this->pickup_tommorow_earliest)
            $pickupDate = S_Useful::workDaysNextDate($pickupDate, $this->pickup_tommorow_earliest, $this->_opt_nad_sb);

        $pickupHourFrom = '08:00';
        $pickupHourTo = '17:00';

        if($this->isDluzyce OR $this->isPallet OR $this->package_weight > 31.5 && !$this->_opt_dom09 && !$this->_opt_dom12)
            $opt = self::OPT_PICK_DR;
        else
            $opt = self::OPT_PICK_EX;

        if($shiftOpt)
        {
            if($opt == self::OPT_PICK_DR)
                $opt = self::OPT_PICK_EX;
            else
                $opt = self::OPT_PICK_DR;
        }

        $selectedDate = $pickupDate;

        // check pickup date
        for ($i = 0; $i < 5; $i++) {

            // prevent rechecking same day
            if(!$i OR $selectedDate != $pickupDate) {

                MyDump::dump('dhl_pick_log.txt', $this->reference.'|OPT:'.$opt.'| ITERATE:' . $i . ' : ' . $selectedDate . "\r\n\r\n", FILE_APPEND);

                $availability = self::getServicesForZipCode($this->sender_zip_code, $selectedDate);
                MyDump::dump('dhl_pick_log.txt', $this->reference.'|OPT:'.$opt.'| '.print_r($availability, 1) . "\r\n\r\n", FILE_APPEND);
                if (S_Useful::sizeof($availability)) {

                    if ($opt == self::OPT_PICK_DR) {
                        if ($availability[self::STD_PICKUP_FROM] != 'brak') {
                            $pickupHourFrom = $availability[self::STD_PICKUP_FROM];
                            $pickupHourTo = $availability[self::STD_PICKUP_TO];
                            MyDump::dump('dhl_pick_log.txt', $this->reference.'|OPT:'.$opt.'| MAM!:' . $pickupHourFrom .' -> '.$pickupHourTo . "\r\n\r\n", FILE_APPEND);
                            break;
                        }
                    } else {
                        if ($availability[self::EX_PICKUP_FROM] != 'brak') {

                            $pickupHourFrom = $availability[self::EX_PICKUP_FROM];
                            $pickupHourTo = $availability[self::EX_PICKUP_TO];
                            MyDump::dump('dhl_pick_log.txt', $this->reference.'|OPT:'.$opt.'| MAM!:' . $pickupHourFrom .' -> '.$pickupHourTo . "\r\n\r\n", FILE_APPEND);
                            break;
                        }
                    }
                }
            }

            $selectedDate = S_Useful::workDaysNextDate($pickupDate, $i, $this->_opt_nad_sb);
        }



        $data->shipment->shipmentInfo->shipmentTime->shipmentDate = $selectedDate;

        //        $pickupHourTo = '24:00';

        $data->shipment->shipmentInfo->shipmentTime->shipmentStartHour = $pickupHourFrom;
        $data->shipment->shipmentInfo->shipmentTime->shipmentEndHour = $pickupHourTo;

        MyDump::dump('dhl_pick_log.txt', $this->reference.'|OPT:'.$opt.'|'.$selectedDate.'|'.$pickupHourFrom.'|'.$pickupHourTo.'|'.$opt.'|'.print_r($availability,1)."\r\n\r\n", FILE_APPEND);

        try {
            $fromAddress = self::convertAddress($this->sender_address);
            $toAddress = self::convertAddress($this->receiver_address);

        }
        catch (CHttpException $ex)
        {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $ex->getMessage(),
                )];
                return $return;
            } else {
                return false;
            }
        }

        $data->shipment->ship = new stdClass();
        $data->shipment->ship->shipper = new stdClass();
        $data->shipment->ship->shipper->address = new stdClass();
        $data->shipment->ship->shipper->address->name = mb_substr($this->sender_name, 0,60);
        $data->shipment->ship->shipper->address->postalCode = $this->sender_zip_code;
        $data->shipment->ship->shipper->address->city = $this->sender_city;
        $data->shipment->ship->shipper->address->street = $fromAddress['street'];
        $data->shipment->ship->shipper->address->houseNumber = $fromAddress['building'];
        $data->shipment->ship->shipper->address->apartmentNumber = $fromAddress['local'];

        $data->shipment->ship->shipper->contact = new stdClass();
//        $data->shipment->ship->shipper->contact->personName = $this->sender_name;
        $data->shipment->ship->shipper->contact->phoneNumber = $this->sender_tel;
        $data->shipment->ship->shipper->contact->emailAddress = $this->sender_mail;
//        $data->shipment->shipmentInfo->shipper->contactPhone = '111222333';
//        $data->shipment->ship->shipper->address->country = 'PL';

        $data->shipment->ship->receiver = new stdClass();
        $data->shipment->ship->receiver->address = new stdClass();
        $data->shipment->ship->receiver->address->country = $this->receiver_country;
        $data->shipment->ship->receiver->address->name =  mb_substr($this->receiver_name, 0,60);
        $data->shipment->ship->receiver->address->postalCode = $this->receiver_zip_code;
        $data->shipment->ship->receiver->address->city = $this->receiver_city;
        $data->shipment->ship->receiver->address->street = $toAddress['street'];
        $data->shipment->ship->receiver->address->houseNumber = $toAddress['building'];
        $data->shipment->ship->receiver->address->apartmentNumber = $toAddress['local'];

        $data->shipment->ship->receiver->contact = new stdClass();
//        $data->shipment->ship->receiver->contact->personName = $this->receiver_name;
        $data->shipment->ship->receiver->contact->phoneNumber = $this->receiver_tel;
        $data->shipment->ship->receiver->contact->emailAddress = $this->receiver_mail;

        $to = $this->receiver_address;
        $to = trim($to);
        $to = mb_substr($to, -82);
        $data->shipment->comment = 'TO:(...)'.$to;

        $data->shipment->comment = mb_substr($data->shipment->comment.';'.$this->reference,0,100);

        $data->shipment->pieceList = new stdClass();
        $data->shipment->pieceList->item = new stdClass();

        if ($this->isPallet)
            $data->shipment->pieceList->item->type = 'PALLET';
        else
            $data->shipment->pieceList->item->type = 'PACKAGE';

        // 02.11.2016
        // DHL requires weight >= 1 KG
        if($this->package_weight < 1)
            $this->package_weight = 1;

        $data->shipment->pieceList->item->weight = ($this->package_weight);

        $data->shipment->pieceList->item->width = ceil($this->package_size_w);
        $data->shipment->pieceList->item->height = ceil($this->package_size_d);
        $data->shipment->pieceList->item->length = ceil($this->package_size_l);

        $data->shipment->pieceList->item->quantity = $this->packages_number;


        $data->shipment->pieceList->item->nonStandard = false;

        if(!$this->isPallet)
        {
            $sizes = [];
            $sizes[] = $this->package_size_l;
            $sizes[] = $this->package_size_d;
            $sizes[] = $this->package_size_w;

            rsort($sizes);

            if ($sizes[0] > 120 OR $sizes[1] > 60)
                $data->shipment->pieceList->item->nonStandard = true;

        }
        else
        {
            $widLen = [];
            $widLen[] = $this->package_size_l;
            $widLen[] = $this->package_size_w;
            rsort($widLen);

            if($widLen[0] > 120 OR $widLen[1] > 80)
                $data->shipment->pieceList->item->nonStandard = true;
        }

        // dluzyce are not NST
        if($this->isDluzyce)
            $data->shipment->pieceList->item->nonStandard = false;

        $data->shipment->shipmentInfo->specialServices = new stdClass();

        if ($this->cod_value > 0) {
            $data->shipment->shipmentInfo->specialServices->item = [];
            $tempIndex = S_Useful::sizeof($data->shipment->shipmentInfo->specialServices->item);
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex] = new stdClass();
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->serviceType = 'COD';
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->serviceValue = $this->cod_value;
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->collectOnDeliveryForm = 'BANK_TRANSFER';

            $this->insurance_value = $this->cod_value > $this->package_value ? $this->cod_value : $this->package_value;

            if(!CourierCodAvailability::compareCurrences($this->cod_currency, CourierCodAvailability::CURR_PLN)) {
                if ($returnError == true) {
                    $return = [0 => array(
                        'error' => 'Incorrect COD currency! Only PLN allowed!',
                    )];
                    return $return;
                } else {
                    return false;
                }
            }

        }


        if ($this->insurance_value > 0) {
            $tempIndex = S_Useful::sizeof($data->shipment->shipmentInfo->specialServices->item);

            $data->shipment->shipmentInfo->specialServices->item[$tempIndex] = new stdClass();
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->serviceType = 'UBEZP';
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->serviceValue = $this->insurance_value;
        }

        if ($this->_opt_rod) {
            $tempIndex = S_Useful::sizeof($data->shipment->shipmentInfo->specialServices->item);

            $data->shipment->shipmentInfo->specialServices->item[$tempIndex] = new stdClass();
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->serviceType = 'ROD';
        }

        if ($this->_opt_nad_sb) {
            $tempIndex = S_Useful::sizeof($data->shipment->shipmentInfo->specialServices->item);

            $data->shipment->shipmentInfo->specialServices->item[$tempIndex] = new stdClass();
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->serviceType = 'NAD_SOBOTA';
        }

        if ($this->_opt_dor_sb) {
            $tempIndex = S_Useful::sizeof($data->shipment->shipmentInfo->specialServices->item);

            $data->shipment->shipmentInfo->specialServices->item[$tempIndex] = new stdClass();
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->serviceType = 'SOBOTA';
        }

        if ($this->_opt_dor_1822) {
            $tempIndex = S_Useful::sizeof($data->shipment->shipmentInfo->specialServices->item);

            $data->shipment->shipmentInfo->specialServices->item[$tempIndex] = new stdClass();
            $data->shipment->shipmentInfo->specialServices->item[$tempIndex]->serviceType = '1722';
        }

        $resp = $this->_soapOperation('createShipment', $data);

        if ($resp->success == true) {
            $return = [];

            $collectId = false;
            if($this->withCollect)
                $collectId = $resp->data->createShipmentResult->dispatchNotificationNumber;

            $ttNumber = $resp->data->createShipmentResult->shipmentTrackingNumber;

            $basePath = Yii::app()->basePath . '/../';
            $dir = 'uplabels/';
            $temp = $dir . 'temp/';

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            @file_put_contents($basePath . $temp . $ttNumber . '.pdf', base64_decode($resp->data->createShipmentResult->label->labelContent));
            for ($i = 0; $i < $this->packages_number; $i++) {
                $im->readImage($basePath . $temp . $ttNumber . '.pdf['.$i.']');
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }
                $im->trimImage(0);
                $im->stripImage();

                $return[] = [
                    'status' => true,
                    'label' => $im->getImageBlob(),
                    'track_id' => $ttNumber,
                    'collect_id' => $collectId
                ];
            }
            @unlink($basePath . $temp . $ttNumber . '.pdf');

            return $return;
        }
        else if(((in_array($resp->errorCode, [101,111])) OR ($resp->errorCode == 106 && in_array(mb_substr($resp->errorDesc,0,66), [
                        'Błędy walidacji przesyłki: Data nadania przesyłki jest niepoprawna',
                        'Błędy walidacji przesyłki: Nie można utworzyć przesyłek z wsteczną'
                    ]))) && ($shiftDays < 1 || !$shiftOpt))
        {
            MyDump::dump('dhl_pick_log.txt', 'SHIFT:'.$this->reference.'|'.$selectedDate.'|'.print_r($resp->errorDesc,1)."\r\n\r\n", FILE_APPEND);

            $shiftDays = $shiftDays + 1;
            $dateNext =  date('Y-m-d',  strtotime('+1 day', strtotime($selectedDate)));

            // after two tries, try another hours type
            if($shiftDays > 1 && !$shiftOpt)
            {
                MyDump::dump('dhl_pick_log.txt', 'NOW SHIFT TYPE:'.$this->reference."\r\n\r\n", FILE_APPEND);
                $shiftDays = 0;
                $shiftOpt = true;
                $dateNext = false;
            }


            return $this->createShipment($returnError, $shiftDays, $dateNext, $shiftOpt);
        }
        else {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => $resp->error,
                )];
                return $return;
            } else {
                return false;
            }
        }
    }

    public static function getServicesForZipCode($zip_code, $pickup_date)
    {
        $zip_code = preg_replace('/\D/', '', $zip_code);

        $CACHE_NAME = 'DHL_SERVICES_'.$zip_code.'_'.$pickup_date;

        $response = Yii::app()->cache->get($CACHE_NAME);

        if ($response === false) {

            $data = new stdClass();
            $data->postCode = $zip_code;
            $data->pickupDate = $pickup_date;

            $model = new self;

            $model->setAccountByCourierLabelNewOperator(CourierLabelNew::OPERATOR_DHL);

            $resp = $model->_soapOperation('getPostalCodeServices', $data);

            if ($resp->success)
                $response = json_decode(json_encode($resp->data->getPostalCodeServicesResult), True);
            else
                $response = false;

            Yii::app()->cache->set($CACHE_NAME, $response, 60 * 1);
        }

        if(!is_array($response))
            $response = [];

        return $response;
    }


    public static function track($number, $customAccount = false)
    {

        $data = new stdClass();
        $data->shipmentId = $number;

        $model = new self;

        if(!$customAccount)
            $model->setAccountByCourierLabelNewOperator(CourierLabelNew::OPERATOR_DHL);
        else
            $model->setAccountByCourierLabelNewOperator(CourierLabelNew::OPERATOR_DHL_F);

        $resp = $model->_soapOperation('getTrackAndTraceInfo', $data);

//        MyDump::dump('dhl-pl-track2.txt',$number.' : '.print_r($customAccount,1) .' : '.print_r($resp,1));

        if(($resp->errorCode == 107 OR $resp->errorDesc == 'Nie znaleziono przesyłki o zadanym identyfikatorze'))
            return self::track($number, $customAccount ? false : true);

        if($resp->success)
        {
            $statuses = [];

            $receivedBy = $resp->data->getTrackAndTraceInfoResult->receivedBy;


            $resp = $resp->data->getTrackAndTraceInfoResult->events->item;



            if(is_array($resp)) {
                foreach ($resp AS $item) {

                    if($receivedBy != '' && (string)$item->status == 'DOR')
                        $item->terminal .= ' (odebrana przez: '.$receivedBy.')';

                    $temp = array(
                        'name' => (string)$item->description,
                        'date' => $item->timestamp,
                        'location' => $item->terminal,
                    );

                    if(self::isIgnoredStat($temp['name']))
                        continue;

                    array_push($statuses, $temp);
                }
            }
            else if($resp instanceof stdClass)
            {
                if($receivedBy != '' && (string)$resp->status == 'DOR')
                    $resp->terminal .= ' (odebrana przez: '.$receivedBy.')';

                $temp = array(
                    'name' => (string)$resp->description,
                    'date' => $resp->timestamp,
                    'location' => $resp->terminal,
                );

                if(!self::isIgnoredStat($temp['name']))
                    array_push($statuses, $temp);

            }


            return $statuses;
        }

        return false;
    }

    protected static function isIgnoredStat($stat)
    {
        return (in_array($stat, [
            'DHL otrzymał dane elektroniczne przesyłki. Informacje zostaną zaktualizowane po przekazaniu przez Nadawcę przesyłki do transportu',
        ]));
    }

}