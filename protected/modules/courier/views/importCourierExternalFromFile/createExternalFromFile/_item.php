<?php
/* @var $model Courier */
/* @var $i Integer */

?>


<tr class="condensed">
    <td <?php echo ($model->hasErrors('package_weight')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model,'['.$i.']package_weight', array('style' => 'width: 25px;', 'title' => $model->getError('package_weight'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_l')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model,'['.$i.']package_size_l', array('style' => 'width: 25px;', 'title' => $model->getError('package_size_l'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_w')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model,'['.$i.']package_size_w', array('style' => 'width: 25px;', 'title' => $model->getError('package_size_w'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_size_d')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model,'['.$i.']package_size_d', array('style' => 'width: 25px;', 'title' => $model->getError('package_size_d'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('packages_number')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeDropDownList($model, '['.$i.']packages_number', S_Useful::generateArrayOfItemsNumber(1,100), array('style' => 'width: 60px;', 'title' => $model->getError('packages_number'))); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_value')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model,'['.$i.']package_value', array('style' => 'width: 75px;', 'title' => $model->getError('package_value'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->hasErrors('package_content')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model,'['.$i.']package_content', array('style' => 'width: 75px;', 'title' => $model->getError('package_content'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td style="<?php echo ($model->senderAddressData->hasErrors('name')?'background: red;':'');?>">
    <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]name', array('title' => $model->senderAddressData->getError('name'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('company')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]company', array('title' => $model->senderAddressData->getError('company'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('country_id')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeDropDownList($model->senderAddressData,'['.$i.'][sender]country_id', CHtml::listData(CountryList::getActiveCountries(),'id','translatedName','firstLetter'), array('empty'=>'--Select a country--', 'title' => $model->senderAddressData->getError('country_id'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('zip_code')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]zip_code', array('title' => $model->senderAddressData->getError('zip_code'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('city')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]city', array('title' => $model->senderAddressData->getError('city'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('address_line_1')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]address_line_1', array('title' => $model->senderAddressData->getError('address_line_1'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->senderAddressData->hasErrors('address_line_2')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]address_line_2', array('title' => $model->senderAddressData->getError('address_line_2'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td style="border-right: 2px dashed gray; <?php echo ($model->senderAddressData->hasErrors('tel')?'background: red;':'');?>">
        <?php echo CHtml::activeTextField($model->senderAddressData,'['.$i.'][sender]tel', array('title' => $model->senderAddressData->getError('tel'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td style="<?php echo ($model->receiverAddressData->hasErrors('name')?'background: red;':'');?>">
    <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]name', array('title' => $model->receiverAddressData->getError('name'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('company')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]company', array('title' => $model->receiverAddressData->getError('company'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('country_id')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeDropDownList($model->receiverAddressData,'['.$i.'][receiver]country_id', CHtml::listData(CountryList::getActiveCountries(),'id','translatedName','firstLetter'), array('empty'=>'--Select a country--', 'title' => $model->receiverAddressData->getError('country_id'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('zip_code')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]zip_code', array('title' => $model->receiverAddressData->getError('zip_code'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('city')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]city', array('title' => $model->receiverAddressData->getError('city'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('address_line_1')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]address_line_1', array('title' => $model->receiverAddressData->getError('address_line_1'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('address_line_2')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]address_line_2', array('title' => $model->receiverAddressData->getError('address_line_2'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
    <td <?php echo ($model->receiverAddressData->hasErrors('tel')?'style="background: red;"':'');?>>
        <?php echo CHtml::activeTextField($model->receiverAddressData,'['.$i.'][receiver]tel', array('title' => $model->receiverAddressData->getError('tel'), 'disabled' => ($split==F_UploadFile::SPLIT_FULL_EDIT)?'':'disabled')); ?>
    </td>
</tr>

