<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
    array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
    array('label'=>'Delete ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1><?php echo Yii::t('m_site','View details'); ?></h1>
<br/>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'date_entered',
        'type',
        'name',
        'company',
        'country',
        'zip_code',
        'city',
        'address_line_1',
        'address_line_2',
        'tel',
        'email',


    ),
));
?>
<br/>
<hr/>

<div style="text-align: center;">
    <?php echo CHtml::link(Yii::t('site','Contact book'), array('/userContactBook/'), array('class' => 'btn')); ?>
</div>