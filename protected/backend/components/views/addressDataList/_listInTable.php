<?php
/* @var $model AddressData */
/* @var $editUrl string  */
?>

<table class="list hLeft" style="width: 550px;">
    <tr>
        <td>Name</td>
        <td><?php echo $model->name; ?></td>
    </tr>
    <tr>
        <td>Company</td>
        <td><?php echo $model->company; ?></td>
    </tr>
    <tr>
        <td>Country</td>
        <td><?php echo $model->country; ?></td>
    </tr>
    <tr>
        <td>City</td>
        <td><?php echo $model->city; ?></td>
    </tr>
    <tr>
        <td>Zip code</td>
        <td><?php echo $model->zip_code; ?></td>
    </tr>
    <tr>
        <td>Address</td>
        <td><?php echo $model->address_line_1; ?></td>
    </tr>
    <tr>
        <td>Address cont.</td>
        <td><?php echo $model->address_line_2; ?></td>
    </tr>
    <tr>
        <td>Tel.</td>
        <td><?php echo $model->tel; ?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?php echo $model->email; ?></td>
    </tr>
    <?php
    if($model->getBankAccount() != ''):
        ?>
        <tr>
            <td>Bank Account</td>
            <td><?php echo $model->getBankAccount(); ?> <?= $editUrl ? CHtml::link('[edit]', $editUrl) : '' ;?></td>
        </tr>
    <?php endif; ?>
</table>