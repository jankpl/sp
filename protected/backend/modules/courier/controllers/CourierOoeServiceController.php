<?php

class CourierOoeServiceController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierOoeService'),
        ));
    }

    public function actionCreate() {

        $model = new CourierOoeService;
        $modelTrs = Array();



        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $serviceTr = new CourierOoeServiceTr();
            $serviceTr->language_id = $item->id;
            $modelTrs[$item->id] = $serviceTr;
        }

        if (isset($_POST['CourierOoeService'])) {
            $model->setAttributes($_POST['CourierOoeService']);
            $model->_countryLists = $_POST['CourierOoeService']['countryLists'];

            foreach($_POST['CourierOoeServiceTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierOoeServiceTr();
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierOoeServiceTrs = $modelTrs;

            if ($model->saveWithTrAndCountries()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,

        ));
    }

    public function actionUpdate($id) {


        $model = $this->loadModel($id, 'CourierOoeService');

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = CourierOoeServiceTr::model()->find('courier_ooe_service_id=:courier_ooe_service_id AND language_id=:language_id', array(':courier_ooe_service_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $serviceTr = new CourierOoeServiceTr();
                $serviceTr->language_id = $item->id;
                $serviceTr->courier_ooe_service_id = $id;
                $modelTrs[$item->id] = $serviceTr;
            }
        }


        if (isset($_POST['CourierOoeService'])) {
            $model->setAttributes($_POST['CourierOoeService']);
            $model->_countryLists = $_POST['CourierOoeService']['countryLists'];

            foreach($_POST['CourierOoeServiceTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierOoeServiceTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierOoeServiceTrs = $modelTrs;

            if ($model->saveWithTrAndCountries()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $this->loadModel($id, 'CourierOoeService')->delete();
            }
            catch(Exception $ex)
            {
                throw new CHttpException(403,'Nie udało się usunąć tego wpisu!');
            }


            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierOoeService('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierOoeService']))
            $model->setAttributes($_GET['CourierOoeService']);


        if(isset($_POST['get-ooe-services']))
        {
            $services = Yii::app()->OneWorldExpress->getServicesForUser($_POST['login'], $_POST['password'], true);
        }

        $this->render('admin', array(
            'model' => $model,
            'services' => $services
        ));
    }

    public function actionStat($id)
    {
        /* @var $model CourierOoeService */


        $model = CourierOoeService::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny id!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}