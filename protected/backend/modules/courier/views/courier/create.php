<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Create'),
);

$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url' => array('index')),
    array('label'=>'Manage' . $model->label(2), 'url' => array('admin')),
);
?>

<h1>Create <?php echo GxHtml::encode($model->label()); ?></h1>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php $this->renderPartial('_base_form',array(
        'model' => $model,
        'form' => $form,
    ));
    ?>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Validate'), array('name' => 'validate'));
    echo GxHtml::submitButton(Yii::t('app', 'Save without validation'), array('name' => 'save_no_validation'));
    echo GxHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'save'));
    $this->endWidget();
    ?>
</div><!-- form -->