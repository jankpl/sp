<?php

/* @var $this InternationalMailController */
/* @var $model InternationalMail */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);



$this->menu=array(

    array('label'=>'User details', 'url'=>array('user/view', 'id' => $model->user_id),'visible'=>$model->user_id!==null),
    array('label'=>'Order details', 'url'=>array('order/view', 'id' => $model->order_id),'visible'=>$model->order_id!==null),
);
?>

<h1>Mail #<?php echo $model->local_id; ?></h1>

<div style="float: left; width: 48%;">

    <h3>Mail details</h3>
    <table class="list hLeft" style="width: 500px;">
        <tr>
            <td>User</td>
            <td><?php echo $model->user_id !== null ? CHtml::link($model->user->login, array('/user/view', 'id' => $model->user_id)) : '-';?></td>
        </tr>
        <tr>
            <td>Creation date</td>
            <td><?php echo substr($model->date_entered,0,16);?></td>
        </tr>
        <tr>
            <td>Status</td>
            <td>[<?php echo $model->international_mail_stat_id;?>] <?php echo $model->stat->name;?></td>
        </tr>
        <tr>
            <td>Order</td>
            <td><?php echo CHtml::link('Order #'.$model->order_id, array('/order/view', 'id' => $model->order->id));?></td>
        </tr>
        <tr>
            <td>Local ID</td>
            <td><?php echo $model->local_id;?></td>
        </tr>
        <tr>
            <td>Weight</td>
            <td><?php echo $model->mail_weight;?> <?php echo InternationalMail::getWeightUnit();?></td>
        </tr>
        <tr>
            <td>Dimensions</td>
            <td><?php echo $model->mail_size_d;?><?php echo InternationalMail::getDimensionUnit();?> x <?php echo $model->mail_size_l;?><?php echo InternationalMail::getDimensionUnit();?> x <?php echo $model->mail_size_w;?><?php echo InternationalMail::getDimensionUnit();?></td>
        </tr>
        <tr>
            <td>Remote ID</td>
            <td><?php echo $model->outside_id;?></td>
        </tr>
    </table>

    <h3>Additions</h3>
    <table class="list hTop" style="width: 500px;">
        <tr>
            <td>Name</td>
            <td>Description</td>
        </tr>
        <?php

        /* @var $item InternationalMailAdditionList */
        if(!S_Useful::sizeof($model->listAdditions()))
            echo '<tr><td colspan="2">none</td></tr>';
        else
            foreach($model->listAdditions() AS $item): ?>
                <tr>
                    <td><?php echo $item->name; ?></td>
                    <td><?php echo $item->description; ?></td>
                </tr>

            <?php endforeach; ?>
    </table>

    <h3>Mail sender</h3>
    <?php

    $this->renderPartial('//_addressData/_listInTable',
        array('model' => $model->senderAddressData,
        ));

    ?>

    <h3>Mail receiver</h3>
    <?php

    $this->renderPartial('//_addressData/_listInTable',
        array('model' => $model->receiverAddressData,
        ));

    ?>

</div>
<div style="float: right; width: 48%;">

    <h3>Carriage ticket</h3>
    <table class="list " style="width: 500px;">
        <tr>
            <td><?php echo CHtml::link('Generate carriage ticket', array('/internationalMail/internationalMail/CarriageTicket', 'id' => $model->id), array('class' => 'likeButton'));?></td>
        </tr>
    </table>

    <h3>Change status</h3>

    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'change-status',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('/internationalMail/internationalMail/changeStat'),
    ));
    ?>

    <?php echo $form->hiddenField($model,'id'); ?>

    <div class="form">

        <?php echo $form->errorSummary($model); ?>

        <table class="list hTop" style="width: 500px;">
            <tr>
                <td>Current status</td>
            </tr>
            <tr>
                <td>[<?php echo $model->stat;?>] <?php echo $model->stat->name;?></td>
            </tr>
        </table>


        <table class="list hTop" style="width: 500px;">
            <tr>
                <td>New status</td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->dropDownList($model, 'international_mail_stat_id', CHtml::listData(InternationalMailStat::model()->findAll(), 'id', 'name')); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo CHtml::submitButton(Yii::t('app', 'Save'),
                        array(
                            'confirm'=>'Na pewno?'
                        ));
                    $this->endWidget();
                    ?>
                </td>
            </tr>
        </table>

    </div><!-- form -->



    <h3>Assign outside ID</h3>

    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'assign-carrier',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('/internationalMail/internationalMail/assingCarrier'),
    ));
    ?>

    <?php echo $form->hiddenField($model,'id'); ?>

    <div class="form">

        <?php echo $form->errorSummary($model); ?>


        <table class="list hTop" style="width: 500px;">
            <tr>
                <td>ID:</td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->textField($model, 'outside_id'); ?>

                </td>
            </tr>
            <tr>
                <td>
                    <?php echo CHtml::checkBox('Other[actualizeStatusWithAssignCourrier]', true , array("id" => 'CB_saveUserContactBook') ); ?> Change status to "<?php echo InternationalMailStat::model()->findByPk(3)->name;?>

                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo CHtml::submitButton(Yii::t('app', 'Save'),
                        array(
                            'confirm'=>'Are you sure?'
                        ));
                    $this->endWidget();
                    ?>
                </td>
            </tr>
        </table>

    </div><!-- form -->

    <h3>History</h3>

    <?php
    $this->renderPartial('/_statHistory/_statHistory', array(
        'model' => $model->internationalMailStatHistories,
        'type' => 3,
    ));
    ?>
</div>
