
<h2><?php echo Yii::t('user', 'Twoje konto'); ?></h2>


<?php
$this->widget('FlashPrinter');
?>

<div class="form">
    <?=  TbHtml::errorSummary($model);?>
</div>

<?php

$tabs = [];
$tabs[] = array('label' => Yii::t('user', 'Dane konta'), 'id' => 'profile', 'active' => true, 'content' => $this->renderPartial('update_tabs/_main', ['model' => $model, 'modelCAE' => $modelCAE,  'modelCRRE' => $modelCRRE], true));
$tabs[] = array('label' => Yii::t('user', 'Blokuj powiadomienia'), 'id' => 'notifications', 'content' => $this->renderPartial('update_tabs/_blockNotifications', ['model' => $model], true));
$tabs[] = array('label' =>Yii::t('user', 'Rachunek bankowy'), 'id' => 'bank', 'content' => $this->renderPartial('update_tabs/_bankAccount', ['model' => $model], true));
$tabs[] = array('label' => Yii::t('user', 'Domyślny numer telefonu'), 'id' => 'tel', 'content' => $this->renderPartial('update_tabs/_defaultTelNumber', ['model' => $model], true));
$tabs[] = array('label' => Yii::t('user', 'Domyślne parametry paczek'), 'id' => 'dimensions', 'content' => $this->renderPartial('update_tabs/_defaultPackageDimensions', ['model' => $model], true));
$tabs[] = array('label' => Yii::t('user', 'Domyślne parametry listów'), 'id' => 'dimensionsPostal', 'content' => $this->renderPartial('update_tabs/_defaultPostalDimensions', ['model' => $model], true));
$tabs[] = array('label' => Yii::t('user', 'Pozostałe'), 'id' => 'other', 'content' => $this->renderPartial('update_tabs/_other', ['model' => $model], true));

if(Yii::app()->user->model->getCourierReturnAvailable())
    $tabs[] =  array('label' => Yii::t('user', 'Zwroty'), 'id' => 'returns', 'content' => $this->renderPartial('update_tabs/_returnPackages', ['model' => $model], true));

$this->widget('bootstrap.widgets.TbTabs', array(
    'tabs' => $tabs,
)); ?>
<script>
    // go to particular tab on reload
    $(function(){
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');

        $('.nav-tabs a').click(function (e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });
    });
</script>
