<?php
/* @var $from string */
/* @var $to string */
?>

<?php

Yii::app()->getModule('courier');

$today = ('Y-m-d');

if($from == '') $from = $today;
if($to == '') $to = $today;

$from .= ' 00:00:00';
$to .= ' 23:59:59';

$days = ceil((strtotime($to) - strtotime($from))/(60*60*24));

if($days <= 0)
{
    $from = $to = $today;
//    $from .= ' 00:00:00';
//    $to .= ' 23:59:59';
    $days = ceil((strtotime($to) - strtotime($from))/(60*60*24));
}

if($this->beginCache('STATISTIC_GENERAL_'.$from.'_'.$to, array('duration'=> 60))) {

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("order");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to'");
    $ordersToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("order");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND payment_type_id IS NOT NULL");
    $ordersPayment = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("order");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND payment_type_id = 1");
    $ordersPayment1Today = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("order");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND payment_type_id = 2");
    $ordersPayment2Today = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to'");
    $couriersToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND courier_type_internal_id != ''");
    $couriersIntToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND courier_type_external_id != ''");
    $couriersExtToday = $cmd->queryScalar();

//    $cmd = Yii::app()->db->createCommand();
//    $cmd->select("COUNT(*)");
//    $cmd->from("courier");
//    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND id IN (SELECT courier_id FROM courier_type_ooe)");
//    $couriersOoeToday = $cmd->queryScalar();
//
//    $cmd = Yii::app()->db->createCommand();
//    $cmd->select("COUNT(*)");
//    $cmd->from("courier");
//    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND id IN (SELECT courier_id FROM ".(new CourierTypeDomestic())->tableName().")");
//    $couriersDomesticToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND id IN (SELECT courier_id FROM ".(new CourierTypeU())->tableName().")");
    $couriersUToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND id IN (SELECT courier_id FROM ".(new CourierTypeExternalReturn())->tableName().")");
    $couriersExtRetToday = $cmd->queryScalar();


    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND id IN (SELECT courier_id FROM ".(new CourierTypeReturn())->tableName().")");
    $couriersRetToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("special_transport");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to'");
    $specialTransportToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("palette");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to'");
    $paletteToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("postal");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to'");
    $postalToday = $cmd->queryScalar();


    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("palette");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to'");
    $palettesToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("special_transport");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to'");
    $specialTransportsToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("user");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to'");
    $usersToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("online_payments");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND err_p24 IS NULL");
    $p24SuccessToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("online_payments");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND err_p24 IS NOT NULL");
    $p24FailToday = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier_label_new");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND stat = ".CourierLabelNew::STAT_SUCCESS);
    $courierLabelsSuccess = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier_label_new");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND stat = ".CourierLabelNew::STAT_NEW);
    $courierLabelsWaiting = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier_label_new");
    $cmd->where("(date_entered) BETWEEN '$from' AND '$to' AND stat = ".CourierLabelNew::STAT_FAILED);
    $courierLabelsFailed = $cmd->queryScalar();

    $couriersCreated = $couriersToday;

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_activated) BETWEEN '$from' AND '$to'");
    $couriersActivated = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_delivered) BETWEEN '$from' AND '$to'");
    $couriersDelivered = $cmd->queryScalar();

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*)");
    $cmd->from("courier");
    $cmd->where("(date_attempted_delivery) BETWEEN '$from' AND '$to'");
    $couriersAttemptedDelivery = $cmd->queryScalar();


    ?>
    <table class="hLeft table table-condensed" style="width: 550px; margin: 0 auto;">
        <tr>
            <td>From</td>
            <td><?php echo $from;?></td>
        </tr>
        <tr>
            <td>To</td>
            <td><?php echo $to;?></td>
        </tr>
        <tr>
            <td>Days</td>
            <td><?php echo $days;?></td>
        </tr>
    </table>
    <br/>
    <h4 class="text-center">Services</h4>
    <table class="hLeft table table-condensed" style="width: 550px; margin: 0 auto;">
        <tr>
            <td style="text-align: left;">Courier</td>
            <td><?php echo $couriersToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- Internal</td>
            <td><?php echo $couriersIntToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- External</td>
            <td><?php echo $couriersExtToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- U</td>
            <td><?php echo $couriersUToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- External return</td>
            <td><?php echo $couriersExtRetToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- Internal return</td>
            <td><?php echo $couriersRetToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left;">SpecialTransport</td>
            <td><?php echo $specialTransportToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left;">Palette</td>
            <td><?php echo $paletteToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left;">Postal</td>
            <td><?php echo $postalToday;?></td>
        </tr>
    </table>


    <h4 class="text-center">Courier Actions</h4>
    <table class="hLeft table table-condensed" style="width: 550px; margin: 0 auto;">
        <tr>
            <td style="text-align: left; text-indent: 15px;">- created</td>
            <td><?php echo $couriersCreated;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- activated</td>
            <td><?php echo $couriersActivated;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- attempted delivery</td>
            <td><?php echo $couriersAttemptedDelivery;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- delivered</td>
            <td><?php echo $couriersDelivered;?></td>
        </tr>
    </table>

    <h4 class="text-center">Orders</h4>
    <table class="hLeft table table-condensed" style="width: 550px; margin: 0 auto;">
        <tr>
            <td style="text-align: left;">Orders</td>
            <td><?php echo $ordersToday;?></td>
        </tr>
    </table>
    <br/>
    <h4 class="text-center">Courier label orders</h4>
    <table class="hLeft table table-condensed" style="width: 550px; margin: 0 auto;">
        <tr>
            <td style="text-align: left; text-indent: 15px;">Waiting</td>
            <td><?php echo $courierLabelsWaiting;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">Success</td>
            <td><?php echo $courierLabelsSuccess;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">Failed</td>
            <td><?php echo $courierLabelsFailed;?></td>
        </tr>
    </table>
    <br/>
    <h4 class="text-center">Payments</h4>
    <table class="hLeft table table-condensed" style="width: 550px; margin: 0 auto;">
        <tr>
            <td style="text-align: left;">Payments</td>
            <td><?php echo $ordersPayment;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- online</td>
            <td><?php echo $ordersPayment1Today;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- on system account</td>
            <td><?php echo $ordersPayment2Today;?></td>
        </tr>
    </table>
    <br/>
    <h4 class="text-center">Users</h4>
    <table class="hLeft table table-condensed" style="width: 550px; margin: 0 auto;">
        <tr>
            <td style="text-align: left;">New users</td>
            <td><?php echo $usersToday;?></td>
        </tr>
    </table>
    <br/>
    <h4 class="text-center">Platnosci 24</h4>
    <table class="hLeft table table-condensed" style="width: 550px; margin: 0 auto;">
        <tr>
            <td style="text-align: left;">Platnosci24 incomes</td>
            <td><?php echo ($p24FailToday + $p24SuccessToday);?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- accepted</td>
            <td><?php echo $p24SuccessToday;?></td>
        </tr>
        <tr>
            <td style="text-align: left; text-indent: 15px;">- rejected</td>
            <td><?php echo $p24FailToday;?></td>
        </tr>

    </table>

    <?php
$this->endCache(); }
?>