<?php

Yii::import('application.modules.courier.models._base.BaseCourierDomesticOperator');

class CourierDomesticOperator extends BaseCourierDomesticOperator
{
	const OPERATOR_INPOST = 1;
	const OPERATOR_SAFEPAK = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_SAFEPAK;
	const OPERATOR_GLS = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_GLS;
	const OPERATOR_UPS = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_UPS;
	const OPERATOR_UPS_SAVER = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_UPS_SAVER;

	const OPERATOR_UPS_EXPRESS = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_UPS_EXPRESS;

	const OPERATOR_RUCH = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_RUCH;


	const OPERATOR_DHL = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_DHL;

    const OPERATOR_DHL_PALETA = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_DHL_PALETA;

    const OPERATOR_DHL_F = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_DHL_F;
    const OPERATOR_DHL_F_PALETA = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_DHL_F_PALETA;

    const OPERATOR_UPS_M_EXPRESS = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_UPS_M_EXPRESS;

    const OPERATOR_DHL_F_DLUZYCE = GLOBAL_CONFIG::COURIER_DOMESTIC_OPERATOR_DHL_F_DLUZYCE;



//////////////////////////////////////////////////////
	const BROKER_INPOST = 1;
	const BROKER_SAFEPAK = 2;
	const BROKER_GLS = 3;
	const BROKER_UPS = 4;
	const BROKER_RUCH = 5;
	const BROKER_DHL = 6;



	protected static function blockedToEdit()
	{
		$data = [self::OPERATOR_INPOST, self::OPERATOR_UPS, self::OPERATOR_UPS_SAVER, self::OPERATOR_UPS_EXPRESS, self::OPERATOR_RUCH, self::OPERATOR_DHL, self::OPERATOR_DHL_PALETA, self::OPERATOR_GLS, self::OPERATOR_UPS_M_EXPRESS, self::OPERATOR_DHL_F_PALETA, self::OPERATOR_DHL_F, self::OPERATOR_DHL_F_DLUZYCE];
		return $data;
	}

	public static function isItDhlOperator($operator_id)
	{
		return in_array($operator_id, [
			self::OPERATOR_DHL_F_PALETA,
			self::OPERATOR_DHL_F,
			self::OPERATOR_DHL,
			self::OPERATOR_DHL_PALETA,
            self::OPERATOR_DHL_F_DLUZYCE,
		]);
	}

	/**
	 * Returns array of operators accepting multipackage orders
	 * @return array
	 */
	public static function getMultiOperators()
	{
		return [
			self::OPERATOR_INPOST,
			self::OPERATOR_SAFEPAK,
			self::OPERATOR_GLS,
			self::OPERATOR_UPS,
			self::OPERATOR_UPS_SAVER,
			self::OPERATOR_UPS_EXPRESS,
			self::OPERATOR_DHL,
			self::OPERATOR_DHL_PALETA,
			self::OPERATOR_DHL_F,
			self::OPERATOR_DHL_F_PALETA,
			self::OPERATOR_UPS_M_EXPRESS,
            self::OPERATOR_DHL_F_DLUZYCE
		];
	}

	public static function getOperatorById($operator_id)
	{
		if($operator_id == self::OPERATOR_INPOST)
			return self::OPERATOR_INPOST;
		else {
			$model = self::model()->findByPk($operator_id);
			if($model->broker == self::BROKER_SAFEPAK)
			{
				return self::OPERATOR_SAFEPAK;
			}
		}
	}

	public static function getBrokerList($onlyAvailableToAdd = false)
	{
		$data = [];
		$data[self::BROKER_SAFEPAK] = 'Safepak';

		if(!$onlyAvailableToAdd)
			$data[self::BROKER_INPOST] = 'Inpost';


		if(!$onlyAvailableToAdd)
			$data[self::BROKER_GLS] = 'Gls';

		if(!$onlyAvailableToAdd)
			$data[self::BROKER_UPS] = 'UPS';

		if(!$onlyAvailableToAdd)
			$data[self::BROKER_RUCH] = 'RUCH';

		if(!$onlyAvailableToAdd)
			$data[self::BROKER_DHL] = 'DHL';

		return $data;
	}

	public function getEditable()
	{
		if(!in_array($this->id, self::blockedToEdit()))
			return true;
		else
			return false;

	}

	public function beforeValidate()
	{
		if($this->broker == self::BROKER_SAFEPAK)
		{
			if($this->_safepak_service == '' OR !is_numeric($this->_safepak_service))
			{
				$this->addError('_safepak_service', 'Podaj poprawny ID usługi Safepak!');
			}
		}

		return parent::beforeValidate();
	}

	public function rules() {
		return array(
			array('date_entered', 'default',
				'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
			array('date_updated', 'default',
				'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
			array('stat', 'default',
				'value'=>S_Status::ACTIVE, 'on'=>'insert'),

			array('name, stat', 'required'),
			array('id, stat', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('date_updated', 'safe'),
			array('broker', 'numerical'),
			array('date_updated, params, broker', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name, date_entered, date_updated, stat, broker', 'safe', 'on'=>'search'),
		);
	}

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function getActiveOperators()
	{

		$models = self::model()->findAllByAttributes(['stat' => S_Status::ACTIVE]);

		// MOD 22.09.2016 - only some operators are availabe for RUCH
		if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH)
		{
			$allowed = [
				self::OPERATOR_UPS_M_EXPRESS,
				self::OPERATOR_UPS,
				self::OPERATOR_DHL_F,
				self::OPERATOR_DHL_F_PALETA,
                self::OPERATOR_DHL_F_DLUZYCE
				];

			$temp = [];
			foreach($models AS $item)
			{
				if(in_array($item->id, $allowed))
					$temp[] = $item;
			}
			$models = $temp;

		}

		return $models;

	}

	public function toggleStat()
	{
		$this->stat = abs($this->stat - 1);

		return $this->save(false,array('stat'));
	}

	public function wrapParams($safepak_service)
	{
		$data = [
			'safepak_service' => $safepak_service,
		];

		$data = serialize($data);
		$data = base64_encode($data);

		return $data;
	}

	protected function unwrapParams()
	{
		$data = $this->params;
		$data = base64_decode($data);
		$data = unserialize($data);

		return $data;
	}

	public function get_Safepak_service()
	{
		return $this->unwrapParams()['safepak_service'];
	}


	public function saveWithTr($ownTransaction = true)
	{
		if($ownTransaction)
			$transaction = Yii::app()->db->beginTransaction();

		$errors = false;

		if(!$this->save())
			$errors = true;


		if(!$errors)
			foreach($this->courierDomesticOperatorTrs AS $item)
			{
				$item->courier_domestic_operator_id = $this->id;

				if(!$item->save()) {
					$errors = true;
				}
			}

		if($errors)
		{
			if($ownTransaction)
				$transaction->rollback();

			return false;
		} else {
			if($ownTransaction)
				$transaction->commit();

			return true;
		}
	}

	public function delete()
	{
		if(!$this->getEditable())
			return false;

		return parent::delete();
	}

}