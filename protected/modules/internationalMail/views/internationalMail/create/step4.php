<?php

/* @var $model InternationalMail */
/* @var $this InternationalMailController */
/* @var $modelInternationalMailAdditionList InternationalMailAdditionList[] */
/* @var $internationalMailAddition InternationalMailAddition[] */
/* @var $form CActiveForm */


?>

<div class="form">

    <h2><?php echo Yii::t('internationalMail','International Mail'); ?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '4/6')); ?> | <?php echo Yii::t('internationalMail','Dodatki');?></h2>

    <?php

    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'international-mail-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));

    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <table class="list hTop" style="width: 100%;">
        <tr>
            <td style="width: 50px;"><?php echo Yii::t('internationalMail','Wybierz');?></td>
            <td style="width: 60%;"><?php echo Yii::t('internationalMail','Dodatek');?></td>
            <td style="text-align: center; width: 150px;"><?php echo Yii::t('internationalMail','Cena netto/brutto');?></td>
        </tr>
        <?php

        foreach($modelInternationalMailAdditionList AS $item):
            ?>
            <tr class="addition_item">
                <td class="with-checkbox">
                    <?php echo Chtml::checkBox('InternationalMailAddition[]', in_array($item->id,$internationalMailAddition), array('value' => $item->id)); ?>
                </td>
                <td>
                    <label style="font-weight: normal; text-align: left;"><h3><?php echo $item->internationalMailAdditionListTr->title;?></h3>
                        <p><?php echo $item->internationalMailAdditionListTr->description; ?></p></label>
                </td>
                <td style="text-align: center;">
                    <?php
                    echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice(), true)).' '.Yii::app()->PriceManager->getCurrencySymbol().'/'.S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($item->getPrice())).' '.Yii::app()->PriceManager->getCurrencySymbol();
                    ?>
                </td>
            </tr>

        <?php
        endforeach;

        ?>
    </table>

    <?php
    Yii::app()->clientScript->registerCoreScript('jquery');
    echo CHtml::script('
 $(document).ready(function(){
        $(".addition_item").children().not(".with-checkbox").css("cursor", "pointer");
        $(".addition_item").children().not(".with-checkbox").click(function(){
             var $checkBox = $(this).parent().find("input[type=checkbox]");
            $checkBox.prop("checked", !$checkBox.prop("checked"));
        });

    });
    ');

    ?>

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step3', 'class' => 'btn btn-small'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'step5', 'class' => 'btn btn-primary'));
        $this->endWidget();
        ?>
    </div>

</div><!-- form -->