<?php

Yii::import('application.modules.courier.models._base.BaseCourierPrice');

class CourierPrice extends BaseCourierPrice
{

    public $_maxWeightErrorContainer;

    const MAX_WEIGHT = 9999;
    const MODE_PICKUP = 'pickup';
    const MODE_DELIVERY = 'delivery';

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('creation_admin_id', 'default',
                'value'=>Yii::app()->user->id, 'setOnEmpty' => false, 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on'=>'update'),
            array('update_admin_id', 'default',
                'value'=>Yii::app()->user->id, 'setOnEmpty' => false, 'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),

            array('stat', 'numerical', 'integerOnly'=>true),
            array('creation_admin_id, update_admin_id', 'length', 'max'=>10),
            array('date_updated', 'safe'),
            array('date_updated, update_admin_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, creation_admin_id, update_admin_id, stat', 'safe', 'on'=>'search'),
        );
    }

    protected static $_maxWeight = self::MAX_WEIGHT;
    protected static $_maxPrice = 9999;

    public function getCourierGroupId()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier_country_group');
        $cmd->from('courier_pickup_price_has_group');
        $cmd->andWhere('courier_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            return $result;

        }

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('courier_country_group');
        $cmd->from('courier_delivery_price_has_hub');
        $cmd->where('courier_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        return $result;
    }

    public function getUserGroupId()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('user_group_id');
        $cmd->from('courier_pickup_price_has_group');
        $cmd->andWhere('courier_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            return $result;

        }

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('user_group_id');
        $cmd->from('courier_delivery_price_has_hub');
        $cmd->where('courier_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        return $result;
    }

    public static function loadOrCreateDeliveryPrice($mode, $courier_country_group, $courier_hub, $user_group = null)
    {

        if($mode == self::MODE_PICKUP)
        {
            if($user_group == NULL)
                $model = CourierPrice::model()->with('courierPickupPriceHasGroups')->find('courierPickupPriceHasGroups.courier_country_group=:id AND user_group_id IS NULL', array(':id' => $courier_country_group));
            else
                $model = CourierPrice::model()->with('courierPickupPriceHasGroups')->find('courierPickupPriceHasGroups.courier_country_group=:id AND user_group_id = :group', array(':id' => $courier_country_group, ':group' => $user_group));
        }
        else if($mode == self::MODE_DELIVERY)
        {
            if($user_group == NULL)
                $model = CourierPrice::model()->with('courierDeliveryPriceHasHubs')->find('courierDeliveryPriceHasHubs.courier_country_group=:id AND user_group_id IS NULL AND courierDeliveryPriceHasHubs.courier_pickup_hub=:hub', array(':id' => $courier_country_group, ':hub' => $courier_hub));
            else
                $model = CourierPrice::model()->with('courierDeliveryPriceHasHubs')->find('courierDeliveryPriceHasHubs.courier_country_group=:id AND user_group_id = :group AND courierDeliveryPriceHasHubs.courier_pickup_hub=:hub', array(':id' => $courier_country_group, ':hub' => $courier_hub, ':group' => $user_group));

        }
        else
            throw new CHttpException(403);

        if($model == NULL)
            $model = new CourierPrice();


        return $model;
    }

    public function deleteWithRelated()
    {
        $success = false;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id');
        $cmd->from('courier_pickup_price_has_group');
        $cmd->andWhere('courier_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('courier_price_value','courier_price_id = :id', array(':id' => $this->id));
            $cmd->delete('courier_pickup_price_has_group','id = :id', array(':id' => $result));
            $cmd->delete('courier_price','id = :id', array(':id' => $this->id));

            $success = true;
        }

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('id');
        $cmd->from('courier_delivery_price_has_hub');
        $cmd->where('courier_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            $cmd = Yii::app()->db->createCommand();

            $cmd->delete('courier_price_value','courier_price_id = :id', array(':id' => $this->id));
            $cmd->delete('courier_delivery_price_has_hub','id = :id', array(':id' => $result));
            $cmd->delete('courier_price','id = :id', array(':id' => $this->id));

            $success = true;
        }

        return $success;
    }

    public static function loadOrCreatePickupPrice($courier_country_group, $user_group = null)
    {

        if($user_group == NULL)
            $model = CourierPrice::model()->with('courierPickupPriceHasGroups')->find('courierPickupPriceHasGroups.courier_country_group=:id AND user_group_id IS NULL', array(':id' => $courier_country_group));
        else
            $model = CourierPrice::model()->with('courierPickupPriceHasGroups')->find('courierPickupPriceHasGroups.courier_country_group=:id AND user_group_id = :group', array(':id' => $courier_country_group, ':group' => $user_group));


        if($model == NULL)
        {
            $errors = false;
            $transaction = Yii::app()->db->beginTransaction();


            $model = new CourierPrice();


            /* if(!$model2->save())
                 $errors = true;*/

            if($errors)
            {
                $transaction->rollback();
                throw new CHttpException(403);
            }
            else
            {
                $transaction->commit();
            }
        }

        return $model;

    }

    public function loadValueItems()
    {
        $courierPriceValues = [];

        if($this->id != NULL)
            $courierPriceValues = CourierPriceValue::model()->findAll(array('condition' => 'courier_price_id = :courier_price_id', 'params' => array(':courier_price_id' => $this->id), 'order' => 'weight_top_limit ASC'));

        if(!S_Useful::sizeof($courierPriceValues))
            $courierPriceValues[0] = new CourierPriceValue();

        return $courierPriceValues;
    }

    public static function getMaxWeight()
    {
        return self::$_maxWeight;
    }

    public static function getMaxPrice()
    {
        return self::$_maxPrice;
    }

    public function generateDefaultPriceValue()
    {
        $maxPriceModel = new CourierPriceValue();
        $maxPriceModel->courier_price_id = $this->id;
        $maxPriceModel->weight_top_limit = self::getMaxWeight();
        $maxPriceModel->pricePerItem = new Price();
        $maxPriceModel->pricePerWeight = new Price();


        return $maxPriceModel;
    }

    /**
     * Returns array with price components of pickup price for given user group
     *
     * @param $weight float
     * @param $country_from int
     * @param $user_group_id int
     * @return array
     */
    protected static function getPickupPriceDataForUserGroup($currency, $weight, $country_from, $user_group_id)
    {

        if($user_group_id == NULL)
            return false;

        $country_group = CourierCountryGroup::getGroupForCountryId($country_from);

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('pricePerItem.value AS price_per_item, pricePerWeight.value AS price_per_weight');
        $cmd->from('courier_price_value');
        $cmd->join('courier_price', 'courier_price_value.courier_price_id = courier_price.id');
        $cmd->join('courier_pickup_price_has_group', 'courier_pickup_price_has_group.courier_price_item = courier_price.id');
        $cmd->join('courier_country_group', 'courier_pickup_price_has_group.courier_country_group = courier_country_group.id');
        $cmd->join('price_value AS pricePerItem', 'courier_price_value.price_per_item = pricePerItem.price_id AND pricePerItem.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->join('price_value AS pricePerWeight', 'courier_price_value.price_per_weight = pricePerWeight.price_id AND pricePerWeight.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('weight_top_limit >= :weight', array(':weight' => $weight));
        $cmd->andWhere('courier_country_group.id = :country_group', array(':country_group' => $country_group));
        $cmd->andWhere('courier_pickup_price_has_group.user_group_id = :user_id', array(':user_id' => $user_group_id));
        $cmd->order('weight_top_limit ASC');
        $cmd->limit('1');
        $result = $cmd->queryRow();

        return $result;

    }

    /**
     * Returns array with price components of pickup price
     *
     * @param $weight float
     * @param $country_from int
     * @param $user_group_id int
     * @return array
     */
    protected static function getPickupPriceData($currency, $weight, $country_from, $user_group_id)
    {


        $country_group = CourierCountryGroup::getGroupForCountryId($country_from);

        $priceForUser = self::getPickupPriceDataForUserGroup($currency, $weight, $country_from, $user_group_id); // find if user has his own price

        if($priceForUser)
            return $priceForUser;

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('pricePerItem.value AS price_per_item, pricePerWeight.value AS price_per_weight');
        $cmd->from('courier_price_value');
        $cmd->join('courier_price', 'courier_price_value.courier_price_id = courier_price.id');
        $cmd->join('courier_pickup_price_has_group', 'courier_pickup_price_has_group.courier_price_item = courier_price.id');
        $cmd->join('courier_country_group', 'courier_pickup_price_has_group.courier_country_group = courier_country_group.id');
        $cmd->join('price_value AS pricePerItem', 'courier_price_value.price_per_item = pricePerItem.price_id AND pricePerItem.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->join('price_value AS pricePerWeight', 'courier_price_value.price_per_weight = pricePerWeight.price_id AND pricePerWeight.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('weight_top_limit >= :weight', array(':weight' => $weight));
        $cmd->andWhere('courier_country_group.id = :country_group', array(':country_group' => $country_group));
        $cmd->andWhere('courier_pickup_price_has_group.user_group_id IS NULL');
        $cmd->order('weight_top_limit ASC');
        $cmd->limit('1');
        $result = $cmd->queryRow();
        return $result;
    }

    /**
     * Returns array with price components of delivery price for given user group
     *
     * @param $weight float
     * @param $country_from int
     * @param $country_to int
     * @param $user_group_id int
     * @return array
     */
    protected static function getDeliveryPriceDataForUserGroup($currency, $weight, $country_from, $country_to, $user_group_id)
    {
        if($user_group_id == NULL)
            return false;

        $country_to_group = CourierCountryGroup::getGroupForCountryId($country_to);
        $country_from_group = CourierCountryGroup::getGroupForCountryId($country_from);
        $pickup_hub = CourierCountryGroup::getPickupHub($country_from_group, $weight, $user_group_id);


        $cmd = Yii::app()->db->createCommand();
        $cmd->select('pricePerItem.value AS price_per_item, pricePerWeight.value AS price_per_weight');
        $cmd->from('courier_price_value');
        $cmd->join('courier_price', 'courier_price_value.courier_price_id = courier_price.id');
        $cmd->join('courier_delivery_price_has_hub', 'courier_delivery_price_has_hub.courier_price_item = courier_price.id');
        $cmd->join('courier_country_group', 'courier_delivery_price_has_hub.courier_country_group = courier_country_group.id');
        $cmd->join('price_value AS pricePerItem', 'courier_price_value.price_per_item = pricePerItem.price_id AND pricePerItem.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->join('price_value AS pricePerWeight', 'courier_price_value.price_per_weight = pricePerWeight.price_id AND pricePerWeight.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('weight_top_limit >= :weight', array(':weight' => $weight));
        $cmd->andWhere('courier_country_group.id = :country_group', array(':country_group' => $country_to_group));
        $cmd->andWhere('courier_delivery_price_has_hub.courier_pickup_hub = :hub', array(':hub' => $pickup_hub));
        $cmd->andWhere('courier_delivery_price_has_hub.user_group_id = :user_id', array(':user_id' => $user_group_id));
        $cmd->order('weight_top_limit ASC');
        $cmd->limit('1');
        $result = $cmd->queryRow();

        return $result;

    }

    /**
     * Returns array with price components of delivery price
     *
     * @param $weight float
     * @param $country_from int
     * @param $country_to int
     * @param $user_group_id int
     * @return array
     */
    protected static function getDeliveryPriceData($currency, $weight, $country_from, $country_to, $user_group_id)
    {
        $country_to_group = CourierCountryGroup::getGroupForCountryId($country_to);
        $country_from_group = CourierCountryGroup::getGroupForCountryId($country_from);
        $pickup_hub = CourierCountryGroup::getPickupHub($country_from_group, $weight, $user_group_id);
        $priceForUser = self::getDeliveryPriceDataForUserGroup($currency, $weight, $country_from, $country_to, $user_group_id); // find if user has his own price

        if($priceForUser)
            return $priceForUser;

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('pricePerItem.value AS price_per_item, pricePerWeight.value AS price_per_weight');
        $cmd->from('courier_price_value');
        $cmd->join('courier_price', 'courier_price_value.courier_price_id = courier_price.id');
        $cmd->join('courier_delivery_price_has_hub', 'courier_delivery_price_has_hub.courier_price_item = courier_price.id');
        $cmd->join('courier_country_group', 'courier_delivery_price_has_hub.courier_country_group = courier_country_group.id');
        $cmd->join('price_value AS pricePerItem', 'courier_price_value.price_per_item = pricePerItem.price_id AND pricePerItem.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->join('price_value AS pricePerWeight', 'courier_price_value.price_per_weight = pricePerWeight.price_id AND pricePerWeight.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('weight_top_limit >= :weight', array(':weight' => $weight));
        $cmd->andWhere('courier_country_group.id = :country_group', array(':country_group' => $country_to_group));
        $cmd->andWhere('courier_delivery_price_has_hub.courier_pickup_hub = :hub', array(':hub' => $pickup_hub));
        $cmd->andWhere('courier_delivery_price_has_hub.user_group_id IS NULL');
        $cmd->order('weight_top_limit ASC');
        $cmd->limit('1');
        $result = $cmd->queryRow();

        return $result;
    }

    /**
     * Returns courier pickup price based on given arguments.
     *
     * @param $weight float Package weight in defined unit
     * @param $country_from int Sender country ID
     * @param $country_to int Receiver country ID
     * @param null $user_group_id int User group ID
     * @return float Returns price in defined unit
     */
    public static function calculatePickupPrice($currency, $weight, $country_from, $country_to, $user_group_id = null)
    {

        //  MODIFICATION 17.10.2016 for packages PL -> PL
        if($country_from == CountryList::COUNTRY_PL && $country_to == CountryList::COUNTRY_PL)
            return 0;

        $priceData = self::getPickupPriceData($currency, $weight, $country_from, $user_group_id);
        $price_per_item = $priceData['price_per_item'];
        $price_per_weight = $priceData['price_per_weight'];

        $price = $price_per_item + round($price_per_weight * $weight,2);

        return $price;

    }

//    public static function calculatePickupPriceWithType($withPickup, $currency, $weight, $country_from, $country_to, $user_group_id = null)
//    {
//        $price = 0;
//
//        if($withPickup == CourierTypeInternal::WITH_PICKUP_RUCH)
//            $price = CourierCustomPickupPrice::getPriceByWeight($weight, CourierCustomPickupPrice::TYPE_RUCH, $user_group_id, $currency);
//        else if($withPickup == CourierTypeInternal::WITH_PICKUP_RM)
//            $price = CourierCustomPickupPrice::getPriceByWeight(NULL, CourierCustomPickupPrice::TYPE_RM, $user_group_id, $currency);
//        else if($withPickup == CourierTypeInternal::WITH_PICKUP_REGULAR)
//            $price = self::calculatePickupPrice($currency, $weight, $country_from, $country_to, $user_group_id);
//
//        return $price;
//    }

    /**
     * Returns courier delivery price based on given arguments.
     *
     * @param $with_pickup int Pickup mode
     * @param $weight float Package weight in defined unit
     * @param $country_from int Sender country ID
     * @param $country_to int Receiver country ID
     * @param null $user_group_id int User group ID
     * @return float Returns price in defined unit
     */
    public static function calculateDeliveryPrice($with_pickup, $currency, $weight, $country_from, $country_to, $user_group_id = null)
    {
//      UPDATE 05.12.2016 - cancelled 09.12.2016
//        if($country_from != CountryList::COUNTRY_PL && in_array($with_pickup,[CourierTypeInternal::WITH_PICKUP_CONTRACT, CourierTypeInternal::WITH_PICKUP_NONE]))
//            $priceData = self::getPickupPriceData($currency, $weight, $country_from, $user_group_id);
//        else
        $priceData = self::getDeliveryPriceData($currency, $weight, $country_from, $country_to, $user_group_id);


        $price_per_item = $priceData['price_per_item'];
        $price_per_weight = $priceData['price_per_weight'];

        $price = $price_per_item + round( (double) $price_per_weight * (double) $weight,2);

        return $price;
    }

//    public static function calculateTotalPrice($currency, $weight, $country_from, $country_to, $user_group_id = NULL, $with_pickup = CourierTypeInternal::WITH_PICKUP_REGULAR)
//    {
//
//        $price = 0;
//        $price += self::calculatePickupPriceWithType($with_pickup, $currency, $weight, $country_from, $country_to, $user_group_id);
//        $price += self::calculateDeliveryPrice($with_pickup, $currency, $weight, $country_from, $country_to, $user_group_id);
//
//        return $price;
//    }

    public function toggleStat()
    {
        if(!S_Useful::sizeof($this->courierPriceValues))
            return false;

        $this->stat = abs($this->stat - 1);
        return $this->save();
    }


}