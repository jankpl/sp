<?php

class CourierTypeExternalReturnController extends Controller {

    public function filters(){
        return array(
            'accessControl',
//            'backend.filters.GridViewHandler' //path to GridViewHandler.php class
        );
    }

    public function accessRules() {
        return array(
//            array('allow',
//                'users'=>array('@'),
//
//            ),
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => ['create'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionCreate()
    {

        $model = new CourierTypeExternalReturn();
        $model->courier = new Courier_CourierTypeExternalReturn;
        $model->courierLabelNew = new CourierLabelNew();
        $model->courier->senderAddressData = new CourierTypeExternalReturn_AddressData();
        $model->courier->receiverAddressData = new CourierTypeExternalReturn_AddressData();

        if (isset($_POST['Courier_CourierTypeExternalReturn'])) {

            $model->courier->setAttributes($_POST['Courier_CourierTypeExternalReturn']);
            $model->courier->senderAddressData->setAttributes($_POST['CourierTypeExternalReturn_AddressData']['sender']);
            $model->courier->receiverAddressData->setAttributes($_POST['CourierTypeExternalReturn_AddressData']['receiver']);
            $model->courierLabelNew->track_id = ($_POST['CourierLabelNew']['track_id']);
            $model->courierLabelNew->_ext_operator_name = ($_POST['CourierLabelNew']['_ext_operator_name']);

            $errors = false;

            if (!$model->validate())
                $errors = true;

            if (!$model->courier->validate())
                $errors = true;

            if (!$model->courier->senderAddressData->validate())
                $errors = true;

            if (!$model->courier->receiverAddressData->validate())
                $errors = true;

            $model->courier->source = Courier::SOURCE_ADMIN;

            if(!$errors && $model->savePackage())
            {
                $this->redirect(array('/courier/courier/view',
                    'id' => $model->courier->id
                ));
            }

        }

        $this->render('create', array(
            'model' => $model
        ));
    }



}