<?php
/* @var $this UserDocumentsController */
/* @var $model UserDocuments */

$this->menu=array(
    array('label'=>'Create UserDocuments', 'url'=>array('create')),
);

?>

<h1>Manage User Documents</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'user-message-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        [
            'name' => 'id',
            'htmlOptions' => [
                'width' => '30',
            ],
        ],
        'date_entered',
       [
            'name' => 'user_id',
            'value' => '$data->user->login',
            'filter' => CHtml::listData(User::model()->findAll(),'id', 'login'),
       ],
       [
            'name' => 'admin_id',
            'value' => '$data->admin->login',
            'filter' => CHtml::listData(Admin::model()->findAll(),'id', 'login'),
        ],
        [
            'name' => 'stat',
            'value' => '$data->getStatName()',
            'filter' => [0 => 'Inactive', 1 => 'Active'],
        ],
        [
            'name' => 'customer_visible',
            'value' => '$data->getCustomerVisibilityName()',
            'filter' => [0 => 'Inactive', 1 => 'Active'],
        ],
        [
            'name' => 'type',
            'filter' => UserDocuments::getTypeList(),
            'value' => '$data->getTypeName()',
        ],
        'file_name',
        'desc',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
