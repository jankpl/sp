<?php

class _CourierStatGridView extends CourierStat
{
    public $_couriersStat;
    public $_map_id;


    public function rules() {

        $array = array(
            array('_couriersStat, _map_id
                ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }


    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;


        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('t.stat', $this->stat);
        $criteria->compare('editable', $this->editable);
        $criteria->compare('external_service_id', $this->external_service_id);
        $criteria->compare('notify', $this->notify);
        $criteria->compare('statMapMapping.map_id', $this->_map_id);


        $courier_table = Courier::model()->tableName();
        $courier_count_sql = "(select count(*) from $courier_table cs where cs.courier_stat_id = t.id)";

        $criteria->select = array(
            '*',
            $courier_count_sql . " as courier_count",
        );


        if($this->_couriersStat)
        {
            $criteria->compare($courier_count_sql, $this->_couriersStat);
        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id ASC';
        $sort->attributes = array(
            '_couriersStat' => array(
                'asc' => 'courier_count ASC',
                'desc' => 'courier_count DESC',
            ),
            '_map_id' => array(
                'asc' => 'statMapMapping.map_id ASC',
                'desc' => 'statMapMapping.map_id DESC',
            ),
            '*', // add all of the other columns as sortable
        );


        return new CActiveDataProvider($this->with('couriersSTAT')->with('statMapMapping'), array(
            'criteria' => $this->searchCriteria(),
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

    }

}