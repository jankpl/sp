<?php

class CourierCountryGroupController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => ['view', 'index','admin','update', 'listAllGroups', 'tempSwitch', 'tempDelete', 'tempAdd'],
                'verbs' => ['get'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

protected static function _emailReportAddresses()
{
    return  [
        'jankopec@swiatprzesylek.pl',
//        'piotrkocon@swiatprzesylek.pl',
//        'maciejszostak@swiatprzesylek.pl',
//        'michalwojciechowski@swiatprzesylek.pl',
    ];
}

    public function actionView($id) {

        $model = CourierCountryGroup::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $this->render('view',array(
            'model' => $model,
        ));

    }

    public function actionCreate() {
        $model = new CourierCountryGroup;

        $this->performAjaxValidation($model, 'courier-country-group-form');

        if (isset($_POST['CourierCountryGroup'])) {
            $model->setAttributes($_POST['CourierCountryGroup']);

            $countryList = $_POST['countryList'];
            if(!is_array($countryList))
                $countryList = [];

            if ($model->saveWithCountries($countryList)) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id, $user_group = NULL) {


        if(YII_LOCAL)
            $notificationReceivers = [
                'jtk@cskstudio.pl',
            ];
        else
            $notificationReceivers = [
                'jankopec@swiatprzesylek.pl',
                'piotrkocon@swiatprzesylek.pl',
                'maciejszostak@swiatprzesylek.pl',
                'michalwojciechowski@swiatprzesylek.pl',
            ];

        if($user_group != NULL)
        {
            $checkUserModel = UserGroup::model()->findByPk($user_group);
            if($checkUserModel == NULL)
                throw new CHttpException(404);
        } else {
            $listUserGroups = [];
            $cmd = Yii::app()->db->createCommand();
            foreach(UserGroup::getListForThisAdmin() AS $item)
            {
                $found = $cmd->select('COUNT(*)')->from((new CourierCountryGroup())->tableName())->where('user_group_id = :group AND parent_courier_country_group_id = :parent', [':group' => $item->id, ':parent' => $id])->queryScalar();
                $listUserGroups[$item->id] = $item->getNameWithAdmin().($found ? ' (yes)' : '');
            }
        }

        /* @var $model CourierCountryGroup */
        $model = CourierCountryGroup::loadModelWithPickupAvailableSet($id, $user_group);

        if($user_group)
            $model->scenario = CourierCountryGroup::SCENARIO_CUSTOM_GROUP;

        $modelDeliveryNew = new CourierCountryGroupOperator();
        $modelDeliveryNew->type = CourierCountryGroupOperator::TYPE_DELIVERY;
        $modelDeliveryNew->scenario = CourierCountryGroupOperator::SCENARIO_DELIVERY;

        $modelPickupNew = new CourierCountryGroupOperator();
        $modelPickupNew->type = CourierCountryGroupOperator::TYPE_PICKUP;

        if($model === NULL)
            throw new CHttpException(404);
        else if($model->parent_courier_country_group_id !== NULL && $model->parent_courier_country_group_id != $id)
            $this->redirect(['/courier/courierCountryGroup/update', 'id' => $model->parent_courier_country_group_id, 'user_group' => $model->user_group_id]);

        if(isset($_POST['deleteGroup']))
        {
            if($model->user_group_id !== NULL)
            {
                $parent_id = $model->parent_courier_country_group_id;

                $ugi = $model->user_group_id;
                if($model->deleteWithOperators())
                {
                    $logText = 'Deleted custom routing setting.'.'<br/>';
                    $logText .= '<br/>';
                    $logText .= '<strong>Admin ID:</strong> '.Yii::app()->user->id;
                    $logText .= '<br/>';
                    $logText .= '<strong>Country group id:</strong> ' .$parent_id . '<br/>';
                    $logText .= '<strong>User group id:</strong> ' .$ugi . '<br/>';

                    S_Mailer::send( $notificationReceivers, $notificationReceivers, '[Changes in routing] Deleted custom routing setting : #'.$parent_id, $logText, false);

                    Yii::app()->user->setFlash('success', 'Custom settings deleted');
                }
                else
                {
                    Yii::app()->user->setFlash('danger', 'Ups, we could not delete custom settings');
                }
                $this->redirect(['update', 'id' => $parent_id]);
                Yii::app()->end();
            }

        } else if (isset($_POST['CourierCountryGroup'])) {

            $attrBefore = '';
            if($model->id)
                $attrBefore = $model->getAttributes(['pickup_hub', 'pickup_operator', 'delivery_hub', 'delivery_operator']);

            $countriesBefore = [];
            foreach($model->courierCountryGroupHasCountries AS $item)
                $countriesBefore[] = $item->country0->id;

            $customDeliveryBefore = [];
            foreach($model->courierCountryGroupOperatorDelivery AS $item)
                $customDeliveryBefore[] = $item->getAttributes(['weight_from', 'weight_to', 'courier_operator', 'courier_hub']);
            $customPickupBefore = [];
            foreach($model->courierCountryGroupOperatorPickup AS $item)
                $customPickupBefore[] = $item->getAttributes(['weight_from', 'weight_to', 'courier_operator', 'courier_hub']);

            $model->setAttributes($_POST['CourierCountryGroup']);

            $attrAfter = $model->getAttributes(['pickup_hub', 'pickup_operator', 'delivery_hub', 'delivery_operator']);

            if($_POST['CourierCountryGroupOperator'][CourierCountryGroupOperator::TYPE_PICKUP]['courier_operator'])
            {

                $modelPickupNew->attributes = $_POST['CourierCountryGroupOperator'][CourierCountryGroupOperator::TYPE_PICKUP];
                $modelPickupNew->validate();
            }

            if($_POST['CourierCountryGroupOperator'][CourierCountryGroupOperator::TYPE_DELIVERY]['courier_operator'])
            {

                $modelDeliveryNew->attributes = $_POST['CourierCountryGroupOperator'][CourierCountryGroupOperator::TYPE_DELIVERY];
                $modelDeliveryNew->validate();
            }

            $deleteList = $_POST['delete'];

            $countryList = $_POST['countryList'];
            if(!is_array($countryList))
                $countryList = [];

            if ($model->saveWithCountries($countryList, true, $modelDeliveryNew, $modelPickupNew, $deleteList)) {

                $countriesAfter = [];
                foreach($countryList AS $item)
                    $countriesAfter[] = $item;

                $customDeliveryAfter = [];
                $customPickupAfter = [];

                $model->refresh();
                foreach($model->courierCountryGroupOperatorDelivery AS $item)
                    $customDeliveryAfter[] = $item->getAttributes(['weight_from', 'weight_to', 'courier_operator', 'courier_hub']);

                foreach($model->courierCountryGroupOperatorPickup AS $item)
                    $customPickupAfter[] = $item->getAttributes(['weight_from', 'weight_to', 'courier_operator', 'courier_hub']);

//                if($modelDeliveryNew->id)
//                    $customDeliveryAfter[] = $modelDeliveryNew->getAttributes(['weight_from', 'weight_to', 'courier_operator', 'courier_hub']);
//                if($modelPickupNew->id)
//                    $customPickupAfter[] = $modelPickupNew->getAttributes(['weight_from', 'weight_to', 'courier_operator', 'courier_hub']);

                Yii::app()->user->setFlash('success', 'Settings has been saved!');

                $custom = '';
                if($model->parent_courier_country_group_id)
                    $custom = 'custom';

                $changesFound = false;
                $logText = 'Created/updated '.$custom.' routing settings.'.'<br/>';
                $logText .= '<br/>';
                $logText .= '<strong>Admin ID:</strong> '.Yii::app()->user->id;
                $logText .= '<br/>';
                $logText .= '<strong>Country group id:</strong> ' . ($model->parent_courier_country_group_id ? $model->parent_courier_country_group_id : $model->id) . '<br/>';

                if($custom) {
                    $logText .= '<strong>User group id:</strong> ' . $model->user_group_id . '<br/><br/>';
                } else {

                    $before = S_Useful::nicePrintR($countriesBefore);
                    $after = S_Useful::nicePrintR($countriesAfter);
                    if($before == $after)
                        $before = $after = '-- NO CHANGES --';
                    else
                        $changesFound = true;

                    $logText .= '<br/><strong>COUNTRY LIST:</strong>'.'<br/>';
                    $logText .= '<table border="1" cellpadding="5"><tr><td style="vertical-align: top;"><strong>BEFORE</strong></td><td style="vertical-align: top;"><strong>AFTER</strong></td></tr><tr><td style="vertical-align: top;">';
                    $logText .= $before;
                    $logText .= '</td><td style="vertical-align: top;">';
                    $logText .= $after;
                    $logText .= '</td></tr></table>';
                    $logText .= '<br/>';
                    $logText .= '<br/>';
                }

                $before = S_Useful::nicePrintR($attrBefore);
                $after = S_Useful::nicePrintR($attrAfter);
                if($before == $after)
                    $before = $after = '-- NO CHANGES --';
                else
                    $changesFound = true;

                $logText .= '<strong>DEFAULT:</strong>'.'<br/>';
                $logText .= '<table border="1" cellpadding="5"><tr><td style="vertical-align: top;"><strong>BEFORE</strong></td><td style="vertical-align: top;"><strong>AFTER</strong></td></tr><tr><td style="vertical-align: top;">';
                $logText .= $before;
                $logText .= '</td><td style="vertical-align: top;">';
                $logText .= $after;
                $logText .= '</td></tr></table>';
                $logText .= '<br/>';
                $logText .= '<br/>';
                $logText .= '<strong>CUSTOM - PICKUP:</strong>'.'<br/>';
                $logText .= '<table border="1" cellpadding="5"><tr><td style="vertical-align: top;"><strong>BEFORE</strong></td><td style="vertical-align: top;"><strong>AFTER</strong></td></tr><tr><td style="vertical-align: top;">';

                $before = S_Useful::nicePrintR($customPickupBefore);
                $after = S_Useful::nicePrintR($customPickupAfter);
                if($before == $after)
                    $before = $after = '-- NO CHANGES --';
                else
                    $changesFound = true;

                $logText .= $before;
                $logText .= '</td><td style="vertical-align: top;">';
                $logText .= $after;
                $logText .= '</td></tr></table>';
                $logText .= '<br/>';
                $logText .= '<strong>CUSTOM - DELIVERY:</strong>'.'<br/>';
                $logText .= '<table border="1" cellpadding="5"><tr><td style="vertical-align: top;"><strong>BEFORE</strong></td><td style="vertical-align: top;"><strong>AFTER</strong></td></tr><tr><td style="vertical-align: top;">';

                $before = S_Useful::nicePrintR($customDeliveryBefore);
                $after = S_Useful::nicePrintR($customDeliveryAfter);
                if($before == $after)
                    $before = $after = '-- NO CHANGES --';
                else
                    $changesFound = true;

                $logText .= $before;
                $logText .= '</td><td style="vertical-align: top;">';
                $logText .= $after;
                $logText .= '</td></tr></table>';

                if($changesFound)
                    S_Mailer::send($notificationReceivers, $notificationReceivers, '[Changes in routing] Created/updated '.$custom.': #'.($model->parent_courier_country_group_id ? $model->parent_courier_country_group_id : $model->id), $logText, false);

                if($model->parent_courier_country_group_id === NULL)
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array('update', 'id' => $model->parent_courier_country_group_id, 'user_group' => $model->user_group_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelsDelivery' => $model->courierCountryGroupOperatorDelivery,
            'modelsPickup' => $model->courierCountryGroupOperatorPickup,
            'modelDeliveryNew' => $modelDeliveryNew,
            'modelPickupNew' => $modelPickupNew,
            'userGroup' => $user_group,
            'userGroupModel' => $checkUserModel,
            'listUserGroups' => $listUserGroups
        ));
    }


    public function actionDelete($id) {

        try
        {
            if (Yii::app()->getRequest()->getIsPostRequest()) {
                $this->loadModel($id, 'CourierCountryGroup')->deleteWithOperators();

                if (!Yii::app()->getRequest()->getIsAjaxRequest())
                    $this->redirect(array('admin'));
            } else
                throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));

        }
        catch (Exception $ex)
        {
            throw new CHttpException(403,'Nie można usunąć tej pozycji!');
        }

    }

    public function actionIndex() {

        $this->redirect(array('courierCountryGroup/admin'));
    }

    public function actionAdmin() {
        $model = new CourierCountryGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierCountryGroup']))
            $model->setAttributes($_GET['CourierCountryGroup']);


        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionListAllGroups()
    {

        $models = CourierCountryGroup::model()->with('countryLists')->findAll();

        $this->render('listAllGroups',array(
            'models' => $models,
        ));
    }


    const COUNTRY = 0;
    const MODE = 1;
    const WEIGHT_FROM = 2;
    const WEIGHT_TO = 3;
    const OPERATOR = 4;
    const HUB = 5;


    public function actionImport($hash = false, $ugi = NULL)
    {
        if($hash == false)
        {

            $formModel = new F_RoutingImport;

            if(isset($_POST['F_RoutingImport'])) {
                $formModel->attributes = $_POST['F_RoutingImport'];
                $formModel->file = CUploadedFile::getInstance($formModel, 'file');

                if($formModel->validate())
                {
                    $fileData = $formModel->fileToArray($formModel->file->tempName);
                    $hash = hash('sha512', uniqid(time(),true));
                    Yii::app()->cacheUserData->set('CRI_'.$hash, $fileData, 60*60);
                    Yii::app()->cacheUserData->set('CRI_ORG_FILE_'.$hash, file_get_contents($formModel->file->tempName), 60*60);
                    Yii::app()->cacheUserData->set('CRI_ORG_FILE_EXT_'.$hash, $formModel->file->extensionName, 60*60);

                    $this->redirect(['/courier/courierCountryGroup/import', 'hash' => $hash, 'ugi' => $formModel->user_group_id]);
                    exit;
                }
            }

            if(isset(Yii::app()->session['CRI']))
            {
                if(isset(Yii::app()->session['CRI_GET'])) {

                    $path = file_get_contents(Yii::app()->session['CRI']);
                    Yii::app()->session['CRI'] = false;
                    Yii::app()->session['CRI_GET'] = false;
                    unset(Yii::app()->session['CRI']);
                    unset(Yii::app()->session['CRI_GET']);
                    $filename = 'backup_courier_routing_'.date('YmdHis').'.csv';
                    header("Content-Type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=\"".$filename."\";");
                    header("Content-Transfer-Encoding: binary");
                    exit($path);
                } else {
                    Yii::app()->session['CRI_GET'] = true;
                }
            }

            $this->render('import_step1', [
                'model' => $formModel,
            ]);
            exit;



        } else {

            $errors = false;

            if($ugi == -1)
                $ugi = NULL;

            $user_group_id = $ugi;


            $backup = [];

            $deleteList = [];

//            $file = file_get_contents('courier-routing.csv');

            $file = Yii::app()->cacheUserData->get('CRI_'.$hash);


            if(!S_Useful::sizeof($file))
            {

                Yii::app()->user->setFlash('error', 'File looks empty...');
                $errors = true;
            }

            $data = [];
            foreach ($file AS $item) {

                if (!S_Useful::sizeof($item))
                    continue;

                foreach ($item AS $key => $temp) {
                    $temp = explode('_', $temp);
                    $item[$key] = $temp[0];
                }

                if (!isset($data[$item[self::COUNTRY]]))
                    $data[$item[self::COUNTRY]] = [];

                if (!isset($data[$item[self::COUNTRY]][$item[self::MODE]]))
                    $data[$item[self::COUNTRY]][$item[self::MODE]] = [];

                if (!isset($data[$item[self::COUNTRY]][$item[self::MODE]][$item[self::WEIGHT_FROM]]))
                    $data[$item[self::COUNTRY]][$item[self::MODE]][$item[self::WEIGHT_FROM]] = [];


                $data[$item[self::COUNTRY]][$item[self::MODE]][$item[self::WEIGHT_FROM]] = [
                    'O' => $item[self::OPERATOR],
                    'H' => $item[self::HUB],
                    'T' => $item[self::WEIGHT_TO],
                ];


            }

            $countries = array_keys($data);
            $countriesModels = [];
            $countriesModelsOld = [];

            foreach ($countries AS $country_id) {

                $countryData = $data[$country_id];

                $models = [];
                $models['P'] = [];
                $models['D'] = [];


                /* @var $model CourierCountryGroup */

                try {
                    $model = CourierCountryGroup::loadModelWithPickupAvailableSet($country_id, $user_group_id);
                }
                catch (Exception $ex)
                {
                    Yii::app()->user->setFlash('error', $ex->getMessage());
                    $errors = true;
                }

                $modelsOld = [];
                $modelsOld['M'] = CourierCountryGroup::loadModelWithPickupAvailableSet($country_id, $user_group_id);
                $modelsOld['D'] = $modelsOld['M']->courierCountryGroupOperatorDelivery;
                $modelsOld['P'] = $modelsOld['M']->courierCountryGroupOperatorPickup;

                if($model === NULL)
                {
                    Yii::app()->user->setFlash('error', 'Unknown country group id! #'.$country_id);
                    $errors = true;
                }

                if($errors)
                {
                    $this->redirect(['/courier/courierCountryGroup/import', 'hash' => false]);
                    exit;
                }

                $deleteList[$country_id] = [];

                if(!$model->getIsNewRecord()) {
                    if ($model->isPickupAvailable()) {
                        $temp = [];
                        $temp[] = $country_id . '_' . $model->name;
                        $temp[] = 'P';
                        $temp[] = -1;
                        $temp[] = -1;
                        $temp[] = $model->pickup_operator . '_' . $model->pickupOperator->name;
                        $temp[] = $model->pickup_hub . '_' . $model->pickupHub->name;

                        $backup[] = $temp;

                        /* @var $pickup CourierCountryGroupOperator */
                        foreach ($model->courierCountryGroupOperatorPickup AS $pickup) {
                            $temp = [];
                            $temp[] = $country_id . '_' . $model->name;
                            $temp[] = 'P';
                            $temp[] = $pickup->weight_from;
                            $temp[] = $pickup->weight_to;
                            $temp[] = $pickup->courier_operator . '_' . $pickup->operator->name;
                            $temp[] = $pickup->courier_hub . '_' . $pickup->hub->name;

                            $backup[] = $temp;

                            $deleteList[$country_id][$pickup->id] = $pickup->id;
                        }
                    }


                    $temp = [];
                    $temp[] = $country_id . '_' . $model->name;
                    $temp[] = 'D';
                    $temp[] = -1;
                    $temp[] = -1;
                    $temp[] = $model->delivery_operator . '_' . $model->deliveryOperator->name;
                    $temp[] = $model->delivery_hub . '_' . $model->deliveryHub->name;

                    $backup[] = $temp;
                }

                /* @var $delivery CourierCountryGroupOperator */
                foreach ($model->courierCountryGroupOperatorDelivery AS $delivery) {
                    $temp = [];
                    $temp[] = $country_id . '_' . $model->name;
                    $temp[] = 'D';
                    $temp[] = $delivery->weight_from;
                    $temp[] = $delivery->weight_to;
                    $temp[] = $delivery->courier_operator . '_' . $delivery->operator->name;
                    $temp[] = $delivery->courier_hub . '_' . $delivery->hub->name;

                    $backup[] = $temp;

                    $deleteList[$country_id][$delivery->id] = $delivery->id;
                }


                if (isset($countryData['P'][-1])) {
//                $model->_is_for_pickup = true;
                    $model->pickup_operator = $countryData['P'][-1]['O'];
                    $model->pickup_hub = $countryData['P'][-1]['H'];
                } else {
//                $model->_is_for_pickup = false;
                    $model->pickup_operator = NULL;
                    $model->pickup_hub = NULL;
                }

                if (isset($countryData['D'][-1])) {

                    $model->delivery_operator = $countryData['D'][-1]['O'];
                    $model->delivery_hub = $countryData['D'][-1]['H'];
                } else {
                    $model->delivery_operator = NULL;
                    $model->delivery_hub = NULL;
                }

                $model->validate();
                $models['M'] = $model;

                if (is_array($countryData['P']))
                    foreach ($countryData['P'] AS $weight_from => $temp) {
                        if ($weight_from == -1)
                            continue;

                        $modelPickupNew = new CourierCountryGroupOperator();
                        $modelPickupNew->courier_country_group = $country_id;
                        $modelPickupNew->type = CourierCountryGroupOperator::TYPE_PICKUP;
                        $modelPickupNew->weight_from = $weight_from;
                        $modelPickupNew->weight_to = $temp['T'];
                        $modelPickupNew->courier_hub = $temp['H'];
                        $modelPickupNew->courier_operator = $temp['O'];
                        $modelPickupNew->validate();

                        $models['P'][] = $modelPickupNew;
                    }

                if (is_array($countryData['D']))
                    foreach ($countryData['D'] AS $weight_from => $temp) {
                        if ($weight_from == -1)
                            continue;

                        $modelDeliveryNew = new CourierCountryGroupOperator();
                        $modelDeliveryNew->courier_country_group = $country_id;
                        $modelDeliveryNew->type = CourierCountryGroupOperator::TYPE_DELIVERY;
                        $modelDeliveryNew->scenario = CourierCountryGroupOperator::SCENARIO_DELIVERY;
                        $modelDeliveryNew->weight_from = $weight_from;
                        $modelDeliveryNew->weight_to = $temp['T'];
                        $modelDeliveryNew->courier_hub = $temp['H'];
                        $modelDeliveryNew->courier_operator = $temp['O'];
                        $modelDeliveryNew->validate();

                        $models['D'][] = $modelDeliveryNew;
                    }

                $countriesModels[$country_id] = $models;
                $countriesModelsOld[$country_id] = $modelsOld;
            }

            if (isset($_POST['confirm-import'])) {
                if ($_POST['confirmation'] == 1) {

                    if ($user_group_id !== NULL)
                    {
                        foreach ($countriesModels AS $country_id => $model) {
                            $model['M']->user_group_id = $user_group_id;
                            $model['M']->parent_courier_country_group_id = $country_id;
                        }
                    }

                    $transaction = Yii::app()->db->beginTransaction();


                    /* @var $model CourierCountryGroup */

                    foreach ($countriesModels AS $country_id => $model) {
                        $onlyDefault = true;

                        foreach ($model['D'] AS $modelDelivery) {
                            $onlyDefault = false;
                            if (!$model['M']->saveWithCountries($model['M']->listCountriesForGroup(true), false, $modelDelivery, NULL, $deleteList[$country_id]))
                                $errors = true;
                        }

                        foreach ($model['P'] AS $modelPickup) {
                            $onlyDefault = false;
                            if (!$model['M']->saveWithCountries($model['M']->listCountriesForGroup(true), false, NULL, $modelPickup, $deleteList[$country_id]))
                                $errors = true;
                        }

                        if($onlyDefault)
                        {
                            if (!$model['M']->saveWithCountries($model['M']->listCountriesForGroup(true), false, NULL, NULL, $deleteList[$country_id]))
                                $errors = true;
                        }
                    }

                    if ($errors) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', 'Correct errrors!');

                    } else {
                        // save backup
                        $backupText = '';
                        $backup = [[
                                'country_group_id',
                                'pickup/delivery',
                                'weight_from',
                                'weight_to',
                                'operator_id',
                                'hub_id'
                            ]] + $backup;
                        foreach ($backup AS $line)
                            $backupText .= implode(';', $line) . PHP_EOL;
//
                        $ugSufix = '';

                        if ($user_group_id)
                            $ugSufix = $user_group_id . '_';


                        $backupPath = 'courier_routing_backup_' . $ugSufix . date('YmdHis') . '.csv';
                        file_put_contents($backupPath, $backupText);

                        $transaction->commit();

                        $orgFileData = Yii::app()->cacheUserData->get('CRI_ORG_FILE_'.$hash);
                        $backupFileData = $backupText;

                        $time = date('Y-m-d-H-i-s');
                        $filenameOrg = 'NEW_import_data_'.$time.'.'.Yii::app()->cacheUserData->get('CRI_ORG_FILE_EXT_'.$hash);
                        $filenameBackup = 'BACKUP_import_data_'.$time.'.csv';

                        $to = self::_emailReportAddresses();

                        S_Mailer::send($to, $to, '[Changes in PRE-ROUTING routing][FILE]', 'Files in attachment<br/></br>User_group_id : '.$user_group_id.'<br/><br/>Admin_id : '.Yii::app()->user->id, false, NULL, NULL,true, false, [$filenameOrg, $filenameBackup], false, false, false, [$orgFileData, $backupFileData]);


//
                        Yii::app()->user->setFlash('success', 'Changes have been saved! Download previous setting backup.');
                        Yii::app()->session['CRI'] = $backupPath;
                        Yii::app()->cacheUserData->set('CRI_'.$hash, false);
                        $this->redirect(['/courier/courierCountryGroup/import', 'hash' => false]);
                        Yii::app()->end();

                    }

                } else
                    Yii::app()->user->setFlash('error', 'Please know what you are doing :)');

            }


            $this->render('import', [
                'countriesModels' => $countriesModels,
                'countriesModelsOld' => $countriesModelsOld,
            ]);
        }
    }

    public function actionExport()
    {

        $formModel = new F_CourierRoutingExport;

        if(isset($_POST['F_CourierRoutingExport'])) {
            $formModel->attributes = $_POST['F_CourierRoutingExport'];

            if($formModel->validate())
            {

                $data = [];

                $country_list_id = implode(',', $formModel->country_list_id);
                $user_group_id = $formModel->user_group_id;

                if($user_group_id == -1)
                    $user_group_id = NULL;


                if($user_group_id === NULL)
                    $models = CourierCountryGroup::model()->findAll('parent_courier_country_group_id IS NULL AND user_group_id IS NULL AND id IN ('.$country_list_id.')');
                else
                    $models = CourierCountryGroup::model()->findAll('user_group_id = :ugi AND parent_courier_country_group_id IN ('.$country_list_id.')', [':ugi' => $user_group_id]);

                /* @var $model CourierCountryGroup */
                foreach($models AS $key => $model)
                {
                    if($user_group_id === NULL)
                        $country_id = $model->id;
                    else
                        $country_id = $model->parent_courier_country_group_id;

                    if($user_group_id === NULL)
                        $name = $model->name;
                    else
                        $name = $model->courierCountryGroupCustomParent->name;

                    if($model->isPickupAvailable()) {

                        $temp = [];
                        $temp[] = $country_id . '_' . $name;
                        $temp[] = 'P';
                        $temp[] = -1;
                        $temp[] = -1;
                        $temp[] = $model->pickup_operator.'_'.$model->pickupOperator->name;
                        $temp[] = $model->pickup_hub.'_'.$model->pickupHub->name;

                        $data[] = $temp;

                        /* @var $pickup CourierCountryGroupOperator */
                        foreach($model->courierCountryGroupOperatorPickup AS $pickup)
                        {
                            $temp = [];
                            $temp[] = $country_id . '_' . $name;
                            $temp[] = 'P';
                            $temp[] = $pickup->weight_from;
                            $temp[] = $pickup->weight_to;
                            $temp[] = $pickup->courier_operator.'_'.$pickup->operator->name;
                            $temp[] = $pickup->courier_hub.'_'.$pickup->hub->name;

                            $data[] = $temp;

                            $deleteList[$country_id][$pickup->id] = $pickup->id;
                        }
                    }


                    $temp = [];
                    $temp[] = $country_id . '_' . $name;
                    $temp[] = 'D';
                    $temp[] = -1;
                    $temp[] = -1;
                    $temp[] = $model->delivery_operator.'_'.$model->deliveryOperator->name;
                    $temp[] = $model->delivery_hub.'_'.$model->deliveryHub->name;

                    $data[] = $temp;

                    /* @var $delivery CourierCountryGroupOperator */
                    foreach($model->courierCountryGroupOperatorDelivery AS $delivery)
                    {
                        $temp = [];
                        $temp[] = $country_id . '_' . $name;
                        $temp[] = 'D';
                        $temp[] = $delivery->weight_from;
                        $temp[] = $delivery->weight_to;
                        $temp[] = $delivery->courier_operator.'_'.$delivery->operator->name;
                        $temp[] = $delivery->courier_hub.'_'.$delivery->hub->name;

                        $data[] = $temp;

                        $deleteList[$country_id][$delivery->id] = $delivery->id;
                    }
                }

                $arrayHeader = ['country_group_id','pickup/delivery','weight_from', 'weight_to', 'operator_id', 'hub_id'];


                $writer = Box\Spout\Writer\WriterFactory::create(Box\Spout\Common\Type::XLSX); // for XLSX files
                $writer->openToBrowser('export.xls');
                $writer->addRow($arrayHeader);

                foreach($data AS $key => $line)
                    $writer->addRow($line);

                $filename = 'export_courier_routing_' . date('YmdHis') . '.xlsx';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0'); //no cache

                $writer->close();
                Yii::app()->end();
            }
        }

        $this->render('export', [
            'model' => $formModel,
        ]);
        exit;


    }


    public function actionTempSwitch($id)
    {
        if(!AdminRestrictions::isUltraAdmin())
            throw new CHttpException(403);

        $model = CourierInternalOperatorChange::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $model->active = !$model->active;
        $model->date_updated = date('Y-m-d H:i:s');

        if($model->update()) {
            Yii::app()->user->setFlash('success', 'Item switched!');
            SystemLog::addToLog(SystemLog::TYPE_TEMP_INTERNAL_OPERATOR_SWITCHED, $model->id);

            $to = self::_emailReportAddresses();

            S_Mailer::send($to, $to, '[Changes in temp routing]['.($model->active ? 'turned ON' : 'turned OFF').']', ($model->active ? 'turned ON' : 'turned OFF').'<br/><br/>'.$model->fromOperator->getNameWithIdAndBroker().' ==> '.$model->toOperator->getNameWithIdAndBroker().'<br/><br/>By admin_id : '.Yii::app()->user->id, false);

        }
        else
            Yii::app()->user->setFlash('danger', 'Something went wrong...');



        $this->redirect(['/courier/courierCountryGroup/admin']);
    }

    public function actionTempDelete($id)
    {
        if(!AdminRestrictions::isUltraAdmin())
            throw new CHttpException(403);

        $model = CourierInternalOperatorChange::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $nameFrom = $model->fromOperator->getNameWithIdAndBroker();
        $nameTo = $model->toOperator->getNameWithIdAndBroker();

        if($model->delete()) {
            Yii::app()->user->setFlash('success', 'Item deleted!');
            SystemLog::addToLog(SystemLog::TYPE_TEMP_INTERNAL_OPERATOR_DELETED, $model->id);

            $to = self::_emailReportAddresses();

            S_Mailer::send($to, $to, '[Changes in temp routing][DELETED]', 'DELETED<br/><br/>'.$nameFrom.' ==> '.$nameTo.'<br/><br/>By admin_id : '.Yii::app()->user->id, false);
        }
        else
            Yii::app()->user->setFlash('danger', 'Something went wrong...');

        $this->redirect(['/courier/courierCountryGroup/admin']);
    }

    public function actionTempAdd()
    {
        if(!AdminRestrictions::isUltraAdmin())
            throw new CHttpException(403);

        $model = new CourierInternalOperatorChange();

        if(isset($_POST['CourierInternalOperatorChange']))
        {
            $model->setAttributes($_POST['CourierInternalOperatorChange']);

            if($model->validate())
            {
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Item created!');
                    SystemLog::addToLog(SystemLog::TYPE_TEMP_INTERNAL_OPERATOR_CREATED, $model->id);

                    $to = self::_emailReportAddresses();

                    S_Mailer::send($to, $to, '[Changes in temp routing][ADDED]', 'ADDED<br/><br/>'.$model->fromOperator->getNameWithIdAndBroker().' ==> '.$model->toOperator->getNameWithIdAndBroker().'<br/><br/>By admin_id : '.Yii::app()->user->id, false);
                }
                else
                    Yii::app()->user->setFlash('danger', 'Something went wrong...');

                $this->redirect(['/courier/courierCountryGroup/admin']);
            }
        }

        $this->render('tempAdd',
            ['model' => $model]
        );
    }

}