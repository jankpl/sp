$(document).ready(function(){

    $(".homeBlock").hide();

    var $blocks = $('.homeBlock');
    var loaded = 0;
    var toLoad = $blocks.length;

    var blocks = $(".homeBlock").toArray();

    if(toLoad>0)
    {
        $.each($blocks, function(index, value) {

            var bg = $(value).css('background-image');


            if (bg != 'none' && bg != '') {
                var src = bg.replace(/(^url\()|(\)$|[\"\'])/g, '');
                if(src == "none")
                {
                    loaded++;
                    if(loaded == toLoad)
                    {
                        onFirstPreloadReady();
                    }
                }
                else
                {
                    $img = $('<img>').on('load', function() {
                        loaded++;
                        if(loaded == toLoad)
                        {
                            getBlock(blocks);
                        }
                    }).attr('src', src);
                }
            } else loaded++;
        });
    }
});

function getBlock(blocks)
{
    blocks.sort(function() { return 0.5 - Math.random() });
    var item = blocks[0];
    blocks.shift();

    showBlock(item, $(".homeBlock").length - blocks.length);
    if(blocks.length > 0)
    {
        getBlock(blocks);
    }
}

function showBlock(i,v)
{
    $(i).delay(100 + v*50).fadeIn(100);
}


