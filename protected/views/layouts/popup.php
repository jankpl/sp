<?php
// FOR PRICE CALCULATORS
if(!$this->blockBootstrap)
{
    Yii::app()->bootstrap->register();
} else {
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/new/bootstrap.min.css');
}
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/system.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/form.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/style.css?v201901");
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/new/bootstrap.vertical-tabs.min.css');
//Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/bootstrap-responsive.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/new/bootstrap-customize-3.css");

Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/v2/style.css?v201901");

if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH)
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/theme/ruch/css/style.css");
else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI)
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/theme/pli/css/style.css");
else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS)
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/theme/quriers/css/style.css");
else if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/theme/cql/css/style.css");

//Yii::app()->clientScript->registerScriptFile('https://use.fontawesome.com/f8722a5953.js');
//Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/font-awesome/font-awesome.min.css");
?>
<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML>
<html lang="<?= Yii::app()->PV->getLangIsoCode();?>">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo Yii::t('meta','description');?>" />
    <meta name="keywords" content="<?php echo Yii::t('meta','keywords');?>" />
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700&subset=latin-ext" rel="stylesheet">
    <link href="<?= Yii::app()->request->baseUrl;?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <?php
    if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH):
        ?>
        <link rel="Shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/ruch/images/favicon.ico" />
    <?php
    elseif(Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI):
        ?>
        <link rel="Shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/pli/images/favicon.ico" />
    <?php
    elseif(Yii::app()->PV->get() == PageVersion::PAGEVERSION_QURIERS):
        ?>
        <link rel="Shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/quriers/images/favicon.ico" />
    <?php
    else:
        ?>
        <link rel="Shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/layout/favicon.ico" />
    <?php
    endif;
    ?>

    <?php
    if(YII_LOCAL):
        ?>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
    <?php
    endif;
    ?>

    <?php

    if(Yii::app()->NPS->isNewPanelSet()) {
        $css = '.gridview-container
                {
                    overflow-x: scroll;
                }';
        Yii::app()->clientScript->registerCss('gv', $css);
    }
    ?>

</head>
<body>
<div class="container" style="position: relative;">
    <div class="row">
        <div class="col-xs-12">
            <?php echo $content; ?>
        </div>
    </div>
</div>
<?php
if(!YII_LOCAL):
    ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-52271630-1', 'swiatprzesylek.pl');
        ga('send', 'pageview');
    </script>
<?php
endif;
?>
</body>
</html>
