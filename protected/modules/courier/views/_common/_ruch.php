<fieldset><legend><?= Yii::t('courier', 'Paczka w RUCHu');?></legend>
    <div class="alert alert-info">
        <p><?= Yii::t('courier', 'Wybierz punkt, do którego ma być dostarczona paczka.');?></p>
        <?php echo $form->error($submodel,'_ruch_receiver_point_address'); ?>
        <?php echo $form->labelEx($submodel,'_ruch_receiver_point'); ?>
        <?php echo $form->textField($submodel, '_ruch_receiver_point_address', ['id' => 'ruch_point_address', 'readonly' => true, 'placeholder' => Yii::t('courier', '-- kliknij, aby wybrać punkt --'), 'style' => 'text-align: center; cursor: pointer; width: 80%; max-width: 500px']); ?>
        <?php echo $form->error($submodel,'_ruch_receiver_point'); ?>

        <?php echo $form->hiddenField($submodel, '_ruch_receiver_point', ['id' => 'ruch_point']); ?>
        <br/>
        <br/>
        <p class="text-center"><strong><?= Yii::t('courier', 'Upewnij się, że podałeś poprawny nr telefonu obiorcy, gdyż zostanie na niego wysłany kod niezbędny do odbioru paczki!');?></strong></p>
    </div>
</fieldset>

<?php
$script = ' // RUCH:
        $(function(){
            var initialized = false;
            // bind on just on click to get fresh data from form

            if(!initialized)
                $("#ruch_point_address").on("click", function()
                {
                    initialized = true;
                    $(this).unbind("click");
                    $(this).pwrgeopicker("popup", {
                        "form": {
                            "city": $("#'.$addressDataModelName.'_receiver_city").val(),
                            "street": $("#'.$addressDataModelName.'_receiver_address_line_1").val(),
                        },
                        "marker_icons" : {
                            "mouseover": yii.urls.mapIcoOrange,
                            "mouseout":  yii.urls.mapIco,
                            "mousedown": yii.urls.mapIcoGreen,
                            "mouseup": yii.urls.mapIco,
                        },
                        "auto_start": true,
//                        "CashOnDelivery": $("#CourierTypeDomestic_cod_value").val() > 0 ? true : false,
                        "onselect": function(data){

                            var address = data.StreetName + ", " + data.City;

                            $("#ruch_point").val(data.DestinationCode);
                            $("#ruch_point_address").val(address);
                        }
                    });
                    $(this).trigger("click");
                });
        });    ';

Yii::app()->clientScript->registerScript('ruch', $script, CClientScript::POS_READY);
?>
