<?php

Yii::import('application.models._base.BaseOrder');
Yii::app()->getModule('courier');
Yii::app()->getModule('internationalMail');
Yii::app()->getModule('hybridMail');
Yii::app()->getModule('postal');

class Order extends BaseOrder
{
//    public $_label_on_mail = false;
//    public $_email_inv;
//    public $_email_lab;
    public $_inv_other_data;

    const COURIER = 1;
    const HYBRID_MAIL = 2;
    const INTERNATIONAL_MAIL = 3;
    const POSTAL = 4;

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(), array(
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                    'createAttribute' => 'date_entered',
                    'updateAttribute' => 'date_updated',
                ),
                'bMailer' => array(
                    'class' => 'bMailerOrder',
                ),
            )
        );
    }

    public function attributeLabels()
    {
        return CMap::mergeArray(
            [
//                '_email_lab' => Yii::t('order','Adres email'),
            ],
            parent::attributeLabels());
    }


    public static function statTranslations()
    {
        $translations = [
            OrderStat::NEW_ORDER => Yii::t('order', 'New order'),
            OrderStat::WAITING_FOR_PAYMENT => Yii::t('order', 'Waiting for payment'),
            OrderStat::FINISHED => Yii::t('order', 'Finished'),
            OrderStat::CANCELLED => Yii::t('order', 'Cancelled'),
        ];

        return $translations;
    }


    public function getNameTr()
    {
        return self::statTranslations()[$this->order_stat_id];
    }

    public function onAfterFinalizeOrder($event)
    {
        $this->raiseEvent('onAfterFinalizeOrder', $event);
    }

    public function calculateCS()
    {
        $salt = 'dsavEEW564yhBVxcvads0-20uxcnasSDAe45cxnas';
        $cs = md5($salt . $this->hash . $salt . $this->date_entered);
        return $cs;
    }

    public function onAfterOrderStatusChange($event)
    {
        //$this->raiseEvent('onAfterOrderStatusChange', $event);
    }

    public static function listProducts()
    {
        $products = Array();
        $products[Order::COURIER] = Array('id' => Order::COURIER, 'name' => Yii::t('products', 'Courier'));
        $products[Order::HYBRID_MAIL] = Array('id' => Order::HYBRID_MAIL, 'name' => Yii::t('products', 'HybridMail'));
        $products[Order::INTERNATIONAL_MAIL] = Array('id' => Order::INTERNATIONAL_MAIL, 'name' => Yii::t('products', 'InternationalMail'));
        $products[Order::POSTAL] = Array('id' => Order::POSTAL, 'name' => Yii::t('products', 'Postal'));

        return $products;
    }

    public static function getProductName($id)
    {
        $products = self::listProducts();
        $name = $products[$id]['name'];

        return $name;

    }

    public static function getOrderIdByHash($hash)
    {
        $model = Order::model()->find('hash=:hash', array(':hash' => $hash));
        return $model->id;
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function _random_key($length, $base)
    {
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $key = '';
        for ($i = 0; $i < $length; $i++) {
            $key .= $chars[(mt_rand(0, ($base - 1)))];
        }
        return $key;
    }

    protected function generateLocalId()
    {
        $is_it_unique = false;

        $today = date('ymd');

        $local_id = null;
        while (!$is_it_unique) {
            $local_id = '0' . $today . $this->_random_key(6, 6);

            $cmd = Yii::app()->db->createCommand();
            $cmd->select = 'COUNT(*)';
            $cmd->from = 'order';
            $cmd->where = "local_id = :local_id";
            $cmd->bindParam(':local_id', $local_id);
            $num = $cmd->queryScalar();

            if ($num == 0)
                $is_it_unique = true;
        }
        return $local_id;
    }

    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => $this->user_id . microtime() . rand(1,9999) ));

//            $cmd = Yii::app()->db->createCommand();
//            $cmd->select('count(*)');
//            $cmd->from('order');
//            $cmd->where('date_entered LIKE :today', array(':today' => date('Y-m-d').'%'));
//            $result = $cmd->queryScalar();
//
//            $result = $result + 1;
//            $result = (string) $result;
//            $this->local_id = '00'.date('ymd').str_pad($result,5,"0", STR_PAD_LEFT);
            $this->local_id = self::generateLocalId();

        }

        $this->params = array(
//            '_email_inv' => $this->_email_inv,
//            '_email_lab' => $this->_email_lab,
//            '_inv_on_mail' => $this->_inv_on_mail,
//            '_label_on_mail' => $this->_label_on_mail,
        );

        // serialize only if not all values are empty
        if (array_filter($this->params))
            $this->params = self::serialize($this->params);
        else
            $this->params = NULL;

        return parent::beforeSave();
    }

    public function afterFind()
    {
        if ($this->params !== NULL)
            $this->params = self::unserialize($this->params);

        if (is_array($this->params))
            foreach ($this->params AS $key => $item) {
                try {
                    $this->$key = $item;
                } catch (Exception $ex) {
                }
            }

        parent::afterFind();
    }

    /**
     * Unserialize data
     * @param $data
     * @return null|string
     */
    public static function serialize($data)
    {
        if ($data == '')
            return NULL;

        $data = serialize($data);
        $data = base64_encode($data);
        return $data;
    }

    /**
     * Serialize data
     * @param $data
     * @return mixed|string
     */
    public static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = @unserialize($data);
        return $data;
    }

    public function afterSave()
    {
        if ($this->isNewRecord) {
            $this->isNewRecord = false;

            $statHistory = new OrderStatHistory;
            $statHistory->order_id = $this->id;
            $statHistory->previous_stat = NULL;
            $statHistory->current_stat = OrderStat::NEW_ORDER;

            if (!$statHistory->save())
                throw new Exception(print_r($statHistory->getErrors(), 1));
        }

        return parent::afterSave();
    }


    // @27.04.2017
    // when duplicate local_id keys error, try to generate new and save again
    protected $_saveCounter = 0;

    public function save($runValidation = true, $attributes = null)
    {
        try {
            return parent::save($runValidation, $attributes);
        } catch (CDbException $ex) {
//            MyDump::dump('order-duplicate-localid.txt', print_r($ex,1));

            if ($ex->errorInfo[0] == 23000 && $this->_saveCounter < 3) {
                MyDump::dump('order-duplicate-localid.txt', 'DUPLIKAT: ' . $this->local_id);

                if ($this->isNewRecord) {
                    $this->local_id = $this->generateLocalId(true);
                    MyDump::dump('order-duplicate-localid.txt', 'NOWY ID: ' . $this->local_id);

                    $this->_saveCounter++;
                    return self::save($runValidation, $attributes);
                }
            }

            throw new CDbException($ex->getMessage(), $ex->getCode(), $ex->errorInfo);
        }
    }


    public function rules()
    {
        return array(

            array('order_stat_id', 'default',
                'value' => OrderStat::NEW_ORDER, 'on' => 'insert, finalize, firstCreate'),
            array('user_id', 'default',
                'value' => Yii::app()->user->id, 'on' => 'insert, finalize, firstCreate'),

            array('payment_type_id', 'required', 'on' => 'finalize, validateForm'),

            array('paid, finalized', 'default', 'setOnEmpty' => true, 'value' => 0),

//            array('_email_lab', 'email'),

            array('_inv_other_data', 'safe'),

            array('value_netto, value_brutto, currency, discount_value, payment_commision_value', 'required', 'except' => 'firstCreate,  validateForm'),
            array('paid, finalized, owner_data_id, order_stat_id', 'numerical', 'integerOnly' => true),
            array('value_netto, value_brutto, discount_value, payment_commision_value', 'numerical'),
            array('currency', 'length', 'max' => 8),
            array('hash', 'length', 'max' => 64),
            array('local_id', 'length', 'max' => 20),
            array('user_id, payment_type_id', 'length', 'max' => 10),
            array('date_updated', 'safe'),
            array('date_updated, user_id, owner_data_id, payment_type_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, local_id, value_netto, value_brutto, currency, date_entered, date_updated, hash, paid, finalized, discount_value, payment_commision_value, user_id, owner_data_id, payment_type_id, order_stat_id,
                          	product_type,
              __payment_type,
              __total_value,', 'safe', 'on' => 'search'),
        );
    }

    public function afterValidate()
    {

//        if($this->_inv_on_mail)
//            if($this->_email_inv == '')
//                $this->addError('_email_inv', Yii::t('order', 'Proszę podać adres e-mail'));
//
//        if($this->_label_on_mail)
//            if($this->_email_lab == '')
//                $this->addError('_email_lab', Yii::t('order', 'Proszę podać adres e-mail'));

        parent::afterValidate();
    }

//    public static function createOrderForProduct(OrderProduct $orderProduct, $firstCreate = false)
//    {
//        $model = new Order;
//        $model->order_product_id = $orderProduct->id;
//        $model->currency = $orderProduct->currency;
//
//        if ($firstCreate) {
//            $model->scenario = 'firstCreate';
//            $model->value_brutto = $orderProduct->value_brutto;
//            $model->value_netto = $orderProduct->value_netto;
//            $model->currency = $orderProduct->currency;
//            $model->save();
//
//            return $model;
//        } else {
//
//            $model->scenario = 'finalize';
////            $model->discount_value = $model->calculateDiscount();
//
//            return $model;
//        }
//    }

    public static function createOrderForProducts(array $orderProducts, $firstCreate = false)
    {
        $model = new Order;
        $model->currency = $orderProducts[0]->currency;

        $value_brutto = 0;
        $value_netto = 0;
        $currency = NULL;

        /* @var $orderProduct OrderProduct */
        foreach ($orderProducts AS $orderProduct) {
            $value_brutto += $orderProduct->value_brutto;
            $value_netto += $orderProduct->value_netto;

            if ($currency !== NULL && $orderProduct->currency != $currency)
                throw new CHttpException(403, 'Błąd przy składaniu zamówienia!');

            $currency = $orderProduct->currency;

            $product_type = $orderProduct->type;


        }


        if ($firstCreate) {
            $model->scenario = 'firstCreate';
            $model->value_brutto = $value_brutto;
            $model->value_netto = $value_netto;
            $model->currency = $currency;
            $model->product_type = $product_type;
            if (!$model->save()) {
                Yii::log(print_r($model->getErrors(), 1), CLogger::LEVEL_ERROR);
            }

            foreach ($orderProducts AS $orderProduct) {
                $orderProduct->order_id = $model->id;
                if (!$orderProduct->save()) {
                    Yii::log(print_r($orderProduct->getErrors(), 1), CLogger::LEVEL_ERROR);
                }

                if ($orderProduct->type == OrderProduct::TYPE_COURIER) {
                    /* @var $courierModel Courier */
                    $courierModel = Courier::model()->findByPk($orderProduct->type_item_id);

                    if ($courierModel->courierTypeInternal != NULL)
                        $courierModel->courierTypeInternal->assingToOrder($model->id);
                } else if ($orderProduct->type == OrderProduct::TYPE_HYBRID_MAIL) {
                    $hmModel = HybridMail::model()->findByPk($orderProduct->type_item_id);
                    if ($hmModel != NULL)
                        $hmModel->assingToOrder($model->id);
                } else if ($orderProduct->type == OrderProduct::TYPE_INTERNATIONAL_MAIL) {
                    $imModel = InternationalMail::model()->findByPk($orderProduct->type_item_id);
                    if ($imModel != NULL)
                        $imModel->assingToOrder($model->id);
                }

            }


            $parent_id = NULL;
            foreach($model->orderProduct AS $product)
            {
                if($product->type == OrderProduct::TYPE_COURIER)
                    $typeNew = OrderNew::TYPE_COURIER;
                else if($product->type == OrderProduct::TYPE_POSTAL)
                    $typeNew = OrderNew::TYPE_POSTAL;

                $on = OrderNew::addNewItem($typeNew, $product->type_item_id, $product->currency, $product->value_netto, $product->value_brutto, $product->value_netto > 0 ? (round($product->value_brutto /$product->value_netto, 2) * 100) - 1 : 0, Yii::app()->user->id, NULL, NULL, $parent_id);

                $parent_id = $on->id;
            }

        } else {

            $model->scenario = 'finalize';
            $model->discount_value = 0;
//            $model->discount_value = $model->calculateDiscount();
        }


        return $model;

    }

    public function calculateDiscount()
    {
        $discount = 0;
//
//
//        if ($this->user_id !== NULL) {
//            $value = $this->value_netto;
//
//            $user_id = $this->user_id;
//            $user = User::model()->findByPk($user_id);
//
//            /* @var $group UserGroup */
//            foreach ($user->userGroups AS $group) {
//
//                $discount_amount = $group->userGroupDiscount->discountPrice;
//                $discount_percentage = $group->userGroupDiscount->discount_percentage;
//
//                /* @var $priceValue PriceValue */
//                if ($discount_amount !== NULL) {
//                    $priceValue = $discount_amount->getValue(Yii::app()->PriceManager->getCurrencyIdByCode($this->currency));
//                    $discount += $priceValue->value;
//                }
//
//                if ($discount_percentage > 0)
//                    $discount += $discount_percentage * 0.01 * $value;
//
//
//            }
//
//        }

        return $discount;
    }

    public function calculateValueWithDiscount()
    {
        $value = $this->value_netto;
//        $value -= $this->calculateDiscount();

        if ($value < 0)
            $value = 0;

        return $value;
    }


    public function calculatePaymentCommision()
    {
        $payment = PaymentType::model()->findByPk($this->payment_type_id);

        if ($payment === NULL)
            return false;

        $value = $payment->calculatePaymentCommission($this->calculateValueWithDiscount());


        return $value;
    }

    public function calculateTotalValue($withTax = true)
    {
        $valueWithDiscount = $this->calculateValueWithDiscount();
        $paymentCommision = $this->calculatePaymentCommision();

        $totalValue = $valueWithDiscount + $paymentCommision;

        if ($withTax)
            $totalValue = Yii::app()->PriceManager->getFinalPrice($totalValue);

        return $totalValue;
    }

    public function createInvoice()
    {
        if ($this->invoiced)
            return false;

        $invoice = Fakturownia::createByOrder($this, false);

        if ($invoice['success']) {
            $inv_no = $invoice['no'];
            $pdf = $invoice['pdf'];
            $date = date('Y-m-d');

            $sheet = $this->createSheet();

            $tax = round((100 * ($this->value_brutto / $this->value_netto)) - 100);
            UserInvoice::createAndPublishWithFileString($this->user_id, $inv_no, $date, $date, $this->value_brutto, Currency::idByName($this->currency), 'Faktura wystawiona automatycznie', UserInvoice::SOURCE_FAKTUROWNIA, $pdf, true, $sheet, $this->id, $tax);

            $this->invoiced = true;
            $this->update(['invoiced']);


        } else
            return false;
    }


    public function createSheet()
    {
        $data = [];
        $headerPrinted = false;

        $this->value_brutto;
        $this->value_netto;
        $this->currency;
        $this->discount_value;
        $this->payment_commision_value;

        $data[] = ['currency', $this->currency];
        $data[] = ['value_netto', $this->value_netto];
        $data[] = ['value_brutto', $this->value_brutto];
        $data[] = ['discount_value', $this->discount_value];
        $data[] = ['payment_commision_value', $this->payment_commision_value];

        /* @var $orderProduct OrderProduct */
        foreach ($this->orderProduct AS $orderProduct) {
            if ($orderProduct->type == OrderProduct::TYPE_COURIER) {
                $courierAttributes = [
                    'local_id',
                    'date_entered',
                    'package_weight',
                    'package_size_l',
                    'package_size_w',
                    'package_size_d',
                    'packages_number',
                    'package_value',
                    'package_content',
                    'cod',
                    'cod_currency',
                    'cod_value',
                    'ref'
                ];

                $addressDataAttributes = [
                    'name',
                    'company',
                    'country',
                    'city',
                    'zip_code',
                    'address_line_1',
                    'address_line_2',
                    'tel',
                    'email'
                ];

                if (!$headerPrinted) {
                    $temp = [];

                    $temp[] = 'value_netto';
                    $temp[] = 'value_brutto';
                    $temp[] = 'what';

                    foreach ($courierAttributes AS $courierAttribute)
                        $temp[] = $courierAttribute;

                    foreach ($addressDataAttributes AS $addressDataAttribute)
                        $temp[] = 'sender_' . $addressDataAttribute;

                    foreach ($addressDataAttributes AS $addressDataAttribute)
                        $temp[] = '_receiver' . $addressDataAttribute;

                    $data[] = $temp;

                    $headerPrinted = true;
                }


                $temp = [];
                $model = Courier::model()->findByPk($orderProduct->type_item_id);


                $temp[] = $orderProduct->value_netto;
                $temp[] = $orderProduct->value_brutto;
                $temp[] = Order::getProductName($orderProduct->type);

                foreach ($courierAttributes AS $courierAttribute)
                    $temp[] = $model->$courierAttribute;

                foreach ($addressDataAttributes AS $addressDataAttribute)
                    $temp[] = $model->senderAddressData->$addressDataAttribute;

                foreach ($addressDataAttributes AS $addressDataAttribute)
                    $temp[] = $model->receiverAddressData->$addressDataAttribute;

                $data[] = $temp;
            }
        }

        return S_XmlGenerator::generateXml($data, 'Order ' . $this->local_id . '.xls', true);
    }

    public function changeStat($newStat, $author = '')
    {

        if ($this->order_stat_id == $newStat)
            return true;

        $orderStatHistory = new OrderStatHistory();
        $orderStatHistory->order_id = $this->id;
        $orderStatHistory->previous_stat = $this->order_stat_id;
        $orderStatHistory->current_stat = $newStat;

        if ($newStat == OrderStat::FINISHED) // change product stat
        {
            $model = null;

            // LOOP DIVIDED INTO SEPARATE LOOPS TO PREVENT BAD ORDER OF ACTIONS FOR COURIER MULTIPACKAGES
            // ON FIRST RUN JUST SET AS "PROCESSING"
            foreach ($this->orderProduct AS $product) {
//                Yii::app()->PM->m('1', 'BEFORE COURIER CHANGE STAT');
                switch ($product->type) {
                    case 1: // Courier
                        /* @var $model Courier */
                        $model = Courier::model()->findByPk($product->type_item_id);
                        $model->changeStat(CourierStat::PACKAGE_PROCESSING);
                        break;
                    case 2: // HybridMail
                        $model = HybridMail::model()->findByPk($product->type_item_id);
                        $model->changeStat(HybridMailStat::PACKAGE_RETREIVE);
                        break;
                    case 3: // InternationalMail
                        $model = InternationalMail::model()->findByPk($product->type_item_id);
                        $model->changeStat(InternationalMailStat::PACKAGE_RETREIVE);
                        break;
                    case 4: // Postal
                        $model = Postal::model()->findByPk($product->type_item_id);
                        $model->onOrderPaid();
                        break;
                }
//                Yii::app()->PM->m('1', 'AFTER COURIER CHANGE STAT');
            }

            // ON SECOND RUN, MARK AS PAID
            // IGNORE OTHER PRODUCTS THAN COURIER
            foreach ($this->orderProduct AS $product) {
//                Yii::app()->PM->m('1', 'BEFORE COURIER SET PAID');
                switch ($product->type) {
                    case 1: // Courier
                        /* @var $model Courier */

                        $model = Courier::model()->findByPk($product->type_item_id);
                        $model->courierTypeInternal->setPaid($this);
                        break;
                }
//                Yii::app()->PM->m('1', 'AFTER COURIER SET PAID');
            }


        } elseif ($newStat == OrderStat::CANCELLED) {
            foreach ($this->orderProduct AS $product) {
//                Yii::app()->PM->m('1', 'BEFORE COURIER CHANGE STAT');
                switch ($product->type) {
                    case 1: // Courier
                        /* @var $model Courier */
                        $model = Courier::model()->findByPk($product->type_item_id);
                        if ($model->getType() == Courier::TYPE_INTERNAL && !$model->courierTypeInternal->paid && in_array($model->courier_stat_id, [CourierStat::NEW_ORDER, CourierStat::PACKAGE_PROCESSING]))
                            $model->changeStat(CourierStat::CANCELLED, 0, NULL, false, null, false, true, false, false, true);
                        break;
                    case 2: // HybridMail
                        /* @var $model HybridMail */
                        $model = HybridMail::model()->findByPk($product->type_item_id);
                        if ($model->stat == HybridMailStat::NEW_ORDER)
                            $model->changeStat(HybridMailStat::CANCELLED);
                        break;
                    case 3: // InternationalMail
                        /* @var $model InternationalMail */
                        $model = InternationalMail::model()->findByPk($product->type_item_id);
                        if ($model->international_mail_stat_id == InternationalMailStat::NEW_ORDER)
                            $model->changeStat(InternationalMailStat::CANCELLED);
                        break;
                    case 4: // Postal
                        /* @var $model Postal */
                        $model = Postal::model()->findByPk($product->type_item_id);
                        if (in_array($model->postal_stat_id, [PostalStat::NEW_ORDER, PostalStat::PACKAGE_PROCESSING]))
                            $model->changeStat(PostalStat::CANCELLED, 0, NULL, false, null, false, true, false, false);
                        break;
                }
//                Yii::app()->PM->m('1', 'AFTER COURIER CHANGE STAT');
            }
        }

        if (!$orderStatHistory->save())
            $error = true;

        $this->order_stat_id = $newStat;


        if (!$this->save(false) OR $error) {
            throw new CHttpException('403', 'Operacja nie powiodła się!');
        }

        $this->onAfterOrderStatusChange(new CEvent($this, $orderStatHistory));

        return true;

    }

    /**
     * Method to fast finalize order and pay "on invoice"
     * @param bool|true $startOwnTransaction Whether to start new, own transaction
     * @return bool
     */
    public function fastFinalizeOrder($startOwnTransaction = true)
    {
        if (Yii::app()->user->isGuest
            OR $this->user_id === NULL
        )
            return false;

        $this->payment_type_id = PaymentType::FAST_PAYMENT_TYPE;

        if ($this->finalizeOrder($startOwnTransaction)) {
//             DUPLICATE REMOVED 08.11.2016
//            $model = Order::model()->findByPk($this->id);
//            $this->onAfterFinalizeOrder(new CEvent($this, array('model' => $model)));

//            Yii::app()->PM->m('1', 'BEFORE ORDER SET PAID');
            return $this->pay();
        } else
            return false;
    }

    public function finalizeOrder($startOwnTransaction = true)
    {

        if ($startOwnTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

//        $this->discount_value = $this->calculateDiscount();
        $this->discount_value = 0;
        $this->payment_commision_value = $this->calculatePaymentCommision();
        $this->value_netto = $this->calculateTotalValue(false);

        $this->value_brutto = Yii::app()->PriceManager->getFinalPrice($this->value_netto);

//        if($this->user_id === NULL)
//        {
//            if(!$this->ownerData->save())
//                $errors = true;
//
//            $this->owner_data_id = $this->ownerData->id;
//        }

        if ($this->addressDataOrder) {
            if (!$this->addressDataOrder->save())
                $errors = true;
            else
                $this->inv_address_data_id = $this->addressDataOrder->id;
        }

        $this->finalized = 1;
//        Yii::app()->PM->m('1', 'BEFORE ORDER CHANGE STAT');
        $this->changeStat(2);

        $this->scenario = 'finalize';

        if (!$this->save())
            $errors = true;

        if (!$errors) {
            if ($startOwnTransaction)
                $transaction->commit();

            $model = Order::model()->findByPk($this->id);
            $this->onAfterFinalizeOrder(new CEvent($this, array('model' => $model)));
            return true;
        } else {
            if ($startOwnTransaction)
                $transaction->rollback();
            return false;
        }


    }

    public function pay()
    {
        switch ($this->payment_type_id) {
            case PaymentType::PAYMENT_TYPE_ONLINE:
                //$this->changeStat(2,'system');
                //$this->paid = 1;
                //$this->update(array('paid'));
                return true;
                break;
            case PaymentType::PAYMENT_TYPE_INVOICE:
//                $userBalance = new UserBalance();
//                $userBalance->createWithOrder($this);
//                if ($userBalance->save()) {
                $this->changeStat(3, 'system');
                $this->paid = 1;
                $this->update(array('paid'));
                return true;
//                } else
//                    Yii::log(print_r($userBalance->getErrors(), 1), CLogger::LEVEL_ERROR);
                break;
            case PaymentType::PAYMENT_TYPE_COD:
                throw new CHttpException(403, 'Depreciated payment type!');
                break;
        }

        return false;
    }

    public static function createReadyOrderOnInvoice(array $orderProducts)
    {
        if (!S_Useful::sizeof($orderProducts))
            throw new Exception('No products for order!');


        $errors = false;

        $model = new Order;
        $model->currency = $orderProducts[0]->currency;
        $model->paid = 1;
        $model->finalized = 1;
        $model->order_stat_id = OrderStat::FINISHED;

        $value_brutto = 0;
        $value_netto = 0;
        $currency = NULL;

        /* @var $orderProduct OrderProduct */
        foreach ($orderProducts AS $orderProduct) {
            $value_brutto += $orderProduct->value_brutto;
            $value_netto += $orderProduct->value_netto;

            if ($currency !== NULL && $orderProduct->currency != $currency)
                throw new CHttpException(403, 'Błąd przy składaniu zamówienia!');

            $currency = $orderProduct->currency;
            $product_type = $orderProduct->type;
        }

        $model->scenario = 'firstCreate';
        $model->value_brutto = $value_brutto;
        $model->value_netto = $value_netto;
        $model->currency = $currency;
        $model->product_type = $product_type;
        if (!$model->save()) {
            $errors = true;
            Yii::log(print_r($model->getErrors(), 1), CLogger::LEVEL_ERROR);
        }

        foreach ($orderProducts AS $orderProduct) {
            $orderProduct->order_id = $model->id;
            if (!$orderProduct->save()) {
                $errors = true;
                Yii::log(print_r($orderProduct->getErrors(), 1), CLogger::LEVEL_ERROR);
            }
        }

        $model->scenario = 'finalize';
//        $model->discount_value = $model->calculateDiscount();
        $model->discount_value = 0;
        $model->order_stat_id = OrderStat::FINISHED;
        $model->payment_type_id = PaymentType::PAYMENT_TYPE_INVOICE;
        $model->paid = 1;

//        $userBalance = new UserBalance();
//        $userBalance->createWithOrder($model);
//        if (!$userBalance->save())
//            $errors = true;

        if ($errors)
            return false;
        else
            return $model->id;

    }


    public function getProductTypeName()
    {
        return isset(self::listProducts()[$this->product_type]) ? (self::listProducts()[$this->product_type])['name'] : NULL;
    }

}