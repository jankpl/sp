<?php

class AdminRestrictions
{
    public static function isUltraAdmin()
    {
        return in_array(Yii::app()->user->id,[
            2,
            3,
            49,
            71,
            73
        ]);
    }

    public static function isNMMMAdmin()
    {
        if(self::isUltraAdmin())
            return true;

        return in_array(Yii::app()->user->id,[
            5,
            22,
            72
        ]);
    }

    public static function isSuperAdmin()
    {
        if(self::isUltraAdmin())
            return true;

        return in_array(Yii::app()->user->id,[
            22,
            46,
            62,
            72,
            41,
        ]);
    }

    public static function isRoutingAdmin()
    {
        if(self::isUltraAdmin())
            return true;

        return in_array(Yii::app()->user->id,[
            22,
            23,
            32,
            46,
            49,
            72
        ]);
    }

    public static function isFinancialAdmin()
    {
        if(self::isUltraAdmin())
            return true;

        return in_array(Yii::app()->user->id,[
            5,
            16,
            34,
            22,
            23,
            46,
            41,
            52,
            32,
            49,
            62,
            72,
            33,
            75
        ]);
    }
}