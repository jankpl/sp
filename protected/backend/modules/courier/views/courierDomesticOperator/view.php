<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
    array('label'=>'Delete ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
        'date_entered',
        'date_updated',
        array(
            'name'=>'broker',
            'value'=>CourierDomesticOperator::getBrokerList()[$model->broker],
        ),
        '_safepak_service',
        array(
            'name'=>'stat',
            'value'=>S_Status::stat($model->stat),
        ),
    ),
)); ?>
