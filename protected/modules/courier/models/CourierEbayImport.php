<?php

Yii::import('application.modules.courier.models._base.BaseCourierEbayImport');

class CourierEbayImport extends BaseCourierEbayImport
{
	const STAT_NEW = 1;
	const STAT_SUCCESS = 2;
	const STAT_ERROR = 9;

	const STAT_UNKNOWN = 19;

	const STAT_CANCELLED = 13;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_entered',
				'updateAttribute' => NULL,
			),

		);
	}

	public function rules() {
		return array(
			array('courier_id, ebay_order_id, stat', 'required'),
			array('courier_id, stat', 'numerical', 'integerOnly'=>true),
			array('ebay_order_id', 'length', 'max'=>32),
			array('date_updated', 'safe'),
			array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, date_entered, date_updated, courier_id, ebay_order_id, stat', 'safe', 'on'=>'search'),
		);
	}

	public static function findActivePackageByEbayOrderId($ebayOrderId, $user_id = NULL)
	{
		if($user_id === NULL)
			$user_id = Yii::app()->user->id;

		$model = EbayImport::model()->find('ebay_order_id = :ebayOrderID AND stat != :stat AND user_id = :user_id AND target = :target', ['ebayOrderID' => $ebayOrderId, ':stat' => self::STAT_CANCELLED, ':user_id' => $user_id, ':target' => EbayImport::TARGET_COURIER]);

		if($model  === NULL)
			return false;
		else
			return $model;
	}
}
