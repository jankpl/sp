<?php
/* @var $selector */
/* @var $afterAjaxUpdate */
/* @var $withChosenPlugin */
/* @var $chosenSelector */

$temp = explode(';', $selector);

$script = " function() {";
$script .= "$('.newWindow').click(function(e)
        {
            e.preventDefault();
            var url=$(this).attr('href');
            window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=100, right=100, width=1020, height=700');
        });";

foreach($temp AS $selector):

    $script .= "
        var options = {
            width: '100px',
            selectAll: false,
            filter: true,                        
        };
        
        var single = false;
        if($('".$selector."').attr('multiple') != 'multiple')
        {
            options.single = true;
//            $('".$selector."').prepend($('<option>', {
//                value: '',
//                text: '-- clear --'
//            }));
            
//            single = true;
        }

        $('".$selector."').multipleSelect(options);          
        
        $('.ms-drop').css('width', '500px');   
        $('.ms-drop li.group input').remove();   
        $('.ms-drop li.group label').css('cursor', 'default');   
        
//        if(single)
//             $('.ms-drop li:first-child input').hide();              
 
";


endforeach;

if($withChosenPlugin) {

    $temp = explode(';', $chosenSelector);

    foreach($temp AS $chosenSelector):
        $script .= "
     $('[name=\'" . $chosenSelector . "\']').chosen({allow_single_deselect:true});
        $('.chosen-drop').css('width', '300px');
        $('[name=\'" . $chosenSelector . "\']').hide();
";
    endforeach;
}

$script .= $additionScript;

$script .= "
    }
";

if($afterAjaxUpdate) {
    echo $script;
} else {

    if($withChosenPlugin) {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.jquery.min.js');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.min.css');
//        $script = "
//$('[name=\'".$chosenSelector."\']').chosen({allow_single_deselect:true});
//$('.chosen-drop').css('width', '300px');
//alert('x');
//";
//        Yii::app()->clientScript->registerScript('chosen-js', $script, CClientScript::POS_READY);
    }

    $script = '$('.$script.');';

    Yii::app()->clientScript->registerScript('multipleSelect', $script, CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/multipleSelect/jquery.multiple.select.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/vendor-own/multipleSelect/multiple-select.css');


}