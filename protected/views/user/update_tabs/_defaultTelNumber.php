
<div class="form">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'default-tel',
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'tel']),
    ));
    ?>

    <h3><?php echo Yii::t('user', 'Domyślny numer telefonu');?></h3>

    <div class="alert alert-info"><?= Yii::t('courier', 'Ta opcja powoduje automatyczne uzupełnienie wymaganych numerów telefonów podanym tutaj numerem, jeżeli właściwy numer telefon nie został podany ręcznie. Opcja dotyczy wybranych typów paczek. Podany numer będzie on przekazywany do zewnętrznych operatorów i może utrudnić dostarczenie paczki. Na podany tutaj numer telefonu nie będą przesyłane żadne powiadomienia z naszego systemu.');?></div>
    <table class="table">
        <tr>
            <td style="vertical-align: middle; width: 100px;"><?= ($model->getDefaultTelNo() != '' ? '<span class="label label-success">'.Yii::t('user', 'Opcja aktywna').'</span>' : '<span class="label label-default">'.Yii::t('user', 'Opcja nieaktywna').'</span>');?></td>
            <td style="vertical-align: middle; width: 150px;"><?php echo $form->textfield($model, '_defaultTelNo'); ?></td>
            <td style="vertical-align: middle;"><?php echo Yii::t('user', 'Domyślny numer telefonu.');?></td>
        </tr>
        <tr>
            <td colspan="3"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'saveDefaultTel'));?></td>
        </tr>
    </table>


    <?php
    $this->endWidget();
    ?>
</div>
