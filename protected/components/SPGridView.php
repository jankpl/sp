<?php

Yii::import('ext.selgridview.BootSelGridView');

/**
 * Class SPGridView
 * This class add extra row in gridViewHeader
 */
class SPGridView extends BootSelGridView {

    public $addingHeaders = array();


    /**
     * Renders the data items for the grid view.
     */
    public function renderItems()
    {
        if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
        {
            echo "<table class=\"{$this->itemsCssClass}\" style=\"table-layout: fixed;\">\n";

            if(is_array($this->columns))
            foreach($this->columns AS $column)
            {
                $option = $column->htmlOptions['width'] != '' ? "width=\"{$column->htmlOptions['width']}\"" : "";

                echo "<col {$option}/>\n";
            }

            $this->renderTableHeader();
            ob_start();
            $this->renderTableBody();
            $body=ob_get_clean();
            $this->renderTableFooter();
            echo $body; // TFOOT must appear before TBODY according to the standard.
            echo "</table>";
        }
        else
            $this->renderEmptyText();
    }

    public function renderTableHeader() {
        if (!empty($this->addingHeaders))
            $this->multiRowHeader();

        parent::renderTableHeader();
    }

    protected function multiRowHeader() {
        echo CHtml::openTag('thead') . "\n";
        foreach ($this->addingHeaders as $row) {
            $this->addHeaderRow($row);
        }
        echo CHtml::closeTag('thead') . "\n";
    }

    // each cell value expects array(array($text,$colspan,$options), array(...))
    protected function addHeaderRow($row) {
        // add a single header row
        echo CHtml::openTag('tr') . "\n";
        // inherits header options from first column
        $fcol = 0;
        while (!isset($this->columns[$fcol])) {
            $fcol++;
        }
        $options = $this->columns[$fcol]->headerHtmlOptions;

        foreach ($row as $header) {
            $options['colspan'] = $header['colspan'];
            $cellOptions=($header['options'] + $options);
            echo CHtml::openTag('th', $cellOptions);
            echo $header['text'];
            echo CHtml::closeTag('th');
        }
        echo CHtml::closeTag('tr') . "\n";
    }

}
?>