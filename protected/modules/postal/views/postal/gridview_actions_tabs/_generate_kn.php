<?php
/* @var $id_prefix string */
?>
    <br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => $id_prefix.'generate-kn',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/knMulti'),
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => '_blank',
    ],
));
?>
<?php
echo TbHtml::submitButton(Yii::t('postal', 'Generate KsiążkaNadawcza'), array(
    'onClick' => 'js:
    $("#'.$id_prefix.'postal-ids-kn").val($("#postalgrid").selGridViewNoGet("getAllSelection").toString());
    ',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_DEFAULT, 'data-preload' => true));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => $id_prefix.'postal-ids-kn')); ?>
<?php
$this->endWidget();
?>
<br/>
<?= Yii::t('postal', 'Dotyczy tylko przesyłek, w których operatorem jest Poczta Polska.');?>