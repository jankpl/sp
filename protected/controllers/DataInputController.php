<?php

//http://localhost/SwiatPrzesylek.pl/swiatPrzesylek_ms5/dataInput/invoiceView?barcode=3274fbc364d5d463141d9463ebeeb0f4&key=f2A5ba3a8n9ja2az2azf6jUY7631

class DataInputController extends CController
{
    const PASSWORD = 'ddjfGBbnv63Gdlrskxnf2dAkqn';
    const LIMIT = 100;

//    public function filters()
//    {
//        return array_merge(array(
//            array(
//                'application.filters.HttpsFilter',
//                'bypass' => false,
//            ),
//        ));
//    }

    public function beforeAction($action)
    {
        MyDump::dump('dataInput.txt',print_r($action->id,1) . ' : ' . print_r($_REQUEST,1));

        return parent::beforeAction($action);
    }

    protected static function checkPass($key)
    {
        return ($key == md5(self::PASSWORD.date('Ymd')));
    }

    public function actionInvoiceView()
    {
        header("Content-type: text/xml");

        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $title = $post['title'];
//        $user_id = $post['user_id'];
//        $inv_value = $post['inv_value'];

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        $model = false;
        if($stat) {

            /* @var $model UserInvoice */
            $model = UserInvoice::model()->find('title = :title AND stat_inv != :stat_canceled AND stat_inv != :stat_corrected', [':title' => $title, ':stat_canceled' => UserInvoice::INV_STAT_CANCELLED, ':stat_corrected' => UserInvoice::INV_STAT_CORRECTED]);
//            $model = UserInvoice::model()->findByAttributes([
//                'title' => $title,
////                'user_id' => $user_id,
////                'inv_value' => $inv_value,
//            ]);

            if(!$model) {
                $stat = false;
                $log = 'Invoice not found!';
            }


        }

        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if($stat && $model) {
            $xml_output .= '<invoices>' . "\n";
            $xml_output .= '<invoice title="' . CHtml::encode($title) . '">' . "\r\n";
            $xml_output .= '<user_id>' . CHtml::encode($model->user_id) . '</user_id>' . "\r\n";
            $xml_output .= '<inv_value>' . CHtml::encode($model->inv_value) . '</inv_value>' . "\r\n";
            $xml_output .= '<paid_value>' . CHtml::encode($model->paid_value) . '</paid_value>' . "\r\n";
            $xml_output .= '<inv_currency>' . CHtml::encode($model->inv_currency) . '</inv_currency>' . "\r\n";
            $xml_output .= '<inv_date>' . CHtml::encode($model->inv_date) . '</inv_date>' . "\r\n";
            $xml_output .= '<inv_payment_date>' . CHtml::encode($model->inv_payment_date) . '</inv_payment_date>' . "\r\n";
            $xml_output .= '<stat>' . CHtml::encode($model->stat) . '</stat>' . "\r\n";
            $xml_output .= '<stat_inv>' . CHtml::encode($model->stat_inv) . '</stat_inv>' . "\r\n";
            $xml_output .= '<note>' . CHtml::encode($model->note) . '</note>' . "\r\n";
            $xml_output .= '<file_inv_mime>' . CHtml::encode($model->inv_file_type) . '</file_inv_mime>' . "\r\n";
            $xml_output .= '<file_inv_name>' . CHtml::encode($model->inv_file_name) . '</file_inv_name>' . "\r\n";
            $xml_output .= '<file_inv_size>' . CHtml::encode($model->inv_file_path ? filesize($model->inv_file_path) : NULL) . '</file_inv_size>' . "\r\n";
            $xml_output .= '<file_sheet_mime>' . CHtml::encode($model->sheet_file_type) . '</file_sheet_mime>' . "\r\n";
            $xml_output .= '<file_sheet_name>' . CHtml::encode($model->sheet_file_name) . '</file_sheet_name>' . "\r\n";
            $xml_output .= '<file_sheet_size>' . CHtml::encode($model->sheet_file_path ? filesize($model->sheet_file_path) : NULL) . '</file_sheet_size>' . "\r\n";
            $xml_output .= '</invoice>' . "\r\n";
            $xml_output .= "</invoices>\n";
        }

        $xml_output .= "</response>";



        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }

    public function actionInvoiceCreate()
    {

        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $title = $post['title'];
        $inv_date = $post['inv_date'];
        $inv_payment_date = $post['inv_payment_date'];
        $inv_value = $post['inv_value'];
        $inv_currency = $post['inv_currency'];
        $user_id = $post['user_id'];
        $url_inv = $post['url_inv'];
        $url_sheet = $post['url_sheet'];
        $paid_value = $post['paid_value'];
        $note = $post['note'];

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";

        $model = false;
        if($stat) {
            $transaction = Yii::app()->db->beginTransaction();
            $model = UserInvoice::createByData($user_id, $title, $inv_date, $inv_payment_date, $inv_value, $inv_currency, $paid_value, $note, UserInvoice::SOURCE_EXT);

            $baseUploadFir = Yii::app()->getBasePath().DIRECTORY_SEPARATOR.'invoice_temp_upload';

            if($model->save())
            {
                if($url_inv != '') {
                    $fileSaved = false;

                    // to get proper hash
                    $modelNew = UserInvoice::model()->findByPk($model->id);

                    $url_inv = basename($url_inv);

                    $fileContent = @file_get_contents($baseUploadFir.DIRECTORY_SEPARATOR.$url_inv);

                    $fileExtension = pathinfo($url_inv, PATHINFO_EXTENSION);
                    $fileName = basename($url_inv);
                    $tmpHandle = tmpfile();
                    $metaDatas = stream_get_meta_data($tmpHandle);
                    fwrite($tmpHandle, $fileContent);
                    $tmpFilename = $metaDatas['uri'];
                    $mimeType = mime_content_type($tmpFilename);
                    fclose($tmpHandle);

                    if($fileContent) {

                        $path = UserInvoice::getFinalDir($modelNew->hash . '_' . UserInvoice::FILE_TYPE_INV . '.' . $fileExtension);

                        if (@file_put_contents($path, $fileContent))
                        {
                            $fileSaved = true;
                            $model->inv_file_path = $path;
                            $model->inv_file_name = $fileName;
                            $model->inv_file_type = $mimeType;
                            $model->update(array('inv_file_path', 'inv_file_name', 'inv_file_type'));
                        }
                    }

                    if(!$fileSaved)
                    {
                        $stat = false;
                        $log = 'Found errors: '.'Invoice file not saved!';
                    }
                }


                if($url_sheet != '') {
                    $fileSaved = false;

                    // to get proper hash
                    $modelNew = UserInvoice::model()->findByPk($model->id);

                    $url_sheet = basename($url_sheet);

                    $fileContent = @file_get_contents($baseUploadFir.DIRECTORY_SEPARATOR.$url_sheet);

                    $fileExtension = pathinfo($url_sheet, PATHINFO_EXTENSION);
                    $fileName = basename($url_sheet);
                    $tmpHandle = tmpfile();
                    $metaDatas = stream_get_meta_data($tmpHandle);
                    fwrite($tmpHandle, $fileContent);
                    $tmpFilename = $metaDatas['uri'];
                    $mimeType = mime_content_type($tmpFilename);
                    fclose($tmpHandle);

                    if($fileContent) {

                        $path = UserInvoice::getFinalDir($modelNew->hash . '_' . UserInvoice::FILE_TYPE_SHEET . '.' . $fileExtension);

                        if (@file_put_contents($path, $fileContent))
                        {
                            $model->sheet_file_path = $path;
                            $model->sheet_file_name = $fileName;
                            $model->sheet_file_type = $mimeType;
                            $model->update(array('sheet_file_path', 'sheet_file_name', 'sheet_file_type'));
                            $fileSaved = true;
                        }
                    }

                    if(!$fileSaved)
                    {
                        $stat = false;
                        $log = 'Found errors: Sheet file not saved!';
                    }
                }

            } else {
                $stat = false;
                $log = 'Found errors: '.print_r($model->getErrors(),1);
            }

            if($stat)
            {
                $transaction->commit();
                @unlink($baseUploadFir.DIRECTORY_SEPARATOR.$url_inv);
                @unlink($baseUploadFir.DIRECTORY_SEPARATOR.$url_sheet);

            } else {
                $transaction->rollBack();
            }
        }

        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if($stat && $model) {
            $xml_output .= '<invoices>' . "\n";
            $xml_output .= '<invoice title="' . CHtml::encode($title) . '">' . "\r\n";
            $xml_output .= '<user_id>' . CHtml::encode($model->user_id) . '</user_id>' . "\r\n";
            $xml_output .= '<inv_value>' . CHtml::encode($model->inv_value) . '</inv_value>' . "\r\n";
            $xml_output .= '<paid_value>' . CHtml::encode($model->paid_value) . '</paid_value>' . "\r\n";
            $xml_output .= '<inv_currency>' . CHtml::encode($model->inv_currency) . '</inv_currency>' . "\r\n";
            $xml_output .= '<inv_date>' . CHtml::encode($model->inv_date) . '</inv_date>' . "\r\n";
            $xml_output .= '<inv_payment_date>' . CHtml::encode($model->inv_payment_date) . '</inv_payment_date>' . "\r\n";
            $xml_output .= '<stat>' . CHtml::encode($model->stat) . '</stat>' . "\r\n";
            $xml_output .= '<stat_inv>' . CHtml::encode($model->stat_inv) . '</stat_inv>' . "\r\n";
            $xml_output .= '<note>' . CHtml::encode($model->note) . '</note>' . "\r\n";
            $xml_output .= '<file_inv_mime>' . CHtml::encode($model->inv_file_type) . '</file_inv_mime>' . "\r\n";
            $xml_output .= '<file_inv_name>' . CHtml::encode($model->inv_file_name) . '</file_inv_name>' . "\r\n";
            $xml_output .= '<file_inv_size>' . CHtml::encode($model->inv_file_path ? filesize($model->inv_file_path) : NULL) . '</file_inv_size>' . "\r\n";
            $xml_output .= '<file_sheet_mime>' . CHtml::encode($model->sheet_file_type) . '</file_sheet_mime>' . "\r\n";
            $xml_output .= '<file_sheet_name>' . CHtml::encode($model->sheet_file_name) . '</file_sheet_name>' . "\r\n";
            $xml_output .= '<file_sheet_size>' . CHtml::encode($model->sheet_file_path ? filesize($model->sheet_file_path) : NULL) . '</file_sheet_size>' . "\r\n";
            $xml_output .= '</invoice>' . "\r\n";
            $xml_output .= "</invoices>\n";
        }

        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }

    public function actionInvoicePublish()
    {
        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $title = $post['title'];
//        $inv_value = $post['inv_value'];
//        $user_id = $post['user_id'];
        $dont_notify = $post['dont_notify'] ? true : false;

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }

        if($stat)
        {
            /* @var $model UserInvoice */
            $model = UserInvoice::model()->find('title = :title AND stat_inv != :stat_canceled AND stat_inv != :stat_corrected', [':title' => $title, ':stat_canceled' => UserInvoice::INV_STAT_CANCELLED, ':stat_corrected' => UserInvoice::INV_STAT_CORRECTED]);
//            $model = UserInvoice::model()->findByAttributes([
//                'title' => $title,
////                'user_id' => $user_id,
////                'inv_value' => $inv_value,
//            ]);

            if(!$model)
            {
                $stat = false;
                $log = 'Invoice not found!';
            } else {
                if ($model->stat != UserInvoice::UNPUBLISHED) {
                    $stat = false;
                    $log = 'Invoice already published!';
                } else {
                    if (!$model->publish())
                    {
                        $stat = false;
                        $log = 'Ups, we could not do it!';
                    }
                    else
                    {
                        if(!$dont_notify)
                            $model->notifyAboutPublished();
                    }


                }
            }
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";
        $xml_output .= '</response>';

        echo $xml_output;
        Yii::app()->end();
    }

    public function actionInvoiceDelete()
    {
        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $title = $post['title'];
//

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }

        if($stat)
        {
            /* @var $model UserInvoice */
            $model = UserInvoice::model()->find('title = :title AND stat_inv != :stat_canceled AND stat_inv != :stat_corrected', [':title' => $title, ':stat_canceled' => UserInvoice::INV_STAT_CANCELLED, ':stat_corrected' => UserInvoice::INV_STAT_CORRECTED]);
//            ([
//                'title' => $title,
////                'user_id' => $user_id,
////                'inv_value' => $inv_value,
//            ]);

            if(!$model)
            {
                $stat = false;
                $log = 'Invoice not found!';
            } else {

                if (!$model->deleteWithFile()) {
                    $stat = false;
                    $log = 'Ups, looks like we did not delete it...';
                }
            }

        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";
        $xml_output .= '</response>';

        echo $xml_output;
        Yii::app()->end();
    }

    public function actionInvoiceUpdate()
    {
        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $title = $post['title'];
//        $inv_value = $post['inv_value'];
//        $user_id = $post['user_id'];

        $stat_inv = $post['stat_inv'];
        $paid_value = $post['paid_value'];

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }

        if($stat)
        {
            /* @var $model UserInvoice */
            $model = UserInvoice::model()->find('title = :title AND stat_inv != :stat_canceled AND stat_inv != :stat_corrected', [':title' => $title, ':stat_canceled' => UserInvoice::INV_STAT_CANCELLED, ':stat_corrected' => UserInvoice::INV_STAT_CORRECTED]);
//            $model = UserInvoice::model()->findByAttributes([
//                'title' => $title,
////                'user_id' => $user_id,
////                'inv_value' => $inv_value,
//            ]);

            if(!$model)
            {
                $stat = false;
                $log = 'Invoice not found!';
            } else {

                if($stat_inv)
                    $model->stat_inv = $stat_inv;

                if($paid_value)
                    $model->paid_value = $paid_value;

                if (!$model->validate(['stat_inv', 'paid_value'])) {
                    $stat = false;
                    $log = 'Errors found: '.print_r($model->getErrors(),1);
                } else {
                    if(!$model->update(['stat_inv', 'paid_value', 'date_updated']))
                    {
                        $stat = false;
                        $log = 'Ups, we could not save it!';
                    }
                }
            }

        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";
        $xml_output .= '</response>';

        echo $xml_output;
        Yii::app()->end();
    }

    public function actionPocztaPolskaSend()
    {
        $LIMIT = 500;


        Yii::app()->getModule('postal');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $ids = $post['ids'];
        $id = $post['id'];



        if($ids && !is_array($ids))
            $ids = explode(',', $ids);

        if($pass != md5(self::PASSWORD))
        {
            $stat = false;
            $log = 'Invalid password';
        }
        else if($id == '' && !is_array($ids))
        {
            $stat = false;
            $log = 'No packages IDs provided';
        }
        else if($id == '' && !S_Useful::sizeof($ids))
        {
            $stat = false;
            $log = 'No packages IDs provided';
        }
        else if($id == '' && S_Useful::sizeof($ids) > $LIMIT)
        {
            $stat = false;
            $log = 'Too many IDs provided (limit is '.$LIMIT.')';
        }

        if($id && (!is_array($ids) OR !S_Useful::sizeof($ids)))
            $ids = [ $id ];


        if($stat) {

            $postals = Postal::model()->with([
                'postalOperator' => [
                    'condition' => 'uniq_id = :uniq_id',
                    'params' => [':uniq_id' => PostalOperator::UNIQID_OPERATOR_PP_SP_Zabrze_Polecona_Firmowa_OWN_LABEL]
                ]
            ])->findAllByAttributes(['local_id' => $ids]);

            $result = "\n";
            if (S_Useful::sizeof($postals)) {
                $resp = PocztaPolskaClient::sendDataToPpByReadyItems($postals);
                foreach ($resp AS $key => $item) {
                    $result .= "<item id=\"" . $key . "\">\n";
                    $result .= '<status>' . ($item['success'] ? 'OK' : 'FAIL') . '</status>' . "\n";
                    $result .= '<error>' . ($item['error']) . '</error>' . "\n";
                    $result .= '<message>' . ($item['response']) . '</message>' . "\n";
                    $result .= "</item>\n";
                }

            } else {
                $stat = false;
                $log = '0 items found!';
            }
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";
        $xml_output .= '<result>' . $result . '</result>' . "\n";
        $xml_output .= '</response>';

        echo $xml_output;
        Yii::app()->end();
    }



    public function actionCodView()
    {
        header("Content-type: text/xml");

        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $ids = $post['ids'];

        if(!is_array($ids))
            $ids = [$ids];

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }
        else if(!is_array($ids))
        {
            $stat = false;
            $log = 'Ids should be an array!';
        }
        else if(S_Useful::sizeof($ids) > self::LIMIT)
        {
            $stat = false;
            $log = 'Too many Ids!';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if($stat) {
            $ids = array_unique($ids);

            /* @var $models Courier[] */
            $models = Courier::model()->with('courierCodSettled')->findAllByAttributes(['local_id' => $ids]);

            $data = [];
            foreach ($models AS $model)
            {
                $data[$model->local_id] = [
                    'package_entered' => $model->date_entered,
                    'package_stat_map' => $model->stat_map_id,
                    'package_stat' => $model->courier_stat_id,
                    'cod_value' => floatval($model->cod_value),
                    'cod_currency' => $model->cod_currency,
                    'cod_settled' =>  $model->courierCodSettled ? true : false,
                    'settle_entered' => $model->courierCodSettled ? $model->courierCodSettled->date_entered : NULL,
                    'settle_date' => $model->courierCodSettled ? $model->courierCodSettled->settle_date : NULL,
                    'settle_source' => $model->courierCodSettled ? $model->courierCodSettled->source : NULL,
                    'settle_type' => $model->courierCodSettled ? $model->courierCodSettled->type : NULL,
                ];
            }


            $xml_output .= '<packages>'."\n";
            foreach($ids AS $id)
            {
                $found = true;
                if(!isset($data[$id]))
                    $found = false;

                $xml_output .= '<package local_id="' . CHtml::encode($id) . '">' . "\r\n";
                $xml_output .= '<found>' . CHtml::encode($found ? 1 : 0) . '</found>' . "\r\n";
                $xml_output .= '<package_entered>' . CHtml::encode(isset($data[$id]['package_entered']) ? $data[$id]['package_entered'] : NULL) . '</package_entered>' . "\r\n";
                $xml_output .= '<package_stat_map>' . CHtml::encode(isset($data[$id]['package_stat_map']) ? $data[$id]['package_stat_map'] : NULL) . '</package_stat_map>' . "\r\n";
                $xml_output .= '<package_stat>' . CHtml::encode(isset($data[$id]['package_stat']) ? $data[$id]['package_stat'] : NULL) . '</package_stat>' . "\r\n";
                $xml_output .= '<cod_value>' . CHtml::encode(isset($data[$id]['cod_value']) ? $data[$id]['cod_value'] : NULL) . '</cod_value>' . "\r\n";
                $xml_output .= '<cod_currency>' . CHtml::encode(isset($data[$id]['cod_currency']) ? $data[$id]['cod_currency'] : NULL) . '</cod_currency>' . "\r\n";
                $xml_output .= '<cod_settled>' . CHtml::encode(isset($data[$id]['cod_settled']) ? (int) $data[$id]['cod_settled'] : NULL) . '</cod_settled>' . "\r\n";
                $xml_output .= '<settle_entered>' . CHtml::encode(isset($data[$id]['settle_entered']) ? $data[$id]['settle_entered'] : NULL) . '</settle_entered>' . "\r\n";
                $xml_output .= '<settle_date>' . CHtml::encode(isset($data[$id]['settle_date']) ? $data[$id]['settle_date'] : NULL) . '</settle_date>' . "\r\n";
                $xml_output .= '<settle_source>' . CHtml::encode(isset($data[$id]['settle_source']) ? $data[$id]['settle_source'] : NULL) . '</settle_source>' . "\r\n";
                $xml_output .= '<settle_type>' . CHtml::encode(isset($data[$id]['settle_type']) ? $data[$id]['settle_type'] : NULL) . '</settle_type>' . "\r\n";
                $xml_output .= '</package>' . "\r\n";
            }
            $xml_output .= "</packages>\n";
        }
        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();

    }

    public function actionCodSettleOk()
    {
        header("Content-type: text/xml");

        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $ids = $post['ids'];
        $date = $post['date'];

        if($date == '')
            $date = date('Y-m-d');

        if(!is_array($ids))
            $ids = [$ids];

        $dateCheck = DateTime::createFromFormat('Y-m-d', $date);

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }
        else if(!is_array($ids))
        {
            $stat = false;
            $log = 'Ids should be an array!';
        }
        else if(S_Useful::sizeof($ids) > self::LIMIT)
        {
            $stat = false;
            $log = 'Too many Ids!';
        }
        else if($dateCheck->format('Y-m-d') != $date)
        {
            $stat = false;
            $log = 'Invalid date format!';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if($stat) {
            $ids = array_unique($ids);

            /* @var $models Courier[] */
            $models = Courier::model()->with('courierCodSettled')->findAllByAttributes(['local_id' => $ids]);

            $data = [];
            $dataIds = [];
            foreach ($models AS $model) {
                $data[$model->local_id] = $model;
                $dataIds[$model->local_id] = $model->id;

                /* @ 26.02.2019 */
                $model->changeStat(CourierStat::STAT_COD_PAID,1050, $date, false, NULL, false, true);
            }

            $settled = CourierCodSettled::addListOfItems($dataIds, CourierCodSettled::TYPE_OK, $date, CourierCodSettled::SOURCE_EXTERNAL);

            $xml_output .= '<settledNo>' . CHtml::encode((int) $settled) . '</settledNo>' . "\n";
            $xml_output .= '<packages>'."\n";
            foreach($ids AS $id)
            {
                $found = true;
                if(!isset($data[$id]))
                    $found = false;

                $xml_output .= '<package local_id="' . CHtml::encode($id) . '">' . "\r\n";
                $xml_output .= '<found>' . CHtml::encode($found ? 1 : 0) . '</found>' . "\r\n";
                $xml_output .= '<package_entered>' . CHtml::encode(isset($data[$id]['package_entered']) ? $data[$id]['package_entered'] : NULL) . '</package_entered>' . "\r\n";
                $xml_output .= '<package_stat_map>' . CHtml::encode(isset($data[$id]['package_stat_map']) ? $data[$id]['package_stat_map'] : NULL) . '</package_stat_map>' . "\r\n";
                $xml_output .= '<package_stat>' . CHtml::encode(isset($data[$id]['package_stat']) ? $data[$id]['package_stat'] : NULL) . '</package_stat>' . "\r\n";
                $xml_output .= '<cod_value>' . CHtml::encode(isset($data[$id]['cod_value']) ? $data[$id]['cod_value'] : NULL) . '</cod_value>' . "\r\n";
                $xml_output .= '<cod_currency>' . CHtml::encode(isset($data[$id]['cod_currency']) ? $data[$id]['cod_currency'] : NULL) . '</cod_currency>' . "\r\n";
                $xml_output .= '<cod_settled>' . CHtml::encode(isset($data[$id]['cod_settled']) ? (int) $data[$id]['cod_settled'] : NULL) . '</cod_settled>' . "\r\n";
                $xml_output .= '<settle_entered>' . CHtml::encode(isset($data[$id]['settle_entered']) ? $data[$id]['settle_entered'] : NULL) . '</settle_entered>' . "\r\n";
                $xml_output .= '<settle_date>' . CHtml::encode(isset($data[$id]['settle_date']) ? $data[$id]['settle_date'] : NULL) . '</settle_date>' . "\r\n";
                $xml_output .= '<settle_source>' . CHtml::encode(isset($data[$id]['settle_source']) ? $data[$id]['settle_source'] : NULL) . '</settle_source>' . "\r\n";
                $xml_output .= '<settle_type>' . CHtml::encode(isset($data[$id]['settle_type']) ? $data[$id]['settle_type'] : NULL) . '</settle_type>' . "\r\n";
                $xml_output .= '</package>' . "\r\n";
            }
            $xml_output .= "</packages>\n";
        }
        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }

    public function actionCodUnsettle()
    {
        header("Content-type: text/xml");

        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $ids = $post['ids'];

        if(!is_array($ids))
            $ids = [$ids];

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }
        else if(!is_array($ids))
        {
            $stat = false;
            $log = 'Ids should be an array!';
        }
        else if(S_Useful::sizeof($ids) > self::LIMIT)
        {
            $stat = false;
            $log = 'Too many Ids!';
        }


        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if($stat) {
            $ids = array_unique($ids);

            /* @var $models Courier[] */
            $models = Courier::model()->with('courierCodSettled')->findAllByAttributes(['local_id' => $ids]);

            $data = [];
            $dataIds = [];
            foreach ($models AS $model) {
                $data[$model->local_id] = $model;
                $dataIds[$model->local_id] = $model->id;
            }

            $unsettled = CourierCodSettled::removeListOfItems($dataIds);

            $xml_output .= '<unsettledNo>' . CHtml::encode((int) $unsettled) . '</unsettledNo>' . "\n";
            $xml_output .= '<packages>'."\n";
            foreach($ids AS $id)
            {
                $found = true;
                if(!isset($data[$id]))
                    $found = false;

                $xml_output .= '<package local_id="' . CHtml::encode($id) . '">' . "\r\n";
                $xml_output .= '<found>' . CHtml::encode($found ? 1 : 0) . '</found>' . "\r\n";
                $xml_output .= '<package_entered>' . CHtml::encode(isset($data[$id]['package_entered']) ? $data[$id]['package_entered'] : NULL) . '</package_entered>' . "\r\n";
                $xml_output .= '<package_stat_map>' . CHtml::encode(isset($data[$id]['package_stat_map']) ? $data[$id]['package_stat_map'] : NULL) . '</package_stat_map>' . "\r\n";
                $xml_output .= '<package_stat>' . CHtml::encode(isset($data[$id]['package_stat']) ? $data[$id]['package_stat'] : NULL) . '</package_stat>' . "\r\n";
                $xml_output .= '<cod_value>' . CHtml::encode(isset($data[$id]['cod_value']) ? $data[$id]['cod_value'] : NULL) . '</cod_value>' . "\r\n";
                $xml_output .= '<cod_currency>' . CHtml::encode(isset($data[$id]['cod_currency']) ? $data[$id]['cod_currency'] : NULL) . '</cod_currency>' . "\r\n";
                $xml_output .= '<cod_settled>' . CHtml::encode(isset($data[$id]['cod_settled']) ? (int) $data[$id]['cod_settled'] : NULL) . '</cod_settled>' . "\r\n";
                $xml_output .= '<settle_entered>' . CHtml::encode(isset($data[$id]['settle_entered']) ? $data[$id]['settle_entered'] : NULL) . '</settle_entered>' . "\r\n";
                $xml_output .= '<settle_date>' . CHtml::encode(isset($data[$id]['settle_date']) ? $data[$id]['settle_date'] : NULL) . '</settle_date>' . "\r\n";
                $xml_output .= '<settle_source>' . CHtml::encode(isset($data[$id]['settle_source']) ? $data[$id]['settle_source'] : NULL) . '</settle_source>' . "\r\n";
                $xml_output .= '<settle_type>' . CHtml::encode(isset($data[$id]['settle_type']) ? $data[$id]['settle_type'] : NULL) . '</settle_type>' . "\r\n";
                $xml_output .= '</package>' . "\r\n";
            }
            $xml_output .= "</packages>\n";
        }
        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }


    public function actionCodSettleCancelled()
    {
        header("Content-type: text/xml");

        Yii::app()->getModule('courier');

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $ids = $post['ids'];
        $date = $post['date'];

        if(!is_array($ids))
            $ids = [$ids];

        if($date == '')
            $date = date('Y-m-d');

        $dateCheck = DateTime::createFromFormat('Y-m-d', $date);

        if(!self::checkPass($pass))
        {
            $stat = false;
            $log = 'Invalid password!';
        }
        else if(!is_array($ids))
        {
            $stat = false;
            $log = 'Ids should be an array!';
        }
        else if(S_Useful::sizeof($ids) > self::LIMIT)
        {
            $stat = false;
            $log = 'Too many Ids!';
        }
        else if($dateCheck->format('Y-m-d') != $date)
        {
            $stat = false;
            $log = 'Invalid date format!';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if($stat) {
            $ids = array_unique($ids);

            /* @var $models Courier[] */
            $models = Courier::model()->with('courierCodSettled')->findAllByAttributes(['local_id' => $ids]);

            $data = [];
            $dataIds = [];
            foreach ($models AS $model) {
                $data[$model->local_id] = $model;
                $dataIds[$model->local_id] = $model->id;
            }

            $settled = CourierCodSettled::addListOfItems($dataIds, CourierCodSettled::TYPE_CANCELLED, $date, CourierCodSettled::SOURCE_EXTERNAL);

            $xml_output .= '<settledNo>' . CHtml::encode((int) $settled) . '</settledNo>' . "\n";
            $xml_output .= '<packages>'."\n";
            foreach($ids AS $id)
            {
                $found = true;
                if(!isset($data[$id]))
                    $found = false;

                $xml_output .= '<package local_id="' . CHtml::encode($id) . '">' . "\r\n";
                $xml_output .= '<found>' . CHtml::encode($found ? 1 : 0) . '</found>' . "\r\n";
                $xml_output .= '<package_entered>' . CHtml::encode(isset($data[$id]['package_entered']) ? $data[$id]['package_entered'] : NULL) . '</package_entered>' . "\r\n";
                $xml_output .= '<package_stat_map>' . CHtml::encode(isset($data[$id]['package_stat_map']) ? $data[$id]['package_stat_map'] : NULL) . '</package_stat_map>' . "\r\n";
                $xml_output .= '<package_stat>' . CHtml::encode(isset($data[$id]['package_stat']) ? $data[$id]['package_stat'] : NULL) . '</package_stat>' . "\r\n";
                $xml_output .= '<cod_value>' . CHtml::encode(isset($data[$id]['cod_value']) ? $data[$id]['cod_value'] : NULL) . '</cod_value>' . "\r\n";
                $xml_output .= '<cod_currency>' . CHtml::encode(isset($data[$id]['cod_currency']) ? $data[$id]['cod_currency'] : NULL) . '</cod_currency>' . "\r\n";
                $xml_output .= '<cod_settled>' . CHtml::encode(isset($data[$id]['cod_settled']) ? (int) $data[$id]['cod_settled'] : NULL) . '</cod_settled>' . "\r\n";
                $xml_output .= '<settle_entered>' . CHtml::encode(isset($data[$id]['settle_entered']) ? $data[$id]['settle_entered'] : NULL) . '</settle_entered>' . "\r\n";
                $xml_output .= '<settle_date>' . CHtml::encode(isset($data[$id]['settle_date']) ? $data[$id]['settle_date'] : NULL) . '</settle_date>' . "\r\n";
                $xml_output .= '<settle_source>' . CHtml::encode(isset($data[$id]['settle_source']) ? $data[$id]['settle_source'] : NULL) . '</settle_source>' . "\r\n";
                $xml_output .= '<settle_type>' . CHtml::encode(isset($data[$id]['settle_type']) ? $data[$id]['settle_type'] : NULL) . '</settle_type>' . "\r\n";
                $xml_output .= '</package>' . "\r\n";
            }
            $xml_output .= "</packages>\n";
        }
        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }



}

