<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
		array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
	);

?>

<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'courier-carrier-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'date_entered',
		'name',
		'description',
        array(
            'header' => 'Packages',
            'value' => '$data->couriersSTAT',
            'type' => 'raw',
        ),
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>