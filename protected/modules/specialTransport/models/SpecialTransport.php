<?php

Yii::import('application.modules.specialTransport.models._base.BaseSpecialTransport');

class SpecialTransport extends BaseSpecialTransport
{
    CONST STAT_NEW = 1;
    CONST STAT_SEEN = 2;
    CONST STAT_CANCELLED = 9;

    public $regulations;
    public $regulations_rodo;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
        );
    }

    public static function numberOfItemsByStat($stat)
    {
        $models = SpecialTransport::model()->findAll('stat=:stat',array(':stat' => $stat));

        return S_Useful::sizeof($models);
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord)
        {
            if($this->user && $this->user->getDisableOrderingNewItems())
                throw new AccountLockedException('Locked!');

            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => $this->user_id.$this->date_entered.microtime()));
        }

        return parent::beforeSave();
    }

    public function rules() {
        return CMap::mergeArray([
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'except'=>'insert'),

            array('stat', 'default',
                'value'=> SpecialTransport::STAT_NEW, 'on'=>'insert'),

            array('user_id', 'default',
                'value'=>Yii::app()->user->isGuest?null:Yii::app()->user->id, 'on'=>'insert'),

            array('customer_notes', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('type', 'required'),
            array('sender_address_data_id, receiver_address_data_id, type', 'numerical', 'integerOnly'=>true),
            array('hash', 'length', 'max'=>64),
            array('user_id', 'length', 'max'=>10),
            array('date_updated', 'safe'),
            array('date_updated, notes', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, hash, sender_address_data_id, receiver_address_data_id, notes, type, user_id, customer_notes
                stat,
                __stat,
                __user_login,
                __sender_country_id,
                __receiver_country_id','safe', 'on'=>'search'),

        ],
            RodoRegulations::regulationsRules('insert'));
    }

    public function attributeLabels()
    {
        return CMap::mergeArray(
            parent::attributeLabels(),
            RodoRegulations::attributeLabels()
        );

    }

    public static function types()
    {
        $types = Array();
        $types[1] = 'BELE';
        $types[2] = 'COLLI';
        $types[3] = 'GITERBOKSY';
        $types[4] = 'KALNISTRY';
        $types[5] = 'KARTONY';
        $types[6] = 'KONTENERY';
        $types[7] = 'ROLKI';
        $types[8] = 'SKRZYNIE';
        $types[9] = 'WIĄZKI';
        $types[10] = 'WORKI';
        $types[11] = 'BECZKI';
        $types[12] = 'KOSZE MODUŁOWE';


        return $types;
    }

    public static function stat()
    {
        $stat = Array();
        $stat[SpecialTransport::STAT_NEW] = 'new';
        $stat[SpecialTransport::STAT_SEEN] = 'processed';
        $stat[SpecialTransport::STAT_CANCELLED] = 'cancelled';

        return $stat;
    }
}
