<?php

class S_UniversalTt
{
    const SERVICE_COURIER = 1;
    const SERVICE_HM = 2;
    const SERVICE_IM = 3;
    const SERVICE_POSTAL = 4;

    protected $track_id;
    protected $model_id;
    protected $service;

    protected $ownerUserModel = NULL;

    protected $_searched = false;
    protected $_tracking_updated = false;

    protected $current_status_id;
    protected $current_status_name_short;
    protected $current_status_name_full;
    protected $current_status_location;

    protected $current_status_date;

    protected $history = array();

    protected $current_stat_map_id;
    protected $current_stat_map_name;

    protected $stat_map_history = [];

    protected $history_external = array();

    protected $service_model;


    protected $_archive = false;

    protected $_receiver_country_name = false;

    protected $_receiver_location;
    protected $_send_location;


    public function getOwnerUserModel()
    {
        return $this->ownerUserModel;
    }

    public function __construct($track_id)
    {
        $this->track_id = trim($track_id);
    }

    public function isArchived()
    {
        return $this->_archive;
    }

    public function getTrackId()
    {
        return $this->track_id;
    }

    public function getCurrentStatusNameFull($convertUrlToLink = false)
    {
        $this->getHistory();

        if($convertUrlToLink)
            $text = $this->current_status_location != '' ? S_Useful::urlToLinkInText($this->current_status_name_full) .' ('.S_Useful::urlToLinkInText($this->current_status_location).')' : S_Useful::urlToLinkInText($this->current_status_name_full);
        else
            $text = $this->current_status_location != '' ? $this->current_status_name_full .' ('.$this->current_status_location.')' : $this->current_status_name_full;

        return $text;
    }

    public function getReceiverCountryName()
    {
        return $this->_receiver_country_name;
    }

    public function getSendLocation()
    {
        return $this->_send_location;
    }

    public function getLastStatDate()
    {
        return $this->current_status_date;
    }

    public function getReceiverLocation()
    {
        return $this->_receiver_location;
    }

    public function getServiceModel()
    {
        $this->getHistory();

        return $this->service_model;
    }

    public function getCurrentStatMapId()
    {
        $this->getHistory();

        return $this->current_stat_map_id;
    }

    public function getCurrentStatMapName()
    {
        $this->getHistory();

        return $this->current_stat_map_name;
    }

    public function getStatMapHistory()
    {
        $this->getHistory();

        return $this->stat_map_history;
    }


    public function getCurrentStatusId()
    {
        return $this->current_status_id;
    }

    public function getCurrentLocation($excludeAdditionalData = false)
    {

        if($this->service == self::SERVICE_COURIER)
        {
            if($this->current_status_id <= CourierStat::PACKAGE_RETREIVE)
            {
                /* @var $model Courier */
                $model = Courier::model()->findByPk($this->model_id);
                return $model->senderAddressData->city.', '.$model->senderAddressData->country0->getTrName();
            }
            else if(in_array($this->current_status_id, [CourierStat::FINISHED, 651, 552])) // differend delivered stats
            {
                /* @var $model Courier */
                $model = Courier::model()->findByPk($this->model_id);
                return $model->receiverAddressData->city.', '.$model->receiverAddressData->country0->getTrName();
            }


            if($this->current_status_location != '')
            {
                if($excludeAdditionalData)
                {

                    $location = $this->current_status_location;
                    // UPS Locator uses '##' as separator (see UpsApiLocation component)
                    $location = explode('##', $location);


                    if(S_Useful::sizeof($location) > 1)
                        return $location[1];
                    else
                        return $location[0];

                } else
                    return $this->current_status_location;

            }

        }




        return '';
    }

    //  Find which type of service is for this $track_id
    public function getService()
    {


        if($this->_searched)
            return $this->service;

        $this->_searched = true;


        /////////////////////////////////////////////
        // postal
        // search for our HM ID

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("id");
        $cmd->from("postal");
        $cmd->where("local_id = :id", array(":id" => $this->track_id));
        $cmd->order('id DESC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if($result)
        {
            $this->service = self::SERVICE_POSTAL;
            $this->model_id = $result;
            return $this->service;
        }

        /////////////////////////////////////////////
        // postal
        // search for REF number

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("id");
        $cmd->from("postal");
        $cmd->where("ref = :id", array(":id" => $this->track_id));
        $cmd->order('id DESC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if($result)
        {
            $this->service = self::SERVICE_POSTAL;
            $this->model_id = $result;
            return $this->service;
        }

        /////////////////////////////////////////////
        // postal
        // search for track_id

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("id");
        $cmd->from("postal_label");
        $cmd->where("track_id = :id", array(":id" => $this->track_id));
        $cmd->order('id DESC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if($result)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select("id");
            $cmd->from("postal");
            $cmd->where("postal_label_id = :id", array(":id" => $result));
            $cmd->order('id DESC');
            $cmd->limit(1);
            $result = $cmd->queryScalar();
        }

        if($result)
        {
            $this->service = self::SERVICE_POSTAL;
            $this->model_id = $result;
            return $this->service;
        }

        /////////////////////////////////////////////
        // postal
        // search for additional ext id

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("postal_id");
        $cmd->from("postal_additional_ext_id");
        $cmd->where("external_id = :id", array(":id" => $this->track_id));
        $cmd->order('postal_id DESC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if($result)
        {
            $this->service = self::SERVICE_POSTAL;
            $this->model_id = $result;
            return $this->service;
        }

        /////////////////////////////////////////////
        // hm
        // search for our HM ID

//        $cmd = Yii::app()->db->createCommand();
//        $cmd->select("id");
//        $cmd->from("hybrid_mail");
//        $cmd->where("local_id = :id", array(":id" => $this->track_id));
//        $cmd->limit(1);
//        $result = $cmd->queryScalar();
//
//        if($result)
//        {
//            $this->service = self::SERVICE_HM;
//            $this->model_id = $result;
//            return $this->service;
//        }
//
//        /////////////////////////////////////////////
//        // im
//        // search for our IM ID
//
//        $cmd = Yii::app()->db->createCommand();
//        $cmd->select("id");
//        $cmd->from("international_mail");
//        $cmd->where("local_id = :id", array(":id" => $this->track_id));
//        $cmd->limit(1);
//        $result = $cmd->queryScalar();
//
//        if($result)
//        {
//            $this->service = self::SERVICE_IM;
//            $this->model_id = $result;
//            return $this->service;
//        }


        /////////////////////////////////////////////
        // courier
        // search for our package ID

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("id");
        $cmd->from("courier");
        $cmd->where("local_id = :id", array(":id" => $this->track_id));
        $cmd->order('id DESC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if($result)
        {
            $this->service = self::SERVICE_COURIER;
            $this->model_id = $result;
            return $this->service;
        }

        /////////////////////////////////////////////
        // courier
        // search for REF number

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("id");
        $cmd->from("courier");
        $cmd->where("ref = :id", array(":id" => $this->track_id));
        $cmd->order('id DESC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if($result)
        {
            $this->service = self::SERVICE_COURIER;
            $this->model_id = $result;
            return $this->service;
        }

        // internal simple



        // OOE
//        $cmd = Yii::app()->db->createCommand();
//        $cmd->select("courier_id");
//        $cmd->from("courier_type_ooe");
//        $cmd->where("remote_id = :id", array(":id" => $this->track_id));
//        $cmd->order('id DESC');
//        $cmd->limit(1);
//        $result = $cmd->queryScalar();
//
//        if($result)
//        {
//            $this->service = self::SERVICE_COURIER;
//            $this->model_id = $result;
//            return $this->service;
//        }

        // External

        $cmd = Yii::app()->db->createCommand();
        $cmd->select("id");
        $cmd->from("courier_type_external");
        $cmd->where("external_id = :id", array(":id" => $this->track_id));
        $cmd->order('id DESC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        if($result)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select("id");
            $cmd->from("courier");
            $cmd->where("courier_type_external_id = :id", array(":id" => $result));
            $cmd->order('id DESC');
            $cmd->limit(1);
            $result = $cmd->queryScalar();
        }

        if($result)
        {
            $this->service = self::SERVICE_COURIER;
            $this->model_id = $result;
            return $this->service;
        }


        // Internal

        //search in labels NEW
        $cmd = Yii::app()->db->createCommand();
        $cmd->select("courier_id");
        $cmd->from("courier_label_new");
        $cmd->where("track_id = :id", array(":id" => $this->track_id));
        $cmd->order('courier_id DESC');
        $cmd->limit(1);
        $result = $cmd->queryScalar();

//        // if not in courier_label_new, find in old type of labels
//        if(!$result) {
//            //search in labels
//            $cmd = Yii::app()->db->createCommand();
//            $cmd->select("id");
//            $cmd->from("courier_label");
//            $cmd->where("track_id_old = :id", array(":id" => $this->track_id));
//            $cmd->limit(1);
//            $result = $cmd->queryScalar();
//
//            //search in courier_type_internal
//            if ($result) {
//
//                $cmd = Yii::app()->db->createCommand();
//                $cmd->select("id");
//                $cmd->from("courier_type_internal");
//                $cmd->where("pickup_label = :id OR common_label = :id OR delivery_label = :id", array(":id" => $result));
//                $cmd->limit(1);
//                $result = $cmd->queryScalar();
//            }
//
//            // search for courier
//            if ($result) {
//                $cmd = Yii::app()->db->createCommand();
//                $cmd->select("id");
//                $cmd->from("courier");
//                $cmd->where("courier_type_internal_id = :id", array(":id" => $result));
//                $cmd->limit(1);
//                $result = $cmd->queryScalar();
//            }
//        }

        if($result)
        {
            $this->service = self::SERVICE_COURIER;
            $this->model_id = $result;
            return $this->service;
        }

    }

    public function isFound()
    {
        if(!$this->_searched)
        {
            return $this->getLocalId();
        }
        else if($this->_searched && $this->model_id)
            return true;
        else
            return false;
    }

    public function getLocalId()
    {
        if($this->model_id)
            return $this->model_id;
        else
        {
            $this->getService();
            return $this->model_id;
        }
    }

    public function getHistory()
    {
        if($this->updateTracking())
        {
            return $this->history;
        } else
            return false;
    }

    public function getHistoryExternal()
    {
        if($this->updateTracking())
        {
            return $this->history_external;
        } else
            return false;
    }


    protected function updateTracking()
    {

        if($this->_tracking_updated)
            return true;

        if(!$this->service OR !$this->model_id)
            return false;

        $this->_tracking_updated = true;

        switch($this->service)
        {
            case self::SERVICE_COURIER:
                return $this->_getTrackingCourier();
                break;
//            case self::SERVICE_HM:
//                return $this->_getTrackingHm();
//                break;
//            case self::SERVICE_IM:
//                return $this->_getTrackingIm();
//                break;
            case self::SERVICE_POSTAL:
                return $this->_getTrackingPostal();
                break;
        }

        return false;
    }

    protected function _getTrackingPostal()
    {
        Yii::app()->getModule('postal');

        /* @var $model Postal */
        $model = Postal::model()->findByPk($this->model_id);


        $this->ownerUserModel = $model->user;

        $this->_archive = $model->isStatHistoryArchived();

        $this->service_model = $model;

        $stat = $model->stat;

        $this->current_status_name_short = $stat ? $stat->getStatText() : '-';
        $this->current_status_name_full = $stat ? $stat->getStatText(true) : '-';
        $this->current_status_location = $model->location;
        $this->current_status_date = $model->getLastStatUpdate();
        $this->current_status_id = $model->postal_stat_id;
        $this->current_stat_map_id = $model->stat_map_id;
        $this->current_stat_map_name = StatMap::getMapNameById($this->current_stat_map_id);

        $this->_receiver_country_name = $model->receiverAddressData->country0->name;
        $this->_send_location = $model->senderAddressData->city.' '.$model->senderAddressData->country0->name;
        $this->_receiver_location = $model->receiverAddressData->city.' '.$model->receiverAddressData->country0->name;

        $this->stat_map_history = StatMapHistory::getReceivedMaps(StatMapMapping::TYPE_POSTAL, $this->model_id);

        $history = array();
        /* @var $item PostalStatHistory */
        foreach($model->postalStatHistoriesVisible(array('order' => 'status_date DESC, id DESC')) AS $item)
            array_push($history, array(
                'date' => $item->status_date,
                'full_text' => $item->currentStat ? $item->currentStat->getStatText(true) : '-',
                'short_text' => $item->currentStat ? $item->currentStat->getStatText() : '-',
                'location' => $item->location,
            ));
        $history = array_unique($history, SORT_REGULAR);
        $this->history = $history;

        return true;
    }

    protected function _getTrackingCourier()
    {
        Yii::app()->getModule('courier');

        /* @var $model Courier */
        $model = Courier::model()->findByPk($this->model_id);


        $this->ownerUserModel = $model->user;

        $this->_archive = $model->isStatHistoryArchived();

        $this->_receiver_country_name = $model->receiverAddressData->country0->name;
        $this->_send_location = $model->senderAddressData->city.' '.$model->senderAddressData->country0->name;
        $this->_receiver_location = $model->receiverAddressData->city.' '.$model->receiverAddressData->country0->name;

        $this->service_model = $model;

        $stat = $model->stat;

        $this->current_status_name_short = $stat->getStatText();
        $this->current_status_name_full = $stat->getStatText(true);
        $this->current_status_location = $model->location;
        $this->current_status_date = $model->getLastStatUpdate();
        $this->current_status_id = $model->courier_stat_id;
        $this->current_stat_map_id = $model->stat_map_id;
        $this->current_stat_map_name = StatMap::getMapNameById($this->current_stat_map_id);

        $this->stat_map_history = StatMapHistory::getReceivedMaps(StatMapMapping::TYPE_COURIER, $this->model_id);

        $historyExternal = array();
        $history = array();
        /* @var $item CourierStatHistory */
        foreach($model->courierStatHistoriesVisible(array('order' => 'status_date DESC, id DESC')) AS $item)
            array_push($history, array(
                'date' => $item->status_date,
                'full_text' => $item->currentStat->getStatText(true),
                'short_text' => $item->currentStat->getStatText(),
                'location' => $item->location,
            ));
        $history = array_unique($history, SORT_REGULAR);
        $this->history = $history;

//        // FROM OOE
//
//        if($model->getType() == Courier::TYPE_INTERNAL_OOE)
//        {
//            $track = Yii::app()->OneWorldExpress->track($model->courierTypeOoe->remote_id);
//            if(is_array($track))
//                foreach($track AS $item)
//                {
//                    array_push($historyExternal, array(
//                        'date' => $item['date'],
//                        'full_text' => $item['information'] .'('.$item['location'].')',
//                        'short_text' => $item['information']
//                    ));
//                }
//            $this->history_external = $historyExternal;
//        }
//        // FROM UPS
//        else if($model->getType() == Courier::TYPE_INTERNAL)
//        {
//
//
//            $typeInternal = $model->courierTypeInternal;
//
//            $ups_track_id = false;
//
//            // common operator must be checked first!
//            if($typeInternal->common_operator_ordered == CourierOperator::OPERATOR_UPS)
//                $ups_track_id = $typeInternal->commonLabel->track_id;
//            else if($typeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS)
//                $ups_track_id = $typeInternal->pickupLabel->track_id;
//            else if($typeInternal->delivery_operator_id == CourierOperator::OPERATOR_UPS)
//                $ups_track_id = $typeInternal->deliveryLabel->track_id;
//
//
//            if($ups_track_id)
//            {
//                $upsTrack = UpsTrackClient::track($ups_track_id);
//                if(is_array($upsTrack))
//                    foreach($upsTrack AS $item)
//                    {
//                        array_push($historyExternal, array(
//                            'date' => $item['date'],
//                            'full_text' => $item['name'],
//                            'short_text' => $item['name']
//                        ));
//                    }
//                $this->history_external = $historyExternal;
//            }
//
//        }

        return true;

    }

//    protected function _getTrackingIm()
//    {
//        Yii::app()->getModule('internationalMail');
//
//        /* @var $model InternationalMail */
//        $model = InternationalMail::model()->findByPk($this->model_id);
//        $statTr = $model->stat->internationalMailStatTr;
//
//        $this->current_status_name_short = $statTr->short_text;
//        $this->current_status_name_full = $statTr->full_text;
//
//        $history = array();
//        /* @var $item InternationalMailStatHistory */
//        foreach($model->internationalMailStatHistoriesVisible(array('order' => 'status_date DESC')) AS $item)
//            array_push($history, array(
//                'date' => $item->status_date,
//                'full_text' => $item->currentStat->internationalMailStatTr->full_text,
//                'short_text' => $item->currentStat->internationalMailStatTr->short_text,
//            ));
//        $history = array_unique($history, SORT_REGULAR);
//        $this->history = $history;
//
//        return true;
//    }
//
//    protected function _getTrackingHm()
//    {
//        Yii::app()->getModule('hybridMail');
//
//        /* @var $model HybridMail */
//        $model = HybridMail::model()->findByPk($this->model_id);
//        $statTr = $model->stat0->hybridMailStatTr;
//
//        $this->current_status_name_short = $statTr->short_text;
//        $this->current_status_name_full = $statTr->full_text;
//
//
//        $history = array();
//        /* @var $item HybridMailStatHistory */
//        foreach($model->hybridMailStatHistoriesVisible(array('order' => 'status_date DESC')) AS $item)
//            array_push($history, array(
//                'date' => $item->status_date,
//                'full_text' => $item->currentStat->hybridMailStatTr->full_text,
//                'short_text' => $item->currentStat->hybridMailStatTr->short_text,
//            ));
//        $history = array_unique($history, SORT_REGULAR);
//        $this->history = $history;
//
//        return true;
//    }

    // UPDATE STATUS FOR ITEM
    public function update()
    {
        Yii::app()->getModule('courier');

        if($this->getService() == self::SERVICE_COURIER)
        {
            $model = Courier::model()->findByPk($this->model_id);
            CourierExternalManager::updateStatusForPackage($model);
        }
    }



}