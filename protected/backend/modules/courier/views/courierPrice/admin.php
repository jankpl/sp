<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);


?>

<h1>Manage courier price</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-country-group-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'date_updated',
        array(
            'header' => 'Pickup',
            'value' => '$data->isForPickup()?"yes":"no"',
        ),
        array(
            'header' => 'Pickup price',
            'value' => '($data->courierPickupPriceHasGroupsSTAT)>0?"yes":"no"',
        ),
        array(
            'header' => 'Pickup group prices',
            'value' => '($data->courierPickupPriceHasGroupsSTATGroup)',
        ),
        array(
            'header' => 'Delivery prices',
            'value' => '$data->courierDeliveryPriceHasHubsSTAT."/".S_Useful::sizeof(CourierHub::listHubs())',
        ),
        array(
            'header' => 'Delivery group prices',
            'value' => '$data->courierDeliveryPriceHasHubsSTATGroup',
        ),
        array(
            'header' => 'Countries',
            'name' => 'countryListsCOUNT',
            'filter' => false,
        ),


        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        ),
    ),
)); ?>

<h2>Pricing simulator</h2>

<?php echo CHtml::link('Pricing simulator',array('simulate'),array('class' => 'likeButton'));?>

<h2>Import / export</h2>

<?php echo CHtml::link('Import',array('import'),array('class' => 'likeButton'));?> <?php echo CHtml::link('Export',array('export'),array('class' => 'likeButton'));?>

