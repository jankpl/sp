<?php
?>

<h1>Poczta Polska - Waiting</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'poczta-polska-waiting-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'date_entered',
        'date_posting',
        [
            'name' => '_is_sent',
            'value' => '$data->date_sent ? "Y" : "-"',
            'filter' => [ -1 => 'No', 1 => 'Yes'],
        ],
        'remote_id',
        [
            'name' => 'type',
            'value' => '$data->getTypeName()',
            'filter' => PocztaPolskaWaiting::getTypeList(),
        ],
        'local_id',
        'envelope_id',
        'envelope_name',
        [
            'name' => 'pp_account_id',
            'value' => 'PocztaPolskaClient::getLoginByAccount($data->pp_account_id)',
            'filter' => CHtml::listData(PocztaPolskaClient::getAccountList(true),'id','l'),
        ],
        'date_updated',
        'date_sent',
        'log',
    ),
)); ?>
