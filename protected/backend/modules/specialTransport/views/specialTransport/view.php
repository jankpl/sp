<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
);
?>

<h1>SpecialTransport #<?php echo $model->id; ?></h1>

<?php
foreach (Yii::app()->user->getFlashes() as
    $key => $message) {
    echo '<div class="alert alert-' . $key . '">' .
         $message . '</div>';
}
?>

<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'tabs' => array(
        array('label' => 'Details', 'content' => $this->renderPartial('_view_details', array('model' => $model), true), 'active' => (($activeTab=='' || $activeTab==1)?true:false)),
        array('label' => 'Address data', 'content' => $this->renderPartial('_view_address_data', array('model' => $model), true), 'active' => (($activeTab==2)?true:false)),
        array('label' => 'Notes', 'content' =>  $this->renderPartial('_view_notes', array('model' => $model), true), 'active' => (($activeTab==3)?true:false)),
    ),
)); ?>
