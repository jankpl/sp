<?php

Yii::import('application.modules.courier.models._base.BaseCourierImportManager');

class CourierImportManager extends BaseCourierImportManager
{
	const SOURCE_FILE = 0;
	const SOURCE_EBAY = 1;
	const SOURCE_LINKER = 2;


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('hash, courier_ids, user_id', 'required'),
			array('source', 'default', 'value' => self::SOURCE_FILE),
			array('hash', 'length', 'max'=>64),
			array('id, hash, courier_ids, date_entered, user_id, source', 'safe', 'on'=>'search'),
		);
	}


	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_entered',
				'updateAttribute' => NULL,
			),

		);
	}

	public function beforeSave()
	{
		$this->courier_ids = serialize($this->courier_ids);
		$this->courier_ids = base64_encode($this->courier_ids);


		return parent::beforeSave();
	}

	public function afterFind()
	{
		if($this->courier_ids == '')
			return array();

		$this->courier_ids = base64_decode($this->courier_ids);
		$this->courier_ids = unserialize($this->courier_ids);

		return parent::afterFind();
	}

	public function addCourierIds(array $courier_ids)
	{
		if(!is_array($this->courier_ids))
			$this->courier_ids = array();

		$this->courier_ids = array_merge($this->courier_ids, $courier_ids);
	}

	public static function getByHash($hash, $dontGenerate = false, $ignoreUserId = false)
	{

		if($ignoreUserId)
			$model = self::model()->findByAttributes(['hash' => $hash]);
		else
			$model = self::model()->findByAttributes(['hash' => $hash, 'user_id' => Yii::app()->user->id]);

		if($model)
			return $model;
		else
		{
			if($dontGenerate)
				return false;

			$model = new self;
			$model->user_id = Yii::app()->user->id;
			$model->hash = $hash;
			$model->save();
		}

		return $model;
	}


}