<?php
if($redirectUrl)
    echo '<script>$(window).load(function(){
    window.location = "'.$redirectUrl.'";
    });
</script>';
?>


<h3>Relabeling Tool</h3>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'relabeling-form',
    'enableAjaxValidation' => false,
));
?>

<div class="form" style="position: relative;">
    <div class="ajax-preloader"><div></div></div>


    <?php
    if($errors && $errorLog)
        echo '<div class="alert alert-danger">'.$errorLog.'</div>';

    if($successLog)
        echo '<div class="alert alert-success">'.$successLog.'</div>';
    ?>



    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <table class="table table-striped table-bordered">
        <tr>
            <th colspan="2" style="text-align: center;">New item type</th>
        </tr>
        <tr>
            <th style="text-align: center; width: 50%;">Courier non-routing</th>
            <th style="text-align: center;">Postal</th>
        </tr>
        <tr>
            <td style="text-align: center;"><?= CHtml::dropDownList('courier_non_routing_operator_id', $courier_non_routing_operator_id, CHtml::listData(CourierUOperator::model()->findAll(), 'id', 'nameWithIdAndBroker'), ['prompt' => '-', 'id' => 'target_courier']);?></td>
            <td style="text-align: center;">Operator: <?= CHtml::dropDownList('postal_operator_id', $postal_operator_id, CHtml::listData(PostalOperator::model()->findAll(), 'id', 'usefulName'), ['prompt' => '-', 'id' => 'target_postal']);?><br/>


            </td>
        </tr>
        <tr>
            <td>If not tel number provied, set this number:<br/>
               Sender: <?= Chtml::textField('default_tel_sender', $default_tel_sender);?><br/>
               Receiver: <?= Chtml::textField('default_tel_receiver', $default_tel_receiver);?><br/>
            </td>
            <td>If created from Courier:<br/>
                Type: <?= Chtml::dropDownList('postal_type', $postal_type, Postal::getPostalTypeList());?><br/>
                Size: <?= Chtml::dropDownList('postal_size', $postal_size, Postal::getSizeClassList());?></td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: center;">Cancel source item?</th>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><?= CHtml::checkBox('cancel', $cancel);?></td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: center;">Label type</th>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><?= CHtml::dropDownList('label_type', $label_type, [1 => 'Download PDF', 2 => 'Send to PrintServer']);?></td>
        </tr>
        <tr>
            <td style="text-align: right;">PrintServer settings (IP:Port)</td>
            <td colspan=""><?= CHtml::textField('ip', $ip,['placeholder' => 'IP']);?>:<?= CHtml::textField('port', $port,['placeholder' => 'PORT', 'style' => 'width: 70px;']);?></td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: center;">Package number</th>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><?= CHtml::textField('track_id','',['placeholder' => 'Paste Package ID', 'style' => 'text-align: center; padding: 25px; width: 350px;', 'id' => 'track_id']);?><br/>automatic submit on paste</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><?= CHtml::submitButton('Relabel', ['class' => 'btn btn-success btn-lg']);?></td>
        </tr>
    </table>

</div><!-- form -->

<?php
$this->endWidget();
?>


<table class="" style="width: 500px; margin: 0 auto;">
    <tr>
        <td id="labels-placeholder" class="text-center" style="vertical-align: top;">

        </td>
    </tr>
</table>

<script>
    $('#target_courier').on('change', function(){
        $('#target_postal').val('');
        $('#target_none').prop('checked', false);
    });

    $('#target_postal').on('change', function(){
        $('#target_courier').val('');
        $('#target_none').prop('checked', false);
    });

    $('#target_none').on('change', function(){
        $('#target_courier').val('');
        $('#target_postal').val('');
    });

    $(document).ready(function(){
        var $ajaxPreloader = $(".ajax-preloader");
        var $form = $("#relabeling-form");

        function automaticSubmit()
        {
            $('#track_id').on("paste", function (e) {

                // e.preventDefault();
                setTimeout(function(){
                    $form.submit();
                }, 50);

            });
        }

        $(document).ready(function() {

            $form.on('submit', function(){
                $ajaxPreloader.show();
            });

            $("#track_id").focus();
            automaticSubmit();

        });
    });
</script>



