<h2>Courier Log</h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-carrier-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        [
            'name' => '_local_id',
            'value' => '$data->courier->local_id',
        ],
        'date_entered',
        [
            'name' => 'text',
            'value' => 'substr($data->text,0, 255)',
        ],
        [
            'name' => 'cat',
            'filter' => CourierLog::getCatList(),
            'value' => '$data->getCatName()',
        ],
        'source'
    ),
)); ?>