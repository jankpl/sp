<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'order-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'date_entered'); ?>
		<?php echo $form->textField($model, 'date_entered'); ?>
		<?php echo $form->error($model,'date_entered'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'date_updated'); ?>
		<?php echo $form->textField($model, 'date_updated'); ?>
		<?php echo $form->error($model,'date_updated'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'hash'); ?>
		<?php echo $form->textField($model, 'hash', array('maxlength' => 64)); ?>
		<?php echo $form->error($model,'hash'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'paid'); ?>
		<?php echo $form->checkBox($model, 'paid'); ?>
		<?php echo $form->error($model,'paid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(User::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'user_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'payment_type_id'); ?>
		<?php echo $form->dropDownList($model, 'payment_type_id', GxHtml::listDataEx(PaymentType::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'payment_type_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'order_stat_id'); ?>
		<?php echo $form->dropDownList($model, 'order_stat_id', GxHtml::listDataEx(OrderStat::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'order_stat_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'product_type_id'); ?>
		<?php echo $form->dropDownList($model, 'product_type_id', GxHtml::listDataEx(ProductType::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'product_type_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'product_id'); ?>
		<?php echo $form->textField($model, 'product_id'); ?>
		<?php echo $form->error($model,'product_id'); ?>
		</div><!-- row -->

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->