<?php

class F_PostalImport extends F_BaseImport {

    public static function _distributeTooLongField(&$source, &$target, $attr_source, $attr_target, $cut, $reg = false)
    {
        $lengthSource = self::getAttributeLength($attr_source, $reg);

        if(mb_strlen($source) > $lengthSource)
        {
            if($target == '') {
                $target = mb_substr($source, $lengthSource);
                $source = mb_substr($source, 0, $lengthSource);

                if($cut)
                {
                    $lengthTarget = self::getAttributeLength($attr_target, $reg);
                    if($lengthTarget)
                        $target = mb_substr($target, 0, $lengthTarget);
                }

            }
            else if($cut)
            {
                $lengthSource = self::getAttributeLength($attr_source, $reg);
                if($lengthSource)
                    $source = mb_substr($source, 0, $lengthSource);
            }
        }
    }

    protected static function _getAttrValue($attribute, $mapOfAttributes,$source, $cut, $decimal = false, $reg = false)
    {
        $val = $source[$mapOfAttributes[$attribute]];
        if($cut)
        {
            $lenght = self::getAttributeLength($attribute, $reg);
            if($lenght)
                $val = mb_substr($val,0, $lenght);
        }

        if($decimal)
            $val = self::convertCommaToDec($val);

        return $val;
    }

    /**
     * Return list of attributes for import
     *
     * @param array $customOrder Array with custom order
     * @param bool|true $reorderKeys Whether to reorder keys. False on displaying order with widget
     * @return array
     */
    public static function getAttributesList($customOrder = [], $reorderKeys = true)
    {
        $attributes = Array(
            0 => 'postal_type',
            1 => 'size_class',
            2 => 'weight',

            3 => 'target_sp_point',

            4 => 'sender_name',
            5 => 'sender_company',
            6 => 'sender_country',
            7 => 'sender_zip_code',
            8 => 'sender_city',
            9 => 'sender_address_line_1',
            10 => 'sender_address_line_2',
            11 => 'sender_tel',
            12 => 'sender_email',

            13 => 'receiver_name',
            14 => 'receiver_company',
            15 => 'receiver_country',
            16 => 'receiver_zip_code',
            17 => 'receiver_city',
            18 => 'receiver_address_line_1',
            19 => 'receiver_address_line_2',
            20 => 'receiver_tel',
            21 => 'receiver_email',
            22 => 'content',
            23 => 'ref',
            24 => 'value',
            25 => 'value_currency',
            26 => 'note1',
            27 => 'note2',

//            24 => 'options',
        );

        if(in_array(Yii::app()->user->id, [812, 1289, 8,2404]))
            $attributes[28] = '_pp_forceTTNumber';

        // apply custom order
        if(is_array($customOrder)) {
            $attributes = array_replace(array_flip($customOrder), $attributes);
            if($reorderKeys)
                $attributes = array_values($attributes);
        }

        return $attributes;
    }

    //

    public static function getAttributeLength($attribute, $reg = false)
    {

        $data = [

            'sender_name' => 45 ,//'sender_name',
            'sender_company' => 45 ,//'sender_company',
            'sender_country' => 45 ,//'sender_country',
            'sender_zip_code' => 45 ,//'sender_zip_code',
            'sender_city' => 45 ,//'sender_city',
            'sender_address_line_1' => 45 ,//'sender_address_line_1',
            'sender_address_line_2' => 45 ,//'sender_address_line_2',
            'sender_tel' => 45 ,//'sender_tel',
            'sender_email' => 45 ,//'sender_email',

            'receiver_name' => 45 ,//'receiver_name',
            'receiver_company' => 45 ,//'receiver_company',
            'receiver_country' => 45 ,//'receiver_country',
            'receiver_zip_code' => 45 ,//'receiver_zip_code',
            'receiver_city' => 45 ,//'receiver_city',
            'receiver_address_line_1' => 45 ,//'receiver_address_line_1',
            'receiver_address_line_2' => 45 ,//'receiver_address_line_2',
            'receiver_tel' => 45 ,//'receiver_tel',
            'receiver_email' => 45 ,//'receiver_email',
            'content' => 45 ,//'content',
            'ref' => 64 ,//'ref',
            'note1' => 50 ,//'ref',
            'note2' => 50 ,//'ref',
        ];

//        if($reg)
//        {
//            $data['sender_name'] = $data['sender_company'] = $data['sender_address_line_1'] = $data['sender_address_line_2'] = $data['receiver_name'] = $data['receiver_company'] = $data['receiver_address_line_1'] = $data['receiver_address_line_2'] = 30;
//            $data['sender_city'] = $data['receiver_city'] = 20;
//            $data['sender_zip_code'] = $data['receiver_zip_code'] = 10;
//        }

        return isset($data[$attribute]) ? $data[$attribute] : false;
    }


    public static function calculateTotalNumberOfPackages(array $data, $customOrder = [])
    {


        $number = 0;
        foreach($data AS $item)
            $number += $item[self::getAttributesMap($customOrder)['packages_number']];

        return $number;
    }

    public static function postalTypeNameToId($postalTypeName)
    {
        switch($postalTypeName)
        {
            case 'STD':
                return Postal::POSTAL_TYPE_STD;
                break;
            case 'REG':
                return Postal::POSTAL_TYPE_REG;
                break;
            case 'PRIO':
                return Postal::POSTAL_TYPE_PRIO;
                break;
        }

        return NULL;
    }


    protected static $_countryCache = [];
    public static function getCountryFromCache($countryId)
    {
        if(!isset(self::$_countryCache[$countryId]))
            self::$_countryCache[$countryId] = CountryList::model()->cache(60*60)->with('countryListTr')->findByPk($countryId);



        return self::$_countryCache[$countryId];
    }


    public static function mapAttributesToModels(array $data, $customOrder = [], $cut = false, $operator_id = false)
    {

        $mapOfAttributes = self::getAttributesMap($customOrder);
        $models = [];

        $countryCache = [];

        $i = 0;
        foreach($data AS $key => $item)
        {
            $i++;

            $postal = new Postal();
            $postal->user_id = Yii::app()->user->id;
            $postal->_import_raw_data_key = $key;

            $postal->source = Postal::SOURCE_FILE_IMPORT;

            $postal->senderAddressData = new Postal_AddressData();
            $postal->receiverAddressData = new Postal_AddressData();

            $postal->senderAddressData->setSmsNotifyActive();
            $postal->receiverAddressData->setSmsNotifyActive();

            $postal->senderAddressData->_postal = $postal;
            $postal->receiverAddressData->_postal = $postal;


            $postal->postal_type = self::postalTypeNameToId($item[$mapOfAttributes['postal_type']]);

            $reg = false;
            if($postal->postal_type == Postal::POSTAL_TYPE_REG)
                $reg = true;


            $postal->weight = $item[$mapOfAttributes['weight']];
            $postal->size_class = $item[$mapOfAttributes['size_class']];

            $postal->value = $item[$mapOfAttributes['value']];
            $postal->value_currency = $item[$mapOfAttributes['value_currency']];

            $postal->target_sp_point = $item[$mapOfAttributes['target_sp_point']];


            $senderName = $item[$mapOfAttributes['sender_name']];
            $senderCompany = $item[$mapOfAttributes['sender_company']];
            self::_distributeTooLongField($senderName, $senderCompany, 'sender_name', 'sender_company', $cut, $reg);
            self::_distributeTooLongField($senderCompany, $senderName, 'sender_company', 'sender_name', $cut, $reg);

            $postal->senderAddressData->name = $senderName;
            $postal->senderAddressData->company = $senderCompany;
            $postal->senderAddressData->country_id = CountryList::findIdByText($item[$mapOfAttributes['sender_country']]);
            $postal->senderAddressData->country0 = self::getCountryFromCache($postal->senderAddressData->country_id);
            $postal->senderAddressData->zip_code = self::_getAttrValue('sender_zip_code', $mapOfAttributes, $item, $cut, $reg);
            $postal->senderAddressData->city = self::_getAttrValue('sender_city', $mapOfAttributes, $item, $cut, $reg);

            $senderAddressLine1 = $item[$mapOfAttributes['sender_address_line_1']];
            $senderAddressLine2 = $item[$mapOfAttributes['sender_address_line_2']];

            self::_distributeTooLongField($senderAddressLine1, $senderAddressLine2, 'sender_address_line_1', 'sender_address_line_2', $cut, $reg);

            $postal->senderAddressData->address_line_1 = $senderAddressLine1;
            $postal->senderAddressData->address_line_2 = $senderAddressLine2;

            $postal->senderAddressData->tel = self::_getAttrValue('sender_tel', $mapOfAttributes, $item, $cut, $reg);
            $postal->senderAddressData->email = self::_getAttrValue('sender_email', $mapOfAttributes, $item, $cut, $reg);

            $receiverName = $item[$mapOfAttributes['receiver_name']];
            $receiverCompany = $item[$mapOfAttributes['receiver_company']];
            self::_distributeTooLongField($receiverName, $receiverCompany, 'receiver_name', 'receiver_company', $cut, $reg);
            self::_distributeTooLongField($receiverCompany, $receiverName, 'receiver_company', 'receiver_name', $cut, $reg);


            $postal->receiverAddressData->name = $receiverName;
            $postal->receiverAddressData->company = $receiverCompany;

            $postal->receiverAddressData->country_id = CountryList::findIdByText($item[$mapOfAttributes['receiver_country']]);
            $postal->receiverAddressData->country0 = self::getCountryFromCache($postal->receiverAddressData->country_id);
            $postal->receiverAddressData->zip_code = self::_getAttrValue('receiver_zip_code', $mapOfAttributes, $item, $cut, $reg);
            $postal->receiverAddressData->city = self::_getAttrValue('receiver_city', $mapOfAttributes, $item, $cut, $reg);

            $receiverAddressLine1 = $item[$mapOfAttributes['receiver_address_line_1']];
            $receiverAddressLine2 = $item[$mapOfAttributes['receiver_address_line_2']];
            self::_distributeTooLongField($receiverAddressLine1, $receiverAddressLine2, 'receiver_address_line_1', 'receiver_address_line_2', $cut, $reg);

            $postal->receiverAddressData->address_line_1 = $receiverAddressLine1;
            $postal->receiverAddressData->address_line_2 = $receiverAddressLine2;

            $postal->receiverAddressData->tel = self::_getAttrValue('receiver_tel', $mapOfAttributes, $item, $cut, $reg);
            $postal->receiverAddressData->email = self::_getAttrValue('receiver_email', $mapOfAttributes, $item, $cut, $reg);

            $postal->content = self::_getAttrValue('content', $mapOfAttributes, $item, $cut, $reg);
            $postal->ref = self::_getAttrValue('ref', $mapOfAttributes, $item, $cut, $reg);
            $postal->note1 = self::_getAttrValue('note1', $mapOfAttributes, $item, $cut, $reg);
            $postal->note2 = self::_getAttrValue('note2', $mapOfAttributes, $item, $cut, $reg);

            // @ 13.09.2017 - special rule for user S.F_Express for importing postals with provided PP tracking number
            // @ 07.11.2017 - special rule for user Post11 for importing postals with provided PP tracking number
            // @ 22.05.2018 - or CZECH POST tracking number
            if(in_array(Yii::app()->user->id, [812, 1289,8,2404]))
                $postal->_pp_forceTTNumber = $item[$mapOfAttributes['_pp_forceTTNumber']];

            array_push($models, $postal);
        }

        return $models;
    }

    public static function validateModels(array &$models, &$returnErrors = false)
    {

        if($returnErrors)
        {
            if(!is_array($returnErrors))
                $returnErrors = [];

        }

        $errors = false;
        foreach($models AS $key => $model)
        {

            $models[$key]->validate();
            $models[$key]->receiverAddressData->validate();
            $models[$key]->senderAddressData->validate();


            // validate if carraige is possible
            if(!self::isCarriagePossible($models[$key])) {
                $models[$key]->receiverAddressData->addError('country_id', Yii::t('postal', 'Niestety, ale ta trasa nie jest obsługiwana.'));
            }

            if($models[$key]->hasErrors() OR $models[$key]->receiverAddressData->hasErrors() OR $models[$key]->senderAddressData->hasErrors())
                $errors = true;
            else
                $models[$key]->_price_total = Yii::app()->PriceManager->getFinalPrice($models[$key]->getPrice(Yii::app()->PriceManager->getCurrency()));

            $models[$key]->_price_currency = Yii::app()->PriceManager->getCurrencyCode();

            if($returnErrors !== false)
            {
                $temp = [];
                if($models[$key]->hasErrors())
                    $temp[] = $models[$key]->getErrors();

                if($models[$key]->receiverAddressData->hasErrors())
                    $temp[] = $models[$key]->receiverAddressData->getErrors();

                if($models[$key]->senderAddressData->hasErrors())
                    $temp[] = $models[$key]->senderAddressData->getErrors();

                $returnErrors[$key] = $temp;
            }

        }

        return $errors;
    }

    public static function isCarriagePossible(Postal $postal)
    {


        return $postal->getPrice() === NULL ? false : true;

    }
}