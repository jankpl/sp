<?php

/* @var $model CourierCollect */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

?>

    <h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' #' . GxHtml::encode($model->id); ?></h1>


    <table class="list hLeft" style="width: 500px;">
        <tr>
            <td>Id</td>
            <td><?php echo $model->id;?></td>
        </tr>
        <tr>
            <td>Status</td>
            <td><?php echo $model->getStats()[$model->stat];?></td>
        </tr>
        <tr>
            <td>Courier Package #</td>
            <td><?php echo CHtml::link(Courier::model()->findByPk($model->courier_id)->local_id,array("/courier/courier/view","id" => $model->courier_id),array("target" => "_blank"));?></td>
        <tr>
            <td>Date Entered</td>
            <td><?php echo $model->date_entered;?></td>
        </tr>
        <tr>
            <td>Date Updated</td>
            <td><?php echo $model->date_updated;?></td>
        </tr>
    </table>

    <br/>
    <br/>

<?php if($model->stat == CourierCollect::STAT_WAITING): ?>

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, 'Service not processed', array('closeText' => false)); ?>

<?php elseif($model->stat == CourierCollect::STAT_ERROR): ?>

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_ERROR, 'Service processed with error:<br/><br/><strong>'.$model->getLogNice().'</strong>', array('closeText' => false)); ?>

<?php else: ?>

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, 'Service processed with success!<br/><br/><strong>Pickup Request Number (PRN): '.$model->getPrn().'</strong>', array('closeText' => false)); ?>


<?php endif; ?>

<?php if($model->stat != CourierCollect::STAT_WAITING): ?>

    <h2>Collect order details:</h2>

   <?php
    if($model->collect_operator_id == CourierCollect::OPERATOR_UPS)
        $this->renderPartial('_details_ups', ['model' => $model]);
    else if($model->collect_operator_id == CourierCollect::OPERATOR_INPOST)
        $this->renderPartial('_details_inpost', ['model' => $model]);
    else if($model->collect_operator_id == CourierCollect::OPERATOR_GLS)
        $this->renderPartial('_details_gls', ['model' => $model]);
    ?>

<?php endif; ?>