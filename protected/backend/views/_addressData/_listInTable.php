<?php 
/* @var $model AddressData */
?>

<table class="table table-striped table-bordered table-condensed hLeft" style="width: 550px;">
    <tr>
        <td>Name</td>
        <td><?php echo $model->name; ?></td>
    </tr>
    <tr>
        <td>Company</td>
        <td><?php echo $model->company; ?></td>
    </tr>
    <tr>
        <td>Country</td>
        <td><?php echo $model->country->name; ?></td>
    </tr>
    <tr>
        <td>City</td>
        <td><?php echo $model->city; ?></td>
    </tr>
    <tr>
        <td>Zip code</td>
        <td><?php echo $model->zip_code; ?></td>
    </tr>
    <tr>
        <td>Address</td>
        <td><?php echo $model->address_line_1; ?></td>
    </tr>
    <tr>
        <td>Address cont.</td>
        <td><?php echo $model->address_line_2; ?></td>
    </tr>
    <tr>
        <td>Tel.</td>
        <td><?php echo $model->tel; ?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?php echo $model->email; ?></td>
    </tr>
</table>