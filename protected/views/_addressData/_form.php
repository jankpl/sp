<?php
/* @var $form CActiveForm */
/* @var $model AddressData */
/* @var $i Integer */
/* @var $type String */
/* @var $countries CountryList[] */

if(!isset($type))
    $type = key(UserContactBook::getTypes_enum());
?>



<?php echo $form->errorSummary($model); ?>
<?php if($model2!== null) echo $form->errorSummary($model2); ?>



<?php

$this->widget('AddressDataSuggester',
    array(
        'type' => $type,
        'fieldName' => 'AddressData',
        'i' => $i,
    ))
?>

<?php
//if(!Yii::app()->user->isGuest)
//{
//
//    ?>
<!--    <div class="row">-->
<!--        --><?php //echo CHtml::label(Yii::t('_addressData','Wybierz ze swoich adresów'),'userAddressList'); ?>
<!--        --><?php
//
//        echo CHtml::dropDownList((isset($i)?'['.$i.']':'').'userAddressList', 'user_address_list_id',
//            CHtml::listData(UserContactBook::listContactBookItems($type, $countries), 'id', 'representingName'),array('id'=>(isset($i)?$i.'_':'').'userAddressList')
//        );
//
//        echo CHtml::ajaxButton(Yii::t('_addressData','Pobierz'),
//            Yii::app()->createUrl('userContactBook/ajaxGetData/'), array(
//                'data' => 'js:{id : $("#'.(isset($i)?$i.'_':'').'userAddressList").val()}',
//                'dataType' => 'json',
//                'type' => 'get',
//                'success' => 'js:function(result) {
//
//            $.each(result, function(i,v)
//            {
//
//                $("#AddressData_'.(isset($i)?$i.'_':'').'" + i).val(v);
//                $("#AddressData_'.(isset($i)?$i.'_':'').'" + i).trigger("change");
//            });
//
//}'
//            ) // ajax
//        ); // script
//
//    echo'</div><!-- row -->';
//
//}
?>

<?php $this->renderPartial('//_addressData/_form_base',
    array('i' => $i, 'model' => $model, 'form' => $form, 'countries' => $countries));
?>

<?php
if(!Yii::app()->user->isGuest)
{
    ?>

    <div class="row">
        <?php echo CHtml::label(Yii::t('_addressData','Dodaj do swojej książki adresowej'),'Other[saveToContactBook]'.(isset($i)?'['.$i.']':'')); ?>
        <?php echo CHtml::checkBox('Other[saveToContactBook]'.(isset($i)?'['.$i.']':''), $saveToContactBook?true:false , array("id" => 'CB_saveToContactBook') ); ?>
    </div><!-- row -->


<?php

}
?>

<p class="note">
    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
</p>