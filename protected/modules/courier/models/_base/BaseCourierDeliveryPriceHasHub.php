<?php

/**
 * This is the model base class for the table "courier_delivery_price_has_hub".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CourierDeliveryPriceHasHub".
 *
 * Columns in table "courier_delivery_price_has_hub" available as properties of the model,
 * followed by relations of table "courier_delivery_price_has_hub" available as properties of the model.
 *
 * @property integer $id
 * @property integer $courier_country_group
 * @property integer $courier_pickup_hub
 * @property integer $courier_price_item
 * @property string $user_group_id
 *
 * @property UserGroup $userGroup
 * @property CourierPrice $courierPriceItem
 * @property CourierCountryGroup $courierCountryGroup
 * @property CourierHub $courierPickupHub
 */
abstract class BaseCourierDeliveryPriceHasHub extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'courier_delivery_price_has_hub';
	}

	public static function label($n = 1) {
		return Yii::t('model_label', 'CourierDeliveryPriceHasHub|CourierDeliveryPriceHasHubs', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('courier_country_group, courier_pickup_hub, courier_price_item', 'required'),
			array('courier_country_group, courier_pickup_hub, courier_price_item', 'numerical', 'integerOnly'=>true),
			array('user_group_id', 'length', 'max'=>11),
			array('user_group_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, courier_country_group, courier_pickup_hub, courier_price_item, user_group_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'userGroup' => array(self::BELONGS_TO, 'UserGroup', 'user_group_id'),
			'courierPriceItem' => array(self::BELONGS_TO, 'CourierPrice', 'courier_price_item'),
			'courierCountryGroup' => array(self::BELONGS_TO, 'CourierCountryGroup', 'courier_country_group'),
			'courierPickupHub' => array(self::BELONGS_TO, 'CourierHub', 'courier_pickup_hub'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('m_app', 'ID'),
			'courier_country_group' => null,
			'courier_pickup_hub' => null,
			'courier_price_item' => null,
			'user_group_id' => null,
			'userGroup' => null,
			'courierPriceItem' => null,
			'courierCountryGroup' => null,
			'courierPickupHub' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('courier_country_group', $this->courier_country_group);
		$criteria->compare('courier_pickup_hub', $this->courier_pickup_hub);
		$criteria->compare('courier_price_item', $this->courier_price_item);
		$criteria->compare('user_group_id', $this->user_group_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}