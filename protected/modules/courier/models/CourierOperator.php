<?php

Yii::import('application.modules.courier.models._base.BaseCourierOperator');

class CourierOperator extends BaseCourierOperator
{

    const OPERATOR_OWN = 1;
    const OPERATOR_UPS = 2;
    const OPERATOR_UPS_SAVER = 14;

    const OPERATOR_UPS_BIS = 30;
    const OPERATOR_UPS_SAVER_BIS = 31;

    const OPERATOR_UPS_F = GLOBAL_CONFIG::COURIER_OPERATOR_UPS_F;
    const OPERATOR_UPS_SAVER_F = GLOBAL_CONFIG::COURIER_OPERATOR_UPS_SAVER_F;

    const OPERATOR_UPS_M = GLOBAL_CONFIG::COURIER_OPERATOR_UPS_M;
    const OPERATOR_UPS_SAVER_M = GLOBAL_CONFIG::COURIER_OPERATOR_UPS_SAVER_M;

    const OPERATOR_UPS_UK = GLOBAL_CONFIG::COURIER_OPERATOR_UPS_UK;

    const OPERATOR_UPS_EXPRESS_L = GLOBAL_CONFIG::COURIER_OPERATOR_UPS_EXPRESS_L; // it's saver, not express

    const OPERATOR_UPS_SAVER_MCM = GLOBAL_CONFIG::COURIER_OPERATOR_UPS_SAVER_MCM;

    const OPERATOR_UPS_SAVER_4VALUES = GLOBAL_CONFIG::COURIER_OPERATOR_UPS_SAVER_4VALUES;

    const OPERATOR_DHL = GLOBAL_CONFIG::COURIER_OPERATOR_DHL;
    const OPERATOR_DHL_F = GLOBAL_CONFIG::COURIER_OPERATOR_DHL_F;

    const OPERATOR_GLS = 25;

    const OPERATOR_INPOST = 18;

    const OPERATOR_RUCH =  GLOBAL_CONFIG::COURIER_OPERATOR_RUCH;

    const OPERATOR_PETER_SK = GLOBAL_CONFIG::COURIER_OPERATOR_PETER_SK;

    const OPERATOR_PP_WROCLAW_Polecona_Firmowa = 53;
    const OPERATOR_PP_WROCLAW_PaczkaPocztowa = 54;

    const OPERATOR_PP_NYSA_Polecona_Firmowa = 55;
    const OPERATOR_PP_WARSZAWA_Polecona_Firmowa = 56;
    const OPERATOR_PP_BOCHNIA_Polecona_Firmowa = 57;

    const OPERATOR_PP_WROCLAW_Polecona_Firmowa_PRIO = 75;
    const OPERATOR_PP_NYSA_Polecona_Firmowa_PRIO = 76;
    const OPERATOR_PP_WARSZAWA_Polecona_Firmowa_PRIO = 77;
    const OPERATOR_PP_BOCHNIA_Polecona_Firmowa_PRIO = 78;

    const OPERATOR_GLS_NL = GLOBAL_CONFIG::COURIER_OPERATOR_GLS_NL;
    const OPERATOR_GLS_NL_EURO = GLOBAL_CONFIG::COURIER_OPERATOR_GLS_NL_EURO;


    const TYPE_TWO = 1;
    const TYPE_NO_PICKUP = 2;
    const TYPE_COMMON = 3;
    const TYPE_CONTRACT = 4;
    const TYPE_RUCH = 50;
    const TYPE_RM = 60;
    const TYPE_LP_EXPRESS = 70;
    const TYPE_INPOST = 80;
    const TYPE_UPS = 90;
    const TYPE_DHLDE = 100;

    const MODE_PICKUP = 11;
    const MODE_DELIVERY = 12;


    const BROKER_OOE = 1;
    const BROKER_SAFEPAK = 2;
    const BROKER_UPS = 5;
    const BROKER_INPOST = 18;
    const BROKER_GLS = 25;
    const BROKER_DHL = GLOBAL_CONFIG::COURIER_OPERATOR_BROKER_DHL;
    const BROKER_POCZTA_POLSKA = 50;
    const BROKER_GLS_NL = 80;
    const BROKER_DHLDE = 90;
    const BROKER_DHLEXPRESS = 100;
    const BROKER_UK_MAIL = 110;
    const BROKER_DPD_DE = 120;
    const BROKER_ROYAL_MAIL = 130;
    const BROKER_DPDPL = 140;
    const BROKER_HUNGARIAN_POST = 150;
    const BROKER_DPDNL = 180;
    const BROKER_LP_EXPRESS = 190;


    const BROKER_POST11 = 220;

    const UNIQID_INPOST_FMS = 50;

    const UNIQID_DHLDE_PAKET = 90;
    const UNIQID_DHLDE_PAKET_INT = 91;
    const UNIQID_DHLDE_EUROPAKET = 92;
    const UNIQID_DHLDE_PAKET_AT = 93;
    const UNIQID_DHLDE_PAKET_CONNECT_AT = 94;
    const UNIQID_DHLDE_PAKET_INT_AT = 95;

    const UNIQID_DHLEXPRESS = 100;
    const UNIQID_DHLEXPRESS_FB = 101;
    const UNIQID_DHLEXPRESS_SP = 102;

    const UNIQID_UK_MAIL = 110;
    const UNIQID_UK_MAIL_3RD = 111;
    const UNIQID_UK_MAIL_RETURN = 112;

    const UNIQID_PP_FIREBUSINESS_Polecona_Firmowa = 120;
    const UNIQID_PP_FIREBUSINESS_Polecona_Firmowa_PRIO = 121;
//    const UNIQID_PP_FIREBUSINESS_Pocztex24 = 122;
//    const UNIQID_PP_FIREBUSINESS_Pocztex48 = 123;

    const UNIQID_DPDDE_REGULAR = 130;

    const UNIQID_DHLDE_CARMAT_PAKET = 140;
    const UNIQID_DHLDE_CARMAT_PAKET_INT = 141;
    const UNIQID_DHLDE_CARMAT_PAKET_AT = 142;

    const UNIQID_DHLDE_GIMEDIA_PAKET = 143;
    const UNIQID_DHLDE_GIMEDIA_PAKET_INT = 144;
    const UNIQID_DHLDE_GIMEDIA_PAKET_AT = 145;

    const UNIQID_ROYALMAIL_24 = 150;
    const UNIQID_ROYALMAIL_48 = 151;
    const UNIQID_ROYALMAIL_TRACKED_RETURN_48 = 152;

    const UNIQID_DPDPL = 160;
    const UNIQID_DPDPL_V = 161;

    const UNIQID_HUNGARIAN_POST = 170;

    const UNIQID_DPDNL = 180;

    const UNIQID_LP_EXPRESS = 190;

    const UNIQID_SWEDEN_POST = 200;
    const UNIQID_DEUTCHE_POST = 210;

    const UNIQID_POST11 = 220;


    /**
     * List of operators that cannot be used normaly on routing
     * @return array
     */
    protected static function specialOperators()
    {
        return [
            self::OPERATOR_RUCH,
        ];
    }

    /**
     * @param $uniq_id
     * @param $broker_id
     * @return bool|string
     */
    public static function getLabelAnnotationByUniqId($uniq_id, $broker_id = false)
    {
        $CACHE_NAME = 'LABEL_ANNOTATION_COURIER_INTERNAL_OPERATOR_'.$uniq_id.'_'.$broker_id;
        $return = Yii::app()->cache->get($CACHE_NAME);

        if($return === false)
        {
            $cmd = Yii::app()->db->createCommand();

            if($uniq_id)
                $cmd->select('label_symbol')->from('courier_operator')->where('uniq_id = :uniq_id', [':uniq_id' => $uniq_id]);
            else
                $cmd->select('label_symbol')->from('courier_operator')->where('broker = :broker', [':broker' => $broker_id]);

            $return = $cmd->queryScalar();

            Yii::app()->cache->set($CACHE_NAME, $return, 60);
        }

        return $return;
    }

    /**
     * Returns array with ID's of operators for giver broker
     * @param $broker_id
     * @return array|CDbDataReader
     */
    public static function getOperatorsForBroker($broker_id)
    {
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        $cmd->select('id');
        $cmd->from((new self)->tableName());
        $cmd->where('broker = :broker', [':broker' => $broker_id]);
        return $cmd->queryColumn();
    }

    public static function getPocztaPolskaPrioOperators($byUniqId = false)
    {
        if($byUniqId)
        {
            return [
                self::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa_PRIO,
            ];
        }
        else
        {
            return [
                self::OPERATOR_PP_WROCLAW_Polecona_Firmowa_PRIO,
                self::OPERATOR_PP_BOCHNIA_Polecona_Firmowa_PRIO,
                self::OPERATOR_PP_NYSA_Polecona_Firmowa_PRIO,
                self::OPERATOR_PP_WARSZAWA_Polecona_Firmowa_PRIO
            ];
        }
    }

    public static function getBrokerList($onlyAvailableToAdd = false)
    {
        $data = [];

        $data[self::BROKER_OOE] = 'OOE';
        $data[self::BROKER_SAFEPAK] = 'Safepak';

        if(!$onlyAvailableToAdd) {
            $data[self::BROKER_UPS] = 'UPS';
            $data[self::BROKER_INPOST] = 'Inpost';
            $data[self::BROKER_GLS] = 'GLS';
            $data[self::BROKER_DHL] = 'DHL';
            $data[self::BROKER_POCZTA_POLSKA] = 'Poczta Polska';
            $data[self::BROKER_GLS_NL] = 'GLS NL';
            $data[self::BROKER_DHLDE] = 'DHL DE';
            $data[self::BROKER_DHLEXPRESS] = 'DHL EXPRESS';
            $data[self::BROKER_UK_MAIL] = 'UK Mail';
            $data[self::BROKER_DPD_DE] = 'DPD DE';
            $data[self::BROKER_ROYAL_MAIL] = 'Royal Mail';
            $data[self::BROKER_DPDPL] = 'DPD PL';
            $data[self::BROKER_DPDNL] = 'DPD NL';
            $data[self::BROKER_HUNGARIAN_POST] = 'Hungarian Post';
            $data[self::BROKER_LP_EXPRESS] = 'LP Express';
            $data[self::BROKER_POST11] = 'Post11';

            $data = $data + GLOBAL_BROKERS::getBrokersList();
        }



        return $data;
    }

    public static function getBrokerName($id)
    {
        return isset(self::getBrokerList()[$id]) ? self::getBrokerList()[$id] : '';
    }

    public static function getBrokerById($operator_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('broker');
        $cmd->from((new self)->tableName());
        $cmd->where('id = :id', [':id' => $operator_id]);
        return $cmd->queryScalar();
    }

    public static function getUniqIdById($operator_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('uniq_id');
        $cmd->from((new self)->tableName());
        $cmd->where('id = :id', [':id' => $operator_id]);
        return $cmd->queryScalar();
    }


    public function getEditable()
    {
        if($this->id == self::OPERATOR_OWN OR UpsSoapShipClient::isCourierOperatorUps($this->id))
            return false;
        else
            return true;
    }

    public function getNameWithIdAndBroker()
    {
        $broker = self::getBrokerName($this->broker);
        $name = $this->id.') '.$this->name;
        if($broker != '')
            $name .= ' ['.self::getBrokerName($this->broker).']';

        return $name;
    }

    public function wrapParams($ooe_login, $ooe_account, $ooe_pass, $ooe_service, $safepak_service)
    {
        $data = [
            'ooe_login' => $ooe_login,
            'ooe_account' => $ooe_account,
            'ooe_pass' => $ooe_pass,
            'ooe_service' => $ooe_service,
            'safepak_service' => $safepak_service,
        ];

        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    protected function unwrapParams()
    {
        $data = $this->params;
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    public function get_Ooe_login()
    {
        return $this->unwrapParams()['ooe_login'];
    }

    public function get_Ooe_account()
    {
        return $this->unwrapParams()['ooe_account'];
    }

    public function get_Ooe_pass()
    {
        return $this->unwrapParams()['ooe_pass'];
    }


    public function get_Ooe_service()
    {
        return $this->unwrapParams()['ooe_service'];
    }

    public function get_Safepak_service()
    {
        return $this->unwrapParams()['safepak_service'];
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('partner_id', 'in', 'range' => array_keys(Partners::getParntersList())),

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=> S_Status::ACTIVE, 'on'=>'insert'),

            array('name', 'required'),

            array('label_symbol', 'length', 'max'=>10),

            array('name, stat', 'length', 'max'=>45),
            array('broker', 'numerical'),
            array('date_updated, description', 'safe'),
            array('date_updated, description, params, uniq_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, description, broker, params, stat, label_symbol', 'safe', 'on'=>'search'),
        );
    }

    protected static $_listOperators = NULL;
    public static function listOperators()
    {
        if(self::$_listOperators === NULL)
        {
            $criteria = new CDbCriteria();
            $criteria->addNotInCondition("id", self::specialOperators());

            $models = self::model()->findAll($criteria);

            self::$_listOperators = $models;
        }
        return self::$_listOperators;
    }


    public static function bookOperators(CourierTypeInternal $courier, $omitParentCheck = false)
    {

        // if were ordered before in for example multiple packages
        if($courier->operators_ordered == true)
            return true;


        // MAKE SURE PARENT PACKAGE WAS BOOKED BEFORE
        if($courier->courier->packages_number > 1 && $courier->family_member_number > 1 && !$omitParentCheck)
        {
            /* @var $parentModel Courier */
            $parentModel = Courier::model()->findByPk($courier->parent_courier_id);
            self::bookOperators($parentModel->courierTypeInternal);

            // AFTER CHEKING PARENT, CALL THIS FUNCTION AGAIN FOR PROCESSSING THIS PACKAGE
            return self::bookOperators($courier, true);

        } else {

            // PICKUP_MOD / 14.09.2016
//            if ($courier->with_pickup) {
            if ($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR) {

                if ($courier->pickup_operator_id == $courier->delivery_operator_id) {
                    //Yii::log('_bookOperators_common', CLogger::LEVEL_ERROR);
                    $return = self::_bookOperators_common($courier);
                } else {
                    //Yii::log('_bookOperators_two', CLogger::LEVEL_ERROR);
                    $return = self::_bookOperators_two($courier);
                }

            }
            else if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_CONTRACT) {
                $return = self::_bookOperators_contract($courier);
            }
            else if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_RUCH)
            {
                $return = self::_bookOperators_ruch($courier);
            }
            else if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_INPOST)
            {
                $return = self::_bookOperators_inpost($courier);
            }
            else if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_DHLDE)
            {
                $return = self::_bookOperators_dhlde($courier);
            }
            else if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_RM) {
                $return = self::_bookOperators_rm($courier);
            }
            else if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_LP_EXPRESS) {
                $return = self::_bookOperators_lp_express($courier);
            }
            else if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_UPS)
            {
                $return = self::_bookOperators_ups_ap($courier);
            } else if($courier->with_pickup == CourierTypeInternal::WITH_PICKUP_NONE) {
                // Yii::log('_bookOperators_noPickup', CLogger::LEVEL_ERROR);
                $return = self::_bookOperators_noPickup($courier);
            } else
                throw new CHttpException(400, 'Pickup mode not valid!');

            return $return;
        }

    }

    protected static function _bookOperators_ruch(CourierTypeInternal $courier)
    {

        if($courier->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
        {

            $pickup_data = $courier->courier->senderAddressData;
            $delivery_data = $courier->courier->receiverAddressData;


            $courier->mode = self::TYPE_RUCH;
            $courier->update(array('mode'));

            $return = self::_bookOperator(self::TYPE_COMMON, CourierOperator::OPERATOR_RUCH, $courier, $pickup_data, $delivery_data);

            return $return;

        } else {

            // RUCH POINT for NON-DIRECT PACKAGES - CLa WARSZAWA
            $courier->_ruch_receiver_point = CourierTypeInternal::_WITH_PICKUP_RUCH_RECEIVER_POINT_ID;

            $courier->mode = self::TYPE_RUCH;
            $courier->update(); // update with params

            $pickup_data_to = SpPoints::getPoint(SpPoints::CENTRAL_SP_POINT_ID)->addressData;
            $pickup_data_from = $courier->courier->senderAddressData;


            $returnPickup = self::_bookOperator(self::MODE_PICKUP, CourierOperator::OPERATOR_RUCH, $courier, $pickup_data_from, $pickup_data_to, $courier->pickup_hub_id);

            $delivery_data_from = $pickup_data_to;
            $delivery_data_to = $courier->courier->receiverAddressData;

            $returnDelivery = self::_bookOperator(self::MODE_DELIVERY, $courier->delivery_operator_id, $courier, $delivery_data_from, $delivery_data_to);

            return ($returnDelivery && $returnPickup);
        }
    }

    protected static function _bookOperators_inpost(CourierTypeInternal $courier)
    {
        $operator = CourierOperator::model()->findByAttributes(['broker' => GLOBAL_BROKERS::BROKER_INPOST_SHIPX]);
        if($courier->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
        {
            $pickup_data = $courier->courier->senderAddressData;
            $delivery_data = $courier->courier->receiverAddressData;

            $courier->mode = self::TYPE_INPOST;
            $courier->update(array('mode'));

            $return = self::_bookOperator(self::TYPE_COMMON, $operator->id, $courier, $pickup_data, $delivery_data);

            return $return;
//
        } else {
//
            // PACZKOMAT for NON-DIRECT PACKAGES - NYSA
            $courier->_inpost_locker_delivery_id = CourierTypeInternal::_WITH_PICKUP_INPOST_PACZKOMAT_ID;
            $courier->_inpost_locker_active = 1;

            $courier->mode = self::TYPE_INPOST;
            $courier->update(); // update with params

            $pickup_data_to = new CourierTypeInternal_AddressData();

            $tempAddressData = SpPoints::getPoint(SpPoints::CENTRAL_SP_POINT_ID)->addressData;
            $pickup_data_to->attributes = $tempAddressData->attributes;
            $pickup_data_to->tel = '517505946';
            $pickup_data_to->save();

            $pickup_data_from = $courier->courier->senderAddressData;

            $returnPickup = self::_bookOperator(self::MODE_PICKUP, $operator->id, $courier, $pickup_data_from, $pickup_data_to, $courier->pickup_hub_id);

            $delivery_data_from = $pickup_data_to;
            $delivery_data_to = $courier->courier->receiverAddressData;

            $returnDelivery = self::_bookOperator(self::MODE_DELIVERY, $courier->delivery_operator_id, $courier, $delivery_data_from, $delivery_data_to);

            return ($returnDelivery && $returnPickup);
        }
    }

    protected static function _bookOperators_dhlde(CourierTypeInternal $courier)
    {
        $operator = CourierOperator::model()->findByAttributes(['broker' => GLOBAL_BROKERS::BROKER_DHLDE_RETOURE]);

        $courier->mode = self::TYPE_DHLDE;
        $courier->update(); // update with params

        $pickup_data_from = $courier->courier->senderAddressData;

        // FOR DHL DE RETOURE receiver address == sender address, because DHL DE Retoure takes only sender's data
        $returnPickup = self::_bookOperator(self::MODE_PICKUP, $operator->id, $courier, $pickup_data_from, $pickup_data_from, $courier->pickup_hub_id);

        $delivery_data_to = $courier->courier->receiverAddressData;
        $delivery_data_from = $courier->deliveryHub->address;

        $returnDelivery = self::_bookOperator(self::MODE_DELIVERY, $courier->delivery_operator_id, $courier, $delivery_data_from, $delivery_data_to);

        return ($returnDelivery && $returnPickup);

    }

    protected static function _bookOperators_rm(CourierTypeInternal $courier)
    {
        $operator = CourierOperator::model()->findByAttributes(['uniq_id' => CourierOperator::UNIQID_ROYALMAIL_TRACKED_RETURN_48]);
        if($courier->courier->receiverAddressData->country_id == CountryList::COUNTRY_UK)
        {

            $pickup_data = $courier->courier->senderAddressData;
            $delivery_data = $courier->courier->receiverAddressData;


            $courier->mode = self::TYPE_RM;
            $courier->update(array('mode'));

            $return = self::_bookOperator(self::TYPE_COMMON, $operator->id, $courier, $pickup_data, $delivery_data);

            return $return;

        } else {

            $courier->mode = self::TYPE_RM;
            $courier->update(); // update with params

            $pickup_data_to = SpPoints::getPoint(SpPoints::CENTRAL_SP_POINT_ID)->addressData;
            $pickup_data_from = $courier->courier->senderAddressData;

            $returnPickup = self::_bookOperator(self::MODE_PICKUP, $operator->id, $courier, $pickup_data_from, $pickup_data_to, false);

            $delivery_data_from = $courier->deliveryHub->address;
            $delivery_data_to = $courier->courier->receiverAddressData;

            $returnDelivery = self::_bookOperator(self::MODE_DELIVERY, $courier->delivery_operator_id, $courier, $delivery_data_from, $delivery_data_to);

            return ($returnDelivery && $returnPickup);
        }
    }

    protected static function _bookOperators_ups_ap(CourierTypeInternal $courier)
    {
//        $operator = CourierOperator::model()->findByAttributes(['uniq_id' => self::UNIQID_LP_EXPRESS]);
//
//        // WHEN DELIVERY OPERATOR IS ALSO LP_EXPRESS
//        if($operator->id == $courier->delivery_operator_id)
//        {
//
//            $pickup_data = $courier->courier->senderAddressData;
//            $delivery_data = $courier->courier->receiverAddressData;
//
//
//            $courier->mode = self::TYPE_LP_EXPRESS;
//            $courier->update(array('mode'));
//
//            $return = self::_bookOperator(self::TYPE_COMMON, $operator->id, $courier, $pickup_data, $delivery_data);
//
//            return $return;
//
//        } else {
//
//            $courier->mode = self::TYPE_LP_EXPRESS;
//            $courier->update(); // update with params
//
//            $pickup_data_to = $courier->pickupHub->address;
//            $pickup_data_from = $courier->courier->senderAddressData;
//
//            $returnPickup = self::_bookOperator(self::MODE_PICKUP, $operator->id, $courier, $pickup_data_from, $pickup_data_to, false);
//
//            $delivery_data_from = $courier->deliveryHub->address;
//            $delivery_data_to = $courier->courier->receiverAddressData;
//
//            $returnDelivery = self::_bookOperator(self::MODE_DELIVERY, $courier->delivery_operator_id, $courier, $delivery_data_from, $delivery_data_to);
//
//            return ($returnDelivery && $returnPickup);
//        }
    }

    protected static function _bookOperators_lp_express(CourierTypeInternal $courier)
    {
//        $operator = CourierOperator::model()->findByAttributes(['uniq_id' => self::UNIQID_LP_EXPRESS]);
//
//        // WHEN DELIVERY OPERATOR IS ALSO LP_EXPRESS
//        if($operator->id == $courier->delivery_operator_id)
//        {
//
//            $pickup_data = $courier->courier->senderAddressData;
//            $delivery_data = $courier->courier->receiverAddressData;
//
//
//            $courier->mode = self::TYPE_LP_EXPRESS;
//            $courier->update(array('mode'));
//
//            $return = self::_bookOperator(self::TYPE_COMMON, $operator->id, $courier, $pickup_data, $delivery_data);
//
//            return $return;
//
//        } else {
//
//            $courier->mode = self::TYPE_LP_EXPRESS;
//            $courier->update(); // update with params
//
//            $pickup_data_to = $courier->pickupHub->address;
//            $pickup_data_from = $courier->courier->senderAddressData;
//
//            $returnPickup = self::_bookOperator(self::MODE_PICKUP, $operator->id, $courier, $pickup_data_from, $pickup_data_to, false);
//
//            $delivery_data_from = $courier->deliveryHub->address;
//            $delivery_data_to = $courier->courier->receiverAddressData;
//
//            $returnDelivery = self::_bookOperator(self::MODE_DELIVERY, $courier->delivery_operator_id, $courier, $delivery_data_from, $delivery_data_to);
//
//            return ($returnDelivery && $returnPickup);
//        }
    }

    protected static function _bookOperators_contract(CourierTypeInternal $courier)
    {
        $pickup_data_from = $courier->courier->senderAddressData;
        $pickup_data_to = $courier->pickupHub->address;

        $courier->mode = self::TYPE_CONTRACT;
        $courier->update(array('mode'));

        $returnPickup = self::_bookOperator(self::MODE_PICKUP, CourierOperator::OPERATOR_OWN, $courier, $pickup_data_from, $pickup_data_to, $courier->pickup_hub_id);

        $delivery_data_from = $courier->deliveryHub->address;
        $delivery_data_to = $courier->courier->receiverAddressData;

        $returnDelivery = self::_bookOperator(self::MODE_DELIVERY, $courier->delivery_operator_id, $courier, $delivery_data_from, $delivery_data_to);

        return ($returnDelivery && $returnPickup);
    }

    protected static function _bookOperators_noPickup(CourierTypeInternal $courier)
    {
        $pickup_data = $courier->deliveryHub->address;
        $delivery_data = $courier->courier->receiverAddressData;

        $courier->mode = self::TYPE_NO_PICKUP;
        $courier->update(array('mode'));


        $return = self::_bookOperator(self::TYPE_NO_PICKUP, $courier->delivery_operator_id, $courier, $pickup_data, $delivery_data);

        return $return;

    }

    protected static function _bookOperators_common(CourierTypeInternal $courier)
    {
        $pickup_data = $courier->courier->senderAddressData;
        $delivery_data = $courier->courier->receiverAddressData;

        $courier->mode = self::TYPE_COMMON;
        $courier->update(array('mode'));

        $return = self::_bookOperator(self::TYPE_COMMON, $courier->delivery_operator_id, $courier, $pickup_data, $delivery_data);

        return $return;
    }


    protected static function _bookOperators_two(CourierTypeInternal $courier)
    {


        $pickup_data_from = $courier->courier->senderAddressData;
        $pickup_data_to = $courier->pickupHub->address;

        $courier->mode = self::TYPE_TWO;
        $courier->update(array('mode'));

        // do not call pick operator if user has active option of generating always second label (16.06.2016)
//        if($courier->courier->user === NULL OR !$courier->courier->user->getAllowCourierInternalExtendedLabel())
        $returnPickup = self::_bookOperator(self::MODE_PICKUP, $courier->pickup_operator_id, $courier, $pickup_data_from, $pickup_data_to, $courier->pickup_hub_id);
//        else
//            $returnPickup = true;

        $delivery_data_from = $courier->deliveryHub->address;
        $delivery_data_to = $courier->courier->receiverAddressData;

        $returnDelivery = self::_bookOperator(self::MODE_DELIVERY, $courier->delivery_operator_id, $courier, $delivery_data_from, $delivery_data_to);

        return ($returnDelivery && $returnPickup);
    }

    /**
     * @param $mode string Whether it's pickup, delivery or common operator
     * @param $operator int Selected operator
     * @param CourierTypeInternal $courier Model
     * @param AddressData $from Package from-addressData (sender or Hub)
     * @param AddressData $to Package to-addressData (receiver or Hub)
     * @param bool|false $deliveryHubId If delivery to hub, provide hub id. Otherwise, false.
     * @return bool
     * @throws CDbException
     */
    protected static function _bookOperator($mode, $operator, CourierTypeInternal $courier, AddressData $from, AddressData $to, $deliveryHubId = false)
    {

        if($operator == self::OPERATOR_OWN)
        {
            $isFirst = true;
            if ($mode == self::MODE_PICKUP) {

                $courier->pickup_operator_ordered = CourierTypeInternal::OPERATOR_OWN;
                $courier->operators_ordered = true;
                $courier->update(array('pickup_operator_ordered', 'operators_ordered'));
//                    $courier->courier->changeStat(CourierStat::PACKAGE_RETREIVE);

            } else if ($mode == self::MODE_DELIVERY) {

                $courier->delivery_operator_ordered = CourierTypeInternal::OPERATOR_OWN;
                $courier->operators_ordered = true;
                $courier->update(array('delivery_operator_ordered', 'operators_ordered'));

                if ($courier->_ebay_order_id != '')
                    self::updateEbayData($courier);

                $isFirst = false;
            } else if ($mode == self::TYPE_COMMON OR $mode == self::TYPE_NO_PICKUP) {

                $courier->common_operator_ordered = CourierTypeInternal::OPERATOR_OWN;
                $courier->operators_ordered = true;
                $courier->update(array('common_operator_ordered', 'operators_ordered'));
//                    $courier->courier->changeStat(CourierStat::PACKAGE_RETREIVE);

                if ($courier->_ebay_order_id != '')
                    self::updateEbayData($courier);
            }

            $courierLabelNew = new CourierLabelNew();

            $courierLabelNew->courier = $courier->courier;

            $courierLabelNew->operator = CourierLabelNew::OPERATOR_OWN;
            $courierLabelNew->stat = CourierLabelNew::STAT_NEW;
            $courierLabelNew->triggers_pickup = $isFirst;
            $courierLabelNew->courier_id = $courier->courier->id;
            $courierLabelNew->address_data_from_id = $from->id;
            $courierLabelNew->address_data_to_id = $to->id;

            if ($mode == self::MODE_DELIVERY)
                $courierLabelNew->_internal_mode = CourierLabelNew::INTERNAL_MODE_SECOND;
            else if ($mode == self::MODE_PICKUP)
                $courierLabelNew->_internal_mode = CourierLabelNew::INTERNAL_MODE_FIRST;
            else if ($mode == self::TYPE_COMMON)
                $courierLabelNew->_internal_mode = CourierLabelNew::INTERNAL_MODE_COMMON;
            else if($mode == self::TYPE_NO_PICKUP)
            {
                $courierLabelNew->_internal_mode = CourierLabelNew::INTERNAL_MODE_COMMON;
            }

            $courierLabelNew->save();

            $courierLabel = new CourierLabel();
//            $courierLabel->source = '_OWN';
            $courierLabel->courier_label_new_id = $courierLabelNew->id;
            $courierLabel->save();


            if ($mode == self::MODE_PICKUP) {
                $courier->pickup_label = $courierLabel->id;
                $courier->update(array('pickup_label'));
                return true;
            } else if ($mode == self::MODE_DELIVERY) {
                $courier->delivery_label = $courierLabel->id;
                $courier->update(array('delivery_label'));
                return true;
            } else if ($mode == self::TYPE_COMMON OR $mode == self::TYPE_NO_PICKUP) {
                $courier->common_label = $courierLabel->id;
                $courier->update(array('common_label'));
                return true;
            }


            return false;

        }
        ///////////////////////////////////////////////////////////////////////
        else
        {

            $operator_uniq_id = self::getUniqIdById($operator);

            // is it first label (of one of two) for this package
            $isFirst = true;
            if ($mode == self::MODE_PICKUP) {
                $courier->pickup_operator_ordered = CourierTypeInternal::OPERATOR_EXT;
                $courier->operators_ordered = true;
                $courier->update(array('pickup_operator_ordered', 'operators_ordered'));
            } else if ($mode == self::MODE_DELIVERY) {
                $courier->delivery_operator_ordered = CourierTypeInternal::OPERATOR_EXT;
                $courier->operators_ordered = true;
                $courier->update(array('delivery_operator_ordered', 'operators_ordered'));
                $isFirst = false;
            } else if ($mode == self::TYPE_COMMON OR $mode == self::TYPE_NO_PICKUP) {
                $courier->common_operator_ordered = CourierTypeInternal::OPERATOR_EXT;
                $courier->operators_ordered = true;
                $courier->update(array('common_operator_ordered', 'operators_ordered'));
            }

            // FOR MULTIPACKAGES AND ORDER IS MADE ON FIRST PACKAGE
            // OMIT REAL ORDERING BECAUSE IT HAS ALREADY BEEN DONE ON FIRST PACKAGE IN FAMILY
            if ($courier->courier->packages_number > 1 && $courier->family_member_number > 1) {

                return true;

            } else {

                $courierLabelNew = new CourierLabelNew();

                // package comes from eBay, so provide ebay order id, if it's not first of two labels, to update ebay.com with tracking id data from second or only one label
                if ($mode != self::MODE_PICKUP)
                    if ($courier->_ebay_order_id != '')
                        $courierLabelNew->_ebay_order_id = $courier->_ebay_order_id;

                if ($deliveryHubId)
                    $courierLabelNew->_deliveryHubId = $deliveryHubId;

                if ($mode == self::MODE_DELIVERY)
                    $courierLabelNew->_internal_mode = CourierLabelNew::INTERNAL_MODE_SECOND;
                else if ($mode == self::MODE_PICKUP)
                    $courierLabelNew->_internal_mode = CourierLabelNew::INTERNAL_MODE_FIRST;
                else if ($mode == self::TYPE_COMMON)
                    $courierLabelNew->_internal_mode = CourierLabelNew::INTERNAL_MODE_COMMON;
                else if($mode == self::TYPE_NO_PICKUP)
                {
                    $courierLabelNew->_internal_mode = CourierLabelNew::INTERNAL_MODE_COMMON;
                }

                /* @var $courier Courier */

                $courierLabelNew->triggers_pickup = $isFirst;

                $brokerType = self::getBrokerById($operator);

                $data = [];
                $data['courier_operator_id'] = $operator;
                $data['operator_uniq_id'] = $operator_uniq_id;
                $courierLabelNew->data = CourierLabelNew::serialize($data);
                $courierLabelNew->operator = self::brokerToCourierLabelNewOperator($brokerType, $operator, $operator_uniq_id);
                $courierLabelNew->courier_id = $courier->courier->id;
                $courierLabelNew->address_data_from_id = $from->id;
                $courierLabelNew->address_data_to_id = $to->id;
                $courierLabelNew->stat = CourierLabelNew::STAT_NEW;
                $courierLabelNew->save();

                $courierLabel = new CourierLabel();
//                $courierLabel->source = self::brokerToSourceName($brokerType, $operator, $operator_uniq_id);
                $courierLabel->courier_label_new_id = $courierLabelNew->id;
                $courierLabel->save();


                $parent = $courierLabelNew;


                ////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////
                // SAVE LABEL FOR NEXT PACKAGES IN THIS MULTIPACKAGE FAMILY
                if ($courier->courier->packages_number > 1 && $courier->family_member_number == 1) {
                    $familyPackages = CourierTypeInternal::model()->findAll(array(
                        'condition' => 'parent_courier_id = :parent',
                        'params' => array(':parent' => $courier->courier->id),
                        'order' => 'family_member_number ASC'));

                    /* @var $familyPackage CourierTypeInternal */
                    foreach ($familyPackages AS $familyPackage) {
                        {
                            $labelNewItem = new CourierLabelNew();

                            if ($mode == self::MODE_DELIVERY)
                                $labelNewItem->_internal_mode = CourierLabelNew::INTERNAL_MODE_SECOND;
                            else if ($mode == self::MODE_PICKUP)
                                $labelNewItem->_internal_mode = CourierLabelNew::INTERNAL_MODE_FIRST;
                            else if ($mode == self::TYPE_COMMON)
                                $labelNewItem->_internal_mode = CourierLabelNew::INTERNAL_MODE_COMMON;
                            else if($mode == self::TYPE_NO_PICKUP)
                            {
                                $labelNewItem->_internal_mode = CourierLabelNew::INTERNAL_MODE_COMMON;
                            }

                            $labelNewItem->triggers_pickup = $isFirst;
                            $labelNewItem->parent_id = $parent->id;

                            if(self::isMultipackageBroker($brokerType, $familyPackage->courier, $operator_uniq_id))
                                $labelNewItem->stat = CourierLabelNew::STAT_WAITING_FOR_PARENT;
                            else
                                $labelNewItem->stat = CourierLabelNew::STAT_NEW;
//
                            $labelNewItem->operator = self::brokerToCourierLabelNewOperator($brokerType, $operator, $operator_uniq_id);
                            $labelNewItem->courier_id = $familyPackage->courier->id;
                            $labelNewItem->address_data_from_id = $from->id;
                            $labelNewItem->address_data_to_id = $to->id;

                            $data = [];
                            $data['courier_operator_id'] = $operator;
                            $data['operator_uniq_id'] = $operator_uniq_id;
                            $labelNewItem->data = CourierLabelNew::serialize($data);

                            $labelNewItem->save();
                        }

                        $familyCourierLabel = new CourierLabel();
//                        $familyCourierLabel->source = self::brokerToSourceName($brokerType, $operator, $operator_uniq_id);//
                        $familyCourierLabel->courier_label_new_id = $labelNewItem->id;
                        $familyCourierLabel->save();

                        if ($mode == self::MODE_PICKUP) {
                            $familyPackage->pickup_label = $familyCourierLabel->id;
                            $familyPackage->update(array('pickup_label'));
                        } else if ($mode == self::MODE_DELIVERY) {
                            $familyPackage->delivery_label = $familyCourierLabel->id;
                            $familyPackage->update(array('delivery_label'));
                        } else if ($mode == self::TYPE_COMMON OR $mode == self::TYPE_NO_PICKUP) {
                            $familyPackage->common_label = $familyCourierLabel->id;
                            $familyPackage->update(array('common_label'));
                        }
                    }
                    ////////////////////////////////////////////////////////////
                    ////////////////////////////////////////////////////////////
                }

                if ($mode == self::MODE_PICKUP) {
                    $courier->pickup_label = $courierLabel->id;
                    $courier->update(array('pickup_label'));
                    return true;
                } else if ($mode == self::MODE_DELIVERY) {
                    $courier->delivery_label = $courierLabel->id;
                    $courier->update(array('delivery_label'));
                    return true;
                } else if ($mode == self::TYPE_COMMON OR $mode == self::TYPE_NO_PICKUP) {
                    $courier->common_label = $courierLabel->id;
                    $courier->update(array('common_label'));
                    return true;
                }

            }
        }

        return false;

    }

    protected static function getMultipackageBrokers()
    {
        $data = [
            self::BROKER_UPS,
            self::BROKER_GLS,
            self::BROKER_DHL,
            self::BROKER_INPOST,
            self::BROKER_DHLDE,
            self::BROKER_DHLEXPRESS,
            self::BROKER_UK_MAIL,
            self::BROKER_DPD_DE,
            self::BROKER_DPDPL,
            self::BROKER_DPDNL,
            self::BROKER_ROYAL_MAIL,
            self::BROKER_HUNGARIAN_POST,
            self::BROKER_GLS_NL,
            self::BROKER_POST11,
        ];

        $data = array_merge($data, GLOBAL_BROKERS::getMultipackageBrokers());
        return $data;
    }

    protected static function isMultipackageBroker($brokerId, Courier $courier, $operator_uniq_id = false)
    {

        if($brokerId == self::BROKER_UPS && ($courier->senderAddressData->country_id != CountryList::COUNTRY_PL OR $courier->receiverAddressData->country_id != CountryList::COUNTRY_PL))
            return false;

        if($brokerId == self::BROKER_UK_MAIL && $operator_uniq_id != self::UNIQID_UK_MAIL)
            return false;

        return in_array($brokerId, self::getMultipackageBrokers());
    }

    public static function brokerToCourierLabelNewOperator($broker, $operator_id = false, $operator_uniq_id = false)
    {


        if ($broker == self::BROKER_UPS) {
            $labelNewOperator = UpsSoapShipClient::mapCourierOperatorUpsToCourierLabelNewOperator($operator_id);
        } else if ($broker == self::BROKER_INPOST)
            $labelNewOperator = CourierLabelNew::OPERATOR_INPOST;
        else if ($broker == self::BROKER_GLS)
            $labelNewOperator = CourierLabelNew::OPERATOR_GLS;
        else if ($operator_id == self::OPERATOR_DHL)
            $labelNewOperator = CourierLabelNew::OPERATOR_DHL;
        else if ($operator_id == self::OPERATOR_DHL_F)
            $labelNewOperator = CourierLabelNew::OPERATOR_DHL_F;
        else if ($operator_id == self::OPERATOR_RUCH)
            $labelNewOperator = CourierLabelNew::OPERATOR_RUCH;
        else if ($operator_id == self::OPERATOR_PETER_SK)
            $labelNewOperator = CourierLabelNew::OPERATOR_PETER_SK;
        else if ($broker == self::BROKER_OOE)
            $labelNewOperator = CourierLabelNew::OPERATOR_OOE;
        else if ($broker == self::BROKER_INPOST)
            $labelNewOperator = CourierLabelNew::OPERATOR_INPOST;
        else if($broker == self::BROKER_POCZTA_POLSKA)
            $labelNewOperator = CourierLabelNew::OPERATOR_POCZTA_POLSKA;
        else if($broker == self::BROKER_GLS_NL)
            $labelNewOperator = CourierLabelNew::OPERATOR_GLS_NL;
        else if($broker == self::BROKER_DHLDE)
            $labelNewOperator = CourierLabelNew::OPERATOR_DHLDE;
        else if($broker == self::BROKER_DHLEXPRESS)
            $labelNewOperator = CourierLabelNew::OPERATOR_DHLEXPRESS;
        else if($broker == self::BROKER_UK_MAIL)
            $labelNewOperator = CourierLabelNew::OPERATOR_UK_MAIL;
        else if($broker == self::BROKER_DPD_DE)
            $labelNewOperator = CourierLabelNew::OPERATOR_DPD_DE;
        else if($broker == self::BROKER_ROYAL_MAIL)
            $labelNewOperator = CourierLabelNew::OPERATOR_ROYALMAIL;
        else if($broker == self::BROKER_DPDPL)
            $labelNewOperator = CourierLabelNew::OPERATOR_DPDPL;
        else if($broker == self::BROKER_DPDNL)
            $labelNewOperator = CourierLabelNew::OPERATOR_DPDNL;
        else if($broker == self::BROKER_LP_EXPRESS)
            $labelNewOperator = CourierLabelNew::OPERATOR_LP_EXPRESS;
        else if($broker == self::BROKER_POST11)
            $labelNewOperator = CourierLabelNew::OPERATOR_POST11;
        else if($broker == self::BROKER_HUNGARIAN_POST)
            $labelNewOperator = CourierLabelNew::OPERATOR_HUNGARIAN_POST;
        else if(in_array($broker, array_keys(GLOBAL_BROKERS::getBrokersList()))) // for items from GLOBAL_BROKERS
            return $broker;
        else if($operator_id == self::OPERATOR_OWN)
            $labelNewOperator = CourierLabelNew::OPERATOR_OWN;

        return $labelNewOperator;
    }

    /**
     * Used to update information about package into ebay, when local operator is used
     *
     * @param CourierTypeInternal $courier
     * @return bool
     * @throws CDbException
     */
    protected static function updateEbayData(CourierTypeInternal $courier)
    {
        $operator = 'SwiatPrzesylek.pl';

        if(Courier::updateEbayData($courier->courier, $operator, $courier->courier->local_id, $courier->_ebay_order_id))
        {
            $courier->_ebay_export_status = EbayImport::STAT_SUCCESS;
            $courier->update();

            return true;
        } else {
            $courier->_ebay_export_status = EbayImport::STAT_ERROR;
            $courier->update();

            return false;
        }


    }
}