<?php

class bDefaultTelNumberAddressData extends bBaseBehavior
{

    protected $_allowedClass = [
        'CourierTypeInternalSimple_AddressData',
        'CourierTypeInternal_AddressData',
        'CourierTypeOoe_AddressData',
        'CourierTypeDomestic_AddressData',
        'CourierTypeU_AddressData',
        'Postal_AddressData',
    ];

    public function afterValidate($event)
    {
        /* @var $that AddressData */
        $that = $this->owner();

        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest && $that->isAttributeRequired('tel') && $that->tel == '')
        {
            $defaultTel = User::getDefaultTelNoByUserId(Yii::app()->user->id);
            if($defaultTel != '')
            {
                $that->tel = $defaultTel;
                $that->clearErrors('tel');
            }
        }

        return parent::afterValidate($event);
    }


    }