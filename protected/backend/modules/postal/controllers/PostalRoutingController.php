<?php

class PostalRoutingController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> Admin::AUTHORITY_OPERATOR.' >= Yii::app()->user->authority',
                'actions' => ['view', 'index','update'],
                'verbs' => ['get'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->redirect(['/postal/postalRouting/update', 'id' => $id]);
    }

    public function actionIndex()
    {
        $model = new CountryGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['CountryGroup']))
            $model->setAttributes($_GET['CountryGroup']);

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id, $user_group = NULL)
    {

        $countryGroupModel = CountryGroup::model()->findByPk($id);
        if($countryGroupModel === NULL)
            throw new CHttpException(404);

        $userGroupModel = NULL;
        if($user_group !== NULL)
        {
            $userGroupModel = PostalUserGroup::model()->findByPk($user_group);

            if($userGroupModel === NULL)
                throw new CHttpException(404);
        }

        $models = [];
        foreach(Postal::getPostalTypeList() AS $postalType => $postalTypeName)
        {
            $models[$postalType] = [];

            foreach(Postal::getSizeClassList() AS $sizeClass => $sizeClassName)
            {
                $models[$postalType][$sizeClass] = [];

                foreach(Postal::getWeightClassList() AS $weightClass => $weightClassName)
                {
                    $models[$postalType][$sizeClass][$weightClass] = PostalRouting::fetchOrCreateModel($id, $postalType, $sizeClass, $weightClass, $user_group);
                }
            }
        }


        // Clicked button for just sort
        if(isset($_POST['delete']))
        {
            if(PostalRouting::deleteUserGroupRouting($id, $user_group))
            {
                Yii::app()->user->setFlash('success','Pricing was deleted!');
            } else {
                Yii::app()->user->setFlash('error','Pricing was  NOT deleted!');
            }
            $this->redirect(array('/postal/postalRouting/update','id' => $id));
            return;
        }

        $groupActionListView = '';
        $groupEditURLs = [];

        /* @var $item PostalUserGroup */
        foreach(PostalUserGroup::getListForThisAdmin() AS $item)
        {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('*');
            $cmd->from((new PostalRouting())->tableName());
            $cmd->where('country_group_id = :group', array(':group' => $id));
            $cmd->andWhere('postal_user_group_id = :user_group', array(':user_group' => $item->id));
            $result = $cmd->queryRow();

            $temp = [];
            $temp['name'] = $item->getNameWithAdmin().' ('.($result?'yes':'no').')';
            $temp['url'] = Yii::app()->createAbsoluteUrl('/postal/postalRouting/update', array('id' => $id, 'user_group' => $item->id));
            array_push($groupEditURLs, $temp);
        }

        if($user_group == NULL)
        {
            $groupActionDelete = true;
            $groupActionListView = $this->renderPartial('_groupActionList', array(
                'groupEditUrls' => $groupEditURLs
            ),
                true);
        } else {
            $groupActionDelete = true;
        }


        if(isset($_POST['PostalRouting']))
        {
            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;
            foreach(Postal::getPostalTypeList() AS $postalType => $postalTypeName)
            {
                foreach(Postal::getSizeClassList() AS $sizeClass => $sizeClassName)
                {
                    foreach(Postal::getWeightClassList() AS $weightClass => $weightClassName)
                    {
                        if(Postal::getWeightClassValueByClass($weightClass) > Postal::getMaxWeightForSizeClass($sizeClass))
                            continue;

                        $models[$postalType][$sizeClass][$weightClass]->attributes = $_POST['PostalRouting'][$postalType][$sizeClass][$weightClass];
                        $models[$postalType][$sizeClass][$weightClass]->price = Price::generateByPost($_POST['PriceValue'][$postalType][$sizeClass][$weightClass]);


                        $temp = $models[$postalType][$sizeClass][$weightClass]->scenario;
                        $models[$postalType][$sizeClass][$weightClass]->scenario = PostalRouting::SCENARIO_JUST_VALIDATE;
                        if(!$models[$postalType][$sizeClass][$weightClass]->validate()) {
                            $errors = true;
                        }

                        $models[$postalType][$sizeClass][$weightClass]->scenario = $temp;
                        $models[$postalType][$sizeClass][$weightClass]->price->validateWithChildren();
                        if(!$models[$postalType][$sizeClass][$weightClass]->price->saveWithPrices()) {
                            $errors = true;
                        } else {
                            $models[$postalType][$sizeClass][$weightClass]->price_id = $models[$postalType][$sizeClass][$weightClass]->price->id;
                        }
                    }
                }
            }

            if($errors)
            {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', 'Correct errrors!');

            } else {
                foreach(Postal::getPostalTypeList() AS $postalType => $postalTypeName)
                {
                    foreach(Postal::getSizeClassList() AS $sizeClass => $sizeClassName)
                    {
                        foreach(Postal::getWeightClassList() AS $weightClass => $weightClassName)
                        {
                            if(Postal::getWeightClassValueByClass($weightClass) > Postal::getMaxWeightForSizeClass($sizeClass))
                                continue;
                            $models[$postalType][$sizeClass][$weightClass]->save();
                        }
                    }
                }
                $transaction->commit();
                Yii::app()->user->setFlash('success', 'Changes have been saved!');
                $this->refresh();
            }

        }

        $this->render('update',
            [
                'models' => $models,
                'countryGroupModel' => $countryGroupModel,
                'groupActionListView' => $groupActionListView,
                'groupActionDelete' => $groupActionDelete,
                'userGroup' => $user_group,
                'userGroupModel' => $userGroupModel,
            ]);
    }

    const COUNTRY = 0;
    const TYPE = 1;
    const SIZE = 2;
    const WEIGHT = 3;
    const OPERATOR = 4;
    const PRICE_PLN = 5;
    const PRICE_EUR = 6;
    const PRICE_GBP = 7;


    protected static function postalTypeNameToId($name)
    {
        $name = strtoupper($name);

        if(in_array($name, ['S', 'STD']))
            return Postal::POSTAL_TYPE_STD;
        else if(in_array($name, ['P', 'PRIO']))
            return Postal::POSTAL_TYPE_PRIO;
        else if(in_array($name, ['R', 'REG']))
            return Postal::POSTAL_TYPE_REG;
        else
            return false;
    }

    protected static function postalSizeNameToId($name)
    {
        $name = strtoupper($name);

        if($name == 'A')
            return Postal::SIZE_CLASS_1;
        elseif($name == 'B')
            return Postal::SIZE_CLASS_2;
        else if($name == 'C')
            return Postal::SIZE_CLASS_3;
        else
            return false;
    }

    protected static function postalTypeIdToName($id)
    {
        if($id == Postal::POSTAL_TYPE_STD)
            return 'STD';
        else if($id == Postal::POSTAL_TYPE_PRIO)
            return 'PRIO';
        else if($id == Postal::POSTAL_TYPE_REG)
            return 'REG';
        else
            return false;
    }

    protected static function postalSizeIdToName($id)
    {
        if($id == Postal::SIZE_CLASS_1)
            return 'A';
        elseif($id == Postal::SIZE_CLASS_2)
            return 'B';
        else if($id == Postal::SIZE_CLASS_3)
            return 'C';
        else
            return false;
    }

    public function actionImport($hash = false, $ugi = NULL)
    {

        if($hash == false)
        {

            $formModel = new F_RoutingImport(F_RoutingImport::SCENARIO_POSTAL);

            if(isset($_POST['F_RoutingImport'])) {
                $formModel->attributes = $_POST['F_RoutingImport'];
                $formModel->file = CUploadedFile::getInstance($formModel, 'file');

                if($formModel->validate())
                {
                    $fileData = $formModel->fileToArray($formModel->file->tempName);
                    $hash = hash('sha512', uniqid(time(),true));
                    Yii::app()->cacheUserData->set('PRI_'.$hash, $fileData, 60*60);
                    Yii::app()->cacheUserData->set('PRI_ORG_FILE_'.$hash, file_get_contents($formModel->file->tempName), 60*60);
                    Yii::app()->cacheUserData->set('PRI_ORG_FILE_EXT_'.$hash, $formModel->file->extensionName, 60*60);

                    $this->redirect(['/postal/postalRouting/import', 'hash' => $hash, 'ugi' => $formModel->user_group_id]);
                    exit;
                }
            }

            if(isset(Yii::app()->session['CRI']))
            {
                if(isset(Yii::app()->session['PRI_GET'])) {

                    $path = file_get_contents(Yii::app()->session['CRI']);
                    Yii::app()->session['CRI'] = false;
                    Yii::app()->session['PRI_GET'] = false;
                    unset(Yii::app()->session['CRI']);
                    unset(Yii::app()->session['PRI_GET']);
                    $filename = 'backup_postal_routing_'.date('YmdHis').'.csv';
                    header("Content-Type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=\"".$filename."\";");
                    header("Content-Transfer-Encoding: binary");
                    exit($path);
                } else {
                    Yii::app()->session['PRI_GET'] = true;
                }
            }

            $this->render('import_step1', [
                'model' => $formModel,
            ]);
            exit;



        } else {


            $backup = [];

            $file = Yii::app()->cacheUserData->get('PRI_'.$hash);


            if(!S_Useful::sizeof($file))
            {

                Yii::app()->user->setFlash('error', 'File looks empty...');
                $errors = true;
            }

            if($ugi == -1)
                $ugi = NULL;

            $postal_user_group_id = $ugi;


            $data = [];
            foreach($file AS $line)
            {

                foreach($line AS $key => $temp)
                {
                    $temp = explode('_', $temp);
                    $item[$key] = $temp[0];
                }

                if(!isset($data[$item[self::COUNTRY]]))
                    $data[$item[self::COUNTRY]] = [];

                if(!isset($data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])]))
                    $data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])] = [];

                if(!isset($data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])][self::postalSizeNameToId($item[self::SIZE])]))
                    $data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])][self::postalSizeNameToId($item[self::SIZE])] = [];

                if(!isset($data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])][self::postalSizeNameToId($item[self::SIZE])][$item[self::WEIGHT]]))
                    $data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])][self::postalSizeNameToId($item[self::SIZE])][$item[self::WEIGHT]] = [];

                if(!isset($data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])][self::postalSizeNameToId($item[self::SIZE])][$item[self::WEIGHT]][$item[self::OPERATOR]]))
                    $data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])][self::postalSizeNameToId($item[self::SIZE])][$item[self::WEIGHT]][$item[self::OPERATOR]] = [];

                $data[$item[self::COUNTRY]][self::postalTypeNameToId($item[self::TYPE])][self::postalSizeNameToId($item[self::SIZE])][$item[self::WEIGHT]] = [
                    'O' => $item[self::OPERATOR],
                    'P_PLN' => round($item[self::PRICE_PLN],2),
                    'P_EUR' => round($item[self::PRICE_EUR],2),
                    'P_GBP' => round($item[self::PRICE_GBP],2),
                ];


            }

            $countries = array_keys($data);
            $countriesModels = [];

            foreach($countries AS $country_id) {

                $countryData = $data[$country_id];

                $models = [];
                foreach (Postal::getPostalTypeList() AS $postalType => $postalTypeName) {
                    $models[$postalType] = [];

                    foreach (Postal::getSizeClassList() AS $sizeClass => $sizeClassName) {
                        $models[$postalType][$sizeClass] = [];

                        foreach (Postal::getWeightClassList() AS $weightClass => $weightClassName) {
                            $models[$postalType][$sizeClass][$weightClass] = PostalRouting::fetchOrCreateModel($country_id, $postalType, $sizeClass, $weightClass, $postal_user_group_id);
                        }
                    }
                }



                foreach (Postal::getPostalTypeList() AS $postalType => $postalTypeName) {
                    foreach (Postal::getSizeClassList() AS $sizeClass => $sizeClassName) {
                        foreach (Postal::getWeightClassList() AS $weightClass => $weightClassName) {
                            if (Postal::getWeightClassValueByClass($weightClass) > Postal::getMaxWeightForSizeClass($sizeClass))
                                continue;


                            $backup[] = [
                                $country_id,
                                self::postalTypeIdToName($postalType),
                                self::postalSizeIdToName($sizeClass),
                                $weightClass,
                                $models[$postalType][$sizeClass][$weightClass]->postal_operator_id,
                                $models[$postalType][$sizeClass][$weightClass]->price ? $models[$postalType][$sizeClass][$weightClass]->price->getValue(Currency::PLN_ID)->value : '',
                                $models[$postalType][$sizeClass][$weightClass]->price ? $models[$postalType][$sizeClass][$weightClass]->price->getValue(Currency::EUR_ID)->value : '',
                                $models[$postalType][$sizeClass][$weightClass]->price ? $models[$postalType][$sizeClass][$weightClass]->price->getValue(Currency::GBP_ID)->value : '',
                            ];

                            $price = [];
                            $price[Price::PLN] = ['value' => $countryData[$postalType][$sizeClass][$weightClass]['P_PLN']];
                            $price[Price::EUR] = ['value' => $countryData[$postalType][$sizeClass][$weightClass]['P_EUR']];
                            $price[Price::GBP] = ['value' => $countryData[$postalType][$sizeClass][$weightClass]['P_GBP']];

                            $attributes = ['postal_operator_id' => $countryData[$postalType][$sizeClass][$weightClass]['O']];

                            $models[$postalType][$sizeClass][$weightClass]->attributes = $attributes;
                            $models[$postalType][$sizeClass][$weightClass]->price = Price::generateByPost($price);


                            $temp = $models[$postalType][$sizeClass][$weightClass]->scenario;
                            $models[$postalType][$sizeClass][$weightClass]->scenario = PostalRouting::SCENARIO_JUST_VALIDATE;
                            if (!$models[$postalType][$sizeClass][$weightClass]->validate()) {
                                $errors = true;
                            }

                            $models[$postalType][$sizeClass][$weightClass]->scenario = $temp;
                            $models[$postalType][$sizeClass][$weightClass]->price->validateWithChildren();
                            if (!$models[$postalType][$sizeClass][$weightClass]->price->saveWithPrices()) {
                                $errors = true;
                            } else {
                                $models[$postalType][$sizeClass][$weightClass]->price_id = $models[$postalType][$sizeClass][$weightClass]->price->id;
                            }
                        }
                    }
                }
                $countriesModels[$country_id] = $models;
            }


            if(isset($_POST['confirm-import']))
            {
                if ($_POST['confirmation'] == 1) {

                    $transaction = Yii::app()->db->beginTransaction();
                    $errors = false;

                    foreach($countries AS $country_id) {
                        foreach (Postal::getPostalTypeList() AS $postalType => $postalTypeName) {
                            foreach (Postal::getSizeClassList() AS $sizeClass => $sizeClassName) {
                                foreach (Postal::getWeightClassList() AS $weightClass => $weightClassName) {
                                    if (Postal::getWeightClassValueByClass($weightClass) > Postal::getMaxWeightForSizeClass($sizeClass))
                                        continue;

                                    $temp = $countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->scenario;
                                    $countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->scenario = PostalRouting::SCENARIO_JUST_VALIDATE;
                                    if (!$countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->validate()) {
                                        $errors = true;
                                    }

                                    $countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->scenario = $temp;
                                    $countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->price->validateWithChildren();
                                    if (!$countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->price->saveWithPrices()) {
                                        $errors = true;
                                    } else {
                                        $countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->price_id = $countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->price->id;
                                    }
                                }
                            }
                        }
                    }

                    if($errors)
                    {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', 'Correct errrors!');

                    } else {
                        foreach($countries AS $country_id) {
                            foreach(Postal::getPostalTypeList() AS $postalType => $postalTypeName)
                            {
                                foreach(Postal::getSizeClassList() AS $sizeClass => $sizeClassName)
                                {
                                    foreach(Postal::getWeightClassList() AS $weightClass => $weightClassName)
                                    {
                                        if(Postal::getWeightClassValueByClass($weightClass) > Postal::getMaxWeightForSizeClass($sizeClass))
                                            continue;

                                        $countriesModels[$country_id][$postalType][$sizeClass][$weightClass]->save();
                                    }
                                }
                            }
                        }

                        // save backup
                        $backupText = '';
                        foreach($backup AS $line)
                            $backupText .= implode(';', $line).PHP_EOL;

                        $backupPath = 'postal_routing_backup_' . date('YmdHis') . '.csv';
                        file_put_contents($backupPath, $backupText);

                        $transaction->commit();

                        $orgFileData = Yii::app()->cacheUserData->get('PRI_ORG_FILE_'.$hash);
                        $backupFileData = $backupText;

                        $time = date('Y-m-d-H-i-s');
                        $filenameOrg = 'NEW_import_data_'.$time.'.'.Yii::app()->cacheUserData->get('PRI_ORG_FILE_EXT_'.$hash);
                        $filenameBackup = 'BACKUP_import_data_'.$time.'.csv';

                        $to = [
                            'jankopec@swiatprzesylek.pl',
                            'piotrkocon@swiatprzesylek.pl',
                            'maciejszostak@swiatprzesylek.pl',
                            'michalwojciechowski@swiatprzesylek.pl',
                        ];

                        S_Mailer::send($to, $to, '[Changes in POSTAL][FILE]', 'Files in attachment<br/></br>User_group_id : '.$ugi.'<br/><br/>Admin_id : '.Yii::app()->user->id, false, NULL, NULL,true, false, [$filenameOrg, $filenameBackup], false, false, false, [$orgFileData, $backupFileData]);

//
                        Yii::app()->user->setFlash('success', 'Changes have been saved! Download previous setting backup.');
                        Yii::app()->session['PRI'] = $backupPath;
                        Yii::app()->cacheUserData->set('PRI_'.$hash, false);
                        $this->redirect(['/postal/postalRouting/import', 'hash' => false]);
                        Yii::app()->end();
                    }
                } else
                    Yii::app()->user->setFlash('error', 'Please know what you are doing :)');

            }

            $this->render('import', [
                'data' => $data,
                'countriesModels' => $countriesModels,
            ]);

        }
    }




    public function actionExport()
    {

        $formModel = new F_PostalRoutingExport;

        if(isset($_POST['F_PostalRoutingExport'])) {
            $formModel->attributes = $_POST['F_PostalRoutingExport'];

            if($formModel->validate())
            {
                $postal_user_group_id = $formModel->user_group_id;

                if($postal_user_group_id == -1)
                    $postal_user_group_id = NULL;


                foreach($formModel->country_list_id AS $country_id) {

                    $countryName = CountryGroup::model()->findByPk($country_id);
                    $countryName = $countryName->name;

                    $models = [];
                    foreach (Postal::getPostalTypeList() AS $postalType => $postalTypeName) {
                        $models[$postalType] = [];

                        foreach (Postal::getSizeClassList() AS $sizeClass => $sizeClassName) {
                            $models[$postalType][$sizeClass] = [];

                            foreach (Postal::getWeightClassList() AS $weightClass => $weightClassName) {
                                $models[$postalType][$sizeClass][$weightClass] = PostalRouting::fetchOrCreateModel($country_id, $postalType, $sizeClass, $weightClass, $postal_user_group_id);
                            }
                        }
                    }

                    foreach (Postal::getPostalTypeList() AS $postalType => $postalTypeName) {
                        foreach (Postal::getSizeClassList() AS $sizeClass => $sizeClassName) {
                            foreach (Postal::getWeightClassList() AS $weightClass => $weightClassName) {
                                if (Postal::getWeightClassValueByClass($weightClass) > Postal::getMaxWeightForSizeClass($sizeClass))
                                    continue;

                                if($models[$postalType][$sizeClass][$weightClass]->isNewRecord)
                                    continue;

                                $operatorName = PostalOperator::model()->findByPk($models[$postalType][$sizeClass][$weightClass]->postal_operator_id);
                                $operatorName = $operatorName->name;

                                $backup[] = [
                                    $country_id.'_'.$countryName,
                                    self::postalTypeIdToName($postalType),
                                    self::postalSizeIdToName($sizeClass),
                                    $weightClass.'_'.Postal::getWeightClassValueByClass($weightClass),
                                    $models[$postalType][$sizeClass][$weightClass]->postal_operator_id.'_'.$operatorName,
                                    $models[$postalType][$sizeClass][$weightClass]->price ? $models[$postalType][$sizeClass][$weightClass]->price->getValue(Currency::PLN_ID)->value : '',
                                    $models[$postalType][$sizeClass][$weightClass]->price ? $models[$postalType][$sizeClass][$weightClass]->price->getValue(Currency::EUR_ID)->value : '',
                                    $models[$postalType][$sizeClass][$weightClass]->price ? $models[$postalType][$sizeClass][$weightClass]->price->getValue(Currency::GBP_ID)->value : '',
                                ];
                            }
                        }
                    }

                }

                $arrayHeader = ['country_group_id','str/prio/reg','size_class', 'weight_class', 'operator_id', 'price_PLN', 'price_EUR', 'price_GBP'];


                $writer = Box\Spout\Writer\WriterFactory::create(Box\Spout\Common\Type::XLSX); // for XLSX files
                $writer->openToBrowser('export.xls');
                $writer->addRow($arrayHeader);

                if(is_array($backup))
                    foreach($backup AS $key => $line)
                        $writer->addRow($line);

                $filename = 'export_postal_routing_' . date('YmdHis') . '.xlsx';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0'); //no cache

                $writer->close();
                Yii::app()->end();
            }
        }

        $this->render('export', [
            'model' => $formModel,
        ]);
        exit;
    }


}