<?php
$sid = uniqid();

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('/postal/postalScanner').'?sid='.$sid,
    'id'=>'user-scanner-form',
    'enableClientValidation' => true,
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => 'scanner-target',
    ]
)); ?>

    <h2><?= Yii::t('postal', 'Masowa obsługa przesyłek');?></h2>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

    <p><?= Yii::t('userScanner', 'Podaj numery przesyłek, dla których chcesz wykonać wybrane akcje. Przesyłki, które nie zostaną znalezione lub nie będą spełniały warunków zostaną pominięte');?></p>
    <p><?= Yii::t('userScanner', 'Maksymalna liczba przesyłek do przetworzenia na raz:');?> <?= PostalScannerController::MAX_NO;?></p>
    <br/>
    <iframe name="scanner-target" id="scanner-target" style="width: 0; height: 0; display: none;"></iframe>

    <div id="message-info" class="alert alert-info" style="display: none; margin: 15px 0;"></div>
    <div id="message-success" class="alert alert-success" style="display: none; margin: 15px 0;"></div>
    <div id="message-danger" class="alert alert-danger" style="display: none; margin: 15px 0;"></div>

    <div id="summary-block" style="display: none; padding-bottom: 15px;">
        <table class="table" style="max-width: 600px; margin: 15px auto;">
            <tr>
                <th class="text-right"><?= Yii::t('userScanner', 'Przetworzonych');?></th>
                <th class="text-center"><?= Yii::t('userScanner', 'Łącznie');?></th>
                <th><?= Yii::t('userScanner', 'Pominiętych');?></th>
            </tr>
            <tr style="font-size: 1.4em;">
                <td id="success-no" class="text-right" style="color: green;">0</td>
                <td id="total-no" class="text-center">0</td>
                <td id="fail-no" style="color: red;">0</td>
            </tr>
            <tr>
                <td><textarea id="success-list" style="width: 100%; resize: vertical; height: 150px; text-align: center;"></textarea></td>
                <td></td>
                <td><textarea id="fail-list" style="width: 100%; resize: vertical; height: 150px; text-align: center;"></textarea></td>
            </tr>
        </table>
    </div>
    <hr/>
    <div class="row text-center">
        <div class="col-xs-5">
            <?= Yii::t('userScanner', 'Wstawionych wierszy:');?> <strong id="numer-of-rows">0</strong><br/>
            <?= $form->textArea($model, 'items_ids', ['id' => 'items-ids', 'style' => 'padding: 15px 5px; text-align: center; width: 400px; font-size: 1.1em; font-weight: bold; height: 500px; resize: vertical;', 'placeholder' => Yii::t('userScanner', 'Podaj numery paczek. Każdy numer musi być w nowej linii.')]);?>
        </div>
        <div class="col-xs-7 text-left">
            <div id="action-buttons">
                <p><?= Yii::t('userScanner', 'Akcje dla przesyłek:');?></p>
                <?= CHtml::submitButton(Yii::t('userScanner','Pobierz Kartę Potwierdzenia'), ['class' => 'btn btn-large btn-primary', 'name' => 'mode_ack']);?> <br/> <br/>
                <?php
                if(Yii::app()->user->model->getManifestGenerationSetsStatus()):?>
                    <?= CHtml::submitButton(Yii::t('userScanner','Pobierz Kartę Potwierdzenia i zmień status'), ['class' => 'btn btn-large btn-primary', 'name' => 'mode_ack_stat']);?> <br/> <br/>
                <?php
                endif;
                ?>
                <?= CHtml::submitButton(Yii::t('userScanner','Pobierz Etykietę 10x15'), ['class' => 'btn btn-large btn-primary', 'name' => 'mode_label_10x15']);?> <br/> <br/>
                <?= CHtml::submitButton(Yii::t('userScanner','Eksportuj do XLS'), ['class' => 'btn btn-large btn-primary', 'name' => 'mode_xls']);?> <br/> <br/>
                <?= CHtml::submitButton(Yii::t('userScanner','Anuluj przesyłki'), ['class' => 'btn btn-large btn-primary', 'name' => 'mode_cancel']);?> <br/> <br/>
                <?= CHtml::submitButton(Yii::t('userScanner','Pobierz Książkę nadawczą').' (*)', ['class' => 'btn btn-large btn-primary', 'name' => 'mode_kn']);?> <br/> <br/>
            </div>
            <div id="too-much-warning" style="display: none;" class="alert alert-danger text-center">
                <?= Yii::t('userScanner', 'Za dużo przesyłek!');?>
            </div>
        </div>
    </div>

<?php
$this->endWidget(); ?>

    <div style="margin: 15px;" class="text-right">
        <small>* <?= Yii::t('userScanner', 'Dotyczy tylko przesyłek, w których operatorem jest Poczta Polska.');?></small>
    </div>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  statusCheck: '.CJSON::encode(Yii::app()->createUrl('/postal/postalScanner/status', ['sid' => $sid])).',
              },
               loader: {
                  textTop: '.CJSON::encode(Yii::t('userScanner', 'Trwa przetwarzanie...')).',
                  textBottom: '.CJSON::encode(Yii::t('userScanner', 'Proszę czekać...')).',
                  error:  '.CJSON::encode(Yii::t('userScanner', 'Wystąpił błąd. Proszę spróbować kontynuować. Jeżeli błąd będzie się powtarzał, prosimy o kontakt.')).',
              },
              stat: {
                 success: '.CJSON::encode(PostalScannerController::STATE_SUCCESS).',
                 fail: '.CJSON::encode(PostalScannerController::STATE_FAIL).',
              },
              no: {
                 max: '.CJSON::encode(PostalScannerController::MAX_NO).',
              },
              text: {
                 noItems: '.CJSON::encode(Yii::t('userScanner', 'Nie podano żadnego numeru przesyłki!')).',
              },
          };
      ',CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/user-scanner.js', CClientScript::POS_END);
?>