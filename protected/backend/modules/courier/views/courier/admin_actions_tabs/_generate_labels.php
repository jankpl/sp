<h4>Generate labels</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-labels',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courier/generateLabelsByGridView'),
));
?>

<?php echo CHtml::dropDownList('Courier[type]','',LabelPrinter::getLabelGenerateTypes(), array('style' => 'width: 120px; display: inline-block;'));?>

<?php
echo TbHtml::submitButton('Generate labels', array(
    'onClick' => 'js:
    $("#courier-ids-labels").val($("#courier").selGridView("getAllSelection").toString());
    ;',
    'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Courier[ids]','', array('id' => 'courier-ids-labels')); ?>
<?php
$this->endWidget();
?>
