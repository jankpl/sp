<?php

Yii::import('application.modules.courier.models._base.BaseCourierCodAvailability');

class CourierCodAvailability extends BaseCourierCodAvailability
{
    const OPERATOR_INTERNAL = 1;
    const OPERATOR_DOMESTIC = 2;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const CURR_PLN = 'PLN';
    const CURR_EUR = 'EUR';
    const CURR_GBP = 'GBP';
    const CURR_USD = 'USD';

    const CURR_AED = 'AED';
    const CURR_AFA = 'AFA';
    const CURR_ALL = 'ALL';
    const CURR_AMD = 'AMD';
    const CURR_ANG = 'ANG';
    const CURR_AON = 'AON';
    const CURR_ARS = 'ARS';
    const CURR_AUD = 'AUD';
    const CURR_AWG = 'AWG';
    const CURR_AZM = 'AZM';
    const CURR_BAM = 'BAM';
    const CURR_BBD = 'BBD';
    const CURR_BDT = 'BDT';
    const CURR_BGN = 'BGN';
    const CURR_BHD = 'BHD';
    const CURR_BIF = 'BIF';
    const CURR_BND = 'BND';
    const CURR_BOB = 'BOB';
    const CURR_BRL = 'BRL';
    const CURR_BSD = 'BSD';
    const CURR_BTN = 'BTN';
    const CURR_BWP = 'BWP';
    const CURR_BYN = 'BYN';
    const CURR_BZD = 'BZD';
    const CURR_CAD = 'CAD';
    const CURR_CDF = 'CDF';
    const CURR_CHF = 'CHF';
    const CURR_CLP = 'CLP';
    const CURR_CNY = 'CNY';
    const CURR_COP = 'COP';
    const CURR_CRC = 'CRC';
    const CURR_CSD = 'CSD';
    const CURR_CUP = 'CUP';
    const CURR_CVE = 'CVE';
    const CURR_CZK = 'CZK';
    const CURR_DJF = 'DJF';
    const CURR_DKK = 'DKK';
    const CURR_DOP = 'DOP';
    const CURR_DZD = 'DZD';
    const CURR_EEK = 'EEK';
    const CURR_EGP = 'EGP';
    const CURR_ERN = 'ERN';
    const CURR_ETB = 'ETB';
    const CURR_FJD = 'FJD';
    const CURR_GEL = 'GEL';
    const CURR_GHC = 'GHC';
    const CURR_GIP = 'GIP';
    const CURR_GMD = 'GMD';
    const CURR_GNF = 'GNF';
    const CURR_GTQ = 'GTQ';
    const CURR_GYD = 'GYD';
    const CURR_HKD = 'HKD';
    const CURR_HNL = 'HNL';
    const CURR_HRK = 'HRK';
    const CURR_HTG = 'HTG';
    const CURR_HUF = 'HUF';
    const CURR_IDR = 'IDR';
    const CURR_ILS = 'ILS';
    const CURR_INR = 'INR';
    const CURR_IQD = 'IQD';
    const CURR_IRR = 'IRR';
    const CURR_ISK = 'ISK';
    const CURR_JMD = 'JMD';
    const CURR_JOD = 'JOD';
    const CURR_JPY = 'JPY';
    const CURR_KES = 'KES';
    const CURR_KGS = 'KGS';
    const CURR_KHR = 'KHR';
    const CURR_KMF = 'KMF';
    const CURR_KRW = 'KRW';
    const CURR_KWD = 'KWD';
    const CURR_KZT = 'KZT';
    const CURR_LAK = 'LAK';
    const CURR_LBP = 'LBP';
    const CURR_LKR = 'LKR';
    const CURR_LRD = 'LRD';
    const CURR_LSL = 'LSL';
    const CURR_LTL = 'LTL';
    const CURR_LVL = 'LVL';
    const CURR_LYD = 'LYD';
    const CURR_MAD = 'MAD';
    const CURR_MDL = 'MDL';
    const CURR_MGA = 'MGA';
    const CURR_MKD = 'MKD';
    const CURR_MMK = 'MMK';
    const CURR_MNT = 'MNT';
    const CURR_MOP = 'MOP';
    const CURR_MRO = 'MRO';
    const CURR_MUR = 'MUR';
    const CURR_MVR = 'MVR';
    const CURR_MWK = 'MWK';
    const CURR_MXN = 'MXN';
    const CURR_MYR = 'MYR';
    const CURR_MZM = 'MZM';
    const CURR_NAD = 'NAD';
    const CURR_NGN = 'NGN';
    const CURR_NIO = 'NIO';
    const CURR_NOK = 'NOK';
    const CURR_NPR = 'NPR';
    const CURR_NZD = 'NZD';
    const CURR_OMR = 'OMR';
    const CURR_PAB = 'PAB';
    const CURR_PEN = 'PEN';
    const CURR_PGK = 'PGK';
    const CURR_PHP = 'PHP';
    const CURR_PKR = 'PKR';
    const CURR_PYG = 'PYG';
    const CURR_QAR = 'QAR';
    const CURR_RON = 'RON';
    const CURR_RUB = 'RUB';
    const CURR_RWF = 'RWF';
    const CURR_SAR = 'SAR';
    const CURR_SBD = 'SBD';
    const CURR_SCP = 'SCP';
    const CURR_SCR = 'SCR';
    const CURR_SDD = 'SDD';
    const CURR_SEK = 'SEK';
    const CURR_SGD = 'SGD';
    const CURR_SLL = 'SLL';
    const CURR_SOS = 'SOS';
    const CURR_SRG = 'SRG';
    const CURR_STD = 'STD';
    const CURR_SVC = 'SVC';
    const CURR_SYP = 'SYP';
    const CURR_SZL = 'SZL';
    const CURR_THB = 'THB';
    const CURR_TJS = 'TJS';
    const CURR_TMM = 'TMM';
    const CURR_TND = 'TND';
    const CURR_TOP = 'TOP';
    const CURR_TRY = 'TRY';
    const CURR_TTD = 'TTD';
    const CURR_TWD = 'TWD';
    const CURR_TZS = 'TZS';
    const CURR_UAH = 'UAH';
    const CURR_UGX = 'UGX';
    const CURR_UYU = 'UYU';
    const CURR_UZS = 'UZS';
    const CURR_VEB = 'VEB';
    const CURR_VND = 'VND';
    const CURR_VUV = 'VUV';
    const CURR_WST = 'WST';
    const CURR_XAF = 'XAF';
    const CURR_XCD = 'XCD';
    const CURR_XDR = 'XDR';
    const CURR_XOF = 'XOF';
    const CURR_XPF = 'XPF';
    const CURR_YER = 'YER';
    const CURR_ZAR = 'ZAR';
    const CURR_ZMK = 'ZMK';
    const CURR_ZWD = 'ZWD';

    public static function getCurrencyList()
    {
        return [
            self::CURR_PLN => self::CURR_PLN,
            self::CURR_EUR => self::CURR_EUR,
            self::CURR_GBP => self::CURR_GBP,
            self::CURR_USD => self::CURR_USD,

            self::CURR_AED => self::CURR_AED,
            self::CURR_AFA => self::CURR_AFA,
            self::CURR_ALL => self::CURR_ALL,
            self::CURR_AMD => self::CURR_AMD,
            self::CURR_ANG => self::CURR_ANG,
            self::CURR_AON => self::CURR_AON,
            self::CURR_ARS => self::CURR_ARS,
            self::CURR_AUD => self::CURR_AUD,
            self::CURR_AWG => self::CURR_AWG,
            self::CURR_AZM => self::CURR_AZM,
            self::CURR_BAM => self::CURR_BAM,
            self::CURR_BBD => self::CURR_BBD,
            self::CURR_BDT => self::CURR_BDT,
            self::CURR_BGN => self::CURR_BGN,
            self::CURR_BHD => self::CURR_BHD,
            self::CURR_BIF => self::CURR_BIF,
            self::CURR_BND => self::CURR_BND,
            self::CURR_BOB => self::CURR_BOB,
            self::CURR_BRL => self::CURR_BRL,
            self::CURR_BSD => self::CURR_BSD,
            self::CURR_BTN => self::CURR_BTN,
            self::CURR_BWP => self::CURR_BWP,
            self::CURR_BYN => self::CURR_BYN,
            self::CURR_BZD => self::CURR_BZD,
            self::CURR_CAD => self::CURR_CAD,
            self::CURR_CDF => self::CURR_CDF,
            self::CURR_CHF => self::CURR_CHF,
            self::CURR_CLP => self::CURR_CLP,
            self::CURR_CNY => self::CURR_CNY,
            self::CURR_COP => self::CURR_COP,
            self::CURR_CRC => self::CURR_CRC,
            self::CURR_CSD => self::CURR_CSD,
            self::CURR_CUP => self::CURR_CUP,
            self::CURR_CVE => self::CURR_CVE,
            self::CURR_CZK => self::CURR_CZK,
            self::CURR_DJF => self::CURR_DJF,
            self::CURR_DKK => self::CURR_DKK,
            self::CURR_DOP => self::CURR_DOP,
            self::CURR_DZD => self::CURR_DZD,
            self::CURR_EEK => self::CURR_EEK,
            self::CURR_EGP => self::CURR_EGP,
            self::CURR_ERN => self::CURR_ERN,
            self::CURR_ETB => self::CURR_ETB,
            self::CURR_FJD => self::CURR_FJD,
            self::CURR_GEL => self::CURR_GEL,
            self::CURR_GHC => self::CURR_GHC,
            self::CURR_GIP => self::CURR_GIP,
            self::CURR_GMD => self::CURR_GMD,
            self::CURR_GNF => self::CURR_GNF,
            self::CURR_GTQ => self::CURR_GTQ,
            self::CURR_GYD => self::CURR_GYD,
            self::CURR_HKD => self::CURR_HKD,
            self::CURR_HNL => self::CURR_HNL,
            self::CURR_HRK => self::CURR_HRK,
            self::CURR_HTG => self::CURR_HTG,
            self::CURR_HUF => self::CURR_HUF,
            self::CURR_IDR => self::CURR_IDR,
            self::CURR_ILS => self::CURR_ILS,
            self::CURR_INR => self::CURR_INR,
            self::CURR_IQD => self::CURR_IQD,
            self::CURR_IRR => self::CURR_IRR,
            self::CURR_ISK => self::CURR_ISK,
            self::CURR_JMD => self::CURR_JMD,
            self::CURR_JOD => self::CURR_JOD,
            self::CURR_JPY => self::CURR_JPY,
            self::CURR_KES => self::CURR_KES,
            self::CURR_KGS => self::CURR_KGS,
            self::CURR_KHR => self::CURR_KHR,
            self::CURR_KMF => self::CURR_KMF,
            self::CURR_KRW => self::CURR_KRW,
            self::CURR_KWD => self::CURR_KWD,
            self::CURR_KZT => self::CURR_KZT,
            self::CURR_LAK => self::CURR_LAK,
            self::CURR_LBP => self::CURR_LBP,
            self::CURR_LKR => self::CURR_LKR,
            self::CURR_LRD => self::CURR_LRD,
            self::CURR_LSL => self::CURR_LSL,
            self::CURR_LTL => self::CURR_LTL,
            self::CURR_LVL => self::CURR_LVL,
            self::CURR_LYD => self::CURR_LYD,
            self::CURR_MAD => self::CURR_MAD,
            self::CURR_MDL => self::CURR_MDL,
            self::CURR_MGA => self::CURR_MGA,
            self::CURR_MKD => self::CURR_MKD,
            self::CURR_MMK => self::CURR_MMK,
            self::CURR_MNT => self::CURR_MNT,
            self::CURR_MOP => self::CURR_MOP,
            self::CURR_MRO => self::CURR_MRO,
            self::CURR_MUR => self::CURR_MUR,
            self::CURR_MVR => self::CURR_MVR,
            self::CURR_MWK => self::CURR_MWK,
            self::CURR_MXN => self::CURR_MXN,
            self::CURR_MYR => self::CURR_MYR,
            self::CURR_MZM => self::CURR_MZM,
            self::CURR_NAD => self::CURR_NAD,
            self::CURR_NGN => self::CURR_NGN,
            self::CURR_NIO => self::CURR_NIO,
            self::CURR_NOK => self::CURR_NOK,
            self::CURR_NPR => self::CURR_NPR,
            self::CURR_NZD => self::CURR_NZD,
            self::CURR_OMR => self::CURR_OMR,
            self::CURR_PAB => self::CURR_PAB,
            self::CURR_PEN => self::CURR_PEN,
            self::CURR_PGK => self::CURR_PGK,
            self::CURR_PHP => self::CURR_PHP,
            self::CURR_PKR => self::CURR_PKR,
            self::CURR_PYG => self::CURR_PYG,
            self::CURR_QAR => self::CURR_QAR,
            self::CURR_RON => self::CURR_RON,
            self::CURR_RUB => self::CURR_RUB,
            self::CURR_RWF => self::CURR_RWF,
            self::CURR_SAR => self::CURR_SAR,
            self::CURR_SBD => self::CURR_SBD,
            self::CURR_SCP => self::CURR_SCP,
            self::CURR_SCR => self::CURR_SCR,
            self::CURR_SDD => self::CURR_SDD,
            self::CURR_SEK => self::CURR_SEK,
            self::CURR_SGD => self::CURR_SGD,
            self::CURR_SLL => self::CURR_SLL,
            self::CURR_SOS => self::CURR_SOS,
            self::CURR_SRG => self::CURR_SRG,
            self::CURR_STD => self::CURR_STD,
            self::CURR_SVC => self::CURR_SVC,
            self::CURR_SYP => self::CURR_SYP,
            self::CURR_SZL => self::CURR_SZL,
            self::CURR_THB => self::CURR_THB,
            self::CURR_TJS => self::CURR_TJS,
            self::CURR_TMM => self::CURR_TMM,
            self::CURR_TND => self::CURR_TND,
            self::CURR_TOP => self::CURR_TOP,
            self::CURR_TRY => self::CURR_TRY,
            self::CURR_TTD => self::CURR_TTD,
            self::CURR_TWD => self::CURR_TWD,
            self::CURR_TZS => self::CURR_TZS,
            self::CURR_UAH => self::CURR_UAH,
            self::CURR_UGX => self::CURR_UGX,
            self::CURR_UYU => self::CURR_UYU,
            self::CURR_UZS => self::CURR_UZS,
            self::CURR_VEB => self::CURR_VEB,
            self::CURR_VND => self::CURR_VND,
            self::CURR_VUV => self::CURR_VUV,
            self::CURR_WST => self::CURR_WST,
            self::CURR_XAF => self::CURR_XAF,
            self::CURR_XCD => self::CURR_XCD,
            self::CURR_XDR => self::CURR_XDR,
            self::CURR_XOF => self::CURR_XOF,
            self::CURR_XPF => self::CURR_XPF,
            self::CURR_YER => self::CURR_YER,
            self::CURR_ZAR => self::CURR_ZAR,
            self::CURR_ZMK => self::CURR_ZMK,
            self::CURR_ZWD => self::CURR_ZWD,
        ];
    }


    public static function getCurrencyMaxValueList()
    {
        return [
            self::CURR_PLN => 5000,
            self::CURR_EUR => 1000,
            self::CURR_GBP => 1000,
            self::CURR_USD => 1000,

            self::CURR_AED => 1000,
            self::CURR_AFA => 1000,
            self::CURR_ALL => 1000,
            self::CURR_AMD => 1000,
            self::CURR_ANG => 1000,
            self::CURR_AON => 1000,
            self::CURR_ARS => 1000,
            self::CURR_AUD => 1000,
            self::CURR_AWG => 1000,
            self::CURR_AZM => 1000,
            self::CURR_BAM => 1000,
            self::CURR_BBD => 1000,
            self::CURR_BDT => 1000,
            self::CURR_BGN => 1000,
            self::CURR_BHD => 1000,
            self::CURR_BIF => 1000,
            self::CURR_BND => 1000,
            self::CURR_BOB => 1000,
            self::CURR_BRL => 1000,
            self::CURR_BSD => 1000,
            self::CURR_BTN => 1000,
            self::CURR_BWP => 1000,
            self::CURR_BYN => 1000,
            self::CURR_BZD => 1000,
            self::CURR_CAD => 1000,
            self::CURR_CDF => 1000,
            self::CURR_CHF => 1000,
            self::CURR_CLP => 1000,
            self::CURR_CNY => 1000,
            self::CURR_COP => 1000,
            self::CURR_CRC => 1000,
            self::CURR_CSD => 1000,
            self::CURR_CUP => 1000,
            self::CURR_CVE => 1000,
            self::CURR_CZK => 10000,
            self::CURR_DJF => 1000,
            self::CURR_DKK => 1000,
            self::CURR_DOP => 1000,
            self::CURR_DZD => 1000,
            self::CURR_EEK => 1000,
            self::CURR_EGP => 1000,
            self::CURR_ERN => 1000,
            self::CURR_ETB => 1000,
            self::CURR_FJD => 1000,
            self::CURR_GEL => 1000,
            self::CURR_GHC => 1000,
            self::CURR_GIP => 1000,
            self::CURR_GMD => 1000,
            self::CURR_GNF => 1000,
            self::CURR_GTQ => 1000,
            self::CURR_GYD => 1000,
            self::CURR_HKD => 1000,
            self::CURR_HNL => 1000,
            self::CURR_HRK => 1000,
            self::CURR_HTG => 1000,
            self::CURR_HUF => 1000000,
            self::CURR_IDR => 1000,
            self::CURR_ILS => 1000,
            self::CURR_INR => 1000,
            self::CURR_IQD => 1000,
            self::CURR_IRR => 1000,
            self::CURR_ISK => 1000,
            self::CURR_JMD => 1000,
            self::CURR_JOD => 1000,
            self::CURR_JPY => 1000,
            self::CURR_KES => 1000,
            self::CURR_KGS => 1000,
            self::CURR_KHR => 1000,
            self::CURR_KMF => 1000,
            self::CURR_KRW => 1000,
            self::CURR_KWD => 1000,
            self::CURR_KZT => 1000,
            self::CURR_LAK => 1000,
            self::CURR_LBP => 1000,
            self::CURR_LKR => 1000,
            self::CURR_LRD => 1000,
            self::CURR_LSL => 1000,
            self::CURR_LTL => 1000,
            self::CURR_LVL => 1000,
            self::CURR_LYD => 1000,
            self::CURR_MAD => 1000,
            self::CURR_MDL => 1000,
            self::CURR_MGA => 1000,
            self::CURR_MKD => 1000,
            self::CURR_MMK => 1000,
            self::CURR_MNT => 1000,
            self::CURR_MOP => 1000,
            self::CURR_MRO => 1000,
            self::CURR_MUR => 1000,
            self::CURR_MVR => 1000,
            self::CURR_MWK => 1000,
            self::CURR_MXN => 1000,
            self::CURR_MYR => 1000,
            self::CURR_MZM => 1000,
            self::CURR_NAD => 1000,
            self::CURR_NGN => 1000,
            self::CURR_NIO => 1000,
            self::CURR_NOK => 1000,
            self::CURR_NPR => 1000,
            self::CURR_NZD => 1000,
            self::CURR_OMR => 1000,
            self::CURR_PAB => 1000,
            self::CURR_PEN => 1000,
            self::CURR_PGK => 1000,
            self::CURR_PHP => 1000,
            self::CURR_PKR => 1000,
            self::CURR_PYG => 1000,
            self::CURR_QAR => 1000,
            self::CURR_RON => 5000,
            self::CURR_RUB => 1000,
            self::CURR_RWF => 1000,
            self::CURR_SAR => 1000,
            self::CURR_SBD => 1000,
            self::CURR_SCP => 1000,
            self::CURR_SCR => 1000,
            self::CURR_SDD => 1000,
            self::CURR_SEK => 1000,
            self::CURR_SGD => 1000,
            self::CURR_SLL => 1000,
            self::CURR_SOS => 1000,
            self::CURR_SRG => 1000,
            self::CURR_STD => 1000,
            self::CURR_SVC => 1000,
            self::CURR_SYP => 1000,
            self::CURR_SZL => 1000,
            self::CURR_THB => 1000,
            self::CURR_TJS => 1000,
            self::CURR_TMM => 1000,
            self::CURR_TND => 1000,
            self::CURR_TOP => 1000,
            self::CURR_TRY => 1000,
            self::CURR_TTD => 1000,
            self::CURR_TWD => 1000,
            self::CURR_TZS => 1000,
            self::CURR_UAH => 10000,
            self::CURR_UGX => 1000,
            self::CURR_UYU => 1000,
            self::CURR_UZS => 1000,
            self::CURR_VEB => 1000,
            self::CURR_VND => 1000,
            self::CURR_VUV => 1000,
            self::CURR_WST => 1000,
            self::CURR_XAF => 1000,
            self::CURR_XCD => 1000,
            self::CURR_XDR => 1000,
            self::CURR_XOF => 1000,
            self::CURR_XPF => 1000,
            self::CURR_YER => 1000,
            self::CURR_ZAR => 1000,
            self::CURR_ZMK => 1000,
            self::CURR_ZWD => 1000,
        ];
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    public function rules() {
        return array(
            array('country_list_id, currency', 'required'),
            array('country_list_id', 'numerical', 'integerOnly'=>true),
            array('currency', 'in', 'range' => self::getCurrencyList()),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, country_list_id, currency', 'safe', 'on'=>'search'),
            array('country_list_id', 'unique'),
        );
    }

    public function getCurrencyMaxValue()
    {
        return isset(self::getCurrencyMaxValueList()[$this->currency]) ? self::getCurrencyMaxValueList()[$this->currency] : false;
    }

    public static function getCurrencyMaxValueById($currency)
    {
        return isset(self::getCurrencyMaxValueList()[$currency]) ? self::getCurrencyMaxValueList()[$currency] : false;
    }

    public function getCurrencyName()
    {
        return isset(self::getCurrencyList()[$this->currency]) ? self::getCurrencyList()[$this->currency] : false;
    }

    public static function getCurrencyNameById($id)
    {
        return isset(self::getCurrencyList()[$id]) ? self::getCurrencyList()[$id] : false;
    }

    public function getAvailableCountryList()
    {
        $countries = CHtml::listData(CountryList::model()->findAll(), 'id', 'name');

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('country_list_id');
        $cmd->from((new self)->tableName());
        if(!$this->isNewRecord)
            $cmd->where('id != :id', [':id' => $this->id]);
        $usedCountries = $cmd->queryColumn();
        $usedCountries = array_flip($usedCountries);

        $countries = array_diff_key($countries, $usedCountries);

        return $countries;
    }

    /**
     * @param $operator_type
     * @param $operator_id
     * @param $country_id
     * @return int|false Returns ID of currency or false if COD not available
     */
    public static function findCurrencyForOperatorAndCountry($operator_type, $operator_id, $country_id)
    {
        $cmd = Yii::app()->db->cache(60)->createCommand();
        $cmd->select('currency');
        $cmd->from((new self)->tableName());
        $cmd->leftJoin((new CourierCodAvailabilityOperator())->tableName(), 'courier_cod_availability.id = courier_cod_availability_id');
        $cmd->where('country_list_id = :country_list_id AND operator_type = :operator_type AND operator_id = :operator_id');
        $cmd->limit(1);

        $result = $cmd->queryScalar([
            ':country_list_id' => $country_id,
            ':operator_type' => $operator_type,
            ':operator_id' => $operator_id,
        ]);

        return $result;
    }

    /**
     * @param $currency Currency symbol
     * @param $operator_type
     * @param $operator_id
     * @param $country_id
     * @return bool
     */
    public static function validateCurrencyNameForOperatorAndCountry($currency, $operator_type, $operator_id, $country_id)
    {
        $result = self::findCurrencyForOperatorAndCountry($operator_type, $operator_id, $country_id);

        if($result)
        {
            $result = self::getCurrencyNameById($result);

            if(self::compareCurrences($currency, $result))
                return true;
        }
        return false;
    }

    /**
     * @param $currency Currency symbol
     * @param $country_id
     * @return bool
     */
    public static function validateCurrencyNameForCountry($currency,  $country_id)
    {
        $result = self::findCurrencyForCountry($country_id);

        if($result)
        {
            $result = self::getCurrencyNameById($result);

            if(self::compareCurrences($currency, $result))
                return true;
        }
        return false;
    }

    public function saveWithOperators(array $internal_operators)
    {
        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        if(!$this->save())
            $errors = true;

        if(!$errors)
        {
            $cmd = Yii::app()->db->createCommand();

            if(!$this->isNewRecord)
                $cmd->delete((new CourierCodAvailabilityOperator())->tableName(),'courier_cod_availability_id = :courier_cod_availability_id', array(':courier_cod_availability_id' => $this->id));

            $data = [];
            /* @var $item CountryList */
            foreach($internal_operators AS $item)
            {
                $data[] = [
                    'courier_cod_availability_id' => $this->id,
                    'operator_id' => $item,
                    'operator_type' => self::OPERATOR_INTERNAL,
                ];
            }

            if(S_Useful::sizeof($data)) {
                $cmd = Yii::app()->db->schema->commandBuilder->createMultipleInsertCommand((new CourierCodAvailabilityOperator())->tableName(), $data);
                $cmd->execute();
            }
        }

        if(!$errors)
        {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollBack();
            return false;
        }

    }

    public function deleteWithOperators()
    {
        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $cmd = Yii::app()->db->createCommand();
        $cmd->delete((new CourierCodAvailabilityOperator())->tableName(),'id = :courier_cod_availability_id', array(':courier_cod_availability_id' => $this->id));

        if(!$this->delete())
            $errors = true;

        if(!$errors)
        {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollBack();
            return false;
        }

    }

    public static function compareCurrences($cur1, $cur2)
    {
        return (strcasecmp($cur1, $cur2) == 0);
    }



    /**
     * @param $country_id
     * @return int|false Returns ID of currency or false if COD not available
     */
    public static function findCurrencyForCountry($country_id)
    {
        $cmd = Yii::app()->db->cache(60)->createCommand();
        $cmd->select('currency');
        $cmd->from((new self)->tableName());
        $cmd->where('country_list_id = :country_list_id');
        $cmd->limit(1);

        $result = $cmd->queryScalar([
            ':country_list_id' => $country_id,
        ]);

        return $result;
    }

}