<?php
/* @var $models [] */
/* @var $model PostalRouting */
/* @var $countryGroupModel CountryGroup */
/* @var $form GxActiveForm */
/* @var $groupActionListView string */
/* @var $groupActionDelete boolean */
/* @var $userGroup int|boolean */
/* @var $userGroupModel PostalUserGroup */
?>

<?php

$postalTypeList = Postal::getPostalTypeList();
$postalTypeListSize = S_Useful::sizeof($postalTypeList);


?>

    <h2><?= $countryGroupModel->name;?></h2>
    <h3>For user group: <?php echo $userGroupModel !== NULL ? $userGroupModel->name:'-';?></h3>
<?php


foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php
if($groupActionListView!=''):
    ?>
    <div class="flash-notice">
        <p>Edit price for user group:</p>
        <?php echo $groupActionListView; ?>
    </div>
<?php
endif;
?>

<?php
if($userGroup === NULL && !AdminRestrictions::isRoutingAdmin()):
    ?>
    <div class="alert alert-warning text-center">You have no authority to modify main routing.</div>
<?php
else:
    ?>

    <div class="form">
        <?php $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'postal-routing-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
        ));
        ?>

        <?php

        $tabs = [];
        $first = true;
        foreach($postalTypeList AS $postalType => $postalTypeName) {
            $tabs[] = [
                'label' => $postalTypeName,
                'content' => $this->renderPartial('_update_type',
                    [
                        'models' => $models[$postalType],
                        'postalType' => $postalType,
                        'form' => $form,
                    ], true),
                'active' => $first ? true : false,
            ];
            $first = false;
        }

        echo TbHtml::tabbableTabs($tabs, array('placement' => TbHtml::TABS_PLACEMENT_ABOVE));
        ?>

        <?php
        if($groupActionDelete):
            ?>
            <div class="flash-notice">
                <p>Delete whole pricing for this group.</p>
                <?php
                echo CHtml::submitButton("Delete",array(
                    'name'=>'delete',
                    'onClick'=>'
                 if(confirm("Do you really want to delete all pricing?"))
                 {
                    $(this).closest(\'form\').attr(\'action\',
                    $(this).closest(\'form\').attr(\'action\'));
                 } else {
                 return false;
                 }',
                    'tabindex' => -1,
                ));
                ?>
            </div>

        <?php
        endif;
        ?>

        <div class="text-center">
            <?php echo CHtml::submitButton('Save', ['class' => 'btn btn-lg']); ?>
        </div>

        <?php
        $this->endWidget();
        ?>
    </div>

    <script>
        $('[data-copy-to-clipboard]').on('click', function(){
            var target = $(this).attr('data-copy-to-clipboard');
            var $target = $('[data-clipboard-target = "' + target + '"]');
            var dataPrice = $target.find('[name^="PriceValue"]').serializeArray();
            var dataOperator = $target.find('[name^="PostalRouting"]').serializeArray();

            localStorage.setItem('PR_Price', JSON.stringify(dataPrice));
            localStorage.setItem('PR_Operator', JSON.stringify(dataOperator));
            localStorage.setItem('PR_SourceTarget', target);

            alert("Copied to clipboard!");
        });

        $('[data-paste-from-clipboard]').on('click', function(){

            var target = $(this).attr('data-paste-from-clipboard');
            var $target = $('[data-clipboard-target = "' + target + '"]');

            var dataPrice = JSON.parse(localStorage.getItem('PR_Price'));
            var dataOperator = JSON.parse(localStorage.getItem('PR_Operator'));
            var sourceTarget = localStorage.getItem('PR_SourceTarget');



            if(dataPrice == null || dataOperator == null || sourceTarget == null)
            {
                alert("Data not found in clipboard!");
                return true;
            }

            $(dataPrice).each(function(i,v){
                var name = v.name;
                name = name.replace('PriceValue[' + sourceTarget + ']', 'PriceValue[' + target + ']');
                $target.find('[name="' + name + '"]').val(v.value);
            });

            $(dataOperator).each(function(i,v){
                var name = v.name;
                name = name.replace('PostalRouting[' + sourceTarget + ']', 'PostalRouting[' + target + ']');
                $target.find('[name="' + name + '"]').val(v.value);
            });

            alert("Done!");
        });


        $('[data-set-common-operator]').on('click', function(){
            var type = $(this).attr('data-set-common-operator');
            $('[data-postal-type-operator=' + type + ']').val($('[data-postal-type-common-operator=' + type + ']').val());
            alert("Done!");
        });

    </script>

<?php
endif;