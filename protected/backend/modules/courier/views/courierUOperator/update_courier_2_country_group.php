<?php
if($userGroup === NULL && !AdminRestrictions::isUltraAdmin()):
    ?>
    <div class="alert alert-warning text-center">You have no authority to modify main routing.</div>
<?php
else:
    ?>


    <?php
    /* @var $model CourierUOperator2CountryGroup */
    ?>

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <h2>Stat</h2>
    <div class="form">
        <?php $form = $this->beginWidget('GxActiveForm', array(
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->dropDownList($model, 'stat', CourierUOperator2CountryGroup::getStatList()); ?>
            <?php echo $form->error($model,'stat'); ?>
        </div><!-- row -->

        <div class="text-center">
            <?php
            echo GxHtml::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-lg', 'name' => 'stat']);
            $this->endWidget();
            ?>
        </div>
    </div><!-- form -->

    <hr/>

    <h2>Delivery Time</h2>
    <div class="form">
        <?php $form = $this->beginWidget('GxActiveForm', array(
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->textField($model, 'delivery_hours'); ?>
            <?php echo $form->error($model,'delivery_hours'); ?>
        </div><!-- row -->

        <div class="text-center">
            <?php
            echo GxHtml::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-lg', 'name' => 'delivery_hours']);
            $this->endWidget();
            ?>
        </div>
    </div><!-- form -->

    <hr/>

    <h2>COD</h2>
    <div class="form">
        <?php $form = $this->beginWidget('GxActiveForm', array(
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->dropDownList($model, 'cod', CourierUOperator2CountryGroup::getStatList()); ?>
            <?php echo $form->error($model,'cod'); ?>
        </div><!-- row -->

        <div class="text-center">
            <?php
            echo GxHtml::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-lg', 'name' => 'cod']);
            $this->endWidget();
            ?>
        </div>
    </div><!-- form -->

    <hr/>

    <h2>COD Price</h2>
    <div class="form">
        <?php $form = $this->beginWidget('GxActiveForm', array(
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php $this->widget('PriceForm', array(
                'form' => $form,
                'model' => $model->price,
                'formFieldPrefix' => '',
            ));?>
        </div><!-- row -->

        <div class="text-center">
            <?php
            echo GxHtml::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-lg', 'name' => 'cod_price']);
            $this->endWidget();
            ?>
        </div>
    </div><!-- form -->

<?php
endif;