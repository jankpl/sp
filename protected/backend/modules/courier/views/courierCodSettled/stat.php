<?php
/* @var int $currency */
?>

<h1>Statistics</h1>

<div style="overflow: auto;">
    <div style="width: 48%; float: left;">
        <?php
        $sumTotal = 0;
        $sumSettled = 0;
        $sumNotSettled = 0;
        ?>
        <table class="table table-striped table-bordered table-condensed " style="width: 525px;">
            <thead>
            <tr>
                <th style="text-align: right;">Status</th>
                <th style="text-align: center;">Items</th>
                <th style="text-align: center;">Unsettled</th>
                <th style="text-align: center;">Settled</th>
            </tr>
            </thead>
            <?php
            foreach(StatMap::mapNameList() AS $key => $name):
                $settled = CourierCodSettled::countNoByStatMap($key, true, false);
                $notSettled = CourierCodSettled::countNoByStatMap($key, false, false);
                $total = $settled + $notSettled;

                $sumTotal += $total;
                $sumNotSettled += $notSettled;
                $sumSettled += $settled;
                ?>
                <tr>
                    <td style="text-align: right;">[<?= $key;?>] <?= $name;?></td>
                    <td style="text-align: center;"><?= $total;?></td>
                    <td style="text-align: center;"><?= $notSettled;?></td>
                    <td style="text-align: center;"><?= $settled;?></td>
                </tr>
                <?php
            endforeach;

            $settled = CourierCodSettled::countNoByStatMap(false, true, false);
            $notSettled = CourierCodSettled::countNoByStatMap(false, false, false);
            $total = $settled + $notSettled;

            $sumTotal += $total;
            $sumNotSettled += $notSettled;
            $sumSettled += $settled;
            ?>
            <tr>
                <td style="text-align: right;">- not mapped -</td>
                <td style="text-align: center;"><?= $total;?></td>
                <td style="text-align: center;"><?= $notSettled;?></td>
                <td style="text-align: center;"><?= $settled;?></td>
            </tr>
            <tr>
                <th style="text-align: right;">Sum:</th>
                <th style="text-align: center;"><?= ($sumTotal);?></th>
                <th style="text-align: center;"><?= ($sumNotSettled);?></th>
                <th style="text-align: center;"><?= ($sumSettled);?></th>
            </tr>
        </table>
    </div>
    <div style="width: 48%; float: right;">Select currency:
        <?php
        foreach(CourierCodAvailability::getCurrencyList() AS $key => $item)
            echo CHtml::link($item, ['/courier/courierCodSettled/stat', 'currency' => $key], ['style' => $key == $currency ? 'font-weight: bold;' : '']).' | ';
        ?>

        <?php
        if($currency):
            ?>

            <?php
            $sumTotal = 0;
            $sumSettled = 0;
            $sumNotSettled = 0;
            ?>
            <table class="table table-striped table-bordered table-condensed " style="width: 525px;">
                <thead>
                <tr>
                    <th style="text-align: right;">Status</th>
                    <th style="text-align: center;">Total</th>
                    <th style="text-align: center;">Unsettled</th>
                    <th style="text-align: center;">Settled</th>
                </tr>
                </thead>
                <?php
                foreach(StatMap::mapNameList() AS $key => $name):
                    $settled = CourierCodSettled::countNoByStatMap($key, true, $currency, true);
                    $notSettled = CourierCodSettled::countNoByStatMap($key, false, $currency, true);
                    $total = $settled + $notSettled;

                    $sumTotal += $total;
                    $sumNotSettled += $notSettled;
                    $sumSettled += $settled;

                    ?>
                    <tr>
                        <td style="text-align: right;">[<?= $key;?>] <?= $name;?></td>
                        <td style="text-align: center;"><?= S_Price::formatPrice($total);?></td>
                        <td style="text-align: center;"><?= S_Price::formatPrice($notSettled);?></td>
                        <td style="text-align: center;"><?= S_Price::formatPrice($settled);?></td>
                    </tr>
                    <?php
                endforeach;

                $settled = CourierCodSettled::countNoByStatMap(false, true, $currency, true);
                $notSettled = CourierCodSettled::countNoByStatMap(false, false, $currency, true);
                $total = $settled + $notSettled;

                $sumTotal += $total;
                $sumNotSettled += $notSettled;
                $sumSettled += $settled;
                ?>
                <tr>
                    <td style="text-align: right;">- not mapped -</td>
                    <td style="text-align: center;"><?= S_Price::formatPrice($total);?></td>
                    <td style="text-align: center;"><?= S_Price::formatPrice($notSettled);?></td>
                    <td style="text-align: center;"><?= S_Price::formatPrice($settled);?></td>
                </tr>
                <tr>
                    <th style="text-align: right;">Sum:</th>
                    <th style="text-align: center;"><?= S_Price::formatPrice($sumTotal);?></th>
                    <th style="text-align: center;"><?= S_Price::formatPrice($sumNotSettled);?></th>
                    <th style="text-align: center;"><?= S_Price::formatPrice($sumSettled);?></th>
                </tr>
            </table>

            <?php
        endif;
        ?>
    </div>
</div>
