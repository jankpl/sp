<?php

class TranslationController extends Controller {


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $languages = Language::model()->findAll();

        $searchResult = '';


        if(isset($_POST['search']) && $_POST['needle'] != '')
        {
            $needle = $_POST['needle'];
            $needle = trim(str_replace('%','', $needle));

            $found = [];



            $languages = Language::model()->findAll();


            foreach($languages AS $item2)
            {

                $files = Translation::loadFilesList($item2->id);

                if(is_array($files))
                    foreach($files AS $file)
                    {

                        $search_lang = intval($_POST['search_lang']);

                        if(!in_array($search_lang, CHtml::listData($languages,'id','id')))
                            $search_lang = 1;

                        list($path, $array) = Translation::loadFileContentArray($search_lang, $file);

                        if(is_array($array))
                            foreach($array AS $key => $item)
                            {

                                if (preg_match("%".$needle."%i", $key)) {

                                    if(!is_array($found[$file]))
                                        $found[$file] = [];

                                    $found[$file][] = $key;
                                }

                                if (preg_match("%".$needle."%i", $item)) {

                                    if(!is_array($found[$file]))
                                        $found[$file] = [];

                                    $found[$file][] = $key;
                                }
                            }
                    }
            }
            $searchResult = 'Result for: "<strong>'.CHtml::encode(($needle)).'</strong>":<br/>';

            if(!S_Useful::sizeof($found))
                $searchResult .= '- NOTHING FOUND -';
            else
            {

                foreach($found AS $fileName => $content)
                    $searchResult .= '- <strong>' . $fileName . '</strong> : ("'.implode('", "', $content).'")<br/>';

            }

        }



        $this->render('index', array(
            'languages' => $languages,
            'searchResult' => $searchResult,
            'search_lang' => $search_lang,
        ));
    }

    public function actionAjaxLoadFiles()
    {

        $language_id = $_POST['language_id'];

        $files = Translation::loadFilesList($language_id);

        $form = CHtml::dropDownList('file','', $files);

        echo CJSON::encode(print_r($form, true));
    }

    public function actionAjaxEditTranslationForm()
    {
        $language_id = $_POST['language_id'];
        $file = $_POST['file'];

        list($path, $array) = Translation::loadFileContentArray($language_id, $file);

        $return = $this->renderPartial('ajaxEditTranslationForm',
            array(
                'array' => $array,
                'path' => $path,
            ),true);

        // 08.10.2016
        // base64_encode because some encoding in broken...
        echo CJSON::encode(base64_encode($return));

    }

    public function actionAjaxSaveTranslationForm()
    {
        $path = $_POST['path'];
        $array = $_POST['data'];

        $print = "<?php \n\r";
        $print .= 'return ';

        $print .= var_export($array, true);
        $print .= ';';

        if(Translation::saveTranslationToFile($path, $array))
        {
            $return = '<div class="flash-success">Translation has been saved!</div>';
        } else {
            $return = '<div class="flash-error">Error! Is file writable?</div>';
        }


        echo CJSON::encode($return);

    }

}