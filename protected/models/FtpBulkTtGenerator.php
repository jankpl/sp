<?php

Yii::import('application.models._base.BaseFtpBulkTtGenerator');

class FtpBulkTtGenerator extends BaseFtpBulkTtGenerator
{
    const TYPE_COURIER = 1;
    const TYPE_POSTAL = 2;

    const MINUTES_INTERVAL = 175;


    protected static function getDirBasepath($user_id)
    {
        return Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'ftpBulkTt'.DIRECTORY_SEPARATOR.$user_id;
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }


    public static function generateFile($user_id, $type)
    {
        $filename = $type.date('ymdhis');
        if($type == self::TYPE_COURIER)
        {
            Yii::app()->getModule('courier');

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('type_max_id, date_entered')->from('ftp_bulk_tt_generator')->where('user_id = :user_id AND type = :type', [
                    ':type' => $type,
                    ':user_id' => $user_id,
                ]
            )->order('id DESC')->limit(1);
            $result = $cmd->queryRow();

            $t1 = time();
            $t2 = strtotime($result['date_entered']);
            $diff = $t1 - $t2;

            $hours = $diff / 60;

            if($hours < self::MINUTES_INTERVAL)
                return [];

            $previous_max_id = $result['type_max_id'];

            if($previous_max_id === NULL)
                $previous_max_id = 0;

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('MAX(id)')->from('courier_stat_history');
            $current_max_id = $cmd->queryScalar();

            $models = CourierStatHistory::model()->with('courier')->findAll('courier.user_id = :user_id AND t.id > :previous_max_id AND courier.date_entered > (NOW() - INTERVAL 1 YEAR)', [':user_id' => $user_id, ':previous_max_id' => $previous_max_id]);

            $items_no = sizeof($models);

            $cmd = Yii::app()->db->createCommand();
            $cmd->insert('ftp_bulk_tt_generator', [
                'user_id' => $user_id,
                'type' => $type,
                'items_no' => $items_no,
                'file_name' => $items_no ? $filename : NULL,
                'type_max_id' => $current_max_id,
            ]);


        } else
            throw new Exception('Type not supported');

        if($items_no) {
            $dir = self::getDirBasepath($user_id);
            if (!is_dir($dir))
                mkdir($dir);


            $hasExternaNumbers = User::model()->findByPk($user_id)->getAllowCourierInternalExtendedLabel();

            $xml = self::_generateXml($models, $hasExternaNumbers);
            file_put_contents($dir . DIRECTORY_SEPARATOR . $filename . '.xml', $xml);

            $json = self::_generateJson($models, $hasExternaNumbers);
            file_put_contents($dir . DIRECTORY_SEPARATOR . $filename . '.json', $json);
        }
    }


    protected static function _generateXml(array $models, $withExternalNumbers = false)
    {
        $now = date('c');

        $statuses_no = sizeof($models);

        $data = [];
        $extIds = [];

        foreach($models AS $model)
        {
            if(!isset($data[$model->courier->local_id]))
                $data[$model->courier->local_id] = [];

            $data[$model->courier->local_id][] = $model;

            if($withExternalNumbers)
                $extIds[$model->courier->local_id] = array_shift($model->courier->getExternalOperatorsAndIds(true)[1]);
        }

        $items_no = sizeof($data);

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<tt>\n";
        $xml_output .= '<date>' . $now . '</date>' . "\n";
        $xml_output .= '<items_no>' . $items_no . '</items_no>' . "\n";
        $xml_output .= '<statuses_no>' . $statuses_no . '</statuses_no>' . "\n";
        $xml_output .= '<items>' . "\n";



        /* @var $item CourierStatHistory */
        foreach($data AS $id => $item) {

            if($withExternalNumbers) {
                $xml_output .= '<item id="' . $id . '" external_id="' . $extIds[$id] . '">' . "\n";
            } else
                $xml_output .= '<item id="' . $id . '">' . "\n";

            $xml_output .= '<statuses>' . "\n";
            foreach($item AS $status) {
                $xml_output .= '<status>' . "\n";
                $xml_output .= '<id>' . StatMapMapping::findMapForStat($status->current_stat_id, StatMapMapping::TYPE_COURIER) . '</id>' . "\n";
                $xml_output .= '<sid>' . $status->current_stat_id . '</sid>' . "\n";
                $xml_output .= '<date>' . $status->status_date . '</date>' . "\n";
                $xml_output .= '<name>' . CHtml::encode($status->currentStat->getStatText()) . '</name>' . "\n";
                $xml_output .= '<location>' . CHtml::encode($status->location) . '</location>' . "\n";
                $xml_output .= '</status>' . "\n";
            }
            $xml_output .= '</statuses>' . "\n";
            $xml_output .= '</item>' . "\n";
        }
        $xml_output .= '</items>' . "\n";
        $xml_output .= "</tt>";

        return $xml_output;
    }

    protected static function _generateJson(array $models, $withExternalNumbers = false)
    {
        $now = date('c');

        $statuses_no = sizeof($models);

        $data = [];
        $extIds = [];

        foreach($models AS $model)
        {
            if(!isset($data[$model->courier->local_id]))
                $data[$model->courier->local_id] = [];

            $data[$model->courier->local_id][] = $model;

            if($withExternalNumbers)
                $extIds[$model->courier->local_id] = array_shift($model->courier->getExternalOperatorsAndIds(true)[1]);
        }

        $items_no = sizeof($data);

        $output = [];
        $output['date'] = $now;
        $output['items_no'] = $items_no;
        $output['statuses_no'] = $statuses_no;
        $output['items'] = [];

        /* @var $item CourierStatHistory */
        foreach($data AS $id => $item) {

            $temp = [];
            $temp['id'] = $id;
            if($withExternalNumbers)
                $temp['external_id'] = $extIds[$id];

            $temp['statuses'] = [];

            foreach($item AS $status) {
                $temp['statuses'][] = [
                    'id' => StatMapMapping::findMapForStat($status->current_stat_id, StatMapMapping::TYPE_COURIER),
                    'sid' => $status->current_stat_id,
                    'date' => $status->status_date,
                    'name' => $status->currentStat->getStatText(),
                    'location' => $status->location,
                ];
            }
            $output['items'][] = $temp;
        }

        return CJSON::encode($output);
    }
}