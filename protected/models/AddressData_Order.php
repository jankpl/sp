<?php

class AddressData_Order extends AddressData
{
    public $_nip;

    const SCENRARIO_COMPANY = 1;
    const SCENARIO_ONE_OR_ANOTHER = 2;
    const SCENARIO_AT_LEAST_ONE = 3;

    public function init()
    {
        parent::init();
    }

    /**
     * Unserialize data
     * @param $data
     * @return null|string
     */
    public static function serialize($data)
    {
        if($data == '')
            return NULL;

        $data = serialize($data);
        $data = base64_encode($data);
        return $data;
    }

    /**
     * Serialize data
     * @param $data
     * @return mixed|string
     */
    public static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = @unserialize($data);
        return $data;
    }

    public function beforeSave()
    {

        $this->address_line_2 = mb_substr($this->address_line_1,45,45);
        $this->address_line_1 = mb_substr($this->address_line_1,0,45);

        $this->params = array(
            '_nip' => $this->_nip,
        );

//        // serialize only if not all values are empty
//        if (array_filter($this->params))
//            $this->params = self::serialize($this->params);
//        else
//            $this->params = NULL;

        return parent::beforeSave();
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->address_line_1 = trim($this->address_line_1.' '.$this->address_line_2);

        $this->_nip = $this->params['_nip'];

    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        if($this->scenario == self::SCENARIO_ONE_OR_ANOTHER)
        {
            if($this->name == '' AND $this->company == '')
            {
                $this->addError('name', 'Provide name or company!');
                $this->addError('company', 'Provide name or company!');
            }

            if($this->name != '' && $this->company != '')
            {
                $this->addError('name', 'Provide name or company!');
                $this->addError('company', 'Provide name or company!');
            }

            if($this->company != '' && $this->_nip == '')
                $this->addError('_nip', 'Provide NIP with company!');

        } elseif ($this->scenario == self::SCENARIO_AT_LEAST_ONE) {

            if($this->company == '' && $this->name == '') {
                $this->addError('name', 'Provide at least name or company!');
                $this->addError('company', 'Provide at least name or company!');
            }
        }


        return parent::afterValidate();
    }

    public function rules() {

        $arrayValidate = array(

            array('name, company, city, address_line_1, address_line_2, zip_code', 'application.validators.lettersNumbersBasicValidator'),

            array('country_id, city, address_line_1, zip_code', 'required'),

            array('name', 'required', 'except' => [self::SCENRARIO_COMPANY, self::SCENARIO_ONE_OR_ANOTHER, self::SCENARIO_AT_LEAST_ONE]),
            array('company, _nip', 'required', 'on' => self::SCENRARIO_COMPANY),

            array('name,company,country,zip_code,city,tel', 'length', 'max'=>45),
            array('address_line_1', 'length', 'max'=>90),
//            array('_nip', 'length', 'max'=>20),

            array('email', 'length', 'max'=>45),

            array('country_id', 'safe'),

            array('id,name,company,country,zip_code,city,address_line_1,address_line_2,tel, email,', 'safe', 'on'=>'search'),

            array('params', 'safe'),

//            array('_nip', 'match', 'pattern' => '/^((\d{3}-\d{3}-\d{2}-\d{2})|(\d{3}-\d{2}-\d{2}-\d{3})|(\d{10}))$/'),
            array('_nip', 'length', 'min' => 8, 'max' => 20),
//            array('zip_code', 'match', 'pattern' => '/^(\d{2}-\d{3})$/'),

            array(implode(',', array_keys(self::getAttributes())),'safe', 'on' => 'clone'),


        );


        $arrayFilter =
            array(
                array('country_id, date_entered, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'filter', 'filter' => array( $this, 'filterStripTags')),


                array('date_entered', 'default',
                    'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

                array('country_id, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'default', 'setOnEmpty' => true, 'value' => null),
            );

        return array_merge($arrayFilter, $arrayValidate);
    }


    public function attributeLabels()
    {
        return CMap::mergeArray(
            [
                '_nip' => Yii::t('order','NIP/VAT'),
            ],
            parent::attributeLabels());
    }


}