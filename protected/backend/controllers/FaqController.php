<?php

class FaqController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Faq'),
        ));
    }


    public function actionUpdate($id) {

        $model = $this->loadModel($id, 'Faq');

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = FaqTr::model()->find('faq_id=:faq_id AND language_id=:language_id', array(':faq_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $faqTr = new FaqTr();
                $faqTr->language_id = $item->id;
                $faqTr->faq_id = $id;
                $modelTrs[$item->id] = $faqTr;
            }
        }

        if (isset($_POST['Faq'])) {

            $model->setAttributes($_POST['Faq']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            foreach($_POST['FaqTr'] AS $key => $item)
            {
                if($item['title'] == '' AND $item['intro'] == '' AND $item['text'] == 0) continue;

                if($modelTrs[$key] === null) $modelTrs[$key] = new FaqTr;
                $modelTrs[$key]->setAttributes($item);


                if(!$modelTrs[$key]->save())
                    $errors = true;

            }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionCreate() {

        $model = new Faq;

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $faqTr = new FaqTr();
            $faqTr->language_id = $item->id;
            $modelTrs[$item->id] = $faqTr;
        }

        if (isset($_POST['Faq'])) {

            $model->setAttributes($_POST['Faq']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            if(!$model->save())
                $errors = true;

            if(!$errors)
                foreach($_POST['FaqTr'] AS $key => $item)
                {
                    if($item['title'] == '' AND $item['text'] == 0) continue;

                    if($modelTrs[$key] === null) $modelTrs[$key] = new FaqTr;
                    $modelTrs[$key]->setAttributes($item);
                    $modelTrs[$key]->faq_id = $model->id;

                    if(!$modelTrs[$key]->save())
                        $errors = true;

                }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }


    public function actionIndex() {

        $this->redirect(array('faq/admin'));
    }

    public function actionAdmin() {
        $model = new Faq('search');
        $model->unsetAttributes();

        if (isset($_GET['Faq']))
            $model->setAttributes($_GET['Faq']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionMoveT($id)
    {
        /* @var $model Faq */

        $model = Faq::model()->findByPk($id);
        if($model === NULL)
            throw new CHttpException(403);

        try
        {
            if(!$model->moveT())
                throw new CHttpException(403);
        }
        catch (Exception $ex)
        {
            throw new CHttpException(403);
        }

        $this->redirect(array('admin'));
    }

    public function actionMoveB($id)
    {
        /* @var $model Faq */

        $model = Faq::model()->findByPk($id);
        if($model === NULL)
            throw new CHttpException(403);

        try
        {
            if(!$model->moveB())
                throw new CHttpException(403);
        }
        catch (Exception $ex)
        {
            throw new CHttpException(403);
        }

        $this->redirect(array('admin'));
    }

    public function actionStat($id)
    {
        /* @var $model CountryList */


        $model = Faq::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}