<?php

class UpsSoapShipClient
{
    const HUB_NL = GLOBAL_CONFIG::HUB_NL_ID;
    const HUB_PL = GLOBAL_CONFIG::HUB_PL_ID;

    const SHIPPER_DEFAULT = 1;
    const SHIPPER_BIS = 2;
    const SHIPPER_F = 3;
    const SHIPPER_M = 4;
    const SHIPPER_UK = 5;
    const SHIPPER_L = 6;
    const SHIPPER_MCM = 7;
    const SHIPPER_4VALUES = 8;

    // REMEMBER TO BE UNIQUE
    const SERVICE_STANDARD = 10;
    const SERVICE_SAVER = 12;
    const SERVICE_EXPRESS = 15;

    const SERVICE_STANDARD_BIS = 30;
    const SERVICE_SAVER_BIS = 40;

    const SERVICE_STANDARD_F = 50;
    const SERVICE_SAVER_F = 60;

    const SERVICE_STANDARD_M = 70;
    const SERVICE_SAVER_M = 80;

    const SERVICE_EXPRESS_M = 90;

    const SERVICE_STANDARD_UK = 100;

    const SERVICE_EXPRESS_L = 110;

    const SERVICE_SAVER_MCM = 120;

    const SERVICE_SAVER_4VALUES = 130;
    // REMEMBER TO BE UNIQUE

    //////////////////////////////////////////////////////////
    const MODE_STANDARD = 1;
    const MODE_SAVER = 2;
    const MODE_EXPRESS = 3;

//Configuration
    protected $access = GLOBAL_CONFIG::UPS_ACCESS;
    protected $userid = GLOBAL_CONFIG::UPS_USERID;
    protected $passwd = GLOBAL_CONFIG::UPS_PASS;
    protected $wsdl = "";
    protected $operation = "ProcessShipment";
    protected $endpointurl = GLOBAL_CONFIG::UPS_ENDPOINT;
    protected $outputFileName = "XOLTResult.xml";

    protected $shipper = "R2E500";
    protected $shipper_name = "KAAB";
    protected $shipper_address = "ul. Grodkowska 40";
    protected $shipper_address_2 = "";
    protected $shipper_city = "NYSA";
    protected $shipper_postal = "48300";
    protected $shipper_country = "PL";
    protected $shipper_tel = "+48221221218";

    protected $shipper_attention_name = false;

    protected $_sender_attention_name = false;

    public $_user_id;

    public $_sender_name;
    public $_sender_addressLine;
    public $_sender_addressLine2;
    public $_sender_postalCode;
    public $_sender_countryCode;
    public $_sender_number;
    public $_sender_city;

    public $_receiver_name;
    public $_receiver_attention_name = '';
    public $_receiver_addressLine;
    public $_receiver_addressLine2;
    public $_receiver_postalCode;
    public $_receiver_countryCode;
    public $_receiver_number;
    public $_receiver_city;

    public $_receiver_mail;

    public $_package_size_l;
    public $_package_size_w;
    public $_package_size_h;
    public $_package_weight;
    public $_package_desc;

    public $_reference_no;

    public $_reference_no_2 = false;

    public $_cod;

    public $_cod_currency;
    public $_cod_amount;

    public $_number = 1;

    public $_number_of_multipackages = 1;

    public $_service_name = 'Standard';
    public $_service_code = '11';

    public $_saturday_delivery = false;

    public $_declared_value = false;


    public $_returnService = false;

    public $_rod = false;

    public $_requireSignature = false;

    public $_notification = false;

    public function setModeType($type)
    {
        if($type == self::MODE_STANDARD)
        {
            $this->_service_name = 'Standard';
            $this->_service_code = '11';
        }
        else if($type == self::MODE_SAVER)
        {
            $this->_service_name = 'Saver';
            $this->_service_code = '65';
        }
        else if($type == self::MODE_EXPRESS)
        {
            $this->_service_name = 'Express';
            $this->_service_code = '07';
        }
    }


    public static function _shipperData($shipper)
    {
        $obj = new stdClass();
        switch($shipper)
        {
            case self::SHIPPER_BIS:
                $obj->access = '3D0B34B6A351D368';
                $obj->userid = 'sp@arrows.com.pl';
                $obj->passwd = 'Swiatprzesylek001@';
                $obj->shipper = 'Y7A709';
                $obj->shipper_name = 'ARROWS';
                $obj->shipper_address = 'Grodkowska 40';
                $obj->shipper_city = 'Nysa';
                $obj->shipper_postal = '48300';
                $obj->shipper_country = 'PL';
                $obj->shipper_tel = '+48503443993';
                break;
            case self::SHIPPER_F:
                $obj->access = '4D0F15465739E458';
                $obj->userid = 'FB2016';
                $obj->passwd = 'FireBusiness1';
                $obj->shipper = '2F277X';
                $obj->shipper_name = 'Fire Business';
                $obj->shipper_address = 'ul. Przedborska 34';
                $obj->shipper_city = 'Piotrków Trybunalski';
                $obj->shipper_postal = '97300';
                $obj->shipper_country = 'PL';
                $obj->shipper_tel = '0048518114583';
                break;
            case self::SHIPPER_M:
                $obj->access = '7D134EE28479E728';
                $obj->userid = 'mr2016';
                $obj->passwd = 'MarketingRelacji1';
                $obj->shipper = '2F2774';
                $obj->shipper_name = 'MRWaKlo';
                $obj->shipper_address = 'Kłobucka 23A';
                $obj->shipper_city = 'Warszawa';
                $obj->shipper_postal = '02699';
                $obj->shipper_country = 'PL';
                $obj->shipper_tel = '+48518131577';
                break;

            case self::SHIPPER_L:
                $obj->access = 'BD1CC8117D2D7B6E';
                $obj->userid = 'LPS2016_1';
                $obj->passwd = 'LupusLupus1';
                $obj->shipper = '2F2785';
                $obj->shipper_name = 'KTLupus';
                $obj->shipper_address = 'Zalesicka 82';
                $obj->shipper_address_2 = '';
                $obj->shipper_city = 'Piotrkow Trybunalski';
                $obj->shipper_postal = '97300';
                $obj->shipper_country = 'PL';
                $obj->shipper_tel = '518114583';
                break;

            case self::SHIPPER_MCM:
                $obj->access = '7D1F0F2EAB96D7DE';
                $obj->userid = 'MCM2016_1';
                $obj->passwd = 'McmMcmMcm1';
                $obj->shipper = '375F5W';
                $obj->shipper_name = 'MCM';
                $obj->shipper_address = 'Komuny Paryskiej 73a/3';
                $obj->shipper_city = 'Wroclaw';
                $obj->shipper_postal = '50452';
                $obj->shipper_country = 'PL';
                $obj->shipper_tel = '+48503027485';
                break;

            case self::SHIPPER_UK:
                $obj->access = '5D180D0976F9E588';
                $obj->userid = 'Inxpressns';
                $obj->passwd = 'Dmsltd#2007';
                $obj->shipper = '5793A4';
                $obj->shipper_name = 'DMS Ltd T/A InXpress North Surrey';
                $obj->shipper_address = 'Suite 18 Compass Point';
                $obj->shipper_address_2 = 'Southwell Park';
                $obj->shipper_city = 'Portland';
                $obj->shipper_postal = 'DT52NA';
                $obj->shipper_country = 'GB';
                $obj->shipper_tel = '01305822000';
                break;

            case self::SHIPPER_4VALUES:
                $obj->access = '8D33385637D60888';
                $obj->userid = '4VALUESPL';
                $obj->passwd = '4Values2017@';
                $obj->shipper = '3743AX';
                $obj->shipper_name = '4Values Sp. z o.o.';
                $obj->shipper_address = 'Ul. Parzniewska 4';
                $obj->shipper_address_2 = '';
                $obj->shipper_city = 'Pruszków';
                $obj->shipper_postal = '05800';
                $obj->shipper_country = 'PL';
                $obj->shipper_tel = '48503640636';
                break;
        }

        return $obj;
    }

    public function setShipper($shipper)
    {

        $obj = self::_shipperData($shipper);

        $this->access = $obj->access;
        $this->userid = $obj->userid;
        $this->passwd = $obj->passwd;
        $this->shipper = $obj->shipper;
        $this->shipper_name = $obj->shipper_name;
        $this->shipper_address = $obj->shipper_address;
        $this->shipper_city = $obj->shipper_city;
        $this->shipper_postal = $obj->shipper_postal;
        $this->shipper_country = $obj->shipper_country;
        $this->shipper_tel = $obj->shipper_tel;

    }

    protected static function _isServiceSaver($service_id)
    {
        return in_array($service_id,[
            self::SERVICE_SAVER,
            self::SERVICE_SAVER_BIS,
            self::SERVICE_SAVER_F,
            self::SERVICE_SAVER_M,
            self::SERVICE_EXPRESS_L, // yup, it's express saver, not express...
            self::SERVICE_SAVER_MCM,
            self::SERVICE_SAVER_4VALUES,
        ]);
    }

    protected static function _isServiceExpress($service_id)
    {
        return in_array($service_id,[
            self::SERVICE_EXPRESS,
        ]);
    }


    public static function isCourierLabelNewOperatorUps($operator_id)
    {
        return in_array($operator_id,[
            CourierLabelNew::OPERATOR_UPS,
            CourierLabelNew::OPERATOR_UPS_SAVER,
            CourierLabelNew::OPERATOR_UPS_BIS,
            CourierLabelNew::OPERATOR_UPS_SAVER_BIS,
            CourierLabelNew::OPERATOR_UPS_F,
            CourierLabelNew::OPERATOR_UPS_SAVER_F,
            CourierLabelNew::OPERATOR_UPS_EXPRESS,
            CourierLabelNew::OPERATOR_UPS_M,
            CourierLabelNew::OPERATOR_UPS_SAVER_M,
            CourierLabelNew::OPERATOR_UPS_EXPRESS_M,
            CourierLabelNew::OPERATOR_UPS_UK,
            CourierLabelNew::OPERATOR_UPS_EXPRESS_L,
            CourierLabelNew::OPERATOR_UPS_SAVER_MCM,
            CourierLabelNew::OPERATOR_UPS_SAVER_4VALUES,

        ]);
    }
/* used for courier simple */
//    public static function isCourierLabelNewOperatorUpsSaver($operator_id)
//    {
//        return in_array($operator_id,[
//            CourierLabelNew::OPERATOR_UPS_SAVER,
//            CourierLabelNew::OPERATOR_UPS_SAVER_BIS,
//            CourierLabelNew::OPERATOR_UPS_SAVER_F,
//            CourierLabelNew::OPERATOR_UPS_SAVER_M,
//            CourierLabelNew::OPERATOR_UPS_SAVER_MCM,
//        ]);
//    }

    public static function mapCourierLabelNewOperatorUps($operator_id)
    {
        if ($operator_id == CourierLabelNew::OPERATOR_UPS_SAVER)
            $upsOperator = UpsSoapShipClient::SERVICE_SAVER;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_SAVER_BIS)
            $upsOperator = UpsSoapShipClient::SERVICE_SAVER_BIS;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_BIS)
            $upsOperator = UpsSoapShipClient::SERVICE_STANDARD_BIS;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_SAVER_F)
            $upsOperator = UpsSoapShipClient::SERVICE_SAVER_F;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_F)
            $upsOperator = UpsSoapShipClient::SERVICE_STANDARD_F;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_SAVER_M)
            $upsOperator = UpsSoapShipClient::SERVICE_SAVER_M;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_M)
            $upsOperator = UpsSoapShipClient::SERVICE_STANDARD_M;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_EXPRESS)
            $upsOperator = UpsSoapShipClient::SERVICE_EXPRESS;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_EXPRESS_M)
            $upsOperator = UpsSoapShipClient::SERVICE_EXPRESS_M;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_UK)
            $upsOperator = UpsSoapShipClient::SERVICE_STANDARD_UK;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_EXPRESS_L)
            $upsOperator = UpsSoapShipClient::SERVICE_EXPRESS_L;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_SAVER_MCM)
            $upsOperator = UpsSoapShipClient::SERVICE_SAVER_MCM;
        else if($operator_id == CourierLabelNew::OPERATOR_UPS_SAVER_4VALUES)
            $upsOperator = UpsSoapShipClient::SERVICE_SAVER_4VALUES;
        else
            $upsOperator = UpsSoapShipClient::SERVICE_STANDARD;

        return $upsOperator;
    }

    public static function isCourierOperatorUps($operator_id)
    {
        return in_array($operator_id,[
            CourierOperator::OPERATOR_UPS,
            CourierOperator::OPERATOR_UPS_SAVER,
            CourierOperator::OPERATOR_UPS_BIS,
            CourierOperator::OPERATOR_UPS_SAVER_BIS,
            CourierOperator::OPERATOR_UPS_F,
            CourierOperator::OPERATOR_UPS_SAVER_F,
            CourierOperator::OPERATOR_UPS_M,
            CourierOperator::OPERATOR_UPS_SAVER_M,
            CourierOperator::OPERATOR_UPS_SAVER_M,
            CourierOperator::OPERATOR_UPS_UK,
            CourierOperator::OPERATOR_UPS_EXPRESS_L,
            CourierOperator::OPERATOR_UPS_SAVER_MCM,
            CourierOperator::OPERATOR_UPS_SAVER_4VALUES,
        ]);
    }

    public static function isCourierOperatorUpsSaver($operator_id)
    {
        return in_array($operator_id,[
            CourierOperator::OPERATOR_UPS_SAVER,
            CourierOperator::OPERATOR_UPS_SAVER_BIS,
            CourierOperator::OPERATOR_UPS_SAVER_F,
            CourierOperator::OPERATOR_UPS_SAVER_M,
            CourierOperator::OPERATOR_UPS_SAVER_MCM,
            CourierOperator::OPERATOR_UPS_SAVER_4VALUES,
        ]);
    }

    public static function mapCourierOperatorUpsToCourierLabelNewOperator($operator_id)
    {


        if ($operator_id == CourierOperator::OPERATOR_UPS)
            $upsOperator = CourierLabelNew::OPERATOR_UPS;
        else if($operator_id == CourierOperator::OPERATOR_UPS_BIS)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_BIS;
        else if($operator_id == CourierOperator::OPERATOR_UPS_F)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_F;
        else if ($operator_id == CourierOperator::OPERATOR_UPS_SAVER)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_SAVER;
        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_BIS)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_SAVER_BIS;
        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_F)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_SAVER_F;
        else if($operator_id == CourierOperator::OPERATOR_UPS_M)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_M;
        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_M)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_SAVER_M;
        else if($operator_id == CourierOperator::OPERATOR_UPS_UK)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_UK;
        else if($operator_id == CourierOperator::OPERATOR_UPS_EXPRESS_L)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_EXPRESS_L;
        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_MCM)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_SAVER_MCM;
        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_4VALUES)
            $upsOperator = CourierLabelNew::OPERATOR_UPS_SAVER_4VALUES;
        else
            $upsOperator = false;

        return $upsOperator;
    }

//    public static function mapCourierOperatorUpsToName($operator_id)
//    {
//        if($operator_id == CourierOperator::OPERATOR_UPS)
//            $name = 'UPS';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER)
//            $name = 'UPS Saver';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_BIS)
//            $name = 'UPS BIS';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_BIS)
//            $name = 'UPS Saver BIS';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_F)
//            $name = 'UPS F';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_F)
//            $name = 'UPS Saver F';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_M)
//            $name = 'UPS M';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_M)
//            $name = 'UPS Saver M';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_UK)
//            $name = 'UPS UK';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_EXPRESS_L)
//            $name = 'UPS Express L';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_MCM)
//            $name = 'UPS Saver MCM';
//        else if($operator_id == CourierOperator::OPERATOR_UPS_SAVER_4VALUES)
//            $name = 'UPS Saver 4VALUES';
//        else
//            $name = '';
//
//        return $name;
//    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $returnError = false, $service = self::SERVICE_STANDARD)
    {
        $model = new self;

        if(self::_isServiceSaver($service))
            $model->setModeType(self::MODE_SAVER);
        elseif(self::_isServiceExpress($service))
            $model->setModeType(self::MODE_EXPRESS);

        $model->_user_id = $courierInternal->courier->user_id;

        $model->_reference_no = $courierInternal->courier->local_id;

        // SPECIAL VALUE FOR 4VALUES - 22.03.2017
        if($courierInternal->courier->user_id == User::USER_4VALUES) {

            $model->_requireSignature = true;

            if ($courierInternal->courier->ref != '')
                $model->_reference_no_2 = mb_substr($courierInternal->courier->ref, 2);
        }

        $model->_package_desc = S_Useful::removeNationalCharacters($courierInternal->courier->package_content);
        $model->_package_size_l = $courierInternal->courier->package_size_l;
        $model->_package_size_w = $courierInternal->courier->package_size_w;
        $model->_package_size_h = $courierInternal->courier->package_size_d;
        $model->_package_weight = $courierInternal->courier->getWeight(true);

        $model->_sender_addressLine = S_Useful::removeNationalCharacters($from->address_line_1);
        $model->_sender_addressLine2 = S_Useful::removeNationalCharacters($from->address_line_2);
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_name = S_Useful::removeNationalCharacters($from->name).' '.S_Useful::removeNationalCharacters($from->company);
        $model->_sender_number = $from->tel;
        $model->_sender_city = S_Useful::removeNationalCharacters($from->city);

        $name = S_Useful::removeNationalCharacters($to->name);
        $company = S_Useful::removeNationalCharacters($to->company);

        $model->_receiver_name = $company != '' ? $company : $name;
        $model->_receiver_attention_name = ($company != '' && $name != '') ? $name : '';

        $model->_receiver_addressLine = S_Useful::removeNationalCharacters($to->address_line_1);
        $model->_receiver_addressLine2 = S_Useful::removeNationalCharacters($to->address_line_2);
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_number = $to->tel;
        $model->_receiver_city = S_Useful::removeNationalCharacters($to->city);
        $model->_receiver_mail = $to->email;

        if($model->_sender_countryCode != $model->_receiver_countryCode && $model->_receiver_attention_name == '')
            $model->_receiver_attention_name = trim($name.' '.$company);

        if($service == self::SERVICE_STANDARD_BIS OR $service == self::SERVICE_SAVER_BIS)
        {
            $model->setShipper(self::SHIPPER_BIS);
        }
        else if($service == self::SERVICE_STANDARD_M OR $service == self::SERVICE_SAVER_M) {
            $model->setShipper(self::SHIPPER_M);

            if ($courierInternal->courier->user_id != '')
                $model->shipper_attention_name = $courierInternal->courier->user->login;

            // SPECIAL RULE FOR "RR DONNELLEY" @ 30.03.2017
//            if($courierInternal->courier->user_id == 258)
//                $model->shipper_attention_name = 'GRECOS';

            // SPECIAL RULE FOR "RR DONNELLEY" @ 29.06.2017
            if($courierInternal->courier->user_id == 258)
            {
                $model->shipper_name = 'SUPER EXPRESS';
                $model->shipper_attention_name = '';
                $model->shipper_address = 'UL. JUBILERSKA 10';
                $model->shipper_address_2 = '';
                $model->shipper_city = 'WARSZAWA';
                $model->shipper_postal = '00939';
                $model->shipper_country = 'PL';
                $model->shipper_tel = '00000000';
            }

            if ($from->country_id == CountryList::COUNTRY_PL)
                $model->_declared_value = $courierInternal->courier->getPackageValueConverted('PLN');

            $model->_sender_name = $model->shipper_name;
            $model->_sender_attention_name = $model->shipper_attention_name;
            $model->_sender_addressLine = $model->shipper_address;
            $model->_sender_addressLine2 = '';
            $model->_sender_city = $model->shipper_city;
            $model->_sender_postalCode = $model->shipper_postal;
            $model->_sender_countryCode = $model->shipper_country;
            $model->_sender_number = $model->shipper_tel;
        }
        else if($service == self::SERVICE_SAVER_MCM)
        {
            $return = [];
            $returnItem = array(
                'error' => 'Inactive UPS contract!',
                'data' => '',
            );

            for($i = 0; $i < $model->_number; $i++)
            {
                $model->_number_of_multipackages = 1;
                array_push($return, $returnItem);
            }

            return $return;

//            $model->setShipper(self::SHIPPER_MCM);
//
//            if($courierInternal->courier->user_id != '')
//                $model->shipper_attention_name = $courierInternal->courier->user->login;
//
//            if($from->country_id == CountryList::COUNTRY_PL)
//                $model->_declared_value = $courierInternal->courier->package_value;
//
//            $model->_sender_name = $model->shipper_name;
//            $model->_sender_attention_name = $model->shipper_attention_name;
//            $model->_sender_addressLine = $model->shipper_address;
//            $model->_sender_addressLine2 = '';
//            $model->_sender_city = $model->shipper_city;
//            $model->_sender_postalCode = $model->shipper_postal;
//            $model->_sender_countryCode = $model->shipper_country;
//            $model->_sender_number = $model->shipper_tel;

        }
        else if($service == self::SERVICE_STANDARD_F OR $service == self::SERVICE_SAVER_F)
        {
            $model->setShipper(self::SHIPPER_F);

            // IF WE SEND FROM HUB PL, CHANGE ADDRES TO HIDE KAAB @30.06.2016
            if($from->id == self::HUB_PL) // HUB PL ADDRESS DATA ID
            {
                $model->_sender_addressLine = $model->shipper_address;
                $model->_sender_addressLine2 = '';
                $model->_sender_countryCode = $model->shipper_country;
                $model->_sender_postalCode = $model->shipper_postal;
                $model->_sender_name = $model->shipper_name;
                $model->_sender_attention_name = '';
                $model->_sender_number = $model->shipper_tel;
                $model->_sender_city = $model->shipper_city;
            }
            else if($to->id == self::HUB_PL) // HUB PL ADDRESS DATA ID
            {
                $model->_receiver_addressLine = $model->shipper_address;
                $model->_receiver_addressLine2 = '';
                $model->_receiver_countryCode = $model->shipper_country;
                $model->_receiver_postalCode = $model->shipper_postal;
                $model->_receiver_name = $model->shipper_name;
                $model->_receiver_attention_name = '';
                $model->_receiver_number = $model->shipper_tel;
                $model->_receiver_city = $model->shipper_city;
            }

            // SPECIAL RULE @ 30.01.2017
            // FOR CLOUDPACK AND PACKAGES TO GB
            if($from->id == self::HUB_NL && $model->_user_id == 442 && strtoupper($model->_receiver_countryCode) == 'GB')
            {
                $model->_sender_addressLine = $model->shipper_address;
                $model->_sender_addressLine2 = '';
                $model->_sender_countryCode = $model->shipper_country;
                $model->_sender_postalCode = $model->shipper_postal;
                $model->_sender_name = $model->shipper_name;
                $model->_sender_attention_name = '';
                $model->_sender_number = $model->shipper_tel;
                $model->_sender_city = $model->shipper_city;
            }

            // 13.10.2016
            // IN CASE OF IMPORT (PACKAGE FROM ABROAD TO PL)
            // RETURN SERVICE:
            if($courierInternal->courier->senderAddressData->country_id != CountryList::COUNTRY_PL && $courierInternal->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
            {
                $model->_returnService = true;

                $model->_receiver_name = $model->shipper_name;
                $model->_receiver_attention_name = $model->shipper_name;
                $model->_receiver_addressLine = $model->shipper_address;
                $model->_receiver_addressLine2 = '';
                $model->_receiver_postalCode = $model->shipper_postal;
                $model->_receiver_city = $model->shipper_city;
                $model->_receiver_countryCode = $model->shipper_country;
                $model->_receiver_number = $model->shipper_tel;

//                $model->shipper_name = S_Useful::removeNationalCharacters($from->name).' '.S_Useful::removeNationalCharacters($from->company);
//                $model->shipper_address = trim($from->address_line_1.' '.$from->address_line_2);
//                $model->shipper_postal = preg_replace("/[^\w]+/", "", $from->zip_code);
//                $model->shipper_city = $from->city;
//                $model->shipper_country = $from->country0->code;
//                $model->shipper_tel = $from->tel;
            }
        } else if($service == self::SERVICE_STANDARD_UK) {

            $model->setShipper(self::SHIPPER_UK);

        }
        else if($service == self::SERVICE_SAVER_4VALUES) {
            $model->setShipper(self::SHIPPER_4VALUES);
        }
        else if($service == self::SERVICE_EXPRESS_L)
        {
            $model->setShipper(self::SHIPPER_L);

            $model->_sender_name = $model->shipper_name;
            $model->_sender_attention_name = $model->shipper_attention_name;
            $model->_sender_addressLine = $model->shipper_address;
            $model->_sender_addressLine2 = '';
            $model->_sender_city = $model->shipper_city;
            $model->_sender_postalCode = $model->shipper_postal;
            $model->_sender_countryCode = $model->shipper_country;
            $model->_sender_number = $model->shipper_tel;

            if($courierInternal->courier->user_id != '')
                $model->shipper_attention_name = $courierInternal->courier->user->login;

            if($from->country_id == CountryList::COUNTRY_PL)
                $model->_declared_value = $courierInternal->courier->getPackageValueConverted('PLN');

            if(!in_array(strtoupper($model->_receiver_countryCode), ['DE', 'FR', 'DK', 'FI', 'NO', 'AX', 'UK', 'GB', 'NL', 'ES'])) {

                $return = [];
                $returnItem = array(
                    'error' => 'UPS_L dostępny jest tylko dla krajów: Niemcy, Francja, Dania, Finlandia, Norwegia, UK, NL, ES',
                    'data' => '',
                );

                for($i = 0; $i < $model->_number; $i++)
                {
                    $model->_number_of_multipackages = 1;
                    array_push($return, $returnItem);
                }

                return $return;
            }
        }

        if(in_array($courierInternal->courier->user_id, [
            442,   // SPECIAL RULE 04.10.2016 - custom UPS account for user "cloudpack"
            520,    // SPECIAL RULE 03.11.2016 - custom UPS account for user "CUSTOMERITUM"
            448,    // SPECIAL RULE 03.02.2017 - custom UPS account for user "ANKLOGISTIK"
            495,    // SPECIAL RULE 27.03.2017 - custom UPS account for user "PHUKK"
            499,    // SPECIAL RULE 27.03.2017 - custom UPS account for user "DSIAKANDK"
        ]))
        {
            if($courierInternal->courier->senderAddressData->country_id == CountryList::COUNTRY_PL && $courierInternal->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
            {
                if($courierInternal->courier->user->getUpsCustomData('active'))
                {
                    $model->access = $courierInternal->courier->user->getUpsCustomData('id');
                    $model->userid = $courierInternal->courier->user->getUpsCustomData('login');
                    $model->passwd = $courierInternal->courier->user->getUpsCustomData('password');
                    $model->shipper = $courierInternal->courier->user->getUpsCustomData('shipper');
                    $model->shipper_name = $courierInternal->courier->user->getUpsCustomData('shipper_name');
                    $model->shipper_address = $courierInternal->courier->user->getUpsCustomData('shipper_address');
                    $model->shipper_city = $courierInternal->courier->user->getUpsCustomData('shipper_city');
                    $model->shipper_postal = $courierInternal->courier->user->getUpsCustomData('shipper_postal');
                    $model->shipper_country = $courierInternal->courier->user->getUpsCustomData('shipper_country');
                    $model->shipper_tel = $courierInternal->courier->user->getUpsCustomData('shipper_tel');
                }

            }
        }


        if($courierInternal->courier->user_id == User::USER_4VALUES  && $from->country_id == CountryList::COUNTRY_PL)
        {
            $model->shipper_name = '4VALUES.PL';
            $model->shipper_attention_name = 'c/o Izettle';
            $model->shipper_address = 'ul. Parzniewska 4';
            $model->shipper_address_2 = '';
            $model->shipper_city = 'Pruszków';
            $model->shipper_postal = '05800';
            $model->shipper_country = 'PL';
            $model->shipper_tel = '48504660711';

            $model->_sender_name = $model->shipper_name;
            $model->_sender_attention_name = $model->shipper_attention_name;
            $model->_sender_addressLine = $model->shipper_address;
            $model->_sender_addressLine2 = '';
            $model->_sender_city = $model->shipper_city;
            $model->_sender_postalCode = $model->shipper_postal;
            $model->_sender_countryCode = $model->shipper_country;
            $model->_sender_number = $model->shipper_tel;
        }

        $model->_number = $courierInternal->courier->packages_number;
        if($courierInternal->courier->cod)
        {
            $model->_cod = true;
            $model->_cod_currency = $courierInternal->courier->cod_currency;
            $model->_cod_amount = $courierInternal->courier->cod_value;
        }

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UPS_ROD))
            $model->_rod = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UPS_SATURDAY_DELIVERY))
            $model->_saturday_delivery = true;

        if($courierInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_UPS_NOTIFICATION))
            $model->_notification = true;

        // MULTIPLE PACKAGES ONLY ON PL->PL, OTHERWISE MAKE N*ORDERS
        if($model->_receiver_countryCode == 'PL' && $model->_sender_countryCode == 'PL') {
            $model->_number_of_multipackages = $model->_number;
            return $model->go($returnError);
        }
        else
        {
            $return = [];
            for($i = 0; $i < $model->_number; $i++)
            {
                $model->_number_of_multipackages = 1;
                array_push($return, $model->go($returnError)[0]);
            }

            return $return;
        }
    }

    public static function isCourierUOperatorSaver(CourierUOperator $courierUOperator)
    {
        if(in_array($courierUOperator->uniq_id,
            [
                CourierUOperator::UNIQID_UPS_SAVER,
                CourierUOperator::UNIQID_UPS_BIS_SAVER,
                CourierUOperator::UNIQID_UPS_F_SAVER,
                CourierUOperator::UNIQID_UPS_M_SAVER,
                CourierUOperator::UNIQID_UPS_MCM_SAVER,
                CourierUOperator::UNIQID_UPS_L_SAVER,
            ]
        ))
            return true;
        else
            return false;
    }

    public static function orderForCourierU(CourierTypeU $courierU, AddressData $from, AddressData $to, $returnError = false)
    {
        $model = new self;

        if(self::isCourierUOperatorSaver($courierU->courierUOperator))
            $model->setModeType(self::MODE_SAVER);

        switch($courierU->courierUOperator->uniq_id)
        {
            case CourierUOperator::UNIQID_UPS_M:
            case CourierUOperator::UNIQID_UPS_M_SAVER:
                $model->setShipper(self::SHIPPER_M);
                break;
            case CourierUOperator::UNIQID_UPS_F:
            case CourierUOperator::UNIQID_UPS_F_SAVER:
                $model->setShipper(self::SHIPPER_F);
                break;
            case CourierUOperator::UNIQID_UPS_BIS:
            case CourierUOperator::UNIQID_UPS_BIS_SAVER:
                $model->setShipper(self::SHIPPER_BIS);
                break;
            case CourierUOperator::UNIQID_UPS_L:
            case CourierUOperator::UNIQID_UPS_L_SAVER:
                $model->setShipper(self::SHIPPER_L);
                break;
            case CourierUOperator::UNIQID_UPS_UK:
                $model->setShipper(self::SHIPPER_UK);
                break;
            case CourierUOperator::UNIQID_UPS_MCM_SAVER:
                $model->setShipper(self::SHIPPER_MCM);
                break;
        }

        $model->_user_id = $courierU->courier->user_id;
        $model->_reference_no = $courierU->courier->local_id;

        // SPECIAL VALUE FOR 4VALUES - 22.03.2017
        if($courierU->courier->user_id == User::USER_4VALUES) {

            $model->_requireSignature = true;

            if ($courierU->courier->ref != '')
                $model->_reference_no_2 = mb_substr($courierU->courier->ref, 2);
        }

        $model->_package_desc = S_Useful::removeNationalCharacters($courierU->courier->package_content);
        $model->_package_size_l = $courierU->courier->package_size_l;
        $model->_package_size_w = $courierU->courier->package_size_w;
        $model->_package_size_h = $courierU->courier->package_size_d;
        $model->_package_weight = $courierU->getFinalWeight();

        $model->_sender_addressLine = S_Useful::removeNationalCharacters($from->address_line_1);
        $model->_sender_addressLine2 = S_Useful::removeNationalCharacters($from->address_line_2);
        $model->_sender_countryCode = $from->country0->code;
        $model->_sender_postalCode = $from->zip_code;
        $model->_sender_name = S_Useful::removeNationalCharacters($from->name).' '.S_Useful::removeNationalCharacters($from->company);
        $model->_sender_number = $from->tel;
        $model->_sender_city = S_Useful::removeNationalCharacters($from->city);

        $name = S_Useful::removeNationalCharacters($to->name);
        $company = S_Useful::removeNationalCharacters($to->company);

        $model->_receiver_name = $company != '' ? $company : $name;
        $model->_receiver_attention_name = ($company != '' && $name != '') ? $name : '';

        $model->_receiver_addressLine = S_Useful::removeNationalCharacters($to->address_line_1);
        $model->_receiver_addressLine2 = S_Useful::removeNationalCharacters($to->address_line_2);
        $model->_receiver_countryCode = $to->country0->code;
        $model->_receiver_postalCode = $to->zip_code;
        $model->_receiver_number = $to->tel;
        $model->_receiver_city = S_Useful::removeNationalCharacters($to->city);
        $model->_receiver_mail = $to->email;

        if($model->_sender_countryCode != $model->_receiver_countryCode && $model->_receiver_attention_name == '')
            $model->_receiver_attention_name = trim($name.' '.$company);

        $model->_number = $courierU->courier->packages_number;
        if($courierU->courier->cod)
        {
            $model->_cod = true;
            $model->_cod_currency = $courierU->courier->cod_currency;
            $model->_cod_amount = $courierU->courier->cod_value;
        }

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UPS_ROD))
            $model->_rod = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UPS_SATURDAY))
            $model->_saturday_delivery = true;

        if($courierU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_UPS_NOTIFICATION))
            $model->_notification = true;

        // MULTIPLE PACKAGES ONLY ON PL->PL, OTHERWISE MAKE N*ORDERS
        if($model->_receiver_countryCode == 'PL' && $model->_sender_countryCode == 'PL') {
            $model->_number_of_multipackages = $model->_number;
            return $model->go($returnError);
        }
        else
        {
            $return = [];
            for($i = 0; $i < $model->_number; $i++)
            {
                $model->_number_of_multipackages = 1;
                array_push($return, $model->go($returnError)[0]);
            }

            return $return;
        }

    }

    protected static function addressLinesConverter($addressLine1, $addressLine2, $getLineNo)
    {
        if(mb_strlen($addressLine1) <= 35 && mb_strlen($addressLine2) <= 35)
        {
            if($getLineNo == 0)
                return $addressLine1;
            else if($getLineNo == 1)
                return $addressLine2;
            else if($getLineNo == 2)
                return '';

        } else {
            $address = trim($addressLine1) . ' ' . trim($addressLine2);
            $address = trim($address);

            $newAddress = [];
            $newAddress[0] = mb_substr($address, 0, 35);
            $newAddress[1] = mb_substr($address, 35, 35);
            $newAddress[2] = mb_substr($address, 70, 35);

            return trim($newAddress[$getLineNo]);
        }
    }

    protected function processShipmentOrder()
    {

        if($this->_receiver_countryCode == 'GR')
        {
            $this->_receiver_name = S_Useful::transliterateGrek($this->_receiver_name);
            $this->_receiver_attention_name = S_Useful::transliterateGrek($this->_receiver_attention_name);
            $this->_receiver_addressLine = S_Useful::transliterateGrek($this->_receiver_addressLine);
            $this->_receiver_addressLine2 = S_Useful::transliterateGrek($this->_receiver_addressLine2);
            $this->_receiver_city = S_Useful::transliterateGrek($this->_receiver_city);
        }

        if($this->_sender_countryCode == 'GR')
        {
            $this->_sender_name = S_Useful::transliterateGrek($this->_sender_name);
            $this->_sender_attention_name = S_Useful::transliterateGrek($this->_sender_attention_name);
            $this->_sender_addressLine = S_Useful::transliterateGrek($this->_sender_addressLine);
            $this->_sender_addressLine2 = S_Useful::transliterateGrek($this->_sender_addressLine2);
            $this->_sender_city = S_Useful::transliterateGrek($this->_sender_city);
        }

        // ADD USA STATE NAME IF PACKAGE IS FROM USA
        $senderStateProvinceCode = '';
        if($this->_sender_countryCode == 'US')
        {
            $state = UsaZipState::getStateByZip($this->_sender_postalCode, true);
            $senderStateProvinceCode = $state;
        }
        else if($this->_sender_countryCode == 'CA')
        {
            $state = CanadaZipState::getStateByZip($this->_sender_postalCode);
            $senderStateProvinceCode = $state;
        }


        // ADD USA STATE NAME IF PACKAGE IS TO USA
        $receiverStateProvinceCode = '';
        if($this->_receiver_countryCode == 'US')
        {
            $state = UsaZipState::getStateByZip($this->_receiver_postalCode, true);
            $receiverStateProvinceCode = $state;
        }
        else if($this->_receiver_countryCode == 'CA')
        {
            $state = CanadaZipState::getStateByZip($this->_receiver_postalCode);
            $receiverStateProvinceCode = $state;
        }

        $request = new stdClass();

        $request->Request = new stdClass();
        $request->Request->RequestOption = 'nonvalidate';

        $request->Shipment = new stdClass();
        $request->Shipment->Description = mb_substr($this->_package_desc,0,35);
        $request->Shipment->Shipper = new stdClass();
        $request->Shipment->Shipper->Name = $this->shipper_name;
        $request->Shipment->Shipper->AttentionName = $this->shipper_attention_name ? mb_substr($this->shipper_attention_name,0,35) : mb_substr($this->shipper_name,0,35);
        $request->Shipment->Shipper->TaxIdentificationNumber = '';
        $request->Shipment->Shipper->ShipperNumber = $this->shipper;
        $request->Shipment->Shipper->Address = new stdClass();
        $request->Shipment->Shipper->Address->AddressLine = [];
        $request->Shipment->Shipper->Address->AddressLine[0] = self::addressLinesConverter($this->shipper_address, $this->shipper_address_2,0);
        $request->Shipment->Shipper->Address->AddressLine[1] = self::addressLinesConverter($this->shipper_address, $this->shipper_address_2,1);
        $request->Shipment->Shipper->Address->AddressLine[2] = self::addressLinesConverter($this->shipper_address, $this->shipper_address_2,2);
        $request->Shipment->Shipper->Address->City = $this->shipper_city;
        $request->Shipment->Shipper->Address->PostalCode = $this->shipper_postal;
        $request->Shipment->Shipper->Address->CountryCode = $this->shipper_country;
        $request->Shipment->Shipper->Phone = new stdClass();
        $request->Shipment->Shipper->Phone->Number = $this->shipper_tel;

        $request->Shipment->ShipFrom = new stdClass();
        $request->Shipment->ShipFrom->Name = mb_substr($this->_sender_name,0,35);
        $request->Shipment->ShipFrom->AttentionName = $this->_sender_attention_name ?  mb_substr($this->_sender_attention_name,0,35) : mb_substr($this->_sender_name,0,35);
        $request->Shipment->ShipFrom->Phone = new stdClass();
        $request->Shipment->ShipFrom->Phone->Number =  mb_substr($this->_sender_number,0,15);
        $request->Shipment->ShipFrom->Address = new stdClass();
        $request->Shipment->ShipFrom->Address->AddressLine = [];
        $request->Shipment->ShipFrom->Address->AddressLine[0] = self::addressLinesConverter($this->_sender_addressLine, $this->_sender_addressLine2,0);
        $request->Shipment->ShipFrom->Address->AddressLine[1] = self::addressLinesConverter($this->_sender_addressLine, $this->_sender_addressLine2,1);
        $request->Shipment->ShipFrom->Address->AddressLine[2] = self::addressLinesConverter($this->_sender_addressLine, $this->_sender_addressLine2,2);
        $request->Shipment->ShipFrom->Address->City =  mb_substr($this->_sender_city,0,30);
        $request->Shipment->ShipFrom->Address->StateProvinceCode = $senderStateProvinceCode;
        $request->Shipment->ShipFrom->Address->PostalCode = mb_substr($this->_sender_postalCode,0,9);
        $request->Shipment->ShipFrom->Address->CountryCode = $this->_sender_countryCode;

        $request->Shipment->ShipTo = new stdClass();
        $request->Shipment->ShipTo->Name =  mb_substr($this->_receiver_name,0,35);
        $request->Shipment->ShipTo->AttentionName =  mb_substr($this->_receiver_attention_name ,0,35);
        $request->Shipment->ShipTo->Phone = new stdClass();
        $request->Shipment->ShipTo->Phone->Number =  mb_substr($this->_receiver_number,0,15);
        $request->Shipment->ShipTo->Address = new stdClass();
        $request->Shipment->ShipTo->Address->AddressLine = [];
        $request->Shipment->ShipTo->Address->AddressLine[0] = self::addressLinesConverter($this->_receiver_addressLine, $this->_receiver_addressLine2,0);
        $request->Shipment->ShipTo->Address->AddressLine[1] = self::addressLinesConverter($this->_receiver_addressLine, $this->_receiver_addressLine2,1);
        $request->Shipment->ShipTo->Address->AddressLine[2] = self::addressLinesConverter($this->_receiver_addressLine, $this->_receiver_addressLine2,2);
        $request->Shipment->ShipTo->Address->City =  mb_substr($this->_receiver_city,0,30);
        $request->Shipment->ShipTo->Address->StateProvinceCode = $receiverStateProvinceCode;
        $request->Shipment->ShipTo->Address->PostalCode = mb_substr($this->_receiver_postalCode,0,9);
        $request->Shipment->ShipTo->Address->CountryCode = $this->_receiver_countryCode;

        $request->Shipment->PaymentInformation = new stdClass();
        $request->Shipment->PaymentInformation->ShipmentCharge = new stdClass();
        $request->Shipment->PaymentInformation->ShipmentCharge->BillShipper = new stdClass();
        $request->Shipment->PaymentInformation->ShipmentCharge->BillShipper->AccountNumber = $this->shipper;
        $request->Shipment->PaymentInformation->ShipmentCharge->Type = '01';

        $request->Shipment->ReferenceNumber[0] = new stdClass();
        $request->Shipment->ReferenceNumber[0]->Number = 1;
        $request->Shipment->ReferenceNumber[0]->Value = $this->_reference_no;
        $request->Shipment->ReferenceNumber[0]->BarCodeIndicator;

        if($this->_user_id OR $this->_reference_no_2) {

            $ref2 = $this->_reference_no_2 ? $this->_reference_no_2 : 'x' . $this->_user_id;

            $request->Shipment->ReferenceNumber[1] = new stdClass();
            $request->Shipment->ReferenceNumber[1]->Number = 1;
            $request->Shipment->ReferenceNumber[1]->Value = $ref2;
        }

        //COD
//        $cod['CODCode'] = 3;
//        $cod['CODFundsCode'] = 1;
//        $codAmount['CurrencyCode'] = 'PLN';
//        $codAmount['MonetaryValue'] = 10;
//        $cod['CODAmount'] = $codAmount;
//
//        $shipment['ShipmentServiceOptions'] = array('COD' => $cod);
        //END COD

        $request->Shipment->Service = new stdClass();
        $request->Shipment->Service->Code = $this->_service_code;
        $request->Shipment->Service->Description = $this->_service_name;

        $request->Shipment->Package = array();

        for($i = 0; $i < $this->_number_of_multipackages; $i++) {

            $request->Shipment->Package[$i] = new stdClass();
            $request->Shipment->Package[$i]->PackagingType = new stdClass();
            $request->Shipment->Package[$i]->PackagingType->Code = '02';
            $request->Shipment->Package[$i]->Description = mb_substr($this->_package_desc,0,35);
            $request->Shipment->Package[$i]->Packaging = new stdClass();
            $request->Shipment->Package[$i]->Packaging->Code = '02';
//            $request->Shipment->Package[$i]->Packaging->Description;
            $request->Shipment->Package[$i]->Dimensions = new stdClass();
            $request->Shipment->Package[$i]->Dimensions->UnitOfMeasurement = new stdClass();
            $request->Shipment->Package[$i]->Dimensions->UnitOfMeasurement->Code = 'CM';
            $request->Shipment->Package[$i]->Dimensions->UnitOfMeasurement->Description = 'Centimeters';
            $request->Shipment->Package[$i]->Dimensions->Length = $this->_package_size_l;
            $request->Shipment->Package[$i]->Dimensions->Width = $this->_package_size_w;
            $request->Shipment->Package[$i]->Dimensions->Height = $this->_package_size_h;
            $request->Shipment->Package[$i]->PackageWeight = new stdClass();
            $request->Shipment->Package[$i]->PackageWeight->UnitOfMeasurement = new stdClass();
            $request->Shipment->Package[$i]->PackageWeight->UnitOfMeasurement->Code = 'KGS';
            $request->Shipment->Package[$i]->PackageWeight->UnitOfMeasurement->Description = 'Kilograms';
            $request->Shipment->Package[$i]->PackageWeight->Weight = $this->_package_weight;

            if($this->_declared_value) {
                $request->Shipment->Package[$i]->PackageServiceOptions = new stdClass();
                $request->Shipment->Package[$i]->PackageServiceOptions->DeclaredValue = new stdClass();
                $request->Shipment->Package[$i]->PackageServiceOptions->DeclaredValue->MonetaryValue = $this->_declared_value;
                $request->Shipment->Package[$i]->PackageServiceOptions->DeclaredValue->CurrencyCode = 'PLN';
            }

        }

        $request->Shipment->LabelSpecification = new stdClass();
        $request->Shipment->LabelSpecification->LabelImageFormat = new stdClass();
        $request->Shipment->LabelSpecification->LabelImageFormat->Code = 'GIF';
        $request->Shipment->LabelSpecification->LabelImageFormat->Description = 'GIF';
        $request->Shipment->LabelSpecification->Description = 'GIF';
        $request->Shipment->LabelSpecification->HTTPUserAgent = 'Mozilla/4.5';

        $request->Shipment->ShipmentServiceOptions = new stdClass();

        if($this->_cod) {

            //COD
            $request->Shipment->ShipmentServiceOptions->COD = new stdClass();
            $request->Shipment->ShipmentServiceOptions->COD->CODCode = 3;
            $request->Shipment->ShipmentServiceOptions->COD->CODFundsCode = 1;
            $request->Shipment->ShipmentServiceOptions->COD->CODAmount = new stdClass();
            $request->Shipment->ShipmentServiceOptions->COD->CODAmount->CurrencyCode = $this->_cod_currency;
            $request->Shipment->ShipmentServiceOptions->COD->CODAmount->MonetaryValue = $this->_cod_amount;
            //END COD
        }

        if($this->_notification && $this->_receiver_mail)
        {
            $request->Shipment->ShipmentServiceOptions->Notification = new stdClass();
            $request->Shipment->ShipmentServiceOptions->Notification->NotificationCode = 8;
            $request->Shipment->ShipmentServiceOptions->Notification->EMail = new stdClass();
            $request->Shipment->ShipmentServiceOptions->Notification->EMail->EMailAddress = $this->_receiver_mail;
        }

        if($this->_rod)
            $request->Shipment->ShipmentServiceOptions->ReturnOfDocumentIndicator = true;

        if($this->_requireSignature)
        {
            $request->Shipment->ShipmentServiceOptions->DeliveryConfirmation = new stdClass();
            $request->Shipment->ShipmentServiceOptions->DeliveryConfirmation->DCISType = 1;
        }

        if($this->_saturday_delivery)
            $request->Shipment->ShipmentServiceOptions->SaturdayDeliveryIndicator = 1;

//        $request->Shipment->ShipmentServiceOptions->InternationalForms = new stdClass();
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->FormType = '01';
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->Product = new stdClass();
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->Product->Description = 'abc';
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->Product->Unit = new stdClass();
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->Product->Unit->Number = 0;
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->Product->Unit->UnitOfMeasurement = new stdClass();
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->Product->Unit->UnitOfMeasurement->Code = 1;
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->Product->OriginCountryCode = 'PL';
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->Product->Unit->Value = 100;
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->ReasonForExport = 'SALE';
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->CurrencyCode = 'PLN';
//        $request->Shipment->ShipmentServiceOptions->InternationalForms->InvoiceDate = '20170312';

        // 13.10.2016
        // IN CASE OF IMPORT (PACKAGE FROM ABROAD TO PL)
        // RETURN SERVICE:
        if($this->_returnService) {
            $request->Shipment->ReturnService = new stdClass();
            $request->Shipment->ReturnService->Code = 9;
        }

        MyDump::dump('ups.txt', print_r($request,1));

        return $request;
    }

    public function __construct()
    {
        $this->wsdl = YiiBase::getPathOfAlias('application.components.ups').'/Ship.wsdl';
    }


//    public static function orderForCourierInternalSimple(CourierTypeInternalSimple $courierSimple, $toKaab = false, $returnError = false, $service = self::SERVICE_STANDARD)
//    {
//        $model = new self;
//
//        $model->_user_id = $courierSimple->courier->user_id;
//
//        if(self::_isServiceSaver($service))
//            $model->setModeType(self::MODE_SAVER);
//
//        $model->_cod = $courierSimple->courier->cod;
//        $model->_reference_no = $courierSimple->courier->local_id;
//
//        $model->_package_desc = S_Useful::removeNationalCharacters($courierSimple->courier->package_content);
//        $model->_package_size_l = $courierSimple->courier->package_size_l;
//        $model->_package_size_w = $courierSimple->courier->package_size_w;
//        $model->_package_size_h = $courierSimple->courier->package_size_d;
//        $model->_package_weight = $courierSimple->courier->package_weight;
//
//        // PICKUP_MOD / 14.09.2016
////        if($courier->with_pickup) {
//        if($courierSimple->with_pickup == 1) {
//
//            $model->_sender_addressLine = S_Useful::removeNationalCharacters($courierSimple->courier->senderAddressData->address_line_1);
//            $model->_sender_addressLine2 = S_Useful::removeNationalCharacters($courierSimple->courier->senderAddressData->address_line_2);
//
//            $model->_sender_countryCode = $courierSimple->courier->senderAddressData->country0->code;
//            $model->_sender_postalCode = $courierSimple->courier->senderAddressData->zip_code;
//            $model->_sender_name = S_Useful::removeNationalCharacters($courierSimple->courier->senderAddressData->name) . ' ' . S_Useful::removeNationalCharacters($courierSimple->courier->senderAddressData->company);
//            $model->_sender_number = $courierSimple->courier->senderAddressData->tel;
//            $model->_sender_city = S_Useful::removeNationalCharacters($courierSimple->courier->senderAddressData->city);
//        } else {
//            $model->_sender_addressLine = S_Useful::removeNationalCharacters('ul. Grodkowska 40');
//            $model->_sender_countryCode = 'PL';
//            $model->_sender_postalCode = '48300';
//            $model->_sender_name = S_Useful::removeNationalCharacters('SwiatPrzesylek');
//            $model->_sender_number = '+48221221218';
//            $model->_sender_city = S_Useful::removeNationalCharacters('Nysa');
//        }
//
//
//        if(!$toKaab)
//        {
//            $name = S_Useful::removeNationalCharacters($courierSimple->courier->receiverAddressData->name);
//            $company = S_Useful::removeNationalCharacters($courierSimple->courier->receiverAddressData->company);
//
//            $model->_receiver_name = $company != '' ? $company : $name;
//            $model->_receiver_attention_name = ($company != '' && $name != '') ? $name : '';
//
//            $model->_receiver_addressLine = S_Useful::removeNationalCharacters($courierSimple->courier->receiverAddressData->address_line_1);
//            $model->_receiver_addressLine2 = S_Useful::removeNationalCharacters($courierSimple->courier->receiverAddressData->address_line_2);
//            $model->_receiver_countryCode = $courierSimple->courier->receiverAddressData->country0->code;
//            $model->_receiver_postalCode = $courierSimple->courier->receiverAddressData->zip_code;
//            $model->_receiver_number = $courierSimple->courier->receiverAddressData->tel;
//            $model->_receiver_city = S_Useful::removeNationalCharacters($courierSimple->courier->receiverAddressData->city);
//
//            if($model->_sender_countryCode != $model->_receiver_countryCode && $model->_receiver_attention_name == '')
//                $model->_receiver_attention_name = trim($name.' '.$company);
//        } else {
//            $model->_receiver_addressLine = S_Useful::removeNationalCharacters('ul. Grodkowska 40');
//            $model->_receiver_countryCode = 'PL';
//            $model->_receiver_postalCode = '48300';
//            $model->_receiver_name = S_Useful::removeNationalCharacters('SwiatPrzesylek');
//            $model->_receiver_number = '+48221221218';
//            $model->_receiver_city = S_Useful::removeNationalCharacters('Nysa');
//
//            if($model->_sender_countryCode != $model->_receiver_countryCode && $model->_receiver_attention_name == '')
//                $model->_receiver_attention_name = $model->_receiver_name;
//        }
//
//        return $model->go($returnError);
//    }


    public static function orderForCourierDomestic(CourierTypeDomestic $courierDomestic, $returnError = false)
    {
        $model = new self;

        $model->_user_id = $courierDomestic->courier->user_id;

        if($courierDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_SAVER)
            $model->setModeType(self::MODE_SAVER);
        if($courierDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_EXPRESS OR $courierDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_M_EXPRESS)
            $model->setModeType(self::MODE_EXPRESS);

        $model->_reference_no = $courierDomestic->courier->local_id;

        $model->_package_desc = S_Useful::removeNationalCharacters($courierDomestic->courier->package_content);
        $model->_package_size_l = $courierDomestic->courier->package_size_l;
        $model->_package_size_w = $courierDomestic->courier->package_size_w;
        $model->_package_size_h = $courierDomestic->courier->package_size_d;
        $model->_package_weight = $courierDomestic->courier->package_weight;

        $model->_sender_addressLine = S_Useful::removeNationalCharacters($courierDomestic->courier->senderAddressData->address_line_1);
        $model->_sender_addressLine2 = S_Useful::removeNationalCharacters($courierDomestic->courier->senderAddressData->address_line_2);

        $model->_sender_countryCode = $courierDomestic->courier->senderAddressData->country0->code;
        $model->_sender_postalCode = $courierDomestic->courier->senderAddressData->zip_code;
        $model->_sender_name = S_Useful::removeNationalCharacters($courierDomestic->courier->senderAddressData->name).' '.S_Useful::removeNationalCharacters($courierDomestic->courier->senderAddressData->company);
        $model->_sender_number = $courierDomestic->courier->senderAddressData->tel;
        $model->_sender_city = S_Useful::removeNationalCharacters($courierDomestic->courier->senderAddressData->city);


        $name = S_Useful::removeNationalCharacters($courierDomestic->courier->receiverAddressData->name);
        $company = S_Useful::removeNationalCharacters($courierDomestic->courier->receiverAddressData->company);

        $model->_receiver_name = $company != '' ? $company : $name;
        $model->_receiver_attention_name = ($company != '' && $name != '') ? $name : '';

        $model->_receiver_addressLine = S_Useful::removeNationalCharacters($courierDomestic->courier->receiverAddressData->address_line_1);
        $model->_receiver_addressLine2 = S_Useful::removeNationalCharacters($courierDomestic->courier->receiverAddressData->address_line_2);
        $model->_receiver_countryCode = $courierDomestic->courier->receiverAddressData->country0->code;
        $model->_receiver_postalCode = $courierDomestic->courier->receiverAddressData->zip_code;
        $model->_receiver_number = $courierDomestic->courier->receiverAddressData->tel;
        $model->_receiver_city = S_Useful::removeNationalCharacters($courierDomestic->courier->receiverAddressData->city);
        $model->_receiver_mail = $courierDomestic->courier->receiverAddressData->email;


        $model->_number = $courierDomestic->courier->packages_number;
        if($courierDomestic->courier->cod)
        {
            $model->_cod = true;
            $model->_cod_currency = $courierDomestic->courier->cod_currency;
            $model->_cod_amount = $courierDomestic->courier->cod_value;
        }


        $model->_number_of_multipackages = $model->_number;


        if($courierDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_M_EXPRESS)
        {
            $model->access = '7D134EE28479E728';
            $model->userid = 'mr2016';
            $model->passwd = 'MarketingRelacji1';
            $model->shipper = '2F2774';

            $model->shipper_name = 'MRWaKlo';
            $model->shipper_address = 'Kłobucka 23A';
            $model->shipper_city = 'Warszawa';
            $model->shipper_postal = '02699';
            $model->shipper_country = 'PL';
            $model->shipper_tel = '+48518131577';
        }

        // custom UPS account for selected users
        if($courierDomestic->courier->user_id !== NULL)
        {
            if($courierDomestic->courier->user->getUpsCustomData('active'))
            {
                $model->access = $courierDomestic->courier->user->getUpsCustomData('id');
                $model->userid = $courierDomestic->courier->user->getUpsCustomData('login');
                $model->passwd = $courierDomestic->courier->user->getUpsCustomData('password');
                $model->shipper = $courierDomestic->courier->user->getUpsCustomData('shipper');
                $model->shipper_name = $courierDomestic->courier->user->getUpsCustomData('shipper_name');
                $model->shipper_address = $courierDomestic->courier->user->getUpsCustomData('shipper_address');
                $model->shipper_city = $courierDomestic->courier->user->getUpsCustomData('shipper_city');
                $model->shipper_postal = $courierDomestic->courier->user->getUpsCustomData('shipper_postal');
                $model->shipper_country = $courierDomestic->courier->user->getUpsCustomData('shipper_country');
                $model->shipper_tel = $courierDomestic->courier->user->getUpsCustomData('shipper_tel');
            }
        }

        // @06.06.2017 - special rule for user RUCH_UPS_COD
        if($courierDomestic->courier->user_id == 997 && strtoupper($model->shipper) == strtoupper('2F2771'))
        {
            $model->shipper_name = 'epinokio.pl';
            $model->shipper_tel = '506087585';
        }

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::UPS_DELIVERY_SATURDAY))
            $model->_saturday_delivery = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::UPS_ROD))
            $model->_rod = true;

        if($courierDomestic->hasAdditionById(CourierDomesticAdditionList::UPS_NOTIFICATION))
            $model->_notification = true;


        return $model->go($returnError);

    }


    public function go($returnError = false)
    {

        try
        {

            $mode = array
            (
                'soap_version' => 'SOAP_1_1',
                'trace' => 1,
                'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false)))
            );

            // initialize soap client
            $client = new SoapClient($this->wsdl , $mode);

            //set endpoint url
            $client->__setLocation($this->endpointurl);


            //create soap header
            $usernameToken['Username'] = $this->userid;
            $usernameToken['Password'] = $this->passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $this->access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
            $client->__setSoapHeaders($header);

            if(strcmp($this->operation,"ProcessShipment") == 0 )
            {
                $resp = $client->__soapCall('ProcessShipment',array($this->processShipmentOrder()));

//                MyDump::dump('ups.txt', 'RESP:' . print_r($resp,1));
                $responseCode = $resp->Response->ResponseStatus->Code;

                $temp = $resp->ShipmentResults->PackageResults;
                if(!S_Useful::sizeof($resp->ShipmentResults->PackageResults) OR S_Useful::sizeof($resp->ShipmentResults->PackageResults) == 1)
                    $temp = [ $temp ];

                $array = [];
                foreach($temp AS $PR)
                {
                    $img = ($PR->ShippingLabel->GraphicImage);

                    $label = imagecreatefromstring(base64_decode($img));
                    $label = imagerotate($label,-90,0);

                    ob_start();
                    imagepng($label);
                    $label =  ob_get_contents();
                    ob_end_clean();

                    $return = array(
                        'status' => $responseCode==1?true:false,
                        'track_id' => $PR->TrackingNumber,
                        'label' => $label,
                    );

                    array_push($array, $return);

                }

                return $array;
            }


        }
        catch(Exception $ex)
        {
//
            Yii::log(print_r($ex,1), CLogger::LEVEL_WARNING);
            if($returnError)
            {
                $return = array(0 => array(
                    'error' => $ex->getMessage().' / '.$ex->detail->Errors->ErrorDetail->PrimaryErrorCode->Description .'('.$ex->detail->Errors->ErrorDetail->PrimaryErrorCode->Code.')'));
                return $return;
            } else {
                return false;
            }
        }
    }

}

?>
