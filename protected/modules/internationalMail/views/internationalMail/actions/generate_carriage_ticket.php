<div class="flash-notice">
    <h4><?php echo Yii::t('internationalMail','List przewozowy');?></h4>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'mass-carriage-ticket',
        'action' => Yii::app()->createUrl('/internationalMail/internationalMail/carriageTicket', array('urlData' => $model->hash)),
        'htmlOptions' => array('class' => 'do-not-block',
            'onsubmit' => 'return confirm("'.Yii::t('site','Are you sure?').'");',
        ),
    ));
    ?>
    <?php

        echo CHtml::dropDownList('type','',array(InternationalMail::PDF_6_ON_1 => '6 on 1', InternationalMail::PDF_ROLL => 'roll'), array('style' => 'width: 120px; display: inline-block; height: 23px; vertical-align: top;'));

    ?>

    <?php echo TbHtml::submitButton(Yii::t('courier','Generuj list przewozowy'));?>
    <?php $this->endWidget();?>


</div>