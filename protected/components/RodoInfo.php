<?php
class RodoInfo extends CWidget
{
    const COOKIE_NAME = 'RODO_INFO';

    public function run()
    {
        $rodoInfo = isset(Yii::app()->request->cookies[self::COOKIE_NAME]) ? Yii::app()->request->cookies[self::COOKIE_NAME]->value : '';
//
        if ($rodoInfo != 10) {

            $content = Page::model()->with('pageTr')->findByPk(60);
            if ($content->pageTr) {
                $this->render('rodoInfo', [
                    'model' => $content->pageTr
                ]);
            }

        }

    }
}
?>