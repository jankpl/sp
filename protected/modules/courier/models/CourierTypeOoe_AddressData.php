<?php

class CourierTypeOoe_AddressData extends AddressData
{
    public $addToContactBook;
    public $contactBookType;

    public function rules() {

        $arrayValidate = array(

//            array('address_line_1',
//                'match',
//                'pattern' => '/^(.+)\s(.+)$/',
//                'message' => Yii::t('site', 'Adres musi zawierać ulicę oraz oddzielony spacją budynek i lokal'),
////                'except' => Courier::MODE_SAVE_NO_VALIDATE,
//                'enableClientValidation' => true
//            ),

            array('addToContactBook, contactBookType', 'safe'),

            array('email', 'email'),

            array('name, company, city, address_line_1, address_line_2, zip_code', 'application.validators.lettersNumbersBasicValidator'),
            array('tel', 'application.validators.telValidator'),

//            array('country_id, city, address_line_1, tel, zip_code, name, company', 'required'),
            array('country_id, city, address_line_1, tel, zip_code', 'required'),

            array('name', 'application.validators.eitherOneValidator', 'other' => 'company'),

            array('name,company,tel', 'length', 'max'=>30),
            array('address_line_1,address_line_2', 'length', 'max'=>45),

            array('email', 'length', 'max'=>45),

            array('city', 'length', 'max'=>45),
            array('zip_code', 'length', 'max'=>10),
            array('tel', 'length', 'max'=>30),

            array('country_id, country', 'safe'),

            array('id,name,company,country,zip_code,city,address_line_1,address_line_2,tel, email', 'safe', 'on'=>'search'),
        );


        $arrayFilter =
            array(
                array('country_id, date_entered, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'filter', 'filter' => array( $this, 'filterStripTags')),

                array('date_entered', 'default',
                    'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

                array('country_id, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'default', 'setOnEmpty' => true, 'value' => null),
            );

        return array_merge($arrayFilter, $arrayValidate);
    }

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bDefaultTelNumberAddressData' =>
                    [
                        'class'=>'application.modules.courier.models.bDefaultTelNumberAddressData'
                    ],
                'bClearEmailAddressData' =>
                    [
                        'class'=>'application.models.bClearEmailAddressData'
                    ],
                'bValidateZipCodeWithBaseAddressData' =>
                    [
                        'class'=>'application.models.bValidateZipCodeWithBaseAddressData'
                    ],
                'bFilterTelNumberAddressData' =>
                    [
                        'class'=>'application.models.bFilterTelNumberAddressData'
                    ],
            ]
        );
    }

    public function attributesLabels()
    {
        return CMap::mergeArray(
            [
                'addToContactBook' => Yii::t('_addressData','Dodaj do swojej ksi��ki adresowej'),
            ],
            parent::attributeLabels());
    }

    /**
     * List attributes including non-db attributes by default
     * @param bool|true $names
     * @param bool|false $omitNonDb Do not include special attributes
     * @return array
     */
    public function getAttributes($names = true, $omitNonDb = false)
    {
        $attributes = parent::getAttributes($names);
        if(!$omitNonDb) {
            $attributes['addToContactBook'] = $this->addToContactBook;
            $attributes['contactBookType'] = $this->contactBookType;
        }

        return $attributes;
    }


    public function afterSave()
    {
        if($this->addToContactBook)
        {
            UserContactBook::createAndSave(false, $this->getAttributes(true, true), $this->contactBookType);
        }

        return parent::afterSave();
    }

    /**
     * Constructor allowing setting userContactBook type for future saving (whether it's sender or receiver)
     *
     * @param string $scenario
     * @param string|false $contactBookType
     */
    public function __construct($scenario='insert', $contactBookType = false)
    {
        if($contactBookType)
            $this->contactBookType = $contactBookType;
        return parent::__construct($scenario);
    }
}