<?php
/* @var $model F_UImport */
?>
    <div id="upload-form">
        <div class="form">
            <?php

            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
                array(
                    'enableAjaxValidation' => false,
                    'htmlOptions' =>
                        array('enctype' => 'multipart/form-data'),
                )
            );
            ?>

            <h2><?php echo Yii::t('courier','Import packages from file');?></h2>

            <?php
            $this->widget('FlashPrinter');
            ?>

            <?php

            echo $form->errorSummary($model);
            ?>

            <table class="table table-striped" style="margin: 10px auto;">
                <tr>
                    <td style="width: 300px;"><?php echo $form->labelEx($model,'file', ['style' => 'width: 100%;']); ?></td>
                    <td><?php echo $form->fileField($model, 'file'); ?></td>
                </tr>
                <tr>
                    <td><?php echo $form->labelEx($model,'u_operator_id', ['style' => 'width: 100%;']); ?></td>
                    <td>
                        <?php echo $form->dropDownList($model, 'u_operator_id', CHtml::listData($model->operators, 'operator_id', 'operator_name'), ['prompt' => '-']); ?>
                        <div class="inline-form">
                            <div style="text-align: center;">
        <span class="courier-u-operators-selector-form">
            <?php
            if(!S_Useful::sizeof($model->operators)):
                ?>
                <span class="badge"><?= Yii::t('courier', 'Brak operatorów dla podanych parametrów paczki');?></span>
                <br/>
                <br/>
                <?php
            else:
                foreach($model->operators AS $operator):
                    ?>
                    <label class="smaller" data-name="<?= $operator['operatorModel']->name_customer;?>" data-broker-id="<?= $operator['operatorModel']->broker_id;?>" data-operator-id="<?= $operator['operatorModel']->id;?>" data-country-list-ids="<?= implode(',',$operator['country_list_ids']);?>">
                       <span <?= $operator['text'] ? 'rel="tooltip" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-html="true" data-title="'.$operator['text'].'"' : '';?>>
                           <?= CHtml::radioButton('CourierTypeU[courier_u_operator_id]', $operator['operatorModel']->id == $model->u_operator_id, ['data-broker' => $operator['operatorModel']->broker_id, 'value' => $operator['operatorModel']->id, 'data-operator-selector' => true]); ?>
                           <div class="operator-content-div" style="background-color: #<?= $operator['operatorModel']->getBgColor();?>">
    <div class="operator-u-name" style=" color: #<?= $operator['operatorModel']->getFontColor();?>;"><?= $operator['operatorModel']->name_customer;?></div>
    <div style="position: absolute; bottom: 10px; left: 0; right: 0;">
    </div>
</div>
                        </span>
                    </label>
                    <?php
                    $i++;
                endforeach;
            endif;
            ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; text-align: right;"><?= Yii::t('courier', 'Dostępne kraje:');?></td>
                    <td data-country-list-placeholder="true">-</td>
                </tr>
                <tr>
                    <td><?php echo $form->labelEx($model,'omitFirst', ['style' => 'width: 100%;']); ?></td>
                    <td><?php echo $form->checkBox($model, 'omitFirst'); ?></td>
                </tr>
                <tr>
                    <td><?php echo $form->labelEx($model,'cutTooLong', ['style' => 'width: 100%;']); ?></td>
                    <td><?php echo $form->checkBox($model, 'cutTooLong'); ?></td>
                </tr>
                <tr>
                    <td><?php echo $form->labelEx($model,'custom_settings', ['style' => 'width: 100%;']); ?></td>
                    <td>
                        <?php echo $form->dropdownList($model,'custom_settings', CHtml::listData(CourierImportSettings::listModelsForUserId(Yii::app()->user->id, CourierImportSettings::TYPE_U),'id','name'), ['id' => 'custom-settings', 'prompt' => '-']); ?> | <input type="button" class="btn btn-xs btn-default" id="open-settings-modal" data-settings-modal="edit" value="<?= Yii::t('courier', 'Customize settings');?>" style="display: none;"/> <input type="button" data-settings-modal="new" class="btn btn-xs btn-info" id="new-settings-modal" value="<?= Yii::t('courier', 'Add new settings');?>"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><?php echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('class' => 'btn btn-lg btn-primary'));?></td>
                </tr>
            </table>
            <?php
            $this->endWidget();
            ?>



            <div class="info">
                <div class="alert alert-info">
                    <?php echo Yii::t('courier', 'Maksymalna liczba wierszy w pliku to: {number1}. Maksymalna liczba paczek w pliku to: {number2}.', array('{number1}' => '<strong>'.F_UImport::MAX_LINES.'</strong>', '{number2}' => '<strong>'.F_UImport::MAX_PACKAGES.'</strong>')); ?>
                </div>
                <p><?php echo Yii::t('courier','Allowed file type:');?> .csv,.xls,.xlsx.</p>
                <br/>
                <p><?php echo Yii::t('courier','The file must contain following columns:');?><br/>
                    [packages_number],
                    [cod],
                    [package_size_l],
                    [package_size_w],
                    [package_size_d],
                    [package_value],
                    [package_content],
                    [sender_name],
                    [sender_company],
                    [sender_zip_code],
                    [sender_city],
                    [sender_address_line_1],
                    [sender_address_line_2],
                    [sender_tel],
                    [sender_email],
                    [receiver_name],
                    [receiver_company],
                    [receiver_zip_code],
                    [receiver_city],
                    [receiver_address_line_1],
                    [receiver_address_line_2],
                    [receiver_tel],
                    [receiver_email],
                    [cod_currency],
                    [cod_bank_account],
                    [ref],
                    [collection],
                    [additions]<sup>1</sup>,
                    [wrapping]<sup>2</sup>,
                    [value_currency],
                    [note1],
                    [note2],
                </p>
                <br/>
                <ul>
                    <li><strong><sup>1</sup></strong>: <?= Yii::t('courier', 'Lista ID dodatków oddzielonych "," lub ";".');?> <a href="#" data-toggle="modal" data-target="#additions-list" class="btn btn-xs btn-primary"><?= Yii::t('courier', 'Zobacz dodatki');?></a></li>
                    <li><strong><sup>2</sup></strong>: <?= Yii::t('courier', 'Typ opakowania');?>: <?php foreach(User::getWrappingList() AS $id => $item) echo '<i>'.$id.'</i> = '.$item.' | ';?></li>
                </ul>
                <?php echo CHtml::link(Yii::t('courier','Download file template'), Yii::app()->baseUrl.'/misc/ipff_template_u.xlsx', array('target' => '_blank', 'class' => 'btn btn-success', 'download' => 'ipff_template_u.xlsx'));?>
                <br/>
                <br/>
                <div class="alert alert-info">
                    <?php echo Yii::t('courier', 'W {a}ustawieniach profilu{/a} istnieje możliwość wyłączenia powiadomień SMS oraz email.', array('{a}' => '<a href="'.Yii::app()->createUrl('/user/update').'#block-notify-form" target="_blank">', '{/a}' => '</a>')); ?>
                </div>

                <br/>

            </div>
        </div>
    </div>



    <div class="modal fade" id="additions-list" tabindex="-1" role="dialog" aria-labelledby="additions-list">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?= Yii::t('courier', 'Lista dodatków');?></h4>
                </div>
                <div class="modal-body">
                    <?= Yii::t('courier', 'Dostępność danego dodatku może być zależna od parametrów paczki oraz adresu nadawcy i odbiorcy.');?><br/><br/>
                    <div style="height: 500px; overflow-y: scroll">
                        <table class="table table-striped table-condensed table-bordered text-center">
                            <tr>
                                <th>Id</th>
                                <th><?= Yii::t('courier', 'Dodatek');?></th>
                                <th><?= Yii::t('courier', 'Operator');?></th>
                                <th><?= Yii::t('courier', 'Cena netto/brutto');?></th>
                            </tr>
                            <?php
                            /* @var $model CourierUAdditionList */
                            $models = CourierUAdditionList::model()->with('courierUAdditionListTr')->findAllByAttributes(['stat' => 1]);
                            foreach($models As $model):
                                ?>
                                <tr>
                                    <td><?= $model->id;?></td>
                                    <td><?= $model->getClientTitle();?></td>
                                    <td><?= $model->broker_id ? CourierUOperator::getBrokerName($model->broker_id) : '-';?></td>
                                    <td><?= S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($model->getPrice(), true)).' '.Yii::app()->PriceManager->getCurrencySymbol().'/'.S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->getPrice())).' '.Yii::app()->PriceManager->getCurrencySymbol();
                                        ?></td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('site', 'Zamknij');?></button>
                </div>
            </div>
        </div>
    </div>
<?php
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/custom-theme/jquery-ui-1.10.3.custom.min.css');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courier-import-settings.js');

$script = "prepareSettingsModal({
        textConfirmDelete: '".Yii::t('courier', 'Na pewno?')."',
        textError: '".Yii::t('courier', 'Wystąpił bład zapisywania. Spróbuj ponownie.')."',
        ajaxUrl: '".Yii::app()->urlManager->createUrl('/courier/courier/ajaxImportModalSettings')."',
         type: '".CourierImportSettings::TYPE_U."',
    });";
Yii::app()->clientScript->registerScript('courier-import-settings', $script);


$script = "
    function getCountryList(country_list_ids) {
    
        var placeholder = $('[data-country-list-placeholder]');
        placeholder.html('?');
        
        $.ajax({
            url: '".Yii::app()->createUrl('/courier/courierU/ajaxGetCountryList')."',
            data: {country_list_ids: country_list_ids},
            method: 'POST',
            dataType: 'json',
            success: function (result) {

                

                if(result instanceof Array && result.length)
                {
                       placeholder.html(result.join(', '));
                       
                
                } else {
                    placeholder.html('-');
                }
                
            },
            error: function (e) {

                console.log(e);
            }
        });
    }

var selectedOperator = $('#F_UImport_u_operator_id').val();

if(selectedOperator)
{    
    $('[data-operator-selector][value=' + selectedOperator + ']').attr('checked', true);          
    getCountryList($('[data-operator-selector][value=\"' + selectedOperator + '\"]').closest('label').attr('data-country-list-ids'));
}

$('[rel=\"tooltip\"]').tooltip();

$('[data-operator-selector]').on('change', function(){
    $('#F_UImport_u_operator_id').val($(this).val());
    getCountryList($('[data-operator-selector][value=\"' + $(this).val() + '\"]').closest('label').attr('data-country-list-ids'));
});

$('#F_UImport_u_operator_id').on('change', function(){

    $('[data-operator-selector]:checked').prop('checked', false);
    $('[data-operator-selector][value=' + $(this).val() + ']').prop('checked', true);
    getCountryList($('[data-operator-selector][value=\"' + $(this).val() + '\"]').closest('label').attr('data-country-list-ids'));

});";

Yii::app()->clientScript->registerScript('import-js', $script, CClientScript::POS_READY);
?>
