<?php

class LabelAnnotor
{
    const MODE_L = 'L';
    const MODE_R = 'R';
    const MODE_T = 'T';
    const MODE_B = 'B';

    const ATTR_CONTENT = 'content';
    const ATTR_REF = 'ref';
    const ATTR_NOTE1 = 'note1';
    const ATTR_NOTE2 = 'note2';

    public static function getModeList() : array
    {
        return [
            self::MODE_L => 'Left',
            self::MODE_R => 'Right',
            self::MODE_T => 'Top',
            self::MODE_B => 'Bottom',
        ];
    }

    public static function getAttributeList() : array
    {
        return [
            self::ATTR_CONTENT => 'content',
            self::ATTR_REF => 'ref',
            self::ATTR_NOTE1 => 'note1',
            self::ATTR_NOTE2 => 'note2',
        ];
    }

    public static function annotateLabel($image, $mode, $text) : string
    {
        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        $im->readImageBlob($image);
        $im->setImageFormat('png8');

        $width = $im->getImageWidth();
        $height = $im->getImageHeight();
        $draw = new ImagickDraw();

        $draw->setTextUnderColor(new ImagickPixel('white'));
        $draw->setFillColor('black');
        $draw->setStrokeColor('black');

        if ($mode == self::MODE_L OR $mode == self::MODE_R) {

            $addition = 30 * round($height / 1800);

            $draw->setFontSize($addition);

            $new = new Imagick();
            $newWidth = $addition * 1.1;
            $new->newImage($height, $newWidth, new ImagickPixel('white'));
            $new->setImageFormat('png');
            $new->annotateImage($draw, 0, $addition, 0, $text);
            $new->rotateImage(new ImagickPixel('white'), 90);

            $imTotal = new \Imagick();
            $imTotal->setResolution(300, 300);

            $imTotal->newimage($width + $newWidth, $height, '#ffffff');

            if($mode == self::MODE_R)
            {
                $imTotal->compositeimage($im, \Imagick::COMPOSITE_DEFAULT, 0, 0);
                $imTotal->compositeimage($new, \Imagick::COMPOSITE_DEFAULT, $width, 0);

            } else {
                $imTotal->compositeimage($new, \Imagick::COMPOSITE_DEFAULT, 0, 0);
                $imTotal->compositeimage($im, \Imagick::COMPOSITE_DEFAULT, $newWidth, 0);
            }

        }
        else if ($mode == self::MODE_T OR $mode == self::MODE_B) {

            $addition = 30 * round($width / 1100);

            $draw->setFontSize($addition);

            $new = new Imagick();
            $newHeight = $addition * 1.1;
            $new->newImage($height, $newHeight, new ImagickPixel('white'));
            $new->setImageFormat('png');
            $new->annotateImage($draw, 0, $addition, 0, $text);


            $imTotal = new \Imagick();
            $imTotal->setResolution(300, 300);

            $imTotal->newimage($width, $height + $newHeight, '#ffffff');

            if($mode == self::MODE_B)
            {
                $imTotal->compositeimage($im, \Imagick::COMPOSITE_DEFAULT, 0, 0);
                $imTotal->compositeimage($new, \Imagick::COMPOSITE_DEFAULT, 0, $height);
            } else {
                $imTotal->compositeimage($new, \Imagick::COMPOSITE_DEFAULT, 0, 0);
                $imTotal->compositeimage($im, \Imagick::COMPOSITE_DEFAULT, 0, $newHeight);
            }

        }

        $imTotal->setImageFormat('png8');
        $imTotal->trimImage(0);

        return $imTotal->getImageBlob();
    }
}