$(document).ready(function() {

    $("form").not(".do-not-block").submit(function(event) {

        var form = event.currentTarget;

        var overlay = $("<div>");
        overlay.addClass('overlay');

        var loader = $("<div>");
        loader.addClass('loader');

        overlay.append(loader);

        $("#" + form.id).css('position','relative');
        $("#" + form.id).append(overlay);

        return true;
    });
});

