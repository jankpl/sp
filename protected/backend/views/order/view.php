<?php
/* @var $model Order */
?>

<?php

$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
);
?>

<h1>Order #<?php echo $model->local_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'tabs' => array(
        array('label' => 'Details', 'content' => $this->renderPartial('_view_details', array('model' => $model), true), 'active' => (($activeTab=='' || $activeTab==1)?true:false)),
        array('label' => 'Products', 'content' =>  $this->renderPartial('_view_products', array('model' => $model), true), 'active' => (($activeTab==2)?true:false)),
        array('label' => 'Change status', 'content' =>  $this->renderPartial('_view_change_status', array('model' => $model), true), 'active' => (($activeTab==3)?true:false)),
        array('label' => 'History', 'content' =>  $this->renderPartial('_view_history', array('model' => $model), true), 'active' => (($activeTab==4)?true:false)),
    ),
)); ?>

