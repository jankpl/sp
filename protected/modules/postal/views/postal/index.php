
<h2><?php echo Yii::t('postal','Przesyłki');?></h2>

<?= CHtml::link(Yii::t('postal', 'Twoje listy'), ['/postal/postal/list'], ['class' => 'btn btn-lg btn-primary']); ?><br/><br/>

<?php echo CHtml::link(Yii::t('courier','Track&Trace'),array('/site/tt'), array('class' => 'btn')); ?>

<?= CHtml::link(Yii::t('postal', 'Potwierdzenia z dzisiaj'), ['/postal/postal/ackCardToday'], ['class' => 'btn']); ?>

<?php echo CHtml::link(Yii::t('courier','Masowe zarządanie przesyłkami'),array('/postal/postalScanner'), array('class' => 'btn')); ?>

<?php echo CHtml::link(Yii::t('postal','Zapytaj o ofertę specjalną'),array('/site/contact'), array('class' => 'btn')); ?>

<?php echo CHtml::link(Yii::t('postal','Przygotowanie listów'),array('/postal/postal/prepare'), array('class' => 'btn')); ?>

<br/>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file',
    'action' => Yii::app()->createUrl('/postal/postal/exportToFileAll'),
    'htmlOptions' => [
        'class' => 'do-not-block',
    ]
));
echo TbHtml::submitButton(Yii::t('postal','Eksportuj wszyskie przesyłki do .xls').' :', ['style' => 'margin-top: -6px; margin-right: 5px;']);
echo Chtml::dropDownList('period', '', S_Useful::listPeriods(), ['style' => 'height: 30px;']);
$this->endWidget();
?>


<hr/>

<h3><?php echo Yii::t('postal','Nowe przesyłki pojedyncze');?></h3>


<?= CHtml::link(Yii::t('postal', 'Pojedynczy list'), ['/postal/postal/create'], ['class' => 'btn']); ?>

<?= CHtml::link(Yii::t('postal', 'Importuj z pliku'), ['/postal/postal/import'], ['class' => 'btn ']); ?>

<?= CHtml::link(Yii::t('postal', 'Importuj z eBay.com'), ['/postal/postal/ebay'], ['class' => 'btn ']); ?>

<br/>
<br/>

<h3><?php echo Yii::t('postal','Nowe przesyłki masowe');?></h3>

<?= CHtml::link(Yii::t('postal', 'Korespondencja masowa'), ['/postal/postal/createBulk'], ['class' => 'btn']); ?>






