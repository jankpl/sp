<?php
/* @var $data array */
/* @var $countriesModels array */
/* @var $model array */
?>

<h1>Import courier price</h1>

<?php

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
    array(
        'enableAjaxValidation' => false,
        'htmlOptions' =>
            array('enctype' => 'multipart/form-data'),
    )
);
?>

<h2><?php echo Yii::t('courier','Import packages from file');?></h2>

<?php
$this->widget('FlashPrinter');
?>

<?php

echo $form->errorSummary($model);
?>

<table class="table table-striped" style="margin: 10px auto;">
    <tr>
        <td style="width: 300px;"><?php echo $form->labelEx($model,'file', ['style' => 'width: 100%;']); ?></td>
        <td><?php echo $form->fileField($model, 'file'); ?></td>
    </tr>
    <tr>
        <td></td>
        <td>First line will be omitted!</td>
    </tr>
    <tr>
        <td><?php echo $form->labelEx($model,'user_group_id', ['style' => 'width: 100%;']); ?></td>
        <td>
            <?php
            $groups[-1] = '-- DEFAULT --';
            $groups +=  CHtml::listData(UserGroup::listGroups(),'id','name');


            echo $form->dropdownList($model,'user_group_id', $groups, ['prompt' => '-']); ?>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><?php echo TbHtml::submitButton(Yii::t('app', 'Upload'), array('class' => 'btn btn-lg btn-primary'));?></td>
    </tr>
</table>
<?php
$this->endWidget();
?>


<h2>Legend</h2>

<div style="overflow: auto; vertical-align: top;">
    <div style="width: 30%; display: inline-block; margin: 0 5px; vertical-align: top;">
        <h3>country_group_id</h3>
        <div style="overflow-y: scroll; max-height: 350px;">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Name</th>
                    <th>Id</th>
                </tr>
                <?php
                foreach(CourierCountryGroup::model()->primary()->findAll() AS $model):
                    ?>
                    <tr>
                        <td><?= $model->name;?></td>
                        <td><?= $model->id;?></td>
                    </tr>
                <?php
                endforeach;
                ?>
            </table>
        </div>
    </div>

</div>

<?php
if(isset(Yii::app()->session['CPIU_GET'])):
    ?>
<script>
    window.location.reload();
</script>
<?php
endif;
?>
