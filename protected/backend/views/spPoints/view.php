<?php
/* @var $this SpPointsController */
/* @var $model SpPoints */

$this->breadcrumbs=array(
	'Sp Points'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Create SpPoints', 'url'=>array('create')),
	array('label'=>'Update SpPoints', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SpPoints', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SpPoints', 'url'=>array('admin')),
);
?>

<h1>View SpPoints #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date_entered',
		'date_updated',
		'name',
		'stat',
	),
)); ?>
<br/>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model->addressData,
	'attributes'=>array(
		'name',
		'company',
		'address_line_1',
		'address_line_2',
		'city',
		'zip_code',
		'country',
	),
)); ?>
