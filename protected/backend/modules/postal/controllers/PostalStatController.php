<?php

class PostalStatController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'PostalStat'),
        ));
    }

    public function actionCreate() {
        $model = new PostalStat;
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $statTr = new PostalStatTr('create');
            $statTr->language_id = $item->id;
            $modelTrs[$item->id] = $statTr;
        }

        if (isset($_POST['PostalStat'])) {
            $model->setAttributes($_POST['PostalStat']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            $model->validate();

            if($model->hasErrors())
                $errors = true;

            $model->save();

            foreach(Language::model()->findAll('stat = 1') AS $item)
            {
                if($modelTrs[$item->id] === null)
                    $modelTrs[$item->id] = new PostalStatTr;

                $modelTrs[$item->id]->setAttributes($_POST['PostalStatTr'][$item->id]);
                $modelTrs[$item->id]->postal_stat_id = $model->id;
                $modelTrs[$item->id]->language_id = $item->id;

                if($modelTrs[$item->id]->short_text == '' && $modelTrs[$item->id]->full_text == '')
                    continue;

                if(! $modelTrs[$item->id]->save())
                    $errors = true;
            }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
            else
            {
                $transaction->rollback();
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionUpdate($id) {


        $model = $this->loadModel($id, 'PostalStat');

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = PostalStatTr::model()->find('postal_stat_id=:postal_stat_id AND language_id=:language_id', array(':postal_stat_id' => $model->id, ':language_id' => $item->id));

            if($modelTrs[$item->id] == NULL)
            {
                $statTr = new PostalStatTr('create');
                $statTr->language_id = $item->id;
                $modelTrs[$item->id] = $statTr;
            }
        }

        if (isset($_POST['PostalStat'])) {
            $model->setAttributes($_POST['PostalStat']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            $model->validate();

            if($model->hasErrors())
                $errors = true;


            $model->save();

            foreach(Language::model()->findAll('stat = 1') AS $item)
            {
                if($modelTrs[$item->id] === null)
                    $modelTrs[$item->id] = new PostalStatTr;
                $modelTrs[$item->id]->setAttributes($_POST['PostalStatTr'][$item->id]);
                $modelTrs[$item->id]->postal_stat_id = $model->id;
                $modelTrs[$item->id]->language_id = $item->id;

                if($modelTrs[$item->id]->short_text == '' && $modelTrs[$item->id]->full_text == '')
                    continue;

                if(! $modelTrs[$item->id]->save())
                    $errors = true;
            }

            if (!$errors) {
                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));
            }
            else
            {
                $transaction->rollback();
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {

        $model = PostalStat::model()->findByPk($id);
        if($model == NULL)
            throw new CHttpException(404);

        if($model->editable != S_Status::ACTIVE )
            throw new CHttpException(403, 'This status cannot be edited or deleted');

        if($model->operator_source_id != NULL)
            throw new CHttpException(403, 'This status cannot be deleted');

        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $model =  $this->loadModel($id, 'PostalStat');

                foreach($model->postalStatTrs AS $item)
                {
                    $item->delete();


                }

                $model->delete();
            }
            catch (Exception $ex)
            {
                throw new CHttpException(400, Yii::t('app', 'You cannot delete it!'));
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {

        $model = new _PostalStatGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['PostalStatGridView']))
            $model->setAttributes($_GET['PostalStatGridView']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}