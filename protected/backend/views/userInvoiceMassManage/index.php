<?php
$sid = uniqid();
/* @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('/userInvoiceMassManage').'?sid='.$sid,
    'id'=>'user-scanner-form',
    'enableClientValidation' => true,
    'htmlOptions' => [
        'class' => 'do-not-block',
        'target' => 'scanner-target',
    ]
)); ?>

    <h2>Mass manage - User Invoice</h2>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

    <p>Provide user invoices numbers, for which you want to perform an action. Not found or invalid items will be ignored.</p>
    <p>Max number of items at once: <?= USerInvoiceMassManageController::MAX_NO;?></p>
    <br/>
    <iframe name="scanner-target" id="scanner-target" style="width: 0; height: 0; display: none;"></iframe>

    <div id="message-info" class="alert alert-info" style="display: none; margin: 15px 0;"></div>
    <div id="message-success" class="alert alert-success" style="display: none; margin: 15px 0;"></div>
    <div id="message-danger" class="alert alert-danger" style="display: none; margin: 15px 0;"></div>

    <div id="summary-block" style="display: none; padding-bottom: 15px;">
        <table class="table" style="max-width: 600px; margin: 15px auto;">
            <tr>
                <th class="text-right"><?= Yii::t('userScanner', 'Przetworzonych');?></th>
                <th class="text-center"><?= Yii::t('userScanner', 'Łącznie');?></th>
                <th><?= Yii::t('userScanner', 'Pominiętych');?></th>
            </tr>
            <tr style="font-size: 1.4em;">
                <td id="success-no" class="text-right" style="color: green;">0</td>
                <td id="total-no" class="text-center">0</td>
                <td id="fail-no" style="color: red;">0</td>
            </tr>
            <tr>
                <td><textarea id="success-list" style="width: 100%; resize: vertical; height: 150px; text-align: center;"></textarea></td>
                <td></td>
                <td><textarea id="fail-list" style="width: 100%; resize: vertical; height: 150px; text-align: center;"></textarea></td>
            </tr>
        </table>
    </div>
    <hr/>
    <div class="row text-center">
        <div class="span5">
            Inserted rows <strong id="numer-of-rows">0</strong><br/>
            <?= $form->textArea($model, 'items_ids',['id' => 'items-ids', 'style' => 'padding: 15px 5px; text-align: center; width: 400px; font-size: 1.1em; font-weight: bold; height: 500px; resize: vertical;', 'placeholder' => 'Provide invoices numbers, each one in new line']);?>
        </div>
        <div class="span6 text-left">
            <br/>
            <div id="action-buttons">

                <?= CHtml::submitButton('Export files in ZIP', ['class' => 'btn btn-large btn-primary', 'name' => 'export']);?> <br/> <br/>
            </div>
            <div id="too-much-warning" style="display: none;" class="alert alert-danger text-center">
               Too many items!
            </div>
        </div>
    </div>

<?php
$this->endWidget(); ?>


<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  statusCheck: '.CJSON::encode(Yii::app()->createUrl('/userInvoiceMassManage/status', ['sid' => $sid])).',
              },
               loader: {
                  textTop: '.CJSON::encode('In progress...').',
                  textBottom: '.CJSON::encode('Please wait...').',
                  error:  '.CJSON::encode('Error occured! Please try again...').',
              },
              stat: {
                 success: '.CJSON::encode(UserInvoiceMassManageController::STATE_SUCCESS).',
                 fail: '.CJSON::encode(UserInvoiceMassManageController::STATE_FAIL).',
              },
              no: {
                 max: '.CJSON::encode(UserInvoiceMassManageController::MAX_NO).',
              },
              text: {
                 noItems: '.CJSON::encode('No item id provided!').',
              },
          };
      ',CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/user-scanner.js', CClientScript::POS_END);
?>