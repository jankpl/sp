<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);

if($message == '')
{
	if($error['code'] == 403)
		$error['message'] = Yii::t('site', 'Nie masz uprawnień do tej treści.');
	else if($error['code'] == 404)
		$error['message'] = Yii::t('site', 'Nie ma takiej strony.');
	else if($error['type'] == 'AccountLockedException')
        $error['message'] = Yii::t('site', 'To konto nie ma możliwości tworzenia nowych zleceń');

}



$desc = Yii::t('site', 'Ups, coś się nie udało... Skontaktuj się z nami, to poszukamy rozwiązania tego problemu!');

if(in_array($error['code'], [ErrorCodes::EBAY_PROBLEM, ErrorCodes::LABEL_PROBLEM, 404,  ErrorCodes::NO_PAYMENT_AVAILABLE]) OR $error['type'] == 'AccountLockedException')
	$desc = $error['message'];


if($error['code'] != 404)
	$error['code'] = '';

?>

<h2><?php echo Yii::t('site','Błąd {error}!', array('{error}' => $error['code']));?></h2>

<div class="alert alert-danger">
	<?= $desc; ?>
	<?php //echo CHtml::encode($message); ?>
</div>

<div style="text-align: center;"><a class="btn btn-lg" href="<?= Yii::app()->createAbsoluteUrl('/');?>"><?= Yii::t('site','Strona główna');?></a></div>
