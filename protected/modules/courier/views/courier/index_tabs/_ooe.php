<div class="alert alert-info"><?php $this->widget('ShowPageContent', ['id' => 26]);?></div>

<?php echo CHtml::link(Yii::t('courier','Utwórz nową przesyłkę'),array('/courier/courierOoe/create'), array('class' => 'btn btn-primary')); ?>

<?php echo CHtml::link(Yii::t('courier','Importuj przesyłki OOE z pliku'),array('/courier/courierOoe/import'), array('class' => 'btn')); ?>

<?php echo CHtml::link(Yii::t('courier','Importuj przesyłki OOE z eBay.com'),array('/courier/courierOoe/ebay'), array('class' => 'btn')); ?>
<br/>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file-ooe',
    'action' => Yii::app()->createUrl('/courier/courier/exportToFileAll', ['mode' => Controller::EXPORT_MODE_OOE]),
    'htmlOptions' => [
        'class' => 'do-not-block',
    ]
));
?>
<hr/>
<?php
echo TbHtml::submitButton(Yii::t('courier','Eksportuj wszyskie przesyłki do .xls').' :', ['style' => 'margin-top: -6px; margin-right: 5px;']);
echo Chtml::dropDownList('period', '', S_Useful::listPeriods(), ['style' => 'height: 30px;']);
$this->endWidget();
?>