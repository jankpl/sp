<?php

class F_CourierRoutingExport extends CFormModel
{

    public $country_list_id;
    public $user_group_id;


    public function rules() {
        return array(
            array('country_list_id, user_group_id', 'required'),
            array('user_group_id', 'in', 'range' => CHtml::listData(UserGroup::model()->findAll(),'id', 'id')),
        );
    }

    public function listCountryGroups()
    {
        return CourierCountryGroup::model()->findAll('parent_courier_country_group_id IS NULL AND user_group_id IS NULL');
    }

    public function afterValidate()
    {
        if($this->user_group_id == -1)
            $this->clearErrors('user_group_id');

        parent::afterValidate();
    }


}