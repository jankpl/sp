<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'plugins-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 64)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'stat'); ?>
		<?php echo $form->dropDownList($model, 'stat', CHtml::listData(S_Status::listStats(),'id','name')); ?>
		<?php echo $form->error($model,'stat'); ?>
		</div><!-- row -->



    <div id="langVersionMenuContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem)
            echo'<div class="langVersionMenu" id="langVersionMenu_'.$languageItem->id.'">'.$languageItem->short_name.'</div>';
        ?>
    </div>


    <div id="langVersionContainer">
        <?php
        foreach(Language::model()->findAll('stat = 1') AS $languageItem):

            echo '<div class="langVersion" id="langVersion_'.$languageItem->id.'">';

            $model_tr = $modelTrs[$languageItem->id];
            if($model_tr === null)
            {
                $model_tr = new PluginsTr();
                $model_tr->language_id = $languageItem->id;
            }

            $this->renderPartial('_tr_form', array(
                'model' => $model_tr,
                'form' => $form,
                'i' => $languageItem->id,
            ));

            echo'</div>';
        endforeach;
        ?>
    </div>

    <?php
    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/js.langVersion.js');
    ?>

    <div>

        <div class="row" id="image-uploader">
            <?php echo $form->labelEx($model,'img'); ?>
            <p>Image will be scalled to max. width: <?= Plugins::IMG_WIDTH;?>px and max. height: <?= Plugins::IMG_HEIGHT;?>px.</p>
            <div class="img-container" style="width: 50%;">
                <div class="drop_zone" id="drop_zone_multi">
                    <div class="dz_waiting">
                        Przeciągnij tutaj zdjęcie lub kliknij <span class="glyphicon glyphicon-share-alt"></span>
                    </div>
                    <div class="dz_drop">
                        Upuść obrazek <span class="glyphicon glyphicon-arrow-down"></span>
                    </div>
                </div>
                <input type="file" data-id="imageLoaderInput" name="imageLoaderInput" style="display: none;" accept="image/gif, image/jpeg, image/jpg, image/png"/>
            </div>
            <?= $form->hiddenField($model, '__img_temp', ['data-img-data' => 'true']); ?>
            <?= $form->error($model, '__img_temp'); ?>

        </div>
        <p>Preview:</p>
        <div class="row" id="image-preview" style="width: <?= (Plugins::IMG_WIDTH + 20);?>px; height: <?= (Plugins::IMG_WIDTH+ 20);?>px;">
        </div>

    </div>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/dropImage.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/preloader.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/backend/dropImage.css');
Yii::app()->clientScript->registerScript('jsHelpers', '
          jsHelpers = {
              url: {
                  resizeImageUrl: '.CJSON::encode(Yii::app()->createAbsoluteUrl('/plugins/ajaxPrepareImage')).',
              },
               text: {
                  imageSaved: '.CJSON::encode('Twój obrazek został zapisany!').',
                  imageSaveError: '.CJSON::encode('Ups, coś się nie udało...').',
                  onlyImagesError: '.CJSON::encode('Dozwolone są jedynie obrazki').',
                  imageTooBigError: '.CJSON::encode('Maksymanla dozwolona waga pliku to: ').',
              },
          };
      ', CClientScript::POS_HEAD);