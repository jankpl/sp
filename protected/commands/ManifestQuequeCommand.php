<?php

class ManifestQuequeCommand extends ConsoleCommand {

    const DEUTCHE_POST = 1;
    const GLT_IT = 2;
    const ANPOST = 3;
    const SWEDEN_POST = 4;
    const POST11 = 5;
    const YUN_EXPRESS_SYNCH = 6; // not in index - separte call in cronjob

    public function init()
    {
        parent::init();
    }

    public function filters()
    {
        return array();
    }

    function actionIndex()
    {
        exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic manifestQueque runOne --what=".self::DEUTCHE_POST." > /dev/null &");
        exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic manifestQueque runOne --what=".self::GLT_IT." > /dev/null &");
        exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic manifestQueque runOne --what=".self::ANPOST." > /dev/null &");
        exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic manifestQueque runOne --what=".self::SWEDEN_POST." > /dev/null &");
        exec("/usr/local/php73/bin/php /home/kaab/domains/swiatprzesylek.pl/public_html/protected/yiic manifestQueque runOne --what=".self::POST11." > /dev/null &");
//        exec("php C:/Dane/WWW/Localhost/httpd/SwiatPrzesylek.pl/swiatPrzesylek_ms5/protected/yiic manifestQueque runOne --what=".self::DEUTCHE_POST."");
    }


    function actionRunOne($what)
    {
        echo 'RUN: '.$what;

        if($what == self::DEUTCHE_POST) {
            try {
                ManifestQueque::runQuequeSingle(GLOBAL_BROKERS::BROKER_DEUTCHE_POST, date('Y-m-d', strtotime("-1 day")));
            } catch (Exception $ex) {
            }

        }

        else if($what == self::GLT_IT)
        {
            try {
                ManifestQueque::runQuequeOnceADay(GLOBAL_BROKERS::BROKER_GLS_IT);
            } catch (Exception $ex) {}
        }

        else if($what == self::ANPOST)
        {
            try {
                ManifestQueque::runQueuqe(GLOBAL_BROKERS::BROKER_ANPOST);
            } catch (Exception $ex) {}
        }

        else if($what == self::SWEDEN_POST)
        {
            try {
                ManifestQueque::runQueuqe(GLOBAL_BROKERS::BROKER_SWEDEN_POST);
            } catch (Exception $ex) {}

        }

        else if($what == self::POST11)
        {
            try {
                ManifestQueque::runQuequeSingle(GLOBAL_BROKERS::BROKER_POST11, date('Y-m-d', strtotime("-1 day")));
            } catch (Exception $ex) {}
        }

        else if($what == self::YUN_EXPRESS_SYNCH)
        {
            try {
                ManifestQueque::runQueuqe(GLOBAL_BROKERS::BROKER_YUNEXPRESS_SYNCH, false);
            } catch (Exception $ex) {}
        }

        echo 'END';
    }



}