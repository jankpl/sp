<?php

class RodoRegulations
{
    public static function regulationsRules($on = 'summary')
    {
        return [
//            array('regulations, regulations_rodo', 'required', 'on' => 'summary'),
            array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('courier', 'Musisz zaakceptować regulamin.'), 'on' => $on),
            array('regulations_rodo', 'compare', 'compareValue' => true, 'message' => Yii::t('rodo', 'Potwierdzenie jest wymagane'), 'on' => $on),
        ];
    }

    public static function attributeLabels() {

        return [
            'regulations' => Yii::t('m_app','I accept {a}terms of use{/a}', array('{a}' => '<a href="'.Yii::app()->createUrl('page/view', array('id' => 2)).'" target="_blank">', '{/a}' => '</a>')),
            'regulations_rodo' => Yii::t('rodo', 'Zapoznałem się i akcetpuję {tooltip}informację o administratorze i warunki przetwarzaniu danych{/tooltip} ', ['{tooltip}' => '<a rel="tooltip" title="" data-original-title="'.ContactForm::getRodoFullText().'" style="text-decoration: underline;">', '{/tooltip}' => '</a>'])
        ];

    }


}