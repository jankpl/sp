<?php

Yii::import('application.models._base.BaseEbayImport');

class EbayImport extends BaseEbayImport
{
    const TARGET_COURIER = 0;
    const TARGET_POSTAL = 2;

    const STAT_NEW = 1;
    const STAT_SUCCESS = 2;
    const STAT_ERROR = 9;

    const STAT_UNKNOWN = 19;

    const STAT_CANCELLED = 13;


    const SOURCE_EBAY = 0;
    const SOURCE_LINKER = 1;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),

        );
    }

    public function rules() {
        return array(
            array('target_item_id, ebay_order_id, stat, source', 'required'),
            array('target_item_id, stat, target, source', 'numerical', 'integerOnly'=>true),
            array('ebay_order_id', 'length', 'max'=>32),
            array('date_updated', 'safe'),
            array('date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, target_item_id, ebay_order_id, stat, source', 'safe', 'on'=>'search'),
        );
    }

    public static function findActivePackageByEbayOrderId($ebayOrderId, $user_id = NULL)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        $model = EbayImport::model()->find('ebay_order_id = :ebayOrderID AND stat != :stat AND user_id = :user_id', ['ebayOrderID' => $ebayOrderId, ':stat' => self::STAT_CANCELLED, ':user_id' => $user_id]);

        if($model  === NULL)
            return false;
        else
            return $model;
    }
}
