<?php
//
//Yii::import('application.models._base.BaseUserGroupDiscount');
//
//class UserGroupDiscount extends BaseUserGroupDiscount
//{
//    public static function model($className=__CLASS__) {
//        return parent::model($className);
//    }
//
//    public function behaviors()
//    {
//        return array_merge(
//            parent::behaviors(), array(
//                'CTimestampBehavior' => array(
//                    'class' => 'zii.behaviors.CTimestampBehavior',
//                    'createAttribute' => 'date_entered',
//                    'updateAttribute' => 'date_updated',
//                ),
//            )
//        );
//    }
//
//    public function rules() {
//        return array(
//
//
//            array('name', 'default',
//                'value'=> 'temp', 'on'=>'insert'),
//
//            array('name, user_group_id', 'required'),
//            array('discount_price_id', 'numerical'),
//            array('discount_percentage', 'numerical',  'min' => 0, 'max' => 100),
//            array('name', 'length', 'max'=>45),
//            array('user_group_id', 'length', 'max'=>10),
//            array('date_updated', 'safe'),
//            array('date_updated, discount_percentage, discount_price_id', 'default', 'setOnEmpty' => true, 'value' => null),
//            array('id, name, date_entered, date_updated, discount_percentage, discount_price_id, user_group_id', 'safe', 'on'=>'search'),
//        );
//    }
//
//    public function saveWithPrices($ownTransaction = true)
//    {
//        if($ownTransaction)
//            $transaction = Yii::app()->db->beginTransaction();
//
//        $errors = false;
//
//        $price_id = $this->discountPrice->saveWithPrices(false);
//
//        if(!$price_id)
//            $errors = true;
//
//        $this->discount_price_id = $price_id;
//
//        if(!$this->save())
//            $errors = true;
//
//
//        if($errors)
//        {
//            if($ownTransaction)
//                $transaction->rollback();
//
//            return false;
//        } else {
//            if($ownTransaction)
//                $transaction->commit();
//
//            return true;
//        }
//
//
//    }
//
//    public function getPrice()
//    {
//        $price = $this->discountPrice->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => Yii::app()->PriceManager->getCurrency())))[0]->value;
//
//        return $price;
//    }
//
//}