<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Create'),
);

?>


<div class="form">

    <h2><?php echo Yii::t('app', 'Krok {step}', array('{step}' => '1/3'));?> | <?php echo Yii::t('user','Parametry konta');?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));

    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'login'); ?>
        <?php echo $form->textField($model, 'login', array('maxlength' => 20)); ?>
        <?php echo $form->error($model,'login'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model, 'email', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'pass'); ?>
        <?php echo $form->passwordField($model, 'pass', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'pass'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'passCompare'); ?>
        <?php echo $form->passwordField($model, 'passCompare', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'passCompare'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model, 'type', CHtml::listData(
            array(
                array('value' => User::TYPE_PRIVATE, 'name' => User::getTypeName(User::TYPE_PRIVATE)),
                array('value' => User::TYPE_COMPANY, 'name' => User::getTypeName(User::TYPE_COMPANY))
            ),'value','name')); ?>
        <?php echo $form->error($model,'type'); ?>
    </div><!-- row -->

    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>

    <div class="navigate">
    <?php
    echo TbHtml::submitButton(Yii::t('app','Dalej'),array('name'=>'step2'));
    $this->endWidget();
    ?>
        </div>
</div><!-- form -->