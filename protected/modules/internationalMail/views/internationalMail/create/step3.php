<?php

/* @var $this InternationalMailController */
/* @var $model AdressData */
/* @var $form CActiveForm */
/* @var $saveUserContactBooks bool[] */
/* @var $countries CountryList[] */


?>

<div class="form">

    <h2><?php echo Yii::t('internationalMail','International Mail'); ?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '3/6')); ?> | <?php echo Yii::t('internationalMail','Dane odbiorcy'); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'address-data',
        'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.2,
        ),
        'stateful'=>true,
    ));
    ?>

    <?php

    $this->renderPartial('_addressData/_form',
        array('form' => $form,
            'model' => $model,
            'model2' => $model3,
            'saveToContactBook' => $saveToContactBook,
            'type' => $type,
            'countries' => $countries,
        ));

    ?>


    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step1', 'class' => 'btn-small'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Dalej'),array('name'=>'step4', 'class' => 'btn-primary'));
        echo ' ';
        echo $jumpToEnd? TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'step5', 'class' => 'btn-small')):'';
        $this->endWidget();
        ?>
    </div>

</div><!-- form -->