<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

?>

<h1>Update <?php echo GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
if(Yii::app()->session['forceChangePassword']):
?>
<div class="alert alert-error text-center">So... EU says you have to change password because current one is older than 30 days!</div>
<?php
endif;
?>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    if($key == 'top-info')
    {
        Yii::app()->user->setFlash('top-info', $message);
        continue;
    }
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>


<?php
/* @var $model Admin */
?>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'admin-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'login'); ?>
        <?php echo $form->textField($model, 'login', array('maxlength' => 20, 'disabled' => $model->getScenario()!='insert'?'disabled':'')); ?>
        <?php echo $form->error($model,'login'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'pass'); ?>
        <?php echo $form->passwordField($model, 'pass', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'pass'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'passCompare'); ?>
        <?php echo $form->passwordField($model, 'passCompare', array('maxlength' => 64)); ?>
        <?php echo $form->error($model,'passCompare'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model, 'email', array('maxlength' => 45, 'readonly' => 'true')); ?>
        <?php echo $form->error($model,'email'); ?>
    </div><!-- row -->

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->