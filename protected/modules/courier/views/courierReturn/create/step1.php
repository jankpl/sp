<?php
/* @var $model Courier */
/* @var $form TbActiveForm */
/* @var $additionsHtml string */
/* @var $noPickup boolean */
/* @var $noCarriage boolean */
/* @var $codCurrency false|string */
/* @var $codIgnored integer */
/* @var $deliveryOperatorBrokerId integer */
?>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {           
               loader: {
                  textTop: '.CJSON::encode(Yii::t('site', 'Trwa ładowanie...')).',
                  textBottom: '.CJSON::encode(Yii::t('site', 'Proszę czekać...')).',
                  error:  '.CJSON::encode(Yii::t('site', 'Wystąpił błąd, ale system spróbuje automatycznie przetworzyć formularz. Następnie proszę spróbować kontynuować. Jeżeli błąd będzie się powtarzał, prosimy o kontakt.')).',
              },    
          };
      ',CClientScript::POS_HEAD);



Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.bootstrap-touchspin.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courierReturnForm.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.bootstrap-touchspin.min.css');

Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
?>

<h2><?php echo Yii::t('courier', 'Create Courier Return'); ?> | <?= Yii::t('app','Krok {step}', array('{step}' => '1/2')); ?></h2>
<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-internal-form',
        'enableClientValidation' => true,
        'stateful'=>true,
//        'htmlOptions' => [
//            'class' => 'form-horizontal',
//        ],
    ));
    ?>

        <?php
    $this->widget('FlashPrinter');
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-info" id="show-desc">
                <?php
                $this->widget('ShowPageContent', array('id' => 55));
                ?>
            </div>
        </div>
    </div>

    <fieldset>
        <legend><?= Yii::t('courier', 'Parametry paczki');?> <a href="#" id="show-requirements" tabindex="-1">(?)</a></legend>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-sm-offset-4 text-center"><strong><?= Yii::t('courier', 'Waga');?> [<?php echo Courier::getWeightUnit();?>]</strong></div>

                    <div class="col-xs-12 col-sm-4 col-sm-offset-4 text-center"><?php echo $form->textField($model, 'package_weight', array('maxlength' => 7, 'style' => 'text-align: center;', 'data-weight' => true, 'data-package-weight' => true, 'data-dependency' => '10', 'data-max-weight' => CourierTypeU::MAX_PACKAGE_WEIGHT)); ?></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-right">
                <?php echo $form->error($model,'package_weight'); ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-12 text-center"><strong><?= Yii::t('courier', 'Wymiary');?> [<?php echo Courier::getDimensionUnit();?>]</strong></div>
            <div class="col-xs-12 col-sm-2 col-sm-offset-3 text-center">
                <?php echo $form->textField($model, 'package_size_l', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => 'l', 'data-dependency' => '10')); ?><br/><?= Yii::t('courier', 'długość');?>
            </div>
            <div class="col-xs-12 col-sm-2 text-center"><?php echo $form->textField($model, 'package_size_w', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => 'w', 'data-dependency' => '10')); ?><br/><?= Yii::t('courier', 'szerokość');?></div>
            <div class="col-xs-12 col-sm-2 text-center"><?php echo $form->textField($model, 'package_size_d', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => 'd', 'data-dependency' => '10')); ?><br/><?= Yii::t('courier', 'wysokość');?></div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3 ">
                <?php echo $form->error($model,'package_size_l'); ?>
                <?php echo $form->error($model,'package_size_w'); ?>
                <?php echo $form->error($model,'package_size_d'); ?>
            </div>
        </div>

    </fieldset>

    <div class="row">
        <?php
        $this->Widget('AddressDataSwitch', ['attrBaseName' => 'CourierTypeReturn_AddressData']);
        ?>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('courier', 'Zwrot od');?></legend>
                <?php
                $this->renderPartial('_addressData/_form',
                    array(
                        'model' => $model->senderAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_SENDER),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => UserContactBook::TYPE_SENDER,
                        'type' => UserContactBook::TYPE_SENDER,
                        'countries' => $countries,
//                    'focusFirst' => true,
                    ));
                ?>

                <div class="row" style="margin-top: 10px;">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'_sendLabelOnMail'); ?>
                        <?php echo $form->checkBox($model,'_sendLabelOnMail', ['data-bootstrap-checkbox' => true, 'data-default-class' => 'btn-sm', 'data-on-class' => 'btn-sm btn-primary', 'data-off-class' => 'btn-sm btn-default', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak')]); ?>
                        <?php echo $form->error($model, '_sendLabelOnMail'); ?>
                    </div>
                </div><!-- row -->
            </fieldset>
        </div>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('courier', 'Zwrot do');?></legend>
                <div class="alert alert-info" style="<?= $deliveryOperatorBrokerId != CourierOperator::BROKER_DHLDE ? 'display: none;' : '';?>" data-delivery-broker-id-show="<?= CourierOperator::BROKER_DHLDE;?>">
                    <?php $this->renderPartial('/_common/_dhlde_packstation');?>
                </div>
                <?php
                $this->renderPartial('_addressData/_form',
                    array(
                        'model' => $model->receiverAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_RECEIVER),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => UserContactBook::TYPE_RECEIVER,
                        'type' => UserContactBook::TYPE_RECEIVER,
                        'countries' => $countries,
//                    'focusFirst' => true,
                    ));
                ?>
            </fieldset>


        </div>
    </div>

    <div class="row"  data-continue="true">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Package content');?></legend>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'package_content'); ?>
                        <?php echo $form->textField($model, 'package_content'); ?>
                        <?php echo $form->error($model,'package_content'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'package_value'); ?>
                        <?php echo $form->textField($model, 'package_value', ['style' => 'border-top-right-radius: 0; border-bottom-right-radius: 0;']); ?> <?php echo $form->dropDownList($model, 'value_currency', Courier::getCurrencyList());  ?>
                        <?php echo $form->error($model,'package_value'); ?>
                    </div>
                </div>
                <?php
                if(!Yii::app()->user->isGuest && Yii::app()->user->getModel()->getCourierRefPrefix()):
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->labelEx($model,'ref'); ?>
                            <?php echo $form->textField($model, 'ref'); ?>
                            <?php echo $form->error($model,'ref'); ?>
                        </div>
                    </div>
                <?php
                endif;
                ?>
            </fieldset>
        </div>
    </div>

    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>

    <fieldset><legend><?= Yii::t('courier','Kontynuuj');?></legend>


        <div style="text-align: center;">
            <?php echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('name' => 'summary', 'class' => 'btn-lg btn-primary','data-continue-button' => 'true')); ?>

            <?= $form->hiddenField($model, 'source'); // for cloning ?>
            <?php $this->endWidget();
            ?>
        </div>
    </fieldset>

</div>