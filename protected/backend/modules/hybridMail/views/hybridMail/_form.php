<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'hybrid-mail-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'pages'); ?>
		<?php echo $form->textField($model, 'pages'); ?>
		<?php echo $form->error($model,'pages'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_country_id'); ?>
		<?php echo $form->dropDownList($model, 'sender_country_id', GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'sender_country_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_name'); ?>
		<?php echo $form->textField($model, 'sender_name', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_country'); ?>
		<?php echo $form->textField($model, 'sender_country', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_country'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_zip_code'); ?>
		<?php echo $form->textField($model, 'sender_zip_code', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_zip_code'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_city'); ?>
		<?php echo $form->textField($model, 'sender_city', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_city'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_address_line_1'); ?>
		<?php echo $form->textField($model, 'sender_address_line_1', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_address_line_1'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_address_line_2'); ?>
		<?php echo $form->textField($model, 'sender_address_line_2', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_address_line_2'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sender_tel'); ?>
		<?php echo $form->textField($model, 'sender_tel', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'sender_tel'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textField($model, 'text', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'text'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'order_id'); ?>
		<?php echo $form->dropDownList($model, 'order_id', GxHtml::listDataEx(Order::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'order_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'stat'); ?>
		<?php echo $form->dropDownList($model, 'stat', GxHtml::listDataEx(HybridMailStat::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'stat'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'local_id'); ?>
		<?php echo $form->textField($model, 'local_id', array('maxlength' => 12)); ?>
		<?php echo $form->error($model,'local_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'file_hash'); ?>
		<?php echo $form->textField($model, 'file_hash', array('maxlength' => 64)); ?>
		<?php echo $form->error($model,'file_hash'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'hash'); ?>
		<?php echo $form->textField($model, 'hash', array('maxlength' => 64)); ?>
		<?php echo $form->error($model,'hash'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->