<div class="form">
    <?php echo $form->hiddenField($model, '['.$i.']id'); ?>
    <?php echo $form->hiddenField($model, '['.$i.']sp_points_id'); ?>
    <?php echo $form->hiddenField($model, '['.$i.']language_id'); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']title'); ?>
        <?php echo $form->textField($model, '['.$i.']title', array('maxlength' => 256)); ?>
        <?php echo $form->error($model,'['.$i.']title'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'['.$i.']text'); ?>
        <?php echo $form->textArea($model, '['.$i.']text'); ?>
        <?php echo $form->error($model,'['.$i.']text'); ?>
    </div><!-- row -->
  </div><!-- form -->