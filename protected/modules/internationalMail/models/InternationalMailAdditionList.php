<?php

Yii::import('application.modules.internationalMail.models._base.BaseInternationalMailAdditionList');

class InternationalMailAdditionList extends BaseInternationalMailAdditionList
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),

            array('price_id', 'numerical', 'integerOnly'=>true),

            array('name, price', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>45),
            array('date_updated', 'safe'),
            array('date_updated, stat', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, description, price, stat', 'safe', 'on'=>'search'),
        );
    }

    public function getClientTitle()
    {
        return $this->internationalMailAdditionListTr->title;
    }

    public function getClientDesc()
    {
        return $this->internationalMailAdditionListTr->description;
    }

    public function toggleStat()
    {
        $this->stat = abs($this->stat - 1);

        return $this->save(false,array('stat'));
    }

    public function saveWithTrAndPrice($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;


        $price_id = $this->price->saveWithPrices();

        if(!$price_id)
            $errors = true;

        $this->price_id = $price_id;

        if(!$this->save())
            $errors = true;

        foreach($this->internationalMailAdditionListTrs AS $item)
        {

            $item->international_mail_addition_list_id = $this->id;
            if(!$item->save()) {
                $errors = true;
            }
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

    public function getPrice()
    {

        $price = $this->price->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => Yii::app()->PriceManager->getCurrency())))[0]->value;

        return $price;


    }
}