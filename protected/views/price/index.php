<h2><?php echo Yii::t('price','Cennik usług krajowych');?></h2>

<?php
$this->Widget('ShowPageContent', array('id' => 23));
?>

<?php
// depreciated
if(0):
    ?>
    <ul id="price-list">
        <li class="local"><?php echo CHtml::link(Yii::t('price','Przesyłki pocztowe - lokalny operator'),array('price/calc/1'), array('class' => '')); ?></li>

        <li class="alternative"><?php echo CHtml::link(Yii::t('price','Przesyłki pocztowe - alternatywni operatorzy'),array('price/calc/2'), array('class' => '')); ?></li>

        <li class="courier"><?php echo CHtml::link(Yii::t('price','Przesyłki kurierskie'),array('price/calc/3'), array('class' => '')); ?></li>

        <li class="hm"><?php echo CHtml::link(Yii::t('price','HybridMail'),array('price/table/hm'), array('class' => '')); ?></li>

        <li class="palette"><?php echo CHtml::link(Yii::t('price','Transport palet'),array('price/table/tp'), array('class' => '')); ?></li>
    </ul>
    <?php
endif;
?>