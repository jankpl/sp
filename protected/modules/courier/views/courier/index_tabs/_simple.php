<?php if(Yii::app()->user->getModel()->isPremium):?>

    <div class="alert alert-info"><?php $this->widget('ShowPageContent', ['id' => 25]);?></div>

    <?php echo CHtml::link(Yii::t('courier','Utwórz nową przesyłkę Simple'),array('/courier/courierSimple/create'), array('class' => 'btn btn-primary')); ?>
    <br/>
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'export-to-file-simple',
        'action' => Yii::app()->createUrl('/courier/courier/exportToFileAll', ['mode' => Controller::EXPORT_MODE_INTERNAL_SIMPLE]),
        'htmlOptions' => [
            'class' => 'do-not-block',
        ]
    ));
    ?>
    <hr/>
    <?php
    echo TbHtml::submitButton(Yii::t('courier','Eksportuj wszyskie przesyłki do .xls').' :', ['style' => 'margin-top: -6px; margin-right: 5px;']);
    echo Chtml::dropDownList('period', '', S_Useful::listPeriods(), ['style' => 'height: 30px;']);
    $this->endWidget();
    ?>
<?php endif; ?>