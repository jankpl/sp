<?php
/* @var $form GxActiveForm */
/* @var $model CourierCodAvailability */
?>

    <div class="form">

        <?php
        foreach(Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
        }
        ?>

        <?php $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'courier-country-group-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));
        ?>

        <p class="note">
            <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
        </p>

            <div class="row">
                <?php echo $form->labelEx($model,'currency'); ?>
                <?php echo $form->dropDownList($model, 'currency', CourierCodAvailability::getCurrencyList(), ['prompt' => '-']);?>
                <?php echo $form->error($model,'currency'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'country_list_id'); ?>
                <?php echo $form->dropDownList($model, 'country_list_id', $model->getAvailableCountryList(), ['prompt' => '-']);?>
                <?php echo $form->error($model,'country_list_id'); ?>
            </div>


        <h3>Internal operators:</h3>

        <div class="row">
            <?php echo CHtml::label('Internal operators:', 'internal-operators'); ?>
            <?php echo CHtml::dropDownList('operators-internal', CHtml::listData($model->courierCodAvailabilityOperators_Internal, 'operator_id', 'operator_id'), CHtml::listData(CourierOperator::listOperators(), 'id', 'nameWithIdAndBroker'), ['multiple' => true, 'size' => 15, 'id' => 'internal-operators', 'data-drag-select' => 'true']);?>
        </div>

    <br/>
        <div class="alert alert-error">
            Not all operators support COD!<br/>
            Not all operators supporting COD supports all currencies!
        </div>

        <div class="text-center">
            <?php
            echo GxHtml::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-large']);
            ?>
        </div>
        <?php
        $this->endWidget();
        ?>


    </div><!-- form -->

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/dragSelect/js/jquery.multi-select.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/dragSelect/css/multi-select.css');
$script = "
$('[data-drag-select]').multiSelect({
selectableHeader: '<div class=\"drag-select-header drag-select-header-available\">Available:</div>',
  selectionHeader: '<div class=\"drag-select-header drag-select-header-selected\">Selected:</div>',
  })
";
Yii::app()->clientScript->registerScript('drag-select', $script, CClientScript::POS_READY);
$css = '
.ms-container{ 
  width: 100%;
}
.ms-container .ms-list{
height: 350px;
}

.drag-select-header
{
background: lightgray;
font-weight: bold;
padding: 2px;
text-indent: 2px;
}

.drag-select-header-available
{
}

.drag-select-header-selected
{
background: lightblue;
}

.ms-selection ul
{
font-weight: bold;
}
';
Yii::app()->clientScript->registerCss('drag-select', $css);
