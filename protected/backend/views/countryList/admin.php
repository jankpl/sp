<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

    <p>
        You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
    </p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'country-list-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'name',
        'date_entered',
        array(
            'name'=>'stat',
            'value'=>'S_Status::stat($data->stat)',
            'filter'=>CHtml::listData(S_Status::listStats(),'id','name'),
        ),
        /*array(
            'name'=>'country_group_id',
            'value'=>'GxHtml::valueEx($data->countryGroup)',
            'filter'=>GxHtml::listDataEx(CountryGroup::model()->findAllAttributes(null, true)),
        ),*/
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update} {view} {delete} {status} {status_a}',
            'htmlOptions'=> array('style'=>'width:80px'),
            'buttons'=>array(
                'status'=>array(
                    'visible' => '$data->stat==1',
                    'label'=>'deactivate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_active.png',
                    'url' => 'Yii::app()->createUrl("countryList/stat",array("id" => $data->id))',
                ),
                'status_a'=>array(
                    'visible' => '$data->stat==0',
                    'label'=>'activate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_not_active.png',
                    'url' => 'Yii::app()->createUrl("countryList/stat",array("id" => $data->id))',
                ),
            ),
        ),
    ),
)); ?>

<div class="info">Deactivated country cannot be used in orders, but can be used in address books.</div>