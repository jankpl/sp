<?php

Yii::import('application.modules.courier.models._base.BaseCourierUAdditionList');

class CourierUAdditionList extends BaseCourierUAdditionList
{

    const ADDITION_NON_STANDARD_PACKAGE = 1;

    const UNIQID_UPS_ROD = 100;
    const UNIQID_UPS_SATURDAY = 101;
    const UNIQID_UPS_NOTIFICATION = 102;

    const UNIQID_GLS_ROD = 200;
    const UNIQID_GLS_DELIVERY_10 = 201;
    const UNIQID_GLS_DELIVERY_12 = 202;
    const UNIQID_GLS_DELIVERY_SATURDAY = 203;

    const UNIQID_DHL_DELIVERY_1822 = 300;
    const UNIQID_DHL_DELIVERY_SATURDAY = 301;
    const UNIQID_DHL_SEND_SATURDAY = 302;
    const UNIQID_DHL_ROD = 303;
    const UNIQID_DHL_DOMESTIC_12 = 304;
    const UNIQID_DHL_DOMESTIC_09 = 305;
    const UNIQID_DHL_INSURANCE = 306;

    const UNIQID_PP_POTW_ODB = 400;
    const UNIQID_PP_PRIO = 401;
    const UNIQID_PP_WARTOSC = 402;

    const UNIQID_DHLDE_PAKET_PRIO = 500;
    const UNIQID_DHLDE_ADDITIONAL_INSURANCE = 501;
    const UNIQID_DHLDE_RECEIVER_EMAIL_NOTIFICATIOON = 502;

    const UNIQID_DHLEXPRESS_UE_12 = 600;
    const UNIQID_DHLEXPRESS_UE_9 = 601;
    const UNIQID_DHLEXPRESS_NON_UE_12 = 602;
    const UNIQID_DHLEXPRESS_NON_UE_9 = 603;
    const UNIQID_DHLEXPRESS_USA_1030 = 604;
    const UNIQID_DHLEXPRESS_BREAK_BULK = 605;
    const UNIQID_DHLEXPRESS_XPD = 606;
    const UNIQID_DHLEXPRESS_UE_ECONOMY = 607;
    const UNIQID_DHLEXPRESS_NON_UE_ECONOMY = 608;
    const UNIQID_DHLEXPRESS_ID8000 = 609;

    const UNIQID_UKMAIL_AIR = 700;
    const UNIQID_UKMAIL_SPECIFIED_ADDR_ONLY = 701;
    const UNIQID_UKMAIL_LEAVE_SAFE_OR_SIG = 702;
    const UNIQID_UKMAIL_NEXT_DAY_9 = 703;
    const UNIQID_UKMAIL_NEXT_DAY_1030 = 704;
    const UNIQID_UKMAIL_NEXT_DAY_12 = 705;
    const UNIQID_UKMAIL_SATURDAY = 706;
    const UNIQID_UKMAIL_SATURDAY_9 = 707;
    const UNIQID_UKMAIL_SATURDAY_1030 = 708;
    const UNIQID_UKMAIL_48 = 709;
    const UNIQID_UKMAIL_48_PLUS = 710;

    const UNIQID_ROYALMAIL_RECORDED = 800;
    const UNIQID_ROYALMAIL_SIGNATURE = 801;

    const UNIQID_DPDPL_GUARANTEED_930 = 1100;
    const UNIQID_DPDPL_GUARANTEED_1200 = 1101;
    const UNIQID_DPDPL_GUARANTEED_SAT = 1102;
    const UNIQID_DPDPL_GUARANTEED_SUN = 1103;
    const UNIQID_DPDPL_CARRYIN = 1104;
    const UNIQID_DPDPL_ROD = 1105;
    const UNIQID_DPDPL_DECLARED_VALUE = 1106;


    const UNIQID_INPOST_EXPRESS_12 = 1500;
    const UNIQID_INPOST_EXPRESS_17 = 1501;
    const UNIQID_INPOST_INSURANCE_5000 = 1502;
    const UNIQID_INPOST_INSURANCE_10000 = 1503;
    const UNIQID_INPOST_INSURANCE_20000 = 1504;
    const UNIQID_INPOST_ROD = 1505;
    const UNIQID_INPOST_INSURANCE_1000 = 1506;

    const UNIQID_DPDNL_TYRES = 1600;
    const UNIQID_DPDNL_SATURDAY = 1601;
    const UNIQID_DPDNL_E10 = 1602;
    const UNIQID_DPDNL_E12 = 1603;
    const UNIQID_DPDNL_G18 = 1604;
    const UNIQID_DPDNL_INTERNATIONAL_EXPRESS = 1605;

    const UNIQID_LPEXPRESS_SATURDAY_DELIVERY = 1800;
    const UNIQID_LPEXPRESS_HAS_LOAD = 1801;
    const UNIQID_LPEXPRESS_DELIVERY_NOTIFICATION = 1802;
    const UNIQID_LPEXPRESS_INSURANCE = 1803;
    const UNIQID_LPEXPRESS_SAME_DAY_DELIVERY = 1804;
    const UNIQID_LPEXPRESS_HAS_COVER_LETTER = 1805;

    const UNIQID_RUCH_INSURANCE = 1900;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function getIdByUniqId($uniq_id)
    {
        $cmd = Yii::app()->db->cache(60*60)->createCommand();
        return $cmd->select('id')->from('courier_u_addition_list')->where('uniq_id = :uniq_id', [':uniq_id' => $uniq_id])->queryScalar();
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),

            array('price_id', 'numerical', 'integerOnly'=>true),

            array('name, price', 'required'),
            array('stat', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>45),
            array('date_updated', 'safe'),
            array('date_updated, stat, uniq_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, name, description, price, stat, broker_id, group', 'safe', 'on'=>'search'),
        );
    }

    protected static $_getOperatorsData = [];
    protected function getOperatorsData()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id');
        $cmd->from((new CourierUOperator())->tableName());
        $cmd->where('broker_id = :broker_id AND img != "" AND stat >= 0', ['broker_id' => $this->broker_id]);
        $cmd->limit(1);
        $cmd->order('id DESC');
        $result = $cmd->queryScalar();

        if ($result) {
            /* @var $model CourierUOperator */
            $model = CourierUOperator::model()->findByPk($result);
            self::$_getOperatorsData[$this->broker_id] = [];
            self::$_getOperatorsData[$this->broker_id]['bg'] = $model->getBgColor();
            self::$_getOperatorsData[$this->broker_id]['font'] = $model->getFontColor();
            self::$_getOperatorsData[$this->broker_id]['name'] = CourierUOperator::getBrokerName($model->broker_id);
        }
    }

    public function getBackgroundColor()
    {
        if($this->broker_id) {
            if (!isset(self::$_getOperatorsData[$this->broker_id]))
                $this->getOperatorsData();

            return self::$_getOperatorsData[$this->broker_id]['bg'];
        }
        return false;
    }

    public function getFontColor()
    {
        if($this->broker_id) {
            if (!isset(self::$_getOperatorsData[$this->broker_id]))
                $this->getOperatorsData();

            return self::$_getOperatorsData[$this->broker_id]['font'];
        }
        return false;
    }

    public function getOperatorName()
    {
        if($this->broker_id) {
            if (!isset(self::$_getOperatorsData[$this->broker_id]))
                $this->getOperatorsData();

            return self::$_getOperatorsData[$this->broker_id]['name'];
        }
        return false;
    }



    public function getClientTitle()
    {
        return $this->courierUAdditionListTr->title;
    }

    public function getClientDesc()
    {
        return $this->courierUAdditionListTr->description;
    }

    public function toggleStat()
    {
        $this->stat = abs($this->stat - 1);

        return $this->save(false,array('stat'));
    }

    public function saveWithTrAndPrice($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $price_id = $this->price->saveWithPrices();

        if(!$price_id)
            $errors = true;

        $this->price_id = $price_id;

        if(!$this->save())
            $errors = true;

        foreach($this->courierUAdditionListTrs AS $item)
        {

            $item->courier_u_addition_list_id = $this->id;
            if(!$item->save()) {
                $errors = true;
            }
        }

        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

    public function getPrice()
    {
        $currency = CourierTypeU::getCurrencyIdForCurrentUser();

        $CACHE_NAME = 'COURIER_ADDITION_PRICE_'.$currency.'_'.$this->id;
        $price = Yii::app()->cache->get($CACHE_NAME);

        if($price === false) {

            $price = $this->price->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => $currency)))[0]->value;
            Yii::app()->cache->set($CACHE_NAME, $price, 60);
        }

        return $price;
    }


    public static function getAdditionsByCourier(Courier $courier)
    {

        $models = self::model()->findAll('stat = '.S_Status::ACTIVE.' AND broker_id IS NULL');
        {

            $temp = self::findOperatorAdditions($courier->courierTypeU->courierUOperator->broker_id, $courier);

            if(is_array($temp))
                $models = array_merge($models, $temp);
        }

        return $models;
    }

    protected static function findOperatorAdditions($broker_id, Courier $courier)
    {
        if(in_array($broker_id, array_keys(GLOBAL_BROKERS::getBrokersList())))
        {
            $operatorClient = GLOBAL_BROKERS::globalOperatorClientFactory($broker_id);
            return $operatorClient::getAdditions($courier);
        }
        else
            switch($broker_id)
            {
                case CourierUOperator::BROKER_GLS:
                    return self::_findGlsAdditions($courier);
                    break;
                case CourierUOperator::BROKER_DHL:
                    return self::_findDhlAdditions($courier);
                    break;
                case CourierUOperator::BROKER_PETER_SK:
                    return self::_findPocztaPolskaAdditions($courier);
                    break;
                case CourierUOperator::BROKER_DHLDE:
                    return self::_findDhlDeAdditions($courier);
                    break;
                case CourierUOperator::BROKER_UKMAIL:
                    return self::_findUkMailAdditions($courier);
                    break;
                case CourierUOperator::BROKER_DHLEXPRESS:
                    return self::_findDhlExpressAdditions($courier);
                    break;
                case CourierUOperator::BROKER_UPS:
                    return self::_findUpsAdditions($courier);
                    break;
                case CourierUOperator::BROKER_DPDPL:
                    return self::_findDpdPlAdditions($courier);
                    break;
                case CourierUOperator::BROKER_INPOST:
                    return self::_findInpostAdditions($courier);
                    break;
                case CourierUOperator::BROKER_ROYALMAIL:
                    return self::_findRoyalMAilAdditions($courier);
                    break;
                case CourierUOperator::BROKER_DPDNL:
                    return self::_findDpdNlAdditions($courier);
                    break;
                case CourierUOperator::BROKER_LP_EXPRESS:
                    return self::_findLpExpressAdditions($courier);
                    break;
//                case GLOBAL_BROKERS::BROKER_SWEDEN_POST:
//                    return self::_findSwedenPostAdditions($courier);
//                    break;
                case CourierUOperator::BROKER_DPDDE:
                    return self::_findDpdDeAdditions($courier);
                    break;
//            case GLOBAL_BROKERS::BROKER_DPDIR:
//                return self::_findDpdIrAdditions($courier);
//                break;
//            case GLOBAL_BROKERS::BROKER_ANPOST:
//                return self::_findAnpostAdditions($courier);
//                break;
                case CourierUOperator::BROKER_RUCH:
                    return self::_findRuchAdditions($courier);
                    break;
                default:
                    return false;
                    break;
            }
    }


    protected static function _findGlsAdditions(Courier $courier)
    {
        $zip_code = $courier->receiverAddressData->zip_code;

        $CACHE_NAME = 'GLS_ADDITIONS_U_' . $zip_code;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {
            $additions = [];

            $services = GlsClient::getPremiumServices($zip_code);

            if ($services) {

                if ($services->{(GlsClient::OPTION_ROD)})
                    $additions[] = self::UNIQID_GLS_ROD;

                if ($services->{(GlsClient::OPTION_DEVLIVERY_TO_12)})
                    $additions[] = self::UNIQID_GLS_DELIVERY_12;

                if ($services->{(GlsClient::OPTION_DEVLIVERY_TO_10)})
                    $additions[] = self::UNIQID_GLS_DELIVERY_10;

                if ($services->{(GlsClient::OPTION_SATURDAY)})
                    $additions[] = self::UNIQID_GLS_DELIVERY_SATURDAY;

            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDhlAdditions(Courier $courier)
    {
        $CACHE_NAME = 'DHL_ADDITIONS_U_'.$courier->senderAddressData->zip_code.'_'.$courier->receiverAddressData->zip_code;


        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $servicesSender = DhlClient::getServicesForZipCode($courier->senderAddressData->zip_code, date('Y-m-d'));
            $servicesReceiver = DhlClient::getServicesForZipCode($courier->receiverAddressData->zip_code, date('Y-m-d'));

            $additions = [];

            $additions[] = self::UNIQID_DHL_ROD;
            $additions[] = self::UNIQID_DHL_INSURANCE;

            if($servicesReceiver[DhlClient::OPT_DOR_SB])
                $additions[] = self::UNIQID_DHL_DELIVERY_SATURDAY;

            if($servicesReceiver[DhlClient::OPT_DOR_1822])
                $additions[] = self::UNIQID_DHL_DELIVERY_1822;

            if($servicesSender[DhlClient::OPT_DOM09] && $servicesReceiver[DhlClient::OPT_DOM09])
                $additions[] = self::UNIQID_DHL_DOMESTIC_09;

            if($servicesSender[DhlClient::OPT_DOM12] && $servicesReceiver[DhlClient::OPT_DOM12])
                $additions[] = self::UNIQID_DHL_DOMESTIC_12;

//            $additions[] = self::DHL_CONNECT;

            if($courier->courierTypeU->collection && $servicesSender[DhlClient::OPT_NAD_SB])
                $additions[] = self::UNIQID_DHL_SEND_SATURDAY;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }


    protected static function _findPocztaPolskaAdditions(Courier $courier)
    {

        $CACHE_NAME = 'POCZTA_POLSKA_ADDITIONS_U';

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = self::UNIQID_PP_WARTOSC;
            $additions[] = self::UNIQID_PP_POTW_ODB;
//            $additions[] = self::PP_PRIO;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDhlDeAdditions(Courier $courier)
    {

        $CACHE_NAME = 'DHL_DE_ADDITIONS_U_'.$courier->senderAddressData->country_id.'_'.$courier->receiverAddressData->country_id;
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = self::UNIQID_DHLDE_ADDITIONAL_INSURANCE;
            $additions[] = self::UNIQID_DHLDE_RECEIVER_EMAIL_NOTIFICATIOON;

            if($courier->courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_DHLDE_PAKET)
                $additions[] = self::UNIQID_DHLDE_PAKET_PRIO;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findUkMailAdditions(Courier $courier)
    {
        $CACHE_NAME = 'UK_MAIL_ADDITIONS_U_'.$courier->senderAddressData->country_id.'_'.$courier->receiverAddressData->country_id;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            if (!in_array($courier->senderAddressData->country_id, [CountryList::COUNTRY_UK, CountryList::COUNTRY_IE]) OR !in_array($courier->receiverAddressData->country_id, [CountryList::COUNTRY_UK, CountryList::COUNTRY_IE]))
            {
                $additions[] = self::UNIQID_UKMAIL_AIR;
            } else {



                if($courier->courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_UKMAIL)
                {
                    $additions[] = self::UNIQID_UKMAIL_SPECIFIED_ADDR_ONLY;
                    $additions[] = self::UNIQID_UKMAIL_LEAVE_SAFE_OR_SIG;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_9;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_1030;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_12;
                    $additions[] = self::UNIQID_UKMAIL_SATURDAY;
                    $additions[] = self::UNIQID_UKMAIL_SATURDAY_9;
                    $additions[] = self::UNIQID_UKMAIL_SATURDAY_1030;
                    $additions[] = self::UNIQID_UKMAIL_48;
                    $additions[] = self::UNIQID_UKMAIL_48_PLUS;
                }
                else if($courier->courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_UKMAIL_RETURN OR $courier->courierTypeU->courierUOperator->uniq_id == CourierUOperator::UNIQID_UKMAIL_3RD)
                {
                    $additions[] = self::UNIQID_UKMAIL_SPECIFIED_ADDR_ONLY;
                    $additions[] = self::UNIQID_UKMAIL_LEAVE_SAFE_OR_SIG;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_9;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_1030;
                    $additions[] = self::UNIQID_UKMAIL_NEXT_DAY_12;
                    $additions[] = self::UNIQID_UKMAIL_SATURDAY;
                }
            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDhlExpressAdditions(Courier $courier)
    {
        $CACHE_NAME = 'DHL_EXPRESS_ADDITIONS_U_'.$courier->senderAddressData->country_id.'_'.$courier->receiverAddressData->country_id.'_'.Yii::app()->user->id;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

//            if( in_array($courier->receiverAddressData->country_id, CountryList::getUeList(true)))
//            {
            $additions[] = self::UNIQID_DHLEXPRESS_UE_12;
            $additions[] = self::UNIQID_DHLEXPRESS_UE_9;
            $additions[] = self::UNIQID_DHLEXPRESS_UE_ECONOMY;

//            }
//            else if($courier->receiverAddressData->country_id == CountryList::COUNTRY_US)
//            {
            $additions[] = self::UNIQID_DHLEXPRESS_USA_1030;
//            } else {
            $additions[] = self::UNIQID_DHLEXPRESS_NON_UE_12;
            $additions[] = self::UNIQID_DHLEXPRESS_NON_UE_9;
            $additions[] = self::UNIQID_DHLEXPRESS_NON_UE_ECONOMY;
//            }

            $additions[] = self::UNIQID_DHLEXPRESS_XPD;
            $additions[] = self::UNIQID_DHLEXPRESS_BREAK_BULK;


            $operator_uniq_id = $courier->courierTypeU->courierUOperator->uniq_id;

            $servicesChecker = DhlExpressClient::getRateRequestCourierTypeU($courier->courierTypeU, $courier->senderAddressData, $courier->receiverAddressData, $operator_uniq_id, $courier->courierTypeU->collection);
            $additions = array_intersect($additions, $servicesChecker);

            // REST OF ADDITONS:



            if($operator_uniq_id == CourierUOperator::UNIQID_DHLEXPRESS_FB)
                $additions[] = self::UNIQID_DHLEXPRESS_ID8000;


            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findUpsAdditions(Courier $courier)
    {

        $CACHE_NAME = 'UPS_ADDITIONS_U';

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];
            $additions[] = self::UNIQID_UPS_ROD;
            $additions[] = self::UNIQID_UPS_SATURDAY;
            $additions[] = self::UNIQID_UPS_NOTIFICATION;

//            if(UpsSoapShipClient::isCourierOperatorUpsSaver($deliveryOperator_id))
//                $additions[] = self::UNIQID_UPS_SATURDAY_DELIVERY;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDpdPlAdditions(Courier $courier)
    {

        $CACHE_NAME = 'DPDPL_ADDITIONS_U';

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];
            $additions[] = self::UNIQID_DPDPL_ROD;
            $additions[] = self::UNIQID_DPDPL_GUARANTEED_SUN;
            $additions[] = self::UNIQID_DPDPL_GUARANTEED_SAT;
            $additions[] = self::UNIQID_DPDPL_GUARANTEED_1200;
            $additions[] = self::UNIQID_DPDPL_GUARANTEED_930;
            $additions[] = self::UNIQID_DPDPL_CARRYIN;
            $additions[] = self::UNIQID_DPDPL_DECLARED_VALUE;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDpdNlAdditions(Courier $courier)
    {

        $CACHE_NAME = 'DPDNL_ADDITIONS_U_'.$courier->senderAddressData->country_id.'_'.$courier->receiverAddressData->country_id;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            if($courier->senderAddressData->country_id == $courier->receiverAddressData->country_id) {
                $additions[] = self::UNIQID_DPDNL_TYRES;
                $additions[] = self::UNIQID_DPDNL_SATURDAY;
                $additions[] = self::UNIQID_DPDNL_E10;
                $additions[] = self::UNIQID_DPDNL_E12;
                $additions[] = self::UNIQID_DPDNL_G18;
            } else
                $additions[] = self::UNIQID_DPDNL_INTERNATIONAL_EXPRESS;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findLpExpressAdditions(Courier $courier)
    {

        $isBefore1pm = (date('H') < 13);
        $CACHE_NAME = 'LPEXPRESS_ADDITIONS_U_'.$courier->receiverAddressData->country_id.'_'.$courier->getWeight(true).'_'.$isBefore1pm;
        $models = Yii::app()->cache->get($CACHE_NAME);



        if ($models === false) {

            $additions = [];

            $additions[] = self::UNIQID_LPEXPRESS_INSURANCE;

            if($courier->receiverAddressData->country_id == CountryList::COUNTRY_LT) {
                $additions[] = self::UNIQID_LPEXPRESS_HAS_COVER_LETTER;
                $additions[] = self::UNIQID_LPEXPRESS_SATURDAY_DELIVERY;
                $additions[] = self::UNIQID_LPEXPRESS_DELIVERY_NOTIFICATION;

                if($courier->getWeight(true) > 30)
                    $additions[] = self::UNIQID_LPEXPRESS_HAS_LOAD;

                if($isBefore1pm)
                    $additions[] = self::UNIQID_LPEXPRESS_SAME_DAY_DELIVERY;

            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

//    protected static function _findSwedenPostAdditions(Courier $courier)
//    {
//        $CACHE_NAME = 'SWEDEN_POST_ADDITIONS_U';
//        $models = Yii::app()->cache->get($CACHE_NAME);
//
//        if ($models === false) {
//
//            $additions = [];
//
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_SWEDEN_POST_LITHIUM;
//
//            $array = implode(',', $additions);
//
//            if(!S_Useful::sizeof($additions))
//                $models = [];
//            else
//                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');
//
//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }
//
//        return $models;
//    }


    protected static function _findRoyalMailAdditions(Courier $courier)
    {



        $CACHE_NAME = 'ROYAL_MAIL_ADDITIONS_U';

        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND broker_id = '.CourierOperator::BROKER_ROYAL_MAIL);

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findInpostAdditions(Courier $courier)
    {

//        $CACHE_NAME = 'INPOST_ADDITIONS_U';

//        $models = Yii::app()->cache->get($CACHE_NAME);

//        if ($models === false) {

        $additions = [];

        $services = Inpost::getServicesForCourierTypeU($courier->courierTypeU);

        if(isset($services[Inpost::SERVICE_EX_17]))
            $additions[] = self::UNIQID_INPOST_EXPRESS_17;

        if(isset($services[Inpost::SERVICE_EX_12]))
            $additions[] = self::UNIQID_INPOST_EXPRESS_12;

        if($courier->cod_value < 5000) {
            $additions[] = self::UNIQID_INPOST_INSURANCE_1000;
            $additions[] = self::UNIQID_INPOST_INSURANCE_5000;
        }

        $additions[] = self::UNIQID_INPOST_INSURANCE_10000;

        $additions[] = self::UNIQID_INPOST_INSURANCE_20000;

        $additions[] = self::UNIQID_INPOST_ROD;


        $array = implode(',', $additions);

        if(!S_Useful::sizeof($additions))
            $models = [];
        else
            $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }

        return $models;
    }

    protected static function _findRuchAdditions(Courier $courier)
    {
        $CACHE_NAME = 'RUCH_ADDITIONS_U';
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = self::UNIQID_RUCH_INSURANCE;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    protected static function _findDpdDeAdditions(Courier $courier)
    {

        $receiver_country_id = $courier->receiverAddressData->country_id;

        $CACHE_NAME = 'DPDDE_ADDITIONS_U_'.$receiver_country_id;
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            if($receiver_country_id == CountryList::COUNTRY_DE) {
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_830;
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_10;
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_12;
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_18;
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_12_SAT;
            } else {
                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDDE_EXPRESS_INT;
            }

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

//    protected static function _findDpdIrAdditions(Courier $courier)
//    {
//
//        $is_receiver_ie = $courier->receiverAddressData->country_id == CountryList::COUNTRY_IE ? 1 : 0;
//
//        $CACHE_NAME = 'DPDIR_ADDITIONS_U_'.$is_receiver_ie;
//        $models = Yii::app()->cache->get($CACHE_NAME);
//
//        if ($models === false) {
//
//            $additions = [];
//
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_EMAIL_NOTIFY;
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SMS_NOTIFY;
//
//            if($is_receiver_ie)
//                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_SATURDAY_DELIVERY;
//            else
//                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDIR_OVERNIGHT_INT_SERVICE;
//
//            $array = implode(',', $additions);
//
//            if(!S_Useful::sizeof($additions))
//                $models = [];
//            else
//                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');
//
//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }
//
//        return $models;
//    }

//    protected static function _findAnpostAdditions(Courier $courier)
//    {
//
//        $is_receiver_ie = $courier->receiverAddressData->country_id == CountryList::COUNTRY_IE ? 1 : 0;
//
//        $CACHE_NAME = 'DPDIR_ANPOST_U_'.$is_receiver_ie;
//        $models = Yii::app()->cache->get($CACHE_NAME);
//
//        if ($models === false) {
//
//            $additions = [];
//
//            $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_NOTIFY_SMS;
//            $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_NOTIFY_EMAIL;
//
//            if($is_receiver_ie)
//                $additions[] = GLOBAL_BROKERS::ADDITON_UNIQID_ANPOST_SATURDAY_PREMIUM;
//
//            $array = implode(',', $additions);
//
//            if(!S_Useful::sizeof($additions))
//                $models = [];
//            else
//                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');
//
//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }
//
//        return $models;
//    }
//
//    protected static function _findWhistlAdditions(Courier $courier)
//    {
//
//        $CACHE_NAME = 'DPDIR_WHISTL_U';
//        $models = Yii::app()->cache->get($CACHE_NAME);
//
//        if ($models === false) {
//
//            $additions = [];
//
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_POD;
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_AEROSOL;
//            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_FRAGILE;
//
//            $array = implode(',', $additions);
//
//            if(!S_Useful::sizeof($additions))
//                $models = [];
//            else
//                $models = self::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');
//
//            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
//        }
//
//        return $models;
//    }

}