<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Create'),
);

$this->menu = array(
    array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('index')),
    array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h1>

<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'palette-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <fieldset><legend><?php echo Yii::t('palette','Typ palety');?></legend>

        <div class="row">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->dropDownList($model, 'type', Palette::types()); ?>
            <?php echo $form->error($model,'type'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model,'height'); ?>
            <?php echo $form->textField($model, 'height'); ?> <?php echo Palette::HEIGHT_UNIT;?>
            <?php echo $form->error($model,'height'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model,'customer_notes'); ?>
            <?php echo $form->textField($model, 'customer_notes'); ?>
            <?php echo $form->error($model,'customer_notes'); ?>
        </div><!-- row -->

    </fieldset>

    <div class="row">
        <div class="col-xs-12 col-md-6" id="sender-data">

            <fieldset><legend><?php echo Yii::t('palette','Nadawca');?></legend>

                <div class="form-part-body" class="errorNextLine" id="sender-data">

                    <?php

                    $this->renderPartial('//_addressData/_form',
                        array('form' => $form,
                            'type' => 'sender',
                            'model' => $model->senderAddressData,
                            'saveToContactBook' => $saveToContactBookCustomer,
                            'i' => 'sender-data',
                            'id' => 'sender-data',
                        ));
                    ?>
                </div>

            </fieldset>

        </div>
        <div class="col-xs-12 col-md-6" id="receiver-data">

            <fieldset><legend><?php echo Yii::t('palette','Odbiorca');?></legend>

            <div class="form-part-body" class="errorNextLine" id="receiver-data">

                <?php

                $this->renderPartial('//_addressData/_form',
                    array('form' => $form,
                        'type' => 'receiver',
                        'model' => $model->receiverAddressData,
                        'saveToContactBook' => $saveToContactBookDelivery,
                        'i' => 'receiver-data',
                        'id' => 'receiver-data',
                    ));
                ?>
            </div>

            </fieldset>

        </div>
    </div>

    <fieldset>
        <legend><?php echo Yii::t('site', 'Regulamin');?></legend>

        <div class="row">

            <?php echo $form->labelEx($model,'regulations'); ?>
            <?php echo $form->checkbox($model, 'regulations'); ?>
            <?php echo $form->error($model,'regulations'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'regulations_rodo'); ?>
            <?php echo $form->checkbox($model, 'regulations_rodo'); ?>
            <?php echo $form->error($model,'regulations_rodo'); ?>
        </div>
    </fieldset>


    <fieldset><legend><?php echo Yii::t('palette','Kontynuuj');?></legend>

    <div class="text-center">
        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary']);
        $this->endWidget();
        ?>
    </div>

    </fieldset>

</div><!-- form -->