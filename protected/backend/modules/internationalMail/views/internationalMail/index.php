<?php

$this->breadcrumbs = array(
	InternationalMail::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
    array('label'=>'Manage' . InternationalMail::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(InternationalMail::label(2)); ?></h1>

<?php echo CHtml::link('manage',array('/internationalMail/internationalMail/admin'), array('class' => 'likeButton')); ?>

<?php echo CHtml::link('scanner',array('/internationalMail/internationalMail/fastAssignCarrier'), array('class' => 'likeButton')); ?>

<?php echo CHtml::link('file',array('/internationalMail/internationalMail/assignCarrierFromFile'), array('class' => 'likeButton')); ?>