<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'order-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'selectableRows'=>2,
    'columns' => array(
        array(
            'name' => 'id',
            'headerHtmlOptions' => array('style' => 'width: 30px;'),
        ),
        array(
            'name' => 'date_entered',
            'value' => 'substr($data->date_entered,0,-3)',
            'headerHtmlOptions' => array('style' => 'width: 100px;'),
        ),
        array(
            'name'=>'stat',
            'value'=>'$data->orderStat->name',
            'filter'=>GxHtml::listDataEx(OrderStat::model()->findAllAttributes(null, true)),
//            'headerHtmlOptions' => array('style' => 'width: 30px;'),
        ),
//        array(
//            'name' => '__product',
//            'header'=>'Product type',
//            'value'=>'$data->product_type_id==1?"Kurier":"HybridMail"',
//            'filter' => CHtml::listData(Order::listProducts(),"id","name"),
//        ),

        array(
            'header'=>'Netto',
            'name' => 'value_netto',
            'value' => 'S_Price::formatPrice($data->value_netto)',
            'headerHtmlOptions' => array('style' => 'width: 50px;'),
        ),
        array(
            'header'=>'Brutto',
            'name' => 'value_brutto',
            'value' => 'S_Price::formatPrice($data->value_brutto)',
            'headerHtmlOptions' => array('style' => 'width: 50px;'),
        ),
        array(
            'header'=>'Currency',
            'name' => 'currency',
            'filter' => GxHtml::listData(PriceCurrency::model()->findAll(),'id','short_name'),
            'headerHtmlOptions' => array('style' => 'width: 60px;'),
        ),
//        array(
//            'header'=>'Paid',
//            'name' => 'paid',
//            'value' => '$data->paid?"yes":"no"',
//            'filter' => (Array(0 => 'no', 1 => 'yes')),
//            'headerHtmlOptions' => array('style' => 'width: 60px;'),
//        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
            'headerHtmlOptions' => array('style' => 'width: 30px;'),
        ),
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'selected_rows'
        ),
    ),
)); ?>
