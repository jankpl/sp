<?php

class PDFData extends CComponent
{
	protected $pages = 0;
	protected $isA4 = false;

	public function init()
	{

	}

	public function loadFile($path)
	{
		$cmd = "pdfinfo";          // Linux
		if(PHP_OS == 'WINNT') $cmd = "C:\\xpdfbin-win-3.04\\bin64\\pdfinfo.exe"; // Windows


		try {
			$filePath = realpath($path);
			// Parse entire output
			exec("$cmd $filePath", $output);



			// Iterate through lines
			$pagecount = 0;
			$a4 = false;
			foreach ($output as $op) {
				// Extract the number
				if (preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1) {
					$pagecount = intval($matches[1]);
				}

				if (substr($op, 0, 10) == 'Page size:') {
					if (strpos($op, 'A4') !== false)
						$a4 = true;
					break;
				}
			}




			$this->pages = $pagecount;
			$this->isA4 = $a4;
		} catch (Exception $ex)
		{
			return false;
		}
	}

	public function getPagesNumer()
	{
		return $this->pages;
	}

	public function getIsA4()
	{
		return $this->isA4;
	}


}