<?php

class UpsApLocation
{

//Configuration
    protected $access = GLOBAL_CONFIG::UPS_AP_ACCESS;
    protected $userid = GLOBAL_CONFIG::UPS_AP_USERID;
    protected $passwd = GLOBAL_CONFIG::UPS_AP_PASS;

    protected $endpointurl = GLOBAL_CONFIG::UPS_AP_ENCPOINT;

    protected $schemaAccessRequest = "";
    protected $schemaRequest = "";
    protected $schemaResponse = "";
    protected $operation = "ProcessShipment";
    protected $outputFileName = "XOLTResult.xml";


    protected $track_id;

    public function __construct()
    {
        $this->schemaAccessRequest = YiiBase::getPathOfAlias('application.components.ups-locator').'/AccessRequest.xsd';
        $this->schemaRequest = YiiBase::getPathOfAlias('application.components.ups-locator').'/LocatorRequest.xsd';
        $this->accessRequest = YiiBase::getPathOfAlias('application.components.ups-locator').'/LocatorResponse.xsd';
    }

    public static function getPoints($address, $city, $zipCode, $countryName)
    {

        $CACHE_NAME = 'UPS_GET_POINTS_' . $address . '_' . $city . '_' .  $zipCode . '_' . $countryName;

        $countryCode = CountryList::findCodeByText($countryName);

        $data = Yii::app()->cache->get($CACHE_NAME);

        if ($data === false) {
            $model = new self;

            try {
                $access = '<AccessRequest><AccessLicenseNumber>' . $model->access . '</AccessLicenseNumber>
<UserId>' . $model->userid . '</UserId>
<Password>' . $model->passwd . '</Password>
</AccessRequest>';

                $accessXml = simplexml_load_string($access);

                $request = '<LocatorRequest>
  <Request>
    <RequestAction>Locator</RequestAction>
    <RequestOption>64</RequestOption>
      </Request>
      <OriginAddress >
        <MaximumListSize>50</MaximumListSize>
        <PhoneNumber>
    		<StructuredPhoneNumber>
    		</StructuredPhoneNumber>
    	</PhoneNumber>
        <AddressKeyFormat>
          	<AddressLine>' . $address . '</AddressLine>
          	<PoliticalDivision2>' . $city . '</PoliticalDivision2>
          	<PostcodePrimaryLow>' . $zipCode . '</PostcodePrimaryLow>
          	<CountryCode>' . $countryCode . '</CountryCode>
        </AddressKeyFormat>
    </OriginAddress>
      <Translate>
        <LanguageCode>PL</LanguageCode>
      </Translate>
      <LocationSearchCriteria><MaximumListSize>50</MaximumListSize></LocationSearchCriteria>
      <SortCriteria><SortType>01</SortType></SortCriteria>
 </LocatorRequest>';

                $requestXml = simplexml_load_string($request);

                //create Post request
                $form = array
                (
                    'http' => array
                    (
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $accessXml->asXML() . $requestXml->asXML()
                    )
                );

                $request = stream_context_create($form);
                $browser = @fopen($model->endpointurl, 'rb', false, $request);
                if (!$browser) {
//                    throw new Exception("Connection failed.");
                    $data = false;
                } else {

                    //get response
                    $response = stream_get_contents($browser);
                    fclose($browser);

                    if ($response == false) {
                        return false;
                    } else {
                        $response = simplexml_load_string($response);

                        if (((string)$response->Response->ResponseStatusDescription) != 'Success')
                            return false;

                        $data = [];
                        foreach($response->SearchResults->children() AS $location) {

                                $data[] = [
                                    'name' => (string)$location->AddressKeyFormat->ConsigneeName,
                                    'address' => (string)$location->AddressKeyFormat->AddressLine,
                                    'city' => (string)$location->AddressKeyFormat->PoliticalDivision2,
                                    'zip' => (string)$location->AddressKeyFormat->PostcodePrimaryLow,
                                    'country_code' => (string)$location->AddressKeyFormat->CountryCode,
                                    'tel' => (string)$location->PhoneNumber,
                                    'desc' => (string)$location->SpecialInstructions->Segment,
                                    'lat' => (string)$location->Geocode->Latitude,
                                    'lng' => (string)$location->Geocode->Longitude,
                                    'id' => (string)$location->LocationID,
                                ];

                        }

//                        if(!$data)
//                            return false;
//
//                        $name = $data['name'];
//                        $name = explode(' - ', $name);
//                        if (!isset($name[1]) OR $name[1] == '')
//                            $data['name'] = $name[0];
//
//                        if (strlen($data['desc']) < 2)
//                            $data['desc'] = '';
                    }
                }

            } catch (Exception $ex) {
                $data = false;
            }

            Yii::app()->cache->set($CACHE_NAME, $data, 60*60*24);
        }


        return $data;
    }






    public static function locate($address, $city, $zipCode, $countryCode)
    {

        $CACHE_NAME = 'UPS_AP_LOCATION_' . $address . '_' . $city . '_' .  $zipCode . '_' . $countryCode;

        $data = Yii::app()->cache->get($CACHE_NAME);

        if ($data === false) {
            $model = new self;

            try {
                $access = '<AccessRequest><AccessLicenseNumber>' . $model->access . '</AccessLicenseNumber>
<UserId>' . $model->userid . '</UserId>
<Password>' . $model->passwd . '</Password>
</AccessRequest>';

                $accessXml = simplexml_load_string($access);

                $request = '<LocatorRequest>
  <Request>
    <RequestAction>Locator</RequestAction>
    <RequestOption>64</RequestOption>
      </Request>
      <OriginAddress >
        <MaximumListSize>1</MaximumListSize>
        <PhoneNumber>
    		<StructuredPhoneNumber>
    		</StructuredPhoneNumber>
    	</PhoneNumber>
        <AddressKeyFormat>
          	<AddressLine>' . $address . '</AddressLine>
          	<PoliticalDivision2>' . $city . '</PoliticalDivision2>
          	<PostcodePrimaryLow>' . $zipCode . '</PostcodePrimaryLow>
          	<CountryCode>' . $countryCode . '</CountryCode>
        </AddressKeyFormat>
    </OriginAddress>
      <Translate>
        <LanguageCode>PL</LanguageCode>
      </Translate>
      <LocationSearchCriteria><MaximumListSize>1</MaximumListSize></LocationSearchCriteria>
      <SortCriteria><SortType>01</SortType></SortCriteria>
 </LocatorRequest>';

                $requestXml = simplexml_load_string($request);

                //create Post request
                $form = array
                (
                    'http' => array
                    (
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $accessXml->asXML() . $requestXml->asXML()
                    )
                );

                $request = stream_context_create($form);
                $browser = @fopen($model->endpointurl, 'rb', false, $request);
                if (!$browser) {
//                    throw new Exception("Connection failed.");
                    $data = false;
                } else {

                    //get response
                    $response = stream_get_contents($browser);
                    fclose($browser);

                    if ($response == false) {
                        return false;
                    } else {
                        $response = simplexml_load_string($response);

                        if (((string)$response->Response->ResponseStatusDescription) != 'Success')
                            return false;

                        foreach($response->SearchResults->children() AS $location) {
                            if (
//                                !strcasecmp((string)$location->AddressKeyFormat->AddressLine, $address) &&
                                !strcasecmp((string)$location->AddressKeyFormat->PoliticalDivision2, $city) &&
                                !strcasecmp((string)$location->AddressKeyFormat->CountryCode, $countryCode) &&
                                !strcasecmp((string)$location->AddressKeyFormat->PostcodePrimaryLow, $zipCode)
                            )
                            {


                                $data = [
                                    'name' => (string)$location->AddressKeyFormat->ConsigneeName,
                                    'address' => (string)$location->AddressKeyFormat->AddressLine,
                                    'city' => (string)$location->AddressKeyFormat->PoliticalDivision2,
                                    'zip' => (string)$location->AddressKeyFormat->PostcodePrimaryLow,
                                    'country_code' => (string)$location->AddressKeyFormat->CountryCode,
                                    'tel' => (string)$location->PhoneNumber,
                                    'desc' => (string)$location->SpecialInstructions->Segment
                                ];
                                break;
                            }
                        }

                        if(!$data)
                            return false;

                        $name = $data['name'];
                        $name = explode(' - ', $name);
                        if (!isset($name[1]) OR $name[1] == '')
                            $data['name'] = $name[0];

                        if (strlen($data['desc']) < 2)
                            $data['desc'] = '';
                    }
                }

            } catch (Exception $ex) {
                $data = false;
            }

            Yii::app()->cache->set($CACHE_NAME, $data, 60*60*24);
        }


        return $data;
    }


}

?>
