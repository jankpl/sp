<?php

class OneWorldExpressPackage extends CFormModel
{
	public $hawb; // BLANK or UNIQUE
	public $company; // REQ
	public $contact; // REQ
	public $address1; // REQ
	public $address2;
	public $address3;
	public $city; // REQ
	public $countryCode; // REQ
	public $postCode; // REQ
	public $telephone; // REQ
	public $numerOfPieces; // REQ
	public $weight; // REQ
	public $description; // REQ
	public $valueOfProduct; // REQ
	public $currency; // REQ
	public $senderName; // REQ
	public $reference; // REQ
	public $serviceCode; // REQ
	public $accountCode; // REQ
	public $accountUsername; // REQ
	public $accountPassword; // REQ
	public $routing; // 0 OR 1
	public $fullPallet; //Palletways // 0 or 1
	public $halfPallet; //Palletways // 0 or 1
	public $quarterPaller; //Palletways // 0 or 1
	public $documentType; // FedEx // DOC or NONDOC
	public $notes; // FedEx
	public $weightWidthLeightHeight; // 1%%2%%3%%4 AND && SEPARATOR FOR MORE PIECES
	public $numberOfBoxes; // FedEx
	public $email;
	public $transactionId;
	public $itemType;
	public $blank1; // future use
	public $blank2; // future use
	public $blank3; // future use

	public function rules()
	{
		return array(
			array('company, contact, address1, city, countryCode, postCode, telephone, numerOfPieces, weight, description, valueOfProduct, currency, senderName, reference, serviceCode, accountCode, accountUsername, accountPassword', 'required'),
		);
	}

	/**
	 * Mapping max attributes length for trimming
	 * @return array
	 */
	protected static function attributesMaxLength()
	{
		return [
			'hawb' => 20,
			'company' => 35,
			'contact' => 35,
			'address1' => 30,
			'address2' => 30,
			'address3' => 30,
			'city' => 45,
			'postCode' => 10,
			'telephone' => 30,
			'numerOfPieces' => 5,
			'weight' => 10,
			'description' => 30,
			'valueOfProduct' => 10,
			'senderName' => 30,
			'reference' => 30,
		];
	}

	/**
	 * List of OneWorldExpressPackage attributes that need to have special characters filtering
	 * @return array
	 */
	protected static function attributesToFilterCharacters()
	{
		return [
			'company',
			'contact',
			'address1',
			'address2',
			'address3',
			'city',
			'description',
			'senderName',
			'reference',
			'notes'
		];
	}




    protected static function addressLinesConverter($addressLine1, $addressLine2, $getLineNo)
    {

        if(mb_strlen($addressLine1) <= 30 && mb_strlen($addressLine2) <= 30)
        {
            if($getLineNo == 0)
                return $addressLine1;
            else if($getLineNo == 1)
                return $addressLine2;
            else if($getLineNo == 2)
                return '';

        } else {

            $address = trim($addressLine1) . ' ' . trim($addressLine2);
            $address = trim($address);

            $newAddress = [];
            $newAddress[0] = mb_substr($address, 0, 30);
            $newAddress[1] = mb_substr($address, 30, 30);
            $newAddress[2] = mb_substr($address, 60, 30);

            return trim($newAddress[$getLineNo]);

        }
    }

	public function getString()
	{
//
//		if(!$this->validate())
//			return false;

		$this->address1 = self::fixDotInAddress($this->address1);

		foreach(self::attributesToFilterCharacters() AS $attribute)
			$this->$attribute = S_Useful::removeNationalCharacters($this->$attribute);

		foreach(self::attributesMaxLength() AS $attribute => $length)
			$this->$attribute = mb_substr($this->$attribute, 0, $length);

		$string = implode('||', str_replace('||', ' ',$this->attributes));

		MyDump::dump('ooe.txt', print_r($string,1));

		return $string;
	}

	public static function createByCourierTypeOoe(CourierTypeOoe $courier, $differentReference = false)
	{
		$model = new self;

//		$model->company = $courier->courier->receiverAddressData->company;
//		$model->contact = $courier->courier->receiverAddressData->name;
		$model->company = $courier->courier->receiverAddressData->company != '' ? $courier->courier->receiverAddressData->company : $courier->courier->receiverAddressData->getUsefulName();
		$model->contact = $courier->courier->receiverAddressData->name != '' ? $courier->courier->receiverAddressData->name : $courier->courier->receiverAddressData->getUsefulName();

		$model->address1 = self::addressLinesConverter($courier->courier->receiverAddressData->address_line_1, $courier->courier->receiverAddressData->address_line_2, 0);
		$model->address2 = self::addressLinesConverter($courier->courier->receiverAddressData->address_line_1, $courier->courier->receiverAddressData->address_line_2, 1);
		$model->address3 = self::addressLinesConverter($courier->courier->receiverAddressData->address_line_1, $courier->courier->receiverAddressData->address_line_2, 2);
		$model->city = $courier->courier->receiverAddressData->city;
		$model->countryCode = $courier->courier->receiverAddressData->country0->code;
		$model->postCode = $courier->courier->receiverAddressData->zip_code;
		$model->telephone = $courier->courier->receiverAddressData->tel;
		$model->numerOfPieces = 1;
//		$model->numberOfBoxes = 1;
		$model->weight = substr($courier->courier->package_weight,0, 5);
		$model->description = $courier->courier->package_content;
		$model->valueOfProduct = $courier->courier->getPackageValueConverted('EUR');
		$model->currency = 'EUR';
		$model->senderName = $courier->courier->senderAddressData->getUsefulName();

		if($differentReference)
			$model->reference = substr($differentReference, 0,30);
		else
			$model->reference = $courier->courier->local_id;

		$model->serviceCode = $courier->service_code;
		$model->accountCode = $courier->_accountCode;
		$model->accountUsername = $courier->_accountUsername;
		$model->accountPassword = $courier->_accountPassword;
		$model->routing = 0;

		$model->itemType = $model->description;
		$model->weightWidthLeightHeight = $model->weight.'%%'.$courier->courier->package_size_w.'%%'.$courier->courier->package_size_l.'%%'.$courier->courier->package_size_d;

		// ADD USA STATE NAME IF PACKAGE IS TO USA
		if($model->countryCode == 'US')
		{
			$state = UsaZipState::getStateByZip($model->postCode);
			$model->address3 = $state;
		}

		// ADD PALLETTE TYPE IF SERVICE IS PALLETWAYS
		if(self::isPalletways($model->serviceCode))
		{
			$model->fullPallet = 0;
			$model->halfPallet = 0;
			$model->quarterPaller = 0;


			if($courier->_palletway_size == CourierTypeOoe::PALLETWAY_FULL)
				$model->fullPallet = 1;
			else if($courier->_palletway_size == CourierTypeOoe::PALLETWAY_HALF)
				$model->halfPallet = 1;
			else if($courier->_palletway_size == CourierTypeOoe::PALLETWAY_QUARTER)
				$model->quarterPaller = 1;
		}


		return $model;
	}

	public static function createByCourierTypeInternal(CourierTypeInternal $courier, AddressData $from, AddressData $to, $service_code, $account_code, $account_username, $account_password)
	{
		$model = new self;

		$model->company = $to->company != '' ? $to->company : $to->getUsefulName();
		$model->contact = $to->name != '' ? $to->name : $to->getUsefulName();
		$model->address1 = self::addressLinesConverter($to->address_line_1, $to->address_line_2, 0);
		$model->address2 = self::addressLinesConverter($to->address_line_1, $to->address_line_2, 1);
		$model->address3 = self::addressLinesConverter($to->address_line_1, $to->address_line_2, 2);
		$model->city = $to->city;
		$model->countryCode = $to->country0->code;
		$model->countryCode = $to->country0->code;
		$model->postCode = $to->zip_code;
		$model->telephone = $to->tel;


		$model->numerOfPieces = 1;
//		$model->numberOfBoxes = 1;
		$model->weight = substr($courier->courier->getWeight(true),0, 5);
		$model->description = $courier->courier->package_content;
		$model->valueOfProduct = $courier->courier->getPackageValueConverted('EUR');
		$model->currency = 'EUR';
		$model->senderName = $from->getUsefulName();
		$model->reference = $courier->courier->local_id;

		$model->serviceCode = $service_code;
		$model->accountCode = $account_code;
		$model->accountUsername = $account_username;
		$model->accountPassword = $account_password;
		$model->routing = 0;

		// ADD USA STATE NAME IF PACKAGE IS TO USA
		if($model->countryCode == 'US')
		{
			$state = UsaZipState::getStateByZip($model->postCode);
			$model->address3 = $state;

            $model->postCode = substr($model->postCode,0,5);
		}

		// ADD PALLETTE TYPE IF SERVICE IS PALLETWAYS
		if(self::isPalletways($model->serviceCode))
		{
			$model->fullPallet = 1;
			$model->halfPallet = 0;
			$model->quarterPaller = 0;
		}


		// @ 06.12.2017
        // Special rule for user YesSport (#46) - put content on label
        if($courier->courier->user_id == 46 && strtoupper($service_code) == 'REGPOSTIREEUR')
            $model->reference = $courier->courier->package_content;



//		// @07.06.2018 - special rule for DHL NL
//        if(strtoupper($service_code) == 'MMC')
//        {
//            if($model->address2 != '')
//            {
//                $model->address1 = trim($model->address1.' '.$model->address2);
//                $model->address2 = '';
//            }
//        }

		return $model;

	}

    public static function createByCourierTypeU(CourierTypeU $courierTypeU)
    {
        $model = new self;

        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model->company = $to->company != '' ? $to->company : $to->getUsefulName();
        $model->contact = $to->name != '' ? $to->name : $to->getUsefulName();
        $model->address1 = self::addressLinesConverter($to->address_line_1, $to->address_line_2, 0);
        $model->address2 = self::addressLinesConverter($to->address_line_1, $to->address_line_2, 1);
        $model->address3 = self::addressLinesConverter($to->address_line_1, $to->address_line_2, 2);
        $model->city = $to->city;
        $model->countryCode = $to->country0->code;
        $model->countryCode = $to->country0->code;
        $model->postCode = $to->zip_code;
        $model->telephone = $to->tel;


        $model->numerOfPieces = 1;
//		$model->numberOfBoxes = 1;
        $model->weight = substr($courierTypeU->courier->package_weight,0, 5);
        $model->description = $courierTypeU->courier->package_content;
        $model->valueOfProduct = $courierTypeU->courier->getPackageValueConverted('EUR');
        $model->currency = 'EUR';
        $model->senderName = $from->getUsefulName();
        $model->reference = $courierTypeU->courier->local_id;

        $model->serviceCode = $courierTypeU->courierUOperator->_ooe_service;
        $model->accountCode = $courierTypeU->courierUOperator->_ooe_account;
        $model->accountUsername = $courierTypeU->courierUOperator->_ooe_login;
        $model->accountPassword = $courierTypeU->courierUOperator->_ooe_pass;
        $model->routing = 0;

        // ADD USA STATE NAME IF PACKAGE IS TO USA
        if($model->countryCode == 'US')
        {
            $state = UsaZipState::getStateByZip($model->postCode);
            $model->address3 = $state;

            $model->postCode = substr($model->postCode,0,5);
        }

        // ADD PALLETTE TYPE IF SERVICE IS PALLETWAYS
        if(self::isPalletways($model->serviceCode))
        {
            $model->fullPallet = 1;
            $model->halfPallet = 0;
            $model->quarterPaller = 0;
        }

        return $model;

    }

	public static function isPalletways($service)
	{
		$service = strtoupper($service);
		return in_array($service, self::listOfPAlletways());
	}

	protected static function listOfPAlletways()
	{
		$data[] = 'B|J';
		$data[] = 'B|Z';
		$data[] = 'A|A';
		$data[] = 'A|C';
		$data[] = 'A|E';
		$data[] = 'A|F';
		$data[] = 'A|H';
		$data[] = 'A|Y';
		$data[] = 'B|B';
		$data[] = 'B|K';

		return $data;
	}

	public static function createByPostal(Postal $postal, $service_code, $account_code, $account_username, $account_password)
	{

		$weight = $postal->weightClassValue;
		$weight /= 1000; // g into kg
		$weight = round($weight,4);

		$model = new self;

		$model->company = $postal->receiverAddressData->company != '' ? $postal->receiverAddressData->company : $postal->receiverAddressData->getUsefulName();
		$model->contact = $postal->receiverAddressData->name != '' ? $postal->receiverAddressData->name : $postal->receiverAddressData->getUsefulName();
		$model->address1 = self::addressLinesConverter($postal->receiverAddressData->address_line_1, $postal->receiverAddressData->address_line_2, 0);
		$model->address2 = self::addressLinesConverter($postal->receiverAddressData->address_line_1, $postal->receiverAddressData->address_line_2, 1);
		$model->address3 = self::addressLinesConverter($postal->receiverAddressData->address_line_1, $postal->receiverAddressData->address_line_2, 2);
		$model->city = $postal->receiverAddressData->city;
		$model->countryCode = $postal->receiverAddressData->country0->code;
		$model->postCode = $postal->receiverAddressData->zip_code;
//		$model->postCode = '44000';
		$model->telephone = $postal->receiverAddressData->tel;

		$model->numerOfPieces = 1;
		$model->weight = $weight;
		$model->description = 'Postal #'.$postal->local_id;
		$model->valueOfProduct = $postal->value ? $postal->getValueConverted('EUR') : 0;
		$model->currency = 'EUR';
		$model->senderName = $postal->senderAddressData->getUsefulName();
		$model->reference = $postal->local_id;

		$model->serviceCode = $service_code;
		$model->accountCode = $account_code;
		$model->accountUsername = $account_username;
		$model->accountPassword = $account_password;
		$model->routing = 0;

		// ADD USA STATE NAME IF PACKAGE IS TO USA
		if($model->countryCode == 'US')
		{
			$state = UsaZipState::getStateByZip($model->postCode);
			$model->address3 = $state;
		}

		return $model;

	}

	protected function fixDotInAddress($text)
	{
		$text = str_replace('.','. ', $text);
		$text = trim($text);
		$text = preg_replace('!\s+!', ' ', $text);

		if(mb_strlen($text) > 30) {
			$text = str_replace('.', '', $text);
			$text = preg_replace('!\s+!', ' ', $text);
		}

		return $text;
	}
}