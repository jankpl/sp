<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<?php

$loginInfo = Yii::app()->user->getFlash('loginInfo');
if($loginInfo != '')
    echo '<div class="alert alert-warning text-center">'.$loginInfo.'</div>';
?>
<div id="loginBlock" style="background: rgba(242,157,1,0.4);">

<?php echo $content; ?>

</div>


<?php $this->endContent(); ?>

