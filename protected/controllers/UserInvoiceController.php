<?php

class UserInvoiceController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    public function filters()
    {
        return array_merge(array(
            'accessControl', // perform access control for CRUD operations
            array(
                'application.filters.HttpsFilter',
                'bypass' => false,
            ),
        ));
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
//    public function actionView($no)
//    {
//        $this->panelLeftMenuActive = 706;
//
//        $model = UserInvoice::model()->findByAttributes(array('user_id' => Yii::app()->user->id, 'no' => $no));
//        if($model === NULL OR $model->stat == UserInvoice::UNPUBLISHED)
//            throw new CHttpException(404);
//
//        $model->seen();
//
//        $this->render('view',array(
//            'model'=> $model,
//        ));
//    }
//
//    const MODE_WAITING = 'w';
//    const MODE_ALL = 'a';
//    const MODE_NEW = 'n';

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->panelLeftMenuActive = 706;

//        $pageSize = 1;
//        $page = intval($page) - 1;

//        if(!in_array($mode, [self::MODE_WAITING, self::MODE_ALL]))
//            throw new CHttpException(404);

        $this->render('index',array(
        ));
    }

    /**
     * Work around method for refreshing only GridViewContent, not whole page in Yii's manner.
     * Description: http://www.yiiframework.com/wiki/205/how-to-show-ajax-delete-status-in-cgridview-like-flash-messages/
     */
    public function _getGridViewUserInvoicegrid(){

        // for transation bot to prepare this text
//        $temp = Yii::t('courier', 'Nowa paczka typu "{internal}"', ['{internal}' => Courier::getTypesNames()[Courier::TYPE_INTERNAL]]);

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new _UserInvoiceGridViewUser('search');
        $model->unsetAttributes();

        $model->stat_inv = UserInvoice::INV_STAT_NONE;

        if (isset($_GET['UserInvoiceGridViewUser']))
            $model->setAttributes($_GET['UserInvoiceGridViewUser']);


//        Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);

        $pageSize = Yii::app()->user->getState('pageSize', 50);

        Yii::t('invoice', 'Pobierz fakturę'); // for translation
        Yii::t('invoice', 'Pobierz zestawienie'); // for translation

        echo '<input type="hidden" class="grid-data"/>';
//$this->widget('zii.widgets.grid.CGridView', array(
//$this->widget('yiiwheels.widgets.grid.WhGridView', array(
//        $this->widget('ext.selgridview.BootSelGridView', array(
        $this->widget('SPGridViewNoGet', array(
            'id' => 'couriergrid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectableRows'=>false,
//            'enableHistory' => true,
//            'selectionChanged'=>'function(id){ checkNumberOfChecks(id); }',
            'beforeAjaxUpdate' => 'function(id,options){$(\'.gridview-overlay\').show();}',
            'afterAjaxUpdate' => 'function(id,data){
            $(\'.gridview-overlay\').hide();
            $(\'[name="UserInvoiceGridViewUser[inv_payment_date]"], [name="UserInvoiceGridViewUser[inv_date]"]\').datepicker({
                 dateFormat : \'yy-mm-dd\',
                minDate: "-5Y", 
                maxDate: "+0M",
                changeMonth: true,
                changeYear: true,
            });
            }',
//    'afterAjaxUpdate' => "function(id,data){  $('[name=\"selected_rows[]\"]').on('click', function(){
//        checkNumberOfChecks($(this));
//    });
//      }",
            //'rowCssClassExpression'=>'$data->stat?"row-open":"row-closed"',
            'template'=>"{pager}\n{summary}\n{items}\n{summary}\n{pager}",
            'htmlOptions' => ['style' => 'table-layout: fixed'],
//            'addingHeaders' => array(
//                array(
//                    array(
//                        'text'=> '',
//                        'colspan'=>4,
//                        'options'=> []
//                    ),
//                    array(
//                        'text'=>Yii::t('courier', 'Data'),
//                        'colspan'=>2,
//                        'options'=>[]
//                    ),
//                    array(
//                        'text'=>Yii::t('courier', 'Nadawca'),
//                        'colspan'=>2,
//                        'options'=>[]
//                    ),
//                    array(
//                        'text'=>Yii::t('courier', 'Odbiorca'),
//                        'colspan'=>2,
//                        'options'=>[]
//                    ),
//                    array(
//                        'text'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10 ,20=>20 ,50=>50 , 100=>100, 250 => 250, 500 => 500),array(
//                            'onchange'=>"$.fn.yiiGridView.update('couriergrid',{ data:{pageSize: $(this).val() }})",
//                        )),
//
//                        'colspan'=>6,
//                        'options'=>[]
//                    )
//                ),
//            ),
//            'rowCssClassExpression' => '
//                ( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) .
//                ( in_array($data->courier_stat_id, [CourierStat::CANCELLED, CourierStat::NEW_ORDER, CourierStat::PACKAGE_PROCESSING]) ? " disabled" : null )
//            ',
            'columns' => array(
                array(
                    'name'=>'no',
                    'header'=>Yii::t('invoice','#No'),
                    'type'=>'raw',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'htmlOptions' => [
                        'class' => '"text-right ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '40',
                    ],
                ),
                [
                    'name' => 'inv_date',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'htmlOptions' => [
                        'class' => '"text-center ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '80',
                    ],
                ],
                [
                    'name' => 'inv_payment_date',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'htmlOptions' => [
                        'class' => '"text-center ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '80',
                    ],
                ],
                [
                    'name' => 'inv_currency',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'filter' => CHtml::listData(Currency::model()->findAll(),"id", "short_name"),
                    'value' => 'Currency::nameById($data->inv_currency)',
                    'htmlOptions' => [
                        'class' => '"text-center ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '60',
                    ],
                ],
                [
                    'name' => 'inv_value',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'value' => '$data->stat_inv != UserInvoice::INV_STAT_CANCELLED ? str_replace(".", ",", $data->inv_value)." ".Currency::nameById($data->inv_currency) : "-"',
                    'htmlOptions' => [
                        'class' => '"text-right ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '110',
                    ],
                ],
                [
                    'name' => 'paid_value',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'value' => 'str_replace(".", ",", $data->paid_value)." ".Currency::nameById($data->inv_currency)',
                    'htmlOptions' => [
                        'class' => '"text-right ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '110',
                    ],
                ],
                [
                    'name' => '_isPaid',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'value' => '$data->isPaid() ? Yii::t(\'site\',\'Tak\') : Yii::t(\'site\',\'Nie\')',
                    'filter' => [
                        0 => Yii::t('site','Nie'),
                        1 => Yii::t('site','Tak'),
                    ],
                    'htmlOptions' => [
                        'class' => '"text-center ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")." ".($data->isPaid() ? "" : "red bold")',
                        'width' => '70',
                    ],
                ],
                [
                    'name' => 'stat_inv',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'value' => 'UserInvoice::getStatInvArray()[$data->stat_inv]',
                    'filter' => UserInvoice::getStatInvArray(),
                    'htmlOptions' => [
                        'class' => '"text-center ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '100',
                    ],
                ],
                [
                    'name' => 'title',
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'htmlOptions' => [
                        'class' => '"text-left ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '150',
                    ],
                ],
                [
                    'type'=>'raw',
                    'header' => Yii::t('invoice', 'Faktura'),
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'htmlOptions' => [
                        'class' => '"text-center ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '140',
                    ],
                    'value' => '$data->inv_file_name != "" && !$data->stat_inv ? CHtml::link(Yii::t(\'invoice\', \'Pobierz fakturę\'), [\'/userInvoice/getFile\', \'hash\' => $data->hash, \'type\' => UserInvoice::FILE_TYPE_INV], [\'class\' => \'btn btn-sm\']) : "-"',
                ],
                [
                    'type'=>'raw',
                    'header' => Yii::t('invoice', 'Zestawienie'),
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                    'htmlOptions' => [
                        'class' => '"text-center ".($data->stat == '.UserInvoice::PUBLISHED.' ? "bold" : "")',
                        'width' => '140',
                    ],
                    'value' => '$data->sheet_file_name != "" && !$data->stat_inv ? CHtml::link(Yii::t(\'invoice\', \'Pobierz zestawienie\'), [\'/userInvoice/getFile\', \'hash\' => $data->hash, \'type\' => UserInvoice::FILE_TYPE_SHEET], [\'class\' => \'btn btn-sm\']) : "-"',
                ],
                [
                    'name' => 'note',
                    'class'=>'DataColumn',
//                    'evaluateHtmlOptions'=>true,
                    'type'=>'raw',
                    'htmlOptions' => [
                        'width' => '250',
                    ],
                    'value' => '$data->note != "" ? "<div style=\"overflow-y: scroll; width: 100%; max-height: 30px;\">".nl2br(CHtml::encode($data->note))."</div>" : "-"',
                ],
            ),
        ));
    }

//    public function actionListMore()
//    {
//
//        if(Yii::app()->request->isAjaxRequest) {
//
//            $model = UserInvoice::model()->findByPk($_GET['id']);
//
//            if($model !== NULL ) {
//
//                $model->seen();
//
//                $html = $this->renderPartial('_listMore', [
//                    'model' => $model,
//                ], true);
//
//                echo $html;
//            }
//
//        }
//    }

    public function actionGetFile($hash, $type)
    {
        $model = UserInvoice::model()->findByAttributes(array('hash' => $hash));

        if($model === NULL ) {
            throw new CHttpException(404);
        }

        if($model->user_id != Yii::app()->user->id) {
            throw new CHttpException(403);
        }

        if($type == UserInvoice::FILE_TYPE_INV) {
            $file_path = $model->inv_file_path;
            $file_name = $model->inv_file_name;
            $file_type = $model->inv_file_type;
        }
        else if($type == UserInvoice::FILE_TYPE_SHEET) {
            $file_path = $model->sheet_file_path;
            $file_name = $model->sheet_file_name;
            $file_type = $model->sheet_file_type;
        }
        else
            throw new CHttpException(404);


        if($file_path == NULL)
            throw new CHttpException(404);

        if($model->stat_inv != 0)
            throw new CHttpException(403);

        $model->seen();

        return Yii::app()->getRequest()->sendFile($file_name, @file_get_contents($file_path), $file_type);
    }

}

