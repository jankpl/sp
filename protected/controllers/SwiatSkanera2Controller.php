<?php

class SwiatSkanera2Controller extends Controller
{
    public $validated = false;


    public $admin_id = false;
    public $device_id = false;

    public $login = false;
    public $message = false;

    const STAT_OK = 'OK';
    const STAT_FAIL = 'FAIL';

    protected function checkPin($pin)
    {
        $data = [
            'adam' => '112233',
            'tomek9' => '999999',
        ];

        $login = array_search($pin, $data);

        $this->login = $login;

        return $login;
    }

    protected function checkMessages($login)
    {
        $messages = [
            'adam' => 'You account is locked!',
        ];

        $message = false;

        if(isset($messages[$login]))
            $message = $messages[$login];

        $this->message = $message;

        return $message;
    }


    public function beforeAction($action)
    {
        MyDump::dump('swiatSkanera2.txt', print_r($action->id,1).' : '.print_r($_REQUEST,1));


        $hash = SwiatSkanera::getCurrentPassword();
        $headers = getallheaders();

        MyDump::dump('swiatSkanera2.txt', 'HEADER : '.print_r($headers,1));

        $pin = $headers['X-Pin'];
        $key = $headers['X-Key'];
        $device_id = $headers['X-Device-Id'];
        $app_version = $headers['X-App-Version'];

        $error = false;

        if($key != $hash)
        {
            $error = true;
            $response = [
                'success' => false,
                'error' => 'Invalid hash',
                'data' => NULL,
            ];
        }
        else if($app_version < 1.2)
        {
            $error = true;
            $response = [
                'success' => false,
                'error' => 'Please update your APP - click to download: https://swiatprzesylek.pl/app.apk',
                'data' => [
                    'url' => 'https://swiatprzesylek.pl/app.apk',
                ],
            ];
        }
        else if(!$this->checkPin($pin))
        {
            $error = true;
            $response = [
                'success' => false,
                'error' => 'Invalid PIN',
                'data' => NULL,
            ];
        }
        else if($this->checkMessages($this->login))
        {
            $error = true;
            $response = [
                'success' => false,
                'error' => $this->message,
                'data' => NULL,
            ];
        }

        if($error)
        {

            MyDump::dump('swiatSkanera2.txt', 'RESP: '.print_r($response,1));
            echo CJSON::encode($response);
            Yii::app()->end();
        } else
            $this->validated = true;

        return parent::beforeAction($action);
    }

    public function actionLogin()
    {

        if($this->validated)
        {
            $response = [
                'success' => true,
//            'error' => $this->message,
                'data' => [
                    'username' => $this->login,
                ],
            ];

            MyDump::dump('swiatSkanera2.txt', 'RESP: '.print_r($response,1));

            echo CJSON::encode($response);
            Yii::app()->end();
        }
    }

    public function actionGetStatuses()
    {
        $response = [
            'success' => true,
            'data' => SwiatSkanera::getStatusesList(),
        ];
        MyDump::dump('swiatSkanera2.txt', 'RESP: '.print_r($response,1));

        echo CJSON::encode($response);
    }

    public function actionSetStatus()
    {
        $input = $_REQUEST;

        $model = new SwiatSkanera();
        $model->setAttributes($input);

        if(!$model->validate())
        {
            MyDump::dump('swiatSkanera2.txt', print_r($model->getErrors(),1));
            $response = [
                'success' => false,
                'error' => S_Useful::arrayToNiceString($model->getErrors(), false),
                'data' => NULL,
            ];
        } else {

            try {
                $model->setStatusForItem();
            }
            catch(Exception $ex)
            {
                $response = [
                    'success' => false,
                    'error' => $ex->getMessage(),
                    'data' => NULL,
                ];
            }

            if(!isset($response))
                $response = [
                    'success' => true,
                    'error' => NULL,
                    'data' => NULL,
                ];
        }

        MyDump::dump('swiatSkanera2.txt', 'RESP: '.print_r($response,1));
        echo CJSON::encode($response);
    }
}