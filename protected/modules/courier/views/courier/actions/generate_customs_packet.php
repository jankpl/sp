<div class="alert alert-success text-center">
    <h4><?php echo Yii::t('courier','Dokumenty celne');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t('courier','Generuj dokumenty celne'), array('/courier/courier/customsPacket', 'hash' => $model->hash), array('target' => '_blank', 'class' => 'btn'));?>
</div>
