<?php

class MatrixController extends Controller
{

    public function actionIndex()
    {
        $response = false;
        $data = false;
        if(isset($_POST['test']))
        {
            $data = $_POST['test'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, '83.13.40.193:51005');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            $response = curl_exec($ch);

        }
        $this->render('index',
            [
                'response' => $response,
                'data' => $data,
            ]);
    }



}