<?php

class KsiazkaNadawcza
{

    const MODE_COURIER = 1;
    const MODE_POSTAL = 2;

    const OP_MAILSOLUTION = 1;
    const OP_FIREBUSINESS = 2;
    const OP_SP = 3;

    public static function generate(array $items, $forceMode, $postingDate = false)
    {
        $done = [];

        Yii::app()->getModule('postal');
        Yii::app()->getModule('courier');

        $ppOperators = CourierOperator::getOperatorsForBroker(CourierOperator::BROKER_POCZTA_POLSKA);
        $ppPostalOperators = PostalOperator::getOperatorsForBroker(Postal::POSTAL_BROKER_POCZTA_POLSKA);

        // filter items for only valid PP
        foreach($items AS $key => $item)
        {
            $valid = true;

            if($item instanceof Courier) {

                if($item->getType() == Courier::TYPE_INTERNAL)
                {
                    $pp = false;
                    if($item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->delivery_operator_id, $ppOperators) && $item->courierTypeInternal->commonLabel)
                    {
                        $pp = true;
                    }
                    else if(!$item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->pickup_operator_id, $ppOperators) && $item->courierTypeInternal->pickupLabel)
                    {
                        $pp = true;
                    }
                    else if(!$item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->delivery_operator_id, $ppOperators) && $item->courierTypeInternal->deliveryLabel)
                    {
                        $pp = true;
                    }

                    if(!$pp)
                        $valid = false;
                }

                if(in_array($item->courier_stat_id, [CourierStat::CANCELLED_PROBLEM, CourierStat::CANCELLED, CourierStat::CANCELLED_USER]))
                    $valid = false;

                if(in_array($item->user_cancel, [Courier::USER_CANCEL_CONFIRMED, Courier::USER_CANCEL_REQUEST, Courier::USER_CANCEL_WARNED]))
                    $valid = false;


            }

            if($item instanceof Postal) {

                if(in_array($item->postal_stat_id, [PostalStat::CANCELLED_PROBLEM, PostalStat::CANCELLED, PostalStat::CANCELLED_USER]))
                    $valid = false;

                if(in_array($item->user_cancel, [Postal::USER_CANCEL_CONFIRMED, Postal::USER_CANCEL_REQUEST, Postal::USER_CANCEL_WARNED]))
                    $valid = false;

                if(!in_array($item->postal_operator_id, $ppPostalOperators))
                    $valid = false;

            }


            if(!$valid)
                unset($items[$key]);
        }
        //////////////////////


        $totalNumber = S_Useful::sizeof($items);

        if($totalNumber > 2500)
            throw new Exception(Yii::t('courier','Za dużo pozycji do wygenerowania KN.'));


        $pageSize = 21;

        $totalPages = ceil($totalNumber / $pageSize);

        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();
        $pdf->setMargins(10,10,10, true);


        if($postingDate)
            $data = $postingDate;
        else
            $data = date('Y-m-d');


        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $baseYFirst = 27;
        $baseY = $baseYFirst;
        $h = 8;
        $i = 1;

        $page = 0;

        $groupedItems = [];
        // Group by PP operator
        foreach($items AS $key => $item)
        {
            if($item instanceof Postal) // kurier
            {
                $uniqId = PostalOperator::getOperatorUniqIdForId($item->postal_operator_id);
                if($uniqId && in_array($uniqId, [PostalOperator::UNIQID_OPERATOR_PP_FIREBUSINESS_Polecona_Firmowa_PRIO, PostalOperator::UNIQID_OPERATOR_PP_FIREBUSINESS_Polecona_Firmowa]))
                {
                    $groupedItems[self::OP_FIREBUSINESS][$key] = $item;
                } else {
                    $groupedItems[self::OP_MAILSOLUTION][$key] = $item;
                }
            }

            if($item instanceof Courier)
            {
                if($item->getType() == Courier::TYPE_INTERNAL)
                {
                    if($item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->delivery_operator_id, $ppOperators) && $item->courierTypeInternal->commonLabel)
                    {

                        $uniqId = CourierOperator::getUniqIdById($item->courierTypeInternal->delivery_operator_id);
                        if($uniqId && in_array($uniqId, [CourierOperator::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa, CourierOperator::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa_PRIO]))
                        {
                            $groupedItems[self::OP_FIREBUSINESS][$key] = $item;
                        } else {
                            $groupedItems[self::OP_MAILSOLUTION][$key] = $item;
                        }

                    }
                    else if(!$item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->pickup_operator_id, $ppOperators) && $item->courierTypeInternal->pickupLabel)
                    {

                        $uniqId = CourierOperator::getUniqIdById($item->courierTypeInternal->pickup_operator_id);
                        if($uniqId && in_array($uniqId, [CourierOperator::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa, CourierOperator::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa_PRIO]))
                        {
                            $groupedItems[self::OP_FIREBUSINESS][$key] = $item;
                        } else {
                            $groupedItems[self::OP_MAILSOLUTION][$key] = $item;
                        }

                    }
                    else if(!$item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->delivery_operator_id, $ppOperators) && $item->courierTypeInternal->deliveryLabel)
                    {

                        $uniqId = CourierOperator::getUniqIdById($item->courierTypeInternal->delivery_operator_id);
                        if($uniqId && in_array($uniqId, [CourierOperator::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa, CourierOperator::UNIQID_PP_FIREBUSINESS_Polecona_Firmowa_PRIO]))
                        {
                            $groupedItems[self::OP_FIREBUSINESS][$key] = $item;
                        } else {
                            $groupedItems[self::OP_MAILSOLUTION][$key] = $item;
                        }
                    }
                }
            }




        }


        foreach($groupedItems AS $opId => $items) {

            if(!S_Useful::sizeof($items))
                continue;

            $pdf->AddPage('L','A4');
            $baseY = $baseYFirst;
            $page++;
            $pageH = $pdf->getPageHeight();
            $pageW = $pdf->getPageWidth();
            $pdf->SetAutoPageBreak(false);
            $i = 1;

            if($opId == self::OP_MAILSOLUTION)
                $sender = "Mail Solution\r\nul. GRODKOWSKA 40, 48-300 NYSA";
            else if($opId == self::OP_SP)
                $sender = "Świat Przesyłek\r\nul. GRODKOWSKA 40, 48-300 NYSA";
            else
                $sender = "Fire Business\r\nul. WAŁBRZYSKA 11/85, 02-739 WARSZAWA";

            foreach ($items AS $key => $item) {              //

                if ($baseY == $baseYFirst) {
                    $pdf->SetFont('freesans', '', 8, '', true);
                    $pdf->MultiCell(30, 8, "\r\nNADAWCA (IMIĘ I NAZWISKO LUB NAZWA, ADRES)", 1, 'L', false, 0, 5, 5, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(125, 8, $sender, 1, 'L', false, 0, 35, 5, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(30, 6, $data, 1, 'C', false, 0, 160, 5, true, 0, false, true, 0, 'M', true);
                    $pdf->SetFont('freesans', 'B', 6, '', true);
                    $pdf->MultiCell(30, 2, 'DATA NADANIA', ['B' => ['width' => 0.1], 'TRL' => ['width' => 0]], 'C', false, 0, 160, 11, true, 0, false, true, 0, 'M', true);
                    $pdf->SetFont('freesans', 'B', 14, '', true);
                    $pdf->MultiCell(81, 8, 'KSIĄŻKA NADAWCZA', 1, 'C', false, 0, 190, 5, true, 0, false, true, 0, 'M', true);
                    $pdf->SetFont('freesans', '', 8, '', true);
                    $pdf->MultiCell(20, 4, 'ORYGINAŁ*', 1, 'C', false, 0, 271, 5, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(20, 4, 'KOPIA*', 1, 'C', false, 0, 271, 9, true, 0, false, true, 0, 'M', true);

                    $pdf->SetFont('freesans', '', 6, '', true);
                    $pdf->MultiCell(8, 8, "L.p.", 1, 'L', false, 0, 5, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(40, 8, "ADRESAT (IMIĘ I NAZWISKO LUB NAZWA)", 1, 'L', false, 0, 13, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(60, 8, "DOKŁADNE MIEJSCE DORĘCZNIA", 1, 'L', false, 0, 53, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(20, 4, "KWOTA ZADEKL. WARTOŚĆ", 1, 'C', false, 0, 113, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(10, 4, "zł", 1, 'C', false, 0, 113, 19, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(10, 4, "gr", 1, 'C', false, 0, 123, 19, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(20, 4, "MASA", 1, 'C', false, 0, 133, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(10, 4, "kg", 1, 'C', false, 0, 133, 19, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(10, 4, "g", 1, 'C', false, 0, 143, 19, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(40, 8, "NUMER NADAWCZY", 1, 'C', false, 0, 153, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(15, 8, "RODZAJ PRZESYŁKI/ UWAGI", 1, 'C', false, 0, 193, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(15, 8, "PRZESYŁKA KRAJOWA/ ZAGRANICZNA", 1, 'C', false, 0, 208, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(15, 8, "GABARYT (A/B)", 1, 'C', false, 0, 223, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(15, 8, "ZWROTNE POTWIERDZENIE\r\nODBIORU (ZPO)", 1, 'C', false, 0, 238, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(20, 4, "OPŁATA", 1, 'C', false, 0, 253, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(10, 4, "zł", 1, 'C', false, 0, 253, 19, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(10, 4, "gr", 1, 'C', false, 0, 263, 19, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(18, 4, "KWOTA POBRANIA", 1, 'C', false, 0, 273, 15, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(9, 4, "zł", 1, 'C', false, 0, 273, 19, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(9, 4, "gr", 1, 'C', false, 0, 282, 19, true, 0, false, true, 0, 'M', true);

                    $pdf->SetFont('freesans', '', 4, '', true);
                    $pdf->MultiCell(8, 3, "", 1, 'C', false, 0, 5, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(40, 3, "2", 1, 'C', false, 0, 13, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(60, 3, "3", 1, 'C', false, 0, 53, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(20, 3, "4", 1, 'C', false, 0, 113, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(20, 3, "5", 1, 'C', false, 0, 133, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(40, 3, "6", 1, 'C', false, 0, 153, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(15, 3, "7", 1, 'C', false, 0, 193, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(15, 3, "8", 1, 'C', false, 0, 208, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(15, 3, "9", 1, 'C', false, 0, 223, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(15, 3, "10", 1, 'C', false, 0, 238, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(20, 3, "11", 1, 'C', false, 0, 253, 23, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(18, 3, "12", 1, 'C', false, 0, 273, 23, true, 0, false, true, 0, 'M', true);


                    $pdf->MultiCell(20, 4, '* - niepotrzebne skreślić', 0, 'C', false, 0, 3, $pageH - 20, true, 0, false, true, 0, 'M', true);

                    $pdf->SetFont('freesans', 'B', 6, '', true);
                    $pdf->MultiCell(50, 3, 'RODZAJ PRZESYŁKI', 1, 'C', false, 0, 5, $pageH - 15, true, 0, false, true, 0, 'M', true);
                    $pdf->SetFont('freesans', '', 6, '', true);
                    $pdf->MultiCell(10, 3, 'PRE', 1, 'C', false, 0, 5, $pageH - 12, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(40, 3, 'PRZESYŁKA REJESTROWANA EKONOMICZNA', 1, 'L', false, 0, 15, $pageH - 12, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(10, 3, 'PRP', 1, 'C', false, 0, 5, $pageH - 9, true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell(40, 3, 'PRZESYŁKA REJESTROWANA PRIORYTETOWA', 1, 'L', false, 0, 15, $pageH - 9, true, 0, false, true, 0, 'M', true);


                    $pdf->SetFont('freesans', '', 8);
                    $x = $pdf->GetX();
                    $y = $pdf->GetY();
                    $pdf->MultiCell(20, 3, $page . '/' . $totalPages, 0, 'R', false, 0, $pageW - 30, $pageH - 10);
                    $pdf->SetAbsX($x);
                    $pdf->SetAbsY($y);
                }


                $numer = 0;

                $receiver = $item->receiverAddressData;

                $adresat = $receiver->name != $receiver->company ? trim($receiver->company . ' ' . $receiver->name) : $receiver->name;
                $adres = trim($receiver->address_line_1 . ' ' . $receiver->address_line_2) . "\r\n" . $receiver->zip_code . ',' . $receiver->city . ' (' . $receiver->country0->name . ')';

                $wartosc_gr = 0;
                $wartosc_zl = 0;


                $krajowa = 'KRAJOWA';
                if ($receiver->country_id != CountryList::COUNTRY_PL)
                    $krajowa = 'ZAGRANICZNA';

                if ($item instanceof Courier) // kurier
                {
                    if ($item->getType() == Courier::TYPE_INTERNAL) {
                        $add = $item->courierTypeInternal->listAdditions();
                        if (in_array(CourierAdditionList::PP_OKRESLONA_WARTOSC, $add)) {
                            $value = $item->package_value;

                            $wartosc_zl = intval($value);
                            $wartosc_gr = $value - intval($value);
                        }
                    }
                }

                $typ = 'PRE';

                if ($item instanceof Postal) // kurier
                {

                    if (in_array($item->postal_operator_id, PostalOperator::getPocztaPolskaPrioOperators())) {
                        $typ = 'PRP';
                    }

                    $uniqId = PostalOperator::getOperatorUniqIdForId($item->postal_operator_id);
                    if ($uniqId && in_array($uniqId, PostalOperator::getPocztaPolskaPrioOperators(true))) {
                        $typ = 'PRP';
                    }

                }

                if ($item instanceof Courier) {
                    if ($item->getType() == Courier::TYPE_INTERNAL) {
                        if ($item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->delivery_operator_id, $ppOperators) && $item->courierTypeInternal->commonLabel) {
                            $numer = $item->courierTypeInternal->commonLabel->getTrack_id();

                            if (in_array($item->courierTypeInternal->delivery_operator_id, CourierOperator::getPocztaPolskaPrioOperators())) {
                                $typ = 'PRP';
                            }

                            $uniqId = CourierOperator::getUniqIdById($item->courierTypeInternal->delivery_operator_id);
                            if ($uniqId && in_array($uniqId, CourierOperator::getPocztaPolskaPrioOperators(true))) {
                                $typ = 'PRP';
                            }

                        } else if (!$item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->pickup_operator_id, $ppOperators) && $item->courierTypeInternal->pickupLabel) {
                            $numer = $item->courierTypeInternal->pickupLabel->getTrack_id();

                            if (in_array($item->courierTypeInternal->pickup_operator_id, CourierOperator::getPocztaPolskaPrioOperators())) {
                                $typ = 'PRP';
                            }

                            $uniqId = CourierOperator::getUniqIdById($item->courierTypeInternal->pickup_operator_id);
                            if ($uniqId && in_array($uniqId, CourierOperator::getPocztaPolskaPrioOperators(true))) {
                                $typ = 'PRP';
                            }
                        } else if (!$item->courierTypeInternal->common_operator_ordered && in_array($item->courierTypeInternal->delivery_operator_id, $ppOperators) && $item->courierTypeInternal->deliveryLabel) {
                            $numer = $item->courierTypeInternal->deliveryLabel->getTrack_id();

                            if (in_array($item->courierTypeInternal->delivery_operator_id, CourierOperator::getPocztaPolskaPrioOperators())) {
                                $typ = 'PRP';
                            }

                            $uniqId = CourierOperator::getUniqIdById($item->courierTypeInternal->delivery_operator_id);
                            if ($uniqId && in_array($uniqId, CourierOperator::getPocztaPolskaPrioOperators(true))) {
                                $typ = 'PRP';
                            }
                        }
                    }
                }


                if ($item instanceof Postal) {
                    if ($item->postal_operator_id)
                        $numer = $item->postalLabel->track_id;
                }

                $masa_kg = $masa_g = 0;

                if ($item instanceof Courier) // kurier
                {
                    $masa_kg = intval($item->package_weight);
                    $masa_g = $item->package_weight - intval($item->package_weight);
                }

                if ($item instanceof Postal) {
                    $weight = $item->getWeightClassValue();

                    $masa_kg = intval($weight / 1000);
                    $masa_g = $weight - 1000 * $masa_kg;
                }


                $zpo = 0;
                if ($item instanceof Courier) // kurier
                {
                    if ($item->getType() == Courier::TYPE_INTERNAL) {
                        $add = $item->courierTypeInternal->listAdditions();
                        if (in_array(CourierAdditionList::PP_POTW_ODBIORU, $add)) {
                            $zpo = 1;
                        }
                    }
                }

                $pdf->SetFont('freesans', '', 6, '', true);
                $pdf->MultiCell(8, $h, $i, 1, 'L', false, 0, 5, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(40, $h, $adresat, 1, 'L', false, 0, 13, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(60, $h, $adres, 1, 'L', false, 0, 53, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(10, $h, $wartosc_zl, 1, 'C', false, 0, 113, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(10, $h, $wartosc_gr, 1, 'C', false, 0, 123, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(10, $h, $masa_kg, 1, 'C', false, 0, 133, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(10, $h, $masa_g, 1, 'C', false, 0, 143, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->write1DBarcode($numer, 'C128', 153, $baseY, 40, 7, 1, $barcodeStyle);
                $pdf->MultiCell(40, $h, "", 1, 'C', false, 0, 153, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(40, 2, $numer, 0, 'C', false, 0, 153, $baseY + 6, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(15, $h, $typ, 1, 'C', false, 0, 193, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(15, $h, $krajowa, 1, 'C', false, 0, 208, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(15, $h, "A", 1, 'C', false, 0, 223, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(15, $h, $zpo, 1, 'C', false, 0, 238, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(20, $h, "zgodnie z umową", 1, 'C', false, 0, 253, $baseY, true, 0, false, true, 0, 'M', true);
//            $pdf->MultiCell(10,$h,"",1,'C',false,0,263, $baseY,true,0,false,true,0,'M',true);
                $pdf->MultiCell(9, $h, "0", 1, 'C', false, 0, 273, $baseY, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(9, $h, "0", 1, 'C', false, 0, 282, $baseY, true, 0, false, true, 0, 'M', true);


                $i++;
                $baseY += $h;

                if (!($i % $pageSize)) {
                    $pdf->AddPage('L', 'A4');
                    $baseY = $baseYFirst;
                    $page++;
                }

                $done[$key] = $item->id;
            }
        }

        /* @var $pdf TCPDF */
        $pdf->Output('KN_'.$data.'.pdf', 'D');



        if($forceMode == self::MODE_COURIER)
        {
            Courier::onAfterAckGenerationGroup($done);
        }
        else if($forceMode == self::MODE_POSTAL)
        {
            Postal::onAfterAckGenerationGroup($done);
        }

        return $done;
    }
}