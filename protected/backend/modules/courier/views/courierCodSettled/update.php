<?php
/* @var $model Courier */
?>

<h1>Update <?= $model->local_id;?> COD</h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div style="overflow: auto;">
    <div style="float: left; width: 49%;">
<h3>Package</h3>
        <table class="table table-striped">
            <tr>
                <td>Package created:</td>
                <td><?= $model->date_entered;?></td>
            </tr>
            <tr>
                <td>Package user:</td>
                <td><?= $model->user->login;?></td>
            </tr>
            <tr>
                <td>COD:</td>
                <td><?= $model->cod_value;?></td>
            </tr>
            <tr>
                <td>Cod Currency:</td>
                <td><?= $model->cod_currency;?></td>
            </tr>
        </table>
    </div>
    <div style="float: right; width: 49%;">
<h3>Settle</h3>
        <table class="table table-striped">
            <tr>
                <td>Settled type:</td>
                <td><?= $model->courierCodSettled ? $model->courierCodSettled->typeName : '-';?></td>
            </tr>
            <tr>
                <td>Settled date:</td>
                <td><?= $model->courierCodSettled ? $model->courierCodSettled->date_entered : '-';?></td>
            </tr>
            <tr>
                <td>Settle date:</td>
                <td><?= $model->courierCodSettled ? $model->courierCodSettled->settle_date : '-';?></td>
            </tr>
        </table>
    </div>
</div>



<?php



if($model->courierCodSettled):
    ?>
<?= CHtml::link('Unsettle', ['/courier/courierCodSettled/unsettle', 'id' => $model->id], ['class' => 'btn btn-danger', 'confirm' => 'Are you sure?']);?>

<?php
else:
    ?>
    <h3>Settle</h3>
    <?php
    /* @var $settleFormModel CourierCodSettledSettleForm */
    /* @var $form GxActiveForm */
    ?>

    <div class="form">
        <?php $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'settle',
            'enableAjaxValidation' => false,
        ));
        ?>

        <?= $form->errorSummary($settleFormModel); ?>
        <table class="table table-striped" style="margin: 0 auto; width: 500px;">
            <tr>
                <td>Settlement type:</td>
                <td style="text-align: center;"><?= $form->dropDownList($settleFormModel, 'type', CourierCodSettled::listType(), ['prompt' => '-']);?></td>
            </tr>
            <tr>
                <td>Settlement date:</td>
                <td style="text-align: center;"><?= $form->dateField($settleFormModel, 'settle_date');?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php
                    echo TbHtml::submitButton('Settle', array('size' => TbHtml::BUTTON_SIZE_LARGE));
                    ?>
                </td>
            </tr>
        </table>

        <?php
        $this->endWidget();
        ?>
    </div>


    <?php
endif;

