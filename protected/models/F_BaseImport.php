<?php

abstract class F_BaseImport extends CFormModel
{
    public $file;
    public $omitFirst;
    public $split;
    public $custom_settings;

    public $cutTooLong;

    public $_customOrder = [];
    public $_customSeparator = false;


    const MAX_LINES = 1000;
    const MAX_PACKAGES = 1000;


    abstract public static function getAttributesList($customOrder = [], $reorderKeys = true);
    abstract public static function getAttributeLength($attribute);
    abstract public static function mapAttributesToModels(array $data, $customOrder = [], $cut = false, $operator_id = false);
    abstract public static function validateModels(array &$models);

    public static function convertCommaToDec($value)
    {
        return str_replace(',', '.', $value);
    }

    /**
     * Function returns array of possible data separators
     * @return array
     */
    public static function listOfSeparators()
    {
        $data = [
            0 => ',',
            1 => ';',
            2 => '-',
            3 => '|',
        ];

        return $data;
    }

    /**
     * Function fetches User custom settings for import file
     *
     * @param null $user_id
     */
    public function applyCustomData($id, $user_id = NULL)
    {
        // for CourierImportManager class
        Yii::app()->getModule('courier');


        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        $model = CourierImportSettings::getModelForUserId($id, $user_id);
        $data = $model->data;

        $this->_customOrder = $data['order'];
        $this->_customSeparator = static::listOfSeparators()[$data['separator']];
    }

    public function rules() {
        return array(
            array('file', 'file',  'allowEmpty' => false,
                'maxSize' => 512400, 'types' => 'csv, xls, xlsx'),

            array('custom_settings', 'numerical', 'integerOnly' => true),
            array('omitFirst, cutTooLong', 'safe'),
            array('split', 'numerical'),
        );
    }

    public function attributeLabels() {
        return array(
            'file' => Yii::t('app', 'File'),
            'omitFirst' => Yii::t('app', 'Omit first line (header)'),
            'split' => Yii::t('app', 'Split after'),
            'custom_settings' => Yii::t('app', 'Custom file settings'),
            'cutTooLong' => Yii::t('app', 'Cut too long values'),
        );
    }

    public static function getAttributesMap($customOrder = [])
    {

        // mapping attributes to columns in file
        $attributes = array_flip(static::getAttributesList($customOrder));

        return $attributes;
    }

    protected static function _distributeTooLongField(&$source, &$target, $attr_source, $attr_target, $cut)
    {
        $lengthSource = static::getAttributeLength($attr_source);

        if(mb_strlen($source) > $lengthSource)
        {
            if($target == '') {
                $target = mb_substr($source, $lengthSource);
                $source = mb_substr($source, 0, $lengthSource);

                if($cut)
                {
                    $lengthTarget = static::getAttributeLength($attr_target);
                    if($lengthTarget)
                        $target = mb_substr($target, 0, $lengthTarget);
                }

            }
            else if($cut)
            {
                $lengthSource = static::getAttributeLength($attr_source);
                if($lengthSource)
                    $source = mb_substr($source, 0, $lengthSource);
            }
        }
    }

    protected static function _getAttrValue($attribute, $mapOfAttributes,$source, $cut, $decimal = false)
    {
        $val = $source[$mapOfAttributes[$attribute]];
        if($cut)
        {
            $lenght = static::getAttributeLength($attribute);
            if($lenght)
                $val = mb_substr($val,0, $lenght);
        }

        if($decimal)
            $val = static::convertCommaToDec($val);

        return $val;
    }
}