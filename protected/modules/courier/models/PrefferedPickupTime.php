<?php

class PrefferedPickupTime
{
    public static function listDates()
    {
        $businnesDays = array();

        $start = 0;

        if(intval(date('G')) >= 16)
            $start = 1;

        $temp = array();
        $temp[0] = S_Useful::workDaysNextDate(date('Y-m-d'), $start);
        $temp[1] = S_Useful::workDaysNextDate(date('Y-m-d'), $start + 1);
        $temp[2] = S_Useful::workDaysNextDate(date('Y-m-d'), $start + 2);

        $businnesDays[0] =
            array(
                'text' => $temp[0],
                'name' => str_replace('-','', $temp[0])
            );
        $businnesDays[1] =
            array(
                'text' => $temp[1],
                'name' => str_replace('-','', $temp[1])
            );
        $businnesDays[2] =
            array(
                'text' => $temp[2],
                'name' => str_replace('-','', $temp[2])
            );

        return $businnesDays;
    }

    public static function listTimes()
    {

        $data = array(
            array(
                'name' => '08001700',
                'text' => '8:00 - 17:00',
            )
        );

        return $data;
    }


    public static function convertTimeNameToText($name)
    {
        $text = substr($name, 0, 2).':'.substr($name,2,2).' - '.substr($name,4,2).':'.substr($name,6,2);
        return $text;
    }

    public static function convertDateNameToText($name)
    {
        $text = substr($name, 0, 4).'-'.substr($name,4,2).'-'.substr($name,6,2);
        return $text;
    }

    public static function convertDateNameToOpenClose($name)
    {
        $open = substr($name, 0, 4);
        $close = substr($name,4,4);

        $data = array(
            'open' => $open,
            'close' => $close,
        );

        return $data;


    }


}
