<?php

/* @var $fieldName String */
/* @var $i Integer */
/* @var $countries Array */
?>
<?php
if(!Yii::app()->user->isGuest)
{
    if(!Yii::app()->request->isAjaxRequest) {
        Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/addressDataSuggester.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/addressDataSuggester.css');
    }
    ?>
    <div class="row">
        <div class="col-xs-12">
            <?php echo TbHtml::label(Yii::t('_addressData','Wybierz ze swoich adresów'),'userAddressList'); ?>
            <?php echo TbHtml::textField('userAddressList'.(isset($i)?'_'.$i:''),'', array(
                'id' => 'userAddressList'.(isset($i)?'_'.$i:''),
                'data-autocomplete-input-field' => 'userAddressList'.(isset($i)?'_'.$i:''),
                'data-autocomplete-id-field' => 'userAddressList-id'.(isset($i)?'_'.$i:''),
                'data-autocomplete-button' => 'addressBookLoad'.(isset($i)?'_'.$i:''),
                'placeholder' => Yii::t('site', 'Wpisz min. 3 znaki z nazwy adresata...'),
                //'data-content' => S_Useful::listDataForAutocomplete(UserContactBook::listContactBookItems($type, $countries), 'id', 'RepresentingName', 'RepresentingNameForSearch'),
                'data-url' => Yii::app()->createUrl('/userContactBook/ajaxSuggestItems'),
                'data-type' => $type,
                'data-countries' => CJSON::encode(UserContactBook::convertCountryListToIds($countries)),
                'data-not-found' => Yii::t('site', 'Nie znaleziono'),
                'style' => 'background: #ffe8a4; text-align: center; font-style: italic;',
                'data-bank-account-notify' => Yii::t('courier', 'Zmieniono numer rachunku bankowego!'),
            ));?>
            <?php echo CHtml::hiddenField('userAddressList-id'.(isset($i)?'_'.$i:''),'', array('id' => 'userAddressList-id'.(isset($i)?'_'.$i:''), 'data-userAddressList-id' => 'true', 'data-i' => $i));?>
            <?php echo TbHtml::button(Yii::t('_addressData','Pobierz'), array('id' => 'addressBookLoad'.(isset($i)?'_'.$i:''), 'data-content' => Yii::t('site','Ładuję...'), 'data-i' => $i, 'data-load-data-from-address-book-field' => $fieldName, 'data-load-url' => Yii::app()->createUrl('/userContactBook/ajaxGetData/'), 'data-input-field' => 'userAddressList'.(isset($i)?'_'.$i:''), 'size' => TbHtml::BUTTON_SIZE_SMALL, 'style' => 'margin-top: -4px;', 'color' => TbHtml::BUTTON_COLOR_INFO));?>
        </div><!-- row -->
    </div><!-- row -->
    <br/>
    <?php
}
?>