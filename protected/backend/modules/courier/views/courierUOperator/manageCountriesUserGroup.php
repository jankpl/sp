<?php
/* @var $modelOperator CourierUOperator */
/* @var $modelUserGroup UserGroup */

?>

<h2>Custom settings for country group</h2>
<h3>Operator: <?= $modelOperator->name;?> (#<?= $modelOperator->id;?>)</h3>
<h3>User Group: <?= $modelUserGroup->name;?> (#<?= $modelUserGroup->id;?>)</h3>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>


<div style="word-break: break-all">
    <?php
    $this->widget('MultipleSelect', array('selector' => '#stat-selector'));
    ?>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
    <?php $this->_getGridViewManageCountriesUserGroup($user_group, $operator_id);?>
    <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
</div>

<h3>Actions for selected</h3>

<?php
$content = [];

echo TbHtml::tabbableTabs(array(
    array('label' => 'Change status', 'active' => true, 'content' => $this->renderPartial('manageCountriesUserGroupActions/_change_status', ['operator_id' => $operator_id, 'user_group' => $user_group], true)),
), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>


<script>
    $(document).ready(function(){
        $('.confirm-me').on('submit', function(){
            return confirm('Are you sure?');
        });
    })
</script>



