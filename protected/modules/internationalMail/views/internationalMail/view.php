<?php

/* @var $model InternationalMail */

/*$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);*/
?>

<h1><?php echo Yii::t('internationalMail','International Mail #{id}', array('{id}' => $model->local_id));?></h1>

<?php echo $actions; ?>

<h3><?php echo Yii::t('internationalMail','Szczegóły');?></h3>
<table class="list hLeft" style="width: 500px;">
    <tr>
        <td><?php echo $model->getAttributeLabel('date_entered');?></td>
        <td><?php echo substr($model->date_entered,0,16);?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('stat');?></td>
        <td><?php echo $model->stat->name;?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('order');?></td>
        <td><?php echo CHtml::link('Zamówienie #'.$model->order->local_id, array('/order/view', 'urlData' => $model->order->hash));?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('mail_weight');?></td>
        <td><?php echo $model->mail_weight;?> <?php echo InternationalMail::getWeightUnit();?></td>
    </tr>
    <tr>
        <td><?php echo Yii::t('internationalMail','Wymiary');?></td>
        <td><?php echo $model->mail_size_d;?><?php echo InternationalMail::getDimensionUnit();?> x <?php echo $model->mail_size_l;?><?php echo InternationalMail::getDimensionUnit();?> x <?php echo $model->mail_size_w;?><?php echo InternationalMail::getDimensionUnit();?></td>
    </tr>
    <tr>
        <td><?php echo $model->getAttributeLabel('mails_number');?></td>
        <td><?php echo $model->mails_number;?></td>
    </tr>
</table>

<h3><?php echo Yii::t('internationalMail','Dodatki');?></h3>
<table class="list hTop" style="width: 500px;">
    <tr>
        <td><?php echo Yii::t('internationalMail','Nazwa');?></td>
        <td><?php echo Yii::t('internationalMail','Opis');?></td>
    </tr>
    <?php

    /* @var $item HybridMailAdditionList */
    if(!S_Useful::sizeof($model->listAdditions()))
        echo '<tr><td colspan="2">'.Yii::t('hybridMail','brak').'</td></tr>';
    else
        foreach($model->listAdditions() AS $item): ?>
            <tr>
                <td><?php echo $item->getClientTitle(); ?></td>
                <td><?php echo $item->getClientDesc(); ?></td>
            </tr>

        <?php endforeach; ?>
</table>

<h3><?php echo Yii::t('internationalMail','Nadawca');?></h3>

<?php

$this->renderPartial('_addressData/_listInTable',
    array('model' => $model->senderAddressData,
    ));

?>

<h3><?php echo Yii::t('internationalMail','Odbiorca');?></h3>
<?php

$this->renderPartial('_addressData/_listInTable',
    array('model' => $model->receiverAddressData,
    ));

?>

<h3><?php echo Yii::t('internationalMail','Historia');?></h3>

<?php
$this->renderPartial('//_statHistory/_statHistory',
    array('model' => $model->internationalMailStatHistoriesVisible));
?>

