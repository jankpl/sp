<?php
/* @var $model Courier_CourierTypeInternal */
/* @var $currency integer */
/* @var $codIngnored integer */
?>

<div class="form">

    <h2><?php echo Yii::t('courier','Podsumowanie');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '2/2')); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Parametry przesyłki');?></h4>
            <table class="detail-view" style="width: 100%; margin: 0 auto;">

                 <tr class="odd">
                    <th><?php echo Yii::t('courier','Package dimensions');?> [<?php echo Courier::getDimensionUnit();?>]</th>
                    <td><?php echo $model->package_size_l.' x '.$model->package_size_d.' x '.$model->package_size_w; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo $model->getAttributeLabel('package_value');?></th>
                    <td><?php echo $model->package_value; ?> <?php echo $model->value_currency; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('package_content');?></th>
                    <td><?php echo $model->package_content; ?></td>
                </tr>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6">




        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Zwrot od');?></h4>

            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));

            ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('courier','Zwrot do');?> </h4>
            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));

            ?>
        </div>

    </div>

    <h3 class="text-center"><?php echo Yii::t('site', 'Regulamin');?></h3>

    <div class="row">
        <table class="detail-view">
            <tr class="odd">
                <td><?php echo $form->labelEx($model->courierTypeReturn,'regulations'); ?>
                    <?php echo $form->checkbox($model->courierTypeReturn, 'regulations'); ?>
                    <?php echo $form->error($model->courierTypeReturn,'regulations'); ?></td>
            </tr>
            <tr class="even">
                <td><?php echo $form->labelEx($model->courierTypeReturn,'regulations_rodo'); ?>
                    <?php echo $form->checkbox($model->courierTypeReturn, 'regulations_rodo'); ?>
                    <?php echo $form->error($model->courierTypeReturn,'regulations_rodo'); ?></td>
            </tr>
        </table>


    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step4', 'class' => 'btn btn-sm'));
        echo ' ';
        if(Yii::app()->user->model->getFastFinalizeInternal() && Yii::app()->user->model->payment_on_invoice)
            echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę'),array('name'=>'finish', 'class' => 'btn btn-success'));
        else
            echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę i idź do kasy'),array('name'=>'finish', 'class' => 'btn btn-success'));
        if(Yii::app()->user->model->getFastFinalizeInternal() && Yii::app()->user->model->payment_on_invoice):
            echo ' ';
            echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę i wróć do formularza'),array('name'=>'finish_goback', 'class' => 'btn-primary'));
        endif;
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p><?php echo Yii::t('courier','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>

    </div>
</div><!-- form -->