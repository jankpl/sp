<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'date_entered'); ?>
		<?php echo $form->textField($model, 'date_entered'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'date_updated'); ?>
		<?php echo $form->textField($model, 'date_updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'hash'); ?>
		<?php echo $form->textField($model, 'hash', array('maxlength' => 64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'paid'); ?>
		<?php echo $form->dropDownList($model, 'paid', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'payment_type_id'); ?>
		<?php echo $form->dropDownList($model, 'payment_type_id', GxHtml::listDataEx(PaymentType::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'order_stat_id'); ?>
		<?php echo $form->dropDownList($model, 'order_stat_id', GxHtml::listDataEx(OrderStat::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

<!--	<div class="row">-->
		<?php //echo $form->label($model, 'product_type_id'); ?>
		<?php //echo $form->dropDownList($model, 'product_type_id', GxHtml::listDataEx(ProductType::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
<!--	</div>-->

<!--	<div class="row">-->
		<?php //echo $form->label($model, 'product_id'); ?>
		<?php //echo $form->textField($model, 'product_id'); ?>
<!--	</div>-->

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
