<h2>Yesterday's orders number</h2>

<?php
$date = new DateTime('yesterday');
?>
<h3><?= $date->format('Y-m-d');?></h3>
<?php

//if($this->beginCache('YESTERDAYS_ORDERS'.$date->format('Y-m-d'), array('duration'=> 60*60))) {

    $dateStart = $date->format('Y-m-d').' 00:00:00';
    $dateEnd = $date->format('Y-m-d').' 23:59:59';

    $cmd = Yii::app()->db->createCommand();
    $cmd->select('login, count(courier.id) AS no,SUM(courier.package_weight) AS weight')->from('courier')->join('user', 'user.id = user_id')->where('courier.date_entered BETWEEN :date_start AND :date_end AND stat_map_id != :map_cancelled', [':date_start' => $dateStart, ':date_end' => $dateEnd, ':map_cancelled' => StatMap::MAP_CANCELLED])->group('login')->order('no DESC, weight DESC, login ASC');

    $result = $cmd->queryAll();

    if($result[0]['login'] === NULL)
        echo 'No orders for yesterday...';
    else {

        echo '<table class="table table-striped table-bordered table-condensed" style="max-width: 500px; margin: 0 auto;">
<tr>
<th style="text-align: left;">Login</th>
<th style="text-align: right;">No of packages</td>
<th style="text-align: right;">Total weight [kg]</td>
</tr>';

        foreach ($result AS $item)
            echo '<tr>
<td style="text-align: left;">' . $item['login'] . '</td>
<td style="text-align: right;">' . $item['no'] . '</td>
<td style="text-align: right;">' . ceil($item['weight']) . '</td>
</tr>';

        echo '</table>';
    }
    ?>
    <?php
//    $this->endCache(); }
?>