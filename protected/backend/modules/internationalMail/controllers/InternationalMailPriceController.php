<?php

class InternationalMailPriceController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }


    /* @parm int id InternationalMail Group Id */
    /* @parm int user_group User Group Id */
    public function actionInternationalMailPrice($id, $user_group = NULL)
    {

        $checkCountryGroup = CountryGroup::model()->findByPk($id);
        if($checkCountryGroup == NULL)
            throw new CHttpException(404);

        if($user_group != NULL)
        {
            $checkUserGroup = UserGroup::model()->findByPk($user_group);
            if($checkUserGroup == NULL)
                throw new CHttpException(404);
        }

        $groupEditURLs = [];
        foreach(UserGroup::model()->findAll() AS $item)
        {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('*');
            $cmd->from('international_mail_price_has_group');
            $cmd->where('country_group = :group', array(':group' => $id));
            $cmd->andWhere('user_group_id = :user_group', array(':user_group' => $item->id));
            $result = $cmd->queryRow();

            $temp = [];
            $temp['name'] = $item->name.' ('.($result?'yes':'no').')';
            $temp['url'] = Yii::app()->createAbsoluteUrl('/internationalMail/internationalMailPrice/internationalMailPrice', array('id' => $id, 'user_group' => $item->id));
            array_push($groupEditURLs, $temp);
        }

        if($user_group == NULL)
        {
            $groupActionDelete = true;
            $groupActionListView = $this->renderPartial('_groupActionList', array(
                    'groupEditUrls' => $groupEditURLs
                ),
                true);
        } else {
            $groupActionDelete = true;

        }


        $this->updatePrice($_POST, $id, $user_group, $groupActionListView,$groupActionDelete);



    }

    protected function updatePrice($post, $country_group, $user_group, $groupActionListView = NULL, $groupActionDelete = false)
    {


        /* @var $model InternationalMailPrice*/
        $model = InternationalMailPrice::loadOrCreatePrice($country_group, $user_group);
        if($model->id == NULL)
            $groupActionDelete = null;


        $internationalMailPriceValues = $model->loadValueItems();

        $this->performAjaxValidation($model, 'international-mail-price-form');

        if (isset($post['InternationalMailPrice']) OR isset($post['add_more']) OR isset($post['sort'])) {


            // Clicked button for just sort
            if(isset($post['delete']))
            {
                if($model->deleteWithRelated())
                {
                    Yii::app()->user->setFlash('success','Pricing was deleted!');
                } else {
                    Yii::app()->user->setFlash('error','Pricing was  NOT deleted!');
                }
                $this->redirect(array('internationalMailPrice/view','id' => $country_group));
                return;
            }

            $internationalMailPriceValues = Array();

            $model->setAttributes($post['InternationalMailPrice']);

            $transaction = Yii::app()->db->beginTransaction();
            $error = false;

            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('international_mail_price_value', 'international_mail_price_id = :international_mail_price_id',
                array(':international_mail_price_id' => $model->id));

            function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
                $sort_col = array();
                foreach ($arr as $key=> $row) {
                    $sort_col[$key] = $row[$col];
                }

                array_multisort($sort_col, $dir, $arr);
            }

            $dataArray = [];
            foreach($post['InternationalMailPriceValue'] AS $key => $item)
                $dataArray[$key] = array_merge($post['InternationalMailPriceValue'][$key], $post['PriceValue'][$key]);


//            array_sort_by_column($post['InternationalMailPriceValue'], 'weight_top_limit');
            array_sort_by_column($dataArray, 'weight_top_limit');


            if($model->id == NULL)
            {
                $model->save(true,'id');


                    $model2 = new InternationalMailPriceHasGroup();
                    $model2->country_group = $country_group;
                    $model2->international_mail_price_item = $model->id;
                    $model2->user_group_id = $user_group;
                    if(!$model2->save())
                        $errors = true;


            }

            $maxWeightFound = false;
            foreach($dataArray AS $key => $item)
            {

                $internationalMailPriceValue = new InternationalMailPriceValue;
                $internationalMailPriceValue->weight_top_limit = $item['weight_top_limit'];
                $internationalMailPriceValue->pricePerItem = Price::generateByPost($item['pricePerItem']);
                $internationalMailPriceValue->pricePerWeight = Price::generateByPost($item['pricePerWeight']);

                $internationalMailPriceValue->price_per_item = $internationalMailPriceValue->pricePerItem->saveWithPrices();
                $internationalMailPriceValue->price_per_weight = $internationalMailPriceValue->pricePerWeight->saveWithPrices();

                $internationalMailPriceValue->international_mail_price_id = $model->id;

                $internationalMailPriceValues[$key] = $internationalMailPriceValue;

                if(!$internationalMailPriceValues[$key]->save())
                    $error = true;

                if($internationalMailPriceValues[$key]->weight_top_limit == InternationalMailPrice::getMaxWeight()) $maxWeightFound = true;
            }

            // Clicked button for more prices
            if(isset($post['add_more']))
            {
                array_unshift($internationalMailPriceValues, new InternationalMailPriceValue);
                $error = true;
            }

            // Clicked button for just sort
            if(isset($post['sort']))
            {
                $error = true;
            }

            if(!$maxWeightFound)
            {
                $error = true;

                $maxPriceModel = $model->generateDefaultPriceValue();

                array_push($internationalMailPriceValues, $maxPriceModel);
            }

            if(!$error AND $model->save())
            {
                $transaction->commit();
                $this->redirect(array('internationalMailPrice/view','id' => $country_group));
            }
            $transaction->rollback();
        }

        $this->render('update', array(
            'model' => $model,
            'internationalMailPriceValues' =>  $internationalMailPriceValues,
            'groupActionListView' => $groupActionListView,
            'groupActionDelete' => $groupActionDelete,
            'userGroup' => $user_group,
            'countryGroup' => $country_group,
        ));

    }

    public function actionView($id) {

        $model = CountryGroup::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $this->render('view',array(
            'model' => $model,
        ));

    }

    public function actionAdmin() {
        $model = new CountryGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['CountryGroup']))
            $model->setAttributes($_GET['CountryGroup']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }
}