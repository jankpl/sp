<?php

class GlobalOperatorResponse
{
    protected $success;
    protected $label; // raw PNG string
    protected $tt_number;
    protected $errorLog = false;

    protected $ackType;
    protected $ack;

    protected $collectionMethodName = false;
    protected $collectionObjName = false;

    protected $customTrackingId = false;

    public function getCollectionMethodName()
    {
        return $this->collectionMethodName;
    }

    public function getCollectionObjName()
    {
        return $this->collectionObjName;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getLabelsNo()
    {
        return is_array($this->label) ? S_Useful::sizeof($this->label) : 1;
    }

    public function getTt()
    {
        return $this->tt_number;
    }

    public function isSuccess()
    {
        return $this->success;
    }

    public function getError()
    {
        return $this->errorLog;
    }

    public function getAck()
    {
        return $this->ack;
    }

    public function getAckType()
    {
        return $this->ackType;
    }

    public function getCustomTrackingId()
    {
        return $this->customTrackingId;
    }

    public static function createSuccessResponse($label, $tt_number, $ackType = false, $ack = false, $collectionObjName = false, $collectionMethodName = false, $customTrackingId = false)
    {
        $model = new self;
        $model->success = true;
        $model->label = $label;
        $model->tt_number = $tt_number;

        $model->ackType = $ackType;
        $model->ack = $ack;

        $model->collectionObjName = $collectionObjName;
        $model->collectionMethodName = $collectionMethodName;

        $model->customTrackingId = $customTrackingId;

        return $model;
    }

    public static function createErrorResponse($errorLog)
    {
        $model = new self;
        $model->success = false;
        $model->label = false;
        $model->tt_number = false;
        $model->errorLog = $errorLog;

        $return = [ $model ];

        return $return;
    }
}