<h3><?= Yii::t('linkerManager', 'Dodaj nową integrację');?></h3>

<?php
$this->widget('ShowPageContent', array('id' => 76));
?>
<br/>
<table class="table table-striped table-bordered table-condensed" style="max-width: 500px;">
    <tr>
        <th style="width: 100px;"></th>
        <th><?= Yii::t('linkerManager', 'Adapter');?></th>
        <th><?= Yii::t('linkerManager', 'Akcja');?></th>
    </tr>
    <?php
    $integrations = $lm->listIntegrations();
    if(!S_Useful::sizeof($integrations)):
        ?>
        <tr>
            <td colspan="3" class="text-center"><?= Yii::t('linkerManager', '-- brak --');?></td>
        </tr>
    <?php
    else:
        $i = 1;
        foreach($integrations AS $item):
            ?>
            <tr>
                <td><img src="data:image/png;base64,<?= $item->image;?>" style="height: 30px; float: right;"/></td>
                <td style="vertical-align: middle; font-weight: bold; font-size: 1em;"><?= $item->name;?></td>
                <td><a href="<?= Yii::app()->urlManager->createUrl('/linker/add', ['adapter' => $item->name]);?>" class="btn btn-primary btn-sm"><?= Yii::t('linkerManager', 'Dodaj &gt; &gt;');?></a></td>
            </tr>
            <?php
            $i++;
        endforeach;
    endif;
    ?>
</table>