<?php
/* @var $this CourierController */
/* @var $preModels [] */
?>

<?php

if(!Yii::app()->NPS->isNewPanelSet())
    $this->renderPartial('index');
?>

    <h2 id="list"><?php echo Yii::t('site','Your packages');?></h2>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    if($key == 'top-info')
    {
        Yii::app()->user->setFlash('top-info', $message);
        continue;
    }
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

    <div class="panel panel-info actions-for-selected">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('courier', 'Akcje dla zaznaczonych paczek');?> (<span class="selected-no">0</span>)</h3>
        </div>
        <div class="panel-body" style="position: relative;">
            <div class="too-few-curain" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; background: rgba(255,255,255,0.9); z-index: 50; padding: 10px 25px;">
                <div class="alert alert-danger too-much-alert hidden"><?= Yii::t('courier', 'Wybrano zbyt dużo pozycji na raz!');?></div>
            </div>
            <div class="selected-actions">
                <?php echo TbHtml::tabbableTabs(array(
                    array('id' => 'tab_bis_1', 'label' => Yii::t('courier','Generate labels'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_labels', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_2', 'label' => Yii::t('courier','Generate ACK cards'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_ack_cards', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_3', 'label' => Yii::t('courier','Generate status cards'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_stat_conf', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_4', 'label' => Yii::t('courier','Cancel packages'), 'content' => $this->renderPartial('gridview_actions_tabs/_user_cancel', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_5', 'label' => Yii::t('courier','Export to .XLS'), 'content' => $this->renderPartial('gridview_actions_tabs/_export', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_6','label' => Yii::t('courier','Generate KsiążkaNadawcza'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_kn', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_7','label' => Yii::t('courier','Generate CN23'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_cn23', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_8','label' => Yii::t('courier','Generate CN22'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_cn22', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_9', 'label' => Yii::t('courier','Generate invoice'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_invoices', array('id_prefix' => 'bis'), true)),
                    array('id' => 'tab_bis_10', 'label' => Yii::t('courier','Dokumenty celne'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_customs', array('id_prefix' => 'bis'), true)),
                ), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>
            </div>
        </div>
    </div>

    <hr/>

    <fieldset>
        <legend><?= Yii::t('courier', 'Prefilter items');?></legend>
        <?php $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'export-to-file',
            'enableAjaxValidation' => false,
//    'action' => Yii::app()->createUrl('/courier/courier/searchByRemote'),
        ));
        ?>

        <div class="overflow: auto;">
            <div style="width: 48%; float: left; text-align: right; vertical-align: top;"><?= Chtml::textArea('preFilter', $preFilter, ['placeholder' => '123456789ABC1'.PHP_EOL.'123456789ABC2'.PHP_EOL.'123456789ABC3', 'style' => 'height: 100px; width: 100%;']); ?></div>
            <div style="width: 48%; float: right; text-align: left; vertical-align: top;">
                <?= Yii::t('courier', 'To pre-filter list, provide local ids, remote ids or refs. Each one in new line.');?>
                <br/>
                <br/>
                <?= TbHtml::submitButton(Yii::t('courier', 'Prefilter')); ?>
                <br/>
                <?php
                if($preFilter !=''):
                    ?>
                    <?= TbHtml::submitButton('Clear', ['name' => 'preFilterClear', 'style' => 'margin-top: 10px;']); ?>
                <?php
                endif;
                ?>
            </div>
        </div>




        <?php $this->endWidget(); ?>
    </fieldset>
    <hr/>
    <div class="gridview-container">
        <div class="gridview-overlay"></div>
        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;"><?= Yii::t('site', 'Zaznaczonych pozycji:');?> <span class="selected-no" style="font-weight: bold;">0</span></div>
        <?= Yii::t('courier', 'Nie pokazuj przesyłek:');?><br/>
        <input type="checkbox" name="hideTypeReturn" id="hideTypeReturn" <?= (isset(Yii::app()->request->cookies['hideTypeReturn']) && Yii::app()->request->cookies['hideTypeReturn']->value > 0) ? 'checked' : '';?>> <label for="hideTypeReturn" style="text-align: left;"><?= Yii::t('courier', 'typu: {typeReturn}, {typeExternalReturn}', ['{typeReturn}' => Courier::getTypesNames()[Courier::TYPE_RETURN], '{typeExternalReturn}' => Courier::getTypesNames()[Courier::TYPE_EXTERNAL_RETURN]]);?></label><br/>
        <input type="checkbox" name="hideCancelled" id="hideCancelled" <?= (isset(Yii::app()->request->cookies['hideCancelled']) && Yii::app()->request->cookies['hideCancelled']->value > 0) ? 'checked' : '';?>> <label for="hideCancelled" style="text-align: left;"><?= Yii::t('courier', 'anulowanych');?></label>
        <?php
        $this->_getGridViewCouriergrid($preModels);

        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        //        Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css');
        //        if(Yii::app()->getLanguage() == 'pl')
        //            Yii::app()->clientScript->registerScriptFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/i18n/ui.datepicker-pl.js');
        //
        //        $script = "$( function() {
        //
        //         $('[name=\"CourierGridViewUser[date_entered]\"], [name=\"CourierGridViewUser[date_updated]\"]').datepicker({
        //                dateFormat : 'yy-mm-dd',
        //                minDate: \"-5Y\",
        //                maxDate: \"+0M\",
        //                changeMonth: true,
        //                changeYear: true,
        //            });
        //
        //        } );";
        //        Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);
        ?>
        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;"><?= Yii::t('site', 'Zaznaczonych pozycji:');?> <span class="selected-no" style="font-weight: bold;">0</span></div>
    </div>

    <hr/>

    <div class="panel panel-info actions-for-selected">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('courier', 'Akcje dla zaznaczonych paczek');?> (<span class="selected-no">0</span>)</h3>
        </div>
        <div class="panel-body" style="position: relative;">
            <div class="too-few-curain" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; background: rgba(255,255,255,0.9); z-index: 50; padding: 10px 25px;">
                <div class="alert alert-danger too-much-alert hidden"><?= Yii::t('courier', 'Wybrano zbyt dużo pozycji na raz!');?></div>
            </div>
            <div class="selected-actions">
                <?php echo TbHtml::tabbableTabs(array(
                    array('label' => Yii::t('courier','Generate labels'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_labels', array(), true)),
                    array('label' => Yii::t('courier','Generate ACK cards'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_ack_cards', array(), true)),
                    array('label' => Yii::t('courier','Generate status cards'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_stat_conf', array(), true)),
                    array('label' => Yii::t('courier','Cancel packages'), 'content' => $this->renderPartial('gridview_actions_tabs/_user_cancel', array(), true)),
                    array('label' => Yii::t('courier','Export to .XLS'), 'content' => $this->renderPartial('gridview_actions_tabs/_export', array(), true)),
                    array('label' => Yii::t('courier','Generate KsiążkaNadawcza'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_kn', array(), true)),
                    array('label' => Yii::t('courier','Generate CN23'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_cn23', array(), true)),
                    array('label' => Yii::t('courier','Generate CN22'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_cn22', array(), true)),
                    array('label' => Yii::t('courier','Generate invoice'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_invoices', array(), true)),
                    array('label' => Yii::t('courier','Dokumenty celne'), 'content' => $this->renderPartial('gridview_actions_tabs/_generate_customs', array(), true)),
                ), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>
            </div>
        </div>
    </div>



    <script>
        // $('.grid-view tbody tr').unbind('click');
    </script>

<?php $this->widget('BackLink'); ?>

<?php
$script = 'function checkNumberOfChecks(id)
    {
        var number = $("#couriergrid").selGridViewNoGet("getAllSelection").length;

        $(".selected-no").html(number);
        if(number == 0)
        {
             $(".too-few-curain").show();
             $(".too-much-alert").addClass("hidden");
        }
        else if(number > 500)
        {
            //bootbox.alert("'.Yii::t('courier', 'Wybrano zbyt dużo pozycji na raz!').' Max: 500");
            $(".too-few-curain").show();
            $(".too-much-alert").removeClass("hidden");
        } else {
            $(".too-few-curain").hide();
            $(".too-much-alert").addClass("hidden");
        }
    }';
Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-bootbox.min.js');
Yii::app()->clientScript->registerScript('cnoc', $script);

$script = "
$('.gridview-container').doubleScroll({contentElement: '.items', resetOnWindowResize: true});
";
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.doubleScroll.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('ds',$script, CClientScript::POS_READY);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/daterangepicker/moment.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/daterangepicker/daterangepicker.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/daterangepicker/daterangepicker.css');
$script = '
setDataRange($("input[name=\"CourierGridViewUser[date_entered]\"]", false, false));
setDataRange($("input[name=\"CourierGridViewUser[date_updated]\"]", false, false));

function setDataRange(input, start, end)
{

var begin = moment("2014-01-01");
var finish = moment();

if(start)
    start = moment(start);
else
    start = begin;

if(end)
    end = moment(end);
else
    end = finish;

input.daterangepicker({
    startDate: start,
    endDate: end,
    autoApply: true,
    autoUpdateInput: false,
    showDropdowns: true,
    ranges: {
        "'.Yii::t('panel', 'Dzisiaj').'": [
            moment(),
            moment(),
        ],
        "'.Yii::t('panel', 'Wczoraj').'": [
           moment().subtract(1, "day"),
           moment().subtract(1, "day")
        ],
        "'.Yii::t('panel', 'Ostatnie 7 dni').'": [
           moment().subtract(7, "day"),
           moment()
        ],
        "'.Yii::t('panel', 'Ostatnie 3 dni').'": [
           moment().subtract(3, "day"),
           moment()
        ],
        "'.Yii::t('panel', 'Ten miesiąc').'": [
           moment().startOf("month"),
           moment()
        ],
        "'.Yii::t('panel', 'Poprzedni miesiąc').'": [
           moment().subtract(1, "month").startOf("month"),
           moment().subtract(1, "month").endOf("month")
        ],
         "'.Yii::t('panel', 'Cały okres').'": [
         begin,
         finish    
        ]
    },
    locale: {
        format: "MM/DD/YYYY",
        separator: " - ",
        applyLabel: "Ok",
        cancelLabel: "'.Yii::t('panel', 'Anuluj').'",
        fromLabel: "'.Yii::t('panel', 'Od').'",
        toLabel: "'.Yii::t('panel', 'Do').'",
        customRangeLabel: "'.Yii::t('panel', 'Niestandardowe').'",
        weekLabel: "T",
        daysOfWeek: [
            "'.Yii::t('panel', 'Nd').'",
            "'.Yii::t('panel', 'Pn').'",
            "'.Yii::t('panel', 'Wt').'",
            "'.Yii::t('panel', 'Śr').'",
            "'.Yii::t('panel', 'Czw').'",
            "'.Yii::t('panel', 'Pt').'",
            "'.Yii::t('panel', 'Sb').'"
      
        ],
        monthNames: [
            "'.Yii::t('panel', 'Styczeń').'",
            "'.Yii::t('panel', 'Luty').'",
            "'.Yii::t('panel', 'Marzec').'",
            "'.Yii::t('panel', 'Kwiecień').'",
            "'.Yii::t('panel', 'Maj').'",
            "'.Yii::t('panel', 'Czerwiec').'",
            "'.Yii::t('panel', 'Lipiec').'",
            "'.Yii::t('panel', 'Sierpień').'",
            "'.Yii::t('panel', 'Wrzesień').'",
            "'.Yii::t('panel', 'Październik').'",
            "'.Yii::t('panel', 'Listopad').'",
            "'.Yii::t('panel', 'Grudzień').'"
        ],
        firstDay: 1
    },
    showCustomRangeLabel: true,
    alwaysShowCalendars: false,  
    maxDate: finish,
    minDate: begin,
    opens: "center",
    drops: "up"
}, function(start, end, label) {
if(start.isValid() && end.isValid)
   input.val(start.format("YYYY-MM-DD") + "|" + end.format("YYYY-MM-DD")).trigger("change");
   else
      input.val("").trigger("change");

});

}';


Yii::app()->clientScript->registerScript('daterangepicker', $script, CClientScript::POS_READY);

$script = '$("#hideTypeReturn").on("change", function(){

        var $filter = $("input[name=\'CourierGridViewUser[_hideTypeReturn]\']");

        if($(this).is(":checked"))
        {
            $filter.val(1);
            document.cookie="hideTypeReturn=10; expires='.(new DateTime())->add(new DateInterval('P1Y'))->format('D, j M Y,h:i:s').' UTC; path=/";            
        }
        else
        {
            $filter.val("");
            document.cookie="hideTypeReturn=NULL; expires='.(new DateTime())->add(new DateInterval('P1Y'))->format('D, j M Y,h:i:s').' UTC; path=/";
        }

        $filter.trigger("change");
    });';

$script .= '$("#hideCancelled").on("change", function(){

        var $filter = $("input[name=\'CourierGridViewUser[_hideCancelled]\']");

        if($(this).is(":checked"))
        {
            $filter.val(1);
            document.cookie="hideCancelled=10; expires='.(new DateTime())->add(new DateInterval('P1Y'))->format('D, j M Y,h:i:s').' UTC; path=/";            
        }
        else
        {
            $filter.val("");
            document.cookie="hideCancelled=NULL; expires='.(new DateTime())->add(new DateInterval('P1Y'))->format('D, j M Y,h:i:s').' UTC; path=/";
        }
        $filter.trigger("change");
    });';


Yii::app()->clientScript->registerScript('hideTypeReturn', $script, CClientScript::POS_READY);

?>