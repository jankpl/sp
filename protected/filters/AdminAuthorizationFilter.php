<?php

class AdminAuthorizationFilter extends CFilter
{

    public $level;

    protected function preFilter($fc)
    {
        if((int) Yii::app()->user->getState('authority') > $this->level)
        {
            throw new CHttpException(403,
                'You are not allowed to do this.');
        }
        else
        {
            $fc->run();
        }
    }

}