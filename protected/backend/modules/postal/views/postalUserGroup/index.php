<?php

?>

<h1><?php echo GxHtml::encode(PostalUserGroup::label(2)); ?></h1>


<table class="list hTop" style="width: 500px; margin: 0 auto;">
    <tr>
        <td>#</td>
        <td>Name</td>
        <td>Admin</td>
        <td>Members</td>
        <td>Action</td>
    </tr>

<?php /* @var $item PostalUserGroup */ ?>
<?php foreach($models AS $item):?>

    <tr>
        <td><?php echo $item->id; ?></td>
        <td><?php echo $item->name; ?></td>
        <td><?php echo $item->admin_id ? $item->admin->login : '-'; ?></td>
        <td><?php echo $item->howManyMembers(); ?></td>
        <td><?php echo CHtml::link('details',Yii::app()->createUrl('/postal/postalUserGroup/view', array('id' => $item->id)), array('class' => 'likeButton'));?> <?php echo CHtml::link('edit',Yii::app()->createUrl('/postal/postalUserGroup/update', array('id' => $item->id)), array('class' => 'likeButton'));?></td>
    </tr>

<?php endforeach; ?>


</table>
<br/>
<div style="text-align: center;">
<?php echo CHtml::link('Add group',Yii::app()->createUrl('/postal/postalUserGroup/create'), array('class' => 'likeButton'));?>
</div>