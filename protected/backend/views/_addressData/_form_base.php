<?php

$array = [];

?>


    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'name'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'name', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'name'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'company'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'company', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'company'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'country'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'country', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'country'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'zip_code', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'city'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'city', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'city'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_1', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_2', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'tel'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'tel', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'tel'); ?>
    </div><!-- row -->
<?php
if(!$noEmail):
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'email'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'email', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'email'); ?>
    </div><!-- row -->
<?php
endif;
?>