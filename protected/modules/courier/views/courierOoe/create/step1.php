<?php
/* @var $model Courier */
/* @var $form TbActiveForm */
/* @var $additionsHtml string */
/* @var $noPickup boolean */
/* @var $noCarriage boolean */
?>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  checkIfAvailable: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierOoe/ajaxCheckIfAvailable')).',
                  serviceDescription: '.CJSON::encode(Yii::app()->createUrl('/courier/courierOoe/ajaxGetServiceDescription')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).'
              },
               loader: {
                  textTop: '.CJSON::encode(Yii::t('site', 'Trwa ładowanie...')).',
                  textBottom: '.CJSON::encode(Yii::t('site', 'Proszę czekać...')).',
                  error:  '.CJSON::encode(Yii::t('site', 'Wystąpił błąd, ale system spróbuje automatycznie przetworzyć formularz. Następnie proszę spróbować kontynuować. Jeżeli błąd będzie się powtarzał, prosimy o kontakt.')).',
              },
          };
      ',CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.bootstrap-touchspin.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courierOoeForm.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.bootstrap-touchspin.min.css');
?>

<h2><?php echo Yii::t('courier', 'Create Courier'); ?> | <?= Yii::t('app','Krok {step}', array('{step}' => '1/2')); ?></h2>
<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-ooe-form',
        'enableClientValidation' => true,
        'stateful'=>true,
//        'htmlOptions' => [
//            'class' => 'form-horizontal',
//        ],
    ));
    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Service');?></legend>
                <div class="row text-center">
                    <?php echo $form->dropDownList($model->courierTypeOoe, 'courier_ooe_service_id', $model->courierTypeOoe->getServiceList(), array('data-service-code' => 'true', 'prompt' => Yii::t('courier', 'Select service'), 'style' => 'width: 350px; text-align: center;', 'data-dependency' => 'true', 'id' => 'operator')); ?> <?php echo $form->error($model->courierTypeOoe,'courier_ooe_service_id'); ?>
                </div><!-- row -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-info" id="service-description">
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row" id="palletways">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Parametry paczki - paleta');?></legend>
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <?php echo $form->labelEx($model->courierTypeOoe,'_palletway_size'); ?>
                        <?php echo $form->dropDownList($model->courierTypeOoe, '_palletway_size', CourierTypeOoe::getPalletwayTypes()); ?>
                        <?php echo $form->error($model->courierTypeOoe,'_palletway_size'); ?>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Parametry paczki');?></legend>
                <div class="row">
                    <div class="col-xs-12 text-center"><strong><?= Yii::t('courier', 'Waga');?> [<?php echo Courier::getWeightUnit();?>]</strong></div>
                    <div class="col-xs-12 col-sm-2 col-xs-offset-5 text-center">  <?php echo $form->textField($model, 'package_weight', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-weight' => true, 'data-package-weight' => true, 'data-max' => S_PackageMaxDimensions::getPackageMaxDimensions()['weight'])); ?><?= Yii::t('courier', 'Pojedyncza paczka');?></div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                        <?php echo $form->error($model,'package_weight', ['class' => 'help-inline']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center"><strong><?= Yii::t('courier', 'Wymiary');?> [<?php echo Courier::getDimensionUnit();?>]</strong></div>
                    <div class="col-xs-12 col-sm-2 text-center col-sm-offset-3 "><?php echo $form->textField($model, 'package_size_l', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => 'true')); ?><?= Yii::t('courier', 'długość');?></div>
                    <div class="col-xs-12 col-sm-2 text-center"><?php echo $form->textField($model, 'package_size_w', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => true)); ?><?= Yii::t('courier', 'szerokość');?></div>
                    <div class="col-xs-12 col-sm-2 text-center"><?php echo $form->textField($model, 'package_size_d', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => true)); ?><?= Yii::t('courier', 'wysokość');?></div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3 ">
                        <?php echo $form->error($model,'package_size_l'); ?>
                        <?php echo $form->error($model,'package_size_w'); ?>
                        <?php echo $form->error($model,'package_size_d'); ?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <?php
        $this->Widget('AddressDataSwitch', ['attrBaseName' => 'CourierTypeOoe_AddressData']);
        ?>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('courier', 'Nadawca');?></legend>
                <?php
                $this->renderPartial('_addressData_create/_form',
                    array(
                        'model' => $model->senderAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_SENDER),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => UserContactBook::TYPE_SENDER,
                        'type' => UserContactBook::TYPE_SENDER,
//                    'focusFirst' => true,
                    ));
                ?>
            </fieldset>
        </div>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('courier', 'Odbiorca');?></legend>
                <?php
                $this->renderPartial('_addressData_create/_form',
                    array(
                        'model' => $model->receiverAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_RECEIVER),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => UserContactBook::TYPE_RECEIVER,
                        'type' => UserContactBook::TYPE_RECEIVER,
//                    'focusFirst' => true,
                    ));
                ?>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Package content');?></legend>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'package_content'); ?>
                        <?php echo $form->textField($model, 'package_content'); ?>
                        <?php echo $form->error($model,'package_content'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'package_value'); ?>
                        <?php echo $form->textField($model, 'package_value', ['style' => 'border-top-right-radius: 0; border-bottom-right-radius: 0;']); ?>
                        <div style="display: inline-block; background: #eeeeee; border: 1px solid #cccccc; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height: 28px; line-height: 26px; padding: 0 5px; margin-left: -5px;"><?= Currency::PLN;?></div>
                        <?php echo $form->error($model,'package_value'); ?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row" style="<?= ($noCarriage OR $noPickup) ? 'display: none;' : '';?>">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Dodatki');?></legend>
                <div id="additions-placeholder">
                    <?= $additionsHtml; ?>
                </div>
            </fieldset>
        </div>
    </div>
    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>
    <div class="row" style="padding: 10px 0;">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Kontynuuj');?></legend>
                <div data-not-available="true" style="<?= ($noCarriage) ? '' : 'display: none;';?>">
                    <div class="alert alert-warning text-center">
                        <?= Yii::t('courier', 'Wybrany operator nie obsługuje tego państwa odbiorcy.');?>
                    </div>
                </div>
                <div class="text-center">
                    <?php echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('name' => 'summary', 'class' => 'btn-lg btn-primary', 'disabled' => ($noCarriage OR $noPickup) ? 'disabled' : '', 'data-continue-button' => 'true')); ?>
                </div>
            </fieldset>
        </div>
    </div>
    <?= $form->hiddenField($model, 'source'); // for cloning ?>
    <?php $this->endWidget(); ?>
</div>