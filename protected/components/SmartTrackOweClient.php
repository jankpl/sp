<?php

class SmartTrackOweClient extends GlobalOperator
{

//    const URL = 'http://developer.smarttrack.co/api';
    const URL = 'https://www.smarttrack.co/api';
    const API_KEY = 'ce7e6aeae9d669575e03bb5f5af97bb6';
    const API_SECRET = 'ef38b7daa59d25c595aa7c6ae1af42f2';


    const SERVICE_ROYALMAIL_TRACKED48 = 'STRYM0TP2';
    const SERVICE_YODEL_MINIPACK24 = 'STYDL01CN';
    const SERVICE_YODEL_HOME24POD = 'STYDL01HS';
    const SERVICE_YODEL_MINIPACK48 = 'STYDL2CXN';
    const SERVICE_YODEL_HOME24 = 'STYDL001H';
    const SERVICE_UPS_DROPOFF = 'STUPSDROP';
    const SERVICE_HUXLOE_HERMES_DROPOFF = 'STHUXRTDR';
    const SERVICE_HUXLOE_HUX_STANDARD = 'STHUXSTND';

    public $_service;

    public $label_annotation_text = false;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_tel;
    public $receiver_mail;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_tel;
    public $sender_mail;

    public $package_weight;
    public $package_size_l;
    public $package_size_d;
    public $package_size_w;

    public $package_content;
    public $package_value;
    public $package_value_currency;
    public $ref;

    public $packages_number = 1;

    protected function setService($operator_uniq_id)
    {
        switch($operator_uniq_id)
        {
            case GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_HUXLOE_HERMES_DROPOFF:
                $this->_service = self::SERVICE_HUXLOE_HERMES_DROPOFF;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_HUXLOE_HUX_STANDARD:
                $this->_service = self::SERVICE_HUXLOE_HUX_STANDARD;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_ROYALMAIL_TRACKED48:
                $this->_service = self::SERVICE_ROYALMAIL_TRACKED48;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_UPS_DROPOFF:
                $this->_service = self::SERVICE_UPS_DROPOFF;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_YODEL_HOME24:
                $this->_service = self::SERVICE_YODEL_HOME24;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_YODEL_HOME24POD:
                $this->_service = self::SERVICE_YODEL_HOME24POD;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_YODEL_MINIPACK24:
                $this->_service = self::SERVICE_YODEL_MINIPACK24;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_SMARTTRACK_OWE_YODEL_MINIPACK48:
                $this->_service = self::SERVICE_YODEL_MINIPACK48;
                break;
            default:
                throw new Exception('Unknown SmartTrack service!');
                break;
        }

    }

//    public static function test($returnError = true)
//    {
//        $model = new self;
//
//        $resp = $model->_call('get-services', new stdClass());
//
//        var_dump($resp);
//        exit;
//
//        $data = new stdClass();
//        $data->sender_country_iso = 'GB';
//        $data->service_code = 'STUPSDROP';
//        $data->order_reference = 'ABV 131'.rand(999,99999);
//        $data->sender_contact = 'Jan Nowak';
//        $data->sender_email = 'test@testesttest.pl';
//        $data->sender_company = 'Test Ins';
//        $data->sender_name = 'Jan Nowak';
//        $data->sender_address_line_1 = 'Testowa 1';
//        $data->sender_address_line_2 = '';
//        $data->sender_address_line_3 = '';
//        $data->sender_city = 'Warszawa';
//        $data->sender_state = '';
//        $data->sender_postcode = '00-001';
//        $data->sender_telephone = '11222333';
//        $data->receiver_country_iso = 'GB';
//        $data->receiver_contact = 'Jan Nowak';
//        $data->receiver_email = 'test@testesttest.pl';
//        $data->receiver_company = 'Test Ins';
//        $data->receiver_name = 'Jan Nowak';
//        $data->receiver_address_line_1 = 'Testowa 1';
//        $data->receiver_address_line_2 = '';
//        $data->receiver_address_line_3 = '';
//        $data->receiver_city = 'Warszawa';
//        $data->receiver_state = '';
//        $data->receiver_postcode = '00-001';
//        $data->receiver_telephone = '11222333';
//        $data->fullpallets = 0;
//        $data->halfpallets = 0;
//        $data->qtrpallets = 0;
//        $data->palletlifts = 0;
//        $data->value = '100';
//        $data->currency = 'EUR';
//        $data->item_type = '';
//        $data->notes = 'Test note';
//        $data->description = 'Test item';
//        $data->parcel = [];
//
//        $parcel = new stdClass();
//        $parcel->weight = 10;
//        $parcel->height = 10;
//        $parcel->width = 10;
//        $parcel->length = 10;
//
//        $data->parcel[] = $parcel;
//        $data->parcel[] = $parcel;
//
//        $data->label_type = 'pdf';
//
//        $resp = $model->_call('generate-label', $data);
//
//        $return = [];
//        if($resp->status_code == 200)
//        {
//
//            $item = $resp->_embedded->LabelResponse[0];
//
//            if($item->status == 'error')
//            {
//                if ($returnError == true) {
//                    return GlobalOperatorResponse::createErrorResponse(print_r($item->errors,1));
//                } else {
//                    return false;
//                }
//            }
//
//            $im = ImagickMine::newInstance();
//            $im->setResolution(300, 300);
//
//            $label = (string) $item->Label_bin_str;
//            $label = base64_decode($label);
//
//            $basePath = Yii::app()->basePath . '/../';
//            $dir = 'uplabels/';
//            $temp = $dir . 'temp/';
//
//            @file_put_contents($basePath . $temp . $id . '.pdf', $label);
//
//            for ($i = 0; $i < 2; $i++) {
//                $im->readImage($basePath . $temp . $id . '.pdf['.$i.']');
//                $im->setImageFormat('png8');
//                if ($im->getImageAlphaChannel()) {
//                    $im->setImageBackgroundColor('white');
//                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
//                }
//
//                if ($im->getImageWidth() > $im->getImageHeight())
//                    $im->rotateImage(new ImagickPixel(), 90);
//
//                $im->trimImage(0);
//                $im->stripImage();
//
//                $labels = [ $im->getImageBlob() ];
//                $return[] = GlobalOperatorResponse::createSuccessResponse($labels,  (string) $item->trackingNumber[$i]);
//            }
//            @unlink($basePath . $temp . $id . '.pdf');
//
//
//            return $return;
//
//        } else {
//
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse(print_r($resp->errors->description,1));
//            } else {
//                return false;
//            }
//
//        }
//    }




    public function createShipment($returnError = false)
    {

//        if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
//            if ($returnError == true) {
//                return GlobalOperatorResponse::createErrorResponse('Not active!');
//            } else {
//                return false;
//            }

        $data = new stdClass();
        $data->sender_country_iso = $this->sender_country;
        $data->service_code = $this->_service;
        $data->order_reference = $this->ref;
        $data->sender_contact = $this->sender_name;
        $data->sender_email = $this->sender_mail;
        $data->sender_company = $this->sender_company;
        $data->sender_name = $this->sender_name;
        $data->sender_address_line_1 = $this->sender_address1;
        $data->sender_address_line_2 = $this->sender_address2;
        $data->sender_address_line_3 = '';
        $data->sender_city = $this->sender_city;
        $data->sender_state = '';
        $data->sender_postcode = $this->sender_zip_code;
        $data->sender_telephone = $this->sender_tel;
        $data->receiver_country_iso = $this->receiver_country;
        $data->receiver_contact = $this->receiver_name;
        $data->receiver_email = $this->receiver_mail;
        $data->receiver_company = $this->receiver_company;
        $data->receiver_name = $this->receiver_name;
        $data->receiver_address_line_1 = $this->receiver_address1;
        $data->receiver_address_line_2 = $this->receiver_address2;
        $data->receiver_address_line_3 = '';
        $data->receiver_city = $this->receiver_city;
        $data->receiver_state = '';
        $data->receiver_postcode = $this->receiver_zip_code;
        $data->receiver_telephone = $this->receiver_tel;
        $data->fullpallets = 0;
        $data->halfpallets = 0;
        $data->qtrpallets = 0;
        $data->palletlifts = 0;
        $data->value = $this->package_value;
        $data->currency = $this->package_value_currency;
        $data->item_type = '';
        $data->notes = $this->ref;
        $data->description =  $this->package_content;
        $data->parcel = [];

        $parcel = new stdClass();
        $parcel->weight = $this->package_weight;
        $parcel->height = $this->package_size_d;
        $parcel->width = $this->package_size_w;
        $parcel->length = $this->package_size_l;

        for($i = 0; $i < $this->packages_number; $i++)
            $data->parcel[] = $parcel;

        $data->label_type = 'pdf';
        $data->label_size = '100x150';
        $resp = $this->_call('generate-label', $data);

        $return = [];
        if($resp->status_code == 200)
        {

            $item = $resp->_embedded->LabelResponse[0];

            if($item->status == 'error')
            {
                if ($returnError == true) {
                    return GlobalOperatorResponse::createErrorResponse(print_r($item->errors,1));
                } else {
                    return false;
                }
            }

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);

            $label = (string) $item->Label_bin_str;
            $label = base64_decode($label);

            $basePath = Yii::app()->basePath . '/../';
            $dir = 'uplabels/';
            $temp = $dir . 'temp/';

            @file_put_contents($basePath . $temp .  $this->ref . '.pdf', $label);
            @file_put_contents('smarttrack.pdf', $label);

            for ($i = 0; $i < $this->packages_number; $i++) {
                $im->readImage($basePath . $temp .  $this->ref . '.pdf['.$i.']');
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }

                if ($im->getImageWidth() > $im->getImageHeight())
                    $im->rotateImage(new ImagickPixel(), 90);

                $im->trimImage(0);
                $im->stripImage();

                $labels = [ $im->getImageBlob() ];
                $return[] = GlobalOperatorResponse::createSuccessResponse($labels,  (string) $item->trackingNumber[$i]);
            }
            @unlink($basePath . $temp .  $this->ref . '.pdf');

        } else {

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse(print_r($resp->errors->description,1));
            } else {
                return false;
            }
        }

        return $return;
    }

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;
        $model->setService($uniq_id);

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierInternal->courier->getWeight(true);

        $model->package_content = $courierInternal->courier->package_content;
        $model->ref = $courierInternal->courier->local_id;
        $model->package_value = $courierInternal->courier->package_value;
        $model->package_value_currency = $courierInternal->courier->value_currency;

        $model->packages_number = $courierInternal->courier->packages_number;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;
        $model->setService($courierU->courierUOperator->uniq_id);

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;
        $model->sender_name = $from->getUsefulName(true);
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_tel = $from->tel;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country0->id;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country0->id;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->package_weight = $courierU->courier->getWeight(true);

        $model->package_content = $courierU->courier->package_content;
        $model->ref = $courierU->courier->local_id;
        $model->package_value = $courierU->courier->package_value;
        $model->package_value_currency = $courierU->courier->value_currency;

        $model->packages_number = $courierU->courier->packages_number;


        return $model->createShipment($returnErrors);
    }


    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        return self::_track($no);
    }


    protected static function _track($no)
    {
//        $model = new self;
//        $resp = $model->_call('dpi/tracking/v1/trackings/'.$no, false);
//        $resp = json_decode($resp);
//
//        $statuses = new GlobalOperatorTtResponseCollection();
//
//        if($resp->events)
//        {
//            foreach($resp->events AS $event)
//            {
//                $name = (string) $event->status;
//                $date = (string) $event->timestamp;
//                $date = mb_substr(str_replace('T', ' ', $date), 0, 19);
//
//                $statuses->addItem($name, $date, NULL);
//            }
//        }
//
//
//        return $statuses;
    }

    protected function _getToken()
    {
        $CACHE_NAME = 'SMARTTRACK_TOKEN';

        $token = Yii::app()->cache->get($CACHE_NAME);
        if($token === false)
        {
            $model = new self;
            $data = new stdClass();
            $data->grant_type = 'client_credentials';
            $data->client_id = self::API_KEY;
            $data->client_secret = self::API_SECRET;

            $resp = $model->_call('token', $data, true);
            $token = $resp->access_token;

            Yii::app()->cache->set($CACHE_NAME, $token, 3000); // less than hour
        }

        return $token;
    }


    protected function _call($method, $data, $loginMethod = false)
    {

        $url = self::URL.'/'.$method;

        MyDump::dump('smart_track_owe.txt', 'REQ : '. $url.' : '.print_r(json_encode($data), 1));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);


        $array = [];
        $array[] = 'Content-Type: application/json';

        // it's getting token
        if(!$loginMethod) {

            $array[] = 'Authorization: Bearer ' . $this->_getToken();
            $array[] = 'Accept: application/json';
        }

        if($data) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $array);

        $response = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $error = curl_error($ch);

        MyDump::dump('smart_track_owe.txt', 'RESP : '. print_r($response, 1));
        MyDump::dump('smart_track_owe.txt', 'RESP CODE : '. print_r($httpcode, 1));
        MyDump::dump('smart_track_owe.txt', 'RESP ERROR : '. print_r($error, 1));

        $response = json_decode($response);

        return $response;

    }


}