<?php

class PostalInvoiceProforma
{


    protected static function printTableHeader(&$pdf)
    {
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(10,3,Yii::t('postal_pdf','L.p.'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('postal_pdf','Paczka'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('postal_pdf','Nadawca'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(50,3,Yii::t('postal_pdf','Odbiorca'),1,'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->MultiCell(30,3,Yii::t('postal_pdf','Parametry'),1,'C',false,0,'','',true,0,false,true,0,'T',true);

        $pdf->ln(4);

        $pdf->SetFont('freesans', '', 8);
    }


    public static function generateCardMulti(array $postals, $number = '', &$done = false)
    {
        $done = [];

        /* @var $postals Postal[] */
        foreach($postals AS $key => $item)
        {
            if(in_array($item->postal_stat_id, [PostalStat::CANCELLED_PROBLEM, PostalStat::CANCELLED, PostalStat::CANCELLED_USER]) OR $item->bulk)
                unset($postals[$key]);
        }


        $totalNumber = S_Useful::sizeof($postals);

        if($totalNumber > 2500)
            throw new Exception(Yii::t('postal','Za dużo paczek do wygenerowania potwierdzenia nadania.'));



        /* @var $pdf TCPDF */
        $pdf = PDFGenerator::initialize();
        $pdf->setMargins(10,10,10, true);

        foreach($postals AS $key => $item) {

            $pdf = self::generateCard($item, $number, $pdf);
            $done[$key] = $item->id;
        }


        /* @var $pdf TCPDF */
        $pdf->Output('inv.pdf', 'D');


        return $done;
    }



    public static function generateCard(Postal $item, $number = '', TCPDF $pdfHandler = NULL)
    {

        /* @var $pdf TCPDF */
        if($pdfHandler === NULL)
            $pdf = PDFGenerator::initialize();
        else
            $pdf = $pdfHandler;

        $pdf->setMargins(5, 10, 5, true);

        $pdf->AddPage('P', [140,100]);
        $pdf->SetAutoPageBreak(false);


//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

        $pdf->SetTextColor(0, 0, 0);

        $pdf->SetFont('freesans', '', 11);
        $y = $pdf->GetY();


        $itemNo = '';
        if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
            $itemNo = ($item->postalLabel ? $item->postalLabel->track_id : $item->external_id);

        if($itemNo == '')
            $itemNo = $item->local_id;

        $pdf->MultiCell(0, 0, 'INVOICE PRO-FORMA NUMBER'."\r\n".'(NUMER FAKTURY PRO-FORMY): '."\r\n".$itemNo, 0, 'C', false);


        $pdf->SetAbsY($pdf->GetY() + 10);
        $pdf->SetAbsX(1);

        $pdf->SetFont('freesans', 'U', 8);
        $pdf->MultiCell(0, 0, 'SELLER (Sprzedawca):', 0, 'L', false, 1, 5, '', true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 8);
        if($item->user_id == 2404)
            $text = 'S.F EXPRESS, Raffles Place 32-01, 048623 Singapore Land, Singapore';
        else
            $text = $item->senderAddressData->getUsefulName(true).', '.trim($item->senderAddressData->address_line_1.' '.$item->senderAddressData->address_line_2).', '.$item->senderAddressData->zip_code.' '.$item->senderAddressData->city.', '.$item->senderAddressData->country0->code;


        $pdf->MultiCell(0, 0, $text, 0, 'L', false, 1, 5, '', true, 0, false, true, 0);

        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'U', 8);
        $pdf->MultiCell(0, 0, 'BUYER (Kupujący):', 0, 'L', false, 1, 5, '', true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 8);

        $text = $item->ref;
        if($text != '')
            $text .= "\r\n";

        $text .= $item->receiverAddressData->getUsefulName(true);
        $text .= "\r\n";
        $text .= $item->receiverAddressData->zip_code;
        $text .= "\r\n";
        $text .= $item->receiverAddressData->city;
        $text .= "\r\n";
        $text .= $item->receiverAddressData->address_line_1.' '.$item->receiverAddressData->address_line_2;
        $text .= "\r\n";
        $text .= $item->receiverAddressData->tel;
        $text .= "\r\n";
        $text .= $item->receiverAddressData->country0->code;

        $pdf->MultiCell(0, 0, $text, 0, 'L', false, 1, 5, '', true, 0, false, true, 0);


        $pdf->SetAbsY($pdf->GetY() + 5);

        $pdf->SetFont('freesans', 'U', 8);
        $pdf->MultiCell(0, 0, 'SHIPMENT DETAILS:', 0, 'L', false, 1, 5, '', true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 8);


        $quantity = 1;
        if($item->user_id == 2404)
        {
            $temp = explode(':', $item->note1);
            $temp = trim($temp[0]);
            $temp = intval($temp);
            $quantity = $temp;
        }

        $text = 'Parcel number (Numer przesyłki): '.($item->postalLabel ? $item->postalLabel->track_id : $item->external_id);
        $text .= "\r\n";
        $text .= 'Content (Zawartość): '.$item->content;
        $text .= "\r\n";
        $text .= 'Quantity (Ilość): '.$quantity;
        $text .= "\r\n";
        $text .= 'Weight (Waga): '.number_format($item->weight/1000, 3);
        $text .= "\r\n";
        $text .= 'Value (Wartość): '.number_format($item->value,2).' '.$item->value_currency;
        $text .= "\r\n";

        $pdf->MultiCell(0, 0, $text, 0, 'L', false, 1, 5, '', true, 0, false, true, 0);


        if($pdfHandler)
            return $pdf;
        else
            $pdf->Output('inv.pdf', 'D');
    }

}
