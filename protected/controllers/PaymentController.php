<?php

class PaymentController extends Controller {

    public $layout = 'other';

    public function init()
    {
        parent::init();

        $this->layout = 'other';
    }

    public function filters()
    {
        return array_merge(array(
            'accessControl', // perform access control for CRUD operations
            array(
                'application.filters.HttpsFilter',
                'bypass' => false,
            ),
        ));
    }

    public function actionPay($hash, $cs)
    {
        /* @var $order Order */
        $order = Order::model()->findByAttributes(array('hash' => $hash));

        if($order == NULL)
            throw new CHttpException(404);

        if($cs != $order->calculateCS())
            throw new CHttpException(404);

        if($order->payment_type_id != PaymentType::PAYMENT_TYPE_ONLINE)
            throw new CHttpException(404);

        if($order->paid)
        {
            throw new CHttpException(403, Yii::t('order','To zamówienie zostało już opłacone!'));
        }


        if(OnlinePayments::model()->findByAttributes(array('order_id' => $order->id)) != NULL)
        {
            Yii::app()->user->setFlash('payment', Yii::t('order', 'Płatność za to zamówienie została już zlecona, czekamy na jej potwierdzenie...'));
            $this->redirect(array('order/view', 'urlData' => $order->hash));
            return;
        }

        if($order->user_id === NULL)
            $ownerData = $order->ownerData;
        else
            $ownerData = $order->user;


        $onlinePayments = new OnlinePayments();
        $onlinePayments->amount = $order->value_brutto;
        $onlinePayments->currency = $order->currency;
        $onlinePayments->order_id = $order->id;


        if(!$onlinePayments->save())
            throw new CHttpException(403, 'Wystąpił błąd z płatnością. Skontaktuj się z nami w tej sprawie.');

        // to get proper hash:
        $onlinePayments = OnlinePayments::model()->findByPk($onlinePayments->id);

        /* @var $p24 Przelewy24 */
        $p24 = Yii::app()->Przelewy24;

        $p24->addValue('p24_amount', $p24->covertMoneyAmountToP24($order->value_brutto));
        $p24->addValue('p24_currency', $order->currency);
        $p24->addValue('p24_description', 'Zamówienie #'.$order->local_id);
        $p24->addValue('p24_client', $ownerData->name);
        $p24->addValue('p24_address', $ownerData->address_line_1);
        $p24->addValue('p24_zip', $ownerData->zip_code);
        $p24->addValue('p24_city', $ownerData->city);
        $p24->addValue('p24_country', $ownerData->country0->code);
        $p24->addValue('p24_email', $ownerData->email);
        $p24->addValue('p24_session_id', $onlinePayments->hash);
        $p24->addValue('p24_url_return', urlencode(Yii::app()->createAbsoluteUrl('payment/paymentReturn', array('cs' => $order->calculateCS(), 'hash' => $order->hash))));

        $res = $p24->trnRegister();


        if($res["error"] ==0)
        {
            $token = $res['token'];

            $onlinePayments->token = $token;
            $onlinePayments->update(array('token'));
            // redirect to P24
            $p24->trnRequest($token, true);
        }
        else
        {
            Yii::log(print_r($res,1), CLogger::LEVEL_ERROR);
            throw new CHttpException(403, 'Wystąpił błąd z płatnością. Skontaktuj się z nami w tej sprawie.');
        }

    }

    public function actionPaymentReturn($hash, $cs)
    {

        /* @var $order Order */

        $order = Order::model()->findByAttributes(array('hash' => $hash));

        if($order == NULL)
            throw new CHttpException(404);

        if($cs != $order->calculateCS())
            throw new CHttpException(404);

        if($order->payment_type_id != PaymentType::PAYMENT_TYPE_ONLINE)
            throw new CHttpException(404);

        $paid = false;
        if($order->order_stat_id == OrderStat::FINISHED)
        {
            $paid = true;
            $htmlPaid = $this->renderPartial('payment_confirmed', array(
                'order' => $order), true);
        }

        $this->render('payment_return', array(
            'order' => $order,
            'paid' => $paid,
            'htmlPaid' => $htmlPaid,
        ));
    }

    public function actionPrzelewy24Listener()
    {

        //file_put_contents('ddd.txt', print_r('abc'));

        MyDump::dump('przelewy24.txt', 'LISTENER: '.print_r($_POST,1));

        /* @var $P24 Przelewy24 */
        $P24 = Yii::app()->Przelewy24;

        foreach($_POST as $k=>$v)
            $P24->addValue($k,$v);

        $orderHash = $_POST['p24_session_id'];

        /* @var $onlinePayment OnlinePayments */
        /* @var $order Order */
        $onlinePayment = OnlinePayments::model()->findByAttributes(array('hash' => $orderHash));
        $order = $onlinePayment->order;


        if($order === NULL)
            return false;


        $P24->addValue('p24_currency', $onlinePayment->currency);
        $P24->addValue('p24_amount', $P24->covertMoneyAmountToP24($onlinePayment->amount));
        $res = $P24->trnVerify();
        if($res["error"] ==0)
        {
            $onlinePayment->makePaid();
        }
        else{
            $onlinePayment->stat = OnlinePayments::STAT_ERROR;
            $onlinePayment->update(array('stat'));
            Yii::log('Błąd weryfikacji P24: '.print_r($res,1), CLogger::LEVEL_ERROR);
        }

    }

    public function actionAjaxCheckIsPaid()
    {
        if(Yii::app()->request->isAjaxRequest)
        {

            $hash = $_POST['hash'];

            /* @var $payment Payment */
            $order = Order::model()->findByAttributes(array('hash' => $hash));


            if($order->order_stat_id == OrderStat::FINISHED)
            {

                $html = $this->renderPartial('payment_confirmed', array(
                    'order' => $order), true);

                echo CJSON::encode(array(
                    'result' => 'OK',
                    'html' => $html));
                Yii::app()->end();
            }

            echo CJSON::encode(array(
                'result' => '',
            ));
        }
    }

}