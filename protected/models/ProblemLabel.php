<?php

class ProblemLabel
{
    /**
     * Generates Problem label and returns it as Image
     * @param Courier $item
     * @param AddressData|NULL $address

     * @return String
     */
    static function generate(Courier $item, $text, $asImageBlob = true)
    {

        if($text == '')
            $text = '-- ? --';

        $pageW = 100;
        $pageH = 150;

        // GENERATE OWN:
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pdf->addPage('H', array($pageW,$pageH));

        $margins = 1;

        /* @var $pdf TCPDF */
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line(0, 0, $blockWidth, 0);
        $pdf->Line(0, 0, 0, $blockHeight);

        $pdf->Line($blockWidth, 0, $blockWidth, $blockHeight);
        $pdf->Line(0, $blockHeight, $blockWidth, $blockHeight, $blockHeight);

        $yBase = 0;
        $xBase = 0;

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

        $pdf->SetFont('freesans', 'B', 12);
        $pdf->MultiCell($blockWidth,5, '#'.$item->local_id, 0, 'L', false, 1, $xBase + $margins, 1, true, 0, false, true, 0);

        $pdf->SetFillColor(0,0,0);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFont('freesans', 'B', 23);
        $pdf->MultiCell($blockWidth - 2, 10, 'PROBLEM', 1, 'C', true, 0, $xBase + $margins, 7, true, 0, false, true, 0);
        $pdf->MultiCell($blockWidth - 2, 10, 'PROBLEM', 1, 'C', true, 0, $xBase + $margins, 129, true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 10);

        $pdf->SetTextColor(0,0,0);

        $pdf->SetFont('freesans', '', 8);

        if($item->user->source_domain != PageVersion::PAGEVERSION_CQL) {
            $pdf->MultiCell($blockWidth - 2, 14, "Wystąpił problem z etykietą dla Twojej paczki.\r\nPoniżej znajduje się komunikat błędu otrzymany od firmy kurierskiej.\r\nJeżeli nie jest zrozumiały - skontaktuj się z nami.", 1, 'C', false, 1, $xBase + $margins, 20, true, 0, false, true, 14, 'M', true);
        }

        $pdf->MultiCell($blockWidth - 2,14, "There is a problem with label for your package.\r\nBelow you'll find message from courier company.\r\nIf you don't understand it - please contact us.", 1, 'C', false, 1, $xBase + $margins, 33, true, 0, false, true, 14, 'M', true);

        $pdf->SetFont('freesans', 'B', 12);
        $pdf->MultiCell($blockWidth - 2,6, "Komunikat / Message:", 0, 'C', false, 1, $xBase + $margins, 48, true, 0, false, true, 6, 'M');

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell($blockWidth - 2,72, $text, 1, 'L', false, 1, $xBase + $margins, 55, true, 0, false, true, 72, 'T', true);


//        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
//        $pdf->Line($xBase + $margins, $yBase + $margins + 139, $blockWidth, $yBase + $margins + 139);

        $pdf->SetFont('freesans', '', 8);

        if(in_array($item->user->source_domain, [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS])) {
            $pdf->MultiCell($blockWidth, 10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, $xBase + $margins, $yBase + 139 + $margins, true, 0, false, true, 0);
            $pdf->MultiCell($blockWidth, 6, 'Member of KAAB GROUP', 0, 'C', false, 1, $xBase + $margins, $yBase + 143 + $margins, true, 0, false, true, 0);
        }


        $pdfString = $pdf->Output('asString', 'S');

        if($asImageBlob) {
            $img = ImagickMine::newInstance();
            $img->setResolution(300, 300);
            $img->readImageBlob($pdfString);
            $img->setImageFormat('png8');

            if ($img->getImageAlphaChannel()) {
                $img->setImageBackgroundColor('white');
                $img->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $img->stripImage();
            $image = $img->getImageBlob();
        } else
            $image = $pdfString;

        return $image;
    }

    static function generatePostal(Postal $item, $text, $pdfHandler = false)
    {

        if($text == '')
            $text = '-- ? --';


        $pageW = 100;
        $pageH = 150;


        if(!$pdfHandler) {
            // GENERATE OWN:
            $pdf = PDFGenerator::initialize();
            $pdf->setFontSubsetting(true);

            $pdf->addPage('H', array($pageW, $pageH));
        }
        else $pdf = $pdfHandler;


        $margins = 1;

        /* @var $pdf TCPDF */
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line(0, 0, $blockWidth, 0);
        $pdf->Line(0, 0, 0, $blockHeight);

        $pdf->Line($blockWidth, 0, $blockWidth, $blockHeight);
        $pdf->Line(0, $blockHeight, $blockWidth, $blockHeight, $blockHeight);

        $yBase = 0;
        $xBase = 0;

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

        $pdf->SetFont('freesans', 'B', 12);
        $pdf->MultiCell($blockWidth,5, '#'.$item->local_id, 0, 'L', false, 1, $xBase + $margins, 1, true, 0, false, true, 0);

        $pdf->SetFillColor(0,0,0);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFont('freesans', 'B', 23);
        $pdf->MultiCell($blockWidth - 2, 10, 'PROBLEM', 1, 'C', true, 0, $xBase + $margins, 7, true, 0, false, true, 0);
        $pdf->MultiCell($blockWidth - 2, 10, 'PROBLEM', 1, 'C', true, 0, $xBase + $margins, 129, true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 10);

        $pdf->SetTextColor(0,0,0);

        $pdf->SetFont('freesans', '', 8);

        if($item->user->source_domain != PageVersion::PAGEVERSION_CQL) {
            $pdf->MultiCell($blockWidth - 2, 14, "Wystąpił problem z etykietą dla Twojej paczki.\r\nPoniżej znajduje się komunikat błędu otrzymany od firmy kurierskiej.\r\nJeżeli nie jest zrozumiały - skontaktuj się z nami.", 1, 'C', false, 1, $xBase + $margins, 20, true, 0, false, true, 14, 'M', true);
        }

        $pdf->MultiCell($blockWidth - 2,14, "There is a problem with label for your package.\r\nBelow you'll find message from courier company.\r\nIf you don't understand it - please contact us.", 1, 'C', false, 1, $xBase + $margins, 33, true, 0, false, true, 14, 'M', true);

        $pdf->SetFont('freesans', 'B', 12);
        $pdf->MultiCell($blockWidth - 2,6, "Komunikat / Message:", 0, 'C', false, 1, $xBase + $margins, 48, true, 0, false, true, 6, 'M');

        $pdf->SetFont('freesans', '', 10);
        $pdf->MultiCell($blockWidth - 2,72, $text, 1, 'L', false, 1, $xBase + $margins, 55, true, 0, false, true, 72, 'T', true);


//        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
//        $pdf->Line($xBase + $margins, $yBase + $margins + 139, $blockWidth, $yBase + $margins + 139);

        $pdf->SetFont('freesans', '', 8);


        if(in_array($item->user->source_domain, [PageVersion::PAGEVERSION_DEFAULT, PageVersion::PAGEVERSION_RS])) {
            $pdf->MultiCell($blockWidth, 10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, $xBase + $margins, $yBase + 139 + $margins, true, 0, false, true, 0);
            $pdf->MultiCell($blockWidth, 6, 'Member of KAAB GROUP', 0, 'C', false, 1, $xBase + $margins, $yBase + 143 + $margins, true, 0, false, true, 0);
        }

        if($pdfHandler)
            return $pdf;

        $pdfString = $pdf->Output('asString', 'S');


        $img = ImagickMine::newInstance();
        $img->setResolution(300, 300);
        $img->readImageBlob($pdfString);
        $img->setImageFormat('png8');

        if ($img->getImageAlphaChannel()) {
            $img->setImageBackgroundColor('white');
            $img->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $img->stripImage();
        $image = $img->getImageBlob();


        return $image;
    }
}