<?php

class UserInvoiceMassManageController extends Controller
{
    const MAX_NO = 1000;

    const STATE_PROGRESS = 1;
    const STATE_SUCCESS = 10;
    const STATE_FAIL = 99;

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority && AdminRestrictions::isSuperAdmin()',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    protected static function getStatusCacheName($sid)
    {
        return 'USER_INVOICE_MASS_MANAGE_STATUS_'.Yii::app()->session->sessionID.'_'.$sid;
    }

    protected static function setStatusValue($sid, $state, $message = false, $done = [], $all = [])
    {

        $totalNo = S_Useful::sizeof($all);
        $doneIds = [];

        foreach($all AS $key => $item)
        {
            if(isset($done[$key]))
            {
                $doneIds[$key] = $item;
                unset($all[$key]);
            }
        }

        $data = [
            'state' => $state,
            'message' => $message,
            'doneNo' => S_Useful::sizeof($doneIds),
            'doneList' => $doneIds,
            'notDoneNo' => S_Useful::sizeof($all),
            'notDoneList' => $all,
            'totalNo' => $totalNo,
        ];

        Yii::app()->cache->set(self::getStatusCacheName($sid), $data, 60*60);
    }

    public function actionStatus($sid)
    {
        if(Yii::app()->request->isAjaxRequest) {
            $data = Yii::app()->cache->get(self::getStatusCacheName($sid));

            echo CJSON::encode($data);
        } else
            Yii::app()->end();
    }

    public function actionIndex($sid = false)
    {

        $done = [];


        self::setStatusValue($sid, self::STATE_PROGRESS);

        $model = new UserInvoiceScanner();

        if($sid OR isset($_POST['UserInvoiceScanner']['items_ids']))
        {
            $data = $_POST;

            $packagesIds = $data['UserInvoiceScanner']['items_ids'];
            $packagesIds = explode(PHP_EOL, $packagesIds);

            $model->items_ids = $packagesIds;

            if($model->validate())
            {

                $invoiceModels = $model->returnUserInvoiceModels();

                if(!S_Useful::sizeof($invoiceModels)) {
                    self::setStatusValue($sid, self::STATE_FAIL, 'No invoices to perfom on!', [], $model->items_ids_backup);
                    Yii::app()->end();
                }
                else if(S_Useful::sizeof($invoiceModels) > self::MAX_NO)
                {
                    self::setStatusValue($sid, self::STATE_FAIL, 'Max number of items at once: '.self::MAX_NO, [], $model->items_ids_backup);
                    Yii::app()->end();
                } else {


                    /* @var $invModel UserInvoice */
                    foreach($invoiceModels AS $key => $invModel)
                    {
                        $files[$invModel->inv_file_path] =  preg_replace("/[^a-z0-9\.]/", "", strtolower($invModel->title)).'_'.$invModel->id.'_'.$invModel->inv_file_name;
                        $files[$invModel->sheet_file_path] = preg_replace("/[^a-z0-9\.]/", "", strtolower($invModel->title)).'_'.$invModel->id.'_'.$invModel->sheet_file_name;
                        $done[$key] = $invModel->id;
                    }

                    ZipOutput::generateZipOutputFile($files, 'invoices');

                    self::setStatusValue($sid, self::STATE_SUCCESS, false, $done, $model->items_ids_backup);


                }

                // important!
                Yii::app()->end();
            }
        }


        $model->items_ids = '';
        $this->render('index',
            [
                'model' => $model,
            ]);

    }
}
