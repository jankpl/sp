    <h2><?php echo Yii::t('palette','Transport palet');?></h2>

<?php
if(Yii::app()->user->isGuest OR !Yii::app()->user->getModel()->isPremium):
    echo TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, Yii::t('site', 'Dostęp do tej usługi jest możliwy tylko dla wybranych klientów. Skontaktuj się z znami w sprawie uzyskania dostępu.'), array('closeText' => false));
else:
    ?>



    <div style="text-align: center;">
        <?php echo CHtml::link(Yii::t('palette','Utwórz nową przesyłkę'),array('/palette/palette/create'), array('class' => 'btn')); ?>

        <?php echo CHtml::link(Yii::t('palette','Lista Twoich przesyłek'),array('/palette/palette/list'), array('class' => 'btn')); ?>
    </div>

<?php endif;?>