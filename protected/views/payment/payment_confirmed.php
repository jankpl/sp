<?php
/* @var $order Order */
?>

<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('payment','Płatność została potwierdzona!'), array('closeText' => false));?>

<div style="text-align: center;">
<?php echo CHtml::link('Przejdź do zamówienia', array('order/view', 'urlData' => $order->hash, 'new' => true), array('class' => 'btn'));?>
</div>