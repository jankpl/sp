<?php

class _UserDocumentsGridViewUser extends UserDocuments
{


    public function rules() {

        $array = array(

            array('        
            ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    public function attributeLabels() {

        $array = array(
        );

        $array = array_merge(parent::attributeLabels(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', Yii::app()->user->id);
        $criteria->compare('customer_visible', true);
        $criteria->compare('stat', $this->stat);
        $criteria->compare('type', $this->type);
        $criteria->compare('desc', $this->desc, true);
        $criteria->compare('desc_customer', $this->desc_customer, true);




        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 'id DESC';
//        $sort->attributes = array(
//            '__sender_country_id' => array(
//                'asc' => 'senderAddressData.country ASC',
//                'desc' => 'senderAddressData.country DESC',
//            ),
//            '__receiver_country_id' => array(
//                'asc' => 'receiverAddressData.country ASC',
//                'desc' => 'receiverAddressData.country DESC',
//            ),
//            '__stat' => array(
//
//
//                'asc' => 'courierStatTr.short_text ASC',
//                'desc' => 'courierStatTr.short_text DESC',
//            ),
//            '*', // add all of the other columns as sortable
//        );
        $criteria = $this->searchCriteria();

        // if there are no conditions (except for User ID), use simple count of packages to improve performance
        $totalCount = NULL;
//        if ($criteria->condition == 'user_id=:ycp0') {
//            $cmd = Yii::app()->db->createCommand();
//            $cmd->select('COUNT(id)')->from((new self)->tableName())->where('user_id = :user_id', [':user_id' => Yii::app()->user->id]);
//            $totalCount = $cmd->queryScalar();
//        }


        return new CActiveDataProvider($this

            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize'=> Yii::app()->user->getState('pageSize', 15),
                ),
            ));

    }


}