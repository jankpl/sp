<?php
/* @var $form CActiveForm */
/* @var $model AddressData */
/* @var $i Integer */
/* @var $noEmail boolean */


?>


<?php //echo $form->errorSummary($model); ?>


<?php

if(1):
//if(S_Useful::sizeof($listAddressData)):

    ?>

    <?php
    if(!$blockAddressSuggester)
        $this->widget('AddressDataSuggester',
            array(
                'type' => $i,
                'fieldName' => 'CourierTypeDomestic_AddressData',
                'i' => $i,
                'countries' => [CountryList::COUNTRY_PL],
            ));
    ?>


    <?php

endif;
$array = [];

?>
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'company'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'company', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'company'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'name'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'name', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'autofocus' => $focusFirst?'autofocus':'')); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'name'); ?>
    </div><!-- row -->

    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_1', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'data-additions-dependency' => 'true')); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_1'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'address_line_2', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, )); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'address_line_2'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'zip_code', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'data-additions-dependency' => 'true')); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'zip_code'); ?>
    </div><!-- row -->

    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'city'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'city', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled, 'data-additions-dependency' => 'true')); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'city'); ?>
    </div><!-- row -->


    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'tel'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'tel', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'tel'); ?>
    </div><!-- row -->
<?php
if(!$noEmail):
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'email'); ?>
        <?php echo $form->textField($model,(isset($i)?'['.$i.']':'').'email', array('maxlength' => 45, 'readOnly' => $readOnly, 'disabled' => $disabled)); ?>
        <?php echo $form->error($model,(isset($i)?'['.$i.']':'').'email'); ?>
    </div><!-- row -->
    <?php
endif;
?>
<?php
if(!Yii::app()->user->isGuest):
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,(isset($i)?'['.$i.']':'').'addToContactBook'); ?>
        <?php echo $form->checkBox($model,(isset($i)?'['.$i.']':'').'addToContactBook', ['data-bootstrap-checkbox' => true, 'data-default-class' => 'btn-sm', 'data-on-class' => 'btn-sm btn-primary', 'data-off-class' => 'btn-sm btn-default', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak')]); ?>
        <?php echo $form->hiddenField($model,(isset($i)?'['.$i.']':'').'contactBookType'); ?>
    </div><!-- row -->
    <?php
endif;
?>