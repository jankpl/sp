<?php
/* @var $model Order */
?>

<div class="flash-notice">
    <h3><?php echo Yii::t('order','Przejdź do finalizacji zamówienia');?></h3>
    <br/>
    <?php echo CHtml::link(Yii::t('order','Finalizuj'),array('order/finalizeOrder', 'urlData' => $model->hash), array('class' => 'btn')); ?>
</div>