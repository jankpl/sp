<?php

class InpostShipx extends GlobalOperator
{
    const URL = 'https://api-shipx-pl.easypack24.net';
    const KEY = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJhcGktc2hpcHgtcGwuZWFzeXBhY2syNC5uZXQiLCJzdWIiOiJhcGktc2hpcHgtcGwuZWFzeXBhY2syNC5uZXQiLCJleHAiOjE1MTkxMzg3NzAsImlhdCI6MTUxOTEzODc3MCwianRpIjoiNWYzNDU2OTQtYzU5Yy00N2E0LTgxYWUtMzZmOTJiMDc2OWNhIn0.OvZa9y0AsB6iFktVSTCI8rk3V1qkB1Td9lhNWcs66jfFyO0Wc7_wx4Y7fYgFa7rleMaUZDRVBvtRYpwv460tyA';
    const ORGANIZATION_ID = 767;

    const SERVICE_FROM_LOCKER_TO_LOCKER = 'inpost_locker_pass_thru';
    const SERVICE_TO_LOCKER_STD = 'inpost_locker_standard';
    const SERVICE_STD = 'inpost_courier_standard';
    const SERVICE_STD_LOCAL = 'inpost_courier_local_standard';
    const SERVICE_SUPER_EXPRESS = 'inpost_courier_local_super_express';
    const SERVICE_EXPRESS = 'inpost_courier_local_express';
    const SERVICE_EXPRESS_17 = 'inpost_courier_express_1700';
    const SERVICE_EXPRESS_12 = 'inpost_courier_express_1200';
    const SERVICE_EXPRESS_10 = 'inpost_courier_express_1000';

    const MODE_KURIER_KURIER = 1;
    const MODE_KURIER_PACZKOMAT = 2;
    const MODE_PACZKOMAT_PACZKOMAT = 3;


    public $sms_nofitication = false;
    public $mail_notification = false;

    public $sender_name;
    public $sender_company;
    public $sender_tel;
    public $sender_address_1;
    public $sender_address_2;
    public $sender_city;
    public $sender_postalCode;
    public $sender_countryCode;
    public $sender_email;

    public $receiver_name;
    public $receiver_company;
    public $receiver_tel;
    public $receiver_address_1;
    public $receiver_address_2;
    public $receiver_city;
    public $receiver_postalCode;
    public $receiver_countryCode;
    public $receiver_email;

    public $package_size_l;
    public $package_size_w;
    public $package_size_d;
    public $package_weight;

    public $package_desc;

    public $_delivery_locker_id = false;
    public $_pickup_locker_id = false;

    public $label_annotation_text;

    public $_rod = false;
    public $_insurance = 0;
    public $_express_10 = false;
    public $_express_12 = false;
    public $_express_17 = false;
    public $_cod = 0;


//    public static function test()
//    {
//
//        $data = false;
//        $points = [];
//
//        $i = 1;
//        do
//        {
//            $resp = self::_call('v1/points?per_page=100&page='.$i, $data);
//
//            var_dump($resp);
//            exit;
//
//            $totalPages = $resp->total_pages;
//
//            if(is_array($resp->items))
//                foreach($resp->items AS $item)
//                    $points[] = [
//                        'lat' => $item->location->latitude,
//                        'lng' => $item->location->longitude,
//                        'address' => $item->address->line1.', '.$item->address->line2,
//                        'desc' => $item->location_description,
//                        'name' => $item->name,
//                        'id' => $item->name,
//                    ];
//
//            $i++;
//        } while ($i <= $totalPages);
//
//
//        var_dump($points);
//
//
//        exit;
//    }

    /**
     * @param $address
     * @return DeliveryPoints[]
     * @throws CDbException
     * @throws CException
     */
    public static function getPoints()
    {
        $CACHE_NAME = 'INPOST_POINTS';

        $points = Yii::app()->cache->get($CACHE_NAME);

        if($points === false) {
            $points = [];
            $i = 1;
            do {
                $resp = self::_call('v1/points?per_page=250&type=parcel_locker&page=' . $i, false);

//                MyDump::dump('inpost_points.txt', print_r($resp,1));

                $totalPages = $resp->total_pages;

                if (is_array($resp->items))
                    foreach ($resp->items AS $item)
                    {

                        $other = [];
                        $other['payment_available'] = $item->payment_available;
                        $other['payment_desc'] = $item->payment_point_descr;
                        $other['payment_type'] = implode(',', json_decode(json_encode($item->payment_type), true));

                        $dp = DeliveryPoints::getUpdateDeliveryPoints(DeliveryPoints::TYPE_INPOST, $item->name, trim($item->address_details->street . ' ' . $item->address_details->building_number  . ' ' . $item->address_details->flat_number), $item->address_details->post_code, $item->address_details->city, 'PL', $item->location_description, $item->location->latitude, $item->location->longitude, implode(',', $item->type), $other);
                        $points[] = $dp;
//
//                        $points[] = [
//                            'lat' => $item->location->latitude,
//                            'lng' => $item->location->longitude,
//                            'address' => $item->address->line1 . ', ' . $item->address->line2,
//                            'desc' => $item->location_description,
//                            'name' => $item->name,
//                            'id' => $item->name,
//                        ];
                    }
                $i++;
            } while ($i <= $totalPages);


            Yii::app()->cache->set($CACHE_NAME, $points, 60*60*24);
        }

        if(!$points)
            $points = [];

        return $points;
    }

//    public static function initTest()
//    {
//        $model = new self;
//        $model->package_size_l = 10;
//        $model->package_size_d = 10;
//        $model->package_size_w = 10;
//
//        $model->package_weight = 1;
//
//        $model->sender_name = 'Jan Nowak';
//        $model->sender_company = 'Nowak Inc';
//        $model->sender_tel = '123456789';
//        $model->sender_address_1 = 'Rudzka 1';
//        $model->sender_address_2 = '';
//        $model->sender_city = 'Rybnik';
//        $model->sender_postalCode = '44-200';
//        $model->sender_countryCode = 'PL';
//        $model->sender_email = 'dsadsasadsda@dsasdaasdasd.pl';
//
//        $model->receiver_name = 'Tomasz Kowalski';
//        $model->receiver_company = 'Kowalski Inc';
//        $model->receiver_tel = '123456789';
//        $model->receiver_address_1 = 'Malborska 130';
//        $model->receiver_address_2 = '';
//        $model->receiver_city = 'Warszawa';
//        $model->receiver_postalCode = '02-677';
//        $model->receiver_countryCode = 'PL';
//        $model->receiver_email = 'adsdsaadssad@sdasdadasdsadas.pl';
//
//        return $model;
//    }

    public function getServices()
    {
        $data = $this->prepareData(true);


        $resp = self::_call('v1/organizations/' . self::ORGANIZATION_ID . '/shipments', $data);
        $id = $resp->id;

        $resp2 = self::_call('v1/shipments/'.$id, false);



        $offers = $resp->offers;

        $return = [];
        if(is_array($offers))
            foreach($offers AS $item)
            {
                if($item->status != 'available')
                    continue;

                $return[] = $item->service->id;
            }

        return $return;

    }

    protected function fillData(Courier $courier, AddressData $from, AddressData $to)
    {
        $senderName = $from->name;
        $senderCompany = $from->company;

        $this->sender_name = $senderName;
        $this->sender_company = $senderCompany;
        $this->sender_postalCode = $from->zip_code;
        $this->sender_city = $from->city;
        $this->sender_address_1 = $from->address_line_1;
        $this->sender_address_2 = $from->address_line_2;
        $this->sender_tel = $from->tel;
        $this->sender_countryCode = $from->country0->code;
        $this->sender_country_id = $from->country0->id;
        $this->sender_email = $from->fetchEmailAddress(true);

        $this->receiver_name = $to->name;
        $this->receiver_company = $to->company;
        $this->receiver_postalCode = $to->zip_code;
        $this->receiver_city = $to->city;
        $this->receiver_address_1 = $to->address_line_1;
        $this->receiver_address_2 = $to->address_line_2;
        $this->receiver_tel = $to->tel;
        $this->receiver_countryCode = $to->country0->code;
        $this->receiver_country_id = $to->country0->id;
        $this->receiver_email = $to->fetchEmailAddress(true);

        $this->package_weight = $courier->getWeight(true);

        $this->package_size_l = $courier->package_size_l;
        $this->package_size_d = $courier->package_size_d;
        $this->package_size_w = $courier->package_size_w;

        $this->package_content = $courier->package_content;
        $this->ref = $courier->local_id;

        if($courier->cod_value)
            $this->_cod = $courier->cod_value;

    }


//    public function orderShipment()
//    {
//        $data = new stdClass();
//
//        $data->parcels = [];
//
//        $parcel = new stdClass();
//        $parcel->dimensions = new stdClass();
//        $parcel->dimensions->length = $this->package_size_l * 10;
//        $parcel->dimensions->width = $this->package_size_w * 10;
//        $parcel->dimensions->height = $this->package_size_d * 10;
//        $parcel->dimensions->unit = 'mm';
//        $parcel->weight = new stdClass();
//        $parcel->weight->amount = $this->package_weight;
//        $parcel->weight->unit = 'kg';
////        $parcel->is_non_standard = false;
//
//        $data->parcels[] = $parcel;
//
//        $data->receiver = new stdClass();
//        $data->receiver->name = $this->receiver_name;
//        $data->receiver->company_name = $this->receiver_company;
//        $data->receiver->email = $this->receiver_email;
//        $data->receiver->phone = $this->receiver_tel;
//        $data->receiver->address = new stdClass();
//        $data->receiver->address->line1 = $this->receiver_address_1;
//        $data->receiver->address->line2 = $this->receiver_address_2;
//        $data->receiver->address->city = $this->receiver_city;
//        $data->receiver->address->post_code = $this->receiver_postalCode;
//        $data->receiver->address->country_code = strtoupper($this->receiver_countryCode);
//
//        $data->sender = new stdClass();
//        $data->sender->name = $this->sender_name;
//        $data->sender->company_name = $this->sender_company;
//        $data->sender->email = $this->sender_email;
//        $data->sender->phone = $this->sender_tel;
//        $data->sender->address = new stdClass();
//        $data->sender->address->line1 = $this->sender_address_1;
//        $data->sender->address->line2 = $this->sender_address_2;
//        $data->sender->address->city = $this->sender_city;
//        $data->sender->address->post_code = $this->sender_postalCode;
//        $data->sender->address->country_code = strtoupper($this->sender_countryCode);
//
//
//
//        $fromLocker = $toLocker = $dropoff_point = $sending_method = $target_point = false;
//
//        $locker_1 = 'ALK01A';
//        $locker_2 = 'BAC01G';
//
//
//        $fromLocker = false;
//        $toLocker = true;
//
//        if($fromLocker)
//        {
//            $sending_method = 'parcel_locker';
//            $dropoff_point = $locker_1;
//        }
//
//        if($toLocker)
//        {
//            $target_point = $locker_2;
//        }
//
//        if($fromLocker && $toLocker)
//        {
//            $service = self::SERVICE_TO_LOCKER_STD;
//        }
//        else if($fromLocker)
//        {
//            throw new Exception('Unavailable option!');
//        }
//        else if($toLocker)
//        {
//            $service = self::SERVICE_TO_LOCKER_STD;
//        }
//        else
//        {
//            $service = self::SERVICE_STD;
//        }
//
//        $data->service = $service;
//
//        $data->custom_attributes = new stdClass();
//
//        if($target_point)
//            $data->custom_attributes->target_point = $target_point;
//
//        if($sending_method)
//            $data->custom_attributes->sending_method = $sending_method;
//
//        if($dropoff_point)
//            $data->custom_attributes->dropoff_point = $dropoff_point;
//
//        $resp = self::_call('v1/organizations/' . self::ORGANIZATION_ID . '/shipments', $data);
//        $id = $resp->id;
//
//        $limit = 0;
//        do
//        {
//            sleep(1);
//            $resp = self::_call('v1/shipments/'.$id, false);
//            $limit++;
//        }
//        while($resp->status == 'created' && $limit < 10);
//
//
//    }
//
//
//    public static function test2()
//    {
//
//        $data = new stdClass();
//
//
//        $data->parcels = [];
//
//        $parcel = new stdClass();
//        $parcel->dimensions = new stdClass();
//        $parcel->dimensions->length = 100;
//        $parcel->dimensions->width = 100;
//        $parcel->dimensions->height = 100;
//        $parcel->dimensions->unit = 'mm';
//        $parcel->weight = new stdClass();
//        $parcel->weight->amount = 1;
//        $parcel->weight->unit = 'kg';
////        $parcel->is_non_standard = false;
//
//        $data->parcels[] = $parcel;
//
//        $data->receiver = new stdClass();
//        $data->receiver->name = 'Jan Nowak';
//        $data->receiver->company_name = 'Inpost';
////        $data->receiver->first_name = 'Maciej';
////        $data->receiver->last_name = 'Prucnal';
//        $data->receiver->email = 'mprucnal@inpost.pl';
//        $data->receiver->phone = '123123123';
//        $data->receiver->address = new stdClass();
//        $data->receiver->address->line1 = 'Malborska 130';
//        $data->receiver->address->line2 = '';
////        $data->receiver->address->street = 'Cybernetyki';
////        $data->receiver->address->building_number = '21';
//        $data->receiver->address->city = 'Warszawa';
//        $data->receiver->address->post_code = '02-677';
//        $data->receiver->address->country_code = 'PL';
//
////        $data->sender = new stdClass();
////        $data->sender->name = 'Nazwa';
////        $data->sender->company_name = 'Nazwa';
////        $data->sender->first_name = 'Nazwa';
////        $data->sender->last_name = 'Nazwa';
////        $data->sender->email = 'a@a.pl';
////        $data->sender->phone = '123123123';
////        $data->sender->address = new stdClass();
////        $data->sender->address->line1 = 'ABC';
////        $data->sender->address->line2 = 'ABC';
////        $data->sender->address->city = 'Rybnik';
////        $data->sender->address->post_code = '44-207';
////        $data->sender->address->country_code = 'pl';
//
//
////        $data->cod = new stdClass();
////        $data->cod->amount = 0;
////        $data->cod->currency = 'PLN';
////
////        $data->insurance = new stdClass();
////        $data->insurance->amount = 0;
////        $data->insurance->currency = 'PLN';
//
//
////        $data->reference = 'ABC 123';
//
//
//
////        $data->service = 'inpost_locker_standard';
////        $data->service = 'inpost_locker_pass_thru';
//
//        $fromLocker = $toLocker = $dropoff_point = $sending_method = $target_point = false;
//
//        $locker_1 = 'KRA010';
//        $locker_2 = 'KRA012';
//
//
//        $fromLocker = false;
//        $toLocker = false;
//
//        if($fromLocker)
//        {
//            $sending_method = 'parcel_locker';
//            $dropoff_point = $locker_1;
//        }
//
//        if($toLocker)
//        {
//            $target_point = $locker_2;
//        }
//
//        if($fromLocker && $toLocker)
//        {
//            $service = self::SERVICE_TO_LOCKER_STD;
//        }
//        else if($fromLocker)
//        {
//            $service = self::SERVICE_STD;
//        }
//        else if($toLocker)
//        {
//            $service = self::SERVICE_TO_LOCKER_STD;
//        }
//        else
//        {
//            $service = self::SERVICE_STD;
//        }
//
////        $data->service = $service;
//        $data->additional_services = [];
//
//        $data->custom_attributes = new stdClass();
//
//        if($target_point)
//            $data->custom_attributes->target_point = $target_point;
//
//        if($sending_method)
//            $data->custom_attributes->sending_method = $sending_method;
//
//        if($dropoff_point)
//            $data->custom_attributes->dropoff_point = $dropoff_point;
//
//        var_dump($data);
//
//        echo '----------------------------------------------------------------------------------';
//
//
//
//
//
//        $resp = self::_call('v1/organizations/' . self::ORGANIZATION_ID . '/shipments', $data);
//
//        var_dump($resp);
//        $id = $resp->id;
////
////        echo '----------------------------------------------------------------------------------';
////        var_dump($resp);
//        echo '----------------------------------------------------------------------------------';
////
////
//        sleep(5);
////
//        $resp = self::_call('v1/shipments/'.$id, false);
//        var_dump($resp);
//
//        exit;
////        echo '----------------------------------------------------------------------------------';
////        $data->selected_offer = '61682632';
////        $resp = self::_call('v1/shipments/'.$id, $data, true);
////        var_dump($resp);
////        echo '----------------------------------------------------------------------------------';
////        $resp = self::_call('v1/shipments/'.$id, false);
////        var_dump($resp);
////        echo '----------------------------------------------------------------------------------';
//
////        $data = new stdClass();
////        $data->format = 'pdf';
////        $data->shipment_ids =[ $id ];
////
////        $resp = self::_call('v1/shipments/' . $id . '/labels', $data, true);
////
////        var_dump($resp);
//
//        $data = false;
//        var_dump('v1/shipments/' . $id . '/label?format=pdf');
//        $label = self::_call('v1/shipments/' . $id . '/label?format=pdf', $data, false, false);
//
//
//
//        file_put_contents('inlab_'.rand(9999,999999).'.pdf', $label);
//
//        var_dump($resp);
//
//        exit;
//    }

    protected static function _call($method, $data = false, $putMethod = false, $jsonDecode = true)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::URL.'/'.$method);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer '.self::KEY;

        MyDump::dump('inpost_shipx.txt', 'DATA: '.print_r($data,1));

        if($data)
            $data = json_encode($data);

//        MyDump::dump('inpost_shipx.txt', 'DATA-JSON: '.print_r($data,1));

        if($data)
        {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

            if($putMethod)
                curl_setopt($curl, CURLOPT_PUT, TRUE);
            else
                curl_setopt($curl, CURLOPT_POST, TRUE);

        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $resp = curl_exec($curl);
//        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//        $curl_error = curl_error($curl);

        curl_close($curl);

        $respDump = $resp;

        if(substr($respDump,0,4) == '%PDF')
            $respDump = '-- TRUNCATED LABEL --';

        MyDump::dump('inpost_shipx.txt', 'RESP: '.print_r($respDump,1));

        if($jsonDecode)
            $resp = json_decode($resp);
        else
            return $resp;

        return $resp;
    }

    protected function prepareData($skipService)
    {
        $data = new stdClass();

        $data->parcels = [];

        $parcel = new stdClass();
        $parcel->dimensions = new stdClass();
        $parcel->dimensions->length = $this->package_size_l * 10;
        $parcel->dimensions->width = $this->package_size_w * 10;
        $parcel->dimensions->height = $this->package_size_d * 10;
        $parcel->dimensions->unit = 'mm';
        $parcel->weight = new stdClass();
        $parcel->weight->amount = $this->package_weight;
        $parcel->weight->unit = 'kg';
//        $parcel->is_non_standard = false;

        $data->parcels[] = $parcel;

        $data->receiver = new stdClass();
        $data->receiver->name = $this->receiver_name;
        $data->receiver->company_name = $this->receiver_company != '' ? $this->receiver_company : $this->receiver_name;
        $data->receiver->email = $this->receiver_email;
        $data->receiver->phone =str_pad($this->receiver_tel,9,0,STR_PAD_LEFT);
        $data->receiver->address = new stdClass();
        $data->receiver->address->line1 = $this->receiver_address_1;
        $data->receiver->address->line2 = $this->receiver_address_2;
        $data->receiver->address->city = $this->receiver_city;
        $data->receiver->address->post_code = $this->receiver_postalCode;
        $data->receiver->address->country_code = strtoupper($this->receiver_countryCode);

        $data->sender = new stdClass();
        $data->sender->name = $this->sender_name;
        $data->sender->company_name = $this->sender_company != '' ? $this->sender_company : $this->sender_name;
        $data->sender->email = $this->sender_email;
        $data->sender->phone = str_pad($this->sender_tel,9,0,STR_PAD_LEFT);
        $data->sender->address = new stdClass();
        $data->sender->address->line1 = $this->sender_address_1;
        $data->sender->address->line2 = $this->sender_address_2;
        $data->sender->address->city = $this->sender_city;
        $data->sender->address->post_code = $this->sender_postalCode;
        $data->sender->address->country_code = strtoupper($this->sender_countryCode);

        if($this->_cod > 0)
        {
            $data->cod = new stdClass();
            $data->cod->amount = $this->_cod;
            $data->cod->currency = 'PLN';
        }

        if($this->_insurance > 0)
        {
            $data->insurance = new stdClass();
            $data->insurance->amount = $this->_insurance;
            $data->insurance->currency = 'PLN';
        }

        if($this->_pickup_locker_id)
        {
            $sending_method = 'parcel_locker';
            $dropoff_point = $this->_pickup_locker_id;
        }

        if($this->_delivery_locker_id)
        {
            $target_point = $this->_delivery_locker_id;
        }


        if($this->_pickup_locker_id && $this->_delivery_locker_id)
        {
            $service = self::SERVICE_TO_LOCKER_STD;
        }
        else if($this->_delivery_locker_id)
        {
            $service = self::SERVICE_TO_LOCKER_STD;

        }
        else if($this->_pickup_locker_id)
        {
            $service = self::SERVICE_FROM_LOCKER_TO_LOCKER;

            // CURRENTLY SENDING FROM LOCKER IS ALLOWED ONLY ASS "PASS_THROUG" SO DELIVERY LOCKER NEEDS TO BE TE SAME AS PICKUP
            $target_point = $this->_pickup_locker_id;
        }
        else
        {
            if($this->_express_10)
                $service = self::SERVICE_EXPRESS_10;
            else if($this->_express_12)
                $service = self::SERVICE_EXPRESS_12;
            else if($this->_express_17)
                $service = self::SERVICE_EXPRESS_17;
            else
                $service = self::SERVICE_STD;
        }

        if(!$skipService)
            $data->service = $service;

        $data->custom_attributes = new stdClass();

        if($target_point)
            $data->custom_attributes->target_point = $target_point;

        if($sending_method)
            $data->custom_attributes->sending_method = $sending_method;

        if($dropoff_point)
            $data->custom_attributes->dropoff_point = $dropoff_point;

        return $data;
    }

    public function createShipment($returnError = false)
    {

        $data = $this->prepareData(false);

        $addServices = [];

        if ($this->_rod)
            $addServices[] = 'rod';

        if ($this->sms_nofitication)
            $addServices[] = 'sms';

        if ($this->mail_notification)
            $addServices[] = 'EMAIL';

        if (S_Useful::sizeof($addServices))
            $data->additional_services = $addServices;


        $resp = self::_call('v1/organizations/' . self::ORGANIZATION_ID . '/shipments', $data);

        if ($resp->error) {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->message . ' : ' . str_replace(['Array', 'stdClass Object'], '', print_r($resp->details, 1)));
            } else {
                return false;
            }
        }


        $id = $resp->id;
        $limit = 0;
        do {
            sleep(2);
            $resp = self::_call('v1/shipments/' . $id, false);
            $limit++;
        } while ($resp->status != 'confirmed' && $limit < 15);

        if ($resp->error) {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->message . ' : ' . str_replace(['Array', 'stdClass Object'], '', print_r($resp->details, 1)));
            } else {
                return false;
            }
        }


        $data = false;

        $ttNumber = $resp->tracking_number;

        $label = self::_call('v1/shipments/' . $id . '/label?format=pdf', $data, false, false);


        if (strlen($label) < 10000) // there is no label, so we expect error
        {
            $label = json_decode($label);

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($label->message . ' : ' . str_replace(['Array', 'stdClass Object'], '', print_r($label->details, 1)));
            } else {
                return false;
            }

        }

        $return = [];
        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        $im->readImageBlob($label);
        $im->setImageFormat('png8');

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

//        if($this->label_annotation_text)
//            LabelAnnotation::createAnnotation($this->label_annotation_text, $im, 1015,730);

        $im->trimImage(0);
        if ($im->getImageWidth() > $im->getImageHeight())
            $im->rotateImage(new ImagickPixel(), 90);
        $im->stripImage();


        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber, false, false, 'Inpost', 'quequeCollectionForCourier');

        return $return;

    }

    public static function getAdditions(Courier $courier)
    {
        $additions = [];

        $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_ROD;
        $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_1000;
        $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_5000;
        $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_10000;
        $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_20000;

//        $toLocker = false;
//        if(
//            $courier->getType() == Courier::TYPE_INTERNAL && (
//                $courier->courierTypeInternal->_inpost_locker_active && $courier->courierTypeInternal->_inpost_locker_delivery_id)
//            OR
//            $courier->getType() == Courier::TYPE_U && (
//                $courier->_inpost_locker_active && $courier->_inpost_locker_delivery_id)
//
//        )
//        {
//            $toLocker = true;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_SIZE_B;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_SIZE_C;
//        }

//        $model->fillData($courier, $courier->senderAddressData, $courier->receiverAddressData);
//        $services = $model->getServices();
//
//        if(is_array($services)) {
//            $services = array_flip($services);
//
//            if(isset($services['inpost_courier_express_1000']))
//                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_10;
//
//            if(isset($services['inpost_courier_express_1200']))
//                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_12;
//
//            if(isset($services['inpost_courier_express_1700']))
//                $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_17;
//        }

        $array = implode(',', $additions);


        $CACHE_NAME = 'INPST_SHIPX_ADDITIONS_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;
        $model->label_annotation_text = CourierOperator::getLabelAnnotationByUniqId($uniq_id, GLOBAL_BROKERS::BROKER_INPOST_SHIPX);

        if($courierInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_INPOST)
            $model->_pickup_locker_id = $courierInternal->pickup_point_id;

        if($courierInternal->_inpost_locker_active && $courierInternal->_inpost_locker_delivery_id)
            $model->_delivery_locker_id = $courierInternal->_inpost_locker_delivery_id;

        $model->fillData($courierInternal->courier, $from, $to);

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_ROD))
            $model->_rod = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_NOTIFICATION_EMAIL))
            $model->mail_notification = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_NOTIFICATION_SMS))
            $model->sms_notification = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_1000))
            $model->_insurance = 1000;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_5000))
            $model->_insurance = 5000;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_10000))
            $model->_insurance = 10000;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_20000))
            $model->_insurance = 20000;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_10))
            $model->_express_10 = true;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_12))
            $model->_express_12 = true;
        else if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_17))
            $model->_express_17 = true;



        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;
        $model->label_annotation_text = $courierU->courierUOperator->label_symbol;

        if($courierU->courier->_inpost_locker_active && $courierU->courier->_inpost_locker_delivery_id)
            $model->_delivery_locker_id = $courierU->courier->_inpost_locker_delivery_id;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->fillData($courierU->courier, $from, $to);

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_ROD))
            $model->_rod = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_NOTIFICATION_EMAIL))
            $model->mail_notification = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_NOTIFICATION_SMS))
            $model->sms_notification = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_1000))
            $model->_insurance = 1000;
        else if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_5000))
            $model->_insurance = 5000;
        else if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_10000))
            $model->_insurance = 10000;
        else if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_INSURANCE_20000))
            $model->_insurance = 20000;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_10))
            $model->_express_10 = true;
        else if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_12))
            $model->_express_12 = true;
        else if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_INPOST_SHIPX_EXPRESS_17))
            $model->_express_17 = true;

        return $model->createShipment($returnErrors);
    }



    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        $resp = self::_call('v1/tracking/' . $no, false);

        if(!$resp->tracking_details)
            return false;

        $statuses = new GlobalOperatorTtResponseCollection();

        if(is_array($resp->tracking_details))
            foreach($resp->tracking_details AS $item)
            {

                $date = (string) $item->datetime;
                $date = str_replace('T', ' ', $date);
                $date = substr($date,0,19);

                $name = self::_getStatFullText((string) $item->status);

                $statuses->addItem($name, $date, (string) $item->agency);
            }

        return  $statuses;
    }

    protected static function _getStatFullText($stat)
    {
        $CACHE_NAME = 'INPOST_SHIPX_STATUSES';

        $statuses = Yii::app()->cache->get($CACHE_NAME);

        if($statuses === false) {

            $resp = self::_call('v1/statuses', false);
            $statuses = [];

            if(is_array($resp->items))
                foreach($resp->items AS $item)
                    $statuses[$item->name] = $item->title;
//                    $statuses[$item->name] = mb_strlen($item->description) > 256 ? $item->title : $item->description;

            Yii::app()->cache->set($CACHE_NAME, $statuses, 60*60*24);
        }

        return isset($statuses[$stat]) ? $statuses[$stat] : $stat;
    }

}