<?php
/* @var $model Postal */
/* @var $currency integer */
?>

<div class="form">

    <h2><?php echo Yii::t('postal','Podsumowanie');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '2/2')); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'postal-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
    $this->widget('FlashPrinter');
    ?>


    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('postal','Parametry przesyłki');?></h4>
            <table class="detail-view" style="width: 100%; margin: 0 auto;">
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('postal_type');?></th>
                    <td><?php echo Postal::getPostalTypeList()[$model->postal_type]; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo $model->getAttributeLabel('size_class');?></th>
                    <td><?php echo Postal::getSizeClassList()[$model->size_class]; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('weight');?></th>
                    <td><?php echo $model->weight; ?> g</td>
                </tr>
                <tr class="even">
                    <th><?php echo $model->getAttributeLabel('content');?></th>
                    <td><?php echo $model->content; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('value');?></th>
                    <td><?php echo $model->value ? $model->value : '-'; ?> <?php echo $model->value_currency; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo $model->getAttributeLabel('note1');?></th>
                    <td><?php echo $model->note1; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo $model->getAttributeLabel('note2');?></th>
                    <td><?php echo $model->note2; ?></td>
                </tr>
            </table>
        </div>
        <?php
        if(!Yii::app()->user->model->getHidePrices()):
            ?>
            <div class="col-xs-12 col-sm-6">
                <h4><?php echo Yii::t('postal','Cena');?></h4>
                <table class="detail-view" style="width: 100%; margin: 0 auto;">
                    <tr class="odd">
                        <th><?php echo Yii::t('postal','Cena');?></th>
                        <td><?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($price,true));?> / <?php echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice(($price)));?>  <?php echo Yii::app()->PriceManager->getCurrencySymbol();?> <?php echo Yii::t('courier','netto/brutto');?></td>
                    </tr>
                </table>
            </div>
        <?php
        endif;
        ?>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('postal','Nadawca');?></h4>

            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));

            ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('postal','Odbiorca');?> </h4>
            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));

            ?>
        </div>

    </div>

    <h3 class="text-center"><?php echo Yii::t('site', 'Regulamin');?></h3>

    <div class="row">
        <table class="detail-view">
            <tr class="odd">
                <td><?php echo $form->labelEx($model,'regulations'); ?>
                    <?php echo $form->checkbox($model, 'regulations'); ?>
                </td>
            </tr>
            <tr class="even">
                <td><?php echo $form->labelEx($model,'regulations_rodo'); ?>
                    <?php echo $form->checkbox($model, 'regulations_rodo'); ?>
                </td>
            </tr>
        </table>

        <?php echo $form->error($model,'regulations'); ?>
    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step4', 'class' => 'btn btn-sm'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę'),array('name'=>'finish', 'class' => 'btn btn-primary'));
        $this->endWidget();
        ?>
    </div>

    <div class="info">
        <p><?php echo Yii::t('postal','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>
    </div>
</div><!-- form -->