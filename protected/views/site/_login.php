<div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>false,
        ),
        'action'=>$this->createAbsoluteUrl('/site/login'),
    )); ?>


    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username'); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password'); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>

    <div class="row buttons" style="text-align: center;">
        <?php echo TbHtml::submitButton(Yii::t('app','Zaloguj')); ?>
    </div>
    <p class="note"><?php echo Yii::t('form','Pola z {html} są wymagane.', array('{html}' => '<span class="required">*</span>'));?></p>

    <div class="navigate">

        <div style="text-align: center; clear: both;">
            <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_PLI):?>
            <?php echo CHtml::link(Yii::t('site','Chcę założyć nowe konto'), Yii::app()->createUrl('user/create'), array('class' => 'btn', 'style' => 'font-size: 10px;'));?><?php endif;?>
            <?php echo CHtml::link(Yii::t('site','Nie pamiętam loginu/hasła'), Yii::app()->createUrl('user/reminder'), array('class' => 'btn', 'style' => 'font-size: 10px;'));?>
        </div>

    </div>
    <?php if(isset($model->returnURL)) echo CHtml::hiddenField('returnURL',$model->returnURL); ?>



    <?php $this->endWidget(); ?>
</div><!-- form -->