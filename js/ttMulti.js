(function() {

    var $actionButtons;
    var $tooMuchWarning;

    $(document).ready(function(){

        $itemsIds = $('#ids');

        $itemsIds.on('change keyup', countLines);
        $itemsIds.on('focusout', filterLines);

        $actionButtons = $('#action-buttons');
        $tooMuchWarning = $('#too-much-warning');

    });

    function countLines()
    {
        var text = $itemsIds.val();

        var count = 0;
        if(text != '') {
            var lines = text.split(/\r|\r\n|\n/);
            count = lines.length;
        }

        if(count > 25)
        {
            $actionButtons.hide();
            $tooMuchWarning.show();
        } else {
            $actionButtons.show();
            $tooMuchWarning.hide();
        }

        $('#numer-of-rows').html(count);

        return count;
    }

    function filterLines()
    {
        var text = $(this).val();

        text = text.replace(/(?:(?:\r\n|\r|\n)\s*){2}/gm, "\r\n");
        text = text.replace(/(?:(?:\r\n|\r|\n)\s*)$/gm, "");
        text = text.replace(/^(?:(?:\r\n|\r|\n)\s*)/gm, "");
        $(this).val(text).trigger('change');
    }

})();