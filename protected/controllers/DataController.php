<?php

//http://localhost/SwiatPrzesylek.pl/swiatPrzesylek_ms5//data/postals?pass=3274fbc364d5d463141d9463ebeeb0f4&date=2016-04-29

class DataController extends CController {

    const PASSWORD = 'cky^dm@x0a';
    const PAGE_SIZE = 500;

    const INPUT_IDS_SIZE = 1000;

    public function filters()
    {
        return array(
            //'postOnly + index',
        );
    }

//    public function actionRead()
//    {
//
//        $date = date('Y-m-d');
//        $date = strtotime ( '-5 day' , strtotime ( $date ) ) ;
//        $date = date ( 'Y-m-d' , $date );
//
//        $ch = curl_init();
//        $params = array(
//            "pass"=> md5(self::PASSWORD),
////            "date" => $date,
//            "page" => 0,
////            "id" => '483160113126658',
//            "ids" => [
//                '483150312006609',
//                ],
//        );
//
//        curl_setopt($ch,CURLOPT_URL,  'http://localhost/swiatPrzesylek_ms5/data/lastStat');
//        curl_setopt($ch,CURLOPT_POST,true);
//        curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($params));
//        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//        $result = curl_exec($ch);
//
//        $result = simplexml_load_string($result);
//
//        var_dump(md5(self::PASSWORD));
//
//        print_r($result);
//    }
//
//    protected static function validateDate($date)
//    {
//        $d = DateTime::createFromFormat('Y-m-d', $date);
//        return $d && $d->format('Y-m-d') == $date;
//    }


    public static function actionDomains()
    {

        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        }
        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            $xml_output .= '<items>' . "\n";

            foreach(PageVersion::pageVersionList() AS $id => $name)
            {
                $xml_output .= '<domain>' . "\n";
                $xml_output .= '<id>' . $id . '</id>' . "\n";
                $xml_output .= '<name>' . $name . '</name>' . "\n";
                $xml_output .= '<url>' . PageVersion::pv2Domain($id) . '</url>' . "\n";
                $xml_output .= '</domain>' . "\n";
            }

            $xml_output .= '</items>' . "\n";
        }

        $xml_output .= "</response>";

        echo $xml_output;
        Yii::app()->end();
    }

    public static function actionUserCounter()
    {

        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $date = $post['date'];

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        }
        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {


            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from((new User)->tableName());
            if ($date)
                $cmd->where('date_entered LIKE :date', [':date' => $date . "%"]);
            $count = $cmd->queryScalar();

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('MAX(id)');
            $cmd->from((new User)->tableName());
            $max = $cmd->queryScalar();

            $xml_output .= '<count>' . $count . '</count>' . "\n";
            $xml_output .= '<date>' . $date . '</date>' . "\n";
            $xml_output .= '<max_id>' . $max . '</max_id>' . "\n";
        }

        $xml_output .= "</response>";

        echo $xml_output;
        Yii::app()->end();
    }

    public static function actionUser()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $id = $post['id'];

        $id = intval($id);

        if($pass != md5(self::PASSWORD))
        {
            $stat = false;
            $log = 'Invalid password';
        }
        else if(!$id)
        {
            $stat = false;
            $log = 'Invalid ID';
        }


        if($stat) {
            /* @var $model User */
            $model = User::model()->findByPk($id);

            if ($model === NULL) {
                $stat = false;
                $log = 'User not found';
            }
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>'.($stat ? 'OK' : 'FAIL').'</status>'."\n";
        $xml_output .= '<log>'.$log.'</log>'."\n";

        if($stat)
        {
            $xml_output .= '<user>'."\n";

            $attributes = [
                'id' => 'id',
                'login' => 'login',
                'email' => 'email',
                'date_entered' => 'date_entered',
                'name' => 'name',
                'surname' => 'surname',
                'company' => 'company',
                'country0.codeForMatrix' => 'country',
                'city' => 'city',
                'zip_code' => 'zip_code',
                'address_line_1' => 'address_line_1',
                'address_line_2' => 'address_line_2',
                'tel' => 'tel',
                'nip' => 'nip',
                'vies' => 'vies',
                'without_vat' => 'without_vat',
                'payment_on_invoice' => 'payment_on_invoice',
                'source_domain' => 'source_domain',
                'partner_id' => 'partner_id',
//                'priceCurrency.short_name' => 'currency',
            ];


            foreach ($attributes AS $key => $item) {

                $attribute = explode('.', $key);

                if (S_Useful::sizeof($attribute) > 1) {
                    // PHP 7 FIX
                    $temp1 = $attribute[0];
                    $temp2 = $attribute[1];

                    $value = $model->$temp1->$temp2;
                } else {
                    // PHP 7 FIX
                    $temp1 = $attribute[0];
                    $value = $model->$temp1;
                }


                $xml_output .= '<' . $item . '>' . CHtml::encode($value) . '</' . $item . '>' . "\r\n";
            }

            $salesman = $model->salesman;

            $xml_output .= '<currency>' . CHtml::encode(PriceCurrency::nameById($model->price_currency_id)) . '</currency>' . "\r\n";
            $xml_output .= '<salesman_id>' . CHtml::encode($salesman ? $salesman->id : NULL) . '</salesman_id>' . "\r\n";
            $xml_output .= '<salesman_name>' . CHtml::encode($salesman ? $salesman->name : NULL) . '</salesman_name>' . "\r\n";
            $xml_output .= '<salesman_email>' . CHtml::encode($salesman ? $salesman->email : NULL) . '</salesman_email>' . "\r\n";
            $xml_output .= '<sp_zoo>' . CHtml::encode($model->getSpZooStatus()) . '</sp_zoo>' . "\r\n";
            $xml_output .= '<sp_zoo_date>' . CHtml::encode($model->getSpZooChangeDate()) . '</sp_zoo_date>' . "\r\n";
            $xml_output .= '<injection_sp_point_id>' . CHtml::encode($model->injection_sp_point_id) . '</injection_sp_point_id>' . "\r\n";
            $xml_output .= '<injection_sp_point_country>' . CHtml::encode($model->injection_sp_point_id ? SpPoints::getPoint($model->injection_sp_point_id)->addressData->country0->code : NULL) . '</injection_sp_point_country>' . "\r\n";
            $xml_output .= "</user>\n";
        }

        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }

    // COURIER:
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    public static function actionLastStat()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $ids = $post['ids'];
        $id = $post['id'];

        if(!is_array($ids))
            $ids = explode(',', $ids);

        if($pass != md5(self::PASSWORD))
        {
            $stat = false;
            $log = 'Invalid password';
        }
        else if($id == '' && !is_array($ids))
        {
            $stat = false;
            $log = 'No packages IDs provided';
        }
        else if($id == '' && !S_Useful::sizeof($ids))
        {
            $stat = false;
            $log = 'No packages IDs provided';
        }
        else if($id == '' && S_Useful::sizeof($ids) > self::INPUT_IDS_SIZE)
        {
            $stat = false;
            $log = 'Too many IDs provided (limit is '.self::INPUT_IDS_SIZE.')';
        }

//        array_walk($ids, function(&$item, $key){
//            $item = preg_replace("/[^a-zA-Z0-9]+/", "", $item);
//        });

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>'.($stat ? 'OK' : 'FAIL').'</status>'."\n";
        $xml_output .= '<log>'.$log.'</log>'."\n";

        if($stat)
        {

            if($id != '')
            {
                $ids = [$id];
            } else {
                $ids = array_unique($ids);
            }



            Yii::app()->getModule('courier');
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('local_id, courier_stat_id, location, stat_map_id');
            $cmd->from((new Courier())->tableName());
            $cmd->where(['in', 'local_id', $ids]);
            $result = $cmd->queryAll();

            $temp = [];
            $location = [];
            $stat_map_id = [];
            foreach($result AS $key => $item) {
                $temp[$item['local_id']] = $item['courier_stat_id'];
                $location[$item['local_id']] = $item['location'];
                $stat_map_id[$item['local_id']] = $item['stat_map_id'];
            }

            $xml_output .= '<last-stats>'."\n";
            foreach($ids AS $id)
            {
                if(!isset($temp[$id]))
                    continue;

                $xml_output .= '<package local_id="' . CHtml::encode($id) . '">' . "\r\n";
                $xml_output .= '<courier-stat-id>' . CHtml::encode($temp[$id]) . '</courier-stat-id>' . "\r\n";
                $xml_output .= '<courier-stat-location>' . CHtml::encode($location[$id]) . '</courier-stat-location>' . "\r\n";
                $xml_output .= '<courier-stat-map-id>' . CHtml::encode($stat_map_id[$id]) . '</courier-stat-map-id>' . "\r\n";
                $xml_output .= '</package>' . "\r\n";
            }


            $xml_output .= "</last-stats>\n";
        }

        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }


    public static function actionLastStatPerOperator()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $ids = $post['ids'];
        $id = $post['id'];

        if(!is_array($ids))
            $ids = explode(',', $ids);

        if($pass != md5(self::PASSWORD))
        {
            $stat = false;
            $log = 'Invalid password';
        }
        else if($id == '' && !is_array($ids))
        {
            $stat = false;
            $log = 'No packages IDs provided';
        }
        else if($id == '' && !S_Useful::sizeof($ids))
        {
            $stat = false;
            $log = 'No packages IDs provided';
        }
        else if($id == '' && S_Useful::sizeof($ids) > self::INPUT_IDS_SIZE)
        {
            $stat = false;
            $log = 'Too many IDs provided (limit is '.self::INPUT_IDS_SIZE.')';
        }

//        array_walk($ids, function(&$item, $key){
//            $item = preg_replace("/[^a-zA-Z0-9]+/", "", $item);
//        });

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>'.($stat ? 'OK' : 'FAIL').'</status>'."\n";
        $xml_output .= '<log>'.$log.'</log>'."\n";

        if($stat)
        {

            Yii::app()->getModule('courier');

            if($id != '')
            {
                $ids = [$id];
            } else {
                $ids = array_unique($ids);
            }


            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id, local_id');
            $cmd->from('courier');
            $cmd->where(['in', 'local_id', $ids]);
            $result = $cmd->queryAll();

            $xml_output .= '<last-stats-per-operator>'."\n";
            foreach($result AS $item)
            {
                $id = $item['id'];


                $sql = 'SELECT p1.status_date, p1.current_stat_id, p1.author, p1.location, smm.map_id
                    FROM courier_stat_history p1
                    INNER JOIN
                            (
                                SELECT MAX(status_date) MaxDate, author
                        FROM courier_stat_history
                        WHERE courier_id = "'.$id.'"
                            AND author > 0 AND author < 1000
                        GROUP BY author
                    ) p2
                      ON p1.author = p2.author
                            AND p1.status_date = p2.MaxDate
                            JOIN stat_map_mapping smm ON smm.type = 1 AND stat_id = p1.current_stat_id
                    WHERE courier_id = "'.$id.'"';

                $connection = Yii::app()->db;
                $command = $connection->createCommand($sql);
                $dataReader = $command->query();
                $rows = $dataReader->readAll();


                $xml_output .= '<package local_id="' . CHtml::encode($item['local_id']) . '">' . "\r\n";

                foreach($rows AS $item2)
                {
                    $xml_output .= '<stat-per-operator>' . "\r\n";
                    $xml_output .= '<operator>' . CHtml::encode(CourierStat::getExternalServiceNameById($item2['author'])) . '</operator>' . "\r\n";
                    $xml_output .= '<stat_id>' . CHtml::encode($item2['current_stat_id']) . '</stat_id>' . "\r\n";
                    $xml_output .= '<stat_date>' . CHtml::encode($item2['status_date']) . '</stat_date>' . "\r\n";
                    $xml_output .= '<location>' . CHtml::encode($item2['location']) . '</location>' . "\r\n";
                    $xml_output .= '<stat-map-id>' . CHtml::encode($item2['map_id']) . '</stat-map-id>' . "\r\n";
                    $xml_output .= '</stat-per-operator>' . "\r\n";
                }

                $xml_output .= '</package>' . "\r\n";

            }
            $xml_output .= "</last-stats-per-operator>\n";


        }

        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }


    public static function actionStat()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $id = $post['id'];

        $id = intval($id);

        if($pass != md5(self::PASSWORD))
        {
            $stat = false;
            $log = 'Invalid password';
        }
        else if(!$id)
        {
            $stat = false;
            $log = 'Invalid ID';
        }


        if($stat) {
            Yii::app()->getModule('courier');

            /* @var $model CourierStat */
            $model = CourierStat::model()->findByPk($id);

            if ($model === NULL) {
                $stat = false;
                $log = 'Stat not found';
            }
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>'.($stat ? 'OK' : 'FAIL').'</status>'."\n";
        $xml_output .= '<log>'.$log.'</log>'."\n";

        if($stat)
        {
            $xml_output .= '<stat>'."\n";
            $xml_output .= '<name>' . CHtml::encode($model->name) . '</name>' . "\r\n";
            $xml_output .= '<stat_map_id>' . CHtml::encode($model->getStatMapId()) . '</stat_map_id>' . "\r\n";
            $xml_output .= '<stat_map_name>' . CHtml::encode($model->getStatMapName()) . '</stat_map_name>' . "\r\n";
            $xml_output .= "</stat>\n";
        }

        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }

    public static function actionPackages()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $date = $post['date'];
        $page = $post['page'];

        $page = intval($page);

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        } else if (!$date) {
            $stat = false;
            $log = 'Invalid date format';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            Yii::app()->getModule('courier');

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from((new Courier)->tableName());
            $cmd->where('date_entered LIKE :date', [':date' => $date . "%"]);
            $count = $cmd->queryScalar();

            $totalPages = ceil($count / self::PAGE_SIZE);

            if ($page > ($totalPages - 1))
                $page = 0;

            $xml_output .= '<count>' . $count . '</count>' . "\n";
            $xml_output .= '<totalPages>' . $totalPages . '</totalPages>' . "\n";
            $xml_output .= '<page>' . $page . '</page>' . "\n";

            $offset = $page * self::PAGE_SIZE;

            $models = Courier::model()->findAll(['condition' => 't.date_entered LIKE :date', 'limit' => self::PAGE_SIZE, 'offset' => $offset, 'order' => 't.id DESC', 'params' => [':date' => $date . "%"]]);
//
//            file_put_contents('ooooooooo.txt', print_r($models,1));

            $xml_output .= "<items>\n";


            $attributesPackage = [
                'local_id' => 'local_id',
                'date_entered' => 'date_entered',
                'courier_stat_id' => 'courier_stat_id',
                'package_weight' => 'package_weight',
                'package_size_l' => 'package_size_l',
                'package_size_w' => 'package_size_w',
                'package_size_d' => 'package_size_d',
//                'packages_number' => 'packages_number',
                'package_value' => 'package_value',
                'value_currency' => 'value_currency',
                'package_content' => 'package_content',
                'user_id' => 'user_id',
                'stat_map_id' => 'stat_map_id',
                'ref' => 'ref',
                'note1' => 'note1',
                'note2' => 'note2',

            ];
            $attributesAddressData = [
                'name' => 'name',
                'company' => 'company',
                'country0.codeForMatrix' => 'country',
                'city' => 'city',
                'zip_code' => 'zip_code',
                'address_line_1' => 'address_line_1',
                'address_line_2' => 'address_line_2',
                'tel' => 'tel',
            ];

            /* @var $model Courier */
            foreach ($models AS $model) {
                // EOBUWIE MOD @01.08.2018
                $forceReturn = false;

                $xml_output .= "<item>\n";

                if ($model->senderAddressData == NULL)
                    $model->senderAddressData = new AddressData;

                if ($model->receiverAddressData == NULL)
                    $model->receiverAddressData = new AddressData;

                // PACKAGE

                $xml_output .= '<package>' . "\r\n";
                foreach ($attributesPackage AS $key => $item) {
                    $attribute = explode('.', $key);

                    $value = $model;
                    foreach ($attribute AS $attributeName)
                        $value = $value->$attributeName;


                    // EOBUWIE MOD @01.08.2018
                    $EOBUWIE_SOURCE_NORMAL = 947;
                    $EOBUWIE_SOURCE_TO_RET = 948;

                    $EOBUWIE_TARGET_NORMAL = 2036;
                    $EOBUWIE_TARGET_TO_RET = 2036;

                    if($item == 'user_id' && $model->date_entered >= '2018-08-01')
                    {
                        if($value == 571 && $model->date_entered >= '2018-11-27') // LITM BOD @ 27.11.2018
                        {
                          $value = 2340;
                        }

                        if($value == $EOBUWIE_SOURCE_NORMAL)
                            $value = $EOBUWIE_TARGET_NORMAL;
                        else if($value == $EOBUWIE_SOURCE_TO_RET)
                        {
                            $value = $EOBUWIE_TARGET_TO_RET;
                            $forceReturn = true;
                        }
                    }

                    $xml_output .= '<' . $item . '>' . CHtml::encode($value) . '</' . $item . '>' . "\r\n";
                }

                // EOBUWIE MOD @01.08.2018
                if($forceReturn)
                    $xml_output .= '<type>' . CHtml::encode('ret.') . '</type>' . "\r\n";
                else
                    $xml_output .= '<type>' . CHtml::encode($model->getTypeName()) . '</type>' . "\r\n";

                $xml_output .= '</package>' . "\r\n";

                // OPERATORS & REMOTE IDS
                $xml_output .= '<operators>' . "\r\n";


                $remoteIds = [];
                $operators = [];

                if ($model->getType() == Courier::TYPE_INTERNAL_SIMPLE) {
                    array_push($operators, $model->courierTypeInternalSimple->getOperator());
                    if($model->courierTypeInternalSimple->courierExtOperatorDetails !== NULL)
                        array_push($remoteIds, $model->courierTypeInternalSimple->courierExtOperatorDetails->getTrack_id());
                    else
                        array_push($remoteIds, '');
                } else if ($model->getType() == Courier::TYPE_DOMESTIC) {
                    array_push($operators, $model->courierTypeDomestic->getOperator());
                    array_push($remoteIds, $model->courierTypeDomestic->courierLabelNew->track_id);
                } else if ($model->getType() == Courier::TYPE_U) {
                    array_push($operators, $model->courierTypeU->courierUOperator->name);
                    array_push($remoteIds, $model->courierTypeU->courierLabelNew->track_id);
                } else if ($model->getType() == Courier::TYPE_INTERNAL_OOE) {
                    array_push($operators, $model->courierTypeOoe->service_code);
                    array_push($remoteIds, $model->courierTypeOoe->remote_id);
                } else if ($model->getType() == Courier::TYPE_EXTERNAL_RETURN) {
                    if($model->courierTypeExternalReturn->courierLabelNew)
                    {
                        array_push($operators, $model->courierTypeExternalReturn->courierLabelNew->_ext_operator_name);
                        array_push($remoteIds, $model->courierTypeExternalReturn->courierLabelNew->track_id);
                    }
                } else if ($model->getType() == Courier::TYPE_RETURN) {
                    if($model->courierTypeReturn->courierLabelNew)
                    {
                        $operatorsTemp = CourierLabelNew::getOperators();
                        $operatorTemp = isset($operatorsTemp[$model->courierTypeReturn->courierLabelNew->operator]) ? $operatorsTemp[$model->courierTypeReturn->courierLabelNew->operator] : NULL;

                        array_push($operators, $operatorTemp);
                        array_push($remoteIds, $model->courierTypeReturn->courierLabelNew->track_id);
                    }
                } else if ($model->getType() == Courier::TYPE_EXTERNAL) {

                        array_push($operators, $model->courierTypeExternal->courierExternalOperator->name);
                        array_push($remoteIds, $model->courierTypeExternal->external_id);

                } else if ($model->getType() == Courier::TYPE_INTERNAL) {

                    $gotCommonRemoteId = false;
                    if ($model->courierTypeInternal->commonLabel !== NULL) {
                        $gotCommonRemoteId = true;
                        array_push($remoteIds, $model->courierTypeInternal->commonLabel->getTrack_id());
                    }

                    if ($model->courierTypeInternal->pickup_operator_name != '') {

                        $pon = $model->courierTypeInternal->pickup_operator_name;
                        if($model->senderAddressData->country_id != CountryList::COUNTRY_PL &&
                            $model->receiverAddressData->country_id == CountryList::COUNTRY_PL &&
                            in_array($model->courierTypeInternal->pickup_operator_id, [CourierOperator::OPERATOR_UPS_F])
                        )
                            $pon = 'UPS_F_Import';

                        array_push($operators, $pon);
                        if ($model->courierTypeInternal->pickupLabel !== NULL)
                            array_push($remoteIds, $model->courierTypeInternal->pickupLabel->getTrack_id());
                        else if(!$gotCommonRemoteId)
                            array_push($remoteIds, '');
                    }

                    if ($model->courierTypeInternal->delivery_operator_name != '') {
                        array_push($operators, $model->courierTypeInternal->delivery_operator_name);
                        if ($model->courierTypeInternal->deliveryLabel !== NULL)
                            array_push($remoteIds, $model->courierTypeInternal->deliveryLabel->getTrack_id());
                        else if(!$gotCommonRemoteId)
                            array_push($remoteIds, '');
                    }


                }

                if (is_array($operators)) {
                    $operators = array_unique($operators);
                    foreach ($operators AS $operator)
                        $xml_output .= '<operator>' . CHtml::encode($operator) . '</operator>' . "\r\n";
                }

                $xml_output .= '</operators>' . "\r\n";


                $xml_output .= '<remote_ids>' . "\r\n";


                if (is_array($remoteIds)) {
                    $remoteIds = array_unique($remoteIds);
                    foreach ($remoteIds AS $remoteId) {
                        if ($remoteId != '')
                            $xml_output .= '<remote_id>' . CHtml::encode($remoteId) . '</remote_id>' . "\r\n";
                    }
                }
                $xml_output .= '</remote_ids>' . "\r\n";

                $pickupHub = $deliveryHub = NULL;
                if(S_Useful::sizeof($remoteIds) == 2 && !$forceReturn)      // EOBUWIE MOD @01.08.2018
                {
                    $pickupHubModel = $model->courierTypeInternal->pickupHub;
                    $pickupHub = "\r\n" . '<id>'.CHtml::encode($pickupHubModel->id).'</id>' ."\r\n";
                    $pickupHub .= '<name>'.CHtml::encode($pickupHubModel->name).'</name>' ."\r\n";
                    $pickupHub .= '<country>'.CHtml::encode($pickupHubModel->address->country0->codeForMatrix).'</country>' ."\r\n";

                    $deliveryHubModel = $model->courierTypeInternal->deliveryHub;
                    $deliveryHub = "\r\n" . '<id>'.CHtml::encode($deliveryHubModel->id).'</id>' ."\r\n";
                    $deliveryHub .= '<name>'.CHtml::encode($deliveryHubModel->name).'</name>' ."\r\n";
                    $deliveryHub .= '<country>'.CHtml::encode($deliveryHubModel->address->country0->codeForMatrix).'</country>' ."\r\n";
                }
                elseif($model->getType() == Courier::TYPE_INTERNAL && in_array($model->courierTypeInternal->with_pickup, [CourierTypeInternal::WITH_PICKUP_NONE]) && !$forceReturn)      // EOBUWIE MOD @01.08.2018
                {
                    $pickupHubModel = $model->courierTypeInternal->deliveryHub; // for "without pickup" delivery hub is the first hub, so pickup hub really...
                    $pickupHub = "\r\n" . '<id>'.CHtml::encode($pickupHubModel->id).'</id>' ."\r\n";
                    $pickupHub .= '<name>'.CHtml::encode($pickupHubModel->name).'</name>' ."\r\n";
                    $pickupHub .= '<country>'.CHtml::encode($pickupHubModel->address->country0->codeForMatrix).'</country>' ."\r\n";
                }

                $xml_output .= '<operators_and_remote_ids>' . "\r\n";

                $first = true;
                foreach ($remoteIds AS $key => $remoteId) {
                    $xml_output .= '<item>' . "\r\n";
                    $xml_output .= '<operator>'.CHtml::encode($operators[$key]).'</operator>' . "\r\n";
                    $xml_output .= '<id>'.CHtml::encode($remoteId).'</id>' . "\r\n";
                    $xml_output .= '<hub>'.($first ? $pickupHub : $deliveryHub).'</hub>' . "\r\n";
                    $xml_output .= '</item>' . "\r\n";

                    $first = false;
                }
                $xml_output .= '</operators_and_remote_ids>' . "\r\n";

                // COD
                $xml_output .= '<cod>' . "\r\n";
                $xml_output .= '<value>' . CHtml::encode($model->cod_value) . '</value>' . "\r\n";
                $xml_output .= '<currency>' . CHtml::encode($model->cod_currency) . '</currency>' . "\r\n";
                $xml_output .= '<bankAccount>' . CHtml::encode($model->senderAddressData->bankAccount) . '</bankAccount>' . "\r\n";

                $xml_output .= '</cod>' . "\r\n";


                // ADDITIONS
                $xml_output .= '<additions>' . "\r\n";

                $additions = [];
                if ($model->getType() == Courier::TYPE_INTERNAL && !$forceReturn) {      // EOBUWIE MOD @01.08.2018
                    $additions = $model->courierTypeInternal->listAdditions();
                } else if ($model->getType() == Courier::TYPE_DOMESTIC) {
                    $additions = $model->courierTypeDomestic->listAdditions();
                } else if ($model->getType() == Courier::TYPE_U) {
                    $additions = $model->courierTypeU->listAdditions();
                }

                if (is_array($additions))
                    foreach ($additions AS $addition) {
                        // Don't pass it to MATRIX - @14.05.2018
                        if($addition == 'POWIADOMIENIE E-MAIL BEZPOŚREDNIO Z DHL_DE' OR $addition == 'DHL DE Receiver email notification')
                            continue;
                        $xml_output .= '<addition>' . CHtml::encode($addition) . '</addition>' . "\r\n";
                    }

                $xml_output .= '</additions>' . "\r\n";

                // SENDER
                $xml_output .= '<sender>' . "\r\n";
                foreach ($attributesAddressData AS $key => $item) {
                    $attribute = explode('.', $key);

                    $value = $model->senderAddressData;
                    foreach ($attribute AS $attributeName)
                        $value = $value->$attributeName;

                    $xml_output .= '<' . $item . '>' . CHtml::encode($value) . '</' . $item . '>' . "\r\n";
                }
                $xml_output .= '</sender>' . "\r\n";

                // RECEIVER
                $xml_output .= '<receiver>' . "\r\n";
                foreach ($attributesAddressData AS $key => $item) {
                    $attribute = explode('.', $key);

                    $value = $model->receiverAddressData;
                    foreach ($attribute AS $attributeName)
                        $value = $value->$attributeName;


                    $xml_output .= '<' . $item . '>' . CHtml::encode($value) . '</' . $item . '>' . "\r\n";
                }
                $xml_output .= '</receiver>' . "\r\n";


                // ORDER NUMBER
                $xml_output .= '<orderNumber>' . "\r\n";

                $number = 1;
                $total = 1;
                if ($model->getType() == Courier::TYPE_INTERNAL &&!$forceReturn) {
                    $number = $model->courierTypeInternal->family_member_number;
                    $total = $model->packages_number;
                } else if ($model->getType() == Courier::TYPE_DOMESTIC) {
                    $number = $model->courierTypeDomestic->family_member_number;
                    $total = $model->packages_number;
                }
                else if ($model->getType() == Courier::TYPE_U) {
                    $number = $model->courierTypeU->family_member_number;
                    $total = $model->packages_number;
                } else if ($model->getType() == Courier::TYPE_EXTERNAL) {
                    $number = '1-18';
                    $total = $model->packages_number;
                }

                $xml_output .= '<value>' . CHtml::encode($number) . '</value>' . "\r\n";
                $xml_output .= '<total>' . CHtml::encode($total) . '</total>' . "\r\n";

                $xml_output .= '</orderNumber>' . "\r\n";




                // PAYMENT DETAILS
                $xml_output .= '<paymentDetails>' . "\r\n";

                $val_netto = $val_brutto = $currency = $total = $payment_type = $order_stat_id = $order_stat_name = NULL;
                $order = false;
                if ($model->getType() == Courier::TYPE_INTERNAL && !$forceReturn)
                    $order = $model->courierTypeInternal->order;

                else if ($model->getType() == Courier::TYPE_U)
                    $order = $model->courierTypeU->order;

                if($order)
                {
                    $val_netto = $order->value_netto;
                    $val_brutto = $order->value_brutto;
                    $currency = $order->currency;
                    $payment_type = PaymentType::getPaymentTypeName($order->payment_type_id);
                    $order_stat_id = $order->order_stat_id;
                    $order_stat_name = $order->orderStat->name;
                    $order_id = $order->id;
                }

                $xml_output .= '<value_netto>' . CHtml::encode($val_netto) . '</value_netto>' . "\r\n";
                $xml_output .= '<value_brutto>' . CHtml::encode($val_brutto) . '</value_brutto>' . "\r\n";
                $xml_output .= '<currency>' . CHtml::encode($currency) . '</currency>' . "\r\n";
                $xml_output .= '<payment_type>' . CHtml::encode($payment_type) . '</payment_type>' . "\r\n";
                $xml_output .= '<order_stat_id>' . CHtml::encode($order_stat_id) . '</order_stat_id>' . "\r\n";
                $xml_output .= '<order_stat_name>' . CHtml::encode($order_stat_name) . '</order_stat_name>' . "\r\n";
                $xml_output .= '<order_id>' . CHtml::encode($order_id) . '</order_id>' . "\r\n";
//
                $xml_output .= '</paymentDetails>' . "\r\n";

                $xml_output .= '<collection>'.CHtml::encode(boolval(CourierCollect::findForCourierId($model->id))).'</collection>' . "\r\n";

                $xml_output .= '<sms_sent>' . CHtml::encode(S_Useful::sizeof($model->smsArchive)) . '</sms_sent>' . "\r\n";
                $xml_output .= '<first_scan_sp_point_id>' . CHtml::encode($model->first_scan_sp_point_id) . '</first_scan_sp_point_id>' . "\r\n";
                $xml_output .= '<first_scan_sp_point_country>' . CHtml::encode($model->first_scan_sp_point_id ? SpPoints::getPoint($model->first_scan_sp_point_id)->addressData->country0->code : NULL) . '</first_scan_sp_point_country>' . "\r\n";
                $xml_output .= '<operator_partner_id>' . CHtml::encode($model->operator_partner_id) . '</operator_partner_id>' . "\r\n";
                $xml_output .= '<operator_partner_id_2>' . CHtml::encode($model->operator_partner_id_2) . '</operator_partner_id_2>' . "\r\n";

                $xml_output .= "</item>\n";
            }
            $xml_output .= "</items>\n";

        }
        $xml_output .= "</response>";


        echo $xml_output;

        Yii::app()->end();
    }


    // POSTAL:
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    public static function actionLastStatPostal()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $ids = $post['ids'];
        $id = $post['id'];


        if(!is_array($ids))
            $ids = explode(',', $ids);

        if($pass != md5(self::PASSWORD))
        {
            $stat = false;
            $log = 'Invalid password';
        }
        else if($id == '' && !is_array($ids))
        {
            $stat = false;
            $log = 'No packages IDs provided';
        }
        else if($id == '' && !S_Useful::sizeof($ids))
        {
            $stat = false;
            $log = 'No packages IDs provided';
        }
        else if($id == '' && S_Useful::sizeof($ids) > self::INPUT_IDS_SIZE)
        {
            $stat = false;
            $log = 'Too many IDs provided (limit is '.self::INPUT_IDS_SIZE.')';
        }

//        array_walk($ids, function(&$item, $key){
//            $item = preg_replace("/[^a-zA-Z0-9]+/", "", $item);
//        });

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>'.($stat ? 'OK' : 'FAIL').'</status>'."\n";
        $xml_output .= '<log>'.$log.'</log>'."\n";

        if($stat)
        {
            if($id != '')
            {
                $ids = [$id];
            } else {
                $ids = array_unique($ids);
            }

            Yii::app()->getModule('postal');
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('local_id, postal_stat_id, stat_map_id');
            $cmd->from((new Postal())->tableName());
            $cmd->where(['in', 'local_id', $ids]);
            $result = $cmd->queryAll();

            $temp = [];
            $location = [];
            $stat_map_id = [];
            foreach($result AS $key => $item) {
                $temp[$item['local_id']] = $item['postal_stat_id'];
                $location[$item['local_id']] = $item['location'];
                $stat_map_id[$item['local_id']] = $item['stat_map_id'];
            }


            $xml_output .= '<last-stats>'."\n";
            foreach($ids AS $id)
            {
                if(!isset($temp[$id]))
                    continue;

                $xml_output .= '<package local_id="' . CHtml::encode($id) . '">' . "\r\n";
                $xml_output .= '<postal-stat-id>' . CHtml::encode($temp[$id]) . '</postal-stat-id>' . "\r\n";
                $xml_output .= '<postal-stat-location>' . CHtml::encode($location[$id]) . '</postal-stat-location>' . "\r\n";
                $xml_output .= '<postal-stat-map-id>' . CHtml::encode($stat_map_id[$id]) . '</postal-stat-map-id>' . "\r\n";
                $xml_output .= '</package>' . "\r\n";
            }

            $xml_output .= "</last-stats>\n";
        }

        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }


    public static function actionStatPostal()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $id = $post['id'];

        $id = intval($id);

        if($pass != md5(self::PASSWORD))
        {
            $stat = false;
            $log = 'Invalid password';
        }
        else if(!$id)
        {
            $stat = false;
            $log = 'Invalid ID';
        }


        if($stat) {
            Yii::app()->getModule('postal');

            /* @var $model PostalStat */
            $model = PostalStat::model()->findByPk($id);

            if ($model === NULL) {
                $stat = false;
                $log = 'Stat not found';
            }
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>'.($stat ? 'OK' : 'FAIL').'</status>'."\n";
        $xml_output .= '<log>'.$log.'</log>'."\n";

        /* @var $model PostalStat */
        if($stat)
        {
            $xml_output .= '<stat>'."\n";
            $xml_output .= '<name>' . CHtml::encode($model->name) . '</name>' . "\r\n";
            $xml_output .= '<stat_map_id>' . CHtml::encode($model->getStatMapId()) . '</stat_map_id>' . "\r\n";
            $xml_output .= '<stat_map_name>' . CHtml::encode($model->getStatMapName()) . '</stat_map_name>' . "\r\n";
            $xml_output .= "</stat>\n";
        }

        $xml_output .= "</response>";

        echo $xml_output;

        Yii::app()->end();
    }

    public static function actionPostals()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $date = $post['date'];
        $page = $post['page'];

        $page = intval($page);

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        } else if (!$date) {
            $stat = false;
            $log = 'Invalid date format';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            Yii::app()->getModule('postal');

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from((new Postal)->tableName());
            $cmd->where('date_entered LIKE :date', [':date' => $date . "%"]);
            $count = $cmd->queryScalar();

            $totalPages = ceil($count / self::PAGE_SIZE);

            if ($page > ($totalPages - 1))
                $page = 0;

            $xml_output .= '<count>' . $count . '</count>' . "\n";
            $xml_output .= '<totalPages>' . $totalPages . '</totalPages>' . "\n";
            $xml_output .= '<page>' . $page . '</page>' . "\n";

            $offset = $page * self::PAGE_SIZE;

            $models = Postal::model()->findAll(['condition' => 't.date_entered LIKE :date', 'limit' => self::PAGE_SIZE, 'offset' => $offset, 'order' => 't.id DESC', 'params' => [':date' => $date . "%"]]);
//
//            file_put_contents('ooooooooo.txt', print_r($models,1));

            $xml_output .= "<items>\n";


            $attributesPackage = [
                'local_id' => 'local_id',
                'date_entered' => 'date_entered',
                'postal_stat_id' => 'postal_stat_id',
                'size_class' => 'size_class',
                'weight_class' => 'weight_class',
                'postal_type' => 'postal_type',
                'bulk' => 'bulk',
                'bulk_id' => 'bulk_id',
                'bulk_number' => 'bulk_number',
                'content' => 'content',
                'target_sp_point' => 'target_sp_point',
                'user_id' => 'user_id',
                'stat_map_id' => 'stat_map_id',
                'ref' => 'ref',
                'weight' => 'weight',
                'value' => 'value',
                'value_currency' => 'value_currency',
                'note1' => 'note1',
                'note2' => 'note2',
            ];
            $attributesAddressData = [
                'name' => 'name',
                'company' => 'company',
                'country0.codeForMatrix' => 'country',
                'city' => 'city',
                'zip_code' => 'zip_code',
                'address_line_1' => 'address_line_1',
                'address_line_2' => 'address_line_2',
                'tel' => 'tel',
            ];
            ;

            /* @var $model Postal */
            foreach ($models AS $model) {

                $xml_output .= "<item>\n";


                // PACKAGE

                $xml_output .= '<package>' . "\r\n";
                foreach ($attributesPackage AS $key => $item) {
                    $attribute = explode('.', $key);

                    $value = $model;
                    foreach ($attribute AS $attributeName)
                        $value = $value->$attributeName;

                    if($item == 'user_id')
                    {
                        if($value == 571 && $model->date_entered >= '2018-11-20') // LITM BOD @ 27.11.2018
                        {
                            $value = 2340;
                        }
                    }

                    $xml_output .= '<' . $item . '>' . CHtml::encode($value) . '</' . $item . '>' . "\r\n";
                }
                $xml_output .= '<receiver_country>' . CHtml::encode($model->getReceiverCountryCode()) . '</receiver_country>' . "\r\n";
                $xml_output .= '<postal_type_name>' . CHtml::encode($model->getPostalTypeName()) . '</postal_type_name>' . "\r\n";
                $xml_output .= '<size_class_name>' . CHtml::encode($model->getSizeClassName()) . '</size_class_name>' . "\r\n";
                $xml_output .= '<weight_class_name>' . CHtml::encode($model->getWeightClassName()) . '</weight_class_name>' . "\r\n";
                $xml_output .= '<target_sp_point_name>' . CHtml::encode($model->targetSpPoint->name) . '</target_sp_point_name>' . "\r\n";

                $xml_output .= '</package>' . "\r\n";

                // OPERATORS & REMOTE IDS
                $xml_output .= '<operators>' . "\r\n";


                $remoteIds = [];
                $operators = [];
                $operatorsIds = [];

                array_push($operators, $model->postalOperator->name);
                array_push($operatorsIds, $model->postalOperator->id);

                if($model->postalLabel !== NULL)
                {
                    array_push($remoteIds, $model->postalLabel->track_id);
                }

                if (is_array($operators))
                    foreach ($operators AS $operator)
                        $xml_output .= '<operator>' . CHtml::encode($operator) . '</operator>' . "\r\n";

                $xml_output .= '</operators>' . "\r\n";

                $xml_output .= '<operators-ids>' . "\r\n";
                if (is_array($operatorsIds))
                    foreach ($operatorsIds AS $operator)
                        $xml_output .= '<operator>' . CHtml::encode($operator) . '</operator>' . "\r\n";

                $xml_output .= '</operators-ids>' . "\r\n";


                $xml_output .= '<remote_ids>' . "\r\n";

                if (is_array($remoteIds))
                    foreach ($remoteIds AS $remoteId) {
                        if ($remoteId != '')
                            $xml_output .= '<remote_id>' . CHtml::encode($remoteId) . '</remote_id>' . "\r\n";
                    }
                $xml_output .= '</remote_ids>' . "\r\n";

                // SENDER

                $xml_output .= '<sender>' . "\r\n";
                foreach ($attributesAddressData AS $key => $item) {
                    $attribute = explode('.', $key);

                    $value = $model->senderAddressData;
                    foreach ($attribute AS $attributeName)
                        $value = $value->$attributeName;

                    $xml_output .= '<' . $item . '>' . CHtml::encode($value) . '</' . $item . '>' . "\r\n";
                }
                $xml_output .= '</sender>' . "\r\n";

                // RECEIVER
                $xml_output .= '<receiver>' . "\r\n";
                foreach ($attributesAddressData AS $key => $item) {
                    $attribute = explode('.', $key);

                    $value = $model->receiverAddressData;
                    foreach ($attribute AS $attributeName)
                        $value = $value->$attributeName;


                    $xml_output .= '<' . $item . '>' . CHtml::encode($value) . '</' . $item . '>' . "\r\n";
                }
                $xml_output .= '</receiver>' . "\r\n";


                // PAYMENT DETAILS
                $xml_output .= '<paymentDetails>' . "\r\n";

                $val_netto = $val_brutto = $currency = $total = $payment_type = $order_stat_id = $order_stat_name = NULL;


                $order = $model->getOrder();
                if($order)
                {
                    $val_netto = $order->value_netto;
                    $val_brutto = $order->value_brutto;
                    $currency = $order->currency;
                    $payment_type = PaymentType::getPaymentTypeName($order->payment_type_id);
                    $order_stat_id = $order->order_stat_id;
                    $order_stat_name = $order->orderStat->name;
                    $order_id = $order->id;
                }

                $xml_output .= '<value_netto>' . CHtml::encode($val_netto) . '</value_netto>' . "\r\n";
                $xml_output .= '<value_brutto>' . CHtml::encode($val_brutto) . '</value_brutto>' . "\r\n";
                $xml_output .= '<currency>' . CHtml::encode($currency) . '</currency>' . "\r\n";
                $xml_output .= '<payment_type>' . CHtml::encode($payment_type) . '</payment_type>' . "\r\n";
                $xml_output .= '<order_stat_id>' . CHtml::encode($order_stat_id) . '</order_stat_id>' . "\r\n";
                $xml_output .= '<order_stat_name>' . CHtml::encode($order_stat_name) . '</order_stat_name>' . "\r\n";
                $xml_output .= '<order_id>' . CHtml::encode($order_id) . '</order_id>' . "\r\n";
//
                $xml_output .= '</paymentDetails>' . "\r\n";

                $xml_output .= '<sms_sent>' . CHtml::encode(S_Useful::sizeof($model->smsArchive)) . '</sms_sent>' . "\r\n";

                $xml_output .= '<operator_partner_id>' . CHtml::encode($model->operator_partner_id) . '</operator_partner_id>' . "\r\n";

                $xml_output .= "</item>\n";
            }
            $xml_output .= "</items>\n";

        }
        $xml_output .= "</response>";


        echo $xml_output;

        Yii::app()->end();
    }



    public static function actionAdditionsCourierInternal()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            Yii::app()->getModule('courier');

            $models = CourierAdditionList::model()->findAll(['order' => 'broker_id']);

            $xml_output .= "<items>\n";

            /* @var $model CourierAdditionList */
            foreach ($models AS $model) {
                $xml_output .= "<item>\r\n";
                $xml_output .= '<id>' . CHtml::encode($model->id) . '</id>' . "\r\n";
                $xml_output .= '<name>' . CHtml::encode($model->name) . '</name>' . "\r\n";
                $xml_output .= '<broker_id>' . CHtml::encode($model->broker_id) . '</broker_id>' . "\r\n";
                $xml_output .= '<broker_name>' . CHtml::encode($model->broker_id ? CourierOperator::getBrokerName($model->broker_id) : NULL) . '</broker_name>' . "\r\n";
                $xml_output .= "</item>\n";
            }
            $xml_output .= "</items>\n";

        }
        $xml_output .= "</response>";


        echo $xml_output;

        Yii::app()->end();
    }


    public static function actionAdditionsCourierU()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            Yii::app()->getModule('courier');

            $models = CourierUAdditionList::model()->findAll(['order' => 'broker_id']);

            $xml_output .= "<items>\n";

            /* @var $model CourierAdditionList */
            foreach ($models AS $model) {
                $xml_output .= "<item>\r\n";
                $xml_output .= '<id>' . CHtml::encode($model->id) . '</id>' . "\r\n";
                $xml_output .= '<name>' . CHtml::encode($model->name) . '</name>' . "\r\n";
                $xml_output .= '<broker_id>' . CHtml::encode($model->broker_id) . '</broker_id>' . "\r\n";
                $xml_output .= '<broker_name>' . CHtml::encode($model->broker_id ? CourierUOperator::getBrokerName($model->broker_id) : NULL) . '</broker_name>' . "\r\n";
                $xml_output .= "</item>\n";
            }
            $xml_output .= "</items>\n";

        }
        $xml_output .= "</response>";


        echo $xml_output;

        Yii::app()->end();
    }


    public static function actionOperatorsCourierInternal()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            Yii::app()->getModule('courier');

            $models = CourierOperator::model()->findAll(['order' => 'broker']);

            $xml_output .= "<items>\n";

            /* @var $model CourierOperator */
            foreach ($models AS $model) {
                $xml_output .= "<item>\r\n";
                $xml_output .= '<id>' . CHtml::encode($model->id) . '</id>' . "\r\n";
                $xml_output .= '<name>' . CHtml::encode($model->name) . '</name>' . "\r\n";
                $xml_output .= '<broker_id>' . CHtml::encode($model->broker) . '</broker_id>' . "\r\n";
                $xml_output .= '<broker_name>' . CHtml::encode($model->broker ? CourierOperator::getBrokerName($model->broker) : NULL) . '</broker_name>' . "\r\n";
                $xml_output .= '<partner_id>' . CHtml::encode($model->partner_id) . '</partner_id>' . "\r\n";
                $xml_output .= "</item>\n";
            }
            $xml_output .= "</items>\n";

        }
        $xml_output .= "</response>";


        echo $xml_output;

        Yii::app()->end();
    }

    public static function actionOperatorsCourierU()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            Yii::app()->getModule('courier');

            $models = CourierUOperator::model()->findAll(['order' => 'broker_id']);

            $xml_output .= "<items>\n";

            /* @var $model CourierOperator */
            foreach ($models AS $model) {
                $xml_output .= "<item>\r\n";
                $xml_output .= '<id>' . CHtml::encode($model->id) . '</id>' . "\r\n";
                $xml_output .= '<name>' . CHtml::encode($model->name) . '</name>' . "\r\n";
                $xml_output .= '<broker_id>' . CHtml::encode($model->broker_id) . '</broker_id>' . "\r\n";
                $xml_output .= '<broker_name>' . CHtml::encode($model->broker_id ? CourierUOperator::getBrokerName($model->broker_id) : NULL) . '</broker_name>' . "\r\n";
                $xml_output .= '<partner_id>' . CHtml::encode($model->partner_id) . '</partner_id>' . "\r\n";
                $xml_output .= "</item>\n";
            }
            $xml_output .= "</items>\n";

        }
        $xml_output .= "</response>";


        echo $xml_output;

        Yii::app()->end();
    }



    ///////// INVOICES
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function actionInvoices()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $date = $post['date'];
        $page = $post['page'];

        $page = intval($page);

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        } else if (!$date) {
            $stat = false;
            $log = 'Invalid date format';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(*)');
            $cmd->from((new UserInvoice())->tableName());
            $cmd->where('date_entered LIKE :date AND (source = :source1 OR source =:source2 OR source = :source3)', [':date' => $date . "%", ':source1' => UserInvoice::SOURCE_FAKTUROWNIA, ':source2' => UserInvoice::SOURCE_FAKTUROWNIA_MANUAL, ':source3' => UserInvoice::SOURCE_CORRECTION_NOTE]);
            $count = $cmd->queryScalar();

            $totalPages = ceil($count / self::PAGE_SIZE);

            if ($page > ($totalPages - 1))
                $page = 0;

            $xml_output .= '<count>' . $count . '</count>' . "\n";
            $xml_output .= '<totalPages>' . $totalPages . '</totalPages>' . "\n";
            $xml_output .= '<page>' . $page . '</page>' . "\n";

            $offset = $page * self::PAGE_SIZE;

            $models = UserInvoice::model()->findAll(['condition' => 't.date_entered LIKE :date  AND source != :ext', 'limit' => self::PAGE_SIZE, 'offset' => $offset, 'order' => 't.id DESC', 'params' => [
                ':date' => $date . "%",
                ':ext' => UserInvoice::SOURCE_EXT
            ]]);
//


            $xml_output .= "<items>\n";

            /* @var $model UserInvoice */
            foreach ($models AS $model) {
                $xml_output .= "<item>\r\n";
                $xml_output .= '<title>' . CHtml::encode($model->title) . '</title>' . "\r\n";
                $xml_output .= '<user_id>' . CHtml::encode($model->user_id) . '</user_id>' . "\r\n";
                $xml_output .= '<inv_value>' . CHtml::encode($model->inv_value) . '</inv_value>' . "\r\n";
                $xml_output .= '<paid_value>' . CHtml::encode($model->paid_value) . '</paid_value>' . "\r\n";
                $xml_output .= '<inv_currency>' . CHtml::encode(Currency::nameById($model->inv_currency)) . '</inv_currency>' . "\r\n";
                $xml_output .= '<tax>' . CHtml::encode($model->tax) . '</tax>' . "\r\n";
                $xml_output .= '<inv_date>' . CHtml::encode($model->inv_date) . '</inv_date>' . "\r\n";
                $xml_output .= '<inv_payment_date>' . CHtml::encode($model->inv_payment_date) . '</inv_payment_date>' . "\r\n";
                $xml_output .= '<stat>' . CHtml::encode($model->stat) . '</stat>' . "\r\n";
                $xml_output .= '<stat_inv>' . CHtml::encode($model->stat_inv) . '</stat_inv>' . "\r\n";
                $xml_output .= '<note>' . CHtml::encode($model->note) . '</note>' . "\r\n";
                $xml_output .= '<source>' . CHtml::encode($model->source) . '</source>' . "\r\n";
                $xml_output .= '<source_name>' . CHtml::encode($model->getSourceName()) . '</source_name>' . "\r\n";
                $xml_output .= '<order_id>' . CHtml::encode($model->order ? $model->order->local_id : '') . '</order_id>' . "\r\n";
                $xml_output .= '<products_type_id>' . CHtml::encode($model->order ? $model->order->product_type : '') . '</products_type_id>' . "\r\n";
                $xml_output .= '<products_type_name>' . CHtml::encode($model->order ? $model->order->getProductTypeName() : '') . '</products_type_name>' . "\r\n";
                $xml_output .= '<products_local_id>' . "\r\n";

                if($model->order)
                    foreach($model->order->orderProduct AS $item)
                    {
                        $temp = $item->getProductModel();
                        if($temp)
                        {
                            $xml_output .= '<product_local_id>' . CHtml::encode($temp->local_id) . '</product_local_id>' . "\r\n";
                        }
                    }

                $xml_output .= '</products_local_id>' . "\r\n";
                $xml_output .= '<url_inv>' . CHtml::encode($model->inv_file_name ? Yii::app()->createAbsoluteUrl('/data/getFileInv', ['hash' => $model->hash]) : '') . '</url_inv>' . "\r\n";
                $xml_output .= '<url_sheet>' . CHtml::encode($model->sheet_file_name ? Yii::app()->createAbsoluteUrl('/data/getFileSheet', ['hash' => $model->hash]) : '') . '</url_sheet>' . "\r\n";
                $xml_output .= "</item>\n";
            }
            $xml_output .= "</items>\n";

        }
        $xml_output .= "</response>";


        echo $xml_output;

        Yii::app()->end();
    }


    public function actionGetFileInv($hash)
    {
        $model = UserInvoice::model()->findByAttributes(['hash' => $hash]);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        $file_path = $model->inv_file_path;
        $file_name = $model->inv_file_name;
        $file_type = $model->inv_file_type;

        if($file_path == NULL)
            throw new CHttpException(404);


        return Yii::app()->getRequest()->sendFile($file_name, @file_get_contents($file_path), $file_type);
    }

    public function actionGetFileSheet($hash)
    {
        $model = UserInvoice::model()->findByAttributes(['hash' => $hash]);

        if($model === NULL) {
            throw new CHttpException(404);
        }

        $file_path = $model->sheet_file_path;
        $file_name = $model->sheet_file_name;
        $file_type = $model->sheet_file_type;

        if($file_path == NULL)
            throw new CHttpException(404);


        return Yii::app()->getRequest()->sendFile($file_name, @file_get_contents($file_path), $file_type);
    }



    public static function actionPackagesCancelFlag()
    {
        header("Content-type: text/xml");

        $post = $_REQUEST;
        $stat = true;
        $log = '';

        $pass = $post['pass'];
        $date = $post['date'];

        if ($pass != md5(self::PASSWORD)) {
            $stat = false;
            $log = 'Invalid password';
        } else if (!$date) {
            $stat = false;
            $log = 'Invalid date format';
        }

        $xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
        $xml_output .= "<response>\n";
        $xml_output .= '<status>' . ($stat ? 'OK' : 'FAIL') . '</status>' . "\n";
        $xml_output .= '<log>' . $log . '</log>' . "\n";

        if ($stat) {

            Yii::app()->getModule('courier');

            $models = Courier::model()->findAll(['condition' => 't.date_entered LIKE :date AND user_cancel = :warning_flag', 'order' => 't.id DESC', 'params' => [':date' => $date . "%", ':warning_flag' => Courier::USER_CANCELL_REQUESTED_FOUND_TT]]);
//
            $xml_output .= '<count>' . S_Useful::sizeof($models) . '</count>' . "\n";
            $xml_output .= "<items>\n";
            /* @var $model Courier */
            foreach ($models AS $model) {

                $xml_output .= "<item>".$model->local_id."</item>\n";
            }
            $xml_output .= "</items>\n";

        }
        $xml_output .= "</response>";


        echo $xml_output;

        Yii::app()->end();
    }


    public static function actionPackagesMonthSummary()
    {
        $post = $_REQUEST;
        $pass = $post['pass'];
        $monthsBack = $post['monthsBack'];

        if ($pass != md5(self::PASSWORD)) {
            die('Invalid password');
        }

        Yii::app()->getModule('courier');


        $monthsBack = intval($monthsBack);
        $date = date('Y-m', strtotime("first day of -".$monthsBack." month"));

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('local_id, stat_map_id, courier_type')->from('courier')->where('date_entered LIKE "'.$date.'%"');
        $result = $cmd->queryAll();

        foreach($result AS $item)
            echo $item['local_id'].','.$item['stat_map_id'].','.Courier::$types[$item['courier_type']].'<br/>';
    }
}
