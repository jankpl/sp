<script>
    $(document).ready(function(){

        $('[data-check-vies-widget]').each(function(i,v){

            $button = $('<input>');
            $button.attr('type', 'button');
            $button.val('check VIES');

            $button.on('click', function(){


                var value = $(v).val();

                if(value == '') {
                    alert('Provide VIES!');
                    return;
                }

                $.ajax({
                    url: '<?= Yii::app()->createUrl('/user/ajaxCheckVies');?>',
                    data: { vies: value},
                    method: 'POST',
                    dataType: 'json',
                    success: function(result){

                        if(result == -1)
                            alert('Service in unavailable - please try again later');
                        else if(result)
                            alert('VIES is VALID!');
                        else
                            alert('VIES is NOT valid!')

                    },
                    error: function(e){

                        console.log(e);
                    }
                });
            });

            $(v).after($button);
        })

    });
</script>
