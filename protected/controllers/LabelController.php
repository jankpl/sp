<?php

class LabelController extends Controller
{

    public function filters()
    {
        return array_merge([
            'accessControl', // perform access control for CRUD operations
        ]);
    }

    public function accessRules()
    {
        return array(

            array('allow', 'actions'=>array('show','ajaxCheckIfReady'
            ),
                'users'=>array('@'),
            ),
            array('allow', 'actions'=>array('get',
            ),
                'users'=>array('*'),
            ),
            array('deny',

                'users'=>array('*'),
            ),
        );
    }

    public function actionShow($hash)
    {
        $model = LabelsUrl::model()->findByAttributes(['hash' => $hash, 'user_id' => Yii::app()->user->id]);

        if($model === NULL)
            throw new CHttpException(404);

        $this->render('show',[
            'model' => $model,
        ]);
    }

    public function actionGet($hash)
    {
        /* @var $model LabelsUrl */
        $model = LabelsUrl::model()->findByAttributes(['hash' => $hash]);

        if($model === NULL)
            throw new CHttpException(404);


        $model->accesed += 1;
        $model->update(['accesed']);

        $CACHE_NAME = 'LABEL_URL_'.$hash;
        $CACHE = Yii::app()->cacheFile;

        $data = $CACHE->get($CACHE_NAME);

        if($data === false) {

            if ($model->type == LabelsUrl::TYPE_COURIER) {

                Yii::app()->getModule('courier');

                $couriers = Courier::model()->findAllByPk($model->ids);

                $data = LabelPrinter::generateLabels($couriers, $model->label_type, false, false, true);
            } else if ($model->type == LabelsUrl::TYPE_POSTAL) {
                Yii::app()->getModule('postal');

                $postals = Postal::model()->findAllByPk($model->ids);

                $data = PostalLabelPrinter::generateLabels($postals, $model->label_type, true, false, true);
            }

            $CACHE->set($CACHE_NAME, $data, 60*60*24);
        }

        header("Content-type:application/pdf");
        header("Content-Disposition:inline;filename='labels.pdf'");
        echo $data;
    }

    public function actionAjaxCheckIfReady()
    {

        echo CJSON::encode(true);
        exit;

        $hash = $_POST['hash'];

        $CACHE_NAME = 'LABEL_URL_'.$hash;
        $CACHE = Yii::app()->cacheFile;

        $data = $CACHE->get($CACHE_NAME);

        if($data === false)
            echo CJSON::encode(false);
        else
            echo CJSON::encode(true);
    }

}