<?php

class CourierOrderController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new _CourierLabelNewGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierLabelNewGridView']))
            $model->setAttributes($_GET['CourierLabelNewGridView']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionGetLabel($id)
    {
        $model = CourierLabelNew::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $content = $model->getFileAsString(true);

        if($model->file_path == '' AND (!$content OR $content == ''))
            throw new CHttpException('404', 'Label do not exists or was not downloaded into our system. Try to check directly in package.');

        $finfo = new finfo();
        $mime = $finfo->buffer($content, FILEINFO_MIME_TYPE);


        switch($mime) {
            case 'image/png':
                $ext = 'png';
                break;
            case 'image/gif':
                $ext = 'gif';
                break;
            default:
                $ext = 'txt';
                break;
        }

        return Yii::app()->getRequest()->sendFile($model->id.'.'.$ext, $content , $mime);
    }

    public function actionEditTrackingId($id)
    {
        /* @var $model CourierLabelNew */
        $model = CourierLabelNew::model()->findByPk($id);

        $oldNo = $model->track_id;
        $oldOperator = isset(CourierLabelNew::getOperators()[$model->operator]) ? CourierLabelNew::getOperators()[$model->operator] : '?';

        $oldExtOperatorName = $model->_ext_operator_name;

        if($model === NULL)
            throw new CHttpException(404);

        if (isset($_POST['CourierLabelNew'])) {
            $model->setAttributes(array_merge($_POST['CourierLabelNew'], ['_ext_operator_name']));

            if ($model->update()) {

                $newNo = $model->track_id;
                $newOperator = isset(CourierLabelNew::getOperators()[$model->operator]) ? CourierLabelNew::getOperators()[$model->operator] : '?';

                $model->courier->addToLog('Changed TT number from: "'.$oldNo.'" ('.$oldOperator.') to: "'.$newNo.'" ('.$newOperator.') by: '.Yii::app()->user->model->login, CourierLog::CAT_INFO);

                if($model->operator == CourierLabelNew::OPERATOR_OWN_EXTERNAL_RETURN)
                    $model->courier->addToLog('Changed Ext Operator Name from: "'.$oldExtOperatorName.'" to: "'.$model->_ext_operator_name.'" by: '.Yii::app()->user->model->login, CourierLog::CAT_INFO);


                $model->expireLabel();
                Yii::app()->user->setFlash('success', 'Tracking ID has been updated!');
                $this->redirect(array('/courier/courier/view', 'id' => $model->courier_id));
            }
        }

        $this->render('updateTrackingId', array( 'model' => $model));



    }


}