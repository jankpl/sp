<?php

class EbayOrderItem
{
    const STAT_NOT_PAID = 0;
    const STAT_PAID = 1;

    const STAT_NOT_SHIPPED = 0;
    const STAT_PARTIALY_SHIPPED = 5;
    const STAT_SHIPPED = 10;

    const STAT_PROCESSED = 1;
    const STAT_NOT_PROCESSED = 0;

    const STAT_SHOW_PROCESSED = 3;
    const STAT_SHOW_NOT_PROCESSED = 2;
    const STAT_SHOW_PROCESSED_ALL = 1;

    const STAT_SHOW_EBAY_READY = 1;
    const STAT_SHOW_EBAY_ALL = 10;



    public $salesRecordId;

    public $orderID;
    public $createdTime;

    public $lastModified;

    public $statPayment;
    public $statShipping;
    public $statProcessed;


    public $shippingAddress;

    // all data for testing
    public $_data;

    public $email;

    public $addr_name;
    public $addr_street_1;
    public $addr_street_2;
    public $addr_city;
    public $addr_state;
    public $addr_country;
    public $addr_tel;
    public $addr_zip_code;

    public $quantity;
    public $buyer_user;

    public $total_price;
    public $price_currency;

    public $transactions = [];

    public $common_track_id;
    public $common_operator_name;

    public $local_package_id;
    public $local_package_hash; // for link

    public static function getStatsShippingArray()
    {
        $data = [];
        $data[self::STAT_NOT_SHIPPED] = Yii::t('courier_ebay', 'Nie wysłane');
        $data[self::STAT_PARTIALY_SHIPPED] = Yii::t('courier_ebay', 'Częściowo wysłane');
        $data[self::STAT_SHIPPED] = Yii::t('courier_ebay', 'Wysłane');
        return $data;
    }

    public static function getStatsPaymentArray()
    {
        $data = [];
        $data[self::STAT_NOT_PAID] = Yii::t('courier_ebay', 'Nie opłacone');
        $data[self::STAT_PAID] = Yii::t('courier_ebay', 'Opłacone');
        return $data;
    }

    public static function getStatShowProcessedList()
    {
        $data = [];
        $data[self::STAT_SHOW_PROCESSED_ALL] = Yii::t('courier_ebay', '--wszystkie--');
        $data[self::STAT_SHOW_PROCESSED] = Yii::t('courier_ebay', 'przetworzone');
        $data[self::STAT_SHOW_NOT_PROCESSED] = Yii::t('courier_ebay', 'nieprzetworzone');

        return $data;
    }

    public static function getStatShowEbayList()
    {
        $data = [];
        $data[self::STAT_SHOW_EBAY_ALL] = Yii::t('courier_ebay', '--wszystkie--');
        $data[self::STAT_SHOW_EBAY_READY] = Yii::t('courier_ebay', 'opłacone, niewysłane');

        return $data;
    }

    public static function getStatProcessedList()
    {
        $data = [];
        $data[self::STAT_PROCESSED] = Yii::t('courier_ebay', 'przetworzone');
        $data[self::STAT_NOT_PROCESSED] = Yii::t('courier_ebay', 'nieprzetworzone');

        return $data;
    }


    public function __construct(stdClass $order, $user_id = NULL, $target = EbayImport::TARGET_COURIER)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;


        $this->orderID = $order->OrderID;

        $this->shippingAddress = $order->ShippingAddress;
        $this->createdTime = $order->CreatedTime;

        $this->addr_name = $order->ShippingAddress->Name;
        $this->addr_street_1 = $order->ShippingAddress->Street1;
        $this->addr_street_2 = $order->ShippingAddress->Street2;
        $this->addr_city = $order->ShippingAddress->CityName;
        $this->addr_state = $order->ShippingAddress->StateOrProvince;
        $this->addr_country = $order->ShippingAddress->Country;
        $this->addr_tel = $order->ShippingAddress->Phone == 'Invalid Request' ? '' : $order->ShippingAddress->Phone;
        $this->addr_zip_code = $order->ShippingAddress->PostalCode;


        $this->buyer_user = $order->BuyerUserID;

        $this->lastModified = $order->CheckoutStatus->LastModifiedTime;

        $this->shipped = $order->ShippedTime != '' ? true : false;

        $this->total_price = $order->Total->_;
        $this->price_currency = $order->Total->currencyID;

        $this->salesRecordId = $order->ShippingDetails->SellingManagerSalesRecordNumber;

        $totalQuantity = 0;

        $transactions = $order->TransactionArray->Transaction;

        // if only one transaction, put it into array for foreach to work
        if(!is_array($transactions))
            $transactions = [$transactions];

        // test flags
//        $allShipped = true;
//        $noneShipped = true;

        $common_operator_valid = true;
        $first = true;
        foreach($transactions AS $transaction)
        {
            $temp = new EbayTransactionItem($transaction);
            $totalQuantity += $temp->quantity;

            if($first)
            {
                $first = false;

                $this->common_track_id = $temp->shipp_track_id;
                $this->common_operator_name = $temp->shippping_operator;

                // just first buyers email is fine...
                $this->email = $transaction->Buyer->Email == 'Invalid Request' ? '' : $transaction->Buyer->Email;

            } else {
                if($this->common_track_id != $temp->shipp_track_id OR
                    $this->common_operator_name != $temp->shippping_operator)
                    $common_operator_valid = false;
            }

            // OLDER MECHANISM:
//            if($temp->shipped)
//                $noneShipped = false;
//            else
//                $allShipped = false;
//
//
//            if($noneShipped === true && $allShipped === false)
//            {
//                $this->statShipping = self::STAT_NOT_SHIPPED;
//            }
//            else if($noneShipped === false && $allShipped === true)
//            {
//                $this->statShipping = self::STAT_SHIPPED;
//            } else {
//                $this->statShipping = self::STAT_PARTIALY_SHIPPED;
//            }


            //

            array_push($this->transactions, $temp);
        }

        if($order->shipped == "1")
            $this->statShipping = self::STAT_SHIPPED;
        else
            $this->statShipping = self::STAT_NOT_SHIPPED;

        $this->statShipping = $order->ShippedTime != '' ? self::STAT_SHIPPED : self::STAT_NOT_SHIPPED;

        if($order->CheckoutStatus->Status === 'Complete' && $order->CheckoutStatus->eBayPaymentStatus === 'NoPaymentFailure' && $order->PaidTime != '')
            $this->statPayment = self::STAT_PAID;
        else
            $this->statPayment = self::STAT_NOT_PAID;

        $this->quantity = $totalQuantity;

        if(!$common_operator_valid)
        {
            $this->common_operator_name = false;
            $this->common_track_id = false;
        }

        // maybe package is already processed by our system - search for it and return ID if found
        /* @var $cmd CDbCommand */
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('target_item_id');
        $cmd->from((new EbayImport())->tableName());
        $cmd->where('ebay_order_id = :ebay_order_id AND stat != :stat AND user_id = :user_id AND target = :target AND source = :source', [':ebay_order_id' => $this->orderID, ':stat' => EbayImport::STAT_CANCELLED, ':user_id' => $user_id, ':target' => $target, ':source' => EbayImport::SOURCE_EBAY]);
        $cmd->order('date_updated DESC, date_entered DESC');
        $cmd->limit(1);
        $this->local_package_id = $cmd->queryScalar();

        if($this->local_package_id)
        {
            $this->statProcessed = self::STAT_PROCESSED;

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('hash');

            if($target == EbayImport::TARGET_COURIER)
                $cmd->from((new Courier())->tableName());
            else if($target == EbayImport::TARGET_POSTAL)
                $cmd->from((new Postal())->tableName());
            else
                throw new Exception;

            $cmd->where('id = :id', [':id' => $this->local_package_id]);
            $cmd->limit(1);

            $this->local_package_hash = $cmd->queryScalar();
        } else
            $this->statProcessed = self::STAT_NOT_PROCESSED;

//        $this->_data = print_r($order, 1);
    }


    public function convertToCourierTypeOoeModel($ooe_operator_id)
    {
        $courier = new Courier_CourierTypeOoe();
        $courier->source = Courier::SOURCE_EBAY;

        $courier->courierTypeOoe = new courierTypeOoe('step_1');
        $courier->senderAddressData = new CourierTypeOoe_AddressData();
        $courier->receiverAddressData = new CourierTypeOoe_AddressData();


        $courier->package_weight = 1;
        $courier->package_size_l = 20;
        $courier->package_size_w = 20;
        $courier->package_size_d = 20;
        $courier->packages_number = 1;

        $courier->courierTypeOoe->service_code = $ooe_operator_id;

        $courier->package_content = 'eBay'.$this->orderID;

//        $courier->senderAddressData->name = 'nadawca';
//        $courier->senderAddressData->company = 'nadawca';
//        $courier->senderAddressData->country_id = CountryList::findIdByText('polska');
//        $courier->senderAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->senderAddressData->country_id);
//        $courier->senderAddressData->zip_code = '00-000';
//        $courier->senderAddressData->city = 'miasto';
//        $courier->senderAddressData->address_line_1 = 'adres 1';
//        $courier->senderAddressData->address_line_2 = 'adres 2';
//        $courier->senderAddressData->tel = '111222333';
//        $courier->senderAddressData->email = 'test@test.pl';

        $courier->receiverAddressData->name = $this->addr_name;
        $courier->receiverAddressData->company = $this->addr_name;
        $courier->receiverAddressData->country_id = CountryList::findIdByText($this->addr_country);
        $courier->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->receiverAddressData->country_id);
        $courier->receiverAddressData->zip_code = $this->addr_zip_code;
        $courier->receiverAddressData->city = $this->addr_city;
        $courier->receiverAddressData->address_line_1 = $this->addr_street_1;
        $courier->receiverAddressData->address_line_2 =  $this->addr_street_2;
        $courier->receiverAddressData->tel = $this->addr_tel;
        $courier->receiverAddressData->email = $this->email;

        $courier->courierTypeOoe->courier = $courier;

        $courier->courierTypeOoe->_ebay_order_id = $this->orderID;

        return $courier;
    }


    public function convertToCourierTypeInternalModel()
    {
        $courier = new Courier_CourierTypeInternal();
        $courier->source = Courier::SOURCE_EBAY;

        $courier->courierTypeInternal = new CourierTypeInternal('import');
        $courier->senderAddressData = new CourierTypeInternal_AddressData();
        $courier->receiverAddressData = new CourierTypeInternal_AddressData();

        $courier->package_weight = 1;
        $courier->package_size_l = 20;
        $courier->package_size_w = 20;
        $courier->package_size_d = 20;
        $courier->packages_number = 1;

        $courier->package_content = 'eBay'.$this->orderID;

//        $courier->senderAddressData->name = 'nadawca';
//        $courier->senderAddressData->company = 'nadawca';
//        $courier->senderAddressData->country_id = CountryList::findIdByText('polska');
//        $courier->senderAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->senderAddressData->country_id);
//        $courier->senderAddressData->zip_code = '00-000';
//        $courier->senderAddressData->city = 'miasto';
//        $courier->senderAddressData->address_line_1 = 'adres 1';
//        $courier->senderAddressData->address_line_2 = 'adres 2';
//        $courier->senderAddressData->tel = '111222333';
//        $courier->senderAddressData->email = 'test@test.pl';

        $courier->receiverAddressData->name = $this->addr_name;
        $courier->receiverAddressData->company = $this->addr_name;
        $courier->receiverAddressData->country_id = CountryList::findIdByText($this->addr_country);
        $courier->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->receiverAddressData->country_id);
        $courier->receiverAddressData->zip_code = $this->addr_zip_code;
        $courier->receiverAddressData->city = $this->addr_city;
        $courier->receiverAddressData->address_line_1 = $this->addr_street_1;
        $courier->receiverAddressData->address_line_2 =  $this->addr_street_2;
        $courier->receiverAddressData->tel = $this->addr_tel;
        $courier->receiverAddressData->email = $this->email;

        $courier->courierTypeInternal->courier = $courier;


        $courier->courierTypeInternal->_ebay_order_id = $this->orderID;


        return $courier;
    }

    public function convertToCourierTypeUModel()
    {
        $courier = new Courier_CourierTypeU();
        $courier->source = Courier::SOURCE_EBAY;

        $courier->courierTypeU = new CourierTypeU('import');
        $courier->senderAddressData = new CourierTypeU_AddressData();
        $courier->receiverAddressData = new CourierTypeU_AddressData();

        $courier->package_weight = 1;
        $courier->package_size_l = 20;
        $courier->package_size_w = 20;
        $courier->package_size_d = 20;
        $courier->packages_number = 1;

        $courier->package_content = 'eBay'.$this->orderID;

//        $courier->senderAddressData->name = 'nadawca';
//        $courier->senderAddressData->company = 'nadawca';
//        $courier->senderAddressData->country_id = CountryList::findIdByText('polska');
//        $courier->senderAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->senderAddressData->country_id);
//        $courier->senderAddressData->zip_code = '00-000';
//        $courier->senderAddressData->city = 'miasto';
//        $courier->senderAddressData->address_line_1 = 'adres 1';
//        $courier->senderAddressData->address_line_2 = 'adres 2';
//        $courier->senderAddressData->tel = '111222333';
//        $courier->senderAddressData->email = 'test@test.pl';

        $courier->receiverAddressData->name = $this->addr_name;
        $courier->receiverAddressData->company = $this->addr_name;
        $courier->receiverAddressData->country_id = CountryList::findIdByText($this->addr_country);
        $courier->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->receiverAddressData->country_id);
        $courier->receiverAddressData->zip_code = $this->addr_zip_code;
        $courier->receiverAddressData->city = $this->addr_city;
        $courier->receiverAddressData->address_line_1 = $this->addr_street_1;
        $courier->receiverAddressData->address_line_2 =  $this->addr_street_2;
        $courier->receiverAddressData->tel = $this->addr_tel;
        $courier->receiverAddressData->email = $this->email;

        $courier->courierTypeU->courier = $courier;


        $courier->_ebay_order_id = $this->orderID;


        return $courier;
    }


    public function convertToPostalModel()
    {
        $postal = new Postal();
        $postal->source = Postal::SOURCE_EBAY;

        $postal->senderAddressData = new Postal_AddressData();
        $postal->receiverAddressData = new Postal_AddressData();

        $postal->senderAddressData->_postal = $postal;
        $postal->receiverAddressData->_postal = $postal;

        $postal->content = 'eBay'.$this->orderID;

//        $courier->senderAddressData->name = 'nadawca';
//        $courier->senderAddressData->company = 'nadawca';
//        $courier->senderAddressData->country_id = CountryList::findIdByText('polska');
//        $courier->senderAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($courier->senderAddressData->country_id);
//        $courier->senderAddressData->zip_code = '00-000';
//        $courier->senderAddressData->city = 'miasto';
//        $courier->senderAddressData->address_line_1 = 'adres 1';
//        $courier->senderAddressData->address_line_2 = 'adres 2';
//        $courier->senderAddressData->tel = '111222333';
//        $courier->senderAddressData->email = 'test@test.pl';

        $postal->receiverAddressData->name = $this->addr_name;
        $postal->receiverAddressData->company = $this->addr_name;
        $postal->receiverAddressData->country_id = CountryList::findIdByText($this->addr_country);
        $postal->receiverAddressData->country0 = CountryList::model()->with('countryListTr')->findByPk($postal->receiverAddressData->country_id);
        $postal->receiverAddressData->zip_code = $this->addr_zip_code;
        $postal->receiverAddressData->city = $this->addr_city;
        $postal->receiverAddressData->address_line_1 = $this->addr_street_1;
        $postal->receiverAddressData->address_line_2 =  $this->addr_street_2;
        $postal->receiverAddressData->tel = $this->addr_tel;
        $postal->receiverAddressData->email = $this->email;



        $postal->postalLabel = new PostalLabel();
        $postal->_ebay_order_id = $this->orderID;

        return $postal;
    }
}