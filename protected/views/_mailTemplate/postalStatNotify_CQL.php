<?php
/* @var $text string */
/* @var $model Courier */
/* @var $status string */
/* @var $packageID string */
/* @var $operator string */
/* @var $addEnAnnouncement boolean */
/* @var $addEnIntro boolean */
?>

<?= $text;?><br/><br/>
<strong><?= Yii::t('mail_courier', 'Operator:');?></strong> <?= $packageID;?><br/>
<strong><?= Yii::t('mail_courier', 'Zew. numer TT:');?></strong> <?= $operator;?> <br/><br/>


<?php
if($model->note1):
    ?>
    <strong><?= Yii::t('mail_courier', 'Note1:');?></strong> <?= $model->note1; ?><br/>
<?php
endif;
?>

<?php
$baseUrl = PageVersion::lang2domain(Yii::app()->language);
echo Yii::t('mail_postal', 'Paczkę możesz śledzić {a}tutaj{/a}.', array('{a}' => '<a href="'.$baseUrl.'/tt/'.$model->local_id.'">', '{/a}' => '</a>')); ?><br/><br/>

<br/><br/>
<?php
if($model->senderAddressData !== NULL):
    ?>
    <strong><?= Yii::t('mail_postal', 'Nadawca paczki:');?></strong><br/>
    <?= $model->senderAddressData->getUsefulName(); ?><br/>
    <?= $model->senderAddressData->address_line_1; ?><br/>
    <?= $model->senderAddressData->address_line_2 != '' ? $model->senderAddressData->address_line_2.'<br/>' : '';?>
    <?= $model->senderAddressData->city;?>, <?= $model->senderAddressData->zip_code;?><br/>
    <?= $model->senderAddressData->getCountryNameUniversal();?><br/><br/>
<?php
endif;
?>
<?php
if($model->receiverAddressData !== NULL):
    ?>
    <strong><?= Yii::t('mail_postal', 'Odbiorca paczki:');?></strong><br/>
    <?= $model->receiverAddressData->getUsefulName(); ?><br/>
    <?= $model->receiverAddressData->address_line_1; ?><br/>
    <?= $model->receiverAddressData->address_line_2 != '' ? $model->receiverAddressData->address_line_2.'<br/>' : '';?>
    <?= $model->receiverAddressData->city;?>, <?= $model->receiverAddressData->zip_code;?><br/>
    <?= $model->receiverAddressData->getCountryNameUniversal();?>
<?php
endif;
?>


