<?php

error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
ini_set('display_errors', '1');

ini_set("log_errors", 1);
ini_set("error_log", "rror.txt");

// change the following paths if necessary
//$yii=dirname(__FILE__).'/../framework/yii.php';
//$yii=dirname(__FILE__).'/../framework/YiiBase.php';
// TO USE WITH OPCACHE:
//$yii=dirname(__FILE__).'/../framework/yiilite.php';
$configBackend=dirname(__FILE__).'/protected/backend/config/main.php';

if($_SERVER['REMOTE_ADDR'] == '127.0.0.1' OR $_SERVER['REMOTE_ADDR'] == '::1') {
    define('YII_LOCAL', true);
}
else
    define('YII_LOCAL', false);

require_once(dirname(__FILE__).'/../isOn.php');

if((!IS_SYSTEM_ON && $_SERVER['REMOTE_ADDR'] != IS_SYSTEM_ON_SAFE_IP))
{
	require_once('index.html.inc');
	exit;
}


//debug
defined('YII_DEBUG') or define('YII_DEBUG',true );
//show profileradmin    bbbb
defined('YII_DEBUG_SHOW_PROFILER') or define('YII_DEBUG_SHOW_PROFILER',true);
//enable profiling
defined('YII_DEBUG_PROFILING') or define('YII_DEBUG_PROFILING',true);
//trace level
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',0);
//execution time
defined('YII_DEBUG_DISPLAY_TIME') or define('YII_DEBUG_DISPLAY_TIME',false);
//require_once($yii);

// TO USE WITH YIIBASE
//class Yii extends YiiBase
//{
//    /**
//     * @static
//     * @return CWebApplication
//     */
//    public static function app()
//    {
//        return parent::app();
//    }
//}


require_once(dirname(__FILE__).'/vendor/autoload.php');

Yii::getLogger()->autoDump=true;
Yii::getLogger()->autoFlush=1;
Yii::createWebApplication($configBackend)->run();



