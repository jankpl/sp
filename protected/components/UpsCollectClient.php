<?php

class UpsCollectClient extends CApplicationComponent{

    protected $access = GLOBAL_CONFIG::UPS_ACCESS;
    protected $userid = GLOBAL_CONFIG::UPS_USERID;
    protected $passwd = GLOBAL_CONFIG::UPS_PASS;

    protected $endpointurl = GLOBAL_CONFIG::UPS_ENDPOINT_COLLECTION;

    protected $outputFileName = "XOLTResult.xml";
    protected $wsdl = "";
    protected $operation = "ProcessPickupCreation";

    protected $shipper = "R2E500";
    protected $shipper_country = "PL";

    protected $pa_company;
    protected $pa_contact_name;
    protected $pa_address_line_1;
    protected $pa_address_line_2;
    protected $pa_room;
    protected $pa_floor;
    protected $pa_city;
    protected $pa_state;
    protected $pa_postal_code;
    protected $pa_country_code;
    protected $pa_tel;
    protected $pa_residential_indicator;

    protected $pp_quantity;
    protected $pp_code;

    protected $reicever_county_code;

    protected $total_weight;

    protected $reference_number;

    protected $service_code;

    protected $pickup_time_open;
    protected $pickup_time_close;
    protected $pickup_date;

    protected $_lastRequest;

    public function __construct()
    {
        $this->wsdl = YiiBase::getPathOfAlias('application.components.ups-collect').'/Pickup.wsdl';
        $this->init();
    }

//    public function init()
//    {
//        return parent::init();
//    }

    public function quequeCollectionForCourier(Courier $courier)
    {
        // For Type Internal do not call for this package if is not first in group. Collection will be ordered for parent package
        if($courier->courierTypeInternal != NULL && $courier->courierTypeInternal->parent_courier_id != NULL)
            return true;

        // For Type Internal do not call if without pickup option
        // PICKUP_MOD / 14.09.2016
//        if($courier->courierTypeInternal != NULL && !$courier->courierTypeInternal->with_pickup)
        if($courier->courierTypeInternal != NULL && $courier->courierTypeInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR)
            return true;

        // For Type Internal Simple do not call if without pickup option
        if($courier->courierTypeInternalSimple != NULL && !$courier->courierTypeInternalSimple->with_pickup)
            return true;


        if($courier->courierTypeU != NULL && !$courier->courierTypeU->collection)
            return true;

        // DO NOT CALL UPS COLLECT SERVICE IF THIS OPTION IS BLOCKED BY ADMIN FOR THIS USER
        if($courier->user != NULL )
        {
            if($courier->getType() == Courier::TYPE_DOMESTIC)
            {
                if ($courier->user->getDomesticCollectBlock())
                    return true;
            }
            else
            {
                if ($courier->user->getUpsCollectBlock())
                    return true;
            }
        }

        // SPECIAL RULE 06.10.2016
        // Block collect for UPS_M
        if($courier->courierTypeInternal != NULL)
            if($courier->courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_M OR $courier->courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_SAVER_M)
                return true;

        if($courier->courierTypeDomestic != NULL)
            if($courier->courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_M_EXPRESS)
                return true;


        $courierCollect = new CourierCollect();
        $courierCollect->courier_id = $courier->id;
        $courierCollect->stat =  CourierCollect::STAT_WAITING;
        $courierCollect->collect_operator_id = CourierCollect::OPERATOR_UPS;
        return $courierCollect->save();
    }

    public function callCollectionForCourierInternal(CourierTypeInternal $courierTypeInternal)
    {

        // service only for orders with pickup
        // PICKUP_MOD / 14.09.2016
//        if(!$courierTypeInternal->with_pickup)
        if($courierTypeInternal->with_pickup != CourierTypeInternal::WITH_PICKUP_REGULAR)
            return false;

        $sad = $courierTypeInternal->courier->senderAddressData;

        $this->pa_company = $sad->company == '' ? $sad->name : $sad->company;
        $this->pa_contact_name = $sad->name == '' ? $sad->company : $sad->name;
        $this->pa_address_line_1 = $sad->address_line_1;
        $this->pa_address_line_2 = $sad->address_line_2;
        $this->pa_room = NULL;
        $this->pa_floor = NULL;
        $this->pa_city = $sad->city;
        $this->pa_state = NULL;
        $this->pa_postal_code = $sad->zip_code;
        $this->pa_country_code = $sad->country0->code;

        $this->pa_tel = $sad->tel;

        $this->pa_residential_indicator = $sad->company !='' ? 'N' : 'Y';

        $this->pp_quantity = $courierTypeInternal->courier->packages_number;

        $this->total_weight = round($courierTypeInternal->courier->package_weight * $courierTypeInternal->courier->packages_number, 1) ;

        $this->reference_number = $courierTypeInternal->courier->local_id;


        if(UpsSoapShipClient::isCourierOperatorUpsSaver($courierTypeInternal->pickup_operator_id))
            $this->service_code = '065';
        else if(UpsSoapShipClient::isCourierOperatorUps($courierTypeInternal->pickup_operator_id))
            $this->service_code = '011';
        else
            return false;

        if($courierTypeInternal->_preffered_pickup_day != '')
            $this->pickup_date = $courierTypeInternal->_preffered_pickup_day;

        if($courierTypeInternal->_preffered_pickup_time != '')
        {
            $timeData = PrefferedPickupTime::convertDateNameToOpenClose($courierTypeInternal->_preffered_pickup_time);

            $this->pickup_time_open =  $timeData['open'];
            $this->pickup_time_close = $timeData['close'];
        }

        // if common operator, delivery address is receiver address. Otherwise, delivery address is pickup hub address
        // PICKUP_MOD / 14.09.2016
//        if($courierTypeInternal->common_operator_ordered && $courierTypeInternal->with_pickup)
        if($courierTypeInternal->common_operator_ordered && $courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_REGULAR)
            $this->reicever_county_code = $courierTypeInternal->courier->receiverAddressData->country0->code;
        else
            $this->reicever_county_code = $courierTypeInternal->pickupHub->address->country0->code;

        $obj = new stdClass();
        if($courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_BIS OR $courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_SAVER_BIS)
        {
            $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_BIS);

        }
        else if($courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_F OR $courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_SAVER_F) {

            $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_F);

        }
        else if($courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_M OR $courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_SAVER_M)
        {
            $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_M);

        }
        else if($courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_SAVER_MCM)
        {
            $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_MCM);

        }
        else if($courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_UK) {

            $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_UK);

        }
        else if($courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_EXPRESS_L)
        {
            $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_L);

        }
        else if($courierTypeInternal->pickup_operator_id == CourierOperator::OPERATOR_UPS_SAVER_4VALUES)
        {
            $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_4VALUES);

        }

        $this->access = $obj->access;
        $this->userid = $obj->userid;
        $this->passwd = $obj->passwd;
        $this->shipper = $obj->shipper;
        $this->shipper_country = $obj->shipper_country;

        // SPECIAL RULE 04.10.2016
        // custom UPS account for user "cloudpack"
        if($courierTypeInternal->courier->user_id == 442 && $courierTypeInternal->courier->senderAddressData->country_id == CountryList::COUNTRY_PL && $courierTypeInternal->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
        {
            if($courierTypeInternal->courier->user->getUpsCustomData('active'))
            {
                $this->access = $courierTypeInternal->courier->user->getUpsCustomData('id');
                $this->userid = $courierTypeInternal->courier->user->getUpsCustomData('login');
                $this->passwd = $courierTypeInternal->courier->user->getUpsCustomData('password');
                $this->shipper = $courierTypeInternal->courier->user->getUpsCustomData('shipper');
                $this->shipper_country = $courierTypeInternal->courier->user->getUpsCustomData('shipper_country');
            }
        }

        // SPECIAL RULE 03.11.2016
        // custom UPS account for user "CUSTOMERITUM"
        if($courierTypeInternal->courier->user_id == 520 && $courierTypeInternal->courier->senderAddressData->country_id == CountryList::COUNTRY_PL && $courierTypeInternal->courier->receiverAddressData->country_id == CountryList::COUNTRY_PL)
        {
            if($courierTypeInternal->courier->user->getUpsCustomData('active'))
            {
                $this->access = $courierTypeInternal->courier->user->getUpsCustomData('id');
                $this->userid = $courierTypeInternal->courier->user->getUpsCustomData('login');
                $this->passwd = $courierTypeInternal->courier->user->getUpsCustomData('password');
                $this->shipper = $courierTypeInternal->courier->user->getUpsCustomData('shipper');
                $this->shipper_country = $courierTypeInternal->courier->user->getUpsCustomData('shipper_country');
            }
        }


        return $this->go(true);
    }

    public function callCollectionForCourierU(CourierTypeU $courierTypeU)
    {

        // service only for orders with pickup
        // PICKUP_MOD / 14.09.2016
//        if(!$courierTypeInternal->with_pickup)
        if(!$courierTypeU->collection)
            return false;

        $sad = $courierTypeU->courier->senderAddressData;

        $this->pa_company = $sad->company == '' ? $sad->name : $sad->company;
        $this->pa_contact_name = $sad->name == '' ? $sad->company : $sad->name;
        $this->pa_address_line_1 = $sad->address_line_1;
        $this->pa_address_line_2 = $sad->address_line_2;
        $this->pa_room = NULL;
        $this->pa_floor = NULL;
        $this->pa_city = $sad->city;
        $this->pa_state = NULL;
        $this->pa_postal_code = $sad->zip_code;
        $this->pa_country_code = $sad->country0->code;

        $this->pa_tel = $sad->tel;

        $this->pa_residential_indicator = $sad->company !='' ? 'N' : 'Y';

        $this->pp_quantity = $courierTypeU->courier->packages_number;

        $this->total_weight = round($courierTypeU->courier->package_weight * $courierTypeU->courier->packages_number, 1) ;

        $this->reference_number = $courierTypeU->courier->local_id;

        if(UpsSoapShipClient::isCourierUOperatorSaver($courierTypeU->courierUOperator))
            $this->service_code = '065';
        else
            $this->service_code = '011';



//        if($courierTypeU->_preffered_pickup_day != '')
//            $this->pickup_date = $courierTypeU->_preffered_pickup_day;
//
//        if($courierTypeU->_preffered_pickup_time != '')
//        {
//            $timeData = PrefferedPickupTime::convertDateNameToOpenClose($courierTypeU->_preffered_pickup_time);
//
//            $this->pickup_time_open =  $timeData['open'];
//            $this->pickup_time_close = $timeData['close'];
//        }

        $obj = new stdClass();
        switch($courierTypeU->courierUOperator->uniq_id)
        {
            case CourierUOperator::UNIQID_UPS_M:
            case CourierUOperator::UNIQID_UPS_M_SAVER:
                $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_M);
                break;
            case CourierUOperator::UNIQID_UPS_F:
            case CourierUOperator::UNIQID_UPS_F_SAVER:
                $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_F);;
                break;
            case CourierUOperator::UNIQID_UPS_BIS:
            case CourierUOperator::UNIQID_UPS_BIS_SAVER:
                $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_BIS);
                break;
            case CourierUOperator::UNIQID_UPS_L:
            case CourierUOperator::UNIQID_UPS_L_SAVER:
                $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_L);
                break;
            case CourierUOperator::UNIQID_UPS_UK:
                $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_UK);
                break;
            case CourierUOperator::UNIQID_UPS_MCM_SAVER:
                $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_MCM);
                break;
        }

        $this->access = $obj->access;
        $this->userid = $obj->userid;
        $this->passwd = $obj->passwd;
        $this->shipper = $obj->shipper;
        $this->shipper_country = $obj->shipper_country;

        return $this->go(true);
    }

    public function callCollectionForCourierInternalSimple(CourierTypeInternalSimple $courierTypeInternalSimple)
    {
        // service only for orders with pickup
        if(!$courierTypeInternalSimple->with_pickup)
            return false;



        $sad = $courierTypeInternalSimple->courier->senderAddressData;

        $this->pa_company = $sad->company == '' ? $sad->name : $sad->company;
        $this->pa_contact_name = $sad->name == '' ? $sad->company : $sad->name;
        $this->pa_address_line_1 = $sad->address_line_1;
        $this->pa_address_line_2 = $sad->address_line_2;
        $this->pa_room = NULL;
        $this->pa_floor = NULL;
        $this->pa_city = $sad->city;
        $this->pa_state = NULL;
        $this->pa_postal_code = $sad->zip_code;
        $this->pa_country_code = $sad->country0->code;

        $this->pa_tel = $sad->tel;

        $this->pa_residential_indicator = $sad->company !='' ? 'N' : 'Y';

        $this->pp_quantity = $courierTypeInternalSimple->courier->packages_number;

        $this->total_weight = $courierTypeInternalSimple->courier->package_weight;

        $this->reference_number = $courierTypeInternalSimple->courier->local_id;

        if($courierTypeInternalSimple->operator == CourierTypeInternalSimple::OPERATOR_UPS)
            $this->service_code = '011';
        else if($courierTypeInternalSimple->operator == CourierTypeInternalSimple::OPERATOR_UPS_SAVER)
            $this->service_code = '065';
        else
            return false;

        if($courierTypeInternalSimple->_preffered_pickup_day != '')
            $this->pickup_date = $courierTypeInternalSimple->_preffered_pickup_day;

        if($courierTypeInternalSimple->_preffered_pickup_time != '')
        {
            $timeData = PrefferedPickupTime::convertDateNameToOpenClose($courierTypeInternalSimple->_preffered_pickup_time);

            $this->pickup_time_open =  $timeData['open'];
            $this->pickup_time_close = $timeData['close'];
        }

        $this->reicever_county_code = $courierTypeInternalSimple->courier->receiverAddressData->country0->code;

        return $this->go(true);
    }


    public function callCollectionForCourierDomestic(CourierTypeDomestic $courierTypeDomestic)
    {


        $sad = $courierTypeDomestic->courier->senderAddressData;

        $this->pa_company = $sad->company == '' ? $sad->name : $sad->company;
        $this->pa_contact_name = $sad->name == '' ? $sad->company : $sad->name;
        $this->pa_address_line_1 = $sad->address_line_1;
        $this->pa_address_line_2 = $sad->address_line_2;
        $this->pa_room = NULL;
        $this->pa_floor = NULL;
        $this->pa_city = $sad->city;
        $this->pa_state = NULL;
        $this->pa_postal_code = $sad->zip_code;
        $this->pa_country_code = $sad->country0->code;

        $this->pa_tel = $sad->tel;

        $this->pa_residential_indicator = $sad->company !='' ? 'N' : 'Y';

        $this->pp_quantity = $courierTypeDomestic->courier->packages_number;

        $this->total_weight = $courierTypeDomestic->courier->package_weight;

        $this->reference_number = $courierTypeDomestic->courier->local_id;


        if($courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS)
            $this->service_code = '011';
        else if($courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_SAVER)
            $this->service_code = '065';
        else if($courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_EXPRESS OR $courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_M_EXPRESS)
            $this->service_code = '007';
        else
            return false;

        $this->reicever_county_code = $courierTypeDomestic->courier->receiverAddressData->country0->code;

        if($courierTypeDomestic->domestic_operator_id == CourierDomesticOperator::OPERATOR_UPS_M_EXPRESS)
        {
            $obj = UpsSoapShipClient::_shipperData(UpsSoapShipClient::SHIPPER_M);

            $this->access = $obj->access;
            $this->userid = $obj->userid;
            $this->passwd = $obj->passwd;
            $this->shipper = $obj->shipper;
            $this->shipper_country = $obj->shipper_country;

        }

        // custom UPS account for selected users
        if($courierTypeDomestic->courier->user_id !== NULL)
        {
            if($courierTypeDomestic->courier->user->getUpsCustomData('active'))
            {
                $this->access = $courierTypeDomestic->courier->user->getUpsCustomData('id');
                $this->userid = $courierTypeDomestic->courier->user->getUpsCustomData('login');
                $this->passwd = $courierTypeDomestic->courier->user->getUpsCustomData('password');
                $this->shipper = $courierTypeDomestic->courier->user->getUpsCustomData('shipper');
                $this->shipper_country = $courierTypeDomestic->courier->user->getUpsCustomData('shipper_country');
            }
        }



        return $this->go(true);
    }

    private function processPickupCreation($shiftDays = 0)
    {


        // ADD USA STATE NAME IF PACKAGE IS FROM USA
        $stateProvinceCode = '';
        if($this->pa_country_code == 'US')
        {
            $state = UsaZipState::getStateByZip($this->pa_country_code, true);
            $stateProvinceCode = $state;
        }
        else if($this->pa_country_code == 'CA')
        {
            $state = CanadaZipState::getStateByZip($this->pa_country_code);
            $stateProvinceCode = $state;
        }

        $request = new stdClass();
        $request->Request = new stdClass();
        $request->Request->RequestOption = '';
        $request->RatePickupIndicator = 'N';

        $request->Shipper = new stdClass();
        $request->Shipper->Account = new stdClass();
        $request->Shipper->Account->AccountNumber = $this->shipper;
        $request->Shipper->Account->AccountCountryCode = $this->shipper_country;

        $request->PickupDateInfo = new stdClass();
        $request->PickupDateInfo->CloseTime =  $this->pickup_time_close ? $this->pickup_time_close :  '1700';
        $request->PickupDateInfo->ReadyTime =  $this->pickup_time_open ? $this->pickup_time_open :  '0800';
        $request->PickupDateInfo->PickupDate = $this->pickup_date ? $this->pickup_date : date('Ymd');

        if($shiftDays)
        {
            $shiftDate = S_Useful::workDaysNextDate($request->PickupDateInfo->PickupDate, $shiftDays);
            $request->PickupDateInfo->PickupDate = str_replace('-','', $shiftDate);
        }

        $request->PickupAddress = new stdClass();
        $request->PickupAddress->CompanyName = mb_substr(S_Useful::removeNationalCharacters($this->pa_company),0,27);
        $request->PickupAddress->ContactName = mb_substr(S_Useful::removeNationalCharacters($this->pa_contact_name),0,22);
        $request->PickupAddress->AddressLine = S_Useful::removeNationalCharacters(mb_substr($this->pa_address_line_1.' '.$this->pa_address_line_2, 0 ,73));


        $request->PickupAddress->Floor = ''; // 2
        $request->PickupAddress->City = mb_substr(S_Useful::removeNationalCharacters($this->pa_city),0,50);
        $request->PickupAddress->StateProvince = $stateProvinceCode; // NJ
        $request->PickupAddress->Urbanization = '';
        $request->PickupAddress->PostalCode = mb_substr(str_replace('-','', $this->pa_postal_code),0,8);
        $request->PickupAddress->CountryCode = $this->pa_country_code;
        $request->PickupAddress->ResidentialIndicator = $this->pa_residential_indicator;
        $request->PickupAddress->PickupPoint = ''; // Lobby

        $request->PickupAddress->Phone = new stdClass();
        $request->PickupAddress->Phone->Number = mb_substr(str_replace('-','', $this->pa_tel),0,25);
        $request->PickupAddress->Phone->Extension = ''; // 911

        $request->AlternateAddressIndicator = 'Y';

        $request->PickupPiece = new stdClass();
        $request->PickupPiece->ServiceCode = $this->service_code;
        $request->PickupPiece->Quantity = $this->pp_quantity;
        $request->PickupPiece->DestinationCountryCode = $this->reicever_county_code;
        $request->PickupPiece->ContainerCode = '01';

//        $request->TotalWeight = new stdClass();
//        $request->TotalWeight->Weight = $this->total_weight;
//        $request->TotalWeight->UnitOfMeasurement = 'KGS';

        $request->OverweightIndicator = 'N';
        $request->PaymentMethod = '01';
        $request->SpecialInstruction = ''; // Test
        $request->ReferenceNumber = $this->reference_number;

        $this->_lastRequest = $request;

//        $request->Notification = new stdClass();
//        $request->Notification->UndeliverableEmailAddress = '';
//        $request->Notification->ConfirmationEmailAddress = array();
//        $request->Notification->ConfirmationEmailAddress[0] = 'jtk@hss.pl';


        return $request;

    }

    private function go($returnError = false, $shiftDays = 0)
    {
        // SPECIAL RULE 06.10.2016
        // Block collect for UPS_M
        if($this->userid == 'mr2016') {
            $return = array(
                'error' => 'Collect jest wyłączony dla UPS_M',
                'data' => '',
            );

            return $return;
        }

        try {

            $mode = array
            (
//                'soap_version' => SOAP_1_1,  // use soap 1.1 client
//                'trace' => 1
            );

            // initialize soap client
            $client = new SoapClient($this->wsdl, $mode);

            //set endpoint url
            $client->__setLocation($this->endpointurl);

            //create soap header
            $usernameToken['Username'] = $this->userid;
            $usernameToken['Password'] = $this->passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $this->access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', $upss);
            $client->__setSoapHeaders($header);

            if (strcmp($this->operation, "ProcessPickupCreation") == 0) {
                //get response


                $resp = $client->__soapCall($this->operation, array($this->processPickupCreation($shiftDays)));
                MyDump::dump('ups-collect.txt', print_r($client->__getLastRequest(),1));
                MyDump::dump('ups-collect.txt', print_r($resp,1));

                $data = $this->_lastRequest;
                $data->ShiftDays = $shiftDays;

                $response = array(
                    'desc' => $resp->Response->ResponseStatus->Description,
                    'prn' => $resp->PRN,
                    'data' => $data,
                );

                //save soap request and response to file
//                $fw = fopen('collect.txt', 'w');
//                fwrite($fw, "Request: \n" . $client->__getLastRequest() . "\n");
//                fwrite($fw, "Response: \n" . $client->__getLastResponse() . "\n");
//                fclose($fw);


                return $response;
            }


        } catch (Exception $ex) {
            if($returnError)
            {

                MyDump::dump('ups-collect.txt', print_r($client->__getLastRequest(),1));
                MyDump::dump('ups-collect.txt', print_r($client->__getLastResponse(),1));

                // if collect current day is not possible, try the next day...
                // 9510110 - Closing time is less than the minimum window after present time
                // 9510113 -
                // 9510118 - The service is not available for pickup on the selected day
                if(in_array($ex->detail->Errors->ErrorDetail->PrimaryErrorCode->Code, ['9510113', '9510110', '9510118']) && $shiftDays < 3)
                {
                    return $this->go($returnError, $shiftDays + 1);
                }

                $data = $this->_lastRequest;
                $data->ShiftDays = $shiftDays;

                $return = array(
                    'error' => $ex->getMessage().' / '.$ex->detail->Errors->ErrorDetail->PrimaryErrorCode->Description .' ('.$ex->detail->Errors->ErrorDetail->PrimaryErrorCode->Code.')',
                    'data' => $data,
                );
                return $return;
            } else {
                return false;
            }
        }

    }



}