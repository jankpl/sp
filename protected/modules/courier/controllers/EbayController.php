<?php

class EbayController extends Controller {

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }


    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('login', 'logout', 'afterLogin', 'afterLoginFail'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionLogin($return)
    {
        $ebayClient = ebayClient::instance();
        $ebayClient->requestLogin($return);
    }

    public function actionLogout($return)
    {
        $ebayClient = ebayClient::instance();
        $ebayClient->logout();

        $url = $this->returnUrl($return);

        Yii::app()->user->setFlash('notice', Yii::t('courier_ebay', 'Zostałeś wylogowany z konta na eBay!'));
        $this->redirect($url);
    }

    protected function returnUrl($return)
    {
        switch($return)
        {
            case ebayClient::RETURN_OOE:
                $url = Yii::app()->createUrl('/courier/courierOoe/ebay');
                break;
            case ebayClient::RETURN_INTERNAL:
                $url = Yii::app()->createUrl('/courier/courierInternal/ebay');
                break;
            case ebayClient::RETURN_U:
                $url = Yii::app()->createUrl('/courier/courierU/ebay');
                break;
            case ebayClient::RETURN_POSTAL:
                $url = Yii::app()->createUrl('/postal/postal/ebay');
                break;
            default:
                $url = Yii::app()->createUrl('/');
        }

        return $url;
    }

    public function actionAfterLogin()
    {
        $ebayClient = ebayClient::instance();
        if ($ebayClient->getIsJustLogged())
            Yii::app()->user->setFlash('notice', Yii::t('courier_ebay', 'Zostałeś zalogowany na konto na eBay!'));

        $url = $this->returnUrl($ebayClient->getReturnToLocation());
        $this->redirect($url);
    }

    public function actionAfterLoginFail()
    {
        $ebayClient = ebayClient::instance();

        $url = $this->returnUrl($ebayClient->getReturnToLocation());
        $ebayClient->logout();

        Yii::app()->user->setFlash('error', Yii::t('courier_ebay', 'Logowanie na konto eBay nie powiodło się!'));
        $this->redirect($url);
    }
}