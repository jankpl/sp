<?php

class B2C2CorreosController extends Controller
{


    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
//                'expression' => Admin::AUTHORITY_COURIER_SCANNER.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($hash = false, $confirm = false)
    {
        Yii::app()->getModule('courier');

        $model = new F_B2C2CorreosForm();



        if($hash)
        {

            $fileData =  Yii::app()->cacheUserData->get('B2C2Correos'.$hash);

            if(!$fileData) {
                Yii::app()->user->setFlash('error', 'No data found!');
                $this->refresh();
                Yii::app()->end();
            }

            if($confirm && md5($hash) == $confirm)
            {
                $done = 0;
                $ignored = 0;
                foreach($fileData AS $line)
                {
                    if($clnId = F_B2C2CorreosForm::isOryginalNoValid($line[0]))
                    {
                        /* @var $cln CourierLabelNew */
                        $cln = CourierLabelNew::model()->findByPk($clnId);

                        $cln->courier->addToLog('Changed TT number by B2C2Correos from: "'.$cln->track_id.'" to: "'.$line[1].'"');
                        $cln->track_id = $line[1];
                        $cln->update('track_id');
                        $done++;

                    } else
                        $ignored++;

                }

                if($done)
                    Yii::app()->user->setFlash('success', 'Number of changed items: '.$done);

                if($ignored)
                    Yii::app()->user->setFlash('error', 'Number of NOT changed items: '.$ignored);

                $this->redirect(['/b2C2Correos/index']);
                Yii::app()->end();
            }


            $this->render('index',
                [
                    'model' => $model,
                    'fileData' => $fileData,
                    'hash' => $hash,
                ]);
            Yii::app()->end();
        }

        if(isset($_POST['F_B2C2CorreosForm'])) {

            $model->file = CUploadedFile::getInstance($model, 'file');

            if($model->validate())
            {

                $fileData = $model->fileToArray($model->file->tempName);

                $hash = hash('sha512', uniqid(time(),true));
                Yii::app()->cacheUserData->set('B2C2Correos'.$hash, $fileData, 60*60);

                $this->redirect(['/b2C2Correos/index', 'hash' => $hash]);
                exit;
            }
        }

        $this->render('index',
            [
                'model' => $model,
            ]);
    }
}