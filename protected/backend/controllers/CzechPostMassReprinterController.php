<?php

class CzechPostMassReprinterController extends Controller
{


    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => Admin::AUTHORITY_ADMIN.' >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }

    function actionIndex()
    {
        Yii::app()->getModule('postal');


        if (isset($_POST['items_ids'])) {


            $force_ddu_mode = intval($_POST['force_ddu_mode']);

            $items_ids = $_POST['items_ids'];
            $items_ids = explode(PHP_EOL, $items_ids);

            foreach ($items_ids AS $key => $item) {

                $item = trim(preg_replace("/[^\w]+/", "", $item));

                if ($item == '')
                    unset($items_ids[$key]);
                else
                    $items_ids[$key] = "'" . $item . "'";
            }

            $models = [];


            if (S_Useful::sizeof($items_ids)) {

                $items_ids = implode(',', $items_ids);

                $models = Postal::model()->with('postalLabel')->with('postalAdditionalExtId')->with('postalOperator')->findAll(['condition' => '(postalLabel.track_id IN (' . $items_ids . ') OR postalAdditionalExtId.external_id IN (' . $items_ids . '))', 'order' => 't.id DESC']);

                if(!S_Useful::sizeof($models))
                {
                    Yii::app()->user->setFlash('error', 'Not a single item found!');
                    $this->refresh();
                }

                $pdf = PDFGenerator::initialize();
                foreach($models AS $model)
                    $pdf = CzechPostClient::generateLabelByPostal($model, false, false, false, false, $pdf, $force_ddu_mode, true);

                $pdf->Output('cp.pdf', 'D');

//                $this->refresh();
                Yii::app()->end();
            }


            if(!S_Useful::sizeof($models)) {
                Yii::app()->user->setFlash('error', 'Zero items found!');
                $this->refresh();
                exit;
            }

        }

        $this->render('index', [

        ]);
    }



}