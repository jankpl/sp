<?php

class _PostalGridViewUser extends Postal
{
    public $__receiver_country_id;
    public $__receiver_usefulName;
    public $__receiver_name;
    public $__receiver_company;
    public $__receiver_address;
    public $__receiver_city;
    public $__receiver_zip_code;
    public $__receiver_tel;
    public $__sender_country_id;
    public $__sender_usefulName;
    public $__sender_name;
    public $__sender_company;
    public $__sender_address;
    public $__sender_city;
    public $__sender_zip_code;
    public $__sender_tel;
    public $__stat;

    public $__type;

    public $__external_id;

    public $subGrid;

    public function rules() {

        $array = array(

            array('__sender_country_id,
                 __sender_usefulName,
                 __sender_name,
                 __sender_company,
                 __sender_address,
                 __sender_city,
                 __sender_zip_code,
                 __sender_tel,
                 __receiver_country_id,
                 __receiver_usefulName,
                 __receiver_name,
                 __receiver_company,
                 __receiver_address,
                 __receiver_city,
                 __receiver_zip_code,
                 __receiver_tel,
                 __stat,
                 __type,
                 value,
                 local_id,
                 __external_id,
                 ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        if($_GET['sm'])
            $this->stat_map_id = intval($_GET['sm']);
        if($_GET['de'])
            $this->date_entered = $_GET['de'];

        if(strpos($this->date_entered,'|'))
        {
            $dateEntered = explode('|', $this->date_entered);
            $start = $dateEntered[0];
            $end = $dateEntered[1];

            if(S_Useful::checkDate($start) && S_Useful::checkDate($end))
                $criteria->addBetweenCondition('(t.date_entered)', $start.' 00:00:00', $end .' 23:59:59');
            else
                $this->date_entered = '';

        } else {
            $criteria->compare('(t.date_entered)', $this->date_entered, true);
        }

        if(strpos($this->date_updated,'|'))
        {
            $dateUpdated = explode('|', $this->date_updated);
            $start = $dateUpdated[0];
            $end = $dateUpdated[1];

            if(S_Useful::checkDate($start) && S_Useful::checkDate($end))
                $criteria->addBetweenCondition('(t.date_updated)', $start.' 00:00:00', $end .' 23:59:59');
            else
                $this->date_updated = '';

        } else {
            $criteria->compare('date(t.date_updated)', $this->date_updated, true);
        }

        $criteria->compare('t.id', $this->id, true);

        $criteria->compare('local_id', $this->local_id, true);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('sender_address_data_id', $this->sender_address_data_id);
        $criteria->compare('receiver_address_data_id', $this->receiver_address_data_id);
        $criteria->compare('postal_stat_id', $this->postal_stat_id);
        $criteria->compare('weight_class', $this->weight_class);
        $criteria->compare('size_class', $this->size_class);
        $criteria->compare('postal_type', $this->postal_type);
        $criteria->compare('user_id', Yii::app()->user->id);
        $criteria->compare('bulk_id', $this->bulk_id);
        $criteria->compare('bulk', $this->bulk);
        $criteria->compare('bulk_number', $this->bulk_number);
        $criteria->compare('stat_map_id', $this->stat_map_id);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('value', $this->value);
        $criteria->compare('postalLabel.track_id', $this->__external_id, true);
        $criteria->compare('note1', $this->note1, true);
        $criteria->compare('note2', $this->note2, true);
        $criteria->compare('ref', $this->ref, true);

        // $criteria->compare('__type', $this->__type);
//        $criteria->addCondition('postal_type_external_id IS NULL');
//        $criteria->addCondition('postal_type_internal_id IS NOT NULL');


        if($this->__stat)
        {
            $criteria->compare('t.postal_stat_id',$this->__stat);
        }


        if($this->__sender_country_id)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');

            if($this->__sender_country_id == -1)
                $criteria->addCondition('senderAddressData.country_id != '.CountryList::COUNTRY_PL);
            else
                $criteria->compare('senderAddressData.country_id',$this->__sender_country_id);


        }

        if($this->__receiver_country_id)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');

            if($this->__receiver_country_id == -1)
                $criteria->addCondition('receiverAddressData.country_id != '.CountryList::COUNTRY_PL);
            else
                $criteria->compare('receiverAddressData.country_id',$this->__receiver_country_id);

        }

        if($this->__receiver_usefulName)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.name LIKE :receiverUsefulName OR receiverAddressData.company LIKE :receiverUsefulName)");
            $criteria->params[':receiverUsefulName'] = "%$this->__receiver_usefulName%";
        }

        if($this->__sender_usefulName)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.name LIKE :senderUsefulName OR senderAddressData.company LIKE :senderUsefulName)");
            $criteria->params[':senderUsefulName'] = "%$this->__sender_usefulName%";
        }

//        if($this->__receiver_name)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('receiverAddressData');
//            $criteria->addcondition("(receiverAddressData.name LIKE '%".$this->__receiver_name."%')");
//        }
//
//        if($this->__sender_name)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('senderAddressData');
//            $criteria->addcondition("(senderAddressData.name LIKE '%".$this->__sender_name."%')");
//        }
//
//        if($this->__receiver_company)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('receiverAddressData');
//            $criteria->addcondition("(receiverAddressData.company LIKE '%".$this->__receiver_company."%')");
//        }
//
//        if($this->__sender_company)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('senderAddressData');
//            $criteria->addcondition("(senderAddressData.company LIKE '%".$this->__sender_company."%')");
//        }
//
//        if($this->__receiver_address)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('receiverAddressData');
//            $criteria->addcondition("(receiverAddressData.address_line_1 LIKE '%".$this->__receiver_address."%' OR receiverAddressData.address_line_2 LIKE '%".$this->__receiver_address."%')");
//        }
//
//        if($this->__sender_address)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('senderAddressData');
//            $criteria->addcondition("(senderAddressData.address_line_1 LIKE '%".$this->__receiver_address."%' OR senderAddressData.address_line_2 LIKE '%".$this->__receiver_address."%')");
//        }
//
//        if($this->__receiver_city)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('receiverAddressData');
//            $criteria->addcondition("(receiverAddressData.city LIKE '%".$this->__receiver_city."%')");
//        }
//
//        if($this->__sender_city)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('senderAddressData');
//            $criteria->addcondition("(senderAddressData.city LIKE '%".$this->__sender_city."%')");
//        }
//
//        if($this->__receiver_zip_code)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('receiverAddressData');
//            $criteria->addcondition("(receiverAddressData.zip_code LIKE '%".$this->__receiver_zip_code."%')");
//        }
//
//        if($this->__sender_zip_code)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('senderAddressData');
//            $criteria->addcondition("(senderAddressData.zip_code LIKE '%".$this->__sender_zip_code."%')");
//        }
//
//        if($this->__receiver_tel)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('receiverAddressData');
//            $criteria->addcondition("(receiverAddressData.tel LIKE '%".$this->__receiver_tel."%')");
//        }
//
//        if($this->__sender_tel)
//        {
//            $criteria->together  =  true;
//            $criteria->with = array('senderAddressData');
//            $criteria->addcondition("(senderAddressData.tel LIKE '%".$this->__sender_tel."%')");
//        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes = array(
            '__sender_country_id' => array(
                'asc' => 'senderAddressData.country ASC',
                'desc' => 'senderAddressData.country DESC',
            ),
            '__receiver_country_id' => array(
                'asc' => 'receiverAddressData.country ASC',
                'desc' => 'receiverAddressData.country DESC',
            ),
            '__stat' => array(


                'asc' => 'postalStatTr.short_text ASC',
                'desc' => 'postalStatTr.short_text DESC',
            ),
            '*', // add all of the other columns as sortable
        );

        $criteria = $this->searchCriteria();

        // if there are no conditions (except for User ID), use simple count of packages to improve performance
        $totalCount = NULL;
        if ($criteria->condition == 'user_id=:ycp0') {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(id)')->from((new self)->tableName())->where('user_id = :user_id', [':user_id' => Yii::app()->user->id]);
            $totalCount = $cmd->queryScalar();
        }

        return new CActiveDataProvider($this
            ->with('user')
            ->with('postalLabel')
            ->with('senderAddressData')
            ->with('senderAddressData.country0')
            ->with('receiverAddressData')
            ->with(
                ["receiverAddressData.country0" => [
                    'alias' => 'receiverCountry',
                ]
                ])
//            ->with('stat')
//            ->with('stat.postalStatTr')

            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize'=> Yii::app()->user->getState('pageSizePostal', 50),
                ),
            ));

    }


}