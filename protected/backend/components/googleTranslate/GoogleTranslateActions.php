<?php

class GoogleTranslateActions extends CAction{

    public function run()
    {

        Yii::import('application.components.GoogleTranslate');

        $text = trim($_POST['text']);
        $language = trim($_POST['language']);

        $translate = new \Google\Cloud\Translate\TranslateClient([
            'projectId' => 'swiatprzesylek-1532953022533',
            'keyFilePath' => Yii::app()->basePath.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'googleTranslate'.DIRECTORY_SEPARATOR.'misc'.DIRECTORY_SEPARATOR.'SwiatPrzesylek-37c4d33651d5.json',
        ]);

        $text = 'samolot';
        $language = 'EN';


        $resp = $translate->translate($text, [
            'target' => $language
        ]);

        $data['success'] = $resp ? true : false;

        $data['data'] = $resp;

        echo CJSON::encode($data);
    }
}