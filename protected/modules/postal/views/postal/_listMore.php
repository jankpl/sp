<?php
/* @var $model Postal */
?>

<table class="table table-bordered table-condensed gridview-show-details" style="margin-bottom: 0; table-layout: fixed;">
    <thead>
    <tr>

        <th style="width: 25%;">
            <?php echo Yii::t('postal','Szczegóły przesyłki'); ?>
        </th>
        <th style="width: 25%;">
            <?= Yii::t('postal', 'Nadawca');?>
        </th>
        <th style="width: 25%;">
            <?= Yii::t('postal', 'Odbiorca');?>
        </th>
        <th style="width: 25%;">
            <?= Yii::t('postal', 'Akcja');?>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="vertical-align: top;">
            <?= $model->getAttributeLabel('type');?>: <strong><?= $model->getPostalTypeName();?></strong><br/>
            <?= $model->getAttributeLabel('size_class');?>: <strong><?= $model->getSizeClassName();?></strong><br/>
            <?= $model->getAttributeLabel('weight_class');?>: <strong><?= $model->getWeightClassName();?></strong><br/>
            <?= $model->getAttributeLabel('weight');?>: <strong><?= $model->weight;?> g</strong><br/>
            <?= $model->getAttributeLabel('content');?>: <strong><?= $model->content;?></strong><br/>
            <?= $model->getAttributeLabel('value');?>: <strong><?= ($model->value ? $model->value : '-').' '.$model->value_currency;?></strong><br/>
            <?php
            if($model->postalLabel)
                echo Yii::t('postal', 'External ID').': <strong>'.$model->postalOperator->name.' ('.$model->postalLabel->track_id.')'.'</strong><br/>';
            ?>
        </td>
        <td style="vertical-align: top;">
            <?php
            if($model->senderAddressData !== NULL):
                ?>
                <?= $model->senderAddressData->name != '' ? '<strong>'.$model->senderAddressData->name.'</strong><br/>' : '';?>
                <?= $model->senderAddressData->company != '' ? '<strong>'.$model->senderAddressData->company.'</strong><br/>' : '';?>
                <?= $model->senderAddressData->address_line_1; ?><br/>
                <?= $model->senderAddressData->address_line_2 != '' ? '<strong>'.$model->senderAddressData->address_line_2.'</strong><br/>' : '';?>
                <?= $model->senderAddressData->city; ?>, <?= $model->senderAddressData->zip_code;?><br/>
                <?= $model->senderAddressData->country0->getTrName();?> (<?= $model->senderAddressData->country0->code;?>)
            <?php
            endif;
            ?>
        </td>
        <td style="vertical-align: top;">
            <?php
            if($model->receiverAddressData !== NULL):
                ?>
                <?= $model->receiverAddressData->name != '' ? '<strong>'.$model->receiverAddressData->name.'</strong><br/>' : '';?>
                <?= $model->receiverAddressData->company != '' ? '<strong>'.$model->receiverAddressData->company.'</strong><br/>' : '';?>
                <?= $model->receiverAddressData->address_line_1; ?><br/>
                <?= $model->receiverAddressData->address_line_2 != '' ? '<strong>'.$model->receiverAddressData->address_line_2.'</strong><br/>' : '';?>
                <?= $model->receiverAddressData->city; ?>, <?= $model->receiverAddressData->zip_code;?><br/>
                <?= $model->receiverAddressData->country0->getTrName();?> (<?= $model->receiverAddressData->country0->code;?>)
            <?php
            endif;
            ?>
        </td>
        <td style="vertical-align: top;">
            <ul style="padding: 0; margin: 0;">
                <li><a href="<?= Yii::app()->createUrl('/site/tt',['urlData' => $model->local_id]);?>" target="_blank"><?= Yii::t('postal', 'Track&trace');?></a></li>
                <?php if($model->postal_stat_id != PostalStat::CANCELLED && $model->postal_stat_id != PostalStat::CANCELLED_USER): ?><li><?php echo CHtml::link(Yii::t('postal','Generuj kartę potwierdzenia'), array('/postal/postal/ackCard', 'hash' => $model->hash), array('target' => '_blank', 'data-popup-me' => "AckCard ".$model->local_id));?></li><?php endif;?>
                <?php if(!$model->bulk && $model->postal_stat_id != PostalStat::CANCELLED && $model->postal_stat_id != PostalStat::CANCELLED_USER && $model->postal_stat_id != PostalStat::PACKAGE_PROCESSING):?><li><strong><?= Yii::t('postal', 'Generate label');?>:</strong><br/>
                    <?php
                    $types = PostalLabelPrinter::getTypeNames();
                    $no = S_Useful::sizeof($types);
                    $i = 1;
                    foreach(PostalLabelPrinter::getTypeNames() AS $key => $name):
                        ?>
                        <a href="<?= Yii::app()->createUrl('/postal/postal/CarriageTicket', ['urlData' => $model->hash, 'type' => $key]);?>" target="_blank" class="btn btn-xs" data-popup-me="Label <?= $model->local_id;?> <?= $name;?>"><?= $name; ?></a>
                        <?php
                        if($i < $no)
                            echo ' | ';
                        $i++;
                    endforeach;
                    ?>
                    <?php endif;?>
            </ul>
        </td>
    </tr>
    </tbody>
</table>