<?php

Yii::import('application.models._base.BaseUserDocuments');

class UserDocuments extends BaseUserDocuments
{

    const FINAL_DIR = 'upadmin/user_documents/';

    public $_file;

    const TYPE_PRICE_LIST = 1;
    const TYPE_CONTRACT = 2;
    const TYPE_DEBTS = 3;
    const TYPE_OTHER = 9;


    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),
        );
    }

    public function rules() {
        return array(
            array('user_id, stat, customer_visible, type', 'required'),
            array(' _file, type', 'required', 'except' => 'update, filestring'),
            array('user_id, admin_id, stat, customer_visible, type', 'numerical', 'integerOnly'=>true),
            array('desc, desc_customer, file_name, file_path', 'length', 'max'=>256),
            array('file_type', 'length', 'max'=>64),
            array('file_size', 'length', 'max'=>32),
            array('desc, desc_customer', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, user_id, admin_id, date_entered, stat, customer_visible, desc, desc_customer, file_name, file_path, file_type, file_size, type', 'safe', 'on'=>'search'),
        );
    }

    public static function getFinalDir($fileName)
    {
        $path = realpath(self::FINAL_DIR);
        $path = $path.'/'.$fileName;
        return $path;
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_PRICE_LIST => Yii::t('userDocuments', 'Price list'),
            self::TYPE_CONTRACT => Yii::t('userDocuments', 'Contract'),
            self::TYPE_DEBTS => Yii::t('userDocuments', 'Debts related'),
            self::TYPE_OTHER => Yii::t('userDocuments', 'Other'),
        ];
    }

    public function getTypeName()
    {
        return isset(self::getTypeList()[$this->type]) ? self::getTypeList()[$this->type] : '';
    }

    public function beforeSave()
    {
        if($this->isNewRecord) {
            $this->hash = new CDbExpression('SHA2(:hash, 512)',
                array(':hash' => microtime().uniqid()));

            $this->admin_id = Yii::app()->user->id;
        }
        return parent::beforeSave();
    }

    public function getStatName()
    {
        if($this->stat)
            return 'Active';
        else
            return 'Inactive';
    }

    public function getCustomerVisibilityName()
    {
        if($this->customer_visible)
            return 'Yes';
        else
            return 'No';
    }

    public static function createByFileString($fileString, $fileName, $user_id, $desc, $type)
    {

        $model = new self;
        $model->setScenario('filestring');
        $model->user_id = $user_id;
        $model->stat = 1;
        $model->customer_visible = 1;
        $model->desc = $desc;
        $model->desc_customer = $desc;
        $model->type = $type;

        $transaction = Yii::app()->db->beginTransaction();
        if(!$model->save())
            $errors = true;
        else {
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);

            $model->refresh(); // to get proper hash

            $path = UserDocuments::getFinalDir($model->hash . '.' . $ext);

            if (!file_put_contents($path, $fileString))
                $errors = true;

            $model->file_path = $path;
            $model->file_name = $fileName;
            $model->file_type = mime_content_type($path);
            $model->update(array('file_path', 'file_name', 'file_type'));
        }

        if($errors)
        {
            $transaction->rollback();
            throw new Exception('Error User Document saving!');
        } else {
            $transaction->commit();
            return $model->id;
        }

    }

}