<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-addition-list-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        [
            'name' => 'size_class',
            'value' => '$data->sizeName',
            'filter' => false,
        ],
        [
            'name' => 'pickup_type',
            'value' => '$data->typeName',
            'filter' => false,
        ],
        'date_entered',
        'date_updated',
        [
            'name' => 'country_list_id',
            'value' => '$data->countryList->code',
            'filter' => false,
        ],
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update} {view}',
            'htmlOptions'=> array('style'=>'width:80px'),
        ),
    ),

)); ?>