<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    const EXPORT_MODE_EXTERNAL = 'ext';
    const EXPORT_MODE_INTERNAL = 'std';
    const EXPORT_MODE_INTERNAL_SIMPLE = 'smpl';
    const EXPORT_MODE_OOE = 'ooe';
    const EXPORT_MODE_DOMESTIC = 'dom';
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs=array();

    public $backLink = false;

    public $blockBootstrap = false;

    public $homePage = false;

    public $panelLeftMenuActive = false;

    public function beforeAction($action)
    {

        $this->pageTitle = Yii::app()->name;
        return parent::beforeAction($action);
    }


    public function filters()
    {
        return array(
            array(
                'application.filters.HttpFilter',
                'bypass' => false,
            ),
        );
    }

    function init()
    {

        if(YII_LOCAL) {
            $app = Yii::app();

            $pv = false;
            if ($_GET['lang'] == 'pl') {
                $pv = PageVersion::PAGEVERSION_DEFAULT;
            } else if ($_GET['lang'] == 'en') {
                $pv = PageVersion::PAGEVERSION_RS;
            } else if ($_GET['lang'] == 'en_uk') {
                $pv = PageVersion::PAGEVERSION_PLI;
            } else if ($_GET['lang'] == 'pl_ruch') {
                $pv = PageVersion::PAGEVERSION_RUCH;
            } else if ($_GET['lang'] == 'pl_quriers') {
                $pv = PageVersion::PAGEVERSION_QURIERS;
            } else if ($_GET['lang'] == 'en_orangep') {
                $pv = PageVersion::PAGEVERSION_ORANGE_POST;
            } else if ($_GET['lang'] == 'en_cql') {
                $pv = PageVersion::PAGEVERSION_CQL;
            }

            if ($pv !== false) {
                $app->session['_pv'] = $pv;
            }

            if (isset($app->session['_pv'])) {
                Yii::app()->PV->set($app->session['_pv']);
            }
        }

        if(Yii::app()->NPS->isNewPanelSet())
            $this->layout = '//layouts/panel-restricted';
        else
            $this->layout = '//layouts/other';

        parent::init();
    }




    ////////////////////////////////////////////
    // for compatilibity with GxController
    public function loadModel($key, $modelClass) {

        // Get the static model.
        $staticModel = GxActiveRecord::model($modelClass);

        if (is_array($key)) {
            // The key is an array.
            // Check if there are column names indexing the values in the array.
            reset($key);
            if (key($key) === 0) {
                // There are no attribute names.
                // Check if there are multiple PK values. If there's only one, start again using only the value.
                if (count($key) === 1)
                    return $this->loadModel($key[0], $modelClass);

                // Now we will use the composite PK.
                // Check if the table has composite PK.
                $tablePk = $staticModel->getTableSchema()->primaryKey;
                if (!is_array($tablePk))
                    throw new CHttpException(400, Yii::t('giix', 'Your request is invalid.'));

                // Check if there are the correct number of keys.
                if (count($key) !== count($tablePk))
                    throw new CHttpException(400, Yii::t('giix', 'Your request is invalid.'));

                // Get an array of PK values indexed by the column names.
                $pk = $staticModel->fillPkColumnNames($key);

                // Then load the model.
                $model = $staticModel->findByPk($pk);
            } else {
                // There are attribute names.
                // Then we load the model now.
                $model = $staticModel->findByAttributes($key);
            }
        } else {
            // The key is not an array.
            // Check if the table has composite PK.
            $tablePk = $staticModel->getTableSchema()->primaryKey;
            if (is_array($tablePk)) {
                // The table has a composite PK.
                // The key must be a string to have a PK separator.
                if (!is_string($key))
                    throw new CHttpException(400, Yii::t('giix', 'Your request is invalid.'));

                // There must be a PK separator in the key.
                if (stripos($key, GxActiveRecord::$pkSeparator) === false)
                    throw new CHttpException(400, Yii::t('giix', 'Your request is invalid.'));

                // Generate an array, splitting by the separator.
                $keyValues = explode(GxActiveRecord::$pkSeparator, $key);

                // Start again using the array.
                return $this->loadModel($keyValues, $modelClass);
            } else {
                // The table has a single PK.
                // Then we load the model now.
                $model = $staticModel->findByPk($key);
            }
        }

        // Check if we have a model.
        if ($model === null)
            throw new CHttpException(404, Yii::t('giix', 'The requested page does not exist.'));

        return $model;
    }

    /**
     * Performs the AJAX validation.
     * #MethodTracker
     * This method is based on the gii generated method controller::performAjaxValidation, from version 1.1.7 (r3135). Changes:
     * <ul>
     * <li>Supports multiple models.</li>
     * </ul>
     * @param CModel|array $model The model or array of models to be validated.
     * @param string $form The name of the form. Optional.
     */
    protected function performAjaxValidation($model, $form = null) {
        if (Yii::app()->getRequest()->getIsAjaxRequest() && (($form === null) || ($_POST['ajax'] == $form))) {
            echo GxActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Finds the related primary keys specified in the form POST.
     * Only for HAS_MANY and MANY_MANY relations.
     * @param array $form The post data.
     * @param array $relations A list of model relations in the format returned by {@link CActiveRecord::relations}.
     * @param string $uncheckValue Since Yii 1.1.7, htmlOptions (in {@link CHtml::activeCheckBoxList})
     * has an option named 'uncheckValue'. If you set it to different values than the default value (''), you will
     * need to set the appropriate value to this argument. This method can't be used when 'uncheckValue' is null.
     * @return array An array where the keys are the relation names (string) and the values are arrays with the related model primary keys (int|string) or composite primary keys (array with pk name (string) => pk value (int|string)).
     * Example of returned data:
     * <pre>
     * array(
     *   'categories' => array(1, 4),
     *   'tags' => array(array('id1' => 3, 'id2' => 7), array('id1' => 2, 'id2' => 0)) // composite pks
     * )
     * </pre>
     * An empty array is returned in case there is no related pk data from the post.
     * This data comes directly from the form POST data.
     * @see GxHtml::activeCheckBoxList
     * @throws InvalidArgumentException If uncheckValue is null.
     */
    protected function getRelatedData($form, $relations, $uncheckValue = '') {
        if ($uncheckValue === null)
            throw new InvalidArgumentException(Yii::t('giix', 'giix cannot handle automatically the POST data if "uncheckValue" is null.'));

        $relatedPk = array();
        foreach ($relations as $relationName => $relationData) {
            if (isset($form[$relationName]) && (($relationData[0] == GxActiveRecord::HAS_MANY) || ($relationData[0] == GxActiveRecord::MANY_MANY)))
                $relatedPk[$relationName] = $form[$relationName] === $uncheckValue ? null : $form[$relationName];
        }
        return $relatedPk;
    }







}