<?php

class F_MassStatusChangePostal extends CFormModel
{


    const MODE_SAVE = 1;
    const MODE_SAVE_NO_VALIDATE = 2;
    const MODE_JUST_VALIDATE = 3;
    const MODE_SAVE_CLIENT = 4;

    public $status_text;
    public $status_id;
    public $postal_local_id;
    public $postal_id;
    protected $postalModel;
    public $status_date_text = NULL;
    public $status_date;
    public $duplicate;
    protected $omit = false;


    public function behaviors(){
        return array(
            'bFilterString' =>
                array('class'=>'application.models.bFilterString'
                ),
        );
    }

    public function rules() {
        return array(

            array('status_text, status_id, postal_local_id, status_date_text, status_date', 'filter', 'filter' => array( $this, 'filterStripTags')),

            array('status_text, status_id, postal_local_id, status_date_text, status_date', 'safe'),
        );

    }

    public function work()
    {
        $postal = Postal::model()->findByAttributes(array('local_id' => $this->postal_local_id));

        $this->postalModel = $postal;
        $this->postal_id = $postal->id;

        if($this->status_id == '')
            $this->status_id = PostalStat::findIdByText($this->status_text);

        if($this->status_date_text != '' && $this->status_date == '')
        {
            try
            {
                $date = new DateTime($this->status_date_text);
                $this->status_date = $date->format('Y-m-d H:i:s');
            }
            catch(Exception $ex)
            {
                $this->status_date = NULL;
            }
        }


        if($this->status_id == '')
        {
            $this->status_id = PostalStat::NEW_ORDER;
        }

        if($this->status_id && $this->postalModel != NULL)
        {

            // because Excel cuts seconds from date

            $status_date = substr($this->status_date,0,strlen($this->status_date)-2);

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id');
            $cmd->from('postal_stat_history');
            $cmd->where('status_date LIKE :stat_date AND postal_id = :postal_id AND current_stat_id = :stat_id', array(':stat_date' => $status_date.'%', ':postal_id' => $this->postalModel->id, ':stat_id' => $this->status_id));
            $result = $cmd->queryScalar();

            if($result)
            {
                $this->omit = true;
                $this->duplicate = true;
            }


        }

    }

    public function Postal()
    {
        return $this->postalModel;
    }

    public function postalFound()
    {

        if($this->postalModel != NULL)
            return true;
        else
            return false;
    }

    public static function saveGroupOfPackages(array $models, $ownTransaction = false)
    {


        if(S_Useful::sizeof($models))
        {
            /* @var $model F_MassStatusChange */
            /* @var $postal Postal */

            if(!$ownTransaction)
                $transaction = Yii::app()->db->beginTransaction();

            $errors = false;
            $i = 0;
            foreach($models AS $model)
            {
                $model->work();

                if($model->postalFound() && !$model->omit)
                {
                    $postal = $model->postalModel;
                    $statIdBefore = $postal->postal_stat_id;
                    if(!$postal->changeStat($model->status_id, 0, $model->status_date, false, false, false, false, false, false, false, Yii::app()->user->getModel()->scanning_sp_point_id))
                    {
                        $errors = true;

                    } else {
                        $statIdAfter = $postal->postal_stat_id;
                        $postal->addToLog('Status changed form #'.$statIdBefore.' to #'.$statIdAfter.' by file', PostalLog::CAT_INFO, Yii::app()->user->name);
                        $i++;
                    }
                }
            }

            if(!$errors)
            {
                if(!$ownTransaction)
                    $transaction->commit();
                return $i;
            }
            else
            {
                if(!$ownTransaction)
                    $transaction->rollback();
                return $models;
            }
        }
        else
        {
            return $models;
        }
    }
}