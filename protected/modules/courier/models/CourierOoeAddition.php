<?php

Yii::import('application.modules.courier.models._base.BaseCourierOoeAddition');

class CourierOoeAddition extends BaseCourierOoeAddition
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


    public function getClientTitle()
    {
        return $this->name;
    }

    public function getClientDesc()
    {
        return $this->description;
    }
}