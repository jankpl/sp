<h2 id="list"><?= Yii::t('postal', 'Potwierdzenia nadania');?></h2>
<br/>
<br/>
<div class="text-center">
    <p><?= Yii::t('postal', 'Wygeneruj potwierdzenia nadania z dzisiaj');?></p>

    <?= CHtml::link(Yii::t('postal', 'Potwierdzenia z dzisiaj'), ['/postal/postal/ackCardToday'], ['class' => 'btn btn-primary']); ?>
</div>
