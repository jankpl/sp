<?php

/**
 * CourierCodSettledSettle class.
 * CourierCodSettledSettle is the data structure for CourierCodSettledSettle form.
 */
class CourierCodSettledSettleForm extends CFormModel
{
    public $package_id;
    public $type;
    public $settle_date;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            ['package_id, type', 'required'],
            [ 'settle_date', 'date', 'format' => 'yyyy-mm-dd'],
            [ 'type', 'in', 'range' => array_keys(CourierCodSettled::listType())],
        );
    }



}