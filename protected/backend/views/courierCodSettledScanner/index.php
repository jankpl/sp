<?php
/* @var $model CourierCodSettledScannerForm */
/* @var $form CActiveForm */
/* @var $automatic boolean */
/* @var $statusList CourierStat[] */
?>


    <h2>Set Cod Settled</h2>

    <div class="form" style="position: relative;">
        <div class="ajax-preloader"><div></div></div>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'=>'scanner-form',
            'enableClientValidation' => true,
        )); ?>


        <h3>Provide Package Id</h3>

        <?= $form->errorSummary($model); ?>

        <?php
        foreach(Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
        }
        ?>

        <table class="table table-striped" style="margin: 0 auto; width: 500px;">
            <tr>
                <td>Automatic on paste:</td>
                <td style="text-align: center;"><?= CHtml::checkBox('automatic', $automatic, ['id' => 'automatic-checkbox']);?></td>
            </tr>
        </table>
        <br/>
        <br/>
        <table class="table table-striped" style="margin: 0 auto; width: 500px;">
            <tr>
                <td>Settlement type:</td>
                <td style="text-align: center;"><?= $form->dropDownList($model, 'type', CourierCodSettled::listType(), ['prompt' => '-']);?></td>
            </tr>
            <tr>
                <td>Settlement date:</td>
                <td style="text-align: center;"><?= $form->textField($model, 'settle_date', ['placeholder' => 'yyyy-mm-dd']);?></td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="2"><?= $form->textArea($model, 'package_ids', ['id' => 'package-id', 'style' => 'padding: 15px 5px; text-align: center; width: 400px; font-size: 1.1em; font-weight: bold; height: 300px;', 'placeholder' => 'Package Id(s) (local or remote). Every ID must be in new line.']);?></td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="2"><?= CHtml::submitButton('Set status', ['class' => 'btn btn-large btn-primary']);?></td>
            </tr>
        </table>
        <?php $this->endWidget(); ?>
    </div>
    <div class="alert alert-warning" id="automatic-warning" style="display: none;">Status will be automatically assigned after pasting data (ctrl + v) into field below!</div>


<br/><br/>
<div style="text-align: center;">
    <a href="<?= Yii::app()->createUrl('/courierCodSettledScanner/index');?>" class="btn btn-mini">Restart</a>
</div>
<?php
Yii::app()->clientScript->registerCoreScript('jquery');
?>

<script>
    var $automaticWarning = $('#automatic-warning');
    var $automaticCheckbox = $('#automatic-checkbox');
    var $packageId = $("#package-id");
    var $ajaxPreloader = $(".ajax-preloader");
    var $form = $("#scanner-form");

    function warningNotificator()
    {
        if($automaticCheckbox.is(':checked'))
            $automaticWarning.show();
        else
            $automaticWarning.hide();
    }

    function automaticSubmit()
    {

        $packageId.on("input paste", function (e) {

            $(this).val(e.originalEvent.clipboardData.getData("text/plain"));
            e.preventDefault();

            if($automaticCheckbox.is(':checked'))
                $(this).closest('form').submit();

        });
    }

    $(document).ready(function() {

        $form.on('submit', function(){
            $ajaxPreloader.show();
        });

        if($packageId.length)
        {
            $("#package-id").focus();
            automaticSubmit();
        }

        warningNotificator();

        $automaticCheckbox.on('change', warningNotificator);
    });
</script>