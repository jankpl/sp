<?php

Yii::import('application.models._base.BaseCountryGroupHasCountry');

class CountryGroupHasCountry extends BaseCountryGroupHasCountry
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}