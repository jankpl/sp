<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {

        /* @var $record User */
        // check if login details exists in database
        $record=User::model()->findByAttributes(array('login'=>$this->username));
        if($record===null)
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        else if($record->pass!== User::generatePassword($this->password))
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else if($record->activated == S_Status::NOT_ACTIVE)
        {
            $this->errorCode = 55;
            $this->errorMessage = Yii::t('user','To konto nie zostało jeszcze aktywowane.');
        }
        else if((!Yii::app()->PV->isLoginAllowed($record->source_domain)
                AND !YII_LOCAL
            )
            AND !($record->id == 2295 && Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL) // @ 31.10.2018 - special rule for customer Anvanda (#2295)
        )
        {
            $this->errorCode = 60;
            $this->errorMessage = Yii::t('user','To konto nie pozwala na zalogowanie do tej wersji serwisu.');
        }
        else
        {
            $this->errorCode=self::ERROR_NONE;

            $this->_id = $record->id;
            $this->setState('name', $record->login);
            $record->updateLoginTime();

            $record->setPrefLang(Yii::app()->language);

        }
        return !$this->errorCode;

    }


    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticateAdminMode()
    {

        /* @var $record User */
        // check if login details exists in database
        $record = User::model()->findByAttributes(array('login'=>$this->username));
        if($record===null)
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        else if(User::preparePasswordHashForAdminModeAuthentication($record->pass) !== $this->password)
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else if($record->activated == S_Status::NOT_ACTIVE)
        {
            $this->errorCode = 55;
            $this->errorMessage = Yii::t('user','To konto nie zostało jeszcze aktywowane.');
        }
        else
        {
            $this->errorCode=self::ERROR_NONE;

            $this->_id = $record->id;
            $this->setState('name', $record->login);
//            $record->updateLoginTime();

//            $record->setPrefLang(Yii::app()->language);

        }
        return !$this->errorCode;
    }

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticateFromYii2(\api\common\models\User $userModel)
    {

        $this->errorCode=self::ERROR_NONE;
        $this->_id = $userModel->id;
        $this->setState('name', $userModel->login);

        return !$this->errorCode;
    }

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticateFromSystem($user_id, $user_login)
    {

        $this->errorCode=self::ERROR_NONE;
        $this->_id = $user_id;
        $this->setState('name',$user_login);

        return !$this->errorCode;
    }



    public function getId()
    {
        return $this->_id;
    }

}