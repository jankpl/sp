<?php
$i = 0; // counter for link anchors #
?>
    <h2 style="background: gray; color: white;"><?= $country->name;?></h2>
<?php
/* @var $operators2CountryGroup CourierUOperator2CountryGroup[] */
$operators2CountryGroup = CourierUOperator2CountryGroup::model()->findAllByAttributes(['courier_country_group_id' => $country->id]);
foreach($operators2CountryGroup AS $operator2CountryGroup):
    ?>
    <div style="overflow: auto; vertical-align: top;">
        <div style="width: 49%; float: left;">
            <h3 style="margin: 0;" id="gid<?= ++$i;?>"><?= $operator2CountryGroup->courierUOperator->name; ?></h3>
        </div>
        <div style="width: 49%; float: right; text-align: right;">
            <strong>Add prices for group: </strong><?php
            echo CHtml::dropDownList('user_group','', CHtml::listData($operator2CountryGroup->getUnunsedUserGroups(),'id','nameWithAdmin'), array('group-edit-id' => 'true'));
            ?>
            <?php
            echo CHtml::button('Add',['edit-group-price' => 'true', 'data-url' => Yii::app()->createUrl('/courier/courierUPrices/editPricesGroup', ['courier_u_2_operator_country_group_id' => $operator2CountryGroup->id, 'user_group_id' => 'GID', ]), 'class' => 'btn btn-primary']);
            ?>
        </div>
    </div>

    <div style="width: 100%; overflow-x: scroll; position: relative;">
        <div style="white-space: nowrap; margin-bottom: 10px;">
            <div style="border: 1px solid gray; margin-right: 5px; display: inline-block;">
                <?php
                /* @var $prices CourierUPriceGroup[] */
                $priceGroup = CourierUPriceGroup::model()->findByAttributes(['courier_u_2_operator_country_group_id' => $operator2CountryGroup->id, 'user_group_id' => null]);
                ?>
                <h4>Default</h4>
                <div class="text-center">
                    <a href="<?= Yii::app()->createUrl('/courier/courierUPrices/editPricesGroup', ['courier_u_2_operator_country_group_id' => $operator2CountryGroup->id]);?>" target="_blank" class="btn btn-primary">edit</a><br/><br/>
                </div>
                <table class="table table-striped table-condensed" style="margin-bottom: 0; min-width: 200px;">
                    <td colspan="2" style="text-align: center;">[<a href="#gid<?= $i;?>" data-toggle="true">toggle</a>]</td>
                    <tbody data-toggle-target="true" style="display: none;">
                    <tr>
                        <th>Weight [kg]</th>
                        <th>Price</th>
                    </tr>
                    <?php
                    if($priceGroup)
                        foreach($priceGroup->getPriceItemsCurrent() AS $priceItem):
                            ?>
                            <tr>
                                <td><?= $priceItem->weight_top_limit;?></td>
                                <td >
                                    <table>
                                        <tr>
                                            <th>PLN</th>
                                            <th>EUR</th>
                                            <th>GBP</th>
                                        </tr>
                                        <tr>
                                            <td><?= $priceItem->pricePerItem->getValue(Price::PLN)->value;?></td>
                                            <td><?= $priceItem->pricePerItem->getValue(Price::EUR)->value;?></td>
                                            <td><?= $priceItem->pricePerItem->getValue(Price::GBP)->value;?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
            <?php
            /* @var $userGroups UserGroup[] */
            $userGroups = UserGroup::model()->forThisAdmin()->findAll(['condition' => 't.id IN (SELECT DISTINCT user_group_id FROM courier_u_price_group WHERE courier_u_2_operator_country_group_id = :courier_u_2_operator_country_group_id)', 'params' => ['courier_u_2_operator_country_group_id' => $operator2CountryGroup->id]]);

            foreach($userGroups AS $userGroup):
                ?>
                <div style="border: 1px solid gray; margin-right: 5px; display: inline-block;">
                    <?php
                    /* @var $prices CourierUPriceGroup[] */
                    $priceGroup = CourierUPriceGroup::model()->findByAttributes(['courier_u_2_operator_country_group_id' => $operator2CountryGroup->id, 'user_group_id' => $userGroup->id]);
                    ?>
                    <h4 id="gid<?= ++$i;?>">G: <?= $userGroup->name;?> (#<?= $userGroup->id;?>)</h4>
                    <?php
                    if($priceGroup):
                        ?>
                        <div class="text-center">
                            <a href="<?= Yii::app()->createUrl('/courier/courierUPrices/editPricesGroup', ['courier_u_2_operator_country_group_id' => $priceGroup->courier_u_2_operator_country_group_id, 'user_group_id' => $userGroup->id]);?>" target="_blank" class="btn btn-primary">edit</a><br/><br/>
                        </div>
                        <?php
                    endif;
                    ?>
                    <table class="table table-striped table-condensed" style="margin-bottom: 0; min-width: 200px;">
                        <tr>
                            <td colspan="2" style="text-align: center;">[<a href="#gid<?= $i;?>" data-toggle="true">toggle</a>]</td>
                        </tr>
                        <tbody data-toggle-target="true" style="display: none;">
                        <tr>
                            <th>Weight [kg]</th>
                            <th>Price</th>
                        </tr>
                        <?php
                        foreach($priceGroup->getPriceItemsCurrent() AS $priceItem):
                            ?>
                            <tr>
                                <td><?= $priceItem->weight_top_limit;?></td>
                                <td>
                                    <table>
                                        <tr>
                                            <th>PLN</th>
                                            <th>EUR</th>
                                            <th>GBP</th>
                                        </tr>
                                        <tr>
                                            <td><?= $priceItem->pricePerItem->getValue(Price::PLN)->value;?></td>
                                            <td><?= $priceItem->pricePerItem->getValue(Price::EUR)->value;?></td>
                                            <td><?= $priceItem->pricePerItem->getValue(Price::GBP)->value;?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
            endforeach;
            ?>
        </div>
    </div>
    <?php
endforeach;
?>