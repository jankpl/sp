<?php

Yii::import('application.modules.courier.models._base.BaseCourierCustomPickupPrice');

class CourierCustomPickupPrice extends BaseCourierCustomPickupPrice
{
    const TYPE_RUCH = 1;
    const TYPE_RM = 2;
    const TYPE_LP_EXPRESS = 3;
    const TYPE_UPS = 4;
    const TYPE_INPOST = 5;
    const TYPE_DHLDE = 6;

    const SIZE_CLASS_A = 1;
    const SIZE_CLASS_B = 2;
    const SIZE_CLASS_C = 3;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(

            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),

            array('price_id', 'numerical', 'integerOnly'=>true),

            array('pickup_type', 'required'),

            array('country_list_id', 'numerical', 'integerOnly'=>true),

            array('size_class, pickup_type, user_group_id', 'numerical', 'integerOnly'=>true),
            array('date_updated', 'safe'),
            array('date_updated, user_group_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, size_class, pickup_type, user_group_id, price_id', 'safe', 'on'=>'search'),
        );
    }

    public static function weightToSizeClass($package_weight)
    {
        if($package_weight === NULL)
            return NULL;

        if(
            $package_weight > 1
        )
            return self::SIZE_CLASS_B;
        else
            return self::SIZE_CLASS_A;
    }

    public  static function dimensionsToSizeClassInpost($l, $w, $d)
    {
        $dim = [$l, $w, $d];
        rsort($dim);

        $smallest = floatval($dim[2]);

        if($smallest <= 8)
            return self::SIZE_CLASS_A;
        else if($smallest <= 19)
            return self::SIZE_CLASS_B;
        else
            return self::SIZE_CLASS_C;
    }

    protected static function _findPriceModel($type, $sender_country_id, $user_group, $size_class)
    {
        if($type == self::TYPE_DHLDE)
            $model = self::model()->findByAttributes(['pickup_type' => $type, 'country_list_id' => $sender_country_id, 'user_group_id' => $user_group]);
        else if(in_array($type, [self::TYPE_RUCH, self::TYPE_INPOST]))
            $model = self::model()->findByAttributes(['pickup_type' => $type, 'size_class' => $size_class, 'user_group_id' => $user_group]);
        else
            $model = self::model()->findByAttributes(['pickup_type' => $type, 'user_group_id' => $user_group]);

        return $model;
    }

    public static function getPriceByWeight($package_weight, $type, $user_group, $sender_country_id, $currency = false, Courier $courier = NULL)
    {
        if(!$currency)
            $currency = Yii::app()->PriceManager->getCurrency();

        if($type == self::TYPE_INPOST)
            $size_class = self::dimensionsToSizeClassInpost($courier->package_size_l, $courier->package_size_w, $courier->package_size_d);
        else
            $size_class = self::weightToSizeClass($package_weight);

        //		$CACHE_NAME = 'COURIER_CUSTOM_PICKUP_PRICE_'.$currency.'_'.$this->id;

//		$price = Yii::app()->cache->get($CACHE_NAME);
//        $price = false;
//        if($price === false) {



            $model = NULL;
            if($user_group)
                $model = self::_findPriceModel($type, $sender_country_id, $user_group, $size_class);

            if($model === NULL)
                $model = self::_findPriceModel($type, $sender_country_id, NULL, $size_class);

            if($model === NULL)
                return false;

            if($model->price === NULL)
                return false;

            $price = $model->price->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => $currency)))[0]->value;

            if($type == self::TYPE_DHLDE)
                $price = $price * $package_weight;
//			Yii::app()->cache->set($CACHE_NAME, $price, 60);
//        }

        return $price;
    }



    public static function getPrice(Courier $courier, $type, $user_group = false)
    {
        return self::getPriceByWeight($courier->package_weight, $type, $user_group, $courier->senderAddressData->country_id, false, $courier);
    }


    public static function getPriceByAttributes()
    {

    }

    public static function getTypeList()
    {
        return [
            self::TYPE_RUCH => 'Ruch',
            self::TYPE_RM => 'RoyalMail',
            self::TYPE_LP_EXPRESS => 'LP Express',
            self::TYPE_INPOST => 'Inpost - Paczkomat',
            self::TYPE_DHLDE => 'DHL DE - Punkt',
        ];
    }

    public function getTypeName()
    {
        return isset(self::getTypeList()[$this->pickup_type]) ? self::getTypeList()[$this->pickup_type] : '-';
    }

    public static function getSizeList()
    {
        return [
            self::SIZE_CLASS_A => 'Gabaryt A',
            self::SIZE_CLASS_B => 'Gabaryt B',
            self::SIZE_CLASS_C => 'Gabaryt C',
        ];
    }

    public function getSizeName()
    {
        return isset(self::getSizeList()[$this->size_class]) ? self::getSizeList()[$this->size_class] : '-';
    }

    public function saveWithPrice($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $price_id = $this->price->saveWithPrices();

        if(!$price_id)
            $errors = true;

        $this->price_id = $price_id;

        if(!$this->save())
            $errors = true;


        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }
    }

}