<?php

class CourierUPricesController extends Controller {


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=> 'Yii::app()->user->authority = 62',
//                'actions' => ['exportToFileAll', 'searchByRemote','generateLabelsByGridView','generateReturnLabel','generateAckCardsByGridView','carriageTicket', 'view', 'index', 'admin', 'labelPrinter'],
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }


    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionEditPricesGroup($courier_u_2_operator_country_group_id, $user_group_id = NULL)
    {

        $errors = false;
        $errorsDesc = [];

        $model = CourierUPriceGroup::findOrCreate($courier_u_2_operator_country_group_id, $user_group_id);

        $priceItems = [];
//        $priceItemsForm = [];

        /* @var $priceItems CourierUPriceItem[] */

        $priceItemsDb = $model->getPriceItems(true);
        foreach($priceItemsDb AS $key => $item)
            $priceItems[$item->weight_top_limit > 0 ? $item->weight_top_limit : 0] = $item;


        if(isset($_POST) && S_Useful::sizeof($_POST))
        {
            $priceItems = [];
            if(isset($_POST['CourierUPriceItem']))
                foreach($_POST['CourierUPriceItem'] AS $key => $item)
                {
                    $temp = new CourierUPriceItem();
                    $temp->courier_u_price_group_id = $model->id;
                    $temp->weight_top_limit = $item['weight_top_limit'];
                    $temp->pricePerItem = Price::generateByPost($_POST['PriceValue'][$key]['pricePerItem']);

                    $priceItems[$temp->weight_top_limit > 0 ? $temp->weight_top_limit : 0] = $temp;
                }

            $model->_openedRevision = $_POST['CourierUPriceGroup']['_openedRevision'];
        }

        if(isset($priceItems[0]))
        {
            $errors = true;
            $errorsDesc[] = 'Provide all weight values!';
        }

        if(!S_Useful::sizeof($priceItems))
        {
            $priceItems[] = new CourierUPriceItem();;
        }

        if(isset($_POST['add_more']) && !$errors)
        {
            $priceItems[] = new CourierUPriceItem();;
        }

        // default top limit
        if(!isset($priceItems[9999]))
        {
            $temp = new CourierUPriceItem();
            $temp->weight_top_limit = 9999;
            $priceItems[9999] = $temp;
        }

        if(isset($_POST['sort']) && !$errors)
            ksort($priceItems);

        if(isset($_POST['save']) && !$errors) {
            ksort($priceItems);

            foreach ($priceItems AS $item) {

                $item->courier_u_price_group_id = $model->id;

                $item->setScenario(CourierUPriceItem::SCENARIO_VALIDATE_PRICE);
                if (!$item->validate()) {
                    $errors = true;
                    $errorsDesc[] = $item->getErrors();
                } else {
                    if (!$item->pricePerItem->validateWithChildren()) {
                        $errors = true;
                        $errorsDesc[] = $item->pricePerItem->getErrorsWithChildren();
                    }

                }

            }

            if(!$model->validate())
                $errors = true;

            if(!$errors)
            {
                $model->courierUPriceItems = $priceItems;
                if($model->saveWithChildren(true))
                {
                    Yii::app()->user->setFlash('success', 'Changes has been saved!');
                }
                else
                    Yii::app()->user->setFlash('error', 'Ups, something went wrong. Changes has NOT been saved!');

                $this->refresh();
                Yii::app()->end();
            }
        }
        $this->render('editPricesGroup',
            [
                'model' => $model,
                'priceItems' => $priceItems,
                'errors' => $errors,
                'errorsDesc' => $errorsDesc
            ]);
    }

    public function actionAjaxGetTab()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $id = $_POST['id'];
            $id = intval($id);

            $country = CourierCountryGroup::model()->findByPk($id);
            $html = $this->renderPartial('_index_tab', array('country' => $country), true);

            echo CJSON::encode($html);
        }
    }

}