<?php
/* @var $model Courier */
?>
<div class="form">

    <h2><?= $domestic ? Yii::t('courier', 'Nowa paczka U Krajowa') : Yii::t('courier', 'Nowa paczka U'); ?></h2>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'stateful'=>true,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div style="overflow: auto;">
        <div style="width: 48%; float: left;">
            <h3><?php echo Yii::t('courier','Parametry przesyłki');?> </h3>
            <table class="detail-view" style="width: 100%;">
                <tr class="odd">
                    <td><?php echo Yii::t('courier', 'Operator');?></td>
                    <td><?php echo $model->courierTypeU->courierUOperator->name_customer; ?></td>
                </tr>
                <tr class="even">
                    <td><?php echo $model->getAttributeLabel('package_number');?></td>
                    <td><?php echo $model->packages_number; ?></td>
                </tr>
                <tr class="odd">
                    <td><?php echo $model->getAttributeLabel('package_weight');?> (<?= Yii::t('courier', 'przyjęta');?>/<?= Yii::t('courier', 'zadeklarowana');?>) [<?php echo Courier::getWeightUnit();?>]</td>
                    <td><strong><?php echo $model->courierTypeU->getFinalWeight();?></strong>/<?= $model->package_weight_client;?></td>
                </tr>
                <tr class="even">
                    <td><?php echo $model->getAttributeLabel('package_dimensions');?> [<?php echo Courier::getDimensionUnit();?>]</td>
                    <td><?php echo $model->package_size_l.' x '.$model->package_size_d.' x '.$model->package_size_w; ?></td>
                </tr>
                <tr class="odd">
                    <td><?php echo $model->getAttributeLabel('package_value');?></td>
                    <td><?php echo $model->package_value; ?> <?php echo $model->value_currency; ?></td>
                </tr>
                <tr class="odd">
                    <td><?php echo $model->getAttributeLabel('package_content');?></td>
                    <td><?php echo $model->package_content; ?></td>
                </tr>
                <tr class="even">
                    <td><?php echo $model->getAttributeLabel('cod_value');?></td>
                    <td><?php echo $model->cod_value;?> <?= $model->getCodCurrency();?></td>
                </tr>
                <tr class="odd">
                    <td><?php echo $model->courierTypeU->getAttributeLabel('_package_wrapping');?></td>
                    <td><?php echo $model->courierTypeU->getPackageWrappingName();?></td>
                </tr>
                <?php
                $odd = false;
                if(!Yii::app()->user->isGuest && Yii::app()->user->getModel()->getCourierRefPrefix()):
                    ?>
                    <tr class="<?= $odd ? 'odd' : 'even';?>">
                        <td><?php echo $model->getAttributeLabel('ref');?></td>
                        <td><?php echo $model->ref; ?></td>
                    </tr>
                    <?php
                    $odd = !$odd;
                endif;
                ?>
                <?php
                if(!Yii::app()->user->isGuest && Yii::app()->user->getModel()->getCourierReturnAvailable()):
                    ?>
                    <tr class="<?= $odd ? 'odd' : 'even';?>">
                        <td><?php echo $model->getAttributeLabel('_generate_return');?></td>
                        <td><?php echo $model->_generate_return ? '&#10004;' : '&#10006'; ?></td>
                    </tr>
                    <?php
                    $odd = !$odd;
                endif;
                ?>
                <?php
                if($model->_inpost_locker_delivery_id):
                    ?>
                    <tr class="<?= $odd ? 'odd' : 'even';?>">
                        <th><?= Yii::t('courier', 'Paczkomat docelowy Inpost:');?></th>
                        <td><?php echo $model->_inpost_locker_delivery_id; ?></td>
                    </tr>
                    <?php
                    $odd = !$odd;
                endif;
                ?>
            </table>
        </div>
        <div style="width: 48%; float: right;">
            <h3><?php echo Yii::t('courier','Dodatki');?></h3>
            <table class="list hTop" style="width: 100%;">
                <tr>
                    <th><?php echo Yii::t('courier','Nazwa');?></th>
                </tr>
                <?php
                $noAdditions = true;
                if(S_Useful::sizeof($model->courierTypeU->listAdditions(true)))
                    $noAdditions = false;
                /* @var $item CourierUAdditionList */
                foreach($model->courierTypeU->listAdditions(true) AS $item): ?>
                    <tr>
                        <td><?php echo $item->getClientTitle(); ?></td>
                    </tr>
                <?php endforeach; ?>
                <?php
                if($model->cod_value):
                    $noAdditions = false;
                    ?>
                    <tr>
                        <td>COD</td>
                    </tr>
                <?php
                endif;
                ?>
                <?php
                if($model->courierTypeU->collection):
                    $noAdditions = false;
                    ?>
                    <tr>
                        <td><?= $model->getAttributeLabel('collection');?></td>
                    </tr>
                <?php
                endif;
                ?>
                <?php
                if($noAdditions)
                    echo '<tr><td>'.Yii::t('app','brak').'</td></tr>';
                ?>
            </table>
            <?php
            if(!Yii::app()->user->model->getHidePrices()):
                ?>
                <h3><?php echo Yii::t('courier','Koszt');?></h3>
                <table class="list hTop" style="width: 100%;">
                    <tr>
                        <th><?= Yii::t('courier', 'Nazwa');?></th>
                        <th><?= Yii::t('courier', 'Kwota netto');?></th>
                        <th><?= Yii::t('courier', 'Kwota brutto');?></th>
                    </tr>
                    <tr>
                        <td><?= Yii::t('courier', 'Przesyłka');?></td>
                        <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->pricePackage(), true));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></td>
                        <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->pricePackage()));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></td>
                    </tr>
                    <?php
                    if($model->courierTypeU->priceCod()):
                        ?>
                        <tr>
                            <td><?= Yii::t('courier', 'Dopłata za COD');?></td>
                            <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceCod(),true));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></td>
                            <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceCod()));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser()?></td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    <?php
                    if($model->courierTypeU->priceRemoteAreaSender()):
                        ?>
                        <tr>
                            <td><?= Yii::t('courier', 'Dopłata za strefę rozszerzoną - nadawca');?></td>
                            <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceRemoteAreaSender(),true));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></td>
                            <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceRemoteAreaSender()));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser()?></td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    <?php
                    if($model->courierTypeU->priceRemoteArea()):
                        ?>
                        <tr>
                            <td><?= Yii::t('courier', 'Dopłata za strefę rozszerzoną - odbiorca');?></td>
                            <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceRemoteArea(),true));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></td>
                            <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceRemoteArea()));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser()?></td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    <?php
                    if($model->courierTypeU->priceAdditions()):
                        ?>
                        <tr>
                            <td><?= Yii::t('courier', 'Dopłata za opcje dodatkowe');?></td>
                            <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceAdditions(),true));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></td>
                            <td><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceAdditions()));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    <tr>
                        <th><?= Yii::t('courier', 'Suma za 1 paczkę');?></th>
                        <th><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceEach(),true));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></th>
                        <th><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceEach()));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></th>
                    </tr>
                    <tr>
                        <th><?= Yii::t('courier', 'Łącznie');?></th>
                        <th><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceTotal(),true));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></th>
                        <th><?= S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($model->courierTypeU->priceTotal()));?> <?= CourierTypeU::getCurrencySymbolForCurrentUser();?></th>
                    </tr>
                </table>
            <?php
            endif;
            ?>
        </div>
    </div>


    <div style="overflow: auto;">
        <div style="width: 48%; float: left;">
            <h3><?php echo Yii::t('courier','Nadawca');?></h3>

            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));

            ?></div>
        <div style="width: 48%; float: right;">
            <h3><?php echo Yii::t('courier','Odbiorca');?></h3>
            <?php

            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));

            ?>
        </div>
    </div>

    <h3><?php echo Yii::t('courier','Regulamin');?></h3>
    <div class="row">
        <?php echo $form->labelEx($model->courierTypeU,'regulations'); ?>
        <?php echo $form->checkbox($model->courierTypeU, 'regulations'); ?>
        <?php echo $form->error($model->courierTypeU,'regulations'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model->courierTypeU,'regulations_rodo'); ?>
        <?php echo $form->checkbox($model->courierTypeU, 'regulations_rodo'); ?>
        <?php echo $form->error($model->courierTypeU,'regulations_rodo'); ?>
    </div><!-- row -->

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step3', 'class' => 'btn-small'));
        echo '<br/><br/>';
        echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę'),array('name'=>'finish', 'class' => 'btn-success'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Utwórz przesyłkę i wróć do formularza'),array('name'=>'finish_goback', 'class' => 'btn-primary'));
        $this->endWidget();
        ?>

    </div>
    <div class="info">
        <p><?php echo Yii::t('courier','Przechodząc do następnego kroku Twoje zlecenie zostanie zapisane w systemie');?>.</p>
    </div>
</div><!-- form -->