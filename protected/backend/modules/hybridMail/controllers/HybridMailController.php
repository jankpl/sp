<?php

class HybridMailController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'actions'=> array('index'),
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionAssingStatusByGridView()
    {
        /* @var $model HybridMail */
        $i = 0;
        if (isset($_POST['HybridMail'])) {

            $hybrid_mail_stat_id = (int) $_POST['hybrid_mail_stat_id'];
            $hybrid_mail_ids =  $_POST['HybridMail']['ids'];

            $courier_ids = explode(',', $hybrid_mail_ids);

            if(is_array($courier_ids) AND S_Useful::sizeof($courier_ids))
            {
                $transaction = Yii::app()->db->beginTransaction();
                $errors = false;


                foreach($courier_ids AS $id)
                {
                    $model = HybridMail::model()->findByPk($id);
                    if($model === null) continue;
                    if(!$model->changeStat($hybrid_mail_stat_id))
                        $errors = true;

                    $i++;
                }

                if(!$errors AND $i > 0)
                {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success','Udało się zmienić statusy. Liczba pozycji: '.$i);
                }
                else
                {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error','Nie udało się zmienić statusu');
                }
            }


        }

        if(!$i) Yii::app()->user->setFlash('error','Zaznacz listę listów!');



        $this->redirect(Yii::app()->createUrl('/hybridMail/hybridMail/Admin').'#mass-assign-stat');

    }

    public function actionShowText($id)
    {
        $model = HybridMail::model()->findByPk($id);

        if($model === null)
            throw new CHttpException(404);

        $text = $model->text;

        echo'<!DOCTYPE HTML>';
        echo'<html lang="pl">';
        echo'<head>';
        echo'<title>HybridMail #'.$model->id.'</title>';
        echo'<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
        echo'</head>';
        echo'<body>';
        echo $text;
        echo'</body>';
        echo'</html>';
    }

    public function actionGetFile($urlData)
    {
//        require_once Yii::getPathOfAlias('application.extensions.tcpdf.tcpdf.tcpdf') . '.php';
//        require_once Yii::getPathOfAlias('application.vendor.fpdi.fpdi') . '.php';
//
//        $fpdf = new FPDI();

        /* @var $model HybridMail */
        $model = HybridMail::model()->findByAttributes(array('hash' => $urlData));

        $file = $model->hybridMailAttachments[0];

        $path = $file->path;
        return Yii::app()->getRequest()->sendFile($file->name, @file_get_contents($path), $file->type);

//        $pdf = new FPDI('P', 'mm', 'A4'); //FPDI extends TCPDF
//        $pdf->setPrintHeader(false);
//        $pdf->setPrintFooter(false);
//        $pages = $pdf->setSourceFile($path);
//
//        for($i = 1; $i <= $pages; $i++)
//        {
//            $pdf->AddPage();
//
//            $page = $pdf->ImportPage($i);
//            $pdf->useTemplate($page);
//
//        }
//
//
//        $pdf->Output('newTest.pdf', 'D');
//
//        exit;
//
//
//        return Yii::app()->getRequest()->sendFile($file->name, @file_get_contents($path), $file->type);

    }

    public function actionGetAddressLabels($urlData)
    {
        /* @var $model HybridMail */
        $model = HybridMail::model()->findByAttributes(array('hash' => $urlData));
        $model->generateAddressPDF();
    }


    public function actionGetFile2($urlData)
    {

        /* @var $model HybridMail */
        $model = HybridMail::model()->findByAttributes(array('hash' => $urlData));
        $file = $model->hybridMailAttachments[0];

        $path = $file->path;

        return Yii::app()->getRequest()->sendFile($file->name, @file_get_contents($path), $file->type);

    }

    public function actionChangeStat()
    {
        /* @var $model HybridMail */
        if (isset($_POST['HybridMail'])) {

            $model = HybridMail::model()->findByPk($_POST['HybridMail']['id']);

            if($model === null)
                throw new Exception('Błąd!');

            if($model->changeStat($_POST['HybridMail']['stat']))
                $this->redirect(array('view', 'id' => $model->id));
            else
                throw new Exception("Błąd!");
        } else
            throw new CHttpException('404');

    }


    public function actionView($id) {

        $model = HybridMail::model()->findByPk($id);

        if($model === null)
            throw new CHttpException('404');

        $this->render('view', array(
            'model' => $model,
        ));
    }


	public function actionIndex() {

        $this->redirect(array('admin'));

        $this->render('index', array(

        ));

    }

	public function actionAdmin() {

		$model = new HybridMail('search');
		$model->unsetAttributes();

		if (isset($_GET['HybridMail']))
			$model->setAttributes($_GET['HybridMail']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}