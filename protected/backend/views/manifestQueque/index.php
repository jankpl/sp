<?php
?>

<h1>Manifest Queque</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'poczta-polska-waiting-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'date_entered',
        [
            'name' => '_is_sent',
            'value' => '$data->date_sent ? "Y" : "-"',
            'filter' => [ -1 => 'No', 1 => 'Yes'],
        ],
        [
            'name' => 'type',
            'value' => '$data->getTypeName()',
            'filter' => ManifestQueque::getTypeList(),
        ],
        'local_id',
        [
            'name' => 'broker',
            'value' => '$data->getBrokerName()',
            'filter' => ManifestQueque::getBrokerList(),
        ],
//        'local_id',
        'date_sent',

    ),
)); ?>
