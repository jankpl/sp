<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-menu" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand visible-xs" href="#"><?php echo Yii::t('site', 'Menu');?></a>
        </div>
        <div class="collapse navbar-collapse" id="top-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo Yii::t('site', 'Nasze usługi');?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/courier/courier/');?>"><?php echo Yii::t('site', 'Courier');?></a></li>
                        <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH):?><li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/hybridMail/hybridMail/');?>"><?php echo Yii::t('site', 'Hybrid Mail');?></a></li><?php endif;?>
                        <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH):?><li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/internationalMail/internationalMail/');?>"><?php echo Yii::t('site', 'International Mail');?></a></li><?php endif;?>
                        <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH):?><li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/specialTransport/specialTransport/');?>"><?php echo Yii::t('site', 'Special Transport');?></a></li><?php endif;?>
                        <?php if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH):?><li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/palette/palette/');?>"><?php echo Yii::t('site', 'Palette');?></a></li><?php endif;?>
                        <?php
                        Yii::app()->getModule('postal');
                        if(Postal::isAvailableForUser()):
                            ?>
                            <li><a tabindex="-1" href="<?php echo Yii::app()->createUrl('/postal/postal/');?>"><?php echo Yii::t('site', 'Postal');?></a></li>
                            <?php
                        endif;
                        ?>
                    </ul>
                </li>
                <li class="divider-vertical"></li>
                <li><a href="<?php echo Yii::app()->createUrl('page/view', array('id' => 1));?>"><?php echo Yii::t('site', 'O nas');?></a></li>
                <li class="divider-vertical"></li>
                <li><a href="<?php echo Yii::app()->createUrl('faq/');?>"><?php echo Yii::t('site', 'Faq');?></a></li>
                <li class="divider-vertical"></li>
                <li><a href="<?php echo Yii::app()->createUrl('site/contact');?>"><?php echo Yii::t('site', 'Kontakt');?></a></li>
                <li class="divider-vertical"></li>
            </ul>
            <?php if(Yii::app()->user->isGuest)
                $this->renderPartial('//layouts/_top_navbar_guest', ['model' => $model]);
            else
                $this->renderPartial('//layouts/_top_navbar_logged', ['model' => $model]);
            ?>
        </div>
    </div>
</nav>