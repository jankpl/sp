<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'hybrid-mail-grid',
    'dataProvider' => $model->searchForUser(),
    'filter' => $model,
    'columns' => array(
        array(
            'name'=>'local_id',
            'header'=>'#ID lokalny',
            'type'=>'raw',
            'value'=>'CHtml::link($data->local_id,array("/hybridMail/hybridMail/view/", "urlData" => $data->hash))',
        ),
        array(
            'name'=>'date_entered',
            'header'=>'Data utworzenia',
        ),
        array(
            'name'=>'__stat',
            'type'=>'raw',
            'filter'=>GxHtml::listDataEx(HybridMailStat::model()->findAllAttributes(null, true)),
            'header' => 'Status',
            'value'=>'$data->stat0->name',
        ),
        array(
            'name'=>'__sender_country_id',
            'header'=>'Kraj nadawcy',
            'value'=>'$data->senderAddressData->country',
            'filter'=>GxHtml::listDataEx(CountryList::model()->findAllAttributes(null, true)),
        ),
        array(
            'name' => '__receivers_number',
            'value' => 'S_Useful::sizeof($data->hybridMailReceivers)',
            'filter' => false,
        ),
        array(
            'name'=>'order_id',
            'type'=>'raw',
            'value'=>'$data->order_id !== null? CHtml::link($data->order->local_id,array("/order/view/", "id" => $data->order->hash)) : "-"',
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}',
            'buttons'=>array(
                'view' => array(
                    'url' => 'Yii::app()->createUrl("/hybridMail/hybridMail/view/", array("urlData" => $data->hash))',
                ),
            ),
        ),
    ),
));
?>