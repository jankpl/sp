<tbody class="_price_item" data-price-id="<?php echo $i; ?>">

<tr>
    <td>
        <?php echo $form->textField($model, '['.$i.']weight_top'); ?> <?php echo Courier::getWeightUnit();?>
    </td>
    <td>
        <?php $this->widget('PriceForm', array(
            'form' => $form,
            'model' => $model->price,
            'formFieldPrefix' => '['.$i.'][price]',
        ));?>
    </td>
    <td>
        <?php echo Chtml::button('Delete',array('data-delete-button' => $i));?>
    </td>
</tr>
<tr>
    <td>
        <?php echo $form->error($model,'['.$i.']weight_top'); ?>
    </td>
    <td>
        <?php echo $form->error($model,'['.$i.']price'); ?>
    </td>
    <td></td>
</tr>
</tbody>




