<?php


class UserSendSms extends CFormModel
{
    public $text;
    public $pv = false;


    public function rules()
    {
        return array(
            array('text', 'required'),
            array('text', 'length', 'max' => 512),
        );
    }


    public function attributeLabels()
    {

    }

    public function send($nr)
    {
        $smsApi = Yii::createComponent('application.components.SmsApi');
        $this->text = S_Useful::removeNationalCharacters($this->text);
        $smsApi->sendSms($nr, $this->text, $this->pv, true);

    }

}
