<?php
/* @var $models UserDocuments[] */
?>
<h2><?= Yii::t('userDocuments', 'Twoje dokumenty');?></h2>
<div class="gridview-container">
    <div class="gridview-overlay"></div>
    <?php
    $this->_getGridViewUserDocumentsgrid();

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('jquery.ui');
    Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css');
    ?>
</div>
