<?php

class internationalMailModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

        $this->defaultController = 'internationalMail';

		// import the module-level models and components
		$this->setImport(array(
			'internationalMail.models.*',
			'internationalMail.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
		    // module is disabled:
            $controller->redirect(['/']);
            exit;



            if(!Yii::app()->user->isGuest && Yii::app()->user->model->getDebtBlockActive())
            {
                $controller->redirect(['/userInvoice']);
                exit;
            }
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
