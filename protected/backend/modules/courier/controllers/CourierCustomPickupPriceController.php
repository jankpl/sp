<?php

class CourierCustomPickupPriceController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierCustomPickupPrice'),
        ));
    }

    public function actionUpdate($id, $user_group = NULL) {


        $model = CourierCustomPickupPrice::model()->findByPk($id);

        if($model == NULL)
            throw new CHttpException(404);
        else if($model->user_group_id != NULL && !isset($_POST))
        {
            // if editing group price - use parent ID & group ID, not child ID itself
            $temp = CourierCustomPickupPrice::model()->findByAttributes(['pickup_type' => $model->pickup_type, 'size_class' => $model->size_class, 'user_group_id' => NULL]);
            $this->redirect(['/courier/courierCustomPickupPrice/update', 'id' => $temp->id, 'user_group' => $model->user_group_id]);
            exit;
        }

        if($user_group != NULL)
        {
            $checkUserModel = UserGroup::model()->findByPk($user_group);
            if($checkUserModel == NULL)
                throw new CHttpException(404);

            $temp = CourierCustomPickupPrice::model()->findByAttributes(['pickup_type' => $model->pickup_type, 'size_class' => $model->size_class, 'user_group_id' => $user_group]);
            if($temp == NULL) {

                $temp = new CourierCustomPickupPrice();
                $temp->pickup_type = $model->pickup_type;
                $temp->size_class = $model->size_class;
                $temp->user_group_id = $user_group;
            }

            $model = $temp;



            // Clicked button for just sort
            if(isset($_POST['delete']))
            {

                $temp2 = CourierCustomPickupPrice::model()->findByAttributes(['pickup_type' => $model->pickup_type, 'size_class' => $model->size_class, 'user_group_id' => NULL]);

                if($model->delete())
                {
                    Yii::app()->user->setFlash('success','Pricing was deleted!');
                } else {
                    Yii::app()->user->setFlash('error','Pricing was NOT deleted!');
                }
                $this->redirect(['/courier/courierCustomPickupPrice/update', 'id' => $temp2->id]);
                return;
            }


        } else {
            $listUserGroups = [];
            $cmd = Yii::app()->db->createCommand();
            foreach(UserGroup::model()->findAll() AS $item)
            {
                $found = $cmd->select('COUNT(*)')->from((new CourierCustomPickupPrice())->tableName())->where('user_group_id = :group AND size_class = :size AND pickup_type = :pickup', [':group' => $item->id, ':size' => $model->size_class, ':pickup' => $model->pickup_type])->queryScalar();
                $listUserGroups[$item->id] = $item->name.($found ? ' (yes)' : '');
            }
        }



        if (isset($_POST['CourierCustomPickupPrice'])) {
//            $model->setAttributes($_POST['CourierCustomPickupPrice']);

            $model->price = Price::generateByPost($_POST['PriceValue']);

            if ($model->saveWithPrice()) {

                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('update', array(
            'model' => $model,
            'userGroup' => $user_group,
            'userGroupModel' => $checkUserModel,
            'listUserGroups' => $listUserGroups
        ));
    }


    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierCustomPickupPrice('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierCustomPickupPrice']))
            $model->setAttributes($_GET['CourierCustomPickupPrice']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}