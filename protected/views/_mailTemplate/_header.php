<?php
/* @var $name string */
/* @var $url string */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <style>
        a { color : #f29d01; text-decoration : underline; font-weight: normal; }
        a:hover { text-decoration: none; color: black; }
        p { margin: 0; padding: 0;}
        #footer a { color: white; font-size: 12px; }

        a.button { cursor: pointer; color: black; font-weight: bold; text-align: center; text-decoration: none; display: inline-block; line-height: 20px; padding: 8px 25px; background-color: #f29d01; border-radius: 5px; box-shadow: 0 2px 0 0 #4a3304; font-size: 14px; margin: -2px 2px 2px 2px; }
        a.button:hover { cursor: pointer; background: #9aacaf; color: white; }
    </style>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; background: white; margin: 0; padding: 0; color: black;">
<div style="width: 100%; height: 80px; background: #f29d01;">
    <a id="header" href="<?= $url;?>" style="display: block; position: relative; width: 900px; text-decoration: none;">
        <img alt="<?= $name; ?>" src="cid:logo" style="padding: 5px 0 0 25px; max-height: 70px;"/>
    </a>
</div>
<div style="padding: 25px 0 25px 25px;">