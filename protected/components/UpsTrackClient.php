<?php

class UpsTrackClient
{

//Configuration
    protected $access = GLOBAL_CONFIG::UPS_ACCESS;
    protected $userid = GLOBAL_CONFIG::UPS_USERID;
    protected $passwd = GLOBAL_CONFIG::UPS_PASS;
    protected $endpointurl = GLOBAL_CONFIG::UPS_ENDPOINT_TRACK;


    protected $outputFileName = "XOLTResult.xml";
    protected $wsdl = "";
    protected $operation = "ProcessShipment";

    protected $track_id;

    public function __construct()
    {
        $this->wsdl = YiiBase::getPathOfAlias('application.components.ups-track').'/Track.wsdl';
    }

    public static function track($id)
    {
        $model = new self;
        $model->track_id = $id;
        return $model->go();
    }

    protected function processTrack()
    {
        //create soap request
        $req['RequestOption'] = '15';
        $tref['CustomerContext'] = '';
        $req['TransactionReference'] = $tref;
        $request['Request'] = $req;
        $request['InquiryNumber'] = $this->track_id;
        $request['TrackingOption'] = '02';

        return $request;
    }

    protected function go()
    {

        try
        {

            $mode = array
            (
//                'soap_version' => SOAP_1_1,  // use soap 1.1 client
                'trace' => 1
            );

            // initialize soap client
            $client = new SoapClient($this->wsdl , $mode);

            //set endpoint url
            $client->__setLocation($this->endpointurl);

            //create soap header
            $usernameToken['Username'] = $this->userid;
            $usernameToken['Password'] = $this->passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $this->access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
            $client->__setSoapHeaders($header);


            //get response
            $resp = $client->__soapCall('ProcessTrack',array($this->processTrack()));

            $activity = $resp->Shipment->Package->Activity;

            $statuses = array();

            if(!is_array($activity))
                $activity = array($activity);

            $packageAddresses = $resp->Shipment->Package->PackageAddress;

            if(!is_array($packageAddresses))
                $packageAddresses = [ $packageAddresses ];

            $apAddress = false;
            foreach($packageAddresses AS $packageAddress)
            {
                if($packageAddress->Type->Code == 05)
                {
                    $apAddress = $packageAddress->Address;
                }
            }

            if($resp->Shipment->Package->AlternateTrackingNumber)
            {
                $couriers = CourierExternalManager::findCourierIdsByRemoteId($this->track_id);
                foreach($couriers AS $courier)
                    CourierAdditionalExtId::add($courier, $resp->Shipment->Package->AlternateTrackingNumber);
            }


            foreach($activity AS $item)
            {

                $name = $item->Status->Description;
                $date = $item->Date;
                $time = $item->Time;

                $where = $item->ActivityLocation->Address->City;

                // PACKAGE IS IN UPS ACCESS POINT
                if(in_array((string)$item->Status->Code, ['5G', '5R', '2Q', 'ZP', 'ZC']) && $resp->Shipment->Package->PackageAddress)
                {
//                    $addressNode = $resp->Shipment->Package->PackageAddress->Address;
//
//                    if($addressNode == NULL && is_array($resp->Shipment->Package->PackageAddress)) {
//                        $addressNode = array_shift($resp->Shipment->Package->PackageAddress);
//
//                    }


                    $loc = false;
                    if($apAddress)
                        $loc = UpsApLocation::locate($apAddress->AddressLine, $apAddress->City, $apAddress->PostalCode, $apAddress->CountryCode);

                    $where = $apAddress->AddressLine.', '.$apAddress->PostalCode.', '.$apAddress->City;

                    if(is_array($loc))
                    {
                        $where = '"'.$loc['name'].'" ## '.$where.' ## tel. '.$loc['tel'];
                        if($loc['desc'] != '')
                            $where .= '; '.$loc['desc'];
                    }
                }

//                if($date == '')
//                    $time = date('Ymd');

                $datetime = DateTime::createFromFormat('Ymd His', $date.' '.$time);

//                if(in_array((string)$item->Status->Code, ['5G', '5R', '2Q', 'ZP']) && $resp->Shipment->Package->PackageAddress)
//                {
//
//                    $addressNode = $resp->Shipment->Package->PackageAddress->Address;
//
//                    $apAddress = ((string) $addressNode->AddressLine).', '.((string) $addressNode->PostalCode).' '.((string) $addressNode->City).', '.((string) $addressNode->CountryCode);
//
//                    if($apAddress != ',  , ')
//                        $where .= ' (adres UPS AP: '.$apAddress.' //'.$this->track_id.'//)';
//                }

                if((string)$item->Status->Type == 'D')
                {
                    $receivedBy = (string) $item->ActivityLocation->SignedForByName;
                    if($receivedBy != '')
                        $where .= ' (signed for by: '.$receivedBy.')';
                }

                $where = trim($where);


                if($name == 'Order Processed: Ready for UPS')
                    continue;

                $temp = array(
                    'name' => $name,
                    'date' => $datetime->format('Y-m-d H:i:s'),
                    'location' => $where,
                );

                array_unshift($statuses, $temp);
            }


            return $statuses;

        }
        catch(Exception $ex)
        {
//            Yii::log(print_r($ex,1), CLogger::LEVEL_ERROR);
            return false;
        }

    }


    /**
     * @param string $number
     * @return bool|string Returns false or PDF string with ePod
     */
    public static function epod($number)
    {
        $CACHE_NAME = 'UPS_EPOD_' . $number;
        $result = Yii::app()->cacheUserData->get($CACHE_NAME);

        if ($result === false) {

            $shipper = mb_substr($number, 2, 6);
            $shipper = strtoupper($shipper);

            $model = new self;

            switch ($shipper) {
                case 'R2E500':
                    $model->access = '9D0634F2D2DF59E4';
                    $model->userid = 'Kaab1';
                    $model->passwd = 'Abc12345';
                    break;

                case '2F277X':
                    $model->access = '4D0F15465739E458';
                    $model->userid = 'FB2016';
                    $model->passwd = 'FireBusiness1';
                    break;

                case '375F5W':
                    $model->access = '7D1F0F2EAB96D7DE';
                    $model->userid = 'MCM2016_1';
                    $model->passwd = 'McmMcmMcm1';
                    break;

                case '5793A4':
                    $model->access = '5D180D0976F9E588';
                    $model->userid = 'Inxpressns';
                    $model->passwd = 'Dmsltd#2007';
                    break;

                case '2F2785':
                    $model->access = 'BD1CC8117D2D7B6E';
                    $model->userid = 'LPS2016_1';
                    $model->passwd = 'LupusLupus1';
                    break;

                case '2F2774':
                    $model->access = '7D134EE28479E728';
                    $model->userid = 'mr2016';
                    $model->passwd = 'MarketingRelacji1';
                    break;
                case 'Y7A709':
                    $model->access = '3D0B34B6A351D368';
                    $model->userid = 'sp@arrows.com.pl';
                    $model->passwd = 'Swiatprzesylek001@';
                    break;
                case '3743AX':
                    $model->access = '8D33385637D60888';
                    $model->userid = '4VALUESPL';
                    $model->passwd = '4Values2017@';
                    break;

                default:
                    return false;
                    break;
            }

            try {

                $mode = array
                (
//                'soap_version' => SOAP_1_1,  // use soap 1.1 client
                    'trace' => 1
                );

                $client = new SoapClient($model->wsdl, $mode);
                $client->__setLocation($model->endpointurl);

                //create soap header
                $usernameToken['Username'] = $model->userid;
                $usernameToken['Password'] = $model->passwd;
                $serviceAccessLicense['AccessLicenseNumber'] = $model->access;
                $upss['UsernameToken'] = $usernameToken;
                $upss['ServiceAccessToken'] = $serviceAccessLicense;

                $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', $upss);
                $client->__setSoapHeaders($header);


                $request = new stdClass();
                $request->InquiryNumber = $number;
                $request->Request = new stdClass();
                $request->Request->TransactionReference = new stdClass();
                $request->Request->TransactionReference->CustomerContext = '';
                $request->Request->RequestOption = '8';

                $resp = $client->__soapCall('ProcessTrack', array($request));

                if ($resp) {
                    $document = $resp->Shipment->Package->Activity->Document;
                    if (!is_array($document))
                        $document = [$document];


                    foreach ($document AS $item) {

                        if ($item->Type->Code != '01')
                            continue;

                        $img = $item->Content;

                        $img = @base64_decode($img);

                        if ($img) {

                            // GENERATE OWN:
                            $pdf = PDFGenerator::initialize();
                            $pdf->setFontSubsetting(false);

                            $pdf->addPage('H', 'A4');
                            $width = $pdf->getPageWidth() - 10;
                            $pdf->Image('@' . ($img), 5, 5, $width, '', '', 'N', false);

                            $result = $pdf->Output('epod.pdf', 'S');

                            Yii::app()->cacheUserData->set($CACHE_NAME, $result, (60*60*24));

                            return $result;
                        }
                    }
                }

                return false;

            } catch (Exception $ex) {
                return false;
            }

        }

        return $result;

    }
}

?>
