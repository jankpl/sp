<?php

class ebayClient extends CApplicationComponent
{

    const LIMIT_NUMBER_OF_FETCH = 5;
    const LIMIT_ITEMS_ON_PAGE = 100; // MAX FOR EBAY IS 100
    const VERSION = 835;
    const SITEID = 0;

    const EBAYLOGINURL = GLOBAL_CONFIG::EBAY_LOGIN_URL;
    const ENDPOINTURL = GLOBAL_CONFIG::EBAY_ENDPOINT;
    const APPID = GLOBAL_CONFIG::EBAY_APPID;
    const DEVID = GLOBAL_CONFIG::EBAY_DEVID;
    const AUTHCERT = GLOBAL_CONFIG::EBAY_AUTHCERT;
    const RUNAME = GLOBAL_CONFIG::EBAY_RUNAME;

    const RETURN_OOE = 1;
    const RETURN_INTERNAL = 2;
    const RETURN_U = 3;
    const RETURN_POSTAL = 10;


    const ORDERS_GOBACK_MONTHS = 3;


    protected $runame = self::RUNAME;

    protected $login;
    protected $eBayAuthToken;
    protected $isLoggedToEbay = false;
    protected $eBayLoggedExpiration;
    protected $sessionId;
    protected $readyToFetchToken = false;
    protected $returnToAfterLogin = false;
    protected $justLogged = false;

    private function __construct()
    {
    }

    public static function instance($hasOwnToken = false)
    {
        $model = new self;
        $model->init();

        if($hasOwnToken)
            $model->eBayAuthToken = $hasOwnToken;

        return $model;
    }

    public function isLogged()
    {
        return $this->login;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getReturnToLocation()
    {
        return $this->returnToAfterLogin;
    }

    public function getIsJustLogged()
    {
        return $this->justLogged;
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function getAuthToken()
    {
        return $this->eBayAuthToken;
    }

    public function init()
    {
        $this->login = Yii::app()->session->get('ebayClient_login', false);
        $this->eBayAuthToken = Yii::app()->session->get('ebayClient_eBayAuthToken', false);
        $this->isLoggedToEbay = Yii::app()->session->get('ebayClient_isLoggedToEbay', false);
        $this->eBayLoggedExpiration = Yii::app()->session->get('ebayClient_eBayLoggedExpiration', false);
        $this->sessionId = Yii::app()->session->get('ebayClient_sessionId', false);
        $this->readyToFetchToken = Yii::app()->session->get('readyToFetchToken', false);
        $this->returnToAfterLogin = Yii::app()->session->get('returnToAfterLogin', false);

        if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
            $this->runame = GLOBAL_CONFIG::EBAY_RUNAME_CQL;

        if($this->isLoggedToEbay && (strtotime($this->eBayLoggedExpiration) -  strtotime('-1 hour') <= 0))
        {
            $this->isLoggedToEbay = false;
            $this->eBayAuthToken = false;
            throw new Exception(Yii::t('courier_ebay', 'Błąd autoryzacji konta eBay.com! 1'));
        } else if($this->readyToFetchToken) {
            $this->fetchToken();
        }

        parent::init();
    }

    protected function saveParams()
    {
        Yii::app()->session->add('ebayClient_login', $this->login);
        Yii::app()->session->add('ebayClient_eBayAuthToken', $this->eBayAuthToken);
        Yii::app()->session->add('ebayClient_isLoggedToEbay', $this->isLoggedToEbay);
        Yii::app()->session->add('ebayClient_eBayLoggedExpiration', $this->eBayLoggedExpiration);
        Yii::app()->session->add('ebayClient_sessionId', $this->sessionId);
        Yii::app()->session->add('readyToFetchToken', $this->readyToFetchToken);
        Yii::app()->session->add('returnToAfterLogin', $this->returnToAfterLogin);
    }

    public function logout()
    {
        $this->login = false;
        $this->eBayLoggedExpiration = false;
        $this->eBayAuthToken = false;
        $this->sessionId = false;
        $this->isLoggedToEbay = false;
        $this->readyToFetchToken = false;
        $this->saveParams();
    }

    public function getOrders($date_from = false, $date_to = false, $stat  = false, $statEbay = false, $target = EbayImport::TARGET_COURIER)
    {
        $date_begining = date('Y-m-d', strtotime('-'.ebayClient::ORDERS_GOBACK_MONTHS.' months'));
        $date_end = date('Y-m-d');

        if (!in_array($stat, array_keys(EbayOrderItem::getStatShowProcessedList())))
            $stat = EbayOrderItem::STAT_SHOW_PROCESSED_ALL;

        if (strtotime($date_from) > strtotime($date_to))
            $date_from = $date_to;

        if (!strtotime($date_from) OR strtotime($date_from) < strtotime($date_begining))
            $date_from = $date_end;

        if (!strtotime($date_to) OR strtotime($date_to) > strtotime($date_end))
            $date_to = $date_end;

        if (!in_array($statEbay, array_keys(EbayOrderItem::getStatShowEbayList())))
            $statEbay = EbayOrderItem::STAT_SHOW_EBAY_READY;

        $data = [];

        $data['GetOrdersRequest'] = [];

        $data['GetOrdersRequest']['Version'] = self::VERSION;
        $data['GetOrdersRequest']['CreateTimeFrom'] = $date_from.' 00:00:00';
        $data['GetOrdersRequest']['CreateTimeTo'] = $date_to.' 23:59:59';
        $data['GetOrdersRequest']['IncludeFinalValueFee'] = false;
        $data['GetOrdersRequest']['OrderRole'] = 'Seller';


        $data['GetOrdersRequest']['Pagination'] = [];
        $data['GetOrdersRequest']['Pagination']['EntriesPerPage'] = self::LIMIT_ITEMS_ON_PAGE;
//        $data['GetOrdersRequest']['OrderStatus'] = 'Completed';

        try {
            $resp = $this->soapOperation('GetOrders', $data);

            $orders = [];
            if ($resp->Ack === 'Success' OR $resp->Ack === 'Warning') {

                $i = 1;
                do
                {
                    $more = $resp->HasMoreOrders;

                    $ordersList = $resp->OrderArray->Order;


                    if(!is_array($ordersList) && $ordersList !== NULL)
                        $ordersList = [$ordersList];


                    if(is_array($ordersList))
                        foreach ($ordersList AS $order) {
                            $temp = new EbayOrderItem($order, NULL, $target);
                            $omit = false;

                            if ($stat == EbayOrderItem::STAT_SHOW_NOT_PROCESSED) {
                                if ($temp->statProcessed != EbayOrderItem::STAT_NOT_PROCESSED)
                                    $omit = true;
                            } else if($stat == EbayOrderItem::STAT_SHOW_PROCESSED) {
                                if ($temp->statProcessed != EbayOrderItem::STAT_PROCESSED)
                                    $omit = true;
                            }

                            if ($statEbay == EbayOrderItem::STAT_SHOW_EBAY_READY) {
                                if ($temp->statPayment != EbayOrderItem::STAT_PAID OR $temp->statShipping == EbayOrderItem::STAT_SHIPPED)
                                    $omit = true;
                            }


                            // if filtering rules are not met, then order is ok
                            if(!$omit)
                                array_push($orders, $temp);
                        }

                    $i++;
                    // if there are more results (on next pages)
                    if($more)
                    {
                        $data['GetOrdersRequest']['Pagination']['PageNumber'] = $i;
                        $resp = $this->soapOperation('GetOrders', $data);
                    }
                }
                while($more && $i <= self::LIMIT_NUMBER_OF_FETCH && ($resp->Ack === 'Success' OR $resp->Ack === 'Warning'));

                return [$orders, $more];
            } else
                throw new CHttpException(500, Yii::t('courier_ebay', 'Operacja nie powiodła się. Spróbuj ponownie lub skontaktuj się z nami.'));
        }
        catch (HttpException $ex)
        {
            throw new CHttpException(500, Yii::t('courier_ebay', 'Operacja nie powiodła się. Spróbuj ponownie lub skontaktuj się z nami.'));
        }
    }

    public function updateTt($orderId, $ttNumber, $operator)
    {
//
//        file_put_contents('ebayTt.txt', print_r(['orderId' => $orderId, 'tt' => $ttNumber, 'operator' => $operator],1)."\r\n", FILE_APPEND);
//
//        return true;
//exit;

        $data = [];

        $operator = $string = preg_replace("/[^- \w]+/", "", $operator);

        $data['CompleteSaleRequest'] = [];

        $data['CompleteSaleRequest']['Version'] = self::VERSION;
        $data['CompleteSaleRequest']['OrderID'] = $orderId;
        $data['CompleteSaleRequest']['Shipped'] = 1;
        $data['CompleteSaleRequest']['Shipment'] = [];
        $data['CompleteSaleRequest']['Shipment']['ShipmentTrackingDetails'] = [];
        $data['CompleteSaleRequest']['Shipment']['ShipmentTrackingDetails']['ShipmentTrackingNumber'] = $ttNumber;
        $data['CompleteSaleRequest']['Shipment']['ShipmentTrackingDetails']['ShippingCarrierUsed'] = $operator;

        try {
            $resp = $this->soapOperation('CompleteSale', $data);

            if ($resp->Ack === 'Success' OR $resp->Ack === 'Warning')
                return true;
            else {
                Yii::log(print_r($resp,1), CLogger::LEVEL_ERROR);
                return false;
            }
        }
        catch (CHttpException $ex)
        {
            Yii::log(print_r($ex,1), CLogger::LEVEL_ERROR);
            return false;
        }
    }

    /**
     * Request login to eBay account and redirect to eBay login page
     */
    public function requestLogin($returnToLocation = false)
    {

        $data = [];

        $data['GetSessionIDRequest'] = [];

        $data['GetSessionIDRequest']['Version'] = self::VERSION;
        $data['GetSessionIDRequest']['RuName'] = $this->runame;

        try {
            $resp = $this->soapOperation('GetSessionID', $data, false);

            $this->returnToAfterLogin = $returnToLocation;

            if ($resp->Ack === 'Success' OR $resp->Ack === 'Warning') {

                $this->sessionId = $resp->SessionID;
                $this->readyToFetchToken = true;
                $this->saveParams();

                $sessionId = urlencode($this->sessionId);
                $url = self::EBAYLOGINURL . '&runame=' . $this->runame . '&SessID=' . $sessionId;

                // redirect to eBay login page
                header("Location: " . $url);
                exit;
            } else
                throw new CHttpException(ErrorCodes::EBAY_PROBLEM, Yii::t('courier_ebay', 'Wystąpił błąd przy komunikacji z eBay! Spróbuj ponownie lub skontaktuj się z nami.'));
        }
        catch(CHttpException $ex)
        {
            throw new CHttpException(ErrorCodes::EBAY_PROBLEM, Yii::t('courier_ebay', 'Wystąpił błąd przy komunikacji z eBay! Spróbuj ponownie lub skontaktuj się z nami.'));
        }
    }

    /**
     * Fetch token by sessionID after request login
     */
    public function fetchToken()
    {
        $data = [];
        $data['FetchTokenRequest'] = [];
        $data['FetchTokenRequest']['Version'] = self::VERSION;
        $data['FetchTokenRequest']['SessionID'] = $this->sessionId;

        try {
            $resp = $this->soapOperation('FetchToken', $data, false);

            if ($resp->Ack === 'Success' OR $resp->Ack === 'Warning') {
                $this->login = $_GET['username'];
                $this->eBayAuthToken = $resp->eBayAuthToken;
                $this->eBayLoggedExpiration = $resp->HardExpirationTime;
                $this->readyToFetchToken = false;
                $this->isLoggedToEbay = true;
                $this->justLogged = true;
                $this->saveParams();
            } else {
                Yii::log('SESSION ID:'.print_r($this->sessionId,1).';;;;'.print_r($this,1).';;;;'.print_r($resp,1), CLogger::LEVEL_ERROR);
                $this->logout();
                throw new CHttpException(500, Yii::t('courier_ebay', 'Błąd autoryzacji konta eBay.com! 2'));
            }

        }
        catch(HttpException $ex)
        {
            throw new CHttpException(500, Yii::t('courier_ebay', 'Wystąpił błąd przy komunikacji z eBay! Spróbuj ponownie lub skontaktuj się z nami.'));
        }

    }


    protected function soapOperation($functionName, array $data, $authenticateByToken = true)
    {
        $mode = array
        (
//            'soap_version' => SOAP_1_1,  // use soap 1.1 client
            'trace' => 1,
//            'exceptions' => true,
        );

        $client = new SoapClient(YiiBase::getPathOfAlias('application.components.ebay').'/ebay.wsdl', $mode);

        $credentials = [];
        if($authenticateByToken)
        {
            if($this->eBayAuthToken == '' OR !$this->eBayAuthToken)
            {
                throw new Exception(Yii::t('courier_ebay', 'Błąd autoryzacji konta eBay.com! 3'));
            }


            $url = self::ENDPOINTURL . '?callname='.$functionName.'&siteid='.self::SITEID.'&appid='.self::APPID.'&version='.self::VERSION.'&Routing=new';

            $credentials['eBayAuthToken'] = $this->eBayAuthToken;

        }
        else
        {
            $url = self::ENDPOINTURL . '?callname='.$functionName.'&siteid='.self::SITEID.'&appid='.self::APPID.'&version='.self::VERSION.'&Routing=new';

            $credentials['Credentials'] = [];
            $credentials['Credentials']['AppId'] = self::APPID;
            $credentials['Credentials']['DevId'] = self::DEVID;
            $credentials['Credentials']['AuthCert'] = self::AUTHCERT;
        }


        MyDump::dump('ebay.txt', print_r($functionName,1).' : '.print_r($data,1));

        $header = new SoapHeader('urn:ebay:apis:eBLBaseComponents', 'RequesterCredentials', $credentials);
        $client->__setSoapHeaders($header);

        $client->__setLocation($url);


        try {
            $resp = $client->__soapCall($functionName, $data);
        }
        catch(SoapFault $ex)
        {
            MyDump::dump('ebay.txt', print_r($ex,1));
            MyDump::dump('ebay.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('ebay.txt', print_r($client->__getLastResponse(),1));
            return false;
        }
        catch (Exception $ex)
        {
            MyDump::dump('ebay.txt', print_r($ex,1));
            MyDump::dump('ebay.txt', print_r($client->__getLastRequest(),1));
            MyDump::dump('ebay.txt', print_r($client->__getLastResponse(),1));
            return false;
        }

        MyDump::dump('ebay.txt', print_r($client->__getLastRequest(),1));
        MyDump::dump('ebay.txt', print_r($client->__getLastResponse(),1));

        return $resp;
    }

    public static function readableDate($date)
    {
        $date = str_replace('T', ' ', $date);
        $date = substr($date,0,-8);
        return $date;
    }
}