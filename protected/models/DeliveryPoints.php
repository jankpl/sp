<?php

class DeliveryPoints
{
    CONST TYPE_DHLDE = 1;
    CONST TYPE_COLISSIMO = 2;
    CONST TYPE_INPOST = 3;
    CONST TYPE_DIRECTLINK = 4;

    public $id;
    public $pid;
    public $name;
    public $lat;
    public $lng;
    public $street;
    public $zip_code;
    public $city;
    public $country;
    public $type;
    public $desc;


    public static function typeId2NameList()
    {
        return [
            self::TYPE_DHLDE => 'DHLDE',
            self::TYPE_COLISSIMO => 'COLISSIMO',
            self::TYPE_INPOST => 'INPOST',
            self::TYPE_DIRECTLINK => 'DIRECTLINK',
        ];
    }

    public static function typeName2IdList()    {

        return array_flip(self::typeId2NameList());
    }

    public static function typeId2Name($id)
    {
        return isset(self::typeId2NameList()[$id]) ? self::typeId2NameList()[$id] : NULL;
    }

    public static function typeName2Id($name)
    {
        return isset(self::typeName2IdList()[$name]) ? self::typeName2IdList()[$name] : NULL;
    }

    /**
     * @param $type
     * @param $type_id
     * @param $address
     * @param $zip_code
     * @param $city
     * @param $country
     * @param $name
     * @param $lat
     * @param $lng
     * @param array $other
     * @return DeliveryPoints
     * @throws CDbException
     * @throws CException
     */
    public static function getUpdateDeliveryPoints($provider, $type_id, $address, $zip_code, $city, $country, $name, $lat, $lng, $type, $other = [])
    {
        $db = Yii::app()->db;
        $cmd = $db->createCommand();
        $cmd->select('id')->from('delivery_points')->where('provider = :provider AND type_id = :id', [':provider' => $provider, ':id' => $type_id]);
        $id = $cmd->queryScalar();

        if(!$id)
        {
            try {
                $cmd->insert('delivery_points', [
                    'provider' => $provider,
                    'type_id' => $type_id,
                    'address' => $address,
                    'zip_code' => $zip_code,
                    'city' => $city,
                    'country' => $country,
                    'name' => $name,
                    'lat' => $lat,
                    'lng' => $lng,
                    'type' => $type,
                    'other' => S_Useful::sizeof($other) ? json_encode($other) : NULL,
                ]);
                $id = $db->getLastInsertID();
            }
            catch(CDbException $e){

                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                throw new CDbException($e);
            }
        } else {
            if($provider == self::TYPE_DHLDE)
            {
                $cmd->update('delivery_points', ['other' => S_Useful::sizeof($other) ? json_encode($other) : NULL], 'id = :id', [':id' => $id]);
            }
        }

        $model = new self;
        $model->id = $id;
        $model->pid = $type_id;
        $model->name = $name;
        $model->lat = $lat;
        $model->lng = $lng;
        $model->street = $address;
        $model->zip_code = $zip_code;
        $model->city = $city;
        $model->country = $country;
        $model->type = $type;
        $model->other = json_encode($other);

        return $model;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        return json_decode(json_encode($this), true);
    }

    public static function getPointById($id, $onlyRequiredForProvider = false, $onlyCheck = false, $forceProvider = false)
    {
        $cmd = Yii::app()->db->createCommand();

        if($onlyCheck)
            $cmd->select('COUNT(*)');
        else
            $cmd->select('*');

        $cmd->from('delivery_points')->where('id = :id', [':id' => $id]);

        if($forceProvider)
            $cmd->andWhere('provider = :provider', [':provider' => $forceProvider]);

        if($onlyCheck)
            return $cmd->queryScalar();
        else
            $response = $cmd->queryRow();

        if($response && $onlyRequiredForProvider)
        {

            if($response['provider'] == self::TYPE_DHLDE)
            {
                $other = @json_decode($response['other'], true);

                return [
                    0 => $other ? $other['primaryKeyZipRegion'] : NULL,
                    1 => $response['type'],
                    2 => $response['zip_code'],
                    3 => $other ? $other->street : NULL,
                    4 => $other ? $other->house_no : NULL,
                ];
            }
            else if($response['provider'] == self::TYPE_COLISSIMO)
            {
                return [
                    0 => $response['type_id'],
                    1 => $response['type'],
                ];
            }
            else if($response['provider'] == self::TYPE_INPOST)
            {
                return [
                    0 => $response['type_id'],
                ];
            }
            else if($response['provider'] == self::TYPE_DIRECTLINK)
            {
                return [
                    0 => $response['type_id'],
                    1 => $response['name'],
                    2 => $response['address'],
                    3 => $response['zip_code'],
                    4 => $response['city'],
                    5 => $response['country'],
                ];
            }
            else
                throw new Exception('Unknown DELIVERY POINT provider!');

        } else
            return $response;
    }

    public static function getIdByProviderAndId($provider, $id)
    {
        $provider = self::typeName2Id($provider);

        if($provider == self::TYPE_DHLDE)
        {
           $parm1 = $id[0];
           $parm2 = $id[1];

           if($parm1 == '' OR $parm2 == '')
               throw new Exception('This provider requires 2 parameters!');


          $cmd = Yii::app()->db->createCommand();
          $cmd->select('id')->from('delivery_points')->where('JSON_CONTAINS(other, :provider_id, \'$.primaryKeyZipRegion\') AND zip_code = :provider_zip_code AND provider = :provider', [
              ':provider_id' => '"'.$id[0].'"',
              ':provider_zip_code' => $id[1],
              ':provider' => self::TYPE_DHLDE,
          ]);
          $id = $cmd->queryScalar();

          return $id;
        }
        else if($provider == self::TYPE_COLISSIMO)
        {
            return NULL;
        }
        else if($provider == self::TYPE_INPOST)
        {
            return NULL;
        }
        else
            throw new Exception('Unknown DELIVERY POINT provider!');

    }

}