<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

    <p>
        You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
    </p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'postal-carrier-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        array(
            'name' => '__postal_id',
            'value' => 'CHtml::link("#".Postal::model()->findByPk($data->postal->id)->local_id,array("/postal/postal/view","id" => $data->postal->id),array("target" => "_blank"))',
            'type' => 'raw',
        ),
        array(
            'name' => 'stat',
            'value' => 'PostalLabel::getStats()[$data->stat]',
            'filter' => PostalLabel::getStats(),
        ),
        array(
            'header' => 'Get label',
            'value' => '$data->stat == PostalLabel::STAT_SUCCESS ? CHtml::link(TbHtml::icon(TbHtml::ICON_FILE), array("/postal/postalLabel/getLabel","id" => $data->id),array("target" => "_blank")):"-"',
            'type' => 'raw',
        ),
        'log',
        [
            'name' => '_date_entered',
            'value' => '$data->postal->date_entered',
        ],
        'date_updated',
        array(
            'name' => '_broker',
            'value' => 'PostalLabel::getOperators()[$data->postalOperator->broker_id]',
            'filter' => PostalLabel::getOperators(),
        ),
    ),
)); ?>