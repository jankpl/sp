<?php

class CourierStuckItemsController extends Controller
{

    public function filters()
    {
        return array(
            'accessControl',
//            'backend.filters.GridViewHandler' //path to GridViewHandler.php class
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
                'expression' => '0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users' => array('*'),
            ),
        );
    }


    public function actionIndex()
    {
        $models = Courier::model()->with('stat')->with('courierLabelNew')->with('user')->findAll(['condition' => "t.date_entered >= '2019-01-01' AND stat_map_id NOT IN (40,1,2,60,90) AND datediff(now(), t.date_updated) > 7 AND courier_type IN (".Courier::TYPE_INTERNAL.", ".Courier::TYPE_U.")", 'order' => 't.user_id ASC, t.date_updated ASC']);

        $this->render('index', [
            'models' => $models
        ]);
    }


}