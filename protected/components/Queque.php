<?php

class Queque extends CApplicationComponent{

    const TYPE_SMS = 'SMS';
    const TYPE_EMAIL = 'email';

    const KEEP_AGE_DAYS = 2;


    public function init()
    {
        return parent::init();
    }


    public function scheudleItem($type, array $params)
    {
        $params = base64_encode(serialize($params));

        $cmd = Yii::app()->db->createCommand();
        return $cmd->insert('queque', array('type' => $type, 'params' => $params));
    }


    public function run($limit = 100)
    {
        $cmd = Yii::app()->db->createCommand();
        $result = $cmd->select('id, params, type')
            ->from('queque')
            ->where('finished = 0')
//            ->limit(100)
            ->order('id ASC');

        if(in_array(intval(date('H')),[22,23,0,1,2,3,4,5,6,7]))
            $result->andWhere('type != :type', [':type' => self::TYPE_SMS]);

        if($limit)
            $result = $result->limit($limit);

        $result = $result->queryAll();

        foreach($result AS $item)
        {
            $CACHE_NAME = 'queque_' . $item['id'];
            // PRINT IS ALREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
            if (Yii::app()->cache->get($CACHE_NAME) !== false) {
                MyDump::dump('queque-pd.txt','-- PREVENT DUPLICATE:'.$item['id']);
                continue;
            }
            Yii::app()->cache->set($CACHE_NAME, 10, 60 * 60);

            $params = unserialize(base64_decode($item['params']));

            if($item['type'] == self::TYPE_SMS)
                $this->runTypeSms($params);
            else if($item['type'] == self::TYPE_EMAIL)
                $this->runTypeEmail($params);

            $cmd = Yii::app()->db->createCommand();
            $cmd->update('queque', array('finished' => 1, 'date_finished' => new CDbExpression('NOW()')), 'id = :id', array(':id' => $item['id']));
        }
    }

    public function howLong()
    {
        $cmd = Yii::app()->db->createCommand();
        $result = $cmd->select('COUNT(*)')
            ->from('queque')
            ->where('finished = 0')->queryScalar();

        return $result;
    }

    protected function runTypeSms(array $params)
    {
        $nr = $params['nr'];
        $text = $params['text'];
        $pv = $params['pv'];

        $smsApi = Yii::createComponent('application.components.SmsApi');
        return $smsApi->realSendSms($nr, $text, $pv);
    }

    protected function runTypeEmail(array $params)
    {

        $params['queque'] = false;
        call_user_func_array(array('S_Mailer', 'send'), $params);
    }

    public function clearOldAndFinished()
    {
        if(self::KEEP_AGE_DAYS > 0)
        {
            $cmd = Yii::app()->db->createCommand();
            return $cmd->delete('queque', 'finished = 1 AND date_finished < (NOW() - INTERVAL :days DAY)', [':days' => self::KEEP_AGE_DAYS]);
        }
        return false;
    }

}