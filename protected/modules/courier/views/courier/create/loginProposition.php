<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Janek
 * Date: 16.09.13
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */
?>

<div class="form row">

    <h2><?php echo Yii::t('app','Krok {step}', array('{step}' => '0/6')); ?> | <?php echo Yii::t('app','Zaloguj się'); ?></h2>

    <div class="col-xs-12">

        <?php $this->renderPartial('//site/_login', array(
            'model' => $model,
        )); ?>


    </div>


</div>