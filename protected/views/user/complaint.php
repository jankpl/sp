<?php
/* @var $this SiteController */
/* @var $model UserComplaintForm */
/* @var $form CActiveForm */
?>

<h2><?php echo Yii::t('complaintForm','Formularz reklamacji');?></h2>
<p><?= Yii::t('complaintForm', 'Dotyczy');?>: <strong><?= $model->getWhatName();?></strong></p>
<p><?= Yii::t('complaintForm', 'Numer');?>: <strong><?= $model->getWhatId();?></strong></p>
<br/>
<div class="row">
    <div class="col-xs-12 col-sm-6">
           <?php
        $this->widget('FlashPrinter');
        ?>
            <div class="form">
                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'contact-form',
                    'enableClientValidation'=>true,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                        //'validateOnType' => true,
                    ),
                )); ?>
                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 text-left"><?= $form->labelEx($model, 'email', ['style' => 'width: 100%; text-align: left;']);?><?php echo $form->textField($model,'email', []);?></div>

                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 text-left"><?= $form->labelEx($model, 'tel', ['style' => 'width: 100%; text-align: left;']);?><?php echo $form->textField($model,'tel', []); ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 text-left"><?= $form->labelEx($model, 'name', ['style' => 'width: 100%; text-align: left;']);?><?php echo $form->textField($model,'name', []); ?></div>
                </div>
                <div><?= $form->labelEx($model, 'body', ['style' => 'width: 100%; text-align: left;']);?>
                    <?php echo $form->textArea($model,'body',['rows'=>6, 'cols'=>50, ]); ?>
                </div>
                <div><?= $form->label($model, 'file_1', ['style' => 'width: 100%; text-align: left;']);?>
                    <?php echo $form->fileField($model,'file_1', ['accept' => implode(',', UserComplaintForm::allowedExtensions(true))]); ?>
                </div>
                <div><?= $form->label($model, 'file_2', ['style' => 'width: 100%; text-align: left;']);?>
                    <?php echo $form->fileField($model,'file_2', ['accept' => implode(',', UserComplaintForm::allowedExtensions(true))]); ?>
                </div>
                <div><?= $form->label($model, 'file_3', ['style' => 'width: 100%; text-align: left;']);?>
                    <?php echo $form->fileField($model,'file_3', ['accept' => implode(',', UserComplaintForm::allowedExtensions(true))]); ?>
                </div>
                <div><?= $form->labelEx($model, 'send_copy', ['style' => 'width: 100%; text-align: left;']);?>
                    <?php echo $form->checkBox($model,'send_copy'); ?>
                </div>
<hr/>
                <div class="text-center">
                    <?php echo TbHtml::submitButton(Yii::t('complaintForm','Wyślij'), ['class' => 'btn btn-primary btn-lg']); ?>
                </div>
                <?php $this->endWidget(); ?>
            </div><!-- form -->
<hr/>
        <?php
        $this->Widget('ShowPageContent', array('id' => 36));
        ?>
    </div>
</div>