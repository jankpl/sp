<?php

Yii::import('application.modules.hybridMail.models._base.BaseHybridMailPrice');

class HybridMailPrice extends BaseHybridMailPrice
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('creation_admin_id', 'default',
                'value'=>Yii::app()->user->id, 'setOnEmpty' => false, 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on'=>'update'),
            array('update_admin_id', 'default',
                'value'=>Yii::app()->user->id, 'setOnEmpty' => false, 'on'=>'update'),
            array('stat', 'default',
                'value'=>S_Status::ACTIVE, 'on'=>'insert'),

            array('stat', 'numerical', 'integerOnly'=>true),
            array('creation_admin_id, update_admin_id', 'length', 'max'=>10),
            array('date_updated', 'safe'),
            array('date_updated, update_admin_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, date_updated, creation_admin_id, update_admin_id, stat', 'safe', 'on'=>'search'),
        );
    }

    protected static $_maxPrice = 9999;

    public function getHybridMailGroupId()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('country_group');
        $cmd->from('hybrid_mail_price_has_group');
        $cmd->andWhere('hybrid_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            return $result;

        }

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('country_group');
        $cmd->from('hybrid_mail_delivery_price_has_hub');
        $cmd->where('hybrid_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        return $result;
    }

    public function getUserGroupId()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('user_group_id');
        $cmd->from('hybrid_mail_price_has_group');
        $cmd->andWhere('hybrid_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            return $result;

        }

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('user_group_id');
        $cmd->from('hybrid_mail_delivery_price_has_hub');
        $cmd->where('hybrid_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        return $result;
    }

    public static function loadOrCreatePrice($country_group, $user_group = null)
    {
        if($user_group == NULL)
            $model = HybridMailPrice::model()->with('hybridMailPriceHasGroups')->find('hybridMailPriceHasGroups.country_group=:id AND user_group_id IS NULL', array(':id' => $country_group));
        else
            $model = HybridMailPrice::model()->with('hybridMailPriceHasGroups')->find('hybridMailPriceHasGroups.country_group=:id AND user_group_id = :group', array(':id' => $country_group, ':group' => $user_group));



        if($model == NULL)
            $model = new HybridMailPrice();


        return $model;
    }

    public function deleteWithRelated()
    {
        $success = false;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id');
        $cmd->from('hybrid_mail_price_has_group');
        $cmd->andWhere('hybrid_mail_price_item = :price', array(':price' => $this->id));
        $result = $cmd->queryScalar();

        if($result)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('hybrid_mail_price_value','hybrid_mail_price_id = :id', array(':id' => $this->id));
            $cmd->delete('hybrid_mail_price_has_group','id = :id', array(':id' => $result));
            $cmd->delete('hybrid_mail_price','id = :id', array(':id' => $this->id));

            $success = true;
        }

        return $success;
    }

    public function loadValueItems()
    {
        $hybridMailPriceValues = [];

        if($this->id != NULL)
            $hybridMailPriceValues = HybridMailPriceValue::model()->findAll(array('condition' => 'hybrid_mail_price_id = :hybrid_mail_price_id', 'params' => array(':hybrid_mail_price_id' => $this->id)));

        if(!S_Useful::sizeof($hybridMailPriceValues))
            $hybridMailPriceValues[0] = new HybridMailPriceValue();

        return $hybridMailPriceValues;
    }

    public static function getMaxPrice()
    {
        return self::$_maxPrice;
    }

    public function generateDefaultPriceValue()
    {
        $maxPriceModel = new HybridMailPriceValue();
        $maxPriceModel->hybrid_mail_price_id = $this->id;
        $maxPriceModel->firstPagePrice = new Price();
        $maxPriceModel->nextPagePrice = new Price();


        return $maxPriceModel;
    }

    /**
     * Returns array with price components of price for given user group
     *
     * @param $weight float
     * @param $country_from int
     * @param $user_group_id int
     * @return array
     */
    protected static function getPriceDataForUserGroup($currency, $weight, $country_to, $user_group_id)
    {


        if($user_group_id == NULL)
            return false;

        $country_group = CountryGroup::getGroupForCountryId($country_to);


        $cmd = Yii::app()->db->createCommand();
        $cmd->select('firstPagePrice.value AS next_page_price, nextPagePrice.value AS first_page_price');
        $cmd->from('hybrid_mail_price_value');
        $cmd->join('hybrid_mail_price', 'hybrid_mail_price_value.hybrid_mail_price_id = hybrid_mail_price.id');
        $cmd->join('hybrid_mail_price_has_group', 'hybrid_mail_price_has_group.hybrid_mail_price_item = hybrid_mail_price.id');
        $cmd->join('country_group', 'hybrid_mail_price_has_group.country_group = country_group.id');
        $cmd->join('price_value AS firstPagePrice', 'hybrid_mail_price_value.next_page_price = firstPagePrice.price_id AND firstPagePrice.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->join('price_value AS nextPagePrice', 'hybrid_mail_price_value.first_page_price = nextPagePrice.price_id AND nextPagePrice.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('country_group.id = :country_group', array(':country_group' => $country_group));
        $cmd->andWhere('hybrid_mail_price_has_group.user_group_id = :user_id', array(':user_id' => $user_group_id));
        $cmd->limit('1');
        $result = $cmd->queryRow();

        return $result;

    }

    /**
     * Returns array with price components of price
     *
     * @param $weight float
     * @param $country_from int
     * @param $user_group_id int
     * @return array
     */
    protected static function getPriceData($currency, $weight, $country_to, $user_group_id)
    {


        $country_group = CountryGroup::getGroupForCountryId($country_to);

        $priceForUser = self::getPriceDataForUserGroup($currency, $weight, $country_to, $user_group_id); // find if user has his own price

        if($priceForUser)
            return $priceForUser;

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('firstPagePrice.value AS next_page_price, nextPagePrice.value AS first_page_price');
        $cmd->from('hybrid_mail_price_value');
        $cmd->join('hybrid_mail_price', 'hybrid_mail_price_value.hybrid_mail_price_id = hybrid_mail_price.id');
        $cmd->join('hybrid_mail_price_has_group', 'hybrid_mail_price_has_group.hybrid_mail_price_item = hybrid_mail_price.id');
        $cmd->join('country_group', 'hybrid_mail_price_has_group.country_group = country_group.id');
        $cmd->join('price_value AS firstPagePrice', 'hybrid_mail_price_value.next_page_price = firstPagePrice.price_id AND firstPagePrice.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->join('price_value AS nextPagePrice', 'hybrid_mail_price_value.first_page_price = nextPagePrice.price_id AND nextPagePrice.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('country_group.id = :country_group', array(':country_group' => $country_group));
        $cmd->andWhere('hybrid_mail_price_has_group.user_group_id IS NULL');
        $cmd->limit('1');
        $result = $cmd->queryRow();
        return $result;
    }


    /**
     * Returns hybridMail pickup price based on given arguments.
     *
     * @param $weight float Package weight in defined unit
     * @param $country_from int Sender country ID
     * @param null $user_group_id int User group ID
     * @return float Returns price in defined unit
     */
    public static function calculatePriceEach($currency, $pages, $country_to, $user_group_id = null)
    {

        $priceData = self::getPriceData($currency, $pages, $country_to, $user_group_id);
        $next_page_price = $priceData['next_page_price'];
        $first_page_price = $priceData['first_page_price'];

        $price = $first_page_price + $next_page_price * ($pages - 1);

        return $price;

    }

    public function toggleStat()
    {
        if(!S_Useful::sizeof($this->hybridMailPriceValues))
            return false;

        $this->stat = abs($this->stat - 1);
        return $this->save();
    }


}