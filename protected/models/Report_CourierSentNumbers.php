<?php

class Report_CourierSentNumbers extends Report
{
    const SEPARATOR = '; ';



    public static function processReport()
    {
        Yii::app()->getModule('courier');

        $date = date('Y-m-d');
        $model = self::model()->find('type = :type AND DATE(date_entered) = :date AND stat = 1', [':type' => self::TYPE_COD_SETTLED, ':date' => $date]);


        if($model)
            return false;

        $cmd = Yii::app()->db->createCommand();
        $values = $cmd->select('local_id, cod_value, cod_currency, courier.user_id, settle_date')->from('courier_cod_settled')->join('courier', 'courier_id = courier.id')
            ->where('DATE(courier_cod_settled.date_entered) = :date AND type = :type',
                [
                    ':date' => date('Y-m-d', strtotime('-1 day')),
                    ':type' => CourierCodSettled::TYPE_OK,
                ])
            ->order('user_id ASC, settle_date ASC, cod_value ASC')
            ->queryAll();

        // group data by user
        $data = [];
        foreach($values AS $value)
        {
            if(!isset($data[$value['user_id']]))
                $data[$value['user_id']] = [];

            $data[$value['user_id']][] = $value;

        }

        $model = new self;
        $model->type = self::TYPE_COD_SETTLED;

        $model->date_entered = date('Y-m-d H:i:s');
        $model->date_sent = date('Y-m-d H:i:s');
        $model->what_id = 0;
        $model->stat = 1;

        foreach($data AS $user_id => $items) {
            self::_processByUser($user_id, $items);
        }

        $model->save();

    }

    protected static function _processByUser($user_id, array $sourceData)
    {

        $xlsData = [];
        $xlsData[] = ['local_id', 'external id', 'cod_value','cod_currency', 'settle_date'];

        $user = User::model()->findByPk($user_id);

        $lang = $user->getPrefLang();

        $tempLangChange = new TempLangChange();
        $tempLangChange->temporarySetLanguage($lang);

        $report = Yii::t('mail_cod', 'COD settled report')."<br/>";
        $report .= "<br/>";
        $report .=  date('Y-m-d', strtotime('-1 day'));
        $report .= "<br/><br/>";
        $report .= Yii::t('mail_cod', 'This raport contains all packages that have COD transferred on the date provided above.')."<br/>";
        $report .= "<br/><br/>";
        $report .= '<table border="1">
<tr style="font-weight: bold;"><td>'.Yii::t('mail_cod', 'Local ID').'</td><td>'.Yii::t('mail_cod', 'Ext. ID').'</td><td>'.Yii::t('mail_cod', 'COD Value').'</td><td>'.Yii::t('mail_cod', 'COD Currency').'</td><td>'.Yii::t('mail_cod', 'Date').'</td></tr>';

        $total = [];
        foreach($sourceData AS $value) {


            $model = \Courier::model()->findByAttributes(['local_id' => $value['local_id']]);
            list($operators, $externalIds) = $model->getExternalOperatorsAndIds(true);


            $report .= '<tr><td style="text-align: left; vertical-align: top;">' . $value['local_id'] . '</td><td style="text-align: left; vertical-align: top;">' . implode(',', $externalIds) . '</td><td style="text-align: right; vertical-align: top;">' . $value['cod_value'] . '</td><td style="text-align: left; vertical-align: top;">' . $value['cod_currency'] . '</td><td style="text-align: center; vertical-align: top;">'.$value['settle_date'].'</td></tr>';

            if(!isset($total[$value['cod_currency']]))
                $total[$value['cod_currency']] = 0;

            $total[$value['cod_currency']] += $value['cod_value'];

            $xlsData[] = [
                $value['local_id'],
                implode(',', $externalIds),
                $value['cod_value'],
                $value['cod_currency'],
                $value['settle_date'],
            ];
        }

        $report .= '</table>';

        $report .= '<br/><br/><table border="1"><tr><th>'.Yii::t('mail_cod', 'Currency').'</th><th>'.Yii::t('mail_cod', 'Total').'</th></tr>';


        foreach($total AS $currency => $value)
            $report .= '<tr><td>'.$currency.'</td><td style="text-align: right;">'.$value.'</td></tr>';

        $report .= '</table>';


        $report .= '</table>';

        $loginSafe = $user->login;
        $loginSafe = preg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $loginSafe);
        $loginSafe = preg_replace("([\.]{2,})", '', $loginSafe);

        $filename = 'cod_raport_'.$loginSafe.'_'.date('Y-m-d', strtotime('-1 day')).'.xlsx';
        $xls = S_XmlGenerator::generateXml($xlsData, $filename, true);


        $mails = [];
        if ($user->getAccountingEmail(false))
            $mails[] = $user->getAccountingEmail(false);

        if ($user->getAccountingEmail2(false))
            $mails[] = $user->getAccountingEmail2(false);

        if ($user->getAccountingEmail3(false))
            $mails[] = $user->getAccountingEmail3(false);

        if (!S_Useful::sizeof($mails))
            $mails[] = $user->email;

        if(in_array($user->id, [2036,2037]))
            $mails[] = 'maciejszostak@swiatprzesylek.pl';


        foreach($mails AS $mail)
        {
            S_Mailer::sendToCustomer($mail, $mail, Yii::t('mail_cod', 'COD Settled report'), $report, false, $lang, true, false, $filename, false, false, $user->source_domain, $xls);
        }


        $tempLangChange->revertToBaseLanguage();
    }

}