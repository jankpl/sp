<?php

class PricingController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionWeight($id) {

        $models = Pricing::model()->findAll('product=:product',array(':product' => $id));

        $this->render('weight', array(
            'models' => $models,
            'id' => $id,
        ));
    }


    public function actionUpdate($id) {


        $priceValues = Pricing::model()->findAll(array('condition' => 'product = :id', 'params' => array(':id' => $id), 'order' => 'top_weight ASC'));

        if(!S_Useful::sizeof($priceValues))
        {
            $new = new Pricing();
            $new->product = $id;
            $priceValues[0] = $new;
        }

        if (isset($_POST['Pricing']) OR isset($_POST['add_more']) OR isset($_POST['sort'])) {

            $priceValues = Array();

            $transaction = Yii::app()->db->beginTransaction();
            $error = false;

            $cmd = Yii::app()->db->createCommand();

            $backupOptions = PricingOptions::model()->findAllByAttributes(array('product' => $id));
            $backupOptionsOrdered = [];

            // Kopiuje opcje cenowe i przywraca je dla znalezionych wag */

            /* @var $item PricingOptions */
            foreach($backupOptions AS $item)
            {
                if(!is_array($backupOptionsOrdered[strval($item->pricing->top_weight)]))
                    $backupOptionsOrdered[strval($item->pricing->top_weight)] = [];

                array_push($backupOptionsOrdered[strval($item->pricing->top_weight)], $item);
            }

            $cmd->delete('pricing_options', 'product = :id',
                array(':id' => $id));

            $cmd->delete('pricing', 'product = :id',
                array(':id' => $id));

            function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
                $sort_col = array();
                foreach ($arr as $key=> $row) {
                    $sort_col[$key] = $row[$col];
                }

                array_multisort($sort_col, $dir, $arr);
            }

            array_sort_by_column($_POST['Pricing'], 'top_weight');

            foreach($_POST['Pricing'] AS $key => $item)
            {
                $priceValues[$key] = new Pricing;
                $priceValues[$key]->setAttributes($item);
                $priceValues[$key]->product = $id;

                if(!$priceValues[$key]->save())
                    $error = true;
            }

            /* Backup of saved options */


            /* @var $savedPrince Pricing */
            foreach($priceValues AS $savedPrice)
            {
                if(!$savedPrice->hasErrors())
                {
                    if(S_Useful::sizeof($backupOptionsOrdered[strval($savedPrice->top_weight)]))
                    {
                        foreach($backupOptionsOrdered[strval($savedPrice->top_weight)] AS $item)
                        {
                            $item->pricing_id = $savedPrice->id;
                            $item->pricing = NULL;
                            $item->date_entered = NULL;
                            $item->id = NULL;
                            $item->isNewRecord = true;

                            $item->save();
                        }
                    }
                }
            }

            // Clicked button for more prices
            if(isset($_POST['add_more']))
            {
                $new = new Pricing;
                $new->id = $id;

                array_unshift($priceValues, $new);
                $error = true;
            }

            // Clicked button for just sort
            if(isset($_POST['sort']))
            {
                $error = true;
            }

            if(!$error)
            {
                $transaction->commit();
                Yii::app()->user->setFlash('success','Changes has been saved! Remember to update Pricing Options!');
                $this->redirect(array('weight', 'id' => $id));

            }
            $transaction->rollback();
        }

        $this->render('update', array(
            'priceValues' => $priceValues,
        ));
    }

    public function actionOptions($id) {


        $pricingOptions = [];

        foreach(Pricing::model()->findAll(array('condition' => 'product = :id', 'params' => array(':id' => $id), 'order' => 'top_weight ASC')) AS $pricing)
        {

            foreach(PricingOptions::getOptions()[$id] AS $optionGroupKey => $optionGroup)
            {

                foreach($optionGroup['options'] AS $key => $option)
                {

                    $model = PricingOptions::model()->find('product=:product AND option_group=:option_group AND option_item=:option_item AND pricing_id = :pricing_id', array(':product' => $id, ':option_group' => $optionGroupKey, ':option_item' => $key, ':pricing_id' => $pricing->id));

                    if($model === NULL)
                    {
                        $model = new PricingOptions();
                        $model->product = $id;
                        $model->option_group = $optionGroupKey;
                        $model->option_item = $key;
                        $model->pricing_id = $pricing->id;
                    }

                    $pricingOptions[$optionGroupKey][$pricing->id][$key] = $model;
                }

            }
        }

        if(isset($_POST['PricingOptions']))
        {

            $transaction = Yii::app()->db->beginTransaction();
            $error = false;

            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('pricing_options', 'product = :id',
                array(':id' => $id));


            foreach($_POST['PricingOptions'] AS $optionGroupKey => $item)
                foreach($item AS $optionKey => $item2)
                    foreach($item2 AS $pricingKey => $item3)
                    {
                        $temp = new PricingOptions;
                        $temp->setAttributes($item3);
                        $temp->pricing_id = $pricingKey;
                        $temp->product = $id;

                        if(!$temp->save())
                            $error = true;

                        $pricingOptions[$temp->option_group][$temp->pricing_id][$temp->option_item] = $temp;
                    }

            if(!$error)
            {
                $transaction->commit();
                Yii::app()->user->setFlash('success','Changes has been saved!');
                $this->redirect(array('pricing/'));
            }
            $transaction->rollback();
        }


        $this->render('updateOptions', array(
            'pricingOptions' => $pricingOptions,
            'id' => $id,
            'pricingOptionsHeader' => PricingOptions::getOptions()[$id],
        ));
    }



}