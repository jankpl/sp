<h1>Autoresponder</h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div class="form" style="position: relative;">
    <?php $form = $this->beginWidget('CActiveForm', array(
    )); ?>
    <table class="table table-striped" style="margin: 0 auto; width: 550px;">
        <tr>
            <th colspan="2">Account</th>
            <td ><?= $account;?>@<?= DaApiController::domainIdToEmailDomain($domain_id);?></td>
        </tr>
        <tr>
            <th colspan="2">Password to your email account (*)</th>
            <td>
                <?php
                if(!AdminRestrictions::isUltraAdmin()):
                    ?>
                    <?= CHtml::passwordField('password', '', ['style' => 'width: 100%;']);?>
                <?php
                else:
                    ?>
                    -- not required for your admin level --
                <?php
                endif;
                ?>
            </td>
        </tr>
        <tr>
            <th class="text-center">Date</th>
            <th class="text-center">Start</th>
            <th class="text-center">End</th>
        </tr>
        <tr>
            <td></td>
            <td class="text-center"><?= CHtml::textField('start', $start, ['date-picker' => 'true',]);?></td>
            <td class="text-center" colspan="2"><?= CHtml::textField('end', $end, ['date-picker' => 'true',]);?></td>
        </tr>
        <tr>
            <th>Prefix:</th>
            <td colspan="2" style="text-align: left;"><?= CHtml::textField('title', $title, ['style' => 'width: 100%;']);?><br/><strong>Prefix</strong>: oryginal subject</td>
        </tr>
        <tr>
            <th>Text</th>
            <td style="text-align: left;" colspan="2"><?= CHtml::textArea('text', $text,  ['style' => 'width: 100%; height: 400px; resize: none;']);?></td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3"><?= CHtml::submitButton($is_active ? 'Modify' : 'Create', ['class' => 'btn btn-large btn-primary', 'name' => 'set']);?></td>
        </tr>
        <?php
        if($is_active):
            ?>
            <tr>
                <td style="text-align: center;" colspan="3"><?= CHtml::submitButton('Delete autoresponder', ['class' => 'btn btn-small btn-danger', 'name' => 'delete']);?><br/>* password is required for deletion as well</td>
            </tr>
        <?php
        endif;
        ?>
    </table>
    <?php $this->endWidget(); ?>
</div>

<?php
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
$script = "$( function() {
            $('[date-picker]').datepicker({
            dateFormat : 'yy-mm-dd',
             firstDay: 1
            });
         } );";

Yii::app()->clientScript->registerScript('datePicker', $script, CClientScript::POS_END);
?>