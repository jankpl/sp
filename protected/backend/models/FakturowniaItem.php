<?php

class FakturowniaItem extends CFormModel
{

    public $name;
    public $no = 1;
    public $value_netto;
    public $value_brutto;
    public $tax = 23;

   protected function afterConstruct()
   {
       $this->name = 'Usługa dystrybucji przesyłek';
       parent::afterConstruct();
   }

    public function rules()
    {
        return array(
            ['no, value_brutto, tax, name', 'required'],
            ['name', 'length', 'max' => 128],
            ['no', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 999],
            ['value_netto, value_brutto', 'numerical', 'integerOnly' => false, 'min' => 0, 'max' => 9999999],
        );
    }


    public function attributeNames()
    {
        return array(

        );
    }

}