<?php
if($model):
    echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, 'Package #' . $model->local_id, array('closeText' => false));
    ?>
    <table style="margin: 0 auto; width: 100%;">
        <tr>
            <td><?php echo TbHtml::link('4 on 1', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_4_ON_1), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('2 on 1', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_2_ON_1), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('Roll', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_ROLL), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('A4 window left', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_A4_WINDOW_LEFT), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('A4 window right', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_A4_WINDOW_RIGHT), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('C4', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_C4), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('C5', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_C5), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('C6', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_C6), array('class' => 'btn'));?></td>
            <td><?php echo TbHtml::link('DL', array('/postal/postal/CarriageTicket', 'id' => $model->id, 'type' => PostalLabelPrinter::PDF_DL), array('class' => 'btn'));?></td>
        </tr>
    </table>
    <?php
else:
    echo TbHtml::alert(TbHtml::ALERT_COLOR_ERROR, 'Package not found!', array('closeText' => false));
endif;
?>
