<?php

Yii::import('application.models._base.BaseUserInvoice');

class UserInvoice extends BaseUserInvoice
{
    const UNPUBLISHED = 0;
    const PUBLISHED = 1;
    const SEEN = 2;

    const INV_STAT_NONE = 0;
    const INV_STAT_COMPLAINT = 5;
    const INV_STAT_CANCELLED = 9;
    const INV_STAT_CORRECTED = 8;

    const FINAL_DIR = 'upadmin/invoices/';

    const FILE_TYPE_INV = 0;
    const FILE_TYPE_SHEET = 1;

    public $_file_invoice;
    public $_file_sheet;

    const SOURCE_OWN = 0;
    const SOURCE_EXT = 1;
    const SOURCE_FAKTUROWNIA = 2;
    const SOURCE_FAKTUROWNIA_MANUAL = 3;
    const SOURCE_FAKTUROWNIA_MANUAL_KEDZIERZYN = 4;
    const SOURCE_CORRECTION_NOTE = 5;

    public $_oldAttributes = false;

    protected $_source_user_id = false; // used when has parent accounting id

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function scopes()
    {
        return array(
            'newFirst' => [
                'order'=>'stat ASC, no DESC',
            ],
            'waiting' => [
                'condition'=>'stat = '.self::SEEN.' AND inv_value > paid_value',
            ],
            'rest' => [
                'condition'=>'stat = '.self::SEEN.' AND inv_value = paid_value OR stat !='.self::INV_STAT_NONE,
            ],
            'published' => [
                'condition'=>'t.stat > '.self::UNPUBLISHED,
            ],
            'active' => [
                'condition'=>'t.stat > '.self::UNPUBLISHED.' AND t.stat_inv = '.self::INV_STAT_NONE,
            ],
            'new' => [
                'condition'=>'stat = '.self::PUBLISHED,
            ],
            'paid' => [
                'condition'=>'inv_value = paid_value',
            ],
            'paidPartialy' => [
                'condition'=>'inv_value > paid_value && paid_value > 0',
            ],
            'notPaid' => [
                'condition' => 'paid_value = 0',
            ],
            'notPaidWithPartialyPaid' => [
                'condition' => 'inv_value > paid_value',
            ],
            'notPaidWithPartialyPaidAndCorrections' => [
                'condition' => 'inv_value > paid_value OR inv_value < 0',
            ],
            'paidWithPartialyPaid' => [
                'condition' => 'paid_value > 0',
            ],
            'outdated' => [
                'condition' => 'inv_value > paid_value && inv_payment_date < :date',
                'params' => [':date' => date('Y-m-d')]
            ],
            'outdatedWithCorrections' => [
                'condition' => '(inv_value > paid_value && inv_payment_date < :date) OR inv_value < 0',
                'params' => [':date' => date('Y-m-d')]
            ],
            'ofCurrentUser' => [
                'condition' => 'user_id = :user_id',
                'params' => [':user_id' => Yii::app()->user->id]
            ]
        );
    }
    /**
     * Scope for items from provided user
     * @param $user_id
     * @return $this
     */
    public function ofUser($user_id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'user_id = :user_id',
            'params' => [':user_id' => $user_id]
        ));
        return $this;
    }

    public static function getSourceList()
    {
        return [
            self::SOURCE_OWN => 'Own',
            self::SOURCE_EXT => 'Ext',
            self::SOURCE_FAKTUROWNIA => 'Fakturownia.pl - auto',
            self::SOURCE_FAKTUROWNIA_MANUAL => 'Fakturownia.pl',
            self::SOURCE_FAKTUROWNIA_MANUAL_KEDZIERZYN => 'Fakturownia.pl[kedzierzyn]',
            self::SOURCE_CORRECTION_NOTE => 'Interest note',
        ];
    }

    public function getSourceName()
    {
        return isset(self::getSourceList()[$this->source]) ? self::getSourceList()[$this->source] : '';
    }

    /**
     * Scopre for pagination
     * @param $user_id
     * @return $this
     */
    public function withPagination($limit, $page)
    {
        $this->getDbCriteria()->mergeWith(array(
            'limit' => $limit,
            'offset' => $page * $limit
        ));
        return $this;
    }




    public function isPaid()
    {
        if($this->paid_value >= $this->inv_value)
            return true;
        else
            return false;
    }

    public static function getClosestPaymentDate($user_id = NULL)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        $cmd = Yii::app()->db->createCommand();

        $cmd->select('inv_payment_date');
        $cmd->from((new self)->tableName());
        $cmd->where('user_id = '.$user_id);
        $cmd->andWhere('stat > '.self::UNPUBLISHED);
//        $cmd->andWhere('inv_payment_date >= :date', [':date' => date('Y-m-d')]);
        $cmd->andWhere('inv_value > paid_value');
        $cmd->order('inv_payment_date ASC');
        $cmd->andWhere('stat_inv = :inv_stat', [':inv_stat' => UserInvoice::INV_STAT_NONE]);
        return $cmd->queryScalar();
    }


    public static function sumTotalInvValue($currency, $justUnpaid = false, $user_id = NULL)
    {
        if($user_id === NULL)
            $user_id = Yii::app()->user->id;

        $cmd = Yii::app()->db->createCommand();

        if($justUnpaid)
            $cmd->select('SUM(inv_value - paid_value)');
        else
            $cmd->select('SUM(inv_value)');
        $cmd->from((new self)->tableName());
        $cmd->where('user_id = :user_id', [':user_id' => $user_id]);
        $cmd->andWhere('stat > '.self::UNPUBLISHED);
        $cmd->andWhere('inv_currency = :currency', [':currency' => $currency]);
        $cmd->andWhere('stat_inv = :inv_stat', [':inv_stat' => UserInvoice::INV_STAT_NONE]);
        return $cmd->queryScalar();
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    public function init()
    {
        $this->paid_value = 0;
        return parent::init();
    }

    public function uniqueInvoice($attribute, $params)
    {
        if($this->isNewRecord) {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id');
            $cmd->from('user_invoice');
//            $cmd->where('user_id = :user_id AND title = :title AND inv_value = :inv_value', [':user_id' => $this->user_id, ':title' => $this->title, ':inv_value' => $this->inv_value]);
            $cmd->where('user_id = :user_id AND title = :title AND stat_inv != :stat_canceled AND stat_inv != :stat_corrected', [':user_id' => $this->user_id, ':title' => $this->title, ':stat_canceled' => UserInvoice::INV_STAT_CANCELLED, ':stat_corrected' => UserInvoice::INV_STAT_CORRECTED]);
            $cmd->limit = 1;
            $result = $cmd->queryScalar();


            if($result)
                $this->addError($attribute, 'Invoice with this title, user_id is already in the system and not cancelled/corrected!');
        }
    }

    public function rules() {

        return array(
            array('user_id, inv_value, inv_date, inv_payment_date, stat_inv, title, inv_currency', 'required'),
//            array('_file_invoice, _file_sheet', 'required', 'except' => 'update'),
            array('user_id, no, inv_currency, stat, stat_inv, notifications', 'numerical', 'integerOnly'=>true),
            array('hash', 'length', 'max'=>64),
            array('inv_value, paid_value, order_id, tax', 'numerical'),
            array('inv_file_path, sheet_file_path', 'length', 'max'=>256),
            array('inv_file_name, sheet_file_name, title', 'length', 'max'=>128),
            array('inv_file_type, sheet_file_type', 'length', 'max'=>128),
            array('date_updated', 'safe'),

            array('inv_currency', 'in', 'range' => Currency::listIds(), 'allowEmpty' => false),
            array('stat_inv', 'in', 'range' => array_keys(self::getStatInvArray()), 'allowEmpty' => false),
            array('user_id', 'exist', 'attributeName' => 'id', 'className' => 'User', 'allowEmpty' => false),

            array('inv_date, inv_payment_date', 'date', 'format' => 'yyyy-mm-dd'),

            array('note', 'safe'),
            array('title', 'uniqueInvoice'),

            array('date_updated, notifications, order_id', 'default', 'setOnEmpty' => true, 'value' => null),

//            array('title', 'unique'),

            array('stat', 'default', 'setOnEmpty' => true, 'value' => self::UNPUBLISHED),
            array('source', 'default', 'setOnEmpty' => true, 'value' => self::SOURCE_OWN),
            array('paid_value, inv_value', 'default', 'setOnEmpty' => true, 'value' => 0),

            array('id, date_entered, date_updated, hash, user_id, no, inv_value, paid_value, inv_currency, inv_date, inv_payment_date, inv_file_path, inv_file_name, inv_file_type, sheet_file_path, sheet_file_name, sheet_file_type, stat, stat_inv, notifications, title, note, source, tax', 'safe', 'on'=>'search'),
        );

    }

    public static function getStatArray()
    {
        $array = [
            self::UNPUBLISHED => 'Unpublished',
            self::PUBLISHED => 'Published',
            self::SEEN => 'Seen',
        ];

        return $array;
    }

    public static function getStatArrayUser()
    {
        $array = [
            self::PUBLISHED => Yii::t('user_invoice', 'Unseen'),
            self::SEEN => Yii::t('user_invoice', 'Seen'),
        ];

        return $array;
    }

    public static function getStatInvArray()
    {
        $array = [
            self::INV_STAT_NONE => Yii::t('user_invoice', '-'),
            self::INV_STAT_COMPLAINT => Yii::t('user_invoice', 'Reklamacja'),
            self::INV_STAT_CANCELLED => Yii::t('user_invoice', 'Anulowana'),
            self::INV_STAT_CORRECTED => Yii::t('user_invoice', 'Skorygowany'),
        ];

        return $array;
    }

    public function getStatName()
    {
        return isset(self::getStatArray()[$this->stat]) ? self::getStatArray()[$this->stat] : NULL;
    }


    public function getStatInvName()
    {
        return isset(self::getStatInvArray()[$this->stat_inv]) ? self::getStatInvArray()[$this->stat_inv] : NULL;
    }


    public function getCurrencyName()
    {
        return Currency::nameById($this->inv_currency);
    }

    public static function getFinalDir($fileName)
    {
        $path = realpath(self::FINAL_DIR);
        $path = $path.'/'.$fileName;
        return $path;
    }

    public function generateHash()
    {
        $this->hash = hash('sha256',microtime().uniqid());
    }

    public function beforeSave()
    {
        if ($this->isNewRecord)
        {
            if($this->hash == '')
                $this->generateHash();

            if($puid = $this->user->getParentAccountingUser())
            {
                $this->_source_user_id = $this->user_id;
                $this->user_id = $puid;
            }

        }

        if(Yii::app()->params['backend'] && !Yii::app()->user->isGuest)
            $this->admin_id = Yii::app()->user->id;
        else
            $this->admin_id = NULL;


        return parent::beforeSave();
    }

    public function deleteWithFile()
    {
        if($this->inv_file_name !== NULL)
            @unlink($this->inv_file_path);

        if($this->sheet_file_name !== NULL)
            @unlink($this->sheet_file_path);

        return $this->delete();
    }

    public function publish()
    {
        $this->stat = self::PUBLISHED;

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('no');
        $cmd->from('user_invoice');
        $cmd->where('user_id = :user_id AND (stat > 0) AND stat_inv NOT IN (:inv_stat, :inv_stat2)', array(':user_id' => $this->user_id, ':inv_stat' => self::INV_STAT_CANCELLED, ':inv_stat2' => self::INV_STAT_CORRECTED));
        $cmd->order('no DESC');
        $cmd->limit(1);
        $count = $cmd->queryScalar();

        $count = intval($count);
        $count++;
        $this->no = $count;

        return $this->update(['stat', 'no']);
    }

    public function notifyAboutPublished()
    {
        $lang = $this->user->getPrefLang();

        $tempLangChange = new TempLangChange();
        $tempLangChange->temporarySetLanguage($lang);

        $attachmentPath = $attachmentName = false;
        if($this->inv_file_name) {
            $attachmentPath = $this->inv_file_path;
            $attachmentName = $this->inv_file_name;
        }

        $mails = [];
        if ($this->user->getAccountingEmail(false))
            $mails[] = $this->user->getAccountingEmail(false);

        if ($this->user->getAccountingEmail2(false))
            $mails[] = $this->user->getAccountingEmail2(false);

        if ($this->user->getAccountingEmail3(false))
            $mails[] = $this->user->getAccountingEmail3(false);

        if (!S_Useful::sizeof($mails))
            $mails[] = $this->user->email;

        foreach($mails AS $mail)
            S_Mailer::sendToCustomer($mail,$this->user->name, Yii::t('user_mail','Nowa faktura'), Yii::t('user_mail',"Na Twoim koncie pojawiła się nowa faktura!{br}", array('{br}' => '<br/>')), true, $lang, true, $attachmentPath, $attachmentName, false, false, $this->user->source_domain);


        $tel = false;
        if($this->user->getAccountingTel())
            $tel = $this->user->getAccountingTel();
        else if($this->user->tel)
            $tel = $this->user->tel;

        if($tel) {
            $smsApi = Yii::createComponent('application.components.SmsApi');
            $smsApi->sendSms($tel, Yii::t('user_sms', 'Na Twoim koncie pojawila sie nowa faktura!'), $this->user->source_domain);
        }

        $tempLangChange->revertToBaseLanguage();
    }

    public function seen()
    {
        if($this->stat == self::SEEN)
            return true;

        $this->stat = self::SEEN;
        return $this->update(array('stat'));
    }


    public static function getNoOfNewInvoicesForUser($user_id)
    {
//        $models = UserMessage::model()->cache(60*15)->findAllByAttributes(array('user_id' => $user_id, 'stat' => UserMessage::PUBLISHED));
        $models = UserInvoice::model()->findAllByAttributes(array('user_id' => $user_id, 'stat' => UserInvoice::PUBLISHED));

        return S_Useful::sizeof($models);

    }

    public static function createByData($user_id, $inv_no, $inv_date, $inv_payment_date, $inv_value, $inv_currency, $inv_paid_value, $note, $source)
    {
        $model = new self;
        $model->stat_inv = self::INV_STAT_NONE;
        $model->user_id = $user_id;
        $model->title = $inv_no;
        $model->inv_date = $inv_date;
        $model->inv_payment_date = $inv_payment_date;
        $model->inv_value = $inv_value;
        $model->inv_currency = $inv_currency;
        $model->paid_value = $inv_paid_value;
        $model->note = $note;
        $model->source = $source;

        return $model;
    }


    public static function findLastPaidOrPartialyPaidInvoiceOfUser($user_id)
    {
        return self::model()->ofUser($user_id)->paidWithPartialyPaid()->find(['order' => 'date_updated DESC', 'limit' => 1]);
    }

    public function afterFind()
    {
        $this->_oldAttributes = $this->attributes;

        return parent::afterFind();
    }

    public function afterSave()
    {
        if($this->getIsNewRecord())
        {
            if($this->_source_user_id)
                UserInvoiceHistory::addOtherLog($this->id, 'Invoice automaticaly moved from source user: #'.$this->_source_user_id.' to parent accounting user: #'.$this->user_id);
        }


        UserInvoiceHistory::add($this);

        DebtBlock::tryToUnlockAccount($this->user_id);

        return parent::afterSave();
    }

    public function getOverdueDays()
    {
        if($this->paid_value >= $this->inv_value)
            return 0;

        $now = time();

        $your_date = strtotime($this->inv_payment_date);
        $datediff = $now - $your_date;
        $days = floor($datediff / (60 * 60 * 24));

        // @10.09.2018
        // also consider difference between inv_date and date_entered
        $daysAdditional = floor((strtotime($this->date_entered) - strtotime($this->inv_date)) / (60 * 60 * 24));
        if($daysAdditional > 0)
            $days -= $daysAdditional;

        return $days;
    }

    public static function createAndPublishWithFileString($user_id, $inv_no, $inv_date, $inv_payment_date, $inv_value, $inv_currency, $note, $source, $file_string, $isPaid = true, $file_string_sheet = false, $order_id = NULL, $tax = NULL, $autoPublish = true)
    {
        $model = new self;
        $model->stat_inv = self::INV_STAT_NONE;
        $model->user_id = $user_id;
        $model->title = $inv_no;
        $model->inv_date = $inv_date;
        $model->inv_payment_date = $inv_payment_date;
        $model->inv_value = $inv_value;
        $model->inv_currency = $inv_currency;
        $model->paid_value =  $isPaid ? $inv_value : 0;
        $model->note = $note;
        $model->source = $source;
        $model->order_id = $order_id;
        $model->tax = $tax;

        $model->generateHash();


        $path = UserInvoice::getFinalDir($model->hash.'_'.UserInvoice::FILE_TYPE_INV.'.'.'pdf');
        if (!@file_put_contents($path, base64_decode($file_string)))
            $errors = true;

        $model->inv_file_path = $path;
        $model->inv_file_name = 'invoice.pdf';
        $model->inv_file_type = mime_content_type($path);

        if($file_string_sheet)
        {
            $path = UserInvoice::getFinalDir($model->hash.'_'.UserInvoice::FILE_TYPE_SHEET.'.'.'xls');
            if (!@file_put_contents($path, $file_string_sheet))
                $errors = true;

            $model->sheet_file_path = $path;
            $model->sheet_file_name = 'sheet.xlsx';
            $model->sheet_file_type = mime_content_type($path);
        }

        if($model->save()) {

            if($autoPublish && $model->publish()) {
                $model->notifyAboutPublished();
            }

            return $model;
        }
    }
}
