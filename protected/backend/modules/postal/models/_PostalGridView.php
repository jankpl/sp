<?php

class _PostalGridView extends Postal
{
    public $__user_login;
    public $__receiver_country_id;
    public $__receiver_usefulName;
    public $__receiver_name;
    public $__receiver_company;
    public $__receiver_address;
    public $__receiver_city;
    public $__receiver_zip_code;
    public $__receiver_tel;
    public $__sender_country_id;
    public $__sender_usefulName;
    public $__sender_name;
    public $__sender_company;
    public $__sender_address;
    public $__sender_city;
    public $__sender_zip_code;
    public $__sender_tel;
    public $__stat;
    public $__operator;

    public $_multiIds;

    public function rules() {

        $array = array(
            array('location,
                __user_login,
                 __sender_country_id,
                 __sender_usefulName,
                 __sender_name,
                 __sender_company,
                 __sender_address,
                 __sender_city,
                 __sender_zip_code,
                 __sender_tel,
                 __receiver_country_id,
                 __receiver_usefulName,
                 __receiver_name,
                 __receiver_company,
                 __receiver_address,
                 __receiver_city,
                 __receiver_zip_code,
                 __receiver_tel,
                 __stat,
                 __type,
                 __operator,
                 local_id,
                 postal_stat_id,
                 _multiIds,
                 ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }

    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('local_id', $this->local_id, false);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('sender_address_data_id', $this->sender_address_data_id);
        $criteria->compare('receiver_country_id', $this->receiver_country_id);
        $criteria->compare('postal_type', $this->postal_type);
        $criteria->compare('postal_stat_id', $this->postal_stat_id);
        $criteria->compare('size_class', $this->size_class);
        $criteria->compare('weight_class', $this->weight_class);
        $criteria->compare('bulk_number', $this->bulk_number);
        $criteria->compare('bulk_id', $this->bulk_id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('user_cancel', $this->user_cancel);
        $criteria->compare('bulk', $this->bulk);
        $criteria->compare('target_sp_point', $this->target_sp_point);
        $criteria->compare('stat_map_id', $this->stat_map_id);
        $criteria->compare('ref', $this->ref, true);
        $criteria->compare('source', $this->source);

        if($this->__operator)
        {
            $criteria->together = true;
            $with[] = ['postalOperator'];
            $criteria->compare('postalOperator.id',$this->__operator);
        }

//        if($this->__stat)
//        {
//            $criteria->compare('postal_stat_id', $this->__stat);
//        }


        if($this->_multiIds)
            $criteria->addInCondition('t.id', explode(',', $this->_multiIds));

        if($this->__user_login)
        {
            $criteria->together  =  true;
            $criteria->with = array('user');
            $criteria->compare('user.login',$this->__user_login,true);
        }

        if($this->__sender_country_id)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->compare('senderAddressData.country_id',$this->__sender_country_id);
        }

        if($this->__receiver_country_id)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->compare('receiverAddressData.country_id',$this->__receiver_country_id);
        }

        if($this->__receiver_usefulName)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.name LIKE '%".$this->__receiver_usefulName."%' OR receiverAddressData.company LIKE '%".$this->__receiver_usefulName."%')");
        }

        if($this->__sender_usefulName)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.name LIKE '%".$this->__sender_usefulName."%' OR senderAddressData.company LIKE '%".$this->__sender_usefulName."%')");
        }

        if($this->__receiver_name)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.name LIKE '%".$this->__receiver_name."%')");
        }

        if($this->__sender_name)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.name LIKE '%".$this->__sender_name."%')");
        }

        if($this->__receiver_company)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.company LIKE '%".$this->__receiver_company."%')");
        }

        if($this->__sender_company)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.company LIKE '%".$this->__sender_company."%')");
        }

        if($this->__receiver_address)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.address_line_1 LIKE '%".$this->__receiver_address."%' OR receiverAddressData.address_line_2 LIKE '%".$this->__receiver_address."%')");
        }

        if($this->__sender_address)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.address_line_1 LIKE '%".$this->__receiver_address."%' OR senderAddressData.address_line_2 LIKE '%".$this->__receiver_address."%')");
        }

        if($this->__receiver_city)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.city LIKE '%".$this->__receiver_city."%')");
        }

        if($this->__sender_city)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.city LIKE '%".$this->__sender_city."%')");
        }

        if($this->__receiver_zip_code)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.zip_code LIKE '%".$this->__receiver_zip_code."%')");
        }

        if($this->__sender_zip_code)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.zip_code LIKE '%".$this->__sender_zip_code."%')");
        }

        if($this->__receiver_tel)
        {
            $criteria->together  =  true;
            $criteria->with = array('receiverAddressData');
            $criteria->addcondition("(receiverAddressData.tel LIKE '%".$this->__receiver_tel."%')");
        }

        if($this->__sender_tel)
        {
            $criteria->together  =  true;
            $criteria->with = array('senderAddressData');
            $criteria->addcondition("(senderAddressData.tel LIKE '%".$this->__sender_tel."%')");
        }

        if(Yii::app()->user->authority >= Admin::AUTHORITY_OPERATOR)
            $criteria->compare('user.salesman_id', Yii::app()->user->model->salesman_id);


        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';


        if($this->_multiIds != '')
            $sort->defaultOrder = 'FIELD(t.id, '.$this->_multiIds.')';


        $sort->attributes = array(
            '__user_login'=>array(
                'asc'=>'user.login ASC',
                'desc'=>'user.login DESC',
            ),
//            'daysFromStatusChange'=>array(
//                'asc'=>'(
//                        SELECT TIMESTAMPDIFF(
//                        DAY , date_entered,
//                        CURRENT_TIMESTAMP )
//                        FROM courier_stat_history
//                        WHERE courier_id = t.id
//                        ORDER BY date_entered DESC
//                        LIMIT 0 , 1
//                        ) ASC',
//                'desc'=>'(
//                        SELECT TIMESTAMPDIFF(
//                        DAY , date_entered,
//                        CURRENT_TIMESTAMP )
//                        FROM courier_stat_history
//                        WHERE courier_id = t.id
//                        ORDER BY date_entered DESC
//                        LIMIT 0, 1
//                        ) DESC',
//            ),
            '__sender_country_id' => array(
                'asc' => 'senderAddressData.country ASC',
                'desc' => 'senderAddressData.country DESC',
            ),
            '__receiver_country_id' => array(
                'asc' => 'receiverAddressData.country ASC',
                'desc' => 'receiverAddressData.country DESC',
            ),
            '__stat' => array(
                'asc' => 'postal_stat_id ASC',
                'desc' => 'postal_stat_id DESC',
            ),
            '*', // add all of the other columns as sortable
        );


        $criteria = $this->searchCriteria();

        // if there are no conditions, use simple count of packages to improve performance
        $totalCount = NULL;
        if ($criteria->condition == '') {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('COUNT(id)')->from((new self)->tableName());
            $totalCount = $cmd->queryScalar();
        }

        return new CActiveDataProvider($this
            ->with('senderAddressData')
            ->with('receiverAddressData')
            ->with('postalOperator')
            ->with('user')
            ->with('stat')
            , array(
                'criteria' => $criteria,
                'sort' => $sort,
                'totalItemCount'=> $totalCount,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', 50),
                ),
            ));

    }


//    public function searchForUser($minStat = NULL, $maxStat = NULL) {
//
//
//        $sort = new CSort();
//        $sort->defaultOrder = 't.id DESC';
//        $sort->attributes = array(
//            'daysFromStatusChange'=>array(
//                'asc'=>'(
//                        SELECT TIMESTAMPDIFF(
//                        DAY , date_entered,
//                        CURRENT_TIMESTAMP )
//                        FROM courier_stat_history
//                        WHERE courier_id = t.id
//                        ORDER BY date_entered DESC
//                        LIMIT 0 , 1
//                        ) ASC',
//                'desc'=>'(
//                        SELECT TIMESTAMPDIFF(
//                        DAY , date_entered,
//                        CURRENT_TIMESTAMP )
//                        FROM courier_stat_history
//                        WHERE courier_id = t.id
//                        ORDER BY date_entered DESC
//                        LIMIT 0, 1
//                        ) DESC',
//            ),
//            '__sender_country_id' => array(
//                'asc' => 'senderAddressData.country ASC',
//                'desc' => 'senderAddressData.country DESC',
//            ),
//            '__receiver_country_id' => array(
//                'asc' => 'receiverAddressData.country ASC',
//                'desc' => 'receiverAddressData.country DESC',
//            ),
//            '__stat' => array(
//                'asc' => 'courier_stat_id ASC',
//                'desc' => 'courier_stat_id DESC',
//            ),
//            '*', // add all of the other columns as sortable
//        );
//
//        $criteria = $this->searchCriteria();
//        $criteria->addCondition('user_id = '.Yii::app()->user->id);
//
//        if($minStat != NULL)
//            $criteria->addCondition('courier_stat_id >= '.$minStat);
//
//        if($maxStat != NULL)
//            $criteria->addCondition('courier_stat_id <= '.$maxStat);
//
//        return new CActiveDataProvider($this->with('user')->with('senderAddressData')->with('receiverAddressData'), array(
//            'criteria' => $criteria,
//            'sort' => $sort,
//            'pagination' => array(
//                'pageSize' => 50,
//            ),
//        ));
//
//    }

}