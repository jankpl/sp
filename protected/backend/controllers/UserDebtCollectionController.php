<?php

class UserDebtCollectionController extends Controller
{

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority && AdminRestrictions::isFinancialAdmin()',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        /* @var $model UserInvoice */
        $model = $this->loadModel($id, 'UserInvoice');

        if(isset($_POST['status']))
        {
            if(UserInvoiceDebtOptions::set($_POST['status'], $id))
            {
                Yii::app()->user->setFlash('success', 'Status has been set!');
            }
            $this->refresh();
        }
        $this->render('view',array(
            'model' => $model,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(array('/userDebtCollection/admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new _UserDebtCollectionGridView('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['UserDebtCollectionGridView']))
            $model->attributes = $_GET['UserDebtCollectionGridView'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function actionExportDebts()
    {

        $data = [];
        $data[] = [
            'User',
            'Date entered',
            'Title',
            'Inv Date',
            'Inv Payment Date',
            'Inv Value',
            'Inv Paid',
            'Inv Currency',
            'Is Paid',
            'Inv Stat',
            'Overdue days',
            'Last payment date',
            'Last payment',
            'Last payment currency',
            'Last payment %',
            'Email',
            'Tel',
            'City',
            'Zip-Code',
            'Address',
            'Country',
            'Salesman',
            'Debt Stat',
            'Debt Stat Date',
            'Contract Type',
            'Source',
            'Stat'
        ];

        $models = UserDebtCollection::model()->with('userInvoiceDebtOptionsLast')->notPaidWithPartialyPaidAndCorrections()->outdatedWithCorrections()->published()->findAll(['order' => 't.user_id ASC, t.date_entered ASC']);


        $lastPayment = [];

        /* @var $model UserDebtCollection */
        foreach($models AS $model) {

            if(!isset($lastPayment[$model->user_id]))
                $lastPayment[$model->user_id] = UserDebtCollection::findLastPaidOrPartialyPaidInvoiceOfUser($model->user_id);

            $data[] = [
                $model->user->login,
                $model->date_entered,
                $model->title,
                $model->inv_date,
                $model->inv_payment_date,
                (double) $model->inv_value,
                (double) $model->paid_value,
                $model->getCurrencyName(),
                $model->isPaid(),
                $model->getStatInvName(),
                (double) (str_replace('-','0', $model->daysAfterPayment()).',0'),
                $lastPayment[$model->user_id] ? substr($lastPayment[$model->user_id]->date_updated,0,10) : '-',
                (double) ( $lastPayment[$model->user_id] ? $lastPayment[$model->user_id]->paid_value : '-'),
                $lastPayment[$model->user_id] ? $lastPayment[$model->user_id]->getCurrencyName() : '-',
                $lastPayment[$model->user_id] ? ($lastPayment[$model->user_id]->inv_value ? round(($lastPayment[$model->user_id]->paid_value / $lastPayment[$model->user_id]->inv_value) * 100) : '-'): '-',
                $model->user->getAccountingEmail(true),
                $model->user->tel,
                $model->user->city,
                $model->user->zip_code,
                trim($model->user->address_line_1 . ' ' . $model->user->address_line_2),
                $model->user->country,
                $model->user->salesman ? $model->user->salesman->email : '',
                $model->userInvoiceDebtOptionsLast ? $model->userInvoiceDebtOptionsLast->getStatName() : '',
                $model->userInvoiceDebtOptionsLast ? $model->userInvoiceDebtOptionsLast->date_entered : '',
                $model->user->getContractTypeName(),
                $model->user->getSourceDomainName(),
                $model->getStatName()
            ];
        }

        S_XmlGenerator::generateXml($data);
    }

    public function actionSetStatusByGridView()
    {
        if(isset($_POST['Debt']))
        {
            $ids = explode(',', $_POST['Debt']['ids']);
            foreach($ids AS $id)
            {

                if(UserInvoiceDebtOptions::set($_POST['status'], $id))
                {
                    Yii::app()->user->setFlash('success', 'Status has been set!');
                }
            }
        }

        $this->redirect(['/userDebtCollection/admin']);
    }

}

