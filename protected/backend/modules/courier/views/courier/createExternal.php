<?php

/* @var $model Courier */


$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url' => array('index')),
    array('label'=>'Manage' . $model->label(2), 'url' => array('admin')),
);
?>

<h1>Create <?php echo GxHtml::encode($model->label()); ?></h1>
<div class="form">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ));
    ?>

    <h2>Nowa paczka</h2>

    <?php $this->renderPartial('_base_form',array(
        'model' => $model,
        'form' => $form,
    ));
    ?>
    <hr/>
    <h2>Dane operatora</h2>
    <div class="inline-form">

        <div class="row">
            <?php echo $form->labelEx($model->courierTypeExternal,'courier_external_operator_id'); ?>
            <?php echo $form->dropDownList($model->courierTypeExternal, 'courier_external_operator_id', CHtml::listData(CourierExternalOperator::model()->findAll(),'id','name'), array('empty' => '--select operator--')); ?>
            <?php echo $form->error($model->courierTypeExternal,'courier_external_operator_id'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model->courierTypeExternal,'external_id'); ?>
            <?php echo $form->textField($model->courierTypeExternal, 'external_id', array('maxlength' => 128)); ?>
            <?php echo $form->error($model->courierTypeExternal,'external_id'); ?>
        </div><!-- row -->

    </div>

    <hr/>

    <div style="text-align: center;">
        <?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'save', 'size' => TbHtml::BUTTON_SIZE_LARGE)); ?>
        <?php $this->endWidget();
        ?>
    </div>
</div>