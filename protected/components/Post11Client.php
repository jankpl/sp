<?php

class Post11Client
{
    const URL = GLOBAL_CONFIG::POST11_URL;
    const LOGIN = GLOBAL_CONFIG::POST11_LOGIN;
    const PASSWORD = GLOBAL_CONFIG::POST11_PASSWORD;

    public $package_weight;
    public $package_value;
    public $package_content;
    public $receiver_name;
    public $receiver_address;
    public $receiver_city;
    public $receiver_country;
    public $receiver_zip_code;
    public $receiver_tel;
    public $receiver_email;
    public $local_id;
    public $packages_number = 1;

    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {
        $model = new self;

        $to = $courierTypeU->courier->receiverAddressData;

        $model->package_weight = $courierTypeU->courier->getWeight(true);
        $model->package_value = $courierTypeU->courier->getPackageValueConverted('USD');
        $model->package_content = $courierTypeU->courier->package_content;
        $model->packages_number = $courierTypeU->courier->packages_number;


        $model->receiver_name = trim($to->name.' '.$to->company);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->local_id = $courierTypeU->courier->local_id;

        return $model->_go($returnError);
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierTypeInternal, AddressData $from, AddressData $to, $returnError = false)
    {
        $model = new self;

        $model->package_weight = $courierTypeInternal->courier->getWeight(true);
        $model->package_value = $courierTypeInternal->courier->getPackageValueConverted('USD');
        $model->package_content = $courierTypeInternal->courier->package_content;
        $model->packages_number = $courierTypeInternal->courier->packages_number;


        $model->receiver_name = trim($to->name.' '.$to->company);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->local_id = $courierTypeInternal->courier->local_id;

        return $model->_go($returnError);
    }

    public static function orderForPostal(Postal $postal)
    {
        $to = $postal->receiverAddressData;

        $model = new self;

        $weight = round($postal->getWeightClassValue()/1000, 2);
        if($weight < 0.01)
            $weight = 0.01;

        $model->package_weight = $weight;
        $model->package_value = $postal->value ? $postal->getValueConverted('USD') : 1;
        $model->package_content = $postal->content ? $postal->content : $postal->local_id;
        $model->packages_number = 1;

        $model->receiver_name = trim($to->name.' '.$to->company);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->local_id = $postal->local_id;

        $return = $model->_go(true);

        if(is_array($return))
            $return = array_pop($return);

        return $return;
    }

    protected static function _calculateControlDigit($base)
    {
        $factors = [8,6,4,2,3,5,9,7];

        $sum = 0;
        for ($i = 0; $i < strlen($base); $i++) {
            $num = $base[$i];
            $sum += $factors[$i] * $num;
        }

        $result = intval($sum / 11);

        $reminder = $sum - ($result * 11);

        if($reminder == 0)
            $number = 5;
        else if($reminder == 1)
            $number = 0;
        else
            $number = 11 - $reminder;

        return $number;
    }

    protected function _getTrackingNo()
    {
        //            RL25972101xEE - RL26072101xEE
        $number = NumbersStorehouse::_getNumber(NumbersStorehouse::POST11, true);

        $controlDigit = self::_calculateControlDigit($number);
        $number = 'RL'.$number.$controlDigit.'EE';

        return $number;
    }

    protected function _getDocumentNo()
    {
        $number = NumbersStorehouse::_getNumber(NumbersStorehouse::POST11_AIRWAY_DOCUMENT, true);
        $number = intval($number);
        $number = str_pad($number,10, '0', STR_PAD_RIGHT);

        $numberSecondPart = substr($number,3);
        $number = substr($number,0,3).'-'.$numberSecondPart;

        $controlDigit = intval($numberSecondPart) - (intval(intval($numberSecondPart) / 7) * 7);
        $number = $number.$controlDigit;

        return $number;
    }


    public static function manifestUpdateAndSend($manifestId, $dateOfItems)
    {
        Yii::app()->getModule('postal');
        Yii::app()->getModule('courier');
        $postals = Postal::model()->with('postalOperator')->with('postalLabel')->findAll('postalOperator.broker_id = :broker_id AND t.date_entered LIKE :date AND postalLabel.stat = :stat', [
            ':broker_id' => Postal::POSTAL_BROKER_POST11,
            ':date' => $dateOfItems.' %',
            ':stat' => PostalLabel::STAT_SUCCESS,
        ]);

        $couriers = CourierLabelNew::model()->with('courier')->findAll('operator = :broker_id AND courier.date_entered LIKE :date AND t.stat = :stat', [
            ':broker_id' => CourierLabelNew::OPERATOR_POST11,
            ':date' => $dateOfItems.' %',
            ':stat' => CourierLabelNew::STAT_SUCCESS
        ]);

        $packagesNumber = 0;
        $packagesWeight = 0;

        /* @var $postal Postal */
        foreach($postals AS $postal)
        {
            $packagesNumber += 1;
            $packagesWeight += round($postal->weight);
        }

        /* @var $courier CourierLabelNew */
        foreach($couriers AS $courier)
        {
            $packagesNumber += $courier->courier->packages_number;
            $packagesWeight += $courier->courier->getWeight(true) * $courier->courier->packages_number;
        }

        if(!$packagesNumber)
            return true;


        $data = new stdClass();
        $data->shipmentPieces = $packagesNumber;
        $data->shipmentWeight = $packagesWeight;

        $response = self::_call('shipment/' . $manifestId, $data, true);

//         if everything is ok, confirm shipment:

        if($response->success) {
            $response = self::_call('shipment/' . $manifestId . '/finish');

            if (!$response->success) {
                return false;
            }
        }
    }

    protected function _go($returnError = false)
    {

        $semaphore = new Semaphore(intval(Semaphore::POSTAL_LABEL_PREFIX.date('Ymd')));
        try {
            $semaphore->acquire();
        } catch (Exception $e) {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => 'Locking problem',
                ]];
                return $return;
            } else {
                return false;
            }
        }

        $manifestId = ManifestQueque::getManifestId(GLOBAL_BROKERS::BROKER_POST11, date('Y-m-d'), 0);

        if(!$manifestId)
        {

            $arriveDate = new DateTime();
            $arriveDate->modify('+3 day');

            // create shipment
            $data = new stdClass();
            $data->documentType = 'cmr';
            $data->documentNumber = self::_getDocumentNo();
//        $data->flightNumber = 'AY1234/01JAN17';
            $data->charter = false;
            $data->arriveDate = $arriveDate->format('Y-m-d').'T15:00:00Z';
            $data->shipmentPieces = 1;
            $data->shipmentWeight = 1;

            $response = self::_call('shipments', $data);

            if(!$response->success)
            {
                if ($returnError == true) {
                    $return = [0 => [
                        'error' => $response->error,
                    ]];
                    return $return;
                } else {
                    return false;
                }
            }

            $manifestId = $response->data->id;

            ManifestQueque::setManifestId(GLOBAL_BROKERS::BROKER_POST11, 0, $manifestId);
        }

        try {
            $semaphore->release();
        } catch (Exception $e) {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => 'Unlocking problem',
                ]];
                return $return;
            } else {
                return false;
            }
        }


        $shipmentID = $manifestId;

//
        // add packages
        $data = new stdClass();
        $data->packageID = $this->local_id;
        $data->tpServiceCode = 'P11_R';
//        $data->clientID = $this->local_id; // is assigned id loop
        $data->mailType = 'REGISTERED';
//        $data->parcelID = 'RS12345678EE'; // is assigned id loop
        $data->destination = $this->receiver_country;
        $data->commodity = $this->package_content;
        $data->itemWeight = number_format($this->package_weight, 3);
        $data->itemPrice = $this->package_value;
        $data->quantity = 1;
        $data->totalWeight = number_format($data->itemWeight, 3);
        $data->totalPrice = $data->itemPrice;
        $data->recipient = new stdClass();
        $data->recipient->name = $this->receiver_name;
        $data->recipient->phoneNumber = $this->receiver_tel;
        $data->recipient->email = $this->receiver_email;
        $data->recipient_address = new stdClass();
        $data->recipient_address->streetAddress = $this->receiver_address;
        $data->recipient_address->city = $this->receiver_city;
        $data->recipient_address->county = '';
        $data->recipient_address->zip = $this->receiver_zip_code;
        $data->recipient_address->country = $this->receiver_country;
//
//
        $baseClientId = $this->local_id;

        $trackIds = [];
        $clientIds = [];
        for($i = 1; $i <= $this->packages_number; $i++)
        {

            if($i == 1)
                $clientId = $baseClientId;
            else
                $clientId = $baseClientId.'_'.$i;

            $clientIds[$clientId] = $clientId;

            $trackIds[$clientId] = $this->_getTrackingNo();
        }

        $toInsert = [];
        foreach($clientIds AS $key => $item)
        {
            $data->clientID = $item;
            $data->parcelID = $trackIds[$key];
            $toInsert[] = clone $data;
        }


        $response = self::_call('shipment/' . $shipmentID . '/parcelsSync', $toInsert);

        if (!$response->success) {
            if ($returnError == true) {
                $return = [0 => [
                    'error' => $response->error,
                ]];
                return $return;
            } else {
                return false;
            }
        }
//        var_dump($response);
//        exit;
//
//
//        if (!$response->success) {
//            if ($returnError == true) {
//                $return = [0 => [
//                    'error' => $response->error,
//                ]];
//                return $return;
//            } else {
//                return false;
//            }
//        }
//
//
//        // loop waiting for async validation
//        $validated = false;
//        $tries = 0;
//        do {
//
//            // at first try, wait for 5 seconds
//            if (!$tries) {
//                sleep(15);
//                $tries++;
//            }
//
//            $response = self::_call('shipment/' . $shipmentID . '/status');
//
//            if ($response->success) {
//                if ($response->data->status == 'VALID') {
//                    $validated = true;
//                    // packages validated with success - go to next step
//                    break;
//                } elseif ($response->data->status == 'INVALID') {
//                    // packages validated with error - return error
//
//                    $errorList = [];
//                    foreach ($response->data->invalidParcels AS $item)
//                        $errorList[] = $item->clientID . ' : ' . $item->validationErrorCode . ' (' . $item->validationErrorMessage . ')';
//
//                    $errorList = implode(', ', $errorList);
//
//                    if ($returnError == true) {
//                        $return = [0 => [
//                            'error' => $errorList,
//                        ]];
//                        return $return;
//                    } else {
//                        return false;
//                    }
//                }
//            }
//
//
//            if($tries > 5)
//                sleep(60);
//            else
//                sleep(15);
//            $tries++;
//
//            // otherwise - go again...
//        } while ($tries < 15); //
//
//        if (!$validated) {
//            if ($returnError == true) {
//                $return = [0 => [
//                    'error' => $response->error ? $response->error : 'Validation timed out!',
//                ]];
//                return $return;
//            } else {
//                return false;
//            }
//        }


        // if shipment is confirmed, get labels

//        $response = self::_call('shipment/' . $shipmentID . '/labels');
//
//
//        if (!$response->success) {
//            if ($returnError == true) {
//                $return = [0 => [
//                    'error' => $response->error,
//                ]];
//                return $return;
//            } else {
//                return false;
//            }
//        }

        $return = [];

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        foreach($clientIds AS $item)
        {

            $response = self::_call('parcel/' . $trackIds[$item] . '/label');

            if (!$response->success) {
                if ($returnError == true) {
                    $return = [0 => [
                        'error' => $response->error,
                    ]];
                    return $return;
                } else {
                    return false;
                }
            }

            $label = base64_decode($response->data->label->pdf);

            $im->readImageBlob($label);
            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $im->trimImage(0);
            $im->stripImage();

            $return[] = [
                'status' => true,
                'label' => $im->getImageBlob(),
                'track_id' => $trackIds[$item],
            ];

        }

        return $return;
    }


//    public static function test()
//    {
//
//        $data = new stdClass();
//        $data->documentType = 'airwaybill';
//        $data->documentNumber = '125-00000000';
//        $data->flightNumber = 'AY1234/01JAN17';
//        $data->charter = false;
//        $data->arriveDate = '2017-11-10T15:00:00Z';
//        $data->shipmentPieces = 1;
//        $data->shipmentWeight = 1;
//
//        return self::_call('shipments', $data);
//    }
//
//    public static function test2()
//    {
//
//        $data = new stdClass();
//        $data->packageID = '123456';
//        $data->clientID = '123456';
//        $data->mailType = 'NOT_REGISTERED';
//        $data->parcelID = 'RS12345678EE';
//        $data->destination = 'PL';
//        $data->commodity = 'Content';
//        $data->itemWeight = 0.1;
//        $data->itemPrice = 1;
//        $data->quantity = 2;
//        $data->totalWeight = 0.2;
//        $data->totalPrice = 2;
//        $data->totalPrice = 2;
//        $data->recipient = new stdClass();
//        $data->recipient->name = 'Jan Nowak';
//        $data->recipient->phoneNumber = '123123123';
//        $data->recipient->email = 'test@test.pl';
//        $data->recipient_address = new stdClass();
//        $data->recipient_address->streetAddress = 'Testowa 1';
//        $data->recipient_address->city = 'Warszawa';
//        $data->recipient_address->county = '';
//        $data->recipient_address->zip = '00-001';
//        $data->recipient_address->country = 'PL';
//
//
//        self::_call('shipment/153/parcels', [ $data ]);
//    }
//
//    public static function test3()
//    {
//
//        $data = new stdClass();
//
//
//        return self::_call('shipment/152/status');
//    }


    protected static function _call($method, $data = false, $forcePut = false)
    {


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::URL.'/'.$method);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        if($data)
        {
            if($forcePut)
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            else
                curl_setopt($curl, CURLOPT_POST, TRUE);


            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

            MyDump::dump('post11.txt', print_r(json_encode($data),1));
        }
        curl_setopt($curl, CURLOPT_USERPWD,  self::LOGIN.':'.self::PASSWORD);

        $resp = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);


        MyDump::dump('post11.txt', $httpCode.' : '.print_r($resp,1));

        $resp = json_decode($resp);

        $response = new stdClass();
        $response->error = false;
        $response->success = false;
        $response->data = NULL;

        if($httpCode == 200)
        {
            $response->success = true;
            $response->data = $resp->data;
        } else {

            if(!$resp)
                $response->error = curl_error($curl);
            else {
                $response->error = $resp->status.' ('.$resp->message.')';
            }
        }
        curl_close($curl);

        return $response;
    }



    public static function track($id)
    {

        $id = str_replace('RS', 'RL', $id);

        $response = self::_call('trackings/'.$id);

        if($response->success)
        {
            $data = $response->data;
            $statuses = [];



            foreach($data->events AS $event)
            {
                $date = $event->eventTime;

                $temp = explode('T', $date);
                $date = $temp[0].' '.substr($temp[1],0,8);

                $statuses[] = [
                    'name' => $event->eventMessage,
                    'date' => $date,
                    'location' => $event->locationCode,
                ];
            }

            return $statuses;
        }

        return false;
    }

}