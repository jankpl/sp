<?php

class HuPostClient
{
    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $returnError = false)
    {
        try
        {
            $parcelNo = self::getTtNumber($courierInternal->courier->packages_number);
        }
        catch(Exception $ex)
        {
            $parcelNo = [];
        }

        if(!S_Useful::sizeof($parcelNo))
        {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'No more tracking numbers!',
                )];
                return $return;
            } else {
                return false;
            }
        }

        $return = [];
        foreach($parcelNo AS $ttNo)
        {
            $label = self::generateLabel($ttNo, $to->company, $to->name, $to->address_line_1, $to->address_line_2, $to->city, $to->zip_code, $to->country0->name, $to->tel, $courierInternal->courier->local_id, $courierInternal->courier->getWeight(true));

            $return[] = [
                'status' => true,
                'label' => $label,
                'track_id' => $ttNo,
            ];
        }

        return $return;
    }

    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {

        try {
            $parcelNo = self::getTtNumber($courierTypeU->courier->packages_number);
        }
        catch(Exception $ex)
        {
            $parcelNo = [];
        }

        if(!S_Useful::sizeof($parcelNo))
        {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'No more tracking numbers!',
                )];
                return $return;
            } else {
                return false;
            }
        }

        $to = $courierTypeU->courier->receiverAddressData;


        $return = [];
        foreach($parcelNo AS $ttNo)
        {
            $label = self::generateLabel($ttNo, $to->company, $to->name, $to->address_line_1, $to->address_line_2, $to->city, $to->zip_code, $to->country0->name, $to->tel, $courierTypeU->courier->local_id, $courierTypeU->courier->package_weight);
            $return[] = [
                'status' => true,
                'label' => $label,
                'track_id' => $ttNo,
            ];
        }

        return $return;

    }

    public static function orderForPostal(Postal $postal, $returnError = false)
    {

        try {
            $parcelNo = self::getTtNumber(1);
        }
        catch(Exception $ex)
        {
            $parcelNo = [];
        }

        if(!S_Useful::sizeof($parcelNo))
        {
            if ($returnError == true) {
                $return = [0 => array(
                    'error' => 'No more tracking numbers!',
                )];
                return $return;
            } else {
                return false;
            }
        }

        $to = $postal->receiverAddressData;


        $weightKg = round($postal->getWeightClassValue() / 1000, 3);

        $return = [];
        foreach($parcelNo AS $ttNo)
        {
            $label = self::generateLabel($ttNo, $to->company, $to->name, $to->address_line_1, $to->address_line_2, $to->city, $to->zip_code, $to->country0->name, $to->tel, $postal->local_id, $weightKg);
            $return[] = [
                'status' => true,
                'label' => $label,
                'track_id' => $ttNo,
            ];
        }

        if(is_array($return))
            return array_pop($return);
        else
            return $return;
    }


    protected static function generateLabel($parcelNo, $company, $name, $address_line_1, $address_line_2, $city, $zip_code, $country, $tel, $ref, $weight)
    {
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);

        $pageW = 100;
        $pageH = 150;

        $xBase = 1;
        $yBase = 1;

        $blockWidth = $pageW - (2 * $xBase);
        $blockHeight = $pageH - (2 * $yBase);

        $margins = 0;

        $border = 0;

        /* @var $pdf TCPDF */
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );


        $pdf->AddPage('H', array(100, 150), true);

        $pdf->Image('@' . self::getLabelBg(), $xBase + $margins, $yBase + $margins, $blockWidth, 130, '', 'N', false);

        $pdf->SetFont('freesans', '', 10);

        $text = $name . "\r\n";
        $text .= $company . "\r\n";
        $text .= $address_line_1 . "\r\n";
        $text .= $address_line_2 . "\r\n";
        $text .= $zip_code . "\r\n";
        $text .= $city . "\r\n";

        $pdf->MultiCell(85, 35, $text, $border, 'L', false, 1, 6, 20,
            true,
            0,
            false,
            true,
            35,
            'T',
            true
        );

        $pdf->SetFont('freesans', 'B', 12);
        $pdf->MultiCell(85, 5, $country, $border, 'L', false, 1, 6, 55,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(60, 7, $tel, $border, 'L', false, 1, 17, 66,
            true,
            0,
            false,
            true,
            7,
            'T',
            true
        );

        $pdf->SetFont('freesans', 'B', 20);
        $pdf->MultiCell(50, 8, $ref, $border, 'L', false, 1, 26, 73,
            true,
            0,
            false,
            true,
            8,
            'T',
            true
        );

        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(60, 5, ($weight * 1000) . ' g', $border, 'L', false, 1, 20, 81,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );

        $pdf->write1DBarcode($parcelNo, 'C128', 30, 100, '', 18, 0.4, $barcodeStyle, 'N');

        $pdf->SetFont('freesans', '', 12);
        $pdf->MultiCell(50, 5, $parcelNo, $border, 'L', false, 1, 45, 117,
            true,
            0,
            false,
            true,
            5,
            'T',
            true
        );


        return $pdf->Output('HuPost.pdf', 'S');
    }

    protected static function getTtNumber($no)
    {
        $data = [];
        for($i = 0; $i < $no; $i++) {
            $temp = NumbersStorehouse::_getNumber(NumbersStorehouse::HUNGARIAN_POST, true);

            if($temp) {
                $data[] = 'RR'.$temp.self::_calculateControlDigit($temp).'HU';
            }
        }

        return $data;
    }

    protected static function _calculateControlDigit($base)
    {
        $factors = [8,6,4,2,3,5,9,7];

        $sum = 0;
        for ($i = 0; $i < strlen($base); $i++) {
            $num = $base[$i];
            $sum += $factors[$i] * $num;
        }

        $result = intval($sum / 11);

        $reminder = $sum - ($result * 11);

        if($reminder == 0)
            $number = 5;
        else if($reminder == 1)
            $number = 0;
        else
            $number = 11 - $reminder;

        return $number;
    }

    public static function track($id)
    {
        return Client17Track::getTtPostal($id, false, false,  'HUPOST');
//
//
//        $track = new Trackingmore;
//
//        $result = $track->createTracking('magyar-posta',$id);
//
//        if($result['meta']['type'] == 'Success')
//            sleep(15); // wait after adding new package
//
//        $result = $track->getSingleTrackingResult('magyar-posta',$id);
//
//        $response = [];
//        if(is_array($result))
//        {
//            if($result['meta']['type'] == 'Success')
//            {
//                $data = $result['data']['origin_info']['trackinfo'];
//
//                if(is_array($data))
//                    foreach($data AS $item)
//                    {
//                        array_push($response, [
//                            'date' => $item['Date'],
//                            'location' => $item['Details'],
//                            'name' => preg_replace('/\s+/', ' ',$item['StatusDescription'])
//                        ]);
//                    }
//            }
//        }
//        return $response;
    }

    protected static function getLabelBg()
    {
        $data = 'iVBORw0KGgoAAAANSUhEUgAABJ0AAAZjCAMAAABlabQ1AAABjFBMVEX////u7u7d3d3MzMy7u7uqqqqZmZmIiIh3d3dmZmZVVVVERERERDMzMzOId3ciIiIRERF3d2ZmZlX//+4zMyIiIhEiERGqmZnd3cwRAAAzIiJmVVUREQCIiHdVVUTu7t3d7v/M7v/u//+73f93u+5mu+5Vqu6ZzO6q3e5Equ5Eqt1mqu6IzO5EMzOqu8wziLvM3e7/7u7/3d3/u7v/mZn/d3fuRFXuM0T/RFXuIjP/iIiq3f93Zmb/mar/VVX/d4iqzO7/qqrdqrvuRET/ZmZEmd3/3e53iKrMd5nud3f/VWaZmYjMu7vd/9277qqZ3YiI3WaI3XeZ3Xeq3Znu/+7M7sxVzDN3zFVVRERmzET/zMyqqpmZiIhmzDPM3btmuzPMd1XM7rtVuzO7ZjNmqjPdMzN3mTPuqqqq7pnuMzP/zN2IzGZ3zGbdzMzu/9277plVqkTd7szu3d0zIjNmVWa7qruId4hEM0RVRFWqmaoiESKZiJm7qqrMu8zu3e7dzN13Znf/7v//AAAAAAANajDbAAA/zElEQVR4XuzUMQ0AAAwDoHqf8Vlo0hdEkA3cCNIDOaEn7ATPvp0utXFEYRg+p/fpptPtBDuBOIkQIIxwFmEDUZx9cRJnX+6obzxgAoXEjIRAgJbv+ac6VMGPt74qpkZ0CwDuLy1AQgBIC5AQIC1AQgBIC5AQIC0AJARIC5AQANICJARIC5AQANICJARICwAJ8RC6J0gLAAkJqZQ2xlrrqsqHYyvxfyF476vKuTes0UpJgbVCWoCE7mSVTOVjTCmfSCkGXz2w5jWttTlm3elixXT6IynGN53RkqYHaQEgIRZSaedDyjH4B0YrKega3hJSGeuqsPrwUfLOvi0FXRfSAkBC72gXV1PO3hklBU9n7KRyPuYUV7yTTBNCWgBIiLVbK2u5MorpFijrUy4lB8t0JUgLAAkJqU1IOQarJdMtYmWcDzl6p8b8IqQFgISEDjHlaLUUTHeBlXY+p+idJMA6XQZISJkq5lAZSfdFWx9TcFrSEKQF5aJx50WChIR2Ia+/axTT/WJpfUreDv4hSAvKoHHnRYGERCole6ZZIcLjshYlnUNaUIaMOy8AJMS6Ws/eCJoprKxPj8J7iukE0oIybNx5ziEh1i4kbxXNIqnd+ylURiCtU1inwSkad55XSIiljTlYSTNPmJTTA8VIC+s08KGMO88nJMQ65uA0zYcPlPXJO/0B0sI61XxqOs8jJKR8ylYwzRN2KT8MjLQWF9YJCbF0ed1Jmj/SxhytRFoLCOuEhJTP0QqmOcYi5BQM0lokWCckZFNOguYf25i9YqS1GPBUHAkJ46NTtCCkT6mSy5kW1qmcmuCMdZr15+AV0wIR2qdgBdKaa3gbEwmxKckyLRod8uPASGtO4ZssSEjG7LWghaUepOR52dLCOo07zz4kpGKpBC04HdcqsQxpYZ3GnWcIEmq1NjZabWoiYvKGloALOUikNTOwTkhoc2u7c2x7a4fqyCo5ScuBlZ2VZ+RYJ6wTEnqy2z23vUHD5Ep5ZGiJqFCKR1r3DeuEhNo73SF7m3ROuJgV0/JhG3KwSOseYZ2Q0NPuJbt0xufsmJaUqLKXSOu+YJ2Q0E63xiadYOWjZVpi/GFeMUuSFtapHJuldUJCT7p19lpEZEL2TMuNlVutFNK6e1gnJLTTbfBRyJJeA+VTkoy07hTWCQl1ug0+rhSdAWmzNwJp3SGsExLqNvqELgBZ5VQhrVkAWKcdGgDsUlCMtO4GIKFWt9F2mwYB+xQs0rotgIR6vd7Au05NOht0GXAqXiKtKQMktP/s+cHh0dHRpwfP9k8P291Gu5tUA9j4FATSmiZAQvuH/XOHn/Xo2F632QuqBSw/j04grakBJPRFf9D11wlcLl4grakAJLR/0B9ysH/tdQK2IWlGWlMASKhf48veDZ6KA1elMNK6IUBCB/06X309Yp1aNAYIl6zEOt0EYJ2+6df79mbvO4GJqVJYp2sDrFPvsF/vu26jp3QVoFORWKfrAqzT9/0mP3Qb7NFVgapSYKzTNQDW6WW/0Y/dBht0dSCqZLFOkwOs0/N+o5863VpbNBGoUrIC6wRYp6n9Y9f/eWc6rxOAtDFJrNNkAOt01G/0qv6Vp02aGLDJlcA6TQCwTvv9Zkf71L60T50XtDT4HE2B+CWkX7FOUwJYJ6LOVd7DtPEiqufjIEejmDikYpoYxyGSRpPxzOrq+no6sx6DM4rphlhnL7FOUwJYp/bTzm73zPYLquXLBZlqcRkUmEbJZUhimpguQyyNZsso8cYDZVKusE5XAlin3qh1ekknWk+2Tgaqs72106Z6sVyQqI6MZYBnGqlcImhSHMsQT6OFMlr2mm6ETagU1qkBYJ1++/2PP//6+59/6US/2asenWq3Wq2NVrtNTf5j7+yWLKeNACzJPv5dxwrJDJlh2DzBXJCtgioKLlLhLkCo3eUMC7sk5J8kQJJX6BcPVUlK+LS7W5LtGe9Of1djnTmyxqw/pFZLuoIADDFyAiv1ehDpXmiwXQyPB4mLxizD9r4v1U4IRe306ePjD7j52LxL2+ndlGYFXjeIcoQJvjE8FWD6ZA0AphRvK+Nrs5DDZ22pdkr6V4WKUTnxW8bEfhUQqMK4uqaonWJ58uw45aPPaTs9MrFII7BTOVXyBt0Yb9L4wgPmueHoII7WLMT240PJT2qnKdLrT/8ObY54O4VLri61Uz4/+eR4yosv3ybt9JaJRZBIAROuCDlJllgadJK9MkIkZjHlCJ3aiYVSCQLZCWLFBtF2CtdcXWqnfB4fMb/9HWknE0vJB55LD2jox3JYQwkNQOrw0HmI5GCWU/VtYeJRO0GsnUJx+HHndlI7fXWc4/d/IOT0viHgp+yc4CZfGInsDotcyWgY3oRoSrMGh3OwaicEOVSj4kv4LHH8Tfmr+L4BpBq+rlTUTk+fHWf545/YGTsZ6yHgrZlw8DBhkOVks+0kB7h99ICyDX6tu8HDlMGsQ+UbxtZqJ/n1J+yExCF9VbZTqEisKxW105MjwZ9n95773BDwI7een6NHp70Jkhgh4HKjTmOk4q4hAJPgmEODzbWccujHVq5L7WSi7TQ/U7eCncxmdlI7Pb05UryTPWGHuykN24W5/nlagrcvM4XQQKBtIu10xuin8jChWnEDTW/NqqidzIRFdgpF29lJ7fTpiyPFX3516qa3E+RkWvqtrWCKTez1tGbMCkUXEBgnHZ+EBBt2iNqZ9aj9agd0qp1g3k6wkp1gCzupnW6OJF8Z82hyWlSKm4y7nKiAmTZrY+Q0TpfFNTlZRtXFxIjOIz+KdqqF4P+lNStiX1+xA6V2MiesZCezlZ3UTs+OJH81xjz64O13vv7eTF+/88Ejk0ThibW9Fib495I7YtNxXm+zJhFDd0waHgpxpQMgDa+Hrcfn6VVqVBxy7CRXSX9/gzk7tdPTI83j/y4I/tuj99//5aNfmEReJ7IdbYv2B5ApTyfYXLgcfm4iORmC2T6qFU6Y23N+XTvhbWjmG6d2kvKdou2EMMl2gvXzndROXx1p/m4wy8NOLUxJ7/W00w6YdwYha8ZNm9LFRfcHcR7QmrWx9Vin16rZmCbfTthHclj9dleyqJ3+sf5iTdfBhPEQ5xXc2UIFCNYy7rSEtooXpuTsAxQVX5uiGfvKyOhKlu3thAp2bSe1kzwWG4iNknyZ3g8bkPxeizPcgCxz8BGBJ3cltNdNA/bb4JrzSu1ELOW95ZEdLogdYKahdvqIjzvl00AA3pwPiI8u2nRII8kJ2jX+RnE+8ZWcZzUWQpoClGYrnB9Q5RoVZ4PlWVHxaDvBCUwz1E6ZvDiSPDELGGaM4nqY8E10gjeOr7OdZCl27XCvp43R7LU1mOdowc42lN1ZR1SvdrqDOTs44Zbn7DTfKR/rsT7cNUx4YOKo53ovPjFb/MGczoaIDlgnRZXQwG4ziq4vDULtdDfZmHAK3Qy10/qBp6cmnxp3ei9hwhs2Z/LPzogF2sSuXD1T9TmhuOKM3yGl8HENWXH9neFRO22/kgXpbhM7qZ2+pTtPZgEtslNBJIhLlFcQuJrtCw1podTBzkzj+YMcdgLHx9rhG7M59tB3hdoJcTurgDNT1tVOufzmSPDCLKBH6ZMeiFXBAuN8VufDpGiPRY5B4fY6ohMotA7MrTBCTvxJ7YR8srKdYHU7qZ2+fbYwKC47ZUSbzfm8CeSxmA1W+yJFlj9G0grFmI6zj63xEuDtsb8eRsHuaqfI3ecy7YS7Ymqnbbrf3x1nuTFLmMioN+WYu9GII3pcFaBRGU1FpTHIJ+md0XZydX9BDFY3xlZD4vYFaqcwug8/xn41NkbO1YVQO0Vgn//ziHnx8YopczVMGDP3PLkoKWu1CZN+lmimd5LXwJf/49C0A0zxyE3bYptLIV1M7QRCHjf31RXtBGonk4lr/Zc3WE7frZnQ22Vvvm3pqbn4IZXz5M3FZnW5Bx5sj6vHlA6U2gmnmefbCftlfTupndwwFjOnsvzLLARoxtzEhIKMbA3xEXq6mRU9QpVpzR1QnfvG6lnAKbxMclA7+b4w3/PxpPv07BOzkBJIhsJEU4y0WLpY4VWMGkFargcxjK0zd4JtxtG9inZS1E629+Hi6adPPrm5uXn87VOznJ8CQW9NApOgc8kIMHrSzzJJmjl26psPrblTKj8eXjU7KWqnYtjq37UFii57fPgad4/YSb8DF1YqMuw01M6au8U27hWzk6J2Ogx9YbahBJLKcMgL7AJetJO8Tq4RmuZAxPe1dsvXRVE7bTIHjrUCIzrzNyfq1LBL5zJSnXBDWyKhQaSxaqf1UNROtq3NFuABU9VPx3Y2Z4uo0bI3Iet0vOEOaI885vOsSL/aSVE7JYecSrMFuNNz6UydlVEwCOGqCnkH41CqN9NSbxAPITDWdVUdDuX3HNoRplyondZBUTuNgzXbUZ4cFmVhQsbo0Am7B/QxnbhrItuTsRPd83LtBtmYaidF7VRtu8i9Oo3mwIQqPdVJcqA384gRrwfsXRyXS956WPcwFrWTonayzdWmcsKHRbWAdhqQaMXB4M+Cv+DKRS0hxnSsnar4T6FTOy1GUTu18IXZlAEdu3KWvOzjCg2b2FFZKcfVnfQbYBl3yTHzUu20OxTtfsuHRdkOfsi5S4o6Dd0sZ5LwJjftuzmuIQCOTeSUhp+9VTvtHEXt1MyMp0qfNAxyI4iIFR4gkZKpwBeS/8ZC7bRzFLXTMPfGN0l7Y7aQyMwcpB0hkYppxSAODH2pdtovitoJL7KbRIkC1nB86CGRseDMkbkNSi8mkR5eoqQCtZOidqqJ5Kb4/KAKksH1vQHJXDFpW5UcYqvVTvtGUTt1hJ38pPzScalO6TTI2Bk4WpKlcJ6U2mn/KGqngQgw1TDhPSHotDAsbiGDisk2kPuJldpp3yhqp5FaWXsNEBVE9pDBpRx1kmn5HX8xrcadXiIUtZOnujSF56bZApDFCqNDGOg5P2JzUXRkzH5ROylqJwjARcVkMXnpBJX8UVkJWfiCjM3LXafBqZ12jaJ2suh1JyfjrPjG+4YFAtCR3Z6x4eiowVknrvQrUDfx1bcTdbANuiR7xGRFfDXiPhfc57HlMAPRIgH+5Dx0TbZdfhaoOEA9fJjh3tkJr9zn59lwUnkT3007I4NORfx2LzU1Qm3lhPb6/tgJmBLSAoF5OwnVwCmMD2Sx4XLRTpBhp4BgJ0YV8rNAxZxzE+xE/Iemi0g17nOVXW+mnIPkjR7YlbuM7CwRdGqlrVoICUnpWfYz6o9RO+EvyXoSqgGE4AP+A1weZyfItBPwduJcIT8L5p64ONNO7IN9aezUcP2JGoSkpwp1rjgIO7QoiYnBDcRKXkk9HQAnYrXTXBGhp3C1np2Aufu03Tu0EyTZCbji8GOinUjFk0X7txMv5BIm+A/5IYAAobK0jYI7Ik7GbobuepRvdV/sxGuEHFjhK+64cFyNGNzCN5HvTlXHyM/Egc0r2WlSGP8sGPdh69P1YADbKdwNFe3eTlg/12KqZc/Jq89LXmggaZ7/TZQSLu1cZ4vuAk4o1E5U/Jm8ZILkuBraH7gE30QSRbydIMNOJtJOWc8iwk7yFffH4pswRfuOitfCyMwOXGS8J/M15bT0B3LUSV6NUs2Lsi3/z6Fuu+uR6xirnWZFQemJ6bzk2Slc0XfPtJPZ3E5YT/KzuC07Ib2joj3bCfulJGUQoD/r0m42zu7564xEMS+zClIY7pmdgLgOnX3BI6SnwuViO7F3x/XJtd+WnQwSw1p2MkvtFH7ERbB3OxXnYjZ4ARNqatTXJEbgAaU64RbIRyOgNPAYeuRAtRNQHiE6MoSdYIGd2PQs/EmkneDO7AS5dgK5kRisH6yil8xOBy+PqmCCd/OpTueph7/AjLDK5Di+y9i7bnDmXtnJTKXB/19UfgPot3YdO83fE6TXlmzoZlHxAGAzLLIT3I6dAvu0Ux2RnzjMb2tpu+RzpcorZCcfGQ2SEpsKiMa/Ze6fnYC+TLUTEF0n6Y2Uo+LTQuJdku2EWyTDhZRlOxGPNCMqHpxxK3YK7NFOaF0I2b8KNKEjxJwhIK/JwxunjOl2apPDTgP+M9VOqEh8+JQLICXfCQXqWTsFkuwEmbniy+2EZRGV7xTY2E47zijAM3IFp7DAeTnT6zmk74eAUxKep9tpQG3kufqRNffOTqH0JGJLzMYhFeEiOmyet5JFtpNhqqML7sJO8rNIKF5gp3BBFwEYs0c7YTmw8SLeKxcZ53oeiKiTRD/TaAcR+L4ygXtmJ0BXoVi0k5xbbhbbyfB2YvsVUotkAJFlJ/lZCNLCn2xrp/kU0B0uYwFZCKGTZR8KE3ay5hpTSCt3ZcVB9GlTbflva2jUTiDZSd6sgO4YyCuNBTvh78XZCdco+22JneRngWoTfLmdnULJLu10HWmnAk6DTAcQ0pTkeoaTTU/sgmMaWiC58H4curow9wLCHMFOoSy+94LtlDgokoMokp3kFFO2RbKdsIFz7CQ/C1ybKKht4k6Bvdqpbn6IoWkm1M5Uk4KDiWRSi53cvjSRFDONrpp56rquyiK48x7bCcLFYjslh2zCB0vsFNjYTka2Ey7PsxPPdnZiv2x2hqJ2goTMpIBcjVyrvFme3NeRWoTZ1E7Y+FkqftXtpKidwmQdmZedbSe5GrlW2WKynfgWbW8n8s5qJ0WJCoiQr17kJpiCJ5bbCVWfZyez3E6cY3Fx8P4O7BS1zg72GRXHKGonpKdsO8FSOwG6ZuuTryDHTlxVdLH8LOSx7Tp2IhsBu7KTonbCu54B5Q9hi0neXWaJneTd52Rj4hal20ne8w4/IOLOWXaCrEFq6v5OsOd8J0XthLSA3zfeTlQ1+XYyeD9trjjUJbRIAPjEbXERivwsMIAg/s4kO+FbA1FEm9/sFEXtZGCKWcVORMtkUVDF29opozHL7YRKJcXn7yu+k3V2itoJ/8Tr4kQDSVGdTDulnhglDmoCyXaS7woTkp6F3F/LX2eXfybLy3Ngq6IHuZ4uN7uroziI4rtpzR3++fm1sEWEA//D3h3aAACAMBCk+y/NDhgSuPMkFe8p8KkcCSEtkBDSQkIgLSSEtJDQF0gLCSEtkBDSQkIgLSSEtJAQSAsJIS2QENJCQkgLJIS0kBBICwkhLSQE0kJCSAskhLSQEEgLCSWjo6zNldZ9zd695bata2EAXrxJvIDl/+QDtGPIDDqEPYA+7Cmcl9qNU2WnSWd+oLXoSLKZuGnjpvtA/4uyKBKEA+mDKdB284ep2lW7Veop9K/MqlPM9OLoq+1Hekl22dCrxG23jk6Se3pBbO7oX5RVJwGmXTVbpeFnfVp1UjH2dIkoq+lFSXjhBERkCuILJ3ktnYAGLfBjYTSdiWLZvuCHMOuuFb1lVp3a8DS0abVy+VM8rTppINIFovdAd0Gd1AbmJ3S6uahOmnXa4qw6G4w8Wbyj87kGEr19Vp2qLW+u06pTt9e/qFMCvl5QJw3Yn9Ap/gadCvjwXIro9Hw/LRPYMnh686w6nf+d23bronoNnladgJ/VyQYZ2Zf0MuD2oJekbPQfqlOPcjTJbTxq6BKd1ckUSGEsvWFWnc49TyJ6trVh1Vtl1SnUkcpouqROztMfqpP653gOlLvjnj+gE0B/SladTiHiuqkTvbJOq05OESnXqV/WaTeNvKBOkpfrtLu4TpyzOtFP6aSMpRrzjcjk3Gu6cFadnsGJmq2vrNOqk34fI5DAlc7gWCKKBQCyk+ez5DI2pGLuddziyhNHuZRzYth6n3HddU6RcbZSd9e/W4JldWvbUP4dOoWL6WTUiU6u7+14xmDTW6uIbEdkgiHqgmKdrkldlZID0xPq/5A2xVKAxJMZIOcBlALc6LHIISD3GQBdPKtOTyzh6ol26+vptOq0BQKRK8DIhwO894Zvidx9Bcr9SA90xqhTRgDngcZELspoTgEn31NAMbIlCQCCINQXTfcZ8CSxCYj3UuTNROadIbLxE5vYffCaatRtr5/RSVm3wEfb+ckvpqGTct1kZdc7NRsdqj8h7lyFdrfN6qDT7RzcLQZPinXqxXUyAFCIIOHZlSn45/AG06LXjD9X6hOKzI6hpdMNsiHKYI/VfuC5/OV1WnWacGq71W59vafiq04ZH5mCDRYruw5eETm5QXp45BhGnW6QvAfKF757NzHELaJa6vTe0ORV0ixN0XYAwH2J9sKaE50GOqTHe83+KbnHizDhmMGdaHKTT3Ti09URgEwEbqpPXMCRn+mUsqUwVBuI1NcEoPTVbENFVHAZALa1Gci26vQ3PabHmGjmOt0N3DjXyTPvjihUnXYJu5AGxBOdtOkANdZVpx4fmMwMsE4ISkpDF82q09M4tddtDZ1+EadVp2K43EPPdco3zIjnukdJYy91BUYrgtEiO4pgWBnrEm6/dk7RTnTSQOpcAT6ITjaB805mQc5AMUc6dSgGYwIZtqHQDLpPXOyHY53CzDoFGFGlasRF+TrXqaBTGLPXIoykDrdGXp0XZJjOHca816JTv8ApFyDMdFIRwbkAohCA6EflrxEwBD3pxC6pjsVSNzOdpudOotPEkBWd0iDuBli6aFadap55INVufb294qtOmeZXO1BVIo7aj7167io6iRCCmGSQs1FG0o5hMChcOiAzO3U2MEU8+D7zKcpl0kneLpmMXLZEfZFbMDqR4GosrqpOH6ZRHSOJekC5NlboY1i9VntgptMWKXlNNqGwBvELm+LFTY/gZispS6QHfmkJxRzpBEQlLE06dcB0fmNFMRk06XSgBbgnFSed/jnRqQdI4pmyVPtew9GFs+okaepETZ3oN+i06pTh+77rnLtCGWHoDzpJbw/uLSnIaqbTB9YpoCOOgNYJFsvHJQHlVCdU04Y77sETs3MqobR1knk32FedRsuyeKCTHPZLnVAq0P5xvClpPAJFNKhr0ipcpbNf6qQwOD6mmU59U6eiFjolKQnoWCdddXInOgWUR7r//s06rTo98fS7uYhr6HQRn1adMIV1cged9gdFHNWoAe8V0feDTnHUSWXoCk7VKarKDkmEqlOdcnUjcPchzLsPbZ0ksj4ykBMdkNiBK83lXKcMxGpD/k416iZrbqovcfJXJemuCsJSJ4108OvhjE6BFjrVkoDwnE5ykiRf0f9enVad2k+/Zz3arZd+Kr7qlNJVzttSeIWFLwJRRlropEIGwCyohU6m1IpM1clLVc7oJCopwEl3Rk3S1+5Vpx0t41EMT6erb4XvYaFE56VOHRdFOktisaJTN4dHxvo6LqqFTt3hTzvTyQHmVKduqZMnyYAPP6zT7W9d2a06tZ8vLXOulX6Jp1WnbVOnAUoprY2xlhcrtqmTicCxTop1sqIQTwA13cfqUS3trnNLJ+mHwcqE6Yd16nmMYCjMzLeIp4VORYpQEayFk2FKJrulGltqr4C80ElEF4I/Mh9jrffIvZ50qrC0dSqIc502rZXdMG13fyOd1k+y/LpOUv5iVp20MGFoyhM6dUJZaujkNo86Jeix6+1cJ1cADGjp1HGBIjPm/WemLIDzhE7Khgxg4050OlT0sNApT0LLkSM6JTois8djBk12plMGSQyC2GqrsRi6+fKwg5tvCr3DA0kAP9cJC51kJXnw3AP0+3VadaK2TjXt1kvptOokSzbE8zplsCsJ21Odhked9vg2DujmOhUge+d/VCeb8axOEUBTJ32oKLV0qha5fKoT8o/oNDR1ojAAw5M6SfflU3Hb1mk3ciTzOWD/JjqtOsnf57cRSC6p06pTUlL253RSQGzqlBcruwx1opPsIbdix7ap019Vp29jD2DobFcqZsc6aQAlpoZO5rxOrEAJnfXP6LSPcRe89706+95J6gw8q1NUUmOsH4D/MOSik17oRA6boEfWi307ndbvd6IzWzBXnV73ElKKMYkLnTKwHVsMBq/HPupJnQwQZEg+0cm8x7fpqfixThafiM7odDfTCbjSrEhbpz2yJep/Vqckq9luqdN2rtM0dqFTgjrS6cv0tE0/p1Ndyn1G4Ql4OHnRSWVYpaZdEz3mu01jeoOvN1+/G7Pp0Jh266rT615Czms+FOCw+CoxxK1/UicN3IgDVSc96aTyQYOuoZPHl5lO75s6yXB+Kq55gid1ckhM6MtXdgqIZBikY522w3equQVsUycRXRqrTv/MTj2nk3CkEmNsC4pSxg+i0/eIkpKb9sPHAYCvQmp6k6w6zY7nPxDcBm7V6fViQy6x16NOQ0sn8oBROgBZdHg36UQGgxrL/wDlRKdU9AQXlXM6eUHgrqXTxGKP4UQnKiBJWOqkSIago4SsGaGFTtfiB9W5mjoJmyJyOJi9eAoP+KVO0myRCmIsgD3MAKCYMkg3W7ZB09tn1WnJkzQeNbdb192Yv+0SUkbJ8SEFosd3CSoBBagfxHBD8Z076EQfEDXRXwXoTnSKuGNWflCnXbHEEzR1+pygntTp03M7CmRNl7Hn8X6hk0FRJFF75LZOqmLqykwnfS8PnoTI0tmGToPrMaaQxIRt8pqcVfQHZtVJpPlVnWjV6aJRSpjSHb+nMglArBsLdB4L1ul+LIGr3m2BqE506uHlnhad3p/RqS6grjHTaUAgia46hZZOftoFOtcJf3MxICvKyJrXUwudCPBUE4C2TgRkS2S3c51uc7B9rvACKGqmkxediiUHlDCJqTX9wVl1OvN1va2xb43Tegn5azNSxTwYAFGpWFdKBZxsaKaTZp0M38ZA1Wl7RifPmOh9WyeVBJ7S0skJPeSx1CnVPh+5EjGOdSpfJpDETKeOdNoC6BPgsXvUKYJzU0cCc52cVxP166W1ZtXpjaLuut7Uwngrbdu6gROA18OP6aSA7BNyWyfqkZ2yu4ZOwlLsXIGDnetUUt+FgihUovN56Jc6dQCGssHAn8EDp9gjnagrAD5pyg/8Ir0eD53vDdUo/W9CaNVpzXoJKc8KRJag5DM6EcYMXW7rpDYY0w9o6KTAiXqh09aDw5PspIdBN+l09LVPGpyojnWib30w9H+fVac16yWku05zEYMcQ+DNAgXIjpJ8g8qxTuTkfU1Bd6oTZQDYGwU30ykLWgKWTgCSUWXUKedANTaWbQ7Sx4UYO71eWmvWrJfQ0W8j3PW9Pvy+m7Vju/vKheRmKMHq3lsi5R8qWU44MqGg/JfIL/c7dTGn/tHE8PWz/HwBaT0zyBj9P3bq2AQAAIZhWP6/unPmUugg3WAc3AkJHR8udadfpIWE2N9JWiAhd5IWEnInpIWE3ElaSAh3khYSQlogIaSFhEBaSAhpISGQFhJCWiAhpIWEkBZICGkhIZAWEkJaSAikhYSQFkgIaSEhGHbqmAYAAIYB0LzPeEX0agIi+BrcNuyEnUBPyAk7QdiBYwIAAACEQfZPbYtdsBoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDVPCjBKBgFo2D0TpZmADt1cAMABABAzO4WN4OHBNcO0XEKgJ0AO70MsBOAnQA7AdgJsFMHYCcAOwF2ArATYKcOwE4AdgLsBGAnwE4dgJ0A7ATYCcBOgJ06ADsB2AmwE4CdADt1AHYCsBNgJwA7AXbqAOwEYCfATgB2AuzUAdgJwE6AnQDsBNipA7ATgJ0AOwHYCbBTB2AnADsBdgKwE2CnDsBOAHYC7ARgJ8BOHYCdAOwE2AnAToCdOgA7AdgJsBOAnQA7dQB2ArATYCcAOwF26gDsBGAnwE4AdgLs1AHYCcBOgJ0A7ATYqQOwE4CdADsB2AmwUwdgJwA7AXYCsBNgpw7ATgB2AuwEYCfATh2AnQDsBNgJwE6AnToAOwHYCbATgJ0AO3UAdgKwE2AnADsBduoA7ARgJ8BOAHYC7NQB2AnAToCdAOwE2KkDsBOAnQA7AdgJsFMHYCcAOwF2ArATYKcOwE4AdgLsBGAnwE4dgJ0A7ATYCcBOgJ06ADsB2AmwE4CdADt1AHYCsBNgJwA7AXbqAOwEYCfATgB2AuzUAdgJwE6AnQDsBNipA7ATgJ0AOwHYCbBTB2AnADsBdgKwE2CnDsBOAHYC7ARgJ8BOHYCdAOwE2AnAToCdOgA7AdgJsBOAnQA7dQB2ArATYCcAOwF26gDsBGAnwE4AdgLs1AHYCcBOgJ0A7ATYqQOwE4CdADsB2AmwUwdgJwA7AXYCsBNgpw7ATgB2AuwEYCfATgWAnQDsBNgJwE7ALjsB2AmwUwdgJwA7AXYCsBNgpw7ATgB2AuwEYCfATh2AnQDsBNgJwE6AnToAOwHYCbATgJ0AO3UAdgKwE2AnADsBduoA7ARgJ8BOAHYC7NQB2AnAToCdAOwE2KkDsBOAnQA7AdgJsFMHYCcAOwF2ArATYKcOwE4AdgLsBGAnwE4dgJ0A7ATYCcBOgJ06ADsB2AmwE4CdADt1AHYCsBNgJwA7AXbqAOwEYCfATgB2AuzUAdgJwE6AnQDsBNipA7ATgJ0AOwHYCbBTB2AnADsBdgKwE2CnDsBOAHYC7ARgJ8BOHYCdAOwE2AnAToCdOgA7AdgJsBOAnQA7dQB2ArATYCcAOwF26gDsBGAnwE4AdgLs1AHYCcBOgJ0A7ATYqQOwE4CdADsB2AmwUwdgJwA7AXYCsBNgpw7ATgB2AuwEYCfATh2AnQDsBNgJwE6AnToAOwHYCbATgJ0AO3UAdgKwE2AnADsBduoA7ARgJ8BOAHYC7NQB2AnAToCdAOwE2KkDsBOAnQA7AdgJsFMHYCcAOwF2ArATYKcOwE4AdgLsBGAnwE4dgJ0A7ATYCcBOgJ06ADsB2AmwE4CdADt1AHYCsBNgJwA7AXbqAOwEYCfATgB2AuzUAdgJwE6AnQDsBNipA7ATgJ0AOwHYCbBTB2AnADsBdgKwE2CnDsBOAHYC7ARgJ8BOHYCdAOwE2AnAToCdPgfYCcBOgJ0A7ATXw06AnToAOwHYCbATgJ0AO3UAdgKwE2AnADsBduoA7ARgJ8BOAHYC7NQB2AnAToCdAOwE2KkDsBOAnQA7AdgJsFMHYCcAOwF2ArATYKcOwE4AdgLsBGAnwE4dgJ0A7ATYCcBOgJ06ADsB2AmwE4CdADt1AHYCsBNgJwA7AXbqAOwEYCfATgB2AuzUAdgJwE6AnQDsBNipA7ATgJ0AOwHYCbBTB2AnADsBdgKwE2CnDsBOAHYC7ARgJ8BOHYCdAOwE2AnAToCdOgA7AdgJsBOAnQA7dQB2ArATYCcAOwF26gDsBGAnwE4AdgLs1AHYCcBOgJ0A7ATYqQOwE4CdADsB2AmwUwdgJwA7AXYCsBNgpw7ATgB2AuwEYCfATh2AnQDsBNgJwE6AnToAOwHYCbATgJ0AO3UAdgKwE2AnADsBduoA7ARgJ8BOAHYC7NQB2AnAToCdAOwE2KkDsBOAnQA7AdgJsFMLYCeAxd6d7khqQwEYve9+XzyKoow17fLS5SowcM4/zOKRov5EEZav1ymHZrb762ABqJM6AeqkToA69QDqpE6AOgHqNG5Vf814930B6gSokzoB6qROgDoB6qROgDrl9QsGqFMBqJM6AeoEqJM6AeqkTqBO6gSoU0+1O6BO6gSokzoB6gSokzoB7ihYBaiTOgHqBKiTOgHqBKiTOgHqpE6AOgHqpE6AOu0PUCdAndQJUCdAnby5F1CnruFGN6sToE6AOqkToE5xYYA6AeoEoE6AOgGo07kAdQJQJ0CdANQJUCcAdToVoE4A6gSoE4A6Aer0PQDxAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkAMfOfTM5N21AahT5e0Dq9MCICcsH/fXG6kTkN/JU6rTGiCnLB51brM96wSoU6pTAeScB9cJUKc8rE6AOkVFnWYAWXTTcUidUp2AmQTsWSdAneLwOuXi7IA6Ndas1ikXwgmoU/+Mp/bpOtV7AerU7c96nXKqjTUnV6BO+dJhdcqXPAcD6pQNi3Uq3mmjO81BnbJpsU7FbBvdywnud5rLw1KdYqpO+Zq7pUCdsmetTsU707tbCo+gPfNJlvbY6IL2/OQTdapb9Lw6QfdaxnPf7zQI1lKdohO+yWDmI+qEMo1W38RqnCKOq9PrDj2qTkjT3FaPqdPgL/4zdar3yqIbnMfUCW1a2PTuX4yarMN6nXJcJyFCmm7ep6k4DaLzqTpFv05ubUKbHtWnhR92H61Ttd9cnerdQJvKjo+57pRt6jQGK415YJ9yxgF1qkZ6dQp1QptOy5M6pToViNPT+jT/EqXpkq3WKdSpgMzjArd/nYrz6lSoE06c1mXerU5xSp1CnQrEaacjqVOoE3z8jCfztm9QmY7AXnUCcVo/nDrVo+53Qpwunid1SnVCnIYy71mnauCbdYpGnUKdEKcFmbes0wFPAdfj3lGAON0+T+t1yt3qlN7vhDgtHPjOdcqPv9+pXuHdmIjTI/K0Xqd6pB77bp2iN6ZOiNNY5hPqlNXIWp26eWqNPu2LUahT/mV2u3xAnSJ74pt18rVNxGm9TpH5yDrFcp3G/w0eXSfEab1OkXnXOsVRdYrJOj31Fk3USZ3m+xBH1CmyKW5bJ8RpvU6b5umQ05fle8Ur3fLV4tZ1QpzW67R/ntYDkS/EAXXqz3+DOkGGOi3KH06f/xYgD6hTmeYBgTp9/ruAjA3rBJAH1GkhT4A4PaZOgDot5AkQp4fXCVCnyJgEkHFknTKmAOShdYqMLQHqlDEDIGPDOgHkwXWKjC0B6pQxBpChToA6Tc4IkOoEqNP0lAAZ96kToE7zGnNeEaBOALllnQDUCVCn+UkBMnasE4A6AepUZACo0zxAnQDU6WyAOgHqFE3qBKjToQB1AlCnAlCnahSgFwp1AtRpR4A6BYA6/c7PqpbFarlSrV/YCMiYoE6pTqBOJzu+TsIFvme3XqdUJ/At4C0uPOXrOsUt6wTqFFvbpE7A4XXKS9YpF+JxozqBOqkTkHl0neK6darvfmqMvDpcWZP/aVxzFyhQp2GefsZiEJO6TlnUdXp1+MKpFBxcp7xcnerlRoeb/aoPU1Mn+G0u1CnKctWhfk7ySnUCdYrL1Sle1SlrS3VKdarAoXXKjO0169ToRqtOWVZUdarXVGlUJ8g8sk6xv1KOdp2qpDQCVi1UK9SpDQ6sU163TmW5zka3TtGvU6hTE2QeV6fYX3XO9Ead2qFp71wvAIfVKa9Zp2jUqfhOnYDMo+oUV6pTdWfBUp3ijToBB9UpL1Wn1n1PB9YJyFSnovUEyz51Anlar9NSnNQJyDxwjv11YlSlpogvXxUHeVKn2LJOIE/iNK7T/P1OuXudQJ3iinVq1Ooj94rHDveKgzzlpesU3eWZp4AX6wTyJE7jGs2+QSUrk3WquwfyJE6TdZp/+5w6rYNMcZqv0/qbe6OxS7tOIE/qFL1vrSx89WChTiBP4jQr/xhvUno3kEcVCeQpM4NSp5OBPInTfjcHgDyJU/vy0nlAnsSp8ypx4OygiFPkC7EFkKcfB5KnAHaoir/GveIETp/EqdisTSBP5QAcHibQp/3jBMjT/m0C9Ck3jBPgmslhaQL0adc2AfqUV/1JBwhUbtcmQKFSmoBdP+B9OoAvPzwGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQO4IIHJHAPEsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQ/3oxFifKH+b/vflD7/jdKbNaN7lBGWuI7wF1ysxWUbrDswXJ2rBO3T3n/1HzoWnuelqdQJ0ae/RDtFinfLNO7ZCO65RTdcrKPnUCdcocd2ixTvl2nfK9OlUHnU/XOXUCdRpetJkaLkYz/LZOdQdb8etPWR90/CMxz7uMB+rUPO8o61rDZfx3xRylor32VZ5yWKeyVadOvQkPrxOoU/vqUnW48fB8MX9dp+LdOsV7dYoz6gTqlHmROo0PVobX6xSb1AnUaXzJpjF8ep3K8tfqFFeuE6hTHFSnMnbbOoE6FTeoUxlertNkbOMEoE71Ymu4jOd7/yNsvU75hTrlXeoE6lTkaIZSgGPrlJN1+t+n6wRkw1Sd5q86l+XRROtPssQPjd27h33rXvEr1wnUaWamrMUBdfrIkywXrxOoU5k6dzl3qi28o+CidQJ16l9TzlcW69Qq0vobVOotjq8TuCq+XqfmzsNHco+uUxXW+cx/u06gTvnHZ+rUnX78SO76HQX9U7J+jse2qhOoUzRWf7JO8aE65QsLdWoHatM6gTq1hu9fpzi4TuC608l1iu/VKVfq1JhuXCdAnbKz9ht1iuvVCTwFXEbV6fJAncrIO/2JHNYpF+uUl/llB+rUGc7p+53mAzTuTy8l63XKPeoE6rT25t4yOl+nrBqTcwHK/F6dWjncrE6gTvm6FDmzRTH52G29OLFntuqUMwWq1dOXxa3qBL5n1/hT7QZkvk7dte8+oRcLdWqvjD3qBOo0/y3glfc7FQvl+nydYuc6gTpFFsPhYZwia931c+Fq1ykX6rT5G1SARm1GdYl3ZbFD4Yt/2IFjAgAAAIRB9k9tiZ2wDgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABw9u4YBWIYBoCg3q6XX+k2KXIseOYD6RYJHHv/Yd4C2KOWKUCdJApQp6eBAtRJoAB1etgngN1inwBWngB1etwngJUnQJ0e9wlgk3kC2GSeADaZJ4CVJ0Cd/pInQJ3kCVAnQJ3kCVAneQLUCVAneQLUCVAneQLUCWBTBkCdAHWSJ0CdAHWSJ0CdEgB1AvimGcE8Aep01OoEqNMRqxOgTkegToA69fMEqNPRrBOgTtOsE6BO06wToE5zUZ0AdQLUSZ4AdQJQJ0CdRp0AdfoAoE5RgDoBqBOgTqNOgDqFAerUB6gToE7qBKgToE7qBKgTgDoB6jTiBKgToE4WO0CdAgB1CgHUaQDUCVAnix2gToA6BRY7QJ3uHZ0AdZrvAerUH50AddpInQB16scJUKcN1glQpw3GCVCnDcYJUKfdWp0Addp35gDYjMkD1AmgGSeAZpwAmnECaMYJoBkngGacAJpxAmi2CaAZJ4BmmwCacQJotgmg2SaAZpsAmnECsNgB6iRQgBMFgDrpE+BPFkCd9AlwgwqA2+cAdZInwKsHgDrJE6BONYC3gAG2YvoAdQL4Jh25PAHqdLTqBKjT0aoToE5Hq06AOh3NPAHqNM06Aeo0zToB6jTqBKgToE6BjwDq1ByeAHW6fHgC1AlAnQB1GnUC1OkjgDoBqBOgTvIEqFMfoE6AOqkToE6AOqkToE4A6lQAqBOgTuoEqBOgTuoEqBOAOgHqpE6AOvUB6gSokzoB6gSokzoB6gSgTgmAOgHqpE6AOgHqpE6AOgGoE6BO8gSoUx+gToA6qROgToA6qROgTgDq9AqgTgDqBKhTIE+AOhmeAHUC1Oni1Q5Qpx0AdQLUqZonQJ2ieQLUKdknQJ1ieQLUyfAEqBOgTu9dVCdAnQB1cqgAUKcfO3BAAwAAgDDI/qnt8cFyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4Ozbwa6bMBCGUT/7PHlbZWHlRxNfIE1pOGdlGNnq6pO4deqP7avP/gNO7PxqoE6lTlcD1EO++kiU1AlQp/8UqFOp0zWBOtV16nR9gDoB6lRNneZssy+W6wr2dep3JECdcjyHNV/GvKbmmHyMs2u7oTv5++sF6jQ2HZirzE8rxt2evk7djhvXCdTpKU8vU1O9H27p6vSyROqUQJ3iQvk2RO3vYfpjMoEZp9zx7E51AnXKOGRyskNdbPo4xToWc9KvY3qLOoE6ZTiiOE2ycjl14Wrq1P3hKwZN4O4F1KnaOo1FnfJTcF2naoK0PHl8KyAyEnWKKuyrU397KutU1Ry7PvkGQJ0iQBV21KliEnbXqdqTx/cDdRpn6xRn7qvTonTdw/hWQGahjtcpjtxVp6xk9LM9eSwA6hSn7KvT6i9VsfzmOgH9h9NTh8K6TrVqyOI2Zl+nukedgP4z7HydxvvqlCerE6jTgftOuas5pllkp5r5feoE6hSPTYjWddomJHc0dWrzlC/vVidQp8xTjOqHdcrT8pjddcp36gTqNNfz3bpOqTkm6/Twakelm9UJ1GlEQqZxrE75NiuYB26PGeoUQJ1GTeNwnfKYdZ1Gd4Q6AZmntx8Tz/2Ofw4AAAAAAAAAAAAAAAAAAAAAAAAAAADqt7+9A0CdAHXqpyoGqNOFAOoEoE6AOgHiUsvh9nU8xI5TdQKoKWvSD+NtzHOUxhJATdGaZhgWO47VCaBrTT/cs+NgnQCiMHP1ahjz2BErNwrOAn8Pzzr1w6rY3e2ovXUCyFxknfphvs0d76wTQNYprOuUs3GuTgD9l1g/7Os03lcngOZ+QAwP1KkO1glgfXvpcnUC3Hc6Vadxvk6AOGWUcujL7pOAaq80xfCjdQKo9kpTDj/5f3YA/f2kxfDS950AdXr/L1kAoiYVNYnh7jod+hUwQFan6nndD/s6zWNG1Zk6AdTWU3PSqk551FOy5moFoKYRAVoMu/uW0zheJ4DKdkRrYrijd/0H5DgC4O0BAVAnQJ0A1AmgHsYagDoBXDNOAF+WJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPjF3rksN67DQBT/jh+/i7vglDvwCQmKQ4+7Vx4MXgVFJ3EVROWrXv4rvkmew6aLz8Esy0pVCp2+cxTX0cl0skyn/Eo6fQClTSfLdMovpNM5SmM608myFEHfSyf5NncDnUwny3QSQ34nnXQMIN+6B+lkmU5hOomhI9PJskynPjI+kk6mk2U65f+SeLGjcxSCOCrMVaAwWKQXqCtdamLuSZL3AjgNJ9csYj5JJ8t0yqxuNLUnOvN2A1jLxSSp0qLTqCV1XxyCu9QhiPdIGFFGqThArXqV9eJLcskCyQ/IMp2ywMEQ+Kp5aKIGFS6KsAiaRYXaqvY5OtUFQsQBap2j00sAkfgcnSzTKYf4XkV0SE2uwTjo00kDlBqtdhbpxKPjgGHt0glIfJhOljcKchiVBvKx8BXzEMYVJahGLixjMp3+dJe63GW8NPyegjg6DtBRTtJpCHJL8odkec9HPhX0Kpw5Rd3JG1DVhZUB2XsCmJtJrQvjATpJQh4dB2j3C3Sqc1fJD8jykyzDBOY/P3CKIY4b1qJw4dx5Bhia0UHxeJhOaufRcYCYp+mEuTX5E7JMJ1gtaNMpgE7Coc104lFwM3GKTjw6DlBrg06Su0h+QJZPUBk2+PVdYIFTsE2zqFGdm9NgOlHddTrNjI4DunSCZg7RyTKdih89xkgWq3qYAuKgf6BTZyIyh1ijU+Y0nWZGxwHDZY1O0Mwww+wta++eT/4kuJnVSVVTgQijhWcowchr00lSdejEo+MAHccanUS30skyndg4c4sBb8R3O53Us0enHDKdTsoynSresKdWreOATQ/RKZBOQXTKNJ0OyzKd1A5faJBOdRzvHgKdQLVz528n7dJ0ekqW6RQqXXKReEmB0jgo/Bidok2nYe/SKUAYsJNOUP8gnSzTSRlR3VuQAsRgFOtxOuUknXZtFIAwYP9GASc/LMt0Ukbw7+IiBbEupYYYt9IpxdSgk3g36ESj44C9dLpgG9MynRA42XNmOukClnjuo1PO0Cmn6JRApyohj+63Abn8gAs9JVMlPyXLdMo+nXKaTnGITnxWg/icoROPjgMYQ0wnyF1keVaW6QTncFTno3AKqbZyRkHmPjopKhKaKeik44GhQEIaHQcUxcgM6LvijALLdBoSa+QQOOO2E8dJYaDE1rdtpog3CqLIlyk+RUIcHQdA99ID0OmC0+cs04l2wlVweCLcYRhXF+7SKYBO4hNAJ1xiZdz9dnQcwHRKoBM185foZJlOAedUy9mu/AoAFcclFFZKdPBEcwA6FV0OO+JubnQcoMXUXNGJm5Esp2RZP4Jl2Kbe2hGF6MceCrf1BgJaGKryeGAmNP2VAN0d5+ScG9Ocl2UZ134h5iWyLPPo8tvbdLIs0ylNpytlWd6+MJ1YlgXvX1pIpVvA36mErYBG1o+nE7y/xrJMp0eVoO+kE29fiCzj6SydTKf4WjqlyHSyJo//MJ16AjZ9Ip24if5TP157sEynxwVrod9KJ9mlN51YxpNa+hlNJ+HTvwXePtl4KqaT6aSGfkbTiahvOoXpZJlOh8QHDJtOYTrNyHiif2c26LSSiaLgUVLKwakTgNOiE9fJBCfuvxaEUb6fJw95IBZ6pEYzoUsRdH+dTKeaKHA+NG0UaKaFY+XAWu7QSGU180vPhYdNOmkdbSsCz4vi/mGkE+2kOMpkivJMp/q0mReHolHoUr0jqPurZTopPzp00kxMJw2qrQlWKEy5I/fSibvlsza5/xC1h6d0yoSumicWanLunukU3P11Mp6UA0O5TqelI3kBLZBq3qz+PToBnLitevY5BHlhpDvpxOWjsAs1Otd4lU6S5zKZTtX5/Q06VZn4p1aCCmvxUerW7fCJ/jvppHXUDLPHBNJuc3j4+vFflQ8ClzZZXzXuHn7/UvfXy3QSUizTCUEknc2+akUxoClqsxZU90U61SUjeUrioa485gI+VWtgl3xUns8QBd5W/UgHE3Ti7s/LYggAhpBOHMJwCn63mtAJ3/rGZrB3zyjgOuojHjwa7RqGB+OA+x7Kw1dL4E3dD1x6aAy7v0WmE12v6NJpfukFOPQwnWIvnXiw40OfTjpmfBV8n06aiCbDsIyjdLpoXcri6yVey3RaeJxBzAA6NRbObNb2N9Ap3wwW6AS8r/PS8MQIU1I6UXkezgSdAB9zdOLub5XplHvoVGWq4cR7d8CFRTrlITrlFjrxaMADwiboBHl4QEKnWKNT5jKd8mY6mU64TtCik4rhFJU3EGGZTiKgE79iV0HLdZhO9WhU2tQzdOLyqjadJFWLTqq4RcbTX6UT4kZ5o77X0Umdt9BJklxOJwZUj05S03Rqy3TiLwBACnG8mE6xkU6a5VPpFEinIDplfg2dTKc4Syd+ZFWr03fAA3QSnaWTjuZaOlED3b+dZAb/Jp2MJ175iHU6vWNPgJhOI02TTiHaSieuw3SSGBjzSTqFCBto02nYu3SKT5HpFC06iakBpzekE2uDTvEMndS0j040Zl7H2Een2EinnKTTro2Cu2U6vV+f2UOn6hERxiDcYPv3nZ6jU2sbk9cqtUyJ9gf3nSaOsZ+nk3r36XQjrKzyqCBaO0A68bLuHJ20vHg26CRp2nTiOlvplEiChLA1OnH50YCaoB2k0+xGe050f69Mp+zSCTPN0SmeplMeolM+RKfsPWeXDTpBebHSD9B5OuXNdDKd4Il2sTKd6kxJdFo6o2B1o0ByP0UnrsN0GkE8Zr6iauQpDSNl5zdGJVxh3ANRZ7VzQo24RFZBpyFFUmbO0Yn3cwLrq/XnlnKZTtpPn044jkk68Wje0oHDmE6Z4qJ5VJPnfgKd4NJniM9w4e7vkukE+9dqZTrVmZhOXH+kUk3TSdvcT6fkOkwnHk3/5F5muLhgefXhM9PXttSGnXHH3VtXvuGVD68OptPEkxdSnuurKZbppG3upxPVYTrBaIgOHLZMJy7PJ6jUPtoPXfraJwK7v1+WsEGNrH4Qv/c74eTV59o8WIdHw3kXw9R3eXwTCIsovejSD6O0xZ39194dEwEAwCAQq3/VVcD+QyKCBa69AaDWngE0j1QCNGeRAMFZJED3sRxAdBYJEA0ngHo2AZo7Hp5QL7sOnKAiAAAAAElFTkSuQmCC';

        $data = base64_decode($data);

        return $data;
    }












}