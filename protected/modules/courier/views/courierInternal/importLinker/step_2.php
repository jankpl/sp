<?php
/* @var $package Courier_CourierTypeInternal */
?>
<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'default-data2',
//    'htmlOptions' => ['data-edit-form-id' => $id],
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));
    ?>

    <div id="linker-form">
        <div style="float: left; width: 48%;">
            <h4><?= Yii::t('courier', 'Pickup');?></h4>
            <div class="row">
                <?php echo $form->labelEx($linkerImport, 'with_pickup'); ?>
                <?php echo $form->dropDownList($linkerImport, 'with_pickup', CourierTypeInternal::getWithPickupList(true,true)); ?>
                <?php echo $form->error($linkerImport, 'with_pickup'); ?>
            </div><!-- row -->

            <h4><?= Yii::t('courier_linker', 'Domyślne dane paczki');?></h4>
            <div class="row">
                <?php echo $form->labelEx($package, 'package_weight'); ?>
                <?php echo $form->textField($package, 'package_weight', array('maxlength' => 45)); ?> <?php echo Courier::getWeightUnit();?>
                <?php echo $form->error($package, 'package_weight'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'package_size_l'); ?>
                <?php echo $form->textField($package, 'package_size_l', array('maxlength' => 45)); ?> <?php echo Courier::getDimensionUnit();?>
                <?php echo $form->error($package, 'package_size_l'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'package_size_w'); ?>
                <?php echo $form->textField($package, 'package_size_w', array('maxlength' => 45)); ?> <?php echo Courier::getDimensionUnit();?>
                <?php echo $form->error($package, 'package_size_w'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'package_size_d'); ?>
                <?php echo $form->textField($package, 'package_size_d', array('maxlength' => 45)); ?> <?php echo Courier::getDimensionUnit();?>
                <?php echo $form->error($package, 'package_size_d'); ?>
            </div><!-- row -->
            <div class="row">
                <?php echo $form->labelEx($package, 'package_value'); ?>
                <?php echo $form->textField($package, 'package_value', array('maxlength' => 45)); ?> <?php echo $form->dropDownList($package, 'value_currency', Courier::getCurrencyList()); ?>
                <?php echo $form->error($package, 'package_value'); ?>
            </div><!-- row -->

            <h4><?= Yii::t('courier_linker', 'Domyślne dane odbiorcy');?></h4>
            <div class="row">
                <?php echo $form->label($receiverAddressData, '[receiver]tel'); ?>
                <?php echo $form->textField($receiverAddressData, '[receiver]tel', array('maxlength' => 45)); ?>
                <?php echo $form->error($receiverAddressData, '[receiver]tel'); ?>
            </div><!-- row -->
            <h4><?= Yii::t('courier_linker', 'Domyślne dodatki');?></h4>
            <div class="row">
                <?php echo CHtml::dropDownList('_additions', $_additions, CHtml::listData(CourierAdditionList::model()->with('courierAdditionListTr')->findAllByAttributes(['stat' => 1]), 'id', 'courierAdditionListTr.title'), array('multiple' => true, 'style' => 'width: 90%; margin: 0 15px;')); ?>
            </div><!-- row -->
        </div>
        <div style="float: right; width: 48%;">
            <h4><?= Yii::t('courier_linker', 'Dane nadawcy paczek');?></h4>
            <?php
            $this->renderPartial('../courier/_addressData/_form',
                array(
                    'model' => $senderAddressData,
                    'form' => $form,
                    'noEmail' => false,
                    'type' => 'sender',
                    'i' => 'sender',
                    'noErrorSummery' => true,
                )
            );
            ?>
        </div>
    </div>

    <div class="navigate" style="clear: both;">
        <input type="submit" value="<?= Yii::t('courier_linker', 'Dalej');?>" name="save-default-data" class="btn btn-primary"/>
    </div>
    <?php $this->endWidget(); ?>
    <div style="text-align: center;">
        <a class="btn btn-xs" href="<?= Yii::app()->createUrl('/courier/courierInternal/linker');?>"><?= Yii::t('courier_linker','zacznij od nowa');?></a>
    </div>
</div>



