<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php
$c = Yii::app()->controller->id;
$ca = Yii::app()->controller->route;
$m = (isset($this->module)?$this->module->getName():NULL);

?>

<div class="container" id="page" <?= Controller::isFullWidthLayout() ? 'style="width: 99%;"' : '';?>>
    <div class="text-right">
        <a href="<?= Yii::app()->createUrl('/site/switchLayout');?>" class="btn btn-mini btn-info">Switch to <?= Controller::isFullWidthLayout() ? 'fixed-width' : '100% width';?> layout</a>&nbsp;
    </div>
    <div class="text-center">
        <h1>Świat Przesyłek</h1>
    </div>
    <?php
    Yii::app()->clientScript->registerCoreScript('jquery');
    ?>

    <?php
    // confirm
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/jquery.confirm.min.js');
    $script = '$("[data-confirm]").confirm()';
    Yii::app()->clientScript->registerScript('dataConfirm', $script, CClientScript::POS_READY);
    ?>

    <?php
    if(!$this->hideMenu):
        ?>
        <div class="mainMbMenu" style="text-align: center;">

            <?php

            $menu = array(
                "items"=>array(
                    array(
                        "label" => "Index",
                        "items"=>array(
                            array(
                                "label"=>"Index",
                                "url"=>array("/"),
                                "active" => $c=='site',
                            ),
                        ),
                    ),
                    array(
                        "label"=>"My account (".Yii::app()->user->name.")",
                        'items' => array(
                            array(
                                "label"=>"Logout",
                                "url"=>array("/site/logout"),
                            ),
                            array(
                                "label"=>"Edit",
                                "url"=>array("/admin/manageOwn"),
                                "active" => $ca=='admin/manageOwn',
                            )
                        ),
                    ),
                    array(
                        "label"=>"Content",
                        'items' => array(
                            array(
                                "label"=>"Pages",
                                "url"=>array("/page"),
                                "active" => $c=='page',
                            ),
                            array(
                                "label"=>"FAQ",
                                "url"=>array("/faq"),
                                "active" => $c=='faq',
                            ),
                            array(
                                "label"=>"Translation",
                                "url"=>array("/translation"),
                                "active" => $c=='translation',
                            ),
                            array(
                                "label"=>"Dictionary",
                                "url"=>array("/dictionary"),
                                "active" => $c=='dictionary',
                            ),
                            array(
                                "label"=>"Pricing",
                                "url"=>array("/pricing"),
                                "active" => $c=='pricing',
                            ),
                        ),
                    ),
                    array(
                        "label" => "Order",
                        "items" => array(
                            array(
                                "label" => "Order",
                                "url"=>array("/order/"),
                                "active" => $c=='order',
                            ),
                        ),
                    ),
//                    array(
//                        "label"=>"HybridMail",
//                        'items' => array(
//                            array(
//                                "label"=>"HybridMail",
//                                "url"=>array("/hybridMail"),
//                                "active" => $c=='hybridMail' && $m == 'hybridMail',
//                            ),
//                            array(
//                                "label"=>"Prices",
//                                "url"=>array("/hybridMail/hybridMailPrice"),
//                                "active" => $c=='hybridMailPrice' AND $ca!='hybridMailPrice/groups' && $m == 'hybridMail',
//                            ),
//                            array(
//                                "label"=>"Additions",
//                                "url"=>array("/hybridMail/hybridMailAdditionList"),
//                                "active" => $c=='hybridMailAdditionList' && $m == 'hybridMail',
//                            ),
//                            array(
//                                "label"=>"Stats",
//                                "url"=>array("/hybridMail/hybridMailStat/"),
//                                "active" => $ca=='hybridMailStat' && $m == 'hybridMail',
//                            ),
//                        ),
//                    ),
                    array(
                        "label"=>"Postal",
                        'items' => array(
                            array(
                                "label"=>"Postal",
                                "url"=>array("/postal"),
                                "active" => ($c=='postal' OR $c=='importPostalStatFromFile'),
                            ),
                            array(
                                "label"=>"Routing / prices",
                                "url"=>array("/postal/postalRouting"),
                                "active" => $c=='postalRouting',
                            ),
                            array(
                                "label"=>"Operators",
                                "url"=>array("/postal/postalOperator"),
                                "active" => $c=='postalOperator',
                            ),
                            array(
                                "label"=>"Stats",
                                "url"=>array("/postal/postalStat/"),
                                "active" => $c=='postalStat' && $m == 'postal',
                            ),
                            array(
                                "label"=>"Labels",
                                "url"=>array("/postal/postalLabel/"),
                                "active" => $c=='postalLabel' && $m == 'postal',
                            ),
                            array(
                                "label"=>"User groups",
                                "url"=>array("/postal/postalUserGroup/"),
                                "active" => $c=='postalUserGroup' && $m == 'postal',
                            ),
                            array(
                                "label"=>"Postal scanner",
                                "url"=>array("/postalScanner"),
                                "active" => $c=='postalScanner',
                            ),
                            array(
                                "label"=>"Stat mapping",
                                "url"=>array("/statMapMapping/index", 'type' => StatMapMapping::TYPE_POSTAL),
                                "active" => $c=='statMapMapping' && $_GET['type'] == StatMapMapping::TYPE_POSTAL,
                            ),
                            array(
                                "label"=>"Label printer",
                                "url"=>array("/postal/postal/labelPrinter"),
                                "active" => $ca=='postal/postal/labelPrinter',
                            ),
                            array(
                                "label"=>"Mass manage",
                                "url"=>array("/postal/postalMassManage"),
                                "active" => $c=='postalMassManage' && $m == 'postal',
                            ),
                            array(
                                "label"=>"SMS Archive",
                                "url"=>array("/postal/smsArchive"),
                                "active" => $c=='smsArchive' && $m == 'postal',
                            ),
                        ),
                    ),
//                    array(
//                        "label"=>"International Mail",
//                        'items' => array(
//                            array(
//                                "label"=>"International Mail",
//                                "url"=>array("/internationalMail"),
//                                "active" => $c=='internationalMail',
//                            ),
//                            array(
//                                "label"=>"Prices",
//                                "url"=>array("/internationalMail/internationalMailPrice"),
//                                "active" => $c=='internationalMailPrice' AND $ca!='internationalMailPrice/groups' && $m == 'internationalMail',
//                            ),
//                            array(
//                                "label"=>"Additions",
//                                "url"=>array("/internationalMail/internationalMailAdditionList"),
//                                "active" => $c=='internationalMailAdditionList' && $m == 'internationalMail',
//                            ),
//                            array(
//                                "label"=>"Stats",
//                                "url"=>array("/internationalMail/internationalMailStat/"),
//                                "active" => $c=='internationalMailStat' && $m == 'internationalMail',
//                            ),
//                        ),
//                    ),
                    array(
                        "label"=>"Courier",
                        'items' => array(
                            array(
                                "label"=>"Packages",
                                "url"=>array("/courier/courier"),
                                "active" => ($c=='courier' OR $c=='importCourierExternalFromFile' OR $c=='importCourierStatFromFile') && $m == 'courier' && $ca != 'courier/courier/labelPrinter',
                            ),
                            array(
                                "label"=>"Statuses",
                                "url"=>array("/courier/courierStat"),
                                "active" => $c=='courierStat' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Country Groups",
                                "url"=>array("/courier/courierCountryGroup"),
                                "active" => $c=='courierCountryGroup' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Hubs",
                                "url"=>array("/courier/courierHub"),
                                "active" => $c=='courierHub' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Prices",
                                "url"=>array("/courier/courierPrice"),
                                "active" => $c=='courierPrice' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Custom Pickup Prices",
                                "url"=>array("/courier/courierCustomPickupPrice"),
                                "active" => $c=='courierCustomPickupPrice' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Internal Operators",
                                "url"=>array("/courier/courierOperator"),
                                "active" => $c=='courierOperator' && $m == 'courier',
                            ),
                            array(
                                "label"=>"External Operators",
                                "url"=>array("/courier/courierExternalOperator"),
                                "active" => $c=='courierExternalOperator' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Addition List",
                                "url"=>array("/courier/courierAdditionList"),
                                "active" => $c=='courierAdditionList' && $m == 'courier',
                            ),
//                            array(
//                                "label"=>"OOE - Accounts",
//                                "url"=>array("/courier/courierOoeAccounts"),
//                                "active" => $c=='courierOoeAccounts' && $m == 'courier',
//                            ),
//                            array(
//                                "label"=>"OOE - Additions",
//                                "url"=>array("/courier/courierOoeAdditionList"),
//                                "active" => $c=='courierOoeAdditionList' && $m == 'courier',
//                            ),
//                            array(
//                                "label"=>"OOE - Services",
//                                "url"=>array("/courier/courierOoeService"),
//                                "active" => $c=='courierOoeService' && $m == 'courier',
//                            ),
                            array(
                                "label"=>"Label orders",
                                "url"=>array("/courier/courierOrder"),
                                "active" => $c=='courierOrder' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Label printer",
                                "url"=>array("/courier/courier/labelPrinter"),
                                "active" => $ca=='courier/courier/labelPrinter',
                            ),
                            array(
                                "label"=>"Courier scanner",
                                "url"=>array("/courierScanner"),
                                "active" => $c=='courierScanner',
                            ),
                            array(
                                "label"=>"Collect Service",
                                "url"=>array("/courier/courierCollect"),
                                "active" => $c=='courierCollect' && $m == 'courier',
                            ),
                            array(
                                "label"=>"DHL Remote Areas",
                                "url"=>array("/courier/courierDhlRemoteAreas"),
                                "active" => $c=='courierDhlRemoteAreas' && $m == 'courier',
                            ),
//                            array(
//                                "label"=>"Domestic - Additions",
//                                "url"=>array("/courier/courierDomesticAdditionList"),
//                                "active" => $c=='courierDomesticAdditionList' && $m == 'courier',
//                            ),
//                            array(
//                                "label"=>"Domestic - Operators",
//                                "url"=>array("/courier/courierDomesticOperator"),
//                                "active" => $c=='courierDomesticOperator' && $m == 'courier',
//                            ),
                            array(
                                "label"=>"Stat mapping",
                                "url"=>array("/statMapMapping/index", 'type' => StatMapMapping::TYPE_COURIER),
                                "active" => $c=='statMapMapping' && $_GET['type'] == StatMapMapping::TYPE_COURIER,
                            ),
                            array(
                                "label"=>"COD Settle",
                                "url"=>array("/courier/courierCodSettled"),
                                "active" => $c=='courierCodSettled' && $m == 'courier',
                            ),
                            array(
                                "label"=>"COD Settle Scanner",
                                "url"=>array("/courierCodSettledScanner/index"),
                                "active" => $c=='courierCodSettledScanner',
                            ),
                            array(
                                "label"=>"COD Availability",
                                "url"=>array("/courier/courierCodAvailability"),
                                "active" => $c=='courierCodAvailability' && $m == 'courier',
                            ),
                            array(
                                "label"=>"U Operators",
                                "url"=>array("/courier/courierUOperator"),
                                "active" => $c=='courierUOperator' && $m == 'courier',
                            ),
                            array(
                                "label"=>"U Prices",
                                "url"=>array("/courier/courierUPrices"),
                                "active" => $c=='courierUPrices' && $m == 'courier',
                            ),
                            array(
                                "label"=>"U Additions",
                                "url"=>array("/courier/courierUAdditionList"),
                                "active" => $c=='courierUAdditionList' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Mass manage",
                                "url"=>array("/courier/courierMassManage"),
                                "active" => $c=='courierMassManage' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Courier logs",
                                "url"=>array("/courier/courierLog"),
                                "active" => $c=='courierLog' && $m == 'courier',
                            ),
                            array(
                                "label"=>"SMS Archive",
                                "url"=>array("/courier/smsArchive"),
                                "active" => $c=='smsArchive' && $m == 'courier',
                            ),
                            array(
                                "label"=>"Prices - Internal Return",
                                "url"=>array("/courier/courierPriceUniversal/admin", 'type' => 2),
                                "active" => $c=='courierPriceUniversal' && $m == 'courier' && $_GET['type'] == 2,
                            ),
                            array(
                                "label"=>"Prices - External Return",
                                "url"=>array("/courier/courierPriceUniversal/admin", 'type' => 1),
                                "active" => $c=='courierPriceUniversal' && $m == 'courier' && $_GET['type'] == 1,
                            ),
                            array(
                                "label"=>"Prices - External",
                                "url"=>array("/courier/courierPriceUniversal/admin", 'type' => 3),
                                "active" => $c=='courierPriceUniversal' && $m == 'courier' && $_GET['type'] == 3,
                            ),
                            array(
                                "label"=>"Buffers",
                                "url"=>array("/courier/courierBuffer"),
                                "active" => $c=='courierBuffer' && $m == 'courier',
                            ),
                        ),
                    ),
                    array(
                        "label"=>"User",
                        'items' => array(
                            array(
                                "label"=>"User",
                                "url"=>array("/user"),
                                "active" => $c=='user',
                            ),
                            array(
                                "label"=>"Invoices",
                                "url"=>array("/userInvoice"),
                                "active" => $c=='userInvoice',
                            ),
                            array(
                                "label"=>"Invoices - Mass Manage",
                                "url"=>array("/userInvoiceMassManage"),
                                "active" => $c=='userInvoiceMassManage',
                            ),
                            array(
                                "label"=>"Debt Collection",
                                "url"=>array("/userDebtCollection"),
                                "active" => $c=='userDebtCollection',
                            ),
                            array(
                                "label"=>"Messages",
                                "url"=>array("/userMessage"),
                                "active" => $c=='userMessage',
                            ),
                            array(
                                "label"=>"UserGroup",
                                "url"=>array("/userGroup"),
                                "active" => $c=='userGroup',
                            ),
                            array(
                                "label"=>"PrintServers",
                                "url"=>array("/printServer"),
                                "active" => $c=='printServer',
                            ),
                            array(
                                "label"=>"Documents",
                                "url"=>array("/userDocuments"),
                                "active" => $c=='userDocuments',
                            ),
                            array(
                                "label"=>"User logs",
                                "url"=>array("/userLog"),
                                "active" => $c=='userLog',
                            ),
                            array(
                                "label"=>"ACK/Manifest archive",
                                "url"=>array("/ackCardArchive"),
                                "active" => $c=='ackCardArchive',
                            ),
                            array(
                                "label"=>"Parent accounting list",
                                "url"=>array("/user/parentAccoutingUserList"),
                                "active" => $c=='user' && $ca = 'parentAccoutingUserList',
                            ),
                        ),
                    ),
                    array(
                        "label"=>"Palette",
                        'items' => array(
                            array(
                                "label"=>"Palette",
                                "url"=>array("/palette/palette"),
                                "active" => $m == 'palette',
                            ),
                        ),
                    ),
                    array(
                        "label"=>"Special Transport",
                        'items' => array(
                            array(
                                "label"=>"Special Transport",
                                "url"=>array("/specialTransport/specialTransport"),
                                "active" => $m == 'specialTransport',
                            ),
                        ),
                    ),
                    array(
                        "label"=>"Other",
                        'items' => array(
                            array(
                                "label" => "Administrators",
                                "url"=>array("/admin"),
                                "active" => $c=='admin' AND $ca!='admin/manageOwn'
                            ),
                            array(
                                "label" => "Country lists",
                                "url"=>array("/countryList"),
                                "active" => $c=='countryList'
                            ),
                            array(
                                "label" => "Country groups",
                                "url"=>array("/countryGroup"),
                                "active" => $c=='countryGroup'
                            ),
                            array(
                                "label" => "SP Points",
                                "url"=>array("/spPoints"),
                                "active" => $c=='spPoints'
                            ),
                            array(
                                "label"=>"Payment Type",
                                "url"=>array("/paymentType"),
                                "active" => $c=='paymentType',
                            ),
                            array(
                                "label"=>"Salesman",
                                "url"=>array("/salesman"),
                                "active" => $c=='salesman',
                            ),
                            array(
                                "label"=>"Site message",
                                "url"=>array("/siteMessage"),
                                "active" => $c=='siteMessage',
                            ),
                            array(
                                "label"=>"Poczta Polska",
                                "url"=>array("/pocztaPolska"),
                                "active" => $c=='pocztaPolska',
                            ),
                            array(
                                "label"=>"Poczta Polska - Waiting",
                                "url"=>array("/pocztaPolskaWaiting"),
                                "active" => $c=='pocztaPolskaWaiting',
                            ),
//                            array(
//                                "label"=>"Poczta Polska - Submit",
//                                "url"=>array("/pocztaPolskaSubmit"),
//                                "active" => $c=='pocztaPolskaSubmit',
//                            ),
                            array(
                                "label"=>"Czech Post - Submit",
                                "url"=>array("/czechPostSubmit"),
                                "active" => $c=='czechPostSubmit',
                            ),
                            array(
                                "label"=>"Czech Post - Reprinter",
                                "url"=>array("/cpostReprinter"),
                                "active" => $c=='cpostReprinter',
                            ),
                            array(
                                "label"=>"Czech Post - Mass Reprinter",
                                "url"=>array("/czechPostMassReprinter"),
                                "active" => $c=='czechPostMassReprinter',
                            ),
                            array(
                                "label"=>"Manifest queque",
                                "url"=>array("/manifestQueque"),
                                "active" => $c=='manifestQueque',
                            ),
                            array(
                                "label"=>"Debt block message",
                                "url"=>array("/debtBlock"),
                                "active" => $c=='debtBlock',
                            ),
                            array(
                                "label"=>"Plugins",
                                "url"=>array("/plugins"),
                                "active" => $c=='plugins',
                            ),
                            array(
                                "label"=>"Fakturownia",
                                "url"=>array("/fakturownia"),
                                "active" => $c=='fakturownia',
                            ),
                            array(
                                "label"=>"B2C2Correos",
                                "url"=>array("/b2C2Correos"),
                                "active" => $c=='b2C2Correos',
                            ),
                            array(
                                "label" => "Email management",
                                "url"=>array("/daApi"),
                                "active" => $c=='daApi'
                            ),
                            array(
                                "label" => "Relabeling tool",
                                "url"=>array("/relabel"),
                                "active" => $c=='relabel'
                            ),
                            array(
                                "label" => "Autenti",
                                "url"=>array("/autenti"),
                                "active" => $m=='autenti'
                            ),
                        ),
                    ),
                ),
            );

            ?>

            <?php
            if(!function_exists('search_in_array')) {
                function search_in_array($array, $key, $value)
                {
                    $return = array();
                    foreach ($array as $k => $subarray) {
                        if (isset($subarray[$key]) && $subarray[$key] == $value) {
                            $return[$k] = $subarray;
                            return $return;
                        }
                    }
                }
            }
            ?>

            <ul class="nav nav-tabs" style="display: inline-block; text-align: left;">
                <?php foreach($menu['items'] AS $item):?>
                    <li class="dropdown <?php echo search_in_array($item['items'],'active', true)?'active':'';?>">
                        <?php if(S_Useful::sizeof($item['items'])>1):?>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <?php echo $item['label'];?>
                                <b class="caret"></b>
                            </a>
                        <?php else: ?>
                            <?php echo CHtml::link($item['items'][0]['label'], $item['items'][0]['url'], $item['items'][0]['linkOptions']);?>
                        <?php endif;?>
                        <?php if(S_Useful::sizeof($item['items'])>1):?>
                            <ul class="dropdown-menu">
                                <?php foreach($item['items'] AS $subitem):?>
                                    <li class="<?php echo $subitem['active']?'active':'';?>"><?php echo CHtml::link($subitem['label'], $subitem['url'], $subitem['linkOptions']);?></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach;?>
            </ul>

        </div><!-- mainmenu -->
    <?php
    else:
        ?>
        <div class="mainMbMenu" style="text-align: center;">

            <?php

            $menu = array(
                "items"=>array(
                    array(
                        "label"=>"My account (".Yii::app()->user->name.")",
                        'items' => array(
                            array(
                                "label"=>"Logout",
                                "url"=>array("/site/logout"),
                            ),
                            array(
                                "label"=>"Edit",
                                "url"=>array("/admin/manageOwn"),
                                "active" => $ca=='admin/manageOwn',
                            )
                        ),
                    ),
                ),
            );

            if(Yii::app()->user->authority == Admin::AUTHORITY_COURIER_SCANNER)
            {
                $menu['items'][] = [
                    "label"=>"Courier scanner",
                    'items' => array(
                        array(
                            "label"=>"Courier scanner",
                            "url"=>array("/courierScanner"),
                            "active" => $c=='courierScanner',
                        ),
                    ),];
            }

            ?>

            <?php
            if(!function_exists('search_in_array')) {
                function search_in_array($array, $key, $value)
                {
                    $return = array();
                    foreach ($array as $k => $subarray) {
                        if (isset($subarray[$key]) && $subarray[$key] == $value) {
                            $return[$k] = $subarray;
                            return $return;
                        }
                    }
                }
            }
            ?>

            <ul class="nav nav-tabs" style="display: inline-block; text-align: left;">
                <?php foreach($menu['items'] AS $item):?>
                    <li class="dropdown <?php echo search_in_array($item['items'],'active', true)?'active':'';?>">
                        <?php if(S_Useful::sizeof($item['items'])>1):?>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <?php echo $item['label'];?>
                                <b class="caret"></b>
                            </a>
                        <?php else: ?>
                            <?php echo CHtml::link($item['items'][0]['label'], $item['items'][0]['url'], $item['items'][0]['linkOptions']);?>
                        <?php endif;?>
                        <?php if(S_Useful::sizeof($item['items'])>1):?>
                            <ul class="dropdown-menu">
                                <?php foreach($item['items'] AS $subitem):?>
                                    <li class="<?php echo $subitem['active']?'active':'';?>"><?php echo CHtml::link($subitem['label'], $subitem['url'], $subitem['linkOptions']);?></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach;?>
            </ul>

        </div><!-- mainmenu -->
    <?php
    endif;
    ?>

    <?php if(isset($this->breadcrumbs) AND 0):?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    <?php endif?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        &copy; <?php echo date('Y'); ?>
    </div><!-- footer -->
</div><!-- page -->
<?php $this->endContent(); ?>

