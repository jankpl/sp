(function($) {

    $(document).ready(function(){

        $('[data-show-address-more]').on('click', showAddressMore);

        $("[data-dimensions]").TouchSpin({
            step: 1,
            decimals: 0,
            max: 330,
            min: 1,
            boostat: 5,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });

        $("[data-weight]").TouchSpin({
            step: 0.01,
            decimals: 2,
            boostat: 5,
            max: $(this).attr('data-max-weight'),
            min: 0.01,
            buttondown_class: 'btn btn-xs',
            buttonup_class: 'btn btn-xs',
        });


        $('.bootstrap-touchspin-down').attr('tabindex', -1);
        $('.bootstrap-touchspin-up').attr('tabindex', -1);

        $('[data-address-line-2]').on('change', function(){
            $(this).closest('[data-address-more]').slideDown();
            $(this).closest('.row').prev().find('[data-show-address-more]').hide();
        });

        $('#courier-return-form').on('submit', function(e)
        {

                e.preventDefault();
                $('.overlay').remove();
                return false;

        });

        $('[data-bootstrap-checkbox]').checkboxpicker({
            offClass : 'btn-default btn-lg',
            onClass : 'btn-primary btn-lg',
            defaultClass: 'btn-lg',
        });

    });

    function showAddressMore()
    {
        $(this).parent().parent().parent().find('[data-address-more]').slideDown();
        $(this).remove();
    }

    var Overlay = (function() {
        "use strict";

        var that = {};
        var $overlay = $("<div>");
        var $textBottom = $("<div>");
        var $textTop = $("<div>");
        var $loader = $("<div>");

        that.isShow = function(){
            return $("#overlay").is(':visible');
        };

        that.show = function() {
            var $form = $("#courier-return-form");

            $overlay.addClass('overlay');


            $loader.addClass('loader');

            $overlay.append($loader);


            $textTop.addClass('loader-text-top');
            $textTop.html(yii.loader.textTop);


            $textBottom.addClass('loader-text-bottom');
            $textBottom.html(yii.loader.textBottom);

            $overlay.append($textTop);
            $overlay.append($textBottom);

            $form.css('position','relative');
            $form.append($overlay);

            $textTop.hide().delay(1000).fadeIn();
            $textBottom.hide().delay(3000).fadeIn();
        };

        that.hide = function() {
            $overlay.remove();
        };

        return that;
    }());


})(jQuery);