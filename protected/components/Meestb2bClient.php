<?php

class Meestb2bClient extends GlobalOperator
{
    const URL = GLOBAL_CONFIG::MEEST_URL;
    const API_KEY = GLOBAL_CONFIG::MEEST_KEY;

    public $local_id;
    public $package_value;

    public $sender_country;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;

    public $package_content;

    public $cod = 0;

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    public static function createShipmentByTest()
    {
        $model = new self;

        $model->sender_country = 'PL';

        $model->receiver_name = 'Jan Nowak';
        $model->receiver_company = 'Nowak Inc';
        $model->receiver_zip_code = '79000';
        $model->receiver_city = 'Kiev';
        $model->receiver_address = 'Glategny Esplanade';
        $model->receiver_tel = '111222333';
        $model->receiver_country = 'UA';
        $model->receiver_email = 'asd@das.pl';

        $model->package_weight = 1;

        $model->package_content = 'Test';
        $model->comments = 'Order #123';

        $model->local_id = 'test133'.rand(999,99999);

//        $model->service = self::SERVICE_48;

        $model->packages_number = 2;

        return $model->createShipment(true);
    }

    protected static function _ownExtractNumber($text, $defaultValue = '0')
    {
        $REG_PATTERN = '/(\s|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*((([a-z]){1,2}|[0-9\/\-]+)((\s)+|$))*/i';
        $building = '';

        if(preg_match_all($REG_PATTERN, $text, $result2))
            $building = $result2[0][0];

        $building = trim($building);

        if(mb_strlen($building) > 8)
        {
            // max length is 8
            // if it's longer, use older, more choosy rexexp
            $REG_PATTERN = '/(\s|\.|\/)*([a-z]?(\d((\-|\/)\d+)?)+[a-z]*)(\s|\.|\/)*/i';
            if(preg_match_all($REG_PATTERN, $text, $result2))
                $building = $result2[2][0];

            $building = trim($building);
        }

        if((!$building || $building == '') && $defaultValue !== false)
            $building = $defaultValue;

        return $building;
    }


    public function createShipment($returnError = false)
    {
//        $data = new stdClass();
//        $data->arg0 = new stdClass();
//        $data->arg0->barCode = '';
//        $data->arg0->currencyName = 'EUR';
//        $data->arg0->deliveryNumber = 'EUR';
//        $data->arg0->deliveryTypeEnum = 'COURRIER';
//        $data->arg0->parcelNumberInternal = '37507/269/01';
//        $data->arg0->parcelPositionApiBeans = new stdClass();
//        $data->arg0->parcelPositionApiBeans->count = 1;
//        $data->arg0->parcelPositionApiBeans->countryCode = 'CN';
//        $data->arg0->parcelPositionApiBeans->customCode = '84713000';
//        $data->arg0->parcelPositionApiBeans->name = 'Планшетный ПК ASUS Fonepad 7 (FE170CG-1A017A) 3G Black';
//        $data->arg0->parcelPositionApiBeans->nameUk = 'ablet ASUS FE170CG Z2520/7&quot 1GB/8GB/3G/GPS/BT/Android 4.3';
//        $data->arg0->parcelPositionApiBeans->value = 100;
//        $data->arg0->parcelPositionApiBeans->valueUah = 1000;
//        $data->arg0->parcelPositionApiBeans->weight = 1;
//        $data->arg0->parcelPositionApiBeans->productEan = '000342273448794';
//        $data->arg0->parcelPositionApiBeans->productSize = 'XXL';
//        $data->arg0->receiverName = 'МорозЮрійВікторович';
//        $data->arg0->receiverEmail = 'mail.mail@gmail.com';
//        $data->arg0->recipentAddress = new stdClass();
//        $data->arg0->recipentAddress->apartment = '';
//        $data->arg0->recipentAddress->city = 'Львов';
//        $data->arg0->recipentAddress->cityIdRef = '0xb11200215aee3ebe11df749b62c3d54a';
//        $data->arg0->recipentAddress->country = 'Украина';
//        $data->arg0->recipentAddress->district = '';
//        $data->arg0->recipentAddress->house = '147';
//        $data->arg0->recipentAddress->name = 'МорозЮрійВікторович';
//        $data->arg0->recipentAddress->phone = '0937393962';
//        $data->arg0->recipentAddress->postCode = '79000';
//        $data->arg0->recipentAddress->postOffice = 'Львов';
//        $data->arg0->recipentAddress->street = 'Зелена';
//        $data->arg0->summaCodEur = 120;
//        $data->arg0->summaCodUah = 120;
//        $data->arg0->value = 120;
//        $data->arg0->weight = 1;
//        $data->arg0->zaUa = '00193668';

        $cityCode = self::getCityId($this->receiver_zip_code);

        if($cityCode->success)
            $cityCode = $cityCode->data;
        else
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($cityCode->error);
            } else {
                return false;
            }
        }

        $receiverStreetNo = trim(self::_ownExtractNumber($this->receiver_address));
        $receiverAddressName = trim(str_replace($receiverStreetNo, '', $this->receiver_address));

        $codEur = $codUah = 0;
        if($this->cod)
        {
            $codUah = $this->cod;
            $codPln = $this->cod * Yii::app()->NBPCurrency->getUAHPLNValue();
            $codEur = Yii::app()->NBPCurrency->PLN2EUR($codPln);
        }

        $data = '<log:createParcel>
         <arg0>
                <barCode></barCode>
                <currencyName>EUR</currencyName>
                <deliveryNumber></deliveryNumber>
                <deliveryTypeEnum>COURRIER</deliveryTypeEnum>
                <divisionIDRRef></divisionIDRRef>
                <errorDescription></errorDescription>
                <note></note>
                <parcelDocumentsLink></parcelDocumentsLink>
                <parcelNumber></parcelNumber>
                <parcelNumberInternal>'.$this->local_id.'</parcelNumberInternal>
                <parcelPositionApiBeans>
                    <count>1</count>
                    <countryCode>'.$this->sender_country.'</countryCode>
                    <customCode>84713000</customCode>
                    <name>'.self::_cdata($this->package_content).'</name>
                    <nameUk>'.self::_cdata($this->package_content).'</nameUk>
                    <value>'.Yii::app()->NBPCurrency->PLN2EUR($this->package_value).'</value>
                    <valueUah>'.Yii::app()->NBPCurrency->PLN2UAH($this->package_value).'</valueUah>
                    <weight>'.$this->package_weight.'</weight>
                    <productEan>'.$this->package_content.'</productEan>
                    <productSize>M</productSize>
                </parcelPositionApiBeans>
                <parcelStatusEnum></parcelStatusEnum>
                <receiverName>'.trim($this->receiver_name.' '.$this->receiver_company).'</receiverName>
                <receiverEmail>'.$this->receiver_email.'</receiverEmail>
                <recipentAddress>
                    <apartment></apartment>
                    <city>'.$this->receiver_city.'</city>
                    <cityIdRef>'.$cityCode.'</cityIdRef>
                    <country>'.$this->receiver_country.'</country>
                    <district></district>
                    <house>'.$receiverStreetNo.'</house>
                    <name>'.trim($this->receiver_name.' '.$this->receiver_company).'</name>
                    <notice></notice>
                    <phone>'.$this->receiver_tel.'</phone>
                    <postCode>'.$this->receiver_zip_code.'</postCode>
                    <postOffice></postOffice>
                    <province></province>
                    <street>'.$receiverAddressName.'</street>
                    <streetWithHouseNumber>'.$this->receiver_address.'</streetWithHouseNumber>
                </recipentAddress>
                <summaCodEur>'.$codEur.'</summaCodEur>
                <summaCodUah>'.$codUah.'</summaCodUah>
                <value>'.Yii::app()->NBPCurrency->PLN2EUR($this->package_value).'</value>
                <weight>'.$this->package_weight.'</weight>
                <zaUa>'.$this->local_id.'</zaUa>
            </arg0>
            <arg1>'.self::API_KEY.'</arg1>
      </log:createParcel>';

        $resp = $this->_soapOperation($data);


        if($resp->error)
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->error);
            } else {
                return false;
            }
        } else {

            $tt = (string) $resp->data->createParcelResponse->return->barCode;
            $labelUrl = (string) $resp->data->createParcelResponse->return->parcelDocumentsLink;

            $label = file_get_contents(($labelUrl));


            $basePath = Yii::app()->basePath . '/../';
            $dir = 'uplabels/';
            $temp = $dir . 'temp/';

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            @file_put_contents($basePath . $temp . $tt . '.pdf', $label);


            $imgRaw = [];


            // first two images are CN23
            for ($i = 2; $i < 4; $i++) {
                $im->readImage($basePath . $temp . $tt . '.pdf['.$i.']');
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }
                $im->rotateImage(new ImagickPixel(), 90);
                $im->trimImage(0);
                $im->stripImage();

                $imgRaw[$i] = $im->getImageBlob();

            }
            @unlink($basePath . $temp . $tt . '.pdf');
//
//            $return[] = [
//                'status' => true,
//                'label' => $imgRaw[3],
//                'ack' => $imgRaw[2],
//                'ack_source' => GLOBAL_BROKERS::BROKER_MEEST,
//                'ack_type' => CourierExtExtAckCards::TYPE_CUSTOMS_DEC,
//                'track_id' => $tt,
//            ];

            $return[] = GlobalOperatorResponse::createSuccessResponse($imgRaw[3], $tt, CourierExtExtAckCards::TYPE_CUSTOMS_DEC);

            return $return;
        }


    }

    protected static function _cdata($text)
    {
        return '<![CDATA['.$text.']]>';
    }

    protected static function getCityId($zipCode)
    {
        $CACHE_NAME = 'MEETS_FIND_CITY_'.$zipCode;

        $resp = Yii::app()->cache->get($CACHE_NAME);

        if($resp === false) {

            $data = '<log:searchCityByPostCode>
         <arg0>' . $zipCode . '</arg0>
      </log:searchCityByPostCode>';

            $model = new self;
            $resp = $model->_soapOperation($data);

            if($resp->success)
                $resp->data = (string) $resp->data->searchCityByPostCodeResponse->return->cityIdRef;

            Yii::app()->cache->set($CACHE_NAME, $resp, 60*60);
        }

        return $resp;

    }


    protected function _soapOperation($data)
    {

        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:log="http://logic.meestgroup.com/">
   <soapenv:Header/>
   <soapenv:Body>
      '.$data.'
   </soapenv:Body>
</soapenv:Envelope>';

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: ",
            "Content-length: ".strlen($xml_post_string),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $return = new stdClass();
        $return->success = false;

        MyDump::dump('meestb2b.txt', 'REQUEST: '.print_r($xml_post_string,1));

        if(curl_error($ch)) {
            $error = curl_error($ch);

            $return->error = $error;
            $return->errorCode = $error;
            $return->errorDesc = $error;
        }
        else
        {
            $response = curl_exec($ch);

            MyDump::dump('meestb2b.txt', 'RETURN-RAW: '.print_r($response,1));

            $response = '<?xml version="1.0" encoding="utf-8"?>' . $response;
            $response = str_replace(['soap:', 'ns2:'], '', $response);
            $response = simplexml_load_string($response);

            if ($response) {
                $response = $response->Body;

                if (isset($response->Fault))
                {
                    $error = (string) $response->Fault->faultstring;

                    $return->error = $error;
                    $return->errorCode = $error;
                    $return->errorDesc = $error;
                } else {
                    $return->success = true;
                    $return->data = $response;
                }
            }
        }
        curl_close($ch);

        MyDump::dump('meestb2b.txt', 'RETURN: '.print_r($return,1));

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;

        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->getUsefulName();
        $model->receiver_zip_code = preg_replace('/\D/', '', $to->zip_code);
        $model->receiver_city = $to->city;
        $model->receiver_address = $to->address_line_1.' '.$to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(true);
        $model->receiver_country = $to->country0->code;

        $model->package_weight = $courierInternal->courier->getWeight(true);

        $model->package_content = $courierInternal->courier->package_content;
        $model->local_id = $courierInternal->courier->local_id;

        $model->business = $to->company != '' ? true : false;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnError = false)
    {

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model = new self;

        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->getUsefulName();
        $model->receiver_zip_code = preg_replace('/\D/', '', $to->zip_code);
        $model->receiver_city = $to->city;
        $model->receiver_address = $to->address_line_1.' '.$to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(true);
        $model->receiver_country = $to->country0->code;

        $model->package_weight = $courierU->courier->getWeight(true);

        $model->package_content = $courierU->courier->package_content;
        $model->local_id = $courierU->courier->local_id;

        return $model->createShipment($returnError);
    }


    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        $data = '<log:trackingParcel>
         <arg0>' . $no . '</arg0>
         <arg1>' . self::API_KEY . '</arg1>
      </log:trackingParcel>';

        $model = new self;
        $resp = $model->_soapOperation($data);

        if($resp->success)
        {
            $data = $resp->data->trackingParcelResponse;

            $statuses = new GlobalOperatorTtResponseCollection();
            foreach($data->return AS $item)
            {
                $date = (string) $item->date;
                $date = str_replace('T', ' ', $date);
                $date = substr($date,0,19);


                $statuses->addItem((string) $item->eventDescription, $date, (string) $item->additionalDescription);

//                $statuses[] = [
//                    'name' => (string) $item->eventDescription,
//                    'date' => $date,
//                    'location' => (string) $item->additionalDescription,
//                ];
            }

            return  $statuses;
        }

        return false;
    }

}