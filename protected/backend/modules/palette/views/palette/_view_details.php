<table class="table table-striped table-bordered hLeft" style="width: 500px;">
	<tr>
		<td>User</td>
		<td><?php echo $model->user_id !== null ? CHtml::link($model->user->login, array('/user/view', 'id' => $model->user_id)) : '-';?></td>
	</tr>
	<tr>
		<td>Creation date</td>
		<td><?php echo substr($model->date_entered,0,16);?></td>
	</tr>
	<tr>
		<td>Update date</td>
		<td><?php echo substr($model->date_updated,0,16);?></td>
	</tr>
	<tr>
		<td>Type</td>
		<td><?php echo Palette::types()[$model->type];?></td>
	</tr>
	<tr>
		<td>Height</td>
		<td><?php echo $model->height;?> <?php echo Palette::HEIGHT_UNIT;?></td>
	</tr>
	<tr>
		<td>Status</td>
		<td><?php echo Palette::stat()[$model->stat];?></td>
	</tr>
	<tr>
		<td>Customer notes</td>
		<td><?php echo nl2br($model->customer_notes);?></td>
	</tr>
	<tr>
		<td>Notes</td>
		<td><?php echo nl2br($model->notes);?></td>
	</tr>
	<tr>
		<td>Action</td>
		<td><?php
			if($model->stat == Palette::STAT_NEW):
				echo Chtml::link('Mark as processed', array('/palette/palette/markAsProcessed', 'id' => $model->id),array('class' => 'btn btn-success btn-small'));
			else:
				echo '-';
			endif;
			?>

            <?php
            if($model->stat != Palette::STAT_CANCELLED):
                echo Chtml::link('Mark as cancelled', array('/palette/palette/markAsCancelled', 'id' => $model->id),array('class' => 'btn btn-danger btn-small'));
            else:
                echo '-';
            endif;
            ?></td>
	</tr>
</table>