<div class="alert alert-success text-center">
    <h4><?php echo Yii::t('courier','Klonuj paczkę');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t('courier','Klonuj paczkę'), array('/courier/courier/clone', 'urlData' => $model->hash), array('target' => '_blank', 'class' => 'btn'));?>
</div>
