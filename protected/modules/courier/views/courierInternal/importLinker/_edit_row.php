<?php
/* @var $model Courier */
/* @var $additionsHtml string */

?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-edit-form',
        'htmlOptions' => ['data-edit-form-id' => $id],
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'stateful'=>false,
        'clientOptions' => [
            'validateOnSubmit'=> true,
            'afterValidate'=>'js:function(form,data,hasError){
                        if(!hasError){
                            onItemEditSubmit("'.Yii::app()->createUrl("courier/courierInternal/ajaxSaveRowOnImport", ['l' => 1]).'");
                        } else {
                            $(".overlay").hide();
                            alert("'.Yii::t('courier','Proszę poprawić błędy formularza!').'");
                         }
                        }'
        ],
    )
);
?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= Yii::t('courier', 'Edycja danych');?></h4>
    </div>
    <div class="modal-body" id="edit-modal-content">

        <div class="form" id="internal-edit-item-form">

            <h4><?php echo Yii::t('courier','Package data');?></h4>
            <div class="inline-form">
                <div style="float: left; width: 48%;">
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_weight'); ?>
                        <?php echo $form->textField($model, 'package_weight'); ?>
                        <?php echo $form->error($model,'package_weight'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_size_l'); ?>
                        <?php echo $form->textField($model, 'package_size_l'); ?>
                        <?php echo $form->error($model,'package_size_l'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_size_w'); ?>
                        <?php echo $form->textField($model, 'package_size_w'); ?>
                        <?php echo $form->error($model,'package_size_w'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_size_d'); ?>
                        <?php echo $form->textField($model, 'package_size_d'); ?>
                        <?php echo $form->error($model,'package_size_d'); ?>
                    </div><!-- row -->
                </div>
                <div style="float: right; width: 48%;">
                    <div class="row">
                        <?php echo $form->labelEx($model->courierTypeInternal, 'with_pickup'); ?>
                        <?php echo $form->dropDownList($model->courierTypeInternal, 'with_pickup', CourierTypeInternal::getWithPickupList(true,true)); ?>
                        <?php echo $form->error($model->courierTypeInternal, 'with_pickup'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_value'); ?>
                        <?php echo $form->textField($model, 'package_value'); ?>
                        <?php echo $form->error($model,'package_value'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'value_currency'); ?>
                        <?php echo $form->dropDownList($model, 'value_currency', Courier::getCurrencyList());  ?>
                        <?php echo $form->error($model,'value_currency'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'package_content'); ?>
                        <?php echo $form->textField($model, 'package_content', array('maxlength' => 256)); ?>
                        <?php echo $form->error($model,'package_content'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'note1'); ?>
                        <?php echo $form->textField($model, 'note1', array('maxlength' => 50)); ?>
                        <?php echo $form->error($model,'note1'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model,'note2'); ?>
                        <?php echo $form->textField($model, 'note2', array('maxlength' => 50)); ?>
                        <?php echo $form->error($model,'note2'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php echo $form->labelEx($model->courierTypeInternal,'_package_wrapping'); ?>
                        <?php echo $form->dropDownList($model->courierTypeInternal, '_package_wrapping', User::getWrappingList()); ?>
                        <?php echo $form->error($model->courierTypeInternal,'_package_wrapping'); ?>
                    </div><!-- row -->
                </div>


                <div style="clear: both;"></div>
            </div>
            <div class="inline-form">
                <div style="width: 48%; float: left">
                    <h4><?= Yii::t('courier','Nadawca');?></h4>
                    <?php
                    $this->renderPartial('_addressData/_form',
                        array(
                            'model' => $model->senderAddressData,
//                    'listAddressData' => UserContactBook::listContactBookItems('sender'),
                            'form' => $form,
                            'noEmail' => false,
                            'type' => 'sender',
                            'i' => 'sender',
//                        'blockAddressSuggester' => true,
                            'noRequiredInfo' => true,
                        )
                    );
                    ?>
                </div>
                <div style="width: 48%; float: right">
                    <h4><?= Yii::t('courier','Odbiorca');?></h4>
                    <?php
                    $this->renderPartial('_addressData/_form',
                        array(
                            'model' => $model->receiverAddressData,
//                    'listAddressData' => UserContactBook::listContactBookItems('receiver'),
                            'form' => $form,
                            'noEmail' => false,
                            'type' => 'receiver',
                            'i' => 'receiver',
//                        'blockAddressSuggester' => true,
                            'countries' => $countriesReceiver,
                            'noRequiredInfo' => true,
//                    'focusFirst' => true,
                        ));?>
                </div>
                <div style="clear: both;"></div>
                <?= $additionsHtml;?>
                <p class="note text-left">
                    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
                </p>
            </div>

        </div>

    </div>
    <div class="modal-footer">
        <?php echo TbHtml::button(Yii::t('app', 'Cancel'), array('class' => 'btn btn-warning', 'data-cancel' => 'true', 'data-dismiss' => 'modal')); ?>
        <?php echo TbHtml::submitButton(Yii::t('app', 'Save changes'), array('class' => 'btn-lg btn-primary', 'data-save-row-changes' => $id)); ?>
    </div>

<?php echo CHtml::hiddenField('hash', $hash); ?>
<?php echo CHtml::hiddenField('id', $id); ?>
<?php $this->endWidget();
?>
<?php
// DOUBLE LOADING OF JQUERY WAS BREAKING SCRIPTS
Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.yiiactiveform.js'] = false;