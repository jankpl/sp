<?php

class PaymentTypeController extends Controller {


    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actionAjaxGetPaymentCommissionValue()
    {

        if(Yii::app()->request->isAjaxRequest) {
            $hash = $_POST['order_hash'];
            $payment_type_id = $_POST['payment_type_id'];

            if ($hash == '' OR $payment_type_id == '')
                $this->redirect(array('/'));

            /* @var $model Order */
            $model = Order::model()->find('hash=:hash', array(':hash' => $hash));
            $model->payment_type_id = $payment_type_id;
            $payment_commision = $model->calculatePaymentCommision();

            if (!$payment_commision)
                $payment_commision = 0;

            $payment_commision = Yii::app()->PriceManager->getFinalPrice($payment_commision);
            echo S_Price::formatPrice(($payment_commision)) . ' ' . $model->currency;
            Yii::app()->end();
        } else {
            $this->redirect(array('/'));
            Yii::app()->end();
        }

    }


}