<?php

/* @var $this CourierController */
/* @var $model Courier */
?>


<h3>Label printer</h3>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'label printer',
    'enableAjaxValidation' => false,
));
?>



<div class="form">

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, 'Type in or paste package local/ref/ext id', array('closeText' => false)); ?>

    <table class="list hLeft" style="width: 500px; margin: 0 auto;">
        <tr>
            <td>Package ID</td>
            <td><?php echo CHtml::textField('local_id', '', array('id' => 'local_id')); ?></td>
            <td><img src="<?php echo Yii::app()->baseUrl;?>/images/layout/preloader.gif" id="ajax-preloader" style="display: none;"/></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><?php echo CHtml::radioButton('print_option', true, array('id' => 'print_option_0', 'value' => 0)); ?> <label for="print_option_0" style="display: inline;">Show options</label></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><?php echo CHtml::radioButton('print_option', '', array('id' => 'print_option_1', 'value' => 1)); ?> <label for="print_option_1" style="display: inline;">Automaticaly generate 2nd roll label</label></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <?php echo CHtml::radioButton('print_option', '', array('id' => 'print_option_2', 'value' => 2)); ?> <label for="print_option_2" style="display: inline;">Automaticaly send 2nd roll label to Zebra printer:</label>
                <br/>
                IP: <?php echo CHtml::textField('ip', $_SERVER['REMOTE_ADDR'], array('style' => 'width: 150px;', 'id' => 'ip')); ?>:<?php echo CHtml::textField('port', 51000, array('style' => 'width: 40px;', 'id' => 'port')); ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><?php echo CHtml::radioButton('print_option', '', array('id' => 'print_option_3', 'value' => 3)); ?> <label for="print_option_1" style="display: inline;">Show content and ref number</label></td>
        </tr>
    </table>
</div><!-- form -->

<?php
$this->endWidget();
?>


<table class="" style="width: 500px; margin: 0 auto;">
    <tr>
        <td id="labels-placeholder" class="text-center" style="vertical-align: top;">

            <?php
            if(isset($_POST['local_id']))
            {
                $this->renderPartial('label_printer_buttons', array('model' => $model));
            }
            ?>
        </td>
    </tr>
</table>

<?php
Yii::app()->clientScript->registerCoreScript('jquery');
?>

<script>
    $(document).ready(function() {

        $("#local_id").focus();

        $("#local_id").on("change", function (e) {

            if($("#local_id").val() != '') {

                $("#ajax-preloader").show();
                $("#labels-placeholder").html('');

                $.ajax({
                    method: "POST",
                    dataType: 'json',
                    url: "<?php echo Yii::app()->createUrl('/courier/courier/labelPrinter'); ?>",
                    data: {
                        local_id: $("#local_id").val(),
                        ajax: 1,
                        print_option: $("[name='print_option']:checked").val(),
                        ip: $('#ip').val(),
                        port: $('#port').val()
                    },
                    success: function (val) {

                        if (val.redirect) {
                            window.location.href = val.url;
                        } else {
                            $("#ajax-preloader").show();
                            $("#labels-placeholder").html(val.html);
                            $("#ajax-preloader").fadeOut('');
                        }
                    }
                });
            }

        });

    });
</script>


