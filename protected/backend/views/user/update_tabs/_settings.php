<?php
/* @var $model User */
?>
Go to:
<ul id="settings-contents"></ul>
<div id="settings-form">

    <div class="form">


        <?php $form = $this->beginWidget('GxActiveForm', array(
            'id' => 'user-form',
            'enableAjaxValidation' => false,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnType'=>true,
                'validationDelay'=> 0.1,
            ),
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_1',
        ));
        ?>


        <div style="overflow: auto; width: 700px; margin: 0 auto;">
            <div style="float: left; width: 48%;">
                <?php

                ?>

                <h3 id="set_1">1) Activation</h3>
                Current status: <strong><?= $model->activated ? 'IS ACTIVE' : 'IS NOT ACTIVE';?></strong><br/>
                <?php

                echo TbHtml::submitButton('change',array(
                    'name'=>'activate',
                ));

                ?>
                <?php

                ?>


                <h3>2) Premium</h3>
                Current status: <strong><?= $model->getIsPremium() ? 'IS PREMIUM' : 'IS NOT PREMIUM';?></strong><br/>

                <?php

                echo TbHtml::submitButton('change',array(
                    'name'=>'premium',
                ));

                ?>
            </div>
            <div style="float: right; width: 48%;">
                <h3>3) VAT setting</h3>
                Current status: <strong><?= $model->without_vat == User::PRICE_WITH_VAT ? 'WITH' : 'WITHOUT';?> VAT</strong><br/>
                <?php

                echo TbHtml::submitButton('change',array(
                    'name'=>'without_vat'
                ));

                ?>
            </div>
            <br/>
            <div style="float: right; width: 48%;">
                <h3>4) Payment on invoice</h3>
                Current status: <strong><?= $model->payment_on_invoice == User::INVOICE_ACTIVE ? 'ON' : 'OFF';?></strong><br/>

                <?php
                if(!in_array($model->partner_id, [Partners::PARTNER_SP, Partners::PARTNER_SP_ZOO]) || (!$model->payment_on_invoice && AdminRestrictions::isNMMMAdmin())):
                    ?>
                    <?php
                    echo TbHtml::submitButton('change',array(
                        'name'=>'on_invoice'
                    ));
                    ?>

                <?php
                else:
                    ?>
                    -- this setting cannont be changed --
                <?php
                endif;
                ?>
            </div>


        </div>

        <?php
        $this->endWidget();
        ?>

    </div><!-- form -->


    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'client-img-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_5',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_5">5) Assign client image</h3>



        <table class="table">
            <tr>
                <td>
                    <div>

                        <div class="row" id="image-uploader">
                            <?php echo $form->labelEx($model,'photo'); ?>
                            <p>Image will be scalled to max. width: <?= User::CLIENT_IMG_WIDTH;?>px and max. height: <?= User::CLIENT_IMG_HEIGHT;?>px.</p>
                            <div class="img-container" style="width: 50%;">
                                <div class="drop_zone" id="drop_zone_multi">
                                    <div class="dz_waiting">
                                        Przeciągnij tutaj zdjęcie lub kliknij <span class="glyphicon glyphicon-share-alt"></span>
                                    </div>
                                    <div class="dz_drop">
                                        Upuść obrazek <span class="glyphicon glyphicon-arrow-down"></span>
                                    </div>
                                </div>
                                <input type="file" data-id="imageLoaderInput" name="imageLoaderInput" style="display: none;" accept="image/gif, image/jpeg, image/jpg, image/png"/>
                            </div>
                            <?= $form->hiddenField($model, 'clientImg', ['data-img-data' => 'true']); ?>
                            <?= $form->error($model, 'clientImg'); ?>

                        </div>
                        <p>Preview:</p>
                        <div class="row" id="image-preview" style="width: <?= (User::CLIENT_IMG_WIDTH + 20);?>px; height: <?= (User::CLIENT_IMG_HEIGHT + 20);?>px;">
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleClientImg'));?>  <?php echo CHtml::submitButton(Yii::t('app', 'Remove image'), array('name' => 'removeClientImg', 'class' => 'btn-xs'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/dropImage.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/backend/preloader.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/backend/dropImage.css');
    Yii::app()->clientScript->registerScript('jsHelpers', '
          jsHelpers = {
              url: {
                  resizeImageUrl: '.CJSON::encode(Yii::app()->createAbsoluteUrl('/user/ajaxPrepareImage')).',
              },
               text: {
                  imageSaved: '.CJSON::encode('Twój obrazek został zapisany!').',
                  imageSaveError: '.CJSON::encode('Ups, coś się nie udało...').',
                  onlyImagesError: '.CJSON::encode('Dozwolone są jedynie obrazki').',
                  imageTooBigError: '.CJSON::encode('Maksymanla dozwolona waga pliku to: ').',
              },
          };
      ', CClientScript::POS_HEAD);
    ?>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'print-server-active-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_6',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_6">6) Allow activating Print Server for user</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'printServerActive'); ?></td>
                <td>Select in order to allow user activating PrintServer</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'togglePrintServerActive'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'courier-ref-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_7',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_7">7) Activate & set Courier REF prefix</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->textField($model, 'courierRefPrefix', ['prompt' => '-', 'maxlength' => 2] ); ?></td>

            </tr>
            <tr>
                <td><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCourierRefPrefix'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <?php
    $cmd = Yii::app()->db->createCommand();
    $refs = $cmd->select('user_options.value, user.login, user_id')->from('user_options')->join('user', 'user.id = user_id')->where('(user_options.name = :name OR user_options.name = :name2)', [
        ':name' => 'courierRefPrefix',
        ':name2' => 'postalRefPrefix',
    ])->order('user_options.value ASC')->queryAll();

    $refs = array_map("unserialize", array_unique(array_map("serialize", $refs)));

    $list = '';

    foreach($refs AS $item)
        $list .= '<a href="'.Yii::app()->createUrl('user/update', ['id' => $item['user_id']]).'" target="_blank" title="'.$item['login'].' (#'.$item['user_id'].')">'.$item['value'].'</a> ';

    ?>
    ---<br/>
    List of used REF prefixes: <strong><?= $list;?></strong><br/>
    ---

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'postal-ref-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_8',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_8">8) Activate & set Postal REF prefix</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->textField($model, 'postalRefPrefix', ['prompt' => '-', 'maxlength' => 2] ); ?></td>

            </tr>
            <tr>
                <td><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'togglePostalRefPrefix'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <h3>9) Assign salesman</h3>

        Option is moved to "Data" settings.


    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'block-notify-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_10',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_10">10) Set custom email adress for accounting notifications. </h3>

        <table class="table">
            <tr>
                <td><?php echo $form->textField($model, 'accountingEmail'); ?></td>
                <td><?php echo $form->textField($model, 'accountingEmail2'); ?></td>
                <td><?php echo $form->textField($model, 'accountingEmail3'); ?></td>
            </tr>
            <tr>
                <td colspan="3">If not set, default will be used</td>
            </tr>
            <tr>
                <td colspan="3"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleAccountingEmail'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'block-notify-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_11',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_11">11) Disable SMS notify option</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'adminBlockNotifySms'); ?></td>
                <td>Select in order to block SMS notification option for this user</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleBlockNotify'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>
    <div class="form">

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'toggle-internal-pickup-type-custom-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_12',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_12">12) Courier Internal Pickup Options</h3>
        <p>You can disable here some of pickup methods for customer. Remember to verify with option #26</p>
        <?php
        if(!$model->getInternalPickupTypeCustomSettings()):
            ?>
            <div class="alert alert-success">User has default options.</div>
        <?php
        else:
            ?>
            <div class="alert alert-warning">User has custom options activated.</div>
        <?php
        endif;
        ?>

        <table class="table ">
            <thead>
            <tr>
                <th>Option</th>
                <th>Active</th>
            </tr>
            </thead>
            <?php
            foreach(CourierTypeInternal::getWithPickupList(false) AS $key => $item):
                ?>
                <tr>
                    <td>(#<?= $key;?>) <?= $item;?></td>
                    <td><?= CHtml::checkBox('internalPickupTypeCustomSettings['.$key.']', $model->getInternalPickupTypeCustomSettings($key));?></td>
                </tr>
            <?php
            endforeach;
            ?>

            <tr>
                <td colspan="3"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleInternalPickupTypeCustomSettings'));?> <?php echo CHtml::submitButton(Yii::t('app', 'Restore default'), array('name' => 'defaultInternalPickupTypeCustomSettings', 'class' => 'btn-xs'));?></td>
            </tr>
        </table>

        <?php
        $this->endWidget();
        ?>
    </div>
    <?php
    //////////////////////////////////////////////////////////////////
    ?>




    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'ups-collect-block-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_13',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_13">13) Disable automatic collect services for packages </h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'upsCollectBlock'); ?></td>
                <td>Select in order to disable automatic collect service for this user for packages</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleUpsCollectBlock'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <div class="form">




        <h3 id="set_14">14) Disable automatic collect services for packages type Domestic</h3>

        -- depreciated --

    </div>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'allow-internal-labels-extended-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_15',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_15">15) Allow extended labels generation for Courier Internal</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'allowCourierInternalExtendedLabel'); ?></td>
                <td>Select to activate - if active, customer will get "last" label (second or only)</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleAllowCourierInternalExtendedLabel'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'allow-internal-labels-extended-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_16',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_16">16) Show user operators names && TT numbers for Courier Internal</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'showCourierInternalOperatorsData'); ?></td>
                <td>Select to activate</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleShowCourierInternalOperatorsData'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'external-ids-in-notifications',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_17',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_17">17) Put external Ids in notifications of this customer</h3>

        <p>Our local number will be replaced with external id in email and sms notifications. For Internal packages with 2 operators, selection of number will be based on setting #15.</p>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'externalIdsInNotifications'); ?></td>
                <td>Select to activate</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleExternalIdsInNotifications'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <div class="form">

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_18">18) Activate Courier OOE</h3>

        -- depreciated --

    </div>

    <div class="form">


        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_19">19) Activate Courier Domestic</h3>

        -- depreciated --

    </div>

    <div class="form">


        <h3 id="set_20">20) Activate Courier Simple</h3>

        -- depreciated --


    </div>

    <div class="form">




        <h3>21) Activate Postal Module</h3>

        <table class="table">
            <tr>
                <td>Postal is active for all Premium with payment on invoice users.</td>
            </tr>
        </table>



    </div>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'activate-postal-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_22',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_22">22) Allow SMS notification for Postal</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'postalAvailableSms'); ?></td>
                <td>Select to activate</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'togglePostalAvailableSms'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>



    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'activate-postal-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_23',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_23">23) Allow API access</h3>
        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'apiAccessActive'); ?></td>
                <td>Select to activate</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleApiAccessActive'));?></td>
            </tr>
        </table>

        <?php
        if($model->apiAccessActive):
            ?>
            <div class="alert alert-info">
                API Key: <input type="text" value="<?= $model->getOrCreateApiKey();?>" readonly="readonly" style="width: 550px;"/>
            </div>
        <?php
        endif;
        ?>

        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_24">24) Custom UPS account for Domestic packages</h3>

        -- depreciated --


    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php echo CHtml::beginForm(Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_25');?>

        <h3 id="set_25">25) Custom DHL DE HUB sender data</h3>

        <table class="table">
            <tr>
                <td>Select to activate</td>
                <td><?php echo CHtml::checkBox('User[dhlDeCustomData][active]', $model->getDhlDeCustomData('active')); ?></td>
            </tr>
            <tr>
                <td>Sender Name</td>
                <td><?php echo CHtml::textField('User[dhlDeCustomData][sender_name]', $model->getDhlDeCustomData('sender_name')); ?></td>
            </tr>
            <tr>
                <td>Sender Company</td>
                <td><?php echo CHtml::textField('User[dhlDeCustomData][sender_company]', $model->getDhlDeCustomData('sender_company')); ?></td>
            </tr>
            <tr>
                <td>Sender Address Line 1</td>
                <td><?php echo CHtml::textField('User[dhlDeCustomData][sender_address_line_1]', $model->getDhlDeCustomData('sender_address_line_1')); ?></td>
            </tr>
            <tr>
                <td>Sender Address Line 2</td>
                <td><?php echo CHtml::textField('User[dhlDeCustomData][sender_address_line_2]', $model->getDhlDeCustomData('sender_address_line_2')); ?></td>
            </tr>
            <tr>
                <td>Sender City</td>
                <td><?php echo CHtml::textField('User[dhlDeCustomData][sender_city]', $model->getDhlDeCustomData('sender_city')); ?></td>
            </tr>
            <tr>
                <td>Sender Zip Code</td>
                <td><?php echo CHtml::textField('User[dhlDeCustomData][sender_zipcode]', $model->getDhlDeCustomData('sender_zipcode')); ?></td>
            </tr>
            <tr>
                <td>Sender Country</td>
                <td>DE</td>
            </tr>
            <tr>
                <td>Sender Tel</td>
                <td><?php echo CHtml::textField('User[dhlDeCustomData][sender_tel]', $model->getDhlDeCustomData('sender_tel')); ?></td>
            </tr>
            <tr>
                <td>Sender Email</td>
                <td><?php echo CHtml::textField('User[dhlDeCustomData][sender_email]', $model->getDhlDeCustomData('sender_email')); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDhlDeCustomData'));?></td>
            </tr>
        </table>


        <?php
        echo CHtml::endForm();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'courier-internal-pickup-api',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_26',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_26">26) Courier Internal Pickup Default Mode For API</h3>
        <p>Set pickup mode for Courier Internal packages ordered via API. Remember to verify with option #12</p>
        <table class="table">
            <tr>
                <td><?php echo $form->dropdownList($model, 'courierInternalDefaultPickupFromApi', CourierTypeInternal::getWithPickupList(false, true), ['prompt' => '- no setting -']); ?></td>
                <td>Select pickup mode</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCourierInternalDefaultPickupFromApi'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'disable-courier-external-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_27',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_27">27) Disable Courier External</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'courierExternalDisable'); ?></td>
                <td>Select to disable</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCourierExternalDisable'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">



        <h3 id="set_28">28) Disable Postal International</h3>

        <table class="table">
            <tr>
                <td>Postal is active for all Premium with payment on invoice users.</td>
            </tr>
        </table>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-contract-new-type',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_29',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_29">29) Set Contract Type</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->dropDownList($model, 'contractType', User::getContractTypeList(), ['prompt' => '-']); ?></td>
                <td>Select to set contract type of this customer (important for debt collection)</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleContractType'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'activate-courier-domestic-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_30',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_30">30) Activate Courier U - Domestic & International</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'courierUAvailable'); ?></td>
                <td>Select to activate (both #30 & #31)</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCourierUAvailable'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'activate-courier-domestic-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_31',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_31">31) Activate Courier U - Only domestic</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'courierUDomesticAvailable'); ?></td>
                <td>Select to activate</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCourierUDomesticAvailable'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'disable-postal-international-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_32',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_32">32) Set Courier U Currency</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->dropDownList($model, 'courierUCurrencyId', Currency::listIdsWithNames(), ['prompt' => '-account default-']); ?></td>
                <td>Select to set currency</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCourierUCurrencyId'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'acounting-tel-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_33',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_33">33) Set custom tel adress for accounting notifications. </h3>

        <table class="table">
            <tr>
                <td><?php echo $form->textField($model, 'accountingTel'); ?></td>
            </tr>
            <tr>
                <td colspan="3">If not set, default will be used</td>
            </tr>
            <tr>
                <td colspan="3"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleAccountingTel'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'debt-block-days-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_34',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_34">34) Set debt block days</h3>

        <?php
        if(AdminRestrictions::isUltraAdmin()):
            ?>

            <?php
            if($model->parentAccountingUser):
                ?>
                This account has Parent Accounting User - see setting #50
            <?php
            else:
                ?>

                <table class="table">
                    <tr>
                        <td><?php echo $form->textField($model, 'debtBlockDays'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="3">Set number of payment delay after account will be block. 0 means default (<?= DebtBlock::DEFAULT_BLOCK_DAYS;?> days)</td>
                    </tr>
                    <tr>
                        <td colspan="3"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDebtBlockDays'));?></td>
                    </tr>
                </table>

            <?php
            endif;
            ?>

        <?php
        else:
            ?>
            Setting allowed only for administrators.
        <?php
        endif;
        ?>

        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>


    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'debt-block-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_35',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_35">35) Set debt block</h3>

        <?php
        if(AdminRestrictions::isSuperAdmin()):
            ?>

            <?php
            if($model->parentAccountingUser):
                ?>
                This account has Parent Accounting User - see setting #50
            <?php
            else:
                ?>

                <table class="table">
                    <tr>
                        <td><?php echo $form->dropDownList($model, 'debtBlock', DebtBlock::getStatList()); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDebtBlock'));?></td>
                    </tr>
                </table>

            <?php
            endif;
            ?>

        <?php
        else:
            ?>
            Setting allowed only for main admin.
        <?php
        endif;
        ?>

        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>


    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'custom-dpdplv-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_36',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_36">36) Set custom DPD PL V subaccount</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->dropDownList($model, 'customDpdPlVSubaccount', User::listCustomDpdPlVAccounts(), ['prompt' => '-- default --']); ?></td>
            </tr>
            <tr>
                <td colspan="1"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'togleCustomDpdPlVSubaccount'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>


    <?php
    //////////////////////////////////////////////////////////////////
    ?>



    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'disable-dimensional-weight',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_37',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_37">37) Activate DPDPL_V to DPDPL redirect for Non-standard packages for Courier Internal</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'dpdplNstRedirectActive'); ?></td>
            </tr>
            <tr>
                <td colspan="1"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDpdplNstRedirectActive'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>



    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'disable-dimensional-weight',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_38',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_38">38) Disable calculating dimensional weight</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'disableDimensionalWeight'); ?></td>
            </tr>
            <tr>
                <td colspan="1"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDisableDimensionalWeight'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>



    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-user-menu-simple',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_39',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_39">39) User menu simple</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'userMenuSimple'); ?></td>
                <td>Select to hide form user menu (left) the following services: Hybird Mail, Pallet transport, Oversize shipments</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleUserMenuSimple'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-api-dhl-de-notificaiton',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_40',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_40">40) Set DHL DE notification addition (#38) for Courier Pre-routing via API for DE</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'apiDhlDeNotification'); ?></td>
                <td>If active, addtion #38 will be set for packages pre-routing do DE added via API (including IAI)</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleApiDhlDeNotification'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php echo CHtml::beginForm(Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_41');?>

        <h3 id="set_41">41) Custom LP Express sender data</h3>

        <table class="table">
            <tr>
                <td>Select to activate</td>
                <td><?php echo CHtml::checkBox('User[lpExpressCustomData][active]', $model->getLpExpressCustomData('active')); ?></td>
            </tr>
            <tr>
                <td>Sender Name</td>
                <td><?php echo CHtml::textField('User[lpExpressCustomData][sender_name]', $model->getLpExpressCustomData('sender_name')); ?></td>
            </tr>
            <tr>
                <td>Sender Address</td>
                <td><?php echo CHtml::textField('User[lpExpressCustomData][sender_address]', $model->getLpExpressCustomData('sender_address')); ?></td>
            </tr>
            <tr>
                <td>Sender City</td>
                <td><?php echo CHtml::textField('User[lpExpressCustomData][sender_city]', $model->getLpExpressCustomData('sender_city')); ?></td>
            </tr>
            <tr>
                <td>Sender Zip Code</td>
                <td><?php echo CHtml::textField('User[lpExpressCustomData][sender_zipcode]', $model->getLpExpressCustomData('sender_zipcode')); ?></td>
            </tr>
            <tr>
                <td>Sender Country</td>
                <td>Lithuania</td>
            </tr>
            <tr>
                <td>Sender Tel</td>
                <td><?php echo CHtml::textField('User[lpExpressCustomData][sender_tel]', $model->getLpExpressCustomData('sender_tel')); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleLpExpressCustomData'));?></td>
            </tr>
        </table>


        <?php
        echo CHtml::endForm();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_42">42) Disable Postal Domestic</h3>

        <table class="table">
            <tr>
                <td>Postal is active for all Premium with payment on invoice users.</td>
            </tr>
        </table>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-activate-courier-type-return',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_43',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_43">43) Activate Courier Type Return</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'courierReturnAvailable'); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCourierReturnAvailable'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-activate-courier-type-return',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_44',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_44">44) Activate Courier Type External Return</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'courierExternalReturnAvailable'); ?></td>
                <td>Creating of Courier Type External Return will be available for customer</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCourierExternalReturnAvailable'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-custom-vat-rate',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_45',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_45">45) Custom VAT rate</h3>

        <?php
        if(AdminRestrictions::isUltraAdmin() OR $model->partner_id == Partners::PARTNER_CQL):
            ?>
            <table class="table">
                <tr>
                    <td><?php echo $form->numberField($model, 'customVatRate', ['min' => 0, 'max' => 100, 'step' => 0.1]); ?> %</td>
                    <td>"0" means NO custom VAT rate</td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleCustomVatRate'));?></td>
                </tr>
            </table>
        <?php
        else:
            ?>
            Setting allowed only for main admin.
        <?php
        endif;
        ?>

        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>


    <div class="form">


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-custom-vat-rate',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_46',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_46">46) Hide sender address on notifications & TT page</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'hideSenderAddressOnNotificationAndTt'); ?></td>
                <td>Allows to hide place of origination of package. On SMS/e-mail there will be only name of sender. On TT page the map and sender's countr will be hidden.</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleHideSenderAddressOnNotificationAndTt'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>


    <div class="form">

        <h3 id="set_47">47) SP Zoo status</h3>

        <?php
        if(AdminRestrictions::isUltraAdmin() OR $model->verifyStatus() == User::STATUS_NEW):
            ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-custom-vat-rate',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_47',
        ));
            ?>


            <?php echo $form->errorSummary($model); ?>




            <table class="table">
                <tr>
                    <td><?php echo $form->checkBox($model, 'spZooStatus'); ?></td>
                    <td>Is customer of SP z o.o.</td>
                </tr>
                <tr>
                    <td>
                        <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'attribute' => 'spZooChangeDate',
                            'model' => $model,
                            'options'=>array(
                                'dateFormat'=>'yy-mm-dd',
                            ),
                        ));
                        ?>
                    <td>Date of switching to SP z o.o.</td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleSpZoo'));?></td>
                </tr>
            </table>

            <?php
            $this->endWidget();
            ?>

        <?php
        else:
            ?>
            Setting allowed only for main admin or new accounts.
        <?php
        endif;
        ?>



    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>


    <div class="form">

        <h3 id="set_48">48) Linker import available</h3>


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-custom-vat-rate',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_48',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>


        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'linkerImportAvailable'); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleLinkerImportAvailable'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>


    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_49">49) Hide prices for customer</h3>


        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-hide-prices',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_49',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>


        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'hidePrices'); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleHidePrices'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>


    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_50">50) Set Parent Accounting User</h3>

        <?php

        if($model->isParentAccountingUser()):
            ?>
            This account is parent accounting user for anohter accout(s), so cannot have parent.
        <?php
        else:

            if(AdminRestrictions::isNMMMAdmin()):
                ?>


                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id' => 'set-hide-prices',
                'enableAjaxValidation' => false,
                'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_50',
            ));
                ?>

                <?php echo $form->errorSummary($model); ?>


                <table class="table">
                    <tr>
                        <td><div><?php echo $form->dropDownList($model, 'parentAccountingUser', CHtml::listData(User::model()->findAll('id != :user_id', [':user_id' => $model->id]),'id', 'loginWithId'), ['prompt' => '-none-', 'data-chosen-widget' => 'true']); ?></div></td>
                        <td>If set, this account will be locked when parent account is locked.</td>
                    </tr>
                    <tr>
                        <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'setParentAccountingUser'));?></td>
                    </tr>
                </table>


                <?php
                $this->endWidget();
                ?>


            <?php
            else:
                ?>
                Setting allowed only for main admin.
            <?php
            endif;
        endif;
        ?>



    </div>

    <?php

    $script = "
     $('[data-chosen-widget]').chosen({allow_single_deselect:true});
        $('.chosen-drop').css('width', '300px');
        $('#User_parentAccountingUser_chosen').css('width', '300px');
        $('[data-chosen-widget]').hide();
    ";
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.jquery.min.js');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/vendor-own/chosen/chosen.min.css');
    Yii::app()->clientScript->registerScript('chosen-widget', $script, CClientScript::POS_READY);
    ?>


    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_51">51) Disable dimenstions limits</h3>


        <?php
        if(AdminRestrictions::isUltraAdmin()):
            ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'set-disable-dimension-limits',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_51',
        ));
            ?>

            <?php echo $form->errorSummary($model); ?>


            <table class="table">
                <tr>
                    <td><?php echo $form->checkBox($model, 'disableDimensionLimits'); ?></td>
                    <td>It will activate option #38 as well (Disable calculating dimensional weight)</td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDisableDimensionLimits'));?></td>
                </tr>
            </table>


            <?php
            $this->endWidget();
            ?>

        <?php
        else:
            ?>
            Setting allowed only for main admin.
        <?php
        endif;
        ?>


    </div>


    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_52">52) Manifest generation in courier&postal customer-side scanner sets status</h3>


        <?php
        if(AdminRestrictions::isUltraAdmin()):
            ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-manigest-generation-sets-status',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_52',
        ));
            ?>

            <?php echo $form->errorSummary($model); ?>

            <?php
            Yii::app()->getModule('postal');
            ?>

            <table class="table">
                <tr>
                    <td><?php echo $form->checkBox($model, 'manifestGenerationSetsStatus'); ?></td>
                    <td>If option is active, customer will have option to generate manifest via courier/postal scanner with automatic status assignment "Items processed for dispatch" (#<?= CourierStat::STAT_MANIFEST_GENERATED_CUSTOMER;?> for Courier and #<?= PostalStat::STAT_MANIFEST_GENERATED_CUSTOMER;?> for Postal) for items</td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleManifestGenerationSetsStatus'));?></td>
                </tr>
            </table>


            <?php
            $this->endWidget();
            ?>

        <?php
        else:
            ?>
            Setting allowed only for main admin.
        <?php
        endif;
        ?>


    </div>


    <?php
    //////////////////////////////////////////////////////////////////
    ///
    ?>

    <div class="form">

        <h3 id="set_53">53) Disable ordering new items</h3>


        <?php
        if(AdminRestrictions::isSuperAdmin() || ($model->partner_id && !in_array($model->partner_id, [Partners::PARTNER_SP, Partners::PARTNER_SP_ZOO]))):
            ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_53',
        ));
            ?>

            <?php echo $form->errorSummary($model); ?>


            <table class="table">
                <tr>
                    <td><?php echo $form->checkBox($model, 'disableOrderingNewItems'); ?></td>
                    <td>If selected, it won't be possible to order new item for this account from both customer or administrator side. Rest of account options will remain the same.</td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDisableOrderingNewItems'));?></td>
                </tr>
            </table>


            <?php
            $this->endWidget();
            ?>

        <?php
        else:
            ?>
            Setting allowed only for main admin.
        <?php
        endif;
        ?>


    </div>



    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_54">54) Set DHL DE Custom Account Code Suffix for domestic (DE) orders</h3>

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_54',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>


        <table class="table">
            <tr>
                <td><?php echo $form->textField($model, 'dhlDeCustomAccountCodeSuffix'); ?></td>
                <td>If empty, default will be used</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDhlDeCustomAccountCodeSuffix'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>



    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_55">55) Don't hide sender name for data transmission to DHL DE</h3>

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_54',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>


        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'dhlDeDontHideSenderName'); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDhlDeDontHideSenderName'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>



    </div>



    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_56">56) Don't overwrite sender's address with Nysa for DPD PL SP</h3>

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_56',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'dpdPlSpDisableSenderOverwrite'); ?></td>
                <td>If option is active, real sender's data will be send to DPD PL SP. If not, Nysa address will be send, but real address will still be printed on label</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleDpdPlSpDisableSenderOverwrite'));?></td>
            </tr>
        </table>

        <?php
        $this->endWidget();
        ?>
    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_57">57) Force package's sender's name/company for DHL DE</h3>

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_57',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'forcePackageSenderDataDhlDe'); ?></td>
                <td>If active, sender's company and name will be taken instead of HUB's or custom data from setting #25. If #55 is not active, then it will be only printed on label, not send to DHL API</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleForcePackageSenderDataDhlDe'));?></td>
            </tr>
        </table>

        <?php
        $this->endWidget();
        ?>
    </div>


    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_58">58) Custom package slip footer</h3>

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_58',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <table class="table">
            <tr>
                <td><?php echo $form->textArea($model, 'packageSlipFooter'); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'togglePackageSlipFooter'));?></td>
            </tr>
        </table>

        <?php
        $this->endWidget();
        ?>
    </div>


    <?php
    //////////////////////////////////////////////////////////////////

    /// ?>





    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_59">59) Label annotations</h3>

        <?php
        if(AdminRestrictions::isNMMMAdmin()):
            ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_59',
        ));
            ?>

            <?php echo $form->errorSummary($model); ?>


            <table class="table">
                <tr>
                    <td>Position:</td>
                    <td><?php echo $form->dropDownList($model, 'annotationMode', LabelAnnotor::getModeList(), ['prompt' => '- disabled -']); ?></td>
                    <td>If selected, all labels will have printent additional data on selected side.</td>

                </tr>
                <tr>
                    <td>Text:</td>
                    <td><?php echo $form->textField($model, 'annotationText', LabelAnnotor::getModeList()); ?></td>
                    <td>Static text, that will be printed on all labels</td>
                </tr>
                <tr>
                    <td>Attribute:</td>
                    <td><?php echo $form->dropDownList($model, 'annotationAttribute', LabelAnnotor::getAttributeList(), ['prompt' => '- none -']); ?></td>
                    <td>Attribute which value will be printed on all labels</td>
                </tr>
                <tr>
                    <td colspan="3">Format: <strong>STATIC TEXT ||| ATTRIBUTE VALUE</strong></td>
                </tr>
                <tr>
                    <td colspan="3"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleAnnotationModeAndText'));?></td>
                </tr>
            </table>


            <?php
            $this->endWidget();
            ?>

        <?php
        else:
            ?>
            Setting allowed only for main admin.
        <?php
        endif;
        ?>

    </div>


    <?php
    //////////////////////////////////////////////////////////////////
    ?>

    <div class="form">

        <h3 id="set_60">60) PocztaPolska send days delay</h3>


            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_60',
        ));
            ?>

            <?php echo $form->errorSummary($model); ?>

            <table class="table">
                <tr>
                    <td><?php echo $form->textField($model, 'ppDaysDelay'); ?></td>
                    <td>If not provided, default setting will be taken (days: <?= PocztaPolskaClient::DEFAULT_DAYS_DELAY;?>)</td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'togglePpDaysDelay'));?></td>
                </tr>
            </table>


            <?php
            $this->endWidget();
            ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>


    <div class="form">

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'custom-dpdplsp-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_61',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_61">61) Set custom DPD PL SP subaccount</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->dropDownList($model, 'customDpdPlSPSubaccount', User::listCustomDpdPlSPAccounts(), ['prompt' => '-- default --']); ?></td>
            </tr>
            <tr>
                <td colspan="1"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'togleCustomDpdPlSPSubaccount'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>


    <?php
    //////////////////////////////////////////////////////////////////
    ?>


    <div class="form">

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'custom-dpdplsp-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_62',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_62">62) Multipackages as one item</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->checkBox($model, 'multipackagesAsOneItem'); ?></td>
                <td>If active, only parent packages (1/n) will be visible on lists and generating labels for them will automatically include all children labels. Do not affect API.</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleMultipackagesAsOneItem'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    ?>


    <div class="form">

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'custom-dpdplsp-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_63',
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <h3 id="set_63">63) YunExpress Sender name addition</h3>

        <table class="table">
            <tr>
                <td><?php echo $form->textField($model, 'yunExpressNameAddition'); ?></td>
                <td>Name will be added to the end of sender name: "SP Parcel Distribution / <strong>NAME</strong>" </td>
            </tr>
            <tr>
                <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleYunExpressNameAddition'));?></td>
            </tr>
        </table>


        <?php
        $this->endWidget();
        ?>

    </div>

    <?php
    //////////////////////////////////////////////////////////////////
    if(0):
        ?>

        <div class="form">

            <h3 id="set_53">53) Test mode</h3>


            <?php
            if(AdminRestrictions::isUltraAdmin()):
                ?>

                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'id' => 'set-test-mode',
                'enableAjaxValidation' => false,
                'action' => Yii::app()->createUrl('user/update', ['id' => $model->id]).'#set_53',
            ));
                ?>

                <?php echo $form->errorSummary($model); ?>


                <table class="table">
                    <tr>
                        <td><?php //echo $form->checkBox($model, 'setTestMode'); ?></td>
                        <td>If test mode is active, all label orders will result in OWN labels, orders won't be visible in admin panel and won't be sent to Matrix</td>
                    </tr>
                    <tr>
                        <td colspan="2"><?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'toggleTestMode'));?></td>
                    </tr>
                </table>


                <?php
                $this->endWidget();
                ?>

            <?php
            else:
                ?>
                Setting allowed only for main admin.
            <?php
            endif;
            ?>


        </div>



    <?php
    endif;
    //////////////////////////////////////////////////////////////////
    ?>

</div>
<script>
    $(document).ready(function(){
        $('#settings-form').find('h3[id]').each(function(i,v){
            $('#settings-contents').append('<li><a href="#' + $(v).attr('id') + '">' + $(v).text() + '</a></li>');
        })
    })
</script>