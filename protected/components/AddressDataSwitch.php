<?php

class AddressDataSwitch extends CWidget
{
    public $attrBaseName;

    public function run()
    {
        $this->render('addressDataSwitch',
            array(
                'attrBaseName' => $this->attrBaseName,
            ));
    }
}