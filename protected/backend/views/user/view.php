<?php
/* @var $model User */

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

Yii::app()->getModule('courier');

$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
if($model->getDebtBlockActive()):
    ?>
    <div class="alert alert-error">User account is blocked!</div>
<?php
endif;
?>


<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<h3>Logs</h3>

<table class="list hTop" style="width: 100%; table-layout: fixed;">
    <tr>
        <td style="width: 150px;">Date</td>
        <td style="width: 150px;">Cat</td>
        <td style="width: 150px;">Author</td>
        <td>Text</td>
    </tr>
    <?php
    foreach($model->userLogs AS $item):
        ?>
        <tr>
            <td><?= $item->date_entered;?></td>
            <td><?= $item->getCatName();?></td>
            <td><?= $item->admin_id ? $item->admin->login.' (#'.$item->admin_id.')' : '-';?></td>
            <td><?= $item->text;?></td>
        </tr>
    <?php
    endforeach;?>
</table>
<br/>
<h3>Details</h3>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'login',
        'email',
        'date_entered',
        'date_updated',
        'last_logged',
        'name',
        'surname',
        'country',
        'city',
        'zip_code',
        'address_line_1',
        'address_line_2',
        'tel',
        'nip',
        'status',
        array(
            'label' => 'Premium',
            'value' => $model->getIsPremium()?"yes":"no",
        ),
        array(
            'label' => 'Without VAT',
            'value' => $model->without_vat?"yes":"no",
        ),
        array(
            'label' => 'PayOnAccount',
            'value' => $model->payment_on_invoice?"yes":"no",
        ),
        array(
            'label' => 'Type',
            'value' => $model->type==User::TYPE_PRIVATE?"private":"company",
        ),
        array(
            'label' => 'Currency',
            'value' => Currency::nameById($model->price_currency_id),
        ),
        array(
            'label' => 'Activated',
            'value' => $model->activated?"yes":"no",
        ),
        array(
            'label' => 'Packages',
            'value' => $model->couriersSTAT,
        ),
        array(
            'label' => 'Source domain',
            'value' => $model->sourceDomainName,
        ),
    ),
)); ?>

<h3>VIES</h3>

<?php
if($model->nip):
    ?>

    <a href="#" id="check-vies" class="btn">check if vies is valid</a>

    <script>
        $('#check-vies').on('click', function()
        {
            $.ajax({
                url: '<?= Yii::app()->createUrl('/user/ajaxCheckVies');?>',
                data: { vies: '<?= $model->nip;?>'},
                method: 'POST',
                dataType: 'json',
                success: function(result){

                    if(result == -1)
                        alert('Service in unavailable - please try again later');
                    else if(result)
                        alert('VIES is VALID!');
                    else
                        alert('VIES is NOT valid!')

                },
                error: function(e){

                    console.log(e);
                }
            });
        });
    </script>

<?php
else:
    ?>
    - no vies provided -
<?php
endif;
?>

<br/>
<br/>

<h3>Bank account</h3>

<?php echo $model->getBankAccount() ? TbHtml::well(nl2br(CHtml::encode($model->getBankAccount()))) : '-';?>

<br/>
<br/>

<h3>Member of groups</h3>

<table class="list hTop" style="width: 500px; margin: 0 auto;">
    <tr>
        <td>#</td>
        <td>Name</td>
    </tr>

    <?php /* @var $model User */ ?>
    <?php foreach($model->userGroups AS $item):?>

        <tr>
            <td><?php echo $item->id; ?></td>
            <td><?php echo CHtml::link($item->name, Yii::app()->createUrl('userGroup/view', array('id' => $item->id))); ?></td>
        </tr>

    <?php endforeach; ?>
</table>

<h3>Member of postal groups</h3>

<?php
Yii::app()->getModule('postal');
?>

<table class="list hTop" style="width: 500px; margin: 0 auto;">
    <tr>
        <td>#</td>
        <td>Name</td>
    </tr>

    <?php /* @var $model User */ ?>
    <?php foreach($model->postalUserGroups AS $item):?>

        <tr>
            <td><?php echo $item->id; ?></td>
            <td><?php echo CHtml::link($item->name, Yii::app()->createUrl('postalUserGroup/view', array('id' => $item->id))); ?></td>
        </tr>

    <?php endforeach; ?>
</table>

<hr/>

<h3>Login as user</h3>
<?php

$homeUrl = Yii::app()->homeUrl;
$homeUrl = str_replace('nysa.php', 'index.php', $homeUrl);

$domain = Yii::app()->PV->pageVersionToDomain($model->source_domain);
if($domain !== NULL)
    $homeUrl = 'https://'.$domain.'/index.php';

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'login-as-user',
    'enableAjaxValidation' => false,
    'action' => $homeUrl.'?r=site/adminModeLogin',
    'htmlOptions' => [
        'target' => '_blank',
    ]
));

?>
<?php
echo CHtml::hiddenField('hash', $model->hash);
echo CHtml::hiddenField('hash2', User::preparePasswordHashForAdminModeAuthentication($model->pass));
?>


<div class="alert alert-warning">
    You will be logged out from your current user session.
</div>

<?php echo CHtml::submitButton('Login as '.$model->login, ['class' => 'btn btn-warning']);?>


<?php
$this->endWidget();
?>

<hr/>

<h2>Send Mesages</h2>

<div style="overflow: auto;">
    <div style="width: 48%; float: left;">
        <div class="form">

            <h3>Send Mail</h3>
            <p>Receiver email: <?= $model->getAccountingEmail(true);?></p>

            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'contact-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>false,
                    //'validateOnType' => true,
                ),
            )); ?>
            <?php echo $form->errorSummary($modelEmail); ?>
            <div>
                <?php echo $form->textField($modelEmail,'title', ['placeholder' => $modelEmail->getAttributeLabel('title'), 'style' => 'width: 95%;']); ?>
            </div>

            <div>
                Text:<br/>
                <?php
                $this->widget('ext.editMe.widgets.ExtEditMe', array(
                    'model'=>$modelEmail,
                    'attribute'=>'text',
                    'resizeMode' => 'vertical',
//                    'width' => '580',
                    'baseHref' => Yii::app()->baseUrl.'/',
                    'toolbar' => array(
                        array(
                            'Source', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo', 'Blockquote', '-', 'Table', 'HorizontalRule','SpecialChar',
                        ),
                        array(
                            'Bold', 'Italic', 'Underline', 'FontSize', 'TextColor', 'Link','NumberedList', 'BulletedList', 'Outdent', 'Indent',  'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
                        ),
                    ),
                    'advancedTabs' => false,
                    'ckeConfig' => array(
                        'forcePasteAsPlainText' => true,
                        'allowedContent' => true,
                    )
                ));
                ?>


            </div>
            <br/>
            <div class="text-center">
                <?php echo TbHtml::submitButton(Yii::t('app','Wyślij'), ['class' => 'btn btn-primary btn-lg']); ?>
            </div>
            <?php $this->endWidget(); ?>
        </div><!-- form -->

        <div class="text-center">
            Preview:<br/>
            <img src="<?= Yii::app()->baseUrl;?>/images/backend/mail_form.jpg"/>
        </div>
    </div>
    <div style="width: 48%; float: right;">
        <?php
        if($model->tel):
            ?>
            <div class="form ">

                <h3>Send SMS</h3>
                <p>Receiver tel: <?= $model->tel;?></p>

                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'contact-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                        //'validateOnType' => true,
                    ),
                )); ?>
                <?php echo $form->errorSummary($modelSms); ?>
                <div>
                    <?php echo $form->textField($modelSms,'text', ['placeholder' => $modelSms->getAttributeLabel('text'), 'style' => 'width: 95%;']); ?>
                </div>


                <div class="text-center">
                    <?php echo TbHtml::submitButton(Yii::t('app','Wyślij'), ['class' => 'btn btn-primary btn-lg']); ?>
                </div>
                <?php $this->endWidget(); ?>
            </div><!-- form -->
        <?php
        endif;
        ?>

    </div>
</div>



<h3>Clone account</h3>

<?php
if(AdminRestrictions::isSuperAdmin()):
    ?>


    <?php

    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'clone-account',
        'action' => Yii::app()->createUrl('/user/clone'),
        'enableAjaxValidation' => false,
        'htmlOptions' => [
//            'target' => '_blank',
        ]
    ));

    ?>


    <div class="alert alert-warning">
        New account will be created with the same settings, groups, etc.<br/>New account will automatically be set to SP ZOO<br/>New account will have the same password, but new API key <br/>This account will have payment on invoice disabled.<br/>You can provide custom new account's suffix
    </div>

    New account login: <strong><?= $model->login;?></strong><?= CHtml::textField('suffix', '2', ['style' => 'width: 50px;', 'maxlength' => 2]); ?>
    <br/>

    <?php echo CHtml::submitButton('Clone account '.$model->login, ['class' => 'btn btn-success']);?>


    <?php
    echo CHtml::hiddenField('hash', $model->hash);
    $this->endWidget();
    ?>

<?php
else:
    ?>
    -- You don't have authority to do it --
<?php
endif;
?>

<hr/>







