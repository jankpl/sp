<?php

Yii::import('application.models._base.BaseUserInvoiceDebtOptions');

class UserInvoiceDebtOptions extends BaseUserInvoiceDebtOptions
{
    const STAT_NEW = 1;
    const STAT_DECLARATION = 2;
    const STAT_NO_CONTACT = 3;
    const STAT_REFUSE = 4;
    const STAT_COMPLAINT = 5;
    const STAT_VERIFICATION = 6;
    const STAT_INFO_SENT = 7;
    const STAT_DEBT_COLLECTION_FORBIDDEN_BY_CENTRAL = 8;
    const STAT_DEBT_COURT = 9;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return array(
            array('user_invoice_id, stat', 'required'),
            array('user_invoice_id, stat', 'numerical', 'integerOnly'=>true),
            array('id, user_invoice_id, date_entered, stat', 'safe', 'on'=>'search'),
        );
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => 'date_entered',
            ),
        );
    }

    public static function getStatList()
    {
        return [
            self::STAT_NEW => 'Nowa sprawa',
            self::STAT_DECLARATION => 'Deklaracja',
            self::STAT_NO_CONTACT => 'Brak kontaktu',
            self::STAT_REFUSE => 'Odmowa spłaty',
            self::STAT_COMPLAINT => 'Reklamacja',
            self::STAT_VERIFICATION => 'Weryfikacja',
            self::STAT_INFO_SENT => 'Wysłano wiadomość o zadłużeniu',
            self::STAT_DEBT_COLLECTION_FORBIDDEN_BY_CENTRAL => 'Zakaz windykacji - centrala',
            self::STAT_DEBT_COURT => 'Etap sądowy',
        ];
    }

    public function getStatName()
    {
        return isset(self::getStatList()[$this->stat]) ? self::getStatList()[$this->stat] : '';
    }

    public static function set($status, $user_invoice_id)
    {
        $model = new self;
        $model->stat = $status;
        $model->user_invoice_id = $user_invoice_id;
        return $model->save();
    }
}