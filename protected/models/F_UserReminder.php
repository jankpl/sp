<?php

class F_UserReminder extends CFormModel
{
    public $login;
    public $email;
    public $verifyCode;



    public function rules() {
        return array(
            array('login, email','required'),
            array('login', 'length', 'max'=>20),
            array('email', 'email',  'allowEmpty' => true,),
            array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
        );
    }

    public function attributeLabels() {
        return array(
            'login' => Yii::t('app', 'Login'),
            'email' => Yii::t('app', 'Email'),
            'verifyCode'=>'Verification Code', // we have added this line
        );
    }
}