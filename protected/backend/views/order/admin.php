<h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<p>
    You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'order-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'selectableRows'=>2,
    'columns' => array(
        array(
            'name' => 'local_id',
            'headerHtmlOptions' => array('style' => 'width: 30px;'),
        ),
        array(
            'name' => 'date_entered',
            'value' => 'substr($data->date_entered,0,-3)',
            'headerHtmlOptions' => array('style' => 'width: 100px;'),
        ),
        array(
            'name' => 'date_updated',
            'value' => 'substr($data->date_updated,0,-3)',
            'headerHtmlOptions' => array('style' => 'width: 100px;'),
        ),
        array(
            'name'=>'__username',
            'value'=>'CHtml::link($data->user->login, array("/user/view", "id" => $data->user->id), array("target" => "_blank"))',
            'type' => 'raw',
//            'headerHtmlOptions' => array('style' => 'width: 30px;'),
        ),
        array(
            'name'=>'order_stat_id',
            'value'=>'$data->orderStat->name',
            'filter'=>GxHtml::listDataEx(OrderStat::model()->findAllAttributes(null, true)),
//            'headerHtmlOptions' => array('style' => 'width: 30px;'),
        ),
//        array(
//            'name' => '__product',
//            'header'=>'Product type',
//            'value'=>'$data->product_type_id==1?"Kurier":"HybridMail"',
//            'filter' => CHtml::listData(Order::listProducts(),"id","name"),
//        ),
        array(
            'header'=>'Product',
            'name' => 'product_type',
            'type' => 'raw',
            'value'=> 'Order::getProductName($data->product_type)',
            'filter' => CHtml::listData(Order::listProducts(),"id","name"),
            'headerHtmlOptions' => array('style' => 'width: 130px;'),
        ),
        array(
            'header'=>'Products',
            'name' => 'orderProductSTAT',
            'headerHtmlOptions' => array('style' => 'width: 50px;'),
        ),
        array(
            'name' => '__payment_type',
            'value' => '$data->paymentType->name',
            'filter' => GxHtml::listDataEx(PaymentType::model()->findAllAttributes(null, true)),
            'headerHtmlOptions' => array('style' => 'width: 80px;'),
        ),

        array(
            'header'=>'Netto',
            'name' => 'value_netto',
            'value' => 'S_Price::formatPrice($data->value_netto)',
            'headerHtmlOptions' => array('style' => 'width: 50px;'),
        ),
        array(
            'header'=>'Brutto',
            'name' => 'value_brutto',
            'value' => 'S_Price::formatPrice($data->value_brutto)',
            'headerHtmlOptions' => array('style' => 'width: 50px;'),
        ),
        array(
            'header'=>'Currency',
            'name' => 'currency',
            'filter' => GxHtml::listData(PriceCurrency::model()->findAll(),'id','short_name'),
            'headerHtmlOptions' => array('style' => 'width: 60px;'),
        ),
//        array(
//            'header'=>'Paid',
//            'name' => 'paid',
//            'value' => '$data->paid?"yes":"no"',
//            'filter' => (Array(0 => 'no', 1 => 'yes')),
//            'headerHtmlOptions' => array('style' => 'width: 60px;'),
//        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
            'headerHtmlOptions' => array('style' => 'width: 30px;'),
        ),
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'selected_rows'
        ),
    ),
)); ?>

<h3>Actions for selected</h3>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>


<?php echo TbHtml::tabbableTabs(array(
    array('label' => 'Change statuses', 'active' => true, 'content' => $this->renderPartial('admin_actions_tabs/_change_statuses', array(), true)),
    array('label' => 'Export to file', 'content' => $this->renderPartial('admin_actions_tabs/_export_to_file', array(), true)),
), array('placement' => TbHtml::TABS_PLACEMENT_ABOVE)); ?>



