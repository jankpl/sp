<?php

//http://localhost/SwiatPrzesylek.pl/swiatPrzesylek_ms5/orangePostApi/getShipmentInfo?barcode=3274fbc364d5d463141d9463ebeeb0f4&key=f2A5ba3a8n9ja2az2azf6jUY7631

class OrangePostApiController extends CController
{
    const PASSWORD = 'f2A5ba3a8n9ja2az2azf6jUY7631';

    // LOCAL:
//    const COURIER_STAT_ID_SCANNED = 377;
//    const COURIER_STAT_ID_RETURNED = 378;

    // PRODUCTION:
    const COURIER_STAT_ID_SCANNED = 7838;
    const COURIER_STAT_ID_RETURNED = 7839;

    const SCANNING_POINT_ID = 5;

    public function filters()
    {
        return array_merge(array(
            array(
                'application.filters.HttpsFilter',
                'bypass' => false,
            ),
        ));
    }

    protected static function checkKey($key)
    {
        MyDump::dump('orangePostApi.txt', substr($key,0,10));

        if($key != self::PASSWORD)
        {
            echo CJSON::encode([
                'success' => false,
                'error' => 'Wrong key...',
                'response' => '',
            ]);

            Yii::app()->end();
        }
    }

    public function actionGetShipmentInfo($barcode, $key)
    {
        MyDump::dump('orangePostApi.txt', 'getShipmentInfo : '.$barcode);

        self::checkKey($key);

        $success = false;
        $error = '';
        $response = '';

        Yii::app()->getModule('courier');


        $models = CourierExternalManager::findCourierIdsByRemoteId($barcode, true);

        if(!sizeof($models))
        {
            $error = 'Package not found!';

        } else {
            $success = true;


            if(sizeof($models) > 1)
            {
                $temp = [];
                foreach($models AS $courier)
                    $temp[$courier->id] = $courier;

                krsort($temp);
                $models = $temp;
            }

            foreach($models AS $courier) {
                $temp = [
                    'weight' => $courier->package_weight,
                    'international_tracking_code' => NULL,
//                    'international_tracking_code' => $courier ? $courier->_glsnl_t400 : NULL,
                    'receiver' => [
                        'name' => $courier->receiverAddressData->getUsefulName(true),
                        'address' => trim($courier->receiverAddressData->address_line_1 . ' ' . $courier->receiverAddressData->address_line_2),
                        'zip_code' => $courier->receiverAddressData->zip_code,
                        'city' => $courier->receiverAddressData->city,
                        'country' => $courier->receiverAddressData->country0->code,
                    ],
                    'sender' => [
                        'name' => $courier->senderAddressData->getUsefulName(true),
                        'address' => trim($courier->senderAddressData->address_line_1 . ' ' . $courier->senderAddressData->address_line_2),
                        'zip_code' => $courier->senderAddressData->zip_code,
                        'city' => $courier->senderAddressData->city,
                        'country' => $courier->senderAddressData->country0->code,
                    ],
                    'reference' => $courier->local_id,
                ];

                if(sizeof($models) == 1)
                    $response = $temp;
                else
                {
                    if(!is_array($response))
                        $response = [];

                    $response[] = $temp;
                }
            }
        }

        echo CJSON::encode([
            'success' => $success,
            'error' => $error,
            'response' => $response,
        ]);
    }

    public function actionSetScanned($barcode, $key)
    {
        self::checkKey($key);

        MyDump::dump('orangePostApi.txt', 'setScanner : '.$barcode);

        self::setStatus($barcode, self::COURIER_STAT_ID_SCANNED);
    }

    public function actionSetReturned($barcode, $key)
    {
        self::checkKey($key);

        MyDump::dump('orangePostApi.txt', 'setReturned : '.$barcode);

        self::setStatus($barcode, self::COURIER_STAT_ID_RETURNED);
    }

    protected function setStatus($barcode, $status_id)
    {
        Yii::app()->getModule('courier');

        $success = false;
        $error = '';
        $response = '';

        Yii::app()->getModule('courier');


        $model = CourierExternalManager::findCourierIdsByRemoteId($barcode, true);

        if(sizeof($model) > 1) {
            $error = 'More than 1 package found!';
        }
        else if(!sizeof($model))
        {
            $error = 'Package not found!';

        } else {
            $courier = array_shift($model);

            if($courier->isStatChangeByTtAllowed(true) && $courier->courier_stat_id != $status_id) {

                if($courier->changeStat($status_id, CourierStat::EXTERNAL_SERVICE_ORANGE_POST_API, false, false, 'NL', false, false, false, false, false,self::SCANNING_POINT_ID))
                    $success = true;
            }
        }

        if(!$success && $error == '')
            $error = 'Status not changed.';

        echo CJSON::encode([
            'success' => $success,
            'error' => $error,
            'response' => $response,
        ]);
    }
}

