<?php
/* @var $form CActiveForm */
/* @var $model AddressData */
/* @var $i Integer */
/* @var $noEmail boolean */

?>


<?php echo $form->errorSummary($model); ?>


<?php $this->renderPartial('/_addressData/_form_base',
    array('i' => $i, 'model' => $model, 'form' => $form, 'countries' => $countries, 'noEmail' => $noEmail));
?>


<p class="note">
    <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
</p>