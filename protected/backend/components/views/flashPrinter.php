<?php
foreach (Yii::app()->user->getFlashes() as $key => $message):
    ?>
    <div class="alert alert-<?= $key;?>"><?= $message;?></div>
    <?php
endforeach;
?>
