<?php

class CourierLogController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {

        $model = new CourierLog('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierLog']))
            $model->setAttributes($_GET['CourierLog']);

        $this->render('index', array(
            'model' => $model,
        ));
    }


}