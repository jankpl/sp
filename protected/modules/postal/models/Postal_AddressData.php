<?php

class Postal_AddressData extends AddressData
{
    public $smsNotification = false;

    public $addToContactBook;
    public $contactBookType;

    public $_postal;

    public function beforeValidate()
    {
        // NO LONGER LOWER LIMITS FOR OWE
//        if($this->_postal !== NULL)
//        {
//            if($this->_postal->postal_type == Postal::POSTAL_TYPE_REG)
//            {
//               $attrs = ['address_line_1', 'address_line_2', 'name', 'company'];
//                foreach($attrs AS $attr)
//                {
//                    if(strlen($this->$attr) > 30)
//                        $this->addError($attr, Yii::t('postal', 'Maksymalna długość tego pola to {n} znaków.', ['{n}' => 30]));
//                }
//
//                if(strlen($this->city) > 20)
//                    $this->addError('city', Yii::t('postal', 'Maksymalna długość tego pola to {n} znaków.', ['{n}' => 20]));
//
//                if(strlen($this->zip_code) > 10)
//                    $this->addError('zip_code', Yii::t('postal', 'Maksymalna długość tego pola to {n} znaków.', ['{n}' => 10]));
//            }
//        }

        return parent::beforeValidate();
    }

    public function rules() {

        $arrayValidate = array(

//            array('address_line_1',
//                'match',
//                'pattern' => '/^(.+)\s(.+)$/',
//                'message' => Yii::t('site', 'Adres musi zawierać ulicę oraz oddzielony spacją budynek i lokal'),
////                'except' => Postal::MODE_SAVE_NO_VALIDATE,
//                'enableClientValidation' => true
//            ),

            array('addToContactBook, contactBookType', 'safe'),

            array('email', 'email'),

            array('name, company, city, address_line_1, address_line_2, zip_code', 'application.validators.lettersNumbersBasicValidator'),
            array('tel', 'application.validators.telValidator'),

            array('country_id, city, address_line_1, zip_code', 'required'),

            array('name', 'application.validators.eitherOneValidator', 'other' => 'company'),

            array('smsNotification', 'boolean', 'allowEmpty' => true),

            array('name,company,country,zip_code,city,address_line_1,address_line_2,tel', 'length', 'max'=>45),

            array('email', 'length', 'max'=>45),

            array('country_id', 'safe'),

            array('id,name,company,country,zip_code,city,address_line_1,address_line_2,tel, email,', 'safe', 'on'=>'search'),

            array(implode(',', array_keys(self::getAttributes())),'safe', 'on' => 'clone'),
        );


        $arrayFilter =
            array(
                array('country_id, date_entered, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'filter', 'filter' => array( $this, 'filterStripTags')),

                array('date_entered', 'default',
                    'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

                array('country_id, name, company, country, zip_code, city, address_line_1, address_line_2, tel, email', 'default', 'setOnEmpty' => true, 'value' => null),
            );

        return array_merge($arrayFilter, $arrayValidate);
    }

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
//                'bDefaultTelNumberAddressData' =>
//                    [
//                        'class'=>'application.modules.courier.models.bDefaultTelNumberAddressData'
//                    ],
//                'bClearEmailAddressData' =>
//                    [
//                        'class'=>'application.models.bClearEmailAddressData'
//                    ],
//                'bValidateZipCodeWithBaseAddressData' =>
//                    [
//                        'class'=>'application.models.bValidateZipCodeWithBaseAddressData'
//                    ],
                'bFilterTelNumberAddressData' =>
                    [
                        'class'=>'application.models.bFilterTelNumberAddressData'
                    ],
            ]
        );
    }

    public function attributeLabels()
    {

        return CMap::mergeArray(
            [
                'addToContactBook' => Yii::t('_addressData', 'Dodaj do swojej książki adresowej'),
                'smsNotification' => Yii::t('sms_courier', 'SMS Notifications'),
            ],
            parent::attributeLabels());
    }

    /**
     * List attributes including non-db attributes by default
     * @param bool|true $names
     * @param bool|false $omitNonDb Do not include special attributes
     * @return array
     */
    public function getAttributes($names = true, $omitNonDb = false)
    {
        $attributes = parent::getAttributes($names);
        if(!$omitNonDb) {
            $attributes['addToContactBook'] = $this->addToContactBook;
            $attributes['contactBookType'] = $this->contactBookType;
            $attributes['smsNotification'] = $this->smsNotification;
        }

        return $attributes;
    }

    /**
     * Save to DB with optional automatic save to contact book
     *
     * @param bool|true $runValidation
     * @param null $attributes
     * @return bool
     */
    public function save($runValidation=true,$attributes=null)
    {
        if($this->addToContactBook)
        {
            UserContactBook::createAndSave(false, $this->getAttributes(true, true), $this->contactBookType);
        }

        return parent::save($runValidation,$attributes);
    }

    /**
     * Constructor allowing setting userContactBook type for future saving (whether it's sender or receiver)
     *
     * @param string $scenario
     * @param string|false $contactBookType
     */
    public function __construct($scenario='insert', $contactBookType = false)
    {
        if($contactBookType)
            $this->contactBookType = $contactBookType;
        return parent::__construct($scenario);
    }

    public function beforeSave()
    {

        if(!Yii::app()->user->isGuest && !Yii::app()->user->getModel()->getPostalAvailableSms())
            $this->smsNotification = false;

        if($this->smsNotification && $this->tel != '')
        {
            $this->setSmsNotifyActive();
        }

        return parent::beforeSave();
    }


}