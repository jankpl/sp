<?php

Yii::import('application.modules.courier.models._base.BaseCourierUPriceItem');

class CourierUPriceItem extends BaseCourierUPriceItem
{
    public $_openedRevision;

    const SCENARIO_VALIDATE_PRICE = 'svp';

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function afterConstruct()
    {
        $this->pricePerItem = new Price();

        return parent::afterConstruct();
    }

    public function rules() {
        return array(
            array('weight_top_limit', 'required'),
            array('price_per_item, courier_u_price_group_id', 'required', 'except' => self::SCENARIO_VALIDATE_PRICE),
            array('courier_u_price_group_id, weight_top_limit, price_per_item', 'numerical', 'integerOnly'=>true),
            array('id, courier_u_price_group_id, weight_top_limit, price_per_item', 'safe', 'on'=>'search'),
        );
    }

    public function getCurrentRevision()
    {
        $cmd = Yii::app()->db->createCommand();
        return $cmd->select('MAX(revision)')->from($this->tableName())->where(['courier_u_price_group_id' => $this->courier_u_price_group_id])->queryScalar();
    }

//    public function beforeSave()
//    {
//        $cmd = Yii::app()->db->createCommand();
//        $revision = $cmd->select('MAX(revision)')->from($this->tableName())->where(['courier_u_price_group_id' => $this->courier_u_price_group_id])->queryScalar();
//        $revision++;
//
//        $this->revision = $revision;
//
//        return parent::beforeSave();
//    }

    public function saveWithChildren($startNewTransaction = false)
    {
        $errors = false;

        if($startNewTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        if(!$errors) {
            if (!$this->pricePerItem->saveWithPrices(false))
                $errors = true;
            else
            {
                $this->price_per_item = $this->pricePerItem->id;
                if(!$this->save())
                    $errors = true;
            }
        }

        if($startNewTransaction)
        {
            if($errors)
                $transaction->rollback();
            else
                $transaction->commit();
        }

        return $errors ? false : true;
    }


}