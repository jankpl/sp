<?php

class UserDebtCollection extends UserInvoice
{

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function attributeLabels() {
        $array =  array(
            '_daysAfterPayment' => 'Overdue days',
        );

        $array = array_merge(parent::attributeLabels(), $array);

        return $array;
    }

    public function daysAfterPayment()
    {

        if($this->isPaid() OR $this->stat_inv == UserInvoice::INV_STAT_CANCELLED)
            return '-';

        $now = new DateTime();
        $paymentDate = new DateTime($this->inv_payment_date);

        return $now->diff($paymentDate)->format("%a");
    }

    public function isPaidWithPartialName()
    {
        if($this->paid_value >= $this->inv_value)
            return 'yes';
        else  if($this->paid_value < $this->inv_value && $this->paid_value > 0)
            return 'partialy';
        else
            return 'no';
    }

}