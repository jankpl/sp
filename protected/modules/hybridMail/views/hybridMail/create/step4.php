<?php

/* @var $model HybridMail */
/* @var $this HybridMailController */
/* @var $modelHybridMailAdditionList HybridMailAdditionList[] */
/* @var $hybridMailAddition HybridMailAddition[] */
/* @var $form CActiveForm */


?>

<div class="form">

    <h2><?php echo Yii::t('hybridMail','HybridMail');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '4/6')); ?> | <?php echo Yii::t('app','Dodatki'); ?></h2>

    <?php

    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'hybrid-mail-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
    ));

    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <table class="list hTop" style="margin: 0 auto; width: 100%;">
        <tr>
            <td rowspan="2"><?php echo Yii::t('hybridMail','Wybierz');?></td>
            <td rowspan="2"><?php echo Yii::t('hybridMail','Dodatek');?></td>
            <td colspan="3" style="text-align: center;"><?php echo Yii::t('hybridMail','Cena netto/brutto');?></td>
        </tr>
        <tr>
            <td><?php echo Yii::t('hybridMail','Pierwsza strona');?></td>
            <td><?php echo Yii::t('hybridMail','Kolejna strona');?></td>
            <td><?php echo Yii::t('hybridMail','Łącznie za 1 HM');?></td>
        </tr>
    <?php

    foreach($modelHybridMailAdditionList AS $item):
?>
        <tr class="addition_item">
            <td class="with-checkbox">
                <?php echo Chtml::checkBox('HybridMailAddition[]', in_array($item->id,$hybridMailAddition), array('value' => $item->id)); ?>
            </td>
            <td style="width: 60%;">
                <h3><label style="font-weight: normal; text-align: left;"><?php echo $item->hybridMailAdditionListTr->title;?></label></h3>
                <p><?php echo $item->hybridMailAdditionListTr->description; ?></p>
            </td>
            <td>
                <?php
                echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice('first'), true)).' '.Yii::app()->PriceManager->getCurrencySymbol().'/'.S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($item->getPrice('first'))).' '.Yii::app()->PriceManager->getCurrencySymbol();
                ?>
            </td>
            <td>
                <?php
                echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->getPrice('next'), true)).' '.Yii::app()->PriceManager->getCurrencySymbol().'/'.S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($item->getPrice('next'))).' '.Yii::app()->PriceManager->getCurrencySymbol();
                ?>
            </td>
            <td>
                <?php
                echo S_Price::formatPrice(Yii::app()->PriceManager->getFinalPrice($item->calculatePriceByPages($model->getTotalPages(), $currency), true)).' '.Yii::app()->PriceManager->getCurrencySymbol().'/'.S_Price::formatPrice( Yii::app()->PriceManager->getFinalPrice($item->calculatePriceByPages($model->getTotalPages(), $currency))).' '.Yii::app()->PriceManager->getCurrencySymbol();
                ?>
            </td>
        </tr>

<?php
    endforeach;

    ?>
    </table>

    <?php
    Yii::app()->clientScript->registerCoreScript('jquery');
    echo CHtml::script('
    $(document).ready(function(){
        $(".addition_item").children().not(".with-checkbox").css("cursor", "pointer");
        $(".addition_item").children().not(".with-checkbox").click(function(){
             var $checkBox = $(this).parent().find("input[type=checkbox]");
            $checkBox.prop("checked", !$checkBox.prop("checked"));
        });

    });
    ');

    ?>

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Wstecz'),array('name'=>'step3', 'class' => 'btn-small'));
        echo ' ';
        echo TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'step5', 'class' => 'btn-primary'));
        $this->endWidget();
        ?>
    </div>

</div><!-- form -->