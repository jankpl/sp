<?php

class CourierExternalReturnController extends Controller {


    public function beforeAction($action)
    {

        $this->panelLeftMenuActive = 151;

        if(Yii::app()->user->model->getCourierExternalReturnAvailable())
        {
            return parent::beforeAction($action);
        } else {
            $this->render('//site/notify', array(
                'text' => Yii::t('courier', 'Twoje konto nie posiada uprawnień do tej usługi.'),
                'header' => 'Courier',

            ));
            exit;
        }
    }

    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function accessRules()
    {
        return array(

            array('allow',
//                'actions'=>array('create','created','index', 'list','ajaxGetOperatorList', 'ajaxGetCodCurrency', 'ajaxCheckRemoteArea', 'ajaxCheckAdditions'),
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect(['/courier/courierExternalReturn/create']);
    }

    public function actionCreate($hash = false)
    {
        if(Yii::app()->session['courierExternalReturnFormFinished']) // prevent going back and sending the same form again
        {
            $_POST = null;
            $this->clearPageStates();
            unset(Yii::app()->session['courierExternalReturnFormFinished']);
            $this->refresh(true);
        }


        //for AJAX validation
//        {
//            $model=new Courier('insert');
//            $this->performAjaxValidation($model, 'courier-form');
//            unset($model);
//        }

        if(isset($_POST['summary']))
            $this->create_step_summary(); // Validate addition list, summary
        elseif (isset($_POST['finish']) OR isset($_POST['finish_goback']))
            $this->create_finish(isset($_POST['finish_goback']) ? true : false);
        else
            $this->create_step_1();
    }


    protected function create_step_1($validate = false)
    {
        if($validate)
        {
            if($this->create_validate_step_1() == 'break')
                return;
        }

        $this->setPageState('form_started', true);
        $this->setPageState('start_user_id', Yii::app()->user->id);

        $this->setPageState('last_step',1); // save information about last step

        $model = new Courier_CourierTypeExternalReturn();
        $model->packages_number = 1;

        $model->user_id = Yii::app()->user->id;

        $model->courierTypeExternalReturn = new CourierTypeExternalReturn();
        $model->courierTypeExternalReturn->courier = $model;

        $model->courierTypeExternalReturn->courierLabelNew = new CourierLabelNew_CourierExternalReturn();

        $model->receiverAddressData = new CourierTypeExternalReturn_AddressData();
        $model->senderAddressData = new CourierTypeExternalReturn_AddressData();

        $model->receiverAddressData = Yii::app()->user->getModel()->getReturnAddressModel(true, $model->receiverAddressData);

        $model->attributes = $this->getPageState('step1_model',[]);
        $model->courierTypeExternalReturn->attributes = $this->getPageState('step1_courierTypeExternalReturn', []);
        $model->courierTypeExternalReturn->courierLabelNew->attributes = $this->getPageState('step1_courierLabelNew', []);
        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);


        $this->setPageState('last_step',1); // save information about last step

//            $model->courierTypeExternalReturn->_selected_operator_id = $model->courierTypeExternalReturn->courier_u_operator_id;

        $this->render('create/step1',
            [
                'model' => $model,
            ]);

    }

    protected function create_validate_step_1()
    {

        $model = new Courier_CourierTypeExternalReturn();
        $model->courierTypeExternalReturn = new CourierTypeExternalReturn();
        $model->courierTypeExternalReturn->courierLabelNew = new CourierLabelNew_CourierExternalReturn();
        $model->senderAddressData = new CourierTypeExternalReturn_AddressData();
        $model->receiverAddressData = new CourierTypeExternalReturn_AddressData();

        $model->user_id = Yii::app()->user->id;

        if (isset($_POST['Courier_CourierTypeExternalReturn'])) {

            $errors = false;

            $model->setAttributes($_POST['Courier_CourierTypeExternalReturn']);
            $model->senderAddressData->setAttributes($_POST['CourierTypeExternalReturn_AddressData']['sender']);
            $model->receiverAddressData->setAttributes($_POST['CourierTypeExternalReturn_AddressData']['receiver']);
            $model->courierTypeExternalReturn->courierLabelNew->track_id = ($_POST['CourierLabelNew_CourierExternalReturn']['track_id']);
            $model->courierTypeExternalReturn->courierLabelNew->_ext_operator_name = ($_POST['CourierLabelNew_CourierExternalReturn']['_ext_operator_name']);

            if (!$model->validate())
                $errors = true;

            if (!$model->courierTypeExternalReturn->validate())
                $errors = true;

            if (!$model->senderAddressData->validate())
                $errors = true;

            if (!$model->receiverAddressData->validate())
                $errors = true;

            if (!$model->courierTypeExternalReturn->courierLabelNew->validate())
                $errors = true;

            $this->setPageState('step1_model',$model->getAttributes(array_merge(array_keys($_POST['Courier_CourierTypeExternalReturn']),[])));  // save previous form into form state
            $this->setPageState('step1_courierLabelNew',$model->courierTypeExternalReturn->courierLabelNew->getAttributes(array_merge(array_keys($_POST['CourierLabelNew_CourierExternalReturn']),[]))); // save previous form into form state
            $this->setPageState('step1_senderAddressData', $model->senderAddressData->getAttributes(array_keys($_POST['CourierTypeExternalReturn_AddressData']['sender']))); // save previous form into form state
            $this->setPageState('step1_receiverAddressData',$model->receiverAddressData->getAttributes(array_keys($_POST['CourierTypeExternalReturn_AddressData']['receiver']))); // save previous form into form state

               if($errors)
            {
                $this->render('create/step1', [

                    'model' => $model,
                ]);
                return 'break';
            }
        }
    }

    protected function create_step_summary()
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');

        $lastStep = $this->getPageState('last_step');

        switch($lastStep)
        {
            case 1 :
                if($this->create_validate_step_1() == 'break')
                    return;
                break;
        }

        $model = new Courier_CourierTypeExternalReturn();
        $model->courierTypeExternalReturn = new CourierTypeExternalReturn(CourierTypeExternalReturn::SCENARIO_SUMMARY);
        $model->courierTypeExternalReturn->courierLabelNew = new CourierLabelNew_CourierExternalReturn();
        $model->senderAddressData = new CourierTypeExternalReturn_AddressData();
        $model->receiverAddressData = new CourierTypeExternalReturn_AddressData();

        $model->attributes = $this->getPageState('step1_model',[]);
//        $model->courierTypeExternalReturn->attributes = $this->getPageState('step1_courierTypeExternalReturn', []);
        $model->courierTypeExternalReturn->courierLabelNew->attributes = $this->getPageState('step1_courierLabelNew', []);
        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData', []);
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData', []);
        $model->courierTypeExternalReturn->courier = $model;

        $model->user_id = Yii::app()->user->id;

        $this->setPageState('last_step', 'summary'); // save information about last step

        $this->render('create/summary',array(
            'model'=>$model,
        ));
    }
//
    protected function create_validate_summary()
    {
        $error = false;

        $model = new Courier_CourierTypeExternalReturn;
        $model->attributes = $this->getPageState('step1_model',array());
        $model->courierTypeExternalReturn = new CourierTypeExternalReturn(CourierTypeExternalReturn::SCENARIO_SUMMARY);
        $model->courierTypeExternalReturn->attributes = $this->getPageState('step1_CourierTypeExternalReturn',array());

        $model->courierTypeExternalReturn->courierLabelNew = new CourierLabelNew_CourierExternalReturn();

        $model->senderAddressData = new CourierTypeExternalReturn_AddressData();
        $model->receiverAddressData = new CourierTypeExternalReturn_AddressData();

        $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());
        $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData',array());

        $model->user_id = Yii::app()->user->id;

        $model->courierTypeExternalReturn->regulations = $_POST['CourierTypeExternalReturn']['regulations'];
        $model->courierTypeExternalReturn->regulations_rodo = $_POST['CourierTypeExternalReturn']['regulations_rodo'];
        $model->courierTypeExternalReturn->validate(array('regulations', 'regulations_rodo'));

        $model->courierTypeExternalReturn->courier = $model;

        if($model->hasErrors() OR $model->courierTypeExternalReturn->hasErrors() OR $error)
        {

            $this->render('create/summary',array(
                'model'=>$model,
            ));
            return 'break';
        }

    }

    protected function create_finish($goBack = true)
    {
        if($this->getPageState('start_user_id') !=  Yii::app()->user->id)
            throw new CHttpException(403, 'Sesja została przerwana!');


        $lastStep =  $this->getPageState('last_step');
        if($lastStep === 'summary')
        {

            if($this->create_validate_summary() == 'break')
                return;

            $this->setPageState('last_step',3); // save information about last step

            $model = new Courier_CourierTypeExternalReturn;
            $model->attributes = $this->getPageState('step1_model',array());
            $model->courierTypeExternalReturn = new CourierTypeExternalReturn('step_1');
            $model->courierTypeExternalReturn->attributes = $this->getPageState('step1_CourierTypeExternalReturn',array());
            $model->courierTypeExternalReturn->courier = $model;
            $model->senderAddressData = new CourierTypeExternalReturn_AddressData();
            $model->receiverAddressData = new CourierTypeExternalReturn_AddressData();

            $model->courierTypeExternalReturn->courierLabelNew = new CourierLabelNew();

            $model->senderAddressData->attributes = $this->getPageState('step1_senderAddressData',array());
            $model->receiverAddressData->attributes = $this->getPageState('step1_receiverAddressData',array());

            $model->courierTypeExternalReturn->courier = $model;

            $model->user_id = Yii::app()->user->id;

            try
            {
                $return = $model->courierTypeExternalReturn->savePackage();
            }
            catch(Exception $ex)
            {
                Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
                throw new CHttpException(403, 'Wystapił nieznany błąd!');
            }

            if($return)
            {


                // to get proper Hash
                $model->courierTypeExternalReturn->courier->refresh();

                if($goBack)
                    Yii::app()->user->setFlash('success', Yii::t('order','Zamówienie {no} zostało sfinalizowane! {list}', ['{no}' => CHtml::link('#'.$model->local_id, Yii::app()->createUrl('/courier/courier/view', ['urlData' => $model->hash]) ,['target' => '_blank', 'class' => 'btn btn-sm']), '{list}' => CHtml::link(Yii::t('order', 'Lista paczek'), Yii::app()->createUrl('/courier/courier/list', ['#' => 'list']) ,['target' => '_blank', 'class' => 'btn btn-sm'])]));

//                    Yii::app()->user->setFlash('previousModel', Yii::t('courier', 'Paczka została utworzona!{br}{br}{a}szczegóły paczki{/a}', array('{br}' => '<br/>', '{a}' => '<a href="' . Yii::app()->createUrl("/courier/courier/view", ["urlData" => $temp->hash]) . '" class="btn">', '{/a}' => '</a>')));


                // get id's of just created packages
                $_justCreated = [];
                $_justCreated[] = $model->id;

                self::addToListOfJustCreated($_justCreated);

                if($goBack)
                    $this->redirect(array('/courier/courierExternalReturn/create'));
                else {
                    $model->refresh(); // to get hash
                    $this->redirect(array('/courier/courier/view', 'urlData' => $model->hash, 'new' => true));
                }


            } else
                throw new CHttpException(403, 'Wystapił nieznany błąd!');

        } else {

            throw new CHttpException(403, 'Sesja została przerwana!');

        }
    }

    protected static function addToListOfJustCreated(array $packagesIds)
    {

        $justCreated = Yii::app()->session['externaReturnJustCreated'];
        $justCreated = CMap::mergeArray($justCreated, $packagesIds);
        Yii::app()->session['externaReturnJustCreated'] = $justCreated;
    }



}