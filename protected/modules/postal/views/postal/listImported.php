<?php
/* @var $import PostalImportManager */
/* @var $postals Postal[] */
/* @var $mainInstance boolean */
?>

<?php
if(!$mainInstance):
    ?>
    <div class="alert alert-warning text-center"><?= Yii::t('postal', 'Ten import jest prawdopodobnie otwarty w innym oknie. W tej instancji zostało zablokwane automatyczny podgląd postępu. Wróć do głównego okna lub ręcznie odświeżaj stronę, aby zobaczyć postęp.');?></div>
<?php
endif;
?>

    <div class="text-center">
        <?php
        if($import->source == PostalImportManager::SOURCE_EBAY):
            ?>
            <a href="<?php echo Yii::app()->createUrl('/postal/postal/ebay');?>" class="btn btn-warning"><?php echo Yii::t('postal', 'Od nowa');?></a>
        <?php
        elseif($import->source == PostalImportManager::SOURCE_LINKER):
            ?>
            <a href="<?php echo Yii::app()->createUrl('/postal/postal/linker');?>" class="btn btn-warning"><?php echo Yii::t('postal', 'Od nowa');?></a>
        <?php
        else:
            ?>
            <a href="<?php echo Yii::app()->createUrl('/postal/postal/import');?>" class="btn btn-warning"><?php echo Yii::t('postal', 'Od nowa');?></a>
        <?php
        endif;
        ?>
        <br/>
    </div>


    <hr/>

<?php
$this->widget('FlashPrinter');
?>
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'generate-labels',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/getLabels', ['hash' => $hash]),
    'htmlOptions' => ['class' => 'do-not-block',
        'target' => Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL ? '_blank' : '_self'
    ],
));
?>
    <div class="alert alert-info"><?= Yii::t('postal', 'Zamknięcie tej strony nie spowoduje anulowania zamawiania paczek.');?></div>

    <table class="table table-striped table-condensed" style="width: 60%; margin: 0 auto;">
        <tr>
            <td>#</td>
            <td><?php echo Yii::t('postal', 'Local Id');?></td>
            <td style="text-align: center;"><?php echo Yii::t('label', 'Label ready');?></td>
            <?php if($import->source == PostalImportManager::SOURCE_EBAY):?>
                <td style="text-align: center;"><?php echo Yii::t('label', 'eBay ready');?></td>
            <?php elseif($import->source == PostalImportManager::SOURCE_LINKER):?>
                <td style="text-align: center;"><?php echo Yii::t('label', 'Linker ready');?></td>
            <?php endif; ?>
            <td style="text-align: center;"><?php echo Yii::t('label', 'Get label');?> <a href="#" data-toggle-labels="true"><i class="glyphicon glyphicon-check"></i></a></td>
        </tr>
        <?php
        $i = 1;
        $erroredKeys = [];
        if(is_array($postals))
            foreach($postals AS $key => $item):
                $dataAction = 'false';


                if($item->postalLabel !== NULL && $item->postalLabel->stat == PostalLabel::STAT_NEW)
                    $dataAction = 'true';

                if($item->postalLabel !== NULL && $item->postalLabel->stat == PostalLabel::STAT_FAILED)
                    $erroredKeys[] = $key;
                ?>
                <tr data-postal-local-id="<?= $item->local_id;?>" data-key="<?= $key;?>" data-label-stat="<?= $item->getUsefulLabelStat();?>" data-postal-temp-id="<?= $item->id;?>" data-action="<?= $dataAction;?>">
                    <td><?= $i;?></td>
                    <td><a href="<?php echo Yii::app()->createUrl('/postal/postal/view', ['urlData' => $item->hash]);?>" target="_blank"><?= $item->local_id;?></a></td>
                    <td class="label-stat" style="text-align: center;"><?= $item->getUsefulLabelStat() == PostalLabel::STAT_SUCCESS ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'?></td>
                    <?php if($import->source == PostalImportManager::SOURCE_EBAY OR $import->source == PostalImportManager::SOURCE_LINKER):?>
                        <td class="ebay-stat" style="text-align: center;">
                            <?php
                            if(($item->getUsefulLabelStat() == PostalLabel::STAT_NEW)):
                                ?>
                                <i class="glyphicon glyphicon-remove"></i>
                            <?php
                            else:
                                ?>
                                <?php
                                if($item->ebayImport->stat == EbayImport::STAT_SUCCESS) echo '<i class="glyphicon glyphicon-ok"></i>';
                                elseif ($item->ebayImport->stat == EbayImport::STAT_NEW) echo '<img src="'.Yii::app()->baseUrl.'/images/layout/preloader.gif" style="width: 10px; height: 10px;"/>';
                                else echo '<i class="glyphicon glyphicon-remove"></i>';?>
                            <?php
                            endif;
                            ?>
                        </td>
                    <?php endif; ?>
                    <td class="label-checkbox" style="text-align: center;"><?= $item->getUsefulLabelStat() == PostalLabel::STAT_NEW ? '<img src="'.Yii::app()->baseUrl.'/images/layout/preloader.gif" style="width: 10px; height: 10px;"/>' : ($item->getUsefulLabelStat() == PostalLabel::STAT_FAILED ? '<i class="glyphicon glyphicon-remove" title="'.Yii::t('postal','Wystąpił błąd przy zamawianiu etykiety!').'"></i>' : '<input type="checkbox" data-get-label="true" data-label-checkbox="true" name="label['.$item->id.']"/>');?></td>
                </tr>

                <?php
                $i++;
            endforeach; ?>
    </table>
    <hr/>
    <div class="text-center" id="generate-labels-action">
        <?php echo CHtml::dropDownList('Postal[type]','',PostalLabelPrinter::getTypeNames(), array('style' => 'width: 250px; display: inline-block;'));?>

        <?php
        echo TbHtml::submitButton('Generate labels', array('data-generate-labels' => 'true', 'class' => 'btn btn-primary'));
        ?>


    </div>
<?php
if($mainInstance):
    ?>
    <div class="text-center" id="work-in-progress">
        <img src="<?php echo Yii::app()->baseUrl;?>/images/layout/preloader.gif" />
        <h3>0/<?php echo S_Useful::sizeof($postals);?></h3>
    </div>
<?php
endif;
?>
<?php
$this->endWidget();
?>

    <div id="errored-keys-container" style="display: none;" class="text-center">
        <br/>
        <br/>
        <?= CHtml::form(Yii::app()->createUrl('/postal/postal/exportImportedWithError', ['hash' => $hash]));?>
        <input type="hidden" id="erorred-keys" name="errored-keys" value="<?= implode(',', $erroredKeys);?>" />
        <input type="submit" value="<?= Yii::t('label', 'Export input data for failed items');?>" class="btn btn-warning"/>
        <?= CHtml::endForm();?>
    </div>

    <script>
        $(document).ready(function() {
            var propToggleTrue = true;
            $('[data-toggle-labels]').on('click', function () {

                $('[data-label-checkbox]').each(function (i, v) {
                    $(v).prop("checked", propToggleTrue);
                });

                propToggleTrue = !propToggleTrue;
            })
        });
    </script>

<?php
if($mainInstance):
    ?>

    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/postalImport.js', CClientScript::POS_END);
    Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
    Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');
    ?>
    <script>
        $(document).ready(function(){
            runOrdering({
                url: '<?= Yii::app()->createUrl('/postal/postal/quequeLabelByLocalId');?>',
                statSuccess: '<?= PostalLabel::STAT_SUCCESS;?>',
                statFailed: '<?= PostalLabel::STAT_FAILED;?>',
                errorUnknown: '<?= Yii::t('postal', 'Wystąpił nieznany błąd. Spróbuj odświeżyć stronę.'); ?>',
                messageDone: '<?= Yii::t('postal', 'Przetworzono kolejną pozycję.'); ?>',
                statEBaySuccess: '<?= EbayImport::STAT_SUCCESS;?>',
                statEBayUnknown: '<?= EbayImport::STAT_UNKNOWN;?>',
                sourceEBay: '<?= ($import->source == PostalImportManager::SOURCE_EBAY OR $import->source == PostalImportManager::SOURCE_LINKER) ? true : false; ?>',
            });

            if($('#erorred-keys').val() != '')
                $('#errored-keys-container').show();

            $('#erorred-keys').on('change', function(){
                $('#errored-keys-container').show();
            });

        });
    </script>
<?php
endif;
