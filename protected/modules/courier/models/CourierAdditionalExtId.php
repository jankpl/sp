<?php

Yii::import('application.modules.courier.models._base.BaseCourierAdditionalExtId');

class CourierAdditionalExtId extends BaseCourierAdditionalExtId
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public static function add(Courier $courier, $ext_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)')->from((new self)->tableName())->where('courier_id = :courier_id AND external_id = :external_id', [':courier_id' => $courier->id, ':external_id' => $ext_id]);
        $result = $cmd->queryScalar();

        if($result)
            return false;

        $model = new self;
        $model->courier_id = $courier->id;
        $model->external_id = $ext_id;

        if($model->save())
        {
            $courier->addToLog('Added new external ID: '.$ext_id);
            return true;
        }

        IdStore::createNumber(IdStore::TYPE_COURIER, $courier->id, $ext_id);

        return false;
    }

    public static function remove($id)
    {
        /* @var $model self */
        $model = self::findByPk($id);


        IdStore::removeNumber(IdStore::TYPE_COURIER, $model->courier->id, $model->external_id);

        if($model) {
            $model->courier->addToLog('Removed external ID: ' . $model->external_id);
            $model->delete();
            return true;

        }

        return false;
    }

}