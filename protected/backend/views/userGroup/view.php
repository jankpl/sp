<?php

/* @var $model UserGroup */

$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
);

?>

<?php
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
        'date_entered',
        'date_updated',
        array(
            'label' => 'members',
            'value' => $model->howManyMembers(),
        ),
    ),
));
?>

<br/><br/>
<h3>Members</h3>

<table class="list hTop" style="width: 500px; margin: 0 auto;">
    <tr>
        <td>#</td>
        <td>Login</td>
    </tr>
    <?php /* @var $item UserGroup */ ?>
    <?php foreach($model->getMembers() AS $item):?>
        <tr>
            <td><?php echo $item->id; ?></td>
            <td><?php echo CHtml::link($item->login, Yii::app()->createUrl('user/view', array('id' => $item->id))); ?></td>
        </tr>
    <?php endforeach; ?>
</table>