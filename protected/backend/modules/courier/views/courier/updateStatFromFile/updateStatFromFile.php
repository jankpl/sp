<?php
/* @var $this CourierController */
/* @var $models F_MassStatusChange[] */
/* @var $fileModel F_FileModel */
?>


<?php
$this->renderPartial('updateStatFromFile/_uploadForm',
    array(
        'model' => $fileModel,
    ));
?>


<?php

if(S_Useful::sizeof($models))
    $this->renderPartial('updateStatFromFile/_list',
        array(
            'models' => $models,
        ));
?>






