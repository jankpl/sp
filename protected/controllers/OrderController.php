<?php

class OrderController extends Controller {

    public $panelLeftMenuActive = 705;

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            array(
                'application.filters.HttpsFilter +index, finalizeOrder, pay, payment, paymentFail, paymentOk, view, list',
                'bypass' => false,
            ),
            array(
                'application.filters.HttpFilter -index, finalizeOrder, pay, payment, paymentFail, paymentOk, view, list, ajaxGetTotalValue',
                'bypass' => false,
            ),
        );
    }

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('index','view','finalizeOrder','pay','payment','paymentFail','paymentOk', 'ajaxGetTotalValue'),
                'users'=>array('*'),
            ),

            array('allow',
                'actions' => array('list'),
                'users' => array('@'),
            ),

            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }


    public function actionAjaxGetTotalValue()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $hash = $_POST['order_hash'];
            $payment_type_id = $_POST['payment_type_id'];

            if($hash == '' OR $payment_type_id == '')
                $this->redirect(array('/'));

            /* @var $model OrderProduct */
            $order = Order::model()->find('hash=:hash', array(':hash' => $hash));
            $order->payment_type_id = $payment_type_id;


            $value = $order->calculateTotalValue();


            echo number_format($value, 2, ',', ' ') . ' ' . $order->currency;
            Yii::app()->end();
        } else {
            $this->redirect(array('/'));
            Yii::app()->end();
        }


    }

    public function actionIndex()
    {
        $this->redirect(array('order/list'));
    }

    public function actionView($urlData, $jumpToProduct = false, $new = false) {

        /* @var $model Order */
        $model = Order::model()->with('orderStatHistories')->find('hash=:hash', array(':hash' => $urlData));
        if($model === NULL)
            throw new CHttpException(404, 'Nie znaleziono takiego zamówienia!');

        if($model->user_id != '' && Yii::app()->user->isGuest)
        {
            $this->render('//site/notify', array(
                'text' => Yii::t('site', 'Prosze się zalogować.'),
                'header' => Yii::t('order','Zamówienie #{id}', array('{id}' => $model->id)),

            ));
            exit;
        }

        $products = [];
        foreach($model->orderProduct AS $product)
        {
            $product = new S_Product($product, $new);
            array_push($products, $product);
        }

        $actions = '';
        switch($model->order_stat_id)
        {
            case 1:
                if(!$model->finalized)
                    $actions = $this->renderPartial('order_actions/finalize', array('model' => $model), true);
                break;
            case 2:
                $actions = $this->renderPartial('order_actions/payOnline', array('model' => $model), true);
                break;
        }

        if($jumpToProduct)
        {
            if(S_Useful::sizeof($products) == 1)
            {
                Yii::app()->user->setFlash('success', Yii::t('order', 'Dziękujemy, Twoje zlecenie zostało zapisane! Oczekuj na powiadomienia o zmianie statusu.'));
                $this->redirect($products[0]->url);
                exit;
            }
        }
        $this->render('view', array(
            'model' => $model,
            'products' => $products,
            'actions' => $actions,
        ));
    }


    public function actionList()
    {
        $model = new Order('search');
        $model->unsetAttributes();

        if (isset($_GET['Order']))
            $model->setAttributes($_GET['Order']);

        $this->render('list', array(
            'model' => $model,
        ));
    }

    public function actionFinalizeOrder($urlData)
    {
        /* @var $order Order */
        /* @var $orderProduct OrderProduct */
//        $orderProduct = OrderProduct::model()->find('hash=:hash', array(':hash' => $urlData));
        $order = Order::model()->find('hash=:hash', array(':hash' => $urlData));
        $orderProducts = $order->orderProduct;

        Yii::app()->PriceManager->setCurrencyByCode($order->currency);

        if($order === NULL OR !S_Useful::sizeof($orderProducts))
            throw new CHttpException('404','Nieprawidłowy adres!');

        if(Yii::app()->user->isGuest && $order->user_id !== NULL)
            throw new CHttpException('401', 'Brak dostępu!');

        if(!Yii::app()->user->isGuest && $order->user_id != Yii::app()->user->id)
            throw new CHttpException('401', 'Brak dostępu!');

        if($order->paid)
        {
            $this->render('//site/error', array('message' => 'Zamówienie zostało już prekazane do realizacji!'));
            return;
        }
        else if($order->order_stat_id > 1)
        {
            $this->redirect(array('order/view', 'urlData' => $order->hash));
            return;
        }

        if($order->calculateValueWithDiscount() < 0)
        {
            throw new CHttpException(403, Yii::t('hm', 'Wystąpił problem z Twoim zamówieniem - zbyt niska wartosć zamówienia. Skontaktuj się z nami w tej sprawie.'));
        }


        $senderAddressModel = new AddressData();
        $orderProductOne = $orderProducts[0];

        if($order->product_type == OrderProduct::TYPE_COURIER)
        {
            $temp = Courier::model()->findByPk($orderProductOne->type_item_id);
            if($temp)
                $senderAddressModel = $temp->senderAddressData;
        }

        $addressModel = new AddressData_Order();
        $errors = false;
        if (isset($_POST['Order'])) {

            $order->setAttributes($_POST['Order']);

            if (!$order->validate())
                $errors = true;



            if($order->_inv_other_data && $order->payment_type_id == PaymentType::PAYMENT_TYPE_ONLINE)
            {
                $addressModel->scenario = $_POST['dataInvCompany'];
                $addressModel->setAttributes($_POST['AddressData_Order']);

                if(!$addressModel->validate())
                    $errors = true;
                else
                    $order->addressDataOrder = $addressModel;
            }
//            else {
//                if($order->user->country_id != CountryList::COUNTRY_PL) {
//                    $order->addError('_inv_other_data', Yii::t('order', 'Odbiorca faktury musi być w Polsce - podaj inne dane!'));
//                    $errors = true;
//                }
//
//            }


//            if($order->payment_type_id != PaymentType::PAYMENT_TYPE_ONLINE)
//                $order->addressDataOrder = NULL;

            if(!$errors) {

                try {
                    if ($order->finalizeOrder()) {
                        $order = Order::model()->findByPk($order->id);
                        $this->redirect(array('pay', 'urlData' => $order->hash));
                    }

                } catch (Exception $e) {
                    Yii::log(print_r($e, 1), CLogger::LEVEL_ERROR);
                }

            }

        }


        $payment = [];

        // PLI HAS DISABLED PAYMENT ONLINE
        if(Yii::app()->PV->get() != PageVersion::PAGEVERSION_PLI)
            array_push($payment, PaymentType::model()->findByPk(PaymentType::PAYMENT_TYPE_ONLINE));

        if(Yii::app()->user->getModel()->payment_on_invoice)
            array_push($payment, PaymentType::model()->findByPk(PaymentType::PAYMENT_TYPE_INVOICE));

        if(!S_Useful::sizeof($payment))
        {
            $this->render('//site/notify', array(
                'text' => Yii::t('order', 'Your account has no available payment methods! Place {a}contact us{/a}.', ['{a}' => '<a href="'.Yii::app()->createUrl('/site/contact').'"><strong>', '{/a}' => '</strong></a>']),
                'header' =>  Yii::t('order', 'Payment'),

            ));
            exit;
        }

        $products = [];
        foreach($order->orderProduct AS $product)
        {
            $product = new S_Product($product);
            array_push($products, $product);
        }



        $this->render('finalize', array(
            'model' => $order,
            'products' => $products,
            'payment' => $payment,
            'addressModel' => $addressModel,
            '_other_data' => $_POST['_other_data'],
            'dataInvCompany' => $_POST['dataInvCompany'],
            'senderAddressModel' => $senderAddressModel,

        ));


    }



    public function actionPay($urlData)
    {
        /* @var $model Order */

        $model = Order::model()->find('hash=:hash', array(':hash' => $urlData));

        if($model === NULL)
        {

            $this->render('//site/error', array('message' => 'Niepoprawny!'));
            Yii::app()->end();
        }

        if(Yii::app()->user->isGuest && $model->user_id !== NULL)
            throw new CHttpException('401', 'Brak dostępu!');

        if(!Yii::app()->user->isGuest && $model->user_id != Yii::app()->user->id)
            throw new CHttpException('401', 'Brak dostępu!');


        /* @var $model Order */
        if($model->pay())
            $this->redirect(array('payment', 'urlData' => $model->hash));
        else
            throw new CException('Bląd!');
    }




    public function actionPayment($urlData)
    {
        $model = Order::model()->find('hash=:hash', array(':hash' => $urlData));

        if($model === NULL)
        {
            $this->render('//site/error', array('message' => 'Niepoprawny!'));
            Yii::app()->end();
        }
        if(Yii::app()->user->isGuest && $model->user_id !== NULL)
            throw new CHttpException('401', 'Brak dostępu!');

        if(!Yii::app()->user->isGuest && $model->user_id != Yii::app()->user->id)
            throw new CHttpException('401', 'Brak dostępu!');


        if($model->user_id === NULL)
            $ownerData = $model->ownerData;
        else
            $ownerData = $model->user;

        $this->render('payment_type/'.$model->payment_type_id, array(
            'model' => $model,
            'ownerData' => $ownerData,

        ));
    }

    public function actionPaymentFail()
    {
        $merchant_id = $_POST['p24_merchant_id'];
        $pos_id = $_POST['p24_pos_id'];
        $session_id = $_POST['p24_session_id'];
        $amount = $_POST['p24_amount'];
        $currency = $_POST['p24_currency'];
        $order_id = $_POST['p24_order_id'];
        $method = $_POST['p24_method_id'];

        $sign = $_POST['p24_sign'];

        $error = $_POST['p24_error_code'];

        if(!Order::testP24return($sign,$session_id,$order_id,$amount, $currency))
            throw new CHttpException(403,'Dane wejściowe są nieprawidłowe!');

        Order::verifyPayment($_POST);

        //Yii::log('APF:'.print_r($_REQUEST,1),CLogger::LEVEL_ERROR);
        $this->render('payment_fail');
    }

    public function actionPaymentOk()
    {


        $merchant_id = $_POST['p24_merchant_id'];
        $pos_id = $_POST['p24_pos_id'];
        $session_id = $_POST['p24_session_id'];
        $amount = $_POST['p24_amount'];
        $currency = $_POST['p24_currency'];
        $order_id = $_POST['p24_order_id'];
        $method = $_POST['p24_method_id'];

        $sign = $_POST['p24_sign'];

        $error = $_POST['p24_error_code'];

        if(!Order::testP24return($sign,$session_id,$order_id,$amount, $currency))
            throw new CHttpException(403,'Dane wejściowe są nieprawidłowe!');

        $return = Order::verifyPayment($_POST);


        if($return[0] == "TRUE")
            $this->render('payment_ok');
        else
            $this->render('payment_fail');

    }


}

