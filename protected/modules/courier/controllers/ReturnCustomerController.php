<?php

class ReturnCustomerController extends Controller {

    public function actionIndex($session = false)
    {
        $model = NULL;
        if(isset($_POST['id']))
        {
            $providedId = $_POST['id'];
            $providedZipCode = $_POST['zipCode'];
            $oryginal = CourierExternalManager::findCourierIdsByRemoteId($providedId, true, true);

            $errors = false;

            if(S_Useful::sizeof($oryginal) > 1)
            {
                Yii::app()->user->setFlash('error', 'aaa');
                $errors = true;
            }

            if(S_Useful::sizeof($oryginal))
            {
                /* @var $oryginal Courier */
                $oryginal = array_pop($oryginal);

                $oryginalZipCode = $oryginal->receiverAddressData->zip_code;

                $providedZipCode = preg_replace("/[^A-Za-z0-9]/", '', strtoupper($providedZipCode));
                $oryginalZipCode = preg_replace("/[^A-Za-z0-9]/", '', strtoupper($oryginalZipCode));

                if($providedZipCode != $oryginalZipCode)
                {
                    Yii::app()->user->setFlash('error', 'aaa');
                    $errors = true;
                }

                if(!$errors) {
                    $model = new CourierTypeReturn();
                    $model->baseCourier = $oryginal;
                }
            }
        }

        $this->render('index', [
            'model' => $model,
            ]
        );
    }

}