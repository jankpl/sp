<?php

class YodelClient extends GlobalOperator
{

    const URL = GLOBAL_CONFIG::YODEL_URL;
    const LOGIN = GLOBAL_CONFIG::YODEL_LOGIN;
    const PASSWORD = GLOBAL_CONFIG::YODEL_PASSWORD;
    const ACCOUNT_ID = GLOBAL_CONFIG::YODEL_ACCOUNT_ID;

    public $label_annotation_text = false;

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_country_id;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $packages_number = 1;

    public $package_weight;
    public $package_size_l;
    public $package_size_d;
    public $package_size_w;

    public $package_content;
    public $package_value;

    public $ref;

    public $user_id;

    public $service_name;

    const SERVICE_XPRESS_24 = '1CN';
    const SERVICE_XPRESS_48 = '2CN';

    public static function getAdditions(Courier $courier)
    {
        return [];
    }

    public static function test($returnError = true)
    {
        $client = new self;

        $data = '<!DOCTYPE ndxml PUBLIC "-//NETDESPATCH//ENTITIES/Latin" "ndentity.ent">
<ndxml version="2.0">
<credentials>
    <identity>'.self::LOGIN.'</identity>
    <password>'.self::PASSWORD.'</password>
</credentials>
<request id="1" function="createNewJob" style="11" responseType="labelURL">
    <job jobType="HT">
        <tariff code="1CN"/>
        <account id="'.self::ACCOUNT_ID.'"/>
        <service code="ON"/>
        <pickupDateTime time="21:00:00" date="2018-09-28"/>
        <reference>EXAMPLE REF 1</reference>
        <costcentre>EXAMPLE REF 2</costcentre>
        <notes/>
        <international>
            <exportType>DAP Permanent Export</exportType>
            <consigneeVATNumber>NONE</consigneeVATNumber>
            <customsCode/>
            <parcels>
                <parcel numParcels="1" length="5" width="4" height="3" weight="3">
                    <parcelContent productDes="Book" HSTariff="" origin="GB" unitQty="1" unitValue="10.0" unitCurrency="GBP" unitWeight="0.50"/>
                    <parcelContent productDes="CD" HSTariff="" origin="GB" unitQty="1" unitValue="10.0" unitCurrency="GBP" unitWeight="0.50"/>
                </parcel>
            </parcels>
        </international>
        <segment type="P" number="1">
            <orderDateTime time="21:00:00" date="2018-09-28"/>
            <address>
            <company>SENDER COMPANY NAME</company>
            <building>YODEL SORT CENTRE</building>
            <street>CIM DEPARTMENT</street>
            <locality>FROBISHER WAY</locality>
            <town>HATFIELD BUS PK</town>
            <county>HATFIELD</county>
            <zip>AL10 9TR</zip>
            <country ISOCode="GB"/>
            </address>
            <contact>
                <name>CONTACT NAME</name>
                <telephone ext="">07711897662</telephone>
                <email>geoff.james@yodel.co.uk</email>
            </contact>
            <pieces>1</pieces>
            <weight>10</weight>
        </segment>
        <segment type="D" number="2">
            <orderDateTime time="21:00:00" date="2018-09-28"/>
            <address>
            <company>COMPANY NAME</company>
            <building>5TH FLOOR</building>
            <street>1 DOCK HOUSE ROAD</street>
            <locality>MEDIA CITY</locality>
            <town>GAIRLOCH</town>
            <county></county>
            <zip>IV21 2QH</zip>
            <country ISOCode="GB"/>
            </address>
            <contact>
                <name>CONTACT NAME</name>
                <telephone ext="">07711897662</telephone>
                <email>geoff.james@yodel.co.uk</email>
            </contact>
            <pieces>1</pieces>
            <weight>10</weight>
            <alertEmail value="true"/>
        </segment>
        <issues>
            <issue id="65536">1</issue>
        </issues>
    </job>
</request>
</ndxml>';

        $resp = $client->_curlCall($data);

        if($resp == '' OR ((string) $resp->status['code']) != 'OK' OR ((string) $resp->response->niceError) != '')
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse((string) $resp->response->niceError != '' ? (string) $resp->response->niceError : 'Operator internal error.');
            } else {
                return false;
            }
        }

        foreach($resp->response->job->parcels->barcode AS $item)
            $ttNumber = (string) $item;

        $label = (string) $resp->response->job->labelData->url;
        $label = @file_get_contents($label);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($label);
        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $im->stripImage();

//            $im->trimImage(0);

        file_put_contents('lab.png', $im->getImageBlob());

        $return = [];
        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);

        return $return;
    }

    protected function _curlCall($data)
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

        $headers = [];
        $headers[] = 'Content-Type: text/xml';

        $curl = curl_init(self::URL);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt_array($curl, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        );
        $resp = curl_exec($curl);

        MyDump::dump('yodel.txt', print_r($data,1));
        MyDump::dump('yodel.txt', print_r($resp,1));
        $resp = @simplexml_load_string($resp);

        return $resp;
    }


    public static function clearString($text, $length = false)
    {
        $text = htmlspecialchars($text);

        if($length)
            $text = mb_substr($text, 0, $length);

        return $text;
    }

    protected static function operatorUniqIdToService($uniq_id)
    {
        switch ($uniq_id)
        {
            case GLOBAL_BROKERS::OPERATOR_UNIQID_YODEL_XPRESS_24:
                return self::SERVICE_XPRESS_24;
                break;
            case GLOBAL_BROKERS::OPERATOR_UNIQID_YODEL_XPRESS_48:
                return self::SERVICE_XPRESS_48;
                break;
            default:
                throw new Exception('Unknown Yodel service!');
                break;
        }
    }


    protected function createShipment($returnError)
    {
        if($this->user_id != User::INTEGRATIONS_TEST_USER_ID)
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse('Not active!');
            } else {
                return false;
            }


        $time = date('H:i:s');
        $date = date('Y-m-d');

        $receiverCompany = $this->receiver_company ? $this->receiver_company : $this->receiver_name;
        $receiverName = $this->receiver_name ? $this->receiver_name : $this->receiver_company;

        $receiverAddress = trim($this->receiver_address1.' '.$this->receiver_address2);
        $receiverAddressAdd = mb_substr($receiverAddress,30);


        $senderCompany = $this->sender_company ? $this->sender_company : $this->sender_name;
        $senderName = $this->sender_name ? $this->sender_name : $this->sender_company;

        $senderAddress = trim($this->sender_address1.' '.$this->sender_address2);
        $senderAddressAdd = mb_substr($senderAddress,30);

        $data = '<!DOCTYPE ndxml PUBLIC "-//NETDESPATCH//ENTITIES/Latin" "ndentity.ent">
<ndxml version="2.0">
<credentials>
    <identity>'.self::LOGIN.'</identity>
    <password>'.self::PASSWORD.'</password>
</credentials>
<request id="1" function="createNewJob" style="11" responseType="labelURL">
    <job jobType="HT">
        <tariff code="'.$this->service_name.'"/>
        <account id="'.self::ACCOUNT_ID.'"/>
        <service code="ON"/>
        <pickupDateTime time="'.$time.'" date="'.$date.'"/>
        <reference>'.self::clearString($this->ref, 20).'</reference>
        <costcentre></costcentre>
        <notes/>
        <international>
            <exportType>DAP Permanent Export</exportType>
            <consigneeVATNumber>NONE</consigneeVATNumber>
            <customsCode/>
            <parcels>
                <parcel numParcels="1" length="'.$this->package_size_l.'" width="'.$this->package_size_w.'" height="'.$this->package_size_d.'" weight="'.$this->package_weight.'">';
        for($i = 0; $i < $this->packages_number; $i++)
            $data .= '<parcelContent productDes="'.$this->package_content.'" HSTariff="" origin="'.$this->sender_country.'" unitQty="1" unitValue="'.$this->package_value.'" unitCurrency="GBP" unitWeight="'.round($this->package_weight/$this->packages_number,2).'"/>';
        $data .= '</parcel>
            </parcels>
        </international>
        <segment type="P" number="1">
            <orderDateTime time="'.$time.'" date="'.$date.'"/>
            <address>
                 <company>'.self::clearString($senderCompany, 30).'</company>
                <building>'.self::clearString($senderAddressAdd, 30).'</building>
                <street>'.self::clearString($senderAddress, 30).'</street>
                <locality></locality>
                <town>'.self::clearString($this->sender_city, 30).'</town>
                <county></county>
                <zip>'.$this->sender_zip_code.'</zip>
                <country ISOCode="'.$this->sender_country.'"/>
            </address>
            <contact>
                 <name>'.self::clearString($senderName, 40).'</name>
                <telephone ext="">'.$this->sender_tel.'</telephone>
                <email>'.$this->sender_mail.'</email>
            </contact>
            <pieces>'.$this->packages_number.'</pieces>
            <weight>'.$this->package_weight.'</weight>
        </segment>
        <segment type="D" number="2">
               <orderDateTime time="'.$time.'" date="'.$date.'"/>
            <address>
                <company>'.self::clearString($receiverCompany, 30).'</company>
                <building>'.self::clearString($receiverAddressAdd, 30).'</building>
                <street>'.self::clearString($receiverAddress, 30).'</street>
                <locality></locality>
                <town>'.self::clearString($this->receiver_city, 30).'</town>
                <county></county>
                <zip>'.$this->receiver_zip_code.'</zip>
                <country ISOCode="'.$this->receiver_country.'"/>
            </address>
            <contact>
                <name>'.self::clearString($receiverName, 40).'</name>
                <telephone ext="">'.$this->receiver_tel.'</telephone>
                <email>'.$this->receiver_mail.'</email>
            </contact>
            <pieces>'.$this->packages_number.'</pieces>
            <weight>'.$this->package_weight.'</weight>
            <alertEmail value="true"/>
        </segment>
        <issues>
            <issue id="65536">1</issue>
        </issues>
    </job>
</request>
</ndxml>';

        $client = new self;
        $resp = $client->_curlCall($data);

        if($resp == '' OR ((string) $resp->status['code']) != 'OK' OR ((string) $resp->response->niceError) != '')
        {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse((string) $resp->response->niceError != '' ? (string) $resp->response->niceError : 'Operator internal error.');
            } else {
                return false;
            }
        }

        foreach($resp->response->job->parcels->barcode AS $item)
            $ttNumber = (string) $item;

        $label = (string) $resp->response->job->labelData->url;
        $label = @file_get_contents($label);

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($label);
        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }
        $im->stripImage();

//            $im->trimImage(0);



        $return = [];
        $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);

        return $return;

    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {
        $model = new self;
        $model->service_name = self::operatorUniqIdToService($uniq_id);

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->email;
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->email;
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierInternal->courier->getWeight(true);
        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->package_value = $courierInternal->courier->getPackageValueConverted('GBP');
        $model->ref = $courierInternal->courier->local_id;

        $model->user_id = $courierInternal->courier->user_id;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {
        $model = new self;

        $model->service_name = self::operatorUniqIdToService($courierU->courierUOperator->uniq_id);

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->getUsefulName(true);
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_country = $from->country0->code;
        $model->sender_country_id = $from->country_id;
        $model->sender_mail = $from->email;
        $model->sender_tel = $from->tel;

        $model->receiver_name = $to->getUsefulName(true);
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->zip_code;
        $model->receiver_country = $to->country0->code;
        $model->receiver_country_id = $to->country_id;
        $model->receiver_mail = $to->email;
        $model->receiver_tel = $to->tel;

        $model->package_weight = $courierU->courier->getWeight(true);
        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->package_value = $courierU->courier->getPackageValueConverted('GBP');
        $model->ref = $courierU->courier->local_id;

        $model->user_id = $courierU->courier->user_id;

        return $model->createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        $statuses = new GlobalOperatorTtResponseCollection();

//        $now = time();
        $now = strtotime('2019-06-06 17:00:00');

        $statuses->addItem('STAT TEST IN TRANSPORT',  date('Y-m-d H:i:s', ++$now), '');
        $statuses->addItem('STAT TEST IN DELIVERY',  date('Y-m-d H:i:s', ++$now), '');
        $statuses->addItem('STAT TEST DELIVERED',  date('Y-m-d H:i:s', ++$now), '');
        $statuses->addItem('STAT TEST IN DELIVERY',  date('Y-m-d H:i:s', ++$now), '');
        $statuses->addItem('STAT TEST RETURN',  date('Y-m-d H:i:s', ++$now), '');
        $statuses->addItem('STAT TEST IN DELIVERY',  date('Y-m-d H:i:s', ++$now), '');
        $statuses->addItem('STAT TEST IN DELIVERY',  date('Y-m-d H:i:s', ++$now), '');
        $statuses->addItem('STAT TEST DELIVERED',  date('Y-m-d H:i:s', ++$now), '');
//
//
//
//
        return $statuses;
//

//        return false;

    }
}