<?php
/* @var $form GxActiveForm */
/* @var $type integer */
/* @var $modelNew CourierCountryGroupOperator */
/* @var $models CourierCountryGroupOperator[] */
?>

<h5>Custom operators:</h5>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>No (*)</th>
        <th>Weight from [<?= Courier::getWeightUnit();?>]</th>
        <th>Weight to [<?= Courier::getWeightUnit();?>]</th>
        <th>Operator</th>
        <th>Hub</th>
        <th>Date entered</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 1;
    if(is_array($models)):
        foreach($models AS $model):
            ?>
            <tr>
                <td><?= $i;?></td>
                <td><?= $model->weight_from;?></td>
                <td><?= $model->weight_to;?></td>
                <td><?= $model->operator->nameWithIdAndBroker;?></td>
                <td><?= $model->hub->name;?></td>
                <td><?= $model->date_entered;?></td>
                <td style="text-align: center;"><?= CHtml::checkBox('delete['.$model->id.']');?></td>

            </tr>
            <?php
            $i++;
        endforeach;
    endif;
    ?>
    </tbody>
</table>

<div class="alert alert-warning">
    <h5>Add custom operator - OPTIONAL:</h5>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Weight from [<?= Courier::getWeightUnit();?>]</th>
            <th>Weight to [<?= Courier::getWeightUnit();?>]</th>
            <th>Operator</th>
            <th>Courier Hub</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= $form->numberField($modelNew, '['.$type.']weight_from', ['step' => 0.01]);?><br/><?= $form->error($modelNew, '['.$type.']weight_from');?></td>
            <td><?= $form->numberField($modelNew, '['.$type.']weight_to', ['step' => 0.01]);?><br/><?= $form->error($modelNew, '['.$type.']weight_to');?></td>
            <td><?= $form->dropDownList($modelNew, '['.$type.']courier_operator', CHtml::listData(CourierOperator::listOperators(),'id','nameWithIdAndBroker'), ['prompt' => '-']);?><br/><?= $form->error($modelNew, '['.$type.']courier_operator');?></td>
            <td><?= $form->dropDownList($modelNew, '['.$type.']courier_hub', CHtml::listData(CourierHub::listHubs(),'id','name'), ['prompt' => '-']);?><br/><?= $form->error($modelNew, '['.$type.']courier_hub');?></td>

        </tr>
        </tbody>
    </table>
</div>