<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    Yii::t('app', 'Manage'),
);

$this->menu = array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
);

?>

    <h2>Manage <?php echo GxHtml::encode($model->label(2)); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'courier-addition-list-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'date_entered',
        'date_updated',
        'name',
        array(
            'name'=>'stat',
            'value'=>'S_Status::stat($data->stat)',
            'filter'=>CHtml::listData(S_Status::listStats(),'id','name'),
        ),
        array(
            'name' => 'broker_id',
            'value'=>'CourierUOperator::getBrokerList()[$data->broker_id]',
            'filter'=> CourierUOperator::getBrokerList(),
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update} {view} {delete} {status} {status_a}',
            'htmlOptions'=> array('style'=>'width:80px'),
            'buttons'=>array(
                'status'=>array(
                    'visible' => '$data->stat==1',
                    'label'=>'deactivate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_active.png',
                    'url' => 'Yii::app()->createUrl("/courier/courierUAdditionList/stat",array("id" => $data->id))',
                ),
                'status_a'=>array(
                    'visible' => '$data->stat==0',
                    'label'=>'activate',
                    'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_not_active.png',
                    'url' => 'Yii::app()->createUrl("/courier/courierUAdditionList/stat",array("id" => $data->id))',
                ),
            ),
        ),
    ),
)); ?>