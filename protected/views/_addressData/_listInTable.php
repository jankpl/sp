<?php
/* @var $model AddressData */
?>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'name',
        'company',
        array(
            'name' => $model->getAttributeLabel('country'),
            'value' => $model->getCountryNameUniversal(),
        ),
        'city',
        'zip_code',
        'address_line_1',
        'address_line_2',
        'tel',
        'email',
    ),
));
?>

