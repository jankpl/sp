<h2 id="list"><?= Yii::t('postal', 'Eksport przesyłek');?></h2>
<br/>
<br/>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file',
    'action' => Yii::app()->createUrl('/postal/postal/exportToFileAll'),
    'htmlOptions' => [
        'class' => 'do-not-block',
    ]
));
echo TbHtml::submitButton(Yii::t('postal','Eksportuj wszyskie przesyłki do .xls').' :', ['style' => 'margin-top: -6px; margin-right: 5px;']);
echo Chtml::dropDownList('period', '', S_Useful::listPeriods(), ['style' => 'height: 30px;']);
$this->endWidget();
?>