<?php

class Report_AdminCourierScan extends Report
{

    const SEPARATOR = '; ';

    public static function checkItem(Courier $courier, $id)
    {
        $date = date('Y-m-d');
        $model = self::model()->find('type = :type AND what_id = :id AND DATE(date_entered) = :date AND stat = 0', [':type' => self::TYPE_ADMIN_COURIER_SCAN, ':date' => $date, ':id' => $id]);

        $ids = $courier->listExternalIdsByCourierLabelNew(true, self::SEPARATOR).self::SEPARATOR;

        if($model)
        {
            $model->content->numberOk += 1;
            $model->content->ids .= $ids;

        } else {
            $model = new self;
            $model->type = self::TYPE_ADMIN_COURIER_SCAN;
            $model->what_id = $id;
            $model->stat = 0;

            $model->content = new stdClass();
            $model->content->numberOk = 1;
            $model->content->numberFailed = 0;
            $model->content->ids = $ids;
        }

        $model->validate();

        return $model->save();
    }


    public static function addFailedNo($id, $number)
    {
        $date = date('Y-m-d');

        $model = self::model()->find('type = :type AND what_id = :id AND DATE(date_entered) = :date AND stat = 0', [':type' => self::TYPE_ADMIN_COURIER_SCAN, ':id' => $id, ':date' => $date]);

        if($model)
        {
            $model->content->numberFailed += $number;
        } else {
            $model = new self;
            $model->type = self::TYPE_ADMIN_COURIER_SCAN;
            $model->what_id = $id;
            $model->stat = 0;

            $model->content = new stdClass();
            $model->content->numberOk = 0;
            $model->content->numberFailed = $number;
        }
        return $model->save();
    }

    protected static function getAdminLogin($id)
    {
        $model = Admin::model()->findByPk($id);
        if($model)
            return $model->login;
        else
            return NULL;
    }


    public static function processReport($id)
    {

        $models = self::model()->findAll('type = :type AND what_id = :id AND stat = 0 AND date_entered < :date', [':type' => self::TYPE_ADMIN_COURIER_SCAN, ':id' => $id, ':date' => date('Y-m-d')]);

        $report = 'Courier scans report'."<br/><br/>";
        $report .= 'Administrator: '.self::getAdminLogin($id).' (#'.$id.')'."<br/>";
        $report .= "<br/>";
        $report .= '<table border="1"><tr style="font-weight: bold;"><td>DATE</td><td>SCANNED OK</td><td>SCANNED FAILED</td><td>TOTAL</td><td>IDS - OK</td></tr>';

        $totalOk = 0;
        $totalFailed = 0;
        $total = 0;
        foreach($models AS $model)
        {
            $report .= '<tr><td style="vertical-align: top;">'.substr($model->date_entered,0,10).'</td><td style="text-align: center; vertical-align: top;">'.$model->content->numberOk.'</td><td style="text-align: center; vertical-align: top;">'.$model->content->numberFailed.'</td><td style="text-align: center; vertical-align: top;">'.($model->content->numberOk + $model->content->numberFailed).'</td><td>'.$model->content->ids.'</td></tr>';

            $totalOk += $model->content->numberOk;
            $totalFailed += $model->content->numberFailed;
            $total = $total + $model->content->numberOk + $model->content->numberFailed;

            $model->stat = 1;
            $model->date_sent = date('Y-m-d H:i:s');
            $model->update(['stat', 'date_sent']);
        }

        $report .= '<tr><td>TOTAL</td><td style="text-align: center;">'.$totalOk.'</td><td style="text-align: center;">'.$totalFailed.'</td><td style="text-align: center;">'.$total.'</td><td>-</td></tr>';
        $report .= '</table>';

        $to = [
            'jbokhorst@kaabnl.nl',
            'ostrzelecka@kaabnl.nl',
            'jankopec@swiatprzesylek.pl',
        ];

        S_Mailer::send($to, $to, 'Courier scans report (#'.$id.')', $report, false);
    }

}