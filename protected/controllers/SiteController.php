<?php

class SiteController extends Controller
{
    public $backLink = true;

    public function init()
    {
        parent::init();

        $this->layout = 'other';
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array_merge(array(
            'accessControl', // perform access control for CRUD operations
//            array(
//                'application.filters.HttpsFilter +login, reminder',
//                'bypass' => false,
//            ),
//            array(
//                'application.filters.HttpFilter -login, reminder, adminModeLogin, error',
//                'bypass' => false,
//            ),
        ));
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
//            array('deny',  // deny double login
//                'actions'=>array('login'),
//                'users'=>array('@'),
//            ),
            array('deny',  // deny logout of no user
                'actions'=>array('logout'),
                'users'=>array('?'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),

//            'coco'=>array(
//                'class'=>'CocoAction',
//            ),
        );
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

//        $model=new LoginForm();

        $this->pageTitle = Yii::app()->name;

        $model = new ContactForm;
        if(isset($_POST['ContactForm']))
        {

            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {

                $text = 'Nadawca: <span style="font-weight: bold;">'.$model->name.'</span><br/>';
                $text .= 'Adres email: <span style="font-weight: bold;">'.$model->email.'</span><br/>';
                $text .= '---<br/><br/>'.$model->body;

                S_Mailer::sendToAdministrator('Wiadomość z formularza kontaktowego SwiatPrzesylek.pl',$text);

                Yii::app()->user->setFlash('contact',Yii::t('m_ContactForm','Dziękujemy, Twoja wiadomość zotała wysłana!'));
                $this->refresh('true');
            }
        }


        $this->layout = 'home';
        $this->render('index', array('model' => $model));

    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {

        if(!Yii::app()->errorHandler->error)
        {
            throw new CHttpException(404);
        }

        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else {

                if($error['errorCode'] == 2002) // CDbConnection failed to open the DB connection.
                {
                    require_once('index.html.inc');
                    exit;
                }
                $this->render('error', ['error' => $error]);
            }
        } else {
            throw new CHttpException(404);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model=new ContactForm;
        if(isset($_POST['ContactForm']))
        {

            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {

                $text = 'Nadawca: <span style="font-weight: bold;">'.$model->name.'</span><br/>';
                $text .= 'Adres email: <span style="font-weight: bold;">'.$model->email.'</span><br/>';
//                $text .= 'Temat: <span style="font-weight: bold;">'.$model->subject.'</span><br/>';
                $text .= '---<br/><br/>'.$model->body;

                S_Mailer::sendToAdministrator('Wiadomość z formularza kontaktowego SwiatPrzesylek.pl',$text);

                Yii::app()->user->setFlash('contact',Yii::t('m_ContactForm','Dziękujemy, Twoja wiadomość zotała wysłana!'));
                $this->refresh('true');
            }
        }
        $this->render('contact',array('model'=>$model));
    }

    public function actionRegulations()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'page_id=:page_id AND language_id=:language_id AND stat = 1';
        $criteria->params = array(
            ':page_id'=> 2,
            ':language_id'=>Yii::app()->params['language'],
        );
        $model = PageTr::model()->find($criteria);

        // if not found, try default language
        if($model === NULL)
        {
            $criteria = new CDbCriteria();
            $criteria->condition = 'page_id=:page_id AND language_id=:language_id AND stat = 1';
            $criteria->params = array(
                ':page_id'=> 2,
                ':language_id'=> 1,
            );
            $model = PageTr::model()->find($criteria);
        }

        if($model === NULL)
            throw new CHttpException('404');

        $this->render('regulations', array(
            'model' => $model,
        ));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {

        if(!Yii::app()->user->isGuest)
            $this->redirect(['/']);

        $model=new LoginForm('Front');

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login()) {

                Yii::app()->user->returnUrl = ['/panel'];

                if (isset($_POST['returnURL']))
                    $this->redirect($_POST['returnURL']);
                else
                    $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        // display the login form
        $this->render('login',array('model'=>$model));


    }
    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout(false);
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionTt($urlData = null)
    {

        $id = $urlData;
        if(isset($_POST['id']))
            $id = $_POST['id'];

        $partial = null;

        if(isset($id))
        {

            if($id == '')
            {
                $error = true;
                Yii::app()->user->setFlash('error',Yii::t('site','Podaj ID przesyłki!'));
            } else {

                $track_id = htmlspecialchars(strip_tags($id));
                $S_UniversalTt = new S_UniversalTt($track_id, true);

                if(!$S_UniversalTt->isFound())
                {

                    Yii::app()->user->setFlash('error',Yii::t('site','Nie znaleziono przesyłki o podanym ID!{br}{br}Upewnij się, że poprawnie wprowadziłeś numer.{br}{br}Jeżeli problem nadal występuje, to skontakuj się z nami!', array('{br}' => '<br/>')));

                    if($urlData !== NULL)
                        $this->redirect(['tt']);

                } else {
                    if($urlData === NULL)
                        $this->redirect(['tt', 'urlData' => $S_UniversalTt->getTrackId()]);

                    //$S_UniversalTt->update();

                    $partial = $this->renderPartial('_tt_status',
                        array(
                            'S_UniversalTt' => $S_UniversalTt
                        ), true);
                }
            }

        }

        $this->render('tt',
            array(
                'S_UniversalTt' => $S_UniversalTt,
                'partial' => $partial
            ));
    }

    public function actionTtMulti()
    {

        $result = [];
        $ids = [];
        if(isset($_POST['ids']))
        {
            $ids = $_POST['ids'];
            $ids = explode(PHP_EOL, $ids);

            foreach($ids AS $key => $item)
            {
                $temp = trim($item);
                if($temp == '')
                    unset($ids[$key]);
                else
                    $ids[$key] = htmlspecialchars(strip_tags($item));
            }

            if(!S_Useful::sizeof($ids))
            {
                Yii::app()->user->setFlash('error',Yii::t('site','Podaj ID przesyłki!'));
            } else {
                foreach($ids AS $key => $item)
                    $result[$item] = new S_UniversalTt($item);
            }
        }

        if(isset($_POST['export']))
        {
            $foundIds = $_POST['foundIds'];
            $foundIds = explode(',', $foundIds);

            $export = [];
            $export[] = [
                'id',
                'local_id',
                'date_created',
                'last_status_date',
                'last_status',
                'last_status_map',
                'weight',
                'size',
                'sender_country',
                'receiver_country'
            ];

            foreach($foundIds AS $key => $item)
            {
                $tempModel = new S_UniversalTt($item);

                $temp = [];
                $temp[] = $item;

                if($tempModel->isFound())
                {
                    $temp[] = $tempModel->getServiceModel()->local_id;
                    $temp[] = $tempModel->getServiceModel()->date_entered;
                    $temp[] = $tempModel->getLastStatDate();
                    $temp[] = $tempModel->getCurrentStatusNameFull();
                    $temp[] = $tempModel->getCurrentStatMapName();

                    if($tempModel->getService() == S_UniversalTt::SERVICE_COURIER) {
                        $temp[] = $tempModel->getServiceModel()->package_weight.' kg';
                        $temp[] = $tempModel->getServiceModel()->package_size_l.' x '. $tempModel->getServiceModel()->package_size_d.' x '. $tempModel->getServiceModel()->package_size_w.' [cm]';
                    }
                    else if($tempModel->getService() == S_UniversalTt::SERVICE_POSTAL) {
                        $temp[] = $tempModel->getServiceModel()->weight.' g';
                        $temp[] = $tempModel->getServiceModel()->getSizeClassName();
                    }
                    else
                    {
                        $temp[] = '';
                        $temp[] = '';
                    }

                    if($tempModel->getServiceModel()->senderAddressData)
                        $temp[] = $tempModel->getServiceModel()->senderAddressData->country0->code;
                    else
                        $temp[] = '';

                    if($tempModel->getServiceModel()->receiverAddressData)
                        $temp[] = $tempModel->getServiceModel()->receiverAddressData->country0->code;
                    else
                        $temp[] = '';
                }
                $export[] = $temp;

            }

            S_XmlGenerator::generateXml($export);
            Yii::app()->end();

        }

        $this->render('ttMulti',
            [
                'result' => $result,
                'ids' => implode(',', $ids),
            ]);
    }

    public function actionCSC($pass)
    {
        if($pass == 'qwerty')
            Yii::app()->cache4DbSchema->flush();
    }


    /**
     * Method to login on user accont by link (used for admins)
     *
     * @param $hash
     * @param $hash2
     */
    public function actionAdminModeLogin()
    {

        $hash = $_POST['hash'];
        $hash2 = $_POST['hash2'];

        if(!Yii::app()->user->isGuest) {
            Yii::app()->user->logout(true);
            session_start();
        }

        if($hash != '' & $hash2 != '') {

            $_identity = NULL;

            $model = User::model()->findByAttributes(['hash' => $hash]);


            if($model === NULL)
            {
                $this->redirect(['/']);
                Yii::app()->end();
            }

            $_identity = new UserIdentity($model->login, $hash2);
            $_identity->authenticateAdminMode();

            if ($_identity->errorCode === UserIdentity::ERROR_NONE) {
                $duration = 0; // 30 days
                Yii::app()->user->login($_identity, $duration);
            }
        }


        $this->redirect(Yii::app()->getBaseUrl(true));
        Yii::app()->end();
    }



    public function actionZipCodeRules($cc = NULL, $ccid = NULL, $pp = false)
    {
        if($pp)
            $this->layout = 'popup';

        if($ccid !== NULL && $cc === NULL)
            $cc = CountryList::getCodeById($ccid);

        $cc = strtoupper($cc);

        $this->render('zipCodeValidator',
            [
                'cc' => $cc,
            ]);
    }


    public function actionUpdateTtOne($local_id)
    {
        Yii::app()->getModule('courier');
        $courier = Courier::model()->with('courierTypeInternal')->with('courierTypeDomestic')->with('courierTypeOoe')->with('courierTypeInternalSimple')->with('courierTypeExternal')->findByAttributes(['local_id' => $local_id]);

        /* $item Courier */
        if($courier !== NULL)
            CourierExternalManager::updateStatusForPackage($courier);
    }

    public function actionAjaxVerifyLoginStatus()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(!Yii::app()->user->isGuest);
        } else {
            $this->redirect(['/']);
        }
        Yii::app()->end();

    }


//    public function actionTh()
//    {
//        $cmd = Yii::app()->db->createCommand();
//        $result = $cmd->select('*')->from('user_options')->where('name = :name', [':name' => 'ups_collect_block'])->queryAll();
//        foreach($result AS $item)
//        {
//            var_dump($item);
//            $cmd->insert('user_options', [
//                'name' => 'domestic_collect_block',
//                'value' => 1,
//                'user_id' => $item['user_id']
//            ]);
//        }
//    }


    public function actionKeepAlive()
    {
        echo 'OK';
        Yii::app()->end();
    }

    public function actionApi()
    {

        $this->layout = '//layouts/panel-restricted';

        $this->panelLeftMenuActive = 162;

        if(Yii::app()->user->isGuest OR !Yii::app()->user->model->getApiAccessActive())
            throw new CHttpException(403);

        $this->render('api');
    }


    public function actionGetLabel($hash)
    {
        Yii::app()->getModule('courier');

        /* @var $model Courier */
        $model = Courier::model()->findByAttributes(['hash' => $hash]);

        $error = false;
        if($model == NULL OR $model->isLabelArchived() OR !$model->isLabelGeneratingAvailableBaseOnStat())
            $error = true;

        if(!$error) {
            $label = LabelPrinter::generateLabels([ $model ], LabelPrinter::PDF_ROLL, true, false, true);

            if($label == '')
                $error = true;
        }

        if($error)
        {
            header("HTTP/1.0 404 Not Found");
            echo "Label is unavailable";
            die();
        }
        else
        {
            header('Content-Type: application/pdf');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=".$model->local_id.'.pdf');
            echo $label;
            die();
        }

    }

    public function actionWidgetTest()
    {
        $this->render('widgetTest');
    }

    public function actionWidgetTest2()
    {
        $this->render('widgetTest2');
    }

}