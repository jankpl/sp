<?php
/* @var $this PageController */
/* @var $model PageTr */
?>

<h2><?php echo $model->title;?></h2>

<?php

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/banners.css');

Yii::app()->clientScript->registerScript('banners', $this->renderPartial('//layouts/_js_banner_list', NULL, true), CClientScript::POS_HEAD);

Yii::app()->clientScript->registerCoreScript('jquery');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.domready.min.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.timers.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.banners.js');




if($showBanners):
    ?>
    <div id="banner">
        <div id="banner-overlay"></div>
        <div id="bannerContainer">
            <div id="preloader"></div>
            <div id="banner-img">
                <div class="imgFader" id="imgFaderOne"></div>
                <div class="imgFader" id="imgFaderTwo"></div>
            </div>
        </div>
        <div id="banner-bottom-bar"></div>
    </div>

<?php
endif;
?>

<p><?php echo S_Useful::correctImgPathInText($model->text);?></p>