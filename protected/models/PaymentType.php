<?php

Yii::import('application.models._base.BasePaymentType');

class PaymentType extends BasePaymentType
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    const FAST_PAYMENT_TYPE = 2;


    const PAYMENT_TYPE_ONLINE = 1;
    const PAYMENT_TYPE_INVOICE = 2;
    const PAYMENT_TYPE_COD = 3;

    const MAX_COD_AMOUNT = 5000;


    public function rules() {
        return array(

            array('stat, available_level', 'numerical', 'integerOnly'=>true),

            array('commission_price_id', 'numerical'),
            array('commission_percentage', 'numerical', 'min' => 0, 'max' => 9999, 'integerOnly' => true),

            array('commission_percentage, commission_price_id', 'length', 'max'=>10),
            array('name, commission_percentage, commission_price_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, stat, commission_percentage, commission_price_id, available_level', 'safe', 'on'=>'search'),
        );
    }


    public static function getPaymentTypes()
    {
        return array(
            self::PAYMENT_TYPE_ONLINE => Yii::t('payment','online'),
            self::PAYMENT_TYPE_COD => Yii::t('payment','cod'),
            self::PAYMENT_TYPE_INVOICE => Yii::t('payment','invoice')
        );
    }


    public function getLocalizedName()
    {
       return self::getPaymentTypes()[$this->id];
    }

    public static function getPaymentTypeName($id)
    {
        return self::getPaymentTypes()[$id];
    }

    public function calculatePaymentCommission($base_value)
    {


        $value = 0;
        $value += (double) round($base_value * 0.01 * $this->commission_percentage,2);
        $value += (double) $this->commissionPrice->getValue(Yii::app()->PriceManager->getCurrency())->value;

        return $value;
    }

    public function saveWithPrices($ownTransaction = true)
    {
        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();

        $errors = false;

        $price_id = $this->commissionPrice->saveWithPrices(false);

        if(!$price_id)
            $errors = true;

        $this->commission_price_id = $price_id;

        if(!$this->save())
            $errors = true;


        if($errors)
        {
            if($ownTransaction)
                $transaction->rollback();

            return false;
        } else {
            if($ownTransaction)
                $transaction->commit();

            return true;
        }


    }

    public function getPrice()
    {
        $price = $this->discountPrice->priceValues(array('condition' => 'currency_id = :currency', 'params' => array(':currency' => Yii::app()->PriceManager->getCurrency())))[0]->value;

        return $price;
    }
}