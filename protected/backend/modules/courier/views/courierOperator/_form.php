<?php
/* @var $model CourierOperator */
?>

<div class="form">


    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'courier-operator-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('maxlength' => 45)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textArea($model, 'description'); ?>
        <?php echo $form->error($model,'description'); ?>
    </div><!-- row -->
    <div class="row">
        <?php echo $form->labelEx($model,'label_symbol'); ?>
        <?php echo $form->textField($model, 'label_symbol'); ?>
        <?php echo $form->error($model,'label_symbol'); ?>
    </div><!-- row -->
    <?php
    if($model->isNewRecord OR in_array($model->broker, array_keys($model->getBrokerList(true)))):
        ?>
        <div class="row">
            <?php echo $form->labelEx($model,'broker'); ?>
            <?php echo $form->dropDownList($model, 'broker', CourierOperator::getBrokerList(true), array('id' => 'broker-option')); ?>
            <?php echo $form->error($model,'broker'); ?>
        </div><!-- row -->
        <?php
    else:
        ?>
        <?php echo $form->hiddenField($model, 'broker'); ?>
    <?php endif; ?>

    <div class="ooe-details">
        <div class="row">
            <?php echo CHtml::label('OOE Login', 'ooe_login') ?>
            <?php echo CHtml::textField('ooe_login', $model->_ooe_login, array('id' => 'ooe_login')) ?>
        </div><!-- row -->

        <div class="row">
            <?php echo CHtml::label('OOE Account', 'ooe_account') ?>
            <?php echo CHtml::textField('ooe_account', $model->_ooe_account, array('id' => 'ooe_account')) ?>
        </div><!-- row -->

        <div class="row">
            <?php echo CHtml::label('OOE Pass', 'ooe_pass') ?>
            <?php echo CHtml::passwordField('ooe_pass', $model->_ooe_pass, array('id' => 'ooe_pass')) ?>
        </div><!-- row -->

        <div class="row">
            <?php echo CHtml::label('OOE Service', 'ooe_service') ?>
            <?php echo CHtml::textField('ooe_service', $model->_ooe_service, array('id' => 'ooe_service')) ?>
        </div><!-- row -->
    </div>

    <div class="safepak-details">
        <div class="row">
            <?php echo CHtml::label('Safepak service ID', 'safepak_service') ?>
            <?php echo CHtml::textField('safepak_service', $model->_safepak_service, array('id' => 'safepak_service')) ?>
        </div><!-- row -->
    </div>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->

<script>
    var $brokerOption;
    var $ooeDetails;
    var $safepakDetails;

    $(document).ready(function(){
        $brokerOption = $("#broker-option");
        $ooeDetails = $(".ooe-details");
        $safepakDetails = $(".safepak-details");

        if($brokerOption.val() == <?= CourierOperator::BROKER_OOE;?>) {
            $ooeDetails.show();
            $safepakDetails.hide();
        }
        else if($brokerOption.val() == <?= CourierOperator::BROKER_SAFEPAK;?>)
        {
            $ooeDetails.hide();
            $safepakDetails.show();
        }
        else {
            $ooeDetails.hide();
            $safepakDetails.hide();
        }


        $brokerOption.on('change', function(){
            if($(this).val() == <?= CourierOperator::BROKER_OOE;?>) {
                $ooeDetails.show();
                $safepakDetails.hide();
            }
            else if($(this).val() == <?= CourierOperator::BROKER_SAFEPAK;?>)
            {
                $ooeDetails.hide();
                $safepakDetails.show();
            }
            else
            {
                $ooeDetails.hide();
                $safepakDetails.hide();
            }
        })
    });


</script>