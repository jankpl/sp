<?php echo CHtml::label(Yii::t('_addressData','Wybierz ze swoich adresów'),'userAddressList'); ?>
<?php

$data = S_Useful::listDataForAutocomplete(UserContactBook::listContactBookItems('receiver'), 'id', 'RepresentingName', 'RepresentingNameForSearch');

Yii::app()->clientScript->registerCssFile('//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
?>
<style>
    #project-label {
        display: block;
        font-weight: bold;
        margin-bottom: 1em;
    }

    #project-description {
        margin: 0;
        padding: 0;
    }

    .ui-autocomplete {
        max-height: 300px;
        overflow-y: scroll;
        /* prevent horizontal scrollbar */
        overflow-x: hidden;
    }
    /* IE 6 doesn't support max-height
    * we use height instead, but this forces the menu to always be this tall
    */
    * html .ui-autocomplete {
        height: 100px;
    }
</style>

<input id="userAddressList<?php echo (isset($i)?$i.'_':'');?>">
<input type="hidden" id="userAddressList-id<?php echo (isset($i)?$i.'_':'');?>" >
<script>
    $(function() {
        var data = [
            <?php
foreach($data AS $item):
  ?>
            {
                value: "<?php echo $item['id'];?>",
                label: "<?php echo $item['text'];?>",
                desc: "<?php echo $item['desc'];?>"
            },


            <?php
            endforeach;
        ?>

        ];
        $( "#userAddressList<?php echo (isset($i)?$i.'_':'');?>" ).autocomplete({
            minLength: 0,
            source: data,
            focus: function( event, ui ) {
                $( "#userAddressList<?php echo (isset($i)?$i.'_':'');?>" ).val( ui.item.label );
                return false;
            },
            select: function( event, ui ) {
                $( "#userAddressList<?php echo (isset($i)?$i.'_':'');?>" ).val( ui.item.label );
                $( "#userAddressList-id<?php echo (isset($i)?$i.'_':'');?>" ).val( ui.item.value );
                return false;
            },

        })
            .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
                .append( "<a>" + item.label + "<br>" + item.desc + "</a>" )
                .appendTo( ul );
        };
    });

    $("#userAddressList<?php echo (isset($i)?$i.'_':'');?>").on('click', function(){
        if (this.value == "")
        {
            $(this).autocomplete("search");
        }
    });
</script>

<?php

echo CHtml::ajaxButton(Yii::t('_addressData','Pobierz'),
    Yii::app()->createUrl('userContactBook/ajaxGetData/'), array(
        'data' => 'js:{id : $("#'.(isset($i)?$i.'_':'').'userAddressList").val()}',
        'dataType' => 'json',
        'type' => 'get',
        'success' => 'js:function(result) {

            $.each(result, function(i,v)
            {

                $("#AddressData_'.(isset($i)?$i.'_':'').'" + i).val(v);
                $("#AddressData_'.(isset($i)?$i.'_':'').'" + i).trigger("change");
            });

}'
    ) // ajax
); // script

?>


