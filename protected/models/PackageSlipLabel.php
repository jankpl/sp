<?php
/**
 * Created by PhpStorm.
 * User: Janek
 * Date: 2019-03-12
 * Time: 14:55
 */

class PackageSlipLabel
{

    public static function generateReminderPdfPage(TCPDF $pdf, $item)
    {
        $packageId = $item->local_id;

        if($item instanceof Postal && $item->postalLabel->track_id)
            $packageId = $item->postalLabel->track_id;
        else
        {
            if($item->getExternalOperatorsAndIds(true)[1][0] != '')
                $packageId = $item->getExternalOperatorsAndIds(true)[1][0];
        }

        $pdf->AddPage('P');
        $pdf->SetMargins(10,10,10);

        if($item->user->getClientImg()) {
            $img = $item->user->getClientImg();
            $img = explode(',', $img);
            $img = base64_decode($img[1]);
            $pdf->Image('@' . $img, 10, 5, '', 15,'','', 'T', false, 300, 'C');
        }

        $pdf->SetY($pdf->GetY() + 17);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(80, 4, 'Packaging slip', 0, 'C', false, 1, 10, '');
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(80, 4, $packageId, 0, 'C', false, 1, 10, '');

        $pdf->SetY($pdf->GetY() + 10);

        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(50, 4, 'Receiver:', 0, 'L', false, 1, 10, '');
        $pdf->SetFont('freesans', '', 8);

        $text = '';
        $text .= $item->receiverAddressData->name ? $item->receiverAddressData->name."\r\n" : '';
        $text .= $item->receiverAddressData->company ? $item->receiverAddressData->company."\r\n" : '';
        $text .= $item->receiverAddressData->address_line_1."\r\n";
        $text .= $item->receiverAddressData->address_line_1."\r\n";
        $text .= $item->receiverAddressData->zip_code."\r\n";
        $text .= $item->receiverAddressData->city."\r\n";
        $text .= $item->receiverAddressData->country."\r\n";
        $pdf->MultiCell(40, 27, $text, 1, 'L', false, 1, '', '', true, 0, false, true,27, 'T', true);


        $pdf->SetY($pdf->GetY() + 5);
        $pdf->SetFont('freesans', 'B', 8);
        $pdf->MultiCell(50, 4, 'Content:', 0, 'L', false, 1, 10);

        if($item instanceof Postal)
            $text = $item->content;
        else
            $text = $item->package_content;


        $pdf->SetFont('freesans', 'B', 7);
        $pdf->MultiCell(10, 4, 'Qty', 1, 'C', false, 0, 10);
        $pdf->MultiCell(70, 4, 'Description', 1, 'L', false, 1, 20);
        $pdf->SetFont('freesans', '', 7);


//        $text = '2:bike|||1:tablet';
        $text = explode('|||', $text);

//        $item->note1 = '123123123';


        foreach($text AS $line) {

            $temp = explode(':', $line);
            if(sizeof($temp) > 1)
            {
                $no = array_shift($temp);
                $desc = implode(':', $temp);
            } else {
                $no = '-';
                $desc = $line;
            }
            $pdf->MultiCell(10, 4, $no, 1, 'C', false, 0, 10);
            $pdf->MultiCell(70, 4, $desc, 1, 'L', false, 1, 20, '', true, 0, false, true,4, 'T', true);
        }

        if($item->note1) {
            $pdf->SetFont('freesans', 'B', 8);
            $pdf->MultiCell(35, 4, 'Order ID:', 0, 'L', false, 1, 55, 39);
            $pdf->SetFont('freesans', '', 8);

            $text = $item->note1;
            $pdf->MultiCell(35, 10, $text, 0, 'L', false, 1, 55, 43);

            $barcodeStyle = array(
                'position' => '',
                'align' => 'L',
                'stretch' => false,
                'fitwidth' => true,
                'cellfitalign' => '',
                'border' => false,
                'hpadding' => 'auto',
                'vpadding' => 'auto',
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false, //array(255,255,255),
                'text' => true,
                'font' => 'helvetica',
                'fontsize' => 8,
                'stretchtext' => 4
            );

            $pdf->write1DBarcode($item->note1, 'C128', 53, 50, 35, 15, 0.3, $barcodeStyle);
//            $pdf->SetY($pdf->GetY() + 70);
        }

        if($item->user->getPackageSlipFooter()) {
            $pdf->SetFont('freesans', '', 6);
            $pdf->MultiCell(80, 4, $item->user->getPackageSlipFooter(), 0, 'C', false, 1, 10, 130);
        }

        return $pdf;
    }


}