<?php

class UserMessageController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    public function filters()
    {
        return array_merge(array(
            'accessControl', // perform access control for CRUD operations
            array(
                'application.filters.HttpsFilter',
                'bypass' => false,
            ),
        ));
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($no)
    {
        $this->panelLeftMenuActive = 702;

        $model = UserMessage::model()->findByAttributes(array('user_id' => Yii::app()->user->id, 'no' => $no));
        if($model === NULL OR $model->stat == UserMessage::UNPUBLISHED)
            throw new CHttpException(404);

        $model->seen();

        $this->render('view',array(
            'model'=> $model,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->panelLeftMenuActive = 702;

        $unseen = UserMessage::model()->findAll(array('condition' => 'user_id = :user_id AND stat = :stat', 'params' => array(':user_id' => Yii::app()->user->id, ':stat' => UserMessage::PUBLISHED), 'order' => 'no DESC'));

        $seen = UserMessage::model()->findAll(array('condition' => 'user_id = :user_id AND stat = :stat', 'params' => array(':user_id' => Yii::app()->user->id, ':stat' => UserMessage::SEEN), 'order' => 'no DESC'));

        $this->render('index',array(
            'unseen'=> $unseen,
            'seen'=> $seen,
        ));
    }



    public function actionGetFile($hash)
    {
        $model = UserMessage::model()->findByAttributes(array('hash' => $hash));

        if($model === NULL) {
            throw new CHttpException(404);
        }

        if($model->file_path == NULL OR $model->user_id != Yii::app()->user->id)
            throw new CHttpException(404);

        $path = $model->file_path;

        return Yii::app()->getRequest()->sendFile($model->file_name, @file_get_contents($path), $model->file_type);
    }
}

