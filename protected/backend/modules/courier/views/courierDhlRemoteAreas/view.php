<?php

/* @var $model CourierDhlRemoteAreas */


$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
    array('label'=>'Manage' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'date_entered',
        'date_updated',
        array(
            'name' => 'countryList',
            'value' => $data->countryList->name,
        ),
        'zip_from',
        'zip_to',
        'zip_txt',
        array(
            'label' => 'Own price',
            'value'=> $data->price_id == "" ? "no" : "yes",
        ),
    ),
)); ?>

<h2>Price addition</h2>
<table class="table table-striped" style="width: 50%;">
    <tr>
        <td>PLN</td>
        <td><?php echo $model->getPrice(Price::PLN); ?></td>
    </tr>
    <tr>
        <td>EUR</td>
        <td><?php echo $model->getPrice(Price::EUR); ?></td>
    </tr>
    <tr>
        <td>GBP</td>
        <td><?php echo $model->getPrice(Price::GBP); ?></td>
    </tr>
</table>