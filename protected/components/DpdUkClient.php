<?php

class DpdUkClient extends GlobalOperator
{
    const URL = 'https://api.interlinkexpress.com/';
    const LOGIN = 'romad1';
    const PASSWORD = 'operations@romad';
    const CUSTOMER_NO = '2297010';

    const TRACKING_URL = 'https://test-apps.geopostuk.com/tracking-core/dpd/parcels';
    const TT_USER = 'GEOTRACK';
    const TT_PASS = 'g30tr4ck';

    public $sender_name;
    public $sender_company;
    public $sender_address1;
    public $sender_address2;
    public $sender_zip_code;
    public $sender_city;
    public $sender_country;
    public $sender_mail;
    public $sender_tel;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_country_id;
    public $receiver_mail;
    public $receiver_tel;

    public $weight;
    public $value;
    public $content;

    public $local_id;

    public $_network_code = self::NC_PARCEL_NEXTDAY;

    const NC_INTERNATIONAL = 19; // DPD Classic

    const NC_PARCEL_NEXTDAY = 12;


    const NC_PARCEL_TWODAY = 11;
    const NC_PARCEL_DPD_12 = 13;
    const NC_PARCEL_DPD_1030 = 14;
    const NC_PARCEL_SATURDAY = 16;
    const NC_PARCEL_SATURDAY_12 = 17;
    const NC_PARCEL_SATURDAY_1030 = 18;
    const NC_PARCEL_SUNDAY_1030 = 7;
    const NC_PARCEL_SUNDAY_12 = 29;

    const NC_HOME_DELIVERY_AFTERNOON = 25;
    const NC_HOME_DELIVERY_EVENING = 21;

    const NC_EXPRESSPAK_NEXTDAY = 32;
    const NC_EXPRESSPAK_DPD_12 = 33;
    const NC_EXPRESSPAK_DPD_1030 = 34;
    const NC_EXPRESSPAK_SATURDAY = 36;
    const NC_EXPRESSPAK_SATURDAY_12 = 37;
    const NC_EXPRESSPAK_SATURDAY_1030 = 38;
    const NC_EXPRESSPAK_SATURDAY_TIMED = 35;
    const NC_EXPRESSPAK_SATURDAY_SUNDAY = '09';
    const NC_EXPRESSPAK_SATURDAY_SUNDAY_1030 = 23;
    const NC_EXPRESSPAK_SATURDAY_SUNDAY_12 = 51;

//    const NC_EXPRESSPAK_SATURDAY_SUNDAY_12 = 51;

    public static function getServices($fromCountry, $fromZip, $fromCity, $toCountry, $toZip, $toCity)
    {
        $data = new stdClass();

        $data->collectionDetails = new stdClass();
        $data->collectionDetails->address = new stdClass();
        $data->collectionDetails->address->countryCode = $fromCountry;
        $data->collectionDetails->address->postcode = $fromZip;
        $data->collectionDetails->address->locality = $fromCity;
        $data->deliveryDetails = new stdClass();
        $data->deliveryDetails->address = new stdClass();
        $data->deliveryDetails->address->countryCode = $toCountry;
        $data->deliveryDetails->address->postcode = $toZip;
        $data->deliveryDetails->address->locality = $toCity;
        $data->numberOfparcels = 1;
        $data->totalWeight = 1;
        $data->shipmentType = 0;
        $data->deliveryDirection = 1;

        $model = new self;
        $resp = $model->_curlCall('shipping/network/', $data, false, false, true);

        $networkCodes = [];
        if(is_array($resp->data->data))
            foreach($resp->data->data AS $item)
            {
                $networkCodes[] = $item->network->networkCode;
            }

        return $networkCodes;
    }

    protected static function getGeoSession()
    {
        $CACHE_NAME = 'DPDUK_GEO_SESSION';

        $geoSession = Yii::app()->cache->get($CACHE_NAME);

        if($geoSession === false OR $geoSession == '')
        {
            $resp = new self;
            $resp = $resp->_curlCall('user/?action=login', false, true);

            $geoSession = false;
            if($resp->success)
                $geoSession = $resp->data->data->geoSession;

            Yii::app()->cache->set($CACHE_NAME, $geoSession, 60*5);
        }

        return $geoSession;
    }

    public static function getAdditions(Courier $courier)
    {
        $CACHE_NAME = 'DPDUK_ADDITIONS_'.$courier->senderAddressData->country_id.'_'.$courier->senderAddressData->zip_code.'_'.$courier->senderAddressData->city.'_'.$courier->receiverAddressData->country_id.'_'.$courier->receiverAddressData->zip_code.'_'.$courier->receiverAddressData->city;

        $models = Yii::app()->cache->get($CACHE_NAME);

        if($models === false)
        {
            $services = self::getServices($courier->senderAddressData->country0->code, $courier->senderAddressData->zip_code, $courier->senderAddressData->city, $courier->receiverAddressData->country0->code, $courier->receiverAddressData->zip_code, $courier->receiverAddressData->city);

            $additions = [];
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_DPDUK_;


            foreach($additions AS $key => $item)
            {
                if(!is_array('2^'.$item, $services))
                    unset($additions[$key]);
            }


            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60*15);
        }

        return $models;
    }

    protected function _createShipment($returnError = false)
    {
        $data = new stdClass();

        $consignment = new stdClass();
        $consignment->collectionDetails = new stdClass();
        $consignment->collectionDetails->contactDetails = new stdClass();
        $consignment->collectionDetails->contactDetails->contactName = $this->sender_name;
        $consignment->collectionDetails->contactDetails->telephone = $this->sender_tel;
        $consignment->collectionDetails->address = new stdClass();
        $consignment->collectionDetails->address->organisation = $this->sender_company;
        $consignment->collectionDetails->address->countryCode = $this->sender_country;
        $consignment->collectionDetails->address->postcode = $this->sender_zip_code;
        $consignment->collectionDetails->address->street = trim($this->sender_address1.' '.$this->sender_address2);
        $consignment->collectionDetails->address->town = $this->sender_city;
        $consignment->deliveryDetails = new stdClass();
        $consignment->deliveryDetails->contactDetails = new stdClass();
        $consignment->deliveryDetails->contactDetails->contactName = $this->receiver_name;
        $consignment->deliveryDetails->contactDetails->telephone = $this->receiver_tel;
        $consignment->deliveryDetails->contactDetails->telephone = $this->sender_tel;
        $consignment->deliveryDetails->address = new stdClass();
        $consignment->deliveryDetails->address->organisation = $this->receiver_company;
        $consignment->deliveryDetails->address->countryCode = $this->receiver_country;
        $consignment->deliveryDetails->address->postcode = $this->receiver_zip_code;
        $consignment->deliveryDetails->address->street = trim($this->receiver_address1.' '.$this->receiver_address2);
        $consignment->deliveryDetails->address->town = $this->receiver_city;
        $consignment->deliveryDetails->notificationDetails = new stdClass();
        $consignment->deliveryDetails->notificationDetails->email = $this->receiver_mail;
        $consignment->deliveryDetails->notificationDetails->mobile = $this->receiver_tel;
        $consignment->networkCode = '2^'.$this->_network_code;
        $consignment->numberOfparcels = 1;
        $consignment->totalWeight = $this->weight;
        $consignment->shippingRef1 = $this->local_id;
        $consignment->customsValue = $this->value;
        $consignment->parcelDescription = $this->content;

        $data->consignment = [];
        $data->consignment[] = $consignment;

        MyDump::dump('dpduk.txt', print_r($data,1));

        $resp = $this->_curlCall('shipping/shipment', $data);

        if ($resp->success == true) {
            $return = [];

            $shipmentId = (string) $resp->data->data->shipmentId;
            $trackingId = (string) $resp->data->data->consignmentDetail[0]->parcelNumbers[0];


            $label = $this->_curlCall('shipping/shipment/' . $shipmentId . '/label/', false, false, true);

            if($label->success)
            {
                $label = $label->data;

                $label = str_replace('display:inline-block;', '', $label);
                $dompdf = new \Dompdf\Dompdf();

                $dompdf->loadHtml($label);
                $dompdf->setPaper('A4', 'horizontal');

                $dompdf->render();


                $pdf = $dompdf->output();

                $im = ImagickMine::newInstance();
                $im->setResolution(300, 300);
                $im->readImageBlob($pdf);
//                    $im->trimImage(0);
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }
                $im->trimImage(0);
                $im->stripImage();

                $label = $im->getImageBlob();

            }

            $return[] = GlobalOperatorResponse::createSuccessResponse($label, $trackingId);

            return $return;
        }
        else {

            $errorDesc = $resp->errorDesc;

            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($errorDesc);
            } else {
                return false;
            }
        }

    }

    protected function _curlCall($action, $data = false, $isLoginAction = false, $isLabelAction = false, $isListServicesAction = false, $isTracking = false)
    {

        $return = new stdClass();
        $return->success = false;
        $return->error = false;

//        $dataOryginal = $data;

        $headers = [];


        if($isTracking)
            $headers[] = 'Content-Type: application/xml';
        else
            $headers[] = 'Content-Type: application/json';

        if($isLabelAction)
            $headers[] = 'Accept: text/html';
        else
            $headers[] = 'Accept: application/json';

        $endpoint = self::URL.$action;


        if($isListServicesAction)
            $endpoint .= '?'.http_build_query($data);
        elseif($isTracking)
            $endpoint = self::TRACKING_URL;

        $endpoint = str_replace(['%5D%5B', '%5B', '%5D='], ['.', '.', '='], $endpoint);
        $curl = curl_init($endpoint);

        if($isLoginAction)
        {
            $pass = base64_encode(self::LOGIN.':'.self::PASSWORD);
            $headers[] = 'Authorization: Basic '.$pass;
        } else {

            if(!$isTracking) {
                $headers[] = 'GeoClient: account/' . self::CUSTOMER_NO;
                $headers[] = 'GeoSession: ' . $this->getGeoSession();
            }

            if($data && !$isListServicesAction) {

                if($isTracking)
                {
                    $data = '<?xml version="1.0" encoding="UTF-8"?>
                    <trackingrequest>
                        <user>'.self::TT_USER.'</user>
                        <password>'.self::TT_PASS.'</password>
                        <trackingnumbers>
                            <trackingnumber>'.$data.'</trackingnumber>
                        </trackingnumbers>
                    </trackingrequest>';
                } else {
                    $data = json_encode($data, JSON_UNESCAPED_UNICODE);
                }

                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
        }

        $post = true;
        if($isLabelAction OR $isListServicesAction)
            $post = false;

        curl_setopt_array($curl, array(
                CURLOPT_POST => $post ? TRUE : FALSE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING => "UTF-8",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false)

        );

        $resp = curl_exec($curl);


        MyDump::dump('dpduk.txt', print_r($endpoint,1));
        MyDump::dump('dpduk.txt', print_r($data,1));
        MyDump::dump('dpduk.txt', print_r($resp,1));
        MyDump::dump('dpduk.txt', print_r(curl_error($curl),1));

        $respOrg = $resp;
        $resp = json_decode($resp);

        if($isLabelAction)
        {
            if($respOrg === NULL)
            {
                $error = curl_error($curl);
                if($error == '')
                    $error = $respOrg;

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            } else {
                $return->success = true;
                $return->data = $respOrg;
            }
        }
        else
            if($resp === NULL)
            {
                $error = curl_error($curl);
                if($error == '')
                    $error = $respOrg;

                $return->error = $error;
                $return->errorCode = '';
                $return->errorDesc = $error;
            }
            else if($resp->error) {

                if(is_array($resp->error))
                    $error = $resp->error[0];
                else
                    $error = $resp->error;

                $return->error = $error->errorMessage.' : '.$error->obj;
                $return->errorCode = '';
                $return->errorDesc = $error->errorMessage.' : '.$error->obj;
            }
            else
            {
                $return->success = true;
                $return->data = $resp;
            }

        return $return;
    }


    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_tel = $from->tel;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->weight = $courierInternal->courier->getWeight(true);
        $model->content = $courierInternal->courier->package_content;
        $model->ref1 = $courierInternal->courier->local_id;
        $model->ref2 = $courierInternal->courier->user_id;

        return $model->_createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnErrors = false)
    {

        $model = new self;

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model->sender_name = $from->name;
        $model->sender_company = $from->company;
        $model->sender_zip_code = $from->zip_code;
        $model->sender_city = $from->city;
        $model->sender_address1 = $from->address_line_1;
        $model->sender_address2 = $from->address_line_2;
        $model->sender_country = $from->country0->code;
        $model->sender_tel = $from->tel;
        $model->sender_mail = $from->fetchEmailAddress(false);

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_country = $to->country0->code;
        $model->receiver_tel = $to->tel;
        $model->receiver_mail = $to->fetchEmailAddress(false);

        $model->weight = $courierU->courier->getWeight(true);
        $model->content = $courierU->courier->package_content;
        $model->ref1 = $courierU->courier->local_id;
        $model->ref2 = $courierU->courier->user_id;

        return $model->_createShipment($returnErrors);
    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {
        $model = new self;
        $model->_curlCall(false, $no, false, false, false, true);


        return false;
    }


}

