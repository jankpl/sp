<?php
/* @var $this SpPointsController */
/* @var $model SpPoints */

$this->breadcrumbs=array(
	'Sp Points'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create SpPoints', 'url'=>array('create')),
	array('label'=>'View SpPoints', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SpPoints', 'url'=>array('admin')),
);
?>

<h1>Update SpPoints <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'modelTrs' => $modelTrs)); ?>