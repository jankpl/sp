<?php

class SpecialTransportController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'actions'=> array('index', 'fastAssignCarrier', 'assignCarrierFromFile'),
            ),
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {

        $model = $this->loadModel($id, 'SpecialTransport');

        if(isset($_POST['SpecialTransport']))
        {

            $model->notes = $_POST['SpecialTransport']['notes'];
            if($model->validate(array('notes')))
            {
                if($model->update(array('notes')))
                {
                    Yii::app()->user->setFlash('success', 'Notes have been updated!');
                    Yii::app()->controller->refresh();
                }
            }
        }

        $this->render('view', array(
            'model' => $model,
        ));
    }

    public function actionMarkAsProcessed($id)
    {

        $model = SpecialTransport::model()->findByPk($id);
        if($model === NULL)
            return false;

        $model->stat = SpecialTransport::STAT_SEEN;
        $model->save(true,'stat');

        $this->redirect(array('/specialTransport/specialTransport/view', 'id' => $id));

    }

    public function actionMarkAsCancelled($id)
    {

        $model = SpecialTransport::model()->findByPk($id);
        if($model === NULL)
            return false;

        $model->stat = SpecialTransport::STAT_CANCELLED;
        $model->notes .= '|| Cancelled by #'.Yii::app()->user->id.' @ '.date('Y-m-d H:i:s');
        $model->save(true,['stat','notes']);

        $this->redirect(array('/specialTransport/specialTransport/view', 'id' => $id));

    }

    public function actionIndex() {

        $this->redirect(array('/specialTransport/specialTransport/admin'));
    }

    public function actionAdmin() {
        $model = new SpecialTransport('search');
        $model->unsetAttributes();

        if (isset($_GET['SpecialTransport']))
            $model->setAttributes($_GET['SpecialTransport']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}