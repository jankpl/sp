<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
	);


?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'salesman-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'name',
		'date_entered',
		'date_updated',
		'tel',
		'email',
		array(
			'name'=>'stat',
			'value'=>'S_Status::stat($data->stat)',
			'filter'=>CHtml::listData(S_Status::listStats(),'id','name'),
		),
        [
            'name' => 'default_injection_sp_point_id',
            'value' => '$data->defaultInjectionSpPoint->nameWithId',
            'filter' => CHtml::listData(SpPoints::model()->findAll(),'id', 'nameWithId'),
        ],
		array(
			'class' => 'CButtonColumn',
			'template'=>'{update} {view} {delete} {status} {status_a}',
			'htmlOptions'=> array('style'=>'width:80px'),
			'buttons'=>array(
				'status'=>array(
					'visible' => '$data->stat==1',
					'label'=>'deactivate',
					'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_active.png',
					'url' => 'Yii::app()->createUrl("salesman/stat",array("id" => $data->id))',
				),
				'status_a'=>array(
					'visible' => '$data->stat==0',
					'label'=>'activate',
					'imageUrl'=>Yii::app()->baseUrl.'/images/backend/ico_not_active.png',
					'url' => 'Yii::app()->createUrl("salesman/stat",array("id" => $data->id))',
				),
			),
		),
	),
)); ?>