<?php
/* @var $this UserInvoiceController */
/* @var $model UserInvoice */

$this->menu=array(
    array('label'=>'Create UserInvoice', 'url'=>array('create')),
);

?>

<h1>Manage User Invoices</h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div class="row">

</div>

    <div style="word-break: break-all">

        <?php
        $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'withChosenPlugin' => true, 'chosenSelector' => 'UserInvoiceGridView[user_id]'));
        ?>
        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>


<?php  $this->widget('backend.extensions.selgridview.BootSelGridView', array(
    'id'=>'user-fv-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'selectableRows'=>2,
    'afterAjaxUpdate' => $this->widget('MultipleSelect', array('selector' => '#stat-selector', 'afterAjaxUpdate' => true, 'withChosenPlugin' => true, 'chosenSelector' => 'UserInvoiceGridView[user_id]'), true),
    'selectionChanged'=>'function(id){  
                var number = $("#user-fv-grid").selGridView("getAllSelection").length;
                $(".selected-no").html(number); 
         }',
    'columns'=>array(
        [
            'name' => 'id',
            'htmlOptions' => [
                'width' => '30',
            ],
        ],
        'date_entered',
        'date_updated',
        array(
            'name' => 'user_id',
            'value' => '$data->user->login',
            'filter' => CHtml::listData(User::model()->findAll(['order' => 'login ASC']),'id', 'login'),
            'htmlOptions' => [
                'width' => '100',
            ],
        ),
        [
            'name' => 'no',
            'htmlOptions' => [
                'width' => '30',
            ],
        ],
        'title',
        'inv_date',
        'inv_payment_date',
        'inv_value',
        'paid_value',
        [
            'name' => 'inv_currency',
            'value' => 'Currency::nameById($data->inv_currency)',
            'filter' => CHtml::listData(Currency::model()->findAll(),"id", "short_name"),
        ],
        [
            'name' => '_isPaid',
            'value' => '$data->isPaid() ? "tak" : "nie"',
            'filter' => [
                0 => 'Nie',
                1 => 'Tak'
            ],
        ],
        [
            'name' => 'stat',
            'value' => 'UserInvoice::getStatArray()[$data->stat]',
            'filter' => UserInvoice::getStatArray(),
        ],
        [
            'name' => 'stat_inv',
            'value' => 'UserInvoice::getStatInvArray()[$data->stat_inv]',
            'filter' => UserInvoice::getStatInvArray(),
        ],
        [
            'name' => 'source',
            'value' => '$data->getSourceName()',
            'filter' => UserInvoice::getSourceList(),
        ],
        /*
        'user_id',
        'seen',
        */
        array(
            'class'=>'CButtonColumn',
        ),
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'selected_rows'
        ),
    ),
)); ?>


        <div class="text-right" style="border-bottom: 1px solid black; display: inline-block; float: right;">Selected items: <span class="selected-no" style="font-weight: bold;">0</span></div>
    </div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendor-own/chosen/chosen.jquery.min.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/vendor-own/chosen/chosen.min.css');
$script = "
$('[name=\'UserInvoiceGridView[user_id]\']').chosen({allow_single_deselect:true});
$('.chosen-drop').css('width', '300px');
";
Yii::app()->clientScript->registerScript('chosen-js', $script, CClientScript::POS_READY);

?>


<?php
if(AdminRestrictions::isSuperAdmin()):
    ?>

    <h4>Export selected invoices to ZIP</h4>

    <?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'mass-assign-stat',
    'enableAjaxValidation' => false, 'action' => Yii::app()->createUrl('/userInvoice/exportToZipPdfByGridView'),
));
    ?>

    <?php
    echo TbHtml::submitButton('Export', array(
        'onClick' => 'js:
    $("#user-invoices-ids").val($("#user-fv-grid").selGridView("getAllSelection").toString());
    ;',
        'name' => 'UserInvoices', 'size' => TbHtml::BUTTON_SIZE_LARGE));
    ?>

    <?php echo CHtml::hiddenField('UserInvoice[ids]','', array('id' => 'user-invoices-ids')); ?>
    <?php
    $this->endWidget();
    ?>

<?php
endif;
?>

<hr/>

<?php
if(AdminRestrictions::isSuperAdmin()):
?>

<h4>Export all invoices</h4>

<a href="<?= Yii::app()->createUrl('/userInvoice/exportAll');?>" class="btn btn-lg">Export all invoices to XLS</a>

<?php
endif;