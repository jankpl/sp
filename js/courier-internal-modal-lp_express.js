(function($) {

    window.CourierInternalModal_Lp_Express = function(){};

    var map;
    var dymek = new google.maps.InfoWindow();
    var markersArray = [];
    var spPointSelector;

    var listOfPoints;

    var mapStarted = false;


    function addMarker(lat,lng,txt,id, imgUrl)
    {

        if (markersArray[id] === undefined) {

            var img = {
                url: imgUrl,
                size: new google.maps.Size(32,32),
                orgin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(16,32)
            };

            var markerOptions =
                {
                    position: new google.maps.LatLng(lat,lng),
                    map: map,
                    icon: img
                };

            var marker = new google.maps.Marker(markerOptions);
            marker.txt=txt;

            google.maps.event.addListener(marker,'click',function()
            {
                dymek.setContent(marker.txt);
                dymek.open(map,marker);

                spPointSelector.val(id);
            });

            markersArray[id] = marker;
        }

    }

    $.extend(CourierInternalModal_Lp_Express, {
        mapStart: function(pointsList){

            listOfPoints = pointsList;

            if(mapStarted)
                return false;

            mapStarted = true;


            var mapOptions =
                {
                    center: new google.maps.LatLng(55.156659, 23.898397),
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.MAP
                };


            setTimeout(function(){
                map = new google.maps.Map(document.getElementById('lp-express-map'), mapOptions);

                mapAddPoints();
            }, 500);

            spPointSelector = $('#_modal_lp_list');
            spPointSelector.on('change', onSelectPoint);

            $('#lp_express_point_confirm').on('click', function(){

                var val = spPointSelector.val();

                if(val == '')
                    alert('Wybierz punkt!');
                else {

                    var valDesc = spPointSelector.find('option[value=' + val + ']').text();

                    console.log(val);
                    console.log(valDesc);

                    $('#_lp_express_receiver_point').val(val);
                    $('#_lp_express_receiver_point_address').val(valDesc + ' (#' + val + ')');

                    $('#modal').modal('hide');

                }

            });

        }
    });

    function onSelectPoint()
    {

        var markerId = $(this).val();

        if(markerId != '')
        {
            var marker = markersArray[markerId];

            map.panTo(marker.getPosition());
            map.setZoom(14);
            google.maps.event.trigger(marker,'click');

            spPointSelector.unbind('change');
            spPointSelector.val(markerId);
            spPointSelector.on('change', onSelectPoint);
        }

    }

    function mapAddPoints()
    {
        $.each(listOfPoints, function(i,v){
            addMarker(v.lat, v.lng,'<strong>' + v.name + '</strong><br />' + v.desc + '<br/><br/>' + v.address, v.id, v.imgUrl);
        });

    }

})(jQuery);

