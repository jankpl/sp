<?php


class lettersNumbersBasicValidator extends CValidator
{
    // LATIN + POLISH + GERMAN
//    protected $pattern = '/^([-0-9a-z\s\.-\/ęóąśłżźćńĘÓĄŚŁŻŹĆŃäåæğëöøßşüÿÄÅÆĞËÖØŞÜŸ])+$/i';
    // LATIN + POLSIGH + GERMAN + FRENCH + SPANISH + RUSSIAN
//    protected $pattern = '/^([-0-9a-z\s\.-\/&_)(,\\\ęóąśłżźćńĘÓĄŚŁŻŹĆŃäåæğëöøßşüÿÄÅÆĞËÖØŞÜŸéèàùâêîôûïçÉÈÀÙÂÊÎÔÛÏÇáíúñÁÍÚÑЩщЁЖХЦЧШЮяёжхцчшюяАБВГДЕЗИЙКЛМНОПРСТУФЬЫЪЭабвгдезийклмнопрстуфьыъэº°ª:\'ŠŘÁÍĚÝšřáíěýŷ"´`òÒ@+*?|\[\]!ı#;’%–~®])+$/iu';
//    protected $pattern = '/^([-0-9a-zęóąśłżźćńĘÓĄŚŁŻŹĆŃ\s\.-\/])+$/i';
//    protected $errorMessage = 'Wprowadzono niedozwolone znaki w polu ';

    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object,$attribute)
    {

        if($object->$attribute == '')
            return;

//        $pattern = $this->pattern;

        // extract the attribute value from it's model object
//        $value=$object->$attribute;

        $object->$attribute = trim(preg_replace('/[^(\p{L}\d\s\-_\/\\\.,&\[\]:*+#;@?%()®]/u', '', $object->$attribute));

//        if(!preg_match($pattern, $value))
//        {
//            $this->addError($object,$attribute,$this->errorMessage.' '.$object->getAttributeLabel($attribute).'!');
//        }
    }

    /**
     * Returns the JavaScript needed for performing client-side validation.
     * @param CModel $object the data object being validated
     * @param string $attribute the name of the attribute to be validated.
     * @return string the client-side validation script.
     * @see CActiveForm::enableClientValidation
     */
//    public function clientValidateAttribute($object,$attribute)
//    {
//
//        $pattern = $this->pattern;
//
//        $condition="!value.match({$pattern}) && value!=''";
//
//        return "
//if(".$condition.") {
//    messages.push(".CJSON::encode($this->errorMessage.' '.$object->getAttributeLabel($attribute).'!').");
//}
//";
//    }

}