<?php

Yii::import('application.modules.courier.models._base.BaseCourierTypeInternalSimple');

class CourierTypeInternalSimple extends BaseCourierTypeInternalSimple
{

    public $regulations;

    const OPERATOR_OWN = 1;
    const OPERATOR_UPS = 2;
    const OPERATOR_UPS_SAVER = 3;
    const OPERATOR_GLS = 4;

    // for preffered pickup time for UPS
    public $_preffered_pickup_day;
    public $_preffered_pickup_time;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

//Austria - AT
//Belgium - BE
//Bulgaria - BG
//Czech Republic - CZ
//Denmark - DK
//Estonia - EE
//Finland - FI
//France - FR
//Germany - DE
//Greece - GR
//Hungary - HU
//Ireland - IE
//Italy - IT
//Latvia - LV
//Lithuania - LT
//Luxembourg - LU
//Netherlands - NL
//Poland - PL
//Portugal - PT
//Romania - RO
//Slovakia - SK
//Slovenia - SI
//Spain - ES
//Sweden - SE
//United Kingdom - GB

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bAcceptRegulationsByDefault' =>
                    [
                        'class'=>'application.models.bAcceptRegulationsByDefault'
                    ],
            ]
        );
    }

    public static function getCountryList()
    {

        // NULL == ALL COUNTRIES
        return NULL;

        $models = CountryList::model()->findAll('code IN (
            "AT",
            "BE",
            "BG",
            "CZ",
            "DK",
            "EE",
            "FI",
            "FR",
            "DE",
            "GR",
            "HU",
            "IE",
            "IT",
            "LV",
            "LT",
            "LU",
            "NL",
            "PL",
            "PT",
            "RO",
            "SK",
            "SI",
            "ES",
            "SE",
            "GB"
        )');

        return $models;
    }


    public function getOperatorForCountry($code)
    {
        $operator = false;
        switch($code)
        {

            case "AT":
                $operator = self::OPERATOR_UPS;
                break;
            case "BE":
                $operator = self::OPERATOR_UPS;
                break;
            case "BG":
                $operator = self::OPERATOR_UPS;
                break;
            case "CZ":
                $operator = self::OPERATOR_UPS;
                break;
            case "DK":
                $operator = self::OPERATOR_UPS;
                break;
            case "EE":
                $operator = self::OPERATOR_UPS;
                break;
            case "FI":
                $operator = self::OPERATOR_UPS;
                break;
            case "FR":
                $operator = self::OPERATOR_UPS;
                break;
            case "DE":
                $operator = self::OPERATOR_UPS;
                break;
            case "GR":
                $operator = self::OPERATOR_UPS;
                break;
            case "HU":
                $operator = self::OPERATOR_UPS;
                break;
            case "IE":
                $operator = self::OPERATOR_UPS;
                break;
            case "IT":
                $operator = self::OPERATOR_UPS;
                break;
            case "LV":
                $operator = self::OPERATOR_UPS;
                break;
            case "LT":
                $operator = self::OPERATOR_UPS;
                break;
            case "LU":
                $operator = self::OPERATOR_UPS;
                break;
            case "NL":
                $operator = self::OPERATOR_UPS;
                break;
            case "PL":
                $operator = self::OPERATOR_UPS;
                break;
            case "PT":
                $operator = self::OPERATOR_UPS;
                break;
            case "RO":
                $operator = self::OPERATOR_UPS;
                break;
            case "SK":
                $operator = self::OPERATOR_UPS;
                break;
            case "SI":
                $operator = self::OPERATOR_UPS;
                break;
            case "ES":
                $operator = self::OPERATOR_UPS;
                break;
            case "SE":
                $operator = self::OPERATOR_UPS;
                break;
            case "GB":
                $operator = self::OPERATOR_UPS;
                break;
        }

        return $operator;
    }

    public static function getOperatorsList()
    {
        $data = [
            self::OPERATOR_UPS => 'UPS',
            self::OPERATOR_UPS_SAVER => 'UPS Saver',
            self::OPERATOR_GLS => 'GLS',
        ];

        return $data;
    }

    public function isItTooBig()
    {
        return S_PackageMaxDimensions::isPackageTooBig($this->courier);
    }

    public function isItTooHeavy()
    {
        return S_PackageMaxDimensions::isPackageTooHeavy($this->courier);
    }


    public function getMaxDimensions()
    {
        return S_PackageMaxDimensions::getPackageMaxDimensions();
    }

    public function getMaxWeight()
    {
        return S_PackageMaxDimensions::getPackageMaxDimensions()['weight'];
    }

    public function getDimensionWeight()
    {
        return S_PackageMaxDimensions::calculateDimensionsWeight($this->courier);
    }

    public function isDimensionWeightBigger()
    {
        if(S_PackageMaxDimensions::calculateDimensionsWeight($this->courier) > $this->courier->package_weight)
            return true;
        else
            return false;
    }

    public function getFinalWeight()
    {
        return S_PackageMaxDimensions::getFinalWeight($this->courier);
    }

    public function saveNewPackage()
    {
        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        if(!$this->courier->saveWithAddressData(false))
            $errors = true;

        // $this->operator = self::getOperatorForCountry($this->courier->receiverAddressData->country0->code);
        $this->dimension_weight = self::getDimensionWeight();

        $this->courier_id = $this->courier->id;

        if(!$this->save())
            $errors = true;

        if($errors)
        {
            $transaction->rollback();
            return false;
        } else {
            $transaction->commit();
            $this->courier->changeStat(CourierStat::PACKAGE_PROCESSING);

            return true;
        }

    }

    public function rules() {
        return array(
            array('_preffered_pickup_day, _preffered_pickup_time', 'numerical'),

            array('stat', 'default', 'setOnEmpty' => true, 'value' => 1),

            array('operator', 'required'),
            array('operator', 'in', 'range' => array_keys(self::getOperatorsList())),

            array('with_pickup', 'numerical', 'integerOnly' => true, 'min' => 0),
            array('with_pickup', 'default', 'setOnEmpty' => true, 'value' => 0),

            array('courier_id', 'required', 'except' => 'step1,api'),
            array('operator, courier_ext_operator_details_id, stat', 'numerical', 'integerOnly'=>true),
            array('courier_id', 'length', 'max'=>10),
            array('courier_ext_operator_details_id', 'default', 'setOnEmpty' => true, 'value' => null),


            array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('courier', 'Musisz zaakceptować regulamin.'), 'on' => 'summary'),

            array('regulations', 'numerical', 'integerOnly' => true),
            array('dimension_weight', 'numerical'),

            array('id, courier_id, operator, courier_ext_operator_details_id, stat, dimension_weight', 'safe', 'on'=>'search'),
        );
    }


    public function finalizeDetails()
    {

        if($this->operator == self::OPERATOR_OWN)
        {

        }
        else if($this->operator == self::OPERATOR_UPS
            OR $this->operator == self::OPERATOR_UPS_SAVER
            OR $this->operator == self::OPERATOR_GLS)
        {
            $courierExtOperatorDetails = new CourierExtOperatorDetails();
            $courierExtOperatorDetails->stat = CourierExtOperatorDetails::STAT_FAIL;
            $courierExtOperatorDetails->operator = $this->operator;
            if($courierExtOperatorDetails->save())
            {
                $this->courier_ext_operator_details_id = $courierExtOperatorDetails->id;
                if($this->update(array('courier_ext_operator_details_id')))
                {

                    $result = $this->orderLabelNew();

                    if($result)
                    {
                        $courierExtOperatorDetails->courier_label_new_id = $result->id;
                        $courierExtOperatorDetails->stat = CourierExtOperatorDetails::STAT_OK;
                        $courierExtOperatorDetails->update(array('stat', 'courier_label_new_id'));

                        return true;
                    } else {
                        $courierExtOperatorDetails->stat = CourierExtOperatorDetails::STAT_FAIL;
                        $courierExtOperatorDetails->update(array('stat'));
                        return false;
                    }
                }
            }
            return false;
        }
    }

    protected function orderLabelNew()
    {

        if($this->operator == self::OPERATOR_UPS OR $this->operator == self::OPERATOR_UPS_SAVER)
        {
            $courierLabelNew = new CourierLabelNew();
            $courierLabelNew->triggers_pickup = true;

            if($this->operator == self::OPERATOR_UPS)
                $courierLabelNew->operator = CourierLabelNew::OPERATOR_UPS;
            else if($this->operator == self::OPERATOR_UPS_SAVER)
                $courierLabelNew->operator = CourierLabelNew::OPERATOR_UPS_SAVER;

            $courierLabelNew->courier_id = $this->courier->id;
            $courierLabelNew->address_data_from_id = $this->courier->sender_address_data_id;
            $courierLabelNew->address_data_to_id = $this->courier->receiver_address_data_id;

            if($this->courier->source == Courier::SOURCE_API)
                $courierLabelNew->stat = CourierLabelNew::STAT_NEW_DIRECT;
            else
                $courierLabelNew->stat = CourierLabelNew::STAT_NEW;

            if($courierLabelNew->save())
                return $courierLabelNew;
            else
                return false;

        }
        else if($this->operator == self::OPERATOR_GLS)
        {

            $courierLabelNew = new CourierLabelNew();
            $courierLabelNew->triggers_pickup = true;

            $courierLabelNew->operator = CourierLabelNew::OPERATOR_GLS;
            $courierLabelNew->courier_id = $this->courier->id;
            $courierLabelNew->address_data_from_id = $this->courier->sender_address_data_id;
            $courierLabelNew->address_data_to_id = $this->courier->receiver_address_data_id;
            $courierLabelNew->stat = CourierLabelNew::STAT_NEW;

            if($courierLabelNew->save())
                return $courierLabelNew;
            else
                return false;

        }
        return false;

    }

    public function getOperator()
    {
        if($this->courierExtOperatorDetails->courierLabelNew != NULL)
        {
            $operator = $this->courierExtOperatorDetails->courierLabelNew->operator;

            if($operator)
                return $this->courierExtOperatorDetails->courierLabelNew->getOperators()[$operator];

        }

        //otherwise, if operator not found in CourierLabelNew (old compatibility):
        switch($this->operator)
        {
            case self::OPERATOR_OWN:

                $return = 'KAAB';
                break;

            case self::OPERATOR_UPS:

                $return = 'UPS';
                break;

            case self::OPERATOR_UPS_SAVER:

                $return = 'UPS Saver';
                break;

            case self::OPERATOR_GLS:

                $return = 'GLS';
                break;
        }

        return $return;
    }

    public static function generateCarriageTicketsNotGeneratedInternalSimple($type = NULL)
    {
        $models = Courier::model()->with('courierPdf')->findAllByAttributes(array('user_id' => Yii::app()->user->id), 'courier_stat_id != :cancelled_stat AND courier_stat_id != :cancelled_stat2 AND (courierPdf.stat IS NULL OR courierPdf.stat != 1) AND t.id IN (SELECT courier_id FROM courier_type_internal_simple) AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', array(':cancelled_stat' => CourierStat::CANCELLED, ':cancelled_stat2' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_CONFIRMED, ':uc2' => Courier::USER_CANCEL_REQUEST, ':uc3' => Courier::USER_CANCEL_WARNED));

        if(!S_Useful::sizeof($models))
            return false;

        LabelPrinter::generateLabels($models, $type);

        return true;
    }


    public static function generateCarriageTicketsTodaysInternalSimple($type = NULL)
    {
        $models = Courier::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id), 'courier_stat_id != :cancelled_stat AND courier_stat_id != :cancelled_stat2 AND t.id IN (SELECT courier_id FROM courier_type_internal_simple) AND  date_entered >= DATE_SUB( CURDATE( ) , INTERVAL 1 DAY ) AND user_cancel != :uc1 AND user_cancel != :uc2 AND user_cancel != :uc3', array(':cancelled_stat' => CourierStat::CANCELLED, ':cancelled_stat2' => CourierStat::CANCELLED_USER, ':uc1' => Courier::USER_CANCEL_CONFIRMED, ':uc2' => Courier::USER_CANCEL_REQUEST, ':uc3' => Courier::USER_CANCEL_WARNED));

        if(!S_Useful::sizeof($models))
            return false;

        LabelPrinter::generateLabels($models, $type);

        return true;
    }
    public function isWithPickup()
    {
        if($this->with_pickup === NULL)
            return '?';
        else if($this->with_pickup)
            return 'yes';
        else
            return 'no';
    }


    protected static function serialize(array $data)
    {
        $data = serialize($data);
        $data = base64_encode($data);

        return $data;
    }

    protected static function unserialize($data)
    {
        $data = base64_decode($data);
        $data = unserialize($data);

        return $data;
    }

    /**
     * Serialize additional params (ex. Preffered pickup time)
     * @return bool
     */
    public function beforeSave()
    {
        $this->params = array(
            '_preffered_pickup_day' => $this->_preffered_pickup_day,
            '_preffered_pickup_time' => $this->_preffered_pickup_time,
        );

        if($this->params !== NULL)
            $this->params = self::serialize($this->params);

        return parent::beforeSave();
    }

    /**
     * Unserialize additional params (ex. Preffered pickup time)
     * @return bool
     */
    public function afterFind()
    {
        if($this->params !== NULL)
            $this->params = self::unserialize($this->params);



        if(is_array($this->params))
            foreach($this->params AS $key => $item)
            {
                try {
                    $this->$key = $item;
                }
                catch (Exception $ex)
                {}
            }

        parent::afterFind();
    }


    /**
     * Return params as arrays with keys as attribute labels
     * @return array
     */
    public function getParamsWithLabelNames()
    {
        $data = array();
        if(is_array($this->params))
        {

            foreach($this->params AS $key => $item)
            {

                $data[$this->getAttributeLabel($key)] = $item;
            }
        }

        return $data;
    }

}