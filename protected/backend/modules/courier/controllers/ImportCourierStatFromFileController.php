<?php

class ImportCourierStatFromFileController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        $fileModel = new F_UploadFile();

        // mapping attributes to columns in file
        $attributes = Array(
            'local_id' => 0,
            'courier_stat' => 1,
            'date' => 2,
        );

        $hash = $_REQUEST['hash'];
        $step = $_REQUEST['step'];
        $part = $_REQUEST['part'];
        $ajax = $_GET['ajax'];

        ///////////////

        if($hash == NULL)
        {
            if(Yii::app()->session['lffs']['hash'] == '')
            {
                $hash = md5(time());
                $session = Yii::app()->session['lffs'];
                $session['hash'] = $hash;
                $session['step'] = 1;
                $session['alreadySaved'] = 0;
                Yii::app()->session['lffs'] = $session;
            }
            else
            {
                $hash = Yii::app()->session['lffs']['hash'];
                $session = Yii::app()->session['lffs'];
                $session['step'] = 1;
                $session['alreadySaved'] = 0;
                Yii::app()->session['lffs'] = $session;
            }
        }

        // file upload
        if(isset($_POST['F_UploadFile']))
        {
            $fileModel->setAttributes($_POST['F_UploadFile']);


            $fileModel->file =
                CUploadedFile::getInstance($fileModel, 'file');
            if(!$fileModel->validate())
            {
                $this->render('importStatFromFile/index', array(
                    'fileModel' => $fileModel,
                ));
                return;
            } else {
                // file upload success:

                $inputFile = $fileModel->file->tempName;

//                Yii::import('ext.phpexcel.XPHPExcel');
//                XPHPExcel::init();

                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFile);
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($inputFile);

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $data = [];
                $start = 1;

                if($fileModel->omitFirst)
                {
                    $headerSeen = false;
                } else
                    $headerSeen = true;

                for ($row = $start; $row <= $highestRow; $row++){
                    if(!$headerSeen)
                    {
                        $headerSeen = true;
                        continue;
                    }

                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, FALSE, TRUE);
                    array_push($data, $rowData[0]);
                }
                Yii::app()->cache->set('tempInput_'.$hash, $data, 86400);

                switch($fileModel->split)
                {
                    case F_UploadFile::SPLIT_FULL_EDIT:
                        $splitAfter = F_UploadFile::SPLIT_AFTER_FULL_EDIT_STAT;
                        break;
                    case F_UploadFile::SPLIT_DEFAULT:
                    default:
                        $splitAfter = F_UploadFile::SPLIT_AFTER_DEFAULT_STAT;
                        break;
                }

                $session = Yii::app()->session['lffs'];
                $session['split'] = $fileModel->split;
                $session['splitAfter'] =  $splitAfter;
                $session['lines'] = S_Useful::sizeof($data);
                $session['parts'] = ceil(S_Useful::sizeof($data) / $splitAfter);

                Yii::app()->session['lffs'] = $session;

//                Yii::log('Sesja:'.print_r($session['split'],1),CLogger::LEVEL_ERROR);

                $this->render('importStatFromFile/index', array(
                    'fileModel' => $fileModel,
                    'uploadSuccess' => true,
                ));
                return;
            }

        } else if($ajax == 1) {

            // because models are loaded from cache:
            Yii::import('zii.behaviors.CTimestampBehavior');
            //

            if($step == NULL OR $step == 1)
            {

                $return = array(
                    'hash' => $hash,
                    'lastStep' => 1,
                    'parts' => $session['parts'],
                    'part' => $part,
                );

                echo CJSON::encode($return);
                return;
            }
            else if($step == 2)
            {
//                Yii::log('step 2', CLogger::LEVEL_ERROR);

                $models = [];
                $data = Yii::app()->cache->get('tempInput_'.$hash);

                $session = Yii::app()->session['lffs'];

                $start =  $session['splitAfter'] * ($part - 1);
                $end = $start + $session['splitAfter'];

                if(S_Useful::sizeof($data))
                {
                    $i = 0;
                    foreach($data AS $item)
                    {
                        $i++;
                        if($i < $start OR $i >= ($end))
                            continue;

                        $model = new F_MassStatusChange();
                        $model->status_date_text = $item[$attributes['date']];
                        $model->courier_local_id = $item[$attributes['local_id']];
                        $model->status_text = $item[$attributes['courier_stat']];
                        $model->work();

                        array_push($models, $model);

                    }
                    Yii::app()->cache->set('tempModels_'.$hash.'_'.$part, $models,86400);

                    $return = array(
                        'hash' => $hash,
                        'lastStep' => 2,
                        'part' => $part,
                        'size' => S_Useful::sizeof($models),
                    );

                    echo CJSON::encode($return);
                    return;
                }


            }
            else if($step == 3)
            {

                $models = Yii::app()->cache->get('tempModels_'.$hash.'_'.$part);

                $session = Yii::app()->session['lffs'];
                $remaining = $session['parts'] - $session['alreadySaved'];


                $html = $this->renderPartial('importStatFromFile/importCourierStatFromFile', array(
                    'models' => $models,
                    'remaining' => $remaining,
                    'total' => $session['parts'],
                    'saved' => $session['alreadySaved'],
                    'split' => $session['split'],
                ), true, false);


                echo CJSON::encode(array('html' => $html));
                return;
            }
            else if($step == 4)
            {


                $mode = $_POST['mode'];


                $session = Yii::app()->session['lffs'];
                $data = Yii::app()->cache->get('tempModels_'.$hash.'_'.$part);

                $models = [];
                if(is_array($_POST['F_MassStatusChange']))
                    foreach($_POST['F_MassStatusChange'] AS $key => $item)
                    {

                        $model = new F_MassStatusChange();
                        $model->setAttributes($item);

                        if(is_array($data) && $session['split'] != F_UploadFile::SPLIT_FULL_EDIT)
                        {
                            $item = $data[$key];

                            $model->status_date_text = $item->status_date_text;
                            $model->courier_local_id = $item->courier_local_id;
                            $model->status_text = $item->status_text;
                        }

                        array_push($models, $model);
                    }


                try
                {

                    if($mode == Courier::MODE_JUST_VALIDATE)
                    {
                        foreach($models AS $model)
                            $model->work();

                        $result = $models;
                    } else {
                        $result = F_MassStatusChange::saveGroupOfPackages($models);

                    }



                }
                catch (Exception $ex)
                {


                    $session = Yii::app()->session['lffs'];
                    $remaining = $session['parts'] - $session['alreadySaved'];



                    $html = $this->renderPartial('importStatFromFile/importCourierStatFromFile', array(
                        'models' => $models,
                        'remaining' => $remaining,
                        'total' => $session['parts'],
                        'saved' => $session['alreadySaved'],
                        'split' => $session['split'],
                        'errors' => true,
                    ), true, false);

                    echo CJSON::encode(array('html' => $html));
                    return;
                }

                if(is_array($result))
                {

                    $session = Yii::app()->session['lffs'];
                    $remaining = $session['parts'] - $session['alreadySaved'];

                    $models = $result;

                    $html = $this->renderPartial('importStatFromFile/importCourierStatFromFile', array(
                        'models' => $models,
                        'remaining' => $remaining,
                        'total' => $session['parts'],
                        'saved' => $session['alreadySaved'],
                        'split' => $session['split'],
                        'errors' => $mode == Courier::MODE_JUST_VALIDATE?false:true,
                    ), true, false);

                    echo CJSON::encode(array('html' => $html));
                    return;


                } else {
                    Yii::app()->cache->set('tempModels_'.$hash.'_'.$part, true, 86400);

                    $session = Yii::app()->session['lffs'];
                    $session['alreadySaved'] += 1;
                    Yii::app()->session['lffs'] = $session;

                    $remaining = $session['parts'] - $session['alreadySaved'];


                    echo CJSON::encode(array('html' =>  TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, 'Statuses updated:'.$result, array('style' => 'font-size: 16px; text-align: center;', 'closeText' => false)).'<br/>'.TbHtml::alert(TbHtml::ALERT_COLOR_WARNING, 'Remaining to save: <strong>'.$remaining.'</strong>/'.$session['parts'], array('style' => 'font-size: 16px; text-align: center;', 'closeText' => false))));
                    return;
                }



            }
        }


        $this->render('importStatFromFile/index', array(
            'fileModel' => $fileModel,
        ));
    }


}