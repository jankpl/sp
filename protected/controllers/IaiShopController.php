<?php


class IaiShopController extends CController {


    public static function actionOrders()
    {
        $logID = uniqid();

        MyDump::dump('iai.txt', $logID.': START');
        $request = file_get_contents('php://input');
        MyDump::dump('iai.txt', $logID.':'.print_r($request,1));

        $request = json_decode($request, true);

        $authorized = false;
        $xmlProcessed = false;

        $user = $request['user'];
        $hash = $request['hash'];
        $isf = $request['isf'];
        $time = $request['time'];

//        MyDump::dump('iai-request.txt', print_r($request,1));

        $cmd = Yii::app()->db->createCommand();
        $api_key = $cmd->select('apikey')
            ->from((new User)->tableName())
            ->where('login = :login AND apikey IS NOT NULL', [
                ':login' => $user,
            ])
            ->queryScalar();


        if($api_key) {

            $isfHash = hash('sha256', $isf);
            $checkHash = hash('sha256', $user . $isfHash . $api_key . $time);


            if($hash == $checkHash)
                $authorized = true;
        }

//        $authorized = true;
        $xml = simplexml_load_string($isf);


        /* @var $user User */
        $user = User::model()->findByAttributes(['apikey' => $api_key]);

        for($i = 0; $i < sizeof($xml->event); $i++) {
            $xmlProcessed = true;

            $cod = floatval($xml->event[$i]->shipment->pickup->cod->amount);
//            $cod_account = (string) $xml->event[$i]->shipment->pickup->cod->transfer_account_id; // Looks like IAI is not providing account
            $cod_account = '';
            if($cod) {

                if ($user)
                    $cod_account = $user->getBankAccount();
            }

            $value1 = floatval($xml->event[$i]->shipment->additional_services->insurance);
            $value2 = floatval($xml->event[$i]->shipment->additional_services->dutiable);

            $value = $value1 > $value2 ? $value1 : $value2;


            $weight = floatval($xml->event[$i]->shipment->package->description->weight);
            $weight /= 1000;
            $weight = round($weight,1);
            if($weight < 0.1)
                $weight = 0.1;

            $package = [];
            $package['weight'] = $weight;
            $package['size_l'] = (string) $xml->event[$i]->shipment->package->description->dimensions->length;
            $package['size_w'] = (string) $xml->event[$i]->shipment->package->description->dimensions->width;
            $package['size_d'] = (string) $xml->event[$i]->shipment->package->description->dimensions->height;
            $package['value'] = $value;
            $package['content'] = (string) $xml->event[$i]->shipment->package->description->content_description;


            $sender = [];
            $sender['name'] = (string) $xml->event[$i]->sender->address->name;
            $sender['company'] = (string) $xml->event[$i]->sender->address->company;
            $sender['address_line_1'] = (string) $xml->event[$i]->sender->address->address;
            $sender['address_line_2'] = '';
            $sender['country'] = (string) $xml->event[$i]->sender->address->country;
            $sender['zip_code'] = (string) $xml->event[$i]->sender->address->postcode;
            $sender['city'] = (string) $xml->event[$i]->sender->address->city;
            $sender['tel'] = (string) $xml->event[$i]->sender->address->telephone;
            $sender['email'] = (string) $xml->event[$i]->sender->notification->email[1]['address'];

            $receiver = [];
            $receiver['name'] = (string) $xml->event[$i]->receiver->address->name;
            $receiver['company'] = (string) $xml->event[$i]->receiver->address->company;
            $receiver['address_line_1'] = (string) $xml->event[$i]->receiver->address->address;
            $receiver['address_line_2'] = '';
            $receiver['country'] = (string) $xml->event[$i]->receiver->address->country;
            $receiver['zip_code'] = (string) $xml->event[$i]->receiver->address->postcode;
            $receiver['city'] = (string) $xml->event[$i]->receiver->address->city;
            $receiver['tel'] = (string) $xml->event[$i]->receiver->contact->telephone;
            $receiver['email'] = (string) $xml->event[$i]->receiver->notification->email[1]['address'];

            if($receiver['tel'] == '' && $user->getDefaultTelNo())
                $receiver['tel'] = $user->getDefaultTelNo();


            $options = [];
            $options['number'] = 1;
            $options2['label_type'] = 'PDF';

            if($cod > 0)
            {
                $options['cod'] = $cod;
                $options['cod_bank_account'] = $cod_account;
            }

            $data = [
                'package' => $package,
                'sender' => $sender,
                'receiver' => $receiver,
                'options' => $options,
                'options2' => $options2,
            ];


            if(!$authorized)
            {
                $xml->event[$i]->errors->error->priority = 'critical';
                $xml->event[$i]->errors->error->code = 'NOT_AUTHORIZED';
                $xml->event[$i]->errors->error->short_description = 'You have provided invalid credentials or your API access is not activated by administrator!';
                $xml->event[$i]->errors->error->note = '';

            } else {
                MyDump::dump('iai.txt', $logID.': START API REQUEST');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

//                curl_setopt($ch, CURLOPT_URL, 'http://localhost/SwiatPrzesylek.pl/f2/V1/courier/create-pre-routing');
                curl_setopt($ch, CURLOPT_URL, 'https://api.swiatprzesylek.pl/V1/courier/create-pre-routing');
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $api_key);
//                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $result = curl_exec($ch);
                MyDump::dump('iai.txt', $logID.': END API REQUEST');

                $result = json_decode($result);

                $packagesIds = [];
                if ($result->result === "OK") {

                    foreach ($result->response->packages AS $package) {

                        $packagesIds[] = "'".$package->package_id."'";
                        $package_id = $package->external_id ? $package->external_id : $package->package_id;

                        $xml->event[$i]->shipment->tracking_number = $package_id;
                        $xml->event[$i]->shipment->tracking_url = 'https://swiatprzesylek.pl/tt/' . $package_id;
                        $xml->event[$i]->shipment->package->tracking_number = $package_id;

                        $xml->event[$i]->shipment->cost->net = '99';
                        $xml->event[$i]->shipment->cost->vat_percent = '23';
                        $xml->event[$i]->shipment->cost->gross = '121.77';

                        if ($package->result === "OK") {
                            $labels_no = $package->labels_no;

                            if ($labels_no)
                                foreach ($package->labels AS $label) {

                                    // label is already base64 label
                                    $xml->event[$i]->shipment->label->base64 = $label;

//                                    $image = base64_decode($label);
//
//                                    $pdf = PDFGenerator::initialize();
//                                    $pageW = 100;
//                                    $pageH = 150;
//
//                                    $xBase = 2;
//                                    $yBase = 2;
//
//                                    $blockWidth = $pageW - (2 * $xBase);
//                                    $blockHeight = $pageH - (2 * $yBase);
//
//                                    $pdf->AddPage('H', array(100, 150), true);
//                                    // PRINT IMAGE TO PDF
//                                    $pdf->Image(
//                                        '@' . $image, // file
//                                        $xBase, // x
//                                        $yBase, // y
//                                        $blockWidth, // w
//                                        $blockHeight, // h
//                                        '', // type
//                                        '', // link
//                                        '', // align
//                                        false, // resize
//                                        300, // dpi
//                                        '', // palign
//                                        false, // ismask
//                                        false, // imgmask
//                                        0, // border
//                                        'CM', // fitbox
//                                        false, // hidden
//                                        false, // fitonpage
//                                        false, // alt
//                                        array() // altimgs
//                                    );
//                                    $label = $pdf->Output('labels.pdf', 'S');
//
//                                    $xml->event[$i]->shipment->label->base64 = base64_encode($label);
                                    break;
                                }

                        }
                    }


                } else {


                    $errorDetails = print_r($result->error->desc, 1).' : '. print_r($result->error->details, 1);
                    $errorDetails = str_ireplace(['stdClass Object', 'Array'], '', $errorDetails);

                    $xml->event[$i]->errors->error->priority = 'critical';
                    $xml->event[$i]->errors->error->code = $result->error->error_code;
                    $xml->event[$i]->errors->error->short_description = $errorDetails;
                    $xml->event[$i]->errors->error->note = '';

                }



            }
        }

//        MyDump::dump('iai-xml.txt', preg_replace('%\[base64\](.*)\)%si', '(...TRUNCATED LABEL BASE64...)',  print_r($xml,1)));



        if($xmlProcessed) {
            $isf = $xml->saveXML();

            $user = $request['user'];
            $isfHash = hash('sha256', $isf);
            $time = time();
            $requestHash = hash('sha256', $user . $isfHash . $api_key . $time);
            $contents = array(
                'user' => $user,
                'isf' => $isf,
                'time' => $time,
                'hash' => $requestHash
            );

            $response = json_encode($contents);

            MyDump::dump('iai.txt', $logID.':'.preg_replace('%<base64>(.*)<\\\/base64>%i', '<base64>(...TRUNCATED LABEL BASE64...)<\/base64>',  print_r($response,1)));

            echo $response;
//            MyDump::dump('iai.txt', $logID.': FINISHED');

            if(sizeof($packagesIds)) {
                Yii::app()->getModule('courier');
                Yii::app()->db->createCommand();
                $cmd->update('courier', ['source' => Courier::SOURCE_IAI], 'local_id IN (' . implode(',', $packagesIds) . ')');
            }
        }
        else
            echo '-';


        Yii::app()->end();
    }


    public static function actionOrdersPostal($type, $sp_point = false)
    {

        $request = file_get_contents('php://input');
        MyDump::dump('iai-request.txt', print_r($request,1));

        $request = json_decode($request, true);

        $authorized = false;
        $xmlProcessed = false;

        $user = $request['user'];
        $hash = $request['hash'];
        $isf = $request['isf'];
        $time = $request['time'];

        MyDump::dump('iai-request.txt', print_r($request,1));

        $cmd = Yii::app()->db->createCommand();
        $api_key = $cmd->select('apikey')
            ->from((new User)->tableName())
            ->where('login = :login AND apikey IS NOT NULL', [
                ':login' => $user,
            ])
            ->queryScalar();


        if($api_key) {

            $isfHash = hash('sha256', $isf);
            $checkHash = hash('sha256', $user . $isfHash . $api_key . $time);


            if($hash == $checkHash)
                $authorized = true;
        }

        $xml = simplexml_load_string($isf);

        /* @var $user User */
        $user = User::model()->findByAttributes(['apikey' => $api_key]);

        for($i = 0; $i < sizeof($xml->event); $i++) {
            $xmlProcessed = true;

            $cod = floatval($xml->event[$i]->shipment->pickup->cod->amount);
//            $cod_account = (string) $xml->event[$i]->shipment->pickup->cod->transfer_account_id; // Looks like IAI is not providing account
            $cod_account = '';
            if($cod) {

                if ($user)
                    $cod_account = $user->getBankAccount();
            }

            $value1 = floatval($xml->event[$i]->shipment->additional_services->insurance);
            $value2 = floatval($xml->event[$i]->shipment->additional_services->dutiable);

            $value = $value1 > $value2 ? $value1 : $value2;


            $weight = floatval($xml->event[$i]->shipment->package->description->weight);
            if($weight < 0.001)
                $weight = 0.001;

            $package = [];

            $sender = [];
            $sender['name'] = (string) $xml->event[$i]->sender->address->name;
            $sender['company'] = (string) $xml->event[$i]->sender->address->company;
            $sender['address_line_1'] = (string) $xml->event[$i]->sender->address->address;
            $sender['address_line_2'] = '';
            $sender['country'] = (string) $xml->event[$i]->sender->address->country;
            $sender['zip_code'] = (string) $xml->event[$i]->sender->address->postcode;
            $sender['city'] = (string) $xml->event[$i]->sender->address->city;
            $sender['tel'] = (string) $xml->event[$i]->sender->address->telephone;
            $sender['email'] = (string) $xml->event[$i]->sender->notification->email[1]['address'];

            $receiver = [];
            $receiver['name'] = (string) $xml->event[$i]->receiver->address->name;
            $receiver['company'] = (string) $xml->event[$i]->receiver->address->company;
            $receiver['address_line_1'] = (string) $xml->event[$i]->receiver->address->address;
            $receiver['address_line_2'] = '';
            $receiver['country'] = (string) $xml->event[$i]->receiver->address->country;
            $receiver['zip_code'] = (string) $xml->event[$i]->receiver->address->postcode;
            $receiver['city'] = (string) $xml->event[$i]->receiver->address->city;
            $receiver['tel'] = (string) $xml->event[$i]->receiver->contact->telephone;
            $receiver['email'] = (string) $xml->event[$i]->receiver->notification->email[1]['address'];

            if($receiver['tel'] == '' && $user->getDefaultTelNo())
                $receiver['tel'] = $user->getDefaultTelNo();


            $size_l = (int) $xml->event[$i]->shipment->package->description->dimensions->length;
            $size_w = (int) $xml->event[$i]->shipment->package->description->dimensions->width;
            $size_d = (int) $xml->event[$i]->shipment->package->description->dimensions->height;


            $dimensions = [
                $size_l,
                $size_w,
                $size_d,
            ];

            // biggest dimension is length and is first
            rsort($dimensions);

            $size_class = false;
            if(strtoupper($sender['country']) != strtoupper($receiver['country']))
            {
                if($dimensions[0] <= 24.5 && $dimensions[1] <= 16.5 && $dimensions[2] <= 0.5)
                    $size_class = 100;
                if($dimensions[0] <= 38.1 && $dimensions[1] <= 30.5 && $dimensions[2] <= 2)
                    $size_class = 101;
                else if($dimensions[0] < 60 && array_sum($dimensions) < 90)
                    $size_class = 102;
            } else {
                if($dimensions[0] <= 32.5 && $dimensions[1] <= 23 && $dimensions[2] <= 2)
                    $size_class = 1;
                else if($dimensions[0] <= 60 && array_sum($dimensions) <= 90)
                    $size_class = 2;

            }

            $weight_class = false;
            if($weight <= 20)
                $weight_class = 7;
            else if($weight <= 50)
                $weight_class = 1;
            else if($weight <= 100)
                $weight_class = 2;
            else if($weight <= 350)
                $weight_class = 3;
            else if($weight <= 500)
                $weight_class = 4;
            else if($weight <= 1000)
                $weight_class = 5;
            else if($weight <= 2000)
                $weight_class = 6;




            if(!$sp_point)
                $sp_point = SpPoints::CENTRAL_SP_POINT_ID;

            $package['weight_class'] = $weight_class;
            $package['size_class'] = $size_class;
            $package['type'] = $type;
            $package['sp_point'] = $sp_point;
            $package['content'] = (string) $xml->event[$i]->shipment->package->description->content_description;

            $data = [
                'package' => $package,
                'sender' => $sender,
                'receiver' => $receiver,
            ];


            if(!$authorized)
            {
                $xml->event[$i]->errors->error->priority = 'critical';
                $xml->event[$i]->errors->error->code = 'NOT_AUTHORIZED';
                $xml->event[$i]->errors->error->short_description = 'You have provided invalid credentials or your API access is not activated by administrator!';
                $xml->event[$i]->errors->error->note = '';

            } else {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

                curl_setopt($ch, CURLOPT_URL, 'https://api.swiatprzesylek.pl/V1/postal/create-single');
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $api_key);
//                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $result = curl_exec($ch);
                $result = json_decode($result);

                $packagesIds = [];
                if ($result->result === "OK") {

                    foreach ($result->response->postals AS $package) {
                        $packagesIds[] = "'".$package->package_id."'";

                        $package_id = $package->external_id ? $package->external_id : $package->postal_id;

                        $xml->event[$i]->shipment->tracking_number = $package_id;
                        $xml->event[$i]->shipment->tracking_url = 'https://swiatprzesylek.pl/tt/' . $package_id;
                        $xml->event[$i]->shipment->package->tracking_number = $package_id;

                        $xml->event[$i]->shipment->cost->net = '99';
                        $xml->event[$i]->shipment->cost->vat_percent = '23';
                        $xml->event[$i]->shipment->cost->gross = '121.77';

                        if ($package->result === "OK") {
                            $labels_no = $package->labels_no;

                            if ($labels_no)
                                foreach ($package->labels AS $label) {
                                    $image = base64_decode($label);

                                    $pdf = PDFGenerator::initialize();
                                    $pageW = 100;
                                    $pageH = 150;

                                    $xBase = 2;
                                    $yBase = 2;

                                    $blockWidth = $pageW - (2 * $xBase);
                                    $blockHeight = $pageH - (2 * $yBase);

                                    $pdf->AddPage('H', array(100, 150), true);
                                    // PRINT IMAGE TO PDF
                                    $pdf->Image(
                                        '@' . $image, // file
                                        $xBase, // x
                                        $yBase, // y
                                        $blockWidth, // w
                                        $blockHeight, // h
                                        '', // type
                                        '', // link
                                        '', // align
                                        false, // resize
                                        300, // dpi
                                        '', // palign
                                        false, // ismask
                                        false, // imgmask
                                        0, // border
                                        'CM', // fitbox
                                        false, // hidden
                                        false, // fitonpage
                                        false, // alt
                                        array() // altimgs
                                    );
                                    $label = $pdf->Output('labels.pdf', 'S');

                                    $xml->event[$i]->shipment->label->base64 = base64_encode($label);
                                    break;
                                }

                        }
                    }


                } else {


                    $errorDetails = print_r($result->error->desc, 1).' : '. print_r($result->error->details, 1);
                    $errorDetails = str_ireplace(['stdClass Object', 'Array'], '', $errorDetails);

                    $xml->event[$i]->errors->error->priority = 'critical';
                    $xml->event[$i]->errors->error->code = $result->error->error_code;
                    $xml->event[$i]->errors->error->short_description = $errorDetails;
                    $xml->event[$i]->errors->error->note = '';

                }



            }
        }

        MyDump::dump('iai-xml.txt', preg_replace('%\[base64\](.*)\)%si', '(...TRUNCATED LABEL BASE64...)',  print_r($xml,1)));


        if($xmlProcessed) {
            $isf = $xml->saveXML();

            $user = $request['user'];
            $isfHash = hash('sha256', $isf);
            $time = time();
            $requestHash = hash('sha256', $user . $isfHash . $api_key . $time);
            $contents = array(
                'user' => $user,
                'isf' => $isf,
                'time' => $time,
                'hash' => $requestHash
            );

            $response = json_encode($contents);
            MyDump::dump('iai-xml.txt', print_r($response,1));

            echo $response;

            if(sizeof($packagesIds)) {
                Yii::app()->getModule('postal');
                Yii::app()->db->createCommand();
                $cmd->update('postal', ['source' => Postal::SOURCE_IAI], 'local_id IN (' . implode(',', $packagesIds) . ')');
            }
        }
        else
            echo '-';


        Yii::app()->end();
    }


}
