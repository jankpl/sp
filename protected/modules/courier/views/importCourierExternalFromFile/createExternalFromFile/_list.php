    <?php $modelTemp = new Courier;?>
    <?php $modelTemp2 = new AddressData();?>
    <?php $modelTemp3 = new CourierTypeExternal();?>

    <?php //echo CHtml::hiddenField('fileTempName', $fileModel->file->tempName);?>
    <div style="width: 100%; position: relative; height: 500px; overflow-x: scroll;">
        <div style="width: 3000px; position: absolute;">

            <table class="list hTop" style="font-size: 10px; text-align: left !important;">
                <tr>
                    <td colspan="12"><?php echo Yii::t('courier','Packages');?></td>
                    <td colspan="8"><?php echo Yii::t('courier','Sender');?></td>
                    <td colspan="8"><?php echo Yii::t('courier','Receiver');?></td>
                </tr>

                <tr>
                    <td>
                        <?php echo $modelTemp->getAttributeLabel('package_weight'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_size_l'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_size_w'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_size_d'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('packages_number'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_value'); ?>
                    </td><td>
                        <?php echo $modelTemp->getAttributeLabel('package_content'); ?>
                    </td><td style="border-left: 2px dashed gray;">
                    <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country_id'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td style="border-left: 2px dashed gray;">
                    <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country_id'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td>
                </tr>

                <?php

                foreach($models AS $key => $item):
                    $this->renderPartial('createExternalFromFile/_item',
                        array(
                            'split' => $split,
                            'model' => $item,
                            'i' => $key,
                        ));

                endforeach;
                ?>
            </table>

        </div>
    </div>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <div class="navigate" style="text-align: center;">

        <?php echo TbHtml::button(Yii::t('courier', 'Validate this part of data'), array('name' => 'validate', 'class' => 'save-packages', 'data-mode' => Courier::MODE_JUST_VALIDATE)); ?>

        <?php echo TbHtml::button(Yii::t('courier', 'Save this part of data'), array('name' => 'save', 'class' => 'save-packages', 'data-mode' => Courier::MODE_SAVE)); ?>

        <?php echo CHtml::hiddenField('mode','', array('id' => 'mode'));?>
    </div>
</div><!-- form -->