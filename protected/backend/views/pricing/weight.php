<?php
/* @var $models Pricing */
/* @var $id Ineger */
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div class="view">


    <div style="width: 500px;">
        <?php
        $modelGV = new Pricing();

        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'pricing-grid-'.$id,
            'dataProvider' => $modelGV->gridViewData($id),
            'columns' => array(
                array(
                    'name' => 'top_weight',
                    'value' => '$data->top_weight." g"',
                ),
                array(
                    'name' => 'price',
                    'value' => 'S_Price::formatPrice($data->price)." zł"',
                ),
            ),
        ));

        ?>

    </div>


    <table class="list hLeft" style="width: 500px;">
        <tr>
            <td>Action</td>
            <td><?php echo CHtml::link('edit',array('pricing/update', 'id' => $id), array('class' => 'likeButton')); ?></td>
        </tr>
    </table>

</div>


