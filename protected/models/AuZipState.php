<?php

class AuZipState
{
    public static function getStateByZip($zip)
    {
        $zip = preg_replace("/[^0-9]/","", $zip);
        $zip = trim($zip);

        switch($zip)
        {
            case $zip >= 1000 && $zip <= 1999:
            case $zip >= 2000 && $zip <= 2599:
            case $zip >= 2619 && $zip <= 2899:
            case $zip >= 2921 && $zip <= 2999:
                return 'NSW';
                break;
            case $zip >= 200 && $zip <= 299:
            case $zip >= 2600 && $zip <= 2618:
            case $zip >= 2900 && $zip <= 2920:
                return 'ACT';
                break;
            case $zip >= 3000 && $zip <= 3999:
            case $zip >= 8000 && $zip <= 8999:
                return 'VIC';
                break;
            case $zip >= 4000 && $zip <= 4999:
            case $zip >= 9000 && $zip <= 9999:
                return 'QLD';
                break;
            case $zip >= 5000 && $zip <= 5799:
            case $zip >= 5800 && $zip <= 5999:
                return 'SA';
                break;
            case $zip >= 6000 && $zip <= 6797:
            case $zip >= 6800 && $zip <= 6999:
                return 'WA';
                break;
            case $zip >= 7000 && $zip <= 7799:
            case $zip >= 7800 && $zip <= 7999:
                return 'TAS';
                break;
            case $zip >= 800 && $zip <= 899:
            case $zip >= 900 && $zip <= 999:
                return 'NT';
                break;
        }
    }


}