<?php

Yii::import('application.modules.courier.models._base.BaseCourierTypeU');

class CourierTypeU extends BaseCourierTypeU
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    const SCENARIO_STEP1 = 'step1';
    const SCENARIO_IMPORT = 'import';
    const SCENARIO_API = 'api';

    const SOURCE_EBAY = 1;
    const SOURCE_LINKER = 2;

    const NUMBER_OF_MULTIPLE_PACKAGES_ONCE = 25;

    const MAX_PACKAGE_WEIGHT = 1000;

    public $_package_wrapping;

    public $_additions;

    public $_imported_additions = [];
    public $_imported_additions_ok = [];
    public $_imported_additions_fail = [];

    public $_courier_additions = [];
    public $regulations;
    public $regulations_rodo;

// used in import
    public $_price_total;
    public $_price_currency;

    public $_selected_operator_id;


    public $__temp_ebay_update_stat;

    public function getPackageWrappingName()
    {
        return isset(User::getWrappingList()[$this->_package_wrapping]) ? User::getWrappingList()[$this->_package_wrapping] : '';
    }

    public function init()
    {
        if(Yii::app()->params['frontend'] && !Yii::app()->user->isGuest)
        {
            $data = User::getDefaultPackageDimensionsForUser(Yii::app()->user->id);
            $this->_package_wrapping = $data->package_wrapping;
        }

        parent::init();
    }

    public function afterValidate()
    {
        if($this->_package_wrapping != User::WRAPPING_DEFAULT && $this->_package_wrapping != '' && !in_array($this->courierUOperator->uniq_id, CourierUOperator::getCustomTypeOperators()))
        {
            if(!in_array(CourierUAdditionList::ADDITION_NON_STANDARD_PACKAGE, $this->_courier_additions))
            {
                $this->addError('_package_wrapping', Yii::t('courier', 'Niestandardowe opakowanie wymaga aktywnej opcji dodatkowej "Przesyłka niestandardowa"'));
            }
        }


        if($this->collection)
        {
            if(!$this->courierUOperator->isCollectable())
                $this->addError('collection', Yii::t('courier', 'Ta opcja jest niedostępna'));

        }

        parent::afterValidate();
    }

    public function calculateDimensionalWeight($dimWeightMode)
    {

        if(!is_numeric($this->courier->package_size_l) OR !is_numeric($this->courier->package_size_d) OR !is_numeric($this->courier->package_size_w))
            return NULL;

        if($dimWeightMode == CourierUOperator::DIM_WEIGHT_MODE_DHL)
        {
            $dw = ($this->courier->package_size_l * $this->courier->package_size_d * $this->courier->package_size_w);
            $dw /= 4000;
            $dw = round($dw,1);

            if($dw > 31.5)
                $dw = 31.5;

        } else {

            $dw = ($this->courier->package_size_l * $this->courier->package_size_d * $this->courier->package_size_w);
            $dw /= 5000;

            $x = round($dw);
            $y = ceil($dw);

            // round up to 0.5
            if ($y - $dw > 0.5 && $y - $dw < 1)
                $dw = $x + 0.5;
            else if ($y - $dw != 0.5) {
                $dw = $y;
            }

        }
        return $dw;
    }

    public function getFinalWeight($dimWeightMode = -10)
    {
        if($dimWeightMode === -10)
            $dimWeightMode = $this->courierUOperator->getDimWeightMode();

        if($dimWeightMode) {
            $dw = $this->calculateDimensionalWeight($dimWeightMode);

            if ($this->courier->package_weight > $dw)
                return $this->courier->package_weight;
            else
                return $dw;
        }
        else
            return $this->courier->package_weight;
    }

    public static function getCurrencyIdForCurrentUser()
    {
        if(Yii::app()->user->isGuest)
            return false;

        return Yii::app()->user->getModel()->getCourierUCurrencyId() ? Yii::app()->user->getModel()->getCourierUCurrencyId() : Yii::app()->PriceManager->getCurrency();
    }

    public static function getCurrencySymbolForCurrentUser()
    {
        if(Yii::app()->user->isGuest)
            return false;

        $model = PriceCurrency::model()->findByPk(self::getCurrencyIdForCurrentUser());
        return $model->short_name;
    }

    public function rules() {
        return array_merge(RodoRegulations::regulationsRules(), array(
//            array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('courier', 'Musisz zaakceptować regulamin.'), 'on' => 'summary'),
//            array('regulations', 'numerical', 'integerOnly' => true),

            array('courier_id', 'required', 'except' => [self::SCENARIO_STEP1, self::SCENARIO_API, self::SCENARIO_IMPORT]),
            array('courier_u_operator_id', 'required', 'except' => self::SCENARIO_API),
            array('_package_wrapping', 'in', 'range' => array_keys(User::getWrappingList()), 'allowEmpty' => true),
            array('courier_id, courier_u_operator_id, family_member_number, parent_courier_id, courier_label_new_id', 'numerical', 'integerOnly'=>true),
            array('_selected_operator_id', 'numerical', 'integerOnly'=>true),
            array('collection', 'boolean'),
            array('family_member_number, parent_courier_id, courier_label_new_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, courier_id, courier_u_operator_id, family_member_number, parent_courier_id, courier_label_new_id', 'safe', 'on'=>'search'),
        ));
    }

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bAcceptRegulationsByDefault' =>
                    [
                        'class'=>'application.models.bAcceptRegulationsByDefault'
                    ],
            ]
        );
    }

    /**
     * @param $receiver_country_id int If is false, return operators for all countries and returns list of countries with prices
     * @param null $user_group_id
     * @param bool $withOperatorModels
     * @return array
     */
    public static function findAvailableOperators($receiver_country_id, $size_l, $size_d, $size_w, $user_group_id = NULL, $withOperatorModels = true)
    {

        if($user_group_id === NULL && !Yii::app()->user->isGuest)
            $user_group_id = Yii::app()->user->getModel()->getUserGroup();

        $CACHE_NAME = 'COURIER_U_AVAILABLE_OPERATORS_'.$receiver_country_id.'_'.$user_group_id.'_'.$withOperatorModels;

        $response = Yii::app()->cache->get($CACHE_NAME);

        if($response === false OR GLOBAL_CONFIG::COURIER_INTERNAL_DISABLE_ROUTING_CACHE) {

            if ($receiver_country_id)
                $receiver_country_group_id = CourierCountryGroup::getGroupForCountryId($receiver_country_id);

            $currency = CourierTypeU::getCurrencyIdForCurrentUser();

//        $cmd = Yii::app()->db->createCommand();
//        $cmd->select('cuo2cg.id AS operator_id_2_cg, cuo2cg.courier_u_operator_id AS operator_id, cuo2cg.stat, cuco4ug.stat AS ug_stat, cupg.id AS cupg_id, cupg_ug.id AS cupg_ug_id, cupg.revision AS cupg_revision, cupg_ug.revision AS cupg_ug_revision, cuo.name_customer AS operator_name, cuo.img AS operator_img, cuo2cg.cod AS cod, cuo2cg.delivery_hours AS delivery_hours, cuo.has_dim_weight, cuotr.text, cuo.broker_id AS broker_id, cod_price.value AS cod_price');
//        $cmd->from('courier_u_operator_2_country_group cuo2cg');
//        $cmd->leftJoin('courier_u_custom_operators_4_user_group cuco4ug', 'cuco4ug.courier_u_operator_2_country_group_id = 	cuo2cg.id AND cuco4ug.user_group_id = :ugi', [':ugi' => $user_group_id]);
//        $cmd->leftJoin('courier_u_price_group cupg', 'cupg.courier_u_2_operator_country_group_id = cuo2cg.id AND cupg.user_group_id IS NULL', [':ugi' => $user_group_id]);
//        $cmd->leftJoin('courier_u_price_group cupg_ug', 'cupg_ug.courier_u_2_operator_country_group_id = cuo2cg.id AND cupg_ug.user_group_id = :ugi2', [':ugi2' => $user_group_id]);
//        $cmd->leftJoin('courier_u_operator cuo', 'cuo.id = cuo2cg.courier_u_operator_id');
//        $cmd->leftJoin('courier_u_operator_tr cuotr', 'cuo.id = cuotr.courier_u_operator_id AND language_id = :language_id', [':language_id' => Yii::app()->params['language']]);
//        $cmd->leftJoin('price_value cod_price', 'cod_price.price_id = cuo2cg.cod_price_id AND cod_price.currency_id = :currency', [':currency' => $currency]);
//
//        if($receiver_country_id)
//            $cmd->where('courier_country_group_id = :courier_country_group_id AND cuo.stat = :stat', [':courier_country_group_id' => $receiver_country_group_id, ':stat' => CourierUOperator::STAT_ACTIVE]);
//        else
//            $cmd->where('cuo.stat = :stat', [':stat' => CourierUOperator::STAT_ACTIVE]);


            $cmd = Yii::app()->db->createCommand();

            $select = 'cuo2cg.id AS operator_id_2_cg, cuo2cg.courier_u_operator_id AS operator_id, cuo2cg.stat, cuco4ug.stat AS ug_stat, cupg.id AS cupg_id, cupg_ug.id AS cupg_ug_id, cupg.revision AS cupg_revision, cupg_ug.revision AS cupg_ug_revision, cuo.name_customer AS operator_name, cuo.img AS operator_img, cuo2cg.cod AS cod, cuo2cg.delivery_hours AS delivery_hours, cuo.has_dim_weight, cuotr.text, cuo.broker_id AS broker_id, cod_price.value AS cod_price';

            if (!$receiver_country_id)
                $select .= ', ccghc.country AS country_list_id';

            $cmd->select($select);
            $cmd->from('courier_u_operator_2_country_group cuo2cg');
            $cmd->leftJoin('courier_u_custom_operators_4_user_group cuco4ug', 'cuco4ug.courier_u_operator_2_country_group_id = cuo2cg.id AND cuco4ug.user_group_id = :ugi', [':ugi' => $user_group_id]);
            $cmd->leftJoin('courier_u_price_group cupg', 'cupg.courier_u_2_operator_country_group_id = cuo2cg.id AND cupg.user_group_id IS NULL');
            $cmd->leftJoin('courier_u_price_group cupg_ug', 'cupg_ug.courier_u_2_operator_country_group_id = cuo2cg.id AND cupg_ug.user_group_id = :ugi2', [':ugi2' => $user_group_id]);
            $cmd->leftJoin('courier_u_operator cuo', 'cuo.id = cuo2cg.courier_u_operator_id');
            $cmd->leftJoin('courier_u_operator_tr cuotr', 'cuo.id = cuotr.courier_u_operator_id AND language_id = :language_id', [':language_id' => Yii::app()->params['language']]);
            $cmd->leftJoin('price_value cod_price', 'cod_price.price_id = cuo2cg.cod_price_id AND cod_price.currency_id = :currency', [':currency' => $currency]);


            if (!$receiver_country_id)
                $cmd->leftJoin('courier_country_group_has_country ccghc', 'ccghc.courier_group = cuo2cg.courier_country_group_id');

            if ($receiver_country_id)
                $cmd->where('courier_country_group_id = :courier_country_group_id AND cuo.stat = :stat', [':courier_country_group_id' => $receiver_country_group_id, ':stat' => CourierUOperator::STAT_ACTIVE]);
            else
                $cmd->where('cuo.stat = :stat AND (cupg_ug.revision IS NOT NULL OR cupg.revision IS NOT NULL)', [':stat' => CourierUOperator::STAT_ACTIVE]);


            $result = $cmd->queryAll();

            $response = [];

            foreach ($result AS $item) {
                if (
                    (!$item['stat'] && ($item['ug_stat'] === NULL OR !$item['ug_stat']))
                    OR
                    (!$item['ug_stat'] && $item['ug_stat'] !== NULL)
                )
                    continue;


                if ($item['has_dim_weight'])
                    $item['dimWeightMode'] = CourierUOperator::courierBrokerId2DimWeightMode($item['broker_id']);


                $country_list_ids_temp = isset($response[$item['operator_id']]['country_list_ids']) ? $response[$item['operator_id']]['country_list_ids'] : [];

                $response[$item['operator_id']] = $item;

                $country_list_ids_temp[] = $item['country_list_id'];
                $response[$item['operator_id']]['country_list_ids'] = $country_list_ids_temp;
            }

            Yii::app()->cache->set($CACHE_NAME, $response, 60*5);
        }

        if ($withOperatorModels) {
            $operatorModels = CourierUOperator::model()->findAllByAttributes(['id' => array_keys($response)]);

            foreach ($operatorModels AS $model)
                $response[$model->id]['operatorModel'] = $model;
        }




        return $response;
    }

//    public static function getCountryListForOperator($operator_id, $user_group_id = NULL)
//    {
//
//        if($user_group_id === NULL && !Yii::app()->user->isGuest)
//            $user_group_id = Yii::app()->user->getModel()->getUserGroup();
//
//        $currency = CourierTypeU::getCurrencyIdForCurrentUser();
//
//        $cmd = Yii::app()->db->createCommand();
//        $cmd->select('ccghc.country');
//        $cmd->distinct = true;
//        $cmd->from('courier_u_operator_2_country_group cuo2cg');
//        $cmd->leftJoin('courier_u_custom_operators_4_user_group cuco4ug', 'cuco4ug.courier_u_operator_2_country_group_id = 	cuo2cg.id AND cuco4ug.user_group_id = :ugi', [':ugi' => $user_group_id]);
//        $cmd->leftJoin('courier_u_price_group cupg', 'cupg.courier_u_2_operator_country_group_id = cuo2cg.id AND cupg.user_group_id IS NULL', [':ugi' => $user_group_id]);
//        $cmd->leftJoin('courier_u_price_group cupg_ug', 'cupg_ug.courier_u_2_operator_country_group_id = cuo2cg.id AND cupg_ug.user_group_id = :ugi2', [':ugi2' => $user_group_id]);
//        $cmd->leftJoin('courier_u_operator cuo', 'cuo.id = cuo2cg.courier_u_operator_id');
//        $cmd->leftJoin('courier_u_operator_tr cuotr', 'cuo.id = cuotr.courier_u_operator_id AND language_id = :language_id', [':language_id' => Yii::app()->params['language']]);
//        $cmd->leftJoin('price_value cod_price', 'cod_price.price_id = cuo2cg.cod_price_id AND cod_price.currency_id = :currency', [':currency' => $currency]);
//        $cmd->leftJoin('courier_country_group_has_country ccghc', 'ccghc.courier_group = cuo2cg.courier_country_group_id');
//
//        $cmd->where('cuo.stat = :stat AND cuo.id = :courier_operator_id AND (cupg_ug.revision IS NOT NULL OR cupg.revision IS NOT NULL)', [':courier_operator_id' => $operator_id, ':stat' => CourierUOperator::STAT_ACTIVE]);
//
//        $result = $cmd->queryColumn();
//
//        return $result;
//
//    }

    public static function getPricesForRoute(CourierTypeU $courierTypeU, $receiver_country_id, $operator_id, $user_group_id = NULL)
    {
        $user_group_id = $user_group_id == NULL ? Yii::app()->user->getUserGroupId() : $user_group_id;

        $routing = self::findAvailableOperators($receiver_country_id, $courierTypeU->courier->package_size_l, $courierTypeU->courier->package_size_d, $courierTypeU->courier->package_size_w, $user_group_id);

        if(!isset($routing[$operator_id]))
            return false;

        $route = $routing[$operator_id];

        if($route['ug_stat'] === false OR ($route['ug_stat'] === NULL && $route['stat'] === false))
            return false;

        $weight = $courierTypeU->getFinalWeight($route['dimWeightMode']);

        if($route['cupg_ug_id'])
        {
            $cupg_id = $route['cupg_ug_id'];
            $cupg_revision = $route['cupg_ug_revision'];
        } else {
            $cupg_id = $route['cupg_id'];
            $cupg_revision = $route['cupg_revision'];
        }

        $currency = CourierTypeU::getCurrencyIdForCurrentUser();

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('pricePerItem.value AS price_per_item');
        $cmd->from('courier_u_price_item');
        $cmd->join('courier_u_price_group', 'courier_u_price_item.courier_u_price_group_id = courier_u_price_group.id');
        $cmd->join('price_value AS pricePerItem', 'price_per_item = pricePerItem.price_id AND pricePerItem.currency_id = :currency_id', array(':currency_id' => $currency));
        $cmd->where('weight_top_limit >= :weight', array(':weight' => $weight));
        $cmd->andWhere('courier_u_price_item.courier_u_price_group_id = :cupg_id', array(':cupg_id' => $cupg_id));
        $cmd->andWhere('courier_u_price_item.revision = :cupg_revision', array(':cupg_revision' => $cupg_revision));
        $cmd->order('weight_top_limit ASC');
        $cmd->limit('1');
        $result = $cmd->queryScalar();


        // @12.04.1029
        // Special rule to ignore no pricings for CQL
        if(!$result && in_array($user_group_id, [13]))
            return 1;

        return $result;
    }

    public function priceAdditions()
    {
        $additions = $this-> _courier_additions;

        if(!is_array($additions))
            return 0;

        $price = 0;
        foreach($additions AS $item)
        {
            /* @var $model CourierAdditionList */
            $model = CourierUAdditionList::model()->cache(60)->findByPk($item);
            if($model === null)
                continue;

            $price += $model->getPrice();
        }

        return $price;
    }

    public function priceRemoteArea()
    {

        $result = $this->courierUOperator->getRemoteAreaPrice($this->courier->receiverAddressData->zip_code, $this->courier->receiverAddressData->country_id, $this->courier->receiverAddressData->city);


        return $result;
    }

    public function priceRemoteAreaSender()
    {
        $result = 0;
        if($this->collection)
        {
            $result = $this->courierUOperator->getRemoteAreaPrice($this->courier->senderAddressData->zip_code, $this->courier->senderAddressData->country_id, $this->courier->senderAddressData->city);
        }

        return $result;
    }


    public function priceCod()
    {
        if($this->courier->cod_value)
        {
            if($this->courierUOperator->getCodPriceForCountryGroup($this->courier->receiverAddressData->country_id))
                return $this->courierUOperator->getCodPriceForCountryGroup($this->courier->receiverAddressData->country_id)->getValue(CourierTypeU::getCurrencyIdForCurrentUser())->value;
        }

        return 0;
    }

    public function pricePackage()
    {
        return self::getPricesForRoute($this,$this->courier->receiverAddressData->country_id, $this->courier_u_operator_id, Yii::app()->user->getUserGroupId());
    }

    public function priceEach()
    {
        $pricing = $this->pricePackage();
        $pricing += $this->priceAdditions();
        $pricing += $this->priceRemoteArea();
        $pricing += $this->priceRemoteAreaSender();
        $pricing += $this->priceCod();

        return $pricing;
    }

    public function priceTotal()
    {
        return $this->courier->packages_number * $this->priceEach();
    }


    public function addAdditions($additions)
    {

        // find if additons IDs are poroper and remove multiple additions from one group
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, group');
        $cmd->from((new CourierUAdditionList())->tableName());
        $cmd->where('stat = :stat', array(':stat' => S_Status::ACTIVE));
        $cmd->andWhere(array('in', 'id', $additions));
        $result = $cmd->queryAll();

        $temp = [];
        $temp2 = []; // for non grouped - not to by filtered by array_unique
        foreach($result AS $key => $item)
        {
            if($item['group'] === NULL)
                $temp2[$item['id']] = $item['group'];
            else
                $temp[$item['id']] = $item['group'];
        }

        // filter grouped additions - only one in group is allowed
        $temp = array_unique($temp);

        $listOfAdditons = \CHtml::listData(CourierUAdditionList::getAdditionsByCourier($this->courier), 'id', 'id');
        $additions = [];
        if(is_array($temp))
            foreach($temp AS $key => $item)
            {
                if(in_array($key, $listOfAdditons))
                    array_push($additions, $key);
            }

        if(is_array($temp2))
            foreach($temp2 AS $key => $item)
            {
                if(in_array($key, $listOfAdditons))
                    array_push($additions, $key);
            }

        $this->_courier_additions = $additions;
    }

    /**
     * @param bool $forceNotSaved Ignore that model has already ID and try to get additions by array of additions ids
     * @return array|mixed|null|static
     */
    public function listAdditions($forceNotSaved = false)
    {
        if($this->id == NULL OR $forceNotSaved)
            $additions = CourierUAdditionList::model()->findAllByAttributes(array('id' => $this->_courier_additions));
        else
            $additions = CourierUAddition::model()->findAllByAttributes(array('courier_id' => $this->courier->id));

        return $additions;
    }


    /**
     * Checks if package has particular addition
     * @param $uniq_id Integer uniq_id of CourierAdditionList
     * @return boolean
     */
    public function hasAdditionByUniqId($uniq_id)
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('COUNT(*)');
        $cmd->from = (new CourierUAddition())->tableName();
        $cmd->join((new CourierUAdditionList())->tableName(),'courier_u_addition_list.id = courier_u_addition.courier_u_addition_list_id');
        $cmd->where('courier_id = :courier_id', [':courier_id' => $this->courier->id]);
        $cmd->andWhere('uniq_id = :uniq_id', [':uniq_id' => $uniq_id]);
        return $cmd->queryScalar();
    }



    public function saveNewPackage()
    {
        if($this->courier->packages_number < 1)
            $this->courier->packages_number = 1;

        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        $courierPackagesFamily = [];
        $courierParentId = NULL;
        $parentLabelId = NULL;
        for ($i = 0; $i < $this->courier->packages_number; $i++) {

            if ($errors)
                continue;

            /* @var $courierTypeU CourierTypeU */

            $courierTypeU = S_Useful::cloneOrmModels((new CourierTypeU()), $this);
            $courierTypeU->_courier_additions = $this->_courier_additions;
            $courierTypeU->_package_wrapping = $this->_package_wrapping;
            $courierTypeU->courier = clone $this->courier;

            $this->courier->courierTypeU = $courierTypeU;

            $courierTypeU->courier->senderAddressData = S_Useful::cloneOrmModels((new CourierTypeU_AddressData()), $this->courier->senderAddressData);
            $courierTypeU->courier->receiverAddressData = S_Useful::cloneOrmModels((new CourierTypeU_AddressData()), $this->courier->receiverAddressData);

            $courierTypeU->parent_courier_id = $courierParentId;
            $courierTypeU->family_member_number = $i + 1;
            $courierTypeU->courier->courierTypeU = $courierTypeU;

            if(!$courierTypeU->_saveNewPackage())
                $errors = true;

            $courierTypeU->courier->changeStat(CourierStat::PACKAGE_PROCESSING);


            $label = $courierTypeU->finalizeDetails($parentLabelId);

            if(!$i) {
                $courierParentId = $courierTypeU->courier->id;
                $parentLabelId = $label->id;
            }

            array_push($courierPackagesFamily, $courierTypeU->courier);
        }

        if(!$errors) {
            $orderProducts = [];
            foreach ($courierPackagesFamily As $package) {
                $product = OrderProduct::createNewProduct(
                    OrderProduct::TYPE_COURIER,
                    $package->id,
                    CourierTypeU::getCurrencySymbolForCurrentUser(),
                    $this->priceEach(),
                    Yii::app()->PriceManager->getFinalPrice($this->priceEach())
                );

                $orderProducts[] = $product;
            }

            if (!S_Useful::sizeof($orderProducts))
                $errors = true;

            if(!$errors)
            {

                $order_id = Order::createReadyOrderOnInvoice($orderProducts);

                if($order_id)
                {

                    foreach($courierPackagesFamily AS $familyItem) {
                        $familyItem->courierTypeU->order_id = $order_id;
                        $familyItem->courierTypeU->update(['order_id']);
                    }
                } else {
                    $errors = true;
                }
            }
        }

        if($errors)
        {
            $transaction->rollback();
            return false;
        } else {
            $transaction->commit();
            return $courierPackagesFamily;
        }

    }


    public static function saveWholeGroup(array $couriers, $source = false, $misc = false, $cancelPackageOnFailure = false)
    {

        $errors = false;

        $packages = [];

        /* @var $courier Courier */
        foreach($couriers AS $key => $courier)
        {
            if($courier->courierTypeU->courier === NULL)
                $courier->courierTypeU->courier = $courier;

            $return = $courier->courierTypeU->saveNewPackage();

            if(is_array($return)) {

                foreach($return AS $item) {
                    if ($source == self::SOURCE_EBAY OR $source == self::SOURCE_LINKER) {

                        // maybe already exists? Then o
                        if($source == self::SOURCE_LINKER)
                            $ebayImportSource = EbayImport::SOURCE_LINKER;
                        else
                            $ebayImportSource = EbayImport::SOURCE_EBAY;

                        // maybe already exists? Then o
                        $ebayImport = EbayImport::model()->findByAttributes(['ebay_order_id' => $item->_ebay_order_id, 'user_id' => $item->user_id, 'target' => EbayImport::TARGET_COURIER, 'source' => $ebayImportSource]);
                        if ($ebayImport === NULL) {
                            $ebayImport = new EbayImport();
                            $ebayImport->user_id = $item->user_id;
                            $ebayImport->source = $ebayImportSource;
                        }
                        $ebayImport->target_item_id = $item->id;
                        $ebayImport->ebay_order_id = $item->_ebay_order_id;
                        $ebayImport->stat = EbayImport::STAT_NEW;
                        $ebayImport->authToken = $misc;
                        $ebayImport->target = EbayImport::TARGET_COURIER;
                        $ebayImport->save();

                    }
                }
                $packages = array_merge($packages, $return);
            }

        }

//        $orderProducts = [];
//        foreach($packages AS $packageItem)
//        {
//            $product = OrderProduct::createNewProduct(
//                OrderProduct::TYPE_COURIER,
//                $packageItem->courier->id,
//                Yii::app()->PriceManager->getCurrencyCode(),
//                $packageItem->priceEach(Yii::app()->PriceManager->getCurrency()),
//                Yii::app()->PriceManager->getFinalPrice($packageItem->priceEach(Yii::app()->PriceManager->getCurrency()))
//            );
//
//            array_push($orderProducts, $product);
//        }
//
//        $order = Order::createOrderForProducts($orderProducts, true);
//
//        if($order->fastFinalizeOrder(true))
        {
            return [
                'success' => true,
                'data' => $packages,
            ];
        }

        return false;
    }


    protected function _saveNewPackage()
    {
        $errors = false;

        if (!$this->courier->saveWithAddressData(false))
            $errors = true;

        $this->courier_id = $this->courier->id;


//        if($this->cod_value > 0)
//        {
//            $this->cod = 1;
//            $this->cod_currency = Currency::PLN;
//        }

        if (!$this->save())
            $errors = true;

        /* @var $item CourierUAdditionList */
        foreach($this->listAdditions(true) AS $item)
        {
            $addition = new CourierUAddition();
            $addition->courier_id = $this->courier->id;
            $addition->description = $item->courierUAdditionListTr->description;
            $addition->name = $item->courierUAdditionListTr->title;
            $addition->courier_u_addition_list_id = $item->id;
            $addition->price = Yii::app()->PriceManager->getFinalPrice($item->getPrice());

            if(!$addition->save())
                $errors = true;
        }

        if ($errors)
            return false;
        else
            return true;

    }

    public function finalizeDetails($parentLabelId = NULL)
    {
        $courierLabelNew = new CourierLabelNew();
        $courierLabelNew->triggers_pickup = true;

        if($this->courier->_ebay_order_id != '')
            $courierLabelNew->_ebay_order_id = $this->courier->_ebay_order_id;

        $courierLabelNew->operator = $this->brokerToCourierLabelNewOperator();

        $courierLabelNew->courier_id = $this->courier->id;
        $courierLabelNew->address_data_from_id = $this->courier->sender_address_data_id;
        $courierLabelNew->address_data_to_id = $this->courier->receiver_address_data_id;

        if($parentLabelId !== NULL && in_array($this->courierUOperator->broker_id, CourierTypeU::getMultiBrokers())) {
            $courierLabelNew->parent_id = $parentLabelId;
            $courierLabelNew->stat = CourierLabelNew::STAT_WAITING_FOR_PARENT;
        } else
            $courierLabelNew->stat = CourierLabelNew::STAT_NEW;

        $courierLabelNew->save();

        $this->courier_label_new_id = $courierLabelNew->id;
        $this->save(array('courier_label_new_id', 'date_updated'));

        return $courierLabelNew;

    }


    public static function getMultiBrokers()
    {
        return CourierUOperator::getMultiBrokers();
    }

    public static function getCollectableBrokers()
    {
        return CourierUOperator::getCollectableBrokers();
    }

    public static function isCollectableByBrokerId($broker_id)
    {
        if(in_array($broker_id, self::getCollectableBrokers()))
            return true;
        else
            return false;
    }

    public function getCollectionName()
    {
        if($this->collection)
            return Yii::t('courier', 'Tak');
        else
            return Yii::t('courier', 'Nie');
    }

    public function brokerToCourierLabelNewOperator()
    {
        return CourierUOperator::brokerToCourierLabelNewOperator($this->courierUOperator->broker_id);
    }

    public function attributeLabels()
    {
        return CMap::mergeArray([
            'regulations' => Yii::t('m_app','I accept {a}terms of use{/a}', array('{a}' => '<a href="'.Yii::app()->createUrl('page/view', array('id' => 2)).'" target="_blank">', '{/a}' => '</a>')),
        ],parent::attributeLabels(), RodoRegulations::attributeLabels());
    }

//    public function getRemoteAreaPrice($currency)
//    {
//
//        $price = 0;
//
//        // FOR RECEIVER:
//        $receiverRemote = CourierDhlRemoteAreas::findZip($this->courier->receiverAddressData->zip_code, $this->courier->receiverAddressData->country_id, $this->courier->receiverAddressData->city);
//        if($receiverRemote)
//        {
//            /* @var $receiverRemote CourierDhlRemoteAreas */
//            $price += $receiverRemote->getPrice($currency);
//        }
//
//        // FOR SENDER:
//        if($this->with_pickup) {
//            $senderRemote = CourierDhlRemoteAreas::findZip($this->courier->senderAddressData->zip_code, $this->courier->senderAddressData->country_id, $this->courier->senderAddressData->city);
//            if ($senderRemote) {
//                /* @var $receiverRemote CourierDhlRemoteAreas */
//                $price += $senderRemote->getPrice($currency);
//            }
//        }
//
//        return $price;
//    }

    /*
     * Get children courier packages (from multipackage)
     */
    public function getChildrenCourierU()
    {
        return CourierTypeU::model()->findAll('parent_courier_id = :courier_id', [':courier_id' => $this->courier->id]);
    }

}