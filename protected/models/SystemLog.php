<?php

Yii::import('application.models._base.BaseSystemLog');

class SystemLog extends BaseSystemLog
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    const CAT_CHANGE = 1;
    const CAT_INFO = 2;

    const TYPE_AUTORESPONDER_DELETE = 1;
    const TYPE_AUTORESPONDER_CHANGE = 2;
    const TYPE_AUTORESPONDER_SET = 3;
    const TYPE_FORWARDER_DELETE = 4;
    const TYPE_FORWARDER_CHANGE = 5;
    const TYPE_FORWARDER_SET = 6;
    const TYPE_TEMP_INTERNAL_OPERATOR_SWITCHED = 7;
    const TYPE_TEMP_INTERNAL_OPERATOR_DELETED = 8;
    const TYPE_TEMP_INTERNAL_OPERATOR_CREATED = 9;
    const TYPE_MAPPING_REMOVED = 10;

    public function typeToText($type)
    {
        switch($type) {
            case self::TYPE_AUTORESPONDER_DELETE:
                return 'Removed email autoresponder';
                break;
            case self::TYPE_AUTORESPONDER_CHANGE:
                return 'Changed email autoresponder';
                break;
            case self::TYPE_AUTORESPONDER_SET:
                return 'Created email autoresponder';
                break;
            default:
                return NULL;
                break;
        }
    }

//    public static function getCatList()
//    {
//        $data[self::CAT_USER_CHANGED] = 'by User';
//        $data[self::CAT_ADMIN_CHANGED] = 'by Admin';
//
//        return $data;
//    }
//
//    public function getCatName()
//    {
//        return self::getCatList()[$this->cat];
//    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),

            array('type', 'required'),
            array('cat, admin_id, type', 'numerical', 'integerOnly'=>true),
            array('cat, text', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, date_entered, text, cat, admin_id, type', 'safe', 'on'=>'search'),
        );
    }

    public function beforeSave()
    {
        $this->text = strip_tags($this->text);
        $this->text = trim($this->text);

        return parent::beforeSave();
    }

    public static function addToLog($type, $text = '', $admin_id = false, $cat = self::CAT_CHANGE)
    {
        if(!$admin_id)
            $admin_id = Yii::app()->user->isGuest ? NULL : Yii::app()->user->id;

        $model = new self;
        $model->text = $text;
        $model->cat = $cat;
        $model->type = $type;
        $model->admin_id = $admin_id;
        $model->text = $text;


        return $model->save();
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('cat', $this->cat);
        $criteria->compare('type', $this->type);
        $criteria->compare('admin_id', $this->admin_id );


        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';

        return new CActiveDataProvider($this->with('admin'), array(
            'criteria' => $criteria,
            'sort'=> $sort,
            'Pagination' => array (
                'PageSize' => 50,
            ),
        ));
    }
}