<?php
if(YII_LOCAL):
    ?>
    <div class="text-right" id="lang-menu">
        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_DEFAULT ? '<span>pl</span>' : '<a href="./?lang=pl">pl</a>';?>
        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_RS ? '<span>int</span>' : '<a href="./?lang=en">int</a>';?>
        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_PLI ? '<span>uk</span>' : '<a href="./?lang=en_uk">uk</a>';?>
        <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_RUCH ?'<span>ruch</span>' : '<a href="./?lang=pl_ruch">ruch</a>';?>
    </div>
    <?php
else:
    if(Yii::app()->PV->get() !== PageVersion::PAGEVERSION_RUCH):
        ?>
        <div class="text-right" id="lang-menu">
            <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_DEFAULT ? '<span>pl</span>':'<a href="http://www.swiatprzesylek.pl">pl</a>';?>
            <?php echo Yii::app()->PV->get() == PageVersion::PAGEVERSION_RS ? '<span>en</span>':'<a href="http://www.royalshipments.com">en</a>';?>
        </div>
        <?php
    endif;
endif;
?>

