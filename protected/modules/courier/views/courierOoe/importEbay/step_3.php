<?php

$bringSenderNameForward = false;

// SPECIAL LAYOUT RULE FOR USER "KWAZAR" && "KWAZAR_DPD" // 29.09.2017
if(in_array(Yii::app()->user->id, [68,937]))
    $bringSenderNameForward = true;

// LAYOUT CONFIGURATION

$colWidth[0] = 20;
$colWidth[1] = 120;
$colWidth[2] = 200;
$colWidth[3] = 90;
$colWidth[4] = 90;
$colWidth[5] = 230;
$colWidth[6] = 100;
$colWidth[7] = 100;
$colWidth[8] = 100;
$colWidth[9] = 100;
$colWidth[10] = 120;
$colWidth[11] = 120;
$colWidth[12] = 240;
$colWidth[13] = 240;
$colWidth[14] = 240;
$colWidth[15] = 240;
$colWidth[16] = 240;
$colWidth[17] = 240;
$colWidth[18] = 240;
$colWidth[19] = 240;
$colWidth[20] = 240;
$colWidth[21] = 240;
$colWidth[22] = 240;
$colWidth[23] = 240;
$colWidth[24] = 240;
$colWidth[25] = 240;
$colWidth[26] = 240;
$colWidth[27] = 240;
$colWidth[28] = 240;
$colWidth[29] = 240;


if($bringSenderNameForward)
{
    for($k = 23; $k > 7; $k--)
        $colWidth[$k] = $colWidth[$k - 1];

    $colWidth[6] = 240;
    $colWidth[7] = 240;
}

$totalWidth = 0;
foreach($colWidth As $item)
    $totalWidth += $item;

$totalWidth += 0;

$height = S_Useful::sizeof($models) * 31 + 60;
if($height > 850)
    $height = 850
?>


<?php $modelTemp = new Courier;?>
<?php $modelTemp2 = new AddressData();?>
<?php $modelTemp3 = new CourierTypeOoe();?>

<?php AddressDataSuggester::registerAssets(); ?>
<?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>

<?php
$this->renderPartial('importEbay/_header',[
    'ebayClient' => false,
    'step' => 3,
]);
?>


<?php
// WE DO NOT NEED FORM HERE - ALL DATA IS IN CACHE
//$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
//    array(
//        'enableAjaxValidation' => false,
//    )
//);
?>

<div class="text-center">
    <a href="<?php echo Yii::app()->createUrl('/courier/courierOoe/ebay', ['al' => true]);?>" class="btn btn-warning"><?php echo Yii::t('courier', 'Od nowa');?></a>
    <br/>
</div>
<hr/>
<div class="text-center">
    <a href="#bottom">[<?php echo Yii::t('courier', 'skocz na dół');?>]</a>
</div>
<br/>
<?php
$this->widget('FlashPrinter');
?>


<div style="position: relative;">
    <div class="overlay"><div class="loader"></div></div>
    <?php
    // for upper scroll:
    ?>
    <div style="width: 100%; position: relative; height: 20px; overflow-x: scroll;" class="dummy-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; height: 20px;">
        </div>
    </div>
    <?php
    // end for upper scroll
    ?>
    <div style="width: 100%; position: relative; height: 60px; overflow: hidden;" class="header-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute; overflow: hidden;">
            <table class="list hTop" style="font-size: 10px; text-align: left !important; table-layout: fixed; height: 60px;">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <tr>
                    <?php
                    if(!$bringSenderNameForward):
                        ?>
                        <td colspan="14"><?php echo Yii::t('courier','Packages');?></td>
                        <td colspan="8" ><?php echo Yii::t('courier','Sender');?></td>
                        <td colspan="8"><?php echo Yii::t('courier','Receiver');?></td>
                        <?php
                    else:
                        ?>
                        <td colspan="6"><?php echo Yii::t('courier','Packages');?></td>
                        <td colspan="2" ><?php echo Yii::t('courier','Receiver');?></td>
                        <td colspan="8"><?php echo Yii::t('courier','Packages');?></td>
                        <td colspan="8" ><?php echo Yii::t('courier','Sender');?></td>
                        <td colspan="6"><?php echo Yii::t('courier','Receiver');?></td>
                        <?php
                    endif;
                    ?>
                </tr>
                <tr>
                    <td>#</td>
                    <td><?php echo Yii::t('courier', 'Is valid');?></td>
                    <td><?php echo Yii::t('courier_ebay', 'eBay order ID');?></td>
                    <td></td>
                    <td></td>
                    <td>
                        <?php echo str_replace('Package','', $modelTemp3->getAttributeLabel('service_code')); ?>
                    </td>
                    <?php
                    if($bringSenderNameForward):
                        ?>
                        <td style="border-left: 2px dashed gray;">
                            <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                        </td>
                        <td>
                            <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                        </td>
                        <?php
                    endif;
                    ?>
                    <td <?= $bringSenderNameForward ? 'style="border-left: 2px dashed gray;"' : '';?>>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_weight')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_l')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_w')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_size_d')); ?>
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_value')); ?> [zł]
                    </td><td>
                        <?php echo str_replace('Package','', $modelTemp->getAttributeLabel('package_content')); ?>
                    </td><td style="border-left: 2px dashed gray;">
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td>
                    <?php
                    if(!$bringSenderNameForward):
                        ?>
                        <td style="border-left: 2px dashed gray;">
                            <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'name'); ?>
                        </td>
                        <td>
                            <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'company'); ?>
                        </td>
                        <?php
                    endif;
                    ?>
                    <td <?= $bringSenderNameForward ? 'style="border-left: 2px dashed gray;"' : '';?>>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'country'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'zip_code'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'city'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_1'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'address_line_2'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'tel'); ?>
                    </td><td>
                        <?php echo $modelTemp2->getAttributeLabel((isset($i)?'['.$i.']':'').'email'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="width: 100%; position: relative; height: <?= $height; ?>px; overflow-x: scroll;" class="real-wrapper">
        <div style="width: <?= $totalWidth;?>px; position: absolute;">

            <table class="list list-special" style="font-size: 10px; text-align: left !important; table-layout: fixed;">
                <colgroup>
                    <?php foreach($colWidth AS $item): ?>
                        <col width="<?= $item; ?>"></col>
                    <?php endforeach;?>
                </colgroup>
                <?php
                foreach($models AS $key => $item):
                    $this->renderPartial('importEbay/_item',
                        array(
                            'model' => $item,
                            'i' => $key,
                            'ebay' => true,
                            'bringSenderNameForward' => $bringSenderNameForward,
                        ));
                endforeach;
                ?>
            </table>

        </div>
    </div>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
        array(
            'enableAjaxValidation' => false,
            'id' => 'import-form',
        )
    );
    ?>

    <div class="navigate" style="text-align: center;" id="bottom">
        <?php echo TbHtml::submitButton(Yii::t('courier', 'Save this part of data'), array('name' => 'save', 'class' => 'save-packages btn-primary')); ?>
    </div>

    <?php
    $this->endWidget();
    ?>

</div>

<div class="modal fade " tabindex="-1" role="dialog" id="edit-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="edit-modal-content">
        </div>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  removeRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierOoe/ajaxRemoveRowOnImport', ['e' => true])).',
                  editRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierOoe/ajaxEditRowOnImport', ['e' => true])).',
                  updateRowOnImport: '.CJSON::encode(Yii::app()->createUrl('courier/courierOoe/ajaxUpdateRowItemOnImport', ['e' => true])).',
              },
               text: {
                  unknownError: '.CJSON::encode(Yii::t('site', 'Wystąpił nieznany błąd!')).',
                     beforeLeaving: '.CJSON::encode(Yii::t('site', 'Jesteś na pewno chcesz opuścić tę stronę? Wprowadzone dane mogą zostać utracone')).',
              },
              misc: {
                  hash: '.CJSON::encode($hash).',
              },
          };
      ',CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/importListModels.js', CClientScript::POS_HEAD);
?>
