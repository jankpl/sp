<?php

require_once('LpExpressApi.php');

class LpExpressClient
{

    const URL = GLOBAL_CONFIG::LP_EXPRESS_URL;
    const CLIENT_KEY = GLOBAL_CONFIG::LP_EXPRESS_CLIENT_KEY;
    const PARTNER_ID =  GLOBAL_CONFIG::LP_EXPRESS_PARTNER_ID;
    const PARNTER_PASSWORD = GLOBAL_CONFIG::LP_EXPRESS_PARTNER_PASSWORD;
    const CUSTOMER_ID = GLOBAL_CONFIG::LP_EXPRESS_CUSTOMER_ID;
    const PAYMENT_PIN = GLOBAL_CONFIG::LP_EXPRESS_PAYMENT_PIN;
    const ADMIN_PIN = GLOBAL_CONFIG::LP_EXPRESS_ADMIN_PIN;

    // SENDER IS ALWAYS KAAB:
    public $sender_name = 'KAAB';
    public $sender_address = 'Erdvės g. 68, Ramučių km., Karmėlavos sen.';
    public $sender_city = 'Kauno raj.';
    public $sender_country = 'LT';
    public $sender_zip_code = '52114';
    public $sender_tel = '0048221221218';
    public $sender_email;

    public $local_id;

    public $package_weight;
    public $package_value;
    public $package_content;

    public $size_l;
    public $size_d;
    public $size_w;

    public $receiver_name;
    public $receiver_address;
    public $receiver_city;
    public $receiver_country;
    public $receiver_zip_code;
    public $receiver_tel;
    public $receiver_email;

    public $cod_value = 0;
    public $cod_currency = false;

    public $saturday_delivery = false;
    public $load_service = false;
    public $delivery_notification = false;
    public $insurance = false;
    public $same_day_delivery = false;
    public $has_cover_letter = false;

    public $destinationTerminalId = false;
    public $pickupInTerminal = false;

    public static function orderForCourierU(CourierTypeU $courierTypeU, $returnError = false)
    {
        $model = new self;

//        $from = $courierTypeU->courier->senderAddressData;
        $to = $courierTypeU->courier->receiverAddressData;

        $model->package_weight = $courierTypeU->courier->getWeight(true);
        $model->package_value = $courierTypeU->courier->getPackageValueConverted('EUR');
        $model->package_content = $courierTypeU->courier->package_content;

        $model->size_l = $courierTypeU->courier->package_size_l;
        $model->size_d = $courierTypeU->courier->package_size_d;
        $model->size_w = $courierTypeU->courier->package_size_w;

//        $model->sender_name = trim($from->name.' '.$from->company);
//        $model->sender_zip_code = $from->zip_code;
//        $model->sender_city = $from->city;
//        $model->sender_address = trim($from->address_line_1.' '.$from->address_line_2);
//        $model->sender_tel = $from->tel;
//        $model->sender_country = strtoupper($from->country0->code);
//        $model->sender_email = $from->fetchEmailAddress(true);

        $model->receiver_name = trim($to->name.' '.$to->company);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->local_id = $courierTypeU->courier->local_id;

        if($courierTypeU->courier->cod_value) {
            $model->cod_value = $courierTypeU->courier->cod_value;
            $model->cod_currency = $courierTypeU->courier->cod_currency;
        }

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_LPEXPRESS_SATURDAY_DELIVERY))
            $model->saturday_delivery = true;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_LPEXPRESS_HAS_LOAD))
            $model->load_service = true;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_LPEXPRESS_DELIVERY_NOTIFICATION))
            $model->delivery_notification = true;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_LPEXPRESS_INSURANCE))
            $model->insurance = true;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_LPEXPRESS_SAME_DAY_DELIVERY))
            $model->same_day_delivery = true;

        if($courierTypeU->hasAdditionByUniqId(CourierUAdditionList::UNIQID_LPEXPRESS_HAS_COVER_LETTER))
            $model->has_cover_letter = true;


        if($courierTypeU->courier->user->getLpExpressCustomData('active'))
        {
            $model->sender_name = $courierTypeU->courier->user->getLpExpressCustomData('sender_name');
            $model->sender_address = $courierTypeU->courier->user->getLpExpressCustomData('sender_address');
            $model->sender_city = $courierTypeU->courier->user->getLpExpressCustomData('sender_city');
            $model->sender_zip_code = $courierTypeU->courier->user->getLpExpressCustomData('sender_zipcode');
            $model->sender_tel = $courierTypeU->courier->user->getLpExpressCustomData('sender_tel');
        }

        return $model->_go($returnError);
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierTypeInternal, AddressData $from, AddressData $to, $returnError = false)
    {
        $model = new self;

        if($courierTypeInternal->_lp_express_receiver_point)
            $model->destinationTerminalId = $courierTypeInternal->_lp_express_receiver_point;

        if($courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_LP_EXPRESS)
            $model->pickupInTerminal = true;


        $model->package_weight = $courierTypeInternal->courier->getWeight(true);
        $model->package_value = $courierTypeInternal->courier->getPackageValueConverted('EUR');
        $model->package_content = $courierTypeInternal->courier->package_content;

        $model->size_l = $courierTypeInternal->courier->package_size_l;
        $model->size_d = $courierTypeInternal->courier->package_size_d;
        $model->size_w = $courierTypeInternal->courier->package_size_w;

//        $model->sender_name = trim($from->name.' '.$from->company);
//        $model->sender_zip_code = $from->zip_code;
//        $model->sender_city = $from->city;
//        $model->sender_address = trim($from->address_line_1.' '.$from->address_line_2);
//        $model->sender_tel = $from->tel;
//        $model->sender_country = strtoupper($from->country0->code);
//        $model->sender_email = $from->fetchEmailAddress(true);

        $model->receiver_name = trim($to->name.' '.$to->company);
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address = trim($to->address_line_1.' '.$to->address_line_2);
        $model->receiver_tel = $to->tel;
        $model->receiver_country = strtoupper($to->country0->code);
        $model->receiver_email = $to->fetchEmailAddress(true);

        $model->local_id = $courierTypeInternal->courier->local_id;

        if($courierTypeInternal->courier->cod_value) {
            $model->cod_value = $courierTypeInternal->courier->cod_value;
            $model->cod_currency = $courierTypeInternal->courier->cod_currency;
        }

        if($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_LPEXPRESS_SATURDAY_DELIVERY))
            $model->saturday_delivery = true;

        if($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_LPEXPRESS_HAS_LOAD))
            $model->load_service = true;

        if($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_LPEXPRESS_DELIVERY_NOTIFICATION))
            $model->delivery_notification = true;

        if($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_LPEXPRESS_INSURANCE))
            $model->insurance = true;

        if($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_LPEXPRESS_SAME_DAY_DELIVERY))
            $model->same_day_delivery = true;

        if($courierTypeInternal->hasAdditionByUniqId(CourierAdditionList::UNIQID_LPEXPRESS_HAS_COVER_LETTER))
            $model->has_cover_letter = true;


        if($courierTypeInternal->courier->user->getLpExpressCustomData('active'))
        {
            $model->sender_name = $courierTypeInternal->courier->user->getLpExpressCustomData('sender_name');
            $model->sender_address = $courierTypeInternal->courier->user->getLpExpressCustomData('sender_address');
            $model->sender_city = $courierTypeInternal->courier->user->getLpExpressCustomData('sender_city');
            $model->sender_zip_code = $courierTypeInternal->courier->user->getLpExpressCustomData('sender_zipcode');
            $model->sender_tel = $courierTypeInternal->courier->user->getLpExpressCustomData('sender_tel');
        }

        return $model->_go($returnError);
    }


    protected static function _getConnector()
    {
        /*
* Configuration
*/
        $options = array(
            'hostname' => self::URL, // Enter hostname of the web service, e. g. http://apibeta.balticpost.lt
            'partnerId' => self::PARTNER_ID, // Enter your Partner ID
            'partnerPassword' => self::PARNTER_PASSWORD, // Enter your Partner password
            'rawOptions' => array(
                'trace' => true,
                'connection_timeout' => 5,
//                'cache_wsdl' => WSDL_CACHE_DISK
            )
        );

        return new LpExpressApi($options);

    }


    protected function _go($returnError = false){

        $client = self::_getConnector();


        $customerId = self::CUSTOMER_ID; // Enter your LP Express customer ID
        $paymentPin = self::PAYMENT_PIN; // Enter your Payment PIN of the customer
        $adminPin = self::ADMIN_PIN; // Enter your Administration PIN of the customer

        $withDeclaration = false;



        if($this->pickupInTerminal)
        {
            if($this->destinationTerminalId)
                $product = 'CC';
            else if(strtoupper($this->receiver_country) == 'LT')
                $product = 'CH';
            else
                $product = 'CA';
        } else {

            if($this->destinationTerminalId)
                $product = 'HC';
            else if(strtoupper($this->receiver_country) == 'LT')
                $product = 'EB';
            else
                $product = 'IN';
        }

        try {

            $label = array(
                'partnerorderartid' => $this->local_id,
                'productcode' => $product,
                'parcelweight' => $this->package_weight,
                'sendername' => $this->sender_name,
                'sendermobile' => $this->sender_tel,
                'senderaddressfield1' => $this->sender_address,
                'senderaddresscity' => $this->sender_city,
                'senderaddresszip' => $this->sender_zip_code,
                'senderaddresscountry' => $this->sender_country,
                'receivername' => $this->receiver_name,
                'receivermobile' => $this->receiver_tel,
                'receiveremail' => $this->receiver_email,
                'receiveraddressfield1' => $this->receiver_address,
                'receiveraddresscity' => $this->receiver_city,
                'receiveraddresszip' => $this->receiver_zip_code,
                'receiveraddresscountry' => $this->receiver_country
                // Customize your order by adding more parameters
            );

            if(in_array($product, ['HC','CC','CH','CA'])) {
                $data = [
                    'product_type' => $product,
                    'dimension_x' => $this->size_l,
                    'dimension_y' => $this->size_d,
                    'dimension_z' => $this->size_w,
                ];

                $boxSizeResponse = $client->call('get_product_by_dimensions', $data);

                $boxSize = $boxSizeResponse->boxsize;
                $label['boxsize'] = $boxSize;
            }

            if($this->destinationTerminalId)
                $label['targetmachineidentification'] = $this->destinationTerminalId;



            $this->cod_value = 0;
//            $this->has_cover_letter = true;
//            $this->delivery_notification = true;
//            $this->insurance = true;

            if($this->cod_value)
            {
                $label['iscod'] = 1;
                $label['parcelvalue'] = $this->cod_value;
                $label['parcelvaluecurrency'] = $this->cod_currency;
            }

            if($this->has_cover_letter)
                $label['hascoverletter'] = 1;

            if($this->delivery_notification)
                $label['hasdeliverynotifi'] = 1;

            if($this->load_service)
                $label['hasload'] = 1;

            if($this->saturday_delivery)
                $label['hassatdelivery'] = 1;

            if($withDeclaration)
            {
                $declaration = [];
                $declaration['name'] = $this->package_content;
                $declaration['quantity'] = 1;
                $declaration['weight'] = $this->package_weight;
                $declaration['value'] = $this->package_value;
                $label['declaration'] = $declaration;
            }

            if($this->insurance)
            {
                $label['hasinsurance'] = 1;
                $label['insuranceamount'] = $this->package_value;
            }

            $labels = [];
            $labels[] = $label;

//            for($i = 2; $i <= $this->packages_number; $i++)
//            {
//                $temp = $label;
//                $temp['partnerorderartid'] .= '_'.$i;
//                $temp['iscod'] = 0;
//                $temp['parcelvalue'] = 0;
//                $temp['parcelvaluecurrency'] = '';
//
//                $labels[] = $temp;
//            }

            $addRequest = array(
                'partnerorderid' => $this->local_id,
                'kepoluserid' => $customerId,
                'paymentpin' => $paymentPin,
                'labels' => $labels,
            );


            $addResponse = $client->call('add_labels', $addRequest);


            /*
             * Activate order
             */
            $confirmRequest = array(
                'partnerid' => $addResponse->orderid
            );

            $confirmResponse = $client->call('confirm_labels', $confirmRequest);

            $label = @file_get_contents($client->getLabelsUri($confirmResponse->orderpdfid));

            $im = ImagickMine::newInstance();
            $im->setResolution(300, 300);
            $im->readImageBlob($label);
            $im->setImageFormat('png8');
            if ($im->getImageAlphaChannel()) {
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            }
            $im->trimImage(0);
            $im->stripImage();

            $return = [];
            /*
             * Call a courier
             */
            $parcels = array();
            foreach ($confirmResponse->labels as $label) {
                $parcels[] = $label->identcode;

                $return[] = [
                    'status' => true,
                    'label' => $im->getImageBlob(),
                    'track_id' => $label->identcode,
                ];
            }

            $callRequest = array(
                'kepoluserid' => $customerId,
                'adminpin' => $adminPin,
                'parcels' => $parcels
            );

//            $callResponse = $client->call('call_courier', $callRequest);
            $client->call('call_courier', $callRequest);

//            /*
//             * Download manifests
//             */
//            $pathPrefix = $downloadDir . 'manifest_' . $addRequest['partnerorderid'] . '_';
//            foreach ($callResponse->calls as $i => $call) {
//                $manifestDocPath = $pathPrefix . $i . '.pdf';
//                $status = file_put_contents(
//                    $manifestDocPath,
//                    fopen($client->getManifestUri($call->manifestid), 'r')
//                );
//                if (!$status) {
//                    // Handle failed download
//                }
//            }


            return $return;

        }
        catch (SoapFault $e)
        {

            MyDump::dump('lp_express.txt', print_r($client->getConnector()->__getLastRequest(),1));
            MyDump::dump('lp_express.txt', print_r($client->getConnector()->__getLastResponse(),1));
            MyDump::dump('lp_express.txt', print_r($e,1));

            if ($returnError == true) {
                $return = [0 => [
                    'error' => print_r($e->getCode().' ('.$e->getMessage().')',1),
                ]];
                return $return;
            } else {
                return false;
            }

        }
//        catch (Exception $e)
//        {
//
//            MyDump::dump('lp_express.txt', print_r($client->getConnector()->__getLastRequest(),1));
//            MyDump::dump('lp_express.txt', print_r($client->getConnector()->__getLastResponse(),1));
//            MyDump::dump('lp_express.txt', print_r($e,1));
//
//            if ($returnError == true) {
//                $return = [0 => [
//                    'error' => print_r($e->getCode().' ('.$e->getMessage().')',1),
//                ]];
//                return $return;
//            } else {
//                return false;
//            }
//
//        }
    }

    public static function getTerminals()
    {
        $CACHE_NAME = 'LP_EXPRESS_TERMINALS';

        $terminals = Yii::app()->cacheFile->get($CACHE_NAME);

        if($terminals === false) {

            $client = self::_getConnector();
            $terminals = $client->call('public_terminals', []);

            foreach($terminals AS $key => $item)
            {
                if($item->latitude == '' OR $item->longitude == '')
                {
                    $address = $item->address . ' ' . $item->city . ' Lithuania';

                    $resp = GoogleApi::getCoordinatesByAddress($address);

                    $terminals[$key]->latitude = $resp[0];
                    $terminals[$key]->longitude = $resp[1];
                }
            }


            Yii::app()->cacheFile->set($CACHE_NAME, $terminals, 60*60*24);
        }

        return $terminals;
    }


    public static function track($id)
    {

        $CACHE_NAME = 'LP_EXPRESS_TRACKING_FILE';

        $file = Yii::app()->cacheFile->get($CACHE_NAME);

        if($file === false)
        {
            $file = @file_get_contents(self::URL.'/download/ttevents/v2?key='.self::CLIENT_KEY.'&compression=raw');
            Yii::app()->cacheFile->set($CACHE_NAME, $file, 60*60);

            MyDump::dump('lp_express_tt_file_backup.txt', print_r($file,1));
        }

        $file = @simplexml_load_string($file);

        if($file)
            foreach($file->data->item AS $item)
            {

                if($item->identcode == $id)
                {
                    $statuses = [];

                    $events = $item->events;

                    if(isset($events->event))
                        $events = $events->event;

                    foreach($events AS $event)
                    {
                        $location = NULL;
                        if($event->title == 'DA_DELIVERED')
                            $location = '(signed for by: '.(string) $event->recipient.')';

                        $statuses[] = [
                            'name' => (string) $event->title,
                            'date' => (string) $event->time,
                            'location' => $location,
                        ];
                    }

                    return $statuses;
                }
            }

        return false;
    }



}