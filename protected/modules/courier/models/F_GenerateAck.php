<?php

class F_GenerateAck extends CFormModel {

    const MAX_DAYS = 7;

    public $onlyNew;
    public $days;
    public $ignoreOperatorsCustoms;

    public static function listOfDays()
    {
        $data = range(1,self::MAX_DAYS);

        return array_combine($data, $data);
    }

    public function rules() {
        return array(
            array('days', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => self::MAX_DAYS),
            array('onlyNew, ignoreOperatorsCustoms', 'boolean'),
        );
    }

    public function attributeLabels() {
        return array(
            'onlyNew' => Yii::t('ack', 'Tylko nowe paczki'),
            'days' => Yii::t('app', 'Z ilu ostatnich dni'),
            'ignoreOperatorsCustoms' => Yii::t('app', 'Ignoruj customowe karty operatorów'),
        );
    }

}