<?php
/* @var $this PrintServerController */
/* @var $models PrintServer[] */

?>

    <h2><?= Yii::t('user', 'Print server'); ?></h2>
    <div class=""><?= Yii::t('printServer', 'Skonfigurowanie tej usługi pozwoli na bezpośrednie przesyłanie etykiet do drukarek typu Zebra natychmiast po ich wygenerowaniu.');?></div>
    <div class=""><?= Yii::t('printServer', 'Usługa dostępna dla przesyłek kurierskich.');?></div>
    <br/>

<?php
$this->widget('FlashPrinter');
?>



<?php
if(!PrintServer::numberOfPsForUser()):
    ?>
    <div class="alert alert-danger text-center"><?= Yii::t('printServer', 'Nie masz jeszcze żadnej drukarki.');?></div>
    <?php
elseif(!PrintServer::getAddressForUser(Yii::app()->user->model)):
    ?>
    <div class="alert alert-warning text-center"><?= Yii::t('printServer', 'Nie masz wybranej drukarki domyślnej - korzystanie z Print Serwera jest możliwe tylko przez API.');?></div>
    <?php
else:
    ?>
    <div class="alert alert-success text-center"><?= Yii::t('printServer', 'Usługa jest aktywna');?></div>
    <?php
endif;
?>
<?php
if(PrintServer::numberOfPsForUser()):
    ?>
    <h3><?= Yii::t('printServer', 'Lista Twoich drukarek');?></h3>
    <div class="row">
        <div class="col-xs-12 col-sm-8">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><?= Yii::t('printServer','Id');?></th>
                    <th><?= Yii::t('printServer','IP');?></th>
                    <th><?= Yii::t('printServer','Port');?></th>
                    <th class="text-center"><?= Yii::t('printServer','Domyślna');?></th>
                    <th class="text-center"><?= Yii::t('printServer','Status');?></th>
                    <th><?= Yii::t('printServer','Akcje');?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if(!S_Useful::sizeof($models)):
                    ?>
                    <tr>
                        <td colspan="6">-</td>
                    </tr>
                    <?php
                else:
                    foreach($models AS $item):
                        ?>
                        <tr>
                            <td><?= $item->no;?></td>
                            <td><?= $item->address_ip;?></td>
                            <td><?= $item->address_port;?></td>
                            <td class="text-center"><?= $item->is_default ? ('<span class="label label-success">'.Yii::t('printServer', 'Tak').'</span>') : ($item->stat == PrintServer::STAT_ACTIVE ? CHtml::link(Yii::t('printServer', 'wybierz'), ['/printServer/setDefault', 'no' => $item->no], ['class' => 'btn btn-xs btn-warning', 'onclick' => 'return confirm("'.Yii::t('printServer', 'Na pewno?').'")']) : '');?></td>
                            <td class="text-center"><?= $item->stat == PrintServer::STAT_ACTIVE ? '<span class="label label-success">'.$item->getStatName().'</span>' : $item->getStatName(); ?></td>
                            <td class="text-center"> <?= CHtml::link(Yii::t('printServer', 'Szczegóły'), ['/printServer/index', 'no' => $item->no], ['class' => 'btn btn-xs']);?></td>
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>
                </tbody>
            </table>
            <span class=""><?= Yii::t('printServer', 'Maksymalna liczba drukarek: ');?><?= PrintServer::MAX_PRINT_SERVERS;?></span>
        </div>
    </div>
    <?php
endif;
?>

    <br/>
<?php
if(PrintServer::isPossibleToAddNew()):
    ?>
    <div class="text-center">
        <?= CHtml::link(Yii::t('printServer', 'Dodaj drukarkę'), ['/printServer/new'], ['class' => 'btn btn-mini btn-success']);?>
    </div>
    <?php
endif;



