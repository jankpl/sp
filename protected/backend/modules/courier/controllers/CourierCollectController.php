<?php

class CourierCollectController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new _CourierCollectGridView('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierCollectGridView']))
            $model->setAttributes($_GET['CourierCollectGridView']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierCollect'),
        ));
    }



}