<?php

class WhistlClient extends GlobalOperator
{
    const URL = GLOBAL_CONFIG::WHISTL_WSDL;

    const FTP_ADDRESS = 'mailnet-ftp.whistl.co.uk';
    const FTP_LOGIN = 'BICL10833';
    const FTP_PASSWORD = 'nviI?piN';
    const FTP_DIR = 'DataOut/';

    const CLIENT_CODE_TRACKED = 'L10833';
    const SITE_CODE_TRACKED = 'L108331';
    const CUSTOMER_NO_TRACKED = 'L1083311';

    const CLIENT_CODE_INSIGHT = 'L10834'; // NOT USED
    const SITE_CODE_INSIGHT = 'L108341'; // NOT USED
    const CUSTOMER_NO_INSIGHT = 'L1083411'; // NOT USED

    const SERVICE_24 = 'TRACKED24';

    const SERVICE_48 = 'TRACKED48';
    const SERVICE_72 = 'TRACKED72';
    const SERVICE_INSIGHT = 'INSIGHT';

    public $packages_number = 1;

    public $local_id;
    public $package_value;

    public $sender_country;

    public $receiver_name;
    public $receiver_company;
    public $receiver_address1;
    public $receiver_address2;
    public $receiver_zip_code;
    public $receiver_city;
    public $receiver_country;
    public $receiver_tel;
    public $receiver_email;

    public $package_weight;
    public $package_size_l;
    public $package_size_w;
    public $package_size_d;

    public $package_content;

//    public $business = false;
    public $pod = false;
    public $aerosol = false;
    public $fragile = false;

    public $service;

    public static function getAdditions(Courier $courier)
    {

        $CACHE_NAME = 'WHISTL_ADDITIONS_'.self::getCacheSuffix($courier);
        $models = Yii::app()->cache->get($CACHE_NAME);

        if ($models === false) {

            $additions = [];

            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_POD;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_AEROSOL;
            $additions[] = GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_FRAGILE;

            $array = implode(',', $additions);

            if(!S_Useful::sizeof($additions))
                $models = [];
            else
                $models = self::getBaseAdditionsListModel($courier)::model()->findAll('stat = ' . S_Status::ACTIVE . ' AND uniq_id IN (' . $array . ')');

            Yii::app()->cache->set($CACHE_NAME, $models, 60 * 15);
        }

        return $models;
    }

    public static function createShipmentByTest()
    {
        $model = new self;

        $model->sender_country = 'GB';

        $model->receiver_name = 'Jan Nowak';
        $model->receiver_company = 'Nowak Inc';
        $model->receiver_zip_code = 'NE28 7JG';
        $model->receiver_city = 'NEWCASTLE UPON TYNE';
        $model->receiver_address1 = '5 BEDE CRESCENT';
        $model->receiver_tel = '111222333';
        $model->receiver_country = 'GB';

        $model->receiver_name = 'Jan Nowak';
        $model->receiver_company = 'Nowak Inc';
        $model->receiver_zip_code = 'GY1 3HW';
        $model->receiver_city = 'St Peter Port';
        $model->receiver_address1 = 'Glategny Esplanade';
        $model->receiver_tel = '111222333';
        $model->receiver_country = 'GB';

        $model->receiver_name = 'Jan Nowak';
        $model->receiver_company = 'Nowak Inc';
        $model->receiver_zip_code = 'KW1 5NU';
        $model->receiver_city = 'Wick';
        $model->receiver_address1 = 'South Road';
        $model->receiver_tel = '111222333';
        $model->receiver_country = 'GB';


        $model->package_weight = 1;
        $model->packages_number = 1;

        $model->package_size_l = 20;
        $model->package_size_d = 20;
        $model->package_size_w = 20;

        $model->package_content = 'Test';
        $model->comments = 'Order #123';

        $model->local_id = 'test133'.rand(999,99999);

        $model->service = self::SERVICE_48;

        $model->packages_number = 2;

        return $model->createShipment(true);
    }

    protected function createShipment($returnError = false)
    {


        if($this->service != self::SERVICE_INSIGHT)
            $tracked = true;

        $pod = $this->pod;
        $fragile = $this->fragile;

//        $customer = false;
        $customer = 'P'; // always private

        $dimensions = [ $this->package_size_l, $this->package_size_d, $this->package_size_w];
        rsort($dimensions);

        $data = new stdClass();



        $name = $this->receiver_name;
        $name = explode(' ', $name);
        $firstName = array_shift($name);
        $lastName = S_Useful::sizeof($name) ? implode(' ', $name) : $firstName;

        $data->parameterIn = new stdClass();
        $data->parameterIn->AddressLine1 = $this->receiver_address1;
        $data->parameterIn->AddressLine2 = $this->receiver_address2;
        $data->parameterIn->AddressLine3 = $this->receiver_city;
        $data->parameterIn->Areosol = false;
        $data->parameterIn->ClientCode = $tracked ? self::CLIENT_CODE_TRACKED : self::CLIENT_CODE_INSIGHT;
        $data->parameterIn->ClientDespatchService = $this->service;
        $data->parameterIn->ClientSiteOrderReference = $this->local_id;
        $data->parameterIn->ClientOrderReference = mb_substr($this->package_content, 0, 50);
        $data->parameterIn->CompanyName = $this->receiver_company;
        $data->parameterIn->FirstName = $firstName;
        $data->parameterIn->LastName = $lastName;
        $data->parameterIn->DeliveryToName = trim($this->receiver_name.' '.$this->receiver_company);
        $data->parameterIn->CountryCode = $this->receiver_country;
        $data->parameterIn->CountryName = $this->receiver_country;
        $data->parameterIn->Currency = 'GBP';
        $data->parameterIn->CustomerType = $customer;
        $data->parameterIn->CustomerNumber = $tracked ? self::CUSTOMER_NO_TRACKED : self::CUSTOMER_NO_INSIGHT;
        $data->parameterIn->DeliveryPointCode = $this->receiver_zip_code;
        $data->parameterIn->DeliveryToEmail = $this->receiver_email;
        $data->parameterIn->DeliveryToMobilePhone = $this->receiver_tel;
        $data->parameterIn->DeliveryToName = $this->receiver_name;
        $data->parameterIn->Fragile = $fragile ? 'Y' : 'N';
        $data->parameterIn->MaxItemWeight = $this->package_weight;
        $data->parameterIn->MaxLength = $dimensions[0] * 10;
        $data->parameterIn->MaxLengthSum = array_sum($dimensions) * 10;
        $data->parameterIn->MaxParcelWeight = $this->package_weight;
        $data->parameterIn->OrderNumber = $this->local_id;
        $data->parameterIn->OrderValue = $this->package_value;
        $data->parameterIn->OrderVolume = round(($this->package_size_l/100)*($this->package_size_d/100)*($this->package_size_w/100),3) * $this->packages_number;
        $data->parameterIn->OrderWeight = $this->package_weight * $this->packages_number;
        $data->parameterIn->PalletDelivery = false;
        $data->parameterIn->PrintType = 'PNG';
        $data->parameterIn->ProofOfDelivery = $pod;
        $data->parameterIn->Site = $tracked ? self::SITE_CODE_TRACKED : self::SITE_CODE_INSIGHT;
        $data->parameterIn->TwoManLift = false;
        $data->parameterIn->VerboseMode = 'F';

        $data->parameterIn->Parcels = [];


        for($i = 0; $i < $this->packages_number; $i++)
        {
            $parcel = new stdClass();
            $parcel->Depth = $dimensions[1] * 10;
            $parcel->Height = $dimensions[2] * 10;
            $parcel->LargestArea = ($dimensions[0] * 10)*($dimensions[1] * 10);
            $parcel->Number = $i;
            $parcel->Weight = $this->package_weight;
            $parcel->ParcelProducts = [];

            $product = new stdClass();
            $product->ClientProductID = '';
            $product->CountryOfOrigin = $this->sender_country;
            $product->Sku = $this->local_id;

            $parcelProduct = new stdClass();
            $parcelProduct->Code = $this->local_id;
            $parcelProduct->Quantity = 1;

            $parcel->ParcelProducts = new stdClass();
            $parcel->ParcelProducts->parcelProduct = $parcelProduct;

            $parcelProduct = new stdClass();
            $parcelProduct->parcelProduct = new stdClass();
            $parcelProduct->parcelProduct->Code = $this->local_id;
            $parcelProduct->parcelProduct->Quantity = 1;

            $data->parameterIn->Parcels[] = $parcel;
        }

        $data->parameterIn->Products = new stdClass();
        $data->parameterIn->Products->product = $product;
        $data->parameterIn->SumOfDimensions = 10*10*10*array_sum($dimensions) * $this->packages_number;
//        $data->parameterIn->TotalPackagingWeight = $this->package_weight;

        $resp = $this->_soapOperation('DespatchParcel', $data);

        if ($resp->success == true) {
            $return = [];

            for($i = 0; $i < $this->packages_number; $i++)
            {
                if($this->packages_number == 1) {
                    $ttNumber = $resp->data->DespatchParcelResult->Parcels->parcelResponse->ParcelUPI;
                    $labelBinary = $resp->data->DespatchParcelResult->RawPrinterData->base64Binary;
                }
                else {
                    $ttNumber = $resp->data->DespatchParcelResult->Parcels->parcelResponse[$i]->ParcelUPI;
                    $labelBinary = $resp->data->DespatchParcelResult->RawPrinterData->base64Binary[$i];
                }


                $im = ImagickMine::newInstance();
                $im->setResolution(300, 300);

                $im->readImageBlob($labelBinary);
                $im->setImageFormat('png8');
                if ($im->getImageAlphaChannel()) {
                    $im->setImageBackgroundColor('white');
                    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                }
//                $im->trimImage(0);
                $im->stripImage();


                $return[] = GlobalOperatorResponse::createSuccessResponse($im->getImageBlob(), $ttNumber);
            }

            return $return;
        }
        else {
            if ($returnError == true) {
                return GlobalOperatorResponse::createErrorResponse($resp->error);
            } else {
                return false;
            }
        }
    }

    protected function _soapOperation($functionName, stdClass $data)
    {
        $mode = array
        (
            'soap_version' => SOAP_1_1,  // use soap 1.1 client
            'trace' => 1,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
            'exceptions' => true
        );

        $return = new stdClass();
        $return->success = false;


        MyDump::dump('whistl.txt', 'REQUEST: '.print_r($data,1));

        try {

            $client = new SoapClient(self::URL , $mode);
            $resp = $client->__soapCall($functionName, [$functionName => $data]);

            MyDump::dump('whistl.txt', 'REQUEST (SOAP): '.print_r($client->__getLastRequest(),1));
            MyDump::dump('whistl.txt', 'RESPONSE: '.print_r($resp,1));

        } catch (SoapFault $E) {
            MyDump::dump('whistl.txt', 'REQUEST (SOAP): '.print_r($client->__getLastRequest(),1));
            MyDump::dump('whistl.txt', 'ERROR: '.print_r($E,1));

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;
            return $return;

        } catch (Exception $E) {
            MyDump::dump('whistl.txt', 'REQUEST (SOAP): '.print_r($client->__getLastRequest(),1));
            MyDump::dump('whistl.txt', 'ERROR: '.print_r($E,1));

            $return->error = $E->faultcode . '(' . $E->faultstring . ')';
            $return->errorCode = $E->faultcode;
            $return->errorDesc = $E->faultstring;
            return $return;
        }

        if($resp->DespatchParcelResult->ErrorMessage != '')
        {
            $return->error = $resp->DespatchParcelResult->ErrorMessage;
            $return->errorCode = $resp->DespatchParcelResult->ErrorMessage;
            $return->errorDesc = $resp->DespatchParcelResult->ErrorMessage;
            return $return;
        }

        $return->success = true;
        $return->data = $resp;

        return $return;
    }

    public static function orderForCourierInternal(CourierTypeInternal $courierInternal, AddressData $from, AddressData $to, $uniq_id, CourierLabelNew $courierLabelNew, $blockPickup, $returnErrors)
    {

        $model = new self;
        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(false);
        $model->receiver_country = $to->country0->code;

        $model->package_weight = $courierInternal->courier->getWeight(true);

        $model->package_size_l = $courierInternal->courier->package_size_l;
        $model->package_size_d = $courierInternal->courier->package_size_d;
        $model->package_size_w = $courierInternal->courier->package_size_w;

        $model->package_content = $courierInternal->courier->package_content;
        $model->local_id = $courierInternal->courier->local_id;

        $model->packages_number = $courierInternal->courier->packages_number;
        $model->package_value = $courierInternal->courier->getPackageValueConverted('GBP');

//        $model->business = $to->company != '' ? true : false;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_POD))
            $model->pod = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_FRAGILE))
            $model->fragile = true;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_AEROSOL))
            $model->aerosol = true;

        $model->service = self::SERVICE_72; // default service
        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_SERVICE_24))
            $model->service = self::SERVICE_24;

        if($courierInternal->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_SERVICE_48))
            $model->service = self::SERVICE_48;

        return $model->createShipment($returnErrors);
    }

    public static function orderForCourierU(CourierTypeU $courierU, CourierLabelNew $cln, $returnError = false)
    {

        $from = $courierU->courier->senderAddressData;
        $to = $courierU->courier->receiverAddressData;

        $model = new self;

        $model->sender_country = $from->country0->code;

        $model->receiver_name = $to->name;
        $model->receiver_company = $to->company;
        $model->receiver_zip_code = $to->zip_code;
        $model->receiver_city = $to->city;
        $model->receiver_address1 = $to->address_line_1;
        $model->receiver_address2 = $to->address_line_2;
        $model->receiver_tel = $to->tel;
        $model->receiver_email = $to->fetchEmailAddress(false);
        $model->receiver_country = $to->country0->code;

        $model->package_weight = $courierU->courier->getWeight(true);

        $model->package_size_l = $courierU->courier->package_size_l;
        $model->package_size_d = $courierU->courier->package_size_d;
        $model->package_size_w = $courierU->courier->package_size_w;

        $model->package_content = $courierU->courier->package_content;
        $model->local_id = $courierU->courier->local_id;

        $model->packages_number = $courierU->courier->packages_number;
        $model->package_value = $courierU->courier->getPackageValueConverted('GBP');

//        $model->business = $to->company != '' ? true : false;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_POD))
            $model->pod = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_FRAGILE))
            $model->fragile = true;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_AEROSOL))
            $model->aerosol = true;

        $model->service = self::SERVICE_72; // default service
        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_SERVICE_24))
            $model->service = self::SERVICE_24;

        if($courierU->hasAdditionByUniqId(GLOBAL_BROKERS::ADDITION_UNIQID_WHISTL_SERVICE_48))
            $model->service = self::SERVICE_48;


        return $model->createShipment($returnError);
    }


    public static function updateTtData($justReturnData = false)
    {
        $CACHE_NAME = 'WHISTL_UPDATE_TT_FTP';
        $DUMP_FILE_NAME = 'whistl_tt_log.txt';

        MyDump::dump($DUMP_FILE_NAME, 'START');

        if(!$justReturnData) {
//            // prevent running mechanism too often
            $flag = Yii::app()->cache->get($CACHE_NAME);
            if ($flag) {
                MyDump::dump($DUMP_FILE_NAME, 'LOCKED!');
                return true;
            }
//
            Yii::app()->cache->set($CACHE_NAME, true, 60 * 15);
        }

        MyDump::dump($DUMP_FILE_NAME, ' FTP...');

        $conn_id = ftp_connect(self::FTP_ADDRESS);
        if(!ftp_login($conn_id, self::FTP_LOGIN, self::FTP_PASSWORD))
        {
            Yii::log('WHISTL TT FTP LOGIN ERROR!', CLogger::LEVEL_ERROR);
            return false;
        }

        $contents = ftp_nlist($conn_id, self::FTP_DIR);

        if(!S_Useful::sizeof($contents))
        {
            Yii::log('WHISTL TT FTP FILES NOT FOUND!', CLogger::LEVEL_ERROR);
            return false;
        }

        $ttData = [];
        if(is_array($contents))
            foreach($contents AS $file)
            {
                $array = explode('.', $file);
                $extension = end($array);

                $file = self::FTP_DIR.'/'.$file;

                if(strcasecmp($extension, 'xml') == 0)
                {
                    if(!TtFileProcessingLog::isProcessed(GLOBAL_BROKERS::BROKER_WHISTL, $file))
                    {
                        // new file found - lock for 3 hours
                        Yii::app()->cache->set($CACHE_NAME, true, 60 * 60 * 3);
//                        file_put_contents($DUMP_FILE_NAME, date('Y-m-d H:i:s').' FOUND NEW FILE: '.$file."\r\n", FILE_APPEND);
                        MyDump::dump($DUMP_FILE_NAME, ' FOUND NEW FILE: '.$file);

                        ob_start();
                        $result = ftp_get($conn_id, "php://output", $file, FTP_BINARY);
                        $data = ob_get_contents();
                        ob_end_clean();

                        $data = explode(PHP_EOL, $data);

                        $first = true;
                        foreach($data AS $line)
                        {
                            if($first)
                            {
                                $first = false;
                                continue;
                            }

                            $item = explode(',', $line);

                            $trackingId = $item[3];
                            $status = $item[1];
                            $date = $item[2];

                            if(!is_array($ttData[$trackingId]))
                                $ttData[$trackingId] = [];

                            $ttData[$trackingId][] = [
                                'name' => $status,
                                'date' => $date,
                                'location' => NULL,
                            ];
                        }

                        if(!$justReturnData)
                            TtFileProcessingLog::markAsProcessed(GLOBAL_BROKERS::BROKER_WHISTL, $file);
                    }
                }
            }

//        file_put_contents($DUMP_FILE_NAME, date('Y-m-d H:i:s').' DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData)."\r\n", FILE_APPEND);
        MyDump::dump($DUMP_FILE_NAME, 'DATA FOR PACKAGES NUMBER: '.S_Useful::sizeof($ttData));

        if($justReturnData)
            return $ttData;

        CourierExternalManager::groupUpdateTt($ttData, GLOBAL_BROKERS::BROKER_WHISTL);

    }

    public static function track($no, CourierLabelNew $courierLabelNew)
    {

        // method only calls updating method which goes another way round - makes updates for all new stats found
        self::updateTtData();

        return false;
    }

}