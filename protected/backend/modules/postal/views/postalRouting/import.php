<?php
/* @var $data array */
/* @var $countriesModels array */
?>

<h1>Import postal routing</h1>


<div class="form">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'postal-routing-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ));
    ?>

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <?php
    $error = false;

    $listOFOperatorsReg = CHtml::listData(PostalOperator::getModels(Postal::POSTAL_TYPE_REG), 'id', 'nameWithBroker');
    $listOFOperatorsNonReg = CHtml::listData(PostalOperator::getModels(false), 'id', 'nameWithBroker');

    foreach($countriesModels AS $country_id => $models):

        $countryModel = CountryGroup::model()->findByPk($country_id);

        echo '<h2>'.$countryModel->id.') '.$countryModel->name.'</h2>';

        ?>

        <table class="table table-striped table-condensed table-bordered">
            <tr>
                <th>Type</th>
                <th>Size class</th>
                <th>Weight class</th>
                <th>Operator</th>
                <th>Price</th>
            </tr>


            <?php
            foreach(Postal::getPostalTypeList() AS $typeClass => $typeClassName):
                ?>

                <tr>
                    <th rowspan="<?= 1 + (Postal::countWeightClassesForSizeClass(Postal::SIZE_CLASS_1) + 1) + (Postal::countWeightClassesForSizeClass(Postal::SIZE_CLASS_2) + 1) + (Postal::countWeightClassesForSizeClass(Postal::SIZE_CLASS_3) + 1);?>"><?= $typeClassName;?></th>
                </tr>

                <?php
                foreach(Postal::getSizeClassList() AS $sizeClass => $sizeClassName):
                    ?>
                    <tr>
                        <th rowspan="<?= Postal::countWeightClassesForSizeClass($sizeClass) + 1; ?>"><?= $sizeClassName;?></th>
                    </tr>

                    <?php
                    foreach(Postal::getWeightClassList() AS $weightClass => $weightClassName):

                        if(Postal::getWeightClassValueByClass($weightClass) > Postal::getMaxWeightForSizeClass($sizeClass))
                            continue;

                        if($models[$typeClass][$sizeClass][$weightClass]->hasErrors())
                            $error = true;
                        ?>
                        <tr>
                            <th><?= $weightClassName;?></th>
                            <td>
                                <?php
                                $operatorName = false;
                                $operatorId = $models[$typeClass][$sizeClass][$weightClass]->postal_operator_id;
                                if($typeClass == Postal::POSTAL_TYPE_REG)
                                    $operatorName = isset($listOFOperatorsReg[$operatorId]) ? $listOFOperatorsReg[$operatorId] : false;
                                else
                                    $operatorName = isset($listOFOperatorsNonReg[$operatorId]) ? $listOFOperatorsNonReg[$operatorId] : false;
                                ?>
                                <?= $operatorName; ?>
                                <?php //echo $form->dropDownList($models[$typeClass][$sizeClass][$weightClass], '['.$country_id.']['.$typeClass.']['.$sizeClass.']['.$weightClass.']postal_operator_id',, ['prompt' => '-', 'data-postal-type-operator' => $typeClass, 'disabled' => true]);?><br/>


                                <?= $form->error($models[$typeClass][$sizeClass][$weightClass], '['.$country_id.']['.$typeClass.']['.$sizeClass.']['.$weightClass.']postal_operator_id');?>
                            </td>
                            <td>
                                <?php $this->widget('PriceForm', array(
                                    'form' => $form,
                                    'model' => $models[$typeClass][$sizeClass][$weightClass]->price,
                                    'formFieldPrefix' => '['.$country_id.']['.$typeClass.']['.$sizeClass.']['.$weightClass.']',
                                    'vertical' => false,
                                    'readonly' => true
                                ));?>
                                <?= $form->error($models[$typeClass][$sizeClass][$weightClass], 'price_id'); ?>
                            </td>
                        </tr>

                    <?php
                    endforeach;
                    ?>

                <?php
                endforeach;
                ?>

            <?php
            endforeach;
            ?>

        </table>
    <?php
    endforeach;
    ?>

    <?php
    if($error):
        ?>
        <div class="alert alert-danger text-center">You need to correct all errors in file and reupload it!</div>
    <?php
    else:
        ?>

        <div class="alert alert-warning text-center">File is ok! Make sure you know what you are doing!</div>


        <div class="text-center">

            <?= CHtml::dropDownList('confirmation', '', [0 => 'I do not know', 1 => 'I am sure']);?>



        </div>
        <br/>
        <br/>
        <br/>
        <div class="text-center">
            <?php echo CHtml::submitButton('Save data', ['class' => 'btn btn-xl btn-danger', 'name' => 'confirm-import']); ?>
        </div>

    <?php
    endif;
    ?>

    <?php
    $this->endWidget();
    ?>

</div>
