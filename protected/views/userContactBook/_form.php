<?php
/* @var $model UserContactBook */
/* @var $model2 AddressData */
?>

<div class="form">


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-contact-book-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
    ));
    ?>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->errorSummary($model2); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model, 'type', UserContactBook::getTypes_enum(), ['data-type' => 'true']); ?>
		<?php echo $form->error($model,'type'); ?>
    </div><!-- row -->

    <?php $this->renderPartial('/_addressData/_form_base',
        array('model' => $model2, 'form' => $form, 'bankAccountEnabled' => true));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>

    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app', 'Save'));
        $this->endWidget();
        ?></div>
</div><!-- form -->
<?php $this->widget('BackLink');?>
<script>
    $(document).ready(function(){

        $('[data-type]').on('change', bankAccountEnable);
        bankAccountEnable();
    });

    function bankAccountEnable()
    {
        if($('[data-type]').val() == '<?= UserContactBook::TYPE_SENDER;?>')
            $('[data-bank-accound-enabled]').show();
        else
            $('[data-bank-accound-enabled]').hide();
    }
</script>
