<?php

Yii::import('application.modules.courier.models._base.BaseCourierCountryGroup');

class CourierCountryGroup extends BaseCourierCountryGroup
{
    public $__pickup_available;

    public $_is_for_pickup;

    const SCENARIO_CUSTOM_GROUP = 'customGroup';

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function scopes()
    {
        return array(
            // without sbugroups
            'primary' => [
                'condition' => 'parent_courier_country_group_id IS NULL',
            ],
            'alphhabetical' => [
                'order' => 'name ASC',
            ],
        );
    }

    public function rules() {
        return array(
            array('date_entered', 'default',
                'value'=>new CDbExpression('NOW()'), 'on'=>'insert'),
            array('date_updated', 'default',
                'value'=>new CDbExpression('NOW()'), 'setOnEmpty' => false,  'on'=>'update'),
            array('stat', 'default',
                'value'=> S_Status::ACTIVE, 'on'=>'insert'),

            array('delivery_operator', 'in', 'range' => CHtml::listData(CourierOperator::model()->findAll(),'id', 'id')),
            array('pickup_operator', 'in', 'range' => CHtml::listData(CourierOperator::model()->findAll(),'id', 'id')),
            array('pickup_hub', 'in', 'range' => CHtml::listData(CourierHub::model()->findAll(),'id', 'id')),
            array('delivery_hub', 'in', 'range' => CHtml::listData(CourierHub::model()->findAll(),'id', 'id')),

            array('delivery_operator', 'required'),
            array('name, delivery_hub', 'required', 'except' => self::SCENARIO_CUSTOM_GROUP),
            array('pickup_hub, pickup_operator, delivery_hub, delivery_operator', 'numerical', 'integerOnly'=>true),
            array('name, stat', 'length', 'max'=>45),
            array('date_updated, __pickup_available', 'safe'),
            array('date_updated, pickup_hub, pickup_operator', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, date_entered, date_updated, pickup_hub, pickup_operator, delivery_hub, delivery_operator, stat,
            _is_for_pickup,
            ', 'safe', 'on'=>'search'),
        );
    }

    public static function getPickupHub_UserGroup($id,$package_weight, $user_group_id = false)
    {
        // no user group provided
        if(!$user_group_id)
            return false;

        // search for group model
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, pickup_hub');
        $cmd->from('courier_country_group');
        $cmd->where('parent_courier_country_group_id=:id AND user_group_id = :user_group', array(':id' => $id, ':user_group' => $user_group_id));
        $cmd->limit(1);
        $result = $cmd->queryRow();

        // group model not found
        if(!$result)
            return false;

        $groupModelHub = $result['pickup_hub'];

        $groupModelId = $result['id'];

        // search for custom hub weights on group model
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier_hub');
        $cmd->from('courier_country_group_operator');
        $cmd->where('courier_country_group=:id ',array(':id' => $groupModelId));
        $cmd->andWhere('weight_from <= :weight AND weight_to >= :weight', [':weight' => $package_weight]);
        $cmd->andWhere('type = :type', [':type' => CourierCountryGroupOperator::TYPE_PICKUP]);
        $cmd->order(CourierCountryGroupOperator::DEFAULT_ORDER);
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        // if custom operator not found and group model found, get data from group model
        if($result)
            return $result;
        else
            return $groupModelHub;
    }


    /**
     * Method returns Pickup Hub for particular country group
     *
     * @param $id integer Country group ID
     * @param bool|false $returnModel Whether to return model or just ID
     * @return integer|false|CourierHub
     */
    public static function getPickupHub($id, $package_weight, $user_group_id, $returnModel = false)
    {
        $result = self::getPickupHub_UserGroup($id, $package_weight, $user_group_id);

        // if hub for user group not found
        if(!$result) {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('courier_hub');
            $cmd->from('courier_country_group_operator');
            $cmd->where('courier_country_group=:id', array(':id' => $id));
            $cmd->andWhere('weight_from <= :weight AND weight_to >= :weight', [':weight' => $package_weight]);
            $cmd->andWhere('type = :type', [':type' => CourierCountryGroupOperator::TYPE_PICKUP]);
            $cmd->order(CourierCountryGroupOperator::DEFAULT_ORDER);
            $cmd->limit(1);
            $result = $cmd->queryScalar();

            // If custom hub for given weight is not found, get default hub for this country group
            if (!$result) {
                $cmd = Yii::app()->db->createCommand();
                $cmd->select('pickup_hub');
                $cmd->from('courier_country_group');
                $cmd->where('id=:id', array(':id' => $id));
                $cmd->limit(1);
                $result = $cmd->queryScalar();
            }

        }

        if($returnModel)
            $result = CourierHub::model()->findByPk($result);


        return $result;

//
//
//        $cmd = Yii::app()->db->createCommand();
//        $cmd->select('pickup_hub');
//        $cmd->from('courier_country_group');
//        $cmd->where('id=:id',array(':id' => $id));
//        $result = $cmd->queryScalar();
//
//        if($returnModel)
//            $result = CourierHub::model()->findByPk($result);
//
//        return $result;
    }

    /**
     * Returns pickup operator for user group.
     * @param $id
     * @param $package_weight
     * @param $user_group_id
     * @return int
     */
    protected static function getPickupOperator_UserGroup($id, $package_weight, $user_group_id)
    {
        // no user group provided
        if(!$user_group_id)
            return false;

        // search for group model
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, pickup_operator');
        $cmd->from('courier_country_group');
        $cmd->where('parent_courier_country_group_id=:id AND user_group_id = :user_group', array(':id' => $id, ':user_group' => $user_group_id));
        $cmd->limit(1);
        $result = $cmd->queryRow();

        // group model not found
        if(!$result)
            return false;

        $groupModelOperator = $result['pickup_operator'];

        $groupModelId = $result['id'];

        // search for custom operator weights on group model
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier_operator');
        $cmd->from('courier_country_group_operator');
        $cmd->where('courier_country_group=:id ',array(':id' => $groupModelId));
        $cmd->andWhere('weight_from <= :weight AND weight_to >= :weight', [':weight' => $package_weight]);
        $cmd->andWhere('type = :type', [':type' => CourierCountryGroupOperator::TYPE_PICKUP]);
        $cmd->order(CourierCountryGroupOperator::DEFAULT_ORDER);
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        // if custom operator not found and group model found, get data from group model
        if($result)
            return $result;
        else
            return $groupModelOperator;
    }

    /**
     * Method returns Pickup Operator for particular country group
     *
     * @param $id integer Country group ID
     * @param $package_weight float Weight of package
     * @param bool|false $returnModel Whether to return model or just ID
     * @return integer|false|CourierOperator
     */
    protected $_pickupOperator = [];
    public static function getPickupOperator($id, $package_weight, $returnModel = false, $user_group_id = false, $overrideOperatorUniqId = false)
    {
        if($overrideOperatorUniqId)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id');
            $cmd->from('courier_operator');
            $cmd->where('uniq_id=:id', array(':id' => $overrideOperatorUniqId));
            $cmd->limit(1);
            $result = $cmd->queryScalar();

            if($returnModel)
                $result = CourierOperator::model()->findByPk($result);

            return $result;
        }


        $result = self::getPickupOperator_UserGroup($id, $package_weight, $user_group_id);

        // if pickup for user group not found
        if(!$result) {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('courier_operator');
            $cmd->from('courier_country_group_operator');
            $cmd->where('courier_country_group=:id', array(':id' => $id));
            $cmd->andWhere('weight_from <= :weight AND weight_to >= :weight', [':weight' => $package_weight]);
            $cmd->andWhere('type = :type', [':type' => CourierCountryGroupOperator::TYPE_PICKUP]);
            $cmd->order(CourierCountryGroupOperator::DEFAULT_ORDER);
            $cmd->limit(1);
            $result = $cmd->queryScalar();

            // If custom operator for given weight is not found, get default operator for this country group
            if (!$result) {
                $cmd = Yii::app()->db->createCommand();
                $cmd->select('pickup_operator');
                $cmd->from('courier_country_group');
                $cmd->where('id=:id', array(':id' => $id));
                $cmd->limit(1);
                $result = $cmd->queryScalar();
            }
        }


        if($returnModel)
            $result = CourierOperator::model()->findByPk($result);

        return $result;
    }

    public static function getDeliveryHub_UserGroup($id,$package_weight, $user_group_id = false)
    {
        // no user group provided
        if(!$user_group_id)
            return false;

        // search for group model
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, delivery_hub');
        $cmd->from('courier_country_group');
        $cmd->where('parent_courier_country_group_id=:id AND user_group_id = :user_group', array(':id' => $id, ':user_group' => $user_group_id));
        $cmd->limit(1);
        $result = $cmd->queryRow();

        // group model not found
        if(!$result)
            return false;

        $groupModelHub = $result['delivery_hub'];

        $groupModelId = $result['id'];

        // search for custom hub weights on group model
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier_hub');
        $cmd->from('courier_country_group_operator');
        $cmd->where('courier_country_group=:id ',array(':id' => $groupModelId));
        $cmd->andWhere('weight_from <= :weight AND weight_to >= :weight', [':weight' => $package_weight]);
        $cmd->andWhere('type = :type', [':type' => CourierCountryGroupOperator::TYPE_DELIVERY]);
        $cmd->order(CourierCountryGroupOperator::DEFAULT_ORDER);
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        // if custom operator not found and group model found, get data from group model
        if($result)
            return $result;
        else
            return $groupModelHub;
    }

    /**
     * Method returns Delivery Hub for particular country group
     *
     * @param $id integer Country group ID
     * @param bool|false $returnModel Whether to return model or just ID
     * @return integer|false|CourierHub
     */
    public static function getDeliveryHub($id, $returnModel = false, $package_weight, $user_group_id = false)
    {

        $result = self::getDeliveryHub_UserGroup($id, $package_weight, $user_group_id);

        // if hub for user group not found
        if(!$result) {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('courier_hub');
            $cmd->from('courier_country_group_operator');
            $cmd->where('courier_country_group=:id', array(':id' => $id));
            $cmd->andWhere('weight_from <= :weight AND weight_to >= :weight', [':weight' => $package_weight]);
            $cmd->andWhere('type = :type', [':type' => CourierCountryGroupOperator::TYPE_DELIVERY]);
            $cmd->order(CourierCountryGroupOperator::DEFAULT_ORDER);
            $cmd->limit(1);
            $result = $cmd->queryScalar();

            // If custom hub for given weight is not found, get default hub for this country group
            if (!$result) {
                $cmd = Yii::app()->db->createCommand();
                $cmd->select('delivery_hub');
                $cmd->from('courier_country_group');
                $cmd->where('id=:id', array(':id' => $id));
                $cmd->limit(1);
                $result = $cmd->queryScalar();
            }

        }

        if($returnModel)
            $result = CourierHub::model()->findByPk($result);


        return $result;
    }

    protected static function getDeliveryOperator_UserGroup($id, $package_weight, $user_group_id)
    {
        // no user group provided
        if(!$user_group_id)
            return false;

        // search for group model
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id, delivery_operator');
        $cmd->from('courier_country_group');
        $cmd->where('parent_courier_country_group_id=:id AND user_group_id = :user_group', array(':id' => $id, ':user_group' => $user_group_id));
        $cmd->limit(1);
        $result = $cmd->queryRow();

        // group model not found
        if(!$result)
            return false;

        $groupModelOperator = $result['delivery_operator'];

        $groupModelId = $result['id'];

        // search for custom operator weights on group model
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('courier_operator');
        $cmd->from('courier_country_group_operator');
        $cmd->where('courier_country_group=:id ',array(':id' => $groupModelId));
        $cmd->andWhere('weight_from <= :weight AND weight_to >= :weight', [':weight' => $package_weight]);
        $cmd->andWhere('type = :type', [':type' => CourierCountryGroupOperator::TYPE_DELIVERY]);
        $cmd->order(CourierCountryGroupOperator::DEFAULT_ORDER);
        $cmd->limit(1);
        $result = $cmd->queryScalar();

        // if custom operator not found and group model found, get data from group model
        if($result)
            return $result;
        else
            return $groupModelOperator;
    }

    /**
     * Method returns Delivery Operator for particular country group
     *
     * @param $id integer Country group ID
     * @param $package_weight float Weight of package
     * @param bool|false $returnModel Whether to return model or just ID
     * @return integer|false|CourierOperator
     */
    protected $_deliveryOperator = [];
    public static function getDeliveryOperator($id, $package_weight, $returnModel = false, $user_group_id = false, $overrideOperatorUniqId = false)
    {
        if($overrideOperatorUniqId)
        {
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id');
            $cmd->from('courier_operator');
            $cmd->where('uniq_id=:id', array(':id' => $overrideOperatorUniqId));
            $cmd->limit(1);
            $result = $cmd->queryScalar();

            if($returnModel)
                $result = CourierOperator::model()->findByPk($result);

            return $result;
        }


        $result = self::getDeliveryOperator_UserGroup($id, $package_weight, $user_group_id);

        if(!$result) {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('courier_operator');
            $cmd->from('courier_country_group_operator');
            $cmd->where('courier_country_group=:id', array(':id' => $id));
            $cmd->andWhere('weight_from <= :weight AND weight_to >= :weight', [':weight' => $package_weight]);
            $cmd->andWhere('type = :type', [':type' => CourierCountryGroupOperator::TYPE_DELIVERY]);
            $cmd->order(CourierCountryGroupOperator::DEFAULT_ORDER);
            $cmd->limit(1);
            $result = $cmd->queryScalar();

            // If custom operator for given weight is not found, get default operator for this country group
            if (!$result) {
                $cmd = Yii::app()->db->createCommand();
                $cmd->select('delivery_operator');
                $cmd->from('courier_country_group');
                $cmd->where('id=:id', array(':id' => $id));
                $cmd->limit(1);
                $result = $cmd->queryScalar();
            }
        }

        if($returnModel)
            $result = CourierOperator::model()->findByPk($result);

        return $result;
    }


    public function numberOfDeliveryPricesForHub($hub, $group = false)
    {

        if(!$group)
            $models = $this->courierDeliveryPriceHasHubs(array('condition' => 'courierDeliveryPriceHasHubs.user_group_id IS NULL AND courierDeliveryPriceHasHubs.courier_pickup_hub = :hub', 'params' => array(':hub' => $hub)));
        else
            $models = $this->courierDeliveryPriceHasHubs(array('condition' => 'courierDeliveryPriceHasHubs.user_group_id IS NOT NULL AND courierDeliveryPriceHasHubs.courier_pickup_hub = :hub', 'params' => array(':hub' => $hub)));

        return S_Useful::sizeof($models);
    }

    public function numberOfPickupPrices($group = false)
    {

        if(!$group)
            $models = $this->courierPickupPriceHasGroups(array('condition' => 'courierPickupPriceHasGroups.user_group_id IS NULL'));
        else
            $models = $this->courierPickupPriceHasGroups(array('condition' => 'courierPickupPriceHasGroups.user_group_id IS NOT NULL'));

        return S_Useful::sizeof($models);
    }

    public function isForPickup()
    {
        if($this->pickup_operator != NULL && $this->pickup_hub != NULL)
            return true;
        else
            return false;
    }

    public static function getGroupForCountryId($id)
    {
        $cmd = Yii::app()->db->cache(60*15)->createCommand();
        $cmd->select('courier_group');
        $cmd->from('courier_country_group_has_country');
        $cmd->where('country=:country',array(':country' => $id));
        $result = $cmd->queryScalar();

        return $result;
    }

    public function isAvailableForPickup()
    {
        if($this->pickup_hub != NULL && $this->pickup_operator != NULL)
            return true;
        else
            return false;
    }

    public function loadPickupPrices()
    {
        $models = $this->courierPickupPriceHasGroups;

        return $models;
    }

    public function loadDeliveryPrices()
    {
        $models = $this->courierDeliveryPriceHasHubs;

        return $models;
    }

    public static function loadModelWithPickupAvailableSet($id, $user_group_id = NULL)
    {
        /* @var $model self */
        if($user_group_id === NULL)
            $model = self::model()->findByPk($id);
        else {
            $model = self::model()->findByAttributes(['user_group_id' => $user_group_id, 'parent_courier_country_group_id' => $id]);
            if($model == NULL)
            {
                $parentModel = self::model()->findByPk($id);

                if($parentModel !== NULL && $parentModel->parent_courier_country_group_id === NULL) {

                    $model = new self;
                    $model->parent_courier_country_group_id = $id;
                    $model->user_group_id = $user_group_id;
                    $model->name = '-';
                } else
                    throw new Exception('Create default routing first four country #'.$id);
            }
        }

        if($model == NULL)
            return $model;

        if($model->isPickupAvailable())
            $model->__pickup_available = 1;

        return $model;

    }

    public function isPickupAvailable()
    {
        return ($this->pickup_operator != NULL && $this->pickup_hub != NULL);
    }

    public function saveWithCountries(Array $countryList = array(), $ownTransaction = true, CourierCountryGroupOperator $modelDeliveryNew = NULL, CourierCountryGroupOperator $modelPickupNew = NULL, $deleteList = [])
    {

        if($this->user_group_id === NULL && $this->__pickup_available != 1 OR $this->user_group_id !== NULL && !$this->courierCountryGroupCustomParent->isPickupAvailable())
        {
            $this->pickup_operator = NULL;
            $this->pickup_hub = NULL;
        }

        if($ownTransaction)
            $transaction = Yii::app()->db->beginTransaction();
        $errors = false;

        if(!$this->save())
            $errors = true;

        if($modelDeliveryNew->operator > 0) {
            $modelDeliveryNew->courier_country_group = $this->id;
            if (!$modelDeliveryNew->save())
                $errors = true;
        }


        if($modelPickupNew->operator > 0)
        {
            $modelPickupNew->courier_country_group = $this->id;
            if(!$modelPickupNew->save())
                $errors = true;
        }


        if(!$errors)
        {
            $cmd = Yii::app()->db->createCommand();

            if(is_array($deleteList))
                foreach($deleteList AS $key => $item)
                    $cmd->delete('courier_country_group_operator','`id`=:id', array(':id' => $key));


            $cmd->delete('courier_country_group_has_country','`courier_group`=:group', array(':group' => $this->id));

            /* @var $item CountryList */
            foreach($countryList AS $item)
            {
                $cmd->insert('courier_country_group_has_country',array('courier_group' => $this->id, 'country' => $item));
            }
        }

        if(!$errors)
        {
            if($ownTransaction)
                $transaction->commit();
            return true;
        } else {
            if($ownTransaction)
                $transaction->rollBack();
            return false;
        }
    }

    public function deleteWithOperators()
    {
        $transaction = Yii::app()->db->beginTransaction();

        try
        {
            foreach($this->courierCountryGroupOperatorPickup AS $item)
                $item->delete();

            foreach($this->courierCountryGroupOperatorDelivery AS $item)
                $item->delete();

            $this->delete();

            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('courier_country_group_operator','`courier_country_group`=:group', array(':group' => $this->id));
        }
        catch(Exception $ex)
        {
            $transaction->rollback();
            return false;
        }

        $transaction->commit();
        return true;
    }

    public static function listUngroupedCountries()
    {
        $models = CountryList::model()->findAll('t.id NOT IN (SELECT country FROM courier_country_group_has_country)');

        return $models;
    }

    public function listUngroupedCountriesAndMine()
    {
        // DELETE ASSIGNMENT COUNTRIES TO NON-EXISTING GROUPS
        $cmd = Yii::app()->db->createCommand('DELETE FROM courier_country_group_has_country WHERE courier_group NOT IN (SELECT id FROM courier_country_group)');
        $cmd->execute();


        if($this->id != NULL)
            $models = CountryList::model()->findAll('t.id NOT IN (SELECT country FROM courier_country_group_has_country WHERE `courier_group` != :group)', array(':group' => $this->id));
        else
            $models = CountryList::model()->findAll('t.id NOT IN (SELECT country FROM courier_country_group_has_country)', array());

        return $models;
    }

    public function listCountriesForGroup($asArrayOfId = false)
    {
        $models =  CountryList::model()->findAll(array('condition' => 't.id IN (SELECT country FROM courier_country_group_has_country WHERE `courier_group` = :group)', 'params' => array(':group' => $this->id)));


        if($asArrayOfId)
        {
            $array = [];
            foreach($models AS $item)
            {
                array_push($array, $item->id);
            }

            $models = $array;
        }

        return $models;
    }
}