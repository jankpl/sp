<?php

class CourierCarrierController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
              array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierCarrier'),
        ));
    }

    public function actionCreate() {
        $model = new CourierCarrier;

        $this->performAjaxValidation($model, 'courier-carrier-form');

        if (isset($_POST['CourierCarrier'])) {
            $model->setAttributes($_POST['CourierCarrier']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array( 'model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'CourierCarrier');

        $this->performAjaxValidation($model, 'courier-carrier-form');

        if (isset($_POST['CourierCarrier'])) {
            $model->setAttributes($_POST['CourierCarrier']);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $this->loadModel($id, 'CourierCarrier')->delete();
            }
            catch (Exception $ex)
            {
                throw new CHttpException(403, "Nie można usunąć tego wpisu!");
            }
            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierCarrier('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierCarrier']))
            $model->setAttributes($_GET['CourierCarrier']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}