<div class="form">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'returns-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->urlManager->createUrl('/user/update', ['#' => 'returns']),
    ));
    ?>

    <h3><?php echo Yii::t('user', 'Zwroty');?></h3>
<p><?= Yii::t('user', 'Domyślnie generuj etykiety zwrotne dla adresatów z krajów:');?></p>
    <table class="table" style="max-width: 500px;">
        <?php
        foreach(CountryList::model()->with('countryListTr')->findAllByAttributes(['id' => CourierTypeReturn::getAvailableAutomaticCoutryList()], ['order' => 'countryListTr.name ASC']) AS $key => $item):
            ?>
            <tr>
                <td><?= $item->trName;?></td>
                <td><?= CHtml::checkBox('returnPackageAutomatic['.$item->id.']', $model->getReturnPackageAutomatic($item->id));?></td>
            </tr>
        <?php
        endforeach;
        ?>
        <tr>
            <td colspan="2"><?php echo TbHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'saveReturnPackageAutomatic'));?></td>
        </tr>
    </table>

    <?php
    $this->endWidget();
    ?>

</div>