<div class="alert alert-danger">
    <h4><?php echo Yii::t('postal','Reklamuj paczkę');?></h4>
    <br/>
    <?php echo CHtml::link(Yii::t('postal','Reklamuj'), array('/user/complaint', "hash" => $model->hash, "type" => UserComplaintForm::WHAT_POSTAL), array('class' => 'btn',));?>
</div>
