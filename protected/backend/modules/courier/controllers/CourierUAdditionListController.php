<?php

class CourierUAdditionListController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierUAdditionList'),
        ));
    }

    public function actionCreate() {

        $model = new CourierUAdditionList;
        $model->price = new Price();
        $model->stat = 0;
        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $additionTr = new CourierUAdditionListTr();
            $additionTr->language_id = $item->id;
            $modelTrs[$item->id] = $additionTr;
        }

        if (isset($_POST['CourierUAdditionList'])) {
            $model->setAttributes($_POST['CourierUAdditionList']);
            $model->price = Price::generateByPost($_POST['PriceValue']);

            foreach($_POST['CourierUAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierUAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierUAdditionListTrs = $modelTrs;

            if ($model->saveWithTrAndPrice()) {
                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('create', array(
            'model' => $model,
            'modelTrs' => $modelTrs,

        ));
    }

    public function actionUpdate($id) {

        /* @var $model CourierUAdditionList */

        $model = $this->loadModel($id, 'CourierUAdditionList');

        $modelTrs = Array();


        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = CourierUAdditionListTr::model()->find('courier_u_addition_list_id=:courier_u_addition_list_id AND language_id=:language_id', array(':courier_u_addition_list_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $additionTr = new CourierUAdditionListTr();
                $additionTr->language_id = $item->id;
                $additionTr->courier_u_addition_list_id = $id;
                $modelTrs[$item->id] = $additionTr;
            }
        }


        if (isset($_POST['CourierUAdditionList'])) {
            $model->setAttributes($_POST['CourierUAdditionList']);

            $model->price = Price::generateByPost($_POST['PriceValue']);

            foreach($_POST['CourierUAdditionListTr'] AS $key => $item)
            {
                if($modelTrs[$key] === null) $modelTrs[$key] = new CourierUAdditionListTr;
                $modelTrs[$key]->setAttributes($item);
            }

            $model->courierUAdditionListTrs = $modelTrs;


            if ($model->saveWithTrAndPrice()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'modelTrs' => $modelTrs,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model = $this->loadModel($id, 'CourierUAdditionList');
            if($model->broker_id != NULL)
                throw new CHttpException(403,'You cannot delete this item!');

            try
            {
                $model->delete();

            }
            catch(Exception $ex)
            {
                throw new CHttpException(403,'Nie udało się usunąć tego wpisu!');
            }


            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierUAdditionList('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierUAdditionList']))
            $model->setAttributes($_GET['CourierUAdditionList']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionStat($id)
    {
        /* @var $model CourierUAdditionList */


        $model = CourierUAdditionList::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404,'Błędny ID!');

        if($model->toggleStat())
            $this->redirect(Yii::app()->request->urlReferrer);
        //else
        //throw new CHttpException(403,'Operacja nie powiodła się!');
    }

}