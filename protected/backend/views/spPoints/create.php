<?php
/* @var $this SpPointsController */
/* @var $model SpPoints */

$this->breadcrumbs=array(
	'Sp Points'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage SpPoints', 'url'=>array('admin')),
);
?>

<h1>Create SpPoints</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'modelTrs' => $modelTrs)); ?>