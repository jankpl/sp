<?php

/* @var $model Postal */
/* @var $actions String[] */
/* @var $new boolean */

/*$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);*/

$stat = $model->postal_stat_id;
?>

<h2><?php echo Yii::t('postal','Przesyłka #{id}', array('{id}' => $model->local_id));?></h2>

<?php
if($new OR $stat == PostalStat::PACKAGE_PROCESSING):
    ?>
    <div style="min-height: 100px;">
        <div class="alert alert-info text-center" id="label-waiting" style="display: <?= $stat != PostalStat::PACKAGE_PROCESSING ? 'none' : '';?>">
            <br/>
            <strong><?= Yii::t('postal', 'Ta paczka oczekuje na wygenerowanie etykiety.');?></strong>
            <br/>
            <br/>
            <i class="fa fa-clock-o" aria-hidden="true" style="font-size: 4em;" id="clock-animation"></i>
            <br/><br/>
            <?= Yii::t('postal', 'Opcja pobrania etykiety będzie automatycznie dostępna po wygenerowaniu etykiety.');?>
        </div>

        <div class="alert alert-success text-center" id="label-ready" style="display: <?= in_array($stat,[PostalStat::PACKAGE_PROCESSING, PostalStat::NEW_ORDER]) ? 'none' : '';?>">
            <br/>
            <strong><?= Yii::t('postal', 'Etykieta jest gotowa do pobrania!');?></strong>
            <br/>
            <br/>
            <i class="fa fa-check-square-o" style="font-size: 4em;" aria-hidden="true"></i>
        </div>
    </div>
    <?php
    if($stat == PostalStat::PACKAGE_PROCESSING):
        ?>
        <script>
            $(document).ready(function(){
                var $labelActionBlockOverlay = $('#label-action-block .overlay');

                $labelActionBlockOverlay.show();

                var $clock = $('#clock-animation');

                function runIt() {
                    $clock.animate({opacity:'1'}, 1000);
                    $clock.animate({opacity:'0.1'}, 1000, runIt);
                }
                runIt();

                function checkData()
                {
                    $.ajax({
                        url: '<?= Yii::app()->createUrl('/postal/postal/ajaxIsLabelReady');?>',
                        data: { hash: '<?= $model->hash;?>'},
                        method: 'POST',
                        dataType: 'json',
                        success: function(result){
                            console.log(result);
                            if(result)
                            {
                                $('#label-waiting').fadeOut('fast', function(){
                                    $('#label-ready').fadeIn('fast', function(){
                                        $labelActionBlockOverlay.hide();
                                        setTimeout(function(){
                                            location.reload();
                                        },500);

                                    });
                                });


                            } else
                                setTimeout(checkData, 1000);

                        },
                        error: function(e){
                            console.log(e);
                        }
                    });
                }

                checkData();
            });
        </script>
    <?php
    endif;
endif;
?>

<?php

$this->widget('FlashMessages');

//if(Yii::app()->user->hasFlash('package-waiting-for-label'))
//{
//
//    if($model->postal_stat_id == PostalStat::PACKAGE_PROCESSING )
//        echo '<div class="flash-success">' . Yii::app()->user->getFlash('package-waiting-for-label') . "</div>\n";
//    else
//        Yii::app()->user->getFlash('package-waiting-for-label'); // just to clear flash
//}

?>


<?php
if($model->user_cancel == Postal::USER_CANCELL_COMPLAINED)
    echo '<div class="alert alert-warning">'.Yii::t('postal', 'Ta paczka została zgłoszona do reklamacji').'</div>';
?>

<?php
if(S_Useful::sizeof($actions)):
    ?>
    <fieldset>
        <legend><?= Yii::t('postal','Akcje');?></legend>

        <?php
        foreach($actions AS $item):
            ?>
            <div style="width: 350px; display: inline-block; margin: 10px; vertical-align: top;">
                <?php echo $item; ?>
            </div>
            <?php
        endforeach;
        ?>
    </fieldset>
    <?php
endif;
?>

<div class="row">
    <div class="col-xs-12 col-sm-6">

        <h4><?php echo Yii::t('postal','Szczegóły przesyłki'); ?></h4>

        <?php

        $attributes = array(
            array(
                'name' => $model->getAttributeLabel('local_id'),
                'value' => $model->local_id,
            ),
            array(
                'name' => $model->getAttributeLabel('date_entered'),
                'value' => substr($model->date_entered,0,16)
            ),
            array(
                'name' => $model->getAttributeLabel('date_updated'),
                'value' => substr($model->date_entered,0,16)
            ),
            array(
                'name' => $model->getAttributeLabel('stat'),
                'value' => StatMap::getMapNameById($model->stat_map_id),
            ),
//                array(
//                    'name' => $model->getAttributeLabel('stat'),
//                    'value' => $model->location != '' ? $model->stat->postalStatTr->full_text .' ('.$model->location.')' : $model->stat->postalStatTr->full_text,
//                ),
            array(
                'name' => $model->getAttributeLabel('postal_type'),
                'value' => $model->getPostalTypeName(),
            ),
            array(
                'name' => $model->getAttributeLabel('weight_class'),
                'value' => $model->getWeightClassName(),
            ),
            array(
                'name' => $model->getAttributeLabel('weight'),
                'value' => $model->weight.' g',
            ),
            array(
                'name' => $model->getAttributeLabel('size_class'),
                'value' => $model->getSizeClassName(),
            ),
            array(
                'name' => $model->getAttributeLabel('content'),
                'value' => $model->content,
            ),
            array(
                'name' => $model->getAttributeLabel('value'),
                'value' => ($model->value ? $model->value : '-').' '.($model->value ? $model->getValueCurrency() : ''),
            ),
            array(
                'name' => $model->getAttributeLabel('note1'),
                'value' => $model->note1,
            ),
            array(
                'name' => $model->getAttributeLabel('note2'),
                'value' => $model->note2,
            ),
        );

        if($model->postalLabel) {
            $attributes[] = array(
                'name' => Yii::t('postal', 'External ID'),
                'value' => $model->postalOperator->name.' ('.$model->postalLabel->track_id.')',
            );
        }

        if($model->ref)
        {
            $attributes[] = array(
                'name' => $model->getAttributeLabel('ref'),
                'value' => $model->ref,
            );
        }


        if($model->bulk)
        {
            $attributes[] =  array(
                'name' => $model->getAttributeLabel('bulk'),
                'value' => $model->getBulkName(),
            );

            $attributes[] =  array(
                'name' => $model->getAttributeLabel('bulk_number'),
                'value' => $model->bulk_number
            );

            $attributes[] =  array(
                'name' => $model->getAttributeLabel('bulk_id'),
                'value' => $model->bulk_id
            );

            $attributes[] =  array(
                'name' => Yii::t('postal', 'Państwo odbiorcy'),
                'value' => $model->getReceiverCountryName()
            );
        }


        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=> $attributes,
        ));
        ?>

    </div>

</div>


<div class="row">
    <div class="col-xs-12 col-sm-6">
               <?php
        if($model->senderAddressData !== NULL):
        ?>
        <h4><?php echo Yii::t('postal','Nadawca'); ?></h4>
        <?php
            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->senderAddressData,
                ));
        ?>
        <?php
        endif;
        ?>
    </div>
    <div class="col-xs-12 col-sm-6">
        <?php
        if($model->senderAddressData !== NULL):
            ?>
            <h4><?php echo Yii::t('postal','Odbiorca'); ?></h4>
            <?php
            $this->renderPartial('//_addressData/_listInTable',
                array('model' => $model->receiverAddressData,
                ));
            ?>
            <?php
        endif;
        ?>
    </div>
</div>

<?php
if(!$model->isStatHistoryArchived()):
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo Yii::t('postal','Historia');?></h4>

            <?php
            $this->renderPartial('//_statHistory/_statHistory',
                array('model' => $model->postalStatHistoriesVisible));
            ?>
        </div>
    </div>
    <?php
endif;
?>


<?php $this->widget('BackLink');?>
