<?php
class FlashMessages extends CWidget
{

    public function run()
    {
        $messages = Yii::app()->user->getFlashes();
        $this->render('flashMessages', [
            'messages' => $messages
        ]);
    }
}
?>