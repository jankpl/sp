<?php

Yii::import('application.modules.courier.models._base.BaseCourierTypeExternalReturn');

class CourierTypeExternalReturn extends BaseCourierTypeExternalReturn
{

    const SCENARIO_SUMMARY = 'summary';

    public $regulations;
    public $regulations_rodo;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }


    public function attributeLabels()
    {
        return CMap::mergeArray(parent::attributeLabels(), RodoRegulations::attributeLabels());
    }

    public function rules() {
        return array_merge(RodoRegulations::regulationsRules(self::SCENARIO_SUMMARY), array(

//            array('regulations', 'compare', 'compareValue' => true, 'message' => Yii::t('courier', 'Musisz zaakceptować regulamin.'), 'on' => self::SCENARIO_SUMMARY),
//            array('regulations', 'numerical', 'integerOnly' => true),

            array('courier_id, courier_label_new_id', 'numerical', 'integerOnly'=>true),
            array('id, courier_id, courier_label_new_id', 'safe', 'on'=>'search'),
        ));
    }

    public function behaviors(){

        return CMap::mergeArray(parent::behaviors(),
            [
                'bAcceptRegulationsByDefault' =>
                    [
                        'class'=>'application.models.bAcceptRegulationsByDefault'
                    ],
            ]
        );
    }


    public function savePackage()
    {
        $transaction = Yii::app()->db->beginTransaction();
        $errors = false;


        if(!$this->courier->senderAddressData->save())
            $errors = true;

        $this->courier->sender_address_data_id = $this->courier->senderAddressData->id;

        $userAddresData = $this->courier->user->getReturnAddressModel(true);

        $returnAddressData = new CourierTypeExternalReturn_AddressData();
        $returnAddressData->attributes = $userAddresData->attributes;

        if($returnAddressData->name == '' && $returnAddressData->company == '')
            $returnAddressData->name = '???';

        if(!$returnAddressData->save())
            $errors = true;

        $this->courier->receiver_address_data_id = $returnAddressData->id;

        if(!$this->courier->save())
            $errors = true;

        $this->courier_id = $this->courier->id;

        $this->courierLabelNew->courier_id = $this->courier_id;
        $this->courierLabelNew->operator = CourierLabelNew::OPERATOR_OWN_EXTERNAL_RETURN;
        $this->courierLabelNew->stat = CourierLabelNew::STAT_SUCCESS;


        $this->courierLabelNew->setScenario(CourierLabelNew::SCENARIO_NO_ADDRESS);

        if(!$this->courierLabelNew->save())
            $errors = true;

        $this->courier_label_new_id = $this->courierLabelNew->id;

        if(!$this->save())
            $errors = true;

        if($errors)
        {
            $transaction->rollBack();
            return false;
        } else {
            $transaction->commit();
            $this->courierLabelNew->saveExternalReturnLabel($this->generateLabel());

            $this->courier->changeStat(CourierStat::RETURNED_TO_SENDER, 0, NULL, false, false, false, true);

            return true;
        }

    }




    /**
     * Generates Return own label and returns it as Image
     * @param Courier $item
     * @param AddressData|NULL $address

     * @return String
     */
    protected function generateLabel()
    {
        $item = $this->courier;
        $addressDataFrom = $this->courier->senderAddressData;
        $addressDataTo = $this->courier->receiverAddressData;

        $pageW = 100;
        $pageH = 150;


        // GENERATE OWN:
        $pdf = PDFGenerator::initialize();
        $pdf->setFontSubsetting(true);


        $pdf->addPage('H', array($pageW, $pageH));

        $margins = 1;

        /* @var $pdf TCPDF */
        $barcodeStyle = array(
            'position' => '',
            'align' => '',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $blockWidth = $pageW - (2 * $margins);
        $blockHeight = $pageH - (2 * $margins);

        $yBase = 0;
        $xBase = 0;

//        $pdf->AddPage();
        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));


        $pdf->SetFillColor(0,0,0);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFont('freesans', 'B', 23);
        $pdf->MultiCell($blockWidth, 10, Yii::t('returnLabel','RETURN'), 1, 'C', true, 0, $xBase + $margins, 1, true, 0, false, true, 0);
        $pdf->MultiCell($blockWidth, 10, Yii::t('returnLabel','RETURN'), 1, 'C', true, 0, $xBase + $margins, 109, true, 0, false, true, 0);
        $pdf->SetFont('freesans', '', 10);

        $pdf->SetTextColor(0,0,0);





        $pdf->SetFont('freesans', 'B', 8);

        $pdf->MultiCell($blockWidth,5, Yii::t('returnLabel','Nadawca:'), 0, 'L', false, 1, $xBase + $margins, 12, true, 0, false, true, 0);

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(13,10, Yii::t('returnLabel','Konto / Login:'), 0, 'L', false, 1, $xBase + $margins, 16, true, 0, false, true, 10, 'M', true);

        $pdf->SetFont('freesans', '', 14);
        if($item->user_id)
            $pdf->MultiCell(84,10, $item->user->login, 1, 'C', false, 1, 14, 16, true, 0, false, true, 10, 'M', true);

        $pdf->SetFont('freesans', 'B', 8);

        $pdf->MultiCell($blockWidth,5, Yii::t('returnLabel','Zwrot OD:'), 0, 'L', false, 1, $xBase + $margins + 1, 27, true, 0, false, true, 0);

        $returnFrom = $addressDataFrom->name ? $addressDataFrom->name."\r\n" : '';
        $returnFrom .= $addressDataFrom->company ? $addressDataFrom->company."\r\n" : '';
        $returnFrom .= $addressDataFrom->address_line_1 ? $addressDataFrom->address_line_1."\r\n" : '';
        $returnFrom .= $addressDataFrom->address_line_2 ? $addressDataFrom->address_line_2."\r\n" : '';
        $returnFrom .= $addressDataFrom->zip_code ? $addressDataFrom->zip_code."\r\n" : '';
        $returnFrom .= $addressDataFrom->city ? $addressDataFrom->city."\r\n" : '';
        $returnFrom .= $addressDataFrom->country0->name ? $addressDataFrom->country0->name : '';

        $pdf->SetFont('freesans', 'I', 7);
        $pdf->MultiCell(96,20,$returnFrom, 1, 'L', false, 1, $xBase + $margins + 1, 31, true, 0, false, true, 20, '', true);

        $pdf->SetFont('freesans', 'B', 11);
        $pdf->MultiCell($blockWidth,5, Yii::t('returnLabel','Zwrot DO:'), 0, 'L', false, 1, $xBase + $margins + 6, 52, true, 0, false, true, 0);

        $returnTo = $addressDataTo->name ? $addressDataTo->name."\r\n" : '';
        $returnTo .= $addressDataTo->company ? $addressDataTo->company."\r\n" : '';
        $returnTo .= $addressDataTo->address_line_1 ? $addressDataTo->address_line_1."\r\n" : '';
        $returnTo .= $addressDataTo->address_line_2 ? $addressDataTo->address_line_2."\r\n" : '';
        $returnTo .= $addressDataTo->zip_code ? $addressDataTo->zip_code."\r\n" : '';
        $returnTo .= $addressDataTo->city ? $addressDataTo->city."\r\n" : '';
        $returnTo .= $addressDataTo->country0->name ? $addressDataTo->country0->name : '';

        $pdf->SetFont('freesans', '', 10);
        $pdf->SetLineStyle(array('width' => 0.7));
        $pdf->MultiCell(90,23, $returnTo, 1, 'L', false, 1, $xBase + $margins + 6, 58, true, 0, false, true, 23, '', true);
        $pdf->SetLineStyle(array('width' => 0.1));

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(20,4, Yii::t('returnLabel','Data:'), 0, 'L', false, 1, $xBase + $margins, 83, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell(74,4, date('Y-m-d H:i'), 0, 'L', false, 1, 25, 83, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', 'I', 8);
        $pdf->MultiCell(60,4, Yii::t('returnLabel','Powód zwrotu (opcjonalnie):'), 0, 'L', false, 1, $xBase + $margins, 88, true, 0, false, true, 4, '', true);
        $pdf->MultiCell(96,15, '', 1, 'L', false, 1, $xBase + $margins + 1, 93, true, 0, false, true, 4, '', true);
        $pdf->SetFont('freesans', '', 8);

        if($this->courierLabelNew->track_id) {
            $pdf->MultiCell($blockWidth,5, 'external return ID:', 0, 'L', false, 1, $xBase + $margins + 15 , $yBase + 92 + $margins, true, 0, false, true, 5);
            $pdf->write1DBarcode($this->courierLabelNew->track_id, 'C128', $xBase + $margins + 15, $yBase + $margins + 94, '', 15, 0.4, $barcodeStyle, 'N');
        }

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins, $yBase + $margins + 120, $xBase + $blockWidth + $margins, $yBase + $margins + 120);

        $pdf->write1DBarcode($item->local_id, 'C128',  $xBase + $margins + 15, $yBase + $margins + 120, '', 18, 0.4, $barcodeStyle, 'N');

        $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->Line($xBase + $margins, $yBase + $margins + 140, $xBase + $blockWidth + $margins, $yBase + $margins + 140);

        $pdf->SetFont('freesans', '', 8);
        $pdf->MultiCell($blockWidth,10, 'www.swiatprzesylek.pl www.royalshipments.com', 0, 'C', false, 1, $xBase + $margins , $yBase + 140 + $margins, true, 0, false, true, 0);
        $pdf->MultiCell($blockWidth,6, 'Member of KAAB GROUP', 0, 'C', false, 1, $xBase + $margins , $yBase + 144 + $margins, true, 0, false, true, 0);



        $pdf = $pdf->Output('RL_'.$item->local_id.'.pdf', 'S');

        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);
        $im->readImageBlob($pdf);

        $im->setImageFormat('png8');
        if ($im->getImageAlphaChannel())
        {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->stripImage();

        return $im->getImageBlob();
    }

}