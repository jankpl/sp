<?php

class _StatMapMappingGridView extends StatMapMapping
{
    public $_statName;

    public function rules() {

        $array = array(
            array('_statName,
                ','safe','on' => 'search',
            ),
        );

        $array = array_merge(parent::rules(), $array);

        return $array;
    }


    protected function searchCriteria()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.date_entered', $this->date_entered, true);
        $criteria->compare('stat_id', $this->stat_id);
        $criteria->compare('type', $this->type);
        $criteria->compare('map_id', $this->map_id);

        if($this->type == self::TYPE_COURIER)
            $this->with('courierStat');
        else if($this->type == self::TYPE_POSTAL)
            $this->with('postalStat');

        if($this->_statName)
        {
            if($this->type == self::TYPE_COURIER)
                $criteria->compare('courierStat.name', $this->_statName, true);
            else if($this->type == self::TYPE_POSTAL)
                $criteria->compare('postalStat.name', $this->_statName, true);
        }

        return $criteria;
    }

    public function search() {


        $sort = new CSort();
        $sort->defaultOrder = 't.id ASC';

        if($this->type == self::TYPE_COURIER)
            $sort->attributes = array(
                '_statName' => array(
                    'asc' => 'courierStat.name ASC',
                    'desc' => 'courierStat.name DESC',
                ),
                '*', // add all of the other columns as sortable
            );
        else if($this->type == self::TYPE_POSTAL)
            $sort->attributes = array(
                '_statName' => array(
                    'asc' => 'postalStat.name ASC',
                    'desc' => 'postalStat.name DESC',
                ),
                '*', // add all of the other columns as sortable
            );

        return new CActiveDataProvider($this, array(
            'criteria' => $this->searchCriteria(),
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

    }

}