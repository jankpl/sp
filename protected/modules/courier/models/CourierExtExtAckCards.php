<?php

Yii::import('application.modules.courier.models._base.BaseCourierExtAckCards');

class CourierExtExtAckCards extends BaseCourierExtAckCards
{
    const SOURCE_DPDPL = 1;
    const SOURCE_DHL_DE_CUSTOMS = 2;
    const SOURCE_RABEN = 3;


    const STAT_OK = 1;
    const STAT_DELETED = 9;


    const TYPE_ACK = 1;
    const TYPE_CUSTOMS_DEC = 2;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_entered',
                'updateAttribute' => NULL,
            ),
        );
    }


    public function rules() {
        return array(
            array('courier_label_new_id, source, path, stat', 'required'),
            array('courier_label_new_id, source, stat, type', 'numerical', 'integerOnly'=>true),
            array('path', 'length', 'max'=>256),
            array('id, courier_label_new_id, date_entered, source, path, stat, type', 'safe', 'on'=>'search'),
        );
    }

    public static function getAckCardAsPdf(CourierLabelNew $courierLabelNew, $pipelinePdf = false, $type = false)
    {
        if($pipelinePdf)
            $pdf = $pipelinePdf;
        else
            $pdf = PDFGenerator::initialize();

        $pdf->setFontSubsetting(true);

        $pdf->AddPage('P','A4');
        $pdf->Image('@'.self::getAckCardAsString($courierLabelNew, $type),0,0,210,297);

        if($pipelinePdf)
            return $pdf;
        else
            return $pdf->Output('ack_card.pdf', 'D');
    }


    public static function getAckCardAsString(CourierLabelNew $courierLabelNew, $type = false)
    {

        if(!$type)
            $model = self::model()->findByAttributes(['courier_label_new_id' => $courierLabelNew->id]);
        else
            $model = self::model()->findByAttributes(['courier_label_new_id' => $courierLabelNew->id, 'type' => $type]);

        if($model && $model->stat == self::STAT_OK)
            return @file_get_contents($model->path);

        return false;
    }

    public static function saveNewAckCard(CourierLabelNew $courierLabelNew, $image, $source, $type)
    {

        $model = new self;
        $model->source = $source;
        $hash = hash('sha512', $model->source . microtime());

        $path = Yii::app()->basePath . '/../'.'upackcards';

        $path .= '/'.date('Ymd').'/';

        if (!file_exists($path)) {
            mkdir($path, 0777, false);
        }


        $fileName = $path.DIRECTORY_SEPARATOR.$hash.'.png';

        if($source == self::SOURCE_DPDPL) {
            self::_saveDpdPl($image, $fileName);
            $model->type = $type;
        }
        else if($source == self::SOURCE_DHL_DE_CUSTOMS) {
            self::_saveDhlDeCustoms($image, $fileName);
            $model->type = $type;
        }
        else
        {
            self::_saveDefault($image, $fileName);
            $model->type = $type;
        }

        $model->path = $fileName;

        $model->stat = self::STAT_OK;
        $model->courier_label_new_id = $courierLabelNew->id;

        if($model->save())
            return $model;
        else
            return false;
    }

    protected static function _saveDpdPl($image, $fileName)
    {
//        $log = print_r($fileName,1)."||| \r\n";
//        $log .= print_r($image,1)."||| \r\n";

//        @file_put_contents('DPDACK_'.$fileName, $image);


        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        $im->readImageBlob($image);

        $im->setImageFormat('png');
        $im->trimImage(0);

        if ($im->getImageHeight() > $im->getImageWidth()) {
            $im->rotateImage(new ImagickPixel(), 90);
        }

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->stripImage();
//        $blob = $im->getImageBlob();
//        $log .= print_r($blob,1)."||| \r\n";


//        MyDump::dump('dpdpl_ack.txt', $log);
//        @file_put_contents('DPDACK_B_'.$fileName, $image);
        @file_put_contents($fileName, $im->getImageBlob());
    }

    protected static function _saveDhlDeCustoms($image, $fileName)
    {
        $im = ImagickMine::newInstance();
        $im->setResolution(300, 300);

        $im->readImageBlob($image);

        $im->setImageFormat('png');
        $im->trimImage(0);

        if ($im->getImageAlphaChannel()) {
            $im->setImageBackgroundColor('white');
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        }

        $im->stripImage();
        @file_put_contents($fileName, $im->getImageBlob());
    }

    protected static function _saveDefault($image, $fileName)
    {
        @file_put_contents($fileName, $image);
    }
}