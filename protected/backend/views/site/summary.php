<?php
$statistic_general_from = date('Y-m-d');
$statistic_general_to = $statistic_general_from;
?>

<div style="text-align: center;">
    <h2>From <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'date_from',
            'value' => $statistic_general_from,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                'minDate' => '2013-01-01',      // minimum date
                'maxDate' => $statistic_general_to,      // maximum date
                'firstDay' => 1,
            ),
            'htmlOptions' => array(
                'size' => '10',         // textField size
                'maxlength' => '10',    // textField maxlength
                'id' => 'statistic-general-from',
            ),
        ));
        ?>
        To
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'date_to',
            'value' => $statistic_general_to,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                'minDate' => '2013-01-01',      // minimum date
                'maxDate' => $statistic_general_to,      // maximum date
                'firstDay' => 1,
            ),
            'htmlOptions' => array(
                'size' => '10',         // textField size
                'maxlength' => '10',    // textField maxlength
                'id' => 'statistic-general-to',
            ),
        ));
        ?>

        <?php echo CHtml::ajaxButton('Show',
            Yii::app()->createUrl('site/ajaxGetStatisticGeneral'),
            array(
                'data' => array('from' => 'js: $("#statistic-general-from").val()', 'to' => 'js:$("#statistic-general-to").val()'),
                'dataType' => 'html',
                'type' => 'post',
                'success' => 'function(r){
                   $("#statistic-general").html(r);
                }',
                'error' => 'function(r){
                    console.log(r);
                    }'
            ) // ajax
        ); // script
        ?>
    </h2>
</div>
<div id="statistic-general">
    <?php
    $this->renderPartial('_statistic_general',
        array(
            'from' => $statistic_general_from,
            'to' => $statistic_general_to,
        ));
    ?>
</div>


<hr/>
<h2>Last week</h2>
<h3>New orders</h3>
<?php
$dataSets = Yii::app()->cache->get('last_week_stat');
//$dataSets = false;

$daysBack = 14;

if($dataSets === false) {


    $dataSets = [];

//    $dates = [];

    for ($i = $daysBack; $i >= 0; $i--) {
        $key = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
//
//        $today = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
//        $dates[$key] = "date_entered LIKE '$today%'";
        $dataSets[$key]['_label'] = date('D,d/m', mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
    }


    $date = new DateTime();
    $date->sub(new DateInterval('P'.($daysBack+1).'D'));
    $dateEnd = $date->format('Y-m-d').' 23:59:59';

    $date = new DateTime('tomorrow');
    $dateStart = $date->format('Y-m-d').' 00:00:00';

//    $condition = implode(' OR ', $dates);

    $condition = "date_entered BETWEEN '$dateEnd' AND '$dateStart'";

    /* @var $cmd CDbCommand */
    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*) AS count, date(date_entered) AS date");
    $cmd->from("courier");
    $cmd->where($condition);
    $cmd->order('date ASC');
    $cmd->group = 'date';
    $countCourier = $cmd->queryAll();

    $temp = [];
    foreach($countCourier AS $key => $item)
        $temp[$item['date']] = $item['count'];
    $countCourier = $temp;

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*) AS count, date(date_entered) AS date");
    $cmd->from("palette");
    $cmd->where($condition);
    $cmd->order('date ASC');
    $cmd->group = 'date';
    $countPalette = $cmd->queryAll();

    $temp = [];
    foreach($countPalette AS $key => $item)
        $temp[$item['date']] = $item['count'];
    $countPalette = $temp;

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*) AS count, date(date_entered) AS date");
    $cmd->from("special_transport");
    $cmd->where($condition);
    $cmd->order('date ASC');
    $cmd->group = 'date';
    $countSt = $cmd->queryAll();

    $temp = [];
    foreach($countSt AS $key => $item)
        $temp[$item['date']] = $item['count'];
    $countSt = $temp;

    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*) AS count, date(date_entered) AS date");
    $cmd->from("postal");
    $cmd->where($condition);
    $cmd->order('date ASC');
    $cmd->group = 'date';
    $countPostal = $cmd->queryAll();

    $temp = [];
    foreach($countPostal AS $key => $item)
        $temp[$item['date']] = $item['count'];
    $countPostal = $temp;

    foreach($dataSets AS $key => $item)
    {
        $dataSets[$key]['courier'] = intval(isset($countCourier[$key]) ? ($countCourier[$key]) : 0);
        $dataSets[$key]['p'] = intval(isset($countPalette[$key]) ? ($countPalette[$key]) : 0);
        $dataSets[$key]['st'] = intval(isset($countSt[$key]) ? ($countSt[$key]) : 0);
        $dataSets[$key]['postal'] = intval(isset($countPostal[$key]) ? ($countPostal[$key]) : 0);
        $i++;
    }


    Yii::app()->cache->set('last_week_stat', $dataSets, 60*60);
}

?>

<?php

$dataJsArrayWeek = '[\'Day\', \'Courier\',\'Palette\',\'Special Transport\',\'Postal\'],';
foreach($dataSets AS $key => $item)
{

    $dataJsArrayWeek .= '[\''.CHtml::encode($item['_label']).'\', '.$item['courier'].', '.$item['p'].', '.$item['st'].', '.$item['postal'].'],';
}

?>


<?php
$googleCharts = "
google.charts.load('current', {'packages':['corechart']});
 google.charts.setOnLoadCallback(drawChartWeek);

    function drawChartWeek() {
        var data = google.visualization.arrayToDataTable([
            ".$dataJsArrayWeek."
        ]);

        var options = {
            width: 1100,
            height: 900,
            legend: { position: 'bottom' },
        };

//        var chart = new google.visualization.LineChart(document.getElementById('chart_week'));
        var chart = new google.visualization.AreaChart(document.getElementById(\"chart_week\"));

        chart.draw(data, options);
    }


";
Yii::app()->clientScript->registerScriptFile('https://www.gstatic.com/charts/loader.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('google-charts', $googleCharts, CClientScript::POS_END);
?>

<div id="chart_week" style="width: 1100px; height: 900px;"></div>
<br/>
<h3>Courier</h3>
<?php
$dataSets = Yii::app()->cache->get('last_week_stat_courier');
//$dataSets = false;
if($dataSets === false) {


    $dataSets = [];

//    $dates = [];

    for ($i = $daysBack; $i >= 0; $i--) {
        $key = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
//
//        $today = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
//        $dates[$key] = "date_entered LIKE '$today%'";
        $dataSets[$key]['_label'] = date('D,d/m', mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
    }


    $date = new DateTime();
    $date->sub(new DateInterval('P'.($daysBack+1).'D'));
    $dateEnd = $date->format('Y-m-d').' 23:59:59';

    $date = new DateTime('tomorrow');
    $dateStart = $date->format('Y-m-d').' 00:00:00';

//    $condition = implode(' OR ', $dates);

    $condition = "BETWEEN '$dateEnd' AND '$dateStart'";

    /* @var $cmd CDbCommand */
    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*) AS count, date(date_entered) AS date");
    $cmd->from("courier");
    $cmd->where('date_entered '.$condition);
    $cmd->order('date ASC');
    $cmd->group = 'date';
    $countCreated = $cmd->queryAll();

    $temp = [];
    foreach($countCreated AS $key => $item)
        $temp[$item['date']] = $item['count'];
    $countCreated = $temp;


    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*) AS count, date(date_activated) AS date");
    $cmd->from("courier");
    $cmd->where('date_activated '.$condition);
    $cmd->order('date ASC');
    $cmd->group = 'date';
    $countActivated = $cmd->queryAll();

    $temp = [];
    foreach($countActivated AS $key => $item)
        $temp[$item['date']] = $item['count'];
    $countActivated = $temp;


    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*) AS count, date(date_attempted_delivery) AS date");
    $cmd->from("courier");
    $cmd->where('date_attempted_delivery '.$condition);
    $cmd->order('date ASC');
    $cmd->group = 'date';
    $countAttemptedDelivery = $cmd->queryAll();

    $temp = [];
    foreach($countAttemptedDelivery AS $key => $item)
        $temp[$item['date']] = $item['count'];
    $countAttemptedDelivery = $temp;



    $cmd = Yii::app()->db->createCommand();
    $cmd->select("COUNT(*) AS count, date(date_delivered) AS date");
    $cmd->from("courier");
    $cmd->where('date_delivered '.$condition);
    $cmd->order('date ASC');
    $cmd->group = 'date';
    $countDelivered = $cmd->queryAll();

    $temp = [];
    foreach($countDelivered AS $key => $item)
        $temp[$item['date']] = $item['count'];
    $countDelivered = $temp;


    foreach($dataSets AS $key => $item)
    {
        $dataSets[$key]['created'] = intval(isset($countCreated[$key]) ? ($countCreated[$key]) : 0);
        $dataSets[$key]['activated'] = intval(isset($countActivated[$key]) ? ($countActivated[$key]) : 0);
        $dataSets[$key]['attemptedDelivery'] = intval(isset($countAttemptedDelivery[$key]) ? ($countAttemptedDelivery[$key]) : 0);
        $dataSets[$key]['delivered'] = intval(isset($countDelivered[$key]) ? ($countDelivered[$key]) : 0);

        $i++;
    }


    Yii::app()->cache->set('last_week_stat_courier', $dataSets, 60*60);
}

?>

<?php

$dataJsArrayWeek = '[\'Day\', \'Created\',\'Activated\',\'Attempted delivery\',\'Delivered\'],';
foreach($dataSets AS $key => $item)
{

    $dataJsArrayWeek .= '[\''.CHtml::encode($item['_label']).'\', '.$item['created'].', '.$item['activated'].', '.$item['attemptedDelivery'].', '.$item['delivered'].'],';
}

?>


<?php
$googleCharts = "
 google.charts.setOnLoadCallback(drawChartWeekCourier);

    function drawChartWeekCourier() {
        var data = google.visualization.arrayToDataTable([
            ".$dataJsArrayWeek."
        ]);

        var options = {
            width: 1100,
            height: 900,
            legend: { position: 'bottom' },
        };

        var chartCourier = new google.visualization.AreaChart(document.getElementById(\"chart_week_courier\"));

        chartCourier.draw(data, options);
    }


";
Yii::app()->clientScript->registerScript('google-charts-courier', $googleCharts, CClientScript::POS_END);
?>
<div id="chart_week_courier" style="width: 1100px; height: 900px"></div>
