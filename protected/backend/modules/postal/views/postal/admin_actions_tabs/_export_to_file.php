<h4>Export to file</h4>

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'export-to-file',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/postal/postal/exportToFileByGridView'),
));
?>

<?php
echo TbHtml::submitButton('Export to .xls', array(
    'onClick' => 'js:
    $("#postal-ids-export").val($("#postal").selGridView("getAllSelection").toString());
    ;',
    'name' => 'PostalIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
?>

<?php echo CHtml::hiddenField('Postal[ids]','', array('id' => 'postal-ids-export')); ?>
<?php
$this->endWidget();
?>