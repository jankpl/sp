<?php
/* @var $settleFormModel CourierCodSettledSettleByGridView */
/* @var $form GxActiveForm */

?>

<h4>Settle</h4>

<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'settle',
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('/courier/courierCodSettled/admin').'#settle_tab',
));
?>

<?= $form->hiddenField($settleFormModel, 'package_ids', ['id' => 'courier-ids-settle']); ?>

<?= $form->errorSummary($settleFormModel); ?>
<table class="table table-striped" style="margin: 0 auto; width: 500px;">
    <tr>
        <td>Settlement type:</td>
        <td style="text-align: center;"><?= $form->dropDownList($settleFormModel, 'type', CourierCodSettled::listType(), ['prompt' => '-']);?></td>
    </tr>
    <tr>
        <td>Settlement date:</td>
        <td style="text-align: center;"><?= $form->textField($settleFormModel, 'settle_date', ['placeholder' => 'yyyy-mm-dd']);?></td>
    </tr>
    <tr>
        <td colspan="2">
            <?php
            echo TbHtml::submitButton('Settle', array(
                'onClick' => 'js:
    $("#courier-ids-settle").val($("#courier-cod-settle-grid").selGridViewNoGet("getAllSelection").toString());
    ;',
                'name' => 'CourierIds', 'size' => TbHtml::BUTTON_SIZE_LARGE));
            ?>
        </td>
    </tr>
</table>

<?php
$this->endWidget();
?>
</div>
