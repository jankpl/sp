<?php

class CronJohn2Controller extends CController {

//    const PACKAGE_DAYS_IGNORE = 31; // for how many days check statuses

//    const PACKAGE_DAYS_IGNORE_INTERNAL_RETURN = 62; // for how many days check statuses of Internal Return packages

    const PACKAGE_DAYS_FRESH = 4; // for how many days package is considered as fresh

    const TTLOG_BLOCK_MINUTES = 360; // how long to waint between checks for not-fresh packages
    const TTLOG_BLOCK_MINUTES_FRESH = 180; // how long to wait between checks for fresh packages
    const DELAY_NEW_MINUTES = 240; // how long to wait before checking TT for new packages

    const STRIP_SIZE = 80;

    const MAX_STEPS = 8;

    const STRIP_SIZE_EXPIRE_LABELS = 500;
    const STRIP_SIZE_ARCHIVE_HISTORY = 5000;

    const TYPE_MAIN = 'main';
    const TYPE_OLDER = 'older';
    const TYPE_CANCELLED = 'cancelled';
    const TYPE_RETURNS = 'returns';

    const INSTANCES_MAIN = 4;
    const INSTANCES_OLDER = 4;
    const INSTANCES_CANCELLED = 4;
    const INSTANCES_RETURNS = 4;

    public function init()
    {
        Yii::app()->getModule('courier');
        parent::init();
    }

    public function filters()
    {
        return array();
    }

    public function actionStats()
    {


        echo '<table style="vertical-align: top;"><tr>';
        echo '<td style="vertical-align: top;">';
        echo 'MAIN:';
        $main = self::_modelsMain(1, 1, false);
        echo (S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';
        echo '<td/><td style="vertical-align: top;">';

        echo '_modelsCancelled:';
        $main = self::_modelsCancelled(1, 1, false);
        echo(S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';
        echo '<td/><td style="vertical-align: top;">';

        echo '_modelsMainSlower:';
        $main = self::_modelsMainSlower(1, 1, false);
        echo(S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';
        echo '<td/><td style="vertical-align: top;">';

        echo '_modelsOld:';
        $main = self::_modelsOld(1, 1, false);
        echo(S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';
        echo '<td/><td style="vertical-align: top;">';

        echo '_modelsReturnNotActivated:';
        $main = self::_modelsReturnNotActivated(1, 1, false);
        echo(S_Useful::sizeof($main));
        echo '<br/>';
        echo '<br/>';
        foreach($main AS $item)
            echo $item->local_id.'<br/>';

        echo '</td>';
        echo '</tr></table>';


    }

    function actionRun()
    {
        exit;

        $multiCurl = [];
        $result = [];

        $mh = curl_multi_init();


        $j = 1;
        if (date('G') <= 23 && date('G') >= 5) {
            for ($i = 1; $i <= self::INSTANCES_MAIN; $i++) {
                $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohn2/index', ['mod' => $i, 'type' => self::TYPE_MAIN]);
                $multiCurl[$j] = curl_init();
                curl_setopt($multiCurl[$j], CURLOPT_URL, $fetchURL);
                curl_setopt($multiCurl[$j], CURLOPT_HEADER, 0);
                curl_setopt($multiCurl[$j], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$j], CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($multiCurl[$j], CURLOPT_MAXREDIRS, 10);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT, 0);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT_MS, 0);
                curl_setopt($multiCurl[$j], CURLOPT_CONNECTTIMEOUT, 0);
                curl_multi_add_handle($mh, $multiCurl[$j]);

                $j++;
            }
        }

        if (date('G') <= 23 && date('G') >= 5) {
            for ($i = 1; $i <= self::INSTANCES_OLDER; $i++) {
                $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohn2/index', ['mod' => $i, 'type' => self::TYPE_OLDER]);
                $multiCurl[$j] = curl_init();
                curl_setopt($multiCurl[$j], CURLOPT_URL, $fetchURL);
                curl_setopt($multiCurl[$j], CURLOPT_HEADER, 0);
                curl_setopt($multiCurl[$j], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$j], CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($multiCurl[$j], CURLOPT_MAXREDIRS, 10);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT, 0);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT_MS, 0);
                curl_setopt($multiCurl[$j], CURLOPT_CONNECTTIMEOUT, 0);
                curl_multi_add_handle($mh, $multiCurl[$j]);

                $j++;
            }
        }

        if (date('G') > 23 OR date('G') < 5) {
            for ($i = 1; $i <= self::INSTANCES_CANCELLED; $i++) {
                $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohn2/index', ['mod' => $i, 'type' => self::TYPE_CANCELLED]);
                $multiCurl[$j] = curl_init();
                curl_setopt($multiCurl[$j], CURLOPT_URL, $fetchURL);
                curl_setopt($multiCurl[$j], CURLOPT_HEADER, 0);
                curl_setopt($multiCurl[$j], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$j], CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($multiCurl[$j], CURLOPT_MAXREDIRS, 10);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT, 0);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT_MS, 0);
                curl_setopt($multiCurl[$j], CURLOPT_CONNECTTIMEOUT, 0);
                curl_multi_add_handle($mh, $multiCurl[$j]);

                $j++;
            }
        }

        if (date('G') > 23 OR date('G') < 5) {
            for ($i = 1; $i <= self::INSTANCES_RETURNS; $i++) {
                $fetchURL = Yii::app()->createAbsoluteUrl('/cronJohn2/index', ['mod' => $i, 'type' => self::TYPE_RETURNS]);
                $multiCurl[$j] = curl_init();
                curl_setopt($multiCurl[$j], CURLOPT_URL, $fetchURL);
                curl_setopt($multiCurl[$j], CURLOPT_HEADER, 0);
                curl_setopt($multiCurl[$j], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$j], CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($multiCurl[$j], CURLOPT_MAXREDIRS, 10);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT, 0);
                curl_setopt($multiCurl[$j], CURLOPT_TIMEOUT_MS, 0);
                curl_setopt($multiCurl[$j], CURLOPT_CONNECTTIMEOUT, 0);
                curl_multi_add_handle($mh, $multiCurl[$j]);

                $j++;
            }
        }

        $index = null;
        do {
            curl_multi_exec($mh, $index);
        } while ($index > 0);

        foreach ($multiCurl as $k => $ch) {
            $result[$k] = curl_multi_getcontent($ch);
            curl_multi_remove_handle($mh, $ch);
        }
        curl_multi_close($mh);

    }


    protected function _modelsMain($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)

WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
    (courier.date_updated > (NOW() - INTERVAL :past_date_fresh DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date_fresh DAY))
)
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
AND ((courier_type != :courier_returns) OR (courier_type = :courier_returns AND date_activated IS NOT NULL))
AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block_fresh MINUTE))
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,

                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_returns' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }

    protected function _modelsCancelled($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0, $instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE courier_stat_id IN ($ignoredStats)
AND
	(courier.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date2 DAY))
 AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 24 HOUR))
AND courier.stat_map_id = :ignored_map AND courier.courier_stat_id != :canceled_problem
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [

                    ':past_date2' => Courier::TIMER_IGNORE_TT_AFTER,
//                    ':ttLog_block' => 24,
                    ':type' => TtLog::TYPE_COURIER,
                    ':ignored_map' => StatMap::MAP_CANCELLED,
                    ':canceled_problem' => CourierStat::CANCELLED_PROBLEM,
                ]);

        return $models;
    }

    protected function _modelsMainSlower($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE courier_stat_id NOT IN ($ignoredStats)
AND
(
(courier.date_updated >= (NOW() - INTERVAL :past_date DAY) AND courier.date_updated < (NOW() - INTERVAL :past_date_fresh DAY)) OR (courier.date_updated IS NULL AND courier.date_entered >= (NOW() - INTERVAL :past_date DAY) AND courier.date_entered < (NOW() - INTERVAL :past_date_fresh DAY))
	)
AND (courier.courier_type != :courier_type OR (courier.courier_type = :courier_type AND date_activated IS NOT NULL) )
 AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER,

                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
//                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,

                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,

                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }

    protected function _modelsOld($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0,$instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
(courier.date_updated >= (NOW() - INTERVAL :past_date_old MONTH) AND courier.date_updated < (NOW() - INTERVAL :past_date DAY)) OR (courier.date_updated IS NULL AND courier.date_entered >= (NOW() - INTERVAL :past_date MONTH) AND courier.date_entered < (NOW() - INTERVAL :past_date DAY))
	)
AND (courier.courier_type != :courier_type OR (courier.courier_type = :courier_type AND ((
courier.date_updated < (NOW() - INTERVAL :past_date_returns DAY)) OR courier.date_entered < (NOW() - INTERVAL :past_date_returns DAY))
	)) 
 AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 7 DAY))
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2 AND courier.stat_map_id != :ignored_map3
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER,
                    ':past_date_returns' => Courier::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS,
                    ':past_date_old' => 6,

//                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
//                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,

                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,

                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':ignored_map3' => StatMap::MAP_RETURN,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }


    protected function _modelsReturnNotActivated($instances, $mod, $limit = true)
    {
        if($mod == 0)
            $mods = implode(',', range(0, $instances));
        else
            $mods = $mod - 1;

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $sql = "SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_type = :courier_type
AND date_activated IS NULL
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
	(courier.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date2 DAY))
)
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
 AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL 12 HOUR))
AND (courier.id MOD ".$instances.") IN ($mods)
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC";

        if($limit)
            $sql .= " LIMIT ".self::STRIP_SIZE;

        $models = Courier::model()
            ->findAllBySql($sql,
                [
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':past_date2' => Courier::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS,
//                    ':ttLog_block' => 24,
                    ':type' => TtLog::TYPE_COURIER,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);

        return $models;
    }

    // update courier packages statuses with external statuses
    public function actionIndex($type, $mod = 0, $set = 0, $ssid = NULL)
    {
exit;

//        sleep(2 * $mod);

        if((date('G') > 23 OR date('G') < 5) && in_array($type, [self::TYPE_MAIN, self::TYPE_OLDER]))
            exit;

        if((date('G') >= 5 AND date('G') <= 23) && in_array($type, [self::TYPE_CANCELLED, self::TYPE_RETURNS]))
            exit;

        if(!$mod)
            exit;

        $start = date('Y-m-d H:i:s');

        $filename = 'cjlog_'.$type.'.txt';


        $CACHE_NAME = 'cronJohnRun2_'.$type.'_'.$mod;
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== $ssid) {

            MyDump::dump($filename, "\r\n".$start." : AIP[$mod] : ".Yii::app()->session->sessionID." : ".$sessionName." : ". $ssid);
            Yii::app()->end();
        } else
            MyDump::dump($filename, "\r\n".$start." : START[$mod][$set] : ".Yii::app()->session->sessionID." : ".$sessionName." : ". $ssid);

        if(S_SystemLoad::isHigh())
        {

            MyDump::dump($filename, "\r\n".$start." : [$mod] : HIGH LOAD");
            Yii::app()->cache->delete($CACHE_NAME);
            Yii::app()->end();
        }

        if($ssid == '')
            $ssid = Yii::app()->session->sessionID;

        Yii::app()->cache->set($CACHE_NAME, $ssid, 60 * 15);

//        if($mod == 1)
//            self::tt4CancelledPackages();

        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);

        if($type == self::TYPE_MAIN)
            $couriersNew = self::_modelsMain(self::INSTANCES_MAIN, $mod);
        else if($type == self::TYPE_OLDER)
            $couriersNew = self::_modelsMainSlower(self::INSTANCES_OLDER, $mod);
        else if($type == self::TYPE_CANCELLED)
            $couriersNew = self::_modelsCancelled(self::INSTANCES_CANCELLED, $mod);
        else if($type == self::TYPE_RETURNS)
            $couriersNew = self::_modelsReturnNotActivated(self::INSTANCES_RETURNS, $mod);
        else
            throw new Exception('Unknowny type!');

        $log = $start.' ['.print_r($mod,1).'] : '.sprintf("%02d",$set)." --- ". sprintf("%03d",S_Useful::sizeof($couriersNew))." --- ";

        try {
            /* $item Courier */
            if (is_array($couriersNew))
                foreach ($couriersNew AS $item) {
                    CourierExternalManager::updateStatusForPackage($item);
                    MyDump::dump('cj_full_log_divided.txt', $item->id.' : '.print_r($type,1).' : '.print_r($mod,1).' : '.print_r($set,1).' : '.print_r($ssid,1));
                }
        }
        catch(Exception $ex)
        {
            MyDump::dump('cj_exception.txt', print_r($ex->getMessage(),1));
        }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(S_Useful::sizeof($couriersNew) == self::STRIP_SIZE)
        {
            if($set < self::MAX_STEPS)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM : ".Yii::app()->session->sessionID." : ". $ssid;
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohn2/index', array('set' => $set, 'mod' => $mod, 'ssid' => $ssid, 'type' => $type));
                MyDump::dump($filename, $log);
                header("Location: ".$path);
                Yii::app()->end();
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM: ".Yii::app()->session->sessionID." : ". $ssid;
                MyDump::dump($filename, $log);

                Yii::app()->cache->delete($CACHE_NAME);
                Yii::app()->end();
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM : ".Yii::app()->session->sessionID." : ". $ssid;
            MyDump::dump($filename, $log);

            Yii::app()->cache->delete($CACHE_NAME);
            Yii::app()->end();
        }


    }


    /**
     * Action expires old labels
     */

    public function actionArchiveHistoryStatus($set = 0)
    {

        $start = date('Y-m-d H:i:s');

        $CACHE_NAME = 'cronJohnArchiveHistoryStatusRun';
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== Yii::app()->session->sessionID) {
            MyDump::dump('cjahslog.txt', "\r\n".$start." : AIP");
            Yii::app()->end();
        }
        Yii::app()->cache->set($CACHE_NAME, Yii::app()->session->sessionID, 60 * 15);

        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);


        // OLD HISTORY:
        $toArchive = Courier::model()->findAll('(date_updated IS NOT NULL AND date_updated < (NOW() - INTERVAL :days DAY) OR (date_updated IS NULL AND date_entered < (NOW() - INTERVAL :days DAY)))       
        AND ((archive != :archive AND archive != :archive2) OR archive IS NULL) LIMIT :limit', [
            ':days' => Courier::TIMER_ARCHIVE_HISTORY_AFTER_DAYS,
            ':archive' => Courier::ARCHIVED_HISTORY,
            ':archive2' => (Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED),
            ':limit' => self::STRIP_SIZE_ARCHIVE_HISTORY,
        ]);

        /* @var $item Courier */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {
                CourierStatHistory::expireOldHistory($item);
            }


        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        $log = $start.' : '.sprintf("%02d",$set)." --- ". sprintf("%03d",S_Useful::sizeof($toArchive))." --- ";

        // there is propably more
        if(S_Useful::sizeof($toArchive) == self::STRIP_SIZE_ARCHIVE_HISTORY)
        {
            if($set < 10)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohn/archiveHistoryStatus', array('set' => $set ));

                MyDump::dump('cjahslog.txt', $log);

                header("Location: ".$path);
                exit;
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";
                MyDump::dump('cjahslog.txt', $log);

                Yii::app()->cache->delete($CACHE_NAME);
                exit;
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";
            // run only on first round deleting old stats
            if(!$set)
            {
                // UNUSED STATS:
                CourierStat::expireUnusedOldStats();
                $log .= "\r\nEOS";
                TtLog::clearOldData();
                $log .="\r\nEOT";
            }

            MyDump::dump('cjahslog.txt', $log);

            Yii::app()->cache->delete($CACHE_NAME);
            exit;
        }



    }



    /**
     * Action expires old labels
     */
    public function actionArchiveLabels($set = 0)
    {

        $start = date('Y-m-d H:i:s');

        $CACHE_NAME = 'cronJohnArchiveLabelsRun';
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== Yii::app()->session->sessionID) {
            MyDump::dump('cjallog.txt', "\r\n".$start." : AIP");
            Yii::app()->end();
        }
        Yii::app()->cache->set($CACHE_NAME, Yii::app()->session->sessionID, 60 * 15);

        self::_clearOldOrders();

        $set = intval($set);

        Yii::app()->getModule('postal');
        $toArchive = CourierLabelNew::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY) AND stat = :stat LIMIT :limit', [
            ':days' => Courier::TIMER_ARCHIVE_LABELS_AFTER_DAYS,
            ':stat' => CourierLabelNew::STAT_SUCCESS,
            ':limit' => self::STRIP_SIZE_EXPIRE_LABELS
        ]);

        $log = $start.' : '.sprintf("%02d",$set)." --- ". sprintf("%03d",S_Useful::sizeof($toArchive))." --- ";

        /* @var $item CourierLabelNew */
        if (is_array($toArchive))
            foreach ($toArchive AS $item) {
                $item->expireLabel();
            }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(S_Useful::sizeof($toArchive) == self::STRIP_SIZE_EXPIRE_LABELS)
        {
            if($set < 10)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                MyDump::dump('cjallog.txt', $log);
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohn/archiveLabels', array('set' => $set));

                header("Location: ".$path);
                exit;
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";

                MyDump::dump('cjallog.txt', $log);
                Yii::app()->cache->delete($CACHE_NAME);
                exit;
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";

            MyDump::dump('cjallog.txt', $log);
            Yii::app()->cache->delete($CACHE_NAME);
            exit;
        }


    }


    public function _clearOldOrders()
    {

        $models = Order::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY) AND (order_stat_id =:order_stat_1 OR order_stat_id = :order_stat_2) ', [':days' => Courier::TIMER_CANCEL_UNFINALIZED_ORDERS, ':order_stat_1' => OrderStat::NEW_ORDER, ':order_stat_2' => OrderStat::WAITING_FOR_PAYMENT]);

        /* @var $model Order */
        foreach($models AS $model) {

            MyDump::dump('coo.txt', 'ORDER CANCELLED: '.$model->id);
            $model->changeStat(OrderStat::CANCELLED);
        }


        $models = Courier::model()->findAll('date_entered < (NOW() - INTERVAL :days DAY) AND (courier_stat_id = :stat_1 OR courier_stat_id = :stat_2) ', [':days' => Courier::TIMER_CANCEL_UNFINALIZED_ITEMS, ':stat_1' => CourierStat::NEW_ORDER, ':stat_2' => CourierStat::PACKAGE_PROCESSING]);
        /* @var $model Courier */
        foreach($models AS $model) {
            MyDump::dump('coo.txt', 'COURIER CANCELLED: '.$model->id);
            $model->changeStat(CourierStat::CANCELLED, 0, NULL, false, null, false, true, false, false, true);
        }

    }







    public function actionOldest($set = 0)
    {

        $start = date('Y-m-d H:i:s');


        $CACHE_NAME = 'cronJohnRun_OLDEST';
        $sessionName = Yii::app()->cache->get($CACHE_NAME);

        // CRON JOHN ALEREADY IN PROGRESS - PREVENT DUPLICATING PROCESS
        if ($sessionName !== false && $sessionName !== Yii::app()->session->sessionID) {

            MyDump::dump('cjlog_oldest.txt', "\r\n".$start." : AIP[]");
            Yii::app()->end();
        }

        if(S_SystemLoad::isHigh())
        {

            MyDump::dump('cjlog_oldest.txt', "\r\n".$start." : [] : HIGH LOAD");
            Yii::app()->end();
        }

        Yii::app()->cache->set($CACHE_NAME, Yii::app()->session->sessionID, 60 * 15);


        // set has no real usage now except from preventing multiple runs...
        $set = intval($set);

        $ignoredStats = implode(',', Courier::getStatUpdateIgnoredStats());

        $couriersNew = Courier::model()
            ->findAllBySql("SELECT DISTINCT courier.* FROM courier
LEFT JOIN tt_log ON (tt_log.item_id = courier.id AND tt_log.item_type = :type AND tt_log.last = 1)
WHERE (courier.date_entered < (NOW() - INTERVAL :delay_new MINUTE))
AND courier_stat_id NOT IN ($ignoredStats)
AND
(
(
    (courier.date_updated > (NOW() - INTERVAL :past_date_fresh DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date_fresh DAY))
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block_fresh MINUTE))
)
OR
(
	(courier.date_updated > (NOW() - INTERVAL :past_date DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date DAY))
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
)
OR
(
	(courier.date_updated > (NOW() - INTERVAL :past_date2 DAY) OR courier.date_entered > (NOW() - INTERVAL :past_date2 DAY)) AND courier.courier_type = :courier_type
	AND (tt_log.id IS NULL OR tt_log.date_entered < (NOW() - INTERVAL :ttLog_block MINUTE))
)
)
AND courier.stat_map_id != :ignored_map AND courier.stat_map_id != :ignored_map2
AND (archive NOT IN (".(Courier::ARCHIVED_HISTORY).",".(Courier::ARCHIVED_HISTORY + Courier::ARCHIVED_LABEL_EXPIRED).") OR archive IS NULL)
ORDER BY tt_log.id ASC
LIMIT ".self::STRIP_SIZE
                ,
                [
                    ':past_date' => Courier::TIMER_IGNORE_TT_AFTER,
                    ':past_date2' => Courier::TIMER_IGNORE_TT_AFTER_INTERNAL_RETURNS,
                    ':past_date_fresh' => self::PACKAGE_DAYS_FRESH,
                    ':ttLog_block' => self::TTLOG_BLOCK_MINUTES,
                    ':ttLog_block_fresh' => self::TTLOG_BLOCK_MINUTES_FRESH,
                    ':type' => TtLog::TYPE_COURIER,
                    ':delay_new' => self::DELAY_NEW_MINUTES,
                    ':ignored_map' => StatMap::MAP_DELIVERED,
                    ':ignored_map2' => StatMap::MAP_CANCELLED,
                    ':courier_type' => Courier::TYPE_RETURN,
                ]);


        $log = $start.' [] : '.sprintf("%02d",$set)." --- ". sprintf("%03d",S_Useful::sizeof($couriersNew))." --- ";

        /* $item Courier */
        if(is_array($couriersNew))
            foreach($couriersNew AS $item)
            {
                CourierExternalManager::updateStatusForPackage($item);
            }

        $timeFirst  = strtotime($start);
        $timeSecond = strtotime(date('Y-m-d H:i:s'));
        $differenceInSeconds = $timeSecond - $timeFirst;

        // there is propably more
        if(S_Useful::sizeof($couriersNew) == self::STRIP_SIZE)
        {
            if($set < 10)
            {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- GFM";
                $set++;
                $path = Yii::app()->createAbsoluteUrl('/cronJohn/oldest', array('set' => $set));
                MyDump::dump('cjlog_oldest.txt', $log);
                header("Location: ".$path);
                exit;
            } else {
                $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- LIM";
                MyDump::dump('cjlog_oldest.txt', $log);

                Yii::app()->cache->delete($CACHE_NAME);
                exit;
            }
        } else {
            $log .= " --- ".sprintf("%04d",$differenceInSeconds)." --- TNM";
            MyDump::dump('cjlog_oldest.txt', $log);

            Yii::app()->cache->delete($CACHE_NAME);
            exit;
        }


    }


}