<?php

class PageController extends Controller {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Page'),
		));
	}


	public function actionUpdate($id) {

        $model = $this->loadModel($id, 'Page');

        $modelTrs = Array();

        foreach(Language::model()->findAll('stat = 1') AS $item)
        {
            $modelTrs[$item->id] = PageTr::model()->find('page_id=:page_id AND language_id=:language_id', array(':page_id' => $model->id, ':language_id' => $item->id));
            if($modelTrs[$item->id] == NULL)
            {
                $pageTr = new PageTr();
                $pageTr->language_id = $item->id;
                $pageTr->page_id = $id;
                $modelTrs[$item->id] = $pageTr;
            }
        }

        if (isset($_POST['Page'])) {

            $model->setAttributes($_POST['Page']);

            $transaction = Yii::app()->db->beginTransaction();
            $errors = false;

            foreach($_POST['PageTr'] AS $key => $item)
            {
                if($item['title'] == '' AND $item['intro'] == '' AND $item['text'] == 0) continue;

                if($modelTrs[$key] === null) $modelTrs[$key] = new PageTr;
                $modelTrs[$key]->setAttributes($item);


                if(!$modelTrs[$key]->save())
                    $errors = true;

            }

			if (!$errors) {
                $transaction->commit();
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
            'modelTrs' => $modelTrs,
				));
	}


	public function actionIndex() {

        $this->redirect(array('page/admin'));
	}

	public function actionAdmin() {
		$model = new Page('search');
		$model->unsetAttributes();

		if (isset($_GET['Page']))
			$model->setAttributes($_GET['Page']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}