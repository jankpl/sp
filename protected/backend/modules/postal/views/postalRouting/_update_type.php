<?php
/* @var $models [] */
/* @var $postalType int */
/* @var $form GxActiveForm */
/* @var $model PostalRouting */
?>

<?php
$sizeClassList = Postal::getSizeClassList();
$sizeClassListSize = S_Useful::sizeof($sizeClassList);

$weightClassList = Postal::getWeightClassList();
$weightClassListSize = S_Useful::sizeof($weightClassList);



?>

<input type="button" value="Copy values to clipboard" class="btn btn-primary" data-copy-to-clipboard="<?= $postalType; ?>"/>
<input type="button" value="Paste values from clipboard" class="btn btn-warning" data-paste-from-clipboard="<?= $postalType; ?>"/>

<br/>
<?= CHtml::dropDownList('_common_operator', '', CHtml::listData(PostalOperator::getModels($postalType), 'id', 'nameWithBroker'), ['prompt' => '-', 'data-postal-type-common-operator' => $postalType]);?>
<?= CHtml::button('Set operator for all items', ['class' => 'btn', 'data-set-common-operator' => $postalType]);?>


<table class="table table-striped table-condensed table-bordered" data-clipboard-target="<?= $postalType; ?>">
    <tr>
        <th>Size class</th>
        <th>Weight class</th>
        <th>Operator</th>
        <th>Price</th>
    </tr>

    <?php
    foreach($sizeClassList AS $sizeClass => $sizeClassName):
        ?>
        <tr>
            <th rowspan="<?= Postal::countWeightClassesForSizeClass($sizeClass) + 1; ?>"><?= $sizeClassName;?></th>
        </tr>

        <?php


        foreach($weightClassList AS $weightClass => $weightClassName):

            if(Postal::getWeightClassValueByClass($weightClass) > Postal::getMaxWeightForSizeClass($sizeClass))
                continue;
            ?>
            <tr>
                <th><?= $weightClassName;?></th>
                <td><?= $form->dropDownList($models[$sizeClass][$weightClass], '['.$postalType.']['.$sizeClass.']['.$weightClass.']postal_operator_id', CHtml::listData(PostalOperator::getModels($postalType), 'id', 'nameWithBroker'), ['prompt' => '-', 'data-postal-type-operator' => $postalType]);?><br/>
                    <?= $form->error($models[$sizeClass][$weightClass], '['.$postalType.']['.$sizeClass.']['.$weightClass.']postal_operator_id');?>
                </td>
                <td>
                    <?php $this->widget('PriceForm', array(
                        'form' => $form,
                        'model' => $models[$sizeClass][$weightClass]->price,
                        'formFieldPrefix' => '['.$postalType.']['.$sizeClass.']['.$weightClass.']',
                        'vertical' => false,
                    ));?>
                    <?= $form->error($models[$sizeClass][$weightClass], 'price_id'); ?>
                </td>
            </tr>

        <?php
        endforeach;
        ?>

    <?php
    endforeach;
    ?>
</table>
