<?php

Yii::import('application.models._base.BaseOrderNew');

class OrderNew extends BaseOrderNew
{
    const TYPE_SUPPLEMENT = 1;
    const TYPE_PAYMENT_PROVISION = 10;

    const TYPE_COURIER = 100;
    const TYPE_POSTAL = 200;

    const STAT_WAITING = 0;
    const STAT_SETTLED = 10;
    const STAT_SETTLED_AUTO = 11;
    const STAT_CANCELLED = 99;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(), array(
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                    'createAttribute' => 'date_entered',
                    'updateAttribute' => NULL,
                ),
            )
        );
    }

    public function rules() {
        return array(
            array('type, value_netto, value_brutto, tax, currency', 'required'),
            array('type, type_item_id, stat, parent_id', 'numerical', 'integerOnly'=>true),
            array('value_netto, value_brutto', 'length', 'max'=>10),
            array('tax', 'length', 'max'=>4),
            array('currency', 'length', 'max'=>3),
            array('note', 'length', 'max'=>256),
            array('date_cleared', 'safe'),
            array('type_item_id, date_cleared, note, parent_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('stat', 'default', 'setOnEmpty' => true, 'value' => self::STAT_WAITING),
            array('id, type, type_item_id, date_entered, date_cleared, value_netto, value_brutto, tax, currency, stat, note, parent_id', 'safe', 'on'=>'search'),
        );
    }

    public static function getStatList()
    {
        return [
            self::STAT_WAITING => Yii::t('orderNew', 'Oczekuje'),
            self::STAT_SETTLED => Yii::t('orderNew', 'Rozliczone'),
            self::STAT_SETTLED_AUTO => Yii::t('orderNew', 'Rozliczone (auto)'),
            self::STAT_CANCELLED => Yii::t('orderNew', 'Anulowane'),
        ];
    }


    public static function addNewItem($type, $type_id, $currency, $value_netto, $value_brutto, $tax_rate, $user_id, $admin_id = NULL, $note = NULL, $parent_id = NULL)
    {
        $model = new self;
        $model->type = $type;
        $model->type_item_id = $type_id;
        $model->currency = $currency;
        $model->value_netto = $value_netto;
        $model->value_brutto = $value_brutto;
        $model->tax = $tax_rate;
        $model->note = $note;
        $model->user_id = $user_id;
        $model->admin_id = $admin_id;
        $model->parent_id = $parent_id;

        if(!$model->save())
            throw new Exception('Could not save new Order_new : '.print_r($model->getErrors(),1));

        return $model;
    }

    public function setCancelled($cancelProduct = false, $withChildren = false)
    {
        $this->date_cleared = date('Y-m-d H:i:s');
        $this->stat = self::STAT_CANCELLED;
        $this->update(['date_cleared', 'stat']);

        if($cancelProduct) {
            switch ($this->type) {
                case self::TYPE_COURIER: // Courier
                    /* @var $model Courier */
                    $model = Courier::model()->findByPk($this->type_item_id);
                    if ($model->getType() == Courier::TYPE_INTERNAL && !$model->courierTypeInternal->paid && in_array($model->courier_stat_id, [CourierStat::NEW_ORDER, CourierStat::PACKAGE_PROCESSING]))
                        $model->changeStat(CourierStat::CANCELLED, 0, NULL, false, null, false, true, false, false, true);
                    break;
                case self::TYPE_POSTAL: // Postal
                    /* @var $model Postal */
                    $model = Postal::model()->findByPk($this->type_item_id);
                    if (in_array($model->postal_stat_id, [PostalStat::NEW_ORDER, PostalStat::PACKAGE_PROCESSING]))
                        $model->changeStat(PostalStat::CANCELLED, 0, NULL, false, null, false, true, false, false);
                    break;
            }
        }

        if($withChildren)
        {
            foreach($this->childrenOrderNew AS $item)
                $item->setCancelled($cancelProduct);
        }

    }

    public function setPaid($activateProduct = false, $withChildren = false)
    {
        $this->date_cleared = date('Y-m-d H:i:s');
        $this->stat = self::STAT_SETTLED_AUTO;
        $this->update(['date_cleared', 'stat']);

        if($activateProduct) {

            switch ($this->type) {
                case self::TYPE_COURIER :
                    /* @var $model Courier */
                    $model = Courier::model()->findByPk($this->type_item_id);
                    $model->changeStat(CourierStat::PACKAGE_PROCESSING);

                    if ($model->getType() == Courier::TYPE_INTERNAL)
                        $model->courierTypeInternal->setPaid();

                    break;
                case self::TYPE_POSTAL: // Postal
                    $model = Postal::model()->findByPk($this->type_item_id);
                    $model->onOrderPaid();
                    break;
            }
        }

        if($withChildren)
        {
            foreach($this->childrenOrderNew AS $item)
                $item->setPaid($activateProduct);
        }
    }


}