<?php

/* @var $this HybridMailController */
/* @var $model HybridMail */
/* @var $modelHybridMailAttachments HybridMailAttachment[] */
/* @var $form CActiveForm */

?>


<div class="form">

    <h2><?php echo Yii::t('hybridMail','HybridMail');?> | <?php echo Yii::t('app','Krok {step}', array('{step}' => '1/6')); ?> | <?php echo Yii::t('app','Treść Hybrid Mail'); ?></h2>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'hybrid-mail-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnType'=>true,
            'validationDelay'=> 0.1,
        ),
        'stateful'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php if(Yii::app()->user->isGuest):?>

        <div class="row">
            <?php echo CHtml::label(Yii::t('pay','Wybierz swoją walutę'),'currency');?>
            <?php echo CHtml::dropDownList('currency', $currency, CHtml::listData(PriceCurrency::model()->findAll(),'id','short_name'), array('id' => 'selected-currency')); ?>
        </div><!-- row -->
        <hr/>

        <script>
            $("#selected-currency").on('change', function(){
                $("#currency").html($(this).find("option:selected").text());
            })
        </script>

    <?php endif; ?>


    <div class="row text-center">
        <?php
        echo $form->fileField($model, 'document', array('data-filename-placement' => 'inside', 'class' => 'btn-warning btn-lg', 'title' => Yii::t('hm', 'Wybierz dokument do przesłania')));
        echo $form->error($model, 'document');
        ?>
    </div>

    <br/>
    <br/>

    <div class="flash-notice">
        <?php echo Yii::t('hm', 'Dozwolone są dokumenty formatu A4 zapisane w formacie:');?>
        <ul>
            <li>PDF</li>
            <li>DOC (MS Word)</li>
            <li>DOCX (MS Word)</li>
        </ul>
        <?php echo Yii::t('hm', 'Zalecamy jednakże format PDF, gdyż pozostałe pliki zostaną automatycznie przekonwertowane do formatu PDF, co nie gwarantuje 100% niezmienności formatowania treści.');?>
    </div>

    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.bootstrap.file-inputs.js',CClientScript::POS_END);
    $script = "$('input[type=file]').bootstrapFileInput();";
    Yii::app()->clientScript->registerScript('bootstrap-file-input', $script, CClientScript::POS_READY);
    ?>

    <?php
    echo '<ul>';
//    foreach($modelHybridMailAttachments AS $file):
//
//        echo '<li>'.$file->name.' ('.$file->sizeHuman.')</li>';
//
//    endforeach;
    echo '</ul>';
    ?>


    <div class="navigate">
        <?php
        echo TbHtml::submitButton(Yii::t('app','Dalej'),array('name'=>'step1cont', 'class' => 'btn-primary'));
        echo ' ';
        echo $jumpToEnd? TbHtml::submitButton(Yii::t('app','Podsumowanie'),array('name'=>'step5', 'class' => 'btn-small')):'';
        $this->endWidget();
        ?>
    </div>



</div><!-- form -->