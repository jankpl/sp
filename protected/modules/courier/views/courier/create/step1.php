<?php
/* @var $model Courier */
/* @var $form TbActiveForm */
/* @var $additionsHtml string */
/* @var $noPickup boolean */
/* @var $noCarriage boolean */
/* @var $codCurrency false|string */
/* @var $codIgnored integer */
/* @var $deliveryOperatorBrokerId integer */
/* @var $favList array */
/* @var $onValidation boolean */
?>

<?php
Yii::app()->clientScript->registerScript('helpers', '
          yii = {
              urls: {
                  spPointsDetails: '.CJSON::encode(Yii::app()->urlManager->createUrl('/spPoints/ajaxGetSpPointDetails')).',
                  checkIfPickupAvailable: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxCheckIfPickupAvailable')).',
                  checkIfCarriageAvailable: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxCheckIfAvailable')).',
                  getAdditions: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxGetAdditions')).',
                  getCod: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxGetCodCurrency')).',
                  getPrice: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxGetPrice')).',
                  base: '.CJSON::encode(Yii::app()->getBaseUrl(true)).',
                  mapIco: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico.png').',
                  mapIcoOrange: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico_orange.png').',
                  mapIcoGreen: '.CJSON::encode(Yii::app()->getBaseUrl(true).'/images/ruch/sp_ruch_map_ico_green.png').',
                  listOfPoints: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierInternal/ajaxListOfPoints')).',
                  listOfPointsSp: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierInternal/ajaxListOfPointsSp')).',
                  loadModalContent: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courierInternal/ajaxLoadModalContent')).',
                  checkAutomaticReturn: '.CJSON::encode(Yii::app()->urlManager->createUrl('/courier/courier/ajaxIsAutomaticReturnAvailable')).',
                  googleMapsApiByAddress: '.CJSON::encode(Yii::app()->urlManager->createUrl('/gA/getByAddress')).',
                  googleMapsApiByCoordinates: '.CJSON::encode(Yii::app()->urlManager->createUrl('/gA/getByCoordinates')).'
              },
               loader: {
                  textTop: '.CJSON::encode(Yii::t('site', 'Trwa ładowanie...')).',
                  textBottom: '.CJSON::encode(Yii::t('site', 'Proszę czekać...')).',
                  error:  '.CJSON::encode(Yii::t('site', 'Wystąpił błąd, ale system spróbuje automatycznie przetworzyć formularz. Następnie proszę spróbować kontynuować. Jeżeli błąd będzie się powtarzał, prosimy o kontakt.')).',
              },
              pickUp: {
                 WITH_PICKUP_NONE: '.CJSON::encode(CourierTypeInternal::WITH_PICKUP_NONE).',
                 WITH_PICKUP_RUCH: '.CJSON::encode(CourierTypeInternal::WITH_PICKUP_RUCH).',
                 WITH_PICKUP_REGULAR: '.CJSON::encode(CourierTypeInternal::WITH_PICKUP_REGULAR).',
                 WITH_PICKUP_CONTRACT: '.CJSON::encode(CourierTypeInternal::WITH_PICKUP_CONTRACT).',
                 WITH_PICKUP_LP_EXPRESS: '.CJSON::encode(CourierTypeInternal::WITH_PICKUP_LP_EXPRESS).',
                 WITH_PICKUP_UPS: '.CJSON::encode(CourierTypeInternal::WITH_PICKUP_UPS).',
                 WITH_PICKUP_INPOST: '.CJSON::encode(CourierTypeInternal::WITH_PICKUP_INPOST).',
                 WITH_PICKUP_DHLDE: '.CJSON::encode(CourierTypeInternal::WITH_PICKUP_DHLDE).',
              },
              pickUpNames: '.CJSON::encode(CourierTypeInternal::getWithPickupList()).',
                message: {
                customWrapping: '.CJson::encode(Yii::t('courier','Niestandardowe opakowanie wymaga opcji "Przesyłka niestandardowa"')).'
              },
              ids: 
                {
                    nonStandardAdditions: '.CJSON::encode([CourierAdditionList::PACKAGE_NON_STANDARD_DOMESTIC, CourierAdditionList::PACKAGE_NON_STANDARD_INT]).',
                    countryDe: '.CJSON::encode(CountryList::COUNTRY_DE).',
                    countryLt: '.CJSON::encode(CountryList::COUNTRY_LT).',
                    countryPl: '.CJSON::encode(CountryList::COUNTRY_PL).',
                    inpostShipx: '.CJSON::encode(GLOBAL_BROKERS::BROKER_INPOST_SHIPX).'
                },
               automaticReturnCountries: '.CJSON::encode(Yii::app()->user->model->getReturnPackageAutomatic()).',
               pickupTypes: '.CJSON::encode(array_keys(CourierTypeInternal::getWithPickupList())).',
               pickupTypeFav: '.CJSON::encode(Yii::app()->user->model->getCourierTypeInternalDefaultPickup()).',
               googleMapsApiKey: '.CJSON::encode(GLOBAL_CONFIG::GOOGLE_MAPS_API_KEY).'           
          };
      ',CClientScript::POS_HEAD);



Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-checkbox.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.bootstrap-touchspin.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/dimensionalWeight.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courierInternalForm.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.bootstrap-touchspin.min.css');

Yii::app()->getComponent('yiiwheels')->registerAssetJs('bootstrap-notify.min.js');
Yii::app()->getComponent('yiiwheels')->registerAssetCss('animate.css');



Yii::app()->clientScript->registerCssFile('https://mapka.paczkawruchu.pl/paczkawruchu.css');
Yii::app()->clientScript->registerScriptFile('https://mapka.paczkawruchu.pl/jquery.pwrgeopicker.min.js');
?>

<h2><?php echo Yii::t('courier', 'Create Courier'); ?> | <?= Yii::t('app','Krok {step}', array('{step}' => '1/2')); ?></h2>
<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'courier-internal-form',
        'enableClientValidation' => true,
        'stateful'=>true,
//        'htmlOptions' => [
//            'class' => 'form-horizontal',
//        ],
    ));
    ?>

    <?php
    if($codIgnored)
        echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, Yii::t('courier', 'Wybrana przez Ciebie opcja COD została anulowania, ponieważ nie jest ona obsługiwana na tej trasie!'), array('closeText' => false));
    ?>

    <?php
    $this->widget('FlashPrinter');
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-info" id="show-desc">
                <?php
                $this->widget('ShowPageContent', array('id' => 22));
                ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Pickup');?></legend>
                <div class="text-center" style="margin: 10px auto;">
                    <div class="btn-group btn-group-lg" role="group">
                        <?php
                        if(CourierTypeInternal::hasPickupTypeUser(CourierTypeInternal::WITH_PICKUP_REGULAR))
                            echo '<button type="button" class="btn btn-default" data-with-pickup-id="'.CourierTypeInternal::WITH_PICKUP_REGULAR.'">'.Yii::t('courier', 'Odbiór przez kuriera').'</button>';
                        if(S_Useful::sizeof(CourierTypeInternal::getWithPickupMapTypes()))
                            echo '<button type="button" class="btn btn-default" data-with-pickup-id="-1"  data-point-type="">'.Yii::t('courier', 'Nadanie w punkcie').'</button>';
                        if(CourierTypeInternal::hasPickupTypeUser(CourierTypeInternal::WITH_PICKUP_CONTRACT))
                            echo '<button type="button" class="btn btn-default" data-with-pickup-id="'.CourierTypeInternal::WITH_PICKUP_CONTRACT.'">'.Yii::t('courier', 'Umowa').'</button>';
                        ?>
                    </div>
                </div>


                <div data-with-pickup-container="true" style="display: none; position: relative;">

                    <div class="text-center" id="fav-container" style="<?= (!S_Useful::sizeof($favList)) ? 'display: none;' : '' ;?>">
                        <br/>
                        <?= $form->dropDownList($model->courierTypeInternal, '_fav_sp_point', $favList, ['prompt' => Yii::t('courier', 'Wybierz z ulubionych:'), 'style' => 'width: 100%; max-width: 500px; text-align: center;', 'id' => 'fav-point-list']);?>
                        <br/>
                        <?= Yii::t('courier', 'lub'); ?>
                    </div>
                    <div class="text-center" id="sp-points-container" >
                        <?= $form->dropDownList($model->courierTypeInternal,'_spPoint', CHtml::listData(SpPoints::getPoints(), 'id', 'name'), ['prompt' => Yii::t('courier', 'Wybierz punkt nadania SP:'), 'id' => 'sp-point-list', 'style' => 'width: 100%; max-width: 500px; text-align: center;']); ?>
                        <br/>
                        <?= Yii::t('courier', 'lub'); ?><br/>
                        <input type="button" class="btn btn-primary" value="<?= Yii::t('courier', 'pokaż wszystkie');?>" id="show-all-points" style="<?= ($model->courierTypeInternal->_fav_sp_point == '' && $model->courierTypeInternal->_spPoint == '' && !in_array($model->courierTypeInternal->with_pickup,[CourierTypeInternal::WITH_PICKUP_NONE, CourierTypeInternal::WITH_PICKUP_CONTRACT, CourierTypeInternal::WITH_PICKUP_REGULAR])) ? 'display: none;' :  '' ;?>"/>
                    </div>
                    <div id="map-container" style="<?= ($model->courierTypeInternal->_fav_sp_point == '' && $model->courierTypeInternal->_spPoint == '' && !in_array($model->courierTypeInternal->with_pickup,[CourierTypeInternal::WITH_PICKUP_NONE, CourierTypeInternal::WITH_PICKUP_CONTRACT, CourierTypeInternal::WITH_PICKUP_REGULAR])) ? '' :  'display: none;' ;?>">
                        <br/>
                        <div id="map-data-loader" style="display: block; position: absolute; left: 0; right: 0; top: 0; bottom: 0; background: rgba(0,0,0,0.5); z-index: 999; text-align: center;">
                            <br/>
                            <br/>
                            <br/>
                            <img src="<?= Yii::app()->baseUrl;?>/images/layout/preloader.gif"/>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-lg-4">
                                <div class="text-right">
                                    <strong><?= Yii::t('courier','Szukaj lokalizacji:');?></strong><br/>
                                    <input type="text" id="map-location" placeholder="<?= Yii::t('courier', 'ulica, miasto, kraj');?>" style="text-align: center;"/>

                                    <br/>
                                    <br/>
                                    <strong><?= Yii::t('courier','Filtruj punkty nadania:');?></strong>
                                    <ul id="point-type-list">
                                        <?php
                                        if(CourierTypeInternal::hasPickupTypeUser(CourierTypeInternal::WITH_PICKUP_NONE)) {
                                            echo '<li data-point-type="' . (CourierTypeInternal::WITH_PICKUP_NONE + 0) . '" style="'.($model->courierTypeInternal->with_pickup == CourierTypeInternal::WITH_PICKUP_NONE ? 'font-weight: bold;' : '').'">' . CourierTypeInternal::getPickupTypeName(CourierTypeInternal::WITH_PICKUP_NONE) . ' (' . Yii::t('courier', 'SP Punkt nadawczy') . ')</li>';

                                            echo ' <li>&nbsp;</li>';
                                        }
                                        foreach(CourierTypeInternal::getWithPickupMapTypes(true, true) AS $key => $name)
                                            echo '<li data-point-type="'.$key.'" style="'.($model->courierTypeInternal->with_pickup == $key ? 'font-weight: bold;' : '').'" '.($model->courierTypeInternal->with_pickup == $key ? 'data-active-type="'.$key.'"' : '').'>'.$name.'</li>';
                                        ?>
                                    </ul>
                                    <br/>
                                    <div class="alert alert-info text-left"  style="width: 100%; max-width: 450px; display: inline-block;">
                                        <strong><?= Yii::t('courier','Wybrany punkt:');?></strong>
                                        <br/>
                                        <br/>
                                        <div id="selected-point-text" class="text-center">
                                            <?php
                                            if($model->courierTypeInternal->pickup_point_text):
                                                ?>
                                                <?= $model->courierTypeInternal->pickup_point_img ? '<img src="'.$model->courierTypeInternal->pickup_point_img.'" style="margin-right: 10px;"/>' : '';?><strong><?= CourierTypeInternal::getPickupTypeName($model->courierTypeInternal->with_pickup);?></strong><br/><br/><?= $model->courierTypeInternal->pickup_point_text;?></strong>
                                            <?php
                                            else:
                                                ?>
                                                -
                                            <?php
                                            endif;
                                            ?></div>
                                        <div id="add-point-to-fav" style="<?= $model->courierTypeInternal->pickup_point_text ? '' : 'display: none;'?>">
                                            <hr/>
                                            <?= $form->checkBox($model->courierTypeInternal, 'pickup_point_add_to_fav');?><label for="add-point-to-fav"><?= Yii::t('courier','Dodaj do ulubionych');?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm- col-lg-8 text-left">
                                <?php
                                Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courier-internal-map.js', CClientScript::POS_END);
                                Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courier-internal-modal-lp_express.js', CClientScript::POS_END);
                                ?>
                                <div id="sp-points-map" style="width: 100%; height: 400px; max-width: 800px; margin: 10px 0;"></div>
                                <br/>
                                <div class="text-left">
                                    <input type="button" class="btn btn-primary btn-lg" value="<?= Yii::t('courier', 'Aktualizuj/pokaż punkty dla tego obszaru');?>" id="show-points-button" style="display: none;"/>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>



                <div style="display: ;">
                    <?php //echo $form->dropDownList($model->courierTypeInternal, 'with_pickup', CourierTypeInternal::getWithPickupList(), array('id'=>'with-pickup')); ?>
                    <?php echo $form->hiddenField($model->courierTypeInternal, 'with_pickup', array('id'=>'with-pickup')); ?>
                    <?php echo $form->hiddenField($model->courierTypeInternal, 'pickup_point_id', array('id'=>'pickup_point_id')); ?>
                    <?php echo $form->hiddenField($model->courierTypeInternal, 'pickup_point_text', array('id'=>'pickup_point_text')); ?>
                    <?php echo $form->hiddenField($model->courierTypeInternal, 'pickup_point_img', array('id'=>'pickup_point_img')); ?>
                    <?php echo $form->hiddenField($model->courierTypeInternal, '_pickup_point_country', array('id'=>'pickup_point_country')); ?>
                </div>
                <?php echo $form->error($model->courierTypeInternal,'with_pickup'); ?>
            </fieldset>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Pickup - details');?></legend>
                <div class="row text-center">
                    <?php //echo $form->checkBox($model->courierTypeInternal, 'with_pickup', array('id' => 'with-pickup', 'data-style' => 'btn-lg', 'data-bootstrap-checkbox' => true, 'data-off-label' => Yii::t('courier','Without pickup'),'data-on-label' =>  Yii::t('courier','With pickup'))); ?>
                </div><!-- row -->
                <?php
                // SPECIAL RULE FOR RUCH // 09.09.2016
                if(0 && in_array(Yii::app()->PV->get(), [PageVersion::PAGEVERSION_RUCH])):
                    ?>
                    <?php
                    $model->courierTypeInternal->_spPoint = SpPoints::CENTRAL_SP_POINT_ID;
                    echo $form->hiddenField($model->courierTypeInternal,'_spPoint');
                    ?>
                <?php
                else:
                ?>

                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_NONE;?>" style="display: none;">


                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_NONE);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                        <?php
                        if(0):
                            ?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 text-center">
                                    <?php
                                    $points = SpPoints::getPoints();
                                    echo $form->label($model->courierTypeInternal, '_spPoint', ['style' => 'width: auto;']);
                                    echo '<br/>';
                                    echo $form->dropDownList($model->courierTypeInternal, '_spPoint', CHtml::listData($points, 'id' ,'spPointsTr.title'), [
//                                        'prompt' => S_Useful::sizeof($points) > 1 ? '-- '.Yii::t('site', 'wybierz punkt z listy').' --' : null,
                                        'data-sp-points-selector' => true, 'style' => 'text-align: center;', 'tabindex' => 1]);
                                    echo $form->error($model->courierTypeInternal, '_spPoint');
                                    ?>
                                </div>
                                <div class="col-xs-12 col-sm-6" id="sp-points-details">
                                    <?php
                                    if($model->courierTypeInternal->_spPoint):

                                        $spModel = SpPoints::getPoint($model->courierTypeInternal->_spPoint);
                                        if($spModel != NULL)
                                            $this->renderPartial('//spPoints/_ajaxGetSpPointDetails', ['model' => $spModel]);
                                    endif;
                                    ?>
                                </div>
                            </div>
                        <?php
                        endif;
                        ?>
                    </div>
                    <?php
                    endif;
                    ?>
                </div>
                <?php
                if($model->courierTypeInternal->pickup_point_text == ''):
                    ?>
                    <div data-pickup-alert="-1" style="display: none;">
                        <div class="alert alert-info">
                            <div><?= Yii::t('courier', 'Wybierz typ nadania w punkcie.');?>
                            </div>
                        </div>
                    </div>
                <?php
                endif;
                ?>
                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_RUCH;?>" style="display: none;">
                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_RUCH);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                        <?php
                        if(0):
                            ?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 text-center">
                                    <input type="button" class="btn btn-primary" value="Wyszukaj najblizsze kioski RUCHU" id="ruch_point_suggester"/>
                                </div>
                                <div class="col-xs-12 col-sm-6" id="sp-points-details">
                                    <div id="ruch-suggester-address" style="display: none;"><div class="well"></div></div>
                                </div>
                            </div>
                        <?php
                        endif;
                        ?>
                    </div>
                </div>
                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_REGULAR;?>" style="display: none;">
                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_REGULAR);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                    </div>
                </div>
                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_CONTRACT;?>" style="display: none;">
                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_CONTRACT);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                    </div>
                </div>
                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_RM;?>" style="display: none;">
                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_RM);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                    </div>
                </div>
                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_LP_EXPRESS;?>" style="display: none;">
                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_LP_EXPRESS);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                    </div>
                </div>
                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_UPS;?>" style="display: none;">
                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_UPS);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                    </div>
                </div>
                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_INPOST;?>" style="display: none;">
                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_INPOST);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                    </div>
                </div>
                <div data-pickup-alert="<?= CourierTypeInternal::WITH_PICKUP_DHLDE;?>" style="display: none;">
                    <div class="alert alert-info">
                        <div><?php
                            $pageId = CourierTypeInternal::getPageIdWithDescForPickupType(CourierTypeInternal::WITH_PICKUP_DHLDE);
                            $this->widget('ShowPageContent', array('id' => $pageId));
                            ?>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Parametry paczki');?> <a href="#" id="show-requirements" tabindex="-1">(?)</a></legend>
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="alert alert-info" id="requirements" style="display: none;">
                            <p><?php echo Yii::t('courier','Paczka musi spełniać następujące wymogi');?>:</p>
                            <ul>
                                <li><?php echo $model->getAttributeLabel('package_weight'); ?> < <?php echo CourierTypeInternal::MAX_PACKAGE_WEIGHT;?> <?php echo Courier::getWeightUnit();?></li>
                                <li><?php echo $model->getAttributeLabel('package_size_l'); ?> < <?php echo S_PackageMaxDimensions::getPackageMaxDimensions()['l'];?> <?php echo Courier::getDimensionUnit();?></li>
                                <li><?php echo $model->getAttributeLabel('package_size_d'); ?> < <?php echo S_PackageMaxDimensions::getPackageMaxDimensions()['d'];?> <?php echo Courier::getDimensionUnit();?></li>
                                <li><?php echo $model->getAttributeLabel('package_size_w'); ?> < <?php echo S_PackageMaxDimensions::getPackageMaxDimensions()['w'];?> <?php echo Courier::getDimensionUnit();?></li>
                                <li><?php echo Yii::t('courier','Suma długości i obwodu <');?> <?php echo S_PackageMaxDimensions::getPackageMaxDimensions()['combination'];?> <?php echo Courier::getDimensionUnit();?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <?php echo $form->labelEx($model,'packages_number', ['style' => 'width: auto;']); ?><br/>
                    <?php echo $form->dropDownList($model, 'packages_number', S_Useful::generateArrayOfItemsNumber(CourierTypeInternal::NUMBER_OF_MULTIPLE_PACKAGES_ONCE), ['style' => 'text-align: center; width: 100px;', 'data-packages-no' => true, 'data-max-no' => CourierTypeInternal::NUMBER_OF_MULTIPLE_PACKAGES_ONCE]); ?>
                    <?php echo $form->error($model,'packages_number'); ?>
                </div>
                <br/>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <div class="row">
                            <div class="col-xs-12 text-center"><strong><?= Yii::t('courier', 'Waga');?> [<?php echo Courier::getWeightUnit();?>]</strong></div>
                            <div class="col-xs-12 col-sm-4 col-sm-offset-1 text-center">
                                <?php echo $form->textField($model, '_package_weight_total', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-weight-total' => true)); ?><?= Yii::t('courier', 'Łączna waga');?></div>
                            <div class="col-xs-12 col-sm-2 text-center hidden-xs" style="line-height: 40px; font-weight: bold; font-size: 1.2em; color: gray;"> = <span data-packages-no-show="true">1</span> &times; </div>
                            <div class="col-xs-12 col-sm-4 text-center"><?php echo $form->textField($model, 'package_weight', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-weight' => true, 'data-package-weight' => true, 'data-dependency' => 'true', 'data-max-weight' => CourierTypeInternal::MAX_PACKAGE_WEIGHT)); ?><?= Yii::t('courier', 'Pojedyncza paczka');?></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <?php echo $form->error($model,'package_weight'); ?>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-xs-12 text-center"><strong><?= Yii::t('courier', 'Wymiary');?> [<?php echo Courier::getDimensionUnit();?>]</strong></div>
                    <div class="col-xs-12 col-sm-2 col-sm-offset-3 text-center">
                        <?php echo $form->textField($model, 'package_size_l', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => true, 'data-dependency' => 'true')); ?><?= Yii::t('courier', 'długość');?>
                    </div>
                    <div class="col-xs-12 col-sm-2 text-center"><?php echo $form->textField($model, 'package_size_w', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => true, 'data-dependency' => 'true')); ?><?= Yii::t('courier', 'szerokość');?></div>
                    <div class="col-xs-12 col-sm-2 text-center"><?php echo $form->textField($model, 'package_size_d', array('maxlength' => 6, 'style' => 'text-align: center;', 'data-dimensions' => true, 'data-dependency' => 'true')); ?><?= Yii::t('courier', 'wysokość');?></div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3 ">
                        <?php echo $form->error($model,'package_size_l'); ?>
                        <?php echo $form->error($model,'package_size_w'); ?>
                        <?php echo $form->error($model,'package_size_d'); ?>
                    </div>
                </div>
                <?php
                if(0):
                    ?>
                    <br/>
                    <div class="row text-center">
                        <?= Yii::t('courier', 'Waga gabarytowa pojedynczej paczki:');?> <span id="_dw_show" style="font-weight: bold;"></span> <?php echo Courier::getWeightUnit();?>
                    </div>
                <?php
                endif;
                ?>
                <br/>
                <div class="row text-center" style="font-size: 0.9em;">
                    <?= Yii::t('courier', 'Maksymalne wymiary oraz waga paczki mogą być dokładniej zweryfikowane w zależności od podanych parametrów w kolejnym kroku.');?>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <?php
        $this->Widget('AddressDataSwitch', ['attrBaseName' => 'CourierTypeInternal_AddressData']);
        ?>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('courier', 'Nadawca');?></legend>
                <?php
                $this->renderPartial('_addressData/_form',
                    array(
                        'model' => $model->senderAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_SENDER),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => UserContactBook::TYPE_SENDER,
                        'type' => UserContactBook::TYPE_SENDER,
//                    'focusFirst' => true,
                    ));
                ?>

                <div class="row" style="margin-top: 10px;">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'_sendLabelOnMail'); ?>
                        <?php echo $form->checkBox($model,'_sendLabelOnMail', ['data-bootstrap-checkbox' => true, 'data-default-class' => 'btn-sm', 'data-on-class' => 'btn-sm btn-primary', 'data-off-class' => 'btn-sm btn-default', 'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak')]); ?>
                        <?php echo $form->error($model, '_sendLabelOnMail'); ?>
                    </div>
                </div><!-- row -->
            </fieldset>
        </div>
        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?= Yii::t('courier', 'Odbiorca');?></legend>
                <div class="alert alert-info" style="<?= $deliveryOperatorBrokerId != CourierOperator::BROKER_DHLDE ? 'display: none;' : '';?>" data-delivery-broker-id-show="<?= CourierOperator::BROKER_DHLDE;?>">
                    <?php $this->renderPartial('/_common/_dhlde_packstation');?>
                </div>
                <?php
                $this->renderPartial('_addressData/_form',
                    array(
                        'model' => $model->receiverAddressData,
                        'listAddressData' => UserContactBook::listContactBookItems(UserContactBook::TYPE_RECEIVER),
                        'form' => $form,
                        'noEmail' => false,
                        'i' => UserContactBook::TYPE_RECEIVER,
                        'type' => UserContactBook::TYPE_RECEIVER,
//                    'focusFirst' => true,
                    ));
                ?>
            </fieldset>

            <div id="inpost-receiver-part" style="<?= ($deliveryOperatorBrokerId != GLOBAL_BROKERS::BROKER_INPOST_SHIPX) ? 'display: none;' : '';?>" data-delivery-broker-id-show="<?= GLOBAL_BROKERS::BROKER_INPOST_SHIPX;?>">
                <?php
                $this->renderPartial('/_common/_inpost_paczkomat',
                    [
                        'model' => $model,
                        'submodel' => $model->courierTypeInternal,
                        'form' => $form,
                    ]);
                ?>
            </div>

            <div id="ruch-receiver-part" style="display: none;">

                <?php
                $this->renderPartial('/_common/_ruch',
                    [
                        'model' => $model,
                        'submodel' => $model->courierTypeInternal,
                        'form' => $form,
                        'addressDataModelName' => 'CourierTypeInternal_AddressData',
                    ]);
                ?>

            </div>


            <div id="lp-express-receiver-part" style="<?= $deliveryOperatorBrokerId != CourierOperator::BROKER_LP_EXPRESS ? 'display: none;' : '';?>" data-delivery-broker-id-show="<?= CourierOperator::BROKER_LP_EXPRESS;?>">
                <fieldset>
                    <legend><?= Yii::t('courier', 'LP Express terminal');?></legend>

                    <div class="alert alert-info text-center">

                        <p><?= Yii::t('courier', 'Wybierz punkt, do którego ma być dostarczona paczka.');?></p>
                        <?php echo $form->error($model->courierTypeInternal,'_lp_express_point_address'); ?>
                        <?php echo $form->textField($model->courierTypeInternal, '_lp_express_receiver_point_address', ['id' => '_lp_express_receiver_point_address', 'readonly' => true, 'placeholder' => Yii::t('courier', '-- kliknij, aby wybrać punkt --'), 'style' => 'text-align: center; cursor: pointer; width: 80%; max-width: 500px', 'data-load-modal' => CourierOperator::BROKER_LP_EXPRESS]); ?>
                        <?php echo $form->error($model->courierTypeInternal,'_lp_express_receiver_point'); ?>

                        <?php echo $form->hiddenField($model->courierTypeInternal, '_lp_express_receiver_point', ['id' => '_lp_express_receiver_point']); ?>
                    </div>

                </fieldset>

            </div>

        </div>
    </div>
    <?php
    if(!Yii::app()->user->model->getHidePrices()):
        ?>
        <div class="row" data-price="true" data-continue="true" style="<?= ($noCarriage OR $noPickup OR $noPrice) ? 'display: none;' : '';?>">
            <div class="col-xs-12 col-sm-12">
                <fieldset>
                    <legend><?= Yii::t('courier', 'Cena');?></legend>

                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4">
                            <table class="detail-view text-center" style="width: 100%;">
                                <tr class="odd">
                                    <th style="text-align: center;"><?php echo Yii::t('courier','Cena za 1 paczkę');?></th>
                                    <th style="text-align: center;"><?php echo Yii::t('courier','Cena łączna');?></th>
                                </tr>
                                <tr class="even">
                                    <td>
                                        <div class="internal-form-price" id="price-each"><?= $priceEach;?></div>
                                    </td>
                                    <td>
                                        <div class="internal-form-price internal-form-price-total" id="price-total"><?= $priceTotal;?></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <p style="text-align: center; font-size: 0.9em; padding-top: 10px"><?= Yii::t('courier', 'Cena brutto. Cena nie uwzględnia indywidualnych rabatów użytkownika oraz dodatków do paczki.');?></p>
                </fieldset>
            </div>
        </div>
    <?php
    endif;
    ?>
    <?php
    if(Yii::app()->user->model->getCourierReturnAvailable()):
        ?>
        <div class="row" data-return="true" <?= CourierTypeReturn::isAutomaticReturnAvailable($model->receiverAddressData->country_id) ? '' : 'style="display: none;"';?>>
            <div class="col-xs-12">
                <fieldset>
                    <legend><?= Yii::t('courier', 'Etykieta zwrotna'); ?></legend>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->labelEx($model,'_generate_return'); ?>
                            <?php echo $form->checkBox($model, '_generate_return', array('data-off-label' => Yii::t('courier','Nie'), 'data-on-label' =>  Yii::t('courier','Tak'), 'data-bootstrap-checkbox' =>true, 'data-default-class' => 'btn-md', 'data-on-class' => 'btn-md btn-primary', 'data-off-class' => 'btn-md btn-default', 'data-generate-return' => true)); ?>
                            <?php echo $form->error($model,'_generate_return'); ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    <?php
    endif;
    ?>
    <?php
    // @ 26.01.2017
    // DEPRECIATED - FROM AVAILABLE ONLY FOR REGISTERED USERS
    //    if((!Yii::app()->user->isGuest
    ////        && Yii::app()->PriceManager->getCurrencySymbol() == Currency::PLN
    //    )
    //    ):
    ?>
    <div data-continue="true">
        <div class="row" data-cod="false" <?= !$codCurrency ? '' : 'style="display: none;"';?>>
            <div class="col-xs-12">
                <fieldset>
                    <legend><?= Yii::t('courier', 'COD');?></legend>
                    <div class="alert alert-warning text-center"><?= Yii::t('courier', 'Opcja COD nie jest dostępna na tej trasie!');?></div>
                </fieldset>
            </div>
        </div>
        <div class="row" data-cod="true" <?= !$codCurrency ? 'style="display: none;"' : '';?>>
            <div class="col-xs-12">
                <fieldset>
                    <legend><?= Yii::t('courier', 'COD');?></legend>
                    <div id="client-cod-block">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php echo $form->labelEx($model,'_client_cod'); ?>
                                <?php echo $form->checkBox($model, '_client_cod', array('id' => '_client_cod',  'data-off-label' => Yii::t('courier','Nie'),'data-on-label' =>  Yii::t('courier','Tak'))); ?>
                                <?php echo $form->error($model,'_client_cod'); ?>
                            </div>
                        </div>
                        <div class="row" id="_client_cod_value_row" style="<?= !$model->_client_cod ? 'display: none;' : ''?>">
                            <br/>
                            <div class="col-xs-12">
                                <?php echo $form->labelEx($model,'cod_value'); ?>
                                <?php echo $form->textField($model, 'cod_value', ['style' => 'border-top-right-radius: 0; border-bottom-right-radius: 0;']); ?>
                                <div style="display: inline-block; background: #eeeeee; border: 1px solid #cccccc; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height: 28px; line-height: 26px; padding: 0 5px; margin-left: -5px;" id="cod-currency-name"><?= $codCurrency ? $codCurrency : '-';?></div>
                                <?php echo $form->error($model,'cod_value'); ?>

                            </div>
                            <div class="col-xs-12">
                                <?php echo $form->labelEx($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccount'); ?>
                                <?php echo $form->textField($model->senderAddressData, '['.UserContactBook::TYPE_SENDER.']bankAccount', array('maxlength' => 50, 'data-bank-account-target' => true)); ?> <input type="button" class="btn btn-xs btn-primary" value="<?= Yii::t('courier', 'Z ustawień');?>" data-copy-bank-account="<?= !Yii::app()->user->isGuest ? Yii::app()->user->model->getBankAccount() : '';?>"/>
                                <?php echo $form->error($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccount'); ?>
                                <div style="font-style: italic; font-size: 0.8em; text-align: center;"><?= Yii::t('courier', 'Nadawca przesyłki musi być właścicielem powyższego konta bankowego.');?></div>
                            </div>
                            <div class="col-xs-12">
                                <?php echo $form->labelEx($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                                <?php echo $form->checkBox($model->senderAddressData, '['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                                <?php echo $form->error($model->senderAddressData,'['.UserContactBook::TYPE_SENDER.']bankAccountSaveInContactBook'); ?>
                            </div>
                            <div class="col-xs-12">
                                <p style="text-align: center; font-size: 0.9em;"><?= Yii::t('courier','Paczka będzie automatycznie ubezpieczona do kwoty równiej kwocie COD.');?></p>
                            </div>
                        </div>
                    </div>

                </fieldset>
            </div>
        </div>
        <?php
        //    endif;
        ?>
    </div>
    <div class="row"  data-continue="true">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Package content');?></legend>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'package_content'); ?>
                        <?php echo $form->textField($model, 'package_content'); ?>
                        <?php echo $form->error($model,'package_content'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'package_value'); ?>
                        <?php echo $form->textField($model, 'package_value', ['style' => 'border-top-right-radius: 0; border-bottom-right-radius: 0;']); ?> <?php echo $form->dropDownList($model, 'value_currency', Courier::getCurrencyList());  ?>
                        <?php echo $form->error($model,'package_value'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model->courierTypeInternal,'_package_wrapping'); ?>
                        <?php echo $form->dropDownList($model->courierTypeInternal, '_package_wrapping', User::getWrappingList(), ['data-package-wrapping' => 'true']); ?>
                        <?php echo $form->error($model->courierTypeInternal,'_package_wrapping'); ?>
                    </div>
                </div>
                <?php
                if(!Yii::app()->user->isGuest && Yii::app()->user->getModel()->getCourierRefPrefix()):
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->labelEx($model,'ref'); ?>
                            <?php echo $form->textField($model, 'ref'); ?>
                            <?php echo $form->error($model,'ref'); ?>
                        </div>
                    </div>
                <?php
                endif;
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'note1'); ?>
                        <?php echo $form->textField($model, 'note1'); ?>
                        <?php echo $form->error($model,'note1'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->labelEx($model,'note2'); ?>
                        <?php echo $form->textField($model, 'note2'); ?>
                        <?php echo $form->error($model,'note2'); ?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row" data-continue="true" style="<?= ($noCarriage OR $noPickup) ? 'display: none;' : '';?>">
        <div class="col-xs-12 col-sm-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Dodatki');?></legend>
                <div id="additions-placeholder">
                    <?= $additionsHtml; ?>
                </div>
            </fieldset>
        </div>
    </div>
    <p class="note">
        <?php echo Yii::t('app', 'Pola oznaczone {star} są wymagane', array('{star}' => '<span class="required">*</span>')); ?>.
    </p>
    <div class="row" style="padding: 10px 0;">
        <div class="col-xs-12">
            <fieldset>
                <legend><?= Yii::t('courier', 'Kontynuuj');?></legend>
                <div data-not-available="true" style="<?= ($noCarriage) ? '' : 'display: none;';?>">
                    <div class="alert alert-warning text-center">
                        <?= Yii::t('courier', 'Niestety, ale nie możemy automatycznie obsłużyć tego kierunku.{br}{br}Skontaktuj się z nami, aby otrzymać indywidualna ofertę!', ['{br}' => '<br/>']);?>
                    </div>
                </div>
                <div data-pickup-not-available="true" style="<?= ($noPickup) ? '' : 'display: none;';?>">
                    <div class="alert alert-warning text-center">
                        <?= Yii::t('courier', 'Niestety, ale nie możemy automatycznie obsłużyć tego kierunku z opcją odbioru. Możesz jednak wybrać opcję bez odbioru paczki.{br}{br}Skontaktuj się z nami, aby otrzymać indywidualna ofertę!', ['{br}' => '<br/>']);?>
                    </div>
                </div>
                <div class="text-center">
                    <?php echo TbHtml::submitButton(Yii::t('app', 'Dalej'), array('name' => 'summary', 'class' => 'btn-lg btn-primary', 'disabled' => ($noCarriage OR $noPickup) ? 'disabled' : '', 'data-continue-button' => 'true')); ?>
                </div>
            </fieldset>
        </div>
    </div>
    <?= $form->hiddenField($model, 'source'); // for cloning ?>
    <?php $this->endWidget(); ?>
</div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/courier-internal-modal-lp_express.js');
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modal-content">
        </div>
    </div>
</div>


