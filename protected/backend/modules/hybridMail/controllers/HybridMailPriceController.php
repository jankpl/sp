<?php

class HybridMailPriceController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }



    /* @parm int id HybridMail Group Id */
    /* @parm int user_group User Group Id */
    public function actionHybridMailPrice($id, $user_group = NULL)
    {

        $checkCountryGroup = CountryGroup::model()->findByPk($id);
        if($checkCountryGroup == NULL)
            throw new CHttpException(404);

        if($user_group != NULL)
        {
            $checkUserGroup = UserGroup::model()->findByPk($user_group);
            if($checkUserGroup == NULL)
                throw new CHttpException(404);
        }

        $groupEditURLs = [];
        foreach(UserGroup::model()->findAll() AS $item)
        {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select('*');
            $cmd->from('hybrid_mail_price_has_group');
            $cmd->where('country_group = :group', array(':group' => $id));
            $cmd->andWhere('user_group_id = :user_group', array(':user_group' => $item->id));
            $result = $cmd->queryRow();

            $temp = [];
            $temp['name'] = $item->name.' ('.($result?'yes':'no').')';
            $temp['url'] = Yii::app()->createAbsoluteUrl('/hybridMail/hybridMailPrice/hybridMailPrice', array('id' => $id, 'user_group' => $item->id));
            array_push($groupEditURLs, $temp);
        }

        if($user_group == NULL)
        {
            $groupActionDelete = true;
            $groupActionListView = $this->renderPartial('_groupActionList', array(
                    'groupEditUrls' => $groupEditURLs
                ),
                true);
        } else {
            $groupActionDelete = true;

        }


        $this->updatePrice($_POST, $id, $user_group, $groupActionListView,$groupActionDelete);



    }

    protected function updatePrice($post, $country_group, $user_group, $groupActionListView = NULL, $groupActionDelete = false)
    {


        /* @var $model HybridMailPrice*/
        $model = HybridMailPrice::loadOrCreatePrice($country_group, $user_group);
        if($model->id == NULL)
            $groupActionDelete = null;


        $hybridMailPriceValues = $model->loadValueItems();

        $this->performAjaxValidation($model, 'hybrid-mail-price-form');

        if (isset($post['HybridMailPrice'])) {


            // Clicked button for just sort
            if(isset($post['delete']))
            {
                if($model->deleteWithRelated())
                {
                    Yii::app()->user->setFlash('success','Pricing was deleted!');
                } else {
                    Yii::app()->user->setFlash('error','Pricing was  NOT deleted!');
                }
                $this->redirect(array('/hybridMail/hybridMailPrice/view','id' => $country_group));
                return;
            }

            $hybridMailPriceValues = Array();

            $model->setAttributes($post['HybridMailPrice']);

            $transaction = Yii::app()->db->beginTransaction();
            $error = false;

            $cmd = Yii::app()->db->createCommand();
            $cmd->delete('hybrid_mail_price_value', 'hybrid_mail_price_id = :hybrid_mail_price_id',
                array(':hybrid_mail_price_id' => $model->id));

            $dataArray = [];
            foreach($post['PriceValue'] AS $key => $item)
                $dataArray[$key] = array_merge($post['PriceValue'][$key], $post['PriceValue'][$key]);


            if($model->id == NULL)
            {
                $model->save(true,'id');


                $model2 = new HybridMailPriceHasGroup();
                $model2->country_group = $country_group;
                $model2->hybrid_mail_price_item = $model->id;
                $model2->user_group_id = $user_group;
                if(!$model2->save())
                    $errors = true;


            }

            $maxWeightFound = false;
            foreach($dataArray AS $key => $item)
            {

                $hybridMailPriceValue = new HybridMailPriceValue;
                $hybridMailPriceValue->firstPagePrice = Price::generateByPost($item['firstPagePrice']);
                $hybridMailPriceValue->nextPagePrice = Price::generateByPost($item['nextPagePrice']);

                $hybridMailPriceValue->first_page_price = $hybridMailPriceValue->firstPagePrice->saveWithPrices();
                $hybridMailPriceValue->next_page_price = $hybridMailPriceValue->nextPagePrice->saveWithPrices();

                $hybridMailPriceValue->hybrid_mail_price_id = $model->id;

                $hybridMailPriceValues[$key] = $hybridMailPriceValue;

                if(!$hybridMailPriceValues[$key]->save())
                    $error = true;

            }

            if(!$error AND $model->save())
            {
                $transaction->commit();
                $this->redirect(array('/hybridMail/hybridMailPrice/view','id' => $country_group));
            }
            $transaction->rollback();
        }

        $this->render('update', array(
            'model' => $model,
            'hybridMailPriceValues' =>  $hybridMailPriceValues,
            'groupActionListView' => $groupActionListView,
            'groupActionDelete' => $groupActionDelete,
            'userGroup' => $user_group,
            'countryGroup' => $country_group,
        ));

    }

    public function actionView($id) {

        $model = CountryGroup::model()->findByPk($id);

        if($model === NULL)
            throw new CHttpException(404);

        $this->render('view',array(
            'model' => $model,
        ));

    }

    public function actionAdmin() {
        $model = new CountryGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['CountryGroup']))
            $model->setAttributes($_GET['CountryGroup']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }
}