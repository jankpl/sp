<?php


$this->menu=array(
    array('label'=>'List ' . $model->label(2), 'url'=>array('index')),
    array('label'=>'Create ' . $model->label(), 'url'=>array('create')),
    array('label'=>'Update ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
    array('label'=>'Delete ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<h3>Podsumowania</h3>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
        'date_entered',
    ),
)); ?>

<h3>Wersje językowe</h3>

<?php foreach($model->postalStatTrs AS $item):

    echo '<h5>'.$item->language->name.'</h5>';

    $this->widget('zii.widgets.CDetailView', array(
        'data' => $item,
        'attributes' => array(
            'short_text',
            'full_text',
        ),
    ));


endforeach;
?>