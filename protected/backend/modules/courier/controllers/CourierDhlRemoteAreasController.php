<?php

class CourierDhlRemoteAreasController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('@'),
                'expression'=>'0 >= Yii::app()->user->authority',
            ),
            array('deny',  // block rest of actions
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'CourierDhlRemoteAreas'),
        ));
    }


    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'CourierDhlRemoteAreas');
        $model->scenario = 'update';

        if (isset($_POST['CourierDhlRemoteAreas'])) {

            $model->attributes = $_POST['CourierDhlRemoteAreas'];

            $noPrices = true;

            if(is_array($_POST['PriceValue']) && S_Useful::sizeof($_POST['PriceValue']))
            {
                foreach($_POST['PriceValue'] AS $item)
                {
                    if($item['value'] != '')
                        $noPrices = false;
                }
            }

            if($noPrices)
            {
                $model->price = NULL;

            } else {
                $model->price = Price::generateByPost($_POST['PriceValue']);
            }

            if ($model->saveWithPrice()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionIndex() {

        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $model = new CourierDhlRemoteAreas('search');
        $model->unsetAttributes();

        if (isset($_GET['CourierDhlRemoteAreas']))
            $model->setAttributes($_GET['CourierDhlRemoteAreas']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            try
            {
                $this->loadModel($id, 'CourierDhlRemoteAreas')->delete();
            }
            catch (Exception $ex)
            {
                throw new CHttpException(403, "Nie mo�na usun�� tego wpisu!");
            }
            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

}