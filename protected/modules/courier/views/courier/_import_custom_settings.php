<?php
/* @var $data array */
/* @var $separator integer */
/* @var $name string */
/* @var $id integer */
/* @var $type integer */
?>
<div class="modal-header"><strong><?= Yii::t('courier', 'Ooe Import Settings');?></strong></div>
<div class="modal-body">
    <form>
        <div>
            <div class="form">
                <?= CHtml::label(Yii::t('courier','Setting name'), 'setting-name');?>
                <?= CHtml::textField('settings[name]', $name, ['id' => 'setting-name']);?>
            </div>
            <br/>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#cfo" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('courier', 'Custom fields order');?></a></li>
                <li role="presentation"><a href="#cds" aria-controls="profile" role="tab" data-toggle="tab"><?= Yii::t('courier', 'Custom data separator');?></a></li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="cfo">
                    <br/>
                    <p><?= Yii::t('courier', 'Przeciągnij pola, aby ustawić ich kolejnosć w importowanym pliku');?></p>
                    <div class="alert alert-info" style="max-width: 300px; margin: 0 auto;">
                        <div style="overflow: auto;">
                            <div style="float: left; width: 30px;">
                                <?php
                                $alphabet = range('A', 'Z');
                                $alphabetSize = S_Useful::sizeof($alphabet);

                                for($i = 0; $i < S_Useful::sizeof($data); $i++)
                                {

                                    $letter = $alphabet[$i];
                                    $prefix = floor($i / $alphabetSize);

                                    $j = $i - $prefix * $alphabetSize;
                                    if($prefix) {
                                        $prefix = $alphabet[$prefix - 1];
                                    }
                                    else
                                        $prefix = '';

                                    echo '<div style="padding: 4px 0; text-align: right; margin-bottom: 1px;">'.$prefix.$alphabet[$j].':</div>';
                                }

                                ?>
                            </div>
                            <div style="float: left;">
                                <ul id="sortable">
                                    <?php
                                    if(is_array($data))
                                        foreach($data AS $key => $item):
                                            $item = str_replace('_', ' ', $item);

                                            if(Yii::app()->PV->get() == PageVersion::PAGEVERSION_CQL)
                                                $item = str_replace(['note1','note2'], ['Order ID', 'Optional Reference'], $item);

                                            $item = ucwords($item);

                                            ?>
                                            <li class="ui-state-default" id="attr-<?= $key;?>" title="<?= Yii::t('site', 'Przytrzymaj i przeciągnij');?>"><?= $item;?></li>
                                        <?php
                                        endforeach;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="cds">
                    <br/>
                    <p><?= Yii::t('courier', 'Wybierz separator oddzielający dane w pliku .csv');?></p>
                    <div class="text-center">
                        <?= CHtml::dropDownList('settings[separator]', $separator, F_OoeImport::listOfSeparators(), ['id' => 'data-separator', 'style' => 'text-align: center;']); ?>
                    </div>
                </div>
            </div>
        </div>
        <input name="settings[order]" type="hidden" id="custom-order-data" value="" />
        <input name="id" type="hidden" value="<?= $id;?>" />
        <input name="type" type="hidden" value="<?= $type;?>" />
    </form>
</div>
<div class="modal-footer">
    <?php if($id): ?>
        <button type="button" class="btn btn-danger btn-xs" data-custom-settings="delete"><?= Yii::t('site', 'Delete setting');?></button>
    <?php endif; ?>
    <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('site', 'Cancel');?></button>
    <button type="button" class="btn btn-primary btn-lg" data-custom-settings="save"><?= Yii::t('courier', 'Save');?></button>
</div>
